<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::post("/getsirelocityinfo", 'APIController@getSireloCityInfo');
//Route::post("/getallcitiesincountry", 'APIController@getAllCitiesInCountry');
//Route::post("/getamountofcustomerspersirelo", 'APIController@getAmountOfCustomersPerSirelo');
//Route::post("/getcityinstateinfo", 'APIController@getCityInStateInfo');
//Route::post("/getamountofcitiesnearby", 'APIController@getAmountOfCitiesNearby');
//Route::post("/getnearbycitysearch", 'APIController@getNearbyCitySearch');
//Route::post("/getcitysearch", 'APIController@getCitySearch');
//Route::post("/getkeybycityname", 'APIController@getKeyByCityName');
//Route::post("/getcountries", 'APIController@getCountries');
//Route::post("/getcountryname", 'APIController@getCountryName');
//Route::post("/getcustomer", 'APIController@getCustomer');
//Route::post("/getcustomerforward", 'APIController@getCustomerForward');
//Route::post("/getcustomersincity", 'APIController@getCustomersInCity');
//Route::post("/getcustomerinsurances", 'APIController@getCustomerInsurances');
//Route::post("/getcustomermemberships", 'APIController@getCustomerMemberships');
//Route::post("/getcustomerobligations", 'APIController@getCustomerObligations');
//Route::post("/getnearbycustomers", 'APIController@getNearbyCustomers');
//Route::post("/getcustomeroffices", 'APIController@getCustomerOffices');
//Route::post("/getsearchedcustomers", 'APIController@getSearchedCustomers');
//Route::post("/getkeybycustomername", 'APIController@getKeyByCustomerName');
//Route::post("/getcustomerwidget", 'APIController@getCustomerWidget');
//Route::post("/getcustomersmovetocountry", 'APIController@getCustomersMoveToCountry');
//Route::post("/getfilteredcustomers", 'APIController@getFilteredCustomers');
//Route::post("/getfilteredcustomeroffices", 'APIController@getFilteredCustomerOffices');
//Route::post("/getinsurances", 'APIController@getInsurances');
//Route::post("/getobligations", 'APIController@getObligations');
////Route::post("/getmemberships", 'APIController@getMemberships'); TODO: What the hekkie
//Route::post("/getcitykeybykeyorcity", 'APIController@getCityKeyByKeyOrCity');
//Route::post("/getcustomerreviews", 'APIController@getCustomerReviews');
//Route::post("/checkstateandcitykey", 'APIController@checkStateAndCityKey');
//Route::post("/getsirelohyperlinks", 'APIController@getSireloHyperlinks');
//Route::post("/getstateinfo", 'APIController@getStateInfo');
//Route::post("/checkstateiscity", 'APIController@checkStateIsCity');
//Route::post("/gettopcities", 'APIController@getTopCities');
//Route::post("/gettopreviews", 'APIController@getTopReviews');
//Route::post("/getstates", 'APIController@getStates');
//Route::post("/getcitiesinstate", 'APIController@getCitiesInState');
//Route::post("/posttest", 'APIController@postTest' );


Route::post("/userexport", 'APIController@userExport' );
Route::post("/postscrapedata", 'APIController@postScrapeData' );
Route::post("/postdata_mobilityex", 'MobilityexFetchedDataController@saveGatheredData');
Route::post("/getsirelosetting", 'APIController@getSireloSetting');


/*Route::middleware('auth:api')->get('/user', function (Request $request) {

});*/
