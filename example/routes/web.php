<?php

use App\Data\CustomerRestrictions;
use App\Data\CustomerType;
use App\Functions\System;
use App\Models\ContactPerson;
use App\Models\Country;
use App\Models\Customer;
use App\Models\Language;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//API
Route::get('admin/automatic', 'AutomaticMatchController@analyseLead');


Route::get('/', function () {
	return Redirect::to("/dashboard");
});

//Redirect old login page
Route::get('/erplogin', function () {
	return Redirect::to("/login");
});


//Mover data
Route::resource('moverdata', 'MoverDataController')->middleware('auth');

Route::post('/2fa', function () {
    $userLogin = new \App\Models\UserLogin();
    $userLogin->uslo_us_id = Auth::user()->id;
    $userLogin->uslo_ap_id = 1;
    $userLogin->uslo_timestamp = date("Y-m-d H:i:s");
    $userLogin->uslo_ip_address = System::getIP();
    $userLogin->uslo_user_agent = $_SERVER['HTTP_USER_AGENT'];
    $userLogin->save();

    $user = \App\Models\User::where("us_id", Auth::user()->id)->first();
    if ($user->us_force_change_password == 0)
    {
        return Redirect::to("/force_pass_change");
    }
    else {
        return redirect(URL()->previous());
    }

})->name('2fa')->middleware('2fa');

Route::get('/2fa', function () {

    return Redirect::to("/dashboard");
});

Route::get('gather_customers_for_python', 'APIController@gatherCustomersForPython');

// Do middleware on all of the containing Route's
Route::middleware(['auth','2fa'])->group( function () {

    Route::get('force_pass_change/', 'UserController@forcePasswordChange');
    Route::post('force_pass_change/{user_id}', 'UserController@forcePasswordChangePost');

    Route::resource('dashboard', 'DashboardController');
	//	Route::get('/home', 'DashboardController@index')->name('dashboard');

	//Customers, affiliates and service providers
	Route::group([
                    ['middleware' => ['permission:customers']],
                    ['middleware' => ['permission:conference']]
                ], function () {


        Route::get('reports_usage', 'AdminController@reportsUsage');

        Route::get('requests/rate', 'RequestRateController@index');
        Route::post('requests/rate', 'RequestRateController@filteredindex');

        Route::get('requests/sync_feedback', 'RequestRateController@syncFeedback');

        Route::get('requests/{request_id}/rate/{list}', 'RequestsController@raterequest');
        Route::post('requests/{request_id?}/rate/{list?}', 'RequestsController@submitrate')->name("requests.submitrate");

        Route::get('requests/{request_id}/feedback/{list}', 'RequestsController@feedbackrequest');
        Route::post('requests/{request_id}/feedback/{list}', 'RequestsController@submitfeedback')->name("requests.submitfeedback");
        Route::post('requests/{request_id}/feedback/-1', 'RequestsController@closefeedback');

        Route::get('customers/{customer_id}/remove_inc_credit_limit/', 'CustomersController@removeIncreasedCreditLimit');
        Route::get('customers/{customer_id}/increase_credit_limit/', 'CustomersController@increaseCreditLimit');
        Route::post('customers/{customer_id}/increase_credit_limit/', 'CustomersController@submitIncreaseCreditLimit');

        Route::get('customers/prospects', 'CustomersController@prospectindex');
        //Customer and nested controllers
        Route::resource('customers', 'CustomersController' );

        Route::get('customersearch', 'CustomerSearchController@search');
        Route::post('customersearch', 'CustomerSearchController@searchResult');

        //Customer Add
        Route::post('customers', 'CustomersController@store');
        Route::post('mobilityex_validate', 'MobilityexFetchedDataController@addCustomerStore');

        Route::resource('customers.contactperson', 'ContactPersonController');
        Route::resource('customers.contactperson.application_user', 'ApplicationUserController');
        Route::resource('customers.customerremark', 'CustomerRemarkController');
        Route::resource('customers.customerstatus', 'CustomerStatusController');
        Route::resource('customers.customerdocument', 'CustomerDocumentController');
        Route::resource('customers.creditcard', 'CreditCardController');
        Route::resource('customers.customercredit', 'CustomerCreditController');
        Route::resource('customers.customeroffice', 'CustomerOfficeController');
        Route::resource('customers.child', 'CustomerChildController');
        Route::resource('customers.customerportal', 'CustomerPortalController');
        Route::resource('customers.subscription', 'CustomerSubscriptionController');
        Route::resource('customers.customerpause', 'CustomerPauseController');
        Route::resource('customers.customerplannedcall', 'CustomerPlannedCallController');


        Route::resource('customers.survey2', 'Survey2Controller');
        Route::resource('customers.survey1', 'Survey1Controller');
        //Route::resource('customers.conversion_tools', 'ConversionToolsController');

        Route::get('customers/{customer_id}/conversion_tools/create', 'ConversionToolsController@create');
        Route::post('customers/{customer_id}/conversion_tools/create', 'ConversionToolsController@store');


        Route::get('customers/{customer_id}/conversion_tools/{form_id}/edit', 'ConversionToolsController@edit');
        Route::post('customers/{customer_id}/conversion_tools/{form_id}/edit', 'ConversionToolsController@update');

        Route::get('customers/{customer_id}/survey2/{survey_id}/proof/{filename}', 'Survey2Controller@showSpacesFile');

        Route::post("customers/{customer_id}/creditcard/create", "CreditCardController@updateCard");

        Route::post('customers/{customer_id}/edit', 'CustomersController@getRequests');
        Route::get('customers_imported', 'CustomersController@customersImported');

        Route::post('customers/{customer_id}/getInvoices', 'CustomersController@getInvoices');

        Route::post('customers/{customer_id}/getSurveys', 'CustomersController@getSurveys');

        Route::get('resellers/portals', 'PortalController@index');
        Route::post('resellers/portals', 'PortalController@filteredindex');

        Route::get('resellers/prospects', 'ResellersController@prospectindex');
        //Customer and nested controllers
        Route::resource('resellers', 'ResellersController' );

        //Customer Add
        Route::post('resellers', 'ResellersController@store');

        Route::resource('resellers.contactperson', 'ContactPersonController');
        Route::resource('resellers.contactperson.application_user', 'ApplicationUserController');
        Route::resource('resellers.customerremark', 'CustomerRemarkController');
        Route::resource('resellers.customerstatus', 'CustomerStatusController');
        Route::resource('resellers.customerdocument', 'CustomerDocumentController');
        Route::resource('resellers.creditcard', 'CreditCardController');
        Route::resource('resellers.customercredit', 'CustomerCreditController');
        Route::resource('resellers.customeroffice', 'CustomerOfficeController');
        Route::resource('resellers.child', 'CustomerChildController');
        Route::resource('resellers.customerportal', 'CustomerPortalController');
        Route::resource('resellers.customerportal.customerportalstatus', 'CustomerPortalStatusController');
        Route::resource('resellers.customerportal.requestdelivery', 'RequestDeliveryController');
        Route::resource('resellers.customerportal.paymentrate', 'PaymentRateController');
        Route::resource('resellers.customerportal.freetrial', 'FreeTrialController');
        Route::resource('resellers.survey2', 'Survey2Controller');
        Route::resource('resellers.survey1', 'Survey1Controller');

        Route::post("resellers/{customer_id}/creditcard/create", "CreditCardController@updateCard");

        Route::post('resellers/{customer_id}/edit', 'ResellersController@getRequests');

        Route::post('resellers/{customer_id}/getInvoices', 'ResellersController@getInvoices');

        Route::post('resellers/{customer_id}/getSurveys', 'ResellersController@getSurveys');



		Route::post('serviceproviders/{customer_id}/edit', 'ServiceProvidersController@getRequests');
		Route::post('affiliatepartners/{customer_id}/edit', 'AffiliatePartnersController@getRequests');


		Route::post('serviceproviders/{customer_id}/getInvoices', 'ServiceProvidersController@getInvoices');

		Route::get('incorrect_customers', 'CustomersController@incorrectCustomers' );

		Route::get('customercache', function () {
			Artisan::call('config:clear');
    		Artisan::call('cache:clear');
			Artisan::call('cache:customer_new');

			return redirect()->back();
		});
	});

	Route::group([
                    ['middleware' => ['permission:python validate']]
                ], function () {

        Route::get('validate_gathered_data', 'PythonProcessController@validateGatheredData');
        Route::post('validate_gathered_data', 'PythonProcessController@validateGatheredDataFiltered');

        Route::get('validate_gathered_data_filtered', 'PythonProcessController@validateGatheredData');
        Route::post('validate_gathered_data_filtered', 'PythonProcessController@process_next');

        Route::get('validate_skipped_data', 'PythonProcessController@validateSkippedData');
        Route::get('validate_skipped_data/{curesc_id}/validate', 'PythonProcessController@validateSkippedDataOpen');
        Route::post('validate_skipped_data/{curesc_id}/validate', 'PythonProcessController@validateSkippedDataProcess');

        //Mobilityex
        Route::get('mobilityex_validate_gathered_data', 'MobilityexFetchedDataController@validateGatheredData');
        Route::post('mobilityex_validate_gathered_data', 'MobilityexFetchedDataController@validateGatheredDataFiltered');

        Route::get('mobilityex_validate_gathered_data_filtered', 'MobilityexFetchedDataController@validateGatheredData');
        Route::post('mobilityex_validate_gathered_data_filtered', 'MobilityexFetchedDataController@process_next');

        Route::get('mobilityex_validate_skipped_data', 'MobilityexFetchedDataController@validateSkippedData');
        Route::get('mobilityex_validate_skipped_data/{curesc_id}/validate', 'MobilityexFetchedDataController@validateSkippedDataOpen');
        Route::post('mobilityex_validate_skipped_data/{curesc_id}/validate', 'MobilityexFetchedDataController@validateSkippedDataProcess');
	});

    Route::group(['middleware' => ['permission:affiliates']], function () {

        Route::post('affiliatepartners/{customer_id}/edit', 'AffiliatePartnersController@getRequests');

        //Affiliate partners
        Route::resource('affiliatepartners', 'AffiliatePartnersController' );
        Route::resource('affiliatepartners.contactperson', 'ContactPersonController');
        Route::resource('affiliatepartners.contactperson.application_user', 'ApplicationUserController');
        Route::resource('affiliatepartners.customerremark', 'CustomerRemarkController');
        Route::resource('affiliatepartners.customerdocument', 'CustomerDocumentController');
        Route::resource('affiliatepartners.customeroffice', 'CustomerOfficeController');
        Route::resource('affiliatepartners.form', 'AffiliatePartnerFormController');
	});

    Route::group(['middleware' => ['permission:service providers']], function () {

        Route::post('serviceproviders/{customer_id}/edit', 'ServiceProvidersController@getRequests');
        Route::post('serviceproviders/{customer_id}/getInvoices', 'ServiceProvidersController@getInvoices');

        //Service providers
        Route::resource('serviceproviders', 'ServiceProvidersController' );
        Route::resource('serviceproviders.contactperson', 'ContactPersonController');
        Route::resource('serviceproviders.contactperson.application_user', 'ApplicationUserController');
        Route::resource('serviceproviders.customerstatus', 'CustomerStatusController');
        Route::resource('serviceproviders.customerremark', 'CustomerRemarkController');
        Route::resource('serviceproviders.customerdocument', 'CustomerDocumentController');
        Route::resource('serviceproviders.customeroffice', 'CustomerOfficeController');
        Route::resource('serviceproviders.question', 'ServiceProviderQuestionsController');
        Route::resource('serviceproviders.newsletterblock', 'ServiceProviderNewsletterBlockController');
        Route::resource('serviceproviders.advertorialblock', 'ServiceProviderAdvertorialBlockController');

        Route::resource('serviceproviders.question.request_delivery', 'ServiceProviderRequestDeliveryController');
	});

    Route::group(['middleware' => ['permission:customers - portals']], function () {
        Route::resource('customers.customerportal', 'CustomerPortalController');

        Route::get('portalprices', 'PortalController@getPortalPrices');
        Route::post('portalprices', 'PortalController@storePortalPrices');

        Route::get('customerportals', 'PortalController@index');
        Route::post('customerportals', 'PortalController@filteredindex');
        Route::resource('customers.customerportal.customerportalstatus', 'CustomerPortalStatusController');
        Route::resource('customers.customerportal.requestdelivery', 'RequestDeliveryController');
        Route::resource('customers.customerportal.paymentrate', 'PaymentRateController');
        Route::resource('customers.customerportal.freetrial', 'FreeTrialController');

        Route::resource('customers.conversion_tools.requestdelivery', 'RequestDeliveryController');

        Route::get('resellers/portals', 'PortalController@index');
        Route::post('resellers/portals', 'PortalController@filteredindex');

        Route::resource('resellers.customerportal', 'CustomerPortalController');
        Route::resource('resellers.customerportal.customerportalstatus', 'CustomerPortalStatusController');
        Route::resource('resellers.customerportal.requestdelivery', 'RequestDeliveryController');
        Route::resource('resellers.customerportal.paymentrate', 'PaymentRateController');
        Route::resource('resellers.customerportal.freetrial', 'FreeTrialController');
        Route::resource('customerportal', 'CustomerPortalController');
        Route::resource('customerportalstatus', 'CustomerPortalStatusController');
	});

    Route::group(['middleware' => ['permission:lead resellers']], function () {


        Route::get('resellers/prospects', 'ResellersController@prospectindex');
        //Customer and nested controllers
        Route::resource('resellers', 'ResellersController' );

        //Customer Add
        Route::post('resellers', 'ResellersController@store');

        Route::resource('resellers.contactperson', 'ContactPersonController');
        Route::resource('resellers.contactperson.application_user', 'ApplicationUserController');
        Route::resource('resellers.customerremark', 'CustomerRemarkController');
        Route::resource('resellers.customerstatus', 'CustomerStatusController');
        Route::resource('resellers.customerdocument', 'CustomerDocumentController');
        Route::resource('resellers.creditcard', 'CreditCardController');
        Route::resource('resellers.customercredit', 'CustomerCreditController');
        Route::resource('resellers.customeroffice', 'CustomerOfficeController');

        Route::resource('resellers.survey2', 'Survey2Controller');
        Route::resource('resellers.survey1', 'Survey1Controller');

        Route::post("resellers/{customer_id}/creditcard/create", "CreditCardController@updateCard");

        Route::post('resellers/{customer_id}/edit', 'ResellersController@getRequests');

        Route::post('resellers/{customer_id}/getInvoices', 'ResellersController@getInvoices');

        Route::post('resellers/{customer_id}/getSurveys', 'ResellersController@getSurveys');
	});

    Route::group(['middleware' => ['permission:validation']], function () {
        /**
         * Python Related companies
         */
        Route::get('validate_related_moving_companies/settings/current_white_blacklist', 'PythonProcessController@settingsRelatedMovingCompaniesWhiteAndBlacklist');
        Route::post('validate_related_moving_companies/settings/current_white_blacklist', 'PythonProcessController@settingsRelatedMovingCompaniesWhiteAndBlacklistFiltered');
        Route::get('validate_related_moving_companies/settings/countries', 'PythonProcessController@settingsRelatedMovingCompaniesCountries');
        Route::get('validate_related_moving_companies/settings', 'PythonProcessController@settingsRelatedMovingCompanies');
        Route::post('validate_related_moving_companies/settings', 'PythonProcessController@settingsWhiteOrBlacklistFiltered');

        Route::get('validate_related_moving_companies', 'PythonProcessController@validateRelatedMovingCompanies');
        Route::post('validate_related_moving_companies', 'PythonProcessController@validateRelatedMovingCompaniesFiltered');

        Route::get('related_moving_companies/{curesc_id}/create/{co_code?}', 'PythonProcessController@createRelatedMovingCompany');
        Route::post('related_moving_companies/{curesc_id}/create/{co_code?}', 'PythonProcessController@storeRelatedMovingCompany');

        Route::get('validation', 'PythonProcessController@index');
    });

    Route::get('factsheets/{co_code}', 'FactsheetsController@index');

    Route::get("/anonymize", "AnonymizeController@index");
	Route::post("/anonymize", "AnonymizeController@filteredIndex");
	Route::post("/anonymized", "AnonymizeController@anonymize");

	//Admin access only to these functionalities
	Route::group(['middleware' => ['permission:finance']], function () {
        Route::get('cafe_planner', 'AdminController@cafePlanner');
        Route::post('cafe_planner', 'AdminController@cafePlannerFiltered');
        Route::post('cafe_planner_submit', 'AdminController@cafePlannerSubmit');

        Route::get('match_exclusions', 'AdminController@showMatchExclusions');
        Route::post('match_exclusions', 'AdminController@updateMatchExclusions');

		Route::get('finance', 'FinanceController@index');

		Route::get('finance/turnoverperyear', 'TurnoverPerYearController@index');
		Route::post('finance/turnoverperyear', 'TurnoverPerYearController@filteredIndex');

		Route::get('finance/turnoverperperiod', 'TurnoverPerPeriodController@index');
		Route::post('finance/turnoverperperiod', 'TurnoverPerPeriodController@filteredIndex');

		Route::get('finance/turnoverjournalentry', 'TurnoverJournalEntryController@index');
		Route::post('finance/turnoverjournalentry', 'TurnoverJournalEntryController@filteredIndex');

		Route::get('finance/costjournalentry', 'CostJournalEntryController@index');
		Route::post('finance/costjournalentry', 'CostJournalEntryController@filteredIndex');

		Route::get('finance/balancejournalentry', 'BalanceJournalEntryController@index');
		Route::post('finance/balancejournalentry', 'BalanceJournalEntryController@filteredIndex');

		Route::get('finance/iclist', 'ICListController@index');
		Route::post('finance/iclist', 'ICListController@filteredIndex');

		Route::get('finance/invoicesummary', 'InvoiceSummaryController@index');
		Route::post('finance/invoicesummary', 'InvoiceSummaryController@filteredIndex');

		Route::get('finance/creditdebitinvoicelines', 'CreditDebitInvoiceLinesController@index');
		Route::post('finance/creditdebitinvoicelines', 'CreditDebitInvoiceLinesController@filteredIndex');
        Route::get('finance/addcreditdebitinvoiceline', 'CreditDebitInvoiceLinesController@addIndex');
        Route::post('finance/addcreditdebitinvoiceline', 'CreditDebitInvoiceLinesController@addLine');

		Route::get('finance/opendebtors', 'OpenDebtorsController@index');
		Route::post('finance/opendebtors', 'OpenDebtorsController@filteredIndex');

		Route::get('finance/paymentreminders', 'PaymentRemindersController@index');
		Route::post('finance/paymentreminders', 'PaymentRemindersController@sendPaymentReminders');

        Route::get('finance/importbanklines', 'ImportBankLinesController@index');
        Route::post('finance/importbanklines', 'ImportBankLinesController@import');

        Route::get('finance/pairbanklines', 'PairBankLinesController@index');
        Route::post('finance/pairbanklines', 'PairBankLinesController@filteredIndex');
        Route::get('finance/{bali_id}/pair', 'PairBankLinesController@pair');

        Route::post('finance/{bali_id}/pair', 'PairBankLinesController@pairEuroLine');
        Route::post('finance/{bali_id}/pairfc', 'PairBankLinesController@pairFCLine');
        Route::get('finance/getInvoiceFields', 'PairBankLinesController@getInvoiceFields');

        Route::get('finance/banktransactionssummary', 'BankTransactionsSummaryController@index');
        Route::post('finance/banktransactionssummary', 'BankTransactionsSummaryController@filteredIndex');

        Route::get('finance/banktransactionsperledgeraccountsummary', 'BankTransactionsPerLedgerAccountSummaryController@index');
        Route::post('finance/banktransactionsperledgeraccountsummary', 'BankTransactionsPerLedgerAccountSummaryController@filteredIndex');

        Route::get('finance/adyen', 'AdyenController@index');
        Route::post('finance/adyen', 'AdyenController@filteredIndex');

        Route::get('finance/creditcardpaymenthistory', 'CreditCardPaymentHistoryController@index');
        Route::post('finance/creditcardpaymenthistory', 'CreditCardPaymentHistoryController@filteredIndex');

        Route::get('finance/addjournalentry', 'AddJournalEntryController@index');
        Route::post('finance/addjournalentry', 'AddJournalEntryController@filteredIndex');

        Route::get('finance/journalentrysummary', 'JournalEntrySummaryController@index');
        Route::post('finance/journalentrysummary', 'JournalEntrySummaryController@filteredIndex');

        Route::get('finance/createautodebitfile', 'CreateAutoDebitFileController@index');
        Route::post('finance/createautodebitfile', 'CreateAutoDebitFileController@filteredIndex');
        Route::post('finance/createautodebitresult', 'CreateAutoDebitFileController@createFile');
        Route::get('createautodebitdownload/{file}', 'CreateAutoDebitFileController@downloadFile');

        Route::get('finance/chargeautodebit', 'ChargeAutoDebitController@index');
        Route::post('finance/chargeautodebit', 'ChargeAutoDebitController@createFile');

        Route::get('finance/chargeprepayment', 'ChargePrepaymentController@index');
        Route::post('finance/chargeprepayment', 'ChargePrepaymentController@chargeConfirmation');
        Route::post('finance/charge', 'ChargePrepaymentController@charge');

        Route::get('finance/chargeprepayment_auto_debit', 'ChargePrepaymentAutoDebitController@index');
        Route::post('finance/chargeprepayment_auto_debit', 'ChargePrepaymentAutoDebitController@chargeConfirmation');
        Route::post('finance/chargeprepaymentautodebit', 'ChargePrepaymentAutoDebitController@createAutoDebitFile');

        Route::get('finance/creditcardcustomers', 'CreditCardCustomersController@index');
        Route::post('finance/creditcardcustomers', 'CreditCardCustomersController@filteredIndex');

        Route::get('finance/createinvoices', 'CreateInvoicesController@index');
        Route::post('finance/createinvoices', 'CreateInvoicesController@previewInvoices');
        Route::post('finance/generateinvoices', 'CreateInvoicesController@createInvoices');

        Route::get('finance/chargecreditcards', 'ChargeCreditCardsController@index');
        Route::post('finance/chargecreditcards', 'ChargeCreditCardsController@chargeConfirmation');
        Route::post('finance/chargeccs', 'ChargeCreditCardsController@charge');
    });

	Route::get( 'ajax/customer/delete', 'CustomersController@delete' );
	Route::get( 'ajax/python_process/customer_delete', 'CustomersController@deleteCustomerViaPythonProcess' );
	Route::get( 'ajax/mobilityex/customer_delete', 'CustomersController@deleteCustomerViaMobilityex' );
	Route::get( 'ajax/subscription/mark_as_invoiced', 'CustomerSubscriptionController@markAsInvoiced' );
	Route::get( 'ajax/subscription/cancel', 'CustomerSubscriptionController@cancel' );
	Route::get( 'ajax/subscription/delete', 'CustomerSubscriptionController@delete' );

    //Newsletters edit
    Route::get('admin/newsletters', 'NewsletterController@index');
    Route::get('admin/newsletters/{newsletter_id}/edit', 'NewsletterController@edit');
    Route::post('admin/newsletters/{newsletter_id}/edit', 'NewsletterController@update');
    Route::get('admin/newsletters/create', 'NewsletterController@create');
    Route::post('admin/newsletters/create', 'NewsletterController@store');


    //Questions edit
    Route::get('admin/questions', 'QuestionsController@index');
    Route::get('admin/questions/{question_id}/edit', 'QuestionsController@edit');
    Route::post('admin/questions/{question_id}/edit', 'QuestionsController@update');
    Route::get('admin/questions/create', 'QuestionsController@create');
    Route::post('admin/questions/create', 'QuestionsController@store');


    Route::get('sirelosynchronise', 'SireloSynchroniseController@index');
    Route::post('sirelosynchronise', 'SireloSynchroniseController@update');

    Route::group(['middleware' => ['permission:admin']], function () {

        Route::get('sirelosynchroniseresponse', 'SireloSynchroniseController@getProgess');


        Route::get('sirelosynchroniseall', 'SireloSynchroniseAllController@index');
        Route::post('sirelosynchroniseall', 'SireloSynchroniseAllController@update');
        Route::get('sirelosynchroniseallresponse', 'SireloSynchroniseAllController@getProgess');
    });

    Route::group(['middleware' => ['permission:sirelo settings']], function () {
        Route::get('sirelo_settings', 'SireloSettingController@index');
        Route::get('sirelo_settings/{setting_id}/edit', 'SireloSettingController@edit');
        Route::post('sirelo_settings/{setting_id}/edit', 'SireloSettingController@update');

        Route::get('movingassistant', 'SireloSettingController@movingAssistant');
        Route::get('movingassistant/add_email', 'SireloSettingController@movingAssistantAddEmail');
        Route::post('movingassistant/add_email', 'SireloSettingController@movingAssistantAddEmailStore');
        Route::get('movingassistant/{moasem_id}/edit', 'SireloSettingController@movingAssistantEditEmail');
        Route::post('movingassistant/{moasem_id}/edit', 'SireloSettingController@movingAssistantEditEmailUpdate');
    });

    Route::group(['middleware' => ['permission:email builder']], function () {
        Route::get('email_builder_templates', 'EmailBuilderTemplateController@index');

        Route::get('email_builder_templates/create', 'EmailBuilderTemplateController@create');
        Route::get('email_builder_templates/emailchecker/{filename}/{language}', 'EmailBuilderTemplateController@emailChecker');
        Route::post('email_builder_templates/create', 'EmailBuilderTemplateController@store');

        Route::get('email_builder_templates/{email_id}/edit', 'EmailBuilderTemplateController@edit');
        Route::post('email_builder_templates/{email_id}/edit', 'EmailBuilderTemplateController@update');

    });

    Route::get('more_then_an_half_diff', 'AdminController@testNewReviewScore_1');
    Route::get('more_then_one_diff', 'AdminController@testNewReviewScore_2');
    Route::get('all_emails', 'AdminController@testingEmails');
    Route::get('all_sirelo_emails', 'AdminController@allSireloEmails');

    //Admin access only to these functionalities
	Route::group(['middleware' => ['permission:admin']], function () {


        Route::get('status', 'StatusController@index');
        Route::get('statusajaxcall', 'StatusController@ajaxFunction');
        Route::get('getlastreceivedandmatchedlead', 'StatusController@getLastLead');
        Route::get('getlastaffiliateleads', 'StatusController@lastAffiliateLeads');

        Route::get('admin', 'AdminController@index');
        Route::get('test_arjan', 'AdminController@testArjan');
        Route::get('test_lorenzo', 'AdminController@testLorenzo');
        Route::get('debugbar', 'AdminController@DebugBar');
        Route::get('test_wilco', 'AdminController@testWilco');
        Route::get('manual_sync', 'AdminController@manualSync');
        Route::get('city_to_states', 'AdminController@cityToStates');
        Route::get('customerstates_to_id', 'AdminController@currentStatesCustomersToID');

        Route::get('admin/portals', 'AdminController@seperateCombinedPortals');

		//Admin permissions
		Route::get('admin/permissions', 'PermissionController@index');
		Route::get('admin/permissions/create', 'PermissionController@create');
		Route::post('admin/permissions/create', 'PermissionController@store');

		//Admin users
		Route::get('admin/users', 'UserController@index');
		Route::get('admin/users/create', 'UserController@create');
		Route::post('admin/users/create', 'UserController@store');
		Route::get('admin/users/{user_id}/edit', 'UserController@edit');
		Route::post('admin/users/{user_id}/edit', 'UserController@update');

		Route::get('admin/phpinfo', 'AdminController@phpinfo');

		//Admin Ledger accounts
		Route::resource('admin/ledgeraccounts', 'LedgerAccountController');

		Route::get('clearcache', function () {
			Artisan::call('config:clear');
    		Artisan::call('cache:clear');
    		system('composer dump-autoload');
    		Artisan::call('view:clear');
			Artisan::call('route:clear');

			return redirect()->back();
		});

        Route::get('admin/websites', 'WebsiteController@index');
        Route::get('admin/websites/{we_id}/edit', 'WebsiteController@edit');
        Route::post('admin/websites/{we_id}/edit', 'WebsiteController@update');

        Route::get('admin/websites/{we_id}/forms/{form_id}/edit', 'WebsiteFormController@edit');
        Route::post('admin/websites/{we_id}/forms/{form_id}/edit', 'WebsiteFormController@update');


        Route::get('admin/websites/{we_id}/forms/create', 'WebsiteFormController@create');
        Route::post('admin/websites/forms/create', 'WebsiteFormController@store');

        Route::get('admin/systemsettings', 'SystemSettingsController@index');
        Route::post('admin/systemsettings', 'SystemSettingsController@update');

		//Memberships
        Route::get('admin/memberships', 'MembershipController@index');
        Route::get('admin/memberships/create', 'MembershipController@create');
        Route::post('admin/memberships/create', 'MembershipController@store');
        Route::get('admin/memberships/{membership_id}/edit', 'MembershipController@edit');
        Route::post('admin/memberships/{membership_id}/edit', 'MembershipController@update');
        Route::get( 'ajax/membership/delete', 'MembershipController@delete' );

        //Obligations
        Route::get('admin/obligations', 'ObligationController@index');
        Route::get('admin/obligations/create', 'ObligationController@create');
        Route::post('admin/obligations/create', 'ObligationController@store');
        Route::get('admin/obligations/{obligation_id}/edit', 'ObligationController@edit');
        Route::post('admin/obligations/{obligation_id}/edit', 'ObligationController@update');
        Route::get( 'ajax/obligation/delete', 'ObligationController@delete' );

        //Insurances
        Route::get('admin/insurances', 'InsuranceController@index');
        Route::get('admin/insurances/create', 'InsuranceController@create');
        Route::post('admin/insurances/create', 'InsuranceController@store');
        Route::get('admin/insurances/{insurances_id}/edit', 'InsuranceController@edit');
        Route::post('admin/insurances/{insurances_id}/edit', 'InsuranceController@update');
        Route::get( 'ajax/insurance/delete', 'InsuranceController@delete' );

        //Notification system
        Route::get('admin/notification_lists', 'NotificationListsController@index');
        Route::get('admin/notification_lists/create', 'NotificationListsController@create');
        Route::post('admin/notification_lists/create', 'NotificationListsController@store');
        Route::get('admin/notification_lists/{notification_id}/edit', 'NotificationListsController@edit');
        Route::post('admin/notification_lists/{notification_id}/edit', 'NotificationListsController@update');
        Route::get( 'ajax/notification_lists/delete', 'NotificationListsController@delete' );

        Route::get( 'ajax/moving_assistant/delete', 'SireloSettingController@movingAssistantDelete' );
	});

	//Seperate controllers
	Route::resource('contactperson', 'ContactPersonController');
	Route::resource('customerremark', 'CustomerRemarkController');
	Route::resource('customerstatus', 'CustomerStatusController');
	Route::resource('customerdocument', 'CustomerDocumentController');
	Route::resource('creditcard', 'CreditCardController');
	Route::resource('customercredit', 'CustomerCreditController');
	Route::resource('customeroffice', 'CustomerOfficeController');
	Route::resource('child', 'CustomerChildController');
	Route::resource('customerportal', 'CustomerPortalController');
	Route::resource('customerportalstatus', 'CustomerPortalStatusController');
	Route::resource('requestdelivery', 'RequestDeliveryController');
	Route::resource('paymentrate', 'PaymentRateController');
	Route::resource('freetrial', 'FreeTrialController');
	Route::resource('applicationuser', 'ApplicationUserController');
	Route::resource('newsletters', 'NewsletterController');
	Route::resource('questions', 'QuestionsController');
	Route::resource('form', 'AffiliatePartnerFormController');
	Route::resource('spquestions', 'ServiceProviderQuestionsController');
	Route::resource('request_delivery', 'ServiceProviderRequestDeliveryController');
	Route::resource('newsletterblock', 'ServiceProviderNewsletterBlockController');
	Route::resource('advertorial', 'ServiceProviderAdvertorialBlockController');
	Route::resource('survey2', 'Survey2Controller');
	Route::resource('survey1', 'Survey1Controller');
	//Route::resource('messages', 'MessagesController' );

    /*Route::get('serviceproviders/{customer_id}/advertorialblock/create', 'ServiceProviderAdvertorialBlockController@create');
    Route::post('serviceproviders/{customer_id}/advertorialblock/create', 'ServiceProviderAdvertorialBlockController@store');
    Route::get('serviceproviders/{customer_id}/advertorialblock/{advertorial_id}/edit', 'ServiceProviderAdvertorialBlockController@edit');
    Route::post('serviceproviders/{customer_id}/advertorialblock/{advertorial_id}/edit', 'ServiceProviderAdvertorialBlockController@update');*/


	Route::group(['middleware' => ['permission:partnerdesk']], function () {
		Route::resource('customerchanges', 'CustomerChangesController');
	});

    // Ajax get
    Route::get( 'ajax/customer/contactperson_delete', 'ContactPersonController@delete' );
    Route::get( 'ajax/customer/lead_form_delete', 'ConversionToolsController@delete' );
    Route::get( 'ajax/customer/status_delete', 'CustomerStatusController@delete' );
    Route::get( 'ajax/customer/remark_delete', 'CustomerRemarkController@delete' );
    Route::get( 'ajax/customer/credit_debit_line_delete', 'CreditDebitInvoiceLinesController@delete' );
    Route::get( 'ajax/customer/office_delete', 'CustomerOfficeController@delete' );
    Route::get( 'ajax/customer/child_delete', 'CustomerChildController@delete' );
    Route::get( 'ajax/customer/pause_delete', 'CustomerPauseController@delete' );
    Route::get( 'ajax/customer/advertorialblock_delete', 'ServiceProviderAdvertorialBlockController@delete' );
    Route::get( 'ajax/customer/newsletterblock_delete', 'ServiceProviderNewsletterBlockController@delete' );
    Route::get( 'ajax/customer/form_delete', 'AffiliatePartnerFormController@delete' );
    Route::get( 'ajax/customer/contactperson/app_user_delete', 'ApplicationUserController@delete' );
    Route::get( 'ajax/customer/getstates', 'CustomerOfficeController@getStates' );
    Route::get( 'ajax/customer/request_delivery_delete', 'ServiceProviderQuestionsController@requestDeliveryDelete' );
    Route::get( 'ajax/message/markasread', 'MessagesController@markAsRead' );
    Route::get( 'ajax/gettranslation', 'AjaxController@getTranslation' );
    Route::get( 'ajax/findsuggestedregion', 'AjaxController@findSuggestedRegion' );
    Route::get( 'ajax/getmovingsizes', 'AjaxController@getMovingSizes' );
    Route::get( 'ajax/addrequestextrainfo', 'AjaxController@addRequestExtraInformation' );
    Route::get( 'ajax/getregions', 'AjaxController@getRegions' );
    Route::get( 'ajax/requestwrongtelephonenumber', 'AjaxController@requestWrongTelephoneNumber' );
    Route::get( 'ajax/confirmsurvey', 'AjaxController@confirmSurvey' );
    Route::get( 'ajax/updateserviceproviderrequest', 'AjaxController@updateServiceProviderRequest' );
    Route::get( 'ajax/customerportal/select_eu_countries', 'CustomerPortalController@selectEUCountries' );
    Route::get( 'ajax/customerportal/select_schengen_area', 'CustomerPortalController@selectSchengenArea' );
    Route::get( 'ajax/profile/changepassword', 'UserController@changePassword' );
    Route::get( 'ajax/addtodialfire', 'AjaxController@addToDialfire' );
    Route::get( 'ajax/loadgooglemapsimage', 'AjaxController@loadGoogleMapsImage' );
    Route::get( 'ajax/customer_portal_calculate_lead_amounts', 'AjaxController@customerPortalCalculateLeadAmounts' );
    Route::get( 'ajax/customer_portal_summary', 'AjaxController@customerPortalSummary' );
    Route::get( 'ajax/customer_portal_summary_leadsstore', 'AjaxController@customerPortalSummaryLeadsStore' );
    Route::get( 'ajax/customer_portal_summary_calculate_formula', 'AjaxController@customerPortalSummaryCalculateFormula' );
    Route::get( 'ajax/plan_call', 'AjaxController@planCall' );
    Route::get( 'ajax/view_newsletterblock_preview_or_send', 'AjaxController@viewNewsletterPreviewOrSend' );
    Route::get( 'ajax/get_messages_count', 'AjaxController@getMessagesCount' );
    Route::get( 'ajax/get_calls_count', 'AjaxController@getCallsCount' );
    Route::get( 'ajax/get_call_notifications', 'AjaxController@getCallNotifications' );
    Route::get( 'ajax/opendebtorsinvoices', 'OpenDebtorsController@getInvoices' );
    Route::get( 'ajax/paymentreminderinvoices', 'PaymentRemindersController@getInvoices' );
    Route::get( 'ajax/banklinerecords', 'PairBankLinesController@getBankLineRecords' );
    Route::get( 'ajax/banklines', 'BankTransactionsPerLedgerAccountSummaryController@getBankLines');
    Route::get( 'ajax/deletefile', 'RequestsController@deleteFile');
    Route::get( 'ajax/deletescreenshot', 'RequestsController@deleteScreenshot' );
    Route::get( 'ajax/approveautomaticchecked', 'RequestsController@approveAutomaticChecked' );
    Route::get( 'ajax/pairedbankline/delete', 'PairBankLinesController@delete' );
    Route::get( 'function/getscreenshot/{request}/{name}', 'AjaxController@getScreenshot' );
    Route::get( 'json/portal_options', 'AjaxController@getPortalOptions' );
    Route::get( 'json/moving_sizes', 'AjaxController@getMovingSizesJSON' );
    Route::get( 'ajax/reportprogress', 'AjaxController@reportProgress' );
    Route::get( 'ajax/getinvoicefields', 'AjaxController@getInvoiceFields');
    Route::get( 'ajax/getadyenccdetails', 'AjaxController@getAdyenCCDetails');
    Route::get( 'ajax/getCurrencyRates', 'PairBankLinesController@getCurrencyRates');
    Route::get( 'ajax/addFCToBankLine', 'PairBankLinesController@addFCToBankLine');
    Route::get( 'ajax/forceEuro', 'PairBankLinesController@forceEuro');
    Route::get( 'ajax/getCustomerFields', 'AjaxController@getCustomerFields');
    Route::post( 'ajax/uploadfiletopcloud', 'AjaxController@uploadFileToPcloud');
    Route::get( 'ajax/emailBuilderLivePreview', 'EmailBuilderTemplateController@livePreview');
    Route::get( 'ajax/select_all_sirelos', 'AjaxController@selectAllSirelos');
    Route::get( 'ajax/select_all_non_sirelos', 'AjaxController@selectAllNonSirelos');
    Route::get( 'ajax/validate_website_urls', 'AjaxController@validateWebsiteURLs');
    Route::get( 'ajax/validate_email_domain', 'AjaxController@validateEmailDomain');
    Route::get( 'ajax/check_for_email_warning', 'AjaxController@checkForEmailWarning');
    Route::get( 'ajax/save_new_domain_in_form', 'AjaxController@saveNewDomainInForm');
    Route::get( 'ajax/validate_website', 'AjaxController@validateWebsite');
    Route::get( 'ajax/put_on_whitelist', 'AjaxController@putOnWhitelist');
    Route::get( 'ajax/put_on_blacklist', 'AjaxController@putOnBlacklist');
    Route::get( 'ajax/gathered_companies_put_country_active', 'AjaxController@gatheredCompaniesPutCountryActive');
    Route::get( 'ajax/gathered_companies_put_country_inactive', 'AjaxController@gatheredCompaniesPutCountryInactive');
    Route::get( 'ajax/validate_phone_numbers', 'AjaxController@validatePhoneNumbers');
    Route::get( 'ajax/validate_phone_numbers_adding_customer', 'AjaxController@validatePhoneNumbersAddingCustomer');
    Route::get( 'ajax/customerlogo/remove', 'AjaxController@customerLogoRemove');
    Route::get( 'ajax/customerprogress/reset', 'AjaxController@customerProgressReset');
    Route::get( 'ajax/mobilityex_get_erp_fields', 'MobilityexFetchedDataController@getERPFields');
    Route::get( 'ajax/mobilityex_customer_available_check', 'MobilityexFetchedDataController@checkIfCustomerIsAvailable');
    Route::get( 'ajax/related_moving_companies/get_categories', 'AjaxController@getRelatedMovingCompaniesCategories');
    Route::get( 'ajax/mover_comments/read', 'SurveysController@readMoverComment' );
    Route::get( 'ajax/select2customersearch', 'AjaxController@select2customersearch');


    //Flag a Review
    Route::group(['middleware' => ['permission:flagged reviews']], function () {
		Route::get('flagged_review/{flre_id}', 'SurveysController@flaggedReview');
		Route::post('flagged_review/{flre_id}', 'SurveysController@approveFlaggedReview');
	});

	//Surveys
	Route::group(['middleware' => ['permission:surveys']], function () {

		//Surveys
		Route::get('surveys', 'SurveysController@index');
		Route::get('surveys/search', 'SurveysController@search');
		Route::post('surveys/search', 'SurveysController@searchResult');
		//Survey Pro Cons
		Route::get('surveys/procons', 'ProsConsController@index');
	});

    Route::get( 'ajax/invoice/view', 'InvoiceController@viewInvoice' );
    Route::get( 'ajax/invoice/preview', 'InvoiceController@previewInvoice' );

    //Premium leads
    Route::group(['middleware' => ['permission:premium leads']], function() {
        //Premium leads
		Route::get('premium_leads', 'RequestsController@premiumLeads');
		Route::get('premium_leads/{request}/edit', 'RequestsController@editPremiumLead');
		Route::put('premium_leads/{request}/edit', 'RequestsController@updatePremiumLead');
	    Route::get('premium_leads/{request}/file/download', 'RequestsController@downloadFile');
	    Route::get('premium_leads/{request}/generate','RequestsController@generatePDF');

	    Route::get('premium_leads/{request}/match', 'RequestsController@match');
		Route::post('premium_leads/{request}/match', 'RequestsController@matchlead');
		Route::get('premium_leads/{request}/reject', 'RequestsController@reject');
		Route::post('premium_leads/{request}/reject', 'RequestsController@rejectlead');

		Route::get('premium_leads/{request}', 'RequestsController@showPremiumLead');
    });

	//Requests
	Route::group(['middleware' => ['permission:requests']], function () {

		Route::get('customerrequest/{request}/free', 'CustomerRequestController@free');
		Route::post('customerrequest/{request}/free', 'CustomerRequestController@freeLead');

		Route::get('customerrequest/{request}/resend', 'CustomerRequestController@resend');
		Route::post('customerrequest/{request}/resend', 'CustomerRequestResendController@resendLead');

		Route::get('customerquestion/{question}/free', 'CustomerQuestionController@free');
		Route::post('customerquestion/{question}/free', 'CustomerQuestionController@freeLead');

		Route::get('customerquestion/{question}/resend', 'CustomerQuestionController@resend');
		Route::post('customerquestion/{question}/resend', 'CustomerQuestionController@resendLead');

		Route::get('requests/search', 'RequestSearchController@search');
		Route::post('requests/search', 'RequestSearchController@searchResult');

		Route::resource('requests', 'RequestsController');

		//Match and Reject Requests
		Route::get('requests/{request}/reject', 'RequestsController@reject');
		Route::post('requests/{request}/reject', 'RequestsController@rejectlead');
        Route::get('requests/{request}/match', 'RequestsController@newmatchBetter');
		Route::post('requests/{request}/match', 'RequestsController@matchlead');

		//Resend requests
		Route::get('requests/{request}/send', 'RequestsController@send');
		Route::post('requests/{request}/send', 'RequestsController@sendlead');


		Route::get('requests/{request}/recover', 'RequestsController@recover');
		Route::post('requests/{request}/recover', 'RequestsController@recoverLead');

		Route::get('requests/{request}/restore', 'RequestsController@restoreLead');


		Route::resource('serviceproviderrequests', 'ServiceProviderRequestController');
		Route::post('serviceproviderrequests', 'ServiceProviderRequestController@filteredindex');

		Route::resource('matchsummary', 'MatchSummaryController');
		Route::post('matchsummary', 'MatchSummaryController@filteredindex');

	});

	//Download a customer document
	Route::get('customers/{customer}/document/{document}/download', 'CustomerDocumentController@downloadDocument');
	Route::get('affiliatepartners/{customer}/document/{document}/download', 'CustomerDocumentController@downloadDocument');
	Route::get('serviceproviders/{customer}/document/{document}/download', 'CustomerDocumentController@downloadDocument');

	//Archive / Recover a customer document
	Route::get('customers/{customer}/document/{document}/archive', 'CustomerDocumentController@archiveDocument');
	Route::get('customers/{customer}/document/{document}/recover', 'CustomerDocumentController@archiveDocument');
	Route::get('affiliatepartners/{customer}/document/{document}/archive', 'CustomerDocumentController@archiveDocument');
	Route::get('affiliatepartners/{customer}/document/{document}/recover', 'CustomerDocumentController@archiveDocument');
	Route::get('serviceproviders/{customer}/document/{document}/archive', 'CustomerDocumentController@archiveDocument');
	Route::get('serviceproviders/{customer}/document/{document}/recover', 'CustomerDocumentController@archiveDocument');

	//Claims
	Route::group(['middleware' => ['permission:claims']], function () {

		Route::resource('claims', 'ClaimsController');
        Route::post('claims', 'ClaimsController@filteredindex');

		Route::resource('claimscheck', 'ClaimsCheckController');
		Route::post('claimscheck', 'ClaimsCheckController@filteredindex');

		//Accept and Reject Claims
		Route::get('claims/{claim}/accept', 'ClaimsController@accept');
		Route::post('claims/{claim}/accept', 'ClaimsController@acceptclaim');

		Route::get('claims/{claim}/reject', 'ClaimsController@reject');
		Route::post('claims/{claim}/reject', 'ClaimsController@rejectclaim');

	});

	//Messages controller
	Route::get('messages', 'MessagesController@index');
	Route::post('messages', 'MessagesController@filteredIndex');
	Route::get('messages/readallmessages', 'MessagesController@readallmessages');

	//Customer Changes
	Route::get('customerchanges/{customerchange}/approve_confirmation', 'CustomerChangesController@approveConfirmation');
	Route::post('customerchanges/{customerchange}/approve_confirmation', 'CustomerChangesController@approveChange');

	Route::get('customerchanges/{customerchange}/deny_confirmation', 'CustomerChangesController@denyConfirmation');
	Route::post('customerchanges/{customerchange}/deny_confirmation', 'CustomerChangesController@denyChange');

	//Sending invoices
	Route::get('customers/{customer_id}/invoice/{invoice_id}/send_invoice', 'CustomersController@confirmSendInvoice');
	Route::post('customers/{customer_id}/invoice/{invoice_id}/send_invoice', 'InvoiceController@sendInvoice');
	Route::get('serviceproviders/{customer_id}/invoice/{invoice_id}/send_invoice', 'ServiceProvidersController@confirmSendInvoice');
	Route::post('serviceproviders/{customer_id}/invoice/{invoice_id}/send_invoice', 'InvoiceController@sendInvoice');

	//Generating password for Contact person User
	Route::get('customers/{customer}/contactperson/{contactperson}/generatepassword', 'ContactPersonController@generatePassword');
	Route::post('customers/{customer}/contactperson/{contactperson}/generatepassword', 'ContactPersonController@generateNewPassword');

	//Unblocking user of contact person
	Route::get('customers/{customer}/contactperson/{contactperson}/unblock_user/{apus_id}', 'ContactPersonController@unblockUserConfirm');
	Route::post('customers/{customer}/contactperson/{contactperson}/unblock_user/{apus_id}', 'ContactPersonController@unblockUser');

  Route::get('memberships/{me_id}/edit', 'MembershipController@edit');
  Route::post('memberships/{me_id}/edit', 'MembershipController@update');

  Route::resource('calendar', 'CalendarController');
  Route::get('allcalendars', 'CalendarController@allCalendars');
  Route::get('plancalls', 'CalendarController@planCalls');
  Route::get('calendar/{id}/edit', 'CalendarController@edit');

	/**
	 * Reports start
	 */
	Route::group(['middleware' => ['permission:reports']], function () {
	    Route::get('active_customer_X_statuses', 'AdminController@listActiveCustomerStatuses');
	    Route::get('customers_on_active_but_not_active', 'AdminController@listWhosMissing');

	    Route::get('reports', 'ReportController@index');
        Route::get('reports/{report_id}/view', 'ReportController@view');

        Route::get('reports/{report_id}/view/affiliatePartnerForms', 'ReportController@viewAffiliatePartnerForms');
        Route::post('reports/{report_id}/view/affiliatePartnerForms', 'ReportController@viewAffiliatePartnerForms');

        Route::get('reports/{report_id}/view/affiliateRequestsPerCountry', 'ReportController@viewAffiliateLeadsPerCountry');
        Route::post('reports/{report_id}/view/affiliateRequestsPerCountry', 'ReportController@viewAffiliateLeadsPerCountry');

        Route::get('reports/{report_id}/view/affiliateLeadsPaid', 'ReportController@viewAffiliateLeadsPaid');
        Route::post('reports/{report_id}/view/affiliateLeadsPaid', 'ReportController@viewAffiliateLeadsPaid');

        Route::get('reports/{report_id}/view/serviceProviderNewsletterblocks', 'ReportController@viewServiceProviderNewsletterblocks');
        Route::post('reports/{report_id}/view/serviceProviderNewsletterblocks', 'ReportController@viewServiceProviderNewsletterblocks');

        Route::get('reports/{report_id}/view/reviewAnalyse', 'ReportController@viewReviewAnalyse');
        Route::post('reports/{report_id}/view/reviewAnalyse', 'ReportController@viewReviewAnalyse');

        Route::get('reports/{report_id}/view/customerBalances', 'ReportController@viewCustomerBalances');
        Route::post('reports/{report_id}/view/customerBalances', 'ReportController@viewCustomerBalances');

        Route::get('reports/{report_id}/view/customer_created', 'ReportController@viewCustomerCreated');
        Route::post('reports/{report_id}/view/customer_created', 'ReportController@viewCustomerCreated');

        Route::get('reports/{report_id}/view/customerGDPR', 'ReportController@viewCustomerGDPR');
        Route::post('reports/{report_id}/view/customerGDPR', 'ReportController@viewCustomerGDPR');

        Route::get('reports/{report_id}/view/customer_languages', 'ReportController@viewCustomerLanguages');
        Route::post('reports/{report_id}/view/customer_languages', 'ReportController@viewCustomerLanguages');

        Route::get('reports/{report_id}/view/customer_portal_analysis', 'ReportController@viewCustomerPortalAnalysis');
        Route::post('reports/{report_id}/view/customer_portal_analysis', 'ReportController@viewCustomerPortalAnalysis');

        Route::get('reports/{report_id}/view/customer_remarks', 'ReportController@viewCustomerRemarks');
        Route::post('reports/{report_id}/view/customer_remarks', 'ReportController@viewCustomerRemarks');

        Route::get('reports/{report_id}/view/customer_statuses', 'ReportController@viewCustomerStatuses');
        Route::post('reports/{report_id}/view/customer_statuses', 'ReportController@viewCustomerStatuses');

        Route::get('reports/{report_id}/view/free_leads_per_sirelo', 'ReportController@viewFreeLeadsPerSirelo');
        Route::post('reports/{report_id}/view/free_leads_per_sirelo', 'ReportController@viewFreeLeadsPerSirelo');

        Route::get('reports/{report_id}/view/lead_pick', 'ReportController@viewLeadPick');
        Route::post('reports/{report_id}/view/lead_pick', 'ReportController@viewLeadPick');

        Route::get('reports/{report_id}/view/load_exchange_usage', 'ReportController@viewLoadExchangeUsage');
        Route::post('reports/{report_id}/view/load_exchange_usage', 'ReportController@viewLoadExchangeUsage');

        Route::get('reports/{report_id}/view/login_report', 'ReportController@viewLoginReports');
        Route::post('reports/{report_id}/view/login_report', 'ReportController@viewLoginReports');

        Route::get('reports/{report_id}/view/margin_affiliate_partners', 'ReportController@viewMarginAffiliatePartners');
        Route::post('reports/{report_id}/view/margin_affiliate_partners', 'ReportController@viewMarginAffiliatePartners');

        Route::get('reports/{report_id}/view/matching_speed', 'ReportController@viewMatchingSpeed');
        Route::post('reports/{report_id}/view/matching_speed', 'ReportController@viewMatchingSpeed');

        Route::get('reports/{report_id}/view/performance_per_employee', 'ReportController@viewPerformancePerEmployee');
        Route::post('reports/{report_id}/view/performance_per_employee', 'ReportController@viewPerformancePerEmployee');

        Route::get('reports/{report_id}/view/portal_status_history', 'ReportController@viewPortalStatusHistory');
        Route::post('reports/{report_id}/view/portal_status_history', 'ReportController@viewPortalStatusHistory');

        Route::get('reports/{report_id}/view/request_quality', 'ReportController@viewRequestQuality');
        Route::post('reports/{report_id}/view/request_quality', 'ReportController@viewRequestQuality');

        Route::get('reports/{report_id}/view/lead_amount', 'ReportController@viewLeadAmounts');
        Route::post('reports/{report_id}/view/lead_amount', 'ReportController@viewLeadAmounts');

        Route::get('reports/{report_id}/view/reviews_per_customer', 'ReportController@viewReviewsPerCustomer');
        Route::post('reports/{report_id}/view/reviews_per_customer', 'ReportController@viewReviewsPerCustomer');

        Route::get('reports/{report_id}/view/surveys_experience', 'ReportController@viewSurveysExperience');
        Route::post('reports/{report_id}/view/surveys_experience', 'ReportController@viewSurveysExperience');

        Route::get('reports/{report_id}/view/turnover_per_sales_manager', 'ReportController@viewTurnoverPerSalesManager');
        Route::post('reports/{report_id}/view/turnover_per_sales_manager', 'ReportController@viewTurnoverPerSalesManager');

        Route::get('reports/{report_id}/view/quality_score_calculations', 'ReportController@viewQualityScoreCalculations');
        Route::post('reports/{report_id}/view/quality_score_calculations', 'ReportController@viewQualityScoreCalculations');

        Route::get('reports/{report_id}/view/conversion_tools_statistics', 'ReportController@viewConversionTools');
        Route::post('reports/{report_id}/view/conversion_tools_statistics', 'ReportController@viewConversionTools');

        Route::get('reports/{report_id}/view/matching_statistics', 'ReportController@viewMatchStatistics');
        Route::post('reports/{report_id}/view/matching_statistics', 'ReportController@viewMatchStatistics');

        Route::get('reports/{report_id}/view/service_provider_requests', 'ReportController@viewServiceProviderRequests');
        Route::post('reports/{report_id}/view/service_provider_requests', 'ReportController@viewServiceProviderRequests');

        Route::get('reports/{report_id}/view/requests', 'ReportController@viewRequests');
        Route::post('reports/{report_id}/view/requests', 'ReportController@viewRequests');

        Route::get('reports/{report_id}/view/rejected_requests', 'ReportController@viewRejectedRequests');
        Route::post('reports/{report_id}/view/rejected_requests', 'ReportController@viewRejectedRequests');

        Route::get('reports/{report_id}/view/requests_per_rejection_reason', 'ReportController@viewRequestsPerRejectionReason');
        Route::post('reports/{report_id}/view/requests_per_rejection_reason', 'ReportController@viewRequestsPerRejectionReason');

        Route::get('reports/{report_id}/view/surveys_per_mover_request', 'ReportController@viewSurveysPerMoverRequest');
        Route::post('reports/{report_id}/view/surveys_per_mover_request', 'ReportController@viewSurveysPerMoverRequest');

        Route::get('reports/{report_id}/view/sirelo_customers', 'ReportController@viewSireloCustomers');
        Route::post('reports/{report_id}/view/sirelo_customers', 'ReportController@queueSireloCustomers');
        Route::get('reports/{report_id}/view/sirelo_customers/{progress_id}/view', 'ReportController@viewFinishedSireloCustomer');
        Route::get('reports/{report_id}/view/sirelo_customers/{progress_id}/clone', 'ReportController@cloneSireloCustomer');

        Route::get('reports/{report_id}/view/customer_logins', 'ReportController@viewCustomerLogins');
        Route::post('reports/{report_id}/view/customer_logins', 'ReportController@queueCustomerLogins');
        Route::get('reports/{report_id}/view/customer_logins/{progress_id}/view', 'ReportController@viewFinishedCustomerLogins');
        Route::get('reports/{report_id}/view/customer_logins/{progress_id}/clone', 'ReportController@cloneCustomerLogins');

        Route::get('reports/{report_id}/view/customer_analysis', 'ReportController@viewCustomerAnalysis');
        Route::post('reports/{report_id}/view/customer_analysis', 'ReportController@queueCustomerAnalysis');
        Route::get('reports/{report_id}/view/customer_analysis/{progress_id}/view', 'ReportController@viewFinishedCustomerAnalysis');
        Route::get('reports/{report_id}/view/customer_analysis/{progress_id}/clone', 'ReportController@cloneCustomerAnalysis');

        Route::get('reports/{report_id}/view/customer_portal_analysis', 'ReportController@viewCustomerPortalAnalysis');
        Route::post('reports/{report_id}/view/customer_portal_analysis', 'ReportController@queueCustomerPortalAnalysis');
        Route::get('reports/{report_id}/view/customer_portal_analysis/{progress_id}/view', 'ReportController@viewFinishedCustomerPortalAnalysis');
        Route::get('reports/{report_id}/view/customer_portal_analysis/{progress_id}/clone', 'ReportController@cloneCustomerPortalAnalysis');

        Route::get('reports/{report_id}/view/customer_cappings', 'ReportController@viewCustomerCappings');
        Route::post('reports/{report_id}/view/customer_cappings', 'ReportController@queueCustomerCappings');
        Route::get('reports/{report_id}/view/customer_cappings/{progress_id}/view', 'ReportController@viewFinishedCustomerCappings');
        Route::get('reports/{report_id}/view/customer_cappings/{progress_id}/clone', 'ReportController@cloneCustomerCappings');

        Route::get('reports/{report_id}/view/results_per_category', 'ReportController@viewResultsPerCategory');
        Route::post('reports/{report_id}/view/results_per_category', 'ReportController@queueResultsPerCategory');
        Route::get('reports/{report_id}/view/results_per_category/{progress_id}/view', 'ReportController@viewFinishedResultsPerCategory');
        Route::get('reports/{report_id}/view/results_per_category/{progress_id}/clone', 'ReportController@cloneResultsPerCategory');

        Route::get('reports/{report_id}/view/results_per_site', 'ReportController@viewResultsPerSite');
        Route::post('reports/{report_id}/view/results_per_site', 'ReportController@queueResultsPerSite');
        Route::get('reports/{report_id}/view/results_per_site/{progress_id}/view', 'ReportController@viewFinishedResultsPerSite');
        Route::get('reports/{report_id}/view/results_per_site/{progress_id}/clone', 'ReportController@cloneResultsPerSite');

        Route::get('reports/{report_id}/view/turnover_per_request_per_country', 'ReportController@viewTurnoverPerRequestPerCountry');
        Route::post('reports/{report_id}/view/turnover_per_request_per_country', 'ReportController@queueTurnoverPerRequestPerCountry');
        Route::get('reports/{report_id}/view/turnover_per_request_per_country/{progress_id}/view', 'ReportController@viewFinishedTurnoverPerRequestPerCountry');
        Route::get('reports/{report_id}/view/turnover_per_request_per_country/{progress_id}/clone', 'ReportController@cloneTurnoverPerRequestPerCountry');

        Route::get('reports/{report_id}/view/region_occupancy', 'ReportController@viewRegionOccupancy');
        Route::post('reports/{report_id}/view/region_occupancy', 'ReportController@queueRegionOccupancy');
        Route::get('reports/{report_id}/view/region_occupancy/{progress_id}/view', 'ReportController@viewFinishedRegionOccupancy');
        Route::get('reports/{report_id}/view/region_occupancy/{progress_id}/clone', 'ReportController@cloneRegionOccupancy');

        Route::get('reports/{report_id}/view/missed_matches', 'ReportController@viewMissedMatches');
        Route::post('reports/{report_id}/view/missed_matches', 'ReportController@queueMissedMatches');
        Route::get('reports/{report_id}/view/missed_matches/{progress_id}/view', 'ReportController@viewFinishedMissedMatches');
        Route::get('reports/{report_id}/view/missed_matches/{progress_id}/clone', 'ReportController@cloneMissedMatches');

        Route::get('reports/{report_id}/view/adyen_credit_card_batch', 'ReportController@viewAdyenBatch');
        Route::get('reports/{report_id}/view/adyen_credit_card_batch/{progress_id}/view', 'ReportController@viewFinishedAdyenBatch');

        //URL/Action to cancel or delete a report.
        Route::get('reports/report_progress/{progress_id}/{action}', 'ReportController@reportProgress');

        Route::get('reports/{report_id}/view/status_update', 'ReportController@viewStatusUpdate');
        Route::post('reports/{report_id}/view/status_update', 'ReportController@viewStatusUpdate');

        Route::get('reports/{report_id}/view/lead_monitoring', 'ReportController@viewLeadMonitoring');
        Route::post('reports/{report_id}/view/lead_monitoring', 'ReportController@viewLeadMonitoring');

        Route::get('reports/{report_id}/view/automatic_matching', 'ReportController@viewAutomaticMatching');
        Route::post('reports/{report_id}/view/automatic_matching', 'ReportController@viewAutomaticMatching');

        Route::get('reports/{report_id}/view/lead_monitoring_per_site', 'ReportController@viewLeadMonitoringPerSite');
        Route::post('reports/{report_id}/view/lead_monitoring_per_site', 'ReportController@viewLeadMonitoringPerSite');

        Route::get('reports/{report_id}/view/invoiced_affiliate_partners', 'ReportController@viewInvoicedAffiliatePartners');
        Route::post('reports/{report_id}/view/invoiced_affiliate_partners', 'ReportController@viewInvoicedAffiliatePartners');

        Route::get('reports/{report_id}/view/active_campaign', 'ReportController@viewActiveCampaign');
        Route::post('reports/{report_id}/view/active_campaign', 'ReportController@viewActiveCampaign');

        Route::get('reports/{report_id}/view/review_ratings_correction', 'ReportController@viewReviewRatingsCorrection');
        Route::post('reports/{report_id}/view/review_ratings_correction', 'ReportController@viewReviewRatingsCorrection');

        Route::get('reports/{report_id}/view/review_platforms_per_country', 'ReportController@viewReviewPlatformsPerCountry');
        Route::post('reports/{report_id}/view/review_platforms_per_country', 'ReportController@viewReviewPlatformsPerCountry');

        Route::get('reports/{report_id}/view/recovered_dashboard_leads', 'ReportController@viewRecoveredDashboardLeads');
        Route::post('reports/{report_id}/view/recovered_dashboard_leads', 'ReportController@viewRecoveredDashboardLeads');

        Route::get('reports/{report_id}/view/lead_quality', 'ReportController@viewLeadQuality');
        Route::post('reports/{report_id}/view/lead_quality', 'ReportController@viewLeadQuality');

        Route::get('reports/{report_id}/view/python_process_validated_rows', 'ReportController@viewPythonProcessValidatedRows');
        Route::post('reports/{report_id}/view/python_process_validated_rows', 'ReportController@viewPythonProcessValidatedRows');

        Route::get('reports/{report_id}/view/mobilityex_validated_rows', 'ReportController@viewMobilityExValidatedRows');
        Route::post('reports/{report_id}/view/mobilityex_validated_rows', 'ReportController@viewMobilityExValidatedRows');

        Route::get('reports/{report_id}/view/google_data_fetch_results', 'ReportController@viewGoogleDataFetchResults');
        Route::post('reports/{report_id}/view/google_data_fetch_results', 'ReportController@viewGoogleDataFetchResults');

        Route::get('reports/{report_id}/view/mobilityex_fetch_results', 'ReportController@viewMobilityExFetchResults');
        Route::post('reports/{report_id}/view/mobilityex_fetch_results', 'ReportController@viewMobilityExFetchResults');

        Route::get('reports/{report_id}/view/related_moving_companies_fetch_results', 'ReportController@viewRelatedMovingCompaniesFetchResults');
        Route::post('reports/{report_id}/view/related_moving_companies_fetch_results', 'ReportController@viewRelatedMovingCompaniesFetchResults');

        Route::get( 'reports/customer_review_scores', 'ReportController@dumpReviewScores' );
        Route::get( 'reports/python_process', 'ReportController@dumpPythonProcess' );

        Route::get('reports/{report_id}/view/pythonProcess', 'ReportController@viewPythonProcess');

        Route::get('reports/{report_id}/view/widget_visits', 'ReportController@viewWidgetVisits');
        Route::post('reports/{report_id}/view/widget_visits', 'ReportController@viewWidgetVisits');

        Route::get('reports/{report_id}/view/lead_hourly_revenues', 'ReportController@viewLeadRevenuesPerHour');
        Route::post('reports/{report_id}/view/lead_hourly_revenues', 'ReportController@viewLeadRevenuesPerHour');

        Route::get('reports/{report_id}/view/sales_activity', 'ReportController@salesActivity');
        Route::post('reports/{report_id}/view/sales_activity', 'ReportController@salesActivity');

        Route::get('reports/{report_id}/view/associated_companies', 'ReportController@associatedCompanies');
        Route::post('reports/{report_id}/view/associated_companies', 'ReportController@associatedCompanies');


    });

    /**
	 * Reports end
	 */

    Route::any('/search', function(\Illuminate\Http\Request $request) {

    	$system = new System();

		// Get the searched term
		$term = $request->{"page-header-search-input"};

		// Customers default array
		$customers = [];
		$countries = [];
		$languages = [];

		// If term is numeric (ID)
		if( is_numeric( $term ) ) {

			// Find customer by id
			$customers = Customer::where( 'cu_id', $term )->get();
		}
		elseif( strlen( $term ) > 2) {

			// Set like search term
			$likeTerm = '%'.$term.'%';

			// Search for contact persons
			$contact_persons = ContactPerson
				::where(function ($query) use ($likeTerm) {
					$query
						->where( 'cope_email','LIKE', $likeTerm )
						->orWhere( 'cope_full_name', 'LIKE', $likeTerm);
					})
				->where("cope_deleted", 0)
				->get();

			$ids = [];

			if($contact_persons->count())
			{
				foreach ($contact_persons as $contact_person)
				{
					$ids[] = $contact_person->cope_cu_id;
				}
			}

			// Search for customers
			$customers = Customer::where(function ($query) use ($likeTerm, $ids) {
					$query
						->where( 'cu_company_name_business','LIKE', $likeTerm )
						->orWhere( 'cu_company_name_legal', 'LIKE', $likeTerm)
						->orWhere( 'cu_email', 'LIKE', $likeTerm )
						->orWhere( 'cu_email_bi', 'LIKE', $likeTerm )
						->whereIn("cu_id", $ids, 'or');
				});

            //Check if this user got restrictions
            $customer_restrictions = CustomerRestrictions::all()[Auth::user()->us_id];

            if (!empty($customer_restrictions)) {
                //User has some restrictions
                $customers->whereIn("cu_co_code", $customer_restrictions);
            }

			$customers = $customers->where('cu_deleted', 0)
				->groupBy( 'cu_id' )
				->get();

		}

		foreach( Country::all() as $country ) {

			$countries[$country->co_code] = $country->co_en;
		}

		foreach( Language::where("la_iframe_only", 0)->get() as $language ) {

			$languages[$language->la_code] = $language->la_language;
		}

		foreach( $customers as $customer ) {
			$customer->status = $system->getStatus($customer->cu_id);
		}

		if( count( $customers ) > 0 ) {

			$customertypes = CustomerType::all();
			$customertypes[1] = "MV";
			$customertypes[2] = "SP";
			$customertypes[3] = "AP";
			$customertypes[6] = "LR";

			return view('search', [
				//'customers' => $customers,
				'countries' => $countries,
				'languages' => $languages,
				'details' => $customers,
				'customertypes' => $customertypes
			])->withQuery( $term );
		}
		else {

			return view( 'search' )->withMessage( 'No Details found. Try to search again!' );
		}
	});
});

Auth::routes();


