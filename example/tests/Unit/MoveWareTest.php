<?php

namespace Tests\Unit;

use App\Models\KTRequestCustomerPortal;
use App\Models\RequestDelivery;
use App\RequestDeliveries\MoveWare;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Prophecy\Argument;
use Tests\TestCase;

class MoveWareTest extends TestCase
{

    public function httpCommunicationErrorsProvider(){
        yield [new RequestException('Error Communicating with Server', new Request('GET', 'test'))];
        yield [new Response(302, ['Content-Length' => 0])];
        yield [new Response(400, ['X-Foo' => 'Bar'], 'abc')];
    }

    /**
     * @dataProvider httpCommunicationErrorsProvider
     */
    public function testHttpCommunicationErrors($HttpResponse)
    {

        $mock = new MockHandler([
            $HttpResponse
        ]);

        $handlerStack = HandlerStack::create($mock);
        $client = new Client(['handler' => $handlerStack]);

        $request = $this->prophesize(\App\Models\Request::class);
        $requestDelivery = $this->prophesize(\App\Models\RequestDelivery::class);
        $requestDelivery->getMatchIdForRequest($request->reveal())->willReturn(5)->shouldBeCalled();
        $requestDelivery->getAttribute(Argument::any())->willReturn(1);

        $moveWare = new MoveWare($client);

        $this->expectException(\Exception::class);

        $moveWare->send($requestDelivery->reveal(), $request->reveal());

    }

    /*public function testXMLBuild(){

    }*/

}
