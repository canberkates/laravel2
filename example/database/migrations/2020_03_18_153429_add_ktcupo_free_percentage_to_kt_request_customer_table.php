<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKtcupoFreePercentageToKtRequestCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kt_request_customer_portal', function (Blueprint $table) {
            $table->decimal('ktrecupo_free_percentage', 10, 2)->after("ktrecupo_free");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kt_request_customer_portal', function (Blueprint $table) {
            $table->dropColumn('ktrecupo_free_percentage');
        });
    }
}
