<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMovingAssistantEmailcronjobTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('moving_assistant_email', function (Blueprint $table) {
            $table->increments('moasem_id');
            $table->timestamp("moasem_timestamp");
            $table->integer('moasem_embute_id');
            $table->integer('moasem_days_before_mail');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('moving_assistant_email');
    }
}
