<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSireloSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sirelo_settings', function (Blueprint $table) {
            $table->increments('sise_id');
            $table->timestamp('sise_timestamp');
            $table->string('sise_name', 100);
            $table->string('sise_description', 100);
            $table->text('sise_value');
            $table->string('sise_type', 10);
            $table->timestamp('sise_updated_timestamp');
        });
        $date_now = date("Y-m-d H:i:s");

        $setting = new \App\Models\SireloSetting();
        $setting->sise_timestamp = $date_now;
        $setting->sise_name = "google_reviews_total_amount";
        $setting->sise_description = "Google Reviews Total Amount";
        $setting->sise_value = "150";
        $setting->sise_type = "int";
        $setting->sise_updated_timestamp = $date_now;
        $setting->save();

        $setting = new \App\Models\SireloSetting();
        $setting->sise_timestamp = $date_now;
        $setting->sise_name = "google_reviews_rate";
        $setting->sise_description = "Google Reviews Rate";
        $setting->sise_value = "4";
        $setting->sise_type = "int";
        $setting->sise_updated_timestamp = $date_now;
        $setting->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sirelo_settings');
    }
}
