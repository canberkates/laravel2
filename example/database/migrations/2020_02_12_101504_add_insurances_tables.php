<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInsurancesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_insurances', function (Blueprint $table) {
            $table->increments('cuin_id');
            $table->string('cuin_name', 255);
            $table->text('cuin_remark');
        });

        Schema::create('kt_customer_insurances', function (Blueprint $table) {
            $table->increments('ktcuin_id');
            $table->integer('ktcuin_cuin_id');
            $table->integer('ktcuin_cu_id');
            $table->integer('ktcuin_cudo_id')->nullable();
            $table->dateTime('ktcuin_verified_timestamp');
            $table->integer('ktcuin_verification_result');
            $table->foreign('ktcuin_cu_id')->references('cu_id')->on('customers');
            $table->foreign('ktcuin_cudo_id')->references('cudo_id')->on('customer_documents');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_insurances');
        Schema::dropIfExists('kt_customer_insurances');
    }
}
