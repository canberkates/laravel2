<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFinalUrlsToCurescTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_review_scores', function (Blueprint $table) {
            $table->string('curesc_final_url_erp', 255)->nullable();
            $table->string('curesc_final_url_other', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_review_scores', function (Blueprint $table) {
            $table->dropColumn('curesc_final_url_erp');
            $table->dropColumn('curesc_final_url_other');
        });
    }
}
