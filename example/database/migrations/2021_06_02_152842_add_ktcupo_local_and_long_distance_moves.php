<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKtcupoLocalAndLongDistanceMoves extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kt_customer_portal', function (Blueprint $table) {
            $table->integer('ktcupo_nat_local_moves')->default(1)->after("ktcupo_export_moves");
            $table->integer('ktcupo_nat_long_distance_moves')->default(1)->after("ktcupo_nat_local_moves");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kt_customer_portal', function (Blueprint $table) {
            $table->dropColumn('ktcupo_nat_local_moves');
            $table->dropColumn('ktcupo_nat_long_distance_moves');
        });
    }
}
