<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeToCalls extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('planned_calls', function (Blueprint $table) {
            $table->integer('plca_type')->after("plca_us_id")->default(1);
            $table->renameColumn('plca_cancel_reason', 'plca_person_spoken_to');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('planned_calls', function (Blueprint $table) {
            $table->dropColumn('plca_type');
            $table->renameColumn('plca_person_spoken_to','plca_cancel_reason');

        });
    }
}
