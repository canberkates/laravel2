<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFormulaValuesToMatchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kt_request_customer_portal', function (Blueprint $table) {
            $table->integer('ktrecupo_match_position')->nullable();
            $table->decimal('ktrecupo_average_match')->nullable();
            $table->decimal('ktrecupo_formula_price')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kt_request_customer_portal', function (Blueprint $table) {
            $table->dropColumn('ktrecupo_match_position');
            $table->dropColumn('ktrecupo_average_match');
            $table->dropColumn('ktrecupo_formula_price');
        });
    }
}
