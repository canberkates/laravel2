<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMinimumPriceToMoverDirectPricingSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mover_direct_pricing_settings', function (Blueprint $table) {
            $table->integer('modiprse_minimum_price')->default(250);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mover_direct_pricing_settings', function (Blueprint $table) {
            $table->dropColumn('modiprse_minimum_price');
        });
    }
}
