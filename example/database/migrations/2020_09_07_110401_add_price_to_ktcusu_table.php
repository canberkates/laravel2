<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPriceToKtcusuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kt_customer_subscription', function (Blueprint $table) {
            $table->decimal('ktcusu_price_this_month', 10, 2)->after('ktcusu_price');

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kt_customer_subscription', function (Blueprint $table) {
            $table->dropColumn('ktcusu_price_this_month');
        });
    }
}
