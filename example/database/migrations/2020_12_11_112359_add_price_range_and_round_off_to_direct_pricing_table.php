<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPriceRangeAndRoundOffToDirectPricingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mover_direct_pricing_settings', function (Blueprint $table) {
            $table->decimal('modiprse_price_range_down', 10 , 2)->default(5);
            $table->decimal('modiprse_price_range_up', 10 , 2)->default(5);
            $table->integer('modiprse_price_round_off')->default(25);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('direct_pricing', function (Blueprint $table) {
            $table->dropColumn('modiprse_price_range_down');
            $table->dropColumn('modiprse_price_range_up');
            $table->dropColumn('modiprse_price_round_off');
        });
    }
}
