<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEurToAfpareRequests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('affiliate_partner_requests', function (Blueprint $table) {
            $table->decimal('afpare_amount_eur', 10, 2)->default(0.00)->after("afpare_amount");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('affiliate_partner_requests', function (Blueprint $table) {
            $table->dropColumn('afpare_amount_eur');
        });
    }
}
