<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoverVolumeCalculatorSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mover_volume_calculator_settings', function (Blueprint $table) {
            $table->increments('movocase_id');
            $table->string('movocase_token', 255);
            $table->Integer('movocase_cu_id');
            $table->string('movocase_theme_color', 255)->default("#7fb866");
            $table->string('movocase_header', 255)->nullable();
            $table->string('movocase_footer', 255)->nullable();
            $table->string('movocase_email_to', 255)->nullable();
            $table->string('movocase_display_website', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mover_volume_calculator_settings');
    }
}
