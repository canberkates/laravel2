<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMoverWidgetVisitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mover_widget_visits', function (Blueprint $table) {
            $table->increments('mowivi_id');
            $table->integer("mowivi_cu_id")->nullable();
            $table->date("mowivi_date");
            $table->integer('mowivi_visits');
            $table->text('mowivi_widget_url')->nullable();
            $table->foreign('mowivi_cu_id')->references('cu_id')->on('customers');
            $table->unique(['mowivi_cu_id', 'mowivi_date']);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mover_widget_visits');
    }
}
