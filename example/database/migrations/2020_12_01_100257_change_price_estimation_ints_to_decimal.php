<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePriceEstimationIntsToDecimal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mover_price_estimation_settings', function (Blueprint $table) {
            $table->decimal('mopres_price_estimation_starting_price', 10, 2)->change();
            $table->decimal('mopres_price_estimation_price_per_km', 10, 2)->change();
            $table->decimal('mopres_price_estimation_price_per_m2', 10, 2)->change();
            $table->decimal('mopres_price_estimation_price_per_m3', 10, 2)->change();
            $table->decimal('mopres_price_estimation_price_per_hour', 10, 2)->change();
            $table->decimal('mopres_price_estimation_price_range_down', 10, 2)->change();
            $table->decimal('mopres_price_estimation_price_range_up', 10, 2)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
