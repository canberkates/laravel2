<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTimestampToFetchedGoogleHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fetched_google_history', function (Blueprint $table) {
            $table->timestamp('fegohi_timestamp')->after("fegohi_cu_id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fetched_google_history', function (Blueprint $table) {
            $table->dropColumn('fegohi_timestamp');
        });
    }
}
