<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableToTrackConversionToolStats extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conversion_tools_tracking', function (Blueprint $table) {
            $table->date("cototr_date");
            $table->integer("cototr_afpafo_id");
            $table->integer('cototr_type'); //1 = VC show, 2 = VC to Form, 3 = VC Mail, 4 = PE show, 5 = PE submit, 6 = PE to Form
            $table->integer("cototr_amount");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conversion_tools_tracking');
    }
}
