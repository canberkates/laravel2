<?php

use App\Models\Report;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAffiliateLeadsPaidToReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports', function (Blueprint $table) {
            $report = new Report();
            $report->rep_report = "affiliate_leads_paid";
            $report->rep_name = "Affiliate leads paid";
            $report->rep_description = "Overview of how much of the received affiliate leads are paid by matched moving companies.";
            $report->rep_heavy = 0;
            $report->save();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports', function (Blueprint $table) {
            //
        });
    }
}
