<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPublicPcloudLinkToPremiumleadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('premium_leads', function (Blueprint $table) {
            $table->text('prle_pcloud_public_link')->nullable();
            $table->integer('prle_pcloud_file_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('premium_leads', function (Blueprint $table) {
            $table->dropColumn('prle_pcloud_public_link');
            $table->dropColumn('prle_pcloud_file_id');
        });
    }
}
