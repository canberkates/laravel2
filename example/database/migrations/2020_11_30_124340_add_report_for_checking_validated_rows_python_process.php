<?php

use App\Models\Report;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReportForCheckingValidatedRowsPythonProcess extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $report = new Report();
        $report->rep_report = "python_process_validated_rows";
        $report->rep_name = "Python Process - Validated rows";
        $report->rep_description = "Report to see who validated which gathered row";
        $report->rep_heavy = 0;
        $report->save();

        Schema::table('customer_review_scores', function (Blueprint $table) {
            $table->integer('curesc_processed_by')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_review_scores', function (Blueprint $table) {
            $table->dropColumn('curesc_processed_by');
        });
    }
}
