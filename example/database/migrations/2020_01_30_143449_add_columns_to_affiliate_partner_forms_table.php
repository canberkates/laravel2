<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToAffiliatePartnerFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('affiliate_partner_forms', function (Blueprint $table) {
            $table->integer('afpafo_tracking_clientid')->default(0)->after("afpafo_conversion_code");
            $table->text('afpafo_tracking_domains')->nullable()->after("afpafo_tracking_clientid");
        });

        Schema::table('users', function (Blueprint $table) {
            $table->integer('us_force_change_password')->default(0);
        });

        Schema::table('mover_data', function (Blueprint $table) {
            $table->integer('moda_not_operational')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('affiliate_partner_forms', function (Blueprint $table) {
            $table->dropColumn('afpafo_tracking_clientid');
            $table->dropColumn('afpafo_tracking_domains');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('us_force_change_password');
        });

        Schema::table('mover_data', function (Blueprint $table) {
            $table->dropColumn('moda_not_operational');
        });
    }
}
