<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVocaColumnsToMoverFormSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('mover_volume_calculator_settings');

        Schema::table('mover_form_settings', function (Blueprint $table) {
            $table->integer('mofose_voca_enabled')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mover_form_settings', function (Blueprint $table) {
            $table->dropColumn('mofose_voca_enabled');
        });
    }
}
