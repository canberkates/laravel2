<?php

use App\Data\CustomerProgressType;
use App\Models\Customer;
use App\Models\CustomerStatus;
use App\Models\KTCustomerProgress;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddModaPauseDaysToMoverDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Set pause days, default on 0
        Schema::table('mover_data', function (Blueprint $table) {
            $table->integer('moda_pause_days')->default(0)->after("moda_load_exchange_email");
        });

        // Select all customers
        $query_customers = Customer::leftJoin("mover_data", "cu_id", "moda_cu_id")
            ->whereIn("cu_type", [1, 6])
            ->where("cu_deleted", 0)
            ->get();

        // Loop customers from bucket
        foreach ($query_customers as $row_customers)
        {
            //First check if they are active now, if they are, give them 60 days as a default.
            $progresses = KTCustomerProgress::where("ktcupr_cu_id", $row_customers->cu_id)
                ->where("ktcupr_deleted", 0)
                ->orderBy("ktcupr_timestamp", "desc")
                ->get();

            $last_won = null;

            foreach($progresses as $key => $progress){
                if($progress->ktcupr_type == CustomerProgressType::CANCELLED){
                    break;
                }elseif($progress->ktcupr_type == CustomerProgressType::WON){
                    $last_won = $key;
                    break;
                }
            }

            if($last_won !== null){
                //If no timestamp, give 60 days
                if($progresses[$last_won]->ktcupr_timestamp == null){
                    $calculation = 60;
                }else{

                    $month = date("m",strtotime($progresses[$last_won]->ktcupr_timestamp));
                    $year = date("Y",strtotime($progresses[$last_won]->ktcupr_timestamp));

                    if($year < date("Y")){
                        $calculation = 60;
                    }else{
                        $calculation = 65 - ($month * 5);
                    }
                }
            }

            DB::table('mover_data')
                ->where('moda_cu_id', $row_customers->cu_id)
                ->update(
                    [
                        'moda_pause_days' => $calculation
                    ]
                );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mover_data', function (Blueprint $table) {
            $table->dropColumn('moda_pause_days');
        });
    }
}
