<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewMatchStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('match_statistics', function (Blueprint $table) {
            $table->Integer('mast_re_id');
            $table->Integer('mast_cu_id');
            $table->Integer('mast_matched');
            $table->Integer('mast_podium');

            $table->foreign('mast_re_id')->references('re_id')->on('requests');
            $table->foreign('mast_cu_id')->references('cu_id')->on('customers');

            $table->primary(["mast_re_id", "mast_cu_id"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('match_statistics');
    }
}
