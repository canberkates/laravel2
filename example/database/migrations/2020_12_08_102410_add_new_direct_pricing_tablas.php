<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewDirectPricingTablas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mover_direct_pricing_settings', function (Blueprint $table) {
            $table->increments('modiprse_id');
            $table->integer('modiprse_mofose_id');
            $table->string('modiprse_currency', 3);
            $table->decimal('modiprse_price_per_mover_per_hour', 10, 2)->default("27");
            $table->decimal('modiprse_price_per_km', 10, 2)->default("0.5");
        });

        Schema::create('mover_direct_pricing_range', function (Blueprint $table) {
            $table->increments('modiprra_id');
            $table->integer('modiprra_modiprse_id');
            $table->integer('modiprra_from_km')->default("0");
            $table->integer('modiprra_to_km')->default("1000");
            $table->integer('modiprra_number_of_people')->default("2");
            $table->decimal('modiprra_hours_per_m3', 10, 2)->default("0.75");
            $table->decimal('modiprra_truck_cost_per_hour', 10, 2)->default("25");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mover_direct_pricing_settings');
        Schema::dropIfExists('mover_direct_pricing_range');
    }
}
