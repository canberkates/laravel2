<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActivatePremiumLeadsToMoverDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mover_data', function (Blueprint $table) {
            $table->integer('moda_activate_premium_leads')->default(0)->after('moda_load_exchange_daily');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mover_data', function (Blueprint $table) {
            $table->dropColumn('moda_activate_premium_leads');
        });
    }
}
