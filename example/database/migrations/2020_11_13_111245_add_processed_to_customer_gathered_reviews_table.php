<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProcessedToCustomerGatheredReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_gathered_reviews', function (Blueprint $table) {
            $table->integer("cugare_processed")->default(0);

        });

        $curesc_rows = \App\Models\CustomerReviewScore::all();

        foreach ($curesc_rows as $row) {
            if ($row->curesc_processed == 1) {
                DB::table('customer_gathered_reviews')
                    ->where('cugare_curesc_id', $row->curesc_id)
                    ->update(
                        [
                            'cugare_processed' => 1
                        ]
                    );
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_gathered_reviews', function (Blueprint $table) {
            $table->dropColumn('cugare_processed');

        });
    }
}
