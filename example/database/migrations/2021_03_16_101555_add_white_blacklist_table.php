<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWhiteBlacklistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gathered_companies_list', function (Blueprint $table) {
            $table->increments('gacoli_id');
            $table->integer('gacoli_type');
            $table->string('gacoli_co_code', 2);
            $table->string('gacoli_title', 255);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gathered_companies_list');
    }
}
