<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewTablesToSaveOtherGoogleData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gathered_companies', function (Blueprint $table) {
            $table->increments('gaco_id');
            $table->string('gaco_searched_city', 255);
            $table->string('gaco_searched_co_code', 3);
            $table->string('gaco_name', 255);
            $table->string('gaco_title', 255)->nullable();
        });

        Schema::create('gathered_related_search', function (Blueprint $table) {
            $table->increments('garese_id');
            $table->string('garese_searched_city', 255);
            $table->string('garese_searched_co_code', 3);
            $table->string('garese_related_search', 255);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gathered_companies');
        Schema::dropIfExists('gathered_related_search');
    }
}
