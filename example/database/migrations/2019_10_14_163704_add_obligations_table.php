<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddObligationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_obligations', function (Blueprint $table) {
            $table->increments('cuob_id');
            $table->integer('cuob_type');
            $table->string('cuob_co_code', 2);
            $table->string('cuob_name', 255);
            $table->text('cuob_remark');
            $table->foreign('cuob_co_code')->references('we_sirelo_co_code')->on('websites');
        });

        Schema::create('kt_customer_obligations', function (Blueprint $table) {
            $table->increments('ktcuob_id');
            $table->integer('ktcuob_cuob_id');
            $table->integer('ktcuob_cu_id');
            $table->integer('ktcuob_cudo_id')->nullable();
            $table->dateTime('ktcuob_verified_timestamp');
            $table->integer('ktcuob_verification_result');
            $table->foreign('ktcuob_cu_id')->references('cu_id')->on('customers');
            $table->foreign('ktcuob_cudo_id')->references('cudo_id')->on('customer_documents');
            $table->foreign('ktcuob_cuob_id')->references('cuob_id')->on('customer_obligations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_obligations');
        Schema::dropIfExists('kt_customer_obligations');
    }
}
