<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultChildValuesToMoverData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mover_data', function (Blueprint $table) {
            $table->integer('moda_default_child_type')->nullable();
            $table->integer('moda_default_child_sirelo_type')->nullable();
            $table->integer('moda_default_child_mover_portal_type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mover_data', function (Blueprint $table) {
            $table->dropColumn('moda_default_child_type');
            $table->dropColumn('moda_default_child_sirelo_type');
            $table->dropColumn('moda_default_child_mover_portal_type');
        });
    }
}
