<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKtcupomovingsize10ToKtcustomerportaltable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kt_customer_portal', function (Blueprint $table) {
            $table->integer('ktcupo_moving_size_10')->after("ktcupo_moving_size_9");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kt_customer_portal', function (Blueprint $table) {
            $table->dropColumn('ktcupo_moving_size_10');
        });
    }
}
