<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVolumeCalculatorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('volume_calculator', function (Blueprint $table) {
            $table->increments('voca_id');
            $table->integer('voca_we_id')->nullable();
            $table->string('voca_la_code', 5);
            $table->integer('voca_type')->default(0);
            $table->dateTime('voca_timestamp');
            $table->string('voca_email', 100);
            $table->decimal('voca_volume_m3', 10, 1)->nullable();
            $table->decimal('voca_volume_ft3', 10, 1)->nullable();
            $table->text('voca_volume_calculator');
            $table->integer('voca_mail_sent')->default(0);

        });

        Schema::table('requests', function (Blueprint $table) {
            $table->integer('re_voca_id')->after('re_volume_calculator')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('volume_calculator');

        Schema::table('requests', function (Blueprint $table) {
            $table->dropColumn('re_voca_id');
        });
    }
}
