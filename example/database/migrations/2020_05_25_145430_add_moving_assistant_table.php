<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMovingAssistantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('moving_assistant', function (Blueprint $table) {
            $table->increments('moas_id');
            $table->timestamp("moas_timestamp");
            $table->string('moas_email', 255);
            $table->string('moas_first_name', 255);
            $table->date('moas_moving_date');
            $table->integer('moas_we_id');
        });

        Schema::create('email_builder_templates', function (Blueprint $table) {
            $table->increments('embute_id');
            $table->timestamp("embute_timestamp");
            $table->string('embute_language', 2);
            $table->string('embute_name', 255);
            $table->text('embute_description');
            $table->text('embute_filename');
            $table->string('embute_from_email', 255);
            $table->string('embute_to_cc', 255)->nullable();
            $table->string('embute_to_bcc', 255)->nullable();
            $table->string('embute_subject', 255)->nullable();
            $table->text('embute_content_html')->nullable();
            $table->integer('embute_created_by');
            $table->integer('embute_last_updated_by');
            $table->timestamp('embute_last_updated_timestamp');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('moving_assistant');
        Schema::dropIfExists('email_builder_templates');
    }
}
