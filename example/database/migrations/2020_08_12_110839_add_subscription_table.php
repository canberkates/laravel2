<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubscriptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->increments('sub_id');
            $table->string('sub_name', 255);
        });

        Schema::create('kt_customer_subscription', function (Blueprint $table) {
            $table->increments('ktcusu_id');
            $table->integer('ktcusu_cu_id');
            $table->integer('ktcusu_sub_id');
            $table->date('ktcusu_start_date');
            $table->date('ktcusu_end_date')->nullable();
            $table->date('ktcusu_cancellation_date')->nullable();
            $table->decimal('ktcusu_price', 10, 2);
            $table->string('ktcusu_pacu_code');
            $table->integer('ktcusu_payment_interval');
            $table->integer('ktcusu_active')->default(0);
            $table->integer('ktcusu_invoiced')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscriptions');
        Schema::dropIfExists('kt_customer_subscription');
    }
}
