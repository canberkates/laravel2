<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCustomerNameToNewGatheringTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gathered_companies', function (Blueprint $table) {
            $table->text('gaco_searched_customer')->after("gaco_id");
        });
        Schema::table('gathered_related_search', function (Blueprint $table) {
            $table->text('garese_searched_customer')->after("garese_id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gathered_companies', function (Blueprint $table) {
            $table->dropColumn('gaco_searched_customer');
        });
        Schema::table('gathered_related_search', function (Blueprint $table) {
            $table->dropColumn('garese_searched_customer');
        });
    }
}
