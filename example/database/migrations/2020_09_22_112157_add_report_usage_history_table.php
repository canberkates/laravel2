<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReportUsageHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_usage_history', function (Blueprint $table) {
            $table->increments('reushi_id');
            $table->timestamp('reushi_timestamp');
            $table->integer('reushi_us_id');
            $table->integer('reushi_rep_id');

            $table->foreign('reushi_rep_id')->references('rep_id')->on('reports');
            $table->foreign('reushi_us_id')->references('us_id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_usage_history');
    }
}
