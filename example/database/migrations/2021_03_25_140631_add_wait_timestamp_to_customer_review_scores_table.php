<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWaitTimestampToCustomerReviewScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_review_scores', function (Blueprint $table) {
            $table->timestamp('curesc_wait_timestamp')->nullable()->after("curesc_timestamp");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_review_scores', function (Blueprint $table) {
            $table->dropColumn('curesc_wait_timestamp');
        });
    }
}
