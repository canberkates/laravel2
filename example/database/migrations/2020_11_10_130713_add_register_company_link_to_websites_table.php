<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRegisterCompanyLinkToWebsitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('websites', function (Blueprint $table) {
            $table->string('we_register_company_location', 255)->nullable()->after("we_sirelo_request_form_location");
        });
        Schema::table('website_subdomains', function (Blueprint $table) {
            $table->string('wesu_register_company_location', 255)->nullable();
        });

        $website = \App\Models\Website::where("we_website", "Sirelo.nl")->first();
        $website->we_register_company_location = "https://sirelo.nl/bedrijf-registreren/";
        $website->save();

        $website = \App\Models\Website::where("we_website", "Sirelo.de")->first();
        $website->we_register_company_location = "https://sirelo.de/firma-registrieren/";
        $website->save();

        $website = \App\Models\Website::where("we_website", "Sirelo.co.uk")->first();
        $website->we_register_company_location = "https://sirelo.co.uk/register-company/";
        $website->save();

        $website = \App\Models\Website::where("we_website", "Sirelo.fr")->first();
        $website->we_register_company_location = "https://sirelo.fr/referencer-une-entreprise/";
        $website->save();

        $website = \App\Models\Website::where("we_website", "Sirelo.es")->first();
        $website->we_register_company_location = "https://sirelo.es/registrar-empresa/";
        $website->save();

        $website = \App\Models\Website::where("we_website", "Sirelo.at")->first();
        $website->we_register_company_location = "https://sirelo.at/firma-registrieren/";
        $website->save();

        $website = \App\Models\Website::where("we_website", "Sirelo.co.za")->first();
        $website->we_register_company_location = "https://sirelo.co.za/register-company/";
        $website->save();

        $website = \App\Models\Website::where("we_website", "Sirelo.com.au")->first();
        $website->we_register_company_location = "https://sirelo.com.au/register-company/";
        $website->save();

        $website = \App\Models\Website::where("we_website", "Sirelo.it")->first();
        $website->we_register_company_location = "https://sirelo.it/registra-lazienda/";
        $website->save();

        $website = \App\Models\Website::where("we_website", "sirelo.com")->first();
        $website->we_register_company_location = "https://sirelo.com/register-company/";
        $website->save();

        $subdomains = \App\Models\WebsiteSubdomain::all();

        foreach ($subdomains as $subdomain) {
            $subdomain->wesu_register_company_location = $subdomain->wesu_url."/register-company/";
            $subdomain->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('websites', function (Blueprint $table) {
            $table->dropColumn('we_register_company_location');
        });
        Schema::table('website_subdomains', function (Blueprint $table) {
            $table->dropColumn('wesu_register_company_location');
        });
    }
}
