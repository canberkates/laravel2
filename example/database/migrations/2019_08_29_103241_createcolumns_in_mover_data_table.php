<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatecolumnsInMoverDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mover_data', function (Blueprint $table) {
            $table->integer('moda_quality_management_iso')->default(0)->after("moda_iso");
            $table->integer('moda_environmental_management_iso')->default(0)->after("moda_iso");
            $table->integer('moda_occupational_health_and_safety_iso')->default(0)->after("moda_iso");
            $table->integer('moda_information_security_management_iso')->default(0)->after("moda_iso");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mover_data', function (Blueprint $table) {
            $table->dropColumn('moda_quality_management_iso');
            $table->dropColumn('moda_environmental_management_iso');
            $table->dropColumn('moda_occupational_health_and_safety_iso');
            $table->dropColumn('moda_information_security_management_iso');
        });
    }
}
