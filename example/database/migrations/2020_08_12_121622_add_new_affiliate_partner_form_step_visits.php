<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewAffiliatePartnerFormStepVisits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('affiliate_partner_form_steps', function (Blueprint $table) {
            $table->increments('afpafost_id');
            $table->Integer('afpafost_afpafo_id');
            $table->date('afpafost_date');
            $table->Integer('afpafost_step');
            $table->Integer('afpafost_visits');

            $table->foreign('afpafost_afpafo_id')->references('afpafo_id')->on('affiliate_partner_forms');

            $table->unique(['afpafost_afpafo_id','afpafost_date', 'afpafost_step'], "primary_id_afpafo_step");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('affiliate_partner_form_steps');
    }
}
