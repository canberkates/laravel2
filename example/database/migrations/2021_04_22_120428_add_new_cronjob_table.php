<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewCronjobTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cronjobs', function (Blueprint $table) {
            $table->integer('cr_id')->primary();
            $table->string('cr_task');
            $table->string('cr_subtask');
            $table->integer('cr_trigger');
            $table->integer('cr_error_count');
            $table->string("cr_interval");
            $table->timestamp("cr_last_ran");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cronjobs');

    }
}
