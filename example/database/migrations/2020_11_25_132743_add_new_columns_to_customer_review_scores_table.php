<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumnsToCustomerReviewScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*Schema::table('customer_review_scores', function (Blueprint $table) {
            $table->integer('curesc_original_score')->nullable();
            $table->integer('curesc_original_range')->nullable();

        });*/
        Schema::table('customer_gathered_reviews', function (Blueprint $table) {
            $table->decimal('cugare_original_score', 10, 1)->nullable()->after("cugare_score");
            $table->integer('cugare_original_range')->nullable()->after("cugare_original_score");

        });
        Schema::table('kt_customer_gathered_reviews', function (Blueprint $table) {
            $table->decimal('ktcugare_original_score', 10, 1)->nullable()->after("ktcugare_score");
            $table->integer('ktcugare_original_range')->nullable()->after("ktcugare_original_score");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_gathered_reviews', function (Blueprint $table) {
            $table->dropColumn('cugare_original_score');
            $table->dropColumn('cugare_original_range');
        });
        Schema::table('kt_customer_gathered_reviews', function (Blueprint $table) {
            $table->dropColumn('ktcugare_original_score');
            $table->dropColumn('ktcugare_original_range');
        });
    }
}
