<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewMoverPriceEstimationCalculationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mover_price_estimation_calculation', function (Blueprint $table) {
            $table->increments('mopresca_id');
            $table->integer('mopresca_mopres_id');
            $table->integer('mopresca_type');
            $table->string('mopresca_size')->nullable();;
            $table->decimal('mopresca_starting_price', 10);
            $table->decimal('mopresca_price_per_km', 10)->nullable();
            $table->decimal('mopresca_price_per_mile', 10)->nullable();
            $table->decimal('mopresca_price_per_m2', 10)->nullable();
            $table->decimal('mopresca_price_per_m3', 10)->nullable();
            $table->decimal('mopresca_price_per_hour', 10)->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mover_price_estimation_calculation');
    }
}
