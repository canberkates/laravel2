<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSireloQuoteFormLocationToWebsitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('websites', function (Blueprint $table) {
            $table->string('we_sirelo_request_form_location', 100)->nullable();
        });

        DB::table("websites")
            ->where("we_website", "Sirelo.nl")
            ->update(
                [
                    "we_sirelo_request_form_location" => "https://sirelo.nl/offerte-formulier/"
                ]
            );

        DB::table("websites")
            ->where("we_website", "Sirelo.de")
            ->update(
                [
                    "we_sirelo_request_form_location" => "https://sirelo.de/anfrageformular/"
                ]
            );

        DB::table("websites")
            ->where("we_website", "Sirelo.co.uk")
            ->update(
                [
                    "we_sirelo_request_form_location" => "https://sirelo.co.uk/quote-form/"
                ]
            );

        DB::table("websites")
            ->where("we_website", "Sirelo.fr")
            ->update(
                [
                    "we_sirelo_request_form_location" => "https://sirelo.fr/formulaire-de-demande/"
                ]
            );

        DB::table("websites")
            ->where("we_website", "Sirelo.es")
            ->update(
                [
                    "we_sirelo_request_form_location" => "https://sirelo.es/formulario-de-cotizacion/"
                ]
            );

        DB::table("websites")
            ->where("we_website", "Sirelo.at")
            ->update(
                [
                    "we_sirelo_request_form_location" => "https://sirelo.at/anfrageformular/"
                ]
            );

        DB::table("websites")
            ->where("we_website", "Sirelo.co.za")
            ->update(
                [
                    "we_sirelo_request_form_location" => "https://sirelo.co.za/quote-form/"
                ]
            );

        DB::table("websites")
            ->where("we_website", "Sirelo.com.au")
            ->update(
                [
                    "we_sirelo_request_form_location" => "https://sirelo.com.au/quote-form/"
                ]
            );

        DB::table("websites")
            ->where("we_website", "Sirelo.it")
            ->update(
                [
                    "we_sirelo_request_form_location" => "https://sirelo.it/modulo-preventivi/"
                ]
            );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('websites', function (Blueprint $table) {
            $table->dropColumn('we_sirelo_request_form_location');
        });
    }
}
