<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddChildrenToCustomers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->integer('cu_parent_id')->nullable()->after("cu_id");
        });

        Schema::table('customer_offices', function (Blueprint $table) {
            $table->integer('cuof_child_id')->nullable()->after("cuof_cu_id");
            $table->string("cuof_street_1")->nullable()->change();
            $table->string("cuof_city")->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->dropColumn('cu_parent_id');
        });


        Schema::table('customer_offices', function (Blueprint $table) {
            $table->dropColumn('cuof_child_id');
        });
    }

}
