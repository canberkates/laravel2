<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMailColumnToCustomerReviewScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_review_scores', function (Blueprint $table) {
            $table->integer("curesc_mail")->default(0);
        });

        DB::table('customer_review_scores')
            ->where('curesc_processed', 1)
            ->where('curesc_accepted', 1)
            ->update(
                [
                    'curesc_mail' => 1
                ]
            );

        DB::table('customer_review_scores')
            ->where('curesc_processed', 1)
            ->where('curesc_accepted', 0)
            ->update(
                [
                    'curesc_mail' => 3
                ]
            );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_review_scores', function (Blueprint $table) {
            $table->dropColumn('curesc_mail');
        });
    }
}
