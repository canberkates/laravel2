<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdvertorialBlocksTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_provider_advertorial_blocks', function (Blueprint $table) {
            $table->increments('sepradbl_id');
            $table->integer('sepradbl_cu_id');
            $table->integer('sepradbl_slot');
            $table->text('sepradbl_description');
            $table->integer('sepradbl_status');
            $table->string('sepradbl_event_category', 50);
            $table->string('sepradbl_event_label', 50);
            $table->string('sepradbl_action', 50);
        });

        Schema::create('kt_service_provider_advertorial_block_language_content', function (Blueprint $table) {
            $table->increments('ktsepradbllaco_id');
            $table->integer('ktsepradbllaco_sepradbl_id');
            $table->string('ktsepradbllaco_la_code', 5);
            $table->string('ktsepradbllaco_title', 100)->nullable();
            $table->text('ktsepradbllaco_content')->nullable();
            $table->string('ktsepradbllaco_button', 100)->nullable();
            $table->string('ktsepradbllaco_url', 50);
        });

        Schema::create('kt_service_provider_advertorial_block_language', function (Blueprint $table) {
            $table->increments('ktsepradblla_id');
            $table->integer('ktsepradblla_sepradbl_id');
            $table->string('ktsepradblla_la_code', 5);
        });

        Schema::create('kt_service_provider_advertorial_block_website', function (Blueprint $table) {
            $table->increments('ktsepradblwe_id');
            $table->integer('ktsepradblwe_sepradbl_id');
            $table->integer('ktsepradblwe_we_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_provider_advertorial_blocks');
        Schema::dropIfExists('kt_service_provider_advertorial_block_language');
        Schema::dropIfExists('kt_service_provider_advertorial_block_website');
        Schema::dropIfExists('kt_service_provider_advertorial_block_language_content');

    }
}
