<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddServicesToMoverDirectPricingSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mover_direct_pricing_settings', function (Blueprint $table) {
            $table->decimal('modiprse_packing_cost', 10 ,2)->default(3.5);
            $table->decimal('modiprse_assembly_cost', 10 ,2)->default(4.5);
            $table->decimal('modiprse_storage_cost_per_m3_per_day', 10 ,2)->default(1.5);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mover_direct_pricing_settings', function (Blueprint $table) {
            $table->dropColumn('modiprse_packing_cost');
            $table->dropColumn('modiprse_assembly_cost');
            $table->dropColumn('modiprse_storage_cost_per_m3_per_day');
        });
    }
}
