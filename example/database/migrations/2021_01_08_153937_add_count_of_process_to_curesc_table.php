<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCountOfProcessToCurescTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_review_scores', function (Blueprint $table) {
            $table->integer('curesc_processed_counter')->default(0);
        });

        DB::table('customer_review_scores')
            ->where('curesc_mail', ">", 0)
            ->update(
                [
                    'curesc_processed_counter' => 1,
                ]
            );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_review_scores', function (Blueprint $table) {
            $table->dropColumn('curesc_processed_counter');
        });
    }
}
