<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePauseHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pause_history', function (Blueprint $table) {
            $table->increments('ph_id');
            $table->dateTime('ph_created_timestamp');
            $table->Integer('ph_us_id')->nullable();
            $table->Integer('ph_apus_id')->nullable();
            $table->Integer('ph_cu_id');
            $table->date('ph_start_date');
            $table->date('ph_end_date');
            $table->Integer('ph_total_days');
            $table->text('ph_remark');
            $table->foreign('ph_us_id')->references('us_id')->on('users');
            $table->foreign('ph_apus_id')->references('apus_id')->on('application_users');
            $table->foreign('ph_cu_id')->references('cu_id')->on('customers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pause_history');
    }
}
