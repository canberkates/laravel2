<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePremiumLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('premium_leads', function (Blueprint $table) {
            $table->increments('prle_id');
            $table->integer('prle_re_id');
            $table->integer('prle_status')->default('0');
            $table->dateTime('prle_preferred_appointment_timestamp')->nullable();
            $table->dateTime('prle_planned_appointment_timestamp')->nullable();
            $table->dateTime('prle_planned_timestamp')->nullable();
            $table->integer('prle_planned_by')->nullable();
            $table->integer('prle_residence_type_from')->nullable();
            $table->text('prle_inside_elevator_from')->nullable();
            $table->text('prle_obstacles_for_moving_van_from')->nullable();
            $table->text('prle_parking_restrictions_and_permits_from')->nullable();
            $table->text('prle_walking_distance_to_house_from')->nullable();
            $table->integer('prle_residence_type_to')->nullable();
            $table->text('prle_inside_elevator_to')->nullable();
            $table->text('prle_obstacles_for_moving_van_to')->nullable();
            $table->text('prle_parking_restrictions_and_permits_to')->nullable();
            $table->text('prle_walking_distance_to_house_to')->nullable();
            $table->text('prle_list_of_all_items')->nullable();
            $table->text('prle_special_items')->nullable();
            $table->text('prle_extra_info_moving_date')->nullable();
            $table->integer('prle_packing_by_mover')->nullable();
            $table->text('prle_packing_by_mover_remarks')->nullable();
            $table->integer('prle_unpacking_by_mover')->nullable();
            $table->text('prle_unpacking_by_mover_remarks')->nullable();
            $table->integer('prle_disassembling_by_mover')->nullable();
            $table->text('prle_disassembling_by_mover_remarks')->nullable();
            $table->integer('prle_assembling_by_mover')->nullable();
            $table->text('prle_assembling_by_mover_remarks')->nullable();
            $table->text('prle_handyman_service')->nullable();
            $table->integer('prle_storage')->nullable();
            $table->text('prle_storage_remarks')->nullable();
            $table->integer('prle_insurance')->nullable();
            $table->text('prle_insurance_remarks')->nullable();
            $table->text('prle_insurance_amount')->nullable();
            $table->foreign('prle_re_id')->references('re_id')->on('requests');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('premium_leads');
    }
}
