<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoasTokenToMoverAssistantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('moving_assistant', function (Blueprint $table) {
            $table->string('moas_token', 255)->after("moas_id");
            $table->integer('moas_unsubscribed')->default(0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('moving_assistant', function (Blueprint $table) {
            $table->dropColumn('moas_token');
            $table->dropColumn('moas_unsubscribed');
        });
    }
}
