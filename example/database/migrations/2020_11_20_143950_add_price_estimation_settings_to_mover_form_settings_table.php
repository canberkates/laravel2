<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPriceEstimationSettingsToMoverFormSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('mover_price_estimation_settings', function (Blueprint $table) {
            $table->increments('mopres_id');
            $table->integer('mopres_mofose_id');
            $table->string('mopres_price_estimation_currency')->default("EUR");
            $table->integer('mopres_price_estimation_starting_price')->default(100);
            $table->integer('mopres_price_estimation_price_per_km')->default(0);
            $table->integer('mopres_price_estimation_price_per_m2')->default(0);
            $table->integer('mopres_price_estimation_price_per_m3')->default(0);
            $table->integer('mopres_price_estimation_price_per_hour')->default(0);
            $table->integer('mopres_price_estimation_price_range_down')->default(5);
            $table->integer('mopres_price_estimation_price_range_up')->default(5);
            $table->integer('mopres_price_estimation_minimum_volume')->default(20);
            $table->integer('mopres_price_estimation_maximum_volume')->default(250);
            $table->integer('mopres_volume_type')->default(0);
            $table->integer('mopres_volume_format')->default(0);
            $table->string('mopres_main_color')->default("#F38422");
            $table->string('mopres_secondary_color')->default("#169fda");
            $table->string('mopres_google_autocomplete_country')->default("NL");
            $table->integer('mopres_show_quote_button')->default(1);
            $table->string('mopres_quote_button_url')->default("https://www.expertsinmoving.com/get-quotes/");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mover_price_estimation_settings');
    }
}
