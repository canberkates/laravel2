<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCuobIdToSireloHyperlinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sirelo_hyperlinks', function (Blueprint $table) {
            $table->integer('sihy_cuob_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sirelo_hyperlinks', function (Blueprint $table) {
            $table->dropColumn('sihy_cuob_id');
        });
    }
}
