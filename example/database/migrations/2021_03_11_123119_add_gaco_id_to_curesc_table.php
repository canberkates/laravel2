<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGacoIdToCurescTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_review_scores', function (Blueprint $table) {
            $table->integer('curesc_gaco_id')->nullable()->after("curesc_cu_id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_review_scores', function (Blueprint $table) {
            $table->dropColumn('curesc_gaco_id');
        });
    }
}
