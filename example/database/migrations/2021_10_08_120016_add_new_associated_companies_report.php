<?php

use App\Models\Report;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewAssociatedCompaniesReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $report = new Report();
        $report->rep_report = "associated_companies";
        $report->rep_name = "Associated Companies";
        $report->rep_description = "Shows companies with association and url";
        $report->rep_heavy = 0;
        $report->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
