<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWhitelistBacklistTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gathered_companies_title_whitelist', function (Blueprint $table) {
            $table->increments('gacotiwh_id');
            $table->string('gacotiwh_co_code', 2);
            $table->string('gacotiwh_title', 255);
        });
        Schema::create('gathered_companies_title_blacklist', function (Blueprint $table) {
            $table->increments('gacotibl_id');
            $table->string('gacotibl_co_code', 2);
            $table->string('gacotibl_title', 255);
        });

        Schema::table('gathered_companies', function (Blueprint $table) {
            $table->integer('gaco_whitelisted')->default(0);
            $table->integer('gaco_blacklisted')->default(0);
            $table->integer('gaco_processed')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gathered_companies_title_whitelist');
        Schema::dropIfExists('gathered_companies_title_blacklist');

        Schema::table('gathered_companies', function (Blueprint $table) {
            $table->dropColumn('gaco_whitelisted');
            $table->dropColumn('gaco_blacklisted');
            $table->dropColumn('gaco_processed');
        });
    }
}
