<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToMoverDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mover_data', function (Blueprint $table) {
            $table->integer('moda_reg_id')->nullable();
            $table->integer('moda_market_type')->nullable();
            $table->integer('moda_services')->nullable();
            $table->integer('moda_crm_status')->nullable();
            $table->integer('moda_owner')->nullable();

            $table->index("moda_reg_id");

            $table->foreign('moda_owner')->references('us_id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mover_data', function (Blueprint $table) {

            $table->dropColumn('moda_reg_id');
            $table->dropColumn('moda_market_type');
            $table->dropColumn('moda_services');
            $table->dropColumn('moda_crm_status');
            $table->dropColumn('moda_owner');
        });
    }
}
