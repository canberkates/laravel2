<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReAutomaticToRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('requests', function (Blueprint $table) {
            $table->Integer('re_automatic')->default(0)->after( 're_timestamp' );
        });

        Schema::create('automated_requests', function (Blueprint $table) {
            $table->increments('aure_id');
            $table->Integer('aure_re_id');
            $table->dateTime('aure_timestamp');
            $table->Integer('aure_decision');
            $table->text('aure_details');

            $table->foreign('aure_re_id')->references('re_id')->on('requests');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('requests', function (Blueprint $table) {
            $table->dropColumn(['re_automatic']);
        });

        Schema::dropIfExists('automated_requests');

    }
}
