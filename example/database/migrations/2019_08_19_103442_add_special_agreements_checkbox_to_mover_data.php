<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSpecialAgreementsCheckboxToMoverData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mover_data', function (Blueprint $table) {
            $table->integer('moda_special_agreements_checkbox')->default(0)->nullable()->after("moda_trucks");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mover_data', function (Blueprint $table) {
            $table->dropColumn('moda_special_agreements_checkbox');
        });
    }
}
