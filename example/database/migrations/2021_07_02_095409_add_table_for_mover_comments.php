<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTableForMoverComments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kt_mover_comments', function (Blueprint $table) {
            $table->increments('ktmoco_id');
            $table->integer('ktmoco_cu_id');
            $table->integer('ktmoco_su_id');
            $table->integer('ktmoco_type');
            $table->integer('ktmoco_read')->default(0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kt_mover_comments');
    }
}
