<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveWeeklyCappingFromKtCustomerPortal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kt_customer_portal', function (Blueprint $table) {
            $table->dropColumn('ktcupo_max_requests_week');
            $table->dropColumn('ktcupo_max_requests_week_left');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kt_customer_portal', function (Blueprint $table) {
            $table->integer('ktcupo_max_requests_week')->default('9999')->after("ktcupo_max_requests_month_left");
            $table->integer('ktcupo_max_requests_week_left')->default('9999')->after("ktcupo_max_requests_month_left");
        });
    }
}
