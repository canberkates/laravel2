<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBackgroundColorandFontColorToForm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mover_form_settings', function (Blueprint $table) {
            $table->string('mofose_background_color')->default("#ffffff")->after("mofose_theme_color");
            $table->string('mofose_font_color')->default("#363d4b")->after("mofose_theme_color");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mover_form_settings', function (Blueprint $table) {
            $table->dropColumn('mofose_background_color');
            $table->dropColumn('mofose_font_color');
        });
    }
}
