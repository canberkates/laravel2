<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKtCustomerLeadsStoreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kt_customer_leads_store', function (Blueprint $table) {
            $table->increments('ktculest_id');
            $table->timestamp('ktculest_timestamp');
            $table->integer('ktculest_cu_id');
            $table->integer('ktculest_re_id');
            $table->string('ktculest_currency', 3);
            $table->decimal('ktculest_price_fc', 10, 2);
            $table->decimal('ktculest_price_eur', 10, 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kt_customer_leads_store');
    }
}
