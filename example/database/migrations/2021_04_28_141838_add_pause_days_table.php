<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPauseDaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pause_days', function (Blueprint $table) {
            $table->increments('pada_id');
            $table->timestamp('pada_timestamp');
            $table->integer('pada_cu_id');
            $table->integer('pada_amount')->default(0);
            $table->integer('pada_amount_left')->default(0);
            $table->timestamp('pada_start_date');
            $table->timestamp('pada_expired');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pause_days');

    }
}
