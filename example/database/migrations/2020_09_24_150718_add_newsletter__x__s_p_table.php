<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewsletterXSPTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kt_service_provider_newsletter_block_sent', function (Blueprint $table) {
            $table->increments('ktseprneblse_id');
            $table->timestamp('ktseprneblse_timestamp');
            $table->integer('ktseprneblse_cu_id');
            $table->integer('ktseprneblse_seprnebl_id');
            $table->integer('ktseprneblse_slot');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kt_service_provider_newsletter_block_sent');
    }
}
