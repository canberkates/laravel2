<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRelationshipStatusToCustomerOffices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_offices', function (Blueprint $table) {
            $table->integer('cuof_child_type')->nullable();
            $table->integer('cuof_child_sirelo_type')->nullable();
            $table->integer('cuof_child_mover_portal_type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_offices', function (Blueprint $table) {
            $table->dropColumn('cuof_child_type');
            $table->dropColumn('cuof_child_sirelo_type');
            $table->dropColumn('cuof_child_mover_portal_type');
        });
    }
}
