<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\CustomerCachedTab;
use App\Models\Customer;

class AddCustomerCachedTabTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_cached_tabs', function (Blueprint $table) {
            $table->increments('cucata_id');
            $table->integer('cucata_cu_id');
            $table->integer('cucata_int_nat')->default(0);
            $table->integer('cucata_int')->default(0);
            $table->integer('cucata_nat')->default(0);
            $table->integer('cucata_leads_store')->default(0);
            $table->integer('cucata_conversion_tools')->default(0);
            $table->integer('cucata_pause')->default(0);
            $table->integer('cucata_cancelled')->default(0);
            $table->integer('cucata_credit_hold')->default(0);
            $table->integer('cucata_prepayment_credit_hold')->default(0);
            $table->integer('cucata_debt_collector')->default(0);
        });

        $customers = Customer::where("cu_deleted", 0)->where("cu_type", \App\Data\CustomerType::MOVER)->get();

        foreach($customers as $customer) {
            $new = new CustomerCachedTab();
            $new->cucata_cu_id = $customer->cu_id;
            $new->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_cached_tabs');
    }
}
