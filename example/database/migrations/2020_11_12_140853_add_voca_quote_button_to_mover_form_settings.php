<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVocaQuoteButtonToMoverFormSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mover_form_settings', function (Blueprint $table) {
            $table->integer('mofose_voca_show_quote_button')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mover_form_settings', function (Blueprint $table) {
            $table->dropColumn('mofose_voca_show_quote_button');
        });
    }
}
