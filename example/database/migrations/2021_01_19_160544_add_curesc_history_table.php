<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCurescHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fetched_google_history', function (Blueprint $table) {
            $table->increments('fegohi_id');
            $table->integer('fegohi_curesc_id');
            $table->integer('fegohi_cu_id');
            $table->integer('fegohi_fetched')->default(0);
            $table->integer('fegohi_skipped')->default(0);
            $table->integer('fegohi_changed')->default(0);
            $table->integer('fegohi_unchanged')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fetched_google_history');
    }
}
