<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKtCustomerProgressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kt_customer_progress', function (Blueprint $table) {
            $table->increments('ktcupr_id');
            $table->Integer('ktcupr_cu_id');
            $table->Integer('ktcupr_type');
            $table->Integer('ktcupr_closed_reason')->nullable();
            $table->dateTime('ktcupr_timestamp')->nullable();
            $table->Integer('ktcupr_us_id')->nullable();
            $table->Integer('ktcupr_cure_id')->nullable();
            $table->Integer('ktcupr_deleted');
            $table->foreign('ktcupr_us_id')->references('us_id')->on('users');
            $table->foreign('ktcupr_cu_id')->references('cu_id')->on('customers');
            $table->foreign('ktcupr_cure_id')->references('cure_id')->on('customer_remarks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kt_customer_progress');
    }
}
