<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToMofedaTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mobilityex_fetched_data', function (Blueprint $table) {
            $table->string('mofeda_co_code', 2)->nullable()->after("mofeda_city");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mobilityex_fetched_data', function (Blueprint $table) {
            $table->dropColumn('mofeda_co_code');
        });
    }
}
