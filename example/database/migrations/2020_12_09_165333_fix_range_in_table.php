<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixRangeInTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mover_direct_pricing_range', function (Blueprint $table) {
            $table->renameColumn('modiprra_from_km', 'modiprra_from_volume');
            $table->renameColumn('modiprra_to_km', 'modiprra_to_volume');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('table', function (Blueprint $table) {

            $table->renameColumn('modiprra_from_volume', 'modiprra_from_km');
            $table->renameColumn('modiprra_to_volume', 'modiprra_to_km');
        });
    }
}
