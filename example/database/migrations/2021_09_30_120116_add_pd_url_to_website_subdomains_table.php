<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPdUrlToWebsiteSubdomainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('website_subdomains', function (Blueprint $table) {
            $table->string('wesu_personal_dashboard_url', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('website_subdomains', function (Blueprint $table) {
            $table->dropColumn('wesu_personal_dashboard_url');
        });
    }
}
