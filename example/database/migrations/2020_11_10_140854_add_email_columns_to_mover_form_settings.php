<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmailColumnsToMoverFormSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mover_form_settings', function (Blueprint $table) {
            $table->text('mofose_voca_email_subject')->nullable();
            $table->text('mofose_voca_email_content')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mover_form_settings', function (Blueprint $table) {
            $table->dropColumn('mofose_voca_email_subject');
            $table->dropColumn('mofose_voca_email_content');
        });
    }
}
