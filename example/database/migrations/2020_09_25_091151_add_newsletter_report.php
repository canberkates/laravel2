<?php

use App\Models\Report;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewsletterReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $report = new Report();
        $report->rep_report = "service_provider_newsletterblocks";
        $report->rep_name = "Service provider newsletterblocks";
        $report->rep_description = "Report to analyse newsletterblocks sent per customer";
        $report->rep_heavy = 0;
        $report->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
