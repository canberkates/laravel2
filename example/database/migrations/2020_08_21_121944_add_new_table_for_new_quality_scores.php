<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewTableForNewQualityScores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quality_score_calculation', function (Blueprint $table) {
            $table->increments('qsc_id');
            $table->Integer('qsc_re_id');
            $table->mediumText('qsc_old_calc');
            $table->mediumText('qsc_new_calc');

            $table->foreign('qsc_re_id')->references('re_id')->on('requests');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quality_score_calculation');
    }
}
