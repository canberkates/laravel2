<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCurescCustomerWebsiteToCustomerReviewScores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_review_scores', function (Blueprint $table) {
            $table->string('curesc_customer_website', 255 )->after('curesc_address' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_review_scores', function (Blueprint $table) {
            $table->dropColumn('curesc_customer_website');
        });
    }
}
