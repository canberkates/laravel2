<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlaggedReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flagged_reviews', function (Blueprint $table) {
            $table->increments('flre_id');
            $table->integer('flre_su_id');
            $table->dateTime('flre_timestamp')->nullable();
            $table->integer('flre_status')->default('0');
            $table->text('flre_personal_information')->nullable();
            $table->text('flre_spam')->nullable();
            $table->text('flre_offensive_language')->nullable();
            $table->text('flre_conflicts')->nullable();
            $table->foreign('flre_su_id')->references('su_id')->on('surveys_2');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flagged_reviews');
    }
}
