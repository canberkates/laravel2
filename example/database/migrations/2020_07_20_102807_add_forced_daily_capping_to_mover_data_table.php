<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForcedDailyCappingToMoverDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mover_data', function (Blueprint $table) {
            $table->Integer("moda_forced_daily_capping")->default(9999)->after("moda_capping_monthly_limit_modifier");
            $table->Integer("moda_forced_daily_capping_left")->default(9999)->after("moda_forced_daily_capping");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mover_data', function (Blueprint $table) {
            $table->dropColumn('moda_forced_daily_capping');
            $table->dropColumn('moda_forced_daily_capping_left');
        });
    }
}
