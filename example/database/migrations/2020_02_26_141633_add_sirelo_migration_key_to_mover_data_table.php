<?php

use App\Functions\System;
use App\Models\ApplicationUser;
use App\Models\Customer;
use App\Models\CustomerOffice;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSireloMigrationKeyToMoverDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mover_data', function (Blueprint $table) {
            $table->string('moda_sirelo_key', 255)->nullable()->after("moda_sirelo_review_verification");
            $table->index('moda_sirelo_key');
        });

        // Select all customers from countries
        $query_customers = Customer::select("cu_id", "cu_city", "cu_company_name_business")
            ->leftJoin("mover_data", "cu_id", "moda_cu_id")
            ->where("cu_type", 1)
            ->where("cu_city", "!=", "")
            ->where("cu_deleted", 0)
            ->where("moda_disable_sirelo_export", 0)
            ->get();

        // Loop customers from bucket
        foreach ($query_customers as $row_customers)
        {
            // Create seo city key from city
            $city_key = System::getSeoKey($row_customers->cu_city);

            // Create customer key from city key and company business name
            $customer_key = $city_key . System::getSeoKey($row_customers->cu_company_name_business);

            DB::table('mover_data')
                ->where('moda_cu_id', $row_customers->cu_id)
                ->update(
                    [
                        'moda_sirelo_key' => $customer_key
                    ]
                );
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mover_data', function (Blueprint $table) {
            $table->dropColumn('moda_sirelo_key');
        });
    }
}
