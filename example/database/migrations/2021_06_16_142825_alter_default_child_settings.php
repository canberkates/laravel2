<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDefaultChildSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mover_data', function (Blueprint $table) {
            $table->integer('moda_parent_sirelo_type')->nullable();
            $table->renameColumn('moda_default_child_sirelo_type', 'moda_child_sirelo_type');
            $table->dropColumn('moda_default_child_type');
            $table->dropColumn('moda_default_child_mover_portal_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
