<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerReviewScores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_review_scores', function (Blueprint $table) {
            $table->increments('curesc_id' );
            $table->Integer('curesc_cu_id' );
            $table->string( 'curesc_platform' );
            $table->decimal( 'curesc_score', 2,1 );
            $table->Integer( 'curesc_amount' );
            $table->string( 'curesc_name', 255 );
            $table->string( 'curesc_telephone', 255 );
            $table->string( 'curesc_address', 255 );
            $table->string( 'curesc_url', 255 );
            $table->dateTime('curesc_timestamp')->nullable();
            $table->foreign( 'curesc_cu_id' )->references( 'cu_id' )->on( 'customers' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_review_scores');
    }
}
