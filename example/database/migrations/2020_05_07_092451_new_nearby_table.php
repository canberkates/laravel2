<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewNearbyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sirelo_cities_nearby', function (Blueprint $table) {
            $table->string('key', 255);
            $table->string('city', 255);
            $table->string('country_code', 2);
            $table->mediumText('nearby');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sirelo_cities_nearby');
    }
}
