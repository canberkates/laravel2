<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumnsToSurvey2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('surveys_2', function (Blueprint $table) {
            $table->text('su_rating_motivation_original')->nullable()->after("su_rating_motivation");
        });

        Schema::table('website_reviews', function (Blueprint $table) {
            $table->text('were_user_agent')->nullable()->after("were_device");
            $table->integer('were_reviewsubmitted_cookie')->default(0);
            $table->text('were_cookie_timestamp')->nullable();
            $table->text('were_cookie_previous_timestamp')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('surveys_2', function (Blueprint $table) {
            $table->dropColumn('su_rating_motivation_original');

        });
        Schema::table('website_reviews', function (Blueprint $table) {
            $table->dropColumn('were_user_agent');
            $table->dropColumn('were_cookie_previous_timestamp');
            $table->dropColumn('were_cookie_timestamp');
        });
    }
}
