<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPricesToPortals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('portals', function (Blueprint $table) {
            $table->decimal('po_minimum_price_int')->nullable();
            $table->decimal('po_maximum_price_int')->nullable();
            $table->decimal('po_minimum_price_nat_local')->nullable();
            $table->decimal('po_maximum_price_nat_local')->nullable();
            $table->decimal('po_minimum_price_nat_long_distance')->nullable();
            $table->decimal('po_maximum_price_nat_long_distance')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('portals', function (Blueprint $table) {
            $table->dropColumn('po_minimum_price_int');
            $table->dropColumn('po_maximum_price_int');
            $table->dropColumn('po_minimum_price_nat_local');
            $table->dropColumn('po_maximum_price_nat_local');
            $table->dropColumn('po_minimum_price_nat_long_distance');
            $table->dropColumn('po_maximum_price_nat_long_distance');
        });
    }
}
