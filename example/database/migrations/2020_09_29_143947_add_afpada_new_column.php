<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAfpadaNewColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('affiliate_partner_data', function (Blueprint $table) {
            $table->text('afpada_revenues')->nullable();
            $table->timestamp('afpada_revenues_calculated_timestamp')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('affiliate_partner_data', function (Blueprint $table) {
            $table->dropColumn('afpada_revenues');
            $table->dropColumn('afpada_revenues_calculated_timestamp');
        });
    }
}
