<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddServiceProviderDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_provider_data', function (Blueprint $table) {
            $table->increments('seprda_id');
            $table->integer('seprda_cu_id');
            $table->integer('seprda_lead_form')->default(0);
            $table->integer('seprda_email_marketing')->default(0);
            $table->integer('seprda_sirelo_advertising')->default(0);
        });

        $service_providers = \App\Models\Customer::where("cu_type", \App\Data\CustomerType::SERVICE_PROVIDER)->get();

        foreach ($service_providers as $sp) {
            DB::table("service_provider_data")
                ->insert([
                    'seprda_cu_id' => $sp->cu_id,
                    'seprda_lead_form' => 0,
                    'seprda_email_marketing' => 0,
                    'seprda_sirelo_advertising' => 0
                ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_provider_data');

    }
}
