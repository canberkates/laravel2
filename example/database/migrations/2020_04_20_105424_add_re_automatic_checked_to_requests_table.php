<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReAutomaticCheckedToRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('requests', function (Blueprint $table) {
            $table->Integer('re_automatic_checked')->default(0)->after( 're_automatic');
            $table->Integer('re_recover_reason')->default(0)->after( 're_rejection_reason');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('requests', function (Blueprint $table) {
            $table->dropColumn(['re_automatic_checked']);
            $table->dropColumn(['re_recover_reason']);
        });
    }
}
