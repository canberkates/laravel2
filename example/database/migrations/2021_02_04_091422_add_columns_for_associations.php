<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsForAssociations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mobilityex_fetched_data', function (Blueprint $table) {
            $table->integer('mofeda_skipped')->default(0)->after("mofeda_processed");
            $table->integer('mofeda_accepted')->default(0)->after("mofeda_skipped");
            $table->integer('mofeda_lock_user_id')->nullable();
            $table->timestamp("mofeda_lock_timestamp")->nullable();
            $table->integer('mofeda_processed_counter')->default(0);
            $table->text("mofeda_remarks")->nullable();
            $table->text("mofeda_history")->nullable();
        });

        Schema::table('kt_customer_membership', function (Blueprint $table) {
            $table->string('ktcume_link', 255)->nullable();
            $table->date("ktcume_exp_date")->nullable();
            $table->timestamp("ktcume_updated_timestamp")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mobilityex_fetched_data', function (Blueprint $table) {
            $table->dropColumn('mofeda_skipped');
            $table->dropColumn('mofeda_accepted');
            $table->dropColumn('mofeda_lock_user_id');
            $table->dropColumn('mofeda_lock_timestamp');
            $table->dropColumn('mofeda_processed_counter');
            $table->dropColumn('mofeda_remarks');
            $table->dropColumn('mofeda_history');
        });

        Schema::table('kt_customer_membership', function (Blueprint $table) {
            $table->dropColumn('ktcume_link');
            $table->dropColumn('ktcume_exp_date');
            $table->dropColumn('ktcume_updated_timestamp');
        });
    }
}
