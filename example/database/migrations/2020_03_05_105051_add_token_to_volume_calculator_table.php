<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTokenToVolumeCalculatorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('volume_calculator', function (Blueprint $table) {
            $table->text('voca_token')->nullable()->after("voca_id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('volume_calculator', function (Blueprint $table) {
            $table->dropColumn('voca_token');
        });
    }
}
