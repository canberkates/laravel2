<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWebsitevalidateToCustomerReviewScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_review_scores', function (Blueprint $table) {
            $table->integer('curesc_website_validate_type')->nullable();
            $table->string('curesc_website_validate_var_1', 255)->nullable();
            $table->string('curesc_website_validate_var_2', 255)->nullable();
            $table->string('curesc_website_validate_var_3', 255)->nullable();
            $table->string('curesc_website_validate_var_4', 255)->nullable();
            $table->string('curesc_website_validate_var_5', 255)->nullable();
            $table->string('curesc_website_validate_var_6', 255)->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_review_scores', function (Blueprint $table) {
            $table->dropColumn('curesc_website_validate_type');
        });
    }
}
