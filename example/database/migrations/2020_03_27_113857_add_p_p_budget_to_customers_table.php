<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPPBudgetToCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->integer('cu_pp_monthly_budget')->nullable()->after('cu_payment_method');
            $table->integer('cu_pp_charge_threshold')->nullable()->after('cu_pp_monthly_budget');
            $table->timestamp('cu_pp_charge_timestamp')->nullable()->after('cu_pp_charge_threshold');
            $table->integer('cu_pp_charge_credit_card_times')->nullable()->after('cu_pp_charge_timestamp');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->dropColumn('cu_pp_monthly_budget');
            $table->dropColumn('cu_pp_charge_threshold');
            $table->dropColumn('cu_pp_charge_timestamp');
            $table->dropColumn('cu_pp_charge_credit_card_times');
        });
    }
}
