<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAcceptDateToSurvey2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('surveys_2', function (Blueprint $table) {
            $table->dateTime("su_accepted_timestamp")->after("su_submitted_timestamp")->default("0000-00-00 00:00:00");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('surveys_2', function (Blueprint $table) {
            $table->dropColumn('su_accepted_timestamp');
        });
    }
}
