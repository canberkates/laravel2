<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMoverFormSettingsIdToRequestDeliveriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('request_deliveries', function (Blueprint $table) {
            $table->integer('rede_mofose_id')->nullable()->after("rede_ktcuqu_id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('request_deliveries', function (Blueprint $table) {
            $table->dropColumn('rede_mofose_id');
        });
    }
}
