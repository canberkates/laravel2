<?php

use App\Models\Report;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReviewReportToReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $report = new Report();
        $report->rep_report = "review_analyse";
        $report->rep_name = "Review Analyse";
        $report->rep_description = "Report for analysing the reviews.";
        $report->rep_heavy = 0;
        $report->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
