<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToMoverFormSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mover_form_settings', function (Blueprint $table) {
            $table->integer('mofose_ask_assembly')->default(0)->after("mofose_ask_flexible_moving_date");
            $table->integer('mofose_ask_packing')->default(0)->after("mofose_ask_flexible_moving_date");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mover_form_settings', function (Blueprint $table) {
            $table->dropColumn('mofose_ask_assembly');
            $table->dropColumn('mofose_ask_packing');
        });
    }
}
