<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToCustomerReviewScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_review_scores', function (Blueprint $table) {
            $table->integer('curesc_processed')->default(0)->after("curesc_url");
            $table->string('curesc_street', 255)->nullable()->after("curesc_address");
            $table->string('curesc_zipcode', 255)->nullable()->after("curesc_street");
            $table->string('curesc_city', 255)->nullable()->after("curesc_zipcode");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_review_scores', function (Blueprint $table) {
            $table->dropColumn('curesc_processed');
        });
    }
}
