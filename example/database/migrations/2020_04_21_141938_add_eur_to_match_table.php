<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEurToMatchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kt_request_customer_portal', function (Blueprint $table) {
            $table->decimal('ktrecupo_amount_eur', 10, 2)->default(0 )->after("ktrecupo_amount");
            $table->decimal('ktrecupo_amount_netto_eur', 10, 2)->default(0 )->after("ktrecupo_amount_netto");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kt_request_customer_portal', function (Blueprint $table) {
            $table->dropColumn('ktrecupo_amount_eur');
            $table->dropColumn('ktrecupo_amount_netto_eur');
        });
    }
}
