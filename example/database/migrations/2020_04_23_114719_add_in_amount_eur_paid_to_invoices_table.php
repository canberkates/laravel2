<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInAmountEurPaidToInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->decimal('in_amount_netto_eur_paid', 10, 2)->default(0 )->after("in_amount_netto_eur");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('amount_eur_paid_to_invoices', function (Blueprint $table) {
            $table->dropColumn('in_amount_netto_eur_paid');
        });
    }
}
