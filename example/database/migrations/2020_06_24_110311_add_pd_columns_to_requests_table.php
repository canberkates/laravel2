<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPdColumnsToRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('requests', function (Blueprint $table) {
            $table->integer('re_pd_validate')->default(0 );
        });

        Schema::create('request_updates', function (Blueprint $table) {
            $table->increments('reup_id');
            $table->integer('reup_re_id');
            $table->timestamp('reup_timestamp');
            $table->string('reup_key', 255)->nullable();
            $table->text('reup_value');
        });

        Schema::table('websites', function (Blueprint $table) {
            $table->text('we_personal_dashboard_url')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('requests', function (Blueprint $table) {
            $table->dropColumn('re_pd_validate');
        });
        Schema::table('websites', function (Blueprint $table) {
            $table->dropColumn('we_personal_dashboard_url');
        });

        Schema::dropIfExists('request_updates');
    }
}
