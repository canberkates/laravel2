<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGatheredCompaniesCountriesWhitelist extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gathered_companies_countries_whitelist', function (Blueprint $table) {
            $table->increments('gacocowh_id');
            $table->string('gacocowh_co_code', 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gathered_companies_countries_whitelist');
    }
}
