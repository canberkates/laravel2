<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToMobilityexFetchedDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mobilityex_fetched_data', function (Blueprint $table) {
            $table->string('mofeda_email', 255)->nullable()->after("mofeda_company_name_business");
            $table->integer('mofeda_processed')->default(0);
            $table->integer('mofeda_processed_by')->nullable();
            $table->integer('mofeda_mail')->default(0);
            $table->string('mofeda_mobilityex_url', 255)->nullable();
            $table->integer('mofeda_website_validate_type')->nullable();
            $table->string('mofeda_website_validate_var_1', 255)->nullable();
            $table->string('mofeda_website_validate_var_2', 255)->nullable();
            $table->string('mofeda_website_validate_var_3', 255)->nullable();
            $table->string('mofeda_website_validate_var_4', 255)->nullable();
            $table->string('mofeda_website_validate_var_5', 255)->nullable();
            $table->string('mofeda_website_validate_var_6', 255)->nullable();
            $table->string('mofeda_final_url_erp', 255)->nullable();
            $table->string('mofeda_final_url_mobilityex', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mobilityex_fetched_data', function (Blueprint $table) {
            $table->dropColumn('mofeda_email');
            $table->dropColumn('mofeda_final_url_erp');
            $table->dropColumn('mofeda_final_url_mobilityex');
            $table->dropColumn('mofeda_processed');
            $table->dropColumn('mofeda_processed_by');
            $table->dropColumn('mofeda_mail');
            $table->dropColumn('mofeda_mobilityex_url');
            $table->dropColumn('mofeda_website_validate_type');
            $table->dropColumn('mofeda_website_validate_var_1');
            $table->dropColumn('mofeda_website_validate_var_2');
            $table->dropColumn('mofeda_website_validate_var_3');
            $table->dropColumn('mofeda_website_validate_var_4');
            $table->dropColumn('mofeda_website_validate_var_5');
            $table->dropColumn('mofeda_website_validate_var_6');
        });
    }
}
