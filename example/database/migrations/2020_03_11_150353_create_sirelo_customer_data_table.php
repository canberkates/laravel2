<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSireloCustomerDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sirelo_customers', function (Blueprint $table) {
            $table->string('key', 255);
            $table->string('city_key', 255);
            $table->string('forward_1', 255);
            $table->string('forward_2', 255);
            $table->Integer('origin_id');
            $table->Integer('top_mover');
            $table->Integer('review_partner');
            $table->Integer('claimed');
            $table->string('company_name', 255);
            $table->string('legal_name', 255);
            $table->string('logo', 255);
            $table->mediumText('description');
            $table->string('coc', 255)->nullable();
            $table->string('language', 255);
            $table->Integer('national_moves');
            $table->Integer('international_moves');
            $table->Integer('long_distance_moving_europe');
            $table->Integer('excess_baggage');
            $table->Integer('man_and_van');
            $table->Integer('car_and_vehicle_transport');
            $table->Integer('piano_transport');
            $table->Integer('pet_transport');
            $table->Integer('art_transport');
            $table->date('established');
            $table->Integer('employees');
            $table->Integer('trucks');
            $table->string('street', 255);
            $table->string('zipcode', 255);
            $table->string('city', 255);
            $table->string('country', 255);
            $table->string('email', 255);
            $table->string('telephone', 255);
            $table->string('website', 255);
            $table->decimal('rating', 3,2);
            $table->Integer('reviews');
            $table->Integer('recommendation');
            $table->Integer('recommendations');
            $table->mediumText('pros_cons');
            $table->Integer('not_operational');
            $table->Integer('type_of_mover');
            $table->Integer('public_liability_insurance');
            $table->Integer('goods_in_transit_insurance');

            $table->primary("key");
        });

        Schema::create('sirelo_cities', function (Blueprint $table) {
            $table->string('key', 255);
            $table->string('city', 255);
            $table->string('country_code', 2);
            $table->Integer('amount');
            $table->Integer('state')->nullable();
            $table->mediumText('nearby');


            $table->primary(["key", "country_code"]);
        });


        Schema::create('sirelo_customer_offices', function (Blueprint $table) {
            $table->string('customer_key', 255);
            $table->string('city_key', 255);
            $table->string('country_code', 2);
            $table->string('street', 255);
            $table->string('zipcode', 255);
            $table->string('city', 255);
            $table->string('email', 255);
            $table->string('telephone', 255);
            $table->string('website', 255);

        });

        Schema::create('sirelo_customers_to_countries', function (Blueprint $table) {
            $table->string('customer_key', 255);
            $table->integer('origin_id');
            $table->string('country_code', 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sirelo_customers');
        Schema::dropIfExists('sirelo_customer_offices');
        Schema::dropIfExists('sirelo_cities');
        Schema::dropIfExists('sirelo_customers_to_countries');
    }
}
