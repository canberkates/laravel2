<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKtcupoTypeToKtCustomerPortalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kt_customer_portal', function (Blueprint $table) {
            $table->integer('ktcupo_type')->default(1)->after('ktcupo_po_id');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kt_customer_portal', function (Blueprint $table) {
            $table->dropColumn('ktcupo_type');
        });
    }
}
