<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWebsiteValidationHtmlToCurescTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_review_scores', function (Blueprint $table) {
            $table->text('curesc_new_company_website_validation_html')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_review_scores', function (Blueprint $table) {
            $table->dropColumn('curesc_new_company_website_validation_html');
        });
    }
}
