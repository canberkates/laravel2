<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeletedToCustomerReviewScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mobilityex_fetched_data', function (Blueprint $table) {
            $table->integer('mofeda_customer_deleted')->default(0);
        });
        Schema::table('customer_review_scores', function (Blueprint $table) {
            $table->integer('curesc_customer_deleted')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mobilityex_fetched_data', function (Blueprint $table) {
            $table->dropColumn('mofeda_customer_deleted');
        });
        Schema::table('customer_review_scores', function (Blueprint $table) {
            $table->dropColumn('curesc_customer_deleted');
        });
    }
}
