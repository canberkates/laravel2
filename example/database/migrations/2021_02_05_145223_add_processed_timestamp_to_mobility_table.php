<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProcessedTimestampToMobilityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mobilityex_fetched_data', function (Blueprint $table) {
            $table->timestamp("mofeda_processed_timestamp")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mobilityex_fetched_data', function (Blueprint $table) {
            $table->dropColumn('mofeda_processed_timestamp');
        });
    }
}
