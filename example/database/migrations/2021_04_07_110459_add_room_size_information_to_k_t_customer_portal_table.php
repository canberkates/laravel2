<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRoomSizeInformationToKTCustomerPortalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kt_customer_portal', function (Blueprint $table) {
            $table->integer('ktcupo_room_sizes_enabled')->default(0)->after("ktcupo_free_trial");
            $table->integer('ktcupo_room_size_1')->default(0)->after("ktcupo_room_sizes_enabled");
            $table->integer('ktcupo_room_size_2')->default(0)->after("ktcupo_room_size_1");
            $table->integer('ktcupo_room_size_3')->default(0)->after("ktcupo_room_size_2");
            $table->integer('ktcupo_room_size_4')->default(0)->after("ktcupo_room_size_3");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kt_customer_portal', function (Blueprint $table) {
            $table->dropColumn('ktcupo_room_sizes_enabled');
            $table->dropColumn('ktcupo_room_size_1');
            $table->dropColumn('ktcupo_room_size_2');
            $table->dropColumn('ktcupo_room_size_3');
            $table->dropColumn('ktcupo_room_size_4');
        });
    }
}
