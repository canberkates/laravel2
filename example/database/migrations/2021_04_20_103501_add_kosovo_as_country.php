<?php

use App\Models\Country;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKosovoAsCountry extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $country = new Country();
        $country->co_code = "XK";
        $country->co_code_3 = "XK";
        $country->co_con_id = 17;
        $country->co_en = "Kosovo";
        $country->co_nl = "Kosovo";
        $country->co_de = "Kosovo";
        $country->co_dk = "Kosovo";
        $country->co_fr = "Kosovo";
        $country->co_ru = "Косово";
        $country->co_es = "Kosovo";
        $country->co_it = "Kosovo";
        $country->co_pt = "Kosovo";
        $country->co_pl = "Kosowo";
        $country->in_eu = 0;
        $country->in_schengen_area = 0;
        $country->co_mare_id = 10;
        $country->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
