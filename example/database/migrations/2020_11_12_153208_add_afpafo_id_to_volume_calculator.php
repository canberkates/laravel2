<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAfpafoIdToVolumeCalculator extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('volume_calculator', function (Blueprint $table) {
            $table->integer("voca_afpafo_id")->nullable()->after("voca_we_id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('volume_calculator', function (Blueprint $table) {
            $table->dropColumn('voca_afpafo_id');
        });
    }
}
