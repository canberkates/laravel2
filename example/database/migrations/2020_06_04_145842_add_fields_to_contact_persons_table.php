<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToContactPersonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contact_persons', function (Blueprint $table) {
            $table->string("cope_last_name", 255)->nullable()->after("cope_first_name");
            $table->string("cope_title", 255)->nullable()->after("cope_gender");
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contact_persons', function (Blueprint $table) {
            $table->dropColumn('cope_last_name');
            $table->dropColumn('cope_title');
        });
    }
}
