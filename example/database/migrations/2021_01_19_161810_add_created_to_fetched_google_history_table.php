<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCreatedToFetchedGoogleHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fetched_google_history', function (Blueprint $table) {
            $table->integer('fegohi_created')->after("fegohi_fetched");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fetched_google_history', function (Blueprint $table) {
            $table->dropColumn('fegohi_created');
        });
    }
}
