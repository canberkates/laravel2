<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExtendMatchStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('match_statistics', function (Blueprint $table) {
            $table->integer('mast_ktcupo_id')->nullable();
            $table->integer('mast_match_position')->nullable();
            $table->decimal('mast_average_match', 20, 10)->nullable();
            $table->decimal('mast_match_percentage_before_podium', 20, 10)->nullable();
            $table->decimal('mast_match_percentage_after_podium', 20, 10)->nullable();
            $table->decimal('mast_correction_bonus', 20, 10)->nullable();
            $table->decimal('mast_lead_price_netto', 20, 10)->nullable();
            $table->decimal('mast_final_price', 20, 10)->nullable();
            $table->decimal('mast_formula_price', 20, 10)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('match_statistics', function (Blueprint $table) {
            $table->dropColumn('mast_ktcupo_id');
            $table->dropColumn('mast_match_position');
            $table->dropColumn('mast_average_match');
            $table->dropColumn('mast_match_percentage_before_podium');
            $table->dropColumn('mast_match_percentage_after_podium');
            $table->dropColumn('mast_correction_bonus');
            $table->dropColumn('mast_lead_price_netto');
            $table->dropColumn('mast_final_price');
            $table->dropColumn('mast_formula_price');
        });
    }
}
