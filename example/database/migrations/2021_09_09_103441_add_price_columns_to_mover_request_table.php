<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPriceColumnsToMoverRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mover_requests', function (Blueprint $table) {
            $table->string('more_price_estimation_low')->nullable();
            $table->string('more_price_estimation_high')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mover_requests', function (Blueprint $table) {
            $table->dropColumn('more_price_estimation_low');
            $table->dropColumn('more_price_estimation_high');
        });
    }
}
