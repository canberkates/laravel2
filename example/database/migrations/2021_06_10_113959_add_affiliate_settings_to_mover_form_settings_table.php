<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAffiliateSettingsToMoverFormSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mover_form_settings', function (Blueprint $table) {
            $table->string('mofose_title')->nullable();
            $table->string('mofose_subtext')->nullable();
            $table->integer('mofose_show_moving_sizes')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mover_form_settings', function (Blueprint $table) {
            $table->dropColumn('mofose_title');
            $table->dropColumn('mofose_subtext');
            $table->dropColumn('mofose_show_moving_sizes');
        });
    }
}
