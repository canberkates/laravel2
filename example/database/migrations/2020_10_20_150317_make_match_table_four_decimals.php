<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeMatchTableFourDecimals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('kt_request_customer_portal', function (Blueprint $table) {
            $table->decimal('ktrecupo_amount_discount', 10, 4)->change();
            $table->decimal('ktrecupo_amount_netto', 10, 4)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kt_request_customer_portal', function (Blueprint $table) {
            $table->decimal('ktrecupo_amount_discount', 10, 2)->change();
            $table->decimal('ktrecupo_amount_netto', 10, 2)->change();
        });
    }
}
