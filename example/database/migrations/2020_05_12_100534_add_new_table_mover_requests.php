<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewTableMoverRequests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mover_requests', function (Blueprint $table) {
            $table->increments('more_id');
            $table->string('more_token', 255);
            $table->Integer('more_cu_more_id');
            $table->Integer('more_cu_id');
            $table->Integer('more_afpafo_id');
            $table->dateTime('more_timestamp');
            $table->Integer('more_status')->default(0);
            $table->Integer('more_spam')->default(0);
            $table->string('more_la_code' ,2);
            $table->date('more_moving_date');
            $table->decimal('more_volume_m3', 10, 1);
            $table->decimal('more_volume_ft3', 10, 1);
            $table->Integer('more_voca_id')->nullable();
            $table->text('more_special_items')->nullable();

            $table->Integer('more_packing')->default(0);
            $table->Integer('more_assembly')->default(0);
            $table->Integer('more_storage')->default(0);
            $table->Integer('more_handyman')->default(0);
            $table->Integer('more_item_disposal')->default(0);
            $table->text('more_service_remarks')->nullable();

            $table->string('more_street_from', 255)->nullable();
            $table->string('more_zipcode_from', 255)->nullable();;
            $table->string('more_city_from', 255);
            $table->string('more_co_code_from', 2);
            $table->integer('more_floor_type_from')->default(0);
            $table->integer('more_walking_distance_from')->default(0);
            $table->integer('more_elevator_available_from')->default(0);
            $table->integer('more_parking_permit_needed_from')->default(0);

            $table->string('more_street_to', 255)->nullable();
            $table->string('more_zipcode_to', 255)->nullable();
            $table->string('more_city_to', 255);
            $table->string('more_co_code_to', 2);
            $table->integer('more_floor_type_to')->default(0);
            $table->integer('more_walking_distance_to')->default(0);
            $table->integer('more_elevator_available_to')->default(0);
            $table->integer('more_parking_permit_needed_to')->default(0);

            $table->string('more_full_name', 255);
            $table->string('more_telephone_1', 255);
            $table->string('more_email', 255);
            $table->string('more_device', 255)->nullable();
            $table->string('more_ip_address', 255)->nullable();

            $table->Integer('more_anonymized')->default(0);

            $table->foreign('more_cu_id')->references('cu_id')->on('customers');
            $table->foreign('more_afpafo_id')->references('afpafo_id')->on('affiliate_partner_forms');
            $table->foreign('more_co_code_from')->references('co_code')->on('countries');
            $table->foreign('more_co_code_to')->references('co_code')->on('countries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mover_requests');
    }
}
