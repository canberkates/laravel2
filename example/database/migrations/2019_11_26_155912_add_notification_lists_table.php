<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNotificationListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification_lists', function (Blueprint $table) {
            $table->increments('noli_id');
            $table->string('noli_name', 255);
        });
        
        Schema::create('kt_notification_user', function (Blueprint $table) {
            $table->increments('ktnous_id');
            $table->integer('ktnous_us_id');
            $table->integer('ktnous_noli_id');
            $table->foreign('ktnous_noli_id')->references('noli_id')->on('notification_lists');
            $table->foreign('ktnous_us_id')->references('us_id')->on('users');
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kt_notification_user');
        Schema::dropIfExists('notification_lists');
    }
}
