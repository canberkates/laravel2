<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGacoCountryStatusToTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gathered_companies', function (Blueprint $table) {
            $table->integer('gaco_country_status')->default(0)->after("gaco_title");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gathered_companies', function (Blueprint $table) {
            $table->dropColumn('gaco_country_status');
        });
    }
}
