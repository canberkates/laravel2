<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTablesForPythonProcess extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_gathered_reviews', function (Blueprint $table) {
            $table->increments('cugare_id');
            $table->integer('cugare_curesc_id');
            $table->integer('cugare_cu_id');
            $table->string('cugare_platform', 255);
            $table->decimal('cugare_score', 2, 1);
            $table->integer('cugare_amount');
            $table->string('cugare_url', 255)->nullable();
            $table->integer('cugare_accepted')->default(0);
        });

        Schema::create('kt_customer_gathered_reviews', function (Blueprint $table) {
            $table->increments('ktcugare_id');
            $table->integer('ktcugare_cu_id');
            $table->string('ktcugare_platform', 255);
            $table->decimal('ktcugare_score', 2, 1);
            $table->integer('ktcugare_amount');
            $table->string('ktcugare_url', 255)->nullable();
            $table->timestamp('ktcugare_timestamp_updated');
        });

        Schema::table('customer_review_scores', function (Blueprint $table) {
            $table->integer('curesc_accepted')->nullable()->after('curesc_use_reviews');
            $table->string('curesc_search_url',255)->nullable()->after('curesc_url');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_gathered_reviews');
        Schema::dropIfExists('kt_customer_gathered_reviews');

        Schema::table('customer_review_scores', function (Blueprint $table) {
            $table->dropColumn('curesc_accepted');
            $table->dropColumn('curesc_search_url');
        });
    }
}
