<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToMoverDataTablesss extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mover_data', function (Blueprint $table) {
            $table->integer('moda_iso')->nullable();
            $table->text('moda_premium_profile_summary')->nullable();
            $table->string('moda_premium_profile_payment_terms')->nullable();
            $table->integer('moda_payment_method_cash')->default(0);
            $table->integer('moda_payment_method_cheque')->default(0);
            $table->integer('moda_payment_method_bank_transfer')->default(0);
            $table->integer('moda_payment_method_auto_debit')->default(0);
            $table->integer('moda_payment_method_credit_card')->default(0);
            $table->integer('moda_payment_method_paypal')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mover_data', function (Blueprint $table) {
            $table->dropColumn('moda_iso');
            $table->dropColumn('moda_premium_profile_summary');
            $table->dropColumn('moda_premium_profile_payment_terms');
            $table->dropColumn('moda_payment_method_cash');
            $table->dropColumn('moda_payment_method_cheque');
            $table->dropColumn('moda_payment_method_bank_transfer');
            $table->dropColumn('moda_payment_method_auto_debit');
            $table->dropColumn('moda_payment_method_credit_card');
            $table->dropColumn('moda_payment_method_paypal');
        });
    }
}
