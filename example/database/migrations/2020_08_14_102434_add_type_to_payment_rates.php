<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeToPaymentRates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payment_rates', function (Blueprint $table) {
            $table->Integer('para_nat_type')->nullable();
        });

        Schema::table('requests', function (Blueprint $table) {
            $table->Integer('re_nat_type')->after("re_destination_type")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payment_rates', function (Blueprint $table) {
            $table->dropColumn('para_nat_type');
        });

       Schema::table('requests', function (Blueprint $table) {
            $table->dropColumn('re_nat_type');
        });
    }
}
