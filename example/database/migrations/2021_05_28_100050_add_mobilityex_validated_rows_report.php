<?php

use App\Models\Report;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMobilityexValidatedRowsReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $report = new Report();
        $report->rep_report = "mobilityex_validated_rows";
        $report->rep_name = "MobilityEx - Validated rows";
        $report->rep_description = "Report to see who validated which gathered row";
        $report->rep_heavy = 0;
        $report->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
