<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddThankYouUrlToMoverFormSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mover_form_settings', function (Blueprint $table) {
            $table->string('mofose_thank_you_url')->nullable()->after("mofose_email_to");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mover_form_settings', function (Blueprint $table) {
            $table->dropColumn('mofose_thank_you_url');
        });
    }
}
