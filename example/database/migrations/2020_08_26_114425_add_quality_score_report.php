<?php

use App\Models\Report;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddQualityScoreReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $report = new Report();
        $report->rep_report = "quality_score_calculations";
        $report->rep_name = "Quality score calculations";
        $report->rep_description = "Report for analysing the new way to match to users";
        $report->rep_heavy = 0;
        $report->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
