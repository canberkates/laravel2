<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSireloHyperlinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sirelo_hyperlinks', function (Blueprint $table) {
            $table->increments('sihy_id');
            $table->string('sihy_co_code', 2);
            $table->integer('sihy_me_id')->nullable();
            $table->text('sihy_link');
            $table->dateTime('sihy_timestamp');
            $table->foreign('sihy_co_code')->references('we_sirelo_co_code')->on('websites');
            $table->foreign('sihy_me_id')->references('me_id')->on('memberships');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sirelo_hyperlinks');
    }
}
