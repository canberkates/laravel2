<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPrefferedContactMethodToMoverRequests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mover_requests', function (Blueprint $table) {
            $table->Integer('more_contact_method')->after('more_email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mover_requests', function (Blueprint $table) {
            $table->dropColumn('more_contact_method');
        });
    }
}
