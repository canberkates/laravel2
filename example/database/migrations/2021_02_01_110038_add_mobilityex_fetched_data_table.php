<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMobilityexFetchedDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mobilityex_fetched_data', function (Blueprint $table) {
            $table->increments('mofeda_id');
            $table->integer('mofeda_type');
            $table->integer('mofeda_mobilityex_id')->nullable();
            $table->integer('mofeda_cu_id')->nullable();
            $table->timestamp("mofeda_timestamp");
            $table->string("mofeda_company_name_legal", 255)->nullable();
            $table->string("mofeda_company_name_business", 255)->nullable();
            $table->string("mofeda_website", 255)->nullable();
            $table->json("mofeda_address")->nullable();
            $table->string("mofeda_street", 255)->nullable();
            $table->string("mofeda_zipcode", 255)->nullable();
            $table->string("mofeda_city", 255)->nullable();
            $table->string("mofeda_telephone", 50)->nullable();
            $table->string("mofeda_established", 50)->nullable();
            //$table->string("mofeda_logo_filename")->nullable();
            $table->integer("mofeda_moving_type")->default(0);
            $table->integer("mofeda_relocation_type")->default(0);
            $table->json("mofeda_member_associations")->nullable();
            $table->json("mofeda_contact_persons")->nullable();
            $table->json("mofeda_quality")->nullable();
            //$table->json("mofeda_compliance")->nullable();
            $table->json("mofeda_moving_capabilities")->nullable();
            //$table->json("mofeda_relocation_capabilities")->nullable();
            //$table->json("mofeda_language_compabilities")->nullable();
            //$table->json("mofeda_files")->nullable();
            //$table->text("mofeda_description")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mobilityex_fetched_data');

    }
}
