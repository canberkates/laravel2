<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddClIncreaseColumnsToCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->decimal('cu_credit_limit_original', 10, 2)->nullable()->after("cu_credit_limit");
            $table->date('cu_credit_limit_increase_timestamp')->nullable()->after("cu_credit_limit_original");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->dropColumn('cu_credit_limit_original');
            $table->dropColumn('cu_credit_limit_increase_timestamp');
        });
    }
}
