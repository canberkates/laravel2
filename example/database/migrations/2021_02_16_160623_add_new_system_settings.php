<?php

use App\Functions\System;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewSystemSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $excluded_from_each_other = [
            "European Moving" => [17337, 17504, 18213, 17609],
            "German IDs" => [19688, 4345],
            "ISS Relocation" => [18814, 18816, 18815, 18813, 18349, 22061],
            "GB Liners" => [4893, 17513, 4894],
            "South African Mover IDs" => [622, 22289],
            "Italian IDs" => [2815, 13129]
        ];

        $ss = new \App\Models\SystemSetting();

        $ss->syse_setting = 'match_exclusions';
        $ss->syse_group = 'Settings';
        $ss->syse_name = 'Match Exclusions';
        $ss->syse_type = 'text';
        $ss->syse_value = System::serialize($excluded_from_each_other);

        $ss->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \App\Models\SystemSetting::where("syse_setting", "match_exclusions")->delete();
    }
}
