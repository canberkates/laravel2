<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRequestFormSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mover_form_settings', function (Blueprint $table) {
            $table->increments('mofose_id');
            $table->Integer('mofose_cu_id');
            $table->Integer('mofose_afpafo_id');
            $table->string('mofose_theme_color', 255)->default("#7fb866");
            $table->Integer('mofose_from_use_google_api')->default(0);
            $table->Integer('mofose_from_ask_residence')->default(0);
            $table->Integer('mofose_from_ask_elevator')->default(0);
            $table->Integer('mofose_from_ask_parking_permit')->default(0);
            $table->Integer('mofose_from_ask_walking_distance')->default(0);
            $table->Integer('mofose_to_use_google_api')->default(0);
            $table->Integer('mofose_to_ask_residence')->default(0);
            $table->Integer('mofose_to_ask_elevator')->default(0);
            $table->Integer('mofose_to_ask_parking_permit')->default(0);
            $table->Integer('mofose_to_ask_walking_distance')->default(0);
            $table->Integer('mofose_international_form')->default(1);
            $table->Integer('mofose_show_vc')->default(1);
            $table->Integer('mofose_ask_flexible_moving_date')->default(0);
            $table->Integer('mofose_ask_storage')->default(0);
            $table->Integer('mofose_ask_handyman')->default(0);
            $table->Integer('mofose_ask_disposal_of_items')->default(0);
            $table->Integer('mofose_send_emails')->default(0);
            $table->string('mofose_header', 255)->nullable();
            $table->string('mofose_footer', 255)->nullable();
            $table->string('mofose_email_from', 255)->nullable();
            $table->string('mofose_email_to', 255)->nullable();
        });

        Schema::create('mover_form_setting_questions', function (Blueprint $table) {
            $table->Integer('mofosequ_mofose_id');
            $table->string('mofosequ_question_tag');
            $table->string('mofosequ_question_value');
            $table->string('mofosequ_question_enabled');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mover_form_settings');
        Schema::dropIfExists('mover_form_setting_questions');


    }
}
