<?php

use Illuminate\Database\Seeder;

class LanguageTableSeerder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('languages')->insert([
            'la_code' => "EN_AU",
            'la_language' => "English (AU)",
            'la_language_local' => "English",
            'la_iso' => "en",
            'la_locale' => "en-AU",
            'la_iframe_only' => 1,
        ]);

        DB::table('languages')->insert([
            'la_code' => "EN_NZ",
            'la_language' => "English (NZ)",
            'la_language_local' => "English",
            'la_iso' => "en",
            'la_locale' => "en-NZ",
            'la_iframe_only' => 1,
        ]);

        DB::table('languages')->insert([
            'la_code' => "EN_US",
            'la_language' => "English (US)",
            'la_language_local' => "English",
            'la_iso' => "en",
            'la_locale' => "en-US",
            'la_iframe_only' => 1,
        ]);

        DB::table('languages')->insert([
            'la_code' => "EN_CA",
            'la_language' => "English (CA)",
            'la_language_local' => "English",
            'la_iso' => "en",
            'la_locale' => "en-CA",
            'la_iframe_only' => 1,
        ]);

        DB::table('languages')->insert([
            'la_code' => "EN_UK",
            'la_language' => "English (UK)",
            'la_language_local' => "English",
            'la_iso' => "en",
            'la_locale' => "en-UK",
            'la_iframe_only' => 1,
        ]);

        DB::table('languages')->insert([
            'la_code' => "EN_IE",
            'la_language' => "English (IE)",
            'la_language_local' => "English",
            'la_iso' => "en",
            'la_locale' => "en-IE",
            'la_iframe_only' => 1,
        ]);

        DB::table('languages')->insert([
            'la_code' => "EN_ZA",
            'la_language' => "English (ZA)",
            'la_language_local' => "English",
            'la_iso' => "en",
            'la_locale' => "en-ZA",
            'la_iframe_only' => 1,
        ]);

        DB::table('languages')->insert([
            'la_code' => "EN_SG",
            'la_language' => "English (SG)",
            'la_language_local' => "English",
            'la_iso' => "en",
            'la_locale' => "en-SG",
            'la_iframe_only' => 1,
        ]);

        DB::table('languages')->insert([
            'la_code' => "EN_AE",
            'la_language' => "English (AE)",
            'la_language_local' => "English",
            'la_iso' => "en",
            'la_locale' => "en-AE",
            'la_iframe_only' => 1,
        ]);

        DB::table('languages')->insert([
            'la_code' => "EN_HK",
            'la_language' => "English (HK)",
            'la_language_local' => "English",
            'la_iso' => "en",
            'la_locale' => "en-HK",
            'la_iframe_only' => 1,
        ]);

        DB::table('languages')->insert([
            'la_code' => "FR_CA",
            'la_language' => "French (CA)",
            'la_language_local' => "French",
            'la_iso' => "fr",
            'la_locale' => "fr-CA",
            'la_iframe_only' => 1,
        ]);
    }
}
