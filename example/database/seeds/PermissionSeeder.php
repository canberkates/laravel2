<?php


use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionSeeder extends Seeder
{
	public function run()
	{
		//Add the permissions
		$permissions_list = [
			"claims",
			"claims - check",
			"customers",
			"customers - bulk edit",
			"customers - documents",
			"customers - finance",
			"customers - invoices",
			"customers - settings",
			"customers - sirelo synchronise",
			"finance",
			"partnerdesk",
			"reports",
			"requests",
			"requests - match check",
			"requests anonymize",
			"surveys",
			"admin"
		];
		
		foreach ($permissions_list as $item)
		{
			$p = Permission::where("name", $item)->first();
			
			if (!$p)
			{
				$permission = new Permission();
				$permission->name = $item;
				$permission->save();
			}
		}
		
		//Add an admin role
		$r = Role::where("name", "admin")->first();
		
		if(!$r){
			$role = new Role();
			$role->name = "admin";
			$role->save();
		}
		
		//Add role to admin models
		$admins = [4559, 4186, 3653];
		foreach($admins as $admin){
			$user = User::find($admin);
			$user->assignRole('admin');
		}
	}
}