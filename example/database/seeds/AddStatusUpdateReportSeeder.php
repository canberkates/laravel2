<?php

use Illuminate\Database\Seeder;

class AddStatusUpdateReportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('reports')->insert([
            'rep_report' => "status_update",
            'rep_name' => "Status update",
            'rep_description' => "In this report you can check all status updates of every customer",
            'rep_heavy' => 0
        ]);
    }
}
