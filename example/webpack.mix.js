const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
	.js('resources/js/app.js', 'public/js')
	.sass('resources/sass/app.scss', 'public/css')
    
	/* CSS */
    .sass('resources/assets/sass/main.scss', 'public/css/dashmix.css')
    .sass('resources/assets/sass/dashmix/themes/xeco.scss', 'public/css/themes/')
    .sass('resources/assets/sass/dashmix/themes/xinspire.scss', 'public/css/themes/')
    .sass('resources/assets/sass/dashmix/themes/xmodern.scss', 'public/css/themes/')
    .sass('resources/assets/sass/dashmix/themes/xsmooth.scss', 'public/css/themes/')
    .sass('resources/assets/sass/dashmix/themes/xwork.scss', 'public/css/themes/')
    .sass('resources/assets/sass/dashmix/themes/xdream.scss', 'public/css/themes/')
    .sass('resources/assets/sass/dashmix/themes/xpro.scss', 'public/css/themes/')
    .sass('resources/assets/sass/dashmix/themes/xplay.scss', 'public/css/themes/')

    /* JS */
    .js('resources/assets/js/laravel/app.js', 'public/js/laravel.app.js')
    .js('resources/assets/js/dashmix/app.js', 'public/js/dashmix.app.js')

    mix.scripts([
        'public/js/custom/complete/general.js',
        'public/js/custom/complete/sidebar.js',
        'public/js/custom/complete/tabs.js',
        'public/js/custom/complete/profile-edit.js',
        'public/js/custom/complete/reset.js',
        'public/js/custom/complete/search.js',
        'public/js/custom/complete/translate.js',
        'public/js/custom/complete/moving-sizes.js',
        'public/js/custom/complete/volumes.js',
        'public/js/custom/complete/regions.js',
        'public/js/custom/complete/maps.js',
    ], 'public/js/custom/erp-complete.js')

    mix.browserSync(process.env.MIX_SENTRY_DSN_PUBLIC)

    /* Options */
    .options({
        processCssUrls: false
    });
;
