<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
	public function index()
	{
		$permissions = Permission::all();
		
		return view('permissions.index',
			[
				'permissions' => $permissions
			]);
	}
	
	public function create()
	{
		return view('permissions.create');
	}
	
	public function store(Request $request)
	{
		$permission = new Permission();
		
		$permission->name = $request->name;
		
		$permission->save();
		
		return redirect('admin/permissions/');
		
	}
}
