<?php

namespace App\Http\Controllers;

use App\Data\CustomerPairStatus;
use App\Data\CustomerPortalForms;
use App\Data\CustomerRequestDeliveryType;
use App\Data\DestinationType;
use App\Data\FreeTrialStopType;
use App\Data\MovingSize;
use App\Data\PortalType;
use App\Data\RequestType;
use App\Data\RequestTypeToMovingSizes;
use App\Data\YesNo;
use App\Functions\System;
use App\Models\Country;
use App\Models\Customer;
use App\Models\CustomerOffice;
use App\Models\KTCustomerPortal;
use App\Models\KTCustomerPortalCountry;
use App\Models\KTCustomerPortalRegion;
use App\Models\KTCustomerPortalRegionDestination;
use App\Models\KTCustomerSubscription;
use App\Models\MacroRegion;
use App\Models\MoverData;
use App\Models\PaymentCurrency;
use App\Models\Portal;
use App\Models\Region;
use App\Models\Subscription;
use Carbon\Carbon;
use DateInterval;
use DateTime;
use DB;
use Illuminate\Http\Request;
use Venturecraft\Revisionable\Revision;

class CustomerSubscriptionController extends Controller
{
    public function create($customer_id)
    {
        $customer = Customer::findOrFail($customer_id);

        return view('customers.subscriptions.create', [
            'customer' => $customer,
            'currency' => PaymentCurrency::where("pacu_code", $customer->cu_pacu_code)->first()->pacu_token,
            'subscriptions' => Subscription::all(),
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'subscription' => 'required',
            'start_date' => 'required',
            'price' => 'required'
        ]);


        // Validate the request...
        $customer = Customer::findOrFail($request->customer_id);

        $customer_subscription = new KTCustomerSubscription();
        $customer_subscription->ktcusu_cu_id = $customer->cu_id;
        $customer_subscription->ktcusu_sub_id = $request->subscription;
        $customer_subscription->ktcusu_start_date = $request->start_date;

        $end_date_yearly = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( $request->start_date ) ) . "+1 year" ) );

        $end_date_monthly = date("Y-m-t", strtotime($request->start_date));

        $your_date = strtotime($end_date_monthly);
        $datediff = $your_date - strtotime($request->start_date);

        $days_till_end_month = round($datediff / (60 * 60 * 24)) + 1;

        $days_in_month = date("t", strtotime($request->start_date));

        $customer_subscription->ktcusu_end_date = (($request->payment_interval == 1) ? $end_date_monthly : $end_date_yearly);

        //$customer_subscription->ktcusu_cancellation_date = $request->cancellation_date ?? null;
        $customer_subscription->ktcusu_price = $request->price;
        $customer_subscription->ktcusu_price_this_month = ($request->price/$days_in_month) * $days_till_end_month;
        $customer_subscription->ktcusu_pacu_code = $customer->cu_pacu_code;
        $customer_subscription->ktcusu_payment_interval = $request->payment_interval;

        if($request->subscription == 2){
            $customer_subscription->ktcusu_invoiced = 1;
            $oldDate = $request->start_date;
            $newDate = new DateTime($oldDate);
            $newDate->sub(new DateInterval('P7D')); // P1D means a period of 1 day
            $request->start_date = $newDate->format('Y-m-d');
            $customer_subscription->ktcusu_start_date = $request->start_date;
        }



        if ($request->start_date <= date("Y-m-d"))
        {
            $customer_subscription->ktcusu_active = 1;
        }



        $customer_subscription->save();

        if ($customer->cu_type == 6)
        {
            return redirect('resellers/'.$request->customer_id.'/edit')->with('message', 'Customer Subscription is successfully added!');
        }
        else
        {
            return redirect('customers/'.$request->customer_id.'/edit')->with('message', 'Customer Subscription is successfully added!');
        }

    }

    public function cancel(Request $request)
    {
        DB::table('kt_customer_subscription')
            ->where('ktcusu_id', $request->id)
            ->update(
                [
                    'ktcusu_cancellation_date' => DB::raw("`ktcusu_end_date`")
                ]
            );
    }

    public function markAsInvoiced(Request $request)
    {
        DB::table('kt_customer_subscription')
            ->where('ktcusu_id', $request->id)
            ->update(
                [
                    'ktcusu_invoiced' => 1
                ]
            );
    }

    public function delete(Request $request)
    {
        DB::table('kt_customer_subscription')
            ->where('ktcusu_id', $request->id)
            ->delete();
    }
}
