<?php

namespace App\Http\Controllers;

use App\Data\CreditDebitType;
use App\Data\YesNo;
use App\Functions\Mail;
use App\Functions\RequestData;
use App\Functions\RequestDeliveries;
use App\Functions\System;
use App\Functions\Translation;
use App\Models\CreditDebitInvoiceLine;
use App\Models\KTRequestCustomerQuestion;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CustomerQuestionController extends Controller
{
	public function free($ktrecuqu_id)
	{
		$ktrecuqu = KTRequestCustomerQuestion::with(
			["request",'customer',"question"]
			)
			->where("ktrecuqu_sent", 1)
			->where("ktrecuqu_hide", 0)
			->whereHas('customer', function ($query) {
				$query->where('cu_deleted', 0);
			})
			->findOrFail($ktrecuqu_id);

		return view('customerquestion.free',
			[
				'yesno' => YesNo::all(),
				'ktrecuqu' => $ktrecuqu,
				'id' => $ktrecuqu_id
			]);
	}


	public function freeLead(Request $request, $ktrecuqu_id)
	{
		$system = new System();
		$translate = new Translation();

		$ktrecuqu = KTRequestCustomerQuestion::with(["request",'customer','question', 'customerquestion'])
			->where("ktrecuqu_sent", 1)
			->where("ktrecuqu_hide", 0)
			->whereHas('customer', function ($query) {
				$query->where('cu_deleted', 0);
			})
			->findOrFail($ktrecuqu_id);

		Log::debug($ktrecuqu);

		if ($request->free == 1 && !$ktrecuqu->ktrecuqu_free)
		{
			if (date("m") == date("m", strtotime($ktrecuqu->ktrecuqu_timestamp)))
			{
				DB::table('kt_customer_question')
					->where('ktcuqu_id', $ktrecuqu_id->customerquestion->ktcuqu_id)
					->update(
						[
							'ktcuqu_max_requests_month_left' => DB::raw("(`ktcuqu_max_requests_month_left` + 1)")
						]
					);
			}

			if (date("d") == date("d", strtotime($ktrecuqu->ktrecuqu_timestamp)))
			{
				DB::table('kt_customer_question')
					->where('ktcuqu_id', $ktrecuqu_id->customerquestion->ktcuqu_id)
					->update(
						[
							'ktcuqu_max_requests_day_left' => DB::raw("(`ktcuqu_max_requests_day_left` + 1)")
						]
					);
			}

		} elseif ($request->free == 0 && $ktrecuqu->ktrecuqu_free)
		{

			if (date("m") == date("m", strtotime($ktrecuqu->ktrecuqu_timestamp)))
			{
				DB::table('kt_customer_question')
					->where('ktcuqu_id', $ktrecuqu_id->customerquestion->ktcuqu_id)
					->update(
						[
							'ktcuqu_max_requests_month_left' => DB::raw("(`ktcuqu_max_requests_month_left` - 1)")
						]
					);
			}

			if (date("d") == date("d", strtotime($ktrecuqu->ktrecuqu_timestamp)))
			{
				DB::table('kt_customer_question')
					->where('ktcuqu_id', $ktrecuqu_id->customerquestion->ktcuqu_id)
					->update(
						[
							'ktcuqu_max_requests_day_left' => DB::raw("(`ktcuqu_max_requests_day_left` - 1)")
						]
					);
			}

		}

		if ($ktrecuqu->ktrecuqu_invoiced)
		{
			if ($request->free == 1 && !$ktrecuqu->ktrecuqu_free)
			{
				$cdil = new CreditDebitInvoiceLine();

				$cdil->crdeinli_timestamp = Carbon::now()->toDateTimeString();
				$cdil->crdeinli_cu_id = $ktrecuqu->customer->cu_id;
				$cdil->crdeinli_ktrecuqu_id = $ktrecuqu->ktrecuqu_id;
				$cdil->crdeinli_leac_number = $ktrecuqu->customer->cu_leac_number;
				$cdil->crdeinli_description = $translate->get("invoice_free_requests_previous_invoice", $ktrecuqu->customer->cu_la_code)." (".$ktrecuqu->question->qu_name.")";


				$cdil->crdeinli_type = CreditDebitType::CREDIT;
				$cdil->crdeinli_currency = $ktrecuqu->customer->cu_pacu_code;
				$cdil->crdeinli_amount = $system->currencyConvert(-$ktrecuqu->ktrecuqu_amount, $ktrecuqu->ktrecuqu_currency, $ktrecuqu->customer->cu_pacu_code);

				$cdil->save();
			}
			elseif ($request->free == 0 &&$ktrecuqu->ktrecuqu_free)
			{
				$cdil = new CreditDebitInvoiceLine();

				$cdil->crdeinli_timestamp = Carbon::now()->toDateTimeString();
				$cdil->crdeinli_cu_id = $ktrecuqu->customer->cu_id;
				$cdil->crdeinli_ktrecuqu_id = $ktrecuqu->ktrecuqu_id;
				$cdil->crdeinli_leac_number = $ktrecuqu->customer->cu_leac_number;
				$cdil->crdeinli_description = $translate->get("invoice_paid_requests_previous_invoice", $ktrecuqu->customer->cu_la_code)." (".$ktrecuqu->question->qu_name.")";

				$cdil->crdeinli_type = CreditDebitType::DEBIT;
				$cdil->crdeinli_currency = $ktrecuqu->customer->cu_pacu_code;
				$cdil->crdeinli_amount = $system->currencyConvert($ktrecuqu->ktrecuqu_amount, $ktrecuqu->ktrecuqu_currency, $ktrecuqu->customer->cu_pacu_code);

				$cdil->save();
			}
		}

		DB::table('kt_request_customer_question')
			->where('ktrecuqu_id', $ktrecuqu->ktrecuqu_id)
			->update(
				[
					'ktrecuqu_free' => $request->free
				]
			);


		return redirect('serviceproviders/' . $ktrecuqu->customer->cu_id . '/edit');
	}

	public function resend($ktrecuqu_id)
	{
		$ktrecuqu = KTRequestCustomerQuestion::with(["request",'customer','question'])
			->findOrFail($ktrecuqu_id);

		return view('customerquestion.resend',
			[
				'ktrecuqu' => $ktrecuqu
			]);
	}

	public function resendLead(Request $request, $ktrecuqu_id)
	{
		$request_data = new RequestData();
		$system = new System();
		$mail = new Mail();
		$request_delivery = new RequestDeliveries();

		$ktrecuqu = DB::table("kt_request_customer_question")
			->leftJoin("kt_customer_question", "ktrecuqu_ktcuqu_id", "ktcuqu_id")
			->leftJoin("requests", "ktrecuqu_re_id", "re_id")
            ->leftJoin("volume_calculator", "voca_id", "re_voca_id")
			->leftJoin("customers", "ktrecuqu_cu_id", "cu_id")
			->where("ktrecuqu_id", $ktrecuqu_id)
			->where("ktrecuqu_sent", 1)
			->where("ktrecuqu_hide", 0)
			->where("cu_deleted", 0)
			->first();

		foreach($request_delivery->receivers(2, $ktrecuqu->ktcuqu_id, $ktrecuqu->cu_email) as $receiver)
		{

			$fields = [
				"request_delivery_value" => $receiver['value'],
				"request_delivery_extra" => $receiver['extra'],
				"cu_re_id" => $ktrecuqu->ktrecuqu_cu_re_id,
				"origin" => $request_data->requestWebsiteField($ktrecuqu->re_id, "we_mail_from_name"),
				"request" => $system->databaseToArray($ktrecuqu)
			];

			if($receiver['type'] == 1)
			{
				$mail->send("service_provider_request", $ktrecuqu->cu_la_code, $fields);
			}
			else
			{
				$request_delivery->send(2, $receiver['type'], $ktrecuqu->cu_la_code, $fields);
			}
		}

		return redirect('serviceproviders/' . $ktrecuqu->cu_id . '/edit');
	}

}
