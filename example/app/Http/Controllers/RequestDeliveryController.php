<?php

namespace App\Http\Controllers;

use App\Data\CustomerRequestDeliveryType;
use App\Data\CustomerType;
use App\Models\Customer;
use App\Models\RequestDelivery;
use Illuminate\Http\Request;
use Log;

class RequestDeliveryController extends Controller
{
	public function edit($customer_id, $customerportal_id, $requestdelivery_id)
	{
		$requestdelivery = RequestDelivery::with("customerportal.customer")->findOrFail($requestdelivery_id);

		return view('requestdelivery.edit',
			[
				'requestdelivery' => $requestdelivery,
				"requestdeliverytypes" => CustomerRequestDeliveryType::all()
			]);

	}


	public function update(Request $request, $rede_id)
	{
		$requestdelivery = RequestDelivery::find($rede_id);

		$requestdelivery->rede_type = $request->type;
		$requestdelivery->rede_value = $request->value;
		$requestdelivery->rede_extra = $request->extra;

		$requestdelivery->save();

		$customer = Customer::where("cu_id", $request->cu_id)->first();

		if ($customer->cu_type == 6)
        {
            return redirect('resellers/' .$request->cu_id . '/customerportal/' . $request->ktcupo_id . '/edit');
        }
		else {
            return redirect('customers/' .$request->cu_id . '/customerportal/' . $request->ktcupo_id . '/edit');
        }
	}

	public function create(Request $request, $customer_id, $customerportal_id)
	{
		$customer = Customer::findOrFail($customer_id);

		return view('requestdelivery.create',
			[
				'customer' => $customer,
				'customerportal' => ($request->conversion_tool ? null : $customerportal_id),
                'conversion_tool' => $request->conversion_tool,
				"requestdeliverytypes" => CustomerRequestDeliveryType::all()
			]);
	}

	public function store(Request $request)
	{
		$request->validate([
			'type' => 'required',
			'value' => 'required'
		]);

		$customer = Customer::where("cu_id", $request->cu_id)->first();

		//Create new contact person
		$requestdelivery = new RequestDelivery();

		$requestdelivery->rede_customer_type = $customer->cu_type;
        if($request->mofose_id){
            $requestdelivery->rede_mofose_id = $request->mofose_id;
        }else{
            $requestdelivery->rede_ktcupo_id = $request->ktcupo_id;
        }
		$requestdelivery->rede_type = $request->type;
		$requestdelivery->rede_value = $request->value;
		$requestdelivery->rede_extra = $request->extra;

		//Set to correct customer and save
		$requestdelivery->save();

        $customer = Customer::where("cu_id", $request->cu_id)->first();

        if ($customer->cu_type == 6)
        {
            return redirect('resellers/' .$request->cu_id . '/customerportal/' . $request->ktcupo_id . '/edit');
        }
        else {
            if($request->mofose_id){
                return redirect('customers/' .$request->cu_id . '/conversion_tools/' . $request->mofose_id . '/edit');
            }else{
                return redirect('customers/' .$request->cu_id . '/customerportal/' . $request->ktcupo_id . '/edit');
            }
        }

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\MoverData $moverData
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($rede_id)
	{
		$requestdelivery = RequestDelivery::find($rede_id);
		Log::debug("Removing the following request delivery from portal: ".$requestdelivery->rede_ktcupo_id . " with type " . $requestdelivery->rede_type . " value " . $requestdelivery->rede_value . " extra " . $requestdelivery->rede_extra);
		$requestdelivery->delete();

		return redirect()->back();
	}
}
