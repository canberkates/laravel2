<?php

namespace App\Http\Controllers;

use App\Data\BothYesNo;
use App\Data\PaymentMethod;
use DB;
use Illuminate\Http\Request;
use Log;

class CreditCardCustomersController extends Controller
{
	public function index()
	{
		
		return view('finance.creditcardcustomers', [
			'selected_contract' => "",
			'selected_paymentmethod' => [],
            'contracts' => BothYesNo::all(),
            'paymentmethods' => PaymentMethod::all()
		]);
	}
	
	public function filteredIndex(Request $request)
	{
        $request->validate([
            'paymentmethod' => 'required'
        ]);
        
        Log::debug($request);
        
        $customers = DB::table("customers")
            ->select("cu_id","cu_company_name_business","cu_payment_method")
            ->selectRaw("adyen_card_details.*")
            ->leftJoin("adyen_card_details", "adcade_cu_id", "cu_id")
            ->where("cu_deleted", 0)
            ->whereRaw("((`adcade_enabled` = 1 AND `adcade_removed` = 0) OR `adcade_enabled` IS NULL)");
        
        if($request->contract != null)
        {
            $customers->whereRaw( "`adcade_cu_id` ".($request->contract == 0 ? "IS NULL" : "IS NOT NULL"));
        }
        
        $customers->whereIn("cu_payment_method", array_values($request->paymentmethod));
        
        $customers = $customers->groupBy("cu_id")
            ->get();
        
        
        return view('finance.creditcardcustomers', [
            'selected_contract' => ($request->contract ?? ""),
            'selected_paymentmethod' => $request->paymentmethod,
            'contracts' => BothYesNo::all(),
            'paymentmethods' => PaymentMethod::all(),
            'customers' => $customers
        ]);
		
	}
	
}
