<?php


namespace App\Http\Controllers;

use App\Data\DebtorStatus;
use App\Data\PaymentMethod;
use App\Data\PaymentReminder;
use App\Functions\Mail;
use App\Functions\System;
use App\Models\Country;
use App\Models\KTBankLineInvoiceCustomerLedgerAccount;
use App\Models\PaymentCurrency;
use App\Models\User;
use DB;
use Illuminate\Http\Request;
use Log;

class PaymentRemindersController extends Controller
{
	public function index()
	{
        $system = new System();

		$query = DB::table("invoices")
			->selectRaw("customers.*")
			->leftJoin("customers", "in_cu_id", "cu_id")
			->whereRaw("in_expiration_date <= NOW()")
			->where("cu_deleted", 0)
			->groupBy("cu_id")
			->get();

		$customer = [];
        $total_amount_left = 0;

		foreach($query as $row){

			$query_invoices = DB::table("invoices")
				->select("in_id", "in_number", "in_date", "in_expiration_date", "in_payment_reminder", "in_payment_reminder_date", "in_currency", "in_amount_netto", "in_amount_netto_eur")
				->whereRaw("in_expiration_date <= NOW()")
				->where("in_cu_id", $row->cu_id)
				->get();

			$invoices = [];

			$amount_left = 0;
			$amount_left_eur = 0;

			foreach($query_invoices as $row_invoices)
			{
				$query_bank_line_invoice_customer_ledger_account = KTBankLineInvoiceCustomerLedgerAccount::select("ktbaliinculeac_amount", "ktbaliinculeac_amount_FC")
					->where("ktbaliinculeac_in_id", $row_invoices->in_id)
					->get();

				$amount_paired = 0;
				$amount_paired_fc = 0;

				foreach($query_bank_line_invoice_customer_ledger_account as $row_bank_line_invoice_customer_ledger_account)
				{
					$amount_paired += $row_bank_line_invoice_customer_ledger_account->ktbaliinculeac_amount;
                    $amount_paired_fc += $row_bank_line_invoice_customer_ledger_account->ktbaliinculeac_amount_FC;
				}

				if($row_invoices->in_amount_netto_eur != round($amount_paired, 2))
				{
					$amount_netto = (($row->cu_pacu_code == "EUR") ? $row_invoices->in_amount_netto_eur : $row_invoices->in_amount_netto);

					$invoices[] = [
						"id" => $row_invoices->in_id,
						"number" => $row_invoices->in_number,
						"date" => $row_invoices->in_date,
						"expiration_date" => $row_invoices->in_expiration_date,
						"expiration_date_time" => strtotime($row_invoices->in_expiration_date),
						"days_overdue" => $system->dateDifference($row_invoices->in_expiration_date, date("Y-m-d")),
						"payment_reminder" => $row_invoices->in_payment_reminder,
						"payment_reminder_date" => $row_invoices->in_payment_reminder_date,
						"payment_reminder_date_time" => strtotime($row_invoices->in_payment_reminder_date),
						"amount_netto" => $amount_netto,
						"amount_paid" => $amount_paired_fc,
						"amount_left" => $amount_netto - $amount_paired_fc
					];

					$amount_left += $amount_netto - $amount_paired_fc;
					$amount_left_eur += $row_invoices->in_amount_netto_eur - $amount_paired;
				}
			}

			if(sizeof($invoices) > 0){
				usort($invoices, function($a, $b){
					if($a['payment_reminder'] == $b['payment_reminder'])
					{
						if($a['payment_reminder'] == 0)
						{
							return $a['expiration_date_time'] - $b['expiration_date_time'];
						}
						else
						{
							return $b['payment_reminder_date_time'] - $a['payment_reminder_date_time'];
						}
					}
					else
					{
						return $b['payment_reminder'] - $a['payment_reminder'];
					}
				});

				$resend_days = [
					0 => 0,
					1 => 7,
					2 => 7,
					3 => 7,
					4 => 5,
					5 => 7,
				];

				if($system->dateDifference((($invoices[0]['payment_reminder'] == 0) ? $invoices[0]['expiration_date'] : $invoices[0]['payment_reminder_date']), date("Y-m-d")) >= $resend_days[$invoices[0]['payment_reminder']])
				{
					if($row->cu_payment_reminder_status)
					{
						$payment_reminder = 1;
					}
					else
					{
						$payment_reminder = $invoices[0]['payment_reminder'] + (($invoices[0]['payment_reminder'] == 5) ? 0 : 1);
					}

					$customer[$row->cu_id]['invoices'] = $invoices;
					$customer[$row->cu_id]['customer_info'] = $system->databaseToArray($row);
					$customer[$row->cu_id]['customer_info']['payment_reminder'] = PaymentReminder::name($payment_reminder);
					$customer[$row->cu_id]['customer_info']['country'] = Country::where("co_code", $customer[$row->cu_id]['customer_info']['cu_co_code'])->first()->co_en;
					$customer[$row->cu_id]['customer_info']['status'] = $system->getCustomerStatus($row->cu_id);
					$customer[$row->cu_id]['customer_info']['debtor_status'] = DebtorStatus::name($customer[$row->cu_id]['customer_info']['cu_debtor_status']);
					$customer[$row->cu_id]['customer_info']['payment_method'] = PaymentMethod::name($customer[$row->cu_id]['customer_info']['cu_payment_method']);
					$customer[$row->cu_id]['customer_info']['amount_left_eur'] = $amount_left_eur;

                    $total_amount_left += $amount_left_eur;

                }

			}

		}

		return view('finance.paymentreminders', [
			'state_per' => "",
			'customers' => $customer,
            'total_amount_left' => $total_amount_left
		]);
	}

	public function sendPaymentReminders(Request $request)
	{
		$request->validate([
			'payment_reminder' => 'required'
		]);

		Log::debug($request);

		$system = new System();
		$mail = new Mail();

		$query = DB::table("invoices")
			->selectRaw("customers.*")
			->leftJoin("customers", "in_cu_id", "cu_id")
			->whereRaw("in_expiration_date < NOW()")
			->where("cu_deleted", 0)
			->whereIn("cu_id", array_keys($request->payment_reminder))
			->groupBy("cu_id")
			->get();

		$sent_to = [];

		foreach($query as $row){

			$query_invoices = DB::table("invoices")
				->select("in_id", "in_number", "in_date", "in_expiration_date", "in_payment_reminder", "in_payment_reminder_date", "in_currency", "in_amount_netto", "in_amount_netto_eur")
				->whereRaw("in_expiration_date < NOW()")
				->where("in_cu_id", $row->cu_id)
				->get();

			$invoices = [];

			$amount_left = 0;
			$amount_left_eur = 0;

			foreach($query_invoices as $row_invoices)
			{
				$query_bank_line_invoice_customer_ledger_account = KTBankLineInvoiceCustomerLedgerAccount::select("ktbaliinculeac_amount", "ktbaliinculeac_amount_FC")
					->where("ktbaliinculeac_in_id", $row_invoices->in_id)
					->get();

				$amount_paired = 0;
				$amount_paired_fc = 0;

				foreach($query_bank_line_invoice_customer_ledger_account as $row_bank_line_invoice_customer_ledger_account)
				{
					$amount_paired += $row_bank_line_invoice_customer_ledger_account->ktbaliinculeac_amount;
                    $amount_paired_fc += $row_bank_line_invoice_customer_ledger_account->ktbaliinculeac_amount_FC;
				}

				if($row_invoices->in_amount_netto_eur != round($amount_paired, 2))
				{
                    $amount_netto = (($row->cu_pacu_code == "EUR") ? $row_invoices->in_amount_netto_eur : $row_invoices->in_amount_netto);

					$invoices[] = [
						"id" => $row_invoices->in_id,
						"number" => $row_invoices->in_number,
						"date" => $row_invoices->in_date,
						"expiration_date" => $row_invoices->in_expiration_date,
						"expiration_date_time" => strtotime($row_invoices->in_expiration_date),
						"days_overdue" => $system->dateDifference($row_invoices->in_expiration_date, date("Y-m-d")),
						"payment_reminder" => $row_invoices->in_payment_reminder,
						"payment_reminder_date" => $row_invoices->in_payment_reminder_date,
						"payment_reminder_date_time" => strtotime($row_invoices->in_payment_reminder_date),
						"amount_netto" => $amount_netto,
						"amount_paid" => $amount_paired_fc,
						"amount_left" => $amount_netto - $amount_paired_fc
					];

					$amount_left += $amount_netto - $amount_paired_fc;
					$amount_left_eur += $row_invoices->in_amount_netto_eur - $amount_paired;
				}
			}

			if(sizeof($invoices) > 0){
				usort($invoices, function($a, $b){
					if($a['payment_reminder'] == $b['payment_reminder'])
					{
						if($a['payment_reminder'] == 0)
						{
							return $a['expiration_date_time'] - $b['expiration_date_time'];
						}
						else
						{
							return $b['payment_reminder_date_time'] - $a['payment_reminder_date_time'];
						}
					}
					else
					{
						return $b['payment_reminder'] - $a['payment_reminder'];
					}
				});

				$resend_days = [
					0 => 0,
					1 => 7,
					2 => 7,
					3 => 7,
					4 => 5,
					5 => 7,
				];

				if($system->dateDifference((($invoices[0]['payment_reminder'] == 0) ? $invoices[0]['expiration_date'] : $invoices[0]['payment_reminder_date']), date("Y-m-d")) >= $resend_days[$invoices[0]['payment_reminder']])
				{
					if($row->cu_payment_reminder_status)
					{
						$payment_reminder = 1;
					}
					else
					{
						$payment_reminder = $invoices[0]['payment_reminder'] + (($invoices[0]['payment_reminder'] == 5) ? 0 : 1);
					}

					$invoice_names = [];

					foreach($invoices as $invoice)
					{
						if($invoice['payment_reminder'] < 5)
						{

							DB::table('invoices')
								->where('in_id', $invoice['id'])
								->update(
									[
										'in_payment_reminder' => DB::raw("(`in_payment_reminder` + 1)"),
										'in_payment_reminder_date' => DB::raw("NOW()")
									]
								);

						}

						$invoice_names[] = "#".$invoice['number'];
					}

					$company_data = [
						"company_name" => System::getSetting("company_name"),
						"street" => System::getSetting("company_street"),
						"zipcode" => System::getSetting("company_zipcode"),
						"city" => System::getSetting("company_city"),
						"country" => System::getSetting("company_co_code"),
						"vat" => System::getSetting("company_vat"),
						"coc" => System::getSetting("company_coc"),
						"bank" => System::getSetting("company_bank"),
						"iban" => System::getSetting("company_iban"),
						"bic" => System::getSetting("company_bic"),
						"telephone" => System::getSetting("company_telephone"),
						"fax" => System::getSetting("company_fax"),
						"email" => System::getSetting("company_email"),
						"website" => System::getSetting("company_website")
					];

					if($row->cu_use_billing_address == 1)
					{
						$debtor_data = [
							"id" => $row->cu_id,
							"debtor_number" => $row->cu_debtor_number,
							"company_name" => $row->cu_company_name_legal,
							"attn" => $row->cu_attn,
							"email" => $row->cu_email_bi,
							"street_1" => $row->cu_street_1_bi,
							"street_2" => $row->cu_street_2_bi,
							"zipcode" => $row->cu_zipcode_bi,
							"city" => $row->cu_city_bi,
							"country" => $row->cu_co_code_bi,
							"payment_term" => $row->cu_payment_term,
							"vat_number" => $row->cu_vat_number
						];
					}
					else
					{
						$debtor_data = [
							"id" => $row->cu_id,
							"debtor_number" => $row->cu_debtor_number,
							"company_name" => $row->cu_company_name_legal,
							"attn" => $row->cu_attn,
							"email" => ((!empty($row->cu_email_bi)) ? $row->cu_email_bi : $row->cu_email),
							"street_1" => $row->cu_street_1,
							"street_2" => $row->cu_street_2,
							"zipcode" => $row->cu_zipcode,
							"city" => $row->cu_city,
							"country" => $row->cu_co_code,
							"payment_term" => $row->cu_payment_term,
							"vat_number" => $row->cu_vat_number
						];
					}

					$fields = [
						"company_data" => $company_data,
						"debtor_data" => $debtor_data,
						"payment_reminder" => $payment_reminder,
						"payment_reminder_mail" => (($payment_reminder == 3) ? 2 : $payment_reminder),
						"currency" => $row->cu_pacu_code,
						"invoices" => $invoices,
						"amount_left" => $amount_left,
						"invoice_names" => implode(", ", $invoice_names)
					];


					if($payment_reminder > 3)
					{

						$subject = "Customer has received 4 or more payment reminders";
						$message = "Customer <strong>".$row->cu_company_name_legal."</strong> has received four or more payment reminders: <strong>(".PaymentReminder::name($payment_reminder).")</strong>.";

						foreach(User::where("us_finance", 1)->get() as $user)
						{
							$system->sendMessage($user->id, $subject, $message);
						}
					}

					if ($row->cu_la_code == "DK") {
                        $row->cu_la_code = "EN";
                    }

					$mail->send("payment_reminder", $row->cu_la_code, $fields);

					$sent_to[] = "Payment reminder <strong>".PaymentReminder::name($payment_reminder)."</strong> successfully created and sent to <strong>".$row->cu_company_name_legal."</strong>.";

					sleep(1);

				}

			}

		}

		return view('finance.paymentreminderssuccess', [
			'sent_to' => $sent_to
		]);

	}

	public function getInvoices(Request $request)
	{
		$system = new System();


		$query = DB::table('invoices')
			->where('in_cu_id',$request->id)
			->whereRaw('in_date <= NOW()')
			->get();

		$html = '';
		$html .= "<table class=\"table display\">";
		$html .= "<thead>";
		$html .= "<tr>";
		$html .= "<th>ID</th>";
		$html .= "<th>Timestamp</th>";
		$html .= "<th>Invoice number</th>";
		$html .= "<th>Invoice date</th>";
		$html .= "<th>Expiration date</th>";
		$html .= "<th>Days overdue</th>";
		$html .= "<th>Reminder</th>";
		$html .= "<th>Payment method</th>";
		$html .= "<th>Amount excl. VAT €</th>";
		$html .= "<th>Amount incl. VAT €</th>";
		$html .= "<th>Amount excl. VAT FC</th>";
		$html .= "<th>Amount incl. VAT FC</th>";
		$html .= "<th>Amount paid</th>";
		$html .= "<th>Amount left</th>";
		$html .= "<th>Payment date(s)</th>";
		$html .= "</tr>";
		$html .= "</thead>";
		$html .= "<tbody>";

		foreach($query as $row)
		{
			$query_bali = DB::table('kt_bank_line_invoice_customer_ledger_account')
				->select("ktbaliinculeac_bali_id","ktbaliinculeac_date","ktbaliinculeac_amount", "bali_date")
				->leftJoin('bank_lines', 'ktbaliinculeac_bali_id', 'bali_id')
				->where('ktbaliinculeac_in_id', $row->in_id)
				->get();

			$amount_paired = 0;
			$payment_dates = [];

			foreach($query_bali as $row_bali)
			{
				$amount_paired += $row_bali->ktbaliinculeac_amount;

				$payment_dates[] = (($row_bali->bali_date == 0) ? $row_bali->ktbaliinculeac_date : $row_bali->bali_date);
			}

			$paymentcurrencies = PaymentCurrency::all();

			if($row->in_amount_netto_eur != round($amount_paired, 2))
			{
				$html .=  '<tr>';
				$html .=  '<td>'.$row->in_number.'</td>';
				$html .=  '<td>'.$row->in_timestamp.'</td>';
				$html .=  '<td>'.$row->in_number.'</td>';
				$html .=  '<td>'.$row->in_date.'</td>';
				$html .=  '<td>'.$row->in_expiration_date.'</td>';
				$html .=  '<td>'.(($system->dateDifference($row->in_expiration_date, date("Y-m-d")) > 0) ? $system->dateDifference($row->in_expiration_date, date("Y-m-d")) : 0).'</td>';
				$html .=  '<td>'.PaymentReminder::name($row->in_payment_reminder).(($row->in_payment_reminder > 0) ? " (".$row->in_payment_reminder_date.")" : "").'</td>';
				$html .=  '<td>'.PaymentMethod::name($row->in_payment_method).'</td>';
				$html .=  '<td>€ '.System::numberFormat($row->in_amount_gross_eur, 2).'</td>';
				$html .=  '<td>€ '.System::numberFormat($row->in_amount_netto_eur, 2).'</td>';
				$html .=  '<td>'.$paymentcurrencies->where('pacu_code', $row->in_currency)->first()->pacu_token.' '.System::numberFormat($row->in_amount_gross, 2).'</td>';
				$html .=  '<td>'.$paymentcurrencies->where('pacu_code', $row->in_currency)->first()->pacu_token.' '.System::numberFormat($row->in_amount_netto, 2).'</td>';
				$html .=  '<td>€ '.System::numberFormat($amount_paired, 2).'</td>';
				$html .=  '<td>€ '.System::numberFormat($row->in_amount_netto_eur - $amount_paired, 2).'</td>';
				$html .=  '<td>'.((!empty($payment_dates)) ? implode(', ', $payment_dates) : '-').'</td>';
				$html .=  '</tr>';
			}
		}
		$html .= '</tbody>';
		$html .= '</table>';

		return json_encode($html);
	}


}

