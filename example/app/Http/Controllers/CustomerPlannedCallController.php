<?php

namespace App\Http\Controllers;

use App\Data\CustomerStatusReason;
use App\Data\CustomerStatusStatus;
use App\Data\PlannedCallReason;
use App\Data\PlannedCallType;
use App\Functions\System;
use App\Models\Customer;
use App\Models\CustomerRemark;
use App\Models\CustomerStatus;
use App\Models\PauseHistory;
use App\Models\PlannedCall;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class CustomerPlannedCallController extends Controller
{

	public function create($customer_id)
	{
		$customer = Customer::findOrFail($customer_id);

        $users = [];
        $users_query = User::where("id", "!=", 0)->where("us_is_deleted", 0)->get();
        foreach($users_query as $us)
        {
            $users[$us->id] = $us->us_name;
        }

		return view('customers.contact_log.create', [
			'customer' => $customer,
            'users' => $users,
            'planned_call_reasons' => PlannedCallReason::all(),
            'call_types' => PlannedCallType::all()
		]);
	}

	public function store(Request $request)
	{
		$request->validate([
			'call_datetime' => 'required',
			'call_reason' => 'required',
			'user' => 'required'
		]);


        $planned_call = new PlannedCall();
        $planned_call->plca_cu_id = $request->customer_id;
        $planned_call->plca_us_id = $request->user;
        $planned_call->plca_type = $request->type;
        $planned_call->plca_status = 0;
        $planned_call->plca_reason = $request->call_reason;
        $planned_call->plca_reason_extra = $request->explain_reason;
        $planned_call->plca_criteria_met = 0;
        $planned_call->plca_planned_date = $request->call_datetime;

        $planned_call->save();

        if(Arr::exists($request, 'finish_call')){
            return redirect('calendar/'.$planned_call->plca_id.'/edit');
        }
        else{
            return redirect('customers/'.$request->customer_id.'/edit')->with('message', 'Call has been successfully added!');
        }

	}

}
