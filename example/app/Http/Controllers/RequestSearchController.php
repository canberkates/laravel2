<?php

namespace App\Http\Controllers;

use App\Data\ClaimReason;
use App\Data\DestinationType;
use App\Data\Device;
use App\Data\MovingSize;
use App\Data\PlatformSource;
use App\Data\RejectionReason;
use App\Data\RequestCustomerPortalType;
use App\Data\RequestStatus;
use App\Data\RequestType;
use App\Data\YesNo;
use App\Functions\RequestData;
use App\Models\AffiliatePartnerForm;
use App\Models\Country;
use App\Models\User;
use App\Models\WebsiteForm;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Functions\System;
use Log;


class RequestSearchController extends Controller
{
	public function search()
	{
	    $websiteforms = WebsiteForm::with("website")->get()->sortBy("we_website");

	    $websites = [];
	    foreach($websiteforms as $website_form){
	       $websites[$website_form->wefo_id] = $website_form->website->we_website." (".$website_form->wefo_name.")";
        }

		$rd = new RequestData();
		return view('requests.search',
			[
				'movingsizes' => MovingSize::all(),
				'rejectionreasons' => RejectionReason::all(),
				'countries' => Country::all(),
				'users' => User::all(),
				'websites' => $websites,
				'affiliates' => AffiliatePartnerForm::join("customers", "afpafo_cu_id", "cu_id")->where("afpafo_deleted", 0)->get()->sortBy("afpafo_name"),
				'platforms' => PlatformSource::all(),
				'categories' => $rd->requestCategories(),
                'devices' => Device::all(),

				//Give back selected values
				'selected_name' => null,
				'selected_email' => null,
				'selected_ip_address' => null,
				'selected_zipcode_from' => null,
				'selected_city_from' => null,
				'selected_zipcode_to' => null,
				'selected_city_to' => null,
				'selected_date' => null,
				'selected_date_to' => Carbon::now()->format('Y-m-d'),
				'selected_date_from' => Carbon::now()->format('Y-m-d'),
				'selected_request_id' => null,
                'selected_destination_id' => null,

				//Give back multiple selects
				'selected_country_from' => [],
				'selected_country_to' => [],
				'selected_size' => [],
				'selected_rejection_reason' => [],
				'selected_rejected' => [],
				'selected_category' => [],
				'selected_website' => [],
				'selected_devices' => [],
				'selected_source' => [],
				'selected_affiliate' => [],
				'selected_platform'=> [],
			]);
	}

	public function searchResult(Request $request)
	{


		$rd = new RequestData();

		$request_query = \App\Models\Request::with(["websiteform.website", "rejectedby", "affiliateform", "countryfrom", "countryto"]);

		if( ! empty( $request->date ) ) {

		    $request_query->whereBetween( 're_timestamp', System::betweenDates( $request->date ) );
        }

		if(!empty($request->request_id))
		{
			$request_query->where('re_id', $request->request_id);
		}

		if(!empty($request->name))
		{
			$request_query->where('re_full_name', 'LIKE', '%'.$request->name.'%');
		}

		if(!empty($request->email))
		{
			$request_query->where('re_email', $request->email);
		}

		if(!empty($request->ip_address))
		{
			$request_query->where('re_ip_address', $request->ip_address);
		}

		if(!empty($request->zipcode_from))
		{
			$request_query->where('re_zipcode_from', $request->zipcode_from);
		}

		if(!empty($request->city_from))
		{
			$request_query->where('re_city_from', $request->city_from);
		}

		if(!empty($request->country_from))
		{
			$request_query->whereIn('re_co_code_from', $request->country_from);
		}

		if(!empty($request->zipcode_to))
		{
			$request_query->where('re_zipcode_to', $request->zipcode_to);
		}

		if(!empty($request->city_to))
		{
			$request_query->where('re_city_to', $request->city_to);
		}

		if(!empty($request->country_to))
		{
			$request_query->whereIn('re_co_code_to', $request->country_to);
		}

		if(!empty($request->request_type))
		{
		    if ($request->request_type == 1)
            {
                $request_query->whereIn('re_request_type', [1,2,3,4]);
            }
		    elseif ($request->request_type == 2)
            {
                $request_query->whereIn('re_request_type', [5]);
            }

		}
        //check for destination type filter
        if ($request->destination_type)
        {
            $request_query->where('re_destination_type', $request->destination_type);
        }


		if(!empty($request->size))
		{
			$request_query->whereIn('re_moving_size', $request->size);
		}

		if(!empty($request->rejection_reason))
		{
			$request_query->whereIn('re_rejection_reason', $request->rejection_reason);
		}

		if(!empty($request->rejected))
		{
			$request_query->whereIn('re_rejection_us_id', $request->rejected);
		}

		if(!empty($request->website))
		{
			$request_query->whereIn('re_wefo_id', array_values($request->website));
		}

		if(!empty($request->device))
		{
			$request_query->whereIn('re_device', array_values($request->device));
		}

		if(!empty($request->affiliate))
		{
			$request_query->whereIn('re_afpafo_id', $request->affiliate);
		}

		if(!empty($request->source))
		{
			$request_query->whereIn('re_platform_source', $request->source);
		}

		if(!empty($request->category))
		{
			$request_query->whereIn('re_category', $request->category);
		}

		if(!empty($request->pd_resubmit))
		{
		    if ($request->pd_resubmit == 1) {
                $request_query->whereIn('re_pd_validate', [1,2]);
            }
            elseif ($request->pd_resubmit == 2) {
                $request_query->where('re_pd_validate', 0);
            }
		}

		$request_query = $request_query->get();


        foreach ($request_query->where("re_status", RequestStatus::MATCHED) as $matched_info){

			$extra_information_table = '
				<table class="table">
				<thead>
					<th width="5%"><b>ID</b></th>
					<th width="20%"><b>Customer</b></th>
					<th width="10%"><b>Portal</b></th>
					<th width="10%"><b>Matched</b></th>
					<th width="20%"><b>Matched by</b></th>
					<th width="10%"><b>Type</b></th>
					<th width="5%"><b>Rematch</b></th>
					<th width="15%"><b>Free</b></th>
					<th width="5%"><b>Invoiced</b></th>
				</thead>
				<tbody>';

			$query = DB::table("kt_request_customer_portal")
				->select(	"ktrecupo_id", "ktrecupo_cu_re_id", "ktrecupo_timestamp", "ktrecupo_us_id", "ktrecupo_type", "ktrecupo_rematch",
				"ktrecupo_free", "ktrecupo_free_reason", "ktrecupo_invoiced", "cu_company_name_business", "po_portal")
				->leftJoin("customers", "ktrecupo_cu_id", "cu_id")
				->leftJoin("portals", "ktrecupo_po_id", "po_id")
				->where("ktrecupo_re_id", $matched_info->re_id)
				->where("cu_deleted", 0)
				->get();

			foreach($query as $row)
			{
				$user = User::find($row->ktrecupo_us_id);

				if($user){
					$name = $user->us_name;
				}else{
					$name = "Unknown";
				}

				$extra_information_table .= "
				<tr>
					<td>{$row->ktrecupo_cu_re_id}</td>
					<td>{$row->cu_company_name_business}</td>
					<td>{$row->po_portal}</td>
					<td>{$row->ktrecupo_timestamp}</td>
					<td>".$name."</td>
					<td>".RequestCustomerPortalType::name($row->ktrecupo_type)."</td>
					<td>".YesNo::name($row->ktrecupo_rematch)."</td>
					<td>".($row->ktrecupo_free ? ClaimReason::name($row->ktrecupo_free_reason) : "No")."</td>
					<td>".YesNo::name($row->ktrecupo_invoiced)."</td>
				</tr>
			";
			}

			$extra_information_table .= '</tbody></table>';

			$details = str_replace("\"", "'", $extra_information_table);

			$matched_info->details = $details;
			$matched_info->matches_left = $rd->requestMatchesLeft($matched_info->re_id);
		}

		//Seperate portals by status
		$requests['open'] = $request_query->where("re_status", RequestStatus::OPEN);
		$requests['matched'] = $request_query->where("re_status", RequestStatus::MATCHED);
		$requests['rejected'] = $request_query->where("re_status", RequestStatus::REJECTED);

        $websiteforms = WebsiteForm::with("website")->get()->sortBy("we_website");

        $websites = [];
        foreach($websiteforms as $website_form){
            $websites[$website_form->wefo_id] = $website_form->website->we_website." (".$website_form->wefo_name.")";
        }
		return view('requests.search',
			[
				'movingsizes' => MovingSize::all(),
				'requesttypes' => RequestType::all(),
				'rejectionreasons' => RejectionReason::all(),
				'countries' => Country::all(),
				'users' => User::all(),
				'websites' => $websites,
                'affiliates' => AffiliatePartnerForm::join("customers", "afpafo_cu_id", "cu_id")->where("afpafo_deleted", 0)->get()->sortBy("afpafo_name"),
                'platforms' => PlatformSource::all(),
				'categories' => $rd->requestCategories(),
				'devices' => Device::all(),
                'destinationtype' => DestinationType::all(),

				//Give back selected values
                'selected_date' => $request->date,
				'selected_date_from' => $request->date_from,
				'selected_date_to' => $request->date_to,
				'selected_name' => $request->name,
				'selected_email' => $request->email,
				'selected_ip_address' => $request->ip_address,
				'selected_zipcode_from' => $request->zipcode_from,
				'selected_city_from' => $request->city_from,
				'selected_zipcode_to' => $request->zipcode_to,
				'selected_city_to' => $request->city_to,
				'selected_request_id' => $request->request_id,
				'selected_pd_resubmit' => $request->pd_resubmit,
                'selected_destination_id' => $request->destination_id,

				'selected_country_from' => ($request->country_from ?? []),
				'selected_country_to' => ($request->country_to ?? []),
				'selected_request_type' => ($request->request_type ?? ""),
				'selected_size' => ($request->size ?? []),
				'selected_rejection_reason' => ($request->rejection_reason ?? []),
				'selected_category' => ($request->category ?? []),
				'selected_rejected' => ($request->rejected_by ?? []),
				'selected_website' => ($request->website ?? []),
				'selected_devices' => ($request->device ?? []),
				'selected_source' => ($request->source ?? []),
				'selected_affiliate' => ($request->affiliate ?? []),
				'selected_platform'=> ($request->platform ?? []),
                'selected_destination_type' => ($request->destination_type ?? ""),

				'requests' => $requests
			]);
	}
}
