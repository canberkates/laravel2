<?php

namespace App\Http\Controllers;

use App\Data\AffiliateFormType;
use App\Data\AffiliatePartnerRejectionReason;
use App\Data\ContactPersonDepartment;
use App\Data\ContactPersonDepartmentRole;
use App\Data\CustomerDocumentType;
use App\Data\CustomerEditForms;
use App\Data\CustomerPairStatus;
use App\Data\CustomerRemarkDepartment;
use App\Data\CustomerRemarkDirection;
use App\Data\CustomerRemarkMedium;
use App\Data\CustomerType;
use App\Data\RequestStatus;
use App\Data\VolumeType;
use App\Functions\System;
use App\Models\AffiliatePartnerData;
use App\Models\AffiliatePartnerRequest;
use App\Models\Cache;
use App\Models\Country;
use App\Models\Customer;
use App\Models\CustomerChange;
use App\Models\CustomerDocument;
use App\Models\Language;
use App\Models\PaymentCurrency;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Intervention\Image\Facades\Image;

class AffiliatePartnersController extends Controller
{
	public function index()
	{
		$system = new System();

		$cache = Cache::where("ca_name", "=", "customer_count")->first();

		$cached_count = $system->unserialize($cache->ca_value);
		$last_timestamp_updated = $cache->ca_timestamp;

		$customer_changes = CustomerChange::where([["cuch_approve_deny", "=", "0"]])->get();

		return view('affiliatepartners.index', [
			//'affiliatepartners' => $affiliatepartners,
			'customer_changes_count' => count($customer_changes),
			'cached_count' => $cached_count,
			'last_timestamp_updated' => $last_timestamp_updated
		]);
	}

	public function edit($id, $request_date_filter = null)
	{
		// get the customer and relations
		$customer = Customer::with(['remarks.user', 'documents.user', 'contactpersons.application_user', 'forms.portal'])->findOrFail($id);

        if (Gate::denies('customer-restrictions', $customer)) {
            abort(403);
        }

        //Check if collegue has the right URL
        $url_check = System::customerTypeToURL($customer->cu_id, $customer->cu_type);

        if ($url_check['redirect'] == true)
        {
            return redirect($url_check['url']);
        }

		//Show different view for deleted customers
		if($customer->cu_deleted == 1){
			return view('affiliatepartners.deleted',
				[
					'customer_id' => $customer->cu_id
				]);
		}

		$customerportalrequests = null;
		$request_date_filter_data = null;

		if ($request_date_filter != null)
		{
			$customerportalrequests = AffiliatePartnerRequest::with(["request.affiliateform" , "request.countryfrom", "request.countryto"])
				->join("requests", "re_id", "afpare_re_id")
				->where("afpare_cu_id", $id)
				->whereBetween("re_timestamp", [date("Y-m-d H:i:s", strtotime($request_date_filter[0])), date("Y-m-d H:i:s", strtotime($request_date_filter[1]))])
				->get();

			$request_date_filter_data = date("Y/m/d", strtotime($request_date_filter[0]))." - ".date("Y/m/d", strtotime($request_date_filter[1]));
		}

		$this->getContactPersonLogin($customer);

		$statuses = CustomerPairStatus::all();
		unset($statuses[2]);

		$documents = CustomerDocument::where("cudo_archived", "=", 0)->where("cudo_cu_id", "=", $customer->cu_id)->get();
		$documents_archived = CustomerDocument::where("cudo_archived", "=", 1)->where("cudo_cu_id", "=", $customer->cu_id)->get();

		return view('affiliatepartners.edit',
			[
				//Customer info
				'customer' => $customer,
				'remarks' => $customer->remarks,
				'documents' => $documents,
				'documents_archived' => $documents_archived,
				'offices' => $customer->offices,
				'moverdata' => $customer->moverdata,
				'affiliatedata' => $customer->affiliatedata,
				'forms' => $customer->forms,

				'customerportalrequests' => $customerportalrequests,

				"request_date_filter" => $request_date_filter_data,

				//Lists
				'users' => User::where("id", "!=", 0)->where("us_is_deleted", "=", 0)->orderBy("us_name")->get(),
				'countries' => Country::orderBy("co_en")->get(),
				'movertypes' => CustomerType::all(),
				'paymentcurrencies' => PaymentCurrency::all(),
				'languages' => Language::where("la_iframe_only", 0)->get(),
				'contactpersondepartments' => ContactPersonDepartment::all(),
				'contactpersondepartmentroles' => ContactPersonDepartmentRole::all(),
				'customerremarkdepartments' => CustomerRemarkDepartment::all(),
				'customerremarkmediums' => CustomerRemarkMedium::all(),
				'customerremarkdirections' => CustomerRemarkDirection::all(),
				'customerdocumenttypes' => CustomerDocumentType::all(),
				'requeststatuses' => RequestStatus::all(),
				'affiliaterejectionreasons' => AffiliatePartnerRejectionReason::all(),
				'statuses' => $statuses,
				'formtypes' => AffiliateFormType::all(),
				'volumetypes' => VolumeType::all()
			]);
	}

	public function update(Request $request, $id)
	{
		$customer = Customer::find($id);

		//If General form is posted
		if ($request->form_name == CustomerEditForms::GENERAL)
		{
			$request->validate([
				'la_code' => 'required',
				'company_name_business' => 'required',
				'company_name_legal' => 'required',
				//'attn' => 'required',
				//'attn_email' => 'required',
				//'lead_email' => 'required'
			]);

			$customer->cu_company_name_business = $request->company_name_business;
			$customer->cu_company_name_legal = $request->company_name_legal;
			$customer->cu_email = $request->lead_email;
			$customer->cu_email_bi = $request->billing_email;

            if ((empty($customer->cu_description_la_code) || $customer->cu_description != $request->description) && !empty($request->description)) {
                $description_la_code = System::getGoogleTranslationLanguage($request->description);

                if (!empty($description_la_code)) {
                    $customer->cu_description_la_code = $description_la_code;
                }
            }

			$customer->cu_description = $request->description;
			$customer->cu_attn = $request->attn;
			$customer->cu_attn_email = $request->attn_email;
			$customer->cu_la_code = $request->la_code;
			$customer->cu_coc = $request->coc;

            //if a new logo was uploaded
            if($request->logo)
            {
                $allowed_types = ['image/jpeg', 'image/png'];

                $image_size = getimagesize($_FILES['logo']['tmp_name']);
				$image_width = $image_size[0];
				$image_height = $image_size[1];

				if($_FILES['logo']['size'] < 1)
				{
					return redirect()->back()->withErrors([ 'logo' => 'The logo has not been saved, because the file is empty.']);
				}
				elseif($_FILES['logo']['size'] > 10000000)
				{
					return redirect()->back()->withErrors([ 'logo' => 'The logo has not been saved, because the file is bigger than 10MB.']);
				}
				elseif(!in_array($_FILES['logo']['type'], $allowed_types))
				{
					return redirect()->back()->withErrors([ 'logo' => 'The logo has not been saved, because the file type is not valid.']);
				}
				/*elseif($image_width < 100 || $image_height < 50)
				{
					return redirect()->back()->withErrors([ 'logo' => 'The logo has not been saved, because the image is too small. The image has to be min. 100 pixels wide and 50 pixels high.']);
				}
				elseif($image_width > 600 || $image_height > 200)
				{
					return redirect()->back()->withErrors([ 'logo' => 'The logo has not been saved, because the image is too big. The image has to be max. 600 pixels wide and 200 pixels high.']);
				}*/

                $system = new System();

                $img = Image::make($request->file('logo'));

                if ($img->width() > 600 || $img->height() > 200) {
                    $img->resize(600, 200, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }

                $file_location = config("system.paths.shared") . 'uploads/logos/';
                $new_logo = $request->file('logo');
                $new_logo_name = System::hashLogoName($request->curesc_id, $new_logo->getClientOriginalExtension());

                $img->save( $file_location.$new_logo_name );

                // Update the logo name in the db
                $customer->cu_logo = $new_logo_name;
            }

            $customer->save();

		} else if ($request->form_name == CustomerEditForms::ADDRESSES)
		{
			$request->validate([
				'street_1' => 'required',
				'zipcode' => 'required',
				'city' => 'required',
				'country' => 'required',
				'telephone' => 'required',
				'website' => 'required'
			]);

			if(($request->int_telephone == 0 && !empty($request->telephone)) || ($request->int_telephone_bi == 0 && !empty($request->telephone_bi)))
			{
				return redirect()->back()->withErrors(['phone' => 'This phone number is incorrect!']);
			}

			$customer->cu_street_1 = $request->street_1;
			$customer->cu_street_2 = $request->street_2;
			$customer->cu_zipcode = $request->zipcode;
			$customer->cu_city = $request->city;
			$customer->cu_telephone = $request->int_telephone;
			$customer->cu_website = $request->website;
			$customer->cu_co_code = $request->country;

			$customer->cu_use_billing_address = ($request->use_billing_address === 'on');

			if($request->use_billing_address === 'on')
			{

				$customer->cu_street_1_bi = $request->street_1_bi;
				$customer->cu_street_2_bi = $request->street_2_bi;
				$customer->cu_zipcode_bi = $request->zipcode_bi;
				$customer->cu_city_bi = $request->city_bi;
				$customer->cu_telephone_bi = $request->telephone_bi;
				$customer->cu_co_code_bi = $request->country_bi;
				$customer->cu_website_bi = $request->website_bi;
			}

			$customer->save();
		}
		else if ($request->form_name == CustomerEditForms::FINANCE)
		{
			$customer->cu_sales_manager = $request->sales_manager;
			$customer->cu_account_manager = $request->account_manager;
			$customer->cu_debt_manager = $request->debt_manager;
			$customer->cu_pacu_code = $request->payment_currency;

			$customer->save();
		}
		else if ($request->form_name == CustomerEditForms::SETTINGS)
		{
			//Update AFFILIATE data
			$affiliatedata = AffiliatePartnerData::query()->where('afpada_cu_id', $customer->cu_id)->first();

			$affiliatedata->afpada_status = $request->status;
            $customer->cu_show_revenues_in_affiliate_portal = ($request->show_revs_in_portal === 'on');
            $affiliatedata->afpada_max_requests_month = $request->max_requests_month;
			$affiliatedata->afpada_max_requests_month_left = $request->max_requests_month_left;
			$affiliatedata->afpada_max_requests_day = $request->max_requests_day;
			$affiliatedata->afpada_max_requests_day_left = $request->max_requests_day_left;

			$affiliatedata->afpada_price_per_lead = $request->price_per_lead;

			$affiliatedata->save();

			if ($request->revenue_share > 100)
				$request->revenue_share = 100;

			//Update CUSTOMER data
			$customer->cu_revenues_share = $request->revenue_share;

			$customer->save();
		}

		return redirect('affiliatepartners/' . $customer->cu_id . "/edit")->with('message', 'Affiliate Partner successfully updated!');
	}

	/**
	 * @param $customer
	 */
	public function getContactPersonLogin($customer): void
	{
		foreach ($customer->contactpersons as $contactperson)
		{
			if (!empty($contactperson->application_user))
			{
			    if ($_SERVER['SERVER_NAME'] == "laravel.localhost") {
			        //LOCAL
                    $start_url = "http://affiliate-partners.system";
                }
			    elseif ($_SERVER['SERVER_NAME'] == "erp2.triglobal-test-back.nl") {
			        //TEST
                    $start_url = "https://affiliate-partners.triglobal-test-back.nl";
                }
			    else {
			        //LIVE
                    $start_url = "https://affiliate-partners.triglobal.info";
                }

				$contactperson['login_url'] = $start_url."/erp.php/?id=" . md5($contactperson->application_user->apus_id) . "&login_key=" . md5($contactperson->application_user->apus_username) . "@" . md5($contactperson->application_user->apus_name);
			}
		}
	}

	public function getRequests(Request $request, $customer_id)
	{
		$system = new System();
		$dates = $system->betweenDates($request->date);

		return self::edit($customer_id, $dates);

	}
}
