<?php

namespace App\Http\Controllers;

use App\Functions\DataIndex;
use App\Functions\System;
use App\Models\Customer;
use App\Models\PaymentCurrency;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Log;

class AdyenController extends Controller
{
	public function index()
	{
	    $customers = Customer::select("cu_id", "cu_company_name_business")->whereIn("cu_type", [1,2,4,6])->get();

		return view('finance.adyen', [
			'selected_date' => "",
            'selected_customer' => "",
            'selected_exclude' => 0,
            'customers' => $customers
		]);
	}

	public function filteredIndex(Request $request)
	{
		$request->validate([
			'date' => 'required'
		]);

		Log::debug($request);

        $system = new System();

        $exclude_ccs = Arr::exists($request, "exclude_ccs") ? 1 : 0;

        $invoices = DB::table("adyen")
            ->leftJoin("customers","ad_cu_id", "cu_id")
            ->where("cu_deleted", 0)
            ->whereBetween("ad_timestamp", System::betweenDates($request->date));

        if(!empty($request->customer)){
            $invoices->where("ad_cu_id", $request->customer);
        }

        if($exclude_ccs){
            $invoices->whereRaw("`ad_merchant_reference` NOT LIKE 'CCPAYMENT%' AND `ad_merchant_reference` NOT LIKE 'CARDCHECK%' AND `ad_merchant_reference` NOT LIKE 'CCCHARGE%'");
        }

        $invoices = $invoices
            ->get();


        $total_amount = 0;

		foreach($invoices as $id => $row){
            $details = "<pre>".print_r($system->unserialize($row->ad_data), true)."</pre>";
            $details = str_replace("\"", "'", $details);

            $invoices[$id]->details = $details;

            if(empty($row->ad_amount_eur)){
                $euro = $system->currencyConvert($row->ad_amount, $row->ad_currency, "EUR");
            }else{
                $euro = $row->ad_amount_eur;
            }

            $invoices[$id]->euro = $euro;
            $total_amount += $euro;
        }

        $currency_tokens = [];

        foreach (PaymentCurrency::all() as $currency)
        {
            $currency_tokens[$currency->pacu_code] = $currency->pacu_token;
        }

        $customers = Customer::select("cu_id", "cu_company_name_business")->whereIn("cu_type", [1,2,4,6])->get();

        return view('finance.adyen', [
			'selected_date' => $request->date,
			'selected_customer' => $request->customer,
			'selected_exclude' => $exclude_ccs,
			'invoices' => $invoices,
			'total' => $total_amount,
            'currencies' => $currency_tokens,
            'customers' => $customers
		]);

	}

}
