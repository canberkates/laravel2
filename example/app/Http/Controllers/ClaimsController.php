<?php

namespace App\Http\Controllers;

use App\Data\ClaimReason;
use App\Data\ClaimStatus;
use App\Data\CreditDebitType;
use App\Data\DestinationType;
use App\Data\Device;
use App\Data\EmptyYesNo;
use App\Data\MoverCappingMethod;
use App\Data\MovingSelfCompany;
use App\Data\RequestCustomerPortalType;
use App\Data\RequestInternalCalled;
use App\Data\RequestResidence;
use App\Data\RequestSource;
use App\Data\RequestStatus;
use App\Data\RequestType;
use App\Data\YesNo;
use App\Functions\Mail;
use App\Functions\Mover;
use App\Functions\RequestData;
use App\Functions\RequestDeliveries;
use App\Functions\System;
use App\Functions\Translation;
use App\Models\AffiliatePartnerForm;
use App\Models\Claim;
use App\Models\Country;
use App\Models\Customer;
use App\Models\InvoiceLine;
use App\Models\Language;
use App\Models\Portal;
use App\Models\Region;
use App\Models\Request;
use App\Models\CreditDebitInvoiceLine;
use App\Models\User;
use App\Models\WebsiteForm;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ClaimsController extends Controller
{
	public function index()
	{
		$open_claims = Claim::with("customerrequestportal.customer")->where("cl_status", 0)->get();

		foreach(Country::all() as $country){
			$countries[$country->co_code] = $country->co_en;
		}

        $date_from = date("Y/m/d", strtotime("-30 days"));
		$date_now = date("Y/m/d");
		$default_date_filter = $date_from." - ".$date_now;

		return view('claims.index',
			[
				'open_claims' => $open_claims,
				'claimstatuses' => ClaimStatus::all(),
				'claimreasons' => ClaimReason::all(),
				'countries' => $countries,
                'users' => User::where("us_is_deleted", 0)->orderBy("us_name", "asc")->get(),
                'default_date_filter' => $default_date_filter
			]);
	}
	public function filteredindex(\Illuminate\Http\Request $request)
	{
        $filter_status = null;
        $filter_date = null;
        $filter_employee = null;

        $claims = Claim::with("customerrequestportal.customer", "claimprocessedbyuser");

        if (!empty($request->status) || $request->status === "0")
        {
            $filter_status = $request->status;
            $claims = $claims->where("cl_status", $filter_status);
        }

        if ($filter_status != 0)
        {
            $filter_date = $request->date;

            $claims = $claims->whereBetween("cl_timestamp", System::betweenDates($filter_date));

            if (!empty($request->employee))
            {
                $filter_employee = $request->employee;
                $claims = $claims->where("cl_processed_us_id", $filter_employee);
            }
        }

        $claims = $claims->get();

		foreach(Country::all() as $country){
			$countries[$country->co_code] = $country->co_en;
		}

		return view('claims.index',
			[
				'open_claims' => $claims,
				'claimstatuses' => ClaimStatus::all(),
				'claimreasons' => ClaimReason::all(),
				'countries' => $countries,
                'users' => User::where("us_is_deleted", 0)->orderBy("us_name", "asc")->get(),

                //FILTERS
                'filter_status' => $filter_status,
                'filter_date' => $filter_date,
                'filter_employee' => $filter_employee
			]);
	}

	public function edit($claim_id)
	{
		$requestData = new RequestData();
		$system = new System();
		$mover = new Mover();
		//Get customer and through the relation of the customer, get the correct contact person
		$claim = Claim::with(["customerrequestportal.customer", "customerrequestportal.request"])->find($claim_id);
		//Get all matches for the request and get there respective claims+status
		$other_claims =
			DB::table('kt_request_customer_portal')
				->select("*")
				->join("requests", "ktrecupo_re_id", "=", "re_id")
				->join("customers", "ktrecupo_cu_id", "=", "cu_id")
				->leftJoin("claims", "ktrecupo_id", "=", "cl_ktrecupo_id")
				->where('re_id', "=" , $claim->customerrequestportal->ktrecupo_re_id)
				->get();

		$claim_rate = $mover->calcReclamationRate(90, $claim->customerrequestportal->customer->cu_id);

		foreach(Country::all() as $country){
			$countries[$country->co_code] = $country->co_en;
		}

		$request = Request::findOrFail($claim->customerrequestportal->ktrecupo_re_id);

		$portal = Portal::find($request->re_po_id);

		if($request->re_source == 1){
			$form = WebsiteForm::with("website")->where("wefo_id", $request->re_wefo_id)->first()->website->we_website;
		}
		else{
			$form = AffiliatePartnerForm::where("afpafo_id", $request->re_afpafo_id)->first()->afpafo_name;
		}

		$destination_type = DestinationType::name($request->re_destination_type);

		$language = Language::where("la_code",$request->re_la_code)->first()->la_language;

		if(!empty( $request->re_reg_id_from))
		{
            $regionFromQuery = Region::where("reg_id", $request->re_reg_id_from)->first();

			$region_from = ((!empty($regionFromQuery->reg_name) ? $regionFromQuery->reg_name : ((!empty($regionFromQuery->reg_parent) ? $regionFromQuery->reg_parent : ""))));
		}
		if(!empty( $request->re_reg_id_to))
		{
            $regionToQuery = Region::where("reg_id", $request->re_reg_id_to)->first();

            $region_to = ((!empty($regionToQuery->reg_name) ? $regionToQuery->reg_name : ((!empty($regionToQuery->reg_parent) ? $regionToQuery->reg_parent : ""))));
		}

		//Get correct volume calculator values
		$volumecalculator = $request->volumecalculator->voca_volume_calculator ? $system->volumeCalculator($request->volumecalculator->voca_volume_calculator) : "";

		return view('claims.edit',
			[
				'claim' => $claim,
				'countries' => $countries,
				'claimstatuses' => ClaimStatus::all(),
				'claimreasons' => ClaimReason::all(),
				'requestcustomerportaltyes' => RequestCustomerPortalType::all(),
				'requeststatuses' => RequestStatus::all(),
				'claimrate' => $claim_rate,
				'cl_called' => YesNo::name($claim->customerrequestportal->request->re_internal_called),
				'yesno' => YesNo::all(),
				'other_claims' => $other_claims,
				'request' => $request,
				'users' => User::where("id", "!=", 0)->where("us_is_deleted", 0)->get(),
				'portal' => $portal->po_portal,
				'form' => $form,
				'emptyyesno' => EmptyYesNo::all(),
				'byselfcompany' => MovingSelfCompany::all(),
				'requestsource' => RequestSource::name($request->re_source),
				'languages' => Language::where("la_iframe_only", 0)->get(),
				'device' => Device::name($request->re_device),
				'destinationtype' => $destination_type,
				'requesttypes' => RequestType::all(),
				'movingsizes' => $system->movingSizes($request->re_request_type),
				'region_from' => $region_from,
				'region_to' => $region_to,
				'language' => $language,
				'requestresidences' => RequestResidence::all(),
				'called' => RequestInternalCalled::all(),
				'volumecalculator' => $volumecalculator,
			]);
	}

	public function accept($claim_id)
	{
		$system = new System();
		$claim = Claim::with("customerrequestportal.request")->find($claim_id);

		if($claim->cl_status > ClaimStatus::OPEN){
			return view('claims.processed');
		}

		$reasons = $system->filteredClaimReasons($claim->customerrequestportal->request->re_request_type, $claim->customerrequestportal->request->re_destination_type);

		return view('claims.accept',
			[
				'claim' => $claim,
				'claimreasons' => $reasons
			]);
	}

	public function acceptclaim(\Illuminate\Http\Request $webrequest, $claim_id)
	{
		$system = new System();
		$mover = new Mover();
		$translate = new Translation();
		$requestdeliveries = new RequestDeliveries();
		$mail = new Mail();

		$claim = DB::table("claims")
			->leftJoin("kt_request_customer_portal","cl_ktrecupo_id","ktrecupo_id")
			->leftJoin("kt_customer_portal","ktrecupo_ktcupo_id","ktcupo_id")
			->leftJoin("requests","ktrecupo_re_id","re_id")
			->leftJoin("customers","ktrecupo_cu_id","cu_id")
			->leftJoin("mover_data","ktrecupo_cu_id","moda_cu_id")
			->leftJoin("portals","ktrecupo_po_id","po_id")
			->where("cl_id", $claim_id)
			->where("cl_status",0)
			->where("cu_deleted",0)
			->first();

		if(!empty($claim)){

			$mover_capping_change = $claim->moda_capping_method == MoverCappingMethod::MONTHLY_SPEND ? $claim->ktrecupo_amount_netto : 1;

			//Set claim as accepted
			DB::table('claims')
				->where('cl_id',  $claim->cl_id)
				->update(
					[
						'cl_status' => ClaimStatus::ACCEPTED,
						'cl_processed_timestamp' => Carbon::now()->toDateTimeString(),
						'cl_processed_us_id' => \Auth::id()
					]
				);

			if($claim->ktrecupo_free == 0){

				if(date("m") == date("m", strtotime($claim->ktrecupo_timestamp)))
				{
					DB::table('kt_customer_portal')
						->where('ktcupo_id', $claim->ktcupo_id)
						->update(
							[
								'ktcupo_max_requests_month_left' =>DB::raw("(`ktcupo_max_requests_month_left` + 1)")
							]
						);

					DB::table('mover_data')
						->where('moda_cu_id', $claim->cu_id)
						->update(
							[
								'moda_capping_monthly_limit_left' =>DB::raw("(`moda_capping_monthly_limit_left` + {$mover_capping_change})")
							]
					);
				}

				if(date("d") == date("d", strtotime($claim->ktrecupo_timestamp)))
				{
					DB::table('kt_customer_portal')
						->where('ktcupo_id', $claim->ktcupo_id)
						->update(
							[
								'ktcupo_max_requests_day_left' =>DB::raw("(`ktcupo_max_requests_day_left` + 1)")
							]
						);
				}

				DB::table('kt_customer_portal')
					->where('ktcupo_id', $claim->ktcupo_id)
					->update(
						[
							'ktcupo_requests_bucket' =>DB::raw("IF(
								`ktcupo_requests_bucket` - 1 > 0,
								`ktcupo_requests_bucket` - 1, 0
							)")
						]
					);

				DB::table('mover_data')
					->where('moda_cu_id', $claim->cu_id)
					->update(
						[
							'moda_capping_bucket' =>DB::raw("IF(
								`moda_capping_bucket` - '{$mover_capping_change}' > 0,
								`moda_capping_bucket` - '{$mover_capping_change}', 0
							)"),
                            'moda_forced_daily_capping_left' => DB::raw("`moda_forced_daily_capping_left` + 1")
						]
					);

				if($claim->ktrecupo_invoiced){
                    $invoice_line = InvoiceLine::where("inli_ktrecupo_id", $claim->ktrecupo_id)->first();

                    $cdil = new CreditDebitInvoiceLine();

					$cdil->crdeinli_timestamp = Carbon::now()->toDateTimeString();
					$cdil->crdeinli_cu_id = $claim->cu_id;
					$cdil->crdeinli_ktrecupo_id = $claim->ktrecupo_id;
					$cdil->crdeinli_leac_number = $claim->cu_leac_number;
					$cdil->crdeinli_description = $translate->get("invoice_free_requests_previous_invoice", $claim->cu_la_code). "(".$translate->get("request_type_val_".$claim->re_request_type, $claim->cu_la_code);
					$cdil->crdeinli_type = CreditDebitType::CREDIT;
                    $cdil->crdeinli_currency = $invoice_line->inli_currency;
                    $cdil->crdeinli_amount = -$invoice_line->inli_amount_netto;

					$cdil->save();
				}
			}

            DB::table('kt_request_customer_portal')
                ->where('ktrecupo_id', $claim->ktrecupo_id)
                ->update(
                    [
                        'ktrecupo_free' => YesNo::YES,
                        'ktrecupo_free_reason' => $webrequest->reason,
                        'ktrecupo_free_percentage' => 1
                    ]
                );


            foreach($requestdeliveries->receivers(1, $claim->ktcupo_id, $claim->cu_email, 1) as $receiver)
			{
				$fields = [
					"request_delivery_value" => $receiver['value'],
					"request_delivery_extra" => $receiver['extra'],
					"claim" => $system->databaseToArray($claim)
				];

				$mail->send("claim_accept", $claim->cu_la_code, $fields);
			}


			/**
			 * Do some calculations so we know when a prepayment user can turn off credithold
			 */
			//Get current balances of customer
            $balance_total = $mover->customerBalancesFC([$claim->cu_id])[$claim->cu_id];
            $balance_start = $balance_total - $claim->ktrecupo_amount_netto;

			//Set credithold off for a customer if balance > 0 and is prepayment
			if ($balance_total > 0 && $claim->cu_credit_hold == 1 && ($claim->cu_payment_method == 4 || $claim->cu_payment_method == 5  || $claim->cu_payment_method == 6  ) && $balance_start < 20)
			{

                //Set customer credit hold
                $get_customer = Customer::where("cu_id", $claim->cu_id)->first();
                $get_customer->cu_credit_hold = 0;
                $get_customer->save();

				//Send message to Hans, so he know that the customer's credithold is removed
				$message = "Credithold is removed from Customer: ".$claim->cu_company_name_business." (Cu ID = ".$claim->cu_id.")";

				$system->sendMessage(4560, "<b>".$claim->cu_company_name_business."</b> Credithold is removed by Claim Accepted", $message);
			}
		}
		return redirect('claims/')->with('message', 'Claim successfully accepted!');

	}

	public function reject($claim_id)
	{
		$claim = Claim::with("customerrequestportal.request")->find($claim_id);

		if($claim->cl_status > ClaimStatus::OPEN){
			return view('claims.processed');
		}

		return view('claims.reject',
			[
				'claim' => $claim
			]);
	}

	public function rejectclaim(\Illuminate\Http\Request $webrequest, $claim_id)
	{
		$system = new System();
		$requestdata = new RequestData();
		$requestdeliveries = new RequestDeliveries();
		$mail = new Mail();

		if(empty($webrequest->rejectionreason))
		{
			return redirect()->back()->withErrors([ 'match_amount' => "Please enter the rejection reason."]);
		}

		$claim = DB::table("claims")
			->leftJoin("kt_request_customer_portal","cl_ktrecupo_id","ktrecupo_id")
			->leftJoin("kt_customer_portal","ktrecupo_ktcupo_id","ktcupo_id")
			->leftJoin("requests","ktrecupo_re_id","re_id")
            ->leftJoin("volume_calculator", "voca_id", "re_id")
			->leftJoin("customers","ktrecupo_cu_id","cu_id")
			->leftJoin("portals","ktrecupo_po_id","po_id")
			->where("cl_id", $claim_id)
			->where("cl_status",0)
			->where("cu_deleted",0)
			->first();

		if(!empty($claim))
		{

			DB::table('claims')
				->where('cl_id', $claim->cl_id)
				->update(
					[
						'cl_status' => ClaimStatus::REJECTED,
						'cl_processed_timestamp' => Carbon::now()->toDateTimeString(),
						'cl_processed_us_id' => \Auth::id(),
						'cl_rejection_reason' => $webrequest->rejectionreason
					]
				);

			foreach($requestdeliveries->receivers(1, $claim->ktcupo_id, $claim->cu_email, 1) as $receiver)
			{
				$fields = [
					"request_delivery_value" => $receiver['value'],
					"request_delivery_extra" => $receiver['extra'],
					"claim" => $system->databaseToArray($claim),
					"rejection_reason" => stripslashes($webrequest->rejectionreason)
				];

				$mail->send("claim_reject", $claim->cu_la_code, $fields);
			}
		}

		if(isset($webrequest->sendmail))
		{
		    $claim->re_volume_calculator = ((!empty($claim->voca_volume_calculator)) ? $claim->voca_volume_calculator : null);

			$fields = [
				"matches" => $requestdata->requestMatchesDone($claim->re_id),
				"claim" => $system->databaseToArray($claim)
			];

			$mail->send("claim_reject_unable_to_reach", $claim->cu_la_code, $fields);
		}

		return redirect('claims/')->with('message', 'Claim successfully rejected!');
	}


}
