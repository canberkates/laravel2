<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\Customer;
use App\Models\CustomerOffice;
use App\Models\State;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class CustomerOfficeController extends Controller
{
	public function edit($customer_id, $office_id)
	{
		//Get customer and through the relation of the customer, get the correct contact person
		$office = CustomerOffice::with(['customer'])->findOrFail($office_id);

        $customer = Customer::where("cu_id", $office->cuof_cu_id)->first();

        //Check if this user has the rights to edit
        if (Gate::denies('customer-restrictions', $customer)) {
            abort(403);
        }

		$states = "";

		if (!empty($office->cuof_state))
		{
			$states = State::where([["st_co_code", "=", $office->cuof_co_code]])->get();
		}

		return view('customeroffice.edit',
			[
				'office' => $office,
				'countries' => Country::all(),
				'states' => $states
			]);
	}

	public function update(Request $request, $office_id)
	{
		$request->validate([
			'street_1' => 'required',
			'city' => 'required',
			'country' => 'required'
		]);

		$customeroffice = CustomerOffice::find($office_id);

		$customeroffice->cuof_street_1 = $request->street_1;
		$customeroffice->cuof_street_2 = $request->street_2;
		$customeroffice->cuof_city = $request->city;
		$customeroffice->cuof_zipcode = $request->zipcode;
		$customeroffice->cuof_state = $request->state;
		$customeroffice->cuof_co_code = $request->country;
		$customeroffice->cuof_email = $request->email;
		$customeroffice->cuof_telephone = $request->telephone;
		$customeroffice->cuof_website = $request->website;

		$customeroffice->save();

		$customer = Customer::findOrFail($customeroffice->cuof_cu_id);

		if ($customer->cu_type == 1)
		{
			return redirect('customers/' . $customer->cu_id . "/edit")->with('message', 'Office is successfully updated!');
		}
		elseif ($customer->cu_type == 6)
		{
			return redirect('resellers/' . $customer->cu_id . "/edit")->with('message', 'Office is successfully updated!');
		}
		elseif ($customer->cu_type == 2)
		{
			return redirect('serviceproviders/' . $customer->cu_id . "/edit")->with('message', 'Office is successfully updated!');
		}
		else
		{
			return redirect('affiliatepartners/' . $customer->cu_id . "/edit")->with('message', 'Office is successfully updated!');
		}
	}

	public function create($customer_id)
	{
		$customer = Customer::findOrFail($customer_id);

		return view('customeroffice.create',
			[
				'customer' => $customer,
				'countries' => Country::all()
			]);
	}

	public function store(Request $request)
	{
		$request->validate([
			'street_1' => 'required',
			'city' => 'required',
			'country' => 'required'
		]);

		// Validate the request...
		$customer = Customer::findOrFail($request->customer_id);

		//Create new contact person
		$customeroffice = new CustomerOffice();

		$customeroffice->cuof_street_1 = $request->street_1;
		$customeroffice->cuof_street_2 = $request->street_2;
		$customeroffice->cuof_city = $request->city;
		$customeroffice->cuof_zipcode = $request->zipcode;
		$customeroffice->cuof_state = $request->state;
		$customeroffice->cuof_co_code = $request->country;
		$customeroffice->cuof_email = $request->email;
		$customeroffice->cuof_telephone = $request->telephone;
		$customeroffice->cuof_website = $request->website;

		//Set to correct customer and save
		$customeroffice->customer()->associate($customer);
		$customeroffice->save();

		if ($customer->cu_type == 1)
		{
			return redirect('customers/' . $customer->cu_id . "/edit")->with('message', 'Office is successfully added!');
		}
		elseif ($customer->cu_type == 6)
		{
			return redirect('resellers/' . $customer->cu_id . "/edit")->with('message', 'Office is successfully added!');
		}
		elseif ($customer->cu_type == 2)
		{
			return redirect('serviceproviders/' . $customer->cu_id . "/edit")->with('message', 'Office is successfully added!');
		}
		else
		{
			return redirect('affiliatepartners/' . $customer->cu_id . "/edit")->with('message', 'Office is successfully added!');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\MoverData  $moverData
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($office_id)
	{
		$office = CustomerOffice::find($office_id);

		$cu_id = $office->cuof_cu_id;

		$customer = Customer::where("cu_id", $cu_id)->first();
		$office->delete();

        if ($customer->cu_type == 1)
        {
            return redirect('customers/' . $customer->cu_id . "/edit")->with('message', 'Office is successfully deleted!');
        }
        elseif ($customer->cu_type == 6)
        {
            return redirect('resellers/' . $customer->cu_id . "/edit")->with('message', 'Office is successfully deleted!');
        }
        elseif ($customer->cu_type == 2)
        {
            return redirect('serviceproviders/' . $customer->cu_id . "/edit")->with('message', 'Office is successfully deleted!');
        }
        else
        {
            return redirect('affiliatepartners/' . $customer->cu_id . "/edit")->with('message', 'Office is successfully deleted!');
        }
	}

	public function getStates(Request $request)
	{
		$query = State::where("st_co_code", "=", $request->co_code)->get();

		$states = [];

		if(!empty($query))
		{
			foreach($query as $state){
				$states[$state['st_zipcode']] = $state['st_name'];
			}
		}

		echo json_encode($states);
	}

	public function delete(Request $request)
	{
		DB::table('customer_offices')->where('cuof_id', '=', $request->id)->delete();
	}
}
