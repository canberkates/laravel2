<?php
/**
 * Created by PhpStorm.
 * User: Arjan de Coninck
 * Date: 31-3-2020
 * Time: 14:59
 */

namespace App\Http\Controllers;


use App\Models\EmailBuilderTemplate;
use App\Models\MovingAssistantEmail;
use App\Models\SireloSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;


class SireloSettingController
{
    public function index()
    {
        return view('sirelo_settings.index',
            [
                'sirelo_settings' => SireloSetting::all(),
            ]);
    }

    public function edit($setting_id)
    {
        $sirelo_setting = SireloSetting::where("sise_id", $setting_id)->first();

        return view('sirelo_settings.edit',
            [
                'sirelo_setting' => $sirelo_setting,
            ]);
    }

    public function update(Request $request, $setting_id)
    {
        $request->validate([
            'value' => 'required',
        ]);

        $sirelo_setting = SireloSetting::findOrFail($setting_id);
        $sirelo_setting->sise_value = $request->value;
        $sirelo_setting->sise_updated_timestamp = date("Y-m-d H:i:s");
        $sirelo_setting->save();

        return redirect('sirelo_settings');
    }

    public function movingAssistant()
    {
        return view('sirelo_settings.movingassistant',
            [
                'emails' => MovingAssistantEmail::all(),
            ]);
    }

    public function movingAssistantAddEmail()
    {
        return view('sirelo_settings.movingassistant_add_email',
            [
                'emails' => EmailBuilderTemplate::groupBy("embute_name")->get(),
            ]);
    }

    public function movingAssistantAddEmailStore(Request $request)
    {
        $request->validate([
            'email_name' => 'required',
            'days_before' => 'required',
        ]);

        $exists = MovingAssistantEmail::where("moasem_embute_id", $request->email_name)->first();

        if (count($exists) > 0) {
            //Must be unique
            return Redirect::back()->withInput($request->input())->withErrors(['This email is already active for the Moving Assistant']);
        }

        $moasem = new MovingAssistantEmail();
        $moasem->moasem_embute_id = $request->email_name;
        $moasem->moasem_timestamp = date("Y-m-d H:i:s");
        $moasem->moasem_days_before_mail = $request->days_before;
        $moasem->save();

        return redirect('movingassistant');
    }

    public function movingAssistantEditEmail($moasem_id)
    {
        $moasem = MovingAssistantEmail::findOrFail($moasem_id);
        return view('sirelo_settings.movingassistant_edit_email',
            [
                'moasem' => $moasem,
                'emails' => EmailBuilderTemplate::groupBy("embute_name")->get(),
            ]);
    }

    public function movingAssistantEditEmailUpdate(Request $request, $moasem_id)
    {
        $request->validate([
            'email_name' => 'required',
            'days_before' => 'required',
        ]);

        $exists = MovingAssistantEmail::where("moasem_embute_id", $request->email_name)->where("moasem_id", "!=", $moasem_id)->first();

        if (count($exists) > 0) {
            //Must be unique
            return Redirect::back()->withInput($request->input())->withErrors(['This email is already active for the Moving Assistant']);
        }

        $moasem = MovingAssistantEmail::findOrFail($moasem_id);
        $moasem->moasem_embute_id = $request->email_name;
        $moasem->moasem_days_before_mail = $request->days_before;
        $moasem->save();

        return redirect('movingassistant');
    }

    public function movingAssistantDelete(Request $request)
    {
        $moasem = MovingAssistantEmail::findOrFail($request->id);
        $moasem->delete();
    }
}

