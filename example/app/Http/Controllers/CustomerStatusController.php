<?php

namespace App\Http\Controllers;

use App\Functions\System;
use App\Models\Customer;
use App\Models\CustomerRemark;
use App\Models\CustomerStatus;
use App\Data\CustomerStatusReason;
use App\Data\CustomerStatusSource;
use App\Data\CustomerStatusStatus;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CustomerStatusController extends Controller
{
	public function create($customer_id)
	{
		$customer = Customer::findOrFail($customer_id);

        $cancellation_reasons = CustomerStatusReason::all();
        $reasons = CustomerStatusReason::all();

        unset($reasons[16]);
        unset($reasons[17]);
        unset($reasons[18]);
        unset($reasons[19]);
        unset($reasons[20]);
        unset($reasons[21]);
        unset($reasons[22]);
        unset($reasons[23]);
        unset($reasons[24]);
        unset($reasons[25]);
        unset($reasons[26]);
        unset($cancellation_reasons[1]);
        unset($cancellation_reasons[2]);
        unset($cancellation_reasons[3]);
        unset($cancellation_reasons[4]);
        unset($cancellation_reasons[5]);
        unset($cancellation_reasons[6]);
        unset($cancellation_reasons[7]);
        unset($cancellation_reasons[8]);
        unset($cancellation_reasons[9]);
        unset($cancellation_reasons[10]);
        unset($cancellation_reasons[11]);
        unset($cancellation_reasons[12]);
        unset($cancellation_reasons[13]);
        unset($cancellation_reasons[14]);
        unset($cancellation_reasons[15]);

		return view('customerstatus.create',
			[
				'customer' => $customer,
				//'users' => User::all(),
				'statuses' => CustomerStatusStatus::all(),
				'sources' => CustomerStatusSource::all(),
				'cancellation_reasons' => $cancellation_reasons,
				'reasons' => $reasons,
				'employees' => User::where([['us_sales', '=', 1], ['us_is_deleted', '=', 0]])->orderBy('us_name', 'ASC')->get()
			]);
	}

	public function store(Request $request){
		// Validate the request...
		$customer = Customer::findOrFail($request->customer_id);

		// Create a User using User model
		$customerstatus = new CustomerStatus();

		if($request->status == CustomerStatusStatus::PAUSE || $request->status == CustomerStatusStatus::UNPAUSE){
			$request->validate([
				'date' => 'required',
				'employee' => 'required',
				'status' => 'required',
				'reason' => 'required',
				'customer_status_remark' => 'required'
			]);
		}
		else{
			$request->validate([
				'date' => 'required',
				'employee' => 'required',
				'status' => 'required',
				'number_of_monthly_leads' => 'required',
				'capping_number_of_leads' => 'required',
				'price_per_lead' => 'required'
			]);
		}

		$plus = [1, 2, 3];
		$zero = [4, 5, 6, 7];
		$minus = [8, 9];

		if(in_array($request->status, $zero))
		{
			$amount = 0;
		}
		else
		{
			$amount = $request->turnover_netto;

			if((in_array($request->status, $plus) && $amount < 0) || (in_array($request->status, $minus) && $amount > 0))
			{
                $amount = $amount * -1;
			}
		}

		$amount = preg_replace("/[^0-9.-]/", "", $amount);

		$customerstatus->cust_date = $request->date;
		$customerstatus->cust_employee = $request->employee;
		$customerstatus->cust_status = $request->status;

		$customerstatus->cust_turnover_netto = $amount;
		$customerstatus->cust_number_of_leads = $request->number_of_monthly_leads;
		$customerstatus->cust_capping_of_leads = $request->capping_number_of_leads;
		$customerstatus->cust_price_per_lead = $request->price_per_lead;
		$customerstatus->cust_remark = $request->customer_status_remark;

		if ($request->status == CustomerStatusStatus::NEW_BOOKING || $request->status == CustomerStatusStatus::REACTIVATION)
		{
			$request->validate([
				'source' => 'required',
			]);

			$customerstatus->cust_booking_source = $request->source;
		}
		elseif ($request->status == CustomerStatusStatus::UPSELL || $request->status == CustomerStatusStatus::DOWNSELL)
		{
			$request->validate([
				'previous_number_of_monthly_leads' => 'required',
				'previous_capping_number_of_leads' => 'required',
				'previous_price_per_lead' => 'required'
			]);

			$customerstatus->cust_previous_leads = $request->previous_number_of_monthly_leads;
			$customerstatus->cust_previous_capping = $request->previous_capping_number_of_leads;
			$customerstatus->cust_previous_price = $request->previous_price_per_lead;
		}
		elseif ($request->status == CustomerStatusStatus::PAUSE || $request->status == CustomerStatusStatus::UNPAUSE)
		{
			$request->validate([
				'reason' => 'required'
			]);

			$customerstatus->cust_reason = $request->reason;
		}
		elseif ($request->status == CustomerStatusStatus::CANCELLATION)
		{
			$request->validate([
				'reason' => 'required',
				'lifetime_in_months' => 'required'
			]);

			$customerstatus->cust_reason = $request->cancellation_reason;
			$customerstatus->cust_lifetime = $request->lifetime_in_months;
		}

		//Set to correct customer and save
		$customerstatus->customer()->associate($customer);
		$customerstatus->save();

		//Use Partnerdesk or Finance as type if the correct managers add a status, Default: System
		$setting_department = 0;

		if(System::getSetting('default_partnerdesk_manager') == Auth::id())
		{
			$setting_department = 4;
		}
		elseif(System::getSetting('default_finance_manager') == Auth::id())
		{
			$setting_department = 2;
		}

		//Create a Customer Remark
		$customer_remark = New CustomerRemark();
		$customer_remark->cure_cu_id = $customer->cu_id;
		$customer_remark->cure_timestamp = date("Y-m-d H:i:s");
		$customer_remark->cure_department = $setting_department;
		$customer_remark->cure_employee = $request->employee;
		$customer_remark->cure_status_id = $customerstatus->cust_id;
		$customer_remark->save();

		if ($customer->cu_type == 1)
		{
			return redirect('customers/' . $customer->cu_id . "/edit")->with('message', 'Customer Status successfully added!');
		}
		elseif ($customer->cu_type == 6)
		{
			return redirect('resellers/' . $customer->cu_id . "/edit")->with('message', 'Customer Status successfully added!');
		}
		elseif ($customer->cu_type == 2)
		{
			return redirect('serviceproviders/' . $customer->cu_id . "/edit")->with('message', 'Customer Status successfully added!');
		}
		else
		{
			return redirect('affiliatepartners/' . $customer->cu_id . "/edit")->with('message', 'Customer Status successfully added!');
		}
	}

	public function delete(Request $request)
	{
		$status = CustomerStatus::with("remark")->find($request->id);

		if(!empty($status->remark)){
			$remark = CustomerRemark::find($status->remark->cure_id);
			$remark->delete();
		}

		$status->delete();
	}
}
