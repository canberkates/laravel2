<?php

namespace App\Http\Controllers;

use App\Data\FreeTrialStopType;
use App\Models\Customer;
use App\Models\FreeTrial;
use App\Models\KTCustomerPortal;
use Auth;
use Illuminate\Http\Request;

class FreeTrialController extends Controller
{
	public function create($customer_id, $customerportal_id)
	{
		$customer = Customer::find($customer_id);


		return view('freetrial.create',
			[
				'customer' => $customer,
				'customerportal' => $customerportal_id,
				'freetrialstoptypes' => FreeTrialStopType::all()
			]);
	}

	public function store(Request $request)
	{
		$request->validate([
			'date' => 'required',
			'stop_type' => 'required'
		]);

		$customerportal = KTCustomerPortal::with("customer")->find($request->ktcupo_id);

		if(self::dateDifference(date("Y-m-d"), date("Y-m-d", strtotime($request->date))) < 0)
		{
			return redirect()->back()->withInput()->withErrors([ 'date'=> 'The free trial date is in the past.']);
		}
		elseif(($request->stop_type == 1 || $request->stop_type == 3) && self::dateDifference(date("Y-m-d"), date("Y-m-d", strtotime($request->stop_date))) <= 0)
		{
			return redirect()->back()->withInput()->withErrors([ 'stop_date'=> 'The stop date is today or in the past.']);
		}
		elseif(($request->stop_type == 1 || $request->stop_type == 3) && self::dateDifference(date("Y-m-d", strtotime($request->date)), date("Y-m-d", strtotime($request->stop_date))) <= 0)
		{
			return redirect()->back()->withInput()->withErrors([ 'stop_date'=> 'The stop date is on the same day or even before the free trial date.']);
		}
		else
		{
			//Create new Free Trial
			$freetrial = new FreeTrial();

			$freetrial->frtr_ktcupo_id = $request->ktcupo_id;
			$freetrial->frtr_us_id = Auth::id();
			$freetrial->frtr_date = $request->date;
			$freetrial->frtr_stop_type = $request->stop_type;

			if ($request->stop_type == 1)
			{
				$request->validate([
					'stop_date' => 'required'
				]);

				$freetrial->frtr_stop_date = $request->stop_date;
			}
			elseif($request->stop_type == 2)
			{
				$request->validate([
					'stop_requests' => 'required',
					'stop_requests_left' => 'required'
				]);

				$freetrial->frtr_stop_requests = $request->stop_requests;
				$freetrial->frtr_stop_requests_left = $request->stop_requests_left;
			}
			else
			{
				$request->validate([
					'stop_date' => 'required',
					'stop_requests' => 'required',
					'stop_requests_left' => 'required'
				]);

				$freetrial->frtr_stop_date = $request->stop_date;
				$freetrial->frtr_stop_requests = $request->stop_requests;
				$freetrial->frtr_stop_requests_left = $request->stop_requests_left;
			}

			$freetrial->frtr_remark = $request->remark;

			//Set to correct customer and save
			$freetrial->save();

			if ($customerportal->customer->cu_type == 6)
            {
                return redirect('resellers/' . $customerportal->ktcupo_cu_id . '/customerportal/' . $customerportal->ktcupo_id . '/edit');
            }
			else {
                return redirect('customers/' . $customerportal->ktcupo_cu_id . '/customerportal/' . $customerportal->ktcupo_id . '/edit');
            }
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\MoverData $moverData
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($freetrial_id)
	{
		$freetrial = FreeTrial::find($freetrial_id);
		$freetrial->delete();

		return redirect()->back();
	}

	private function dateDifference($date_from, $date_to)
	{
		$ts1 = strtotime($date_from);
		$ts2 = strtotime($date_to);

		$seconds_diff = $ts2 - $ts1;

		return floor($seconds_diff / 3600 / 24);
	}
}
