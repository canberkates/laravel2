<?php

namespace App\Http\Controllers;

use App\Functions\System;
use App\Models\Cache;
use App\Models\MoverData;
use App\Models\Request;
use App\Models\User;
use Auth;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $from = date('Y-m-d');
        $to = date('Y-m-d');

        $dateFrom = date( 'Y-m-d H:i:s', strtotime( $from.' 00:00:00' ) );
        $dateTo = date( 'Y-m-d H:i:s', strtotime( $to.' 23:59:59' ) );

        $requests = Request::selectRaw( '*' )
            ->selectRaw( "SUM(if(`re_status` = '0', 1, 0)) AS `open`")
            ->selectRaw( "SUM(if(`re_status` = '1', 1, 0)) AS `matched`" )
            ->selectRaw( "SUM(if(`re_status` = '2', 1, 0)) AS `rejected`" )
            ->whereBetween( 're_timestamp', [$dateFrom, $dateTo] )
            ->first();

        $birthdays = User::selectRaw( 'us_id, us_name, TIMESTAMPDIFF(YEAR, `us_date_of_birth`, CURDATE()) AS `age`' )->whereRaw( 'SUBSTR( CURDATE(), 6, 10 ) = SUBSTR( `us_date_of_birth`, 6, 10 )' )->where("us_is_deleted", 0)
            ->get();

        $system = new System();

        $cache = Cache::where("ca_name", "=", "requests_monthly_chart")->first();
		$results = $system->unserialize($cache->ca_value);
		//dd($results);

		$labels = [];
		$request_bar_previous_year = [];
		$request_bar_current_year = [];

        //Loop through every month, check the amount of requests for current year and previous year
        for($monthcount = 11; $monthcount >= 0; $monthcount--)
        {
            //Set current date & previous year
            $currentDate = date("M Y", strtotime("today GMT+1 first day of -".$monthcount." Months"));
            $previousDate = date("M Y", strtotime("-1 year first day of -".$monthcount." Months"));

            $labels[] = date('M', strtotime($currentDate));
        }

        //Loop through every month, check the amount of requests for current year and previous year
		for($monthcount = 11; $monthcount >= 0; $monthcount--)
		{
			//Set current date & previous year
			$currentDate = date("M Y", strtotime("today GMT+1 first day of -".$monthcount." Months"));
			$previousDate = date("M Y", strtotime("-1 year first day of -".$monthcount." Months"));

			//$labels[] = date('M', strtotime($currentDate));

			if (empty($results[date('M Y', strtotime($previousDate))]['current']))
			{
				$request_bar_previous_year[] = 0;
			}
			else
			{
				$request_bar_previous_year[] = $results[date('M Y', strtotime($previousDate))]['current'];
			}

			if (empty($results[date('M Y', strtotime($currentDate))]['current']))
			{
				$request_bar_current_year[] = 0;
			}
			else
			{
				$request_bar_current_year[] = $results[date('M Y', strtotime($currentDate))]['current'];
			}
		}

        $not_operational_previous_year = [];
        $not_operational_current_year = [];

        //Loop through every month, check the count of bankrupt or not operational customers for current year and previous year
		for($monthcount = 11; $monthcount >= 0; $monthcount--)
		{
			//Set current date & previous year
            $currentDate = date("Y-m", strtotime("today GMT+1 first day of -".$monthcount." Months"));
            $previousDate = date("Y-m", strtotime("-1 year first day of -".$monthcount." Months"));

            //Previous year
            $moverdata = MoverData::selectRaw("count(*) as `count`")
                ->where("moda_not_operational", 1)
                ->whereRaw("moda_not_operational_timestamp LIKE '".$previousDate."%'")
                ->first();

            if ($moverdata->count > 0)
            {
                $not_operational_previous_year[] = $moverdata->count;
            }
            else {
                $not_operational_previous_year[] = 0;
            }

            //Current year
            $moverdata = MoverData::selectRaw("count(*) as `count`")
                ->where("moda_not_operational", 1)
                ->whereRaw("moda_not_operational_timestamp LIKE '".$currentDate."%'")
                ->first();

            if ($moverdata->count > 0)
            {
                $not_operational_current_year[] = $moverdata->count;
            }
            else {
                $not_operational_current_year[] = 0;
            }
		}

        $data = [
            'requests' => [$requests->open,$requests->matched,$requests->rejected],
            'birthdays' => $birthdays,
			'labels' => $labels,
			'request_bar_previous_year' => $request_bar_previous_year,
            'request_bar_current_year' => $request_bar_current_year,
            'not_operational_previous_year' => $not_operational_previous_year,
            'not_operational_current_year' => $not_operational_current_year,
            'dashboard_timestamp' => Cache::select("ca_timestamp")->where("ca_name", "requests_monthly_chart")->first()->ca_timestamp
        ];

        return view('dashboard', compact( 'data' ) );
    }
}
