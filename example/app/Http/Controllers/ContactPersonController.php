<?php

namespace App\Http\Controllers;

use App\Functions\Mail;
use App\Functions\System;
use App\Models\ApplicationUser;
use App\Data\ContactPersonDepartment;
use App\Data\ContactPersonDepartmentRole;
use App\Models\ContactPerson;
use App\Models\Customer;
use App\Data\ContactPersonFunction;
use App\Models\Language;
use App\Models\MoverPortalLoginAttempt;
use App\Models\UserLogin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;


class ContactPersonController extends Controller
{
	public function edit($customer_id, $contact_person_id)
	{
		//Get customer and through the relation of the customer, get the correct contact person
		$contact_person = ContactPerson::with(['application_user', 'customer'])->findOrFail($contact_person_id);

        $customer = Customer::where("cu_id", $contact_person->cope_cu_id)->first();

        if (Gate::denies('customer-restrictions', $customer)) {
            abort(403);
        }

		$system = new System();

		$ips = explode(",", System::getSetting("own_ip_addresses"));

		$user_logins = UserLogin::where("uslo_apus_id", "=", $contact_person->cope_apus_id)
			->whereRaw("`uslo_ip_address` NOT IN(".$system->escapedImplode($ips, ",").")")
			->orderBy("uslo_timestamp", "desc")
			->get();

		return view('contactperson.edit',
			[
				'contactperson' => $contact_person,
				'contactpersonfunctions' => ContactPersonFunction::all(),
				'contactpersondepartments' => ContactPersonDepartment::all(),
				'contactpersondepartmentroles' => ContactPersonDepartmentRole::all(),
				'languages' => Language::where("la_iframe_only", 0)->get(),
				'userlogins' => $user_logins
			]);
	}

	public function show($customer_id, $contact_person_id)
	{
		//Get customer and through the relation of the customer, get the correct contact person
		$customer = Customer::findOrFail($customer_id);
		$contactperson = $customer->contactpersons()->findOrFail($contact_person_id);

		return view('contactperson.show',
			[
				'contactperson' => $contactperson,
				'contactpersonfunctions' => ContactPersonFunction::all(),
				'customer' => $customer,
				'languages' => Language::where("la_iframe_only", 0)->get()
			]);


	}

	public function update(Request $request, $cope_id)
	{
		/*$email_checker = ContactPerson::where("cope_email", "=", $request->email)->where("cope_deleted", "=", "0")->where("cope_id", "!=", $cope_id)->first();

		if (!empty($email_checker))
		{
			return redirect()->back()->withErrors([ 'email'=> 'Email already exists for an active contact person.']);
		}*/

        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
        ]);

		$contact_person = ContactPerson::find($cope_id);

		$contact_person->cope_title = $request->title;
		$contact_person->cope_full_name = $request->first_name." ".$request->last_name;
		$contact_person->cope_first_name = $request->first_name;
		$contact_person->cope_last_name = $request->last_name;
		$contact_person->cope_department = $request->department;
		$contact_person->cope_department_role = $request->department_role;
		$contact_person->cope_language = $request->language;
		$contact_person->cope_email = $request->email;
		$contact_person->cope_telephone = ($request->int_telephone != 0 ? $request->int_telephone : null);
		$contact_person->cope_mobile = ($request->int_mobile != 0 ? $request->int_mobile : null);
		$contact_person->cope_remark = $request->remark;

		$contact_person->save();

		if(!empty($contact_person->cope_apus_id))
        {
            $apus = ApplicationUser::findOrFail($contact_person->cope_apus_id);
            $apus->apus_name = $contact_person->cope_full_name;
            $apus->save();
        }


		return redirect()->back();
	}

	public function create($customer_id)
	{
		$customer = Customer::findOrFail($customer_id);

		return view('contactperson.create',
			[
				'customer' => $customer,
				'contactpersondepartments' => ContactPersonDepartment::all(),
				'contactpersondepartmentroles' => ContactPersonDepartmentRole::all(),
				'languages' => Language::where("la_iframe_only", 0)->get()
			]);
	}

	public function store(Request $request)
	{
		$request->validate([
			'first_name' => 'required',
			'last_name' => 'required',
			'email' => 'required'
		]);

		$email_checker = ContactPerson::where("cope_email", "=", $request->email)->where("cope_deleted", "=", "0")->first();

		if (!empty($email_checker))
		{
			return redirect()->back()->withErrors([ 'email'=> 'Email already exists for an active contact person.']);
		}

		// Validate the request...
		$customer = Customer::findOrFail($request->customer_id);

		//Create new contact person
		$contactperson = new ContactPerson();

		//Set correct values
		$contactperson->cope_title = $request->title;
		$contactperson->cope_full_name = $request->first_name." ".$request->last_name;
		$contactperson->cope_first_name = $request->first_name;
		$contactperson->cope_last_name = $request->last_name;
		$contactperson->cope_email = $request->email;
		$contactperson->cope_telephone = ($request->int_telephone != 0 ? $request->int_telephone : null);
		$contactperson->cope_mobile = ($request->int_mobile != 0 ? $request->int_mobile : null);
		$contactperson->cope_department = $request->department;
		$contactperson->cope_department_role = $request->department_role;
		$contactperson->cope_remark = $request->remark;
		$contactperson->cope_language = $request->language;

		//Set to correct customer and save
		$contactperson->customer()->associate($customer);
		$contactperson->save();

		if ($customer->cu_type == 1)
		{
			return redirect('customers/' . $customer->cu_id . "/edit")->with('message', 'Contact person '.$request->full_name.' successfully added!');
		}
		elseif ($customer->cu_type == 6)
		{
			return redirect('resellers/' . $customer->cu_id . "/edit")->with('message', 'Contact person '.$request->full_name.' successfully added!');
		}
		elseif ($customer->cu_type == 2)
		{
			return redirect('serviceproviders/' . $customer->cu_id . "/edit")->with('message', 'Contact person '.$request->full_name.' successfully added!');
		}
		else
		{
			return redirect('affiliatepartners/' . $customer->cu_id . "/edit")->with('message', 'Contact person '.$request->full_name.' successfully added!');
		}
	}

	public function delete(Request $request)
	{
		//Get the contactperson
		$contact_person = ContactPerson::findOrFail($request->id);

		//Soft delete this user when confirmed (cu_deleted = 1)
		$contact_person->cope_deleted = 1;

		//Save the contact person
		$contact_person->save();

		if (!empty($contact_person->cope_apus_id))
		{
			//Search for an application user for this contact person and set apus_deleted 1.
			$application_user = ApplicationUser::find($contact_person->cope_apus_id);
			$application_user->apus_deleted = 1;

			//Save application user
			$application_user->save();
		}
	}

	public function generatePassword($customer_id, $contactperson_id)
	{
		$contact_person = ContactPerson::with("application_user")->find($contactperson_id);

		return view('contactperson.generate_password',
			[
				"contactperson" => $contact_person
			]);
	}

	public function generateNewPassword(Request $request, $customer_id, $contactperson_id)
	{
		$contact_person = ContactPerson::with("application_user", "customer")->find($contactperson_id);
		$application_user = ApplicationUser::findOrFail($contact_person->cope_apus_id);
		$mail = new Mail();

		//Create a random password
		$random_password = substr(md5(mt_rand()), 0, 8);

		$application_user->apus_password = md5(md5($random_password."!R@3#l%0^4&u*1(6)"));

		$application_user->save();

		//Start collecting right info for the email
		if ($contact_person->customer->cu_type == 1 || $contact_person->customer->cu_type == 6)
		{
			$link = "https://mover.triglobal.info/";
		}
		elseif($contact_person->customer->cu_type == 2)
		{
			$link = "https://service-providers.triglobal.info/";
		}
		else{
			$link = "https://affiliate-partners.triglobal.info/";
		}

		$fields = [
			"email" => $contact_person->application_user->apus_email,
			"password" => $random_password,
			"link" => $link,
			"from" => "erp"
		];

		//Sending mail
		$forgot_pass_result = $mail->send("forgot_password", $contact_person->cope_language, $fields);

		Log::debug($forgot_pass_result);

		$customer = Customer::findOrFail($contact_person->cope_cu_id);

		if ($customer->cu_type == 1)
		{
			return redirect('customers/' . $contact_person->cope_cu_id .'/contactperson/'.$contact_person->cope_id.'/edit');
		}
		elseif ($customer->cu_type == 6)
		{
			return redirect('resellers/' . $contact_person->cope_cu_id .'/contactperson/'.$contact_person->cope_id.'/edit');
		}
		elseif ($customer->cu_type == 2)
		{
			return redirect('serviceproviders/' . $contact_person->cope_cu_id .'/contactperson/'.$contact_person->cope_id.'/edit');
		}
		else
		{
			return redirect('affiliatepartners/' . $contact_person->cope_cu_id .'/contactperson/'.$contact_person->cope_id.'/edit');
		}
	}

	public function unblockUserConfirm($customer_id, $contactperson_id, $apus_id)
	{
		$contact_person = ContactPerson::with("application_user", "customer")->find($contactperson_id);

		return view('contactperson.unblock_confirmation', ['contact_person' => $contact_person]);
	}

	public function unblockUser(Request $request, $contactperson_id, $apus_id)
	{
		$contact_person = ContactPerson::with("application_user", "customer")->find($request->cope_id);
		$application_user = ApplicationUser::findOrFail($request->apus_id);

		$application_user->apus_blocked = 0;

		$application_user->save();

		$login_attempts = MoverPortalLoginAttempt::where("mopoloat_cope_id", "=", $contact_person->cope_id)->get();

		foreach ($login_attempts as $attempt)
		{
			$attempt->mopoloat_deleted = 1;
			$attempt->save();
		}

		if ($contact_person->customer->cu_type == 1)
		{
			return redirect('customers/' . $contact_person->cope_cu_id .'/contactperson/'.$contact_person->cope_id.'/edit');
		}
		elseif ($contact_person->customer->cu_type == 6)
		{
			return redirect('resellers/' . $contact_person->cope_cu_id .'/contactperson/'.$contact_person->cope_id.'/edit');
		}
		elseif ($contact_person->customer->cu_type == 2)
		{
			return redirect('serviceproviders/' . $contact_person->cope_cu_id .'/contactperson/'.$contact_person->cope_id.'/edit');
		}
		else
		{
			return redirect('affiliatepartners/' . $contact_person->cope_cu_id .'/contactperson/'.$contact_person->cope_id.'/edit');
		}
	}
}
