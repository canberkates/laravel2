<?php

namespace App\Http\Controllers;

use App\Data\CustomerRemarkDepartment;
use App\Data\CustomerRemarkDirection;
use App\Data\CustomerRemarkMedium;
use App\Data\PlannedCallReason;
use App\Data\PlannedCallStatus;
use App\Data\PlannedCallType;
use App\Functions\Data;
use App\Functions\System;
use App\Models\Country;
use App\Models\Customer;
use App\Models\CustomerRemark;
use App\Models\CustomerStatus;
use App\Models\PauseHistory;
use App\Models\PlannedCall;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;

class CalendarController extends Controller
{
    public function index()
    {

        $open_calls = PlannedCall::with("customer")->where("plca_us_id", Auth::id())->where("plca_status", 0)->whereDate("plca_planned_date", "<=", Carbon::now()->toDateString())->get();
        $future_calls = PlannedCall::with("customer")->where("plca_us_id", Auth::id())->where("plca_status", 0)->whereDate("plca_planned_date", ">", Carbon::now()->toDateString())->get();
        $processed_calls = PlannedCall::with("customer")->where("plca_us_id", Auth::id())->where("plca_status","!=", 0)->get();

        return view('calendar.index',
            [
                'open_calls' => $open_calls,
                'future_calls' => $future_calls,
                'processed_calls' => $processed_calls,
                'planned_call_reasons' => PlannedCallReason::all(),
                'planned_call_status' => PlannedCallStatus::all(),
                'planned_call_types' => PlannedCallType::all()
            ]);
    }

    public function allCalendars()
    {
        $open_calls = PlannedCall::with(["customer", "user"])->where("plca_status", 0)->whereDate("plca_planned_date", "<=", Carbon::now()->toDateString())->get();
        $future_calls = PlannedCall::with(["customer", "user"])->where("plca_status", 0)->whereDate("plca_planned_date", ">", Carbon::now()->toDateString())->get();
        $processed_calls = PlannedCall::with(["customer", "user"])->where("plca_status","!=", 0)->get();

        return view('calendar.allcalendars',
            [
                'open_calls' => $open_calls,
                'future_calls' => $future_calls,
                'processed_calls' => $processed_calls,
                'planned_call_reasons' => PlannedCallReason::all(),
                'planned_call_status' => PlannedCallStatus::all(),
                'planned_call_types' => PlannedCallType::all()
            ]);
    }

    public function planCalls()
    {
        $active_customers = Data::activeCustomers();
        $customers = Customer::with("moverdata")->whereIn("cu_id", array_keys($active_customers))->orderByRaw("RAND()")->limit(50)->get();

        foreach($customers as $key => $customer){
            $calls = PlannedCall::where("plca_cu_id", $customer->cu_id)->where("plca_status", 0)->get();
            if($calls->count()){
                unset($customers[$key]);
            }
        }

        foreach(Country::all() as $country){
            $countries[$country->co_code] = $country->co_en;
        }

        return view('calendar.plancalls',
            [
                'customers' => $customers,
                'countries' => $countries,
                'employees' => User::where('us_is_deleted', '=', 0)->orderBy('us_name', 'ASC')->get(),
                'planned_call_status' => PlannedCallStatus::all()
            ]);
    }

    public function edit($call_id)
    {
        $call = PlannedCall::with("customer")->where("plca_id", $call_id)->first();

        $users = [];
        $users_query = User::where("id", "!=", 0)->where("us_is_deleted", 0)->get();
        foreach($users_query as $us)
        {
            $users[$us->id] = $us->us_name;
        }

        return view('calendar.edit', [
            'call' => $call,
            'users' => $users,
            'planned_call_reason' => PlannedCallReason::name($call->plca_reason),
            'statuses' => PlannedCallStatus::all()
        ]);
    }

    public function update(Request $request, $call_id)
    {
        $request->validate([
            'call_datetime' => 'required',
            'employee' => 'required',
            'status' => 'required',
            'call_remark' => 'required',
            'person_spoken_to' => 'required'
        ]);

        //Get call & update info
        $call = PlannedCall::findOrFail($call_id);

        $call->plca_status = $request->status;
        $call->plca_handled_timestamp = $request->call_datetime;
        $call->plca_us_id = $request->employee;
        $call->plca_comment = $request->call_remark;
        $call->plca_person_spoken_to = $request->person_spoken_to;

        $call->save();

        $redirect = 'back';

        if($request->status == PlannedCallStatus::RESCHEDULED){
            $new_call = new PlannedCall();

            $new_call->plca_cu_id = $call->plca_cu_id;
            $new_call->plca_us_id = $request->employee;
            $new_call->plca_status = PlannedCallStatus::OPEN;
            $new_call->plca_reason = $call->plca_reason;
            $new_call->plca_reason_extra = $call->plca_reason_extra;
            $new_call->plca_planned_date = $request->rescheduled_call_datetime;

            $new_call->save();
        }elseif($request->status == PlannedCallStatus::COMPLETED){

            //Set at title to the remark
            $remark_with_title = "<span id='remark_title' style='font-weight: bold'>".PlannedCallReason::name($call->plca_reason)."</span><br><br>".$request->call_remark;

            $remark = new CustomerRemark();

            $remark->cure_cu_id = $call->plca_cu_id;
            $remark->cure_timestamp = Carbon::now()->toDateTimeString();
            $remark->cure_contact_date = Carbon::now();
            $remark->cure_department = CustomerRemarkDepartment::SALES;
            $remark->cure_employee = $request->employee;
            $remark->cure_medium = CustomerRemarkMedium::PHONE;
            $remark->cure_direction = CustomerRemarkDirection::OUTGOING;
            $remark->cure_text = $remark_with_title;

            $remark->save();

            $call->plca_cure_id = $remark->cure_id;

            $call->save();

            $redirect = 'new_call';

            //If just completed a 6 month follow up
            if($call->plca_reason == PlannedCallReason::SIX_MONTHS_FOLLOW_UP || $call->plca_reason == PlannedCallReason::NINETY_DAY_CHECK){
                $new_call = new PlannedCall();

                $new_call->plca_cu_id = $call->plca_cu_id;
                $new_call->plca_us_id = $request->employee;
                $new_call->plca_status = PlannedCallStatus::OPEN;
                $new_call->plca_reason = PlannedCallReason::SIX_MONTHS_FOLLOW_UP;
                $new_call->plca_reason_extra = $call->plca_reason_extra;

                $expiry_date = new DateTime($request->call_datetime);
                $expiry_date->modify("+6 months");
                $expiry_date->format("Y-m-d H:m:s");

                $new_call->plca_planned_date = $expiry_date;

                $new_call->save();

                $redirect = 'call_planned';
            }
        }


        if($redirect == 'back'){
            return redirect('/calendar')->with('message', 'Call has successfully been processed!');
        }elseif ($redirect == 'new_call'){
            return redirect('/customers/'.$call->plca_cu_id.'/customerplannedcall/create')->with('message', 'Call has successfully been processed! You can plan a new call here.');
        }elseif ($redirect == 'call_planned'){
            return redirect('/calendar')->with('message', 'Call has successfully been processed! An automatic call was made 6 months in the future.');
        }
        return redirect('/calendar');
    }


}
