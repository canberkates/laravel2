<?php

namespace App\Http\Controllers;

use App\Data\ClaimReason;
use App\Data\CustomerMarketType;
use App\Data\CustomerProgressLostContractType;
use App\Data\CustomerProgressType;
use App\Data\CustomerRestrictions;
use App\Data\CustomerServices;
use App\Data\CustomerServiceType;
use App\Data\CustomerStatusType;
use App\Data\Device;
use App\Data\MovingSize;
use App\Data\PlatformSource;
use App\Data\RejectionReason;
use App\Data\RequestCustomerPortalType;
use App\Data\RequestStatus;
use App\Data\RequestType;
use App\Data\YesNo;
use App\Functions\RequestData;
use App\Models\AffiliatePartnerForm;
use App\Models\Country;
use App\Models\Customer;
use App\Models\Membership;
use App\Models\PlannedCall;
use App\Models\Region;
use App\Models\User;
use App\Models\WebsiteForm;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Functions\System;
use Log;


class CustomerSearchController extends Controller
{
    public function search()
    {
        return view('customersearch.search',
            [
                "countries" => Country::all(),
                "associations" => Membership::all(),
                "progresstypes" => CustomerProgressType::all(),
                "customermarkettypes" => CustomerMarketType::all(),
                "customerservices" => CustomerServices::all(),
                "customerstatustypes" => CustomerStatusType::all(),
                "customerservicetypes" => CustomerServiceType::all(),
                'regions' => Region::join("countries", "reg_co_code", "co_code")->where("reg_destination_type", 1)->where("reg_deleted", 0)->groupBy("reg_co_code", "reg_parent")->get(),
                'users' => User::where("id", "!=", 0)->orderBy("us_name")->get(),
                'selected_progress' => -1
            ]);
    }

    public function searchResult(Request $request)
    {
        $customer_query = Customer::join("mover_data", "moda_cu_id", "cu_id")
            ->join("countries", "co_code", "cu_co_code");

        if (!empty($request->countries))
        {

            $customer_query->whereIn('cu_co_code', $request->countries);
        }

        if (!empty($request->regions))
        {
            $customer_query->whereIn('moda_reg_id', $request->regions);
        }

        if (!empty($request->market))
        {
            $customer_query->where('moda_market_type', $request->market);
        }

        if (!empty($request->services))
        {
            $customer_query->whereIn('moda_services', $request->services);
        }

        if (!empty($request->statuses))
        {
            $customer_query->whereIn('moda_crm_status', $request->statuses);
        }

        if (!empty($request->owners))
        {
            $customer_query->whereIn('moda_owner', $request->owners);
        }

        //Check if this user got restrictions
        $customer_restrictions = CustomerRestrictions::all()[Auth::user()->us_id];

        if (!empty($customer_restrictions)) {
            //User has some restrictions
            $customer_query->whereIn("cu_co_code", $customer_restrictions);
        }

        $customers = $customer_query->where("cu_deleted", 0)->get();

        $date_filter = $request->date;

        //Unset customers if last call date after picked date
        if (!empty($request->call_date_before))
        {
            foreach ($customers as $key => $customer)
            {
                $customer_query = DB::table("planned_calls")
                    ->where("plca_cu_id", $customer->cu_id)
                    ->whereDate("plca_planned_date", "<", $request->call_date_before)
                    ->get();

                if (!$customer_query->count())
                {
                    $customers->forget($key);
                }
            }
        }

        //Unset customers if associations arent set
        if (!empty($request->associations))
        {
            foreach ($customers as $key => $customer)
            {
                $customer_query = DB::table("kt_customer_membership")
                    ->where("ktcume_cu_id", $customer->cu_id)
                    ->whereIn("ktcume_me_id", $request->associations)
                    ->get();

                if (!$customer_query->count())
                {
                    $customers->forget($key);
                }
            }
        }

        //Unset customers if progress arent set
        if (!empty($request->progress))
        {
            foreach ($customers as $key => $customer)
            {
                $progress_query = DB::table("kt_customer_progress")
                    ->where("ktcupr_cu_id", $customer->cu_id)
                    ->where("ktcupr_type", $request->progress)
                    ->where("ktcupr_deleted", 0)
                    ->whereBetween("ktcupr_timestamp", System::betweenDates($date_filter))
                    ->get();

                if (!$progress_query->count())
                {
                    $customers->forget($key);
                    continue;
                }else{
                    $customer->progress_timestamp = $progress_query[0]->ktcupr_timestamp;
                }

                //If customer has any 'higher' progress, unset them
                $higher = DB::table("kt_customer_progress")
                    ->where("ktcupr_cu_id", $customer->cu_id)
                    ->where("ktcupr_type", ">", $request->progress)
                    ->where("ktcupr_deleted", 0)
                    ->whereBetween("ktcupr_timestamp", System::betweenDates($date_filter))
                    ->get();

                if ($higher->count())
                {
                    $customers->forget($key);
                }
            }
        }elseif($request->progress === "0"){
            foreach ($customers as $key => $customer)
            {
                $progress_query = DB::table("kt_customer_progress")
                    ->where("ktcupr_cu_id", $customer->cu_id)
                    ->where("ktcupr_deleted", 0)
                    ->whereBetween("ktcupr_timestamp", System::betweenDates($date_filter))
                    ->get();

                if ($progress_query->count())
                {
                    $customers->forget($key);
                }else{
                    $customer->progress_timestamp = $progress_query[0]->ktcupr_timestamp;
                }
            }
        }

        //Get last and next planned call
        foreach($customers as $customer){
            $last_call = PlannedCall::where("plca_cu_id", $customer->cu_id)->whereDate("plca_planned_date", "<=", Carbon::now()->toDateString())->first()->plca_planned_date;
            $next_call = PlannedCall::where("plca_cu_id", $customer->cu_id)->whereDate("plca_planned_date", ">=", Carbon::now()->toDateString())->first()->plca_planned_date;

            $customer->last_call = $last_call;
            $customer->next_call = $next_call;

        }

        return view('customersearch.search',
            [
                //To fill the dropdowns
                "countries" => Country::all(),
                "associations" => Membership::all(),
                "progresstypes" => CustomerProgressType::all(),
                "customermarkettypes" => CustomerMarketType::all(),
                "customerservices" => CustomerServices::all(),
                "customerstatustypes" => CustomerStatusType::all(),
                "customerservicetypes" => CustomerServiceType::all(),
                'regions' => Region::join("countries", "reg_co_code", "co_code")->where("reg_destination_type", 1)->where("reg_deleted", 0)->groupby("reg_parent")->get(),
                'users' => User::where("id", "!=", 0)->orderBy("us_name")->get(),

                //To fill dropdowns with selected values
                "selected_countries" => $request->countries,
                "selected_regions" => $request->regions,
                "selected_market" => $request->market,
                "selected_services" => $request->services,
                "selected_statuses" => $request->statuses,
                "selected_owners" => $request->owners,
                "selected_progress" => $request->progress ?? -1,
                "selected_associations" => $request->associations,
                "selected_call_before_date" => $request->call_date_before,
                "date_filter" => $date_filter,
                //To display the result
                'customers' => $customers
            ]);
    }
}
