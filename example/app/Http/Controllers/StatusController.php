<?php

namespace App\Http\Controllers;



use App\Models\KTRequestCustomerPortal;
use App\Models\Request;
use App\Models\Website;
use App\Models\WebsiteForm;
use DB;
use Log;

class StatusController extends Controller
{
    public function index()
    {
        return view('status.index', [
            "websites" =>  Website::select("we_website", "we_id")->get()
        ]);
    }

    public function ajaxFunction()
    {

        $timestamps = Request::select(DB::raw("we_website, MAX(re_timestamp) as last_timestamp"))
            ->leftJoin("website_forms", "re_wefo_id", "wefo_id")
            ->leftJoin("websites", "wefo_we_id", "we_id")
            ->whereNotNull("re_wefo_id")
            ->groupBy("we_id")
            ->get();

        $array['data'] = [];

        foreach($timestamps as $timestamp){
            $row['Website'] = $timestamp->we_website;
            $row['Last lead'] = $timestamp->last_timestamp;

            array_push($array['data'], $row);
        }

        return json_encode($array);

    }

    public function lastAffiliateLeads()
    {

        $timestamps = Request::select(DB::raw("cu_company_name_business, MAX(re_timestamp) as last_timestamp"))
            ->leftJoin("affiliate_partner_forms", "re_afpafo_id", "afpafo_id")
            ->leftJoin("customers", "cu_id", "afpafo_cu_id")
            ->whereNotNull("re_afpafo_id")
            ->where("afpafo_deleted", 0)
            ->groupBy("cu_company_name_business")
            ->get();

        $array['data'] = [];

        foreach($timestamps as $timestamp){
            $row['Affiliate'] = $timestamp->cu_company_name_business;
            $row['Last lead'] = $timestamp->last_timestamp;

            array_push($array['data'], $row);
        }

        return json_encode($array);

    }

    public function getLastLead()
    {
        $last_own_received = Request::select("re_timestamp", "we_website")
            ->leftJoin("website_forms", "re_wefo_id", "wefo_id")
            ->leftJoin("websites", "wefo_we_id", "we_id")
            ->where("re_source", 1)
            ->where("we_sirelo", 0)
            ->orderBy("re_timestamp", "desc")
            ->first();

        $last_sirelo_received = Request::select("re_timestamp", "we_website")
            ->leftJoin("website_forms", "re_wefo_id", "wefo_id")
            ->leftJoin("websites", "wefo_we_id", "we_id")
            ->where("re_source", 1)
            ->where("we_sirelo", 1)
            ->orderBy("re_timestamp", "desc")
            ->first();

        $last_affiliate_received = Request::select("re_timestamp", "afpafo_name")
            ->leftJoin("affiliate_partner_forms", "re_afpafo_id", "afpafo_id")
            ->where("re_source", 2)
            ->orderBy("re_timestamp", "desc")
            ->first();

        $last_matched = KTRequestCustomerPortal::select("ktrecupo_timestamp", "us_name")
            ->leftJoin("users", "us_id", "ktrecupo_us_id")
            ->orderBy("ktrecupo_timestamp", "desc")
            ->first();

        $data['last_own'] = $last_own_received;
        $data['last_sirelo'] = $last_sirelo_received;
        $data['last_affiliate'] = $last_affiliate_received;
        $data['last_matched'] = $last_matched;


        return json_encode($data);

    }

}
