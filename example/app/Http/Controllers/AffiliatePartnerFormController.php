<?php

namespace App\Http\Controllers;

use App\Data\AffiliateFormType;
use App\Data\VolumeType;
use App\Functions\Mover;
use App\Models\AffiliatePartnerForm;
use App\Models\Country;
use App\Models\Customer;
use App\Models\Language;
use App\Models\MoverFormSettings;
use App\Models\Portal;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class AffiliatePartnerFormController extends Controller
{
	public function edit($customer_id, $form_id)
	{
		//Get customer and through the relation of the customer
		$affiliateform = AffiliatePartnerForm::with(['customer'])->findOrFail($form_id);

        $mover_form = MoverFormSettings::where("mofose_afpafo_id", $affiliateform->afpafo_id)->first();

        $customer = Customer::where("cu_id", $affiliateform->afpafo_cu_id)->first();

        if (Gate::denies('customer-restrictions', $customer)) {
            abort(403);
        }

		return view('affiliateform.edit',
			[
				'affiliateform' => $affiliateform,
				'mover_form' => $mover_form,
				'countries' => Country::all(),
				'formtypes' => AffiliateFormType::all(),
				'volumetypes' => VolumeType::all(),
				'languages' => Language::all(),
				'portals' => Portal::with("country")->get(),
				'countries' => Country::all()
			]);
	}

	public function update(Request $request, $form_id)
	{
		$request->validate([
			'name' => 'required',
			'form_type' => 'required',
			'language' => 'required',
			'volume_type' => 'required'
		]);

		//Find the form
		$affiliate_form = AffiliatePartnerForm::find($form_id);

		$affiliate_form->afpafo_name = $request->name;
		$affiliate_form->afpafo_po_id = $request->portal;
		$affiliate_form->afpafo_form_type = $request->form_type;
		$affiliate_form->afpafo_disable_ip_check = ((isset($request->disable_ip_check)) ? 1 : 0);
		$affiliate_form->afpafo_la_code = $request->language;
		$affiliate_form->afpafo_volume_type = $request->volume_type;
		$affiliate_form->afpafo_hide_pre_form = ((isset($request->hide_pre_form)) ? 1 : 0);
		$affiliate_form->afpafo_hide_questions = ((isset($request->hide_questions)) ? 1 : 0);
		$affiliate_form->afpafo_custom_css = $request->custom_css;
		$affiliate_form->afpafo_analytics_ua_code = $request->analytics_ua_code;
        $affiliate_form->afpafo_conversion_code = $request->conversion_code;
        $affiliate_form->afpafo_tracking_clientid = ((isset($request->tracking_clientid)) ? 1 : 0);
        $affiliate_form->afpafo_tracking_domains = $request->tracking_domains;



        //Also create Mover Form Settings (With Affiliate defauls)
        if($request->form_type == AffiliateFormType::CONVERSION_TOOLS){

            $mover_form = MoverFormSettings::where("mofose_afpafo_id", $affiliate_form->afpafo_id)->first();

            if($mover_form){

                $mover_form->mofose_title = $request->form_title;
                $mover_form->mofose_subtext = $request->form_subtitle;
                $mover_form->mofose_theme_color = (!empty($request->color) && str_contains($request->color, "#")) ? $request->color : "#009fe3";
                $mover_form->save();

            }else {
                $mover_form = new MoverFormSettings();

                $mover_form->mofose_cu_id = $customer->cu_id;
                $mover_form->mofose_afpafo_id = $affiliate_form->afpafo_id;
                $mover_form->mofose_theme_color = "#009fe3";
                $mover_form->mofose_show_vc = 1;
                $mover_form->mofose_email_from = $customer->cu_email;
                $mover_form->mofose_email_to = $customer->cu_email;
                $mover_form->mofose_title = $request->form_title;
                $mover_form->mofose_subtext = $request->form_subtitle;
                $mover_form->mofose_show_moving_sizes = 1;

                $mover_form->save();
            }
        }

		//Save fields
		$affiliate_form->save();

		//Redirect to affiliate partners edit
		return redirect('affiliatepartners/' . $request->customer_id . '/edit');
	}

	public function create($customer_id)
	{
		$customer = Customer::findOrFail($customer_id);

		return view('affiliateform.create',
			[
				'customer' => $customer,
				'countries' => Country::all(),
				'formtypes' => AffiliateFormType::all(),
				'volumetypes' => VolumeType::all(),
				'languages' => Language::all(),
				'portals' => Portal::with("country")->get()
			]);
	}

	public function store(Request $request)
	{
		$request->validate([
			'name' => 'required',
			'form_type' => 'required',
			'language' => 'required',
			'volume_type' => 'required'
		]);

		// Validate the request...
		$customer = Customer::findOrFail($request->customer_id);

		//Create new form
		$affiliate_form = new AffiliatePartnerForm();

		$affiliate_form->afpafo_token = md5(time());
		$affiliate_form->afpafo_name = $request->name;
		$affiliate_form->afpafo_po_id = $request->portal;
		$affiliate_form->afpafo_form_type = $request->form_type;
		$affiliate_form->afpafo_disable_ip_check = ((isset($request->disable_ip_check)) ? 1 : 0);
		$affiliate_form->afpafo_la_code = $request->language;
		$affiliate_form->afpafo_volume_type = $request->volume_type;
		$affiliate_form->afpafo_hide_pre_form = ((isset($request->hide_pre_form)) ? 1 : 0);
		$affiliate_form->afpafo_hide_questions = ((isset($request->hide_questions)) ? 1 : 0);
		$affiliate_form->afpafo_custom_css = $request->custom_css;
		$affiliate_form->afpafo_analytics_ua_code = $request->analytics_ua_code;
		$affiliate_form->afpafo_conversion_code = $request->conversion_code;

        //Set to correct customer and save
        $affiliate_form->customer()->associate($customer);

        $affiliate_form->save();

		//Also create Mover Form Settings (With Affiliate defauls)
		if($request->form_type == AffiliateFormType::CONVERSION_TOOLS){
            $mover_form = new MoverFormSettings();

            $mover_form->mofose_cu_id = $customer->cu_id;
            $mover_form->mofose_afpafo_id = $affiliate_form->afpafo_id;
            $mover_form->mofose_theme_color = (!empty($request->color) && str_contains($request->color, "#")) ? $request->color : "#009fe3";
            $mover_form->mofose_show_vc = 1;
            $mover_form->mofose_email_from = $customer->cu_email;
            $mover_form->mofose_email_to = $customer->cu_email;
            $mover_form->mofose_title = $request->form_title;
            $mover_form->mofose_subtext = $request->form_subtitle;
            $mover_form->mofose_show_moving_sizes = 1;

            $mover_form->save();
        }




		return redirect('affiliatepartners/' . $request->customer_id .'/edit');
	}


	public function delete(Request $request)
	{
		$form = AffiliatePartnerForm::find($request->id);
		$form->afpafo_deleted = 1;
		$form->save();
	}
}
