<?php

namespace App\Http\Controllers;

use App\Data\DeliveryLead;
use App\Models\RequestDelivery;
use App\Providers\RequestDeliveryServiceProvider;
use App\Repository\CustomerPortalRepository;
use App\Repository\RequestCustomerPortalRepository;
use App\Repository\RequestRepository;
use App\RequestDeliveries\MoveWare;
use App\RequestDeliveries\RequestDeliverySender;

class CustomerRequestResendController extends Controller
{

    private $customerPortalRepository;
    private $requestRepository;
    private $requestDeliverySender;

    public function __construct(
        CustomerPortalRepository $customerPortalRepository,
        RequestRepository $requestRepository,
        RequestDeliverySender $requestDeliverySender
    ){
        $this->customerPortalRepository = $customerPortalRepository;
        $this->requestRepository = $requestRepository;
        $this->requestDeliverySender = $requestDeliverySender;
    }

    public function resendLead($ktrecupo_id){

        $customerPortal = $this->customerPortalRepository->findByReCuPoId($ktrecupo_id);
        $request = $this->requestRepository->findByReCuPoId($ktrecupo_id);
        $lead = DeliveryLead::createFromRequest($request);

        $requestDeliveries = $customerPortal->requestdeliveries;

        //Get fallback RequestDelivery in case of missing deliveries.
        if(!count($requestDeliveries)){
            $temp_rd = new RequestDelivery();
            $temp_rd->rede_type = 1;
            $temp_rd->rede_value = $customerPortal->customer->cu_email;
            $temp_rd->rede_ktcupo_id = $customerPortal->ktcupo_id;

            $requestDeliveries[] = $temp_rd;
        }

        foreach ($requestDeliveries as $requestDelivery){
            $this->requestDeliverySender->sendRequest($requestDelivery, $lead);
        }

        return redirect('customers/' . $customerPortal->customer->cu_id . '/edit')->with('message', "Lead {$ktrecupo_id} has been resent successfully");

    }
}
