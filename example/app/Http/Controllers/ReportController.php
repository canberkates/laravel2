<?php

namespace App\Http\Controllers;

use App\Data\AffiliateFormType;
use App\Data\AffiliatePartnerStatus;
use App\Data\AutomatedDecision;
use App\Data\BothYesNo;
use App\Data\CustomerMarketType;
use App\Data\CustomerProgressType;
use App\Data\CustomerSourceType;
use App\Data\RequestSource;
use Carbon\Carbon;

use App\Data\CustomerPairStatus;
use App\Data\CustomerRemarkDepartment;
use App\Data\CustomerRemarkDirection;
use App\Data\CustomerRemarkMedium;
use App\Data\CustomerServices;
use App\Data\CustomerStatusReason;
use App\Data\CustomerStatusStatus;
use App\Data\CustomerStatusType;
use App\Data\CustomerType;
use App\Data\DebtorStatus;
use App\Data\DestinationType;
use App\Data\MoverStatus;
use App\Data\MovingSize;
use App\Data\NewsletterSlot;
use App\Data\PaymentMethod;
use App\Data\RejectionReason;
use App\Data\RequestStatus;
use App\Data\RequestType;
use App\Data\ServiceProviderRejectionStatus;
use App\Models\KTCustomerProgress;
use App\Data\StatusHistoryType;
use App\Functions\AdyenTransaction;
use App\Functions\Cache;
use App\Functions\Data;
use App\Functions\DataIndex;
use App\Functions\Mover;
use App\Functions\SireloCustomer;
use App\Functions\System;
use App\Functions\Reporting;
use App\Models\Adyen;
use App\Models\AffiliatePartnerData;
use App\Models\AffiliatePartnerForm;
use App\Models\AffiliatePartnerFormStep;
use App\Models\AffiliatePartnerFormVisit;
use App\Models\AffiliatePartnerRequest;
use App\Models\AutomatedRequest;
use App\Models\ContactPerson;
use App\Models\Country;
use App\Models\Customer;
use App\Models\CustomerGatheredReview;
use App\Models\CustomerGDPR;
use App\Models\CustomerLoadExchange;
use App\Models\CustomerLoadReaction;
use App\Models\CustomerRemark;
use App\Models\CustomerReviewScore;
use App\Models\CustomerStatus;
use App\Models\FetchedGoogleHistory;
use App\Models\GatheredCompany;
use App\Models\Invoice;
use App\Models\KTBankLineInvoiceCustomerLedgerAccount;
use App\Models\KTCustomerMembership;
use App\Models\KTCustomerPortal;
use App\Models\KTCustomerQuestion;
use App\Models\KTRequestCustomerPortal;
use App\Models\KTRequestCustomerQuestion;
use App\Models\KTServiceProviderNewsletterBlockSent;
use App\Models\KTUserReport;
use App\Models\Language;
use App\Models\MacroRegion;
use App\Models\MatchStatistic;
use App\Models\Membership;
use App\Models\MobilityexFetchedData;
use App\Models\MoverRequest;
use App\Models\MoverWidgetVisit;
use App\Models\PaymentCurrency;
use App\Models\Portal;
use App\Models\QualityScoreCalculation;
use App\Models\Region;
use App\Models\Report;
use App\Models\ReportProgress;
use App\Models\ReportUsageHistory;
use App\Models\RequestUpdate;
use App\Models\ServiceProviderNewsletterBlock;
use App\Models\StatusHistory;
use App\Models\StatusUpdate;
use App\Models\Survey1Customer;
use App\Models\Survey2;
use App\Models\User;
use App\Models\UserLogin;
use App\Models\VolumeCalculator;
use App\Models\Website;
use App\Models\WebsiteForm;
use DateTime;
use DB;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Log;
use Venturecraft\Revisionable\Revision;
use function Symfony\Component\String\s;

class ReportController extends Controller
{
    public function index()
    {
        $reports = Report::all();

        $last_used_per_report = [];

        foreach ($reports as $rep) {
            $last_used = "Unknown";

            $last_used_query = ReportUsageHistory::select("reushi_timestamp")
                ->where("reushi_rep_id", $rep->rep_id)
                ->orderBy("reushi_timestamp", "DESC")
                ->first();

            if (count($last_used_query) > 0) {
                $last_used = $last_used_query->reushi_timestamp;
            }

            $last_used_per_report[$rep->rep_id] = $last_used;
        }

        $report_rights = [];
        foreach (KTUserReport::where("ktusre_us_id", Auth::id())->get() as $re_ri)
        {
            array_push($report_rights, $re_ri->ktusre_rep_id);
        }

        return view('reports.index', ['reports' => $reports, 'report_rights' => $report_rights, "last_used_per_report" => $last_used_per_report]);
    }

    public function view($rep_id)
    {
        $report = Report::findOrFail($rep_id);

        $rep_right = KTUserReport::where("ktusre_us_id", Auth::id())->where("ktusre_rep_id", $rep_id)->get();
        $rights = true;
        if (count($rep_right) == 0)
        {
            $rights = false;
        }

        if ($report->rep_report == "customer_languages")
        {
            return self::viewCustomerLanguages($rep_id);
        } elseif ($report->rep_report == "customer_portals")
        {
            return self::viewCustomerPortals($rep_id);
        } elseif ($report->rep_report == "missed_matches")
        {
            return self::viewMissedMatches($rep_id);

        } elseif ($report->rep_report == "python_process")
        {
            return self::viewPythonProcess($rep_id);

        }
        else
        {
            $times_used = self::getTimesUsedLast90Days($rep_id);
            $countries = Country::all();
            $request_source = RequestSource::all();
            $all_markets = CustomerMarketType::all();
            $all_services = CustomerServices::all();
            $all_statuses = CustomerStatusType::all();
            $all_memberships = Membership::all();
            $destination_type = DestinationType::all();
            $associations = \App\Models\Membership::all();
            $sirelos = Website::where("we_sirelo", 1)->orderBy("we_website", "ASC")->get();

            $websites = Website::all();


            return view('reports.' . $report->rep_report, [
                'report' => $report,
                'report_rights' => $rights,
                'sirelos' => $sirelos,
                'countries' => $countries,
                'markets' => $all_markets,
                'services' => $all_services,
                'statuses' => $all_statuses,
                'destination_type' => $destination_type,
                'all_memberships' => $all_memberships,
                'websites' => $websites,
                'request_source' => $request_source,
                'times_used' => $times_used,
                'associations' => $associations
            ]);
        }
    }

    public function viewWidgetVisits(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);

        $total_visits = 0;

        if ($request->date != null)
        {
            $date_filter = $request->date;
        } else
        {
            return Redirect::back()->withErrors(['Please select a date']);
        }

        $filtered_data = [];

        $widget_visits = MoverWidgetVisit::leftJoin("customers", "mowivi_cu_id", "cu_id")
            ->whereBetween("mowivi_date", System::betweenDates($request->date))
            ->where("cu_deleted", 0)
            ->orderBy("mowivi_date", "ASC")
            ->orderBy("cu_company_name_business", "ASC")
            ->get();

        foreach ($widget_visits as $row)
        {
            if (!isset($filtered_data[$row->mowivi_cu_id])) {
                $filtered_data[$row->mowivi_cu_id] = [
                    "company_name" => $row->cu_company_name_business,
                    "country" => Data::country($row->cu_co_code),
                    "visits" => 0,
                    "widget_url" => $row->mowivi_widget_url
                ];
            }

            $filtered_data[$row->mowivi_cu_id]['visits'] += $row->mowivi_visits;

            if (empty($filtered_data[$row->mowivi_cu_id]['widget_url']) && !empty($row->mowivi_widget_url)) {
                $filtered_data[$row->mowivi_cu_id]['widget_url'] = $row->mowivi_widget_url;
            }

            $total_visits += $row->mowivi_visits;
        }

        if (!empty($_POST)){
            self::insertReportUsageHistory($rep_id);
        }

        return view('reports.widget_visits', [
            'report' => $report,
            'date_filter' => $date_filter,
            'filtered_data' => $filtered_data,
            'total_visits' => $total_visits,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function viewAffiliatePartnerForms(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);

        $date_filter = null;
        $affiliate_partner_filter = null;
        $merge_iframes_filter = null;
        $total_visits = 0;
        $total_requests = 0;
        $total_revenues = 0;

        if ($request->date != null)
        {
            $date_filter = $request->date;
        } else
        {
            return Redirect::back()->withErrors(['Please select a date']);
        }

        if ($request->affiliate_partner != null)
        {
            $affiliate_partner_filter = $request->affiliate_partner;
        }

        if ($request->merge_iframe_data_per_partner != null)
        {
            $merge_iframes_filter = true;
        }


        $filtered_data = [];

        if ($merge_iframes_filter) {
            $query = AffiliatePartnerForm::leftJoin("customers", "afpafo_cu_id", "cu_id")
                ->leftJoin("affiliate_partner_data", "afpada_cu_id", "cu_id")
                ->leftJoin("portals", "afpafo_po_id", "po_id")
                ->whereIn("afpafo_form_type", [1,4])
                ->where("cu_deleted", 0);

            if ($affiliate_partner_filter != null)
            {
                $query = $query->where("afpafo_cu_id", $affiliate_partner_filter);
            }

            $query = $query->orderBy("cu_company_name_business", "asc")
                ->get();

            foreach ($query as $row)
            {
                $query_afpafo_visits = AffiliatePartnerFormVisit::where("afpafovi_afpafo_id", "=", $row->afpafo_id)
                    ->whereBetween("afpafovi_date", System::betweenDates($request->date))
                    ->get();

                $visits = 0;

                foreach ($query_afpafo_visits as $row_affiliate_partner_form_visits)
                {
                    $visits += $row_affiliate_partner_form_visits->afpafovi_visits;
                }

                $data = new DataIndex();

                $requests_count = 0;
                $revenues = 0;
                $re_ids = [];

                $requests_query = \App\Models\Request::select("re_id", "ktrecupo_free", "ktrecupo_free_percentage", "ktrecupo_amount_netto_eur")
                    ->leftJoin("kt_request_customer_portal", "re_id", "ktrecupo_re_id")
                    ->where("re_afpafo_id", "=", $row->afpafo_id)
                    ->whereBetween("re_timestamp", System::betweenDates($request->date))
                    ->get();

                foreach ($requests_query as $re) {
                    if (!in_array($re->re_id, $re_ids)){
                        $requests_count++;
                        $re_ids[] = $re->re_id;
                    }
                    if ($re->ktrecupo_free != 1 && $re->ktrecupo_free_percentage != 1)
                    {
                        $revenues += $re->ktrecupo_amount_netto_eur;
                    }
                }

                if (!array_key_exists($row->afpafo_cu_id, $filtered_data)){
                    $filtered_data[$row->afpafo_cu_id] = [
                        "cu_company_name_business" => $row->cu_company_name_business,
                        "afpada_status" => $row->afpada_status,
                        "visits" => 0,
                        "requests" => 0,
                        "revenues" => 0,
                        "conversion" => 0,
                        "revenue_per_visit" => 0
                    ];
                }

                $filtered_data[$row->afpafo_cu_id]['visits'] += $visits;
                $filtered_data[$row->afpafo_cu_id]['requests'] += $requests_count;
                $filtered_data[$row->afpafo_cu_id]['revenues'] += $revenues;

                $total_visits += $visits;
                $total_requests += $requests_count;
                $total_revenues += $revenues;
            }

            foreach ($filtered_data as $cu_id => $data) {
                $filtered_data[$cu_id]['revenues'] = "€ ".System::numberFormat($data['revenues'], 2, ",", ".");
                $filtered_data[$cu_id]['conversion'] = (($data['requests'] > 0) ? System::numberFormat(($data['requests'] / $data['visits']) * 100, 2, ",", ".")."%" : "0,00%");
                $filtered_data[$cu_id]['revenue_per_visit'] = "€ ".(($data['requests'] > 0) ? System::numberFormat($data['revenues'] / $data['visits'], 2, ",", ".") : "0,00");
            }

            $total_conversion = ($total_requests / $total_visits) * 100;
            $total_revenue_per_visit = $total_revenues / $total_visits;
        }
        else {
            $query = AffiliatePartnerForm::leftJoin("customers", "afpafo_cu_id", "cu_id")
                ->leftJoin("affiliate_partner_data", "afpada_cu_id", "cu_id")
                ->leftJoin("portals", "afpafo_po_id", "po_id")
                ->whereIn("afpafo_form_type", [1,4])
                ->where("cu_deleted", 0);

            if ($affiliate_partner_filter != null)
            {
                $query = $query->where("afpafo_cu_id", $affiliate_partner_filter);
            }

            $query = $query->orderBy("cu_company_name_business", "asc")
                ->get();

            foreach ($query as $row)
            {
                $query_afpafo_visits = AffiliatePartnerFormVisit::where("afpafovi_afpafo_id", "=", $row->afpafo_id)
                    ->whereBetween("afpafovi_date", System::betweenDates($request->date))
                    ->get();

                $visits = 0;

                foreach ($query_afpafo_visits as $row_affiliate_partner_form_visits)
                {
                    $visits += $row_affiliate_partner_form_visits->afpafovi_visits;
                }

                $data = new DataIndex();

                $requests_count = 0;
                $revenues = 0;
                $re_ids = [];

                $requests_query = \App\Models\Request::select("re_id", "ktrecupo_free", "ktrecupo_free_percentage", "ktrecupo_amount_netto_eur")
                    ->leftJoin("kt_request_customer_portal", "re_id", "ktrecupo_re_id")
                    ->where("re_afpafo_id", "=", $row->afpafo_id)
                    ->whereBetween("re_timestamp", System::betweenDates($request->date))
                    ->get();

                foreach ($requests_query as $re) {
                    if (!in_array($re->re_id, $re_ids)){
                        $requests_count++;
                        $re_ids[] = $re->re_id;
                    }
                    if ($re->ktrecupo_free != 1 && $re->ktrecupo_free_percentage != 1)
                    {
                        $revenues += $re->ktrecupo_amount_netto_eur;
                    }
                }

                $filtered_data[$row->afpafo_id] = [
                    "afpafo_id" => $row->afpafo_id,
                    "cu_company_name_business" => $row->cu_company_name_business,
                    "afpada_status" => $row->afpada_status,
                    "afpafo_name" => $row->afpafo_name,
                    "po_portal" => $row->po_portal,
                    "afpafo_la_code" => $data->getLanguage($row->afpafo_la_code),
                    "visits" => $visits,
                    "requests" => $requests_count,
                    "revenues" => "€ ".System::numberFormat($revenues, 2, ",", "."),
                    "conversion" => (($requests_count > 0) ? System::numberFormat(($requests_count / $visits) * 100, 2, ",", ".")."%" : "0,00%"),
                    "revenue_per_visit" => "€ ".(($requests_count > 0) ? System::numberFormat($revenues / $visits, 2, ",", ".") : "0,00")
                ];

                $total_visits += $visits;
                $total_requests += $requests_count;
                $total_revenues += $revenues;
            }

            $total_conversion = ($total_requests / $total_visits) * 100;
            $total_revenue_per_visit = $total_revenues / $total_visits;
        }


        if (!empty($_POST)){
            self::insertReportUsageHistory($rep_id);
        }

        return view('reports.affiliate_partner_forms', [
            'report' => $report,
            'date_filter' => $date_filter,
            'affiliate_partner_filter' => $affiliate_partner_filter,
            'merge_iframes_filter' => $merge_iframes_filter,
            'filtered_data' => $filtered_data,
            'total_visits' => $total_visits,
            'total_requests' => $total_requests,
            'total_conversion' => System::numberFormat($total_conversion, 2, ",", ".")."%",
            'total_revenue_per_visit' => "€ ".System::numberFormat($total_revenue_per_visit, 2, ",", "."),
            'total_revenues' => "€ ".System::numberFormat($total_revenues, 2, ",", "."),
            'affiliatepartnerstatuses' => AffiliatePartnerStatus::all(),
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function viewAffiliateLeadsPerCountry(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);
        $system = new System();
        $countries = [];
        foreach(Country::all() as $country){
            $countries[$country->co_code] = $country->co_en;
        }

        $date_filter = null;
        $total_requests = 0;
        $total_rejected = 0;
        $total_revenues = 0;

        if ($request->date != null)
        {
            $date_filter = $request->date;
        } else
        {
            return Redirect::back()->withErrors(['Please select a date']);
        }

        $filtered_data = [];
        $request_ids = [];

        $affiliate_partners = Customer::where("cu_type", 3)
            ->where("cu_deleted", 0)
            ->orderBy("cu_company_name_business", "asc")
            ->get();

        //Loop through all NON deleted affiliate partners OR through all selected affiliate partners
        foreach ($affiliate_partners as $afpa){

            $filtered_data[$afpa->cu_id] = [
                "company_name" => $afpa->cu_company_name_business
            ];

            //Get received requests from the affiliate
            $requests = AffiliatePartnerRequest::selectRaw("requests.re_co_code_from, requests.re_id, afpare_status")
                ->leftJoin("requests", "re_id", "afpare_re_id")
                ->where("afpare_cu_id", $afpa->cu_id)
                ->whereNotNull("re_co_code_from")
                ->whereBetween("re_timestamp", System::betweenDates($request->date))
                ->get();

            foreach ($requests as $req)
            {
                if (!array_key_exists($req->re_co_code_from, $filtered_data[$afpa->cu_id])) {
                    $filtered_data[$afpa->cu_id][$req->re_co_code_from]["revenues"] = 0;
                }

                if (!array_key_exists($req->afpare_status, $filtered_data[$afpa->cu_id][$req->re_co_code_from])) {
                    $filtered_data[$afpa->cu_id][$req->re_co_code_from][$req->afpare_status] = 0;
                }

                $filtered_data[$afpa->cu_id][$req->re_co_code_from][$req->afpare_status]++;

                $total_requests++;

                if($req->afpare_status == 2){
                    $total_rejected++;
                }

                $request_ids[$req->re_co_code_from][$afpa->cu_id][$req->re_id] = $req->re_co_code_from;
            }
        }

        foreach ($request_ids as $co_code => $customer_array) {
            foreach ($customer_array as $customer_id => $requests_array){
                $query_request_customer_portal = KTRequestCustomerPortal::select("ktrecupo_free", "ktrecupo_amount_netto", "ktrecupo_free_percentage", "ktrecupo_amount_netto_eur", "po_pacu_code")
                    ->join("portals", "ktrecupo_po_id", "po_id")
                    ->whereIn("ktrecupo_re_id", array_keys($requests_array))
                    ->get();

                $free = 0;
                $free_no_claim_discount = 0;
                $turnover = 0;

                foreach ($query_request_customer_portal as $row_request_customer_portal)
                {
                    $matches++;

                    if (!$row_request_customer_portal->ktrecupo_free && $row_request_customer_portal->ktrecupo_free_percentage > 0 && $row_request_customer_portal->ktrecupo_free_percentage < 1)
                    {
                        if ($row_request_customer_portal->ktrecupo_amount_netto_eur > 0.00) {
                            $free_no_claim_discount -= round($row_request_customer_portal->ktrecupo_amount_netto_eur * $row_request_customer_portal->ktrecupo_free_percentage,2);

                        }
                        else {
                            $free_no_claim_discount -= $system->currencyConvert($row_request_customer_portal->ktrecupo_amount_netto * $row_request_customer_portal->ktrecupo_free_percentage, $row_request_customer_portal->po_pacu_code, "EUR");
                        }
                    }

                    if ($row_request_customer_portal->ktrecupo_free)
                    {
                        $matches_free++;

                        if ($row_request_customer_portal->ktrecupo_amount_netto_eur > 0.00) {
                            $free -= round($row_request_customer_portal->ktrecupo_amount_netto_eur,2);
                        }
                        else {
                            $free -= $system->currencyConvert($row_request_customer_portal->ktrecupo_amount_netto, $row_request_customer_portal->po_pacu_code, "EUR");
                        }
                    } else
                    {
                        if ($row_request_customer_portal->ktrecupo_amount_netto_eur > 0.00) {
                            $turnover += round($row_request_customer_portal->ktrecupo_amount_netto_eur,2);
                        }
                        else {
                            $turnover += $system->currencyConvert($row_request_customer_portal->ktrecupo_amount_netto, $row_request_customer_portal->po_pacu_code, "EUR");
                        }
                    }
                }

                $filtered_data[$customer_id][$co_code]['revenues'] = $filtered_data[$customer_id][$co_code]['revenues'] + ($turnover + $free_no_claim_discount);
                $total_revenues = $total_revenues + ($turnover + $free_no_claim_discount);
            }
        }

        if (!empty($_POST)){
            self::insertReportUsageHistory($rep_id);
        }

        return view('reports.affiliate_leads_per_country', [
            'report' => $report,
            'date_filter' => $date_filter,
            'filtered_data' => $filtered_data,
            'countries' => $countries,
            'total_requests' => $total_requests,
            'total_rejected' => $total_rejected,
            'total_revenues' => $total_revenues,
            'affiliatepartnerstatuses' => AffiliatePartnerStatus::all(),
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function viewAffiliateLeadsPaid(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);

        $date_filter = null;
        $affiliate_partner_filter = [];
        $total_invoiced = 0;
        $total_not_paid = 0;
        $total_never_paid = 0;
        $total_paid = 0;

        if ($request->date != null)
        {
            $date_filter = $request->date;
        } else
        {
            return Redirect::back()->withErrors(['Please select a date']);
        }

        if (!empty($request->affiliate_partner))
        {
            $affiliate_partner_filter = $request->affiliate_partner;
        }

        $filtered_data = [];


        if (!empty($request->affiliate_partner))
        {
            $affiliate_partners = Customer::whereIn("cu_id", $request->affiliate_partner)->orderBy("cu_company_name_business")->get();
        }
        else {
            $affiliate_partners = Customer::where("cu_type", 3)
                ->where("cu_deleted", 0)
                ->orderBy("cu_company_name_business", "asc")
                ->get();
        }

        //Loop through all NON deleted affiliate partners OR through all selected affiliate partners
        foreach ($affiliate_partners as $afpa){

            $filtered_data[$afpa->cu_id] = [
                "company_name" => $afpa->cu_company_name_business,
                "invoiced" => 0,
                "paid" => 0,
                "not_paid" => 0,
                "never_paid" => 0
            ];

            //Get received requests from the affiliate
            $requests = AffiliatePartnerRequest::leftJoin("requests", "re_id", "afpare_re_id")
                ->leftJoin("kt_request_customer_portal", "afpare_re_id", "ktrecupo_re_id")
                ->leftJoin("invoice_lines", "inli_ktrecupo_id", "ktrecupo_id")
                ->leftJoin("invoices", "inli_in_id", "in_id")
                ->where("ktrecupo_invoiced", 1)
                ->where("afpare_status", 1)
                ->where("afpare_cu_id", $afpa->cu_id)
                ->whereBetween("re_timestamp", System::betweenDates($request->date))
                ->get();

            foreach ($requests as $req)
            {
                $bank_lines = KTBankLineInvoiceCustomerLedgerAccount::leftJoin("bank_lines", "ktbaliinculeac_bali_id", "bali_id")
                    ->where("ktbaliinculeac_in_id", "=", $req->inli_in_id)
                    ->get();

                foreach($bank_lines as $bank_line){
                    $total_invoice_price = $req->in_amount_netto_eur;
                    $currently_paid_price = $bank_line->ktbaliinculeac_amount;

                    $percentage_paid = ($currently_paid_price / $total_invoice_price);

                    if ($bank_line->ktbaliinculeac_leac_number == 121500)
                    {
                        $filtered_data[$afpa->cu_id]['never_paid'] = $filtered_data[$afpa->cu_id]['never_paid'] + ($percentage_paid * $req->inli_amount_netto_eur);

                    }else{
                        $filtered_data[$afpa->cu_id]['paid'] = $filtered_data[$afpa->cu_id]['paid'] + ($percentage_paid * $req->inli_amount_netto_eur);
                    }

                }


                $filtered_data[$afpa->cu_id]['invoiced'] = $filtered_data[$afpa->cu_id]['invoiced'] + $req['inli_amount_netto_eur'];
            }


            $total_not_paid = $total_not_paid + ($filtered_data[$afpa->cu_id]['invoiced'] - $filtered_data[$afpa->cu_id]['paid']);
            $filtered_data[$afpa->cu_id]['not_paid'] = $filtered_data[$afpa->cu_id]['invoiced'] - $filtered_data[$afpa->cu_id]['paid'];

            $total_paid = $total_paid + $filtered_data[$afpa->cu_id]['paid'];

            $total_never_paid = $total_never_paid + $filtered_data[$afpa->cu_id]['never_paid'];

            $total_invoiced = $total_invoiced + $filtered_data[$afpa->cu_id]['invoiced'];
        }

        if (!empty($_POST)){
            self::insertReportUsageHistory($rep_id);
        }

        return view('reports.affiliate_leads_paid', [
            'report' => $report,
            'date_filter' => $date_filter,
            'affiliate_partner_filter' => $affiliate_partner_filter,
            'filtered_data' => $filtered_data,
            'total_invoiced' => $total_invoiced,
            'total_paid' => $total_paid,
            'total_not_paid' => $total_not_paid,
            'total_never_paid' => $total_never_paid,
            'affiliatepartnerstatuses' => AffiliatePartnerStatus::all(),
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function viewServiceProviderNewsletterblocks(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);

        $date_filter = null;
        $service_provider_filter = [];
        $total_amount_sent = 0;

        if ($request->date != null)
        {
            $date_filter = $request->date;
        } else
        {
            return Redirect::back()->withErrors(['Please select a date']);
        }

        if (!empty($request->service_provider))
        {
            $service_provider_filter = $request->service_provider;
        }

        $filtered_data = [];


        if (!empty($request->service_provider))
        {
            $newsletterblocks_sent = KTServiceProviderNewsletterBlockSent::leftJoin("customers", "cu_id", "ktseprneblse_cu_id")
                ->leftJoin("service_provider_newsletter_blocks", "ktseprneblse_seprnebl_id", "seprnebl_id")
                ->whereBetween("ktseprneblse_timestamp", System::betweenDates($request->date))
                ->whereIn("cu_id", $request->service_provider)
                ->orderBy("cu_company_name_business", "ASC")
                ->get();
        }
        else {
            $newsletterblocks_sent = KTServiceProviderNewsletterBlockSent::leftJoin("customers", "cu_id", "ktseprneblse_cu_id")
                ->leftJoin("service_provider_newsletter_blocks", "ktseprneblse_seprnebl_id", "seprnebl_id")
                ->whereBetween("ktseprneblse_timestamp", System::betweenDates($request->date))
                ->orderBy("cu_company_name_business", "ASC")
                ->get();
        }

        $slots = NewsletterSlot::all();

        foreach ($newsletterblocks_sent as $sent) {

            $group = $sent->cu_id."_".$sent->ktseprneblse_slot;
            if (!array_key_exists($group, $filtered_data)) {
                $filtered_data[$group] = [
                    "cu_id" => $sent->ktseprneblse_cu_id,
                    "company_name" => $sent->cu_company_name_business,
                    "newsletterblock_name" => $slots[$sent->ktseprneblse_slot],
                    "amount_sent" => 1,
                ];
            }
            else {
                $filtered_data[$group]['amount_sent']++;
            }

            $total_amount_sent++;
        }


        if (!empty($_POST)){
            self::insertReportUsageHistory($rep_id);
        }

        return view('reports.service_provider_newsletterblocks', [
            'report' => $report,
            'date_filter' => $date_filter,
            'service_provider_filter' => $service_provider_filter,
            'filtered_data' => $filtered_data,
            'total_amount_sent' => $total_amount_sent,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function viewReviewAnalyse(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);
        $website_filter = [];
        $date_filter = null;
        $source_filter = null;
        if ($request->date != null)
        {
            $date_filter = $request->date;
        } else
        {
            return Redirect::back()->withErrors(['Please select a date']);
        }

        if (!empty($request->source)){
            $source_filter = $request->source;
        }

        if(!empty($request->websites)){
            $website_filter = $request->websites;
        }


        $filtered_data = [];
        $total_total_submissions = 0;
        $total_to_be_checked_on_hold = 0;
        $total_published_outright = 0;
        $total_published_after_email = 0;
        $total_rejected_outright = 0;
        $total_rejected_after_email = 0;
        $total_confirmation_mail_sent = 0;

        //Get received requests from the affiliate
        $reviews = Survey2::leftJoin("website_reviews", "su_were_id", "were_id")
            ->leftJoin("website_forms", "were_wefo_id", "=", "wefo_id")
            ->leftJoin("websites", "we_id", "=", "wefo_we_id")
            ->whereBetween("su_submitted_timestamp", System::betweenDates($request->date))
            ->where("su_rating", "!=", 0)
            ->whereIn("su_show_on_website", [1,2]);

        if($website_filter != null)
        {
            $reviews->whereIn("we_id", $request->websites);
        }

        if ($source_filter != "both")
        {
            $reviews = $reviews->where("su_source", $source_filter);
        }

        $reviews = $reviews->get();
        foreach ($reviews as $rev){
            $date = date("Y-m-d", strtotime($rev->su_submitted_timestamp));

            if(!isset($filtered_data[$date])) {
                $filtered_data[$date] = [
                    "date" => $date,
                    "total_submissions" => 0,
                    "to_be_checked_on_hold" => 0,
                    "published_outright" => 0,
                    "published_after_email" => 0,
                    "rejected_outright" => 0,
                    "rejected_after_email" => 0,
                    "confirmation_mail_sent" => 0,
                ];
            }

            $filtered_data[$date]["total_submissions"]++;
            $total_total_submissions++;

            if ($rev->su_confirmation_email) {
                $filtered_data[$date]["confirmation_mail_sent"]++;
                $total_confirmation_mail_sent++;
            }

            if ($rev->su_on_hold || $rev->su_show_on_website == 0) {
                $filtered_data[$date]["to_be_checked_on_hold"]++;
                $total_to_be_checked_on_hold++;
            }
            else {
                if ($rev->su_confirmation_email) {
                    if ($rev->su_show_on_website == 1) {
                        $filtered_data[$date]["published_after_email"]++;
                        $total_published_after_email++;
                    }

                    if ($rev->su_show_on_website == 2) {
                        $filtered_data[$date]["rejected_after_email"]++;
                        $total_rejected_after_email++;
                    }
                }
                else {
                    if ($rev->su_show_on_website == 1) {
                        $filtered_data[$date]["published_outright"]++;
                        $total_published_outright++;
                    }

                    if ($rev->su_show_on_website == 2) {
                        $filtered_data[$date]["rejected_outright"]++;
                        $total_rejected_outright++;
                    }
                }
            }

        }

        if (!empty($_POST)){
            self::insertReportUsageHistory($rep_id);
        }

        return view('reports.review_analyse', [
            'report' => $report,
            'sirelos' => Website::where("we_sirelo", 1)->orderBy("we_website", "ASC")->get(),
            'date_filter' => $date_filter,
            'source_filter' => $source_filter,
            'website_filter' => $website_filter,
            'filtered_data' => $filtered_data,
            'times_used' => self::getTimesUsedLast90Days($rep_id),

            //TOTALS
            'total_total_submissions' => $total_total_submissions,
            'total_to_be_checked_on_hold' => $total_to_be_checked_on_hold,
            'total_published_outright' => $total_published_outright,
            'total_published_after_email' => $total_published_after_email,
            'total_rejected_outright' => $total_rejected_outright,
            'total_rejected_after_email' => $total_rejected_after_email,
            'total_confirmation_mail_sent' => $total_confirmation_mail_sent,

        ]);
    }

    public function viewCustomerBalances(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);

        $status_checked = [];
        $payment_methods_checked = [];
        $filtered_data = null;

        $mover = new Mover();
        $data = new DataIndex();
        $system = new System();

        if (isset($request->status) && $request->payment_method)
        {
            foreach (CustomerPairStatus::all() as $key => $value)
            {
                $status_checked[$key] = ((isset($request['status'][$key])) ? "checked" : "");
            }

            foreach (PaymentMethod::all() as $key => $value)
            {
                $payment_methods_checked[$key] = ((isset($request['payment_method'][$key])) ? "checked" : "");
            }

            $statuses = [];
            $payment_methods = [];

            if (isset($request->status))
            {
                foreach ($request->status as $status => $on)
                {
                    $statuses[] = $status;
                }
            }

            if (isset($request->payment_method))
            {
                foreach ($request->payment_method as $payment_method => $on)
                {
                    $payment_methods[] = $payment_method;
                }
            }

            $query = Customer::leftJoin("kt_customer_portal", "cu_id", "ktcupo_cu_id")
                ->where("cu_deleted", "=", 0);

            if (!empty($statuses))
            {
                $query = $query->whereIn("ktcupo_status", $statuses);
            }

            if (!empty($payment_methods))
            {
                $query = $query->whereIn("cu_payment_method", $payment_methods);
            }

            $query = $query->get();

            $customers = [];

            foreach ($query as $customer)
            {
                $customers[$customer->cu_id] = "";
            }

            if (count($query) > 0)
            {
                $balances = $mover->customerBalancesFC(array_keys($customers));

                foreach ($query as $row)
                {
                    $balance = @System::useOr($balances[$row->cu_id], 0);

                    $country = Country::select("co_en")
                        ->where("co_code", "=", $row->cu_co_code)
                        ->first()->co_en;

                    $filtered_data[$row->cu_id] = [
                        "cu_id" => $row->cu_id,
                        "cu_company_name_business" => $row->cu_company_name_business,
                        "country" => $country,
                        "status" => System::getCustomerStatus($row->cu_id),
                        "debtorstatus" => DebtorStatus::all()[$row->cu_debtor_status],
                        "credithold" => (($row->cu_credit_hold == 1) ? "Yes" : "No"),
                        "paymentmethod" => PaymentMethod::all()[$row->cu_payment_method],
                        "creditlimit" => $system->paymentCurrencyToken($row->cu_pacu_code) . " " . $system->numberFormat($row->cu_credit_limit, 2),
                        "balance" => $system->paymentCurrencyToken($row->cu_pacu_code) . " " . $system->numberFormat($balance, 2),
                        "to_spend" => $system->paymentCurrencyToken($row->cu_pacu_code) . " " . $system->numberFormat($balance + $row->cu_credit_limit, 2),
                    ];
                }
            }
        }

        if (!empty($_POST)){
            self::insertReportUsageHistory($rep_id);
        }

        return view('reports.customer_balances', [
            'report' => $report,
            'filtered_data' => $filtered_data,
            'status_checked' => $status_checked,
            'payment_methods_checked' => $payment_methods_checked,
            'dataFiltered' => true,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function viewCustomerCreated(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);

        $date_filter = null;

        if ($request->date != null)
        {
            $date_filter = $request->date;
        } else
        {
            return Redirect::back()->withErrors(['Please select a date']);
        }

        $filtered_data = [];


        $customers_created = Customer::whereBetween("cu_created_timestamp", System::betweenDates($request->date))
            ->where("cu_deleted", "=", 0)
            ->groupBy("cu_id")
            ->orderBy("cu_company_name_business", "asc")
            ->get();

        foreach ($customers_created as $row)
        {
            $data = new DataIndex();
            $system = new System();

            $filtered_data[$row->cu_id] = [
                "id" => $row->cu_id,
                "customer" => $row->cu_company_name_business,
                "country" => $data->getCountry($row->cu_co_code),
                "status" => System::getCustomerStatus($row->cu_id),
                "created_timestamp" => $row->cu_created_timestamp,
                "created_by" => $data->getUserName($row->cu_created_us_id)
            ];
        }

        if (!empty($_POST)){
            self::insertReportUsageHistory($rep_id);
        }

        return view('reports.customer_created', [
            'report' => $report,
            'date_filter' => $date_filter,
            'filtered_data' => $filtered_data,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function viewStatusUpdate(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);

        $date_filter = null;

        if ($request->date != null)
        {
            $date_filter = $request->date;
        } else
        {
            return Redirect::back()->withErrors(['Please select a date']);
        }

        $filtered_data = [];

        /*$changes_query = TGB_QueryBuilder::selectAll()
            ->from("revisions")
            ->whereBetween("created_at", "{$yesterday} 08:30:00", "{$today} 08:30:00")
            ->whereIn("revisionable_type", ["App\\Models\\\Customer", "App\\Models\\\KTCustomerPortal", "App\\Models\\\KTCustomerPortalRegion", "kt_customer_portal_country", "kt_customer_portal_region_destination"])
            ->orderBy("created_at", TGB_QueryBuilder::ORDER_MODE_DESC)
            ->collectQuery();*/

        $changes_query = DB::table("revisions")
            ->whereBetween("created_at", System::betweenDates($request->date))
            //->whereBetween("created_at", [ '2019-11-12 08:30:00' , '2019-11-13 08:30:00' ])
            ->whereIn("revisionable_type", [
                "App\Models\Customer",
                "App\Models\KTCustomerPortal",
                "App\Models\KTCustomerPortalRegion",
                "App\Models\KTCustomerPortalCountry",
                "App\Models\KTCustomerPortalRegionDestination"
            ])
            ->whereRaw("
                `key` != 'cu_email' AND `key` != 'cu_coc' AND `key` != 'cu_email_bi' AND `key` != 'cu_logo' AND `key` != 'cu_attn' AND `key` != 'cu_description'
                AND `key` != 'created_at' AND `key` != 'cu_attn_email' AND `key` != 'cu_telephone' AND `key` != 'cu_finance_remarks' AND `key` != 'cu_credit_limit'
                AND `key` != 'cu_street_1' AND `key` != 'cu_street_2' AND `key` != 'cu_use_billing_address' AND `key` != 'cu_city' AND `key` != 'cu_zipcode'
                AND `key` != 'cu_website_bi' AND `key` != 'cu_website' AND `key` != 'cu_national_permit' AND `key` != 'cu_company_name_legal'
                AND `key` != 'cu_company_name_business'
            ")
            ->orderBy("created_at", "DESC")
            ->get();

        //dd($changes_query);

        $changes_query = System::databaseToArray($changes_query);

        $changed_customers = [];
        $changed_ktcupos = [];
        $changes = [];

        foreach ($changes_query as $changes_row)
        {
            $changes[] = $changes_row;
            if ($changes_row['revisionable_type'] === 'App\Models\Customer')
            {
                $changed_customers[$changes_row['revisionable_id']] = $changes_row['revisionable_id'];
            } else
            {
                $changed_ktcupos[$changes_row['revisionable_id']] = $changes_row['revisionable_id'];
            }
        }

        if (empty($changed_customers) && empty($changed_ktcupos))
        {
            //EMPTY
            return view('reports.status_update', [
                'report' => $report,
                'date_filter' => $date_filter,
                'filtered_data' => $filtered_data
            ]);
        }

        //Gather customers from portal
        if (!empty($changed_ktcupos))
        {
            $ktcupo_cu_query = DB::table("kt_customer_portal")
                ->select("ktcupo_cu_id")
                ->whereIn("ktcupo_id", $changed_ktcupos)
                ->distinct()
                ->get();

            $ktcupo_cu_query = System::databaseToArray($ktcupo_cu_query);

            foreach ($ktcupo_cu_query as $ktcupo_cu_row)
            {
                $changed_customers[$ktcupo_cu_row['ktcupo_cu_id']] = $ktcupo_cu_row['ktcupo_cu_id'];
            }
        }


        //Build customer array
        /*$customers = TGB_QueryBuilder::select(
            "cu_id", "cu_company_name_business", "cu_credit_hold", "cu_debt_collector"
        )
            ->from("customers")
            ->whereIn("cu_id", $changed_customers)
            ->whereEqual("cu_type", 1)
            ->collectQuery("cu_id", ["portals" => []]);
        $customer_ids = array_keys($customers);*/

        $customers_query = Customer::select("cu_id", "cu_company_name_business", "cu_credit_hold", "cu_debt_collector")
            ->whereIn("cu_id", $changed_customers)
            ->where("cu_type", 1)
            ->get();

        $customers = [];

        foreach ($customers_query as $cust)
        {
            $customers[$cust->cu_id] = [
                "cu_id" => $cust->cu_id,
                "cu_company_name_business" => $cust->cu_company_name_business,
                "cu_credit_hold" => $cust->cu_credit_hold,
                "cu_debt_collector" => $cust->cu_debt_collector,
                "portals" => []
            ];
        }

        //$customers = System::databaseToArray($customers);

        $customer_ids = array_keys($customers);

        if (empty($customer_ids))
        {
            //EMPTY
            return view('reports.status_update', [
                'report' => $report,
                'date_filter' => $date_filter,
                'filtered_data' => $filtered_data
            ]);
        }

        //=> Add kt_customer_portals
        $ktcupo_ids = [];

        $ktcupo_builder = KTCustomerPortal::select(
            "ktcupo_id", "ktcupo_cu_id", "ktcupo_po_id", "ktcupo_request_type", "ktcupo_status", "ktcupo_free_trial",
            "ktcupo_business_only", "ktcupo_min_volume_m3", "ktcupo_min_volume_ft3",
            "ktcupo_max_requests_month", "ktcupo_max_requests_day",
            "ktcupo_moving_size_1", "ktcupo_moving_size_2", "ktcupo_moving_size_3", "ktcupo_moving_size_4",
            "ktcupo_moving_size_5", "ktcupo_moving_size_6", "ktcupo_moving_size_7", "ktcupo_moving_size_8"
        )
            ->whereIn("ktcupo_cu_id", $customer_ids)
            ->get();

        $ktcupo_query = System::databaseToArray($ktcupo_builder);

        foreach ($ktcupo_query as $ktcupo_row)
        {
            $ktcupo_ids[$ktcupo_row['ktcupo_id']] = $ktcupo_row['ktcupo_cu_id'];
            $customers[$ktcupo_row['ktcupo_cu_id']]["portals"][$ktcupo_row['ktcupo_id']] = $ktcupo_row;
        }


        //=> Add origins/destinations
        $tables = [
            "kt_customer_portal_region" => ["ktcupore_ktcupo_id", "ktcupore_reg_id"],
            "kt_customer_portal_country" => ["ktcupoco_ktcupo_id", "ktcupoco_co_code"],
            "kt_customer_portal_region_destination" => ["ktcuporede_ktcupo_id", "ktcuporede_reg_id"]
        ];
        foreach ($tables as $table => $columns)
        {
            $ktcupo_column = $columns[0];
            $foreign_column = $columns[1];

            $table_query = DB::table($table)
                ->select($ktcupo_column, $foreign_column)
                ->whereIn($ktcupo_column, array_keys($ktcupo_ids))
                ->get();

            $table_query = System::databaseToArray($table_query);

            foreach ($table_query as $table_row)
            {
                $ktcupo_id = $table_row[$ktcupo_column];
                $cu_id = $ktcupo_ids[$ktcupo_id];
                $foreign_id = $table_row[$foreign_column];

                $customers[$cu_id]["portals"][$ktcupo_id][$table][$foreign_id] = $foreign_id;
            }
        }

        //Copy customers
        $old_customers = $customers;


        //dd($changes);


        //Roll back time
        foreach ($changes as $change)
        {
            //$fich_changes = System::unserialize($change['fich_changes']);

            $fich_changes = [
                $change['key'] => [$change['old_value'], $change['new_value']]
            ];

            $relevant = false;
            switch ($change['revisionable_type'])
            {
                case 'App\Models\Customer':
                    $customer = &$old_customers[$change['revisionable_id']];
                    $cu_id = $customer['cu_id'];
                    foreach ($fich_changes as $fich_change => $fich_values)
                    {
                        if (isset($customer[$fich_change]))
                        {
                            $relevant = true;
                            $customer[$fich_change] = $fich_values[0];
                        } else
                        {
                            unset($fich_changes[$fich_change]);
                        }
                    }
                    unset($customer);
                    break;

                case 'App\Models\KTCustomerPortal':
                    $ktcupo_id = $change['revisionable_id'];
                    $cu_id = $ktcupo_ids[$ktcupo_id];
                    $ktcupo = &$old_customers[$cu_id]["portals"][$ktcupo_id];

                    foreach ($fich_changes as $fich_change => $fich_values)
                    {
                        if (isset($ktcupo[$fich_change]))
                        {
                            $relevant = true;
                            $ktcupo[$fich_change] = $fich_values[0];
                        } else
                        {
                            unset($fich_changes[$fich_change]);
                        }
                    }
                    unset($ktcupo);
                    break;

                default:
                    //Link with regions/countries
                    $ktcupo_id = $change['revisionable_id'];
                    $cu_id = $ktcupo_ids[$ktcupo_id];
                    $ktcupo = &$old_customers[$cu_id]["portals"][$ktcupo_id];

                    $relevant = true;

                    foreach ($fich_changes as $value => $old_new)
                    {
                        if ($old_new == 1)
                        {
                            unset($ktcupo[$change['revisionable_type']][$value]);
                        } else
                        {
                            $ktcupo[$change['revisionable_type']][$value] = $value;
                        }
                    }

                    unset($ktcupo);
            }

            $change["fich_changes"] = $fich_changes;

            if ($relevant)
            {
                $customers[$cu_id]["changes"][] = $change;
            }
        }


        //Clean old customers
        foreach ($old_customers as $cu_id => $customer)
        {
            if ($customer === null || !isset($customer['cu_id']))
            {
                unset($old_customers[$cu_id]);
            }
        }

        $old_customer_statuses = self::determineCustomerStatuses($old_customers);
        $customer_statuses = self::determineCustomerStatuses($customers);

        //dd($customer_statuses[96]);

        $date = explode('-', $request->date);

        //Get planned status updates
        $status_updates = [];
        $escaped_today = trim($date[1]) . ' 23:59:59';

        $stup_query = StatusUpdate::select("stup_ktcupo_id", "stup_date", "stup_status", "stup_description")
            ->whereRaw("(`stup_date` > '{$escaped_today}' OR (`stup_date` = '{$escaped_today}' AND `stup_hour` > 8))")
            ->whereIn("stup_ktcupo_id", array_keys($ktcupo_ids))
            ->orderBy("stup_date", "ASC")
            ->get();


        $stup_query = System::databaseToArray($stup_query);

        foreach ($stup_query as $stup_row)
        {
            if (!isset($status_updates[$stup_row['stup_ktcupo_id']]))
            {
                $status_updates[$stup_row['stup_ktcupo_id']] = $stup_row;
            }
        }

        $users_query = User::get();
        $users = [];
        foreach ($users_query as $us)
        {
            $users[$us->id] = $us->us_name;
        }
        $portals = Data::portals();


        $counter = 0;

        //Rows
        $cur_row = 2;
        foreach ($customers as $cu_id => $customer)
        {
            if (!isset($customer['changes']))
            {
                continue;
            }
            $counter++;
            $header_row = $cur_row;

            //Set statuses
            $old_status = $old_customer_statuses[$cu_id];
            $new_status = $customer_statuses[$cu_id];

            $row["B"] = self::setCustomerStatusFields($old_customer_statuses[$cu_id]);
            $row["C"] = self::setCustomerStatusFields($customer_statuses[$cu_id]);

            $summary_comments = [];
            $changed_by = [];
            $last_change_date = "";


            //Loop through changes
            $changes = array_reverse($customer["changes"]);
            foreach ($changes as $change)
            {
                $user = $change['user_id'] == 0 ? "System" : @System::useOr($users[$change['user_id']], "Unknown");
                $changed_by[$change['user_id']] = $user;
                $last_change_date = $change['created_at'];
                $change_cause = (empty($change['key']) ? "Manual" : ucfirst(str_replace("_", " ", $change["key"])));

                $row = [
                    "D" => $change['created_at'],
                    "E" => $user,
                    "F" => $change_cause
                ];

                $change['fich_changes'] = [
                    $change['key'] => [
                        0 => $change['old_value'],
                        1 => $change['new_value']
                    ]
                ];

                if ($change['revisionable_type'] !== 'App\Models\Customer')
                {
                    $ktcupo = $customer['portals'][$change['revisionable_id']];
                    $row["G"] = @System::useOr($portals[$ktcupo['ktcupo_po_id']], "Unknown") . " - " . RequestType::all()[$ktcupo['ktcupo_request_type']];
                }

                if ($change['revisionable_type'] == 'App\Models\KTCustomerPortal' || $change['revisionable_type'] == 'App\Models\Customer')
                {
                    foreach ($change['fich_changes'] as $fich_change => $fich_values)
                    {
                        $change_values = self::fieldChangeValues($fich_change, $fich_values);
                        $row["H"] = $change_values[0];
                        $row["I"] = $change_values[1];
                        $row["J"] = $change_values[2];

                        //Custom stuff for ktcupo status change
                        if ($fich_change == 'ktcupo_status')
                        {
                            //Check for planned
                            if (isset($status_updates[$change['revisionable_id']]))
                            {
                                $status_update = $status_updates[$change['revisionable_id']];
                                $comment = "Change planned on [{$status_update['stup_date']}] to " . CustomerPairStatus::all()[$status_update['stup_status']];
                                if (!empty($status_update['sthi_description']))
                                {
                                    $comment .= ": " . $status_update['sthi_description'];
                                }
                            } elseif ($fich_values[1] == 2)
                            {
                                $comment = "On pause without reactivation!";
                            }

                            if (isset($comment))
                            {
                                $row["K"] = $comment;
                                $summary_comments[$comment] = $comment;
                            }

                            unset($comment);
                        }

                        if (!isset($row['D']))
                        {
                            $row["D"] = $change['created_at'];
                            $row["E"] = $user;
                            $row["F"] = $change_cause;
                        }

                        //$row = [];
                    }
                } else
                {
                    $changed_thing = ($change['revisionable_type'] == 'App\Models\KTCustomerPortalRegion' ? "origins" : "destinations");
                    $row["H"] = ucfirst($changed_thing);

                    //Add comment about how many changed
                    $added = 0;
                    $removed = 0;
                    foreach ($change['fich_changes'] as $thing => $values)
                    {
                        if ($values[1] == 1)
                        {
                            $added++;
                        } else
                        {
                            $removed++;
                        }
                    }
                    foreach (["Added" => [$added, "J"], "Removed" => [$removed, "I"]] as $mutation => $amount_cell)
                    {
                        if ($amount_cell[0] > 0)
                        {
                            $row[$amount_cell[1]] = "{$mutation} {$amount_cell[0]} {$changed_thing}";
                        }
                    }
                }


                if ($row["F"] == "Ktcupo status")
                {
                    $row["F"] = "Portal Status";
                }
                elseif($row["F"] == "Cu credit hold")
                {
                    $row["F"] = "Credit hold";
                }

                if (self::setCustomerStatusFields($old_customer_statuses[$cu_id]) != self::setCustomerStatusFields($customer_statuses[$cu_id]))
                {
                    $filtered_data[$customer['cu_id']] = [
                        "customer" => $customer['cu_company_name_business'],
                        "old_customer_status" => self::setCustomerStatusFields($old_customer_statuses[$cu_id]),
                        "new_customer_status" => self::setCustomerStatusFields($customer_statuses[$cu_id]),
                        "date_of_change" => $row["D"],
                        "changed_by" => $row["E"],
                        "change_cause" => $row["F"],
                        "portal" => $row["G"],
                        "changed" => $row["H"],
                        "old_value" => $row["I"],
                        "new_value" => $row["J"],
                        "comment" => $row["K"]
                    ];
                }
            }


            /*  //Set comment
              $sheet->setCellValue("D{$header_row}", $last_change_date);

              foreach(["K{$header_row}" => $summary_comments, "E{$header_row}" => $changed_by] as $cell_coords => $array)
              {
                  $cell = $sheet->getCell($cell_coords);
                  $cell->setValue(implode("\n", $array));
                  $cell->getStyle()->getAlignment()->setWrapText(true);
              }*/
        }

        if (!empty($_POST)){
            self::insertReportUsageHistory($rep_id);
        }

        return view('reports.status_update', [
            'report' => $report,
            'date_filter' => $date_filter,
            'filtered_data' => $filtered_data,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    /**
     * Parse a field_change name and it's values.
     * Returns an array containing the display name and the 2 values.
     * @param string $field The field name
     * @param array $values An array with the 2 values [0 => val1, 1 => val2]
     * @return array an array with the name & values
     */
    public static function fieldChangeValues($field, $values)
    {
        $modifier = null;
        $name = null;

        switch ($field)
        {
            //Booleans
            case "ktcupo_free_trial":
            case "ktcupo_business_only":
            case "ktcupo_export_moves":
            case "ktcupo_import_moves":

            case "cu_finance_lock":
            case "cu_credit_hold":
            case "cu_debt_collector":
            case "cu_enforce_credit_limit":
            case "cu_use_billing_address":

            case "moda_international_moves":
            case "moda_national_moves":
            case "moda_no_claim_discount":
            case "moda_hide_lead_partner_functions":
            case "moda_hide_response_chart":
            case "moda_activate_lead_pick":
            case "moda_disable_sirelo_export":
            case "moda_review_partner":
            case "moda_sirelo_widget_bypass":
            case "afpada_status":

                $modifier = function ($value) {
                    return $value == 0 ? "Off" : "On";
                };
                break;


            case "moda_quality_score_override":
                $modifier = function ($value) {
                    return DataIndex::qualityScoreOverrides()[$value];
                };
                break;

            //People
            case "cu_sales_manager":
            case "cu_account_manager":
                $modifier = function ($value) {
                    return Data::userName($value, "<span style=\"color: #FF0000;\">Functions</span>");
                };
                break;

            //kt_customer_portal
            case "ktcupo_request_type":
                $modifier = function ($value) {
                    return DataIndex::requestTypes()[$value];
                };
                break;
            case "ktcupo_status":
                $modifier = function ($value) {
                    return DataIndex::customerPairStatuses()[$value];
                };
                break;

            //customers
            case "moda_capping_method":
                $modifier = function ($value) {
                    return DataIndex::moverCappingMethods()[$value];
                };
                break;
            case "cu_debtor_status":
                $modifier = function ($value) {
                    return DataIndex::debtorStatuses()[$value];
                };
                break;
            case "cu_auto_debit_co_code":
                $name = "Auto debit country";
                $modifier = function ($value) {
                    return Data::country($value);
                };
                break;

            default:
                //Try to find fields with {table}_XX_code format
                $exploded_field = explode("_", $field);
                if (isset($exploded_field[2]))
                {
                    if ($exploded_field[2] == "code")
                    {
                        switch ($exploded_field[1])
                        {
                            case "la":
                                $name = "Language";
                                $modifier = function ($value) {
                                    return Data::getLanguage($value);
                                };
                                break;

                            case "co":
                                if (isset($exploded_field[3]) && $exploded_field[3] == "bi")
                                {
                                    $name = "Country" . (isset($exploded_field[3]) && $exploded_field[3] == "bi" ? " (Business)" : "");
                                }
                                $modifier = function ($value) {
                                    return Data::country($value);
                                };
                                break;

                            case "pacu":
                                $name = "Payment currency";
                                $modifier = function ($value) {
                                    return Data::getPaymentCurrency($value);
                                };
                                break;
                        }
                    }

                    //Other common ones
                    if ($exploded_field[1] == "payment" && $exploded_field[2] == "method")
                    {
                        $modifier = function ($value) {
                            return DataIndex::paymentMethods()[$value];
                        };
                    } elseif ($exploded_field[1] == "leac" && $exploded_field[2] == "number")
                    {
                        $modifier = function ($value) {
                            return Data::getLedgerAccount($value);
                        };
                    } elseif (strpos($field, "moving_size_") !== false)
                    {
                        $moving_size = explode("moving_size_", $field)[1];
                        $name = "Moving size: " . DataIndex::movingSizes()[$moving_size];
                        $modifier = function ($value) {
                            return $value == 0 ? "Off" : "On";
                        };
                    }
                }
        }

        if ($name === null)
        {
            $exploded_field = explode("_", $field);
            $last = count($exploded_field) - 1;
            unset($exploded_field[0]);
            if ($exploded_field[$last] == "bi")
            {
                $exploded_field[$last] = "(Business)";
            }
            $name = ucfirst(implode(" ", $exploded_field));
        }

        if ($modifier == null)
        {
            $old = $values[0];
            $new = $values[1];
        } else
        {
            $old = $modifier($values[0]);

            if (is_array($old))
            {
                $old = $old[$values[0]];
            }

            $new = $modifier($values[1]);

            if (is_array($new))
            {
                $new = $new[$values[1]];
            }
        }

        return [
            $name,
            $old,
            $new
        ];
    }

    public function setCustomerStatusFields($status)
    {
        $mv_status = MoverStatus::all()[$status];
        $val = explode("MV - ", $mv_status)[1];

        switch ($status)
        {
            case 1: //Active
                $color = '00CC66'; //Green
                break;

            case 2: //Free trial
                $color = '0000CC'; //Blue
                break;

            case 3: //Paused
                $color = 'FF8000'; //Orange
                break;

            case 5: //Prospect
                $color = '808080'; //Grey
                break;

            default: //Cancelled, Credit hold, Debt collector
                $color = 'FF0000'; //Red
        }

        return "<span style='font-weight:800;color:#" . $color . "!important;'>" . $val . "</span>";
    }

    //Determine customer statuses
    public function determineCustomerStatuses($customers)
    {
        $results = [];
        $cancellations = [];

        //Check for cancelled
        /*$cancellations = TGB_QueryBuilder::select("cust_cu_id")
            ->from("customer_statuses")
            ->setDistinct(true)
            ->whereInKeys("cust_cu_id", $customers)
            ->whereEqual("cust_status", 2)
            ->whereBetweenRaw("cust_date", "NOW() - INTERVAL 1 YEAR", "NOW()")
            ->collectMap("cust_cu_id", "cust_cu_id");*/

        $cancellations_query = DB::table("customer_statuses")
            ->select("cust_cu_id")
            ->distinct()
            ->whereIn("cust_cu_id", $customers)
            ->where("cust_status", 2)
            ->whereRaw("`cust_date` BETWEEN NOW() - INTERVAL 1 YEAR AND NOW()")
            ->get();

        $cancellations_query = System::databaseToArray($cancellations_query);

        foreach ($cancellations_query as $cancellation)
        {
            $cancellations[$cancellation['cust_cu_id']] = $cancellation['cust_cu_id'];
        }

        foreach ($customers as $cu_id => $customer)
        {
            if ($customer['cu_debt_collector'] == 1)
            {
                $status = 7;
            } elseif ($customer['cu_credit_hold'] == 1)
            {
                $status = 6;
            } else
            {
                $ktcupo_statuses = [];
                foreach ($customer["portals"] as $ktcupo_id => $ktcupo)
                {
                    if ($ktcupo['ktcupo_status'] == 1)
                    {
                        $key = ($ktcupo['ktcupo_free_trial'] == 1 ? 2 : 1);
                        $ktcupo_statuses[$key] = true;
                    } elseif ($ktcupo['ktcupo_status'] == 2)
                    {
                        $ktcupo_statuses[3] = true;
                    }
                }

                if (isset($cancellations[$cu_id]))
                {
                    $ktcupo_statuses[4] = true;
                }

                $status = 5;
                foreach ([1, 2, 3, 4] as $key)
                {
                    if (isset($ktcupo_statuses[$key]))
                    {
                        $status = $key;
                        break;
                    }
                }
            }

            $results[$cu_id] = $status;
        }

        return $results;
    }

    public function viewCustomerGDPR(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);

        $status_filter = null;

        if ($request->status != null)
        {
            $status_filter = $request->status;
        } else
        {
            return Redirect::back()->withErrors(['Please select a status']);
        }
        $system = new System();
        $cache = new Cache();

        $filtered_data = [];
        $eu_countries = ["AT", "BE", "BG", "HR", "CY", "CZ", "DK", "EE", "FI", "FR", "DE", "GR", "HU", "IE", "IT", "LV", "LT", "LU", "MT", "NL", "PL", "PT", "RO", "SK", "SI", "ES", "SE", "GB", "NO", "CH", "IS"];
        //dd($cache->get("customer_gdpr_list"));

        foreach ($system->unserialize($cache->get("customer_gdpr_list")['value']) as $row)
        {
            $companies = CustomerGDPR::rightJoin("customers", "cu_id", "cugdpr_cu_id")->where("cu_id", "=", $row)->get();

            $company_row = CustomerGDPR::rightJoin("customers", "cu_id", "cugdpr_cu_id")
                ->where("cu_id", "=", $row)
                ->first();

            //Only show filled in
            //IF EU: any record = TRUE
            //IF NON-EU: Must be 2 records
            if ($request->status == 2)
            {
                if (empty($company_row->cugdpr_id))
                {
                    continue;
                }
                if (!in_array($company_row->cu_co_code, $eu_countries) && count($companies) < 2)
                {
                    continue;
                }
            }
            $system = new System();

            $status = $system->getStatus($company_row->cu_id);

            if ($company_row->cu_credit_hold)
            {
                $credit_hold = "Credit Hold";
            } else
            {
                $credit_hold = "NO";
            }

            //Only show NOT filled in
            //IF EU: ANY RECORD: CONTINUE
            //IF NON-EU: LESS THAN 2 RECORDS
            if ($request->status == 3)
            {
                if (!empty($company_row->cugdpr_id) && in_array($company_row->cu_co_code, $eu_countries))
                {
                    continue;
                }

                if (count($companies) == 2 && !in_array($company_row->cu_co_code, $eu_countries))
                {
                    continue;
                }
            }

            if (in_array($company_row->cu_co_code, $eu_countries))
            {
                $region = "EU";
            } else
            {
                $region = "Non-EU";
            }

            if (count($companies) == 2)
            {
                //Confirmed
                $agreement_1 = "confirmed";
                $agreement_2 = "confirmed";
            } else
            {
                if ($company_row['cugdpr_version'] == 1)
                {
                    //Confirmed
                    $agreement_1 = "confirmed";

                    if (!in_array($company_row->cu_co_code, $eu_countries))
                    {
                        //Not confirmed
                        $agreement_2 = "not_confirmed";
                    } else
                    {
                        $agreement_2 = "";
                    }
                } else
                {
                    //Not confirmed
                    $agreement_1 = "not_confirmed";
                    if (!in_array($company_row->cu_co_code, $eu_countries))
                    {
                        //Not confirmed
                        $agreement_2 = "not_confirmed";
                    } else
                    {
                        $agreement_2 = "";
                    }
                }
            }

            $filtered_data[$company_row->cu_id] = [
                "id" => $company_row->cu_id,
                "customer" => $company_row->cu_company_name_business,
                "email" => $company_row->cu_email,
                "country_code" => $company_row->cu_co_code,
                "region" => $region,
                "status" => $status,
                "credit_hold" => $credit_hold,
                "agreement_1" => $agreement_1,
                "agreement_2" => $agreement_2
            ];
        }

        if (!empty($_POST)){
            self::insertReportUsageHistory($rep_id);
        }

        return view('reports.customer_gdpr', [
            'report' => $report,
            'status_filter' => $status_filter,
            'filtered_data' => $filtered_data,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function viewCustomerLanguages($rep_id)
    {
        $report = Report::findOrFail($rep_id);

        $filtered_data = [];

        $customers = Customer::select("cu_id", "cu_company_name_business", "cu_la_code", "cu_co_code", "cu_type")
            ->where("cu_deleted", "=", 0)
            ->groupBy("cu_id")
            ->get();

        foreach ($customers as $row)
        {
            $data = new DataIndex();
            $system = new System();

            $filtered_data[$row->cu_id] = [
                "cu_id" => $row->cu_id,
                "cu_company_name_business" => $row->cu_company_name_business,
                "country" => $data->getCountry($row->cu_co_code),
                "language" => $row->cu_la_code,
                "status" => $system->getStatus($row->cu_id),
                "cu_co_code" => $row->cu_co_code
            ];
        }

        $rights = true;
        $rep_right = KTUserReport::where("ktusre_us_id", Auth::id())->where("ktusre_rep_id", $rep_id)->get();

        if (count($rep_right) == 0)
        {
            $rights = false;
        }

        if (!empty($_POST)){
            self::insertReportUsageHistory($rep_id);
        }

        $times_used = self::getTimesUsedLast90Days($rep_id);

        return view('reports.customer_languages', [
            'report' => $report,
            'filtered_data' => $filtered_data,
            'report_rights' => $rights,
            'times_used' => $times_used
        ]);
    }

    public function viewCustomerPortals($rep_id)
    {
        $report = Report::findOrFail($rep_id);
        $dataindex = new DataIndex();
        $data = new Data();
        $system = new System();
        $filtered_data = [];

        foreach ($data->activeCustomers() as $customer)
        {
            $query_customer = Customer::join("countries", "co_code", "cu_co_code")
                ->where("cu_id", $customer['cu_id'])
                ->first();

            if ($query_customer->cu_credit_hold == 1)
            {
                $credithold = "Yes";
            } else
            {
                $credithold = "No";
            }

            $customers[$customer['cu_id']] = [
                "cu_id" => $customer['cu_id'],
                "name" => $query_customer->cu_company_name_business,
                "country" => $dataindex->getCountry($query_customer->cu_co_code),
                "baggage" => "-",
                "national" => "-",
                "international" => "-",
                "credithold" => $credithold
            ];

            $query_national_portals = KTCustomerPortal::join("portals", "po_id", "ktcupo_po_id")
                ->where("ktcupo_status", 1)
                ->where("ktcupo_cu_id", $customer['cu_id'])
                ->where("ktcupo_request_type", "!=", 2)
                ->where("po_destination_type", 2)
                ->get();

            if (count($query_national_portals) > 0)
            {
                $customers[$customer['cu_id']]['national'] = "Active";
            }

            //if national still empty
            if ($customers[$customer['cu_id']]['national'] == "-")
            {
                //check for Paused
                $query_national_portals_paused = KTCustomerPortal::join("portals", "po_id", "ktcupo_po_id")
                    ->where("ktcupo_status", 2)
                    ->where("ktcupo_cu_id", $customer['cu_id'])
                    ->where("ktcupo_request_type", "!=", 2)
                    ->where("po_destination_type", 2)
                    ->get();

                if (count($query_national_portals_paused) > 0)
                {
                    $customers[$customer['cu_id']]['national'] = "Paused";
                }
            }

            //if national still empty
            if ($customers[$customer['cu_id']]['national'] == "-")
            {
                //check for Paused
                $query_national_portals_inactive = KTCustomerPortal::join("portals", "po_id", "ktcupo_po_id")
                    ->where("ktcupo_status", 0)
                    ->where("ktcupo_cu_id", $customer['cu_id'])
                    ->where("ktcupo_request_type", "!=", 2)
                    ->where("po_destination_type", 2)
                    ->get();

                if (count($query_national_portals_inactive) > 0)
                {
                    $customers[$customer['cu_id']]['national'] = "Off";
                }
            }

            //Query active international portals
            $query_international_portals = KTCustomerPortal::join("portals", "po_id", "ktcupo_po_id")
                ->where("ktcupo_status", 1)
                ->where("ktcupo_cu_id", $customer['cu_id'])
                ->where("ktcupo_request_type", "!=", 2)
                ->where("po_destination_type", 1)
                ->get();

            if (count($query_international_portals) > 0)
            {
                $customers[$customer['cu_id']]['international'] = "Active";
            }

            //if international still empty
            if ($customers[$customer['cu_id']]['international'] == "-")
            {
                //check for Paused
                $query_international_portals_paused = KTCustomerPortal::join("portals", "po_id", "ktcupo_po_id")
                    ->where("ktcupo_status", 2)
                    ->where("ktcupo_cu_id", $customer['cu_id'])
                    ->where("ktcupo_request_type", "!=", 2)
                    ->where("po_destination_type", 1)
                    ->get();

                if (count($query_international_portals_paused) > 0)
                {
                    $customers[$customer['cu_id']]['international'] = "Paused";
                }


            }

            //if international still empty
            if ($customers[$customer['cu_id']]['international'] == "-")
            {
                //check for Paused
                $query_international_portals_inactive = KTCustomerPortal::join("portals", "po_id", "ktcupo_po_id")
                    ->where("ktcupo_status", 0)
                    ->where("ktcupo_cu_id", $customer['cu_id'])
                    ->where("ktcupo_request_type", "!=", 2)
                    ->where("po_destination_type", 1)
                    ->get();

                if (count($query_international_portals_inactive) > 0)
                {
                    $customers[$customer['cu_id']]['international'] = "Off";
                }
            }


            //Query active baggage portals
            $query_baggage_portals = KTCustomerPortal::join("portals", "po_id", "ktcupo_po_id")
                ->where("ktcupo_status", 1)
                ->where("ktcupo_cu_id", $customer['cu_id'])
                ->where("ktcupo_request_type", 2)
                ->get();

            if (count($query_baggage_portals) > 0)
            {
                $customers[$customer['cu_id']]['baggage'] = "Active";
            }

            //if baggage still empty
            if ($customers[$customer['cu_id']]['baggage'] == "-")
            {
                //check for Paused
                $query_baggage_portals_paused = KTCustomerPortal::join("portals", "po_id", "ktcupo_po_id")
                    ->where("ktcupo_status", 2)
                    ->where("ktcupo_cu_id", $customer['cu_id'])
                    ->where("ktcupo_request_type", 2)
                    ->get();

                if (count($query_baggage_portals_paused) > 0)
                {
                    $customers[$customer['cu_id']]['baggage'] = "Paused";
                }
            }

            //if baggage still empty
            if ($customers[$customer['cu_id']]['baggage'] == "-")
            {
                //check for Paused
                $query_baggage_portals_inactive = KTCustomerPortal::join("portals", "po_id", "ktcupo_po_id")
                    ->where("ktcupo_status", 0)
                    ->where("ktcupo_cu_id", $customer['cu_id'])
                    ->where("ktcupo_request_type", 2)
                    ->get();

                if (count($query_baggage_portals_inactive) > 0)
                {
                    $customers[$customer['cu_id']]['baggage'] = "Off";
                }
            }
        }

        foreach ($customers as $row)
        {
            $filtered_data[$row['cu_id']] = [
                "cu_id" => $row['cu_id'],
                "name" => $row['name'],
                "country" => $row['country'],
                "baggage" => $row['baggage'],
                "national" => $row['national'],
                "international" => $row['international'],
                "credithold" => $row['credithold']
            ];
        }

        $rights = true;
        $rep_right = KTUserReport::where("ktusre_us_id", Auth::id())->where("ktusre_rep_id", $rep_id)->get();

        if (count($rep_right) == 0)
        {
            $rights = false;
        }

        if (!empty($_POST)){
            self::insertReportUsageHistory($rep_id);
        }

        $times_used = self::getTimesUsedLast90Days($rep_id);

        return view('reports.customer_portals', [
            'report' => $report,
            'filtered_data' => $filtered_data,
            'report_rights' => $rights,
            'times_used' => $times_used
        ]);
    }

    public function viewCustomerRemarks(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);

        $date_filter = null;
        $department_filter = null;
        $employee_filter = null;
        $medium_filter = null;
        $direction_filter = null;

        if ($request->date != null)
        {
            $date_filter = $request->date;
        } else
        {
            return Redirect::back()->withErrors(['Please select a date']);
        }

        if ($request->department != null)
        {
            $department_filter = $request->department;
        }

        if ($request->employee != null)
        {
            $employee_filter = $request->employee;
        }

        if ($request->medium != null)
        {
            $medium_filter = $request->medium;
        }

        if ($request->direction != null)
        {
            $direction_filter = $request->direction;
        }

        $filtered_data = [];

        $customer_remarks = CustomerRemark::leftJoin("customers", "cure_cu_id", "cu_id")
            ->leftJoin("users", "cu_sales_manager", "us_id")
            ->Leftjoin("customer_statuses", "cust_id", "cure_status_id")
            ->whereBetween("cure_timestamp", System::betweenDates($request->date))
            ->where("cu_deleted", 0);

        if ($request->department != null)
        {
            $customer_remarks = $customer_remarks->where("cure_department", $request->department);
        }

        if ($request->employee != null)
        {
            $customer_remarks = $customer_remarks->where("cure_employee", $request->employee);
        }

        if ($request->medium != null)
        {
            $customer_remarks = $customer_remarks->where("cure_medium", $request->medium);
        }

        if ($request->direction != null)
        {
            $customer_remarks = $customer_remarks->where("cure_direction", $request->direction);
        }

        $customer_remarks = $customer_remarks->orderBy("cure_timestamp", "DESC")
            ->get();

        $system = new System();
        $dataindex = new DataIndex();
        $customerstatuses = CustomerStatusStatus::all();
        $customerstatusreasons = CustomerStatusReason::all();

        $departments = CustomerRemarkDepartment::all();
        $mediums = CustomerRemarkMedium::all();
        $directions = CustomerRemarkDirection::all();

        foreach ($customer_remarks as $remark)
        {
            if ($remark->cure_status_id != 0)
            {
                //Create a table to display status
                $status = "<table id='remark'>";
                $status .= "<th colspan=\"4\"><strong>Status update</strong></th>";
                $status .= "<tr><td><strong>Status</strong></td><td><strong>Reason</strong></td><td><strong>Netto turnover</strong></td><td><strong>Remark</strong></td></tr>";
                $status .= "<tr><td>" . $customerstatuses[$remark->cust_status] . "</td>";
                $status .= "<td>" . $customerstatusreasons[$remark->cust_reason] . "</td>";
                $status .= "<td>" . $system->paymentCurrencyToken("EUR") . " " . $system->numberFormat($remark->cust_turnover_netto, 2) . "</td>";
                $status .= "<td>" . $remark->cust_remark . "</td></tr>";
                $status .= "</table>";
            } else
            {
                $status = $remark->cure_text;
            }

            $filtered_data[$remark->cure_id] = [
                "id" => $remark->cure_id,
                "timestamp" => $remark->cure_timestamp,
                "customer" => $remark->cu_company_name_business,
                "country" => $dataindex->getCountry($remark->cu_co_code),
                "contact_date" => $remark->cure_contact_date,
                "sales_manager" => $remark->us_name,
                "employee" => User::select("us_name")->where("us_id", $remark->cure_employee)->first()->us_name,
                "department" => $departments[$remark->cure_department],
                "medium" => $mediums[$remark->cure_medium],
                "direction" => $directions[$remark->cure_direction],
                "remarks" => $status
            ];
        }

        if (!empty($_POST)){
            self::insertReportUsageHistory($rep_id);
        }

        return view('reports.customer_remarks', [
            'report' => $report,
            'date_filter' => $date_filter,
            'filtered_data' => $filtered_data,

            'department_filter' => $department_filter,
            'employee_filter' => $employee_filter,
            'medium_filter' => $medium_filter,
            'direction_filter' => $direction_filter,
            'departments' => CustomerRemarkDepartment::all(),
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function viewCustomerStatuses(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);

        $date_filter = null;
        $status_filter = null;
        $reason_filter = null;
        $sales_manager_filter = null;

        if ($request->date != null)
        {
            $date_filter = $request->date;
        } else
        {
            return Redirect::back()->withErrors(['Please select a date']);
        }

        if ($request->status != null)
        {
            $status_filter = $request->status;
        }

        if ($request->reason != null)
        {
            $reason_filter = $request->reason;
        }

        if ($request->sales_manager != null)
        {
            $sales_manager_filter = $request->sales_manager;
        }

        $filtered_data = [];

        $customer_statuses = CustomerStatus::join("customers", "cust_cu_id", "cu_id")
            ->leftjoin("users", "cu_sales_manager", "us_id")
            ->whereBetween("cust_date", System::betweenDates($request->date))
            ->where("cu_deleted", 0);

        if ($request->status != null)
        {
            $customer_statuses = $customer_statuses->where("cust_status", $request->status);
        }

        if ($request->sales_manager != null)
        {
            $customer_statuses = $customer_statuses->where("cu_sales_manager", $request->sales_manager);
        }

        if ($request->reason != "")
        {
            $customer_statuses = $customer_statuses->where("cust_reason", $request->reason);
        }

        $customer_statuses = $customer_statuses->orderBy("cust_date", "DESC")
            ->get();

        $system = new System();
        $dataindex = new DataIndex();

        $total_turnover = 0;
        $customerstatuses = CustomerStatusStatus::all();
        $customerstatusreasons = CustomerStatusReason::all();


        foreach ($customer_statuses as $status)
        {
            $latest_remark = "";

            $customer_status_query = CustomerStatus::select("cust_remark", "cust_date")
                ->where("cust_cu_id", $status->cust_cu_id)
                ->orderBy("cust_date", "DESC")
                ->limit(1)
                ->first();
            if (count($customer_status_query) > 0)
            {
                $latest_remark_date = $customer_status_query->cust_date;
                $latest_remark_text = $customer_status_query->cust_remark;
            }

            $filtered_data[$status->cust_id] = [
                "id" => $status->cust_id,
                "customer" => $status->cu_company_name_business,
                "country" => $dataindex->getCountry($status->cu_co_code),
                "date" => $status->cust_date,
                "sales_manager" => $status->us_name,
                "employee" => User::select("us_name")->where("us_id", $status->cust_employee)->first()->us_name,
                "status" => $customerstatuses[$status->cust_status],
                "reason" => $customerstatusreasons[$status->cust_reason],
                "turnover" => $system->paymentCurrencyToken("EUR") . " " . $system->numberFormat($status->cust_turnover_netto, 2),
                "remark" => $status->cust_remark,
                "latest_status_date" => $latest_remark_date,
                "latest_status_text" => $latest_remark_text,
            ];

            $total_turnover += $status->cust_turnover_netto;
        }

        if (!empty($_POST)){
            self::insertReportUsageHistory($rep_id);
        }

        return view('reports.customer_statuses', [
            'report' => $report,
            'date_filter' => $date_filter,
            'filtered_data' => $filtered_data,

            'status_filter' => $status_filter,
            'reason_filter' => $reason_filter,
            'sales_manager_filter' => $sales_manager_filter,
            'departments' => CustomerRemarkDepartment::all(),

            'total_turnover' => $system->paymentCurrencyToken("EUR") . " " . $system->numberFormat($total_turnover, 2),
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function viewFreeLeadsPerSirelo(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);

        $date_filter = null;
        $show_who_received = null;
        $show_exclusive = null;

        if ($request->date != null)
        {
            $date_filter = $request->date;
        } else
        {
            return Redirect::back()->withErrors(['Please select a date']);
        }

        if (!empty($request->show_who_received)){
            $show_who_received = $request->show_who_received;
        }
        else {
            $show_who_received = 0;
        }

        if (!empty($request->show_exclusive)){
            $show_exclusive = $request->show_exclusive;
        }
        else {
            $show_exclusive = 0;
        }


        $filtered_data = [];

        $free_leads = \App\Models\Request::selectRaw("*, count(*) as `amount`")
            ->leftJoin("website_forms", "wefo_id", "re_wefo_id")
            ->leftJoin("websites", "we_id", "wefo_we_id")
            ->leftJoin("customers", "cu_id", "re_sirelo_customer")
            ->where("we_sirelo", 1)
            ->where("re_sirelo_customer", ">", 0)
            ->whereBetween("re_timestamp", System::betweenDates($request->date));

        if ($show_exclusive === 0 && $show_who_received === 0) {
            $free_leads = $free_leads->groupBy("we_website");
        }
        elseif($show_exclusive === 0 && $show_who_received === "on") {
            $free_leads = $free_leads->groupBy("we_website", "cu_company_name_business");
        }
        elseif($show_exclusive === "on" && $show_who_received === 0) {
            $free_leads = $free_leads->groupBy("we_website");
        }
        elseif($show_exclusive === "on" && $show_who_received === "on") {
            $free_leads = $free_leads->groupBy("we_website", "cu_company_name_business");
        }

        $free_leads = $free_leads->get();

        $system = new System();
        $dataindex = new DataIndex();

        $total_free_leads = 0;
        $total_exclusive = 0;

        foreach ($free_leads as $sirelo)
        {
            $key = $sirelo->we_website;

            if ($show_who_received === "on") {
                $key = $key."_".$sirelo->cu_company_name_business;
            }

            /*if ($show_exclusive === "on") {
                $key = $key."_".$sirelo->re_exclusive_match;
            }*/

            //dd($key);

            $filtered_data[$key] = [
                "name" => $sirelo->we_website,
                "amount" => $sirelo->amount,
            ];

            if ($show_who_received === "on") {
                $filtered_data[$key]['company_name'] = $sirelo->cu_company_name_business;
            }


            if ($show_exclusive === "on") {
                $exclusive_count = \App\Models\Request::selectRaw("count(*) as `amount`")
                    ->leftJoin("website_forms", "wefo_id", "re_wefo_id")
                    ->leftJoin("websites", "we_id", "wefo_we_id")
                    //->leftJoin("customers", "cu_id", "re_sirelo_customer")
                    ->where("we_sirelo", 1)
                    //->where("re_sirelo_customer", ">", 0)
                    ->whereBetween("re_timestamp", System::betweenDates($request->date))
                    ->where("we_website", $sirelo->we_website);

                if ($show_who_received === "on") {
                    $exclusive_count = $exclusive_count->where("re_sirelo_customer", $sirelo->cu_id);
                }
                else {
                    $exclusive_count = $exclusive_count->where("re_sirelo_customer", ">", 0);
                }

                $exclusive_count = $exclusive_count->where("re_exclusive_match", 1)->first()->amount;

                $filtered_data[$key]['exclusive'] = $exclusive_count;

                $total_exclusive+= $exclusive_count;
            }

            $total_free_leads = $total_free_leads + $sirelo->amount;
        }

        if (!empty($_POST)){
            self::insertReportUsageHistory($rep_id);
        }

        return view('reports.free_leads_per_sirelo', [
            'report' => $report,
            'date_filter' => $date_filter,
            'show_who_received' => $show_who_received,
            'show_exclusive' => $show_exclusive,
            'total_exclusive' => $total_exclusive,
            'filtered_data' => $filtered_data,
            'total_free_leads' => $total_free_leads,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function viewLeadPick(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);

        $date_filter = null;

        if ($request->date != null)
        {
            $date_filter = $request->date;
        } else
        {
            return Redirect::back()->withErrors(['Please select a date']);
        }

        $filtered_data = [];
        $total_lead_picks = 0;
        $dataindex = new DataIndex();

        $lead_picks = KTRequestCustomerPortal::selectRaw("cu_id, cu_company_name_business, co_en, COUNT(*) as leadpickcount")
            ->join("customers", "cu_id", "ktrecupo_cu_id")
            ->join("countries", "co_code", "cu_co_code")
            ->whereBetween("ktrecupo_timestamp", System::betweenDatesWithoutTime($request->date))
            ->where("ktrecupo_type", 4)
            ->groupBy("cu_company_name_business")
            ->orderBy("cu_company_name_business", "asc")
            ->get();

        foreach ($lead_picks as $row)
        {
            $filtered_data[$row->cu_id] = [
                "customer" => $row->cu_company_name_business,
                "country" => $row->co_en,
                "leads_picked" => $row->leadpickcount
            ];

            $total_lead_picks += $row->leadpickcount;

        }

        if (!empty($_POST)){
            self::insertReportUsageHistory($rep_id);
        }

        return view('reports.lead_pick', [
            'report' => $report,
            'date_filter' => $date_filter,
            'filtered_data' => $filtered_data,
            'total_lead_picks' => $total_lead_picks,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function viewLoadExchangeUsage(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);

        $date_filter = null;
        $table_with_filter = null;

        if ($request->date != null)
        {
            $date_filter = $request->date;
        } else
        {
            return Redirect::back()->withErrors(['Please select a date']);
        }

        if ($request->table_with != null)
        {
            $table_with_filter = $request->table_with;
        }

        if (!empty($_POST)){
            self::insertReportUsageHistory($rep_id);
        }

        $filtered_data = [];
        $dataindex = new DataIndex();

        if ($request->table_with == 1)
        {
            //Set totals
            $total_loads = 0;
            $total_quotes = 0;
            $total_times_interested = 0;
            $total_chosen_for_someone = 0;

            //Make empty customers array
            $customers_array = [];

            //Get all loads in date range
            $loads = CustomerLoadExchange::join("customers", "cu_id", "culoex_cu_id")
                ->whereBetween("culoex_timestamp", System::betweenDates($request->date))
                ->get();

            //Loop through all loads
            foreach ($loads as $customer_load)
            {
                //Get customer ID
                $cu_id = $customer_load->culoex_cu_id;

                //Add customer to array if cu_id (key) not exists
                if (!array_key_exists($cu_id, $customers_array))
                {
                    $customers_array[$cu_id] = [
                        'company_name' => $customer_load->cu_company_name_business,
                        'loads_posted' => 0,
                        'quotes_offered' => 0,
                        'shown_interest' => 0,
                        'got_contacted' => 0,
                        'chosen_for_someone_bool' => false
                    ];
                }

                //Counter up loads for this customer
                $customers_array[$cu_id]['loads_posted']++;

                //Counter of total loads up
                $total_loads++;
            }

            //Get all quotes in date range
            $quotes = CustomerLoadReaction::join("customer_load_exchange", "culore_culoex_id", "culoex_id")
                ->join("customers", "cu_id", "culore_cu_id")
                ->whereBetween("culoex_timestamp", System::betweenDates($request->date))
                ->get();

            //Loop through all quotes
            foreach ($quotes as $customer_quote)
            {
                //Get customer ID
                $cu_id = $customer_quote->culore_cu_id;

                //Add customer to array if cu_id (key) not exists
                if (!array_key_exists($cu_id, $customers_array))
                {
                    $customers_array[$cu_id] = [
                        'company_name' => $customer_quote->cu_company_name_business,
                        'loads_posted' => 0,
                        'quotes_offered' => 0,
                        'shown_interest' => 0,
                        'got_contacted' => 0,
                        'chosen_for_someone_bool' => false
                    ];
                }

                //Counter up quotes for this customer
                $customers_array[$cu_id]['quotes_offered']++;

                if ($customer_quote->culore_confirmed == 1)
                {
                    $customers_array[$cu_id]['shown_interest']++;
                    $total_times_interested++;
                }

                //Counter of total quotes up
                $total_quotes++;
            }

            foreach ($customers_array as $customer_id => $values)
            {
                if ($customers_array[$customer_id]['chosen_for_someone_bool'] == false)
                {
                    //Get how many chosen for someone
                    $query_chosen_for_someone = CustomerLoadReaction::selectRaw("count(*) as `count`")
                        ->join("customer_load_exchange", "culore_culoex_id", "culoex_id")
                        ->join("customers", "cu_id", "culore_cu_id")
                        ->whereBetween("culoex_timestamp", System::betweenDates($request->date))
                        ->where("culoex_cu_id", $customer_id)
                        ->where("culore_confirmed", 1)
                        ->first();

                    if ($query_chosen_for_someone->count > 0)
                    {
                        $customers_array[$customer_id]['got_contacted'] = $query_chosen_for_someone->count;
                        $total_chosen_for_someone = $total_chosen_for_someone + $query_chosen_for_someone->count;
                        $customers_array[$customer_id]['chosen_for_someone_bool'] = true;
                    }
                }
            }

            $filtered_data = $customers_array;

            return view('reports.load_exchange_usage', [
                'report' => $report,
                'date_filter' => $date_filter,
                'table_with_filter' => $table_with_filter,
                'filtered_data' => $filtered_data,

                //TOTALS
                'total_loads_posted' => $total_loads,
                'total_quotes_offered' => $total_quotes,
                'total_shown_interest' => $total_times_interested,
                'total_got_contacted' => $total_chosen_for_someone,
                'times_used' => self::getTimesUsedLast90Days($rep_id)
            ]);
        }

        if ($request->table_with == 2)
        {
            //Get all loads in date range
            $loads = CustomerLoadExchange::join("customers", "cu_id", "culoex_cu_id")
                ->whereBetween("culoex_timestamp", System::betweenDates($request->date))
                ->get();

            //Loop through all loads
            foreach ($loads as $load)
            {
                //Get quotes of this load
                $quotes = CustomerLoadReaction::join("customers", "cu_id", "culore_cu_id")
                    ->where("culore_culoex_id", $load->culoex_id)
                    ->get();

                $sub_table = "
                    <table class='table'>
                        <thead>
                            <tr>
                                <th>Quotes from:</th>
                                <th>Timestamp:</th>
                                <th>Price:</th>
                                <th>Remark:</th>
                             </tr>
                        </thead>
                        <tbody>
                 ";

                //Loop through all quotes
                foreach ($quotes as $quote)
                {
                    $sub_table .= "
                        <tr>
                            <td>" . $quote->cu_company_name_business . "</td>
                            <td>" . $quote->culore_timestamp . "</td>
                            <td>" . $quote->culore_price . "</td>
                            <td>" . $quote->culore_remark . "</td>
                        </tr>
                    ";
                }

                $sub_table .= "</tbody></table>";

                $sub_table = str_replace("\"", "'", $sub_table);


                $country_from = Country::where("co_code", $load->culoex_co_code_from)->first()->co_en;
                $country_to = Country::where("co_code", $load->culoex_co_code_to)->first()->co_en;


                $filtered_data[$load->culoex_id] = [
                    'sub_table' => $sub_table,
                    'company_name' => $load->cu_company_name_business,
                    'city_and_country_from' => $load->culoex_city_from . " (" . $country_from . ")",
                    'city_and_country_to' => $load->culoex_city_to . " (" . $country_to . ")",
                    'load_timestamp' => $load->culoex_timestamp,
                ];
            }

            return view('reports.load_exchange_usage', [
                'report' => $report,
                'date_filter' => $date_filter,
                'table_with_filter' => $table_with_filter,
                'filtered_data' => $filtered_data,
                'times_used' => self::getTimesUsedLast90Days($rep_id)
            ]);
        }
    }

    public function viewLoginReports(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);

        $date_filter = null;

        if ($request->date != null)
        {
            $date_filter = $request->date;
        } else
        {
            return Redirect::back()->withErrors(['Please select a date']);
        }

        $filtered_data = [];

        //CHANGE THIS ARRAY IF YOU WANT TO ADD ANOTHER USER TO THIS REPORT
        $users = [4662, 4660, 4568, 4605, 4670, 4039];

        $logins = UserLogin::join("users", "us_id", "uslo_us_id")
            ->whereIn("uslo_us_id", $users)
            ->whereBetween("uslo_timestamp", System::betweenDates($request->date))
            ->where("uslo_us_id", "!=", "")
            ->orderBy("uslo_timestamp", "asc")
            ->get();

        foreach ($logins as $row)
        {
            $first_match = "-";

            //Check when the first match was for this user.
            $first_match_query = KTRequestCustomerPortal::whereBetween("ktrecupo_timestamp", [date("Y-m-d H:i:s", strtotime($row->uslo_timestamp)), date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s")))])
                ->where("ktrecupo_us_id", $row->us_id)
                ->limit(1)
                ->first();

            $first_match = $first_match_query->ktrecupo_timestamp;

            $filtered_data[$row->uslo_id] = [
                "name" => $row->us_name,
                "timestamp" => $row->uslo_timestamp,
                "first_match" => $first_match
            ];
        }

        if (!empty($_POST)){
            self::insertReportUsageHistory($rep_id);
        }

        //dd($filtered_data);
        return view('reports.login_report', [
            'report' => $report,
            'date_filter' => $date_filter,
            'filtered_data' => $filtered_data,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function viewMarginAffiliatePartners(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);
        $date_filter = null;
        $status_active_filter = false;
        $status_inactive_filter = false;
        $notes_after_showing = "";
        if ($request->date != null)
        {
            $date_filter = $request->date;
            $date = explode( '-', $request->date );
            if (date("Y-m-d",strtotime($date[0])) < date("Y-m-d", strtotime("2020-05-01"))) {
                $notes_after_showing .= "<span style='color: red;'>Results before May 2020 are inaccurate, because the currency flactuates every hour.</span>";
            }

        } else
        {
            return Redirect::back()->withErrors(['Please select a date']);
        }

        $status_checked = [];

        if (isset($request->status_active))
        {
            $status_checked[] = 1;
            $status_active_filter = true;
        }

        if (isset($request->status_inactive))
        {
            $status_checked[] = 0;
            $status_inactive_filter = true;
        }

        $affiliatepartnerstatuses = AffiliatePartnerStatus::all();
        $system = new System();
        $dataindex = new DataIndex();

        $total_requests = 0;

        $total_ap_requests_approved = 0;
        $total_ap_requests_rejected = 0;
        $total_ap_requests_open = 0;
        $total_ap_requests_open_percentage = 0;

        $total_requests_move = 0;
        $total_requests_baggage_service = 0;

        $total_matches = 0;
        $total_matches_free = 0;
        $total_gross_revenue = 0;
        $total_free_no_claim_discount = 0;
        $total_free = 0;
        $total_turnover = 0;
        $total_costs = 0;

        $total_requests_matched = 0;
        $total_requests_rejected = 0;

        $filtered_data = [];

        $query = Customer::join("affiliate_partner_data", "afpada_cu_id", "cu_id")
            ->where("cu_type", 3)
            ->where("cu_deleted", 0);

        if (!empty($status_checked))
        {
            $query = $query->whereIn("afpada_status", $status_checked);
        }

        $query = $query->orderBy("cu_company_name_business", "asc")->get();

        foreach ($query as $row)
        {
            $query_affiliate_partner_requests = AffiliatePartnerRequest::join("requests", "afpare_re_id", "re_id")
                ->where("afpare_cu_id", $row->cu_id)
                ->whereBetween("re_timestamp", System::betweenDates($request->date))
                ->whereNotIn("re_rejection_reason", [16,17])
                ->get();

            $request_ids = [];

            $requests = 0;

            $ap_requests_approved = 0;
            $ap_requests_rejected = 0;
            $ap_requests_open = 0;

            $requests_move = 0;
            $requests_baggage_service = 0;

            $matches = 0;
            $matches_free = 0;
            $free = 0;
            $free_no_claim_discount = 0;
            $turnover = 0;
            $costs = 0;

            $requests_matched = 0;
            $requests_rejected = 0;

            foreach ($query_affiliate_partner_requests as $row_affiliate_partner_requests)
            {
                $request_ids[] = $row_affiliate_partner_requests->re_id;
                $requests++;

                if ($row_affiliate_partner_requests->afpare_status == 1)
                {
                    $ap_requests_approved++;
                } elseif ($row_affiliate_partner_requests->afpare_status == 2)
                {
                    $ap_requests_rejected++;
                } elseif ($row_affiliate_partner_requests->afpare_status == 0)
                {
                    $ap_requests_open++;
                }

                if ($row_affiliate_partner_requests->re_request_type == 1)
                {
                    $requests_move++;
                } elseif ($row_affiliate_partner_requests->re_request_type == 2)
                {
                    $requests_baggage_service++;
                }

                if (($row_affiliate_partner_requests->afpare_status == 0 || $row_affiliate_partner_requests->afpare_status == 1) && !$row->cu_revenues_share)
                {
                    if($row_affiliate_partner_requests->afpare_amount_eur <= 0){
                        $costs += $system->currencyConvert($row_affiliate_partner_requests->afpare_amount, $row_affiliate_partner_requests->afpare_currency, "EUR");
                    }else{
                        $costs += $row_affiliate_partner_requests->afpare_amount_eur;

                    }

                    //$costs += $row_affiliate_partner_requests->afpare_amount_eur;
                }

                if ($row_affiliate_partner_requests->re_status == 1)
                {
                    $requests_matched++;
                } elseif ($row_affiliate_partner_requests->re_status == 2)
                {
                    $requests_rejected++;
                }
            }



            if (!empty($request_ids))
            {
                //TODO: check query (SELECT PART)
                $query_request_customer_portal = KTRequestCustomerPortal::select("ktrecupo_free", "ktrecupo_amount_netto", "ktrecupo_free_percentage", "ktrecupo_amount_netto_eur", "po_pacu_code")
                    ->join("portals", "ktrecupo_po_id", "po_id")
                    ->whereIn("ktrecupo_re_id", $request_ids)
                    ->get();

                foreach ($query_request_customer_portal as $row_request_customer_portal)
                {
                    $matches++;

                    if (!$row_request_customer_portal->ktrecupo_free && $row_request_customer_portal->ktrecupo_free_percentage > 0 && $row_request_customer_portal->ktrecupo_free_percentage < 1)
                    {
                        if ($row_request_customer_portal->ktrecupo_amount_netto_eur > 0.00) {
                            $free_no_claim_discount -= round($row_request_customer_portal->ktrecupo_amount_netto_eur * $row_request_customer_portal->ktrecupo_free_percentage,2);

                        }
                        else {
                            $free_no_claim_discount -= $system->currencyConvert($row_request_customer_portal->ktrecupo_amount_netto * $row_request_customer_portal->ktrecupo_free_percentage, $row_request_customer_portal->po_pacu_code, "EUR");
                        }

                    }

                    if ($row_request_customer_portal->ktrecupo_free)
                    {
                        $matches_free++;

                        if ($row_request_customer_portal->ktrecupo_amount_netto_eur > 0.00) {
                            $free -= round($row_request_customer_portal->ktrecupo_amount_netto_eur,2);

                        }
                        else {
                            $free -= $system->currencyConvert($row_request_customer_portal->ktrecupo_amount_netto, $row_request_customer_portal->po_pacu_code, "EUR");
                        }


                        //$free -= $system->currencyConvert($row_request_customer_portal->ktrecupo_amount_netto, $row_request_customer_portal->po_pacu_code, "EUR");

                    } else
                    {
                        if ($row_request_customer_portal->ktrecupo_amount_netto_eur > 0.00) {
                            $turnover += round($row_request_customer_portal->ktrecupo_amount_netto_eur,2);
                        }
                        else {
                            $turnover += $system->currencyConvert($row_request_customer_portal->ktrecupo_amount_netto, $row_request_customer_portal->po_pacu_code, "EUR");
                        }
                        //$turnover += $system->currencyConvert($row_request_customer_portal->ktrecupo_amount_netto, $row_request_customer_portal->po_pacu_code, "EUR");

                    }
                }
            }

            if ($requests > 0)
            {
                $ap_requests_approved_percentage = (($ap_requests_approved > 0) ? (($ap_requests_approved / $requests) * 100) : 0);
                $ap_requests_rejected_percentage = (($ap_requests_rejected > 0) ? (($ap_requests_rejected / $requests) * 100) : 0);
                $ap_requests_open_percentage = (($ap_requests_open > 0) ? (($ap_requests_open / $requests) * 100) : 0);

                $requests_move_percentage = (($requests_move > 0) ? (($requests_move / $requests) * 100) : 0);
                $requests_baggage_service_percentage = (($requests_baggage_service > 0) ? (($requests_baggage_service / $requests) * 100) : 0);

                $requests_matched_percentage = (($requests_matched > 0) ? (($requests_matched / $requests) * 100) : 0);
                $requests_rejected_percentage = (($requests_rejected > 0) ? (($requests_rejected / $requests) * 100) : 0);
            } else
            {
                $ap_requests_approved_percentage = 0;
                $ap_requests_rejected_percentage = 0;
                $ap_requests_open_percentage = 0;

                $requests_move_percentage = 0;
                $requests_baggage_service_percentage = 0;

                $requests_matched_percentage = 0;
                $requests_rejected_percentage = 0;
            }

            if ($matches_free != 0 && $matches != 0)
            {
                $free_percentage = (($matches_free / $matches) * 100);
            } else
            {
                $free_percentage = 0;
            }

            if($row->cu_revenues_share && $costs == 0){
                $percent = ($row->cu_revenues_share/100);
                $costs = $turnover * $percent;
            }

            $margin = $turnover - $costs;

            if ($margin != 0 && $turnover != 0)
            {
                $margin_percentage = (($margin / $turnover) * 100);
            } else
            {
                $margin_percentage = 0;
            }
            $gross_revenues = $turnover - $free;

            $filtered_data[$row->cu_id] = [
                "id" => $row->cu_id,
                "affiliate_partner" => $row->cu_company_name_business,
                "status" => $affiliatepartnerstatuses[$row->afpada_status],
                "requests" => $requests,
                "matched" => $requests_matched,
                "percentage_matched" => System::numberFormat($requests_matched_percentage, 2) . "%",
                "percentage_rejected" => System::numberFormat($requests_rejected_percentage, 2) . "%",
                "percentage_moves" => System::numberFormat($requests_move_percentage, 2) . "%",
                "percentage_baggage" => System::numberFormat($requests_baggage_service_percentage, 2) . "%",
                "gross_revenues" => $system->paymentCurrencyToken("EUR") . " " . System::numberFormat($gross_revenues, 2),
                "free" => $system->paymentCurrencyToken("EUR") . " " . System::numberFormat($free, 2),
                "percentage_free" => System::numberFormat($free_percentage, 2) . "%",
                "free_no_claim_discount" => $system->paymentCurrencyToken("EUR") . " " . System::numberFormat($free_no_claim_discount, 2),
                "revenues" => $system->paymentCurrencyToken("EUR") . " " . System::numberFormat($turnover + $free_no_claim_discount, 2),
                "costs" => $system->paymentCurrencyToken("EUR") . " " . System::numberFormat((($turnover + $free_no_claim_discount) / 100) * $row->cu_revenues_share, 2),
                "margin" => $system->paymentCurrencyToken("EUR") . " " . System::numberFormat($margin, 2),
                "percentage_margin" => System::numberFormat($margin_percentage, 2) . "%",
            ];

            //START calculating totals
            $total_requests += $requests;

            $total_ap_requests_approved += $ap_requests_approved;
            $total_ap_requests_rejected += $ap_requests_rejected;
            $total_ap_requests_open += $ap_requests_open;

            $total_requests_move += $requests_move;
            $total_requests_baggage_service += $requests_baggage_service;

            $total_matches += $matches;
            $total_matches_free += $matches_free;
            $total_gross_revenue += $gross_revenues;
            $total_free += $free;
            $total_free_no_claim_discount += $free_no_claim_discount;
            $total_turnover += ($turnover + $free_no_claim_discount);
            $total_costs += $costs;

            $total_requests_matched += $requests_matched;
            $total_requests_rejected += $requests_rejected;
            //END calculating totals
        }

        if ($total_requests > 0)
        {
            $total_requests_move_percentage = (($total_requests_move > 0) ? (($total_requests_move / $total_requests) * 100) : 0);
            $total_requests_baggage_service_percentage = (($total_requests_baggage_service > 0) ? (($total_requests_baggage_service / $total_requests) * 100) : 0);

            $total_requests_matched_percentage = (($total_requests_matched > 0) ? (($total_requests_matched / $total_requests) * 100) : 0);
            $total_requests_rejected_percentage = (($total_requests_rejected > 0) ? (($total_requests_rejected / $total_requests) * 100) : 0);
        } else
        {
            $total_requests_move_percentage = 0;
            $total_requests_baggage_service_percentage = 0;

            $total_requests_matched_percentage = 0;
            $total_requests_rejected_percentage = 0;
        }

        if ($total_matches_free != 0 && $total_matches != 0)
        {
            $total_free_percentage = (($total_matches_free / $total_matches) * 100);
        } else
        {
            $total_free_percentage = 0;
        }

        $total_margin = $total_turnover - $total_costs;

        if ($total_margin != 0 && $total_turnover != 0)
        {
            $total_margin_percentage = (($total_margin / $total_turnover) * 100);
        } else
        {
            $total_margin_percentage = 0;
        }

        if (!empty($_POST)){
            self::insertReportUsageHistory($rep_id);
        }

        return view('reports.margin_affiliate_partners', [
            'report' => $report,
            'date_filter' => $date_filter,
            'filtered_data' => $filtered_data,
            'notes_after_showing' => $notes_after_showing,

            'status_active_filter' => $status_active_filter,
            'status_inactive_filter' => $status_inactive_filter,

            //TOTALS
            'total_requests' => $total_requests,
            'total_requests_matched' => $total_requests_matched,
            'total_requests_matched_percentage' => System::numberFormat($total_requests_matched_percentage, 2) . "%",
            'total_requests_rejected_percentage' => System::numberFormat($total_requests_rejected_percentage, 2) . "%",
            'total_requests_move_percentage' => System::numberFormat($total_requests_move_percentage, 2) . "%",
            'total_requests_baggage_service_percentage' => System::numberFormat($total_requests_baggage_service_percentage, 2) . "%",
            'total_gross_revenue' => "€ " . System::numberFormat($total_gross_revenue, 2),
            'total_free' => "€ " . System::numberFormat($total_free, 2),
            'total_free_percentage' => System::numberFormat($total_free_percentage, 2) . "%",
            'total_turnover' => "€ " . System::numberFormat($total_turnover, 2),
            'total_costs' => "€ " . System::numberFormat($total_costs, 2),
            'total_margin' => "€ " . System::numberFormat($total_margin, 2),
            'total_free_no_claim_discount' => "€ " . System::numberFormat($total_free_no_claim_discount, 2),
            'total_margin_percentage' => System::numberFormat($total_margin_percentage, 2) . "%",
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function viewInvoicedAffiliatePartners(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);
        $date_filter = null;
        $status_active_filter = false;
        $status_inactive_filter = false;

        if ($request->date != null)
        {
            $date_filter = $request->date;
        } else
        {
            return Redirect::back()->withErrors(['Please select a date']);
        }

        $status_checked = [];

        if (isset($request->status_active))
        {
            $status_checked[] = 1;
            $status_active_filter = true;
        }

        if (isset($request->status_inactive))
        {
            $status_checked[] = 0;
            $status_inactive_filter = true;
        }

        $affiliatepartnerstatuses = AffiliatePartnerStatus::all();
        $system = new System();

        $total_requests = 0;

        $total_ap_requests_approved = 0;
        $total_ap_requests_rejected = 0;
        $total_ap_requests_open = 0;

        $total_requests_move = 0;
        $total_requests_baggage_service = 0;

        $total_matches = 0;
        $total_matches_free = 0;
        $total_gross_revenue = 0;
        $total_free = 0;
        $total_turnover = 0;
        $total_costs = 0;

        $total_requests_matched = 0;
        $total_requests_rejected = 0;

        $filtered_data = [];

        $query = Customer::join("affiliate_partner_data", "afpada_cu_id", "cu_id")
            ->where("cu_type", CustomerType::AFFILIATE_PARTNER)
            ->where("cu_deleted", 0);

        if (!empty($status_checked))
        {
            $query = $query->whereIn("afpada_status", $status_checked);
        }

        $query = $query->orderBy("cu_company_name_business", "asc")->get();

        foreach ($query as $row)
        {
            $query_affiliate_partner_requests = AffiliatePartnerRequest::join("requests", "afpare_re_id", "re_id")
                ->where("afpare_cu_id", $row->cu_id)
                ->whereBetween("re_timestamp", System::betweenDates($request->date))
                ->whereNotIn("re_rejection_reason", [16,17])
                ->get();

            $request_ids = [];

            $requests = 0;

            $ap_requests_approved = 0;
            $ap_requests_rejected = 0;
            $ap_requests_open = 0;

            $requests_move = 0;
            $requests_baggage_service = 0;

            $matches = 0;
            $matches_free = 0;
            $free = 0;
            $turnover = 0;
            $costs = 0;

            $requests_matched = 0;
            $requests_rejected = 0;

            foreach ($query_affiliate_partner_requests as $row_affiliate_partner_requests)
            {
                $request_ids[] = $row_affiliate_partner_requests->re_id;
                $requests++;

                if ($row_affiliate_partner_requests->afpare_status == 1)
                {
                    $ap_requests_approved++;
                } elseif ($row_affiliate_partner_requests->afpare_status == 2)
                {
                    $ap_requests_rejected++;
                } elseif ($row_affiliate_partner_requests->afpare_status == 0)
                {
                    $ap_requests_open++;
                }

                if ($row_affiliate_partner_requests->re_request_type == 1)
                {
                    $requests_move++;
                } elseif ($row_affiliate_partner_requests->re_request_type == 2)
                {
                    $requests_baggage_service++;
                }

                if (($row_affiliate_partner_requests->afpare_status == 0 || $row_affiliate_partner_requests->afpare_status == 1) && !$row->cu_revenues_share)
                {
                    if($row_affiliate_partner_requests->afpare_amount_eur <= 0){
                        $costs += $system->currencyConvert($row_affiliate_partner_requests->afpare_amount, $row_affiliate_partner_requests->afpare_currency, "EUR");
                    }else{
                        $costs += $row_affiliate_partner_requests->afpare_amount_eur;

                    }
                }

                if ($row_affiliate_partner_requests->re_status == 1)
                {
                    $requests_matched++;
                } elseif ($row_affiliate_partner_requests->re_status == 2)
                {
                    $requests_rejected++;
                }
            }


            if (!empty($request_ids))
            {
                /*$query_request_customer_portal = KTRequestCustomerPortal::select("ktrecupo_free", "ktrecupo_amount_netto", "po_pacu_code")
                    ->join("portals", "ktrecupo_po_id", "po_id")
                    ->whereIn("ktrecupo_re_id", $request_ids)
                    ->get();*/

                $query_request_customer_portal = KTRequestCustomerPortal::select( "inli_amount_netto_eur")
                    ->join("invoice_lines", "inli_ktrecupo_id", "ktrecupo_id")
                    ->whereIn("ktrecupo_re_id", $request_ids)
                    ->get();

                foreach ($query_request_customer_portal as $row_request_customer_portal)
                {
                    $matches++;

                    if ($row_request_customer_portal->inli_amount_netto_eur < 0)
                    {
                        $matches_free++;

                        $free += $row_request_customer_portal->inli_amount_netto_eur;
                    }

                    $turnover += $row_request_customer_portal->inli_amount_netto_eur;

                }
            }

            if ($requests > 0)
            {
                $requests_move_percentage = (($requests_move > 0) ? (($requests_move / $requests) * 100) : 0);
                $requests_baggage_service_percentage = (($requests_baggage_service > 0) ? (($requests_baggage_service / $requests) * 100) : 0);

                $requests_matched_percentage = (($requests_matched > 0) ? (($requests_matched / $requests) * 100) : 0);
                $requests_rejected_percentage = (($requests_rejected > 0) ? (($requests_rejected / $requests) * 100) : 0);
            } else
            {
                $requests_move_percentage = 0;
                $requests_baggage_service_percentage = 0;

                $requests_matched_percentage = 0;
                $requests_rejected_percentage = 0;
            }

            if ($matches_free != 0 && $matches != 0)
            {
                $free_percentage = (($matches_free / $matches) * 100);
            } else
            {
                $free_percentage = 0;
            }

            if($row->cu_revenues_share && $costs == 0){
                $percent = ($row->cu_revenues_share/100);
                $costs = $turnover * $percent;
            }

            $margin = $turnover - $costs;

            if ($margin != 0 && $turnover != 0)
            {
                $margin_percentage = (($margin / $turnover) * 100);
            } else
            {
                $margin_percentage = 0;
            }
            $gross_revenues = $turnover - $free;

            $filtered_data[$row->cu_id] = [
                "id" => $row->cu_id,
                "affiliate_partner" => $row->cu_company_name_business,
                "status" => $affiliatepartnerstatuses[$row->afpada_status],
                "requests" => $requests,
                "matched" => $requests_matched,
                "percentage_matched" => System::numberFormat($requests_matched_percentage, 2) . "%",
                "percentage_rejected" => System::numberFormat($requests_rejected_percentage, 2) . "%",
                "percentage_moves" => System::numberFormat($requests_move_percentage, 2) . "%",
                "percentage_baggage" => System::numberFormat($requests_baggage_service_percentage, 2) . "%",
                "gross_revenues" => $system->paymentCurrencyToken("EUR") . " " . System::numberFormat($gross_revenues, 2),
                "free" => $system->paymentCurrencyToken("EUR") . " " . System::numberFormat($free, 2),
                "percentage_free" => System::numberFormat($free_percentage, 2) . "%",
                "revenues" => $system->paymentCurrencyToken("EUR") . " " . System::numberFormat($turnover, 2),
                "costs" => $system->paymentCurrencyToken("EUR") . " " . System::numberFormat($costs, 2),
                "margin" => $system->paymentCurrencyToken("EUR") . " " . System::numberFormat($margin, 2),
                "percentage_margin" => System::numberFormat($margin_percentage, 2) . "%",
            ];

            //START calculating totals
            $total_requests += $requests;

            $total_ap_requests_approved += $ap_requests_approved;
            $total_ap_requests_rejected += $ap_requests_rejected;
            $total_ap_requests_open += $ap_requests_open;

            $total_requests_move += $requests_move;
            $total_requests_baggage_service += $requests_baggage_service;

            $total_matches += $matches;
            $total_matches_free += $matches_free;
            $total_gross_revenue += $gross_revenues;
            $total_free += $free;
            $total_turnover += $turnover;
            $total_costs += $costs;

            $total_requests_matched += $requests_matched;
            $total_requests_rejected += $requests_rejected;
            //END calculating totals
        }

        if ($total_requests > 0)
        {
            $total_requests_move_percentage = (($total_requests_move > 0) ? (($total_requests_move / $total_requests) * 100) : 0);
            $total_requests_baggage_service_percentage = (($total_requests_baggage_service > 0) ? (($total_requests_baggage_service / $total_requests) * 100) : 0);

            $total_requests_matched_percentage = (($total_requests_matched > 0) ? (($total_requests_matched / $total_requests) * 100) : 0);
            $total_requests_rejected_percentage = (($total_requests_rejected > 0) ? (($total_requests_rejected / $total_requests) * 100) : 0);
        } else
        {
            $total_requests_move_percentage = 0;
            $total_requests_baggage_service_percentage = 0;

            $total_requests_matched_percentage = 0;
            $total_requests_rejected_percentage = 0;
        }

        if ($total_matches_free != 0 && $total_matches != 0)
        {
            $total_free_percentage = (($total_matches_free / $total_matches) * 100);
        } else
        {
            $total_free_percentage = 0;
        }

        $total_margin = $total_turnover - $total_costs;

        if ($total_margin != 0 && $total_turnover != 0)
        {
            $total_margin_percentage = (($total_margin / $total_turnover) * 100);
        } else
        {
            $total_margin_percentage = 0;
        }

        if (!empty($_POST)){
            self::insertReportUsageHistory($rep_id);
        }

        return view('reports.invoiced_affiliate_partners', [
            'report' => $report,
            'date_filter' => $date_filter,
            'filtered_data' => $filtered_data,

            'status_active_filter' => $status_active_filter,
            'status_inactive_filter' => $status_inactive_filter,

            //TOTALS
            'total_requests' => $total_requests,
            'total_requests_matched' => $total_requests_matched,
            'total_requests_matched_percentage' => System::numberFormat($total_requests_matched_percentage, 2) . "%",
            'total_requests_rejected_percentage' => System::numberFormat($total_requests_rejected_percentage, 2) . "%",
            'total_requests_move_percentage' => System::numberFormat($total_requests_move_percentage, 2) . "%",
            'total_requests_baggage_service_percentage' => System::numberFormat($total_requests_baggage_service_percentage, 2) . "%",
            'total_gross_revenue' => "€ " . System::numberFormat($total_gross_revenue, 2),
            'total_free' => "€ " . System::numberFormat($total_free, 2),
            'total_free_percentage' => System::numberFormat($total_free_percentage, 2) . "%",
            'total_turnover' => "€ " . System::numberFormat($total_turnover, 2),
            'total_costs' => "€ " . System::numberFormat($total_costs, 2),
            'total_margin' => "€ " . System::numberFormat($total_margin, 2),
            'total_margin_percentage' => System::numberFormat($total_margin_percentage, 2) . "%",
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function viewActiveCampaign(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);
        $countries_filter = [];
        $types_filter = [];
        $status_filter = [];
        $market_filter = [];
        $membership_filter = [];
        $filtered_data = [];

        if ($request->countries != null)
        {
            $countries_filter = $request->countries;
        }
        if ($request->markets != null)
        {
            $market_filter = $request->markets;
        }
        if ($request->statuses != null)
        {
            $status_filter = $request->statuses;
        }
        if ($request->memberships != null)
        {
            $membership_filter = $request->memberships;
        }
        if ($request->types != null)
        {
            $types_filter = $request->types;
        }

        $all_markets = CustomerMarketType::all();
        $all_services = CustomerServices::all();
        $all_statuses = CustomerStatusType::all();
        $all_countries = Country::orderBy("co_en", "ASC")->get();
        $languages = Language::all();

        $all_memberships = Membership::all();


        $all_languages = [];
        foreach ($languages as $lang) {
            $all_languages[$lang->la_code] = $lang->la_language;
        }

        $countries = [];

        foreach($all_countries as $country) {
            $countries[$country->co_code] = $country->co_en;
        }

        $customers = Customer::leftJoin("mover_data", "moda_cu_id", "cu_id")
            ->leftJoin("kt_customer_membership", "ktcume_cu_id", "cu_id")
            ->where("cu_deleted", 0);

        if (!empty($status_filter)) {
            $customers = $customers->whereIn("moda_crm_status", array_values($status_filter));
        }

        if (!empty($market_filter)) {
            $customers = $customers->whereIn("moda_market_type", array_values($market_filter));
        }

        if (!empty($request->countries)) {
            $customers = $customers->whereIn("cu_co_code", $request->countries);
        }

        if (!empty($request->memberships)) {
            $customers = $customers->whereIn("ktcume_me_id", $request->memberships);
        }

        if (!empty($request->types)) {
            $customers = $customers->whereIn("cu_type", $request->types);
        }

        $customers = $customers->orderBy("cu_company_name_business", "ASC")
            ->get();

        $list_of_cu_ids_and_cope = [];
        foreach ($customers as $customer) {
            $memberships = KTCustomerMembership::where("ktcume_cu_id", $customer->cu_id)->first();

            $continent = MacroRegion::leftJoin("countries", "co_mare_id", "mare_id")->where("co_code", $customer->cu_co_code)->first()->mare_continent_name;

            $contactpersons = ContactPerson::where("cope_email", "!=", "")
                ->where("cope_deleted", 0)
                ->where("cope_cu_id", $customer->cu_id)
                ->get();

            $region = "";

            $region_result = Region::select("reg_co_code", "reg_parent", "reg_name", "co_en")
                ->leftJoin("countries", "reg_co_code", "co_code")
                ->where("reg_id", $customer->moda_reg_id)
                ->first();

            if (count($region_result) > 0) {
                $region = $region_result->reg_parent;
            }

            if (count($contactpersons) > 0) {
                foreach ($contactpersons as $cp) {

                    if (!in_array(trim($cp->cope_email), $list_of_cu_ids_and_cope[$cp->cope_cu_id])) {
                        $list_of_cu_ids_and_cope[$cp->cope_cu_id][] = trim($cp->cope_email);

                        $filtered_data[] = [
                            'company_name' => $customer->cu_company_name_business,
                            'country' => $countries[$customer->cu_co_code],
                            'continent' => $continent,
                            'market' => $all_markets[$customer->moda_market_type],
                            'services' => $all_services[$customer->moda_services],
                            'associations' => ((count($memberships) > 0) ? "Yes" : "No"),
                            'status' => $all_statuses[$customer->moda_crm_status],
                            'title' => $cp->cope_title,
                            'first_name' => $cp->cope_first_name,
                            'last_name' => $cp->cope_last_name,
                            'email' => trim($cp->cope_email),
                            'language' => $all_languages[$cp->cope_language],
                            'region' => $region
                        ];
                    }
                }
            }
            else {
                if (!empty($customer->cu_attn_email) && !in_array(trim($customer->cu_attn_email), $list_of_cu_ids_and_cope[$customer->cu_id])) {
                    $list_of_cu_ids_and_cope[$customer->cu_id][] = trim($customer->cu_attn_email);
                    $filtered_data[] = [
                        'company_name' => $customer->cu_company_name_business,
                        'country' => $countries[$customer->cu_co_code],
                        'continent' => $continent,
                        'market' => $all_markets[$customer->moda_market_type],
                        'services' => $all_services[$customer->moda_services],
                        'associations' => ((count($memberships) > 0) ? "Yes" : "No"),
                        'status' => $all_statuses[$customer->moda_crm_status],
                        'title' => "",
                        'first_name' => "",
                        'last_name' => "",
                        'email' => trim($customer->cu_attn_email),
                        'language' => $all_languages[$customer->cu_la_code],
                        'region' => $region
                    ];
                }
            }
        }

        if (!empty($_POST)){
            self::insertReportUsageHistory($rep_id);
        }

        return view('reports.active_campaign', [
            'report' => $report,
            'types_filter' => $types_filter,
            'countries_filter' => $countries_filter,
            'market_filter' => $market_filter,
            'status_filter' => $status_filter,
            'membership_filter' => $membership_filter,
            'filtered_data' => $filtered_data,

            //DATA
            'countries' => $all_countries,
            'markets' => $all_markets,
            'statuses' => $all_statuses,
            'all_memberships' => $all_memberships,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function viewMatchingSpeed(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);

        $date_filter = null;

        if ($request->date != null)
        {
            $date_filter = $request->date;
        } else
        {
            return Redirect::back()->withErrors(['Please select a date']);
        }

        $filtered_data = [];
        $total_lead_picks = 0;
        $dataindex = new DataIndex();

        $total['total_leads'] = 0;
        $total['matched'] = 0;
        $total['rejected'] = 0;
        $total['within_15_min'] = 0;
        $total['between_15_30_min'] = 0;
        $total['between_30_min_to_1_hour'] = 0;
        $total['between_1_to_2_hours'] = 0;
        $total['between_2_to_4_hours'] = 0;
        $total['between_4_to_12_hours'] = 0;
        $total['over_12_hours'] = 0;
        $users = [];

        $users_list = User::get();
        foreach ($users_list as $user)
        {
            $users[$user->us_id] = [
                "employee" => $user->us_name,
                "total_leads" => 0,
                "matched" => 0,
                "rejected" => 0,
                "within_15_min" => 0,
                "between_15_30_min" => 0,
                "between_30_min_to_1_hour" => 0,
                "between_1_to_2_hours" => 0,
                "between_2_to_4_hours" => 0,
                "between_4_to_12_hours" => 0,
                "over_12_hours" => 0,
            ];
        }

        $query_matching_speed = \App\Models\Request::leftJoin("kt_request_customer_portal", "re_id", "ktrecupo_re_id")
            ->leftJoin("users", "us_id", "ktrecupo_us_id")
            ->whereBetween("re_timestamp", System::betweenDates($request->date))
            ->whereRaw("(`re_status` = 1 OR `re_status` = 2)")
            ->where("re_pd_validate", 0)
            ->where("re_rejection_reason", "!=", "16")
            ->groupBy("re_id")
            ->get();

        foreach ($query_matching_speed as $matching_speed)
        {
            //Counter of total matched leads
            $total['total_leads'] += 1;

            if ($matching_speed->re_status == 1)
            {
                $us_id = $matching_speed->ktrecupo_us_id;

                $total['matched'] += 1;
                $users[$us_id]['matched'] += 1;

                $since_datetime = $matching_speed->ktrecupo_timestamp;
            }

            if ($matching_speed->re_status == 2)
            {
                $us_id = $matching_speed->re_rejection_us_id;

                $total['rejected'] += 1;
                $users[$us_id]['rejected'] += 1;

                $since_datetime = $matching_speed->re_rejection_timestamp;
            }

            $users[$us_id]['total_leads'] += 1;

            //Calculate minutes
            $received_date = new DateTime($matching_speed->re_timestamp);
            $since_received = $received_date->diff(new DateTime($since_datetime));

            $minutes = $since_received->days * 24 * 60;
            $minutes += $since_received->h * 60;
            $minutes += $since_received->i;

            //Within 15 min
            if ($minutes <= 15)
            {
                $total['within_15_min'] += 1;
                $users[$us_id]['within_15_min'] += 1;
            }

            //Between 15-30 min
            if ($minutes > 15 && $minutes <= 30)
            {
                $total['between_15_30_min'] += 1;
                $users[$us_id]['between_15_30_min'] += 1;
            }

            //Between 30 min and 1 hour
            if ($minutes > 30 && $minutes <= 60)
            {
                $total['between_30_min_to_1_hour'] += 1;
                $users[$us_id]['between_30_min_to_1_hour'] += 1;
            }

            //Between 1 - 2 hours
            if ($minutes > 60 && $minutes <= 120)
            {
                $total['between_1_to_2_hours'] += 1;
                $users[$us_id]['between_1_to_2_hours'] += 1;
            }

            //Between 2 - 4 hours
            if ($minutes > 120 && $minutes <= 240)
            {
                $total['between_2_to_4_hours'] += 1;
                $users[$us_id]['between_2_to_4_hours'] += 1;
            }

            //Between 4 - 12 hours
            if ($minutes > 240 && $minutes <= 720)
            {
                $total['between_4_to_12_hours'] += 1;
                $users[$us_id]['between_4_to_12_hours'] += 1;
            }

            //Over 12 hours
            if ($minutes > 720)
            {
                $total['over_12_hours'] += 1;
                $users[$us_id]['over_12_hours'] += 1;
            }
        }

        if (!empty($users))
        {
            foreach ($users as $id => $user)
            {
                if ($user['total_leads'] > 10)
                {
                    $within_15_min = round((100 / $user['total_leads']) * $user['within_15_min'], 1) . " %";
                    $between_15_30_min = round((100 / $user['total_leads']) * $user['between_15_30_min'], 1) . " %";
                    $between_30_min_to_1_hour = round((100 / $user['total_leads']) * $user['between_30_min_to_1_hour'], 1) . " %";
                    $between_1_to_2_hours = round((100 / $user['total_leads']) * $user['between_1_to_2_hours'], 1) . " %";
                    $between_2_to_4_hours = round((100 / $user['total_leads']) * $user['between_2_to_4_hours'], 1) . " %";
                    $between_4_to_12_hours = round((100 / $user['total_leads']) * $user['between_4_to_12_hours'], 1) . " %";
                    $over_12_hours = round((100 / $user['total_leads']) * $user['over_12_hours'], 1) . " %";

                    $filtered_data[$id] = [
                        "employee" => $user['employee'],
                        "total_leads" => $user['total_leads'],
                        "matched" => $user['matched'],
                        "rejected" => $user['rejected'],
                        "percent_matched_within_15_min" => $within_15_min,
                        "percent_matched_betweem_15_30_min" => $between_15_30_min,
                        "percent_matched_between_30_min_1_hour" => $between_30_min_to_1_hour,
                        "percent_matched_between_1_2_hour" => $between_1_to_2_hours,
                        "percent_matched_between_2_4_hour" => $between_2_to_4_hours,
                        "percent_matched_between_4_12_hour" => $between_4_to_12_hours,
                        "percent_matched_over_12_hours" => $over_12_hours,
                    ];
                } else
                {
                    $total['total_leads'] = $total['total_leads'] - $user['total_leads'];
                }
            }
        }

        $total_within_15 = round((100 / $total['total_leads']) * $total['within_15_min'], 1) . " %";
        $total_between_15_30 = round((100 / $total['total_leads']) * $total['between_15_30_min'], 1) . " %";
        $total_between_30_min_to_1_hour = round((100 / $total['total_leads']) * $total['between_30_min_to_1_hour'], 1) . " %";
        $total_between_1_to_2_hours = round((100 / $total['total_leads']) * $total['between_1_to_2_hours'], 1) . " %";
        $total_between_2_to_4_hours = round((100 / $total['total_leads']) * $total['between_2_to_4_hours'], 1) . " %";
        $total_between_4_to_12_hours = round((100 / $total['total_leads']) * $total['between_4_to_12_hours'], 1) . " %";
        $total_over_12_hours = round((100 / $total['total_leads']) * $total['over_12_hours'], 1) . " %";

        if (!empty($_POST)){
            self::insertReportUsageHistory($rep_id);
        }

        return view('reports.matching_speed', [
            'report' => $report,
            'date_filter' => $date_filter,
            'filtered_data' => $filtered_data,

            //TOTALS
            'total_leads' => $total['total_leads'],
            'total_matched' => $total['matched'],
            'total_rejected' => $total['rejected'],
            'total_within_15' => $total_within_15,
            'total_between_15_30' => $total_between_15_30,
            'total_between_30_min_to_1_hour' => $total_between_30_min_to_1_hour,
            'total_between_1_to_2_hours' => $total_between_1_to_2_hours,
            'total_between_2_to_4_hours' => $total_between_2_to_4_hours,
            'total_between_4_to_12_hours' => $total_between_4_to_12_hours,
            'total_over_12_hours' => $total_over_12_hours,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function viewLeadMonitoring(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);

        $date_filter = null;
        $type_filter = null;
        $destination_type_filter = null;

        if ($request->date != null)
        {
            $date_filter = $request->date;
        } else
        {
            return Redirect::back()->withErrors(['Please select a date']);
        }

        if ($request->type != null)
        {
            $type_filter  = $request->type;
        }

        if ($request->destination_type != null)
        {
            $destination_type_filter = $request->destination_type;
        }

        $filtered_data = [];

        $total['total'] = 0;
        $total['0_1_month'] = 0;
        $total['1_2_month'] = 0;
        $total['2_3_month'] = 0;
        $total['3_4_month'] = 0;
        $total['more_than_4_months'] = 0;


        $requests = \App\Models\Request::leftJoin("website_forms", "wefo_id", "re_wefo_id")
            ->leftJoin("websites", "we_id", "wefo_we_id");

        $requests = $requests->whereBetween("re_timestamp", System::betweenDates($request->date));

        if ($request->destination_type == 'int')
        {
            $requests = $requests->where("re_destination_type", 1);
        }
        elseif ($request->destination_type == 'nat')
        {
            $requests = $requests->where("re_destination_type", 2);
        }

        if ($request->type == 'sea')
        {
            $requests = $requests->where("we_sirelo", 0);
        }
        elseif($request->type == 'seo') {
            $requests = $requests->where("we_sirelo", 1);
        }
        elseif($request->type == 'affiliate') {
            $requests = $requests->whereRaw("re_afpafo_id IS NOT NULL");
        }

        $requests = $requests->get();

        $countries = Country::all();
        foreach ($requests as $request)
        {
            $country = Country::where("co_code", $request->re_co_code_from)->first()->co_en;
            if(!isset($filtered_data[$country]))
            {
                $filtered_data[$country] = [
                    '0_30_days' => 0,
                    '30_60_days' => 0,
                    '0_60_days' => 0,
                    '0_90_days' => 0,
                    '0_120_days' => 0,
                    '120_plus_days' => 0,
                ];
            }

            $date1 = new DateTime($request->re_timestamp);
            $date2 = new DateTime($request->re_moving_date);

            $diff = $date1->diff($date2);

            $days_diff = $date2->diff($date1)->format("%a");

            if ($days_diff > 0 && $days_diff <= 30)
            {
                $filtered_data[$country]['0_30_days']++;
                $total['0_30_days']++;
            }

            if ($days_diff > 30 && $days_diff <= 60)
            {
                $filtered_data[$country]['30_60_days']++;
                $total['30_60_days']++;
            }

            if ($days_diff > 0 && $days_diff <= 60)
            {
                $filtered_data[$country]['0_60_days']++;
                $total['0_60_days']++;
            }

            if ($days_diff > 0 && $days_diff <= 90)
            {
                $filtered_data[$country]['0_90_days']++;
                $total['0_90_days']++;
            }

            if ($days_diff > 0 && $days_diff <= 120)
            {
                $filtered_data[$country]['0_120_days']++;
                $total['0_120_days']++;
            }

            if ($days_diff > 120)
            {
                $filtered_data[$country]['120_plus_days']++;
                $total['120_plus_days']++;
            }

            $total['total']++;
            $filtered_data[$country]['total']++;
        }

        if (!empty($_POST)){
            self::insertReportUsageHistory($rep_id);
        }

        return view('reports.lead_monitoring', [
            'report' => $report,
            'date_filter' => $date_filter,
            'type_filter' => $type_filter,
            'destination_type_filter' => $destination_type_filter,
            'filtered_data' => $filtered_data,
            'countries' => $countries,
            'total' => $total,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function viewAutomaticMatching(Request $request, $rep_id)
    {
        $system = new System();
        $report = Report::findOrFail($rep_id);

        if ($request->date == null)
        {
            return Redirect::back()->withErrors(['Please select a date']);
        }

        $automated_requests = AutomatedRequest::leftJoin("requests", "aure_re_id", "re_id")
            ->whereBetween("aure_timestamp", System::betweenDates($request->date));

        if ($request->type == 'matched')
        {
            $automated_requests->where("aure_decision", AutomatedDecision::AUTOMATIC_MATCH);
        }
        elseif($request->type == 'rejected') {
            $automated_requests->where("aure_decision", AutomatedDecision::AUTOMATIC_REJECT);
        }
        elseif($request->type == 'sent_back') {
            $automated_requests->where("aure_decision", AutomatedDecision::MANUAL);
        }

        $automated_requests = $automated_requests->get();

        //Get Changes
        $portal_rev_types = ["App\Models\Request", "TelephoneAPI"];
        $request_ignore_fields = ["re_on_hold", "re_automatic", "re_automatic_checked", "re_on_hold_by", "re_rejection_timestamp", "re_rejection_us_id", "re_rejection_reason", "re_match_rating", "re_status"];


        foreach($automated_requests as $id => $automated_request)
        {
            $revisions = Revision::whereIn("revisionable_type", $portal_rev_types)->where("revisionable_id", "=", $automated_request->re_id)->whereNotIn("key", $request_ignore_fields)->get();
            $changes = [];

            foreach ($revisions as $revision)
            {
                if(trim($revision->old_value) != trim($revision->new_value))
                {
                   /* if($revision->key == "re_reg_id_to"){
                        //Get Region Suggestion
                        $regions_to = $system->findRegionsByDestinationCity($automated_request->re_city_to, $automated_request->re_destination_type, $automated_request->re_co_code_to);

                        $changes["region_suggestion_to"] = [
                            "suggested" => $regions_to[0]->re_reg_id_to
                        ];
                    }

                    if($revision->key == "re_reg_id_from"){
                        //Get Region Suggestion
                        $regions_from = $system->findRegionsByOriginCity($automated_request->re_city_from, $automated_request->re_destination_type, $automated_request->re_co_code_from);

                        $changes["region_suggestion_from"] = [
                            "suggested" => $regions_from[0]->re_reg_id_from
                        ];
                    }*/

                    $changes[$revision->key] = [
                        "old" => $revision->old_value,
                        "new" => $revision->new_value
                    ];
                }
            }

            $automated_requests[$id]->revisions = $changes;

        }

        if (!empty($_POST)){
            self::insertReportUsageHistory($rep_id);
        }

        return view('reports.automatic_matching', [
            'report' => $report,
            'date_filter' => $request->date,
            'type_filter' => $request->type,
            'filtered_data' => $automated_requests,
            'decisions' => AutomatedDecision::all(),
            'requeststatuses' => RequestStatus::all(),
            'rejectionreasons' => RejectionReason::all(),
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function viewLeadMonitoringPerSite(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);

        $date_filter = null;
        $destination_type_filter = null;

        if ($request->date != null)
        {
            $date_filter = $request->date;
        } else
        {
            return Redirect::back()->withErrors(['Please select a date']);
        }

        if ($request->destination_type != null)
        {
            $destination_type_filter = $request->destination_type;
        }

        $filtered_data = [];

        $total['total'] = 0;
        $total['0_1_month'] = 0;
        $total['1_2_month'] = 0;
        $total['2_3_month'] = 0;
        $total['3_4_month'] = 0;
        $total['more_than_4_months'] = 0;


        $requests = \App\Models\Request::leftJoin("website_forms", "wefo_id", "re_wefo_id")
            ->leftJoin("websites", "we_id", "wefo_we_id");

        $requests = $requests->where("we_sirelo", 0);

        $requests = $requests->whereBetween("re_timestamp", System::betweenDates($request->date));

        if ($request->destination_type == 'int')
        {
            $requests = $requests->where("re_destination_type", 1);
        }
        elseif ($request->destination_type == 'nat')
        {
            $requests = $requests->where("re_destination_type", 2);
        }

        $requests = $requests->get();


        foreach ($requests as $request)
        {
            if(!isset($filtered_data[$request->we_website]))
            {
                $filtered_data[$request->we_website] = [
                    '0_30_days' => 0,
                    '30_60_days' => 0,
                    '0_60_days' => 0,
                    '0_90_days' => 0,
                    '0_120_days' => 0,
                    '120_plus_days' => 0,
                ];
            }

            $date1 = new DateTime($request->re_timestamp);
            $date2 = new DateTime($request->re_moving_date);
            $days_diff = $date2->diff($date1)->format("%a");

            if ($days_diff > 0 && $days_diff <= 30)
            {
                $filtered_data[$request->we_website]['0_30_days']++;
                $total['0_30_days']++;
            }

            if ($days_diff > 30 && $days_diff <= 60)
            {
                $filtered_data[$request->we_website]['30_60_days']++;
                $total['30_60_days']++;
            }

            if ($days_diff > 0 && $days_diff <= 60)
            {
                $filtered_data[$request->we_website]['0_60_days']++;
                $total['0_60_days']++;
            }

            if ($days_diff > 0 && $days_diff <= 90)
            {
                $filtered_data[$request->we_website]['0_90_days']++;
                $total['0_90_days']++;
            }

            if ($days_diff > 0 && $days_diff <= 120)
            {
                $filtered_data[$request->we_website]['0_120_days']++;
                $total['0_120_days']++;
            }

            if ($days_diff > 120)
            {
                $filtered_data[$request->we_website]['120_plus_days']++;
                $total['120_plus_days']++;
            }

            $total['total']++;
            $filtered_data[$request->we_website]['total']++;
        }

        if (!empty($_POST)){
            self::insertReportUsageHistory($rep_id);
        }

        return view('reports.lead_monitoring_per_site', [
            'report' => $report,
            'date_filter' => $date_filter,
            'destination_type_filter' => $destination_type_filter,
            'filtered_data' => $filtered_data,
            'total' => $total,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function viewPerformancePerEmployee(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);

        $date_filter = null;

        if ($request->date != null)
        {
            $date_filter = $request->date;
        } else
        {
            return Redirect::back()->withErrors(['Please select a date']);
        }

        $total_new_booking = 0;
        $total_count_new_booking = 0;
        $total_upsell = 0;
        $total_count_upsell = 0;
        $total_reactivation = 0;
        $total_count_reactivation = 0;
        $total_pause = 0;
        $total_count_pause = 0;
        $total_unpause = 0;
        $total_count_unpause = 0;
        $total_credithold = 0;
        $total_count_credithold = 0;
        $total_out_of_credithold = 0;
        $total_count_out_of_credithold = 0;
        $total_cancellation = 0;
        $total_count_cancellation = 0;
        $total_downsell = 0;
        $total_count_downsell = 0;
        $total_total = 0;
        $total_count_total = 0;

        $filtered_data = [];

        $dataindex = new DataIndex();
        $system = new System();

        foreach ($dataindex->customerStatuses() as $key => $value)
        {
            ${"total_turnover" . $key} = 0;
            ${"total_count_" . $key} = 0;
        }

        $total_turnover_total = 0;
        $total_count_total = 0;

        foreach ($dataindex->salesUsers() as $user_id => $user_name)
        {

            $total_turnover = 0;
            $total_count = 0;
            $filtered_data[$user_id] = [
                'id' => $user_id,
                'employee' => $user_name,
                'new_booking' => 0,
                'new_booking_count' => 0,
                'upsell' => 0,
                'upsell_count' => 0,
                'reactivation' => 0,
                'reactivation_count' => 0,
                'pause' => 0,
                'pause_count' => 0,
                'unpause' => 0,
                'unpause_count' => 0,
                'credithold' => 0,
                'credithold_count' => 0,
                'out_of_credithold' => 0,
                'out_of_credithold_count' => 0,
                'cancellation' => 0,
                'cancellation_count' => 0,
                'downsell' => 0,
                'downsell_count' => 0,
            ];

            $filtered_data[$user_id]['id'] = $user_id;
            $filtered_data[$user_id]['employee'] = $user_name;

            foreach ($dataindex->customerStatuses() as $key => $value)
            {
                $turnover = 0;
                $count = 0;

                $query_customer_statuses = CustomerStatus::
                whereBetween("cust_date", System::betweenDatesWithoutTime($request->date))
                    ->where("cust_employee", $user_id)
                    ->where("cust_status", $key)
                    ->get();

                foreach ($query_customer_statuses as $row_customer_statuses)
                {
                    $turnover += $row_customer_statuses->cust_turnover_netto;

                    $count++;
                }

                $total_turnover += $turnover;
                $total_count += $count;

                if ($key == 1)
                {
                    $total_new_booking += $turnover;
                    $total_count_new_booking += $count;

                    $filtered_data[$user_id]['new_booking'] += $turnover;
                    $filtered_data[$user_id]['new_booking_count'] += $count;
                } elseif ($key == 2)
                {
                    $total_upsell += $turnover;
                    $total_count_upsell += $count;

                    $filtered_data[$user_id]['upsell'] += $turnover;
                    $filtered_data[$user_id]['upsell_count'] += $count;
                } elseif ($key == 3)
                {
                    $total_reactivation += $turnover;
                    $total_count_reactivation += $count;

                    $filtered_data[$user_id]['reactivation'] += $turnover;
                    $filtered_data[$user_id]['reactivation_count'] += $count;
                } elseif ($key == 4)
                {
                    $total_pause += $turnover;
                    $total_count_pause += $count;

                    $filtered_data[$user_id]['pause'] += $turnover;
                    $filtered_data[$user_id]['pause_count'] += $count;
                } elseif ($key == 5)
                {
                    $total_unpause += $turnover;
                    $total_count_unpause += $count;

                    $filtered_data[$user_id]['unpause'] += $turnover;
                    $filtered_data[$user_id]['unpause_count'] += $count;
                } elseif ($key == 6)
                {
                    $total_credithold += $turnover;
                    $total_count_credithold += $count;

                    $filtered_data[$user_id]['credithold'] += $turnover;
                    $filtered_data[$user_id]['credithold_count'] += $count;
                } elseif ($key == 7)
                {
                    $total_out_of_credithold += $turnover;
                    $total_count_out_of_credithold += $count;

                    $filtered_data[$user_id]['out_of_credithold'] += $turnover;
                    $filtered_data[$user_id]['out_of_credithold_count'] += $count;
                } elseif ($key == 8)
                {
                    $total_cancellation += $turnover;
                    $total_count_cancellation += $count;

                    $filtered_data[$user_id]['cancellation'] += $turnover;
                    $filtered_data[$user_id]['cancellation_count'] += $count;
                } elseif ($key == 9)
                {
                    $total_downsell += $turnover;
                    $total_count_downsell += $count;

                    $filtered_data[$user_id]['downsell'] += $turnover;
                    $filtered_data[$user_id]['downsell_count'] += $count;
                }

                $total_turnover_total += $turnover;
                $total_count_total += $count;
            }

            $filtered_data[$user_id]['total'] = $total_turnover;
            $filtered_data[$user_id]['total_count'] = $total_count;
        }

        if (!empty($_POST)){
            self::insertReportUsageHistory($rep_id);
        }

        return view('reports.performance_per_employee', [
            'report' => $report,
            'date_filter' => $date_filter,
            'filtered_data' => $filtered_data,

            //TOTALS
            'total_new_booking' => $total_new_booking,
            'total_count_new_booking' => $total_count_new_booking,
            'total_upsell' => $total_upsell,
            'total_count_upsell' => $total_count_upsell,
            'total_reactivation' => $total_reactivation,
            'total_count_reactivation' => $total_count_reactivation,
            'total_pause' => $total_pause,
            'total_count_pause' => $total_count_pause,
            'total_unpause' => $total_unpause,
            'total_count_unpause' => $total_count_unpause,
            'total_credithold' => $total_credithold,
            'total_count_credithold' => $total_count_credithold,
            'total_out_of_credithold' => $total_out_of_credithold,
            'total_count_out_of_credithold' => $total_count_out_of_credithold,
            'total_cancellation' => $total_cancellation,
            'total_count_cancellation' => $total_count_cancellation,
            'total_downsell' => $total_downsell,
            'total_count_downsell' => $total_count_downsell,
            'total_total' => $total_turnover_total,
            'total_count_total' => $total_count_total,
            'times_used' => self::getTimesUsedLast90Days($rep_id)

        ]);
    }

    public function viewPortalStatusHistory(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);

        $date_filter = null;
        $portal_filter = null;

        if ($request->date != null)
        {
            $date_filter = $request->date;
        } else
        {
            return Redirect::back()->withErrors(['Please select a date']);
        }

        if ($request->portal != null)
        {
            $portal_filter = $request->portal;
        }

        $filtered_data = [];

        $result = StatusHistory::leftJoin("customers", "sthi_cu_id", "cu_id")
            ->leftJoin("kt_customer_portal", "sthi_ktcupo_id", "ktcupo_id")
            ->leftJoin("portals", "ktcupo_po_id", "po_id")
            ->whereBetween("sthi_timestamp", System::betweenDates($request->date))
            ->where("cu_deleted", 0);

        if ($portal_filter != null)
        {
            $result = $result->where("ktcupo_po_id", $portal_filter);
        }

        $result = $result->get();

        foreach ($result as $row)
        {

            $filtered_data[$row->sthi_id] = [
                "id" => $row->sthi_id,
                "portal_id" => $row->ktcupo_id,
                "timestamp" => $row->sthi_timestamp,
                "customer" => $row->cu_company_name_business,
                "type" => StatusHistoryType::all()[$row->sthi_type],
                "changed_by" => (($row->sthi_us_id == 0) ? "<span style=\"color: #FF0000;\">System</span>" : User::select("us_name")->where("us_id", $row->sthi_us_id)->first()->us_name),
                "portal" => $row->po_portal . " (" . DataIndex::requestTypes()[$row->ktcupo_request_type] . ") - " . $row->ktcupo_description,
                "status_from" => DataIndex::customerPairStatuses()[$row->sthi_status_from],
                "status_to" => DataIndex::customerPairStatuses()[$row->sthi_status_to],
            ];
        }

        if (!empty($_POST)){
            self::insertReportUsageHistory($rep_id);
        }

        return view('reports.portal_status_history', [
            'report' => $report,
            'date_filter' => $date_filter,
            'portal_filter' => $portal_filter,
            'filtered_data' => $filtered_data,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function viewRequestQuality(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);
        $system = new System();

        $date_filter = null;
        $groupby_filter = null;

        if ($request->date != null)
        {
            $date_filter = $request->date;
        } else
        {
            return Redirect::back()->withErrors(['Please select a date']);
        }

        if ($request->groupby != null)
        {
            $groupby_filter = $request->groupby;
        }

        $filtered_data = [];

        if ($request->groupby == 'country')
        {
            $outerQuery = \App\Models\Request::leftJoin("countries", "re_co_code_from", "co_code")
                ->whereBetween("re_timestamp", System::betweenDates($request->date))
                ->groupBy("re_co_code_from")
                ->get();
        } elseif ($request->groupby == 'website')
        {
            $outerQuery = WebsiteForm::select("wefo_id", "wefo_we_id", "we_website")
                ->leftJoin("websites", "wefo_we_id", "we_id")
                ->groupBy("wefo_id")
                ->get();

            $outerQuery = $system->databaseToArray($outerQuery);

            // Set default result
            $result = [];

            if (!empty($outerQuery))
            {

                // Loop website forms
                foreach ($outerQuery as $single)
                {
                    // If the website id is already in the results
                    if (isset($result[$single['wefo_we_id']]))
                    {
                        // If the website form ids is an array
                        if (is_array($result[$single['wefo_we_id']]['wefo_id']))
                        {
                            // Add single website form id to the array
                            $result[$single['wefo_we_id']]['wefo_id'][] = $single['wefo_id'];
                        } else
                        {
                            // Add single website and create an array
                            $result[$single['wefo_we_id']]['wefo_id'] = [$result[$single['wefo_we_id']]['wefo_id'], $single['wefo_id']];
                        }
                    } else
                    {
                        // Website id is not in the results
                        $result[$single['wefo_we_id']] = $single;
                    }
                }
            }

            // Turn results in outerquery
            $outerQuery = $result;
        } elseif ($request->groupby == 'form')
        {
            $outerQuery = \App\Models\Request::select("re_wefo_id", "wefo_we_id", "wefo_name", "we_website")
                ->leftJoin("website_forms", "re_wefo_id", "wefo_id")
                ->leftJoin("websites", "wefo_we_id", "we_id")
                ->whereBetween("re_timestamp", System::betweenDates($request->date))
                ->groupBy("re_wefo_id")
                ->get();
        } elseif ($request->groupby == 'portal')
        {
            $outerQuery = \App\Models\Request::select("re_po_id", "po_portal")
                ->leftJoin("portals", "re_po_id", "po_id")
                ->whereBetween("re_timestamp", System::betweenDates($request->date))
                ->groupBy("re_po_id")
                ->get();
        } elseif ($request->groupby == 'user')
        {
            $outerQuery = KTRequestCustomerPortal::select("ktrecupo_re_id", "ktrecupo_us_id", "re_timestamp", "us_name")
                ->leftJoin("requests", "ktrecupo_re_id", "re_id")
                ->leftJoin("users", "ktrecupo_us_id", "us_id")
                ->whereRaw("`ktrecupo_us_id` is not NULL")
                ->whereBetween("re_timestamp", System::betweenDates($request->date))
                ->groupBy("ktrecupo_re_id")
                ->get();

            if (count($outerQuery) > 0)
            {

                $result = [];

                foreach ($outerQuery as $row)
                {
                    if (isset($result[$row->ktrecupo_us_id]['requests']))
                    {
                        $result[$row->ktrecupo_us_id]['requests'][] = $row->ktrecupo_re_id;
                    } else
                    {
                        $result[$row->ktrecupo_us_id]['requests'] = [$row->ktrecupo_re_id];
                    }

                    // Store name
                    $result[$row->ktrecupo_us_id]['us_name'] = $row->us_name;
                }
                // Turn results in outerquery
                $outerQuery = $result;
            }
        }

        foreach ($outerQuery as $row)
        {

            Log::debug($row->we_website);

            $query = \App\Models\Request::selectRaw("COUNT(*) as `amount`, `re_score`")
                ->whereBetween("re_timestamp", System::betweenDates($request->date));

            if ($request->groupby == 'country')
            {
                $query = $query->where("re_co_code_from", $row->re_co_code_from);
            } elseif ($request->groupby == 'website')
            {
                // Wefo id could be array or integer
                if (is_array($row['wefo_id']))
                {
                    $query = $query->whereIn("re_wefo_id", $row['wefo_id']);
                } else
                {
                    $query = $query->where("re_wefo_id", $row['wefo_id']);
                }
            } elseif ($request->groupby == 'form')
            {
                $query = $query->where("re_wefo_id", $row->re_wefo_id);
            } elseif ($request->groupby == 'portal')
            {
                $query = $query->where("re_po_id", $row->re_po_id);
            } elseif ($request->groupby == 'user')
            {
                $query = $query->whereIn("re_id", $row['requests']);
            }

            $query = $query->groupBy("re_score")->get();

            Log::debug($query);
            Log::debug("---------------------------------------------------------------");

            // Set defaults
            $count = 0;
            $sum = 0;
            $requests = 0;

            // Loop query
            foreach ($query as $total)
            {
                // Add requests total
                $requests = $requests + $total->amount;

                // Loop amount of times
                for ($i = 0; $i <= $total->amount; $i++)
                {
                    // Add one to count
                    $count++;

                    // Add score to sum
                    $sum = $sum + $total->re_score;
                }
            }

            // If either sum or count is 0
            if ($sum == 0 || $count == 0)
                continue;

            // Calculate average and round to 1 decimal
            $average = round($sum / $count, 1);

            // How to format the table rows
            if ($request->groupby == 'country')
            {
                $filtered_data[] = [
                    "groupby" => $row->co_en,
                    "average" => $average,
                    "requests" => $requests
                ];
            } elseif ($request->groupby == 'website')
            {
                $filtered_data[] = [
                    "groupby" => $row['we_website'],
                    "average" => $average,
                    "requests" => $requests
                ];
            } elseif ($request->groupby == 'form')
            {
                if (!empty($row->we_website) && !empty($row->wefo_name))
                {
                    $filtered_data[] = [
                        "groupby" => $row->we_website . ' - ' . $row->wefo_name,
                        "average" => $average,
                        "requests" => $requests
                    ];
                }
            } elseif ($request->groupby == 'portal')
            {
                $filtered_data[] = [
                    "groupby" => $row->po_portal,
                    "average" => $average,
                    "requests" => $requests
                ];
            } elseif ($request->groupby == 'user')
            {
                $filtered_data[] = [
                    "groupby" => $row['us_name'],
                    "average" => $average,
                    "requests" => $requests
                ];
            }
        }

        if (!empty($_POST)){
            self::insertReportUsageHistory($rep_id);
        }

        return view('reports.request_quality', [
            'report' => $report,
            'date_filter' => $date_filter,
            'groupby_filter' => $groupby_filter,
            'filtered_data' => $filtered_data,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function viewReviewsPerCustomer(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);

        $date_filter = null;
        $customer_filter = null;
        $moverstatus_filter = null;
        $sirelo_filter = null;

        if ($request->date != null)
        {
            $date_filter = $request->date;
        } else
        {
            return Redirect::back()->withErrors(['Please select a date']);
        }

        if ($request->customer != null)
        {
            $customer_filter = $request->customer;
        }

        if ($request->mover_status != null)
        {
            $moverstatus_filter = $request->mover_status;
        }

        if ($request->sirelo != null)
        {
            $sirelo_filter = $request->sirelo;
        }

        $filtered_data = [];

        $query = Customer::select("cu_id", "cu_company_name_business", "co_en", "cu_type")
            ->leftJoin("countries", "cu_co_code", "co_code")
            ->leftJoin("mover_data", "cu_id", "moda_cu_id");

        if ($sirelo_filter != null)
        {
            if ($sirelo_filter == 'XX')
            {
                $query = $query->leftJoin("websites", "we_sirelo_co_code", "cu_co_code")
                    ->whereRaw("cu_co_code NOT IN (SELECT we_sirelo_co_code FROM websites WHERE we_sirelo = 1 AND we_sirelo_co_code IS NOT NULL)");
            } else
            {
                $query = $query->leftJoin("websites", "we_sirelo_co_code", "cu_co_code")
                    ->where("cu_co_code", $sirelo_filter);
            }
        }

        if ($customer_filter != null)
        {
            $query = $query->where("cu_id", $customer_filter);
        }

        $query = $query->where("cu_deleted", 0)
            ->where("moda_disable_sirelo_export", 0)
            ->whereRaw("(cu_type = 1 OR cu_type = 6)")
            ->orderBy("cu_company_name_business", "asc")
            ->get();

        $total_reviews = 0;
        $total_1 = 0;
        $total_2 = 0;
        $total_3 = 0;
        $total_4 = 0;
        $total_5 = 0;

        foreach ($query as $row)
        {
            $status = self::getStatus($row);

            if ($moverstatus_filter != null)
            {
                if ($moverstatus_filter != $status)
                {
                    continue;
                }
            }

            $ratings = Survey2::selectRaw("COUNT(su_rating) as amount, su_rating")
                ->whereBetween("su_submitted_timestamp", System::betweenDatesWithoutTime($request->date))
                ->where("su_show_on_website", 1)
                ->whereRaw("(`su_mover` = '" . $row->cu_id . "' OR `su_other_mover` = '" . $row->cu_id . "')")
                ->groupBy("su_rating")
                ->orderBy("su_rating", "asc")
                ->get();

            if (count($ratings) == 0)
            {
                continue;
            }

            $data[1] = 0;
            $data[2] = 0;
            $data[3] = 0;
            $data[4] = 0;
            $data[5] = 0;

            for ($i = 0; $i < 5; $i++)
            {
                if (!empty($ratings[$i]))
                {
                    $data[$ratings[$i]->su_rating] = $ratings[$i]->amount;
                }
            }

            $total = (int)$data[1] * 1;
            $total += (int)$data[2] * 2;
            $total += (int)$data[3] * 3;
            $total += (int)$data[4] * 4;
            $total += (int)$data[5] * 5;

            $sum = $data[1] + $data[2] + $data[3] + $data[4] + $data[5];

            $filtered_data[$row->cu_id] = [
                "id" => $row->cu_id,
                "customer" => $row->cu_company_name_business,
                "status" => $status,
                "country" => $row->co_en,
                "reviews" => $sum,
                "average" => number_format((float)($sum > 0 ? $total / $sum : 0), 2, '.', ''),
                "1star" => $data[1],
                "2stars" => $data[2],
                "3stars" => $data[3],
                "4stars" => $data[4],
                "5stars" => $data[5],
            ];

            $total_reviews += $sum;
            $total_1 += $data[1];
            $total_2 += $data[2];
            $total_3 += $data[3];
            $total_4 += $data[4];
            $total_5 += $data[5];
        }

        $total_average = ($total_5 * 5) + ($total_4 * 4) + ($total_3 * 3) + ($total_2 * 2) + ($total_1 * 1);

        if (!empty($_POST)){
            self::insertReportUsageHistory($rep_id);
        }

        return view('reports.reviews_per_customer', [
            'report' => $report,
            'date_filter' => $date_filter,
            'customer_filter' => $customer_filter,
            'moverstatus_filter' => $moverstatus_filter,
            'sirelo_filter' => $sirelo_filter,
            'filtered_data' => $filtered_data,

            //TOTALS
            'total_reviews' => $total_reviews,
            'total_average' => number_format((float)($total_reviews > 0 ? $total_average / $total_reviews : 0), 2, '.', ''),
            'total_1' => $total_1,
            'total_2' => $total_2,
            'total_3' => $total_3,
            'total_4' => $total_4,
            'total_5' => $total_5,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function viewLeadAmounts(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);

        $date_filter = null;
        $table_with_filter = null;

        if ($request->date != null)
        {
            $date_filter = $request->date;
        } else
        {
            return Redirect::back()->withErrors(['Please select a date']);
        }

        if ($request->table_with != null)
        {
            $table_with_filter = $request->table_with;
        }

        $filtered_data = [];

        //International Affiliate
        [$int_af, $int_af_sirelo, $int_af_total, $count_af, $count_af_sirelo] = $this->CalculateIntAF($request);

        $filtered_data['INT_AF']['TABLE_AF'] = $int_af;
        $filtered_data['INT_AF']['TABLE_SIRELO'] = $int_af_sirelo;
        $filtered_data['INT_AF']['TABLE_TOTAL'] = $int_af_total;
        $filtered_data['INT_AF']['TOTAL_AF'] = $count_af;
        $filtered_data['INT_AF']['TOTAL_SIRELO'] = $count_af_sirelo;
        $filtered_data['INT_AF']['TOTAL_COMBINED'] = $count_af+$count_af_sirelo;

        [$int_tgb, $int_tgb_count] = $this->CalculateIntTGB($request);

        $filtered_data['INT_TGB']['TABLE'] = $int_tgb;
        $filtered_data['INT_TGB']['TOTAL'] = $int_tgb_count;

        $total_int = $this->CalculateIntTOTAL($int_af_total, $int_tgb);

        $filtered_data['INT_TOTAL']['TABLE'] = $total_int;
        $filtered_data['INT_TOTAL']['TOTAL'] = $count_af+$count_af_sirelo+$int_tgb_count;

        [$nat_af, $nat_af_sirelo, $nat_af_total, $count_nat, $count_nat_sirelo] = $this->CalculateNatAF($request);

        $filtered_data['NAT_AF']['TABLE_AF'] = $nat_af;
        $filtered_data['NAT_AF']['TABLE_SIRELO'] = $nat_af_sirelo;
        $filtered_data['NAT_AF']['TABLE_TOTAL'] = $nat_af_total;
        $filtered_data['NAT_AF']['TOTAL_AF'] = $count_nat;
        $filtered_data['NAT_AF']['TOTAL_SIRELO'] = $count_nat_sirelo;
        $filtered_data['NAT_AF']['TOTAL_COMBINED'] = $count_nat+$count_nat_sirelo;

        [$nat_tgb, $nat_tgb_count] = $this->CalculateNatTGB($request);

        $filtered_data['NAT_TGB']['TABLE'] = $nat_tgb;
        $filtered_data['NAT_TGB']['TOTAL'] = $nat_tgb_count;

        $total_nat = $this->CalculateNatTOTAL($nat_af_total, $nat_tgb);

        $filtered_data['NAT_TOTAL']['TABLE'] = $total_nat;
        $filtered_data['NAT_TOTAL']['TOTAL'] = $count_nat+$count_nat_sirelo+$nat_tgb_count;

        if (!empty($_POST)){
            self::insertReportUsageHistory($rep_id);
        }

        return view('reports.lead_amount', [
            'report' => $report,
            'date_filter' => $date_filter,
            'table_with_filter' => $table_with_filter,
            'filtered_data' => $filtered_data,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function viewConversionTools(Request $request, $rep_id)
    {
        $test_conversion_tools_id = 203;
        $report = Report::findOrFail($rep_id);

        if ($request->date == null)
        {
            return Redirect::back()->withErrors(['Please select a date']);
        }

        $filtered_data = [];

        $forms = AffiliatePartnerForm::leftJoin("customers", "afpafo_cu_id", "cu_id")
            ->where("afpafo_form_type", 4)
            ->where("afpafo_id", "!=", $test_conversion_tools_id)
            ->where("cu_type", "!=", CustomerType::AFFILIATE_PARTNER)
            ->get();

        foreach($forms as $form){
            $filtered_data[$form->afpafo_id]['name'] = $form->cu_company_name_business;
        }

        $mover_requests = MoverRequest::selectRaw("COUNT(*) as amount, more_afpafo_id")
            ->whereBetween("more_timestamp", System::betweenDates($request->date))
            ->where("more_afpafo_id", "!=", $test_conversion_tools_id)
            ->groupBy("more_afpafo_id")
            ->get();

        $total_requests = 0;
        $total_visits = 0;
        $total_vcs = [];
        $total_steps = [];

        foreach($mover_requests as $mover_request){
            if(array_key_exists($mover_request->more_afpafo_id, $filtered_data))
            {
                $filtered_data[$mover_request->more_afpafo_id]['requests'] += $mover_request->amount;
                $total_requests += $mover_request->amount;
            }
        }

        $steps_query = AffiliatePartnerFormStep::whereBetween("afpafost_date", System::betweenDates($request->date))
            ->where("afpafost_afpafo_id", "!=", $test_conversion_tools_id)
            ->get();

        foreach($steps_query as $step_query){
            if(array_key_exists($step_query->afpafost_afpafo_id, $filtered_data))
            {
                $filtered_data[$step_query->afpafost_afpafo_id]['step'][$step_query->afpafost_step] += $step_query->afpafost_visits;
                $total_steps['step'][$step_query->afpafost_step] += $step_query->afpafost_visits;
            }
        }

        $visits_query = AffiliatePartnerFormVisit::leftJoin("affiliate_partner_forms", "afpafovi_afpafo_id", "afpafo_id")

            ->whereBetween("afpafovi_date", System::betweenDates($request->date))
            ->where("afpafo_form_type", AffiliateFormType::CONVERSION_TOOLS)
            ->where("afpafovi_afpafo_id", "!=", $test_conversion_tools_id)
            ->get();

        foreach($visits_query as $visit_query){
            if(array_key_exists($visit_query->afpafovi_afpafo_id, $filtered_data))
            {
                $filtered_data[$visit_query->afpafovi_afpafo_id]['visits'] += $visit_query->afpafovi_visits;
                $total_visits += $visit_query->afpafovi_visits;
            }
        }

        //Volume Calcs
        $vcs = VolumeCalculator::selectRaw("COUNT(*) as amount, voca_afpafo_id, voca_type")
            ->whereBetween("voca_timestamp", System::betweenDates($request->date))
            ->whereIn("voca_type", [2,3])
            ->whereNotNull("voca_afpafo_id")
            ->where("voca_afpafo_id", "!=", $test_conversion_tools_id)
            ->groupBy(["voca_afpafo_id", "voca_type"])
            ->get();

        foreach($vcs as $vc){
            if(array_key_exists($vc->voca_afpafo_id, $filtered_data))
            {
                $filtered_data[$vc->voca_afpafo_id]['vc'][$vc->voca_type] = $vc->amount;
                $total_vcs[$vc->voca_type] += $vc->amount;
            }
        }

        if (!empty($_POST)){
            self::insertReportUsageHistory($rep_id);
        }


        return view('reports.conversion_tools_statistics', [
            'report' => $report,
            'date_filter' => $request->date,
            'filtered_data' => $filtered_data,
            'times_used' => self::getTimesUsedLast90Days($rep_id),
            'total_requests' => $total_requests,
            'total_visits' => $total_visits,
            'total_vcs' => $total_vcs ,
            'total_steps' => $total_steps,

        ]);

    }

    public function viewQualityScoreCalculations(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);

        $timestamp_of_code_change = "2021-01-21 11:06:20";

        if ($request->date == null)
        {
            return Redirect::back()->withErrors(['Please select a date']);
        }

        $quality_scores = QualityScoreCalculation::leftJoin("requests", "qsc_re_id", "re_id")
            ->join("kt_request_customer_portal", "ktrecupo_re_id", "re_id")
            ->whereBetween("ktrecupo_timestamp", System::betweenDates($request->date))
            ->groupBy("qsc_re_id")
            ->get();

        $solution = [];
        $prices = [];
        $amounts = [];

        //Collect and order per ktcupo_id
        foreach($quality_scores->where("ktrecupo_timestamp", "<", $timestamp_of_code_change) as $quality_score){
            $new = unserialize(base64_decode($quality_score->qsc_new_calc));

            foreach($new['customers'] as $portal => $customers){
                foreach($customers as $ktcupo_id => $customer){
                    $solution[$quality_score->re_id]['new'][$ktcupo_id] = $customer['checked']."";
                }
            }

            //Order by percentage
            arsort($solution[$quality_score->re_id]['new'], SORT_NUMERIC);
            $amounts[$quality_score->re_id] = count($solution[$quality_score->re_id]['new']);
        }

        //Collect and order per ktcupo_id NEW MATCH
        foreach($quality_scores->where("ktrecupo_timestamp", ">=", $timestamp_of_code_change) as $quality_score){
            $new = unserialize(base64_decode($quality_score->qsc_new_calc));

            foreach($new['customers'] as $customer){
                $solution[$quality_score->re_id]['new'][$customer->ktcupo_id] = $customer->checked."";
            }

            //Order by percentage
            arsort($solution[$quality_score->re_id]['new'], SORT_NUMERIC);
            $amounts[$quality_score->re_id] = count($solution[$quality_score->re_id]['new']);
        }



        //Once ordered by percentage, add missing info like name, average match and price
        foreach($quality_scores->where("ktrecupo_timestamp", "<", $timestamp_of_code_change)  as $quality_score){
            $new = unserialize(base64_decode($quality_score->qsc_new_calc));

            $main_portal = key($new["customers"]);
            foreach($new['customers'] as $portal => $customers){
                foreach($customers as $ktcupo_id => $customer){

                    $solution[$quality_score->re_id]['new'][$ktcupo_id] =
                        [
                            intval($new['customers'][$portal][$ktcupo_id]["checked"]) =>
                                [
                                    "Company" => $new['customers'][$portal][$ktcupo_id]["company"],
                                    "Average match 30 days" => number_format($new['customers'][$portal][$ktcupo_id]['average_match_30_days'], 2),
                                    "Bonus price" => number_format($new['customers'][$portal][$ktcupo_id]["adjusted_average_match"]*100, 2)."%",
                                    "Corrected bonus price" => number_format($new['customers'][$portal][$ktcupo_id]["correction_bonus"]*100, 2)."%",
                                    "Price after claiming" => number_format($new['customers'][$portal][$ktcupo_id]["claim_adjusted_lead_price"], 2),
                                    "Price after correction" => number_format($new['customers'][$portal][$ktcupo_id]["formula_price"], 2),
                                    "Matching Percentage (NEW)" => number_format($new['customers'][$portal][$ktcupo_id]["matching_percentage"], 2)."%",
                                    "Matching Percentage (NEW) after podium" => number_format($new['customers'][$portal][$ktcupo_id]["matching_percentage_after_podium"], 2)."%",
                                    "Checked" => intval($new['customers'][$portal][$ktcupo_id]["checked"]),
                                    "Main Portal" => intval($main_portal == $portal)
                                ]
                        ];

                }
            }

        }
        //Once ordered by percentage, add missing info like name, average match and price NEW MATCH
        foreach($quality_scores->where("ktrecupo_timestamp", ">=", $timestamp_of_code_change) as $quality_score){
            $new = unserialize(base64_decode($quality_score->qsc_new_calc));

            foreach($new['customers'] as $customer){

                    $solution[$quality_score->re_id]['new'][$customer->ktcupo_id] =
                        [
                            intval($customer->checked) =>
                                [
                                    "Company" => $customer->cu_company_name_business,
                                    "Average match 30 days" =>  number_format($customer->average_match_30_days, 2),
                                    "Bonus price" => number_format($customer->adjusted_average_match*100, 2)."%",
                                    "Corrected bonus price" => number_format($customer->correction_bonus*100, 2)."%",
                                    "Price after claiming" => number_format($customer->claim_adjusted_lead_price, 2),
                                    "Price after correction" => number_format($customer->formula_price, 2),
                                    "Matching Percentage" => number_format($customer->matching_percentage, 2)."%",
                                    "Matching Percentage after podium" => number_format($customer->matching_percentage_after_podium, 2)."%",
                                    "Checked" => intval($customer->checked),
                                    "Main Portal" => intval($new['main_portal'] == $customer->po_id)
                                ]
                        ];

            }

        }

        if (!empty($_POST)){
            self::insertReportUsageHistory($rep_id);
        }

        return view('reports.quality_score_calculations', [
            'report' => $report,
            'date_filter' => $request->date,
            'filtered_data' => $quality_scores,
            'extra_data' => $solution,
            'prices' => $prices,
            'amounts' => $amounts,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);

    }

    public function viewMatchStatistics(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);

        if ($request->date == null)
        {
            return Redirect::back()->withErrors(['Please select a date']);
        }

        $results = [];

        $matches = MatchStatistic::leftJoin("requests", "mast_re_id", "re_id")
            ->leftJoin("customers", "cu_id", "mast_cu_id")
            ->leftJoin("mover_data", "moda_cu_id", "cu_id")
            ->whereBetween("re_timestamp", System::betweenDates($request->date))
            ->where("mast_capping_limitation", 0)
            ->get();

        $match_amounts_query = MatchStatistic::selectRaw("COUNT(*) as amount, mast_re_id")
            ->leftJoin("requests", "mast_re_id", "re_id")
            ->whereBetween("re_timestamp", System::betweenDates($request->date))
            ->where("mast_capping_limitation", 0)
            ->groupBy("mast_re_id")
            ->get();

        $match_amounts = [];

        foreach($match_amounts_query as $match_amount_row){
            $match_amounts[$match_amount_row->mast_re_id] = $match_amount_row->amount;
        }

        $date = explode( '-', $request->date);
        $diff = strtotime( trim( $date[1] )) - strtotime( trim( $date[0] ));
        $amount_of_days = round($diff / (60 * 60 * 24))+1;

        foreach($matches as $match){
            if(array_key_exists($match->mast_cu_id, $results)){
                $results[$match->mast_cu_id]["amount"] += 1;
                $results[$match->mast_cu_id]["matched"] += $match->mast_matched;
                if($match_amounts[$match->mast_re_id] > 5 && $match->mast_matched)
                {
                    $results[$match->mast_cu_id]["matched_in_lottery"] += 1;
                }
            }else{

                $results[$match->mast_cu_id] = [
                    "customer" => $match->cu_company_name_business,
                    "country" => Country::select("co_en")->where("co_code", $match->cu_co_code)->first()->co_en,
                    "amount" => 1,
                    "matched" => $match->mast_matched,
                    "matched_in_lottery" => ($match_amounts[$match->mast_re_id] > 5 && $match->mast_matched ? 1 : 0),
                    "formula_price" => 0
                ];

                if($match->moda_capping_method == 1 || $match->moda_capping_method == 2){
                    $results[$match->mast_cu_id]["daily_average_days"] = ($match->moda_capping_daily_average * $amount_of_days);
                }else{
                   $active_portals = KTCustomerPortal::where("ktcupo_cu_id", $match->mast_cu_id)->where("ktcupo_status", 1)->get();

                   $capping_total = 0;
                   foreach($active_portals as $portal){
                       $capping_total += $portal->ktcupo_daily_average;
                   }

                    $results[$match->mast_cu_id]["daily_average_days"] = ($capping_total * $amount_of_days);
                }
            }
        }

        if (!empty($_POST)){
            self::insertReportUsageHistory($rep_id);
        }

        return view('reports.match_statistics', [
            'report' => $report,
            'date_filter' => $request->date,
            'filtered_data' => $results,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);

    }

    function getStatus($company_row)
    {
        //Active or inactive customer?
        $statuses = [
            1 => "active",
            2 => "pause"
        ];

        if ($company_row->cu_type == 1 || $company_row->cu_type == 6)
        {
            $status = "inactive";

            foreach ($statuses as $status_id => $class)
            {
                $query_customer_portal = KTCustomerPortal::select("ktcupo_id")
                    ->where("ktcupo_cu_id", $company_row->cu_id)
                    ->where("ktcupo_status", $status_id)
                    ->get();

                if (count($query_customer_portal) > 0)
                {
                    $status = $class;

                    break;
                }
            }
        } elseif ($company_row->cu_type == 2)
        {
            $status = "inactive";

            foreach ($statuses as $status_id => $class)
            {
                $query_customer_question = KTCustomerQuestion::select("ktcuqu_id")
                    ->where("ktcuqu_cu_id", $company_row->cu_id)
                    ->where("ktcuqu_status", $status_id)
                    ->get();

                $query_sp_newsletter_blocks = ServiceProviderNewsletterBlock::select("seprnebl_id")
                    ->where("seprnebl_cu_id", $company_row['cu_id'])
                    ->where("seprnebl_status", $status_id)
                    ->get();

                if (count($query_customer_question) > 0 || count($query_sp_newsletter_blocks) > 0)
                {
                    $status = $class;

                    break;
                }
            }
        } elseif ($company_row->cu_type == 3)
        {
            $status = "inactive";

            $query_afpada = AffiliatePartnerData::select("afpada_id")
                ->where("afpada_cu_id", $company_row->cu_id)
                ->where("afpada_status", 1)
                ->get();


            if (count($query_afpada) > 0)
            {
                $status = "active";
            }
        } else
        {
            $status = "unknown";
        }

        return $status;
    }

    public function viewSurveysExperience(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);

        $date_filter = null;
        $portal_filter = null;
        $system = new System();

        if ($request->date != null)
        {
            $date_filter = $request->date;
        } else
        {
            return Redirect::back()->withErrors(['Please select a date']);
        }

        $filtered_data = [];

        $query = Portal::orderBy("po_portal", "asc")->get();

        $total_surveys_sent = 0;
        $total_surveys_opened = 0;
        $total_surveys_submitted = 0;
        $total_response_1 = 0;
        $total_response_2 = 0;
        $total_response_3 = 0;
        $total_response_4 = 0;

        foreach ($query as $row)
        {
            $query_surveys_2 = Survey2::leftJoin("requests", "su_re_id", "re_id")
                ->leftJoin("website_reviews", "su_were_id", "were_id")
                ->leftJoin("portals", DB::raw("(IF(`surveys_2`.`su_source` = '1', `requests`.`re_po_id`, `website_reviews`.`were_po_id`))"), "po_id")
                ->whereBetween("su_timestamp", System::betweenDates($request->date))
                ->where("po_id", $row->po_id)
                ->get();

            $surveys_sent = 0;
            $surveys_opened = 0;
            $surveys_submitted = 0;
            $response_1 = 0;
            $response_2 = 0;
            $response_3 = 0;
            $response_4 = 0;

            foreach ($query_surveys_2 as $row_surveys_2)
            {
                if ($row_surveys_2->su_submitted)
                {
                    $surveys_submitted++;

                    if ($row_surveys_2->su_moved == 1)
                    {
                        if ($row_surveys_2->su_mover > 0)
                        {
                            $response_1++;
                        } elseif ($row_surveys_2->su_mover == -1)
                        {
                            $response_2++;
                        }
                    } elseif ($row_surveys_2->su_moved == 2)
                    {
                        $response_3++;
                    } elseif ($row_surveys_2->su_moved == 3)
                    {
                        $response_4++;
                    }
                }

                if ($row_surveys_2->su_opened)
                {
                    $surveys_opened++;
                }

                $surveys_sent++;
            }

            if ($response_1 != 0 && $surveys_submitted != 0)
            {
                $response_1_percentage = (($response_1 / $surveys_submitted) * 100);
            } else
            {
                $response_1_percentage = 0;
            }

            if ($response_2 != 0 && $surveys_submitted != 0)
            {
                $response_2_percentage = (($response_2 / $surveys_submitted) * 100);
            } else
            {
                $response_2_percentage = 0;
            }

            if ($response_3 != 0 && $surveys_submitted != 0)
            {
                $response_3_percentage = (($response_3 / $surveys_submitted) * 100);
            } else
            {
                $response_3_percentage = 0;
            }

            if ($response_4 != 0 && $surveys_submitted != 0)
            {
                $response_4_percentage = (($response_4 / $surveys_submitted) * 100);
            } else
            {
                $response_4_percentage = 0;
            }

            $total_surveys_sent += $surveys_sent;
            $total_surveys_opened += $surveys_opened;
            $total_surveys_submitted += $surveys_submitted;
            $total_response_1 += $response_1;
            $total_response_2 += $response_2;
            $total_response_3 += $response_3;
            $total_response_4 += $response_4;

            $filtered_data[$row->po_id] = [
                'id' => $row->po_id,
                'portal' => $row->po_portal,
                'sent' => $surveys_sent,
                'opened' => $surveys_opened,
                'submitted' => $surveys_submitted,
                'moves_with_mover' => $system->numberFormat($response_1_percentage, 2) . "%",
                'moves_with_other_mover' => $system->numberFormat($response_2_percentage, 2) . "%",
                'moving_plans_cancelled' => $system->numberFormat($response_3_percentage, 2) . "%",
                'moving_date_changed' => $system->numberFormat($response_4_percentage, 2) . "%",
            ];
        }

        if ($total_response_1 != 0 && $total_surveys_submitted != 0)
        {
            $total_response_1_percentage = (($total_response_1 / $total_surveys_submitted) * 100);
        } else
        {
            $total_response_1_percentage = 0;
        }

        if ($total_response_2 != 0 && $total_surveys_submitted != 0)
        {
            $total_response_2_percentage = (($total_response_2 / $total_surveys_submitted) * 100);
        } else
        {
            $total_response_2_percentage = 0;
        }

        if ($total_response_3 != 0 && $total_surveys_submitted != 0)
        {
            $total_response_3_percentage = (($total_response_3 / $total_surveys_submitted) * 100);
        } else
        {
            $total_response_3_percentage = 0;
        }


        if ($total_response_4 != 0 && $total_surveys_submitted != 0)
        {
            $total_response_4_percentage = (($total_response_4 / $total_surveys_submitted) * 100);
        } else
        {
            $total_response_4_percentage = 0;
        }

        if (!empty($_POST)){
            self::insertReportUsageHistory($rep_id);
        }

        return view('reports.surveys_experience', [
            'report' => $report,
            'date_filter' => $date_filter,
            'filtered_data' => $filtered_data,

            //TOTALS
            'total_sent' => $total_surveys_sent,
            'total_opened' => $total_surveys_opened,
            'total_submitted' => $total_surveys_submitted,
            'total_response_1' => $system->numberFormat($total_response_1_percentage, 2) . "%",
            'total_response_2' => $system->numberFormat($total_response_2_percentage, 2) . "%",
            'total_response_3' => $system->numberFormat($total_response_3_percentage, 2) . "%",
            'total_response_4' => $system->numberFormat($total_response_4_percentage, 2) . "%",
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function viewPythonProcess($rep_id)
    {
        $report = Report::findOrFail($rep_id);

        $countries = Country::all();
        $data = new Data();

        $total_gathered = 0;
        $total_processed = 0;
        $total_processed_first_time = 0;
        $total_processed_first_time_left = 0;
        $total_processed_not_first = 0;
        $total_processed_not_first_left = 0;
        $total_skipped = 0;
        $total_unique = 0;

        foreach ($countries as $country) {
            $co_code = $country->co_code;

            $rows = DB::table( 'customer_review_scores' )
                ->selectRaw('COUNT(*) as `amount`')
                ->leftJoin('customers', 'curesc_cu_id', 'cu_id')
                ->where("cu_co_code", $co_code)
                ->first();

            if ($rows->amount == 0) {
                continue;
            }

            $rows_processed = DB::table( 'customer_review_scores' )
                ->selectRaw('COUNT(*) as `amount`')
                ->leftJoin('customers', 'curesc_cu_id', 'cu_id')
                ->where("curesc_processed", 1)
                ->where("cu_co_code", $co_code)
                ->first();


            $rows_processed_first_left = DB::table( 'customer_review_scores' )
                ->selectRaw('COUNT(*) as `amount`')
                ->leftJoin('customers', 'curesc_cu_id', 'cu_id')
                ->where("curesc_processed", 0)
                ->where("cu_co_code", $co_code)
                ->where("curesc_mail", 0)
                ->first();

            $rows_processed_not_first_left = DB::table( 'customer_review_scores' )
                ->selectRaw('COUNT(*) as `amount`')
                ->leftJoin('customers', 'curesc_cu_id', 'cu_id')
                ->where("curesc_processed", 0)
                ->where("cu_co_code", $co_code)
                ->where("curesc_mail", ">", 0)
                ->first();

            $rows_skipped = DB::table( 'customer_review_scores' )
                ->selectRaw('COUNT(*) as `amount`')
                ->leftJoin('customers', 'curesc_cu_id', 'cu_id')
                ->where("curesc_skip", 1)
                ->where("cu_co_code", $co_code)
                ->first();

            $color = "none";

            if ($rows->amount == $rows_processed->amount) {
                $color = "green";
            }

            $unique_results = DB::table( 'customer_review_scores' )
                ->selectRaw('COUNT(*) as `amount`')
                ->leftJoin('customers', 'curesc_cu_id', 'cu_id')
                ->where("cu_co_code", $co_code)
                ->where("curesc_name", "!=", "")
                ->first();

            $total_gathered+=$rows->amount;
            $total_processed+=$rows_processed->amount;
            $total_processed_first_time_left+=$rows_processed_first_left->amount;
            $total_processed_not_first_left+=$rows_processed_not_first_left->amount;
            $total_skipped+=$rows_skipped->amount;
            $total_unique+=$unique_results->amount;

            $filtered_data[$co_code] = [
                'country' => $data->country($co_code, "EN"),
                'gathered' => $rows->amount,
                'skipped' => $rows_skipped->amount,

                'processed' => $rows_processed->amount,
                'processed_first_left' => $rows_processed_first_left->amount,
                'processed_updated_left' => $rows_processed_not_first_left->amount,

                'color' => $color,
                'unique_results' => $unique_results->amount
            ];
        }



        self::insertReportUsageHistory($rep_id);

        return view('reports.python_process', [
            'report' => $report,
            'filtered_data' => $filtered_data,
            'times_used' => self::getTimesUsedLast90Days($rep_id),

            //Totals
            'total_gathered' => $total_gathered,
            'total_processed' => $total_processed,
            'total_processed_first_time_left' => $total_processed_first_time_left,
            'total_processed_not_first_left' => $total_processed_not_first_left,
            'total_skipped' => $total_skipped,
            'total_unique' => $total_unique,
        ]);
    }

    public function viewPythonProcessValidatedRows(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);

        $system = new System();
        $filtered_data = [];
        $date_filter = null;

        if ($request->date != null)
        {
            $date_filter = $request->date;
        } else
        {
            return Redirect::back()->withErrors(['Please select a date']);
        }

        $users = User::all();

        $users_list = [];

        $users_list[0] = "System";

        foreach ($users as $user) {
            $users_list[$user->us_id] = $user->us_name;
        }
        $curesc_rows = CustomerReviewScore::leftJoin("customers", "curesc_cu_id", "cu_id")
            ->leftJoin("mover_data", "cu_id", "moda_cu_id")
            ->where("curesc_processed", 1)
            ->whereRaw("curesc_processed_timestamp is NOT NULL")
            ->whereBetween("curesc_processed_timestamp",$system->betweenDates($request->date))
            ->get();

        foreach ($curesc_rows as $curesc) {
            $timestamp_of_changes = date("Y-m-d H", strtotime($curesc->curesc_processed_timestamp));

            $revisions_customers = Revision::where("revisionable_type", "App\Models\Customer")
                ->where("revisionable_id", $curesc->curesc_cu_id)
                ->where("user_id", $curesc->curesc_processed_by)
                ->whereBetween("created_at", [date("Y-m-d H:i:s", strtotime($timestamp_of_changes.":00:00")), date("Y-m-d H:i:s", strtotime($timestamp_of_changes.":59:59"))])
                ->get();

            $revisions_moverdata = Revision::where("revisionable_type", "App\Models\MoverData")
                ->where("revisionable_id", $curesc->moda_id)
                ->where("user_id", $curesc->curesc_processed_by)
                ->whereBetween("created_at", [date("Y-m-d H:i:s", strtotime($timestamp_of_changes.":00:00")), date("Y-m-d H:i:s", strtotime($timestamp_of_changes.":59:59"))])
                ->get();

            $revision_string = "";

            if (count($revisions_customers) > 0 || count($revisions_moverdata)) {

                $revision_string = "<table style='margin-top:10px;'>";

                $revision_string .= "<thead>";
                    $revision_string .= "<tr>";
                        $revision_string .= "<th>Field</th>";
                        $revision_string .= "<th>Old value</th>";
                        $revision_string .= "<th>New value</th>";
                    $revision_string .= "</tr>";
                $revision_string .= "</thead>";

                $revision_string .= "<tbody>";
            }

            if (count($revisions_customers) > 0) {
                foreach ($revisions_customers as $rev) {
                    $revision_string .= "<tr>";
                        $revision_string .= "<td>".$rev->key."</td>";
                        $revision_string .= "<td>".$rev->old_value."</td>";
                        $revision_string .= "<td>".$rev->new_value."</td>";
                    $revision_string .= "</tr>";
                }
            }

            if (count($revisions_moverdata) > 0) {
                foreach ($revisions_moverdata as $rev) {
                    $revision_string .= "<tr>";
                        $revision_string .= "<td>".$rev->key."</td>";
                        $revision_string .= "<td>".$rev->old_value."</td>";
                        $revision_string .= "<td>".$rev->new_value."</td>";
                    $revision_string .= "</tr>";
                }
            }

            if (count($revisions_customers) > 0 || count($revisions_moverdata) > 0) {
                $revision_string .= "</tbody>";
                $revision_string .= "</table>";
            }

            $filtered_data[$curesc->curesc_id] = [
                'cu_id' => $curesc->curesc_cu_id,
                'gaco_id' => $curesc->curesc_gaco_id,
                'name' => $curesc->cu_company_name_business,
                'processed_timestamp' => $curesc->curesc_processed_timestamp,
                'processed_by' => $users_list[$curesc->curesc_processed_by],
                'processed_counter' => $curesc->curesc_processed_counter,
                'revisions' => $revision_string
            ];
        }

        self::insertReportUsageHistory($rep_id);

        return view('reports.python_process_validated_rows', [
            'report' => $report,
            'sirelos' => Website::where("we_sirelo", 1)->orderBy("we_website", "ASC")->get(),
            'date_filter' => $date_filter,
            'filtered_data' => $filtered_data,
            'times_used' => self::getTimesUsedLast90Days($rep_id),

            //TOTALS
            /*'total_leads' => $total_leads,
            'total_matched' => $total_matched,
            'total_rejected' => $total_rejected,
            'total_with_volume' => $total_with_volume,
            'total_with_volume_calc' => $total_with_volume_calc,
            'total_with_remarks' => $total_with_remarks,
            'total_with_volume_and_remarks' => $total_with_volume_and_remarks,
            'total_complete_address_details' => $total_complete_address_details,
            'total_complete_origin_address_details' => $total_complete_origin_address_details,
            'total_complete_destination_address_details' => $total_complete_destination_address_details,
            'total_claims' => $total_claims,
            'total_matches' => $total_matches,
            'total_turnover' => $total_turnover,*/
        ]);
    }

    public function viewMobilityExValidatedRows(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);

        $system = new System();
        $filtered_data = [];
        $date_filter = null;

        if ($request->date != null)
        {
            $date_filter = $request->date;
        } else
        {
            return Redirect::back()->withErrors(['Please select a date']);
        }

        $users = User::all();

        $users_list = [];

        $users_list[0] = "System";

        foreach ($users as $user) {
            $users_list[$user->us_id] = $user->us_name;
        }

        $mobilityex_rows = MobilityexFetchedData::leftJoin("customers", "mofeda_cu_id", "cu_id")
            ->where("mofeda_processed", 1)
            ->whereRaw("mofeda_processed_timestamp is NOT NULL")
            ->whereBetween("mofeda_processed_timestamp",$system->betweenDates($request->date))
            ->get();

        foreach ($mobilityex_rows as $mofeda) {
            $timestamp_of_changes = date("Y-m-d H", strtotime($mofeda->curesc_processed_timestamp));

            $revisions_customers = Revision::where("revisionable_type", "App\Models\Customer")
                ->where("revisionable_id", $mofeda->curesc_cu_id)
                ->where("user_id", $mofeda->curesc_processed_by)
                ->whereBetween("created_at", [date("Y-m-d H:i:s", strtotime($timestamp_of_changes.":00:00")), date("Y-m-d H:i:s", strtotime($timestamp_of_changes.":59:59"))])
                ->get();

            $revisions_moverdata = Revision::where("revisionable_type", "App\Models\MoverData")
                ->where("revisionable_id", $mofeda->moda_id)
                ->where("user_id", $mofeda->curesc_processed_by)
                ->whereBetween("created_at", [date("Y-m-d H:i:s", strtotime($timestamp_of_changes.":00:00")), date("Y-m-d H:i:s", strtotime($timestamp_of_changes.":59:59"))])
                ->get();

            $revision_string = "";

            if (count($revisions_customers) > 0 || count($revisions_moverdata)) {

                $revision_string = "<table style='margin-top:10px;'>";

                $revision_string .= "<thead>";
                    $revision_string .= "<tr>";
                        $revision_string .= "<th>Field</th>";
                        $revision_string .= "<th>Old value</th>";
                        $revision_string .= "<th>New value</th>";
                    $revision_string .= "</tr>";
                $revision_string .= "</thead>";

                $revision_string .= "<tbody>";
            }

            if (count($revisions_customers) > 0) {
                foreach ($revisions_customers as $rev) {
                    $revision_string .= "<tr>";
                        $revision_string .= "<td>".$rev->key."</td>";
                        $revision_string .= "<td>".$rev->old_value."</td>";
                        $revision_string .= "<td>".$rev->new_value."</td>";
                    $revision_string .= "</tr>";
                }
            }

            if (count($revisions_moverdata) > 0) {
                foreach ($revisions_moverdata as $rev) {
                    $revision_string .= "<tr>";
                        $revision_string .= "<td>".$rev->key."</td>";
                        $revision_string .= "<td>".$rev->old_value."</td>";
                        $revision_string .= "<td>".$rev->new_value."</td>";
                    $revision_string .= "</tr>";
                }
            }

            if (count($revisions_customers) > 0 || count($revisions_moverdata) > 0) {
                $revision_string .= "</tbody>";
                $revision_string .= "</table>";
            }

            $filtered_data[$mofeda->mofeda_id] = [
                'cu_id' => $mofeda->mofeda_cu_id,
                'name' => $mofeda->mofeda_company_name_legal,
                'processed_timestamp' => $mofeda->mofeda_processed_timestamp,
                'processed_by' => $users_list[$mofeda->mofeda_processed_by],
                'connected_or_created_customer' => $mofeda->cu_company_name_business,
                'what_happend' => $mofeda->mofeda_what_happend,
                'revisions' => $revision_string
            ];
        }

        self::insertReportUsageHistory($rep_id);

        return view('reports.mobilityex_validated_rows', [
            'report' => $report,
            'sirelos' => Website::where("we_sirelo", 1)->orderBy("we_website", "ASC")->get(),
            'date_filter' => $date_filter,
            'filtered_data' => $filtered_data,
            'times_used' => self::getTimesUsedLast90Days($rep_id),
        ]);
    }

    public function viewGoogleDataFetchResults(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);

        $system = new System();

        $filtered_data = [];
        $date_filter = null;
        $total_fetched = 0;
        $total_empty_skipped = 0;
        $total_created = 0;
        $total_unchanged = 0;
        $total_check_manually = 0;

        if ($request->date != null)
        {
            $date_filter = $request->date;
        } else
        {
            return Redirect::back()->withErrors(['Please select a date']);
        }

        //Get rows per country with total of fetched, created, skipped, changed and unchanged rows
        $fetched_google_history_rows = FetchedGoogleHistory::selectRaw("co_code, SUM(`fegohi_fetched`) as `fetched`, SUM(`fegohi_created`) as `created`, SUM(`fegohi_skipped`) as `empty_skipped`, SUM(`fegohi_unchanged`) as `unchanged`, SUM(`fegohi_changed`) as `check_manually`")
            ->leftJoin("customers", "cu_id", "fegohi_cu_id")
            ->leftJoin("countries", "co_code", "cu_co_code")
            ->whereBetween("fegohi_timestamp", $system->betweenDates($request->date))
            ->groupBy("co_code")
            ->get();

        foreach ($fetched_google_history_rows as $row) {
            $country = Data::country($row->co_code, "EN");

            //Save row of this country in $filtered data array
            $filtered_data[$country] = [
                'country' => $country,
                'fetched' => $row->fetched,
                'empty_skipped' => $row->empty_skipped,
                'created' => $row->created,
                'unchanged' => $row->unchanged,
                'check_manually' => $row->check_manually,
            ];

            //Update totals
            $total_fetched+=$row->fetched;
            $total_empty_skipped+=$row->empty_skipped;
            $total_created+=$row->created;
            $total_unchanged+=$row->unchanged;
            $total_check_manually+=$row->check_manually;
        }

        self::insertReportUsageHistory($rep_id);

        return view('reports.google_data_fetch_results', [
            'report' => $report,
            'date_filter' => $date_filter,
            'filtered_data' => $filtered_data,
            'times_used' => self::getTimesUsedLast90Days($rep_id),

            //TOTALS
            'total_fetched' => $total_fetched,
            'total_empty_skipped' => $total_empty_skipped,
            'total_created' => $total_created,
            'total_unchanged' => $total_unchanged,
            'total_check_manually' => $total_check_manually
        ]);
    }

    public function viewMobilityExFetchResults(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);

        $system = new System();

        $filtered_data = [];
        $date_filter = null;

        $total_processed = 0;
        $total_accepted = 0;
        $total_rejected = 0;
        $total_changed_to_another_type = 0;

        if ($request->date != null)
        {
            $date_filter = $request->date;
        } else
        {
            return Redirect::back()->withErrors(['Please select a date']);
        }

        $mofeda_rows = MobilityexFetchedData::whereBetween("mofeda_processed_timestamp", $system->betweenDates($request->date))->orderBy("mofeda_processed_timestamp", "ASC")->get();

        foreach ($mofeda_rows as $row) {
            $date = date("Y-m-d", strtotime($row->mofeda_processed_timestamp));

            if(!isset($filtered_data[$date])) {
                $filtered_data[$date] = [
                    "date" => $date,
                    "total_fetched" => 0,
                    "total_processed" => 0,
                    "total_accepted" => 0,
                    "total_rejected" => 0,
                    //"total_left" => 0,
                    "total_changed_to_another_type" => 0,
                ];
            }

            $filtered_data[$date]['total_processed']++;
            $total_processed++;

            if (!empty($row->mofeda_original_type) && $row->mofeda_type != $row->mofeda_original_type) {
                $filtered_data[$date]['total_changed_to_another_type']++;
            }

            if ($row->mofeda_accepted == 1) {
                $filtered_data[$date]['total_accepted']++;
                $total_accepted++;
            }
            else {
                $filtered_data[$date]['total_rejected']++;
                $total_rejected++;
            }
        }

        self::insertReportUsageHistory($rep_id);

        return view('reports.mobilityex_fetch_results', [
            'report' => $report,
            'date_filter' => $date_filter,
            'filtered_data' => $filtered_data,
            'times_used' => self::getTimesUsedLast90Days($rep_id),

            //TOTALS
            'total_processed' => $total_processed,
            'total_accepted' => $total_accepted,
            'total_rejected' => $total_rejected,
            'total_changed_to_another_type' => $total_changed_to_another_type
        ]);
    }

    public function viewRelatedMovingCompaniesFetchResults(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);

        $system = new System();

        $filtered_data = [];
        $date_filter = null;

        $total_backlog = 0;
        $total_fetched = 0;
        $total_processed = 0;
        $total_accepted = 0;
        $total_rejected = 0;

        if ($request->date != null)
        {
            $date_filter = $request->date;
        } else {
            return Redirect::back()->withErrors( ['Please select a date'] );

        }

        $countries = [];

        $countries_query = GatheredCompany::leftJoin("countries", "co_code", "gaco_searched_co_code")
            ->where("gaco_country_status", 1)
            ->groupBy("gaco_searched_co_code")
            ->get();

        foreach ($countries_query as $country) {
            $countries[$country->co_code] = $country->co_en;
        }

        foreach ($countries as $country_code => $country_name) {
            $add_to_filtered_data = false;

            $total_fetched_query = CustomerReviewScore::selectRaw("COUNT(*) as amount_fetched")
                ->leftJoin("gathered_companies", "gaco_id", "curesc_gaco_id")
                ->whereNotNull("curesc_gaco_id")
                ->whereBetween("curesc_timestamp", $system->betweenDates($request->date))
                ->where("gaco_searched_co_code", $country_code)
                ->first()->amount_fetched;

            $total_backlog_query = GatheredCompany::selectRaw("COUNT(*) as amount_backlog")
                ->join("gathered_companies_list", "gacoli_title", "gaco_title")
                ->where("gaco_searched_co_code", $country_code)
                ->where("gaco_country_status", 1)
                ->where("gaco_processed", 0)
                ->first()->amount_backlog;

            $total_processed_query = CustomerReviewScore::selectRaw("COUNT(*) as amount_processed")
                ->leftJoin("gathered_companies", "gaco_id", "curesc_gaco_id")
                ->whereNotNull("curesc_gaco_id")
                ->whereBetween("curesc_processed_timestamp", $system->betweenDates($request->date))
                ->where("gaco_searched_co_code", $country_code)
                ->first()->amount_processed;

            $total_accepted_query = CustomerReviewScore::selectRaw("COUNT(*) as amount_accepted")
                ->leftJoin("gathered_companies", "gaco_id", "curesc_gaco_id")
                ->whereNotNull("curesc_gaco_id")
                ->whereBetween("curesc_processed_timestamp", $system->betweenDates($request->date))
                ->where("gaco_searched_co_code", $country_code)
                ->where("curesc_accepted", 1)
                ->first()->amount_accepted;

            $total_rejected_query = $total_processed_query - $total_accepted_query;

            if ($total_fetched_query > 0 || $total_processed_query > 0 || $total_accepted_query > 0) {
                $add_to_filtered_data = true;
            }


            if ($add_to_filtered_data) {
                $filtered_data[$country_code] = [
                    'country' => $country_name,
                    'backlog' => $total_backlog_query,
                    'fetched' => $total_fetched_query,
                    'processed' => $total_processed_query,
                    'accepted' => $total_accepted_query,
                    'rejected' => $total_rejected_query,
                ];

                //Update totals
                $total_backlog += $total_backlog_query;
                $total_fetched += $total_fetched_query;
                $total_processed += $total_processed_query;
                $total_accepted += $total_accepted_query;
                $total_rejected += $total_rejected_query;
            }
        }


        /*$curesc_rows = CustomerReviewScore::leftJoin("gathered_companies", "gaco_id", "curesc_gaco_id")
            ->whereBetween("curesc_timestamp", $system->betweenDates($request->date))
            ->whereNotNull("curesc_gaco_id")
            ->groupBy("gaco_searched_co_code")
            ->get();

        foreach ($curesc_rows as $row) {

            if(!isset($filtered_data[$row->gaco_searched_co_code])) {

                $total_fetched_query = CustomerReviewScore::selectRaw("COUNT(*) as amount_fetched")
                    ->leftJoin("gathered_companies", "gaco_id", "curesc_gaco_id")
                    ->whereNotNull("curesc_gaco_id")
                    ->whereBetween("curesc_timestamp", $system->betweenDates($request->date))
                    ->where("gaco_searched_co_code", $row->gaco_searched_co_code)
                    ->first()->amount_fetched;

                $total_processed_query = CustomerReviewScore::selectRaw("COUNT(*) as amount_processed")
                    ->leftJoin("gathered_companies", "gaco_id", "curesc_gaco_id")
                    ->whereNotNull("curesc_gaco_id")
                    ->whereBetween("curesc_processed_timestamp", $system->betweenDates($request->date))
                    ->where("gaco_searched_co_code", $row->gaco_searched_co_code)
                    ->first()->amount_processed;

                $total_accepted_query = CustomerReviewScore::selectRaw("COUNT(*) as amount_accepted")
                    ->leftJoin("gathered_companies", "gaco_id", "curesc_gaco_id")
                    ->whereNotNull("curesc_gaco_id")
                    ->whereBetween("curesc_processed_timestamp", $system->betweenDates($request->date))
                    ->where("gaco_searched_co_code", $row->gaco_searched_co_code)
                    ->where("curesc_accepted", 1)
                    ->first()->amount_accepted;

                $total_rejected_query = $total_processed_query - $total_accepted_query;

                $filtered_data[$row->gaco_searched_co_code] = [
                    "country" => Data::country($row->gaco_searched_co_code, "EN"),
                    "total_fetched" => $total_fetched_query,
                    "total_processed" => $total_processed_query,
                    "total_accepted" => $total_accepted_query,
                    "total_rejected" => $total_rejected_query,
                ];

                $total_fetched+=$total_fetched_query;
                $total_processed+=$total_processed_query;
                $total_accepted+=$total_accepted_query;
                $total_rejected+=$total_rejected_query;
            }
        }*/

        self::insertReportUsageHistory($rep_id);

        return view('reports.related_moving_companies_fetch_results', [
            'report' => $report,
            'date_filter' => $date_filter,
            'filtered_data' => $filtered_data,
            'times_used' => self::getTimesUsedLast90Days($rep_id),

            //TOTALS
            'total_backlog' => $total_backlog,
            'total_fetched' => $total_fetched,
            'total_processed' => $total_processed,
            'total_accepted' => $total_accepted,
            'total_rejected' => $total_rejected,
        ]);
    }

    public function viewLeadQuality(Request $webrequest, $rep_id)
    {
        $report = Report::findOrFail($rep_id);

        $date_filter = null;

        if ($webrequest->date != null)
        {
            $date_filter = $webrequest->date;
        } else
        {
            return Redirect::back()->withErrors(['Please select a date']);
        }

        $affiliates_filter = [];

        if ($webrequest->affiliates != null)
        {
            $affiliates_filter = $webrequest->affiliates;
        }
        else {
            if ($webrequest->website == null) {
                return Redirect::back()->withErrors(['Please select an affiliate or website']);

            }
        }

        $request_types_filter = [];

        if ($webrequest->request_types != null)
        {
            $request_types_filter = $webrequest->request_types;
        }
        else {
            return Redirect::back()->withErrors(['Please select a request type']);
        }


        $websites_filter = [];

        if ($webrequest->website != null)
        {
            $websites_filter = $webrequest->website;
        }

        $filtered_data = [];

        $total_leads = 0;
        $total_matched = 0;
        $total_rejected = 0;
        $total_with_volume = 0;
        $total_with_volume_calc = 0;
        $total_with_remarks = 0;
        $total_with_volume_and_remarks = 0;
        $total_complete_address_details = 0;
        $total_complete_origin_address_details = 0;
        $total_complete_destination_address_details = 0;
        $total_claims = 0;
        $total_matches = 0;
        $total_turnover = 0;

        $system = new System();

        if (!empty($webrequest->affiliates)) {
            $affiliates_query = Customer::where("cu_type", 3)->whereIn("cu_id", $webrequest->affiliates)->get();

            foreach ($affiliates_query as $affiliate) {
                $cu_id = $affiliate->cu_id;

                $leads = 0;
                $matched = 0;
                $rejected = 0;
                $with_volume = 0;
                $with_volume_calc = 0;
                $with_remarks = 0;
                $with_volume_and_remarks = 0;
                $complete_address_details = 0;
                $complete_origin_address_details = 0;
                $complete_destination_address_details = 0;
                $claims = 0;
                $matches = 0;
                $turnover = 0;

                $affiliate_requests = AffiliatePartnerRequest::leftJoin("requests", "re_id", "afpare_re_id")
                    ->where("re_status", "!=", 0)
                    ->whereBetween("re_timestamp",$system->betweenDates($webrequest->date))
                    ->where("afpare_cu_id", $cu_id)
                    ->get();

                if(count($affiliate_requests) == 0) {
                    continue;
                }

                foreach ($affiliate_requests as $request) {
                    $leads++;

                    if ($request->re_status == RequestStatus::MATCHED) {
                        $matched++;

                        $ktrecupo = KTRequestCustomerPortal::leftJoin("portals", "po_id", "ktrecupo_po_id")
                            ->leftJoin("requests", "re_id", "ktrecupo_re_id")
                            ->where("ktrecupo_re_id", $request->re_id)
                            ->whereIn("ktrecupo_type", [1,2,3,4])
                            ->whereIn('re_request_type', $webrequest->request_types)
                            ->get();

                        foreach ($ktrecupo as $ktrecupo_row) {
                            $matches++;

                            if ($ktrecupo_row->ktrecupo_free == 1 || $ktrecupo_row->ktrecupo_free_percentage == 1) {
                                $claims++;
                            }

                            if ($ktrecupo_row->ktrecupo_amount_netto_eur > 0) {
                                $turnover = $turnover + $ktrecupo_row->ktrecupo_amount_netto_eur;
                            }
                            else {
                                $turnover = $turnover + $system->currencyConvert($ktrecupo_row->ktrecupo_amount_netto, $ktrecupo_row->po_pacu_code, "EUR");
                            }
                        }
                    }
                    elseif($request->re_status == RequestStatus::REJECTED) {
                        $rejected++;
                    }

                    if ($request->re_volume_m3 > 0) {
                        $with_volume++;
                    }

                    if (!empty($request->re_voca_id)) {
                        $with_volume_calc++;
                    }

                    if (!empty($request->re_remarks)) {
                        $with_remarks++;
                    }

                    if (!empty($request->re_remarks) && $request->re_volume_m3 > 0) {
                        $with_volume_and_remarks++;
                    }

                    if (!empty($request->re_street_from) && !empty($request->re_city_from)) {
                        $complete_origin_address_details++;
                    }

                    if (!empty($request->re_street_to) && !empty($request->re_city_to)) {
                        $complete_destination_address_details++;
                    }
                    if (!empty($request->re_street_from) && !empty($request->re_city_from) && !empty($request->re_street_to) && !empty($request->re_city_to)) {
                        $complete_address_details++;
                    }

                }

                $total_leads = $total_leads + $leads;
                $total_matched = $total_matched + $matched;
                $total_rejected = $total_rejected + $rejected;
                $total_with_volume = $total_with_volume + $with_volume;
                $total_with_volume_calc = $total_with_volume_calc + $with_volume_calc;
                $total_with_remarks = $total_with_remarks + $with_remarks;
                $total_with_volume_and_remarks = $total_with_volume_and_remarks + $with_volume_and_remarks;
                $total_complete_address_details = $total_complete_address_details + $complete_address_details;
                $total_complete_origin_address_details = $total_complete_origin_address_details + $complete_origin_address_details;
                $total_complete_destination_address_details = $total_complete_destination_address_details + $complete_destination_address_details;
                $total_claims = $total_claims + $claims;
                $total_matches = $total_matches + $matches;
                $total_turnover = $total_turnover + $turnover;

                $filtered_data[$affiliate->cu_company_name_business] = [
                    'name' => $affiliate->cu_company_name_business,
                    'leads' => $leads,
                    'matched' => $matched,
                    'rejected' => $rejected,
                    'matches' => $matches,
                    'matched_percentage' => (($leads > 0) ? round((100 / $leads) * $matched) : 0),
                    'rejected_percentage' => (($leads > 0) ? round((100 / $leads) * $rejected) : 0),
                    'volume_percentage' => (($leads > 0) ? round((100 / $leads) * $with_volume) : 0),
                    'volume_calc_percentage' => (($leads > 0) ? round((100 / $leads) * $with_volume_calc) : 0),
                    'remarks_percentage' => (($leads > 0) ? round((100 / $leads) * $with_remarks) : 0),
                    'volume_and_remarks_percentage' => (($leads > 0) ? round((100 / $leads) * $with_volume_and_remarks) : 0),
                    'complete_origin_address_percentage' => (($leads > 0) ? round((100 / $leads) * $complete_origin_address_details) : 0),
                    'complete_destination_address_percentage' => (($leads > 0) ? round((100 / $leads) * $complete_destination_address_details) : 0),
                    'complete_addresses_percentage' => (($leads > 0) ? round((100 / $leads) * $complete_address_details) : 0),
                    'claims_percentage' => (($claims > 0) ? round((100 / $matches) * $claims) : 0),
                    'avg_matched' => (($matched > 0) ? round($matches / $matched, 1) : 0) ,
                    'avg_turnover' => (($turnover > 0) ? $system->numberFormat($turnover / $matched, 2, ",", ".") : "0,00")
                ];
            }
        }

        if (!empty($webrequest->website)) {
            $websites = Website::whereIn("we_id", $webrequest->website)->get();

            foreach ($websites as $website) {
                $we_id = $website->we_id;

                $leads = 0;
                $matched = 0;
                $rejected = 0;
                $with_volume = 0;
                $with_volume_calc = 0;
                $with_remarks = 0;
                $with_volume_and_remarks = 0;
                $complete_address_details = 0;
                $complete_origin_address_details = 0;
                $complete_destination_address_details = 0;
                $claims = 0;
                $matches = 0;
                $turnover = 0;

                $requests = \App\Models\Request::leftJoin("website_forms", "wefo_id", "re_wefo_id")
                    ->where("wefo_we_id", $we_id)
                    ->where("re_status", "!=", 0)
                    ->whereBetween("re_timestamp",$system->betweenDates($webrequest->date))
                    ->whereIn('re_request_type', $webrequest->request_types)
                    ->get();

                if(count($requests) == 0) {
                    continue;
                }

                foreach ($requests as $request) {
                    $leads++;

                    if ($request->re_status == RequestStatus::MATCHED) {
                        $matched++;

                        $ktrecupo = KTRequestCustomerPortal::leftJoin("portals", "po_id", "ktrecupo_po_id")
                            ->where("ktrecupo_re_id", $request->re_id)
                            ->whereIn("ktrecupo_type", [1,2,3,4])
                            ->get();

                        foreach ($ktrecupo as $ktrecupo_row) {
                            $matches++;

                            if ($ktrecupo_row->ktrecupo_free == 1 || $ktrecupo_row->ktrecupo_free_percentage == 1) {
                                $claims++;
                            }

                            if ($ktrecupo_row->ktrecupo_amount_netto_eur > 0) {
                                $turnover = $turnover + $ktrecupo_row->ktrecupo_amount_netto_eur;
                            }
                            else {
                                $turnover = $turnover + $system->currencyConvert($ktrecupo_row->ktrecupo_amount_netto, $ktrecupo_row->po_pacu_code, "EUR");
                            }
                        }
                    }
                    elseif($request->re_status == RequestStatus::REJECTED) {
                        $rejected++;
                    }

                    if ($request->re_volume_m3 > 0) {
                        $with_volume++;
                    }

                    if (!empty($request->re_voca_id)) {
                        $with_volume_calc++;
                    }

                    if (!empty($request->re_remarks)) {
                        $with_remarks++;
                    }

                    if (!empty($request->re_remarks) && $request->re_volume_m3 > 0) {
                        $with_volume_and_remarks++;
                    }

                    if (!empty($request->re_street_from) && !empty($request->re_city_from)) {
                        $complete_origin_address_details++;
                    }

                    if (!empty($request->re_street_to) && !empty($request->re_city_to)) {
                        $complete_destination_address_details++;
                    }
                    if (!empty($request->re_street_from) && !empty($request->re_city_from) && !empty($request->re_street_to) && !empty($request->re_city_to)) {
                        $complete_address_details++;
                    }

                }

                $total_leads = $total_leads + $leads;
                $total_matched = $total_matched + $matched;
                $total_rejected = $total_rejected + $rejected;
                $total_with_volume = $total_with_volume + $with_volume;
                $total_with_volume_calc = $total_with_volume_calc + $with_volume_calc;
                $total_with_remarks = $total_with_remarks + $with_remarks;
                $total_with_volume_and_remarks = $total_with_volume_and_remarks + $with_volume_and_remarks;
                $total_complete_address_details = $total_complete_address_details + $complete_address_details;
                $total_complete_origin_address_details = $total_complete_origin_address_details + $complete_origin_address_details;
                $total_complete_destination_address_details = $total_complete_destination_address_details + $complete_destination_address_details;
                $total_claims = $total_claims + $claims;
                $total_matches = $total_matches + $matches;
                $total_turnover = $total_turnover + $turnover;

                $filtered_data[$website->we_website] = [
                    'name' => $website->we_website,
                    'leads' => $leads,
                    'matched' => $matched,
                    'rejected' => $rejected,
                    'matches' => $matches,
                    'matched_percentage' => (($leads > 0) ? round((100 / $leads) * $matched) : 0),
                    'rejected_percentage' => (($leads > 0) ? round((100 / $leads) * $rejected) : 0),
                    'volume_percentage' => (($leads > 0) ? round((100 / $leads) * $with_volume) : 0),
                    'volume_calc_percentage' => (($leads > 0) ? round((100 / $leads) * $with_volume_calc) : 0),
                    'remarks_percentage' => (($leads > 0) ? round((100 / $leads) * $with_remarks) : 0),
                    'volume_and_remarks_percentage' => (($leads > 0) ? round((100 / $leads) * $with_volume_and_remarks) : 0),
                    'complete_origin_address_percentage' => (($leads > 0) ? round((100 / $leads) * $complete_origin_address_details) : 0),
                    'complete_destination_address_percentage' => (($leads > 0) ? round((100 / $leads) * $complete_destination_address_details) : 0),
                    'complete_addresses_percentage' => (($leads > 0) ? round((100 / $leads) * $complete_address_details) : 0),
                    'claims_percentage' => (($claims > 0) ? round((100 / $matches) * $claims) : 0),
                    'avg_matched' => (($matched > 0) ? round($matches / $matched, 1) : 0) ,
                    'avg_turnover' => (($turnover > 0) ? $system->numberFormat($turnover / $matched, 2, ",", ".") : "0,00")
                ];
            }
        }

        $websites = Website::all();

        self::insertReportUsageHistory($rep_id);

        return view('reports.lead_quality', [
            'report' => $report,
            'sirelos' => Website::where("we_sirelo", 1)->orderBy("we_website", "ASC")->get(),
            'websites_filter' => $websites_filter,
            'affiliates_filter' => $affiliates_filter,
            'request_types_filter' => $request_types_filter,
            'date_filter' => $date_filter,
            'websites' => $websites,
            'filtered_data' => $filtered_data,
            'times_used' => self::getTimesUsedLast90Days($rep_id),

            //TOTALS
            'total_leads' => $total_leads,
            'total_matched' => (($total_leads > 0) ? round((100/$total_leads) * $total_matched) : 0 ),
            'total_rejected' => (($total_leads > 0) ? round((100/$total_leads) * $total_rejected) : 0 ),
            'total_with_volume' => (($total_leads > 0) ? round((100/$total_leads) * $total_with_volume) : 0 ),
            'total_with_volume_calc' => (($total_leads > 0) ? round((100/$total_leads) * $total_with_volume_calc) : 0 ),
            'total_with_remarks' => (($total_leads > 0) ? round((100/$total_leads) * $total_with_remarks) : 0 ),
            'total_with_volume_and_remarks' => (($total_leads > 0) ? round((100/$total_leads) * $total_with_volume_and_remarks) : 0 ),
            'total_complete_address_details' => (($total_leads > 0) ? round((100/$total_leads) * $total_complete_address_details) : 0 ),
            'total_complete_origin_address_details' => (($total_leads > 0) ? round((100/$total_leads) * $total_complete_origin_address_details) : 0 ),
            'total_complete_destination_address_details' => (($total_leads > 0) ? round((100/$total_leads) * $total_complete_destination_address_details) : 0 ),
            'total_claims' => (($total_leads > 0) ? round((100/$total_matches) * $total_claims) : 0 ),
            'total_matches' => (($matched > 0) ? round($total_matches / $total_matched, 1) : 0),
            'total_turnover' => (($total_turnover > 0) ? $system->numberFormat($total_turnover/$total_matched, 2, ",", ".") : "0,00"),
        ]);
    }

    public function viewReviewRatingsCorrection(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);

        $sirelo_filter = [];

        if ($request->sirelo != null)
        {
            $sirelo_filter = $request->sirelo;
        }

        $sirelocustomer = new SireloCustomer();
        $data = new Data();

        $filtered_data = [];

        //Normal Sirelo's
        $customers = Customer::leftJoin("websites", "we_sirelo_co_code", "cu_co_code")->where("we_sirelo_active", 1)->whereIn("we_id", $sirelo_filter)->where("cu_deleted", 0)->get();

        if (in_array(55, $sirelo_filter)) {
            //Bucket Sirelo's
            $bucket_customers = Customer::leftJoin("website_subdomains", "wesu_country_code", "cu_co_code")->where("wesu_we_id", 55)->where("cu_deleted", 0)->get();
        }

        foreach ($customers as $customer) {
            $cu_id = $customer->cu_id;

            $review_score_old = $sirelocustomer->getReviews_old($cu_id);

            if ($review_score_old['reviews'] == 0) {
                continue;
            }

            $review_score_new = $sirelocustomer->getReviews($cu_id);

            $rating = $review_score_old['rating'] * 2;
            $new_rating = $review_score_new['rating'] * 2;

            if ($rating > $new_rating) {
                $difference = round($rating - $new_rating, 2);
            }
            else {
                $difference = round($new_rating - $rating, 2);
            }

            $filtered_data[$cu_id] = [
                'customer' => $customer->cu_company_name_business,
                'old_rating' => $rating,
                'new_rating' => $new_rating,
                'total_reviews' => $review_score_new['reviews'],
                'difference' => $difference,
                'country' => $data->country($customer->cu_co_code, 'EN')
            ];
        }

        foreach ($bucket_customers as $customer) {
            $cu_id = $customer->cu_id;

            $review_score_old = $sirelocustomer->getReviews_old($cu_id);

            if ($review_score_old['reviews'] == 0) {
                continue;
            }

            $review_score_new = $sirelocustomer->getReviews($cu_id);

            $rating = $review_score_old['rating'] * 2;
            $new_rating = $review_score_new['rating'] * 2;

            if ($rating > $new_rating) {
                $difference = round($rating - $new_rating, 2);
            }
            else {
                $difference = round($new_rating - $rating, 2);
            }

            $filtered_data[$cu_id] = [
                'customer' => $customer->cu_company_name_business,
                'old_rating' => $rating,
                'new_rating' => $new_rating,
                'total_reviews' => $review_score_new['reviews'],
                'difference' => $difference,
                'country' => $data->country($customer->cu_co_code, 'EN')
            ];
        }


        /*$filtered_data[] = [
            'gathered' => $rows->amount,
            'skipped' => $rows_skipped->amount,
            'processed' => $rows_processed->amount
        ];*/

        self::insertReportUsageHistory($rep_id);

        return view('reports.review_ratings_correction', [
            'report' => $report,
            'sirelos' => Website::where("we_sirelo", 1)->orderBy("we_website", "ASC")->get(),
            'sirelo_filter' => $sirelo_filter,
            'filtered_data' => $filtered_data,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function viewReviewPlatformsPerCountry(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);

        $countries_filter = [];

        if ($request->countries != null)
        {
            $countries_filter = $request->countries;
        }

        $filtered_data = [];
        $total_amount = 0;

        $cugare_query = CustomerGatheredReview::selectRaw("`cu_co_code` as `country_code`, `cugare_platform` as `platform`, (SELECT count(*) FROM customer_review_scores LEFT JOIN `customers` ON `cu_id` = `curesc_cu_id` WHERE cu_co_code = country_code) as `count_companies`, `co_en`")
            ->leftJoin("customers", "cu_id", "cugare_cu_id")
            ->leftJoin("countries", "cu_co_code", "co_code");

        //SELECT count(*) FROM customer_gathered_reviews LEFT JOIN `customers` ON `cu_id` = `cugare_cu_id` WHERE cugare_platform = platform AND cu_co_code = country_code GROUP BY `cugare_platform`

        //SELECT COUNT(`cugare_cu_id`) as `amount` FROM(SELECT DISTINCT(`cugare_cu_id`) FROM customer_gathered_reviews LEFT JOIN `customers` ON `cu_id` = `cugare_cu_id` WHERE cugare_platform = platform AND cu_co_code = country_code) as `total`
        if (!empty($countries_filter)) {
            $cugare_query = $cugare_query->whereIn("cu_co_code", $countries_filter);
        }

        $cugare_query = $cugare_query->groupBy("country_code", "platform", "cugare_cu_id")
            ->where("cugare_platform", "!=", "google")
            ->orderBy("co_en", "ASC")
            ->get();

        foreach ($cugare_query as $cugare) {
            $customer_gathered_reviews = CustomerGatheredReview::selectRaw("COUNT(*) as `amount`")
                ->leftJoin("customers", "cu_id", "cugare_cu_id")
                ->where("cu_co_code", $cugare->country_code)
                ->where("cugare_platform", $cugare->platform)
                ->groupBy("cugare_platform", "cugare_cu_id")
                ->get();

            $total_amount+= $customer_gathered_reviews->count();
            $filtered_data[$cugare->country_code."_".strtolower($cugare->platform)] = [
                'country' => $cugare->co_en,
                'amount_gathered_companies' => $cugare->count_companies,
                'platform' => ucfirst($cugare->platform),
                'amount' => $customer_gathered_reviews->count()
            ];
        }

        /*$filtered_data[] = [
            'gathered' => $rows->amount,
            'skipped' => $rows_skipped->amount,
            'processed' => $rows_processed->amount
        ];*/

        self::insertReportUsageHistory($rep_id);

        return view('reports.review_platforms_per_country', [
            'report' => $report,
            'countries' => Country::all(),
            'countries_filter' => $countries_filter,
            'filtered_data' => $filtered_data,
            'times_used' => self::getTimesUsedLast90Days($rep_id),
            'total_amount' => $total_amount
        ]);
    }

    public function viewLeadRevenuesPerHour(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);
        $system = new System();
        $countries_filter = [];
        $destination_filter = null;
        $total_leads = 0;
        $total_leads_matched = 0;
        $total_no_region_occupancy = 0;
        $total_revenue_gross = 0;
        $total_revenue_net = 0;
        $total_matched = 0;

        if($request->date != null){

            $requests = \App\Models\Request::select("re_timestamp", "re_co_code_from", "re_status", "re_id", "re_source", "re_destination_type", "we_sirelo")
                ->join('website_forms', 'requests.re_wefo_id', '=', 'website_forms.wefo_id')
                ->join('websites', 'wefo_we_id', '=', 'we_id')
                ->whereBetween("re_timestamp", $system->betweenDates($request->date))
                ->whereRaw("(`re_status` = 1 OR (`re_status` = 2 AND `re_rejection_reason` IN (2,3)) AND we_sirelo = 0)");
            $destination_filter = $request->destination_type;
            if ($request->countries != null)
            {
                $countries_filter = $request->countries;
                $requests->whereIn("re_co_code_from", $request->countries);
            }
            if($request->destination_type){
                $requests->where("re_destination_type", $request->destination_type);
            }
            $requests = $requests->get();
        }
        $leads_per_country = [];

        foreach ($requests as $a_request){
            $hour = Carbon::parse($a_request->re_timestamp)->format('H');
            $co_code = $a_request->re_co_code_from;

            if (!isset($leads_per_country[$co_code]))
            {
                for($x=0; $x<24; $x++){
                    $leads_per_country[$co_code][sprintf("%02d", $x)] = [
                        "leads" => 0,
                        "leads_matched" => 0,
                        "no_region_occupancy" => 0,
                        "revenue_gross" => 0,
                        "revenue_net" => 0,
                        "matches" => 0,
                    ];
                }
            }
            $leads_per_country[$co_code][$hour]['leads']++;
            $total_leads++;

            if($a_request->re_status == 1){
                $leads_per_country[$co_code][$hour]['leads_matched']++;
                $total_matched++;

                $matches = \App\Models\KTRequestCustomerPortal::selectRaw("ktrecupo_re_id, ktrecupo_amount AS revenue_gross, ktrecupo_amount_netto AS revenue_net, ktrecupo_free")
                    ->where("ktrecupo_re_id", $a_request->re_id)
                    ->get();
                foreach($matches as $match){
                    $leads_per_country[$co_code][$hour]["matches"]++;
                    $total_leads_matched++;
                    if($match->ktrecupo_free == 0){
                        $leads_per_country[$co_code][$hour]["revenue_gross"] += $match->revenue_gross;
                        $leads_per_country[$co_code][$hour]["revenue_net"] += $match->revenue_net;
                        $total_revenue_gross += $match->revenue_gross;
                        $total_revenue_net += $match->revenue_net;
                    }
                }
            }
            else{
                $leads_per_country[$co_code][$hour]['no_region_occupancy']++;
                $total_no_region_occupancy ++;
            }
        }

        $total_amount = 0;

        $countries_list = [];
        foreach(Country::get() as $country){
            $countries_list[$country->co_code] = $country->co_en;
        }

        self::insertReportUsageHistory($rep_id);

        return view('reports.lead_hourly_revenues', [
            'report' => $report,
            'date_filter' => $request->date,
            'countries' => Country::all(),
            'request_source' => RequestSource::all(),
            'destination_type'=> DestinationType::all(),
            'total_leads' => $total_leads,
            'total_leads_matched' => $total_leads_matched,
            'total_no_region_occupancy' => $total_no_region_occupancy,
            'total_revenue_gross' => $total_revenue_gross,
            'total_revenue_net' => $total_revenue_net,
            'total_matched' => $total_matched,
            'destination_filter' => $destination_filter,
            'countries_list' => $countries_list,
            'countries_filter' => $countries_filter,
            'filtered_data' => $leads_per_country,
            'times_used' => self::getTimesUsedLast90Days($rep_id),
            'total_amount' => $total_amount
        ]);
    }

    public function salesActivity(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);
        $system = new System();
        $filtered_data = [];
        $total_amount = 0;

        if ($request->date != null)
        {
            $requests = KTCustomerProgress::select("us_name", "cu_company_name_business", "ktcupr_type", "ktcupr_timestamp", "us_name", "cu_company_name_business", "ktcupr_id", "moda_source")
                ->leftJoin('customers', 'ktcupr_cu_id', '=', 'cu_id')
                ->leftJoin('users', 'us_id', '=', 'ktcupr_us_id')
                ->leftJoin('mover_data', 'moda_cu_id', '=', 'cu_id')
                ->whereBetween('ktcupr_timestamp', $system->betweenDates($request->date))
                ->get();
        }
        else{
            return Redirect::back()->withErrors(['Please select a date']);
        }

        foreach($requests as $a_request){
            $ktcupr_id = $a_request->ktcupr_id;
            if (!isset($filtered_data[$ktcupr_id])){
                $filtered_data[$ktcupr_id] = [
                    "Name" => '',
                    "Company" => '',
                    "Sales type" => '',
                    "Timestamp" => '',
                    "Source" => ''
                ];
            }
            $filtered_data[$ktcupr_id]["Name"] = $a_request->us_name;
            $filtered_data[$ktcupr_id]["Company"] = $a_request->cu_company_name_business;
            $filtered_data[$ktcupr_id]["Timestamp"] = $a_request->ktcupr_timestamp;
            $filtered_data[$ktcupr_id]["Source"] = $a_request->moda_source;
            $filtered_data[$ktcupr_id]["Sales type"] = $a_request->ktcupr_type;

        }


        self::insertReportUsageHistory($rep_id);

        return view('reports.sales_activity', [
            'report' => $report,
            'date_filter' => $request->date,
            'filtered_data' => $filtered_data,
            'customer_source_type' => CustomerSourceType::all(),
            'sales_type' => CustomerProgressType::all(),
            'times_used' => self::getTimesUsedLast90Days($rep_id),
            'total_amount' => $total_amount
        ]);
    }

    public function associatedCompanies(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);
        $filtered_data = [];


        if($request->association != null){
            $memberships = Membership::select("ktcume_link", "cu_deleted", "me_name", "me_id", "cu_id", "ktcume_id", "ktcume_cu_id", "ktcume_me_id", "cu_company_name_business", "me_location", "cu_co_code", "co_code", "co_en")
                ->leftJoin("kt_customer_membership", "me_id", "=", "ktcume_me_id")
                ->leftJoin("customers", "cu_id", "=", "ktcume_cu_id")
                ->leftJoin("countries", "cu_co_code", "=", "co_code")
                ->where("me_id", $request->association)
                ->where("cu_deleted", 0)
                ->get();
        }
        else{
            return Redirect::back()->withErrors(['Please select an association']);
        }

        foreach($memberships as $a_membership){
            $customer_id = $a_membership->cu_id;
            if (!isset($filtered_data[$customer_id])){
                $filtered_data[$customer_id] = [
                    "Name" => '',
                    "Country" => '',
                    "Company" => '',
                    "Website" => '',
                    "Id" => 0
                ];
            }
            $filtered_data[$customer_id]["Name"] = $a_membership->me_name;
            $filtered_data[$customer_id]["Company"] = $a_membership->cu_company_name_business;
            $filtered_data[$customer_id]["Website"] = $a_membership->ktcume_link;
            $filtered_data[$customer_id]["Country"] = $a_membership->co_en;
            $filtered_data[$customer_id]["Id"] = $a_membership->cu_id;
        }



        self::insertReportUsageHistory($rep_id);

        return view('reports.associated_companies', [
            'report' => $report,
            'filtered_data' => $filtered_data,
            'times_used' => self::getTimesUsedLast90Days($rep_id),
            'association_filter' => $request->association,
            'associations' => Membership::all(),
        ]);
    }


    public function viewRecoveredDashboardLeads(Request $webrequest, $rep_id)
    {
        $report = Report::findOrFail($rep_id);
        $system = new System();

        $date_filter = null;

        if ($webrequest->date != null)
        {
            $date_filter = $webrequest->date;
        } else
        {
            return Redirect::back()->withErrors(['Please select a date']);
        }

        $requests = \App\Models\Request::whereBetween("re_timestamp", $system->betweenDates($webrequest->date))
            ->get();

        $recovered_telephone = 0;
        $updated_telephone = 0;

        $rejection_reasons_amount = [];
        $testcounter = 0;

        $prefix = "R/R ";

        $array_reason_to_column = [
            0 => $prefix."Other",
            1 => $prefix."Transport",
            2 => $prefix."No region occupancy M.",
            3 => $prefix."No region occupancy B.",
            4 => $prefix."Contact",
            5 => $prefix."Address",
            6 => $prefix."Too recent",
            7 => $prefix."Too far away",
            8 => $prefix."No relocation",
            9 => $prefix."Domestic",
            //10 => $prefix."Double submit",
            //11 => $prefix."Double submit",
            12 => $prefix."Already received",
            13 => $prefix."Already received",
            14 => $prefix."Already received",
            15 => $prefix."Already received",
            //16 => $prefix."Test lead",
            //17 => $prefix."Test lead",
            //18 => $prefix."Empty lead",
            //19 => $prefix."Spam lead",
            //20 => $prefix."Fake lead",
            //21 => $prefix."Double submit",
            22 => $prefix."Double submit",
        ];

        foreach ($array_reason_to_column as $id => $column) {
            $rejection_reasons_amount[$column] = [
                'rejected'=> 0,
                'recovered' => 0
            ];
        }

        foreach($requests as $request){
            if(RequestUpdate::where("reup_re_id", $request->re_id)->where("reup_key", "LIKE", "%telephone%")->first())
            {
                if ($request->re_rejection_reason != 0 && $request->re_status == 1 && RequestUpdate::where("reup_re_id", $request->re_id)->where("reup_key", "LIKE", "%telephone%")->first())
                {
                    $recovered_telephone += 1;
                }
                $updated_telephone += 1;
            }

            if ($request->re_rejection_reason != 0 && array_key_exists($request->re_rejection_reason, $array_reason_to_column)) {
                $rejection_reasons_amount[$array_reason_to_column[$request->re_rejection_reason]]['rejected']++;

                if ($request->re_status == 1) {
                    $rejection_reasons_amount[$array_reason_to_column[$request->re_rejection_reason]]['recovered']++;
                }
            }
        }

        $telephone_mail = 0;

        foreach($requests as $request){
            if(Revision::where("revisionable_type", "App\Models\Request")->where("revisionable_id", "=", $request->re_id)->where("key", "re_telephone_1_mail")->first()){
                $telephone_mail += 1;
            }
        }


        $filtered_data = [
            "leads" => count($requests),
            "rejected"  => count($requests->where("re_rejection_reason", "!=", 0)),
            "recovered" => count($requests->where("re_rejection_reason", "!=", 0)->where("re_status", 1)),
            "recovered_telephone" => $recovered_telephone,
            "telephone_mail" => $telephone_mail,
            "updated_telephone" => $updated_telephone,
            "rejection_reasons" => $rejection_reasons_amount
        ];

        $filtered_data["percentages"] = [
            "rejected" => $filtered_data["rejected"]/$filtered_data["leads"] * 100,
            "recovered" => $filtered_data["recovered"]/$filtered_data["rejected"] * 100,
            "recovered_telephone" => $filtered_data["recovered_telephone"]/$filtered_data["recovered"] * 100,
        ];

        self::insertReportUsageHistory($rep_id);

        return view('reports.recovered_dashboard_leads', [
            'report' => $report,
            'sirelos' => Website::where("we_sirelo", 1)->orderBy("we_website", "ASC")->get(),
            'filtered_data' => $filtered_data,
            'date_filter' => $date_filter,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function viewTurnoverPerSalesManager(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);

        $sales_manager_filter = null;

        if ($request->sales_manager != null)
        {
            $sales_manager_filter = $request->sales_manager;
        } else
        {
            return Redirect::back()->withErrors(['Please select a sales manager']);
        }

        $filtered_data = [];

        $months = [];

        foreach (range(1, 13) as $min_month)
        {
            $months[] = date("Y-m", strtotime("first day of -" . $min_month . " months"));
        }

        $query = Customer::leftJoin("invoices", "cu_id", "in_cu_id")
            ->where("cu_sales_manager", $request->sales_manager)
            ->where("cu_deleted", 0)
            ->whereIn("cu_type", [1, 2, 4, 6])
            ->whereBetween("in_date", [date('Y-m-d', strtotime(end($months) . '-01')), date('Y-m-t', strtotime($months[0] . '-01'))])
            ->groupBy("cu_id")
            ->orderBy("cu_company_name_business", "asc")
            ->get();

        $total_invoices = 0;

        $total_month_0 = 0;
        $total_month_1 = 0;
        $total_month_2 = 0;
        $total_month_3 = 0;
        $total_month_4 = 0;
        $total_month_5 = 0;
        $total_month_6 = 0;
        $total_month_7 = 0;
        $total_month_8 = 0;
        $total_month_9 = 0;
        $total_month_10 = 0;
        $total_month_11 = 0;
        $total_month_12 = 0;

        $total_total = 0;

        foreach ($query as $row)
        {
            $total = 0;

            $query_invoices = Invoice::where("in_cu_id", $row->cu_id)
                ->whereBetween("in_date", [date('Y-m-d', strtotime(end($months) . '-01')), date('Y-m-t', strtotime($months[0] . '-01'))])
                ->get();

            $invoices = sizeof($query_invoices);

            foreach ($months as $month_id => $month)
            {
                $date_from = date("Y-m-d", strtotime($month . "-01"));
                $date_to = date("Y-m-t", strtotime($date_from));

                $amount = 0;
                $pay_status_color = 'black';
                $invoice = [];

                //TODO: Klopt dit?
                $query = Invoice::leftJoin("customers", "cu_id", "in_cu_id")
                    ->where("cu_id", $row->cu_id)
                    ->where("cu_deleted", 0)
                    ->whereBetween("in_date", [$date_from, $date_to])
                    ->get();

                foreach ($query as $row_invoices)
                {
                    $ledger_accounts = KTBankLineInvoiceCustomerLedgerAccount::select("ktbaliinculeac_in_id", "ktbaliinculeac_amount", "ktbaliinculeac_amount_FC")
                        ->where("ktbaliinculeac_in_id", $row_invoices->in_id)
                        ->get();

                    $invoice[$row_invoices->in_id]['amount_paired'] = 0;
                    $invoice[$row_invoices->in_id]['amount_paired_fc'] = 0;

                    foreach ($ledger_accounts as $ledger)
                    {
                        $invoice[$row_invoices->in_id]['amount_paired'] += $ledger->ktbaliinculeac_amount;
                        $invoice[$row_invoices->in_id]['amount_paired_fc'] += $ledger->ktbaliinculeac_amount_FC;
                    }

                    $fully_paid = ($row_invoices->in_amount_netto_eur == round($invoice[$row_invoices->in_id]['amount_paired'], 2) ? 1 : 0);
                    if ($fully_paid)
                    {
                        $pay_status_color = 'black';
                    } elseif ($invoice[$row_invoices->in_id]['amount_paired'] == 0)
                    {
                        $pay_status_color = 'red';
                    } elseif ($invoice[$row_invoices->in_id]['amount_paired'] > 0)
                    {
                        $pay_status_color = 'orange';
                    }

                    $amount += $row_invoices->in_amount_gross_eur;
                }

                $total += $amount;
                ${"total_month_" . $month_id} += $amount;
                $total_total += $amount;

                $filtered_data[$row->cu_id]['month_' . $month_id] = $amount;
                $filtered_data[$row->cu_id]['month_' . $month_id . '_pay_status_color'] = $pay_status_color;
            }

            $total_invoices += $invoices;

            $cu_status = System::getCustomerStatus($row->cu_id);

            if ($cu_status == "MV - Prospect"){
                $cu_status = MoverStatus::name(4);
            }

            $filtered_data[$row->cu_id] = [
                'customer' => $row->cu_company_name_business,
                'method' => (($row->cu_payment_method == 4 || $row->cu_payment_method == 5 || $row->cu_payment_method == 6) ? "Pre" : "Post"),
                'invoices' => $invoices,
                'status' => $cu_status,
                'credit_hold' => (($row->cu_credit_hold) ? "Yes" : "No"),
                'month_0' => System::paymentCurrencyToken("EUR") . " " . System::numberFormat($filtered_data[$row->cu_id]['month_0'], 2),
                'month_1' => System::paymentCurrencyToken("EUR") . " " . System::numberFormat($filtered_data[$row->cu_id]['month_1'], 2),
                'month_2' => System::paymentCurrencyToken("EUR") . " " . System::numberFormat($filtered_data[$row->cu_id]['month_2'], 2),
                'month_3' => System::paymentCurrencyToken("EUR") . " " . System::numberFormat($filtered_data[$row->cu_id]['month_3'], 2),
                'month_4' => System::paymentCurrencyToken("EUR") . " " . System::numberFormat($filtered_data[$row->cu_id]['month_4'], 2),
                'month_5' => System::paymentCurrencyToken("EUR") . " " . System::numberFormat($filtered_data[$row->cu_id]['month_5'], 2),
                'month_6' => System::paymentCurrencyToken("EUR") . " " . System::numberFormat($filtered_data[$row->cu_id]['month_6'], 2),
                'month_7' => System::paymentCurrencyToken("EUR") . " " . System::numberFormat($filtered_data[$row->cu_id]['month_7'], 2),
                'month_8' => System::paymentCurrencyToken("EUR") . " " . System::numberFormat($filtered_data[$row->cu_id]['month_8'], 2),
                'month_9' => System::paymentCurrencyToken("EUR") . " " . System::numberFormat($filtered_data[$row->cu_id]['month_9'], 2),
                'month_10' => System::paymentCurrencyToken("EUR") . " " . System::numberFormat($filtered_data[$row->cu_id]['month_10'], 2),
                'month_11' => System::paymentCurrencyToken("EUR") . " " . System::numberFormat($filtered_data[$row->cu_id]['month_11'], 2),
                'month_12' => System::paymentCurrencyToken("EUR") . " " . System::numberFormat($filtered_data[$row->cu_id]['month_12'], 2),
                'month_0_pay_status_color' => $filtered_data[$row->cu_id]['month_0_pay_status_color'],
                'month_1_pay_status_color' => $filtered_data[$row->cu_id]['month_1_pay_status_color'],
                'month_2_pay_status_color' => $filtered_data[$row->cu_id]['month_2_pay_status_color'],
                'month_3_pay_status_color' => $filtered_data[$row->cu_id]['month_3_pay_status_color'],
                'month_4_pay_status_color' => $filtered_data[$row->cu_id]['month_4_pay_status_color'],
                'month_5_pay_status_color' => $filtered_data[$row->cu_id]['month_5_pay_status_color'],
                'month_6_pay_status_color' => $filtered_data[$row->cu_id]['month_6_pay_status_color'],
                'month_7_pay_status_color' => $filtered_data[$row->cu_id]['month_7_pay_status_color'],
                'month_8_pay_status_color' => $filtered_data[$row->cu_id]['month_8_pay_status_color'],
                'month_9_pay_status_color' => $filtered_data[$row->cu_id]['month_9_pay_status_color'],
                'month_10_pay_status_color' => $filtered_data[$row->cu_id]['month_10_pay_status_color'],
                'month_11_pay_status_color' => $filtered_data[$row->cu_id]['month_11_pay_status_color'],
                'month_12_pay_status_color' => $filtered_data[$row->cu_id]['month_12_pay_status_color'],
                "total" => System::paymentCurrencyToken("EUR") . " " . System::numberFormat($total, 2),
            ];
        }

        if (!empty($_POST)){
            self::insertReportUsageHistory($rep_id);
        }

        return view('reports.turnover_per_sales_manager', [
            'report' => $report,
            'sales_manager_filter' => $sales_manager_filter,
            'filtered_data' => $filtered_data,
            'months' => $months,

            //TOTALS
            'total_invoices' => $total_invoices,
            'total_total' => System::paymentCurrencyToken("EUR") . " " . System::numberFormat($total_total, 2),
            'total_month_0' => System::paymentCurrencyToken("EUR") . " " . System::numberFormat($total_month_0, 2),
            'total_month_1' => System::paymentCurrencyToken("EUR") . " " . System::numberFormat($total_month_1, 2),
            'total_month_2' => System::paymentCurrencyToken("EUR") . " " . System::numberFormat($total_month_2, 2),
            'total_month_3' => System::paymentCurrencyToken("EUR") . " " . System::numberFormat($total_month_3, 2),
            'total_month_4' => System::paymentCurrencyToken("EUR") . " " . System::numberFormat($total_month_4, 2),
            'total_month_5' => System::paymentCurrencyToken("EUR") . " " . System::numberFormat($total_month_5, 2),
            'total_month_6' => System::paymentCurrencyToken("EUR") . " " . System::numberFormat($total_month_6, 2),
            'total_month_7' => System::paymentCurrencyToken("EUR") . " " . System::numberFormat($total_month_7, 2),
            'total_month_8' => System::paymentCurrencyToken("EUR") . " " . System::numberFormat($total_month_8, 2),
            'total_month_9' => System::paymentCurrencyToken("EUR") . " " . System::numberFormat($total_month_9, 2),
            'total_month_10' => System::paymentCurrencyToken("EUR") . " " . System::numberFormat($total_month_10, 2),
            'total_month_11' => System::paymentCurrencyToken("EUR") . " " . System::numberFormat($total_month_11, 2),
            'total_month_12' => System::paymentCurrencyToken("EUR") . " " . System::numberFormat($total_month_12, 2),
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function viewServiceProviderRequests(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);

        $date_filter = null;
        $service_provider_filter = null;
        $sent_filter = null;
        $total_gross_eu = 0;
        $total_netto_eu = 0;

        if ($request->date != null)
        {
            $date_filter = $request->date;
        } else
        {
            return Redirect::back()->withErrors(['Please select a date']);
        }

        if ($request->service_provider != null)
        {
            $service_provider_filter = $request->service_provider;
        }

        $sent_filter = $request->sent;

        $filtered_data = [];

        $query = KTRequestCustomerQuestion::leftJoin("requests", "ktrecuqu_re_id", "re_id")
            ->leftJoin("customers", "ktrecuqu_cu_id", "cu_id")
            ->leftJoin("questions", "ktrecuqu_qu_id", "qu_id")
            ->where("ktrecuqu_hide", 0)
            ->where("cu_deleted", 0);

        if ($service_provider_filter != null)
        {
            $query = $query->where("ktrecuqu_cu_id", $service_provider_filter);
        }

        if ($sent_filter == "")
        {
            $query = $query->whereBetween("ktrecuqu_timestamp", System::betweenDates($request->date));
        } elseif ($sent_filter == 1)
        {
            $query = $query->where("ktrecuqu_sent", $sent_filter)
                ->whereBetween("ktrecuqu_sent_timestamp", System::betweenDates($request->date));
        } else
        {
            $query = $query->where("ktrecuqu_sent", $sent_filter)
                ->whereBetween("ktrecuqu_timestamp", System::betweenDates($request->date));
        }

        $query = $query->orderBy("ktrecuqu_timestamp", "asc")
            ->get();

        foreach ($query as $row)
        {
            $to_gross = System::currencyConvert($row->ktrecuqu_amount, $row->ktrecuqu_currency, "EUR");

            $to_netto = 0;

            if ($row->ktrecuqu_sent && !$row->ktrecuqu_free)
            {
                $to_netto += $to_gross;
            } elseif (!$row->ktrecuqu_sent && ($row->re_status == 1 || ($row->re_status == 2 && $row->ktrecuqu_rejection_status == 1)))
            {
                $to_netto += $to_gross;
            }

            $filtered_data[$row->ktrecuqu_id] = [
                "id" => $row->ktrecuqu_id,
                "cu_re_id" => $row->ktrecuqu_cu_re_id,
                "submitted" => $row->ktrecuqu_timestamp,
                "sent" => (($row->ktrecuqu_sent) ? "Yes" : "No" . (($row->ktrecuqu_sent == 0 && $row->re_status == 2) ? " (" . ServiceProviderRejectionStatus::all()[$row->ktrecuqu_rejection_status] . ")" : "")),
                "sent_date" => (($row->ktrecuqu_sent) ? $row->ktrecuqu_sent_timestamp : "-"),
                "service_provider" => $row->cu_company_name_business,
                "question" => $row->qu_name,
                "answer_1" => $row->ktrecuqu_answer_1,
                "answer_2" => $row->ktrecuqu_answer_2,
                "status" => RequestStatus::all()[$row->re_status],
                "name" => $row->re_full_name,
                "email" => $row->re_email,
                "country_from" => DataIndex::getCountry($row->re_co_code_from),
                "country_to" => DataIndex::getCountry($row->re_co_code_to),
                "free" => (($row->ktrecuqu_free) ? "Yes" : "No"),
                "to_gross_fc" => System::paymentCurrencyToken($row->ktrecuqu_currency) . " " . System::numberFormat($row->ktrecuqu_amount, 2),
                "to_gross_eu" => System::paymentCurrencyToken("EUR") . " " . System::numberFormat($to_gross, 2),
                "to_netto_eu" => System::paymentCurrencyToken("EUR") . " " . System::numberFormat($to_netto, 2),
                "origin" => (($row->re_source == 1) ? System::getWebsiteForm($row->re_wefo_id) : System::affiliatePartnerForm($row->re_afpafo_id)),
            ];

            $total_gross_eu += $to_gross;
            $total_netto_eu += $to_netto;
        }

        if (!empty($_POST)){
            self::insertReportUsageHistory($rep_id);
        }

        return view('reports.service_provider_requests', [
            'report' => $report,
            'date_filter' => $date_filter,
            'service_provider_filter' => $service_provider_filter,
            'sent_filter' => $sent_filter,
            'filtered_data' => $filtered_data,

            //TOTALS
            'total_gross_eu' => System::paymentCurrencyToken("EUR") . " " . System::numberFormat($total_gross_eu, 2),
            'total_netto_eu' => System::paymentCurrencyToken("EUR") . " " . System::numberFormat($total_netto_eu, 2),
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function viewRequests(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);
        $date_filter = null;
        $portal_filter = null;
        $status_filter = null;
        $request_statuses = RequestStatus::all();
        $rejection_reasons = RejectionReason::all();
        $total_turnover = 0;
        $request_types = RequestType::all();
        $moving_sizes = MovingSize::all();

        if ($request->date != null)
        {
            $date_filter = $request->date;
        } else
        {
            return Redirect::back()->withErrors(['Please select a date']);
        }

        if ($request->portal != null)
        {
            $portal_filter = $request->portal;
        }

        if ($request->status != null)
        {
            $status_filter = $request->status;
        }

        $destination_checked = [];

        if (isset($request->destination_type))
        {
            foreach ($request->destination_type as $destination_type => $on)
            {
                $destination_checked[] = $destination_type;
            }
        }

        $moving_size_checked = [];

        if (isset($request->moving_size))
        {
            foreach ($request->moving_size as $moving_size => $on)
            {
                $moving_size_checked[] = $moving_size;
            }
        }

        $filtered_data = [];

        $query = \App\Models\Request::whereBetween("re_timestamp", System::betweenDates($request->date));

        if ($portal_filter != null)
        {
            $query = $query->where("re_po_id", $portal_filter);
        }
        if ($status_filter != null)
        {
            $query = $query->where("re_status", $status_filter);
        }
        if (!empty($destination_checked))
        {
            $query = $query->whereIn("re_destination_type", $destination_checked);
        }
        if (!empty($moving_size_checked))
        {
            $query = $query->whereIn("re_moving_size", $moving_size_checked);
        }

        $query = $query->orderBy("re_timestamp", "asc")
            ->get();

        foreach ($query as $row)
        {
            $query_request_customer_portal = KTRequestCustomerPortal::leftJoin("portals", "ktrecupo_po_id", "po_id")
                ->where("ktrecupo_re_id", $row->re_id)
                ->get();

            $matched = 0;
            $free = 0;
            $turnover = 0;

            foreach ($query_request_customer_portal as $row_request_customer_portal)
            {
                $matched++;

                if ($row_request_customer_portal->ktrecupo_free)
                {
                    $free++;
                } else
                {
                    $turnover += System::currencyConvert($row_request_customer_portal->ktrecupo_amount_netto, $row_request_customer_portal->po_pacu_code, "EUR");
                }
            }

            $total_turnover += $turnover;

            $filtered_data[$row->re_id] = [
                "re_id" => $row->re_id,
                "timestamp" => $row->re_timestamp,
                "status" => $request_statuses[$row->re_status],
                "rejection_reason" => (($status_filter == 2) ? $rejection_reasons[$row->re_rejection_reason][0] : ""),
                "name" => $row->re_full_name,
                "email" => $row->re_email,
                "region_from" => DataIndex::getRegion($row->re_reg_id_from),
                "country_from" => DataIndex::getCountry($row->re_co_code_from),
                "region_to" => DataIndex::getRegion($row->re_reg_id_to),
                "country_to" => DataIndex::getCountry($row->re_co_code_to),
                "request_type" => $request_types[$row->re_request_type],
                "moving_size" => $moving_sizes[$row->re_moving_size],
                "moving_date" => $row->re_moving_date,
                "volume" => $row->re_volume_m3,
                "matched" => $matched,
                "free" => $free,
                "netto" => ($matched - $free),
                "turnover" => System::paymentCurrencyToken("EUR") . " " . System::numberFormat($turnover, 2),
                "origin" => (($row->re_source == 1) ? System::getWebsiteForm($row->re_wefo_id) : System::affiliatePartnerForm($row->re_afpafo_id))
            ];
        }

        if (!empty($_POST)){
            self::insertReportUsageHistory($rep_id);
        }

        return view('reports.requests', [
            'report' => $report,
            'date_filter' => $date_filter,
            'portal_filter' => $portal_filter,
            'status_filter' => $status_filter,
            'filtered_data' => $filtered_data,
            'destination_checked' => $destination_checked,
            'moving_size_checked' => $moving_size_checked,
            'total_turnover' => System::paymentCurrencyToken("EUR") . " " . System::numberFormat($total_turnover, 2),
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function viewRejectedRequests(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);
        $date_filter = null;
        $portal_filter = null;
        $rejection_reason_filter = null;
        $recover_button_filter = null;

        $rejection_reasons = RejectionReason::all();
        $total_turnover = 0;
        $request_types = RequestType::all();
        $moving_sizes = MovingSize::all();

        if ($request->date != null)
        {
            $date_filter = $request->date;
        } else
        {
            return Redirect::back()->withErrors(['Please select a date']);
        }

        if ($request->portal != null)
        {
            $portal_filter = $request->portal;
        }

        if (isset($request->recover_button))
        {
            $recover_button_filter = 1;
        }

        if ($request->rejection_reason != null)
        {
            $rejection_reason_filter = $request->rejection_reason;
        }

        $destination_checked = [];

        if (isset($request->destination_type))
        {
            foreach ($request->destination_type as $destination_type => $on)
            {
                $destination_checked[] = $destination_type;
            }
        }

        $moving_size_checked = [];

        if (isset($request->moving_size))
        {
            foreach ($request->moving_size as $moving_size => $on)
            {
                $moving_size_checked[] = $moving_size;
            }
        }

        $filtered_data = [];

        $query = \App\Models\Request::whereBetween("re_rejection_timestamp", System::betweenDates($request->date))
            ->where("re_status", 2);

        if ($portal_filter != null)
        {
            $query = $query->where("re_po_id", $portal_filter);
        }
        if (!empty($destination_checked))
        {
            $query = $query->whereIn("re_destination_type", $destination_checked);
        }
        if (!empty($moving_size_checked))
        {
            $query = $query->whereIn("re_moving_size", $moving_size_checked);
        }
        if ($rejection_reason_filter != null)
        {
            if (array_key_exists($request->rejection_reason, DataIndex::CombinedReasons()))
            {
                $rejection_reasons = DataIndex::CombinedReasons()[$rejection_reason_filter];

                $query = $query->whereIn("re_rejection_reason", $rejection_reasons);
            } else
            {
                $query = $query->where("re_rejection_reason", $rejection_reason_filter);
            }
        }
        $query = $query->orderBy("re_rejection_timestamp", "asc")
            ->get();

        foreach ($query as $row)
        {
            $filtered_data[$row->re_id] = [
                "re_id" => $row->re_id,
                "timestamp" => $row->re_timestamp,
                "rejected" => $row->re_rejection_timestamp,
                "rejected_by" => (($row->re_rejection_us_id == 0) ? "System" : User::where("us_id", $row->re_rejection_us_id)->first()->us_name),
                "rejection_reason" => $rejection_reasons[$row->re_rejection_reason][0],
                "name" => $row->re_full_name,
                "email" => $row->re_email,
                "country_from" => DataIndex::getCountry($row->re_co_code_from),
                "country_to" => DataIndex::getCountry($row->re_co_code_to),
                "request_type" => $request_types[$row->re_request_type],
                "moving_size" => $moving_sizes[$row->re_moving_size],
                "moving_date" => $row->re_moving_date,
                "volume" => $row->re_volume_m3,
                "origin" => (($row->re_source == 1) ? System::getWebsiteForm($row->re_wefo_id) : System::affiliatePartnerForm($row->re_afpafo_id))
            ];
        }

        if (!empty($_POST)){
            self::insertReportUsageHistory($rep_id);
        }

        return view('reports.rejected_requests', [
            'report' => $report,
            'date_filter' => $date_filter,
            'portal_filter' => $portal_filter,
            'rejection_reason_filter' => $rejection_reason_filter,
            'recover_button_filter' => $recover_button_filter,
            'filtered_data' => $filtered_data,
            'destination_checked' => $destination_checked,
            'moving_size_checked' => $moving_size_checked,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function viewRequestsPerRejectionReason(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);
        $date_filter = null;
        $request_type_filter = [];
        $request_origin_filter = null;
        $request_types = RequestType::all();
        $moving_sizes = MovingSize::all();

        if ($request->date != null)
        {
            $date_filter = $request->date;
        } else
        {
            return Redirect::back()->withErrors(['Please select a date']);
        }

        if ($request->request_origin != null)
        {
            $request_origin_filter = $request->request_origin;
        }


        if (isset($request->request_type))
        {
            foreach ($request->request_type as $type => $on)
            {
                $request_type_filter[] = $type;
            }
        }

        $filtered_data = [];

        $total_requests = 0;
        $rejection_reasons = [];

        foreach (RejectionReason::all() as $id => $rejection_reason)
        {
            $query_requests = \App\Models\Request::selectRaw("COUNT(*) as amount")
                ->whereBetween("re_timestamp", System::betweenDates($request->date))
                ->where("re_status", 2)
                ->where("re_rejection_reason", $id);

            if ($request_origin_filter != null)
            {
                $query_requests = $query_requests->where("re_source", $request_origin_filter);
            }
            if (!empty($request_type_filter))
            {
                $query_requests = $query_requests->whereIn("re_request_type", $request_type_filter);
            }

            $query_requests = $query_requests
                ->orderBy("re_rejection_timestamp", "asc")
                ->first();
            $requests = (int)$query_requests->amount;

            $rejection_reasons[] = [
                "rejection_reason" => $rejection_reason[0],
                "requests" => $requests
            ];

            $total_requests += $requests;
        }

        foreach ($rejection_reasons as $id => $rejection_reason)
        {
            if ($rejection_reason['requests'] != 0 && $total_requests != 0)
            {
                $percentage = (($rejection_reason['requests'] / $total_requests) * 100);
            } else
            {
                $percentage = 0;
            }

            $filtered_data[$id] = [
                "rejection_reason" => $rejection_reason['rejection_reason'],
                "requests" => $rejection_reason['requests'],
                "percentage" => System::numberFormat($percentage, 2) . "%"
            ];
        }

        if (!empty($_POST)){
            self::insertReportUsageHistory($rep_id);
        }

        return view('reports.requests_per_rejection_reason', [
            'report' => $report,
            'date_filter' => $date_filter,
            'request_type_filter' => $request_type_filter,
            'request_origin_filter' => $request_origin_filter,
            'filtered_data' => $filtered_data,

            //TOTALS
            'total_requests' => $total_requests,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function viewSurveysPerMoverRequest(Request $request, $rep_id)
    {
        $report = Report::findOrFail($rep_id);
        $date_filter = null;
        $status_filter = [];

        $request_types = RequestType::all();
        $moving_sizes = MovingSize::all();

        if ($request->date != null)
        {
            $date_filter = $request->date;
        } else
        {
            return Redirect::back()->withErrors(['Please select a date']);
        }


        if (isset($request->status))
        {
            foreach ($request->status as $status => $on)
            {
                $status_filter[] = $status;
            }
        }

        $filtered_data = [];

        $query = Customer::select("cu_id", "cu_company_name_business")
            ->leftJoin("kt_customer_portal", "cu_id", "ktcupo_cu_id")
            ->whereRaw("(cu_type = 1 OR cu_type = 6)")
            ->where("cu_deleted", 0);

        if (!empty($status_filter))
        {
            $query = $query->whereIn("ktcupo_status", $status_filter);
        }

        $query = $query
            ->groupBy("cu_id")
            ->orderBy("cu_company_name_business", "asc")
            ->get();

        $total_requests = 0;
        $total_responses = 0;
        $total_response_1 = 0;
        $total_response_2 = 0;
        $total_response_3 = 0;
        $total_response_0 = 0;

        foreach ($query as $row)
        {
            $query_request_customer_portal = KTRequestCustomerPortal::select("ktrecupo_re_id")
                ->whereBetween("ktrecupo_timestamp", System::betweenDates($request->date))
                ->where("ktrecupo_cu_id", $row->cu_id)
                ->get();

            $requests = 0;
            $request_ids = [];

            $responses = 0;
            $response_1 = 0;
            $response_2 = 0;
            $response_3 = 0;
            $response_0 = 0;

            foreach ($query_request_customer_portal as $row_request_customer_portal)
            {
                $requests++;

                $request_ids[] = $row_request_customer_portal->ktrecupo_re_id;
            }

            if (!empty($request_ids))
            {
                $query_survey_1_customers = Survey1Customer::select("sucu_contacted")
                    ->leftJoin("surveys_1", "sucu_su_id", "su_id")
                    ->where("sucu_cu_id", $row->cu_id)
                    ->whereIn("su_re_id", $request_ids)
                    ->get();

                foreach ($query_survey_1_customers as $row_survey_1_customers)
                {
                    $responses++;

                    if ($row_survey_1_customers->sucu_contacted == 1)
                    {
                        $response_1++;
                    } elseif ($row_survey_1_customers->sucu_contacted == 2)
                    {
                        $response_2++;
                    } elseif ($row_survey_1_customers->sucu_contacted == 3)
                    {
                        $response_3++;
                    } elseif ($row_survey_1_customers->sucu_contacted == 0)
                    {
                        $response_0++;
                    }
                }
            }

            if ($responses != 0 && $requests != 0)
            {
                $responses_percentage = (($responses / $requests) * 100);
            } else
            {
                $responses_percentage = 0;
            }

            if ($response_1 != 0 && $responses != 0)
            {
                $response_1_percentage = (($response_1 / $responses) * 100);
            } else
            {
                $response_1_percentage = 0;
            }

            if ($response_2 != 0 && $responses != 0)
            {
                $response_2_percentage = (($response_2 / $responses) * 100);
            } else
            {
                $response_2_percentage = 0;
            }

            if ($response_3 != 0 && $responses != 0)
            {
                $response_3_percentage = (($response_3 / $responses) * 100);
            } else
            {
                $response_3_percentage = 0;
            }

            if ($response_0 != 0 && $responses != 0)
            {
                $response_0_percentage = (($response_0 / $responses) * 100);
            } else
            {
                $response_0_percentage = 0;
            }

            $filtered_data[$row->cu_id] = [
                "id" => $row->cu_id,
                "customer" => $row->cu_company_name_business,
                "requests" => $requests,
                "responses" => System::numberFormat($responses_percentage, 2) . "%",
                "responded_within_48_hours" => System::numberFormat($response_1_percentage, 2) . "%",
                "responded_after_48_hours" => System::numberFormat($response_2_percentage, 2) . "%",
                "not_contacted" => System::numberFormat($response_3_percentage, 2) . "%",
                "empty" => System::numberFormat($response_0_percentage, 2) . "%",
                "responses_full" => $responses,
                "responded_within_48_hours_full" => $response_1,
                "responded_after_48_hours_full" => $response_2,
                "not_contacted_full" => $response_3,
                "empty_full" => $response_0,
            ];

            $total_requests += $requests;
            $total_responses += $responses;
            $total_response_1 += $response_1;
            $total_response_2 += $response_2;
            $total_response_3 += $response_3;
            $total_response_0 += $response_0;
        }

        if ($total_responses != 0 && $total_requests != 0)
        {
            $total_responses_percentage = (($total_responses / $total_requests) * 100);
        } else
        {
            $total_responses_percentage = 0;
        }

        if ($total_response_1 != 0 && $total_responses != 0)
        {
            $total_response_1_percentage = (($total_response_1 / $total_responses) * 100);
        } else
        {
            $total_response_1_percentage = 0;
        }

        if ($total_response_2 != 0 && $total_responses != 0)
        {
            $total_response_2_percentage = (($total_response_2 / $total_responses) * 100);
        } else
        {
            $total_response_2_percentage = 0;
        }

        if ($total_response_3 != 0 && $total_responses != 0)
        {
            $total_response_3_percentage = (($total_response_3 / $total_responses) * 100);
        } else
        {
            $total_response_3_percentage = 0;
        }

        if ($total_response_0 != 0 && $total_responses != 0)
        {
            $total_response_0_percentage = (($total_response_0 / $total_responses) * 100);
        } else
        {
            $total_response_0_percentage = 0;
        }

        if (!empty($_POST)){
            self::insertReportUsageHistory($rep_id);
        }

        return view('reports.surveys_per_mover_request', [
            'report' => $report,
            'date_filter' => $date_filter,
            'status_filter' => $status_filter,
            'filtered_data' => $filtered_data,

            //TOTALS
            'total_requests' => $total_requests,
            'total_responses' => System::numberFormat($total_responses_percentage, 2) . "%",
            'total_responded_within_48_hours' => System::numberFormat($total_response_1_percentage, 2) . "%",
            'total_responded_after_48_hours' => System::numberFormat($total_response_2_percentage, 2) . "%",
            'total_not_contacted' => System::numberFormat($total_response_3_percentage, 2) . "%",
            'total_empty' => System::numberFormat($total_response_0_percentage, 2) . "%",
            'total_requests_full' => $total_requests,
            'total_responses_full' => $total_responses,
            'total_responded_within_48_hours_full' => $total_response_1,
            'total_responded_after_48_hours_full' => $total_response_2,
            'total_not_contacted_full' => $total_response_3,
            'total_empty_full' => $total_response_0,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function viewSireloCustomers($rep_id)
    {
        $countries = Country::all();
        $report = Report::findOrFail($rep_id);

        return view('reports.sirelo_customers', [
            'countries' => $countries,
            'bothyesno' => BothYesNo::all(),
            'report' => $report,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function cloneSireloCustomer($rep_id, $repr_id)
    {
        echo self::viewFinishedSireloCustomer($rep_id, $repr_id, true);
    }

    public function queueSireloCustomers(Request $request, $rep_id)
    {
        $request->validate([
            'report_comment' => 'required'
        ]);

        self::insertReportUsageHistory($rep_id);

        $repr_id = Reporting::addToQueue($rep_id, "sirelo_customers", $request->request->all());

        return redirect('reports/' . $rep_id . '/view/sirelo_customers/' . $repr_id . '/view');
    }

    public function viewFinishedSireloCustomer($rep_id, $repr_id, $clone = false)
    {
        $report_progress = ReportProgress::where("repr_id", $repr_id)->first();
        $report = Report::findOrFail($rep_id);
        $countries = Country::all();

        if ($report_progress->repr_status != 3)
        {
            return view('reports.sirelo_customers', [
                'countries' => $countries,
                'bothyesno' => BothYesNo::all(),
                'report' => $report,
                'repr_id' => $repr_id,
                'selected_countries' => System::unserialize($report_progress->repr_posted_values)['country'],
                'clone' => $clone
            ]);
        }

        $filename = config("app.shared_folder") . "uploads/reports/report-result-" . $repr_id . ".html";
        $file = File::get($filename);

        return view('reports.sirelo_customers', [
            'countries' => $countries,
            'bothyesno' => BothYesNo::all(),
            'report' => $report,
            'file' => $file,
            'repr_id' => $repr_id,
            'report_progress' => $report_progress,
            'selected_countries' => System::unserialize($report_progress->repr_posted_values)['country'],
            'clone' => $clone,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function viewCustomerLogins($rep_id)
    {
        $countries = Country::all();
        $report = Report::findOrFail($rep_id);

        return view('reports.customer_logins', [
            'countries' => $countries,
            'bothyesno' => BothYesNo::all(),
            'report' => $report,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function cloneCustomerLogins($rep_id, $repr_id)
    {
        echo self::viewFinishedCustomerLogins($rep_id, $repr_id, true);
    }

    public function queueCustomerLogins(Request $request, $rep_id)
    {
        $request->validate([
            'report_comment' => 'required'
        ]);

        $date = explode('-', $request->date);

        $array = [
            'date_from' => $date[0],
            'date_to' => $date[1],
            'report_comment' => $request->report_comment,
            'status' => $request->status,
            'form_submit' => $request->form_submit

        ];

        self::insertReportUsageHistory($rep_id);

        $repr_id = Reporting::addToQueue($rep_id, "customer_logins", $array);

        return redirect('reports/' . $rep_id . '/view/customer_logins/' . $repr_id . '/view');
    }

    public function viewFinishedCustomerLogins($rep_id, $repr_id, $clone = false)
    {
        //dd(System::unserialize("YTo1OntzOjk6ImRhdGVfZnJvbSI7czoxMDoiMjAxOS0wOS0xMSI7czo3OiJkYXRlX3RvIjtzOjEwOiIyMDE5LTA5LTExIjtzOjY6InN0YXR1cyI7YTozOntpOjA7czoyOiJvbiI7aToxO3M6Mjoib24iO2k6MjtzOjI6Im9uIjt9czoxNDoicmVwb3J0X2NvbW1lbnQiO3M6MTg6InRlc3RjdXN0b21lcmxvZ2lucyI7czoxMToiZm9ybV9zdWJtaXQiO3M6MTI6IlF1ZXVlIHJlcG9ydCI7fQ=="));
        $report_progress = ReportProgress::where("repr_id", $repr_id)->first();
        $report = Report::findOrFail($rep_id);
        $countries = Country::all();

        if ($report_progress->repr_status != 3)
        {
            return view('reports.customer_logins', [
                'countries' => $countries,
                'bothyesno' => BothYesNo::all(),
                'report' => $report,
                'repr_id' => $repr_id,
                'clone' => $clone
            ]);
        }

        $filename = config("app.shared_folder") . "uploads/reports/report-result-" . $repr_id . ".html";
        $file = File::get($filename);

        return view('reports.customer_logins', [
            'countries' => $countries,
            'bothyesno' => BothYesNo::all(),
            'report' => $report,
            'file' => $file,
            'repr_id' => $repr_id,
            'report_progress' => $report_progress,
            'clone' => $clone,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function viewCustomerAnalysis($rep_id)
    {
        $countries = Country::all();
        $report = Report::findOrFail($rep_id);

        return view('reports.customer_analysis', [
            'countries' => $countries,
            'bothyesno' => BothYesNo::all(),
            'report' => $report,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function cloneCustomerAnalysis($rep_id, $repr_id)
    {
        echo self::viewFinishedCustomerAnalysis($rep_id, $repr_id, true);
    }

    public function queueCustomerAnalysis(Request $request, $rep_id)
    {
        $request->validate([
            'report_comment' => 'required'
        ]);

        $date = explode('-', $request->date);

        $array = [
            'date_from' => $date[0],
            'date_to' => $date[1],
            'report_comment' => $request->report_comment,
            'status' => $request->status,
            'portal' => $request->portal,
            'request_type' => $request->request_type,
            'form_submit' => $request->form_submit,
            'free_trial' => $request->free_trial,
            'credit_hold' => $request->credit_hold

        ];

        self::insertReportUsageHistory($rep_id);

        $repr_id = Reporting::addToQueue($rep_id, "customer_analysis", $array);

        return redirect('reports/' . $rep_id . '/view/customer_analysis/' . $repr_id . '/view');
    }

    public function viewFinishedCustomerAnalysis($rep_id, $repr_id, $clone = false)
    {
        $report_progress = ReportProgress::where("repr_id", $repr_id)->first();
        $report = Report::findOrFail($rep_id);
        $countries = Country::all();

        if ($report_progress->repr_status != 3)
        {
            return view('reports.customer_analysis', [
                'countries' => $countries,
                'bothyesno' => BothYesNo::all(),
                'report' => $report,
                'repr_id' => $repr_id,
                'selected_portals' => System::unserialize($report_progress->repr_posted_values)['portal'],
                'selected_request_types' => System::unserialize($report_progress->repr_posted_values)['request_type'],
                'report_progress' => $report_progress,
                'clone' => $clone,
                'times_used' => self::getTimesUsedLast90Days($rep_id)
            ]);
        }

        $filename = config("app.shared_folder") . "uploads/reports/report-result-" . $repr_id . ".html";
        $file = File::get($filename);

        return view('reports.customer_analysis', [
            'countries' => $countries,
            'bothyesno' => BothYesNo::all(),
            'report' => $report,
            'file' => $file,
            'repr_id' => $repr_id,
            'report_progress' => $report_progress,
            'selected_portals' => System::unserialize($report_progress->repr_posted_values)['portal'],
            'selected_request_types' => System::unserialize($report_progress->repr_posted_values)['request_type'],
            'report_progress' => $report_progress,
            'clone' => $clone,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function viewCustomerPortalAnalysis($rep_id)
    {
        $countries = Country::all();
        $report = Report::findOrFail($rep_id);
        $portals = Portal::all();

        return view('reports.customer_portal_analysis', [
            'countries' => $countries,
            'portals' => $portals,
            'bothyesno' => BothYesNo::all(),
            'report' => $report,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function cloneCustomerPortalAnalysis($rep_id, $repr_id)
    {
        echo self::viewFinishedCustomerPortalAnalysis($rep_id, $repr_id, true);
    }

    public function queueCustomerPortalAnalysis(Request $request, $rep_id)
    {
        $request->validate([
            'report_comment' => 'required'
        ]);

        $date = explode('-', $request->date);

        $array = [
            'date_from' => $date[0],
            'date_to' => $date[1],
            'report_comment' => $request->report_comment,
            'status' => $request->status,
            'portal' => $request->portal,
            'request_type' => $request->request_type,
            'form_submit' => $request->form_submit,
            'free_trial' => $request->free_trial,
            'credit_hold' => $request->credit_hold

        ];

        self::insertReportUsageHistory($rep_id);

        $repr_id = Reporting::addToQueue($rep_id, "customer_portal_analysis", $array);

        return redirect('reports/' . $rep_id . '/view/customer_portal_analysis/' . $repr_id . '/view');
    }

    public function viewFinishedCustomerPortalAnalysis($rep_id, $repr_id, $clone = false)
    {
        $report_progress = ReportProgress::where("repr_id", $repr_id)->first();
        $report = Report::findOrFail($rep_id);
        $countries = Country::all();

        if ($report_progress->repr_status != 3)
        {
            return view('reports.customer_portal_analysis', [
                'countries' => $countries,
                'bothyesno' => BothYesNo::all(),
                'report' => $report,
                'repr_id' => $repr_id,
                'clone' => $clone,
                'report_progress' => $report_progress,
                'selected_portals' => System::unserialize($report_progress->repr_posted_values)['portal'],
                'selected_request_types' => System::unserialize($report_progress->repr_posted_values)['request_type'],
                'times_used' => self::getTimesUsedLast90Days($rep_id)
            ]);
        }

        $filename = config("app.shared_folder") . "uploads/reports/report-result-" . $repr_id . ".html";
        $file = File::get($filename);

        return view('reports.customer_portal_analysis', [
            'countries' => $countries,
            'bothyesno' => BothYesNo::all(),
            'report' => $report,
            'file' => $file,
            'repr_id' => $repr_id,
            'report_progress' => $report_progress,
            'clone' => $clone,
            'selected_portals' => System::unserialize($report_progress->repr_posted_values)['portal'],
            'selected_request_types' => System::unserialize($report_progress->repr_posted_values)['request_type'],
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function viewCustomerCappings($rep_id)
    {
        $report = Report::findOrFail($rep_id);
        $portals = Portal::all();

        return view('reports.customer_cappings', [
            'portals' => $portals,
            'bothyesno' => BothYesNo::all(),
            'report' => $report,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function cloneCustomerCappings($rep_id, $repr_id)
    {
        echo self::viewFinishedCustomerCappings($rep_id, $repr_id, true);
    }

    public function queueCustomerCappings(Request $request, $rep_id)
    {
        $request->validate([
            'report_comment' => 'required'
        ]);

        $array = [
            'report_comment' => $request->report_comment,
            'portal' => $request->portal,
            'form_submit' => $request->form_submit

        ];

        self::insertReportUsageHistory($rep_id);

        $repr_id = Reporting::addToQueue($rep_id, "customer_cappings", $array);

        return redirect('reports/' . $rep_id . '/view/customer_cappings/' . $repr_id . '/view');
    }

    public function viewFinishedCustomerCappings($rep_id, $repr_id, $clone = false)
    {
        $report_progress = ReportProgress::where("repr_id", $repr_id)->first();
        $report = Report::findOrFail($rep_id);

        if ($report_progress->repr_status != 3)
        {
            return view('reports.customer_cappings', [
                'report' => $report,
                'repr_id' => $repr_id,
                'clone' => $clone,
                'report_progress' => $report_progress,
                'times_used' => self::getTimesUsedLast90Days($rep_id)
            ]);
        }

        $filename = config("app.shared_folder") . "uploads/reports/report-result-" . $repr_id . ".html";
        $file = File::get($filename);

        return view('reports.customer_cappings', [
            'report' => $report,
            'file' => $file,
            'repr_id' => $repr_id,
            'report_progress' => $report_progress,
            'clone' => $clone,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function viewResultsPerCategory($rep_id)
    {
        $report = Report::findOrFail($rep_id);
        $moving_sizes = MovingSize::all();

        return view('reports.results_per_category', [
            'moving_sizes' => $moving_sizes,
            'bothyesno' => BothYesNo::all(),
            'report' => $report,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function cloneResultsPerCategory($rep_id, $repr_id)
    {
        echo self::viewFinishedResultsPerCategory($rep_id, $repr_id, true);
    }

    public function queueResultsPerCategory(Request $request, $rep_id)
    {
        $request->validate([
            'report_comment' => 'required'
        ]);

        $date = explode('-', $request->date);

        $array = [
            'date_from' => $date[0],
            'date_to' => $date[1],
            'report_comment' => $request->report_comment,
            'moving_size' => $request->moving_size,
            'destination_type' => $request->destination_type,
            'source' => $request->source,
            'form_submit' => $request->form_submit

        ];

        self::insertReportUsageHistory($rep_id);

        $repr_id = Reporting::addToQueue($rep_id, "results_per_category", $array);

        return redirect('reports/' . $rep_id . '/view/results_per_category/' . $repr_id . '/view');
    }

    public function viewFinishedResultsPerCategory($rep_id, $repr_id, $clone = false)
    {
        $report_progress = ReportProgress::where("repr_id", $repr_id)->first();
        $report = Report::findOrFail($rep_id);

        if ($report_progress->repr_status != 3)
        {
            return view('reports.results_per_category', [
                'report' => $report,
                'repr_id' => $repr_id,
                'report_progress' => $report_progress,
                'clone' => $clone,
                'times_used' => self::getTimesUsedLast90Days($rep_id)
            ]);
        }

        $filename = config("app.shared_folder") . "uploads/reports/report-result-" . $repr_id . ".html";
        $file = File::get($filename);

        return view('reports.results_per_category', [
            'report' => $report,
            'file' => $file,
            'repr_id' => $repr_id,
            'report_progress' => $report_progress,
            'clone' => $clone,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function viewResultsPerSite($rep_id)
    {
        $report = Report::findOrFail($rep_id);
        $moving_sizes = MovingSize::all();

        return view('reports.results_per_site', [
            'moving_sizes' => $moving_sizes,
            'bothyesno' => BothYesNo::all(),
            'report' => $report,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function cloneResultsPerSite($rep_id, $repr_id)
    {
        echo self::viewFinishedResultsPerSite($rep_id, $repr_id, true);
    }

    public function queueResultsPerSite(Request $request, $rep_id)
    {
        $request->validate([
            'report_comment' => 'required'
        ]);

        $date = explode('-', $request->date);

        $array = [
            'date_from' => $date[0],
            'date_to' => $date[1],
            'report_comment' => $request->report_comment,
            'moving_size' => $request->moving_size,
            'form_submit' => $request->form_submit

        ];

        self::insertReportUsageHistory($rep_id);

        $repr_id = Reporting::addToQueue($rep_id, "results_per_site", $array);

        return redirect('reports/' . $rep_id . '/view/results_per_site/' . $repr_id . '/view');
    }

    public function viewFinishedResultsPerSite($rep_id, $repr_id, $clone = false)
    {
        $report_progress = ReportProgress::where("repr_id", $repr_id)->first();
        $report = Report::findOrFail($rep_id);

        if ($report_progress->repr_status != 3)
        {
            return view('reports.results_per_site', [
                'report' => $report,
                'repr_id' => $repr_id,
                'report_progress' => $report_progress,
                'clone' => $clone,
                'times_used' => self::getTimesUsedLast90Days($rep_id)
            ]);
        }

        $filename = config("app.shared_folder") . "uploads/reports/report-result-" . $repr_id . ".html";
        $file = File::get($filename);

        return view('reports.results_per_site', [
            'report' => $report,
            'file' => $file,
            'repr_id' => $repr_id,
            'report_progress' => $report_progress,
            'clone' => $clone,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function viewTurnoverPerRequestPerCountry($rep_id)
    {
        $report = Report::findOrFail($rep_id);
        $moving_sizes = MovingSize::all();

        return view('reports.turnover_per_request_per_country', [
            'moving_sizes' => $moving_sizes,
            'bothyesno' => BothYesNo::all(),
            'report' => $report,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function cloneTurnoverPerRequestPerCountry($rep_id, $repr_id)
    {
        echo self::viewFinishedTurnoverPerRequestPerCountry($rep_id, $repr_id, true);
    }

    public function queueTurnoverPerRequestPerCountry(Request $request, $rep_id)
    {
        $request->validate([
            'report_comment' => 'required'
        ]);

        $date = explode('-', $request->date);

        $array = [
            'date_from' => $date[0],
            'date_to' => $date[1],
            'report_comment' => $request->report_comment,
            'request_type' => $request->request_type,
            'request_source' => $request->request_source,
            'min_requests' => $request->min_requests,
            'destination_type' => $request->destination_type,
            'affiliate_partner' => $request->affiliate_partner,
            'exclude_rejection_reason_1' => $request->exclude_rejection_reason_1,
            'exclude_rejection_reason_2' => $request->exclude_rejection_reason_2,
            'exclude_rejection_reason_3' => $request->exclude_rejection_reason_3,
            'form_submit' => $request->form_submit

        ];

        self::insertReportUsageHistory($rep_id);

        $repr_id = Reporting::addToQueue($rep_id, "turnover_per_request_per_country", $array);

        return redirect('reports/' . $rep_id . '/view/turnover_per_request_per_country/' . $repr_id . '/view');
    }

    public function viewFinishedTurnoverPerRequestPerCountry($rep_id, $repr_id, $clone = false)
    {
        $report_progress = ReportProgress::where("repr_id", $repr_id)->first();
        $report = Report::findOrFail($rep_id);

        if ($report_progress->repr_status != 3)
        {
            return view('reports.turnover_per_request_per_country', [
                'report' => $report,
                'repr_id' => $repr_id,
                'clone' => $clone,
                'report_progress' => $report_progress
            ]);
        }

        $filename = config("app.shared_folder") . "uploads/reports/report-result-" . $repr_id . ".html";
        $file = File::get($filename);

        return view('reports.turnover_per_request_per_country', [
            'report' => $report,
            'file' => $file,
            'repr_id' => $repr_id,
            'report_progress' => $report_progress,
            'clone' => $clone,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function viewMissedMatches($rep_id)
    {
        $report = Report::findOrFail($rep_id);
        $moving_sizes = MovingSize::all();

        $websiteforms = WebsiteForm::with("website")->get()->sortBy("we_website");

        $websites = [];
        foreach($websiteforms as $website_form){
            $websites[$website_form->wefo_id] = $website_form->website->we_website." (".$website_form->wefo_name.")";
        }

        asort($websites);

        $times_used = self::getTimesUsedLast90Days($rep_id);

        return view('reports.missed_matches', [
            'moving_sizes' => $moving_sizes,
            'websites' => $websites,
            'bothyesno' => BothYesNo::all(),
            'report' => $report,
            'times_used' => $times_used
        ]);
    }

    public function viewAdyenBatch($rep_id)
    {
        $report = Report::findOrFail($rep_id);

        return view('reports.adyen_credit_card_batch', [
            'report' => $report,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function cloneMissedMatches($rep_id, $repr_id)
    {
        echo self::viewFinishedMissedMatches($rep_id, $repr_id, true);
    }

    public function queueMissedMatches(Request $request, $rep_id)
    {
        $request->validate([
            'report_comment' => 'required'
        ]);

        self::insertReportUsageHistory($rep_id);

        $date = explode('-', $request->date);

        if ($request->request_type == 2)
        {
            $request_type_array = [5];
        } elseif ($request->request_type == 1)
        {
            $request_type_array = [1, 2, 3, 4];
        } else
        {
            $request_type_array = [1, 2, 3, 4, 5];
        }

        $array = [
            'date_from' => $date[0],
            'date_to' => $date[1],
            'report_comment' => $request->report_comment,
            'request_type' => $request_type_array,
            'moving_size' => $request->moving_size,
            'origin_type' => $request->origin_type,
            'origin_values' => $request->origin_values,
            'destination_type' => $request->destination_type,
            'destination_values' => $request->destination_values,
            'report_type' => $request->report_type,
            'source' => $request->source,
            'website' => $request->website,
            'form_submit' => $request->form_submit

        ];

        self::insertReportUsageHistory($rep_id);

        $repr_id = Reporting::addToQueue($rep_id, "missed_matches", $array);

        return redirect('reports/' . $rep_id . '/view/missed_matches/' . $repr_id . '/view');
    }

    public function viewFinishedMissedMatches($rep_id, $repr_id, $clone = false)
    {
        $report_progress = ReportProgress::where("repr_id", $repr_id)->first();
        $report = Report::findOrFail($rep_id);
        $filter_data = null;
        $filter_origins = null;

        if (System::unserialize($report_progress->repr_posted_values)['origin_type'] == 1)
        {
            $type = System::unserialize($report_progress->repr_posted_values)['report_type'];
            $data_origins = Data::regions(null, $type, true);
        } elseif (System::unserialize($report_progress->repr_posted_values)['origin_type'] == 2)
        {
            $type = System::unserialize($report_progress->repr_posted_values)['report_type'];
            $data_origins = Data::greaterRegions(null, $type, true);
        } elseif (System::unserialize($report_progress->repr_posted_values)['origin_type'] == 3)
        {
            $data_origins = Data::getCountries(LANGUAGE, true);
            $array_europe = Data::getEUcountries(LANGUAGE);
            $array_schengen_area = Data::getSchengenAreaCountries(LANGUAGE);
            $data_origins['All Schengen Area Members'] = $array_schengen_area;
            $data_origins['All Europe Members'] = $array_europe;
        } elseif (System::unserialize($report_progress->repr_posted_values)['origin_type'] == 4)
        {
            $data_origins = Data::continents(true);
        }

        if (System::unserialize($report_progress->repr_posted_values)['destination_type'] == 1)
        {
            $type = System::unserialize($report_progress->repr_posted_values)['report_type'];
            $data_destinations = Data::regions(null, $type, true);
        } elseif (System::unserialize($report_progress->repr_posted_values)['destination_type'] == 2)
        {
            $type = System::unserialize($report_progress->repr_posted_values)['report_type'];
            $data_destinations = Data::greaterRegions(null, $type, true);
        } elseif (System::unserialize($report_progress->repr_posted_values)['destination_type'] == 3)
        {
            $data_destinations = Data::getCountries(LANGUAGE, true);
            $array_europe = Data::getEUcountries(LANGUAGE);
            $array_schengen_area = Data::getSchengenAreaCountries(LANGUAGE);
            $data_destinations['All Schengen Area Members'] = $array_schengen_area;
            $data_destinations['All Europe Members'] = $array_europe;
        } elseif (System::unserialize($report_progress->repr_posted_values)['destination_type'] == 4)
        {
            $data_destinations = Data::continents(true);
        }

        if (System::unserialize($report_progress->repr_posted_values)['report_type'] == 1)
        {

            $filter_data = [
                'destination_types' => [
                    "" => "",
                    3 => "Countries",
                    4 => "Continents",
                    6 => "Macro-regions",
                    5 => "Worldwide",
                ],
                'filter_origins' => [
                    $data_origins
                ],
                'filter_destinations' => [
                    $data_destinations
                ]
            ];
        } else
        {
            $filter_data = [
                'destination_types' => [
                    "" => "",
                    1 => "Regions",
                    3 => "Nationwide",
                ],
                'filter_origins' => [
                    $data_origins
                ],
                'filter_destinations' => [
                    $data_destinations
                ]
            ];
        }

        $selected_websites = System::unserialize($report_progress->repr_posted_values)['website'];

        $websiteforms = WebsiteForm::with("website")->get()->sortBy("we_website");

        $websites = [];
        foreach($websiteforms as $website_form){
            $websites[$website_form->wefo_id] = $website_form->website->we_website." (".$website_form->wefo_name.")";
        }

        asort($websites);

        if ($report_progress->repr_status != 3)
        {
            return view('reports.missed_matches', [
                'report' => $report,
                'repr_id' => $repr_id,
                'clone' => $clone,
                'websites' => $websites,
                'selected_websites' => $selected_websites,
                'report_progress' => $report_progress,
                'filter_data' => $filter_data,
                'times_used' => self::getTimesUsedLast90Days($rep_id)
            ]);
        }

        $filename = config("app.shared_folder") . "uploads/reports/report-result-" . $repr_id . ".html";
        $file = File::get($filename);

        return view('reports.missed_matches', [
            'report' => $report,
            'file' => $file,
            'repr_id' => $repr_id,
            'clone' => $clone,
            'report_progress' => $report_progress,
            'filter_data' => $filter_data,
            'websites' => $websites,
            'selected_websites' => $selected_websites,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function viewFinishedAdyenBatch($rep_id, $repr_id)
    {
        $report_progress = ReportProgress::where("repr_id", $repr_id)->first();
        $report = Report::findOrFail($rep_id);

        if ($report_progress->repr_status != 3)
        {
            return view('reports.adyen_credit_card_batch', [
                'report' => $report,
                'repr_id' => $repr_id,
                'report_progress' => $report_progress,
                'times_used' => self::getTimesUsedLast90Days($rep_id)
            ]);
        }

        $filename = config("app.shared_folder") . "uploads/reports/report-result-" . $repr_id . ".html";
        $file = File::get($filename);

        return view('reports.adyen_credit_card_batch', [
            'report' => $report,
            'file' => $file,
            'repr_id' => $repr_id,
            'report_progress' => $report_progress,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function viewRegionOccupancy($rep_id)
    {
        $report = Report::findOrFail($rep_id);
        $moving_sizes = MovingSize::all();

        return view('reports.region_occupancy', [
            'moving_sizes' => $moving_sizes,
            'bothyesno' => BothYesNo::all(),
            'report' => $report,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function cloneRegionOccupancy($rep_id, $repr_id)
    {
        echo self::viewFinishedRegionOccupancy($rep_id, $repr_id, true);
    }

    public function queueRegionOccupancy(Request $request, $rep_id)
    {
        $request->validate([
            'report_comment' => 'required'
        ]);

        $date = explode('-', $request->date);

        $array = [
            'date_from' => $date[0],
            'date_to' => $date[1],
            'portal' => $request->portal,
            'origin_type' => $request->origin_type,
            'region' => $request->region,
            'destination_type' => $request->destination_type,
            'targets' => $request->targets,
            'request_type' => $request->request_type,
            'status' => $request->status,
            'moving_size' => $request->moving_size,
            'volume_restriction' => $request->volume_restriction,
            'business_only' => $request->business_only,
            'hide_import_data' => $request->hide_import_data,
            'hide_cappings' => $request->hide_cappings,
            'minimum_requests' => $request->min_requests,
            'report_comment' => $request->report_comment,
            'form_submit' => $request->form_submit,

        ];

        self::insertReportUsageHistory($rep_id);

        $repr_id = Reporting::addToQueue($rep_id, "region_occupancy", $array);

        return redirect('reports/' . $rep_id . '/view/region_occupancy/' . $repr_id . '/view');
    }

    public function viewFinishedRegionOccupancy($rep_id, $repr_id, $clone = false)
    {
        $report_progress = ReportProgress::where("repr_id", $repr_id)->first();
        $report = Report::findOrFail($rep_id);

        if ($report_progress->repr_status != 3)
        {
            return view('reports.region_occupancy', [
                'report' => $report,
                'repr_id' => $repr_id,
                'report_progress' => $report_progress,
                'clone' => $clone,
                'times_used' => self::getTimesUsedLast90Days($rep_id)
            ]);
        }

        $filename = config("app.shared_folder") . "uploads/reports/report-result-" . $repr_id . ".html";
        $file = File::get($filename);

        return view('reports.region_occupancy', [
            'report' => $report,
            'file' => $file,
            'repr_id' => $repr_id,
            'report_progress' => $report_progress,
            'clone' => $clone,
            'times_used' => self::getTimesUsedLast90Days($rep_id)
        ]);
    }

    public function reportProgress($repr_id, $action)
    {
        //Get the data
        $id = intval($repr_id);
        $data = Reporting::getProgressInfo($id, false, true);

        //Get the report
        $report = Report::select("rep_id", "rep_report", "rep_name")
            ->where("rep_id", $data['report'])
            ->first();

        $status = intval($data['status']);

        switch (isset($action) ? $action : "")
        {
            case "cancel":
                if ($status !== 0 && $status !== 1)
                {
                    return Redirect::back()->withErrors(['This report can no longer be canceled.']);
                }

                Reporting::cancelReport($id, $data['mysql_id'], $status);

                return redirect('/reports/' . $report->rep_id . "/view");

                $text = "cancel";
                break;

            case "delete":
                if ($status !== 3 && $status !== 5)
                {
                    return Redirect::back()->withErrors(['There is no data that can be deleted.']);
                }

                //Update the row
                $report_progress = ReportProgress::where("repr_id", $id)->where("repr_rep_id", $report->rep_id)->first();
                $report_progress->repr_status = 4;
                $report_progress->save();

                //Delete the file
                $file_path = config("app.shared_folder") . "uploads/reports/report-result-" . $id . ".html";
                if (file_exists($file_path))
                {
                    unlink($file_path);
                }

                return redirect('/reports/' . $report->rep_id . "/view");

                $text = "delete";
                break;

            default:
                System::redirect("/reports/" . $report->rep_id . "/view");
        }
    }/**
 * @param Request $request
 * @return array
 */
    public function CalculateIntAF(Request $request): array
    {
        $int_af = \App\Models\Request::select("re_co_code_from", "co_en")
            ->selectRaw("count(*) as `count`")
            ->join("countries", "co_code", "re_co_code_from")
            ->where("re_request_type", 1)
            ->whereRaw("(`re_status` = 1 OR (`re_status` = 2 AND `re_rejection_reason` IN (2,3)))")
            ->whereBetween("re_timestamp", System::betweenDates($request->date))
            ->whereRaw("`re_co_code_from` != `re_co_code_to`")
            ->where("re_source", 2)
            ->groupBy("re_co_code_from")
            ->get()
            ->keyBy("re_co_code_from");

        $int_af_sirelo = \App\Models\Request::select("re_co_code_from", "co_en")
            ->selectRaw("count(*) as `count`")
            ->join("website_forms", "re_wefo_id", "wefo_id")
            ->join("websites", "wefo_we_id", "we_id")
            ->join("countries", "co_code", "re_co_code_from")
            ->where("re_request_type", 1)
            ->whereRaw("(`re_status` = 1 OR (`re_status` = 2 AND `re_rejection_reason` IN (2,3)))")
            ->whereBetween("re_timestamp", System::betweenDates($request->date))
            ->whereRaw("`re_co_code_from` != `re_co_code_to`")
            ->where("re_source", 1)
            ->where("we_sirelo", 1)
            ->groupBy("re_co_code_from")
            ->get()
            ->keyBy("re_co_code_from");

        $system = new System();
        $int_af = $system->databaseToArray($int_af);
        $int_af_sirelo = $system->databaseToArray($int_af_sirelo);

        $total_inf = $int_af;

        foreach ($int_af_sirelo as $country => $sirelo)
        {
            if (array_key_exists($sirelo["re_co_code_from"], $total_inf))
            {
                $total_inf[$sirelo["re_co_code_from"]]['count'] = $total_inf[$sirelo["re_co_code_from"]]['count'] + $sirelo['count'];
            } else
            {
                $total_inf[$sirelo["re_co_code_from"]] = $sirelo;
            }
        }

        $count_af = 0;
        foreach ($int_af as $values)
        {
            $count_af += $values['count'];
        }

        $count_sirelo = 0;
        foreach ($int_af_sirelo as $values)
        {
            $count_sirelo += $values['count'];
        }

        return array($int_af, $int_af_sirelo, $total_inf, $count_af, $count_sirelo);
    }

    public function CalculateIntTGB(Request $request): array
    {
        $system = new System();
        //International TriGlobal
        $int_tgb = \App\Models\Request::select("re_co_code_from", "co_en")
            ->selectRaw("count(*) as `count`")
            ->join("countries", "co_code", "re_co_code_from")
            ->join("website_forms", "re_wefo_id", "wefo_id")
            ->join("websites", "wefo_we_id", "we_id")
            ->where("re_request_type", 1)
            ->whereRaw("(`re_status` = 1 OR (`re_status` = 2 AND `re_rejection_reason` IN (2,3)))")
            ->whereBetween("re_timestamp", System::betweenDates($request->date))
            ->whereRaw("`re_co_code_from` != `re_co_code_to`")
            ->where("re_source", 1)
            ->where("we_sirelo", 0)
            ->groupBy("re_co_code_from")
            ->get()
            ->keyBy("re_co_code_from");

        $count = 0;
        foreach ($int_tgb as $values)
        {
            $count += $values['count'];
        }

        return array($system->databaseToArray($int_tgb), $count);
    }

    public function CalculateNatTGB(Request $request): array
    {
        $system = new System();
        //International TriGlobal
        $nat_tgb = \App\Models\Request::select("re_co_code_from", "co_en")
            ->selectRaw("count(*) as `count`")
            ->join("countries", "co_code", "re_co_code_from")
            ->join("website_forms", "re_wefo_id", "wefo_id")
            ->join("websites", "wefo_we_id", "we_id")
            ->where("re_request_type", 1)
            ->whereRaw("(`re_status` = 1 OR (`re_status` = 2 AND `re_rejection_reason` IN (2,3)))")
            ->whereBetween("re_timestamp", System::betweenDates($request->date))
            ->whereRaw("`re_co_code_from` = `re_co_code_to`")
            ->where("re_source", 1)
            ->where("we_sirelo", 0)
            ->groupBy("re_co_code_from")
            ->get()
            ->keyBy("re_co_code_from");

        $count = 0;
        foreach ($nat_tgb as $values)
        {
            $count += $values['count'];
        }

        return array($system->databaseToArray($nat_tgb), $count);
    }

    /**
     * @param $int_af
     * @param $int_tgb
     * @return mixed
     */
    public function CalculateIntTOTAL($int_af, $int_tgb)
    {
        $total_int = $int_af;

        foreach ($int_tgb as $country => $sirelo)
        {
            if (array_key_exists($sirelo["re_co_code_from"], $int_af))
            {
                $total_int[$sirelo["re_co_code_from"]]['count'] = $int_af[$sirelo["re_co_code_from"]]['count'] + $sirelo['count'];
            } else
            {
                $total_int[$sirelo["re_co_code_from"]] = $sirelo;
            }
        }

        $total_count = 0;
        foreach ($total_int as $values)
        {
            $total_count += $values['count'];
        }

        return $total_int;
    }/**
 * @param Request $request
 * @return array
 */

    public function CalculateNatTOTAL($nat_af, $nat_tgb)
    {
        $total_int = $nat_af;

        foreach ($nat_tgb as $country => $sirelo)
        {
            if (array_key_exists($sirelo["re_co_code_from"], $nat_af))
            {
                $total_int[$sirelo["re_co_code_from"]]['count'] = $nat_af[$sirelo["re_co_code_from"]]['count'] + $sirelo['count'];
            } else
            {
                $total_int[$sirelo["re_co_code_from"]] = $sirelo;
            }
        }

        $total_count = 0;
        foreach ($total_int as $values)
        {
            $total_count += $values['count'];
        }

        return $total_int;
    }

    public function CalculateNatAF(Request $request): array
    {
        $nat_af = \App\Models\Request::select("re_co_code_from", "co_en")
            ->selectRaw("count(*) as `count`")
            ->join("countries", "co_code", "re_co_code_from")
            ->where("re_request_type", 1)
            ->whereRaw("(`re_status` = 1 OR (`re_status` = 2 AND `re_rejection_reason` IN (2,3)))")
            ->whereBetween("re_timestamp", System::betweenDates($request->date))
            ->whereRaw("`re_co_code_from` = `re_co_code_to`")
            ->where("re_source", 2)
            ->groupBy("re_co_code_from")
            ->get()
            ->keyBy("re_co_code_from");

        $nat_af_sirelo = \App\Models\Request::select("re_co_code_from", "co_en")
            ->selectRaw("count(*) as `count`")
            ->join("website_forms", "re_wefo_id", "wefo_id")
            ->join("websites", "wefo_we_id", "we_id")
            ->join("countries", "co_code", "re_co_code_from")
            ->where("re_request_type", 1)
            ->whereRaw("(`re_status` = 1 OR (`re_status` = 2 AND `re_rejection_reason` IN (2,3)))")
            ->whereBetween("re_timestamp", System::betweenDates($request->date))
            ->whereRaw("`re_co_code_from` = `re_co_code_to`")
            ->where("re_source", 1)
            ->where("we_sirelo", 1)
            ->groupBy("re_co_code_from")
            ->get()
            ->keyBy("re_co_code_from");


        $system = new System();
        $nat_af = $system->databaseToArray($nat_af);
        $nat_af_sirelo = $system->databaseToArray($nat_af_sirelo);

        $total_nat = $nat_af;

        foreach ($nat_af_sirelo as $country => $sirelo)
        {
            if (array_key_exists($sirelo["re_co_code_from"], $total_nat))
            {
                $total_nat[$sirelo["re_co_code_from"]]['count'] = $total_nat[$sirelo["re_co_code_from"]]['count'] + $sirelo['count'];
            } else
            {
                $total_nat[$sirelo["re_co_code_from"]] = $sirelo;
            }
        }

        $count_af = 0;
        foreach ($nat_af as $values)
        {
            $count_af += $values['count'];
        }

        $count_sirelo = 0;
        foreach ($nat_af_sirelo as $values)
        {
            $count_sirelo += $values['count'];
        }

        return array($nat_af, $nat_af_sirelo, $total_nat, $count_af, $count_sirelo);

    }

    public function insertReportUsageHistory($rep_id)
    {
        $history = new ReportUsageHistory();
        $history->reushi_rep_id = $rep_id;
        $history->reushi_us_id = Auth::user()->id;
        $history->reushi_timestamp = date("Y-m-d H:i:s");
        $history->save();

        return true;
    }

    public function getTimesUsedLast90Days($rep_id) {
        $times_used = ReportUsageHistory::selectRaw("COUNT(*) as `times_used`")
            ->where("reushi_rep_id", $rep_id)
            ->whereRaw("`reushi_timestamp` BETWEEN NOW() - INTERVAL 90 DAY AND NOW()")
            ->first();

        return $times_used->times_used;
    }

    public function dumpReviewScores() {

        $result = DB::table( 'customer_review_scores' )
            ->selectRaw( 'customer_review_scores.*, customers.cu_company_name_business' )
            ->leftJoin( 'customers', 'curesc_cu_id', '=', 'cu_id' )
            ->get();

//        $result = DB::table('customer_review_scores' )->get();
        echo '<pre>';
        print_r( $result );
        echo '</pre>';
    }

    public function dumpPythonProcess() {

        $rows = DB::table( 'customer_review_scores' )
            ->selectRaw('COUNT(*) as `amount`')
            ->leftJoin( 'customers', 'curesc_cu_id', '=', 'cu_id' )
            ->first();

        $rows_processed = DB::table( 'customer_review_scores' )
            ->selectRaw('COUNT(*) as `amount`')
            ->where("curesc_processed", 1)
            ->first();

        $rows_skipped = DB::table( 'customer_review_scores' )
            ->selectRaw('COUNT(*) as `amount`')
            ->where("curesc_skip", 1)
            ->first();


        echo "Total = ".$rows->amount."<br />";
        echo "Skipped = ".$rows_skipped->amount."<br />";
        echo "Processed = ".$rows_processed->amount."<br />";
    }
}
