<?php

namespace App\Http\Controllers;

use App\Data\BankTransactionMutationType;
use App\Data\LedgerAccountType;
use App\Functions\System;
use App\Models\LedgerAccount;
use DB;
use Illuminate\Http\Request;
use Log;

class BankTransactionsPerLedgerAccountSummaryController extends Controller
{
	public function index()
	{
		return view('finance.banktransactionsperledgeraccountsummary', [
			'selected_period' => "",
			'selected_statement' => "",
			'selected_ledger' => "",
            'ledgeraccounts' => LedgerAccountType::all()
		]);
	}
	
	public function filteredIndex(Request $request)
	{
        
        $invoices = DB::table("kt_bank_line_invoice_customer_ledger_account")
            ->leftJoin("bank_lines","ktbaliinculeac_bali_id", "bali_id")
            ->leftJoin("ledger_accounts","ktbaliinculeac_leac_number", "leac_number")
            ->whereNotNull("ktbaliinculeac_bali_id")
            ->whereBetween("bali_date", System::betweenDates($request->date));
        
        
        if(!empty($request->statement_number)){
            $invoices->where("bali_statement_number", $request->statement_number);
        }
        
        if(!empty($request->ledger)){
            $invoices
                ->where("leac_type",$request->ledger);
        }
        
        $invoices = $invoices
            ->get();
        
        $totals['credit'] = 0;
        $totals['debit'] = 0;
        $totals['total'] = 0;
        
        $ledger_amounts = [];
        
        foreach($invoices as $invoice){
           
            if(!array_key_exists($invoice->ktbaliinculeac_leac_number, $ledger_amounts))
            {
                $ledger_amounts[$invoice->ktbaliinculeac_leac_number] = [
                    "leac_number" => $invoice->leac_number,
                    "leac_name" => $invoice->leac_name,
                    "credit" => 0,
                    "debit" => 0,
                    "total" => 0
                ];
            }
    
            if($invoice->bali_credit_debit == "credit")
            {
                $ledger_amounts[$invoice->ktbaliinculeac_leac_number]['credit'] += $invoice->ktbaliinculeac_amount;
                $totals['credit'] += $invoice->ktbaliinculeac_amount;
            }
            elseif($invoice->bali_credit_debit == "debit")
            {
                $ledger_amounts[$invoice->ktbaliinculeac_leac_number]['debit'] += $invoice->ktbaliinculeac_amount;
                $totals['debit'] += $invoice->ktbaliinculeac_amount;
            }
    
            $ledger_amounts[$invoice->ktbaliinculeac_leac_number]['total'] += $invoice->ktbaliinculeac_amount;
            $totals['total'] += $invoice->ktbaliinculeac_amount;
        }
        
        Log::debug($ledger_amounts);
        
        return view('finance.banktransactionsperledgeraccountsummary', [
            'selected_period' => $request->date,
            'selected_statement' => $request->statement_number,
            'selected_ledger' => $request->ledger,
            'invoices' => $invoices,
            'ledgeraccounts' => LedgerAccountType::all(),
            'totals' => $totals,
            'ledger_amounts' => $ledger_amounts
        ]);
		
	}
    
    public function getBankLines(Request $request)
    {
        $system = new System();
        
        $exp = explode('&date=', $request->id);
        $id = $exp[0];
        $date = $exp[1];
    
        $invoices = DB::table("kt_bank_line_invoice_customer_ledger_account")
            ->select("ktbaliinculeac_cu_id","ktbaliinculeac_id","in_id","in_number","cu_id","cu_company_name_legal", "ktbaliinculeac_leac_number","bali_date", "bali_reference", "bali_credit_debit", "bali_debtor_details", "ktbaliinculeac_amount", "bali_id", "bali_statement_number", "bali_information")
           ->leftJoin("bank_lines","ktbaliinculeac_bali_id", "bali_id")
           ->leftJoin("invoices","ktbaliinculeac_in_id", "in_id")
           ->leftJoin("customers","ktbaliinculeac_cu_id", "cu_id")
           ->whereNotNull("ktbaliinculeac_bali_id")
           ->where("ktbaliinculeac_leac_number", $id)
           ->whereBetween("bali_date", System::betweenDates($date))
           ->get();
        
        $html = '';
        $html .=  '<table class="table display">';
        $html .=  '<thead>';
        $html .=  '<tr>';
        $html .= '<th>ID</th>';
        $html .= '<th>Statement number</th>';
        $html .= '<th>Mutation type</th>';
        $html .= '<th>Invoice number</th>';
        $html .= '<th>Customer</th>';
        $html .= '<th>Ledger account</th>';
        $html .= '<th>Bankline date</th>';
        $html .= '<th>Reference</th>';
        $html .= '<th>Credit / Debit</th>';
        $html .= '<th>Debtor details</th>';
        $html .= '<th>Amount</th>';
        $html .= '<th>Information</th>';
        $html .=  '</tr>';
        $html .=  '</thead>';
        $html .=  '<tbody>';
    
        $total_amount = 0;
        
        foreach($invoices as $row)
        {
    
            if($row->ktbaliinculeac_leac_number){
                $ledger = LedgerAccount::where("leac_number", $row->ktbaliinculeac_leac_number)->first();
            }
    
            if($row->ktbaliinculeac_cu_id != 0)
            {
                $mutation_type = 1;
            }
            elseif($row->ktbaliinculeac_cu_id == 0)
            {
                $mutation_type = 2;
            }
            
            $html .=  '<tr>';
            $html .=  '<td>'.$row->ktbaliinculeac_id.'</td>';
            $html .=  '<td>'.$row->bali_statement_number.'</td>';
            $html .=  '<td>'.BankTransactionMutationType::name($mutation_type).'</td>';
            $html .= "<td>".((!empty($row->in_id)) ? $row->in_number : "-")."</td>";
            $html .= "<td>".((!empty($row->cu_id)) ? $row->cu_company_name_legal : "-")."</td>";
            $html .=  '<td>'.($ledger ? $ledger->leac_number." (".$ledger->leac_name.")" : "-").'</td>';
            $html .=  '<td>'.$row->bali_date.'</td>';
            $html .=  '<td>'.$row->bali_reference.'</td>';
            $html .= "<td>".ucfirst($row->bali_credit_debit)."</td>";
            $html .= "<td>".nl2br($row->bali_debtor_details)."</td>";
            $html .= "<td>€ ".number_format($row->ktbaliinculeac_amount, 2, ',', '.')."</td>";
            $html .= "<td>".nl2br($row->bali_information)."</td>";
            $html .=  '</tr>';
    
            $total_amount += $row->ktbaliinculeac_amount;
        }
        $html .= '</tbody>';
        $html .= '<tfoot>';
        $html .= '<tr>';
        $html .= '<th colspan="10"></th>';
        $html .= '<th>€'.number_format($total_amount, 2, ',', '.').'</th>';
        $html .= '<th></th>';
        $html .= '</tr>';
        $html .= '</tfoot>';
        $html .= '</table>';
        
        return json_encode($html);
    }
	
}

