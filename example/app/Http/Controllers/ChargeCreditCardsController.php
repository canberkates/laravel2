<?php

namespace App\Http\Controllers;

use App\Data\AdyenPaymentStatus;
use App\Data\PaymentMethod;
use App\Functions\DataIndex;
use App\Functions\Reporting;
use App\Functions\System;
use App\Models\PaymentCurrency;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Log;

class ChargeCreditCardsController extends Controller
{
    
    public function index()
    {
        return view('finance.chargecreditcards', [
            "selected_date" => null,
            "selected_outstanding" => 1,
            "selected_virtually" => 1,
            "selected_amex" => 0,
            "selected_bank" => 1
        ]);
    }
    
    public function chargeConfirmation(Request $request)
    {
        $request->validate([
            'date' => 'required'
        ]);
        
        $system = new System();
        
        define("PAIRED", "paired");
        define("AMOUNT_LEFT", "amount_left");
        define("PAYMENTS", "payments");
        define("VIRTUALLY_LEFT", "vl");
    
        $payment_currencies = PaymentCurrency::all();
        $invoices = self::getInvoices($request);
        self::associatePayments($invoices, $request);
        self::fillCustomerData($invoices);
    
        $system->databaseToArray($invoices);
        
        foreach($invoices as $id => $invoice){
            if(!isset($invoice['card']))
            {
                unset($invoices[$id]);
            }
            
            $FC = $payment_currencies->where("pacu_code", $invoice['in_currency'])->first()->pacu_token;
            $EUR = $payment_currencies->where("pacu_code", "EUR")->first()->pacu_token;
    
            $invoices_2 = [$invoice['in_id'] => $invoice] + (isset($invoice['children']) ? $invoice['children'] : []);
            $payments = [];
    
            //Calculate what's left
            $modified = false;
    
            $left = 0;
    
            foreach($invoices_2 as $invoice_id => $invoice2)
            {
                if($left !== 0)
                {
                    $modified = true;
                }
        
                if($invoice2[PAIRED] === 0)
                {
                    $left += $invoice2['in_amount_netto'];
                }
                else
                {
                    $left += round($invoice2[AMOUNT_LEFT] * $payment_currencies->where("pacu_code", $invoice['in_currency'])->first()->pacu_rate, 2);
                    $modified = true;
                }
        
                //Merge payments
                $payments = $payments + $invoice2[PAYMENTS];
            }
            
    
            if($left == 0)
            {
                unset($invoices[$id]);
            }
    
            //Get the latest payment status
            $latest_status = "Open";
            $mapped_times = [];
            foreach($payments as $adpa_id => $adpa_row)
            {
                $mapped_times[$adpa_id] = strtotime($adpa_row['adpa_timestamp']);
            }
            arsort($mapped_times);
    
            if(!empty($mapped_times))
            {
                $modified = true;
                $adpa_row = $payments[array_keys($mapped_times)[0]];
        
                if($adpa_row['adpa_status'] == 3 && $adpa_row['adpa_result_code'] == "Refused")
                {
                    $latest_status = $adpa_row['adpa_result_code'].System::useOr($adpa_row['adpa_refusal_reason'], "", " - ");
                }
                elseif(($adpa_row['adpa_status'] == 2 || $adpa_row['adpa_status'] == 3) && $adpa_row['adpa_result_code'] == "Authorised")
                {
                    $latest_status = $adpa_row['adpa_result_code']." - ".AdyenPaymentStatus::name($adpa_row['adpa_status']);
                }
                else
                {
                    $latest_status = AdyenPaymentStatus::name($adpa_row['adpa_status']);
                }
            }
    
            //Combine certain fields
            $combined = [];
            $combinations = [
                'in_id' => null,
                'in_number' => null,
                'in_date' => null,
                'in_expiration_date' => null,
                'in_amount_netto_eur' => $EUR,
                'in_amount_netto' => $FC,
                PAIRED => $EUR,
                VIRTUALLY_LEFT => $FC,
                AMOUNT_LEFT => $EUR
            ];
    
            foreach($combinations as $invoice_key => $modifier)
            {
                $result = [];
                foreach($invoices_2 as $invoice_id => $invoice)
                {
                    if($modifier === null)
                    {
                        $result[] = $invoice[$invoice_key];
                    }
                    else
                    {
                        $result[] = $modifier." ".System::numberFormat($invoice[$invoice_key], 2);
                    }
                }
        
                $combined[$invoice_key] = implode("<br />", $result);
            }
            
            $invoices[$id]['combined'] = $combined;
            $invoices[$id]['latest_status'] = $latest_status;
            $invoices[$id]['number_input_class'] = "number-input".($modified ? " modified" : "");
            $invoices[$id]['left'] = $left;
            $invoices[$id]['FC'] = $FC;
    
            if($payments)
            {
                $invoices[$id]['details'] = self::createPaymentsTable($payments, $payment_currencies);
            }
         
    
        }
        
        //Combine the invoices
        
        return view('finance.chargecreditcards', [
            'results' => $invoices,
            'selected_date' => $request->date,
            'selected_outstanding' => Arr::exists($request, 'exclude_outstanding') ? 1  : 0,
            'selected_virtually' => Arr::exists($request, 'exclude_virtually_paid') ? 1  : 0,
            'selected_amex' => Arr::exists($request, 'exclude_amex') ? 1  : 0,
            'selected_bank' => Arr::exists($request, 'exclude_bank_transfers') ? 1  : 0
        ]);
    }
    
    public function charge(Request $request)
    {
        Log::debug($request);
    
        //Transform the data to single array
        $invoice_to_charge = ["report_comment" => "Adyen Payment Batch"];
        $checked_invoices = $request->invoices;
        foreach($checked_invoices as $invoice => $x)
        {
            $invoice_to_charge["charges"][$invoice] = [
                "charge" => $request[$invoice."-charge"],
                "children" => []
            ];
        }
        
        Log::debug($invoice_to_charge);
    
        $rep_id = 27;
        $repr_id = Reporting::addToQueue("27", "adyen_credit_card_batch", $invoice_to_charge);
    
        return redirect('reports/'.$rep_id.'/view/adyen_credit_card_batch/'.$repr_id.'/view');
        
       // $result = AdyenTransaction::insertCreditCardPayment($request->adcade_id, $request->amount, "CCCHARGE");
        //$message = AdyenTransaction::makeCreditCardPayment($result['adcade'], $result['adpa_id'], $result['adpa_merchant_reference'], $request->amount);
        
        //return redirect()->back()->with('message', $message);
    }
    
    private function getInvoices(Request $request)
    {
        $system = new System();
        $invoices = [];
        
        $date = explode( ' - ', $request->date );
        
        $query = DB::table("invoices")
            ->leftJoin("customers", "cu_id", "in_cu_id")
            ->where("in_date", ">=", $date[0])
            ->where("in_date", "<=", $date[1])
            ->where("cu_deleted", 0);
        
        
        if (Arr::exists($request, 'exclude_amex'))
        {
            $query->where("cu_credit_card_amex", 0);
        }
        
        if (Arr::exists($request, 'exclude_bank_transfers'))
        {
            $query->where("in_payment_method", PaymentMethod::CREDIT_CARD);
        } else
        {
            $query->whereIn("in_payment_method", [PaymentMethod::CREDIT_CARD, PaymentMethod::BANK_TRANSFER]);
        }
        
        $query = $query
            ->get();
        
        foreach ($query as $row)
        {
            $row = $system->databaseToArray($row);
            $row[PAIRED] = 0;
            $row[AMOUNT_LEFT] = $row['in_amount_netto_eur'];
            $row[PAYMENTS] = [];
            $row[VIRTUALLY_LEFT] = $row['in_amount_netto'];
            $invoices[$row['in_id']] = $row;
        }
        
        //Check if payed
        if (!empty($invoices))
        {
            $bank_query = DB::table("kt_bank_line_invoice_customer_ledger_account")
                ->select("ktbaliinculeac_in_id", "ktbaliinculeac_amount")
                ->whereIn("ktbaliinculeac_in_id", array_keys($invoices))
                ->get();
            
            
            foreach ($bank_query as $bank_row)
            {
                $invoice = &$invoices[$bank_row->ktbaliinculeac_in_id];
                $invoice[PAIRED] += $bank_row->ktbaliinculeac_amount;
                $invoice[AMOUNT_LEFT] -= $bank_row->ktbaliinculeac_amount;
                unset($invoice);
            }
            
            foreach ($invoices as $in_id => $invoice)
            {
                if (round($invoice[AMOUNT_LEFT], 2) == 0)
                {
                    unset($invoices[$in_id]);
                }
            }
        }
        
        return $invoices;
    }

    private function associatePayments(&$invoices, $request)
    {
        if(empty($invoices))
        {
            return;
        }
        
        $system = new System();
    
        $query = DB::table("kt_adyen_payments_invoices")->select("ktadpain_in_id", "adpa_id", "adpa_timestamp",
            "adpa_result_code", "adpa_refusal_reason", "adpa_status", "adpa_amount", "adpa_pacu_code",
            "adcade_card_expiry_month", "adcade_card_expiry_year", "adcade_card_last_digits",
            "adcade_enabled", "adcade_removed")
            ->leftJoin("adyen_payments", "ktadpain_adpa_id", "adpa_id")
            ->leftJoin("adyen_card_details", "adpa_adcade_id", "adcade_id")
            ->whereIn("ktadpain_in_id", array_keys($invoices))
            ->get();
        
        foreach($query as $row)
        {
            $in_id = $row->ktadpain_in_id;
            if($row->adpa_status == 2 && Arr::exists($request, 'exclude_outstanding'))
            {
                unset($invoices[$in_id]);
            }
            elseif(isset($invoices[$in_id]))
            {
                unset($row->ktadpain_in_id);
                $invoices[$in_id][PAYMENTS][$row->adpa_id] = $system->databaseToArray($row);
                
                if($row->adpa_status == 3 && $row->adpa_result_code == 'Authorised')
                {
                    $invoices[$in_id][VIRTUALLY_LEFT] -= $row->adpa_amount;
                }
            }
        }
        
        if(Arr::exists($request, 'exclude_virtually_paid'))
        {
            foreach($invoices as $in_id => $invoice)
            {
                if($invoice[VIRTUALLY_LEFT] <= 0.0 && $invoice['in_amount_netto'] != $invoice[VIRTUALLY_LEFT])
                {
                    unset($invoices[$in_id]);
                }
            }
        }
    }

    private function fillCustomerData(&$invoices)
    {
        $system = new System();
        if(empty($invoices))
        {
            return;
        }
        
        $customer_to_invoices = [];
        foreach($invoices as $invoice_id => $invoice)
        {
            $cu_id = $invoice['in_cu_id'];
            
            if(isset($customer_to_invoices[$cu_id]))
            {
                $customer_to_invoices[$cu_id][] = $invoice_id;
            }
            else
            {
                $customer_to_invoices[$cu_id][] = $invoice_id;
            }
        }
    
        $query = DB::table("customers")
            ->select("cu_id", "cu_company_name_business")
            ->whereIn("cu_id", array_keys($customer_to_invoices))
            ->get();
        
        foreach($query as $row)
        {
            $cu_id = $row->cu_id;
            foreach($customer_to_invoices[$cu_id] as $invoice_id)
            {
                $invoices[$invoice_id]["cu_company_name_business"] = $row->cu_company_name_business;
            }
        }
        
        $processed_customers = [];
    
        $cards_query = DB::table("adyen_card_details")
            ->select("adcade_cu_id", "adcade_card_expiry_month", "adcade_card_expiry_year", "adcade_card_last_digits")
            ->whereIn("adcade_cu_id", array_keys($customer_to_invoices))
            ->where("adcade_enabled", 1)
            ->where("adcade_removed", 0)
            ->get();
        
        foreach($cards_query as $cards_row)
        {
            $cards_row = $system->databaseToArray($cards_row);
            $cu_id = $cards_row['adcade_cu_id'];
            unset($cards_row['adcade_cu_id']);
            $processed_customers[$cu_id] = $cu_id;
            foreach($customer_to_invoices[$cu_id] as $invoice_id)
            {
                $invoices[$invoice_id]["card"] = $cards_row;
            }
        }
        
        foreach($customer_to_invoices as $cu_id => $invoice_ids)
        {
            //Remove invoices without cards
            if(!isset($processed_customers[$cu_id]))
            {
                foreach($invoice_ids as $invoice_id)
                {
                    unset($invoices[$invoice_id]);
                }
                unset($customer_to_invoices[$cu_id]);
            }
            else
            {
                //Merge invoices
                if(count($invoice_ids) > 1)
                {
                    self::mergeInvoices($invoices, $invoice_ids);
                }
            }
        }
    }

    private function mergeInvoices(&$invoices, $invoice_ids)
    {
        $system = new System();
        //Check if any negative invoices & check total
        $total = 0;
        $current_invoices = [];
        $negatives = [];
        foreach($invoice_ids as $invoice_id)
        {
            $total += $invoices[$invoice_id]['in_amount_netto'];
            $current_invoices[$invoice_id] = $invoices[$invoice_id];
            if($invoices[$invoice_id]['in_amount_netto'] < 0)
            {
                $negatives[$invoice_id] = $invoice_id;
            }
        }
        
        //No credit invoices
        if(empty($negatives))
        {
            return;
        }
        
        //Resulting amount is negative (we owe them money)
        if($total < 0)
        {
            foreach($invoice_ids as $invoice_id)
            {
                unset($invoices[$invoice_id]);
            }
            
            return;
        }
        
        //Compare dates
        foreach($negatives as $invoice_id)
        {
            $options = [];
            
            $invoice = $invoices[$invoice_id];
            $invoice_abs = abs($invoice['in_amount_netto']);
            
            foreach($invoice_ids as $other_invoice_id)
            {
                //Check if still exists
                if(!isset($invoices[$other_invoice_id]))
                {
                    continue;
                }
                
                //Check if not the same and/or credit
                $other_invoice = $invoices[$other_invoice_id];
                if($other_invoice_id == $invoice_id || $other_invoice['in_amount_netto'] < 0)
                {
                    continue;
                }
                
                //Calculate amount left
                $other_invoice_amount = $other_invoice['in_amount_netto'];
                if(isset($other_invoice['children']))
                {
                    foreach($other_invoice['children'] as $other_invoice_child)
                    {
                        $other_invoice_amount += $other_invoice_child['in_amount_netto'];
                    }
                }
                
                //Other invoice needs to be bigger than the credit and in the past (or same day)
                $date_difference = $system->dateDifference($invoice['in_date'], $other_invoice['in_date']);
                if($other_invoice_amount > $invoice_abs && $date_difference >= 0)
                {
                    $options[$other_invoice_id] = $date_difference;
                }
            }
            
            //No options available, merge all into one
            if(empty($options))
            {
                //Find the biggest invoice (combine on that one)
                $biggest = 0;
                $biggest_id = null;
                foreach($current_invoices as $invoice_id => $invoice)
                {
                    if($invoice['in_amount_netto'] > $biggest)
                    {
                        $biggest_id = $invoice_id;
                    }
                }
                
                //Add the other invoices
                foreach($current_invoices as $invoice_id => $invoice)
                {
                    if($invoice_id != $biggest_id)
                    {
                        unset($invoices[$invoice_id]);
                    }
                    
                    if($biggest !== null)
                    {
                        $invoices[$biggest_id]['children'][$invoice_id] = $invoice;
                    }
                }
                
                return;
            }
            
            //Pick the best option
            $other_invoice_id = self::findBestDateMatch($options);
            $invoices[$other_invoice_id]["children"][$invoice_id] = $invoices[$invoice_id];
            
            //Remove the invoice from the list
            unset($invoices[$invoice_id]);
            unset($invoice_ids[$invoice_id]);
        }
        
    }

    private function findBestDateMatch($options)
    {
        asort($options, SORT_NUMERIC);
        
        $current_value = null;
        $current_key = null;
        foreach($options as $key => $value)
        {
            if($value < 0)
            {
                $current_value = $value;
                $current_key = $key;
            }
            elseif($value == 0)
            {
                return $key;
            }
            elseif($current_value !== null)
            {
                return $current_key;
            }
            elseif($value > 0)
            {
                return $key;
            }
        }
        
        return $current_key;
    }
    
    
    private static function createPaymentsTable($payments, $payment_currencies)
    {
        $html = "<table class='table display table-bordered table-striped table-vcenter js-dataTable-full dataTable no-footer'>";
        $html .= "<thead>
                <tr>
                    <th>ID</th>
                    <th>Timestamp</th>
                    <th>Result code</th>
                    <th>Status</th>
                    <th>Amount</th>
                    <th>Card details</th>
                    <th>Card status</th>
                </tr>
               </thead>";
        $html .= "<tbody>";
        
        foreach($payments as $payment){
            $html .= "<tr>";
            $html .= "<td>".$payment['adpa_id']."</td>";
            $html .= "<td>".$payment['adpa_timestamp']."</td>";
            $html .= "<td>".$payment['adpa_result_code']." - ".($payment['adpa_refusal_reason'] ?? "-")."</td>";
            $html .= "<td>".AdyenPaymentStatus::name($payment['adpa_status'])."</td>";
            $html .= "<td>".$payment_currencies->where("pacu_code", $payment['adpa_pacu_code'])->first()->pacu_token." ".System::numberFormat($payment['adpa_amount'], 2)."</td>";
            $html .= "<td>Digits: ".$payment['adcade_card_last_digits']."<br>Expires: ".$payment['adcade_card_expiry_year']."/".$payment['adcade_card_expiry_month']."</td>";
            $html .= "<td>".($payment['adcade_removed'] == 1 ? "Removed" : ($payment['adcade_enabled'] == 1 ? "Active" : "Inactive"))."</td>";
            $html .= "</tr>";
        }
        
        $html .= "</tbody>";
        $html .= "</table>";
        
        return $html;
    }
    
    
}
