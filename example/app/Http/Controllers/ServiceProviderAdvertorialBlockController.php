<?php

namespace App\Http\Controllers;

use App\Data\CustomerEditForms;
use App\Data\NewsletterSlot;
use App\Models\Customer;
use App\Models\KTServiceProviderAdvertorialBlockLanguage;
use App\Models\KTServiceProviderAdvertorialBlockLanguageContent;
use App\Models\KTServiceProviderAdvertorialBlockWebsite;
use App\Models\KTServiceProviderNewsletterBlockCountryDestination;
use App\Models\KTServiceProviderNewsletterBlockCountryOrigin;
use App\Models\KTServiceProviderNewsletterBlockLanguage;
use App\Models\Language;
use App\Models\ServiceProviderAdvertorialBlock;
use App\Models\ServiceProviderNewsletterBlock;
use App\Models\Website;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class ServiceProviderAdvertorialBlockController extends Controller
{
    public function create($customer_id)
    {
        $customer = Customer::findOrFail($customer_id);

        return view('serviceprovideradvertorialblock.create',
            [
                'customer' => $customer,
                'newsletterslots' => NewsletterSlot::all()
            ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'slot' => 'required'
        ]);

        $SPadvertorialblock = new ServiceProviderAdvertorialBlock();

        $SPadvertorialblock->sepradbl_cu_id = $request->customer_id;
        $SPadvertorialblock->sepradbl_slot = $request->slot;
        $SPadvertorialblock->sepradbl_description = $request->description;

        $SPadvertorialblock->save();

        return redirect('serviceproviders/' . $request->customer_id .'/edit');
    }

    public function edit($customer_id, $SPadvertorialblock_id)
    {
        // get the customer and relations
        $customer = Customer::with(['customerquestions.questions'])->findOrFail($customer_id);

        if (Gate::denies('customer-restrictions', $customer)) {
            abort(403);
        }

        $SPadvertorialblock = ServiceProviderAdvertorialBlock::findOrFail($SPadvertorialblock_id);

        $set_languages = KTServiceProviderAdvertorialBlockLanguage::where('ktsepradblla_sepradbl_id', $SPadvertorialblock_id)->get();

        $adbl_languages = [];

        foreach($set_languages as $sl)
        {
            $adbl_languages[] .= $sl->ktsepradblla_la_code;
        }

        $set_websites = KTServiceProviderAdvertorialBlockWebsite::where('ktsepradblwe_sepradbl_id', $SPadvertorialblock_id)->get();

        $adbl_websites = [];

        foreach($set_websites as $sw)
        {
            $adbl_websites[] .= $sw->ktsepradblwe_we_id;
        }

        $websites = [];

        $websites_query = Website::where("we_sirelo", 1)
            ->where("we_sirelo_active", 1)
            ->get();

        foreach ($websites_query as $web) {
            $websites[$web->we_id] = $web->we_website;
        }

        $titles_per_lang = [];
        $content_per_lang = [];
        $buttons_per_lang = [];
        $urls_per_lang = [];

        $languages_content = KTServiceProviderAdvertorialBlockLanguageContent::where("ktsepradbllaco_sepradbl_id", $SPadvertorialblock_id)->get();

        foreach ($languages_content as $lc) {
            $titles_per_lang[$lc->ktsepradbllaco_la_code] = $lc->ktsepradbllaco_title;
            $content_per_lang[$lc->ktsepradbllaco_la_code] = $lc->ktsepradbllaco_content;
            $buttons_per_lang[$lc->ktsepradbllaco_la_code] = $lc->ktsepradbllaco_button;
            $urls_per_lang[$lc->ktsepradbllaco_la_code] = $lc->ktsepradbllaco_url;
        }

        return view('serviceprovideradvertorialblock.edit',
            [
                //Customer info
                'advertorialblock_id' => $SPadvertorialblock_id,
                'advertorialblock' => $SPadvertorialblock,
                'customer' => $customer,
                'languages' => Language::whereNotIn("la_code", ["EN", "PL", "RU", "PT"])->orderBy('la_code')->get(),
                'adbl_languages' => $adbl_languages,
                'adbl_websites' => $adbl_websites,
                'websites' => $websites,
                'statuses' => [1 => 'Active', 0 => 'Inactive'],
                'newsletterslots' => NewsletterSlot::all(),
                'titles_per_lang' => $titles_per_lang,
                'content_per_lang' => $content_per_lang,
                'buttons_per_lang' => $buttons_per_lang,
                'urls_per_lang' => $urls_per_lang
            ]);
    }

    public function update(Request $request, $SPadvertorialblock_id)
    {
        $SPadvertorialblock = ServiceProviderAdvertorialBlock::findOrFail($SPadvertorialblock_id);

        //Filters Languages
        $languages = Language::whereNotIn("la_code", ["EN", "PL", "RU", "PT"])->get();

        if ($request->form_name == CustomerEditForms::GENERAL)
        {
            $request->validate([
                'slot' => 'required',
                'status' => 'required',
            ]);

            $SPadvertorialblock->sepradbl_slot = $request->slot;
            $SPadvertorialblock->sepradbl_description = $request->description;
            $SPadvertorialblock->sepradbl_status = $request->status;
            //$SPadvertorialblock->sepradbl_url = $request->url;
            $SPadvertorialblock->sepradbl_event_category = $request->event_category;
            $SPadvertorialblock->sepradbl_event_label = $request->event_label;
            $SPadvertorialblock->sepradbl_action = $request->event_action;

            foreach($languages as $row_languages)
            {
                if (isset($request->blocklanguages[$row_languages->la_code]))
                {
                    //check if this language is already added to this advertorial
                    $advertorialblock_lang = KTServiceProviderAdvertorialBlockLanguage::where([["ktsepradblla_sepradbl_id", "=", $SPadvertorialblock->sepradbl_id], ["ktsepradblla_la_code", "=", $row_languages->la_code]])->first();

                    if (empty($advertorialblock_lang))
                    {
                        Log::debug($request);
                        $kt_service_provider_advertorial_block_language = new KTServiceProviderAdvertorialBlockLanguage();
                        $kt_service_provider_advertorial_block_language->ktsepradblla_sepradbl_id = $SPadvertorialblock->sepradbl_id;
                        $kt_service_provider_advertorial_block_language->ktsepradblla_la_code = $row_languages->la_code;
                        $kt_service_provider_advertorial_block_language->save();
                    }
                }
                else
                {
                    $lang = KTServiceProviderAdvertorialBlockLanguage::where("ktsepradblla_sepradbl_id", "=", $SPadvertorialblock->sepradbl_id)->where("ktsepradblla_la_code", "=", $row_languages->la_code)->first();
                    if (!empty($lang))
                        $lang->delete();
                }
            }

            //Filters Languages
            $websites = Website::where("we_sirelo", 1)->where("we_sirelo_active", 1)->get();

            foreach($websites as $row_websites)
            {
                if (isset($request->blockwebsites[$row_websites->we_id]))
                {
                    //check if this language is already added to this advertorial
                    $advertorialblock_website = KTServiceProviderAdvertorialBlockWebsite::where([["ktsepradblwe_sepradbl_id", "=", $SPadvertorialblock->sepradbl_id], ["ktsepradblwe_we_id", "=", $row_websites->we_id]])->first();

                    if (empty($advertorialblock_website))
                    {
                        Log::debug($request);
                        $kt_service_provider_advertorial_block_website = new KTServiceProviderAdvertorialBlockWebsite();
                        $kt_service_provider_advertorial_block_website->ktsepradblwe_sepradbl_id = $SPadvertorialblock->sepradbl_id;
                        $kt_service_provider_advertorial_block_website->ktsepradblwe_we_id = $row_websites->we_id;
                        $kt_service_provider_advertorial_block_website->save();
                    }
                }
                else
                {
                    $website_row = KTServiceProviderAdvertorialBlockWebsite::where("ktsepradblwe_we_id", "=", $row_websites->we_id)->where("ktsepradblwe_sepradbl_id", "=", $SPadvertorialblock->sepradbl_id)->first();
                    if (!empty($website_row))
                        $website_row->delete();
                }
            }

            //Save question
            $SPadvertorialblock->save();
        }
        elseif ($request->form_name == "Content")
        {
            foreach($languages as $row_languages)
            {
                $ktspablc = KTServiceProviderAdvertorialBlockLanguageContent::where("ktsepradbllaco_sepradbl_id", $SPadvertorialblock->sepradbl_id)
                    ->where("ktsepradbllaco_la_code", $row_languages->la_code)
                    ->first();

                if (count($ktspablc) > 0) {
                    //Update record
                    $ktspablc->ktsepradbllaco_title = $request->title[$row_languages->la_code];
                    $ktspablc->ktsepradbllaco_content = $request->blockcontent[$row_languages->la_code];
                    $ktspablc->ktsepradbllaco_button = $request->button[$row_languages->la_code];
                    $ktspablc->ktsepradbllaco_url = $request->url[$row_languages->la_code];
                    $ktspablc->save();
                }
                else {
                    //Make new record
                    $kt_service_provider_advertorial_block_language_content = new KTServiceProviderAdvertorialBlockLanguageContent();
                    $kt_service_provider_advertorial_block_language_content->ktsepradbllaco_sepradbl_id = $SPadvertorialblock->sepradbl_id;
                    $kt_service_provider_advertorial_block_language_content->ktsepradbllaco_la_code = $row_languages->la_code;
                    $kt_service_provider_advertorial_block_language_content->ktsepradbllaco_title = $request->title[$row_languages->la_code];
                    $kt_service_provider_advertorial_block_language_content->ktsepradbllaco_content = $request->blockcontent[$row_languages->la_code];
                    $kt_service_provider_advertorial_block_language_content->ktsepradbllaco_button = $request->button[$row_languages->la_code];
                    $kt_service_provider_advertorial_block_language_content->ktsepradbllaco_url = $request->url[$row_languages->la_code] ?? '';
                    $kt_service_provider_advertorial_block_language_content->save();
                }
            }
        }

        return redirect('serviceproviders/' . $SPadvertorialblock->sepradbl_cu_id . '/advertorialblock/'.$SPadvertorialblock->sepradbl_id.'/edit');
    }

    public function delete(Request $request)
    {
        $advertorial_block = ServiceProviderAdvertorialBlock::findOrFail($request->id);

        DB::table('kt_service_provider_advertorial_block_language')->where('ktsepradblla_sepradbl_id', '=', $advertorial_block->sepradbl_id)->delete();

        DB::table('kt_service_provider_advertorial_block_website')->where('ktsepradblwe_sepradbl_id', '=', $advertorial_block->sepradbl_id)->delete();

        $advertorial_block->delete();
    }
}
