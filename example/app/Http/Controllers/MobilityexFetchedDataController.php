<?php

namespace App\Http\Controllers;


use App\Data\CustomerMarketType;
use App\Data\CustomerServices;
use App\Data\CustomerStatusType;
use App\Data\CustomerType;
use App\Data\DebtorStatus;
use App\Data\PaymentMethod;
use App\Data\PaymentReminderStatus;
use App\Data\WebsiteValidateString;
use App\Data\WebsiteValidateType;
use App\Functions\Data;
use App\Functions\System;
use App\Models\Country;
use App\Models\Customer;
use App\Models\CustomerCachedTab;
use App\Models\CustomerGatheredReview;
use App\Models\CustomerRemark;
use App\Models\KTCustomerMembership;
use App\Models\KTRequestCustomerPortal;
use App\Models\Language;
use App\Models\LedgerAccount;
use App\Models\MobilityexFetchedData;
use App\Models\MoverData;
use App\Models\PaymentCurrency;
use App\Models\Region;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class MobilityexFetchedDataController extends Controller
{
    /**
     * @param Request $request
     *
     * Function to Save the data from Python Mobilityex cronjob into our DB
     */
    public function saveGatheredData(Request $request) {
        $json = file_get_contents('php://input');
        $data = json_decode($json, true);
        $system = new System();

        //Check if we received some data from the Python server
        if (!empty($data)) {
            //Loop through the data
            foreach ($data as $mobilityex_row) {
                //Turn JSON into an object
                $mobilityex_row = json_decode($mobilityex_row);
                //$system->sendMessage(4186, $mobilityex_row->companylegalname, $system->serialize($mobilityex_row));

                /*foreach ($mobilityex_row->memberCapabilities as $id => $memberCapability) {
                    $system->sendMessage(4186, $mobilityex_row->companylegalname, $memberCapability->capabilityType->lookupvalue);

                }*/

                $mofeda_record = MobilityexFetchedData::where("mofeda_mobilityex_id", trim($mobilityex_row->id))->first();

                if (count($mofeda_record) == 0) {
                    //Create new mobilityex record to save the data in the DB
                    $new_mobilityex = new MobilityexFetchedData();

                    $domain_mobilityex_url = trim($system->getDomainFromURL($mobilityex_row->website), "/");

                    $customer_search = Customer::where("cu_website", "like", "%".$domain_mobilityex_url."%");

                    $cu_co_code = null;

                    /**
                     * Following part is to get the Main Office address
                     */
                    foreach ($mobilityex_row->serviceproviderAddresses as $id => $address) {
                        if (strpos(strtolower(trim($address->description)), 'main') !== false || strpos(strtolower(trim($address->addressType->lookupvalue)), 'main') !== false) {
                            $new_mobilityex->mofeda_street = trim($address->addressline1);
                            $new_mobilityex->mofeda_zipcode = trim($address->postalcode);
                            $new_mobilityex->mofeda_city = trim($address->city);
                            $new_mobilityex->mofeda_telephone = trim($address->phoneNumber);

                            $new_mobilityex->mofeda_email = trim($address->emailaddress);

                            $cu_co_code = trim($address->countrycode);
                        }
                    }

                    if (!empty($cu_co_code)) {
                        $customer_search = $customer_search->where("cu_co_code", $cu_co_code);
                    }

                    $customer_search = $customer_search->where("cu_deleted", 0)->get();

                    $final_url_erp = null;
                    $final_url_mobilityex = null;
                    $cu_id = null;

                    if (count($customer_search) == 1) {
                        //1 customer found in our ERP
                        $type = 1;

                        $customer = $customer_search[0];
                        $cu_id = $customer->cu_id;

                        $system->sendMessage(4186, "1 found, cu_id = ".$cu_id, "go");

                        $new_mobilityex->mofeda_cu_id = $cu_id;

                        /**
                         * Start block 'Website URLs Validation'
                         *
                         * Validating URL of ERP and URL of Google and save the final URLs in DB
                         */
                        $response = null;
                        $variables = "";

                        //Instance of AjaxController; So we can easily run validation functions
                        $ajaxcontroller = new AjaxController();

                        //Define Website Validation type array, array which tells what ID belongs to what message
                        $website_validate_types = WebsiteValidateType::all();

                        //Create a new post request to send it to function in the AjaxController to validate websites
                        $request = new Request();
                        $request->url_erp = $customer->cu_website;
                        $request->url_other = $mobilityex_row->website;

                        //Set starting domains
                        $start_domain_erp = $system->getDomainFromURL($customer->cu_website);
                        $start_domain_other = $system->getDomainFromURL($mobilityex_row->website);

                        //$system->sendMessage(4186, "mob-url", $mobilityex_row->mofeda_website);

                        //Post the request to validate website URLs function and save data in $response variable
                        $response = json_decode($ajaxcontroller->validateWebsiteURLs($request));

                        //Store final ERP URL and final Google URL in variables, will be saved later in code into customer_review_scores table
                        $final_url_erp = $response->final_erp;
                        $final_url_mobilityex = $response->final_other;

                        //Get final domains of ERP and Google URL and save them into variables
                        $final_domain_erp = $system->getDomainFromURL($final_url_erp);
                        $final_domain_other = $system->getDomainFromURL($final_url_mobilityex);

                        //Get the right type which belongs to the response string
                        $website_validation_type = $website_validate_types[$response->website_validate_string];

                        $new_mobilityex->mofeda_final_url_erp = $final_url_erp;
                        $new_mobilityex->mofeda_final_url_mobilityex = $final_url_mobilityex;
                        $new_mobilityex->mofeda_website_validate_type = $website_validation_type;

                        //Explode all variables which we got back (all variables are about final url and domain.)
                        $variables = explode(",", $response->variables);

                        //Count for variable thing below this
                        $count = 1;

                        //Save to DB
                        foreach ($variables as $var) {
                            $new_mobilityex->{"mofeda_website_validate_var_".$count} = $var;
                            $count++;
                        }

                        /**
                         * End block 'Website URLs Validation'
                         */

                    }
                    elseif (count($customer_search) >= 2) {
                        //2 or more customers found
                        $type = 2;
                    }
                    else {
                        //No customers found. This means we can later check on type 3 for all customers which we can possibly create
                        $type = 3;
                    }


                    $new_mobilityex->mofeda_type = $type;
                    $new_mobilityex->mofeda_mobilityex_id = trim($mobilityex_row->id);
                    $new_mobilityex->mofeda_timestamp = date("Y-m-d H:i:s");
                    $new_mobilityex->mofeda_company_name_legal = trim($mobilityex_row->companylegalname);
                    $new_mobilityex->mofeda_company_name_business = trim($mobilityex_row->tradenames);
                    $new_mobilityex->mofeda_website = trim($mobilityex_row->website);

                    $new_mobilityex->mofeda_established = trim($mobilityex_row->established);
                    $new_mobilityex->mofeda_mobilityex_url = "https://iamovers.mobilityex.com/#/search/service-providers/".trim($mobilityex_row->id);

                    //JSON Objects
                    if (!empty($mobilityex_row->serviceproviderAddresses)) {
                        $new_mobilityex->mofeda_address = json_encode($mobilityex_row->serviceproviderAddresses);
                    }

                    if (!empty($mobilityex_row->memberAssociations)) {
                        $new_mobilityex->mofeda_member_associations = json_encode($mobilityex_row->memberAssociations);
                    }

                    if (!empty($mobilityex_row->serviceproviderContacts)) {
                        $new_mobilityex->mofeda_contact_persons = json_encode($mobilityex_row->serviceproviderContacts);
                    }

                    if (!empty($mobilityex_row->quality)) {
                        $new_mobilityex->mofeda_quality = json_encode($mobilityex_row->quality);
                    }

                    if (!empty($mobilityex_row->movingCapabilities)) {
                        $new_mobilityex->mofeda_moving_capabilities = json_encode($mobilityex_row->movingCapabilities);
                    }

                    /**
                     * Following part is to save the moving/relocation type
                     */
                    foreach ($mobilityex_row->memberCapabilities as $id => $memberCapability) {
                        if (strtolower($memberCapability->capabilityType->lookupvalue) == "moving" || strtolower($memberCapability->capabilityType->lookupvalue) == "relocation") {
                            $new_mobilityex->{"mofeda_".strtolower($memberCapability->capabilityType->lookupvalue)."_type"} = 1;
                        }
                    }

                    $new_mobilityex->save();
                }
            }
        }
    }

    /**
     * Starting page / Filter with countries
     */
    public function validateGatheredData($none_left = false) {
        $countries = [];
        $data = new Data();


        $countries_query = MobilityexFetchedData::select("cu_co_code")
            ->leftJoin("customers", "cu_id", "mofeda_cu_id")
            ->where("mofeda_processed", 0)
            ->where("mofeda_skipped", 0)
            //->where("mofeda_type", 1)
            ->whereNull("mofeda_lock_user_id")
            ->whereNotIn("mofeda_cu_id", function($query) {
                $query->select('mofeda_cu_id')
                    ->from('mobilityex_fetched_data')
                    ->whereNotNull("mofeda_cu_id")
                    ->groupBy("mofeda_cu_id")
                    ->havingRaw("COUNT(*) > 1");
            })
            ->whereRaw("(`mofeda_moving_type` = 1 OR `mofeda_relocation_type` = 1)")
            ->where("cu_deleted", 0)
            ->groupBy("cu_co_code")
            ->get();

        //Loop through all countries and make array of countries with how many left
        foreach ($countries_query as $country)
        {
            $count_left = MobilityexFetchedData::selectRaw("COUNT(*) as `counter`")
                ->leftJoin("customers", "cu_id", "mofeda_cu_id")
                //->where("mofeda_type", 1)
                ->where("mofeda_processed", 0)
                ->where("mofeda_skipped", 0)
                ->where("cu_co_code", $country->cu_co_code)
                ->whereNotIn("mofeda_cu_id", function($query) {
                    $query->select('mofeda_cu_id')
                        ->from('mobilityex_fetched_data')
                        ->whereNotNull("mofeda_cu_id")
                        ->groupBy("mofeda_cu_id")
                        ->havingRaw("COUNT(*) > 1");
                })
                ->whereRaw("(`mofeda_moving_type` = 1 OR `mofeda_relocation_type` = 1)")
                ->whereNull("mofeda_lock_user_id")
                ->where("cu_deleted", 0)
                ->first();

            $countries[$country->cu_co_code] = $data->country($country->cu_co_code, "EN")." (".$count_left->counter.")";
        }

        asort($countries);
        $skipped_count = MobilityexFetchedData::where("mofeda_skipped", 1)->get()->count();

        return view('mobilityex.validate_data_filter',
            [
                'countries' => $countries,
                'skipped_count' => $skipped_count,
                'none_left' => $none_left,
            ]);
    }

    /**
     * Function when clicked on filter
     */
    public function validateGatheredDataFiltered(Request $request) {
        $status = $request->status;
        $co_code = $request->country;
        $type = $request->mobilityex_type;

        if ($type == 1) {
            $mofeda = MobilityexFetchedData::leftJoin("customers", "cu_id", "mofeda_cu_id")
                ->leftJoin("mover_data", "moda_cu_id", "cu_id")
                ->where("cu_deleted", 0)
                ->where("mofeda_type", $type);

            if (!empty($co_code)) {
                $mofeda = $mofeda->where("cu_co_code", $co_code);
            }

            if ($status == 1) {
                $mofeda = $mofeda->where("moda_crm_status",1);
            }
            elseif ($status == 2) {
                $mofeda = $mofeda->where("moda_crm_status", "!=",1);
            }
        }
        else {
            $mofeda = MobilityexFetchedData::where("mofeda_type", $type);
        }

        $mofeda = $mofeda->where("mofeda_processed", 0)
            ->where("mofeda_skipped", 0)
            ->whereNull("mofeda_lock_user_id")
            ->whereRaw("(`mofeda_moving_type` = 1 OR `mofeda_relocation_type` = 1)")
            ->first();

        if (count($mofeda) == 0) {
            return Redirect::back()->withErrors(['There is no data to validate for '.Data::country($request->country, "EN").(($status != 0) ? (($status == 1) ? " with status only partners" : " with status only NON-partners") : "")." and type ".$type]);
        }

        return self::processFiltered($request->country, $status, $type);
    }

    public function processFiltered($co_code, $status = 0, $type) {
        if ($type == 1) {
            $mofeda = MobilityexFetchedData::leftJoin("customers", "cu_id", "mofeda_cu_id")
                ->leftJoin("mover_data", "moda_cu_id", "cu_id")
                ->where("cu_deleted", 0)
                ->where("mofeda_type", $type);

            if (!empty($co_code)) {
                $mofeda = $mofeda->where("cu_co_code", $co_code);
            }

            if ($status == 1) {
                $mofeda = $mofeda->where("moda_crm_status",1);
            }
            elseif($status == 2) {
                $mofeda = $mofeda->where("moda_crm_status","!=", 1);
            }
        }
        else {
            $mofeda = MobilityexFetchedData::where("mofeda_type", $type);
        }

        $mofeda = $mofeda->where("mofeda_skipped", 0)
            ->where("mofeda_processed", 0)
            ->whereNull("mofeda_lock_user_id");

        $mofeda = $mofeda->orderBy("mofeda_timestamp", "ASC")->first();

        if (count($mofeda) == 0) {
            return Redirect::back()->withErrors(['There is no data to validate for '.Data::country($co_code, "EN").(($status != 0) ? (($status == 1) ? " with status only partners" : " with status only NON-partners") : "")." and type ".$type]);
        }

        if ($type == 1) {
            $mofeda_left = MobilityexFetchedData::selectRaw("COUNT(*) as `count`")
                ->leftJoin("customers", "cu_id", "mofeda_cu_id")
                ->leftJoin("mover_data", "moda_cu_id", "cu_id")
                ->where("cu_deleted", 0)
                ->where("mofeda_type", $type);

            if (!empty($co_code)) {
                $mofeda_left = $mofeda_left->where("cu_co_code", $co_code);
            }

            if ($status == 1) {
                $mofeda_left = $mofeda_left->where("moda_crm_status",1);
            }
            elseif($status == 2) {
                $mofeda_left = $mofeda_left->where("moda_crm_status","!=", 1);
            }
        }
        else {
            $mofeda_left = MobilityexFetchedData::selectRaw("COUNT(*) as `count`")->where("mofeda_type", $type);
        }

        $mofeda_left = $mofeda_left->where("mofeda_processed", 0)
            ->where("mofeda_skipped", 0)
            ->whereNull("mofeda_lock_user_id")
            ->whereRaw("(`mofeda_moving_type` = 1 OR `mofeda_relocation_type` = 1)")
            ->first()->count;


        $notices = [];
        $warnings = [];

        $mofeda_finder = MobilityexFetchedData::leftJoin("customers", "cu_id", "mofeda_cu_id")->where("mofeda_website", $mofeda->mofeda_website)->where("mofeda_type", 1)->where("mofeda_website", "!=", "")->where("mofeda_id", "!=", $mofeda->mofeda_id)->where("cu_deleted", 0)->get();
        $website_ERP_finder = Customer::where("cu_website", $mofeda->mofeda_website)->where("cu_website", "!=", "")->where("cu_id", "!=", $mofeda->mofeda_cu_id)->where("cu_deleted", 0)->get();
        if (count($mofeda_finder) > 0 || count($website_ERP_finder) > 0) {
            $notices['Website'] = "The website URL is not unique. We got the following customer(s) with the same URL: ";

            $count = 0;
            if (count($mofeda_finder) > 0) {
                foreach($mofeda_finder as $mf) {
                    $count++;
                    $notices['Website'] .= (($count > 1) ? ", " : "")."<a target='_blank' href='/mobilityex_validate_skipped_data/".$mf->mofeda_id."/validate'>".$mf->cu_company_name_business." (".$mf->mofeda_cu_id.")</a>";
                }
            }

            if (count($website_ERP_finder) > 0) {
                foreach($website_ERP_finder as $wef) {
                    $count++;
                    $notices['Website'] .= (($count > 1) ? ", " : "")."<a target='_blank' href='/customers/".$wef->cu_id."/edit'>ERP - ".$wef->cu_company_name_business." (".$wef->cu_id.")</a>";
                }
            }
        }

        if ($mofeda->moda_disable_sirelo_export) {
            $warnings['Sirelo export disabled'] = 'This company is disabled from the export to Sirelo!';
        }

        if ($mofeda->moda_not_operational) {
            $warnings['Not operational'] = 'This company is not operational anymore!';
        }

        $search_url = $mofeda->mofeda_mobilityex_url;

        $system = new System();

        $domain_erp = str_replace("www.", "", $system->getDomainFromURL($mofeda->cu_website));
        $domain_other = str_replace("www.", "", $system->getDomainFromURL($mofeda->mofeda_website));

        if (!empty($domain_erp) && !empty($domain_other) && $domain_erp != $domain_other)
        {
            $notices['Other domains'] = "The ERP and MobilityEx URL have different domains. Please check if its the same company!";
        }

        //Check if this customer was partner (or is partner)
        $did_we_send_leads_last_3_years = false;

        $ktrecupo = KTRequestCustomerPortal::where("ktrecupo_cu_id", $mofeda->cu_id)->whereRaw("`ktrecupo_timestamp` BETWEEN NOW() - INTERVAL 3 YEAR AND NOW()")->where("ktrecupo_type", 1)->first();
        if (count($ktrecupo) > 0)
        {
            $did_we_send_leads_last_3_years = true;
        }

        $associations = [];

        foreach (json_decode($mofeda->mofeda_member_associations) as $assocation){
            if ($assocation->memberAssociation->lookupvalue == "FEDEMAC"
                || $assocation->memberAssociation->lookupvalue == "FIDI"
                || $assocation->memberAssociation->lookupvalue == "IAM"
            ) {
                $associations[$assocation->memberAssociation->lookupvalue] = [
                    'name' => $assocation->memberAssociation->lookupvalue,
                    'link' => $assocation->website,
                    'exp_date' => $assocation->valExpiryDate,
                ];
            }
        }

        $website_validate_message = null;

        $website_validate_strings = WebsiteValidateString::all();

        if (!empty($mofeda->mofeda_website_validate_type)) {
            $var_counter = 0;
            for ($x = 1; $x <= 6; $x++) {
                if(!empty($mofeda->{"mofeda_website_validate_var_".$x})) {
                    $var_counter++;
                }
            }
            if($var_counter == 0) {
                $website_validate_message = $website_validate_strings[$mofeda->mofeda_website_validate_type];
            }
            elseif ($var_counter == 1) {
                $website_validate_message = sprintf($website_validate_strings[$mofeda->mofeda_website_validate_type], $mofeda->mofeda_website_validate_var_1);
            }
            elseif($var_counter == 2) {
                $website_validate_message = sprintf($website_validate_strings[$mofeda->mofeda_website_validate_type], $mofeda->mofeda_website_validate_var_1, $mofeda->mofeda_website_validate_var_2);
            }
            elseif($var_counter == 3) {
                $website_validate_message = sprintf($website_validate_strings[$mofeda->mofeda_website_validate_type], $mofeda->mofeda_website_validate_var_1, $mofeda->mofeda_website_validate_var_2, $mofeda->mofeda_website_validate_var_3);
            }
            elseif($var_counter == 4) {
                $website_validate_message = sprintf($website_validate_strings[$mofeda->mofeda_website_validate_type], $mofeda->mofeda_website_validate_var_1, $mofeda->mofeda_website_validate_var_2, $mofeda->mofeda_website_validate_var_3, $mofeda->mofeda_website_validate_var_4);
            }
            elseif($var_counter == 5) {
                $website_validate_message = sprintf($website_validate_strings[$mofeda->mofeda_website_validate_type], $mofeda->mofeda_website_validate_var_1, $mofeda->mofeda_website_validate_var_2, $mofeda->mofeda_website_validate_var_3, $mofeda->mofeda_website_validate_var_4, $mofeda->mofeda_website_validate_var_5);
            }
            elseif($var_counter == 6) {
                $website_validate_message = sprintf($website_validate_strings[$mofeda->mofeda_website_validate_type], $mofeda->mofeda_website_validate_var_1, $mofeda->mofeda_website_validate_var_2, $mofeda->mofeda_website_validate_var_3, $mofeda->mofeda_website_validate_var_4, $mofeda->mofeda_website_validate_var_5, $mofeda->mofeda_website_validate_var_6);
            }
        }


        $mofeda->mofeda_lock_user_id = Auth::user()->id;
        $mofeda->mofeda_lock_timestamp = date("Y-m-d H:i:s");

        $mofeda->save();

        return view('mobilityex.process',
            [
                'to_be_processed' => $mofeda,
                'left' => $mofeda_left,
                'notices' => $notices,
                'warnings' => $warnings,
                'search_url' => $search_url,
                'associations'  => $associations,
                'did_we_send_leads_last_3_years' => $did_we_send_leads_last_3_years,
                'crm_statuses' => CustomerStatusType::all(),
                'status' => $status,
                'co_code' => $co_code,
                'website_validate_message' => $website_validate_message,
                'unserialized_history' => System::unserialize($mofeda->mofeda_history)
            ]);
    }

    public function validateSkippedDataOpen($mofeda_id)
    {
        $type = MobilityexFetchedData::where("mofeda_id", $mofeda_id)->first()->mofeda_type;

        if ($type == 1) {
            $mofeda = MobilityexFetchedData::leftJoin("customers", "cu_id", "mofeda_cu_id")->leftJoin("mover_data", "moda_cu_id", "cu_id")->where("mofeda_id", $mofeda_id)->first();
        }
        else {
            $mofeda = MobilityexFetchedData::where("mofeda_id", $mofeda_id)->first();
        }

        if (count($mofeda) == 0) {
            abort(404);
        }

        $warnings = [];
        $notices = [];

        $mofeda_finder = MobilityexFetchedData::leftJoin("customers", "cu_id", "mofeda_cu_id")->where("mofeda_type", 1)->where("mofeda_website", $mofeda->mofeda_website)->where("mofeda_website", "!=", "")->where("mofeda_id", "!=", $mofeda->mofeda_id)->where("cu_deleted", 0)->get();
        $website_ERP_finder = Customer::where("cu_website", $mofeda->mofeda_website)->where("cu_website", "!=", "")->where("cu_id", "!=", $mofeda->mofeda_cu_id)->where("cu_deleted", 0)->get();

        if (count($mofeda_finder) > 0 || count($website_ERP_finder) > 0) {
            $notices['Website'] = "The website URL is not unique. We got the following customer(s) with the same URL: ";

            $count = 0;
            if (count($mofeda_finder) > 0) {
                foreach($mofeda_finder as $cf) {
                    $count++;
                    $notices['Website'] .= (($count > 1) ? ", " : "")."<a target='_blank' href='/mobilityex_validate_skipped_data/".$cf->mofeda_id."/validate'>".$cf->cu_company_name_business." (".$cf->mofeda_cu_id.")</a>";
                }
            }

            if (count($website_ERP_finder) > 0) {
                foreach($website_ERP_finder as $wef) {
                    $count++;
                    $notices['Website'] .= (($count > 1) ? ", " : "")."<a target='_blank' href='/customers/".$wef->cu_id."/edit'>ERP - ".$wef->cu_company_name_business." (".$wef->cu_id.")</a>";
                }
            }
        }

        if ($mofeda->moda_disable_sirelo_export) {
            $warnings['Sirelo export disabled'] = 'This company is disabled from the export to Sirelo!';
        }

        if ($mofeda->moda_not_operational) {
            $warnings['Not operational'] = 'This company is not operational anymore!';
        }

        $system = new System();

        $domain_erp = str_replace("www.", "", $system->getDomainFromURL($mofeda->cu_website));
        $domain_other = str_replace("www.", "", $system->getDomainFromURL($mofeda->mofeda_website));

        if (!empty($domain_erp) && !empty($domain_other) && $domain_erp != $domain_other)
        {
            $notices['Other domains'] = "The ERP and MobilityEx URL have different domains. Please check if its the same company!";
        }

        $search_url = $mofeda->mofeda_mobilityex_url;

        //Check if this customer was partner (or is partner)
        $did_we_send_leads_last_3_years = false;

        $ktrecupo = KTRequestCustomerPortal::where("ktrecupo_cu_id", $mofeda->cu_id)->whereRaw("`ktrecupo_timestamp` BETWEEN NOW() - INTERVAL 3 YEAR AND NOW()")->where("ktrecupo_type", 1)->first();
        if (count($ktrecupo) > 0)
        {
            $did_we_send_leads_last_3_years = true;
        }

        $associations = [];

        foreach (json_decode($mofeda->mofeda_member_associations) as $association){
            if ($association->memberAssociation->lookupvalue == "FEDEMAC"
                || $association->memberAssociation->lookupvalue == "FIDI"
                || $association->memberAssociation->lookupvalue == "IAM"
            ) {
                $associations[$association->memberAssociation->lookupvalue] = [
                    'name' => $association->memberAssociation->lookupvalue,
                    'link' => $association->website,
                    'exp_date' => $association->valExpiryDate,
                ];
            }
        }

        $website_validate_message = null;

        $website_validate_strings = WebsiteValidateString::all();

        if (!empty($mofeda->mofeda_website_validate_type)) {
            $var_counter = 0;
            for ($x = 1; $x <= 6; $x++) {
                if(!empty($mofeda->{"mofeda_website_validate_var_".$x})) {
                    $var_counter++;
                }
            }

            if($var_counter == 0) {
                $website_validate_message = $website_validate_strings[$mofeda->mofeda_website_validate_type];
            }
            elseif ($var_counter == 1) {
                $website_validate_message = sprintf($website_validate_strings[$mofeda->mofeda_website_validate_type], $mofeda->mofeda_website_validate_var_1);
            }
            elseif($var_counter == 2) {
                $website_validate_message = sprintf($website_validate_strings[$mofeda->mofeda_website_validate_type], $mofeda->mofeda_website_validate_var_1, $mofeda->mofeda_website_validate_var_2);
            }
            elseif($var_counter == 3) {
                $website_validate_message = sprintf($website_validate_strings[$mofeda->mofeda_website_validate_type], $mofeda->mofeda_website_validate_var_1, $mofeda->mofeda_website_validate_var_2, $mofeda->mofeda_website_validate_var_3);
            }
            elseif($var_counter == 4) {
                $website_validate_message = sprintf($website_validate_strings[$mofeda->mofeda_website_validate_type], $mofeda->mofeda_website_validate_var_1, $mofeda->mofeda_website_validate_var_2, $mofeda->mofeda_website_validate_var_3, $mofeda->mofeda_website_validate_var_4);
            }
            elseif($var_counter == 5) {
                $website_validate_message = sprintf($website_validate_strings[$mofeda->mofeda_website_validate_type], $mofeda->mofeda_website_validate_var_1, $mofeda->mofeda_website_validate_var_2, $mofeda->mofeda_website_validate_var_3, $mofeda->mofeda_website_validate_var_4, $mofeda->mofeda_website_validate_var_5);
            }
            elseif($var_counter == 6) {
                $website_validate_message = sprintf($website_validate_strings[$mofeda->mofeda_website_validate_type], $mofeda->mofeda_website_validate_var_1, $mofeda->mofeda_website_validate_var_2, $mofeda->mofeda_website_validate_var_3, $mofeda->mofeda_website_validate_var_4, $mofeda->mofeda_website_validate_var_5, $mofeda->mofeda_website_validate_var_6);
            }
        }

        $mofeda->mofeda_lock_user_id = Auth::user()->id;
        $mofeda->mofeda_lock_timestamp = date("Y-m-d H:i:s");

        $mofeda->save();

        return view('mobilityex.process',
            [
                'to_be_processed' => $mofeda,
                'notices' => $notices,
                'warnings' => $warnings,
                'associations' => $associations,
                'skip' => true,
                'search_url' => $search_url,
                'did_we_send_leads_last_3_years' => $did_we_send_leads_last_3_years,
                'crm_statuses' => CustomerStatusType::all(),
                'website_validate_message' => $website_validate_message,
                'unserialized_history' => System::unserialize($mofeda->mofeda_history)

            ]);
    }

    public function validateSkippedDataProcess(Request $request, $mofeda_id)
    {
        return self::process_next($request, "SKIP");
    }

    public function validateSkippedData()
    {
        $mofeda_rows = MobilityexFetchedData::leftJoin("customers", "cu_id", "mofeda_cu_id")->where("mofeda_skipped", 1)->get();

        return view('mobilityex.validate_skipped',
            [
                'mofeda_rows' => $mofeda_rows,
            ]);
    }

    public function addCustomer($mofeda_id) {
        $mofeda = MobilityexFetchedData::where("mofeda_id", $mofeda_id)->first();

        $mofeda_co_code = null;

        foreach (json_decode($mofeda->mofeda_address) as $id => $address) {
            if (strpos(strtolower(trim($address->description)), 'main') !== false || strpos(strtolower(trim($address->addressType->lookupvalue)), 'main') !== false) {
                $mofeda_co_code = trim($address->countrycode);
            }
        }

        return view('mobilityex.add_customer',
            [
                'mofeda' => $mofeda,
                'mofeda_co_code' => $mofeda_co_code,
                //'users' => User::where("id", "!=", 0),
                'countries' => Country::all(),
                'debtorstatuses' => DebtorStatus::all(),
                'paymentreminderstatuses' => PaymentReminderStatus::all(),
                'paymentmethods' => PaymentMethod::all(),
                'paymentcurrencies' => PaymentCurrency::all(),
                'ledgeraccounts' => LedgerAccount::all(),
                'languages' => Language::where("la_iframe_only", 0)->get(),
                "customerservices" => CustomerServices::all(),
                'users' => User::where("id", "!=", 0)->where("us_is_deleted", 0)->orderBy("us_name")->get(),
                "customerstatustypes" => CustomerStatusType::all(),
                "customermarkettypes" => CustomerMarketType::all(),
                'regions' => Region::join("countries", "reg_co_code", "co_code")->where("reg_destination_type", 1)->where("reg_deleted", 0)->groupBy("reg_co_code", "reg_parent")->get(),


            ]);
    }

    public function addCustomerStore(Request $request) {
        $mofeda = MobilityexFetchedData::where("mofeda_id", $request->mofeda_id)->first();

        $skipped = $mofeda->mofeda_skipped;

        // Create a User using User model
        $customer = new Customer;

        $customer->cu_type = 1;
        $customer->cu_la_code = $request->la_code;
        $customer->cu_company_name_business = $request->company_name_business;
        $customer->cu_company_name_legal = $request->company_name_legal;
        $customer->cu_attn = $request->attn;
        $customer->cu_attn_email = $request->attn_email;
        $customer->cu_coc = $request->coc;
        $customer->cu_street_1 = $request->street_1;
        $customer->cu_street_2 = $request->street_2;
        $customer->cu_city = $request->city;
        $customer->cu_zipcode = $request->zipcode;
        $customer->cu_email = $request->leads_email;
        $customer->cu_co_code = $request->country;
        $customer->cu_telephone = $request->telephone;

        //IF country is US, than try to find the right state and save it
        if ($request->country == "US" && !empty($request->city)) {
            $admincontroller = new AdminController();

            $stateId = $admincontroller->getStateIdByCity($request->city, $request->country);

            if (!empty($stateId)) {
                $customer->cu_state = $stateId;
            }
        }

        $customer->cu_sales_manager = null;
        $customer->cu_account_manager = null;

        $website_url = $request->website;
        if (!empty($website_url))
        {
            if (strpos($website_url, 'http') === false)
            {
                $website_url = "http://" . $website_url;
            }
        }

        $customer->cu_website = $website_url;
        $customer->cu_street_1_bi = $request->street_1_bi;
        $customer->cu_street_2_bi = $request->street_2_bi;
        $customer->cu_city_bi = $request->city_bi;
        $customer->cu_zipcode_bi = $request->zipcode_bi;
        $customer->cu_email_bi = $request->email_bi;
        $customer->cu_description = $request->description;
        if ($request->int_telephone_bi != 0)
        {
            $customer->cu_telephone_bi = $request->int_telephone_bi;
        }

        $website_url = $request->website_bi;
        if (!empty($website_url))
        {
            if (strpos($website_url, 'http') === false)
            {
                $website_url = "http://" . $website_url;
            }
        }

        $customer->cu_website_bi = $website_url;
        $customer->cu_co_code_bi = $request->country_bi;
        $customer->cu_created_timestamp = date("Y-m-d H:i:s");
        $customer->cu_debtor_number = DB::table('customers')->max('cu_debtor_number') + 1;
        $customer->cu_use_billing_address = ($request->use_billing_address === 'on');

        $customer->save();

        $moverdata = new MoverData();

        $moverdata->moda_cu_id = $customer->cu_id;
        $moverdata->moda_contact_email = $request->leads_email;
        $moverdata->moda_load_exchange_email = $request->load_exchange_email;
        $moverdata->moda_review_email = $request->review_email;
        $moverdata->moda_info_email = $request->general_info;
        $moverdata->moda_contact_telephone = $request->telephone;
        $moverdata->moda_max_claim_percentage = 10;
        $moverdata->moda_activate_lead_pick = 1;
        $moverdata->moda_international_moves = 1;

        $moverdata->moda_reg_id = $request->fields_region;
        $moverdata->moda_market_type = $request->fields_market;
        $moverdata->moda_services = $request->fields_services;
        $moverdata->moda_crm_status = $request->fields_status;
        $moverdata->moda_owner = $request->fields_owner;

        $moverdata->save();

        $mofeda->mofeda_original_type = $mofeda->mofeda_type;
        $mofeda->mofeda_type = 1;
        $mofeda->mofeda_cu_id = $customer->cu_id;
        $mofeda->mofeda_processed = 1;
        $mofeda->mofeda_skipped = 0;
        $mofeda->mofeda_processed_counter = $mofeda->mofeda_processed_counter + 1;
        $mofeda->mofeda_processed_timestamp = date("Y-m-d H:i:s");
        $mofeda->mofeda_processed_by = Auth::user()->us_id;
        $mofeda->mofeda_what_happend = "Created";

        $mofeda->mofeda_accepted = 1;
        $mofeda->mofeda_mail = 4;

        $associations_json = json_decode($mofeda->mofeda_member_associations);

        foreach ($associations_json as $association){
            if ($association->valApproved) {
                if ($association->memberAssociation->lookupvalue == "IAM") {
                    $membership_id = 4;

                    //Create record
                    $ktcume_new = new KTCustomerMembership();
                    $ktcume_new->ktcume_cu_id = $customer->cu_id;
                    $ktcume_new->ktcume_me_id = $membership_id;
                    $ktcume_new->ktcume_link = $mofeda->mofeda_mobilityex_url ?? null;
                    $ktcume_new->ktcume_exp_date = $association->valExpiryDate ?? null;
                    $ktcume_new->ktcume_updated_timestamp = date("Y-m-d H:i:s");
                    $ktcume_new->save();
                }
                elseif ($association->memberAssociation->lookupvalue == "FEDEMAC") {
                    $membership_id = 1;

                    //Create record
                    $ktcume_new = new KTCustomerMembership();
                    $ktcume_new->ktcume_cu_id = $customer->cu_id;
                    $ktcume_new->ktcume_me_id = $membership_id;
                    $ktcume_new->ktcume_link = $association->website ?? null;
                    $ktcume_new->ktcume_exp_date = $association->valExpiryDate ?? null;
                    $ktcume_new->ktcume_updated_timestamp = date("Y-m-d H:i:s");
                    $ktcume_new->save();
                }
                elseif ($association->memberAssociation->lookupvalue == "FIDI") {
                    $membership_id = 3;

                    //Create record
                    $ktcume_new = new KTCustomerMembership();
                    $ktcume_new->ktcume_cu_id = $customer->cu_id;
                    $ktcume_new->ktcume_me_id = $membership_id;
                    $ktcume_new->ktcume_link = $association->website ?? null;
                    $ktcume_new->ktcume_exp_date = $association->valExpiryDate ?? null;
                    $ktcume_new->ktcume_updated_timestamp = date("Y-m-d H:i:s");
                    $ktcume_new->save();
                }
            }
        }

        $mofeda->save();

        $new_CCT = new CustomerCachedTab();
        $new_CCT->cucata_cu_id = $customer->cu_id;
        $new_CCT->save();

        //Before returning to skipped list or validation process, open new tab with Customer Edit of the new customer
        $customer_edit_url = "https://erp2.triglobal.info/customers/".$customer->cu_id."/edit";

        echo "<script type='text/javascript'>window.open('".$customer_edit_url."', '_blank')</script>";

        if ($skipped) {
            return redirect("/mobilityex_validate_skipped_data");
        }
        else {
            return self::processFiltered(null, null, 3);
        }

    }

    public function process_next(Request $request, $skip = null)
    {
        if ($request->what_to_do == "nothing" || $request->selected_customer == "nothing") {
            //Do nothing with this row
            $mofeda = MobilityexFetchedData::where("mofeda_id", $request->mofeda_id)->first();

            $mofeda->mofeda_processed = 1;
            $mofeda->mofeda_skipped = 0;
            $mofeda->mofeda_processed_counter = $mofeda->mofeda_processed_counter + 1;
            $mofeda->mofeda_processed_timestamp = date("Y-m-d H:i:s");
            $mofeda->mofeda_processed_by = Auth::user()->us_id;
            $mofeda->mofeda_what_happend = "Nothing/Deleted";
            $mofeda->save();

            if ($skip != null) {
                return redirect("/mobilityex_validate_skipped_data");
            }
            else {
                $mofeda_finder = MobilityexFetchedData::where("mofeda_type", $request->mobilityex_type)
                    ->where("mofeda_processed", 0)
                    ->where("mofeda_skipped", 0)
                    ->whereNull("mofeda_lock_user_id")
                    ->whereRaw("(`mofeda_moving_type` = 1 OR `mofeda_relocation_type` = 1)")
                    ->first();

                if ($mofeda_finder == 0){
                    return self::validateGatheredData(true);
                }

                return self::processFiltered($request->co_code, $request->status, $request->mobilityex_type);
            }
        }
        if ($request->what_to_do == "add") {
            //Redirect to page to add the customer
            return self::addCustomer($request->mofeda_id);
        }

        if (Arr::exists($request, 'go_to_next')) {
            if ($skip != null) {
                return redirect("/mobilityex_validate_skipped_data");
            }
            else {

                if ($request->mobilityex_type == 1) {
                    $mofeda_finder = MobilityexFetchedData::leftJoin("customers", "cu_id", "mofeda_cu_id")
                        ->leftJoin("mover_data", "cu_id", "moda_cu_id")
                        ->where("mofeda_type", $request->mobilityex_type)
                        ->where("cu_deleted", 0);

                    if (!empty($request->co_code)) {
                        $mofeda_finder = $mofeda_finder->where("cu_co_code", $request->co_code);
                    }

                    if ($request->status == 1) {
                        $mofeda_finder = $mofeda_finder->where("moda_crm_status",1);
                    }
                    elseif ($request->status == 2) {
                        $mofeda_finder = $mofeda_finder->where("moda_crm_status", "!=",1);
                    }
                }
                else {
                    $mofeda_finder = MobilityexFetchedData::where("mofeda_type", $request->mobilityex_type);
                }

                $mofeda_finder = $mofeda_finder->where("mofeda_processed", 0)
                    ->where("mofeda_skipped", 0)
                    ->whereNull("mofeda_lock_user_id")
                    ->whereRaw("(`mofeda_moving_type` = 1 OR `mofeda_relocation_type` = 1)")
                    ->first();

                if ($mofeda_finder == 0){
                    return self::validateGatheredData(true);
                }

                return self::processFiltered($request->co_code, $request->status, $request->mobilityex_type);
            }
        }

        $mofeda = MobilityexFetchedData::leftJoin("customers", "cu_id", "mofeda_cu_id")->where("mofeda_id", $request->mofeda_id)->first();

        if (Arr::exists($request, 'failed_to_delete_next')) {
            $mofeda->mofeda_processed = 1;
            $mofeda->mofeda_skipped = 0;
            $mofeda->mofeda_processed_by = Auth::user()->us_id;
            $mofeda->mofeda_processed_timestamp = date("Y-m-d H:i:s");
            $mofeda->mofeda_lock_user_id = null;
            $mofeda->mofeda_lock_timestamp = null;
            $mofeda->save();

            if ($skip != null) {
                return redirect("/mobilityex_validate_skipped_data");
            }
            else {
                if ($request->mobilityex_type == 1) {
                    $mofeda_finder = MobilityexFetchedData::leftJoin("customers", "cu_id", "mofeda_cu_id")
                        ->leftJoin("mover_data", "cu_id", "moda_cu_id")
                        ->where("mofeda_type", $request->mobilityex_type)
                        ->where("cu_deleted", 0);

                    if (!empty($request->co_code)) {
                        $mofeda_finder = $mofeda_finder->where("cu_co_code", $request->co_code);
                    }

                    if ($request->status == 1) {
                        $mofeda_finder = $mofeda_finder->where("moda_crm_status",1);
                    }
                    elseif ($request->status == 2) {
                        $mofeda_finder = $mofeda_finder->where("moda_crm_status", "!=",1);
                    }
                }
                else {
                    $mofeda_finder = MobilityexFetchedData::where("mofeda_type", $request->mobilityex_type);
                }

                $mofeda_finder = $mofeda_finder->where("mofeda_processed", 0)
                    ->where("mofeda_skipped", 0)
                    ->whereNull("mofeda_lock_user_id")
                    ->whereRaw("(`mofeda_moving_type` = 1 OR `mofeda_relocation_type` = 1)")
                    ->first();

                if ($mofeda_finder == 0){
                    return self::validateGatheredData(true);
                }

                return self::processFiltered($request->co_code, $request->status, $request->mobilityex_type);
            }
        }

        $customer = Customer::where("cu_id", $request->cu_id)->first();
        if ($customer->cu_type == CustomerType::MOVER || $customer->cu_type == CustomerType::LEAD_RESELLER) {
            $mover_data = MoverData::where("moda_cu_id", $request->cu_id)->first();
        }

        if (Arr::exists($request, 'skip_btn')) {
            $mofeda->mofeda_remarks = $request->remarks;
            $mofeda->mofeda_skipped = 1;
            $mofeda->mofeda_lock_user_id = null;
            $mofeda->mofeda_lock_timestamp = null;
            $mofeda->save();

            if ($request->mobilityex_type == 1) {
                $mofeda_finder = MobilityexFetchedData::leftJoin("customers", "cu_id", "mofeda_cu_id")
                    ->leftJoin("mover_data", "cu_id", "moda_cu_id")
                    ->where("mofeda_type", $request->mobilityex_type)
                    ->where("cu_deleted", 0);

                if (!empty($request->co_code)) {
                    $mofeda_finder = $mofeda_finder->where("cu_co_code", $request->co_code);
                }

                if ($request->status == 1) {
                    $mofeda_finder = $mofeda_finder->where("moda_crm_status",1);
                }
                elseif ($request->status == 2) {
                    $mofeda_finder = $mofeda_finder->where("moda_crm_status", "!=",1);
                }
            }
            else {
                $mofeda_finder = MobilityexFetchedData::where("mofeda_type", $request->mobilityex_type);
            }

            $mofeda_finder = $mofeda_finder->where("mofeda_processed", 0)
                ->where("mofeda_skipped", 0)
                ->whereNull("mofeda_lock_user_id")
                ->whereRaw("(`mofeda_moving_type` = 1 OR `mofeda_relocation_type` = 1)")
                ->first();

            if ($mofeda_finder == 0){
                return self::validateGatheredData(true);
            }

            return self::processFiltered($request->co_code, $request->status, $request->mobilityex_type);

        }

        if ($request->mobilityex_type == 2 && $request->mobilityex_type_changed == 1) {
            $mofeda->mofeda_original_type = $request->mobilityex_type;
            $mofeda->mofeda_what_happend = "Connected";

            $mofeda->mofeda_type = 1;
            $mofeda->mofeda_cu_id = $request->cu_id;

            $mofeda_customers_json = json_decode($mofeda->mofeda_customers_json);
            $arr_customers = System::databaseToArray($mofeda_customers_json);

            $arr_customer = $arr_customers[$request->cu_id];

            $mofeda->mofeda_website_validate_type = $arr_customer['website_validate_type'];

            if (isset($arr_customer['mofeda_website_validate_var_1']) && !empty($arr_customer['mofeda_website_validate_var_1'])) {
                $mofeda->mofeda_website_validate_var_1 = $arr_customer['mofeda_website_validate_var_1'];
            }

            if (isset($arr_customer['mofeda_website_validate_var_2']) && !empty($arr_customer['mofeda_website_validate_var_2'])) {
                $mofeda->mofeda_website_validate_var_2 = $arr_customer['mofeda_website_validate_var_2'];
            }

            if (isset($arr_customer['mofeda_website_validate_var_3']) && !empty($arr_customer['mofeda_website_validate_var_3'])) {
                $mofeda->mofeda_website_validate_var_3 = $arr_customer['mofeda_website_validate_var_3'];
            }

            if (isset($arr_customer['mofeda_website_validate_var_4']) && !empty($arr_customer['mofeda_website_validate_var_4'])) {
                $mofeda->mofeda_website_validate_var_4 = $arr_customer['mofeda_website_validate_var_4'];
            }

            if (isset($arr_customer['mofeda_website_validate_var_5']) && !empty($arr_customer['mofeda_website_validate_var_5'])) {
                $mofeda->mofeda_website_validate_var_5 = $arr_customer['mofeda_website_validate_var_5'];
            }

            if (isset($arr_customer['mofeda_website_validate_var_6']) && !empty($arr_customer['mofeda_website_validate_var_6'])) {
                $mofeda->mofeda_website_validate_var_6 = $arr_customer['mofeda_website_validate_var_6'];
            }

            $mofeda->mofeda_final_url_erp = $arr_customer['final_url_erp'];
            $mofeda->mofeda_final_url_mobilityex = $arr_customer['final_url_mobilityex'];
        }
        elseif ($request->mobilityex_type == 3 && $request->mobilityex_type_changed == 1) {
            $mofeda->mofeda_original_type = $request->mobilityex_type;

            $mofeda->mofeda_type = 1;
            $mofeda->mofeda_cu_id = $request->cu_id;

            $mofeda->mofeda_what_happend = "Connected";

            $mofeda_searcher = MobilityexFetchedData::where("mofeda_cu_id", $request->cu_id)->first();

            if ($mofeda_searcher > 0) {
                $mofeda_searcher->mofeda_type = 3;
                $mofeda_searcher->mofeda_cu_id = null;
                $mofeda_searcher->save();
            }
        }
        else {
            $mofeda->mofeda_original_type = 1;
        }

        if(!empty($request->remarks)) {

            // Create a User using User model
            $customerremark = new CustomerRemark();

            $customerremark->cure_timestamp = Carbon::now()->toDateTimeString();
            $customerremark->cure_employee = Auth::user()->us_id;
            $customerremark->cure_text = $request->remarks;
            $customerremark->cure_cu_id = $request->cu_id;

            //Set to correct customer and save
            $customerremark->save();
        }

        $something_changed = false;

        $data = new Data();
        if (!empty($request->website)) {
            if($customer->cu_website != $request{$request->website.'_website'}) {
                $customer->cu_website = $request{$request->website.'_website'};
                $something_changed = true;
            }
        }

        if (!empty($request->company_name_business)) {
            if ($customer->cu_company_name_business != $request{$request->company_name_business.'_company_name_business'}){
                $customer->cu_company_name_business = $request{$request->company_name_business.'_company_name_business'};

                //Make sirelo forward thing
                $data->updateSireloKeyAndForwards($customer,  true);
                $something_changed = true;
            }
        }

        if (!empty($request->company_name_legal)) {
            if ($customer->cu_company_name_legal != $request{$request->company_name_legal.'_company_name_legal'}){
                $customer->cu_company_name_legal = $request{$request->company_name_legal.'_company_name_legal'};
                $something_changed = true;
            }
        }

        if (!empty($request->street)) {
            if ($customer->cu_street_1 != $request{$request->street.'_street'}){
                $customer->cu_street_1 = $request{$request->street.'_street'};
                $something_changed = true;
            }
        }

        if (!empty($request->zipcode)) {
            if ($customer->cu_zipcode != $request{$request->zipcode.'_zipcode'}) {
                $customer->cu_zipcode = $request{$request->zipcode.'_zipcode'};
                $something_changed = true;
            }
        }

        if (!empty($request->city)) {
            if ($customer->cu_city != $request{$request->city.'_city'}) {
                $customer->cu_city = $request{$request->city.'_city'};

                //Sirelo forward thing
                $data->updateSireloKeyAndForwards($customer,  true);
                $something_changed = true;
            }
        }

        if (!empty($request->telephone_main)) {
            if ($customer->cu_telephone != $request{$request->telephone_main.'_telephone_main'}) {
                $customer->cu_telephone = $request{$request->telephone_main.'_telephone_main'};
                $something_changed = true;
            }
        }

        if ($customer->cu_type == CustomerType::MOVER || $customer->cu_type == CustomerType::LEAD_RESELLER) {
            if (!empty($request->telephone_sirelo)) {
                if ($mover_data->moda_contact_telephone != $request{$request->telephone_sirelo . '_telephone_sirelo'}) {
                    $mover_data->moda_contact_telephone = $request{$request->telephone_sirelo . '_telephone_sirelo'};
                    $something_changed = true;
                }
            }

            if (!empty($request->email_sirelo)) {
                if ($mover_data->moda_contact_email != $request{$request->email_sirelo . '_email_sirelo'}) {
                    $mover_data->moda_contact_email = $request{$request->email_sirelo . '_email_sirelo'};
                    $something_changed = true;
                }
            }

            /*if (!empty($request->email_sirelo)) {
                if ($mover_data->moda_contact_email != $request->email_sirelo) {
                    $mover_data->moda_contact_email = $request->email_sirelo;
                    $something_changed = true;
                }
            }*/

            if ($mover_data->moda_disable_sirelo_export && $request->mobilityex_type == 1) {
                $mover_data->moda_disable_sirelo_export = ($request->disable_sirelo_exp === 'on');
            }
        }

        /*if (!empty($request->email_general)) {
            if ($customer->cu_email != $request->email_general) {
                $customer->cu_email = $request->email_general;
                $something_changed = true;
            }
        }*/

        if (!empty($request->email_general)) {
            if ($customer->cu_email != $request{$request->email_general . '_email_general'}) {
                $customer->cu_email = $request{$request->email_general . '_email_general'};
                $something_changed = true;
            }
        }

        if (!empty($request->remarks)) {
            $mofeda->mofeda_remarks = $request->remarks;
        }

        if ($request->use_associations === 'on') {
            $mofeda->mofeda_accepted = 1;

            $associations_json = json_decode($mofeda->mofeda_member_associations);
            $iam = false;
            foreach ($associations_json as $association){
                if ($association->valApproved) {
                    if ($association->memberAssociation->lookupvalue == "IAM") {
                        $iam = true;
                        $membership_id = 4;

                        $ktcume = KTCustomerMembership::where("ktcume_me_id", $membership_id)->where("ktcume_cu_id", $request->cu_id)->first();

                        if (count($ktcume) > 0) {
                            //Update existing record
                            $ktcume->ktcume_link = $mofeda->mofeda_mobilityex_url ?? null;
                            $ktcume->ktcume_exp_date = $association->valExpiryDate ?? null;
                            $ktcume->ktcume_updated_timestamp = date("Y-m-d H:i:s");
                            $ktcume->save();
                        }
                        else {
                            //Create record
                            $ktcume_new = new KTCustomerMembership();
                            $ktcume_new->ktcume_cu_id = $request->cu_id;
                            $ktcume_new->ktcume_me_id = $membership_id;
                            $ktcume_new->ktcume_link = $mofeda->mofeda_mobilityex_url ?? null;
                            $ktcume_new->ktcume_exp_date = $association->valExpiryDate ?? null;
                            $ktcume_new->ktcume_updated_timestamp = date("Y-m-d H:i:s");
                            $ktcume_new->save();
                        }
                    }
                    elseif ($association->memberAssociation->lookupvalue == "FEDEMAC") {
                        $membership_id = 1;

                        $ktcume = KTCustomerMembership::where("ktcume_me_id", $membership_id)->where("ktcume_cu_id", $request->cu_id)->first();

                        if (count($ktcume) > 0) {
                            //Update existing record
                            $ktcume->ktcume_link = $association->website ?? null;
                            $ktcume->ktcume_exp_date = $association->valExpiryDate ?? null;
                            $ktcume->ktcume_updated_timestamp = date("Y-m-d H:i:s");
                            $ktcume->save();
                        }
                        else {
                            //Create record
                            $ktcume_new = new KTCustomerMembership();
                            $ktcume_new->ktcume_cu_id = $request->cu_id;
                            $ktcume_new->ktcume_me_id = $membership_id;
                            $ktcume_new->ktcume_link = $association->website ?? null;
                            $ktcume_new->ktcume_exp_date = $association->valExpiryDate ?? null;
                            $ktcume_new->ktcume_updated_timestamp = date("Y-m-d H:i:s");
                            $ktcume_new->save();
                        }

                    }
                    elseif ($association->memberAssociation->lookupvalue == "FIDI") {
                        $membership_id = 3;

                        $ktcume = KTCustomerMembership::where("ktcume_me_id", $membership_id)->where("ktcume_cu_id", $request->cu_id)->first();

                        if (count($ktcume) > 0) {
                            //Update existing record
                            $ktcume->ktcume_link = $association->website ?? null;
                            $ktcume->ktcume_exp_date = $association->valExpiryDate ?? null;
                            $ktcume->ktcume_updated_timestamp = date("Y-m-d H:i:s");
                            $ktcume->save();
                        }
                        else {
                            //Create record
                            $ktcume_new = new KTCustomerMembership();
                            $ktcume_new->ktcume_cu_id = $request->cu_id;
                            $ktcume_new->ktcume_me_id = $membership_id;
                            $ktcume_new->ktcume_link = $association->website ?? null;
                            $ktcume_new->ktcume_exp_date = $association->valExpiryDate ?? null;
                            $ktcume_new->ktcume_updated_timestamp = date("Y-m-d H:i:s");
                            $ktcume_new->save();
                        }

                    }
                }
            }

            if (!$iam) {
                $ktcume = KTCustomerMembership::where("ktcume_me_id", 4)->where("ktcume_cu_id", $request->cu_id)->first();

                if (count($ktcume) > 0) {
                    DB::table('kt_customer_membership')->where('ktcume_me_id', '=', 4)->where("ktcume_cu_id", $request->cu_id)->delete();

                }


            }
        }

        //Send email when something has been changed
        if ($something_changed) {
            $mofeda->mofeda_mail = 1;
            $mofeda->mofeda_what_happend = "Edited";
        }
        else {
            $mofeda->mofeda_mail = 3; //3 = DONT SEND EMAIL
            $mofeda->mofeda_what_happend = "Nothing";
        }

        if (Arr::exists($request, 'skip_and_save_btn')) {
            $mofeda->mofeda_skipped = 1;
            $mofeda->mofeda_processed = 0;
        } else {
            $mofeda->mofeda_skipped = 0;
            $mofeda->mofeda_processed = 1;
        }

        $mofeda->mofeda_processed_by = Auth::user()->us_id;
        $mofeda->mofeda_processed_timestamp = date("Y-m-d H:i:s");
        $mofeda->mofeda_lock_user_id = null;
        $mofeda->mofeda_lock_timestamp = null;

        $mofeda->mofeda_processed_counter = $mofeda->mofeda_processed_counter + 1;

        if ($customer->cu_type == CustomerType::MOVER || $customer->cu_type == CustomerType::LEAD_RESELLER) {
            $mover_data->save();
        }

        $customer->save();
        $mofeda->save();

        if ($skip != null) {
            return redirect("/mobilityex_validate_skipped_data");
        }
        else {

            if ($request->mobilityex_type == 1) {
                $mofeda_finder = MobilityexFetchedData::leftJoin("customers", "cu_id", "mofeda_cu_id")
                    ->leftJoin("mover_data", "cu_id", "moda_cu_id")
                    ->where("mofeda_type", $request->mobilityex_type)
                    ->where("cu_deleted", 0);

                if (!empty($request->co_code)) {
                    $mofeda_finder = $mofeda_finder->where("cu_co_code", $request->co_code);
                }

                if ($request->status == 1) {
                    $mofeda_finder = $mofeda_finder->where("moda_crm_status",1);
                }
                elseif ($request->status == 2) {
                    $mofeda_finder = $mofeda_finder->where("moda_crm_status", "!=",1);
                }
            }
            else {
                $mofeda_finder = MobilityexFetchedData::where("mofeda_type", $request->mobilityex_type);
            }

            $mofeda_finder = $mofeda_finder->where("mofeda_processed", 0)
                ->where("mofeda_skipped", 0)
                ->whereNull("mofeda_lock_user_id")
                ->whereRaw("(`mofeda_moving_type` = 1 OR `mofeda_relocation_type` = 1)")
                ->first();

            if ($mofeda_finder == 0){
                return self::validateGatheredData(true);
            }

            return self::processFiltered($request->co_code, $request->status, $request->mobilityex_type);
        }
    }

    public function getERPFields(Request $request) {
        $mofeda_row = MobilityexFetchedData::where("mofeda_id", $request->mofeda_id)->first();
        $customer_row = Customer::leftJoin("mover_data", "cu_id", "moda_cu_id")->where("cu_id", $request->cu_id)->first();

        $customers_array = json_decode($mofeda_row->mofeda_customers_json);
        $customers_array = System::databaseToArray($customers_array);
        $customer_array = $customers_array[$customer_row->cu_id];

        $website_validate_strings = WebsiteValidateString::all();

        if (!empty($customer_array['website_validate_type'])) {
            $var_counter = 0;
            for ($x = 1; $x <= 6; $x++) {
                if(!empty($customer_array['mofeda_website_validate_var_'.$x])) {
                    $var_counter++;
                }
            }
            if($var_counter == 0) {
                $website_validate_message = $website_validate_strings[$customer_array['website_validate_type']];
            }
            elseif ($var_counter == 1) {
                $website_validate_message = sprintf($website_validate_strings[$customer_array['website_validate_type']], $customer_array['mofeda_website_validate_var_1']);
            }
            elseif($var_counter == 2) {
                $website_validate_message = sprintf($website_validate_strings[$customer_array['website_validate_type']], $customer_array['mofeda_website_validate_var_1'], $customer_array['mofeda_website_validate_var_2']);
            }
            elseif($var_counter == 3) {
                $website_validate_message = sprintf($website_validate_strings[$customer_array['website_validate_type']], $customer_array['mofeda_website_validate_var_1'], $customer_array['mofeda_website_validate_var_2'], $customer_array['mofeda_website_validate_var_3']);
            }
            elseif($var_counter == 4) {
                $website_validate_message = sprintf($website_validate_strings[$customer_array['website_validate_type']], $customer_array['mofeda_website_validate_var_1'], $customer_array['mofeda_website_validate_var_2'], $customer_array['mofeda_website_validate_var_3'], $customer_array['mofeda_website_validate_var_4']);
            }
            elseif($var_counter == 5) {
                $website_validate_message = sprintf($website_validate_strings[$customer_array['website_validate_type']], $customer_array['mofeda_website_validate_var_1'], $customer_array['mofeda_website_validate_var_2'], $customer_array['mofeda_website_validate_var_3'], $customer_array['mofeda_website_validate_var_4'], $customer_array['mofeda_website_validate_var_5']);
            }
            elseif($var_counter == 6) {
                $website_validate_message = sprintf($website_validate_strings[$customer_array['website_validate_type']], $customer_array['mofeda_website_validate_var_1'], $customer_array['mofeda_website_validate_var_2'], $customer_array['mofeda_website_validate_var_3'], $customer_array['mofeda_website_validate_var_4'], $customer_array['mofeda_website_validate_var_5'], $customer_array['mofeda_website_validate_var_6']);
            }
        }

        $website_validate_message_mobilityex = str_replace(["Google URL", "Google url"], "MobilityEx URL", $website_validate_message);

        $array = [
            "cu_id" => $request->cu_id,
            "cu_company_name_legal" => $customer_row->cu_company_name_legal,
            "cu_company_name_business" => $customer_row->cu_company_name_business,
            "cu_website" => $customer_row->cu_website,
            "cu_street" => $customer_row->cu_street_1,
            "cu_zipcode" => $customer_row->cu_zipcode,
            "cu_city" => $customer_row->cu_city,
            "cu_co_code" => $customer_row->cu_co_code,
            "cu_telephone" => $customer_row->cu_telephone,
            "moda_contact_telephone" => $customer_row->moda_contact_telephone,
            "cu_email" => $customer_row->cu_email,
            "moda_contact_email" => $customer_row->moda_contact_email,

            "final_url_erp" => $customer_array['final_url_erp'],
            "final_url_mobilityex" => $customer_array['final_url_mobilityex'],
            "mobilityex_url" => $mofeda_row['mofeda_website'],
            "final_domain_erp" => System::getDomainFromURL($customer_array['final_url_erp']),
            "final_domain_mobilityex" => System::getDomainFromURL($customer_array['final_url_mobilityex']),
            "website_validate_type" => $customer_array['website_validate_type'],
            "website_validate_message" => $website_validate_message,
            "website_validate_message_mobilityex" => $website_validate_message_mobilityex,
            "mofeda_website_validate_var_1" => $customer_array['mofeda_website_validate_var_1'] ?? '',
            "mofeda_website_validate_var_2" => $customer_array['mofeda_website_validate_var_2'] ?? '',
            "mofeda_website_validate_var_3" => $customer_array['mofeda_website_validate_var_3'] ?? '',
            "mofeda_website_validate_var_4" => $customer_array['mofeda_website_validate_var_4'] ?? '',
            "mofeda_website_validate_var_5" => $customer_array['mofeda_website_validate_var_5'] ?? '',
            "mofeda_website_validate_var_6" => $customer_array['mofeda_website_validate_var_6'] ?? '',
        ];

        echo json_encode($array);
    }

    public function checkIfCustomerIsAvailable(Request $request) {
        $mofeda_row = MobilityexFetchedData::where("mofeda_cu_id", $request->customer_id)->first();

        if (count($mofeda_row) == 0) {
            //Customer is available
            $array = [
                "available" => "yes",
                "mofeda_id" => null
            ];
        }
        else {
            //Customer not available
            $array = [
                "available" => "no",
                "mofeda_id" => $mofeda_row->mofeda_id
            ];
        }

        echo json_encode($array);
    }
}
