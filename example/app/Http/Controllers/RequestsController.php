<?php

namespace App\Http\Controllers;

use App\Data\CustomerPairStatus;
use App\Data\CustomerRequestDeliveryType;
use App\Data\DestinationType;
use App\Data\Device;
use App\Data\EmptyYesNo;
use App\Data\FreeTrialStopType;
use App\Data\MatchMistakeType;
use App\Data\MoverCappingMethod;
use App\Data\MovingSelfCompany;
use App\Data\MovingSize;
use App\Data\NatPaymentType;
use App\Data\PaymentMethod;
use App\Data\PaymentType;
use App\Data\PremiumLeadResidenceType;
use App\Data\PremiumLeadStatus;
use App\Data\RecoverReason;
use App\Data\RejectionReason;
use App\Data\RequestInternalCalled;
use App\Data\RequestResidence;
use App\Data\RequestSource;
use App\Data\RequestStatus;
use App\Data\RequestType;
use App\Data\RequestUpdateFields;
use App\Data\RoomAmounts;
use App\Data\SurveyExecutor;
use App\Data\YesNo;
use App\Functions\Data;
use App\Functions\DataIndex;
use App\Functions\Mail;
use App\Functions\Mover;
use App\Functions\RequestData;
use App\Functions\RequestDeliveries;
use App\Functions\SireloCustomer;
use App\Functions\System;
use App\Models\AffiliatePartnerForm;
use App\Models\AutomatedRequest;
use App\Models\Country;
use App\Models\Customer;
use App\Models\CustomerCredit;
use App\Models\CustomerRemark;
use App\Models\KTCustomerPortal;
use App\Models\KTRequestCustomerPortal;
use App\Models\KTRequestCustomerQuestion;
use App\Models\Language;
use App\Models\MatchStatistic;
use App\Models\MoverData;
use App\Models\Portal;
use App\Models\PremiumLead;
use App\Models\Region;
use App\Models\Request;
use App\Models\RequestUpdate;
use App\Models\User;
use App\Models\Website;
use App\Models\WebsiteForm;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use DateTime;
use http\Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Venturecraft\Revisionable\Revision;

class RequestsController extends Controller
{
    public function index()
    {
        $requests = Request::with("onholdby")->where("re_status", RequestStatus::OPEN)->where("re_automatic", 0)->where("re_request_type", "!=", RequestType::PREMIUM)->get();

        $general_sources = [1, 2];

        $requests['leads'] = $requests->whereIn("re_source", $general_sources)->where("re_double", 0)->where("re_spam", 0)->where("re_on_hold", 0)->where("re_pd_validate", 0);
        //$requests['affiliate'] = $requests->where("re_source", 2)->where("re_double", 0)->where("re_spam", 0)->where("re_on_hold", 0);
        $requests['double'] = $requests->where("re_double", 1)->where("re_pd_validate", 0);
        $requests['spam'] = $requests->where("re_spam", 1)->where("re_pd_validate", 0);
        $requests['on_hold'] = $requests->where("re_on_hold", 1)->where('re_pd_validate', '!=', 1);
        $requests['pd_submitted'] = $requests->where("re_pd_validate", 1);
        $requests['auto_reject'] = Request::where("re_status", 2)->where("re_automatic_checked", 0)->where("re_automatic", 1)->whereNull("re_rejection_us_id")->whereDate("re_rejection_timestamp", ">=", Carbon::now()->subDays(2))->get();

        $requests['automatic'] = Request::where("re_automatic", 1)->count();

        $on_hold_requests_per_users = [];

        foreach ($requests['on_hold'] as $rq)
        {
            if (!array_key_exists($rq->re_on_hold_by, $on_hold_requests_per_users))
            {
                $on_hold_requests_per_users[$rq->re_on_hold_by] = [];
            }

            $on_hold_requests_per_users[$rq->re_on_hold_by][] = $rq;
        }

        ksort($on_hold_requests_per_users);

        foreach (Country::all() as $country)
        {
            $countries[$country->co_code] = $country->co_en;
        }

        $sp_requests_count = KTRequestCustomerQuestion::selectRaw("COUNT(*) as `count`")->with(['request', 'customer', 'question'])
            ->where("ktrecuqu_sent", 0)
            ->where("ktrecuqu_hide", 0)
            ->where("ktrecuqu_rejection_status", 0)
            ->whereHas('request', function ($query) {
                $query->where('re_status', '=', 2);
            })
            ->orderBy("ktrecuqu_timestamp", "asc")
            ->first()->count;

        $feedback_req_query = Request::select("re_id")->leftJoin("kt_request_customer_portal", "re_id", "ktrecupo_re_id")
            ->where("re_match_rating_read", 0)
            ->whereRaw("(re_rejection_us_id = " . Auth::user()->id . " OR ktrecupo_us_id = " . Auth::user()->id . ")")
            ->where("re_show_this_feedback", 1)
            ->orderBy("re_id", "asc")
            ->groupBy("re_id")
            ->get();

        $feedback_first = null;
        $counter = 0;
        $feedback_requests = "-1";
        $length = count($feedback_req_query);

        foreach ($feedback_req_query as $fb)
        {
            $counter++;
            if ($counter == 1)
            {
                $feedback_first = $fb->re_id;
            } else
            {
                if ($counter == 2)
                {
                    $feedback_requests = "" . $fb->re_id . ",";
                } else
                {
                    $feedback_requests .= "" . $fb->re_id . ",";
                }
            }
        }

        if (System::endsWith($feedback_requests, ',') == true)
        {
            $feedback_requests = substr_replace($feedback_requests, "", -1);
        }


        return view('requests.index',
            [
                'requests' => $requests,
                'countries' => $countries,
                'requesttypes' => RequestType::all(),
                'movingsizes' => MovingSize::all(),
                'rejectionreasons' => RejectionReason::all(),
                'on_hold_requests_per_users' => $on_hold_requests_per_users,
                'sp_requests_count' => $sp_requests_count,
                'feedback_requests' => $feedback_requests,
                'feedback_first' => $feedback_first
            ]);
    }

    public function premiumLeads()
    {
        $requests = Request::join("premium_leads", "re_id", "prle_re_id")->where("re_status", RequestStatus::OPEN)->where("re_request_type", 5)->get();

        $general_sources = [1, 2];

        //$requests['leads'] = $requests->whereIn("re_source", $general_sources)->where("re_double", 0)->where("re_spam", 0)->where("re_on_hold", 0);
        //$requests['affiliate'] = $requests->where("re_source", 2)->where("re_double", 0)->where("re_spam", 0)->where("re_on_hold", 0);
        //$requests['double'] = $requests->where("re_double", 1);
        //$requests['spam'] = $requests->where("re_spam", 1);

        $requests['make_appointment'] = $requests->whereIn("re_source", $general_sources)->where("prle_status", "=", 0);
        $requests['planned_for_survey'] = $requests->whereIn("re_source", $general_sources)->where("prle_status", "=", 1);
        $requests['ready_for_matching'] = $requests->whereIn("re_source", $general_sources)->where("prle_status", "=", 2);

        $requests['matched'] = Request::join("premium_leads", "re_id", "prle_re_id")->where("re_status", RequestStatus::MATCHED)->where("re_request_type", 5)->get();

        //dd($requests['make_appointment']);
        //$requests['on_hold'] = $requests->where("re_on_hold", 1);


        foreach (Country::all() as $country)
        {
            $countries[$country->co_code] = $country->co_en;
        }

        $sp_requests_count = KTRequestCustomerQuestion::selectRaw("COUNT(*) as `count`")->with(['request', 'customer', 'question'])
            ->where("ktrecuqu_sent", 0)
            ->where("ktrecuqu_hide", 0)
            ->where("ktrecuqu_rejection_status", 0)
            ->whereHas('request', function ($query) {
                $query->where('re_status', '=', 2);
            })
            ->orderBy("ktrecuqu_timestamp", "asc")
            ->first()->count;

        return view('requests.premium_leads',
            [
                'requests' => $requests,
                'countries' => $countries,
                'requesttypes' => RequestType::all(),
                'movingsizes' => MovingSize::all(),
                'sp_requests_count' => $sp_requests_count
            ]);
    }

    public function edit($request_id)
    {
        $request = Request::findOrFail($request_id);

        //Show different view for already matched or rejected lead
        if ($request->re_status == RequestStatus::MATCHED || $request->re_status == RequestStatus::REJECTED)
        {
            return view('requests.processed',
                [
                    'request_id' => $request->re_id
                ]);
        }

        $requestData = new RequestData();
        $system = new System();

        $requestData->checkDestinationType($request);
        //Get Service Provider questions

        //Get amount of matches left for this request
        $amount_matched = $requestData->requestMatchesLeft($request_id);

        //Get correct portal
        $portal = Portal::find($request->re_po_id);

        //Get name of SEA-website or affiliate website this request came from
        if ($request->re_source == 1)
        {
            $form = WebsiteForm::with("website")->where("wefo_id", $request->re_wefo_id)->first()->website->we_website;
        } else
        {
            $form = AffiliatePartnerForm::where("afpafo_id", $request->re_afpafo_id)->first()->afpafo_name;
        }

        $request_types = RequestType::all();
        $destination_type = DestinationType::name($request->re_destination_type);

        //Get correct volume calculator values
        $volumecalculator = $request->volumecalculator->voca_volume_calculator ? $system->volumeCalculator($request->volumecalculator->voca_volume_calculator) : "";

        foreach (Country::all() as $country)
        {
            $countries[$country->co_code] = $country->co_en;
        }

        $notices = $this->validateRequest($requestData, $request, $system);

        $regions_from = $system->countryRegions($request->re_co_code_from, $request->re_destination_type);
        $regions_to = $system->countryRegions($request->re_co_code_to, $request->re_destination_type);

        $suggested_phone_number_from = null;
        $suggested_phone_number_to = null;
        $isValid_from = false;
        $isValid_to = false;
        $isValid_ip = false;

        //Validation telephone number X country from
        $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();

        try
        {
            $swissNumberProto_from = $phoneUtil->parse($request->re_telephone1, $request->re_co_code_from);

            $isValid_from = $phoneUtil->isValidNumber($swissNumberProto_from);

            if ($isValid_from)
            {
                $suggested_phone_number_from = $phoneUtil->format($swissNumberProto_from, \libphonenumber\PhoneNumberFormat::E164);
            }

        } catch (\libphonenumber\NumberParseException $e)
        {
            Log::debug("Telephone API FROM is failed");
        }

        try
        {
            $swissNumberProto_to = $phoneUtil->parse($request->re_telephone1, $request->re_co_code_to);

            $isValid_to = $phoneUtil->isValidNumber($swissNumberProto_to);

            if ($isValid_to)
            {
                $suggested_phone_number_to = $phoneUtil->format($swissNumberProto_to, \libphonenumber\PhoneNumberFormat::E164);
            }

        } catch (\libphonenumber\NumberParseException $e)
        {
            Log::debug("Telephone API FROM is failed");
        }

        if (!$isValid_from && !$isValid_to && !empty($request->re_ip_address_country))
        {
            try
            {
                $swissNumberProto_ip = $phoneUtil->parse($request->re_telephone1, $request->re_ip_address_country);

                $isValid_ip = $phoneUtil->isValidNumber($swissNumberProto_ip);

                if ($isValid_ip)
                {
                    $suggested_phone_number_ip = $phoneUtil->format($swissNumberProto_ip, \libphonenumber\PhoneNumberFormat::E164);
                }

            } catch (\libphonenumber\NumberParseException $e)
            {
                Log::debug("Telephone API FROM is failed");
            }
        }

        $request_updates = RequestUpdate::whereReupReId($request->re_id)->orderBy("reup_id")->get();

        $request_update_remarks = "";
        $request_update_fields = [];

        $requestupdatefields = RequestUpdateFields::all();
        $remarks = false;
        foreach ($request_updates as $update)
        {
            if (empty($update->reup_key))
            {
                $request_update_remarks .= (($remarks == true) ? "<br />" : "") . "<b>" . $update->reup_timestamp . ":</b> " . $update->reup_value;
                $remarks = true;
            } else
            {
                $request_update_fields[$requestupdatefields[$update->reup_key]] = $update->reup_value;
            }
        }

        $rejected_before_reason = null;

        if (($request->re_pd_validate == 1 || $request->re_pd_validate == 2) && $request->re_rejection_reason != 0)
        {
            $rejected_before_reason = "<b>This request was originally rejected for: </b>" . RejectionReason::name($request->re_rejection_reason)[0];
        }

        return view('requests.edit',
            [
                'request' => $request,
                'countries' => $countries,
                'movingsizes' => $system->movingSizes($request->re_request_type),
                'roomsizes' => RoomAmounts::all(),
                'requestsource' => RequestSource::name($request->re_source),
                'requeststatuses' => RequestStatus::all(),
                'languages' => Language::where("la_iframe_only", 0)->get(),
                'device' => Device::name($request->re_device),
                'emptyyesno' => EmptyYesNo::all(),
                'byselfcompany' => MovingSelfCompany::all(),
                'requestresidences' => RequestResidence::all(),
                'called' => RequestInternalCalled::all(),
                'volumecalculator' => $volumecalculator,
                'amountmatched' => $amount_matched,
                'portal' => $portal->po_portal,
                'form' => $form,
                'requesttypes' => $request_types,
                'destinationtype' => $destination_type,
                'regionsfrom' => $regions_from,
                'regionsto' => $regions_to,
                'notices' => $notices,
                'users' => User::where("id", "!=", 0)->where("us_is_deleted", 0)->get(),
                'suggested_phone_number_from' => $suggested_phone_number_from,
                'suggested_phone_number_to' => $suggested_phone_number_to,
                'suggested_phone_number_ip' => $suggested_phone_number_ip,
                'isValid_to' => $isValid_to,
                'isValid_from' => $isValid_from,
                'isValid_ip' => $isValid_ip,

                //REQUEST UPDATES
                'request_update_fields' => $request_update_fields,
                'request_update_remarks' => $request_update_remarks,
                'rejected_before_reason' => $rejected_before_reason

            ]);
    }

    public function editPremiumLead($request_id)
    {
        $request = Request::join("premium_leads", "re_id", "prle_re_id")->findOrFail($request_id);

        //Show different view for already matched or rejected lead
        if ($request->re_status == RequestStatus::MATCHED || $request->re_status == RequestStatus::REJECTED)
        {
            return view('requests.processed',
                [
                    'request_id' => $request->re_id
                ]);
        }

        if ($request->re_source == 1)
        {
            $form = WebsiteForm::with("website")->where("wefo_id", $request->re_wefo_id)->first()->website->we_website;
        } else
        {
            $form = AffiliatePartnerForm::where("afpafo_id", $request->re_afpafo_id)->first()->afpafo_name;
        }

        $requestData = new RequestData();
        $system = new System();
        $screenshots_path = "./" . env("SHARED_FOLDER") . "uploads/premium_leads_screenshots/";

        //Get amount of matches left for this request
        $amount_matched = $requestData->requestMatchesLeft($request_id);

        //Get correct portal
        $destination_type = DestinationType::name($request->re_destination_type);

        //Get correct volume calculator values
        $volumecalculator = $request->volumecalculator->voca_volume_calculator ? $system->volumeCalculator($request->volumecalculator->voca_volume_calculator) : "";

        foreach (Country::all() as $country)
        {
            $countries[$country->co_code] = $country->co_en;
        }

        $notices = $this->validateRequest($requestData, $request, $system);

        $users = [];
        $users_query = User::where("id", "!=", 0)->where("us_is_deleted", 0)->get();
        foreach ($users_query as $us)
        {
            $users[$us->id] = $us->us_name;
        }

        return view('requests.premium_lead_edit',
            [
                'request' => $request,
                'countries' => $countries,
                'movingsizes' => $system->movingSizes($request->re_request_type),
                'requestsource' => RequestSource::name($request->re_source),
                'languages' => Language::where("la_iframe_only", 0)->get(),
                'device' => Device::name($request->re_device),
                'emptyyesno' => EmptyYesNo::all(),
                'byselfcompany' => MovingSelfCompany::all(),
                'requestresidences' => RequestResidence::all(),
                'called' => RequestInternalCalled::all(),
                'premiumleadstatuses' => PremiumLeadStatus::all(),
                'premiumleadresidencetypes' => PremiumLeadResidenceType::all(),
                'volumecalculator' => $volumecalculator,
                'amountmatched' => $amount_matched,
                'destinationtype' => $destination_type,
                'notices' => $notices,
                'users' => $users,
                'screenshots_path' => $screenshots_path,
                'surveyexecutors' => SurveyExecutor::all(),
                'form' => $form
            ]);
    }

    public function show($request_id)
    {
        $requestData = new RequestData();
        $system = new System();

        $request = Request::findOrFail($request_id);

        $matches = KTRequestCustomerPortal::leftJoin("customers", "cu_id", "ktrecupo_cu_id")
            ->where("ktrecupo_re_id", $request_id)
            ->get();

        foreach ($matches as $match)
        {
            $ids[] = $match->cu_id;
        }


        $capping_statistics = MatchStatistic::leftJoin("customers", "cu_id", "mast_cu_id")
            ->where("mast_re_id", $request_id);

        if ($ids)
        {
            $capping_statistics->whereNotIn("mast_cu_id", $ids);
        }

        $capping_statistics = $capping_statistics->get();


        //Get amount of matches left for this request
        $amount_matched = $requestData->requestMatchesLeft($request_id);

        //Get correct portal
        $portal = Portal::find($request->re_po_id);

        //Get name of SEA-website or affiliate website this request came from
        if ($request->re_source == 1)
        {
            $website_form = WebsiteForm::with("website")->where("wefo_id", $request->re_wefo_id)->first();
            $form = $website_form ? $website_form->website->we_website : "";
        } else
        {
            $affiliate_form = AffiliatePartnerForm::where("afpafo_id", $request->re_afpafo_id)->first();
            $form = $affiliate_form ? $affiliate_form->afpafo_name : "";
        }

        $request_types = RequestType::all();
        $destination_type = DestinationType::name($request->re_destination_type);

        //Get correct volume calculator values
        $volumecalculator = $request->volumecalculator->voca_volume_calculator ? $system->volumeCalculator($request->volumecalculator->voca_volume_calculator) : "";

        foreach (Country::all() as $country)
        {
            $countries[$country->co_code] = $country->co_en;
        }

        $language = Language::where("la_code", $request->re_la_code)->first()->la_language;

        $get_region_from = Region::where("reg_id", $request->re_reg_id_from)->first();
        $region_from = $get_region_from ? ($get_region_from->reg_name == "" ? $get_region_from->reg_parent : $get_region_from->reg_name) : "";

        $get_region_to = Region::where("reg_id", $request->re_reg_id_to)->first();
        $region_to = $get_region_to ? ($get_region_to->reg_name == "" ? $get_region_to->reg_parent : $get_region_to->reg_name) : "";

        $isValid_from = false;
        $isValid_to = false;
        $isValid_ip = false;

        //Validation telephone number X country from
        $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();

        try
        {
            $swissNumberProto_from = $phoneUtil->parse($request->re_telephone1, $request->re_co_code_from);

            $isValid_from = $phoneUtil->isValidNumber($swissNumberProto_from);

        } catch (\libphonenumber\NumberParseException $e)
        {
            Log::debug("Telephone API FROM is failed");
        }

        try
        {
            $swissNumberProto_to = $phoneUtil->parse($request->re_telephone1, $request->re_co_code_to);

            $isValid_to = $phoneUtil->isValidNumber($swissNumberProto_to);

        } catch (\libphonenumber\NumberParseException $e)
        {
            Log::debug("Telephone API FROM is failed");
        }

        if (!$isValid_from && !$isValid_to && !empty($request->re_ip_address_country))
        {
            try
            {
                $swissNumberProto_ip = $phoneUtil->parse($request->re_telephone1, $request->re_ip_address_country);

                $isValid_ip = $phoneUtil->isValidNumber($swissNumberProto_ip);

            } catch (\libphonenumber\NumberParseException $e)
            {
                Log::debug("Telephone API FROM is failed");
            }
        }
        $notices = $this->validateRequest($requestData, $request, $system);

        return view('requests.show',
            [
                'request' => $request,
                'countries' => $countries,
                'movingsizes' => MovingSize::all(),
                'requestsource' => RequestSource::name($request->re_source),
                'requeststatuses' => RequestStatus::all(),
                'language' => $language,
                'device' => Device::name($request->re_device),
                'emptyyesno' => EmptyYesNo::all(),
                'byselfcompany' => MovingSelfCompany::all(),
                'requestresidences' => RequestResidence::all(),
                'called' => RequestInternalCalled::all(),
                'volumecalculator' => $volumecalculator,
                'amountmatched' => $amount_matched,
                'portal' => $portal->po_portal,
                'form' => $form,
                'requesttypes' => $request_types,
                'destinationtype' => $destination_type,
                'region_from' => $region_from,
                'region_to' => $region_to,
                'notices' => $notices,
                'isValid_to' => $isValid_to,
                'isValid_from' => $isValid_from,
                'isValid_ip' => $isValid_ip,
                'capping_statistics' => $capping_statistics,
                'matches' => $matches
            ]);
    }

    public function showPremiumLead($request_id)
    {
        $requestData = new RequestData();
        $system = new System();

        $request = Request::leftJoin("premium_leads", "prle_re_id", "re_id")->where("re_id", $request_id)->first();

        //Get Service Provider questions

        //Get amount of matches left for this request
        $amount_matched = $requestData->requestMatchesLeft($request_id);

        //Get correct portal
        $portal = Portal::find($request->re_po_id);

        //Get name of SEA-website or affiliate website this request came from
        if ($request->re_source == 1)
        {
            $website_form = WebsiteForm::with("website")->where("wefo_id", $request->re_wefo_id)->first();
            $form = $website_form ? $website_form->website->we_website : "";
        } else
        {
            $affiliate_form = AffiliatePartnerForm::where("afpafo_id", $request->re_afpafo_id)->first();
            $form = $affiliate_form ? $affiliate_form->afpafo_name : "";
        }

        $request_types = RequestType::all();
        $destination_type = DestinationType::name($request->re_destination_type);

        //Get correct volume calculator values
        $volumecalculator = $request->volumecalculator->voca_volume_calculator ? $system->volumeCalculator($request->volumecalculator->voca_volume_calculator) : "";

        foreach (Country::all() as $country)
        {
            $countries[$country->co_code] = $country->co_en;
        }

        $language = Language::where("la_code", $request->re_la_code)->first()->la_language;

        $region_from = Region::where("reg_id", $request->re_reg_id_from)->first()->reg_name ?? "";
        $region_to = Region::where("reg_id", $request->re_reg_id_to)->first()->reg_name ?? "";

        $notices = $this->validateRequest($requestData, $request, $system);

        $premiumleadstatuses = PremiumLeadStatus::all();

        return view('requests.premium_lead_show',
            [
                'request' => $request,
                'countries' => $countries,
                'movingsizes' => MovingSize::all(),
                'requestsource' => RequestSource::name($request->re_source),
                'requeststatuses' => RequestStatus::all(),
                'language' => $language,
                'device' => Device::name($request->re_device),
                'emptyyesno' => EmptyYesNo::all(),
                'byselfcompany' => MovingSelfCompany::all(),
                'requestresidences' => RequestResidence::all(),
                'called' => RequestInternalCalled::all(),
                'volumecalculator' => $volumecalculator,
                'amountmatched' => $amount_matched,
                'portal' => $portal->po_portal,
                'form' => $form,
                'requesttypes' => $request_types,
                'destinationtype' => $destination_type,
                'region_from' => $region_from,
                'region_to' => $region_to,
                'notices' => $notices,
                'premiumleadstatuses' => $premiumleadstatuses
            ]);
    }

    public function raterequest($request_id, $to_be_rated)
    {
        $requestData = new RequestData();
        $system = new System();

        $to_be_rated = explode(",", urldecode($to_be_rated));

        if (($key = array_search($request_id, $to_be_rated)) !== false)
        {
            unset($to_be_rated[$key]);
        }

        $request = Request::findOrFail($request_id);

        //Get amount of matches left for this request
        $amount_matched = $requestData->requestMatchesLeft($request_id);

        //Get correct portal
        $portal = Portal::find($request->re_po_id);

        //Get name of SEA-website or affiliate website this request came from
        if ($request->re_source == 1)
        {
            $website_form = WebsiteForm::with("website")->where("wefo_id", $request->re_wefo_id)->first();
            $form = $website_form ? $website_form->website->we_website : "";
        } else
        {
            $affiliate_form = AffiliatePartnerForm::where("afpafo_id", $request->re_afpafo_id)->first();
            $form = $affiliate_form ? $affiliate_form->afpafo_name : "";
        }

        $request_types = RequestType::all();
        $destination_type = DestinationType::name($request->re_destination_type);

        //Get correct volume calculator values
        $volumecalculator = $request->volumecalculator->voca_volume_calculator ? $system->volumeCalculator($request->volumecalculator->voca_volume_calculator) : "";

        foreach (Country::all() as $country)
        {
            $countries[$country->co_code] = $country->co_en;
        }

        $language = Language::where("la_code", $request->re_la_code)->first()->la_language;

        $get_region_from = Region::where("reg_id", $request->re_reg_id_from)->first();
        $region_from = $get_region_from ? ($get_region_from->reg_name == "" ? $get_region_from->reg_parent : $get_region_from->reg_name) : "";

        $get_region_to = Region::where("reg_id", $request->re_reg_id_to)->first();
        $region_to = $get_region_to ? ($get_region_to->reg_name == "" ? $get_region_to->reg_parent : $get_region_to->reg_name) : "";

        $notices = $this->validateRequest($requestData, $request, $system);

        $isValid_from = false;
        $isValid_to = false;
        $isValid_ip = false;

        //Validation telephone number X country from
        $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();

        try
        {
            $swissNumberProto_from = $phoneUtil->parse($request->re_telephone1, $request->re_co_code_from);

            $isValid_from = $phoneUtil->isValidNumber($swissNumberProto_from);

        } catch (\libphonenumber\NumberParseException $e)
        {
            Log::debug("Telephone API FROM is failed");
        }

        try
        {
            $swissNumberProto_to = $phoneUtil->parse($request->re_telephone1, $request->re_co_code_to);

            $isValid_to = $phoneUtil->isValidNumber($swissNumberProto_to);

        } catch (\libphonenumber\NumberParseException $e)
        {
            Log::debug("Telephone API FROM is failed");
        }

        if (!$isValid_from && !$isValid_to && !empty($request->re_ip_address_country))
        {
            try
            {
                $swissNumberProto_ip = $phoneUtil->parse($request->re_telephone1, $request->re_ip_address_country);

                $isValid_ip = $phoneUtil->isValidNumber($swissNumberProto_ip);

            } catch (\libphonenumber\NumberParseException $e)
            {
                Log::debug("Telephone API FROM is failed");
            }
        }

        $request_changes = Revision::where("revisionable_type", "App\Models\Request")->where("revisionable_id", "=", $request_id)->get();

        $requestupdatefields = RequestUpdateFields::all();

        $request_changes_string = "";
        foreach ($request_changes as $change)
        {
            if (!empty($change->old_value)
                && $change->key != "re_status"
                && $change->key != "re_pd_validate"
                && $change->key != "re_rejection_timestamp"
                && $change->key != "re_volume_calculator"
                && $change->key != "re_recover_reason"
                && $change->key != "re_rejection_timestamp"
                && $change->key != "re_rejection_reason"
                && $change->key != "re_on_hold_by"
                && $change->key != "re_internal_remarks"
            )
            {
                if ($change->key == RequestUpdateFields::REGION_FROM || $change->key == RequestUpdateFields::REGION_TO)
                {
                    $old_region = Region::where("reg_id", $change->old_value)->first();
                    $new_region = Region::where("reg_id", $change->new_value)->first();

                    if (!empty($old_region->reg_name))
                    {
                        $old_value = $old_region->reg_name;
                    } else
                    {
                        $old_value = $old_region->reg_parent;
                    }

                    if (!empty($new_region->reg_name))
                    {
                        $new_value = $new_region->reg_name;
                    } else
                    {
                        $new_value = $new_region->reg_parent;
                    }
                } elseif ($change->key == RequestUpdateFields::MOVING_SIZE)
                {
                    $moving_sizes = MovingSize::all();
                    $old_value = $moving_sizes[$change->old_value];
                    $new_value = $moving_sizes[$change->new_value];

                } elseif ($change->key == RequestUpdateFields::REQUEST_TYPE)
                {
                    $request_types = RequestType::all();
                    $old_value = $request_types[$change->old_value];
                    $new_value = $request_types[$change->new_value];

                } else
                {
                    $old_value = $change->old_value;
                    $new_value = $change->new_value;
                }

                if ($new_value == "")
                {
                    $new_value = "[EMPTY]";
                }
                $request_changes_string .= "<b><i>" . $requestupdatefields[$change->key] . "</i> </b>from " . $old_value . " to " . $new_value . "<br />";
            }
        }

        return view('requests.raterequest',
            [
                'request' => $request,
                'countries' => $countries,
                'movingsizes' => MovingSize::all(),
                'requestsource' => RequestSource::name($request->re_source),
                'requeststatuses' => RequestStatus::all(),
                'language' => $language,
                'device' => Device::name($request->re_device),
                'emptyyesno' => EmptyYesNo::all(),
                'byselfcompany' => MovingSelfCompany::all(),
                'requestresidences' => RequestResidence::all(),
                'rejectionreasons' => RejectionReason::all(),
                'called' => RequestInternalCalled::all(),
                'volumecalculator' => $volumecalculator,
                'amountmatched' => $amount_matched,
                'portal' => $portal->po_portal,
                'form' => $form,
                'requesttypes' => $request_types,
                'destinationtype' => $destination_type,
                'region_from' => $region_from,
                'region_to' => $region_to,
                'notices' => $notices,
                'toberated' => $to_be_rated,
                'ratings' => MatchMistakeType::all(),
                'request_changes' => $request_changes,
                'request_changes_string' => $request_changes_string,
                'isValid_to' => $isValid_to,
                'isValid_from' => $isValid_from,
                'isValid_ip' => $isValid_ip,
            ]);
    }

    public function submitrate(\Illuminate\Http\Request $webrequest)
    {
        $request_id = $webrequest->request_id;
        $to_be_rated = $webrequest->list;

        //Save this rating
        $request = Request::find($request_id);
        $request->re_match_rating = $webrequest->match_rating;
        $request->re_match_rating_remarks = $webrequest->rating_motivation;
        if ($webrequest->match_rating != 0 && $webrequest->match_rating != 1)
        {
            $request->re_match_rating_read = 0;
            $request->re_show_this_feedback = 0;
        }

        $request->save();

        if ($to_be_rated == -1)
        {
            return redirect("/requests/rate");
        }

        //Get ID and next to be rated
        $tobearray = explode(",", $to_be_rated);

        if (($key = array_search($request_id, $tobearray)) !== false)
        {
            unset($to_be_rated[$key]);
        }

        $next_id = $tobearray[0];
        unset($tobearray[0]);

        if (empty($tobearray))
        {
            $tobearray = "-1";
        } else
        {
            $tobearray = implode(",", $tobearray);
        }

        return redirect("/requests/" . $next_id . "/rate/" . $tobearray);
    }

    public function update(\Illuminate\Http\Request $webrequest, $request_id)
    {
        $request = Request::find($request_id);

        if ($request->re_status == 1 || $request->re_status == 2)
        {
            //Redirect to /edit to see that the requests is already matches or rejected
            return redirect('requests/' . $request_id . '/edit');
        }

        $re_type_before = $request->re_request_type;
        Log::debug($webrequest);

        //Save general block
        $request->re_spam = Arr::exists($webrequest, 'request_spam') ? 1 : 0;
        $request->re_double = Arr::exists($webrequest, 'request_double') ? 1 : 0;
        $request->re_on_hold = Arr::exists($webrequest, 'request_on_hold') ? 1 : 0;
        $request->re_on_hold_by = (Arr::exists($webrequest, 'request_on_hold') && $webrequest->on_hold_by > 0 ? $webrequest->on_hold_by : null);
        $request->re_la_code = $webrequest->language;

        //Premium lead or normal lead?
        //$request->re_type = Arr::exists($webrequest, 'request_type_premium') ? 2 : 1;

        if (Arr::exists($webrequest, 'match'))
        {
            if (empty($webrequest->region_from))
            {
                return redirect()->back()->withErrors(['errors' => "Please select the origin region."]);
            }

            if (empty($webrequest->region_to))
            {
                return redirect()->back()->withErrors(['errors' => "Please select the destination region."]);
            }
        }

        if ($webrequest->country_to == $webrequest->country_from)
        {
            $parent_from = Region::find($webrequest->region_from)->reg_parent;
            $parent_to = Region::find($webrequest->region_to)->reg_parent;

            if ($parent_from == $parent_to)
            {
                $request->re_nat_type = NatPaymentType::NAT_LOCAL;
            } else
            {
                $request->re_nat_type = NatPaymentType::NAT_LONG_DISTANCE;
            }
        }

        //Save moving details
        $request->re_request_type = $webrequest->request_type;
        $request->re_moving_size = $webrequest->moving_size;
        $request->re_moving_date = $webrequest->moving_date;
        $request->re_volume_m3 = $webrequest->volume_m3;
        $request->re_volume_ft3 = $webrequest->volume_ft3;
        $request->re_storage = $webrequest->storage;
        $request->re_packing = $webrequest->packing;
        $request->re_assembly = $webrequest->assembly;
        $request->re_business = Arr::exists($webrequest, 'business') ? 1 : 0;

        //Save moving from
        $request->re_street_from = $webrequest->street_from;
        $request->re_zipcode_from = $webrequest->zipcode_from;
        $request->re_city_from = $webrequest->city_from;
        $request->re_co_code_from = $webrequest->country_from;
        $request->re_reg_id_from = $webrequest->region_from;
        $request->re_residence_from = $webrequest->select_residence_from;
        $request->re_google_maps_from = $webrequest->google_maps_link_from_hidden;

        //Save moving to
        $request->re_street_to = $webrequest->street_to;
        $request->re_zipcode_to = $webrequest->zipcode_to;
        $request->re_city_to = $webrequest->city_to;
        $request->re_co_code_to = $webrequest->country_to;
        $request->re_reg_id_to = $webrequest->region_to;
        $request->re_residence_to = $webrequest->select_residence_to;
        $request->re_google_maps_to = $webrequest->google_maps_link_to_hidden;

        //Save contact details
        $request->re_company_name = $webrequest->company_name;
        $request->re_full_name = $webrequest->full_name;
        $request->re_telephone1 = $webrequest->telephone1;
        $request->re_telephone2 = $webrequest->telephone2;
        $request->re_email = $webrequest->email;

        //Save remarks
        $request->re_remarks = $webrequest->remark;

        //Save internal information
        $request->re_internal_called = $webrequest->called;
        $request->re_internal_called_timestamp = $webrequest->called_on;
        $request->re_internal_remarks = $webrequest->internal_remarks;

        //If request type is changing from normal lead to a Premium lead
        if ($re_type_before != 5 && $request->re_request_type == 5)
        {
            //Check if premium lead row already exists or not
            $prem_lead = PremiumLead::where("prle_re_id", "=", $request->re_id)->first();

            if (count($prem_lead) == 0)
            {
                //If not exist, then create the premium lead row in Premium_leads table
                $premium_lead = new PremiumLead();
                $premium_lead->prle_re_id = $request->re_id;
                $premium_lead->save();
            }
        }

        if ($request->re_request_type == 5)
        {
            $start_url = "premium_leads";
        } else
        {
            $start_url = "requests";
        }

        if (Arr::exists($webrequest, 'match'))
        {
            //Update database
            $request->save();

            return redirect($start_url . '/' . $request_id . '/match');
        } elseif (Arr::exists($webrequest, 'reject'))
        {
            //Update database
            $request->save();

            return redirect($start_url . '/' . $request_id . '/reject');
        } elseif (Arr::exists($webrequest, 'save'))
        {
            //Update database
            $request->save();

            if ($start_url == "premium_leads")
            {
                return redirect($start_url . '/' . $request_id . "/edit")->with('message', 'Request successfully updated!');
            } else
            {
                return redirect($start_url . '/')->with('message', 'Request successfully updated!');
            }
        }

    }

    public function updatePremiumLead(\Illuminate\Http\Request $webrequest, $request_id)
    {
        $request = Request::find($request_id);
        $premium_lead = PremiumLead::where("prle_re_id", "=", $request_id)->first();

        $start_prle_status = $premium_lead->prle_status;

        if ($webrequest->hasFile('uploaded_excel_file'))
        {
            if ($_FILES['uploaded_excel_file']['size'] < 1)
            {
                return redirect()->back()->withErrors(['uploaded_excel_file' => 'The document has not been saved, because the file is empty.']);
            } elseif ($_FILES['uploaded_excel_file']['size'] > 10485760)
            {
                return redirect()->back()->withErrors(['uploaded_excel_file' => 'The document has not been saved, because the file is bigger than 10MB.']);
            }

            // Get filename with extension
            $filenameWithExt = $webrequest->file('uploaded_excel_file')->getClientOriginalName();

            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

            // Get just ext
            $extension = $webrequest->file('uploaded_excel_file')->getClientOriginalExtension();

            $filename_excellist = "Premium Lead - " . $request->re_full_name . " - " . System::generateRandomString(10) . '.' . $extension;

            // Store the file
            $path = env("SHARED_FOLDER") . "/uploads/excel_itemlist/" . $filename_excellist;

            $webrequest->file('uploaded_excel_file')->move(env("SHARED_FOLDER") . "/uploads/excel_itemlist/", $filename_excellist);

            $premium_lead->prle_list_of_all_items = $filename_excellist;
        }

        if ($webrequest->hasFile('uploaded_screenshots'))
        {
            $screenies = $webrequest->file('uploaded_screenshots');

            $screenshots = [];

            if (!empty($premium_lead->prle_screenshots))
            {
                $screenshots = unserialize($premium_lead->prle_screenshots);
            }

            foreach ($screenies as $screen)
            {
                // Get filename with extension
                $filenameWithExt = $screen->getClientOriginalName();

                // Get just filename
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

                // Get just ext
                $extension = $screen->getClientOriginalExtension();

                $filename_screenshot = md5(uniqid(time(), true) . uniqid($filename, true)) . '.' . $extension;

                // Store the file
                //$screen->move(public_path()."/premium_leads_screenshots/".$request_id."/", $filename_screenshot);
                $screen->move(env("SHARED_FOLDER") . "/uploads/premium_leads_screenshots/" . $request_id . "/", $filename_screenshot);


                $screenshots[] = $filename_screenshot;
            }

            $premium_lead->prle_screenshots = serialize($screenshots);
        }

        //Save Premium lead info block
        $request->re_double = Arr::exists($webrequest, 'request_double') ? 1 : 0;
        $premium_lead->prle_status = $webrequest->premiumlead_status;
        $premium_lead->prle_planned_appointment_timestamp = $webrequest->planned_date_appointment;
        $premium_lead->prle_survey_executor = $webrequest->survey_executor;

        if ($start_prle_status == 0 && $webrequest->premiumlead_status != 0)
        {
            $premium_lead->prle_planned_timestamp = date("Y-m-d H:i:s");
            $premium_lead->prle_planned_by = Auth::id();

            if ($premium_lead->prle_survey_executor == 1 && $premium_lead->prle_appointment_email_sent == 0)
            {
                $system = new System();
                $dt = new DateTime($premium_lead->prle_planned_appointment_timestamp);

                $date = $dt->format("Y-m-d");
                $time = $dt->format('H:i');

                $fields = [
                    "name" => $request->re_full_name,
                    "date" => $date,
                    "time" => $time,
                    "country" => $request->re_co_code_to,
                    "email_to" => $request->re_email,
                    "request" => $system->databaseToArray($request)
                ];

                $mail = new Mail();

                $mail->send("premium_lead_appointment_confirmation", $request->re_la_code, $fields);

                $premium_lead->prle_appointment_email_sent = 1;
            }
        }

        //Save Volume of move block
        $premium_lead->prle_special_items = $webrequest->special_items;
        $request->re_volume_m3 = $webrequest->volume_m3;
        $request->re_volume_ft3 = $webrequest->volume_ft3;

        //Save Loading Address
        $request->re_street_from = $webrequest->street_from;
        $request->re_zipcode_from = $webrequest->zipcode_from;
        $request->re_city_from = $webrequest->city_from;
        $request->re_co_code_from = $webrequest->country_from;
        $request->re_reg_id_from = $webrequest->region_from;
        $request->re_residence_from = $webrequest->floor_level_from;
        $premium_lead->prle_residence_type_from = $webrequest->type_of_residence_from;
        $premium_lead->prle_inside_elevator_from = $webrequest->inside_elevator_from;
        $premium_lead->prle_obstacles_for_moving_van_from = $webrequest->obstacles_for_moving_van_from;
        $premium_lead->prle_parking_restrictions_and_permits_from = $webrequest->parking_restrictions_and_permits_from;
        $premium_lead->prle_walking_distance_to_house_from = $webrequest->walking_distance_to_house_from;

        //Save Destination address
        $request->re_street_to = $webrequest->street_to;
        $request->re_zipcode_to = $webrequest->zipcode_to;
        $request->re_city_to = $webrequest->city_to;
        $request->re_co_code_to = $webrequest->country_to;
        $request->re_reg_id_to = $webrequest->region_to;
        $request->re_residence_to = $webrequest->floor_level_to;
        $premium_lead->prle_residence_type_to = $webrequest->type_of_residence_to;
        $premium_lead->prle_inside_elevator_to = $webrequest->inside_elevator_to;
        $premium_lead->prle_obstacles_for_moving_van_to = $webrequest->obstacles_for_moving_van_to;
        $premium_lead->prle_parking_restrictions_and_permits_to = $webrequest->parking_restrictions_and_permits_to;
        $premium_lead->prle_walking_distance_to_house_to = $webrequest->walking_distance_to_house_to;

        //Contact details block
        $request->re_company_name = $webrequest->company_name;
        $request->re_full_name = $webrequest->full_name;
        $request->re_telephone1 = $webrequest->telephone1;
        $request->re_telephone2 = $webrequest->telephone2;
        $request->re_email = $webrequest->email;
        $request->re_la_code = $webrequest->language;

        //Save Moving details block
        $request->re_moving_date = $webrequest->moving_date;
        $premium_lead->prle_extra_info_moving_date = $webrequest->extra_info_moving_date;
        $premium_lead->prle_packing_by_mover = $webrequest->packing_by_mover;
        $premium_lead->prle_packing_by_mover_remarks = $webrequest->packing_by_mover_remarks;
        $premium_lead->prle_unpacking_by_mover = $webrequest->unpacking_by_mover;
        $premium_lead->prle_unpacking_by_mover_remarks = $webrequest->unpacking_by_mover_remarks;
        $premium_lead->prle_disassembling_by_mover = $webrequest->disassembling_by_mover;
        $premium_lead->prle_disassembling_by_mover_remarks = $webrequest->disassembling_by_mover_remarks;
        $premium_lead->prle_assembling_by_mover = $webrequest->assembling_by_mover;
        $premium_lead->prle_assembling_by_mover_remarks = $webrequest->assembling_by_mover_remarks;
        $premium_lead->prle_storage = $webrequest->storage;
        $premium_lead->prle_storage_remarks = $webrequest->storage_remarks;
        $premium_lead->prle_handyman_service = $webrequest->handyman_service;

        //Save Insurance block
        $premium_lead->prle_insurance = $webrequest->insurance;
        $premium_lead->prle_insurance_remarks = $webrequest->insurance_remarks;
        $premium_lead->prle_insurance_amount = $webrequest->insurance_amount;

        //Save Remarks - Additional information block
        $request->re_remarks = $webrequest->remarks;

        //Save internal information
        $request->re_internal_called = $webrequest->called;
        $request->re_internal_called_timestamp = $webrequest->called_on;
        $request->re_internal_remarks = $webrequest->internal_remarks;

        //Premium lead or normal lead?

        if (Arr::exists($webrequest, 'match'))
        {
            if (empty($webrequest->region_from))
            {
                return redirect()->back()->withErrors(['errors' => "Please select the origin region."]);
            }

            if (empty($webrequest->region_to))
            {
                return redirect()->back()->withErrors(['errors' => "Please select the destination region."]);
            }
        }

        //Update database
        $request->save();
        $premium_lead->save();

        if (Arr::exists($webrequest, 'match'))
        {
            return redirect('premium_leads/' . $request_id . '/match');
        } elseif (Arr::exists($webrequest, 'reject'))
        {
            return redirect('premium_leads/' . $request_id . '/reject');
        } elseif (Arr::exists($webrequest, 'save'))
        {
            return redirect('premium_leads/')->with('message', 'Request successfully updated!');
        }

    }

    public function generatePDF(Request $webrequest, $request_id)
    {
        $system = new System();
        $request = Request::join("premium_leads", "re_id", "prle_re_id")->findOrFail($request_id);

        $destination_type = DestinationType::name($request->re_destination_type);
        foreach (Country::all() as $country)
        {
            $countries[$country->co_code] = $country->co_en;
        }
        $users = [];
        $users_query = User::where("id", "!=", 0)->where("us_is_deleted", 0)->get();
        foreach ($users_query as $us)
        {
            $users[$us->id] = $us->us_name;
        }

        $pdf = PDF::loadView('requests.premium_lead_pdf', [
            'request' => $request,
            'languages' => Language::where("la_iframe_only", 0)->get(),
            'requestresidences' => RequestResidence::all(),
            'called' => RequestInternalCalled::all(),
            'countries' => $countries,
            'destinationtype' => $destination_type,
            'emptyyesno' => EmptyYesNo::all(),
            'byselfcompany' => MovingSelfCompany::all(),
            'movingsizes' => $system->movingSizes($request->re_request_type),
            'premiumleadstatuses' => PremiumLeadStatus::all(),
            'premiumleadresidencetypes' => PremiumLeadResidenceType::all(),
            'users' => $users,
        ]);

        return $pdf->download('premium_lead_' . $request->re_id . '.pdf');
    }

    /*public function viewPDF(Request $webrequest, $request_id){

        //$items = DB::table("items")->get();
        $request = Request::join("premium_leads", "re_id", "prle_re_id")->findOrFail($request_id);
        view()->share('request',$request);
        //dd($webrequest);

        if($webrequest->has('download')){
            $pdf = PDF::loadView('requests.pdfview');
            return $pdf->download('pdfview.pdf');
        }


        return view('requests.pdfview');
    }*/

    public function downloadFile($re_id)
    {

        $premium_lead = PremiumLead::where("prle_re_id", "=", $re_id)->first();

        //Get filepath
        $file = env("SHARED_FOLDER") . "/uploads/excel_itemlist/" . $premium_lead->prle_list_of_all_items;

        if (file_exists($file))
        {
            //Download the file
            header('Content-Type: application/octet-stream');
            header("Content-Transfer-Encoding: Binary");
            header("Content-disposition: attachment; filename=\"" . basename($file) . "\"");
            readfile($file);
        }
    }

    public function deleteFile(\Illuminate\Http\Request $request)
    {
        $premium_lead = PremiumLead::where("prle_re_id", "=", $request->re_id)->first();

        //Get filepath
        $file = env("SHARED_FOLDER") . "/uploads/excel_itemlist/" . $premium_lead->prle_list_of_all_items;

        if (file_exists($file))
        {
            unlink($file);
        }

        $premium_lead->prle_list_of_all_items = "";
        $premium_lead->save();
    }

    public function deleteScreenshot(\Illuminate\Http\Request $request)
    {
        $premium_lead = PremiumLead::where("prle_re_id", "=", $request->re_id)->first();

        $screenshots_array = unserialize($premium_lead->prle_screenshots);

        if (($key = array_search($request->screenshot_name, $screenshots_array)) !== false)
        {
            unset($screenshots_array[$key]);
        }

        if (empty($screenshots_array))
        {
            $screenshots_array = "";
        }

        $file = env("SHARED_FOLDER") . "/uploads/premium_leads_screenshots/" . $request->re_id . "/" . $request->screenshot_name;

        if (file_exists($file))
        {
            unlink($file);
        }

        $premium_lead->prle_screenshots = (($screenshots_array == "") ? $screenshots_array : serialize($screenshots_array));
        $premium_lead->save();
    }

    public function reject($request_id)
    {
        $requestData = new RequestData();
        $system = new System();
        $request = Request::findOrFail($request_id);

        //Show different view for already matched or rejected lead
        if ($request->re_status == RequestStatus::MATCHED || $request->re_status == RequestStatus::REJECTED)
        {
            return view('requests.processed',
                [
                    'request_id' => $request->re_id
                ]);
        }

        //Get correct portal
        $portal = Portal::find($request->re_po_id);

        //Get name of SEA-website or affiliate website this request came from
        if ($request->re_source == 1)
        {
            $form = WebsiteForm::with("website")->where("wefo_id", $request->re_wefo_id)->first()->website->we_website;
        } else
        {
            $form = AffiliatePartnerForm::where("afpafo_id", $request->re_afpafo_id)->first()->afpafo_name;
        }

        $request_type = RequestType::name($request->re_request_type);

        $mail_reasons = $this->getMailReasons();

        foreach (RejectionReason::all() as $rejection_id => $rejection_reason)
        {
            if ($rejection_reason[1] == 0) continue;
            if (in_array($rejection_id, $mail_reasons))
            {
                if ($request->re_source == 2)
                {
                    if ($rejection_id !== 14 && $rejection_id !== 15)
                    {
                        $rejection_reasons[$rejection_id . ";valid"] = "[✔] " . $rejection_reason[0];
                    }
                } else
                {
                    $rejection_reasons[$rejection_id . ";valid"] = "[✔] " . $rejection_reason[0];
                }

            } else
            {
                if ($request->re_source == 2)
                {
                    if ($rejection_id !== 0 && $rejection_id !== 11 && $rejection_id !== 14 && $rejection_id !== 15)
                    {
                        $rejection_reasons[$rejection_id . ";unvalid"] = "[✖] " . $rejection_reason[0];
                    }
                } else
                {
                    if ($rejection_id !== 0 && $rejection_id !== 10 && $rejection_id !== 12 && $rejection_id !== 13)
                    {
                        $rejection_reasons[$rejection_id . ";unvalid"] = "[✖] " . $rejection_reason[0];
                    }
                }
            }
        }
        unset($rejection_reasons["9" . ";valid"]);

        $notices = $this->validateRequest($requestData, $request, $system);

        return view('requests.reject',
            [
                'request' => $request,
                'requestsource' => RequestSource::name($request->re_source),
                'requeststatuses' => RequestStatus::all(),
                'portal' => $portal->po_portal,
                'form' => $form,
                'requesttype' => $request_type,
                'rejectionreasons' => $rejection_reasons,
                'yesno' => YesNo::all(),
                'notices' => $notices
            ]);
    }

    public function rejectlead(\Illuminate\Http\Request $webrequest, $request_id)
    {
        $mail = new Mail();
        $system = new System();

        $request = Request::findOrFail($request_id);
        $pd_url = Website::where("we_sirelo_co_code",  $request->re_co_code_from)->first()->we_personal_dashboard_url;

        if ($request->re_pd_validate == 1)
        {
            $request->re_pd_validate = 2;
        }

        if (isset($webrequest->system_user))
        {
            $user_id = $webrequest->system_user;
        } else
        {
            $user_id = Auth::id();
        }

        $request->re_status = 2;
        $request->re_rejection_reason = $webrequest->rejection_reason;
        $request->re_rejection_us_id = $user_id;
        $request->re_rejection_timestamp = Carbon::now()->toDateTimeString();
        $request->re_on_hold = 0;
        $request->save();

        Log::debug($webrequest);

        if (isset($webrequest->send_email) && $webrequest->send_email == 1 && in_array($webrequest->rejection_reason, $this->getMailReasons()))
        {
            if (empty($pd_url)) {
                $pd_url = Website::where("we_sirelo_url", "https://sirelo.org")->first()->we_personal_dashboard_url;
            }

            $pd_link = $pd_url. "?id=" . $request->re_token . "&utm_source=personal-dashboard&utm_medium=email";

            $excludes = ['RU'];

            if (in_array($request->re_la_code, $excludes))
            {

                $request->re_la_code = "EN";
            }

            $fields = [
                "request" => $system->databaseToArray($request),
                "rejection_reason" => $webrequest->rejection_reason,
                "pd_link" => $pd_link
            ];

            if (in_array($webrequest->rejection_reason, [12, 13, 14, 15, 22]))
            {
                $fields['rejection_reason'] = 6;
            }

            $mail->send("request_reject", $request->re_la_code, $fields);

        }

        $affiliate_reasons = DataIndex::rejectionReasonToAffiliateRejectionReason();

        DB::table('affiliate_partner_requests')
            ->where('afpare_re_id', $request_id)
            ->update(
                [
                    'afpare_status' => 2,
                    'afpare_rejection_reason' => $affiliate_reasons[$webrequest->rejection_reason]
                ]
            );

        if ($request->re_request_type == 5)
        {
            $start_url = "/premium_leads";
        } else
        {
            $start_url = "/requests";
        }

        return redirect($start_url)->with('message', 'Request successfully rejected!');
    }

    public function automaticMatch($request_id)
    {
        $system = new System();
        $requestData = new RequestData();

        $request = Request::findOrFail($request_id);
        $requestData->checkDestinationType($request);

        //Before automatic match, fix region to and phone number (currently the only automatic changes)
        $this->fixRegionTo($request);
        $this->fixTelephoneNumber($request);

        Log::debug("Automatically matching: " . $request_id);
        Log::debug($request);

        $moving_date_diff = $system->dateDifference(date("Y-m-d"), date("Y-m-d", strtotime($request->re_moving_date)));
        $minimum_days = $requestData->minimumMovingDate($request->re_request_type, $request->re_destination_type);

        if ($request->re_double == 1)
        {
            return "The request is marked as <strong>DOUBLE</strong>, please change it.";
        }

        if ($request->re_spam == 1)
        {
            return "The request is marked as <strong>SPAM</strong>, please change it.";
        }

        if ($moving_date_diff < ($request->re_status == 1 ? $minimum_days - 1 : $minimum_days))
        {
            return "The moving date is within <strong>" . $moving_date_diff . "</strong> day(s), which should be at least " . $minimum_days . " days.";
        }

        if ($moving_date_diff < 0)
        {
            return "The moving date is today or in the past.";
        }

        if ($moving_date_diff > 270)
        {
            return "The moving date is in <strong>" . $moving_date_diff . "</strong> days, this should be less then 270 days.";
        }

        if (empty($request->re_co_code_from))
        {
            return "The origin country hasn't been selected, please change it.";
        }

        if (empty($request->re_co_code_to))
        {
            return "The destination country hasn't been selected, please change it.";
        }

        if (empty($request->re_reg_id_from))
        {
            return "Please select the origin region.";
        }

        if (empty($request->re_reg_id_to))
        {
            return "Please select the destination region.";
        }

        if (($request->re_co_code_from == $request->re_co_code_to) && $request->re_destination_type == 1)
        {
            return "The destination type is wrong.";
        }


        $rd = new RequestData();

        Log::debug("Automatically matching to:");
        if (!$request->re_exclusive_match)
        {
            $matching_customers = $rd->matchLeadBetter($request);
        } else
        {
            $matching_customers = $rd->getMatchedTo($request_id);
        }

        Log::debug($matching_customers);

        $match_ids = [];

        foreach ($matching_customers['customers'] as $customer)
        {
            if ($customer['checked'])
            {
                $match_ids[$customer['ktcupo_id']] = $customer['ktcupo_id'];
            }
        }

        Log::debug($match_ids);

        $myRequest = new \Illuminate\Http\Request();
        $myRequest->setMethod('POST');

        if (!empty($match_ids))
        {

            $myRequest->request->add(['customers' => $match_ids]);
            $myRequest->request->add(['matching_customers' => [json_encode($matching_customers)]]);
            $myRequest->request->add(['system_user' => 4700]);

            Log::debug("Send the lead to actual match");
            Log::debug($request_id);
            Log::debug($myRequest);

            self::matchlead($myRequest, $request_id);

        } else
        {
            $myRequest->request->add(['send_email' => YesNo::YES]);
            $myRequest->request->add(['system_user' => 4700]);

            if ($request->re_request_type == RequestType::BAGGAGE)
            {
                $myRequest->request->add(['rejection_reason' => RejectionReason::NO_REGION_OCCUPANCY_BAGGAGE]);
            } elseif ($request->re_request_type == RequestType::TRANSPORT)
            {
                $myRequest->request->add(['rejection_reason' => RejectionReason::ONLY_TRANSPORT]);
            } else
            {
                $myRequest->request->add(['rejection_reason' => RejectionReason::NO_REGION_OCCUPANCY_MOVE]);
            }
            $rqc = new RequestsController();
            $rqc->rejectLead($myRequest, $request->re_id);
        }

    }

    public function newmatchBetter($request_id)
    {

        $system = new System();
        $requestData = new RequestData();

        $request = Request::findOrFail($request_id);
        $requestData->checkDestinationType($request);

        Log::debug("Request info in /match");
        Log::debug($request);

        $moving_date_diff = $system->dateDifference(date("Y-m-d"), date("Y-m-d", strtotime($request->re_moving_date)));
        $minimum_days = $requestData->minimumMovingDate($request->re_request_type, $request->re_destination_type);

        if ($request->re_double == 1)
        {
            return redirect()->back()->withErrors(['errors' => "The request is marked as <strong>DOUBLE</strong>, please change it."]);
        }

        if ($request->re_spam == 1)
        {
            return redirect()->back()->withErrors(['errors' => "The request is marked as <strong>SPAM</strong>, please change it."]);
        }

        if ($moving_date_diff < ($request->re_status == 1 ? $minimum_days - 1 : $minimum_days))
        {
            return redirect()->back()->withErrors(['errors' => "The moving date is within <strong>" . $moving_date_diff . "</strong> day(s), which should be at least " . $minimum_days . " days."]);
        }

        if ($moving_date_diff < 0)
        {
            return redirect()->back()->withErrors(['errors' => "The moving date is today or in the past."]);
        }

        if ($moving_date_diff > 270)
        {
            return redirect()->back()->withErrors(['errors' => "The moving date is in <strong>" . $moving_date_diff . "</strong> days, this should be less then 270 days."]);
        }

        if (empty($request->re_co_code_from))
        {
            return redirect()->back()->withErrors(['errors' => "The origin country hasn't been selected, please change it."]);
        }

        if (empty($request->re_co_code_to))
        {
            return redirect()->back()->withErrors(['errors' => "The destination country hasn't been selected, please change it."]);
        }

        if (empty($request->re_reg_id_from))
        {
            return redirect()->back()->withErrors(['errors' => "Please select the origin region."]);
        }

        if (empty($request->re_reg_id_to))
        {
            return redirect()->back()->withErrors(['errors' => "Please select the destination region."]);
        }

        if (($request->re_co_code_from == $request->re_co_code_to) && $request->re_destination_type == 1)
        {
            //This is a national lead for a country where we don't have a national portal, return an empty match page.
            return view('requests.match',
                [
                    'request' => $request,
                    'requestsource' => RequestSource::name($request->re_source),
                    'portal' => "",
                    'form' => "",
                    'matchingcustomers' => [],
                    'sirelo_customer' => [],
                    'portalnames' => [],
                    'amountofmatches' => 0
                ]);
        }

        $amount = 0;

        $sirelo_customer = [];

        if ($request->re_sirelo_customer)
        {
            $sirelo_customer = Customer::find($request->re_sirelo_customer);
            $amount++;
        }

        //Show different view for already matched or rejected lead
        /* if($request->re_status == RequestStatus::MATCHED || $request->re_status == RequestStatus::REJECTED)
         {
             return view('requests.processed',
                 [
                     'request_id' => $request->re_id
                 ]);
         }*/

        $rd = new RequestData();
        $portal_names = [];

        Log::debug("Matching to:");
        if (!$request->re_exclusive_match)
        {
            $matching_customers = $rd->matchLeadBetter($request);

            foreach ($matching_customers as $portal_id => $customer)
            {
                $portal_names[$portal_id] = Portal::find($portal_id)->po_portal;
            }
        } else
        {
            $matching_customers = $rd->getMatchedTo($request_id);
        }

        Log::debug($matching_customers);

        if ($matching_customers)
        {
            $amount += count($matching_customers["customers"]);
        }

        //Get correct portal
        $portal = Portal::find($request->re_po_id);

        //Get name of SEA-website or affiliate website this request came from
        if ($request->re_source == 1)
        {
            $form = WebsiteForm::with("website")->where("wefo_id", $request->re_wefo_id)->first()->website->we_website;
        } else
        {
            $form = AffiliatePartnerForm::where("afpafo_id", $request->re_afpafo_id)->first()->afpafo_name;
        }

        $rejection_reasons = [];

        foreach (RejectionReason::all() as $id => $rejection_reason)
        {
            if ($rejection_reason[1] == 0)
            {
                continue;
            }
            $rejection_reasons[$id] = $rejection_reason[0];
        }

        return view('requests.newmatchbetter',
            [
                'request' => $request,
                'requestsource' => RequestSource::name($request->re_source),
                'portal' => $portal->po_portal,
                'form' => $form,
                'matchingcustomers' => $matching_customers,
                'sirelo_customer' => $sirelo_customer,
                'portalnames' => $portal_names,
                'amountofmatches' => $amount
            ]);
    }

    public function matchlead(\Illuminate\Http\Request $webrequest, $request_id)
    {
        $request_data = new RequestData();
        $system = new System();
        $sirelo_function = new SireloCustomer();
        $mover = new Mover();
        $mail = new Mail();
        $request_deliveries = new RequestDeliveries();

        $customers = [];

        //Get the actual request
        $request = Request::findOrFail($request_id);
        $pd_url = Website::where("we_sirelo_co_code",  $request->re_co_code_from)->first()->we_personal_dashboard_url;

        if (empty($pd_url)) {
            $pd_url = Website::where("we_sirelo_url", "https://sirelo.org")->first()->we_personal_dashboard_url;
        }

        $matching_customers = json_decode($webrequest->matching_customers[0], true);

        if (isset($webrequest->system_user))
        {
            $user_id = $webrequest->system_user;
        } else
        {
            $user_id = Auth::id();
        }

        Log::debug($webrequest);
        Log::debug($request_id);
        Log::debug($matching_customers);

        $checked_list = [];
        $remaining_list = [];

        foreach ($matching_customers['customers'] as $match_customer)
        {
            if ($matching_customers['main_portal'] == $match_customer['po_id'])
            {
                if ($match_customer['checked'])
                {
                    $checked_list[$match_customer['cu_id']] = $match_customer['formula_price'];
                } else
                {
                    $remaining_list[$match_customer['cu_id']] = $match_customer['formula_price'];
                }
            }
        }

        arsort($checked_list);
        arsort($remaining_list);

        DB::beginTransaction();

        foreach ($matching_customers['customers'] as $match_customer)
        {
            if ($matching_customers['main_portal'] == $match_customer['po_id'])
            {

                if ($match_customer['checked'])
                {
                    $ranking = array_search($match_customer['cu_id'], array_keys($checked_list)) + 1;
                } else
                {
                    $ranking = array_search($match_customer['cu_id'], array_keys($remaining_list)) + 1 + count($checked_list);
                }

                try
                {
                    //Enter into DB which customers were selected and which were chosen to be matched to
                    $match_statistic = new MatchStatistic();
                    $match_statistic->mast_re_id = $request->re_id;
                    $match_statistic->mast_cu_id = $match_customer['cu_id'];
                    $match_statistic->mast_matched = $match_customer['checked'] ?? 0;
                    $match_statistic->mast_ktcupo_id = $match_customer['ktcupo_id'];
                    $match_statistic->mast_lead_price_netto = $match_customer['lead_price'];
                    $match_statistic->mast_correction_bonus = $match_customer['correction_bonus'];
                    $match_statistic->mast_final_price = $match_customer['final_price'];
                    $match_statistic->mast_formula_price = $match_customer['formula_price'];
                    $match_statistic->mast_match_position = $ranking;
                    $match_statistic->mast_average_match = $match_customer['average_match_30_days'];
                    $match_statistic->mast_match_percentage_before_podium = $match_customer['matching_percentage'] ?? 0;
                    $match_statistic->mast_match_percentage_after_podium = $match_customer['matching_percentage_after_podium'] ?? 0;
                    $match_statistic->mast_podium = ($match_customer['matching_percentage_after_podium'] == 100 ? 1 : 0);
                    $match_statistic->save();
                } catch (\Exception $e)
                {
                    DB::rollBack();

                    break;
                }
            }
        }

        DB::commit();

        $matches_left = $request_data->requestMatchesLeft($request_id);

        if ($matches_left <= 0)
        {
            return redirect()->back()->withErrors(['matches_left' => 'The request has reached the maximum amount of matches per request.']);
        }

        if (isset($webrequest->customers))
        {
            if (sizeof($webrequest->customers) > 5)
            {
                return redirect()->back()->withErrors(['match_amount' => 'Please match to a maximum of 5 customers!']);
            }
            if (sizeof($webrequest->customers) < 1 && !$request->re_sirelo_customer)
            {
                return redirect()->back()->withErrors(['match_amount' => 'Please match to a minimum of 1 customer!']);
            }
        }

        if ($request->re_sirelo_customer)
        {

            $sirelo_customer_query = DB::table("customers")
                ->select("moda_disable_sirelo_export", "moda_no_claim_discount", "moda_no_claim_discount_percentage", "moda_contact_email")
                ->selectRaw("customers.*")
                ->leftJoin("mover_data", "cu_id", "moda_cu_id")
                ->where("cu_id", $request->re_sirelo_customer)
                ->where("cu_type", 1)
                ->where("cu_deleted", 0)
                ->first();

            $already_matched = false;

            foreach ($request_data->getMatchedTo($request_id) as $matched_customer)
            {
                if ($matched_customer->customer->cu_id == $request->re_sirelo_customer)
                {
                    $already_matched = true;
                }
            };

            if (!$already_matched)
            {
                $matches_left--;

                $sirelo_customer = $sirelo_customer_query;
            }
        }

        if (isset($sirelo_customer) && $request_data->requestMatchesLeft($request_id) > 0)
        {
            $sirelo_valid = $sirelo_function->sireloValid($sirelo_customer->cu_id);

            $surveys = DB::table("surveys_2")
                ->selectRaw("COUNT(*) AS `reviews`")
                ->selectRaw("SUM(`su_rating`) AS `rating`")
                ->where("su_submitted", 1)
                ->where("su_show_on_website", 1)
                ->where(function ($query) use ($sirelo_customer) {
                    $query
                        ->where('su_mover', '=', $sirelo_customer->cu_id)
                        ->orWhere('su_other_mover', '=', $sirelo_customer->cu_id);
                })
                ->first();

            $rating = 0;

            if ($surveys->reviews > 0)
            {
                $rating = round($surveys->rating / $surveys->reviews, 1);
            }

            $sirelo_data = [
                "sirelo_valid" => $sirelo_valid,
                "sirelo_page" => $sirelo_function->getPage($sirelo_customer->cu_id),
                "rating" => number_format(($rating * 2), 1, ",", ""),
                "reviews" => $surveys->reviews
            ];

            $customers[] = ["type" => "sirelo"] + $system->databaseToArray($sirelo_customer) + $sirelo_data;

            //Get latest customer request ID
            $latest_id = DB::table("kt_request_customer_portal")
                ->select("ktrecupo_cu_re_id")
                ->where("ktrecupo_cu_id", $sirelo_customer->cu_id)
                ->orderBy("ktrecupo_cu_re_id", 'desc')
                ->first();

            if (!empty($latest_id))
            {
                $cu_re_id = $latest_id->ktrecupo_cu_re_id + 1;
            } else
            {
                $cu_re_id = 1;
            }


            //Insert match into Database
            $match_request = new KTRequestCustomerPortal();

            $match_request->ktrecupo_re_id = $request->re_id;
            $match_request->ktrecupo_cu_id = $sirelo_customer->cu_id;
            $match_request->ktrecupo_cu_re_id = $cu_re_id;
            $match_request->ktrecupo_po_id = $request->re_po_id;
            $match_request->ktrecupo_timestamp = Carbon::now()->toDateTimeString();
            $match_request->ktrecupo_us_id = $user_id;
            $match_request->ktrecupo_type = 3;
            $match_request->ktrecupo_rematch = (($request->re_status == 1) ? 1 : 0);
            $match_request->ktrecupo_free_percentage = (($sirelo_customer->moda_no_claim_discount) ? (1 / 100) * $sirelo_customer->moda_no_claim_discount_percentage : 0);

            $match_request->save();

            //Add credits
            $credits = $request_data->getCreditsForRequest($request->re_moving_size);

            $cc = new CustomerCredit();

            $cc->cucr_cu_id = $sirelo_customer->cu_id;
            $cc->cucr_amount = $credits;
            $cc->cucr_amount_left = $credits;
            $cc->cucr_timestamp = Carbon::now()->toDateTimeString();
            $cc->cucr_type = 1;
            $cc->cucr_expired = date('Y-m-d H:m:s', strtotime("+30 days"));

            $cc->save();


            //Do plus 1 for sirelo customer.
            $matched_to = (count($webrequest->customers) ?? 0) + 1 - 1;

            if ($matched_to <= 2)
            {
                $matched_to = "0-2";
            }

            $request->re_volume_calculator = $request->volumecalculator->voca_volume_calculator;

            $fields = [
                "request_delivery_value" => ((!empty($sirelo_customer->cu_email)) ? $sirelo_customer->cu_email : $sirelo_customer->moda_contact_email),
                "request_delivery_extra" => "",
                "match_type" => 2,
                "cu_id" => $sirelo_customer->cu_id,
                "cu_re_id" => $cu_re_id,
                "language" => $sirelo_customer->cu_la_code,
                "request" => $system->databaseToArray($request),
                "matched_to" => $matched_to
            ];

            Log::debug("Customer Request");
            Log::debug($fields);

            $mail->send("customer_request", $sirelo_customer->cu_la_code, $fields);
        }

        if ($webrequest->customers)
        {
            foreach ($webrequest->customers as $ktcupo_id => $rest)
            {
                $ktrecupo = KTRequestCustomerPortal::where("ktrecupo_re_id", $request->re_id)->where("ktrecupo_ktcupo_id", $ktcupo_id)->get();

                //Check if this lead hasn't been matched to this customer yet
                if ($ktrecupo->count() == 0 && $request_data->requestMatchesLeft($request->re_id) > 0)
                {


                    //Get portal and all relevant info
                    $customer_info = DB::table("kt_customer_portal")
                        ->select("ktcupo_id", "ktcupo_cu_id", "ktcupo_po_id", "ktcupo_status", "ktcupo_free_trial", "ktcupo_request_type", "ktcupo_description", "po_portal", "po_pacu_code")
                        ->selectRaw("customers.*")
                        ->selectRaw("mover_data.*")
                        ->leftJoin("customers", "ktcupo_cu_id", "cu_id")
                        ->leftJoin("mover_data", "cu_id", "moda_cu_id")
                        ->leftJoin("portals", "ktcupo_po_id", "po_id")
                        ->where("ktcupo_id", $ktcupo_id)
                        ->where("cu_credit_hold", 0)
                        ->where("cu_deleted", 0)
                        ->first();

                    if (!empty($customer_info))
                    {

                        $sirelo_valid = $sirelo_function->sireloValid($customer_info->cu_id);

                        $surveys = DB::table("surveys_2")
                            ->selectRaw("COUNT(*) AS `reviews`")
                            ->selectRaw("SUM(`su_rating`) AS `rating`")
                            ->where("su_submitted", 1)
                            ->where("su_show_on_website", 1)
                            ->where(function ($query) use ($customer_info) {
                                $query
                                    ->where('su_mover', '=', $customer_info->cu_id)
                                    ->orWhere('su_other_mover', '=', $customer_info->cu_id);
                            })
                            ->first();

                        $rating = 0;

                        if ($surveys->reviews > 0)
                        {
                            $rating = round($surveys->rating / $surveys->reviews, 1);
                        }

                        $sirelo_data = [
                            "sirelo_valid" => $sirelo_valid,
                            "sirelo_page" => $sirelo_function->getPage($customer_info->cu_id),
                            "rating" => number_format(($rating * 2), 1, ",", ""),
                            "reviews" => $surveys->reviews
                        ];

                        $customers[] = ["type" => "post"] + $system->databaseToArray($customer_info) + $sirelo_data;

                        //Get destination type and correct regions for matching
                        if ($request->re_destination_type == DestinationType::NATMOVING)
                        {

                            $parent_from = Region::select("reg_parent")->where("reg_id", $request->re_reg_id_from)->first()->reg_parent;
                            $parent_to = Region::select("reg_parent")->where("reg_id", $request->re_reg_id_to)->first()->reg_parent;

                            //Get payment rate
                            $payment_rate = DB::table("payment_rates")
                                ->select("para_rate", "para_discount_type", "para_discount_rate")
                                ->where("para_ktcupo_id", $customer_info->ktcupo_id)
                                ->whereRaw("`para_date` <= NOW()")
                                ->where("para_nat_type", ($parent_from == $parent_to ? NatPaymentType::NAT_LOCAL : NatPaymentType::NAT_LONG_DISTANCE))
                                ->orderBy("para_date", 'desc')
                                ->first();

                        } else
                        {
                            //Get payment rate
                            $payment_rate = DB::table("payment_rates")
                                ->select("para_rate", "para_discount_type", "para_discount_rate")
                                ->where("para_ktcupo_id", $customer_info->ktcupo_id)
                                ->whereRaw("`para_date` <= NOW()")
                                ->orderBy("para_date", 'desc')
                                ->first();
                        }


                        if ($customer_info->ktcupo_free_trial == 0 && !empty($payment_rate))
                        {
                            $currency = $customer_info->po_pacu_code;
                            $currency_customer = $customer_info->cu_pacu_code;

                            $amount = $payment_rate->para_rate;
                            $amount_eur = $system->currencyConvert($amount, $currency, "EUR");
                            $amount_customer = $system->currencyConvert($amount, $currency, $currency_customer);

                            $discount_type = $payment_rate->para_discount_type;
                            $discount_rate = $payment_rate->para_discount_rate;

                            $amount_discount = 0.0;
                            $amount_netto = $system->calculatePaymentRate($payment_rate, $amount_discount);

                            $amount_netto_eur = $system->currencyConvert($amount_netto, $currency, "EUR");
                            $amount_netto_customer = $system->currencyConvert($amount_netto, $currency, $currency_customer);
                        } else
                        {
                            $amount = 0;
                            $amount_eur = 0;
                            $amount_customer = 0;

                            $discount_type = 0;
                            $discount_rate = 0;

                            $amount_discount = 0;
                            $amount_netto = 0;

                            $amount_netto_eur = 0;
                            $amount_netto_customer = 0;
                        }

                        //Get latest customer request ID
                        $latest_id = DB::table("kt_request_customer_portal")
                            ->select("ktrecupo_cu_re_id")
                            ->where("ktrecupo_cu_id", $customer_info->cu_id)
                            ->orderBy("ktrecupo_cu_re_id", 'desc')
                            ->first();

                        if (!empty($latest_id))
                        {
                            $cu_re_id = $latest_id->ktrecupo_cu_re_id + 1;
                        } else
                        {
                            $cu_re_id = 1;
                        }

                        //Insert match into Database
                        $match_request = new KTRequestCustomerPortal();

                        $match_request->ktrecupo_re_id = $request->re_id;
                        $match_request->ktrecupo_ktcupo_id = $customer_info->ktcupo_id;
                        $match_request->ktrecupo_cu_id = $customer_info->cu_id;
                        $match_request->ktrecupo_cu_re_id = $cu_re_id;
                        $match_request->ktrecupo_po_id = $customer_info->ktcupo_po_id;
                        $match_request->ktrecupo_timestamp = Carbon::now()->toDateTimeString();
                        $match_request->ktrecupo_us_id = $user_id;
                        $match_request->ktrecupo_type = (($customer_info->ktcupo_free_trial == 1) ? 2 : 1);
                        $match_request->ktrecupo_rematch = (($request->re_status == 1) ? 1 : 0);
                        $match_request->ktrecupo_amount = $amount;
                        $match_request->ktrecupo_amount_eur = $amount_eur;
                        $match_request->ktrecupo_amount_customer = $amount_customer;
                        $match_request->ktrecupo_discount_type = $discount_type;
                        $match_request->ktrecupo_discount_rate = $discount_rate;
                        $match_request->ktrecupo_amount_discount = $amount_discount;
                        $match_request->ktrecupo_amount_netto = $amount_netto;
                        $match_request->ktrecupo_amount_netto_eur = $amount_netto_eur;
                        $match_request->ktrecupo_amount_netto_customer = $amount_netto_customer;
                        $match_request->ktrecupo_free_percentage = (($customer_info->moda_no_claim_discount) ? (1 / 100) * $customer_info->moda_no_claim_discount_percentage : 0);
                        $match_request->save();

                        //Lower cappings
                        DB::table('kt_customer_portal')
                            ->where('ktcupo_id', $customer_info->ktcupo_id)
                            ->update(
                                [
                                    'ktcupo_max_requests_month_left' => DB::raw("(`ktcupo_max_requests_month_left` - 1)"),
                                    'ktcupo_max_requests_day_left' => DB::raw("(`ktcupo_max_requests_day_left` - 1)"),
                                    'ktcupo_requests_bucket' => DB::raw("(`ktcupo_requests_bucket` + 1)")
                                ]
                            );

                        //Modify the bucket (Customer level)
                        if ($customer_info->moda_capping_method == MoverCappingMethod::MONTHLY_LEADS || $customer_info->moda_capping_method == MoverCappingMethod::MONTHLY_SPEND)
                        {
                            $bucket_change = ($customer_info->moda_capping_method == 1 ? 1 : $amount_netto);

                            DB::table('mover_data')
                                ->where('moda_cu_id', $customer_info->cu_id)
                                ->update(
                                    [
                                        'moda_capping_bucket' => DB::raw("(`moda_capping_bucket` + '{$bucket_change}')"),
                                        'moda_capping_monthly_limit_left' => DB::raw("(`moda_capping_monthly_limit_left` - '{$bucket_change}')")
                                    ]
                                );
                        }

                        DB::table('mover_data')
                            ->where('moda_cu_id', $customer_info->cu_id)
                            ->update(
                                [
                                    'moda_forced_daily_capping_left' => DB::raw("(`moda_forced_daily_capping_left` - '1')")
                                ]
                            );

                        //Set Prepayment customers on CH if no more money
                        $mover->setCustomersOnCreditHoldAfterMatch($customer_info->cu_id);

                        //Add credits
                        $credits = $request_data->getCreditsForRequest($request->re_moving_size);

                        $cc = new CustomerCredit();

                        $cc->cucr_timestamp = Carbon::now()->toDateTimeString();
                        $cc->cucr_cu_id = $customer_info->cu_id;
                        $cc->cucr_amount = $credits;
                        $cc->cucr_amount_left = $credits;
                        $cc->cucr_expired = date('Y-m-d H:m:s', strtotime("+30 days"));
                        $cc->cucr_type = 1;

                        $cc->save();

                        if ($customer_info->ktcupo_free_trial == 1)
                        {

                            $free_trial = DB::table("free_trials")
                                ->select("frtr_id", "frtr_stop_type", "frtr_us_id", "frtr_stop_requests", "frtr_stop_requests_left")
                                ->where("frtr_ktcupo_id", $customer_info->ktcupo_id)
                                ->where("frtr_finished", 0)
                                ->whereRaw("`frtr_date` <= NOW()")
                                ->orderBy("frtr_date", 'desc')
                                ->first();

                            if (!empty($free_trial))
                            {
                                if ($free_trial->frtr_stop_type == FreeTrialStopType::REQUESTS || $free_trial->frtr_stop_type == FreeTrialStopType::DATE_OR_REQUESTS)
                                {
                                    DB::table('free_trials')
                                        ->where('frtr_id', $free_trial->frtr_id)
                                        ->update(
                                            [
                                                'frtr_stop_requests_left' => DB::raw("(`frtr_stop_requests_left` - 1)")
                                            ]
                                        );

                                    $users = [1778];

                                    foreach ([$free_trial->frtr_us_id, $free_trial->cu_sales_manager] as $us_id)
                                    {
                                        $users[$us_id] = $us_id;
                                    }

                                    if ($free_trial->frtr_stop_requests_left <= 1)
                                    {
                                        DB::table('kt_customer_portal')
                                            ->where('ktcupo_id', $customer_info->ktcupo_id)
                                            ->update(
                                                [
                                                    'ktcupo_status' => CustomerPairStatus::INACTIVE,
                                                ]
                                            );

                                        DB::table((new Revision())->getTable())->insert([
                                            [
                                                'revisionable_type' => "App\Models\KTCustomerPortal",
                                                'revisionable_id' => $customer_info->ktcupo_id,
                                                'key' => $customer_info->po_portal . " - " . $customer_info->ktcupo_description,
                                                'old_value' => $customer_info->ktcupo_status,
                                                'new_value' => 0,
                                                'user_id' => \Auth::id(),
                                                'created_at' => Carbon::now()->toDateTimeString(),
                                                'updated_at' => Carbon::now()->toDateTimeString()
                                            ]
                                        ]);

                                        /*
                                        $status_history = new StatusHistory();

                                        $status_history->sthi_timestamp = Carbon::now()->toDateTimeString();
                                        $status_history->sthi_type = 1;
                                        $status_history->sthi_us_id = \Auth::id();
                                        $status_history->sthi_cu_id = $customer_info->ktcupo_cu_id;
                                        $status_history->sthi_ktcupo_id = $customer_info->ktcupo_id;
                                        $status_history->sthi_status_from = $customer_info->ktcupo_status;
                                        $status_history->sthi_status_to = 0;

                                        $status_history->save();
                                        */

                                        $subject = "Free trial finished";
                                        $message = "The free trial of customer <strong>" . $customer_info->cu_company_name_business . " - " . $customer_info->po_portal . " (" . RequestType::name($customer_info->ktcupo_request_type) . " - " . $customer_info->ktcupo_description . "</strong> has been finished, because the stop requests has been reached. The status has been changed to <strong>" . CustomerPairStatus::name(0) . "</strong>.";

                                        $system->sendMessage($users, $subject, $message);

                                        DB::table('free_trials')
                                            ->where('frtr_id', $free_trial->frtr_id)
                                            ->update(
                                                [
                                                    'frtr_finished' => 1,
                                                ]
                                            );

                                    } elseif ($free_trial->frtr_stop_requests == $free_trial->frtr_stop_requests_left)
                                    {
                                        $subject = "First free trial request";
                                        $message = "Customer <strong>" . $customer_info->cu_company_name_business . " - " . $customer_info->po_portal . " (" . RequestType::name($customer_info->ktcupo_request_type) . ") - " . $customer_info->ktcupo_description . "</strong> has received their first free trial request.";

                                        $system->sendMessage($users, $subject, $message);
                                    }
                                }
                            }
                        }

                        foreach ($request_deliveries->receivers($customer_info->cu_type, $customer_info->ktcupo_id, $customer_info->cu_email) as $receiver)
                        {
                            if (isset($sirelo_customer))
                            {
                                //Do plus 1 because of the sirelo customer
                                $matched_to = count($_POST['customers']) + 1 - 1;
                            } else
                            {
                                $matched_to = count($_POST['customers']) - 1;
                            }

                            if ($matched_to <= 2)
                                $matched_to = "0-2";

                            $request->re_volume_calculator = $request->volumecalculator['voca_volume_calculator'];

                            if ($request->re_request_type == 5)
                            {
                                $ktrecupo_id = KTRequestCustomerPortal::where("ktrecupo_re_id", $request->re_id)->where("ktrecupo_cu_id", $customer_info->cu_id)->first()->ktrecupo_id;

                                $fields = [
                                    "request_delivery_value" => $receiver['value'],
                                    "request_delivery_extra" => $receiver['extra'],
                                    "cu_id" => $customer_info->cu_id,
                                    "cu_re_id" => $cu_re_id,
                                    "ktrecupo_id" => $ktrecupo_id,
                                    "language" => $customer_info->cu_la_code,
                                    "request" => $system->databaseToArray($request)
                                ];
                            } else
                            {
                                $fields = [
                                    "request_delivery_value" => $receiver['value'],
                                    "request_delivery_extra" => $receiver['extra'],
                                    "match_type" => 1,
                                    "cu_id" => $customer_info->cu_id,
                                    "cu_re_id" => $cu_re_id,
                                    "language" => $customer_info->cu_la_code,
                                    "request" => $system->databaseToArray($request),
                                    "matched_to" => $matched_to
                                ];
                            }

                            Log::debug("Customer Request (Customer)");
                            Log::debug($fields);

                            if ($receiver['type'] == 1)
                            {
                                if ($request->re_request_type == 5)
                                {
                                    $premium_lead = PremiumLead::select("prle_list_of_all_items")->where("prle_re_id", $request->re_id)->first();

                                    if (!empty($premium_lead->prle_list_of_all_items))
                                    {
                                        $mail->send("customer_premium_lead", $customer_info->cu_la_code, $fields, "uploads/excel_itemlist/" . $premium_lead->prle_list_of_all_items);
                                    } else
                                    {
                                        $mail->send("customer_premium_lead", $customer_info->cu_la_code, $fields);
                                    }

                                } else
                                {
                                    $mail->send("customer_request", $customer_info->cu_la_code, $fields);
                                }
                            } else
                            {
                                $request_deliveries->send($customer_info->cu_type, $receiver['type'], $customer_info->cu_la_code, $fields);
                            }
                        }

                    }
                }
            }
        }
        if (count($customers) < 1)
        {
            return redirect()->back()->withErrors(['match_amount' => "The request hasn't been matched, because no eligible customers were found."]);
        } else
        {
            $request->re_status = 1;
            if ($request->re_pd_validate == 1)
            {
                $request->re_pd_validate = 2;
            }
            $request->save();

            if ($request->re_source == RequestSource::AFFILIATE)
            {

                //Set affiliate lead as matched
                DB::table('affiliate_partner_requests')
                    ->where('afpare_re_id', $request->re_id)
                    ->update(
                        [
                            'afpare_status' => RequestStatus::MATCHED,
                        ]
                    );

                $affiliate_partner_form = DB::table("affiliate_partner_forms")
                    ->select("afpafo_cu_id", "afpada_max_requests_month_left", "afpada_max_requests_month")
                    ->selectRaw("customers.*")
                    ->leftJoin("affiliate_partner_data", "afpafo_cu_id", "afpada_cu_id")
                    ->leftJoin("customers", "afpafo_cu_id", "cu_id")
                    ->where("afpafo_id", $request->re_afpafo_id)
                    ->first();

                //Set affiliate lead as matched
                DB::table('affiliate_partner_data')
                    ->where('afpada_cu_id', $affiliate_partner_form->afpafo_cu_id)
                    ->update(
                        [
                            'afpada_max_requests_month_left' => DB::raw("(`afpada_max_requests_month_left` - 1)"),
                        ]
                    );

                if ($affiliate_partner_form->afpada_max_requests_month_left <= 1)
                {
                    $users = [];

                    foreach ([$affiliate_partner_form->cu_sales_manager, $affiliate_partner_form->cu_account_manager] as $us_id)
                    {
                        $users[$us_id] = $us_id;
                    }

                    //Send message to Account Manager if affiliate has reached its daily request limit
                    $subject = "Affiliate partner has reached its monthly request limit";
                    $message = "The <strong>monthly</strong> request limit for affiliate partner <strong>" . $affiliate_partner_form->cu_company_name_business . "</strong> of " . $affiliate_partner_form->afpada_max_requests_month . " requests has been reached.";

                    $system->sendMessage($users, $subject, $message);
                }
            }

            foreach ($customers as $customer)
            {
                if ($customer['cu_payment_method'] == PaymentMethod::PREPAYMENT)
                {
                    //Get current balances of customer
                    $balance_total = $mover->customerBalancesFC([$customer['cu_id']])[$customer['cu_id']];

                    //Get amount of last 5 days
                    $customer_portals = DB::table("kt_request_customer_portal")
                        ->selectRaw("SUM(ktrecupo_amount) as amount")
                        ->where("ktrecupo_cu_id", $customer['cu_id'])
                        ->where("ktrecupo_invoiced", 0)
                        ->whereRaw("ktrecupo_timestamp > NOW() - INTERVAL 5 DAY")
                        ->first();

                    //Total amount of last 5 days
                    $amount_last_5_days = $customer_portals->amount;

                    //If there are no leads last 5 days, then set amount on 0
                    if (empty($amount_last_5_days))
                    {
                        $amount_last_5_days = 0;
                    }

                    //If balance can be negative in 5 days
                    if ($balance_total < $amount_last_5_days && empty($customer['cu_credit_limit_increase_timestamp']))
                    {
                        /**
                         * If date which one is set in Database, less then 5 days? Then don't mail, more then 5 days? MAIL!
                         * */

                        //Set dates
                        $date_now = date("Y-m-d H:i:s");
                        $date_sent = $customer['cu_prepayment_mail_timestamp'];

                        $date1 = new DateTime(date('Y-m-d', strtotime($date_sent)));
                        $date2 = new DateTime(date('Y-m-d', strtotime($date_now)));

                        //Calculate days difference
                        $days_difference = $date1->diff($date2)->days;
                        if ($days_difference >= 5)
                        {
                            $fields = [
                                "amount_left" => $balance_total,
                                "cu_email" => $customer['cu_email'],
                                "company_name" => $customer['cu_company_name_business'],
                                "currency" => $system->paymentCurrencyToken($customer['cu_pacu_code']),
                                "number_of_days" => 5,
                            ];

                            //Send mail
                            $mail->send("prepayment_low_balance", $customer['cu_la_code'], $fields);


                            //Set affiliate lead as matched
                            DB::table('customers')
                                ->where('cu_id', $customer['cu_id'])
                                ->update(
                                    [
                                        'cu_prepayment_mail_timestamp' => DB::raw("NOW()"),
                                    ]
                                );

                            //Send message to these users when prepayment mail is sent.
                            $message = "Prepayment email has been send to Customer: " . $customer['cu_company_name_business'] . " (Cu ID = " . $customer['cu_id'] . ", Email = " . $customer['cu_email'] . ")";

                            $system->sendMessage([4560, 4459], "<b>Prepayment email sent to " . $customer['cu_company_name_business'] . "</b> when lead matched", $message);
                        }
                    }

                }
            }

            $count = sizeof($webrequest->customers);

            $customers_history = [];

            foreach ($customers as $customer)
            {
                if ($customer['type'] == "sirelo")
                {
                    $count++;

                    $ktcupo_id = "Sirelo";
                } elseif ($customer['type'] == "post")
                {
                    $ktcupo_id = $customer['ktcupo_id'];
                }

                $customers_history[] = [
                    "cu_id" => $customer['cu_id'],
                    "ktcupo_id" => $ktcupo_id
                ];
            }

            $sp_requests = KTRequestCustomerQuestion::leftJoin("customers", "ktrecuqu_cu_id", "cu_id")->where("ktrecuqu_re_id", $request->re_id)->get();

            $pd_link = $pd_url . "?id=" . $request->re_token . "&utm_source=personal-dashboard&utm_medium=email";

            $excludes = ['RU'];

            if (in_array($request->re_la_code, $excludes))
            {

                $request->re_la_code = "EN";
            }

            $fields = [
                "request" => $system->databaseToArray($request),
                "customers" => $customers,
                "sp_requests" => $system->databaseToArray($sp_requests),
                "pd_link" => $pd_link
            ];

            Log::debug("Request Match");
            Log::debug($fields);

            $mail->send("request_match", $request->re_la_code, $fields);

            if ($count > count($customers))
            {
                return redirect()->back()->withErrors(['match_amount' => "You have checked <strong>" . $count . "</strong> customer(s), but the request has only been sent to the first <strong>" . count($customers) . "</strong> checked customer(s)."]);
            }
        }

        if ($request->re_request_type == 5)
        {
            $start_url = "/premium_leads";
        } else
        {
            $start_url = "/requests";
        }

        return redirect($start_url)->with('message', 'Request successfully matched to ' . count($customers) . ' customer(s)!');
    }

    public function send($request_id)
    {

        Request::findOrFail($request_id);

        $userEmailTypes = [
            1 => "Request received (Before match)",
            2 => "Request matched (After match)"
        ];

        return view('requests.send',
            [
                'languages' => Language::whereIn("la_code", ["NL", "DK", "DE", "IT", "ES", "FR", "EN"])->get(),
                'requestdeliveries' => CustomerRequestDeliveryType::all(),
                'request_id' => $request_id,
                'usermailtypes' => $userEmailTypes
            ]);
    }

    public function sendlead(\Illuminate\Http\Request $webrequest, $request_id)
    {
        $mail = new Mail();
        $requestdeliveries = new RequestDeliveries();
        $system = new System();
        $request_data = new RequestData();
        $sirelo_function = new SireloCustomer();

        $request = Request::findOrFail($request_id);
        $pd_url = Website::where("we_sirelo_co_code",  $request->re_co_code_from)->first()->we_personal_dashboard_url;

        if (empty($pd_url)) {
            $pd_url = Website::where("we_sirelo_url", "https://sirelo.org")->first()->we_personal_dashboard_url;
        }

        Log::debug($webrequest);
        $request->re_volume_calculator = $request->volumecalculator->voca_volume_calculator;

        if ($webrequest->form == "mover")
        {
            $count_matched_to = $request_data->requestMatchesDone($request->re_id);
            $matched_to = $count_matched_to - 1;

            if ($matched_to <= 2)
                $matched_to = "0-2";

            $fields = [
                "request_delivery_value" => $webrequest->receiver,
                "request_delivery_extra" => $webrequest->extra,
                "match_type" => 1,
                "cu_id" => "Manual",
                "cu_re_id" => $webrequest->request_id,
                "language" => $webrequest->language,
                "request" => $system->databaseToArray($request),
                "matched_to" => $matched_to
            ];

            if ($webrequest->type == 1)
            {
                $mail->send("customer_request", $webrequest->language, $fields);
            } else
            {
                $requestdeliveries->send(1, $webrequest->type, $webrequest->language, $fields);
            }
        } elseif ($webrequest->form == "user")
        {
            if ($webrequest->type == 1)
            {
                $pd_link = $pd_url . "?id=" . $request->re_token . "&utm_source=personal-dashboard&utm_medium=email";

                $excludes = ['RU'];

                if (in_array($request->re_la_code, $excludes))
                {

                    $request->re_la_code = "EN";
                }

                $fields = [
                    "request" => $system->databaseToArray($request),
                    "pd_link" => $pd_link
                ];

                $mail->send("request_submit", $request->re_la_code, $fields, null, false);
            } elseif ($webrequest->type == 2)
            {

                $customers = DB::table("kt_request_customer_portal")
                    ->leftJoin("customers", "cu_id", "ktrecupo_cu_id")
                    ->leftJoin("mover_data", "cu_id", "moda_cu_id")
                    ->where("ktrecupo_re_id", $request->re_id)
                    ->get();

                if ($customers->count())
                {
                    $count = 0;

                    $customers = $system->databaseToArray($customers);

                    foreach ($customers as $customer)
                    {
                        $website = Website::where("we_sirelo", 1)->where("we_sirelo_active", 1)->where("we_sirelo_co_code", $customer['cu_co_code'])->first();

                        $surveys = DB::table("surveys_2")
                            ->selectRaw("COUNT(*) AS `reviews`")
                            ->selectRaw("SUM(`su_rating`) AS `rating`")
                            ->where("su_submitted", 1)
                            ->where("su_show_on_website", 1)
                            ->where(function ($query) use ($customer) {
                                $query
                                    ->where('su_mover', '=', $customer['cu_id'])
                                    ->orWhere('su_other_mover', '=', $customer['cu_id']);
                            })
                            ->first();

                        $rating = 0;

                        if ($surveys->reviews > 0)
                        {
                            $rating = round($surveys->rating / $surveys->reviews, 1);
                        }

                        $customers[$count]["sirelo_valid"] = !$customer['moda_disable_sirelo_export'] && !empty($website);
                        $customers[$count]["sirelo_page"] = $sirelo_function->getPage($customer['cu_id']);
                        $customers[$count]["rating"] = number_format(($rating * 2), 1, ",", "");
                        $customers[$count]["reviews"] = $surveys->reviews;

                        $count++;
                    }

                    $pd_link = $pd_url . "?id=" . $request->re_token . "&utm_source=personal-dashboard&utm_medium=email";

                    $excludes = ['RU'];

                    if (in_array($request->re_la_code, $excludes))
                    {

                        $request->re_la_code = "EN";
                    }

                    $fields = [
                        "request" => $system->databaseToArray($request),
                        "customers" => $system->databaseToArray($customers),
                        "pd_link" => $pd_link
                    ];

                    $mail->send("request_match", $request->re_la_code, $fields);

                }
            }
        }

        return redirect()->back()->with('message', 'Request successfully sent!');
    }

    /**
     * @param RequestData $requestData
     * @param $request
     * @param System $system
     * @return array
     */
    public function validateRequest(RequestData $requestData, $request, System $system): array
    {
        $notices = $requestData->getRequestsDouble($request);

        if (!$system->validatePhone($request->re_telephone1, $request->re_co_code_from, $request->re_co_code_to))
        {
            $notices['phone_1'] = "INVALID";
        }
        if (!$system->validatePhone($request->re_telephone2, $request->re_co_code_from, $request->re_co_code_to))
        {
            $notices['phone_2'] = "INVALID";
        }

        $bounced_mail = $system->getPostmarkBounce($request->re_email, $request->re_timestamp);

        if ($bounced_mail)
        {
            $notices['email_bounce'] = "<strong>ATTENTION:</strong> Mail from <strong>" . $bounced_mail[0]['From'] . "</strong> to <strong>" . $bounced_mail[0]['Email'] . "</strong> bounced on <strong>" . date("Y-m-d H:i:s", strtotime($bounced_mail[0]['BouncedAt'])) . "</strong> with subject <strong>" . $bounced_mail[0]['Subject'] . "</strong>.";
        }

        if (!empty($request->re_ip_address_country) && ($request->re_ip_address_country != $request->re_co_code_from && $request->re_ip_address_country != $request->re_co_code_to))
        {
            $notices['ip_address'] = "<strong>ATTENTION:</strong> The IP address is not located in the origin or destination country";
        }

        $moving_date_diff = $system->dateDifference(date("Y-m-d"), date("Y-m-d", strtotime($request->re_moving_date)));
        $minimum_days = $requestData->minimumMovingDate($request->re_request_type, $request->re_destination_type);

        if ($moving_date_diff < ($request->re_status == 1 ? $minimum_days - 1 : $minimum_days))
        {
            $notices['moving_date'] = "The moving date is within <strong>" . $moving_date_diff . "</strong> day(s), which should be at least " . $minimum_days . " days.";
        }

        if ($moving_date_diff < 0)
        {
            $notices['moving_date'] = "The moving date is today or in the past.";
        }

        if ($moving_date_diff > 270)
        {
            $notices['moving_date'] = "The moving date is in <strong>" . $moving_date_diff . "</strong> days, this should be less then 270 days.";
        }

        return $notices;
    }

    private function getMailReasons()
    {
        //All the rejection reasons that have an e-mail
        return [1, 2, 3, 4, 5, 6, 7, 8, 9, 12, 13, 14, 15, 22];
    }

    public function recover($re_id)
    {
        $request = Request::findOrFail($re_id);

        return view('requests.recover',
            [
                'request' => $request,
                'recover_reasons' => RecoverReason::all()
            ]);
    }

    public function recoverLead(\Illuminate\Http\Request $request)
    {
        $request_row = Request::findOrFail($request->re_id);

        $request_row->re_status = 0;
        $request_row->re_automatic = 0;
        $request_row->re_on_hold = 1;
        $request_row->re_on_hold_by = Auth::id();
        $request_row->re_rejection_timestamp = null;
        $request_row->re_rejection_reason = "";
        $request_row->re_recover_reason = $request->recover_reason;
        $request_row->re_pd_validate = 0;

        $request_row->save();

        return redirect('/requests')->with('message', 'Request successfully recovered!');
    }

    public function restoreLead(Request $request)
    {
        $request_row = Request::findOrFail($request->re_id);

        $request_row->re_on_hold = 0;
        $request_row->re_on_hold_by = null;

        $request_row->save();

        return redirect('/requests')->with('message', 'Request of ' . $request_row->re_full_name . ' successfully restored!');
    }

    public function feedbackrequest($request_id, $feedback_re_ids)
    {
        $requestData = new RequestData();
        $system = new System();

        $feedback_re_ids = explode(",", urldecode($feedback_re_ids));

        if (($key = array_search($request_id, $feedback_re_ids)) !== false)
        {
            unset($feedback_re_ids[$key]);
        }

        $request = Request::findOrFail($request_id);

        //Get amount of matches left for this request
        $amount_matched = $requestData->requestMatchesLeft($request_id);

        //Get correct portal
        $portal = Portal::find($request->re_po_id);

        //Get name of SEA-website or affiliate website this request came from
        if ($request->re_source == 1)
        {
            $website_form = WebsiteForm::with("website")->where("wefo_id", $request->re_wefo_id)->first();
            $form = $website_form ? $website_form->website->we_website : "";
        } else
        {
            $affiliate_form = AffiliatePartnerForm::where("afpafo_id", $request->re_afpafo_id)->first();
            $form = $affiliate_form ? $affiliate_form->afpafo_name : "";
        }

        $request_types = RequestType::all();
        $destination_type = DestinationType::name($request->re_destination_type);

        //Get correct volume calculator values
        $volumecalculator = $request->volumecalculator->voca_volume_calculator ? $system->volumeCalculator($request->volumecalculator->voca_volume_calculator) : "";

        foreach (Country::all() as $country)
        {
            $countries[$country->co_code] = $country->co_en;
        }

        $language = Language::where("la_code", $request->re_la_code)->first()->la_language;

        $region_from = Region::where("reg_id", $request->re_reg_id_from)->first()->reg_name ?? "";
        $region_to = Region::where("reg_id", $request->re_reg_id_to)->first()->reg_name ?? "";

        $notices = $this->validateRequest($requestData, $request, $system);

        return view('requests.feedback',
            [
                'request' => $request,
                'countries' => $countries,
                'movingsizes' => MovingSize::all(),
                'requestsource' => RequestSource::name($request->re_source),
                'requeststatuses' => RequestStatus::all(),
                'language' => $language,
                'device' => Device::name($request->re_device),
                'emptyyesno' => EmptyYesNo::all(),
                'byselfcompany' => MovingSelfCompany::all(),
                'requestresidences' => RequestResidence::all(),
                'rejectionreasons' => RejectionReason::all(),
                'called' => RequestInternalCalled::all(),
                'volumecalculator' => $volumecalculator,
                'amountmatched' => $amount_matched,
                'portal' => $portal->po_portal,
                'form' => $form,
                'requesttypes' => $request_types,
                'destinationtype' => $destination_type,
                'region_from' => $region_from,
                'region_to' => $region_to,
                'notices' => $notices,
                'feedback_re_ids' => $feedback_re_ids,
                'ratings' => MatchMistakeType::all()
            ]);
    }

    public function submitfeedback(\Illuminate\Http\Request $webrequest, $request_id, $feedback_re_ids)
    {
        //Save this rating
        $request = Request::find($request_id);
        $request->re_match_rating_read = 1;
        $request->save();

        if ($feedback_re_ids == -1)
        {
            return redirect("/requests");
        }

        //Get ID and next to be rated
        $tobearray = explode(",", $feedback_re_ids);
        $next_id = $tobearray[0];
        unset($tobearray[0]);

        if (empty($tobearray))
        {
            $tobearray = "-1";
        } else
        {
            $tobearray = implode(",", $tobearray);
        }

        return redirect("/requests/" . $next_id . "/feedback/" . $tobearray);
    }

    public function approveAutomaticChecked(\Illuminate\Http\Request $request)
    {
        $request = Request::findOrFail($request->id);

        $request->re_automatic_checked = 1;
        $request->save();

    }

    /**
     * @param $request
     */
    public function fixRegionTo($request): void
    {
        $region_to = ($request->re_reg_id_to != "" && $request->re_reg_id_to != null ? 1 : 0);

        if ($region_to == 0)
        {
            $system = new System();
            $total_matches = 0;

            $nearby = $system->findRegionsByDestinationCity($request->re_city_to, $request->re_destination_type, $request->re_co_code_to);
            Log::debug($nearby);

            foreach ($nearby as $region)
            {
                $total_matches += $region->count;
            }

            if ($nearby[0]->count > 10 && (($nearby[0]->count / $total_matches) * 100) > 95)
            {
                $request->re_reg_id_to = $nearby->first()->re_reg_id_to;
                $request->save();
            }
        }
    }

    /**
     * @param $request
     */
    public function fixTelephoneNumber($request): void
    {
        //Validation telephone number X country from
        $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();

        try
        {
            $parsed_phone_from = $phoneUtil->parse($request->re_telephone1, $request->re_co_code_from);
            $isValid_from = $phoneUtil->isValidNumber($parsed_phone_from);

            if ($isValid_from)
            {
                $request->re_telephone1 = $phoneUtil->format($parsed_phone_from, \libphonenumber\PhoneNumberFormat::E164);
                $request->save();

                return;
            } else
            {
                $parsed_phone_to = $phoneUtil->parse($request->re_telephone1, $request->re_co_code_to);
                $isValid_to = $phoneUtil->isValidNumber($parsed_phone_to);

                if ($isValid_to)
                {
                    $request->re_telephone1 = $phoneUtil->format($parsed_phone_to, \libphonenumber\PhoneNumberFormat::E164);
                    $request->save();

                    return;
                }
            }

        } catch (\libphonenumber\NumberParseException $e)
        {
            Log::debug("Telephone API FROM is failed");
        }

    }
}
