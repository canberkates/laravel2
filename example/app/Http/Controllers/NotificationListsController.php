<?php

namespace App\Http\Controllers;

use App\Functions\System;
use App\Models\Country;
use App\Models\KTNotificationUser;
use App\Models\NotificationList;
use App\Models\Obligation;
use App\Models\SireloLink;
use App\Models\User;
use App\Models\Website;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NotificationListsController extends Controller
{
    public function index()
    {
        return view('notificationlists.index',
            [
                'notificationlists' => NotificationList::all()
            ]);
    }
    
    public function create()
    {
        return view('notificationlists.create', [
            'users' => User::where("us_is_deleted", 0)->get()
        ]);
    }
    
    public function store(Request $request)
    {
        $request->validate([
            'noli_name' => 'required',
        ]);
        
        $list = new NotificationList();
        $list->noli_name = $request->noli_name;
        $list->save();
        
        if (isset($request->users))
        {
            foreach($request->users as $us_id => $on)
            {
                $kt_notification_user = new KTNotificationUser();
                $kt_notification_user->ktnous_us_id = $us_id;
                $kt_notification_user->ktnous_noli_id = $list->noli_id;
                $kt_notification_user->save();
            }
        }
        
        return redirect('admin/notification_lists');
    }
    
    public function edit($noli_id)
    {
        $notification_list = NotificationList::where("noli_id", $noli_id)->first();
        $users_noli = KTNotificationUser::where("ktnous_noli_id", $noli_id)->get();
        $selected_users = [];
        
        foreach($users_noli as $us)
        {
            $selected_users[$us->ktnous_us_id] = $us->ktnous_us_id;
        }
        
        return view('notificationlists.edit',
            [
                'notification_list' => $notification_list,
                'selected_users' => $selected_users,
                'users' => User::where("us_is_deleted", 0)->get(),
            ]);
        
    }
    
    public function update(Request $request, $noli_id)
    {
        $request->validate([
            'noli_name' => 'required',
        ]);
        
        $notification_list = NotificationList::where("noli_id", $noli_id)->first();
        $notification_list->noli_name = $request->noli_name;
        $notification_list->save();
    
        if (isset($request->users))
        {
            //Delete rows which are not in array
            DB::table("kt_notification_user")
                ->whereNotIn("ktnous_us_id", $request->users)
                ->where("ktnous_noli_id", $notification_list->noli_id)
                ->delete();
            
            foreach($request->users as $us_id => $on)
            {
                //Check if row already exists
                $KTNotificationUser = KTNotificationUser::where("ktnous_us_id", $us_id)
                    ->where("ktnous_noli_id", $notification_list->noli_id)
                    ->first();
                
                if (count($KTNotificationUser) == 0)
                {
                    $new = new KTNotificationUser();
                    $new->ktnous_us_id = $us_id;
                    $new->ktnous_noli_id = $notification_list->noli_id;
                    $new->save();
                }
            }
        }
        else {
            //Delete all rows in KTNotificationUser
            DB::table("kt_notification_user")
                ->where("ktnous_noli_id", $notification_list->noli_id)
                ->delete();
        }
        
        //Loop through every sirelo link post
        foreach ($request->sirelo as $co_code => $link)
        {
            $sireloLink = SireloLink::where("sihy_co_code", $co_code)->where("sihy_cuob_id", $obligation_id)->first();
            
            //check if link already exists
            if (count($sireloLink) > 0)
            {
                //Update hyperlink
                $sireloLink->sihy_link = $link;
                $sireloLink->sihy_timestamp = date("Y-m-d H:i:s");
                $sireloLink->save();
            }
            else
            {
                //Create hyperlink
                $newLink = new SireloLink();
                
                $newLink->sihy_co_code = $co_code;
                $newLink->sihy_cuob_id = $obligation_id;
                $newLink->sihy_link = ((empty($link)) ? "" : $link);
                $newLink->sihy_timestamp = date("Y-m-d H:i:s");
                
                $newLink->save();
            }
        }
        
        return redirect('admin/notification_lists');
    }
    
    public function delete(Request $request)
    {
        DB::table("kt_notification_user")
            ->where("ktnous_noli_id", $request->id)
            ->delete();
        
        DB::table("notification_lists")
            ->where("noli_id", $request->id)
            ->delete();
    }
    
}
