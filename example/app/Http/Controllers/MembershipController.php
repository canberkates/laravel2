<?php

namespace App\Http\Controllers;

use App\Models\Membership;
use App\Models\SireloLink;
use App\Models\Website;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MembershipController extends Controller
{
    public function index()
    {
        return view('membership.index',
            [
                'memberships' => Membership::all()
            ]);
    }

    public function create()
    {
        return view('membership.create');
    }

    public function store(Request $request)
    {
        $membership = new Membership();
        $membership->me_name = $request->name;
        $membership->me_location = $request->location;
        $membership->me_website = $request->website;
        $membership->save();

        return redirect('admin/memberships');
    }

    public function edit($membership_id)
    {
        $membership = Membership::where("me_id", $membership_id)->first();

        $sirelo_websites = Website::whereRaw("we_sirelo_co_code IS NOT NULL")->get();

        $sirelo_links = Website::leftJoin("sirelo_hyperlinks", "we_sirelo_co_code", "sihy_co_code")->where("sihy_me_id", $membership->me_id)->get();

        $websites = [];
        $sirelo_websites_list = [];

        foreach ($sirelo_links as $link)
        {
            $sirelo_websites_list[$link->sihy_co_code] = [
                'website_name' => $link->we_website,
                'link' => $link->sihy_link
            ];
        }

        foreach ($sirelo_websites as $website)
        {
            if (array_key_exists($website->we_sirelo_co_code, $sirelo_websites_list))
            {
                $websites[$website->we_sirelo_co_code] = [
                    'website_name' => $website->we_website,
                    'link' => $sirelo_websites_list[$website->we_sirelo_co_code]['link'],
                ];
            }
            else
            {
                $websites[$website->we_sirelo_co_code] = [
                    'website_name' => $website->we_website,
                    'link' => ""
                ];
            }
        }

        return view('membership.edit',
            [
                'membership' => $membership,
                'websites' => $websites,

            ]);

    }

    public function update(Request $request, $membership_id)
    {
        $request->validate([
            'name' => 'required',
            'location' => 'required',
            'website' => 'required'
        ]);

        $membership = Membership::findOrFail($membership_id);

        //Save basics
        $membership->me_name = $request->name;
        $membership->me_location = $request->location;
        $membership->me_website = $request->website;

        $membership->save();

        //Loop through every sirelo link post
        foreach ($request->sirelo as $co_code => $link)
        {
            $sireloLink = SireloLink::where("sihy_co_code", $co_code)->where("sihy_me_id", $membership_id)->first();

            //check if link already exists
            if (count($sireloLink) > 0)
            {
                //Update hyperlink
                $sireloLink->sihy_link = $link;
                $sireloLink->sihy_timestamp = date("Y-m-d H:i:s");
                $sireloLink->save();
            }
            else
            {
                //Create hyperlink
                $newLink = new SireloLink();

                $newLink->sihy_co_code = $co_code;
                $newLink->sihy_me_id = $membership_id;
                $newLink->sihy_link = ((empty($link)) ? "" : $link);
                $newLink->sihy_timestamp = date("Y-m-d H:i:s");

                $newLink->save();
            }
        }

        return redirect('admin/memberships');
    }

    public function delete(Request $request)
    {
        DB::table("sirelo_hyperlinks")
            ->where("sihy_me_id", $request->id)
            ->delete();

        DB::table("kt_customer_membership")
            ->where("ktcume_me_id", $request->id)
            ->delete();

        DB::table("memberships")
            ->where("me_id", $request->id)
            ->delete();
    }

}
