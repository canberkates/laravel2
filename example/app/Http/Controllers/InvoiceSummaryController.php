<?php

namespace App\Http\Controllers;

use App\Data\DestinationType;
use App\Data\PaymentMethod;
use App\Data\PaymentReminder;
use App\Data\PortalType;
use App\Functions\System;
use App\Models\Country;
use App\Models\Customer;
use App\Models\InvoiceLine;
use App\Models\KTBankLineInvoiceCustomerLedgerAccount;
use App\Models\PaymentCurrency;
use DB;
use Illuminate\Http\Request;
use Log;

class InvoiceSummaryController extends Controller
{
	public function index()
	{

		return view('finance.invoicesummary', [
			'selected_period' => "",
			'selected_invoicedate' => "",
			'invoices_paid_filter' => "",
			'customers' => Customer::select("cu_id", "cu_company_name_legal")->whereIn("cu_type", [1,2,4,6])->where("cu_deleted", "=", "0")->get()
		]);
	}

	public function filteredIndex(Request $request)
	{
		$request->validate([
			'invoicedate' => 'required',
			'period' => 'required'
		]);

		Log::debug($request);

		$system = new System();

		$date = explode( '-', $request->period );


		$invoices = DB::table("invoices")
			->select("cu_company_name_legal", "cu_co_code")
			->selectRaw("invoices.*")
			->leftJoin("customers","cu_id", "in_cu_id")
			->where("in_period_from", ">=" , date("Y-m-d", strtotime(trim($date[0]))))
			->where("in_period_to", "<=",date("Y-m-d", strtotime(trim($date[1]))))
			->whereBetween("in_date", System::betweenDates($request->invoicedate))
			->where("cu_deleted", 0);

		if(!empty($request->customer)){
			$invoices->where("in_cu_id", $request->customer);
		}

		$invoices = $invoices
			->get();


		$invoice_totals['total_amount_gross'] = 0;
		$invoice_totals['total_amount_netto'] = 0;
		$invoice_totals['total_amount_paid'] = 0;

		foreach($invoices as $key => $invoice)
		{
			Log::debug(json_encode($invoice));
			$bank_lines = KTBankLineInvoiceCustomerLedgerAccount::leftJoin("bank_lines", "ktbaliinculeac_bali_id", "bali_id")
				->where("ktbaliinculeac_in_id", "=", $invoice->in_id)
				->get();

			$amount_paired_per_invoice[$invoice->in_id] = 0;
			$payment_dates_per_invoice[$invoice->in_id] = [];

			foreach ($bank_lines as $bali)
			{
				$amount_paired_per_invoice[$invoice->in_id] += $bali->ktbaliinculeac_amount;

				$payment_dates_per_invoice[$invoice->in_id][] = (($bali->bali_date == 0) ? $bali->ktbaliinculeac_date : $bali->bali_date);
			}

			$invoice_totals['total_amount_gross'] += $invoice->in_amount_gross_eur;
			$invoice_totals['total_amount_netto'] += $invoice->in_amount_netto_eur;
			$invoice_totals['total_amount_paid'] += $amount_paired_per_invoice[$invoice->in_id];

			$invoice->days_overdue = $system->dateDifference($invoice->in_expiration_date, date("Y-m-d"));

			[$int, $nat, $lead_store] = $this->checkIntNatLeads($invoice->in_id);

			if($request->category == DestinationType::INTMOVING){
                if(!$int){
                    unset($invoices[$key]);
                }
                if($nat){
                    unset($invoices[$key]);
                }
            }elseif($request->category == DestinationType::NATMOVING){
                if(!$nat){
                    unset($invoices[$key]);
                }
                if($int){
                    unset($invoices[$key]);
                }
            }elseif($request->category == 3){
                if(!$nat || !$int){
                    unset($invoices[$key]);
                }
            }

            if($int){
                $invoice->categories[] = "INTMOVING";
            }
			if($nat){
                $invoice->categories[] = "NATMOVING";
            }
			if($lead_store){
                $invoice->categories[] = "LEADSTORE";
            }

		}

		$currency_tokens = [];

		$currencies = PaymentCurrency::all();

		foreach ($currencies as $currency)
		{
			$currency_tokens[$currency->pacu_code] = $currency->pacu_token;
		}

        foreach(Country::all() as $country){
            $countries[$country->co_code] = $country->co_en;
        }


		return view('finance.invoicesummary', [
			'selected_period' => $request->period,
			'selected_invoicedate' => $request->invoicedate,
			'invoices_paid_filter' => $request->paid,
			'invoices_category_filter' => $request->category,
			'invoices' => $invoices,
			'invoice_totals' => $invoice_totals,
			'amount_paired_per_invoice' => $amount_paired_per_invoice ?? [],
			'payment_dates_per_invoice' => $payment_dates_per_invoice ?? [],
			'paymentreminder' => PaymentReminder::all(),
			'paymentmethods' => PaymentMethod::all(),
			'currency_tokens' => $currency_tokens,
			'countries' => $countries,
			'customers' => Customer::select("cu_id", "cu_company_name_legal")->whereIn("cu_type", [1,2,4,6])->where("cu_deleted", "=", "0")->get()
		]);

	}

	private function checkIntNatLeads($invoice_id){

	    $lines = InvoiceLine::where("inli_in_id", $invoice_id)
            ->leftJoin("kt_request_customer_portal", "inli_ktrecupo_id","ktrecupo_id")
            ->leftJoin("kt_customer_portal", "ktrecupo_ktcupo_id", "ktcupo_id")
            ->leftJoin("portals", "ktrecupo_po_id", "po_id")
            ->get();

	    $int = false;
	    $nat = false;
	    $lead_store = false;

	    foreach($lines->where("ktcupo_type", PortalType::CORE) as $line){
	        if($line->po_destination_type == 1){
	            $int = true;
            }elseif ($line->po_destination_type == 2){
	            $nat = true;
            }

	        if($int && $nat){
	            break;
            }
        }

        foreach($lines->where("ktcupo_type", PortalType::STORE) as $line){
            if($line){
                $lead_store = true;
                break;
            }

        }

	    return [$int, $nat, $lead_store];

    }

}
