<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\Obligation;
use App\Models\SireloLink;
use App\Models\Website;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ObligationController extends Controller
{
    public function index()
    {
        $country_query = Country::all();

        $countries = [];

        foreach ($country_query as $co)
        {
            $countries[$co->co_code] = $co->co_en;
        }

        return view('obligation.index',
            [
                'obligations' => Obligation::all(),
                'countries' => $countries
            ]);
    }

    public function create()
    {
        return view('obligation.create', ['countries' => Country::all()]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'type' => 'required',
            'country' => 'required'
        ]);

        $obligation = new Obligation();

        $obligation->cuob_type = $request->type;
        $obligation->cuob_co_code = $request->country;
        $obligation->cuob_name = $request->name;
        $obligation->cuob_remark = $request->remark;

        $obligation->save();

        return redirect('admin/obligations');
    }

    public function edit($obligation_id)
    {
        $obligation = Obligation::where("cuob_id", $obligation_id)->first();

        $country_query = Country::all();

        $countries = [];

        foreach ($country_query as $co)
        {
            $countries[$co->co_code] = $co->co_en;
        }

        $sirelo_websites = Website::whereRaw("we_sirelo_co_code IS NOT NULL")->get();

        $sirelo_links = Website::leftJoin("sirelo_hyperlinks", "we_sirelo_co_code", "sihy_co_code")->where("sihy_cuob_id", $obligation->cuob_id)->get();

        $websites = [];
        $sirelo_websites_list = [];

        foreach ($sirelo_links as $link)
        {
            $sirelo_websites_list[$link->sihy_co_code] = [
                'website_name' => $link->we_website,
                'link' => $link->sihy_link
            ];
        }

        foreach ($sirelo_websites as $website)
        {
            if (array_key_exists($website->we_sirelo_co_code, $sirelo_websites_list))
            {
                $websites[$website->we_sirelo_co_code] = [
                    'website_name' => $website->we_website,
                    'link' => $sirelo_websites_list[$website->we_sirelo_co_code]['link'],
                ];
            }
            else
            {
                $websites[$website->we_sirelo_co_code] = [
                    'website_name' => $website->we_website,
                    'link' => ""
                ];
            }
        }

        return view('obligation.edit',
            [
                'obligation' => $obligation,
                'websites' => $websites,
                'countries' => $countries
            ]);

    }

    public function update(Request $request, $obligation_id)
    {
        $request->validate([
            'name' => 'required',
            'type' => 'required',
            'country' => 'required'
        ]);

        $obligation = Obligation::findOrFail($obligation_id);

        //Save basics
        $obligation->cuob_type = $request->type;
        $obligation->cuob_co_code = $request->country;
        $obligation->cuob_name = $request->name;
        $obligation->cuob_remark = $request->remark;

        $obligation->save();

        //Loop through every sirelo link post
        foreach ($request->sirelo as $co_code => $link)
        {
            $sireloLink = SireloLink::where("sihy_co_code", $co_code)->where("sihy_cuob_id", $obligation_id)->first();

            //check if link already exists
            if (count($sireloLink) > 0)
            {
                //Update hyperlink
                $sireloLink->sihy_link = $link;
                $sireloLink->sihy_timestamp = date("Y-m-d H:i:s");
                $sireloLink->save();
            }
            else
            {
                //Create hyperlink
                $newLink = new SireloLink();

                $newLink->sihy_co_code = $co_code;
                $newLink->sihy_cuob_id = $obligation_id;
                $newLink->sihy_link = ((empty($link)) ? "" : $link);
                $newLink->sihy_timestamp = date("Y-m-d H:i:s");

                $newLink->save();
            }
        }

        return redirect('admin/obligations');
    }

    public function delete(Request $request)
    {
        DB::table("kt_customer_obligations")
            ->where("ktcuob_cuob_id", $request->id)
            ->delete();

        DB::table("customer_obligations")
            ->where("cuob_id", $request->id)
            ->delete();
    }

}
