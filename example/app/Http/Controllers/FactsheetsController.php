<?php

namespace App\Http\Controllers;

use App\Functions\Mail;
use App\Functions\System;
use App\Functions\Translation;
use App\Models\Country;
use App\Models\EmailBuilderTemplate;
use App\Models\Request;
use DateTime;
use Illuminate\Support\Facades\DB;

class FactsheetsController extends Controller
{
    public function index($co_code = null)
    {
        $co_code = strtoupper($co_code);

        //Set timestamps from and to
        $date_from = "2020-01-01 00:00:00";
        $date_to = "2020-12-31 23:59:59";

        $date_text_top = date("F Y", strtotime($date_from))." - ".date("F Y", strtotime($date_to));
        $date_text = date("j M Y", strtotime($date_from))." - ".date("j M Y", strtotime($date_to));

        //Get country name
        $country = Country::where("co_code", $co_code)->first();
        $country_name = strtoupper($country->co_en);

        $all_requests = DB::table('requests')
            ->whereIn("re_status", [1,2])
            ->whereIn("re_rejection_reason", [0,2,3])
            ->whereBetween("re_timestamp", [date("Y-m-d", strtotime($date_from)), date("Y-m-d", strtotime($date_to))])
            ->get();


        //Calculate total removals, import and export for X country
        $removals = $this->calculateRemovalsForCountry($all_requests, $co_code);
        $total_removals = $removals['total_removals'];
        $import_removals = $removals['import_removals'];
        $export_removals = $removals['export_removals'];

        $removals_per_month = $this->calculateRemovalsPerMonth($co_code, $date_from, $date_to);
        $all_removals = $removals_per_month['all_requests'];
        unset($removals_per_month['all_requests']);

        //Calculate total import and export per continent
        $removals_per_continent = $this->calculateImportExportPerContinent($all_requests);

        /*$all_requests = Request::whereIn("re_status", [1,2])
            ->whereIn("re_rejection_reason", [0,2,3])
            ->whereBetween("re_timestamp", [date("Y-m-d", strtotime($date_from)), date("Y-m-d", strtotime($date_to))])
            ->get();*/

        $donut_continent_removals = [];

        foreach ($removals_per_continent as $continent => $rpc) {
           $donut_continent_removals[$continent] = $rpc['import'] + $rpc['export'];
        }

        /*$donut_continent_removals = [
            1 => 3, //EUROPE
            2 => 2, //ASIA MIDDLE EAST
            3 => 2, //AMERICA
            4 => 3, //AFRICA
            5 => 3 //OCEANIA
        ];*/

        return view('factsheets.factsheet',
            [
                'web' => 1,
                //'background' => $background,
                'country_name' => $country_name,
                'co_code' => $co_code,
                'date_text_top' => $date_text_top,
                'date_text' => $date_text,

                //Removals
                'total_removals' => number_format($total_removals, 0, ",", "."),
                'import_removals' => number_format($import_removals, 0, ",", "."),
                'export_removals' => number_format($export_removals, 0, ",", "."),

                //Removals per continent
                'removals_per_continent' => $removals_per_continent,

                //Removals per month
                'removals_per_month' => $removals_per_month,
                'all_removals' => $all_removals,

                //Donut
                'donut_colors' => ["#f58221", "#214e96", "#1ba7da", "#f01347", "#5cbf64"],
                'donut_continent_removals' => $donut_continent_removals,

            ]);
    }

    private function calculateRemovalsPerMonth($co_code, $date_from, $date_to) {
        $removals_per_month = [
            'all_requests' => 0,
        ];

        //@TODO where re_co_code_from != re_co_code_to
        $requests_per_month = Request::selectRaw("COUNT(*) AS `requests`, MONTH(`re_timestamp`) AS `m`")
            ->whereBetween("re_timestamp", [date("Y-m-d", strtotime($date_from)), date("Y-m-d", strtotime($date_to))])
            ->where(function ($query) use($co_code) {
                $query->where('re_co_code_from', '=', $co_code)
                    ->orWhere('re_co_code_to', '=', $co_code);
            })
            ->whereIn("re_status", [1,2])
            ->whereIn("re_rejection_reason", [0,2,3])
            ->whereRaw("re_co_code_from != re_co_code_to")
            ->groupBy("m")
            ->orderBy("re_timestamp")
            ->get();

        foreach ($requests_per_month as $val) {
            //Get month name based on month number
            $date = DateTime::createFromFormat('!m', intval($val->m));
            $monthName = $date->format('M');

            $removals_per_month[$monthName] = intval($val->requests);
            $removals_per_month['all_requests'] += intval($val->requests);
        }

        return $removals_per_month;
    }

    private function calculateRemovalsForCountry($all_requests, $co_code) {
        $removals = [
            'total_removals' => 0,
            'import_removals' => 0,
            'export_removals' => 0,
        ];

        $all_requests = $all_requests->filter(function($item) use ($co_code) {
            return ($item->re_co_code_from == $co_code || $item->re_co_code_to == $co_code) && $item->re_co_code_from != $item->re_co_code_to;
        });

        foreach($all_requests as $values)
        {
            if($values->re_co_code_from !== $co_code && $values->re_co_code_to === $co_code)
            {
                //Counter up import
                $removals['import_removals']++;
                $removals['total_removals']++;
            }

            if($values->re_co_code_from === $co_code && $values->re_co_code_to !== $co_code)
            {
                //Counter up export
                $removals['export_removals']++;
                $removals['total_removals']++;
            }
        }

        return $removals;
    }

    private function calculateImportExportPerContinent($requests) {
        $removals_per_continent = [
            'europe' => ['import' => 0, 'export' => 0],
            'america' => ['import' => 0, 'export' => 0],
            'oceania' => ['import' => 0, 'export' => 0],
            'asia_middle_east' => ['import' => 0, 'export' => 0],
            'africa' => ['import' => 0, 'export' => 0],
            'other' => ['import' => 0, 'export' => 0],
        ];

        /*$requests = DB::table('requests')
            ->whereIn("re_status", [1,2])
            ->whereIn("re_rejection_reason", [0,2,3])
            ->whereBetween("re_timestamp", [date("Y-m-d", strtotime($date_from)), date("Y-m-d", strtotime($date_to))])
            ->get();*/


        $countryToContinentID = $this->countryToContinentID();
        $continentCombiner = $this->continentCombiner();

        foreach ($requests as $value) {
            $country_from = $value->re_co_code_from;
            $country_to = $value->re_co_code_to;

            if ($country_from == $country_to) {
                continue;
            }

            $con_id_from = $countryToContinentID[$country_from];
            $con_id_to = $countryToContinentID[$country_to];

            $continent_from = $continentCombiner[$con_id_from];
            $continent_to = $continentCombiner[$con_id_to];

            $removals_per_continent[$continent_from]['export']++;
            $removals_per_continent[$continent_to]['import']++;
        }



        return $removals_per_continent;
    }

    private function continentCombiner() {
        $list = [
            //LOCAL
            1 => "africa",
            2 => "oceania",
            3 => "asia_middle_east",
            4 => "europe",
            5 => "europe",
            6 => "europe",
            7 => "europe",
            8 => "europe",
            9 => "europe",
            10 => "europe",
            11 => "america",
            12 => "america",
            13 => "asia_middle_east",
            14 => "other",

            //LIVE
            15 => "africa",
            16 => "asia_middle_east",
            17 => "europe",
            18 => "asia_middle_east",
            19 => "america",
            20 => "oceania",
            21 => "other",
            22 => "america"
        ];

        return $list;
    }

    private function countryToContinentID() {
        $list = [];

        $countries = Country::all();

        foreach ($countries as $country) {
            $list[$country->co_code] = $country->co_con_id;
        }

        return $list;
    }
}
