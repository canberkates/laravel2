<?php

namespace App\Http\Controllers;

use App\Data\BankLineSource;
use App\Data\CustomerType;
use App\Data\LedgerAccountType;
use App\Data\PaymentMethod;
use App\Functions\Mover;
use App\Functions\DataIndex;
use App\Functions\System;
use App\Models\BankLine;
use App\Models\Country;
use App\Models\Customer;
use App\Models\Invoice;
use App\Models\KTBankLineInvoiceCustomerLedgerAccount;
use App\Models\LedgerAccount;
use App\Models\PaymentCurrency;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Log;

class PairBankLinesController extends Controller
{
    public function index()
    {
        $bank_lines = DB::table("bank_lines")
            ->where("bali_source", 1)
            ->whereBetween("bali_date", [date("Y-m-d", strtotime(date("Y") . "-01-01")), date("Y-m-d", strtotime(date("Y") . "-12-31"))])
            ->groupBy("bali_id")
            ->get();

        $total_credit = 0;
        $total_debit = 0;

        foreach ($bank_lines as $key => $row)
        {

            $amount_paired = 0;
            $amount_left = $row->bali_amount;

            $customer_ledger_account = DB::table("kt_bank_line_invoice_customer_ledger_account")
                ->leftJoin("customers", "ktbaliinculeac_cu_id", "cu_id")
                ->where("ktbaliinculeac_bali_id", $row->bali_id)
                ->whereRaw("(
                    `customers`.`cu_deleted` = '0' OR
                    `customers`.`cu_deleted` IS NULL
                )")
                ->get();

            foreach ($customer_ledger_account as $row_bank_line_invoice_customer_ledger_account)
            {
                $amount_paired += $row_bank_line_invoice_customer_ledger_account->ktbaliinculeac_amount;
                $amount_left -= $row_bank_line_invoice_customer_ledger_account->ktbaliinculeac_amount;
            }

            if (round($amount_left, 2) != 0)
            {
                $bank_lines[$key]->amount_paired = $amount_paired;
                $bank_lines[$key]->amount_left = $amount_left;

                if ($row->bali_credit_debit == "credit")
                {
                    $total_credit += $row->bali_amount;
                } elseif ($row->bali_credit_debit == "debit")
                {
                    $total_debit += $row->bali_amount;
                }

            } else
            {
                unset($bank_lines[$key]);
            }
        }

        $paymentcurrencies = [];

        foreach (PaymentCurrency::all() as $pc)
        {
            $paymentcurrencies[$pc->pacu_code] = $pc->pacu_token;
        }


        return view('finance.banklines.index', [
            'selected_period' => "",
            'selected_statement' => "",
            'selected_ledger' => "",
            'selected_source' => 1,
            'open' => 1,
            'paired' => 0,
            'ledgeraccounts' => LedgerAccount::all(),
            'paymentcurrencies' => $paymentcurrencies,
            'bank_lines' => $bank_lines,
            'total_debit' => $total_debit,
            'total_credit' => $total_credit,
            'sources' => BankLineSource::all()
        ]);
    }

    public function filteredIndex(Request $request)
    {
        $system = new System();

        $open = Arr::exists($request, 'open') ? 1 : 0;
        $paired = Arr::exists($request, 'paired') ? 1 : 0;

        if (intval($request->source) === BankLineSource::BANK_LINE)
        {

            $bank_lines = DB::table("bank_lines");

            if (!empty($request->ledger))
            {
                $bank_lines->leftJoin("kt_bank_line_invoice_customer_ledger_account", "bali_id", "ktbaliinculeac_bali_id");
            }

            $bank_lines->whereBetween("bali_date", System::betweenDates($request->period));

            if (!empty($request->statement_number))
            {
                $bank_lines->where("bali_statement_number", $request->statement_number);
            }

            if (!empty($request->ledger))
            {
                $bank_lines->where("ktbaliinculeac_leac_number", $request->ledger);
            }

            $bank_lines = $bank_lines
                ->where("bali_source", BankLineSource::BANK_LINE)
                ->groupBy("bali_id")
                ->get();

            $total_credit = 0;
            $total_debit = 0;

            if ($paired || $open)
            {
                foreach ($bank_lines as $key => $row)
                {
                    $amount_paired = 0;
                    $amount_left = $row->bali_amount;

                    $customer_ledger_account = DB::table("kt_bank_line_invoice_customer_ledger_account")
                        ->leftJoin("customers", "ktbaliinculeac_cu_id", "cu_id")
                        ->where("ktbaliinculeac_bali_id", $row->bali_id)
                        ->whereRaw("(
                            `customers`.`cu_deleted` = '0' OR
                            `customers`.`cu_deleted` IS NULL
                          )")
                        ->get();

                    foreach ($customer_ledger_account as $row_bank_line_invoice_customer_ledger_account)
                    {
                        $amount_paired += $row_bank_line_invoice_customer_ledger_account->ktbaliinculeac_amount;
                        $amount_left -= $row_bank_line_invoice_customer_ledger_account->ktbaliinculeac_amount;
                    }


                    if ($open && !$paired && (round($amount_left, 2) == 0))
                    {
                        unset($bank_lines[$key]);
                        continue;
                    }

                    if ($paired && !$open && (round($amount_left, 2) != 0))
                    {
                        unset($bank_lines[$key]);
                        continue;
                    }

                    $bank_lines[$key]->amount_paired = $amount_paired;
                    $bank_lines[$key]->amount_left = $amount_left;

                    if ($row->bali_credit_debit == "credit")
                    {
                        $total_credit += $row->bali_amount;
                    } elseif ($row->bali_credit_debit == "debit")
                    {
                        $total_debit += $row->bali_amount;
                    }

                }
            }
            $paymentcurrencies = [];

            foreach (PaymentCurrency::all() as $pc)
            {
                $paymentcurrencies[$pc->pacu_code] = $pc->pacu_token;
            }

            return view('finance.banklines.index', [
                'selected_period' => $request->period,
                'selected_statement' => $request->statement_number,
                'selected_ledger' => $request->ledger,
                'selected_source' => $request->source,
                'open' => $open,
                'paired' => $paired,
                'ledgeraccounts' => LedgerAccount::all(),
                'paymentcurrencies' => $paymentcurrencies,
                'bank_lines' => ($paired || $open ? $bank_lines : []),
                'total_debit' => $total_debit,
                'total_credit' => $total_credit,
                'sources' => BankLineSource::all()
            ]);
        } else
        {
            $bank_lines = DB::table("bank_lines")
                ->leftJoin("customers", "cu_id", "bali_debtor_cu_id")
                ->leftJoin("invoices", "in_id", "bali_debtor_in_id");

            if (!empty($request->ledger))
            {
                $bank_lines->leftJoin("kt_bank_line_invoice_customer_ledger_account", "bali_id", "ktbaliinculeac_bali_id");
            }

            $bank_lines->whereBetween("bali_timestamp", System::betweenDates($request->period));

            if (!empty($request->statement_number))
            {
                $bank_lines->where("bali_statement_number", $request->statement_number);
            }

            if (!empty($request->ledger))
            {
                $bank_lines->where("ktbaliinculeac_leac_number", $request->ledger);
            }

            $bank_lines = $bank_lines
                ->where("bali_source", $request->source)
                ->groupBy("bali_id")
                ->get();

            $total_credit = 0;
            $total_debit = 0;

            foreach ($bank_lines as $key => $row)
            {

                $customer_ledger_account = DB::table("kt_bank_line_invoice_customer_ledger_account")
                    ->leftJoin("customers", "ktbaliinculeac_cu_id", "cu_id")
                    ->where("ktbaliinculeac_bali_id", $row->bali_id)
                    ->whereRaw("(
                        `customers`.`cu_deleted` = '0' OR
                        `customers`.`cu_deleted` IS NULL
                     )")
                    ->get();

                $amount_paired = 0;
                if($row->bali_currency == "EUR"){
                    $amount_left = $row->bali_amount;
                }else{
                    $amount_left = $row->bali_amount_fc;
                }


                foreach ($customer_ledger_account as $row_bank_line_invoice_customer_ledger_account)
                {
                    if($row->bali_currency == "EUR"){

                        $amount_paired += $row_bank_line_invoice_customer_ledger_account->ktbaliinculeac_amount;
                        $amount_left -= $row_bank_line_invoice_customer_ledger_account->ktbaliinculeac_amount;
                    }else{
                        $amount_paired += $row_bank_line_invoice_customer_ledger_account->ktbaliinculeac_amount_FC;
                        $amount_left -= $row_bank_line_invoice_customer_ledger_account->ktbaliinculeac_amount_FC;
                    }

                }

                if ($open && !$paired && (round($amount_left, 2) == 0))
                {
                    unset($bank_lines[$key]);
                    continue;
                }

                if ($paired && !$open && (round($amount_left, 2) != 0))
                {
                    unset($bank_lines[$key]);
                    continue;
                }

                $bank_lines[$key]->amount_paired = $amount_paired;
                $bank_lines[$key]->amount_left = $amount_left;

                $details = "<pre>" . print_r($system->unserialize($row->bali_adyen_data), true) . "</pre>";
                $details = str_replace("\"", "'", $details);

                $bank_lines[$key]->details = $details;

                if ($row->bali_credit_debit == "credit")
                {
                    $total_credit += $row->bali_amount;
                } elseif ($row->bali_credit_debit == "debit")
                {
                    $total_debit += $row->bali_amount;
                }

            }

            $paymentcurrencies = [];

            foreach (PaymentCurrency::all() as $pc)
            {
                $paymentcurrencies[$pc->pacu_code] = $pc->pacu_token;
            }

            return view('finance.banklines.index', [
                'selected_period' => $request->period,
                'selected_statement' => $request->statement_number,
                'selected_ledger' => $request->ledger,
                'selected_source' => $request->source,
                'open' => $open,
                'paired' => $paired,
                'ledgeraccounts' => LedgerAccount::all(),
                'paymentcurrencies' => $paymentcurrencies,
                'bank_lines' => ($paired || $open ? $bank_lines : []),
                'total_debit' => $total_debit,
                'total_credit' => $total_credit,
                'sources' => BankLineSource::all()
            ]);
        }

    }

    public function pair($bank_line_id)
    {
        $start = microtime(true);

        $system = new System();
        $bank_line = BankLine::findOrFail($bank_line_id);

        //Get customers
        $customers = DB::table("customers")
            ->select("cu_id", "cu_company_name_legal", "cu_type", "cu_city", "cu_co_code")
            ->where("cu_deleted", 0)
            ->get()
            ->keyBy('cu_id');

        //Get invoices
        $invoices = DB::table("invoices")
            ->select("in_id", "in_cu_id", "in_number", "in_currency", "in_amount_netto", "in_amount_netto_eur")
            ->get()
            ->keyBy('in_id')
            ->map(function ($item) {
                $item->paired = 0;
                $item->paired_FC = 0;

                return $item;
            });

        //=> Pair bank lines
        $ktbali_query = DB::table("kt_bank_line_invoice_customer_ledger_account")
            ->select("ktbaliinculeac_in_id", "ktbaliinculeac_amount", "ktbaliinculeac_amount_FC", "ktbaliinculeac_currency")
            ->get();


        foreach ($ktbali_query as $ktbali_row)
        {
            $invoices[$ktbali_row->ktbaliinculeac_in_id]->paired += $ktbali_row->ktbaliinculeac_amount;
            $invoices[$ktbali_row->ktbaliinculeac_in_id]->paired_FC += $ktbali_row->ktbaliinculeac_amount_FC;
        }

        $currency_tokens = [];

        $currencies = PaymentCurrency::all();

        foreach ($currencies as $currency)
        {
            $currency_tokens[$currency->pacu_code] = $currency->pacu_token;
        }

        $eur = "€";
        $found_invoices = [];

        foreach ($invoices as $in_id => $invoice)
        {
            if ($invoice->in_amount_netto_eur == round($invoice->paired, 2))
            {
                continue;
            }

            $netto_eur = System::numberFormat($invoice->in_amount_netto_eur, 2);
            $paired = System::numberFormat($invoice->paired, 2);
            $paired_fc = System::numberFormat($invoice->paired_FC, 2);

            //Calculate amount left FC
            if ($invoice->in_currency != "EUR")
            {
                $foreign = " [" . $currency_tokens[$invoice->in_currency] . " " . System::numberFormat($invoice->in_amount_netto, 2) . " / " . $currency_tokens[$invoice->in_currency] . " {$paired_fc}]";
            } else
            {
                $foreign = "";
            }

            $found_invoices[$in_id] = $invoice->in_number . " ({$eur} {$netto_eur} / {$eur} {$paired}) {$foreign} {$customers[$invoice->in_cu_id]->cu_company_name_legal}";
        }

        //Build the form
        $ledger_accounts = [
            LedgerAccountType::name(LedgerAccountType::COSTS) => [],
            LedgerAccountType::name(LedgerAccountType::BALANCE) => [],
        ];

        foreach (LedgerAccount::whereIn("leac_type", [2, 3])->where("leac_number", "!=", 120500)->get() as $cl)
        {
            $ledger_accounts[LedgerAccountType::name($cl->leac_type)][$cl->leac_number] = $cl->leac_number . " (" . $cl->leac_name . ")";
        }

        foreach (Country::all() as $country)
        {
            $countries[$country->co_code] = $country->co_en;
        }

        $found_customers = [
            CustomerType::name(CustomerType::MOVER) => [],
            CustomerType::name(CustomerType::SERVICE_PROVIDER) => [],
            CustomerType::name(CustomerType::OTHER) => [],
            CustomerType::name(CustomerType::LEAD_RESELLER) => [],
        ];

        foreach ($customers as $customer)
        {
            $found_customers[CustomerType::name($customer->cu_type)][$customer->cu_id] = $customer->cu_company_name_legal . " / " . $customer->cu_city . ", " . (!empty($customer->cu_co_code) ? $countries[$customer->cu_co_code] : "-");
        }

        foreach (LedgerAccount::all() as $ledger)
        {
            $ledgeraccounts[$ledger->leac_number] = $ledger->leac_number . " (" . $ledger->leac_name . ")";
        }

        $extras = [
            "amount_left" => $bank_line->bali_amount,
            "amount_left_fc" => $bank_line->bali_amount_fc,
            "eur" => $eur,
            'ledger_accounts' => $ledgeraccounts
        ];

        //Find coupled lines
        $paired_bank_lines = DB::table("kt_bank_line_invoice_customer_ledger_account")
            ->leftJoin("customers", "ktbaliinculeac_cu_id", "cu_id")
            ->leftJoin("invoices", "ktbaliinculeac_in_id", "in_id")
            ->select(
                "in_number", "customers.cu_company_name_legal", "ktbaliinculeac_id", "ktbaliinculeac_in_id", "ktbaliinculeac_cu_id", "ktbaliinculeac_bali_id",
                "ktbaliinculeac_leac_number", "ktbaliinculeac_amount", "ktbaliinculeac_amount_FC", "ktbaliinculeac_currency")
            ->where("ktbaliinculeac_bali_id", $bank_line->bali_id)
            ->get();

        foreach ($paired_bank_lines as $paired)
        {
            $extras["amount_left"] -= $paired->ktbaliinculeac_amount;
            $extras["amount_left_fc"] -= $paired->ktbaliinculeac_amount_FC;
        }

        foreach (PaymentCurrency::all() as $currency)
        {
            $currency_tokens[$currency->pacu_code] =
                [
                    "token" => $currency->pacu_token,
                    "name" => $currency->pacu_name,
                ];
        }

        //Set variables for PAIR BALANCE TO INVOICE
        $show_pair_balance_to_invoice_button = false;
        $customer = null;
        $customer_paired = null;

        //Query to show 'PAIR BALANCE TO INVOICE' button or not (ONLY EUR CURRENCY FOR NOW)
        $kt_bank_line_invoice_customer = KTBankLineInvoiceCustomerLedgerAccount::where("ktbaliinculeac_bali_id", $bank_line->bali_id)
            ->whereNotNull("ktbaliinculeac_cu_id")
            ->whereNull("ktbaliinculeac_in_id")
            ->first();

        if (count($kt_bank_line_invoice_customer) > 0)
        {
            $show_pair_balance_to_invoice_button = true;

            $customer_paired = Customer::select("cu_id", "cu_payment_method")
                ->where("cu_id", $kt_bank_line_invoice_customer->ktbaliinculeac_cu_id)
                ->first();
        }

        $balance_on_customer_ids = [];
        /*foreach ($paired_bank_lines as $bank_line) {
            if ($bank_line->ktbaliinculeac_leac_number == 120500
                && empty($bank_line->ktbaliinculeac_in_id)
                && !empty($bank_line->ktbaliinculeac_cu_id))
            {
                $balance_on_customer_ids[] = $bank_line->ktbaliinculeac_cu_id;
            }
        }*/

        foreach ($paired_bank_lines as $bl) {
            if (($bl->ktbaliinculeac_leac_number == 120500 || $bl->ktbaliinculeac_leac_number == 120503)
                && empty($bl->ktbaliinculeac_in_id)
                && !empty($bl->ktbaliinculeac_cu_id))
            {
                $balance_on_customer_ids[$bl->ktbaliinculeac_cu_id] = $bl->ktbaliinculeac_cu_id;
            }
        }


        return view('finance.banklines.pair', [
            'bank_line' => $bank_line,
            'paired_extras' => $extras,
            'paired_bank_lines' => $paired_bank_lines,
            'found_invoices' => $found_invoices,
            'found_customers' => $found_customers,
            'ledger_accounts' => $ledger_accounts,
            'currencies' => $currency_tokens,
            'customer_paired' => $customer_paired,
            //'balance_on_customer_ids' => json_encode($balance_on_customer_ids),
            'balance_on_customer_ids' => $balance_on_customer_ids,
            'show_pair_balance_to_invoice_button' => $show_pair_balance_to_invoice_button
        ]);
    }

    public function getBankLineRecords(Request $request)
    {
        $html = '';
        $html .= "<table class='table display' style='width: 100%;'>";
        $html .= "<thead>";
        $html .= "<tr>";
        $html .= "<th>ID</th>";
        $html .= "<th>Invoice number</th>";
        $html .= "<th>Customer</th>";
        $html .= "<th>Ledger account</th>";
        $html .= "<th>Amount</th>";
        $html .= "<th>Unpair</th>";
        $html .= "</tr>";
        $html .= "</thead>";
        $html .= "<tbody>";

        $customer_ledger_account = DB::table("kt_bank_line_invoice_customer_ledger_account")
            ->leftJoin("invoices", "ktbaliinculeac_in_id", "in_id")
            ->leftJoin("customers", "ktbaliinculeac_cu_id", "cu_id")
            ->where("ktbaliinculeac_bali_id", $request->id)
            ->whereRaw("(
                    `customers`.`cu_deleted` = '0' OR
                    `customers`.`cu_deleted` IS NULL
			    )")
            ->get();

        foreach ($customer_ledger_account as $row)
        {
            if ($row->ktbaliinculeac_leac_number)
            {
                $ledger = LedgerAccount::where("leac_number", $row->ktbaliinculeac_leac_number)->first();
            }

            $html .= '<tr>';
            $html .= '<td>' . $row->ktbaliinculeac_id . '</td>';
            $html .= '<td>' . ($row->ktbaliinculeac_in_id != 0 ? $row->in_number : "-") . '</td>';
            $html .= '<td>' . ($row->ktbaliinculeac_cu_id != 0 ? $row->cu_company_name_legal : "-") . '</td>';
            $html .= '<td>' . ($ledger ? $ledger->leac_number . " (" . $ledger->leac_name . ")" : "-") . '</td>';
            $html .= '<td>€' . number_format($row->ktbaliinculeac_amount, 2, ",", ".") . '</td>';
            $html .= '<td><button type=\'button\' class=\'btn btn-sm btn-primary paired_bankline_delete js-tooltip-enabled\' data-toggle=\'tooltip\' data-id=\'' . $row->ktbaliinculeac_id . '\' data-original-title=\'Delete\'><i class=\'fa fa-times\'></i></button></td>';
            $html .= '</tr>';

        }
        $html .= '</tbody>';
        $html .= '</table>';


        return json_encode($html);
    }

    public function delete(Request $request)
    {
        //Update amount on invoice paid if line has invoice
        $kt_bank_line_invoice = DB::table("kt_bank_line_invoice_customer_ledger_account")
            ->select("ktbaliinculeac_in_id","ktbaliinculeac_amount", "in_amount_netto_eur_paid")
            ->leftJoin("invoices", "in_id", "ktbaliinculeac_in_id")
            ->whereNotNull("ktbaliinculeac_in_id")
            ->where("ktbaliinculeac_id", $request->id)
            ->first();

        if($kt_bank_line_invoice){
            DB::table('invoices')
                ->where('in_id', $kt_bank_line_invoice->ktbaliinculeac_in_id)
                ->update(
                    [
                        'in_amount_netto_eur_paid' => ($kt_bank_line_invoice->in_amount_netto_eur_paid - $kt_bank_line_invoice->ktbaliinculeac_amount)
                    ]
                );
        }

        DB::table('kt_bank_line_invoice_customer_ledger_account')->where('ktbaliinculeac_id', '=', $request->id)->delete();


    }

    public function getInvoiceFields(Request $request)
    {
        $invoice = Invoice::where("in_id", $request->invoice_id)->first();

        if (!empty($invoice))
        {
            $customer_ledger_account = DB::table("kt_bank_line_invoice_customer_ledger_account")
                ->where("ktbaliinculeac_in_id", $invoice->in_id)
                ->get();

            $amount_left = $invoice->in_amount_netto_eur;
            $amount_left_fc = $invoice->in_amount_netto;

            foreach ($customer_ledger_account as $row_bank_line_invoice_customer_ledger_account)
            {
                $amount_left -= $row_bank_line_invoice_customer_ledger_account->ktbaliinculeac_amount;
                $amount_left_fc -= $row_bank_line_invoice_customer_ledger_account->ktbaliinculeac_amount_fc;
            }

            $invoice->amount_left = $amount_left;
            $invoice->amount_left_fc = $amount_left_fc;

            echo json_encode($invoice);
        }
    }

    public function pairEuroLine(Request $request, $bali_id)
    {
        Log::debug($request);
        Log::debug($bali_id);

        if(empty($request->pair_amount) && empty($request->pair_ledger_account_amount)){
            return redirect()->back()->withErrors([ 'errors' => "Please enter a valid pair/ledger amount! Pair: ".$request->pair_amount. " Ledger: ".intval($request->pair_ledger_account_amount)]);
        }

        $system = new System();
        $mover = new Mover();
        $bank_line = BankLine::findOrFail($bali_id);

        //Query to get the right line to edit the line when clicked on 'PAIR BALANCE TO INVOICE' (ONLY EUR CURRENCY FOR NOW)
        $kt_bank_line_invoice_customer = KTBankLineInvoiceCustomerLedgerAccount::where("ktbaliinculeac_bali_id", $bali_id)
            //->whereRaw("ktbaliinculeac_cu_id IS NOT NULL")
            ->where("ktbaliinculeac_cu_id", $request->pair_customer)
            ->whereRaw("ktbaliinculeac_in_id IS NULL")
            ->first();

        if(!empty($request->pair_amount) || !empty($request->pair_ledger_account_amount)){

            if (Arr::exists($request, 'pair_balance_to_invoice') && count($kt_bank_line_invoice_customer) > 0)
            {

                $total_deficit = $request->pair_amount + ($request->pair_ledger_account_amount ?? 0);

                if($total_deficit > $kt_bank_line_invoice_customer->ktbaliinculeac_amount){
                    return redirect()->back()->withErrors([ 'errors' => "You're trying to pair more in the invoice + ledger than the customer has as balance. You are pairing: ".$total_deficit. ". Available: ".$kt_bank_line_invoice_customer->ktbaliinculeac_amount]);
                }
            }

        }

        if ($request->pair_invoice)
        {
            if ($request->pair_amount)
            {
                $legder = ($bank_line->bali_source == 2 ? 120503 : 120500);

                if (Arr::exists($request, 'pair_balance_to_invoice') && count($kt_bank_line_invoice_customer) > 0)
                {
                    $start_amount_EUR = $kt_bank_line_invoice_customer->ktbaliinculeac_amount;
                    $start_cu_id = $kt_bank_line_invoice_customer->ktbaliinculeac_cu_id;

                    $kt_bank_line_invoice_customer->delete();

                    if (($start_amount_EUR - $request->pair_amount) > 0)
                    {
                        $kt_bali_invoice_customer = new KTBankLineInvoiceCustomerLedgerAccount();
                        $kt_bali_invoice_customer->ktbaliinculeac_bali_id = $bank_line->bali_id;
                        $kt_bali_invoice_customer->ktbaliinculeac_date = Carbon::now()->toDateTimeString();
                        $kt_bali_invoice_customer->ktbaliinculeac_cu_id = $start_cu_id;
                        $kt_bali_invoice_customer->ktbaliinculeac_currency = $request->currency;
                        $kt_bali_invoice_customer->ktbaliinculeac_amount_FC = $start_amount_EUR - $request->pair_amount;
                        $kt_bali_invoice_customer->ktbaliinculeac_amount = $start_amount_EUR - $request->pair_amount;
                        $kt_bali_invoice_customer->ktbaliinculeac_leac_number = $legder;

                        $kt_bali_invoice_customer->save();
                    }
                }

                $legder = ($bank_line->bali_source == 2 ? 120503 : 120500);

                $ktline = new KTBankLineInvoiceCustomerLedgerAccount();

                $ktline->ktbaliinculeac_date = Carbon::now()->toDateTimeString();
                $ktline->ktbaliinculeac_bali_id = $bali_id;
                $ktline->ktbaliinculeac_in_id = $request->pair_invoice;
                $ktline->ktbaliinculeac_cu_id = $request->pair_customer;
                $ktline->ktbaliinculeac_leac_number = $legder;
                $ktline->ktbaliinculeac_amount = $request->pair_amount;
                $ktline->ktbaliinculeac_currency = "EUR";
                $ktline->ktbaliinculeac_amount_FC = $request->pair_amount;

                $ktline->save();

                //Also update Invoice
                //Update amount on invoice paid if line has invoice
                $kt_bank_line_invoice = DB::table("kt_bank_line_invoice_customer_ledger_account")
                    ->select("ktbaliinculeac_in_id","ktbaliinculeac_amount", "in_amount_netto_eur_paid")
                    ->leftJoin("invoices", "in_id", "ktbaliinculeac_in_id")
                    ->whereNotNull("ktbaliinculeac_in_id")
                    ->where("ktbaliinculeac_id", $ktline->ktbaliinculeac_id)
                    ->first();

                if($kt_bank_line_invoice){
                    DB::table('invoices')
                        ->where('in_id', $kt_bank_line_invoice->ktbaliinculeac_in_id)
                        ->update(
                            [
                                'in_amount_netto_eur_paid' => ($kt_bank_line_invoice->in_amount_netto_eur_paid + $kt_bank_line_invoice->ktbaliinculeac_amount)
                            ]
                        );
                }

                DB::table('kt_bank_line_invoice_customer_ledger_account')->where('ktbaliinculeac_id', '=', $request->id)->delete();

                $check_customer = true;
            }
        } elseif ($request->pair_customer && $request->pair_amount)
        {
            $customer_payment_method = DB::table("customers")
                ->select("cu_payment_method", "cu_company_name_business")
                ->where("cu_id", $request->pair_customer)
                ->first();

            /**
             * Calculate started balance, before pairing
             */
            if ($customer_payment_method->cu_payment_method == PaymentMethod::PREPAYMENT || $customer_payment_method->cu_payment_method == PaymentMethod::PREPAYMENT_AUTO_CHARGE_CREDIT_CARD || $customer_payment_method->cu_payment_method == PaymentMethod::PREPAYMENT_AUTO_DIRECT_DEBIT)
            {
                //Get current balances of customer
                $balance_started = $mover->customerBalancesFC([$request->pair_customer])[$request->pair_customer];
            }

            $legder = ($bank_line->bali_source == BankLineSource::ADYEN ? 120503 : 120500);

            $ktline = new KTBankLineInvoiceCustomerLedgerAccount();

            $ktline->ktbaliinculeac_date = Carbon::now()->toDateTimeString();
            $ktline->ktbaliinculeac_bali_id = $bali_id;
            $ktline->ktbaliinculeac_cu_id = $request->pair_customer;
            $ktline->ktbaliinculeac_leac_number = $legder;
            $ktline->ktbaliinculeac_amount = $request->pair_amount;
            $ktline->ktbaliinculeac_currency = "EUR";
            $ktline->ktbaliinculeac_amount_FC = $request->pair_amount;

            $ktline->save();

            //Also update Invoice
            //Update amount on invoice paid if line has invoice
            $kt_bank_line_invoice = DB::table("kt_bank_line_invoice_customer_ledger_account")
                ->select("ktbaliinculeac_in_id","ktbaliinculeac_amount", "in_amount_netto_eur_paid")
                ->leftJoin("invoices", "in_id", "ktbaliinculeac_in_id")
                ->whereNotNull("ktbaliinculeac_in_id")
                ->where("ktbaliinculeac_id", $ktline->ktbaliinculeac_id)
                ->first();

            if($kt_bank_line_invoice){
                DB::table('invoices')
                    ->where('in_id', $kt_bank_line_invoice->ktbaliinculeac_in_id)
                    ->update(
                        [
                            'in_amount_netto_eur_paid' => ($kt_bank_line_invoice->in_amount_netto_eur_paid + $kt_bank_line_invoice->ktbaliinculeac_amount)
                        ]
                    );
            }

            $check_customer = true;
        }

        if (!empty($request->pair_ledger_account) && !empty($request->pair_ledger_account_amount) && $request->pair_ledger_account_amount != 0)
        {
            $ktline = new KTBankLineInvoiceCustomerLedgerAccount();

            if (Arr::exists($request, 'pair_balance_to_invoice') && count($kt_bank_line_invoice_customer) > 0)
            {
                //Query to get the right line to edit the line when clicked on 'PAIR BALANCE TO INVOICE' (ONLY EUR CURRENCY FOR NOW)
                $kt_bank_line_invoice_customer = KTBankLineInvoiceCustomerLedgerAccount::where("ktbaliinculeac_bali_id", $bali_id)
                    ->whereRaw("ktbaliinculeac_cu_id IS NOT NULL")
                    ->whereRaw("ktbaliinculeac_in_id IS NULL")
                    ->first();

                if (($kt_bank_line_invoice_customer->ktbaliinculeac_amount - $request->pair_ledger_account_amount) == 0)
                {
                    $kt_bank_line_invoice_customer->delete();
                }
                else {
                    $kt_bank_line_invoice_customer->ktbaliinculeac_amount = $kt_bank_line_invoice_customer->ktbaliinculeac_amount - $request->pair_ledger_account_amount;
                    $kt_bank_line_invoice_customer->ktbaliinculeac_amount_FC = $kt_bank_line_invoice_customer->ktbaliinculeac_amount_FC - $request->pair_ledger_account_amount;
                    $kt_bank_line_invoice_customer->save();
                }
            }

            $ktline->ktbaliinculeac_date = Carbon::now()->toDateTimeString();
            $ktline->ktbaliinculeac_bali_id = $bali_id;
            $ktline->ktbaliinculeac_leac_number = $request->pair_ledger_account;
            $ktline->ktbaliinculeac_amount = $request->pair_ledger_account_amount;

            $ktline->save();
        }

        if ($check_customer)
        {

            $row_customer = DB::table("customers")
                ->select("cu_id", "cu_company_name_business", "cu_credit_limit", "cu_pacu_code")
                ->where("cu_id", $request->pair_customer)
                ->where("cu_credit_hold", 1)
                ->first();

            if (!empty($row_customer))
            {
                $credit_limit = $system->paymentCurrencyToken($row_customer->cu_pacu_code) . " " . System::numberFormat($row_customer->cu_credit_limit, 2);
                $received_payment = $system->paymentCurrencyToken("EUR") . " " . System::numberFormat($request->pair_amount, 2);

                $system->sendMessage(
                    array_keys(DataIndex::financeUsers()),
                    "Payment received from a customer on credit hold",
                    "Customer <strong>" . $row_customer->cu_company_name_business . "</strong> who has a credit limit of <strong>" . $credit_limit . "</strong> has received a payment of <strong>" . $received_payment . "</strong>."
                );
            }

            $row_customer2 = DB::table("customers")
                ->select("cu_id", "cu_company_name_business", "cu_credit_limit", "cu_pacu_code")
                ->where("cu_id", $request->pair_customer)
                ->where("cu_payment_method", PaymentMethod::PREPAYMENT)
                ->where("cu_credit_limit", ">", 0)
                ->first();

            if (!empty($row_customer2))
            {
                $credit_limit = $system->paymentCurrencyToken($row_customer2->cu_pacu_code) . " " . System::numberFormat($row_customer2->cu_credit_limit, 2);
                $received_payment = $system->paymentCurrencyToken("EUR") . " " . System::numberFormat($request->pair_amount, 2);

                $system->sendMessage(
                    array_keys(DataIndex::financeUsers()),
                    "Payment received from a prepayment customer with credit limit",
                    "Customer <strong>" . $row_customer2->cu_company_name_business . "</strong> who has a credit limit of <strong>" . $credit_limit . "</strong> has received a payment of <strong>" . $received_payment . "</strong>."
                );
            }
        }

        if (!empty($request->pair_customer) && $check_customer)
        {

            $customer_payment_method = DB::table("customers")->select("cu_payment_method", "cu_company_name_business", "cu_credit_hold", "cu_debt_manager")
                ->where("cu_id", $request->pair_customer)
                ->first();

            if ($customer_payment_method->cu_payment_method == PaymentMethod::PREPAYMENT || $customer_payment_method->cu_payment_method == PaymentMethod::PREPAYMENT_AUTO_CHARGE_CREDIT_CARD || $customer_payment_method->cu_payment_method == PaymentMethod::PREPAYMENT_AUTO_DIRECT_DEBIT)
            {
                //Get current balances of customer
                $balance_total = $mover->customerBalancesFC([$request->pair_customer])[$request->pair_customer];

                //If Customer with prepayment method has a positive balance, then remove credithold from this customer
                if ($balance_total > 0
                    && ($customer_payment_method->cu_payment_method == PaymentMethod::PREPAYMENT || $customer_payment_method->cu_payment_method == PaymentMethod::PREPAYMENT_AUTO_CHARGE_CREDIT_CARD || $customer_payment_method->cu_payment_method == PaymentMethod::PREPAYMENT_AUTO_DIRECT_DEBIT)
                    && $customer_payment_method->cu_credit_hold == 1
                    && $balance_started < 20
                    && !Arr::exists($request, 'pair_balance_to_invoice')
                )
                {

                    //Set customer credit hold
                    $get_customer = Customer::where("cu_id", $request->pair_customer)->first();
                    $get_customer->cu_credit_hold = 0;
                    $get_customer->save();

                    //Send message to Hans, so he know that the customer's credithold is removed
                    $message = "Credithold is removed from Customer: " . $customer_payment_method->cu_company_name_business . " (Cu ID = " . $request->pair_customer . ")";

                    $message_users = [
                        4459,
                        4560
                    ];

                    if (!empty($customer_payment_method->cu_debt_manager)) {
                        //Add debt manager to $finance_users array
                        $message_users[] = $customer_payment_method->cu_debt_manager;
                    }

                    $system->sendMessage($message_users, "<b>" . $customer_payment_method->cu_company_name_business . "</b> Credithold removed by Bank Line Pair", $message);
                }
            }
        }

        return redirect()->back();
    }

    public function pairFCLine(Request $request, $bali_id)
    {
        Log::debug($request);
        Log::debug($bali_id);

        $system = new System();
        $mover = new Mover();
        $bank_line = BankLine::findOrFail($bali_id);

        if($bank_line->bali_source == BankLineSource::ADYEN && empty($request->pair_invoice) && $request->currency != "EUR" && empty($request->pair_amount))
        {
            $request->pair_amount = $system->currencyConvert($request->pair_amount_fc, $request->currency, "EUR");
        }

        //Query to get the right line to edit the line when clicked on 'PAIR BALANCE TO INVOICE'
        $kt_bank_line_invoice_customer = KTBankLineInvoiceCustomerLedgerAccount::leftJoin("bank_lines", "bali_id", "ktbaliinculeac_bali_id")
            ->where("ktbaliinculeac_bali_id", $bali_id)
            //->whereRaw("ktbaliinculeac_cu_id IS NOT NULL")
            ->where("ktbaliinculeac_cu_id", $request->pair_customer)
            ->whereRaw("ktbaliinculeac_in_id IS NULL")
            ->first();

        if(!empty($request->pair_amount) || !empty($request->pair_ledger_account_amount)){

            if (Arr::exists($request, 'pair_balance_to_invoice') && count($kt_bank_line_invoice_customer) > 0)
            {
                $total_deficit = $request->pair_amount_fc;

                if($total_deficit > $kt_bank_line_invoice_customer->ktbaliinculeac_amount_FC){
                    return redirect()->back()->withErrors([ 'errors' => "You're trying to pair more in the invoice + ledger than the customer has as balance. You are pairing: ".$total_deficit. ". Available: ".$kt_bank_line_invoice_customer->ktbaliinculeac_amount_FC]);
                }
            }
        }

        $check_customer = false;

       if ($request->pair_invoice)
        {
            if ($request->pair_amount)
            {
                $legder = ($bank_line->bali_source == BankLineSource::ADYEN ? 120503 : 120500);

                $ktline = new KTBankLineInvoiceCustomerLedgerAccount();

                $ktline->ktbaliinculeac_date = Carbon::now()->toDateTimeString();
                $ktline->ktbaliinculeac_bali_id = $bali_id;
                $ktline->ktbaliinculeac_in_id = $request->pair_invoice;
                $ktline->ktbaliinculeac_cu_id = $request->pair_customer;
                $ktline->ktbaliinculeac_leac_number = $legder;
                $ktline->ktbaliinculeac_amount = $request->pair_amount;
                $ktline->ktbaliinculeac_currency = $request->currency;
                $ktline->ktbaliinculeac_amount_FC = $request->pair_amount_fc;

                $ktline->save();

                //Also update Invoice
                //Update amount on invoice paid if line has invoice
                $kt_bank_line_invoice = DB::table("kt_bank_line_invoice_customer_ledger_account")
                    ->select("ktbaliinculeac_in_id","ktbaliinculeac_amount", "in_amount_netto_eur_paid")
                    ->leftJoin("invoices", "in_id", "ktbaliinculeac_in_id")
                    ->whereNotNull("ktbaliinculeac_in_id")
                    ->where("ktbaliinculeac_id", $ktline->ktbaliinculeac_id)
                    ->first();

                if($kt_bank_line_invoice){
                    DB::table('invoices')
                        ->where('in_id', $kt_bank_line_invoice->ktbaliinculeac_in_id)
                        ->update(
                            [
                                'in_amount_netto_eur_paid' => ($kt_bank_line_invoice->in_amount_netto_eur_paid + $kt_bank_line_invoice->ktbaliinculeac_amount)
                            ]
                        );
                }

                if (Arr::exists($request, 'pair_balance_to_invoice') && count($kt_bank_line_invoice_customer) > 0)
                {
                    $start_amount_FC = $kt_bank_line_invoice_customer->ktbaliinculeac_amount_FC;
                    $start_amount_EUR = $kt_bank_line_invoice_customer->ktbaliinculeac_amount;
                    $start_cu_id = $kt_bank_line_invoice_customer->ktbaliinculeac_cu_id;

                    $kt_bank_line_invoice_customer->delete();

                    foreach (LedgerAccount::all() as $ledger)
                    {
                        $ledgeraccounts[$ledger->leac_number] = $ledger->leac_number . " (" . $ledger->leac_name . ")";
                    }

                    $boeken_op_koersverschil = (($start_amount_EUR / $start_amount_FC) - ($request->pair_amount / $request->pair_amount_fc)) * $request->pair_amount_fc;

                    if (($start_amount_FC - $request->pair_amount_fc) > 0)
                    {
                        $kt_bali_invoice_customer = new KTBankLineInvoiceCustomerLedgerAccount();
                        $kt_bali_invoice_customer->ktbaliinculeac_bali_id = $bank_line->bali_id;
                        $kt_bali_invoice_customer->ktbaliinculeac_date = Carbon::now()->toDateTimeString();
                        $kt_bali_invoice_customer->ktbaliinculeac_cu_id = $start_cu_id;
                        $kt_bali_invoice_customer->ktbaliinculeac_currency = $request->currency;
                        $kt_bali_invoice_customer->ktbaliinculeac_amount_FC = $start_amount_FC - $request->pair_amount_fc;
                        $kt_bali_invoice_customer->ktbaliinculeac_amount = ($start_amount_EUR - $request->pair_amount) - $boeken_op_koersverschil;
                        $kt_bali_invoice_customer->ktbaliinculeac_leac_number = $legder;
                        $kt_bali_invoice_customer->save();
                    }

                    if ($boeken_op_koersverschil != 0 && $kt_bank_line_invoice_customer->bali_source != 2)
                    {
                        $ktline = new KTBankLineInvoiceCustomerLedgerAccount();

                        $ktline->ktbaliinculeac_date = Carbon::now()->toDateTimeString();
                        $ktline->ktbaliinculeac_bali_id = $bank_line->bali_id;
                        $ktline->ktbaliinculeac_leac_number = 771000;
                        $ktline->ktbaliinculeac_amount = $boeken_op_koersverschil;

                        $ktline->save();
                    }
                }

                $check_customer = true;
            }
        }
        elseif ($request->pair_customer && $request->pair_amount)
        {
            $customer_payment_method = DB::table("customers")
                ->select("cu_payment_method", "cu_company_name_business")
                ->where("cu_id", $request->pair_customer)
                ->first();

           // Calculate started balance, before pairing

            if ($customer_payment_method->cu_payment_method == PaymentMethod::PREPAYMENT || $customer_payment_method->cu_payment_method == PaymentMethod::PREPAYMENT_AUTO_CHARGE_CREDIT_CARD || $customer_payment_method->cu_payment_method == PaymentMethod::PREPAYMENT_AUTO_DIRECT_DEBIT)
            {
                //Get current balances of customer
                $balance_started = $mover->customerBalancesFC([$request->pair_customer])[$request->pair_customer];
            }


            if ($request->pair_amount != 0.00) {
                $legder = ($bank_line->bali_source == 2 ? 120503 : 120500);

                $ktline = new KTBankLineInvoiceCustomerLedgerAccount();

                $ktline->ktbaliinculeac_date = Carbon::now()->toDateTimeString();
                $ktline->ktbaliinculeac_bali_id = $bali_id;
                $ktline->ktbaliinculeac_cu_id = $request->pair_customer;
                $ktline->ktbaliinculeac_leac_number = $legder;
                $ktline->ktbaliinculeac_amount = $request->pair_amount;
                $ktline->ktbaliinculeac_currency = $request->currency;
                $ktline->ktbaliinculeac_amount_FC = $request->pair_amount_fc;

                $ktline->save();
            }


            $check_customer = true;
        }

        if (!empty($request->pair_ledger_account) && !empty($request->pair_ledger_account_amount) && $request->pair_ledger_account_amount != 0 && !Arr::exists($request, 'pair_balance_to_invoice'))
        {
            $ktline = new KTBankLineInvoiceCustomerLedgerAccount();

            $ktline->ktbaliinculeac_date = Carbon::now()->toDateTimeString();
            $ktline->ktbaliinculeac_bali_id = $bali_id;
            $ktline->ktbaliinculeac_leac_number = $request->pair_ledger_account;
            $ktline->ktbaliinculeac_amount = $request->pair_ledger_account_amount;

            if($request->pair_ledger_account == "770000" || $request->pair_ledger_account == "470200" || $request->pair_ledger_account == "470105" || $request->pair_ledger_account == "771020")
            {
                $ktline->ktbaliinculeac_currency = $request->currency;
                $ktline->ktbaliinculeac_amount_FC = $request->pair_ledger_account_amount_fc;
            }

            $ktline->save();
        }

        if ($check_customer)
        {

            $row_customer = DB::table("customers")
                ->select("cu_id", "cu_company_name_business", "cu_credit_limit", "cu_pacu_code")
                ->where("cu_id", $request->pair_customer)
                ->where("cu_credit_hold", 1)
                ->first();

            if (!empty($row_customer))
            {
                $credit_limit = $system->paymentCurrencyToken($row_customer->cu_pacu_code) . " " . System::numberFormat($row_customer->cu_credit_limit, 2);
                $received_payment = $system->paymentCurrencyToken("EUR") . " " . System::numberFormat($request->pair_amount, 2);

                $system->sendMessage(
                    array_keys(DataIndex::financeUsers()),
                    "Payment received from a customer on credit hold",
                    "Customer <strong>" . $row_customer->cu_company_name_business . "</strong> who has a credit limit of <strong>" . $credit_limit . "</strong> has received a payment of <strong>" . $received_payment . "</strong>."
                );
            }

            $row_customer2 = DB::table("customers")
                ->select("cu_id", "cu_company_name_business", "cu_credit_limit", "cu_pacu_code")
                ->where("cu_id", $request->pair_customer)
                ->where("cu_payment_method", PaymentMethod::PREPAYMENT)
                ->where("cu_credit_limit", ">", 0)
                ->first();

            if (!empty($row_customer2))
            {
                $credit_limit = $system->paymentCurrencyToken($row_customer2->cu_pacu_code) . " " . System::numberFormat($row_customer2->cu_credit_limit, 2);
                $received_payment = $system->paymentCurrencyToken("EUR") . " " . System::numberFormat($request->pair_amount, 2);

                $system->sendMessage(
                    array_keys(DataIndex::financeUsers()),
                    "Payment received from a prepayment customer with credit limit",
                    "Customer <strong>" . $row_customer2->cu_company_name_business . "</strong> who has a credit limit of <strong>" . $credit_limit . "</strong> has received a payment of <strong>" . $received_payment . "</strong>."
                );
            }
        }

        if (!empty($request->pair_customer))
        {
            $customer_payment_method = DB::table("customers")->select("cu_payment_method", "cu_company_name_business", "cu_credit_hold")
                ->where("cu_id", $request->pair_customer)
                ->first();

            if ($customer_payment_method->cu_payment_method == PaymentMethod::PREPAYMENT || $customer_payment_method->cu_payment_method == PaymentMethod::PREPAYMENT_AUTO_CHARGE_CREDIT_CARD || $customer_payment_method->cu_payment_method == PaymentMethod::PREPAYMENT_AUTO_DIRECT_DEBIT)
            {
                //Get current balances of customer
                $balance_total = $mover->customerBalancesFC([$request->pair_customer])[$request->pair_customer];

                //If Customer with prepayment method has a positive balance, then remove credithold from this customer
                if ($balance_total > 0
                    && ($customer_payment_method->cu_payment_method == PaymentMethod::PREPAYMENT || $customer_payment_method->cu_payment_method == PaymentMethod::PREPAYMENT_AUTO_CHARGE_CREDIT_CARD || $customer_payment_method->cu_payment_method == PaymentMethod::PREPAYMENT_AUTO_DIRECT_DEBIT)
                    && $customer_payment_method->cu_credit_hold == 1
                    && $balance_started < 20
                    && !Arr::exists($request, 'pair_balance_to_invoice')
                )
                {

                    //Set customer credit hold
                    $get_customer = Customer::where("cu_id", $request->pair_customer)->first();
                    $get_customer->cu_credit_hold = 0;
                    $get_customer->save();

                    //Send message to Hans, so he know that the customer's credithold is removed
                    $message = "Credithold is removed from Customer: " . $customer_payment_method->cu_company_name_business . " (Cu ID = " . $request->pair_customer . ")";

                    $system->sendMessage([4459, 4560], "<b>" . $customer_payment_method->cu_company_name_business . "</b> Credithold removed by Bank Line Pair", $message);
                }
            }
        }
        return redirect()->back();
    }

    public function getCurrencyRates(Request $request){
        $currency = PaymentCurrency::where("pacu_code", $request->currency)
            ->first();

        if($currency)
        {
            echo json_encode($currency);
        }
    }

    public function addFCToBankLine(Request $request){
        if (isset($request->bali_id) && isset( $request->currency) && isset( $request->amount))
        {
            DB::table('bank_lines')
                ->where('bali_id',  $request->bali_id)
                ->update(
                    [
                        'bali_currency' => $request->currency,
                        'bali_amount_fc' =>  $request->amount
                    ]
                );
        }
    }

    public function forceEuro(Request $request){
        if (isset($request->bali_id))
        {
            DB::table('bank_lines')
                ->where('bali_id',  $request->bali_id)
                ->update(
                    [
                        'bali_currency' => "EUR",
                        'bali_amount_fc' => DB::raw("`bali_amount`")
                    ]
                );
        }
    }
}

