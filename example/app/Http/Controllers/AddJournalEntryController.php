<?php

namespace App\Http\Controllers;

use App\Functions\System;
use App\Models\KTBankLineInvoiceCustomerLedgerAccount;
use App\Models\LedgerAccount;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Log;

class AddJournalEntryController extends Controller
{
	public function index()
	{

        $query_invoices = DB::table("invoices")
            ->leftJoin("customers", "in_cu_id", "cu_id")
            ->where("cu_deleted", 0)
            ->orderBy("in_date", 'desc')
            ->get();

        $invoices = ["" => ""];


        foreach($query_invoices as $row_invoices)
        {
            $query_bank_line_invoice_customer_ledger_account = DB::table("kt_bank_line_invoice_customer_ledger_account")
                ->where("ktbaliinculeac_in_id", $row_invoices->in_id)
                ->get();

            $amount_paired = 0;

            foreach($query_bank_line_invoice_customer_ledger_account as $row_bank_line_invoice_customer_ledger_account)
            {
                $amount_paired += $row_bank_line_invoice_customer_ledger_account->ktbaliinculeac_amount;
            }

            if($row_invoices->in_amount_netto_eur != round($amount_paired, 2))
            {
                $invoices[$row_invoices->in_id] = $row_invoices->in_number." (€ ".System::numberFormat($row_invoices->in_amount_netto_eur, 2)." / € ".System::numberFormat($amount_paired, 2).") ".$row_invoices->cu_company_name_legal;
            }
        }

		return view('finance.addjournalentry', [
            'ledgeraccounts' => LedgerAccount::whereIn("leac_type", [2,3])->where("leac_number", "!=", 120500)->get(),
            'invoices' => $invoices
		]);
	}

	public function filteredIndex(Request $request)
	{
	    Log::debug($request);

        $request->validate([
            'ledger' => 'required',
            'invoice' => 'required'
        ]);

        if(count(array_filter($request->invoice)) != count(array_unique(array_filter($request->invoice))))
        {
            return redirect()->back()->withErrors([ 'errors' => "It's not possible to select the same invoice more then once."]);
        }

        $amount = 0;

        $invoice_pair_amount = [];

        foreach(array_filter($request->invoice) as $key => $invoice)
        {
            $invoice_pair_amount[$key] = str_replace(",", ".", $request->amount[$key]);

            $amount += $invoice_pair_amount[$key];
        }

        foreach(array_filter($request->invoice) as $key => $invoice)
        {
            $row_invoice = DB::table("invoices")
                ->where("in_id", $invoice)
                ->first();

            if(!empty($row_invoice))
            {
                $ktbali = new KTBankLineInvoiceCustomerLedgerAccount();

                $ktbali->ktbaliinculeac_date = Carbon::now()->format('Y/m/d');
                $ktbali->ktbaliinculeac_in_id = $row_invoice->in_id;
                $ktbali->ktbaliinculeac_cu_id = $row_invoice->in_cu_id;
                $ktbali->ktbaliinculeac_leac_number = $request->ledger;
                $ktbali->ktbaliinculeac_amount = $invoice_pair_amount[$key];

                $ktbali->save();

                DB::table('invoices')
                    ->where('in_id', $row_invoice->in_id)
                    ->update(
                        [
                            'in_amount_netto_eur_paid' => DB::raw("(`in_amount_netto_eur_paid` + ".$invoice_pair_amount[$key].")")
                        ]
                    );

            }
        }


        return view('finance.addjournalentrysuccess');

	}

}
