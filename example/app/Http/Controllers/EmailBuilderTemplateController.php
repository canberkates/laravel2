<?php

namespace App\Http\Controllers;

use App\Functions\Mail;
use App\Functions\System;
use App\Models\Country;
use App\Models\EmailBuilderTemplate;
use App\Models\Obligation;
use App\Models\SireloLink;
use App\Models\Website;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class EmailBuilderTemplateController extends Controller
{
    public function index()
    {
        $mail = new Mail();

        return view('email_builder.index',
            [
                'emails' => EmailBuilderTemplate::groupBy("embute_name")->get(),
            ]);
    }
    public function emailChecker($filename, $lang)
    {
        $mail = new Mail();

        $fields = [
            'customername' => 'Arjan'
        ];

        $html = $mail->sendCustomMail($filename, $lang, $fields, null, null, null, true);

        return view('email_builder.mailchecker',
            [
                'html' => $html,
            ]);
    }

    public function create()
    {
        $mail = new Mail();

        $email_preview = self::loadEmailPreview();
        $languages = self::getLanguages();

        return view('email_builder.create', [
            'email_preview' => $email_preview,
            'languages' => $languages
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'from_email' => 'required',
        ]);

        $mail = new Mail();

        $languages = self::getLanguages();

        foreach ($languages as $id => $lang)
        {
            $filename = strtolower($request->name);
            $filename = str_replace(" ", "_", $filename)."_".$id.".php";
            $from_email = $request->from_email;
            $to_cc = $request->to_cc;
            $to_bcc = $request->to_bcc;

            $subject = $request->{"subject_".$id};
            $data_content = self::makeEmailContentForFile($from_email, ((!empty($to_cc)) ? $to_cc : ""), ((!empty($to_bcc)) ? $to_bcc : ""), $subject, $request->{'email_content_'.$id});

            file_put_contents(env("SHARED_FOLDER")."mails/mails/custom/".$filename, "<?php".$data_content."?>");

            $emailtemplate = new EmailBuilderTemplate();
            $emailtemplate->embute_timestamp = date("Y-m-d H:i:s");
            $emailtemplate->embute_language = strtoupper($id);
            $emailtemplate->embute_last_updated_timestamp = date("Y-m-d H:i:s");
            $emailtemplate->embute_created_by = Auth::user()->us_id;
            $emailtemplate->embute_last_updated_by = Auth::user()->us_id;
            $emailtemplate->embute_name = $request->name;
            $emailtemplate->embute_description = $request->description;
            $emailtemplate->embute_from_email = $request->from_email;
            $emailtemplate->embute_subject = $subject;
            $emailtemplate->embute_to_cc = $request->to_cc;
            $emailtemplate->embute_to_bcc = $request->to_bcc;
            $emailtemplate->embute_filename = $filename;
            $emailtemplate->embute_content_html = System::serialize($request->{'email_content_'.$id});
            $emailtemplate->save();
        }

        return redirect('email_builder_templates');
    }

    public function edit($embute_id)
    {
        $template = EmailBuilderTemplate::where("embute_id", $embute_id)->first();

        $templates = EmailBuilderTemplate::where("embute_name", $template->embute_name)->orderBy("embute_language", "ASC")->get();

        $languages = self::getLanguages();

        $data_per_lang = [];

        foreach ($templates as $tem) {
            $data_per_lang[$tem->embute_language] = [
                'embute_subject' => $tem->embute_subject,
                'embute_content_html' => System::unserialize($tem->embute_content_html),
            ];
        }

        return view('email_builder.edit',
            [
                'template' => $template,
                'templates' => $templates,
                'languages' => $languages,
                'data_per_lang' => $data_per_lang
            ]);
    }

    public function update(Request $request, $embute_name)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'from_email' => 'required',
        ]);

        $mail = new Mail();

        $languages = self::getLanguages();

        foreach ($languages as $id => $lang)
        {
            $emailtemplate = EmailBuilderTemplate::where("embute_name", $embute_name)
                ->where("embute_language", strtoupper($id))
                ->first();

            $filename = strtolower($request->name);
            $filename = str_replace(" ", "_", $filename)."_".$id.".php";
            $from_email = $request->from_email;
            $to_cc = $request->to_cc;
            $to_bcc = $request->to_bcc;

            $subject = $request->{"subject_".$id};
            $data_content = self::makeEmailContentForFile($from_email, ((!empty($to_cc)) ? $to_cc : ""), ((!empty($to_bcc)) ? $to_bcc : ""), $subject, $request->{'email_content_'.$id});

            if (file_exists(env("SHARED_FOLDER")."mails/mails/custom/".$filename)) {
                unlink(env("SHARED_FOLDER")."mails/mails/custom/".$filename);
            }

            file_put_contents(env("SHARED_FOLDER")."mails/mails/custom/".$filename, "<?php".$data_content."?>");

            $emailtemplate->embute_last_updated_timestamp = date("Y-m-d H:i:s");
            $emailtemplate->embute_last_updated_by = Auth::user()->us_id;
            $emailtemplate->embute_name = $request->name;
            $emailtemplate->embute_description = $request->description;
            $emailtemplate->embute_from_email = $request->from_email;
            $emailtemplate->embute_subject = $subject;
            $emailtemplate->embute_to_cc = $request->to_cc;
            $emailtemplate->embute_to_bcc = $request->to_bcc;
            $emailtemplate->embute_filename = $filename;
            $emailtemplate->embute_content_html = System::serialize($request->{'email_content_'.$id});
            $emailtemplate->save();
        }

        return redirect('email_builder_templates/');
    }

    public function get_string_between($string, $start, $end){
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }

    public function loadEmailPreview($subject = "", $template = "") {
        $mail = new Mail();

        //FROM NAME IS USED FOR PREVIEW

        $from_name = Website::select("we_mail_from_name")->where("we_id", 45)->first()->we_mail_from_name;

        // This retrieves the $header
        include env("SHARED_FOLDER").'mails/template/header_phoenix.php';

        // This retrieves the $footer
        include env("SHARED_FOLDER").'mails/template/footer_phoenix.php';

        $emailContentWidth = '900';

        if(!empty($template)) {
            $template = str_replace("[customername]", "Name", $template);

            $findme = "[button";

            if (strpos($template, $findme)) {
                //Button tag is found
                $btn_found = true;

                while ($btn_found) {
                    $button_data = self::get_string_between($template, '[button', ']');

                    $button_link = self::get_string_between($button_data, "link=\"", "\"");

                    $button_text = self::get_string_between($button_data, "text=\"", "\"");

                    $search = "/[[button](.*)[]]/";
                    $replace = $mail->button($button_link, $button_text, true);
                    $template = preg_replace($search,$replace,$template, 1);

                    if (!strpos($template, $findme)) {
                        $btn_found = false;
                    }
                }
            }
        }

        /** @noinspection PhpUndefinedVariableInspection */
        $html = $header;

        $html .= $template;

        /** @noinspection PhpUndefinedVariableInspection */
        $html .= $footer;

        return $html;
    }

    public function livePreview(Request $request) {
        echo self::loadEmailPreview($request->subject ,$request->data);
    }

    public function createEmail($subject = "", $template) {
        $mail = new Mail();
        $from_name = System::getSetting( 'mail_default_from_name' );

        // This retrieves the $header
        include env("SHARED_FOLDER").'mails/template/header_phoenix.php';

        // This retrieves the $footer
        include env("SHARED_FOLDER").'mails/template/footer_phoenix.php';

        $emailContentWidth = '900';

        /** @noinspection PhpUndefinedVariableInspection */
        $html = $header;

        $html .= $mail->p($template);

        /** @noinspection PhpUndefinedVariableInspection */
        $html .= $footer;

        return $html;
    }

    public function getLanguages() {
        return [
            'en' => 'English',
            'nl' => 'Dutch',
            'it' => 'Italian',
            'fr' => 'France',
            'dk' => 'Danish',
            'de' => 'Deutsch',
            'es' => 'Spanish',
        ];
    }

    public function makeEmailContentForFile($from_email, $to_cc = "", $to_bcc = "", $subject, $content) {
        return '
        use Functions\Data;
        use Functions\Mail;
        use Functions\System;
        
        $from_name = $fields["from_name"];
        $to = $fields["to"];
        
        $from = "'.$from_email.'";
        '.((!empty($to_cc)) ? '$to_cc = "'.$to_cc.'";' : "").'
        '.((!empty($to_bcc)) ? '$to_bcc = "'.$to_bcc.'";' : "").'
        $subject = "'.$subject.'";
        $template = \''.$content.'\';
        ';
    }

    public function delete(Request $request)
    {
        DB::table("kt_customer_obligations")
            ->where("ktcuob_cuob_id", $request->id)
            ->delete();

        DB::table("customer_obligations")
            ->where("cuob_id", $request->id)
            ->delete();
    }

}
