<?php

namespace App\Http\Controllers;

use App\Data\CustomerProgressLostContractType;
use App\Data\CustomerProgressType;
use App\Data\PlannedCallReason;
use App\Data\PlannedCallStatus;
use App\Models\Customer;
use App\Models\CustomerRemark;
use App\Data\CustomerRemarkDepartment;
use App\Data\CustomerRemarkDirection;
use App\Data\CustomerRemarkMedium;
use App\Models\KTCustomerProgress;
use App\Models\PauseDay;
use App\Models\PlannedCall;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Log;

class CustomerRemarkController extends Controller
{
	public function create(Request $request, $customer_id)
	{
		$customer = Customer::findOrFail($customer_id);

		$title = "";
		//Create standard remark title based on which process
		if($request->type == 1){
		    $title = \Auth::user()->us_name . " has first contact with ".$customer->cu_company_name_business. " - ". Carbon::now()->toDateTimeString();
        }elseif($request->type == 2){
            $title = \Auth::user()->us_name . " is working on ".$customer->cu_company_name_business. " - ". Carbon::now()->toDateTimeString();
        }elseif($request->type == 3){
            $title = \Auth::user()->us_name . " has sent a proposal to ".$customer->cu_company_name_business. " - ". Carbon::now()->toDateTimeString();
        }elseif($request->type == 4){
            $title = \Auth::user()->us_name . " has sent a contract to ".$customer->cu_company_name_business. " - ". Carbon::now()->toDateTimeString();
        }elseif($request->type == 5){
            $title = $customer->cu_company_name_business. " lost - ". Carbon::now()->toDateTimeString();
        }elseif($request->type == 6){
            $title = $customer->cu_company_name_business. " won - ". Carbon::now()->toDateTimeString();
        }elseif($request->type == 7){
            $title = $customer->cu_company_name_business. " has cancelled - ". Carbon::now()->toDateTimeString();
        }

		return view('customerremark.create',
			[
				'customer' => $customer,
				'users' => User::all(),
				'departments' => CustomerRemarkDepartment::all(),
				'mediums' => CustomerRemarkMedium::all(),
				'directions' => CustomerRemarkDirection::all(),
                'progresstypes' => CustomerProgressType::all(),
                'progresslostcontracttypes' => CustomerProgressLostContractType::all(),
                'request' => $request,
                'title' => $title
			]);
	}

	public function store(Request $request){

	    Log::debug($request);

		// Validate the request...
		$customer = Customer::with("moverdata")->findOrFail($request->customer_id);

		if($request->progress_type == 5 && !$request->progress_reason){
            return redirect()->back()->withErrors(['errors' => "You didnt select a reason for the lost contract!"]);
        }

		$remark = "<span id='remark_title' style='font-weight: bold'>".$request->title."</span><br><br>".$request->customer_remark;

		// Create a User using User model
		$customerremark = new CustomerRemark();

		$customerremark->cure_timestamp = Carbon::now()->toDateTimeString();
		$customerremark->cure_contact_date = $request->date;
		$customerremark->cure_department = $request->department;
		$customerremark->cure_employee = $request->employee;
		$customerremark->cure_medium = $request->medium;
		$customerremark->cure_direction = $request->direction;
		$customerremark->cure_text = $remark;

		//Set to correct customer and save
		$customerremark->customer()->associate($customer);
		$customerremark->save();

        if($request->progress_type)
        {
            $progress = new KTCustomerProgress();

            $progress->ktcupr_cu_id = $request->customer_id;
            $progress->ktcupr_type = $request->progress_type;
            $progress->ktcupr_timestamp = Carbon::now()->toDateTimeString();
            $progress->ktcupr_us_id = \Auth::id();
            $progress->ktcupr_cure_id = $customerremark->cure_id;

            if ($request->progress_type == 6) {
                $end_month = 12;
                $now_month = date("n");
                $total_months_left = $end_month - $now_month;

                $total_days_left = (60 / 12) * $total_months_left;

                $new_pause_days = new PauseDay();
                $new_pause_days->pada_timestamp = date("Y-m-d H:i:s");
                $new_pause_days->pada_cu_id = $request->customer_id;
                $new_pause_days->pada_amount = $total_days_left;
                $new_pause_days->pada_amount_left = $total_days_left;
                $new_pause_days->pada_start_date = date("Y-m-d H:i:s");
                $new_pause_days->pada_expired = date("Y-12-31 23:59:59");
                $new_pause_days->save();
            }
            elseif($request->progress_type == 7) {
                DB::table("pause_days")
                    ->where("pada_cu_id", $request->customer_id)
                    ->update(
                        [
                            "pada_deleted" => 1,
                        ]
                    );
            }

            if($request->progress_reason){
                $progress->ktcupr_closed_reason = $request->progress_reason;
            }

            $progress->save();
        }

        //If contract won, schedule two calls
        if($request->progress_type == CustomerProgressType::WON)
        {
            $first_check = new PlannedCall();

            $first_check->plca_cu_id = $request->customer_id;
            $first_check->plca_us_id = $customer->moverdata->moda_owner ?? 1778;
            $first_check->plca_status = PlannedCallStatus::OPEN;
            $first_check->plca_reason = PlannedCallReason::THIRTY_DAY_CHECK;
            $first_check->plca_planned_date = Carbon::now()->addDays(30)->toDateString();

            $first_check->save();

            $second_check = new PlannedCall();

            $second_check->plca_cu_id = $request->customer_id;
            $second_check->plca_us_id = $customer->moverdata->moda_owner ?? 1778;
            $second_check->plca_status = PlannedCallStatus::OPEN;
            $second_check->plca_reason = PlannedCallReason::NINETY_DAY_CHECK;
            $second_check->plca_planned_date = Carbon::now()->addDays(90)->toDateString();

            $second_check->save();
        }

		if ($customer->cu_type == 1)
		{
			return redirect('customers/' . $customer->cu_id . "/edit")->with('message', 'Customer Remark successfully added!');
		}
		elseif ($customer->cu_type == 6)
		{
			return redirect('resellers/' . $customer->cu_id . "/edit")->with('message', 'Customer Remark successfully added!');
		}
		elseif ($customer->cu_type == 2)
		{
			return redirect('serviceproviders/' . $customer->cu_id . "/edit")->with('message', 'Customer Remark successfully added!');
		}
		else
		{
			return redirect('affiliatepartners/' . $customer->cu_id . "/edit")->with('message', 'Customer Remark successfully added!');
		}
	}

	public function delete(Request $request)
	{
		$remark = CustomerRemark::find($request->id);

		$remark->delete();
	}
}
