<?php

namespace App\Http\Controllers;

use App\Data\CustomerRequestDeliveryType;
use App\Data\DiscountType;
use App\Data\NatPaymentType;
use App\Models\Customer;
use App\Models\KTCustomerPortal;
use App\Models\PaymentRate;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;

class PaymentRateController extends Controller
{
	public function edit($customer_id, $customerportal_id, $paymentrate_id)
	{
		$paymentrate = PaymentRate::with("customerportal.customer")->findOrFail($paymentrate_id);
        $destination_type = KTCustomerPortal::leftJoin("portals", "po_id", "ktcupo_po_id")->select("po_destination_type")->whereKtcupoId($customerportal_id)->first()->po_destination_type;

		Log::debug($paymentrate);

		return view('paymentrate.edit',
			[
				'paymentrate' => $paymentrate,
                'destination_type' => $destination_type,
				'pricetypes' => NatPaymentType::all(),
				"requestdeliverytypes" => CustomerRequestDeliveryType::all(),
				"discounttypes" => DiscountType::all()
			]);

	}

	public function update(Request $request, $para_id)
	{
		$request->validate([
			'date' => 'required',
			'rate' => 'required',
			'discount_type' => 'required'
		]);

		$paymentrate = PaymentRate::find($para_id);

		$rate = str_replace(",", ".", $request->rate);

		if($rate < 0)
		{
			$rate = $rate * -1;
		}

		$discount_rate = $request->discount_amount;

		if($discount_rate < 0)
		{
			$discount_rate = $discount_rate * -1;
		}

		$paymentrate->para_ktcupo_id = $request->ktcupo_id;
		$paymentrate->para_date = $request->date;
		$paymentrate->para_rate = $rate;
		$paymentrate->para_discount_type = $request->discount_type;
		$paymentrate->para_discount_rate = (($request->discount_type > 0) ? $discount_rate : 0);
		$paymentrate->para_remark = $request->remark;

        if(Arr::exists($request, "nat_price_type")){
            $paymentrate->para_nat_type = $request->nat_price_type;
        }

		$paymentrate->save();

		$customer = Customer::where("cu_id", $request->cu_id)->first();

		if ($customer->cu_type == 6)
        {
            return redirect('resellers/' .$request->cu_id . '/customerportal/' . $request->ktcupo_id . '/edit');
        }
		else {
            return redirect('customers/' .$request->cu_id . '/customerportal/' . $request->ktcupo_id . '/edit');
        }
	}

	public function create($customer_id, $customerportal_id)
	{
		$customer = Customer::findOrFail($customer_id);
		$destination_type = KTCustomerPortal::leftJoin("portals", "po_id", "ktcupo_po_id")->select("po_destination_type")->whereKtcupoId($customerportal_id)->first()->po_destination_type;

		return view('paymentrate.create',
			[
				'customer' => $customer,
                'destination_type' => $destination_type,
                'pricetypes' => NatPaymentType::all(),
				'customerportal' => $customerportal_id,
				"discounttypes" => DiscountType::all()
			]);
	}

	public function store(Request $request)
	{
		$request->validate([
			'date' => 'required',
			'rate' => 'required',
			'discount_type' => 'required'
		]);

		$rate = str_replace(",", ".", $request->rate);

		if($rate < 0)
		{
			$rate = $rate * -1;
		}

		$discount_rate = $request->discount_amount;

		if($discount_rate < 0)
		{
			$discount_rate = $discount_rate * -1;
		}

		$paymentrate = new PaymentRate();

		$paymentrate->para_ktcupo_id = $request->ktcupo_id;
		$paymentrate->para_date = $request->date;
		$paymentrate->para_rate = $rate;
		$paymentrate->para_discount_type = $request->discount_type;
		$paymentrate->para_discount_rate = (($request->discount_type > 0) ? $discount_rate : 0);
		$paymentrate->para_remark = $request->remark;

		if(Arr::exists($request, "nat_price_type")){
            $paymentrate->para_nat_type = $request->nat_price_type;
        }

		$paymentrate->save();

        $customer = Customer::where("cu_id", $request->cu_id)->first();

        if ($customer->cu_type == 6)
        {
            return redirect('resellers/' .$request->cu_id . '/customerportal/' . $request->ktcupo_id . '/edit');
        }
        else {
            return redirect('customers/' .$request->cu_id . '/customerportal/' . $request->ktcupo_id . '/edit');
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\MoverData $moverData
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($para_id)
	{
		$payment_rate = PaymentRate::find($para_id);
		$payment_rate->delete();

		return redirect()->back();
	}
}
