<?php

namespace App\Http\Controllers;

use App\Data\AutomatedDecision;
use App\Data\DestinationType;
use App\Data\RejectionReason;
use App\Data\RequestStatus;
use App\Data\YesNo;
use App\Functions\RequestData;
use App\Functions\System;
use App\Models\AutomatedRequest;
use App\Models\Country;
use App\Models\SystemSetting;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Log;
use Venturecraft\Revisionable\Revision;

class AutomaticMatchController extends Controller
{

    public function analyseLead(Request $webrequest)
    {
        if(empty($webrequest) || empty($webrequest->re_id) || $webrequest->re_id == -1){exit;}

        echo "Analysing request: ". $webrequest->re_id;
        Log::debug("Analysing request: ". $webrequest->re_id);

        sleep(60);

        //Requirements for matching or rejecting
        $match_requirements = self::setAutomaticMatchRequirements();
        $reject_requirements = self::setAutomaticRejectRequirements();

        //Get Request
        $request = \App\Models\Request::leftJoin("volume_calculator", "voca_id", "re_voca_id")->where("re_status", RequestStatus::OPEN)->findOrFail($webrequest->re_id);

        $requestdata = new RequestData();
        $rqc = new RequestsController();
        $system = new System();

        $automatic_match = true;

        $validations = $rqc->validateRequest($requestdata, $request, $system);

        $this->checkMatchRequirements($request, $match_requirements, $validations);
        $this->checkRejectRequirements($validations, $reject_requirements, $system, $request);

        //Determine if this lead should be automatically matched
        foreach($match_requirements as $category => $items){
            foreach($items as $index => $item){
                if($item == 0){
                    $automatic_match = false;
                }
            }
        }

        //Set-up request reject POST
        $myRequest = new \Illuminate\Http\Request();
        $myRequest->setMethod('POST');
        $myRequest->request->add(['send_email' => YesNo::YES]);
        $myRequest->request->add(['system_user' => 4700]);

        if($reject_requirements['move_date_too_far_away']['move_date']){
            $move_date_too_far_away = true;

            $myRequest->request->add(['rejection_reason' => RejectionReason::MOVING_DATE_TOO_FAR_AWAY]);
        }

        if($reject_requirements['double_submit_within_30_days']['email_and_moving_size']){
            $double_submit_within_30_days = true;

            $myRequest->request->add(['rejection_reason' => RejectionReason::DOUBLE_SUBMIT_BETWEEN_1_AND_30_DAYS]);
        }

        if($reject_requirements['double_submit_within_1_day']['email_and_moving_size']){
            $double_submit_within_1_day = true;

            $myRequest->request->add(['rejection_reason' => RejectionReason::DOUBLE_SUBMIT_WITHIN_24_HOURS]);
        }

        if($reject_requirements['invalid_contact_details']['phone'] && $reject_requirements['invalid_contact_details']['e-mail']){
            $invalid_contact_details = true;

            $myRequest->request->add(['rejection_reason' => RejectionReason::INVALID_CONTACT_DETAILS]);
        }

        if($reject_requirements['own_test_lead']['e-mail']){
            $own_test_lead = true;

            $myRequest->request->add(['rejection_reason' => RejectionReason::OWN_TEST_LEAD]);
        }

        $automated_request = new AutomatedRequest();

        $automated_request->aure_re_id = $request->re_id;
        $automated_request->aure_timestamp = Carbon::now()->toDateTimeString();


        //We will automatically match this lead
        if($automatic_match){
            $automated_request->aure_decision = AutomatedDecision::AUTOMATIC_MATCH;
            $rqc->automaticMatch($request->re_id);
        }
        //We will automatically reject this lead
        elseif($invalid_contact_details || $move_date_too_far_away || $double_submit_within_30_days || $double_submit_within_1_day || $own_test_lead){

            $automated_request->aure_decision = AutomatedDecision::AUTOMATIC_REJECT;
            $rqc->rejectLead($myRequest, $request->re_id);

        }
        //We will not proces this lead automatically
        elseif(!$automatic_match && !($invalid_contact_details || $move_date_too_far_away || $double_submit_within_30_days || $own_test_lead)){
            $automated_request->aure_decision = AutomatedDecision::MANUAL;
        }

        //For now, set automatic to 0
        $request->re_automatic = 0;
        $request->save();

        $result = array_merge([$match_requirements], [$reject_requirements]);

        $automated_request->aure_details = base64_encode(json_encode($result));
        $automated_request->save();

        return;
    }

    private function setAutomaticRejectRequirements(){
        return
            [
                "invalid_contact_details" => [
                    "phone" => 0,
                    "e-mail" => 0,
                ],
                "move_date_too_far_away" => [
                    "move_date" => 0,
                ],
                "double_submit_within_30_days" => [
                    "email_and_moving_size" => 0
                ],
                "double_submit_within_1_day" => [
                    "email_and_moving_size" => 0
                ],
                "own_test_lead" => [
                    "e-mail" => 0,
                ],
            ];
    }


    private function setAutomaticMatchRequirements(){
        return
            [
                "address_from" => [
                    "street" => 0,
                    "zipcode" => 0,
                    "city" => 0,
                    "country" => 0,
                    "google_valid" => 0,
                    "street_and_city_unique" => 0
                ],
                "address_to" => [
                    "city" => 0,
                    "country" => 0,
                    "google_valid" => 0,
                    "street_and_city_unique" => 0
                ],
                "addresses_not_identical" => [
                    "streets_unique" => 0,
                    "zipcodes_unique" => 0,
                    "cities_unique" => 0
                ],
                "correct_destination_type" => [
                    "correct_nat_or_int" => 0
                ],
                "regions" => [
                    "from" => 0,
                    "to" => 0
                ],
                "status" => [
                    "not_double" => 0,
                    "not_on_hold" => 0,
                    "not_spam" => 0,
                ],
                "volume_and_vol_calc" => [
                    "volume_max_size_100" => 0,
                    "vol_calc_item_less_than_20" => 0
                ],
                "remark" => [
                    "no_remark" => 0
                ],
                "moving_date" => [
                    "within_range" => 0
                ],
                "full_name" => [
                    "full_name" => 0,
                    "name_length" => 0
                ],
                "telephone" => [
                    "telephone" => 0,
                    "fake_number" => 0,
                    "valid" => 0
                ],
                "email" => [
                    "email" => 0,
                    "valid" => 0,
                    "bounce" => 0
                ],
                "ip" => [
                    "ip" => 0,
                    "valid" => 0
                ],
            ];
    }


    /**
     * @param $request
     * @param array $match_requirements
     * @param array $validations
     * @return array
     */
    private function checkMatchRequirements($request, array &$match_requirements, array $validations): void
    {

        $match_requirements["address_from"] =
            [
                "street" => ($request->re_street_from != "" && $request->re_street_from != null ? 1 : 0),
                "zipcode" => ($request->re_zipcode_from != "" && $request->re_zipcode_from != null ? 1 : 0),
                "city" => ($request->re_city_from != "" && $request->re_city_from != null ? 1 : 0),
                "country" => ($request->re_co_code_from != "" && $request->re_co_code_from != null ? 1 : 0),
                "google_valid" => $this->getGoogleGeocoding($request->re_street_from,$request->re_zipcode_from,$request->re_city_from,$request->re_co_code_from),
                "street_and_city_unique" => ($request->re_street_from != $request->re_city_from ? 1 : 0)
            ];

        $match_requirements["address_to"] =
            [
                "city" => ($request->re_city_to != "" && $request->re_city_to != null ? 1 : 0),
                "country" => ($request->re_co_code_to != "" && $request->re_co_code_to != null ? 1 : 0),
                "google_valid" => $this->getGoogleGeocoding($request->re_street_to,$request->re_zipcode_to,$request->re_city_to,$request->re_co_code_to),
                "street_and_city_unique" => ($request->re_street_to != $request->re_city_to ? 1 : 0)
            ];


        $match_requirements["addresses_not_identical"] =
            [
                "streets_unique" => ($request->re_street_from != $request->re_street_to ? 1 : 0),
                "zipcodes_unique" => ($request->re_zipcode_from != $request->re_zipcode_to ? 1 : 0),
                "cities_unique" => ($request->re_city_from != $request->re_city_to ? 1 : 0),
            ];

        $correct_dest_type = 1;

        //Destination type checks
        $query_portals = DB::table("portals")
            ->select("po_id")
            ->where("po_destination_type",DestinationType::NATMOVING)
            ->whereRaw("(
					`po_co_code` = '".$request->re_co_code_from."' AND
					`po_co_code` = '".$request->re_co_code_to."'
				)")
            ->first();

        if(($request->re_co_code_from == $request->re_co_code_to) && !empty($query_portals) &&  $request->re_destination_type == DestinationType::INTMOVING)
        {
            //Change request to national
            $correct_dest_type = 0;
        }
        elseif(($request->re_co_code_from != $request->re_co_code_to) && $request->re_destination_type == DestinationType::NATMOVING)
        {
            //Change request to international
            $correct_dest_type = 0;
        }

        $match_requirements["correct_destination_type"] =
            [
                "correct_nat_or_int" => $correct_dest_type
            ];

        $region_to = ($request->re_reg_id_to != "" && $request->re_reg_id_to != null ? 1 : 0);

        if($region_to == 0)
        {
            $system = new System();
            $total_matches = 0;

            $nearby = $system->findRegionsByDestinationCity($request->re_city_to, $request->re_destination_type, $request->re_co_code_to);

            foreach ($nearby as $region)
            {
                $total_matches += $region->count;
            }

            if ($nearby[0]->count > 10 && (($nearby[0]->count / $total_matches) * 100) > 95)
            {
                //Set region_to to this region
                $region_to = 1;
            }
        }

        $match_requirements["regions"] =
            [
                "from" => ($request->re_reg_id_from != "" && $request->re_reg_id_from != null ? 1 : 0),
                "to" => $region_to,
            ];

        $match_requirements["status"] =
            [
                "not_double" => ($request->re_double == 0 ? 1 : 0),
                "not_on_hold" => ($request->re_on_hold == 0 ? 1 : 0),
                "not_spam" => ($request->re_spam == 0 ? 1 : 0),
            ];

        $match_requirements["remark"] =
            [
                "no_remark" => (empty($request->re_remarks) ? 1 : 0)
            ];

        $vol_calc_item_less_than_20 = 1;
        $vol_calc = System::unserialize($request->voca_volume_calculator);

        foreach($vol_calc as $room => $items){
            foreach($items as $name => $amount)
            {
                if($amount > 20){
                    $vol_calc_item_less_than_20 = 0;
                }
            }
        }

        $match_requirements["volume_and_vol_calc"] = [
            "volume_max_size_100" => ($request->re_volume_m3 < 100 ? 1 : 0),
            "vol_calc_item_less_than_20" => $vol_calc_item_less_than_20,
        ];

        $match_requirements["moving_date"] =
            [
                "within_range" => (!array_key_exists("moving_date", $validations) ? 1 : 0),
            ];

        $match_requirements["full_name"] =
            [
                "full_name" => (!empty($request->re_full_name) ? 1 : 0),
                "name_length" => strlen($request->re_full_name > 3 ? 1 : 0)
            ];

        $potentially_fake_numbers = [
            '0000', '1111', '2222', '3333', '4444', '5555', '6666', '7777', '8888', '9999',
            '0123', '1234', '2345', '3456', '4567', '5678', '6789',
            '9876', '8765', '7654', '6543', '5432', '4321', '3210'
        ];

        $non_fake_number = 1;
        foreach($potentially_fake_numbers as $potentially_fake_number){
            if(strpos($request->re_telephone1, $potentially_fake_number) !== false){
                $non_fake_number = 0;
            }
        }

        //Check Phone and validity
        $match_requirements["telephone"] =
            [
                "telephone" => (!empty($request->re_telephone1) ? 1 : 0),
                "non_fake_number" => $non_fake_number,
                "valid" => (!array_key_exists("phone_1", $validations) ? 1 : 0)
            ];

        //Check E-mail and validity
        $match_requirements["email"] =
            [
                "email" => (!empty($request->re_email) ? 1 : 0),
                "valid" => (filter_var($request->re_email, FILTER_VALIDATE_EMAIL) ? 1 : 0),
                "bounce" => (!array_key_exists("email_bounce", $validations) ? 1 : 0)
            ];

        //Check IP and validity
        $match_requirements["ip"] =
            [
                "ip" => (!empty($request->re_ip_address) ? 1 : 0),
                "valid" => (!array_key_exists("ip_address", $validations) ? 1 : 0),
            ];

        //Validation telephone number X country from
        $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();

        try {
            $swissNumberProtoFrom = $phoneUtil->parse($request->re_telephone1, $request->re_co_code_from);

            $isValid_from = $phoneUtil->isValidNumber($swissNumberProtoFrom);

            if ($isValid_from) {
                $formatted_from = $phoneUtil->format($swissNumberProtoFrom, \libphonenumber\PhoneNumberFormat::E164);
            }

            DB::table((new Revision())->getTable())->insert([
                [
                    'revisionable_type' => "TelephoneAPI",
                    'revisionable_id' => $request->re_id,
                    'key' => "re_api_telephone_from",
                    'old_value' => $request->re_telephone1,
                    'new_value' => $request->re_co_code_from.": ".(($isValid_from) ? $formatted_from : "INVALID"),
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'updated_at' => Carbon::now()->toDateTimeString()
                ]
            ]);
        } catch (\libphonenumber\NumberParseException $e) {
            Log::debug("Telephone API FROM is failed");
        }


        //Validation telephone number X country to
        $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();

        try {
            $swissNumberProtoTo = $phoneUtil->parse($request->re_telephone1, $request->re_co_code_to);

            $isValid_to = $phoneUtil->isValidNumber($swissNumberProtoTo);

            if ($isValid_to) {
                $formatted_to = $phoneUtil->format($swissNumberProtoTo, \libphonenumber\PhoneNumberFormat::E164)."<br />";
            }

            DB::table((new Revision())->getTable())->insert([
                [
                    'revisionable_type' => "TelephoneAPI",
                    'revisionable_id' => $request->re_id,
                    'key' => "re_api_telephone_to",
                    'old_value' => $request->re_telephone1,
                    'new_value' => $request->re_co_code_to.": ".(($isValid_to) ? $formatted_to : "INVALID"),
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'updated_at' => Carbon::now()->toDateTimeString()
                ]
            ]);
        } catch (\libphonenumber\NumberParseException $e) {
            Log::debug("Telephone API TO is failed");
        }
    }

    /**
     * @param array $validations
     * @param array $reject_requirements
     * @param System $system
     * @param $request
     */
    private function checkRejectRequirements(array $validations, array &$reject_requirements, System $system, $request): void
    {
        $reject_requirements["invalid_contact_details"] = [
            "phone" => (array_key_exists("phone_1", $validations) ? 1 : 0),
            "e-mail" => (array_key_exists("email_bounce", $validations) ? 1 : 0)
        ];

        $moving_date_diff = $system->dateDifference(date("Y-m-d"), date("Y-m-d", strtotime($request->re_moving_date)));

        $reject_requirements["move_date_too_far_away"] = [
            "move_date" => ($moving_date_diff >= 300 ? 1 : 0),
        ];

        $double_requests = DB::table("requests")
            ->select("re_id", "re_timestamp", "re_status", "re_rejection_reason", "re_co_code_from", "re_co_code_to", "re_email", "re_ip_address")
            ->where("re_id", "!=", $request->re_id)
            ->whereRaw("
				(
					(
						`re_email` = '" . $request->re_email . "' AND
						`re_email` != ''
					)
				)")
            ->whereRaw("re_timestamp >= NOW() - INTERVAL 30 DAY")
            ->where("re_status", RequestStatus::MATCHED)
            ->where("re_request_type", $request->re_request_type)
            ->get();

        foreach($double_requests as $double_request){
            if($request->re_co_code_from == $double_request->re_co_code_from && $request->re_co_code_to == $double_request->re_co_code_to){
                if($double_request->re_timestamp < Carbon::now()->subDays(1)->toDateTimeString()){
                    $double_30_day = 1;
                }else{
                    $double_1_day = 1;
                }
            }
        }

        $reject_requirements["double_submit_within_1_day"] = [
            "email_and_moving_size" => $double_1_day ?? 0
        ];

        $reject_requirements["double_submit_within_30_days"] = [
            "email_and_moving_size" => $double_30_day ?? 0
        ];

        [$user, $domain] = explode('@', $request->re_email);

        $reject_requirements["own_test_lead"] = [
            "e-mail" => ($domain == 'triglobal.org' ? 1 : 0)
        ];
    }

    private function getGoogleGeocoding($street, $zipcode, $city, $country){

        //Get the Google Key
        $setting = SystemSetting::where("syse_setting", "=", "google_key")->first();
        $google_key = $setting->syse_value;

        $parameters = [];

        if (!empty($street))
        {
            $parameters[] = $street;
        }

        if (!empty($zipcode))
        {
            $parameters[] = $zipcode;
        }

        if (!empty($city))
        {
            $parameters[] = $city;
        }

        if (!empty($country))
        {
            $country_query = Country::select("co_en")->where("co_code", "=", $country)->first();

            $parameters[] = $country_query->co_en;
        }

        $parameters = urlencode(implode(",", $parameters));

        $result = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address=".$parameters."&key=".$google_key);

        $data = json_decode($result);

        if($data->status == "ZERO_RESULTS"){
            return 0;
        }else{
            return 1;
        }

    }

}
