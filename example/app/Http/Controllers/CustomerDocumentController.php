<?php

namespace App\Http\Controllers;

use App\Data\CustomerDocumentType;
use App\Models\Customer;
use App\Models\CustomerDocument;
use App\Models\Insurance;
use App\Models\KTCustomerInsurance;
use App\Models\KTCustomerObligation;
use App\Models\Obligation;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CustomerDocumentController extends Controller
{
	public function create($customer_id)
	{
		$customer = Customer::findOrFail($customer_id);

		$obligations = Obligation::where("cuob_co_code", $customer->cu_co_code)->orderBy("cuob_name", "ASC")->get();
		$insurances = Insurance::orderBy("cuin_name", "ASC")->get();

		return view('customerdocument.create',
			[
				'customer' => $customer,
				'documenttypes' => CustomerDocumentType::all(),
                'obligations' => $obligations,
                'insurances' => $insurances
			]);
	}

	public function store(Request $request){
        if ($request->cu_co_code == "US" && $request->type == 6) {
            $request->validate([
                'type' => 'required',
                'number' => 'required'
            ]);
        }
        else {
            $request->validate([
                'type' => 'required'
            ]);
        }

		$allowed_types = ['application/pdf', 'image/jpeg', 'image/png', 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'];

        $document_required = true;

        if ($request->cu_co_code == "US" && $request->type == 6)
        {
            $document_required = false;
        }

		if(!$request->hasFile('uploaded_file') && $document_required)
		{
			return redirect()->back()->withErrors([ 'uploaded_file' => 'Please select a document to upload.']);
		}
		elseif($_FILES['uploaded_file']['size'] < 1 && $document_required)
		{
			return redirect()->back()->withErrors([ 'uploaded_file' => 'The document has not been saved, because the file is empty.']);
		}
		elseif($_FILES['uploaded_file']['size'] > 10485760 && $document_required)
		{
			return redirect()->back()->withErrors([ 'uploaded_file' => 'The document has not been saved, because the file is bigger than 10MB.']);
		}
		elseif(!in_array($_FILES['uploaded_file']['type'], $allowed_types) && $document_required)
		{
			return redirect()->back()->withErrors([ 'uploaded_file' => 'The document has not been saved, because the file type is not valid.']);
		}
		else
		{
			$customer = Customer::findOrFail($request->customer_id);

			// Handle File Upload
			if($request->hasFile('uploaded_file')) {
				// Get filename with extension
				$filenameWithExt = $request->file('uploaded_file')->getClientOriginalName();

				// Get just filename
				$filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

			   // Get just ext
				$extension = $request->file('uploaded_file')->getClientOriginalExtension();

				//Filename to store
				$fileNameToStore = $filename.'.'.$extension;

				// Store the file
				//$path = $request->file('uploaded_file')->storeAs('public/documents/'.$request->customer_id.'/', $fileNameToStore);
				$path = env("SHARED_FOLDER")."/uploads/customer_documents/".$customer->cu_id."/".$request->uploaded_file->getClientOriginalName();

				$request->file('uploaded_file')->move(env("SHARED_FOLDER")."/uploads/customer_documents/".$customer->cu_id."/", $request->uploaded_file->getClientOriginalName());
			}

			// Create a User using User model
			$customerdocument = new CustomerDocument();

			$customerdocument->cudo_timestamp = Carbon::now()->toDateTimeString();
			$customerdocument->cudo_type = $request->type;
			$customerdocument->cudo_description = $request->description;
			if (isset($request->number)) {
			    $customerdocument->cudo_number = $request->number;
            }
			$customerdocument->cudo_filename = $fileNameToStore;
			$customerdocument->cudo_us_id = \Auth::id();

			//Set to correct customer and save
			$customerdocument->customer()->associate($customer);
			$customerdocument->save();

            if (isset($request->obligation) && $request->obligation != "none" && $request->type == 6)
            {
                $ktcuob = KTCustomerObligation::where("ktcuob_cu_id", $customer->cu_id)->where("ktcuob_cuob_id", $request->obligation)->first();

                if (count($ktcuob) > 0)
                {
                    $ktcuob->ktcuob_verified_timestamp = date("Y-m-d H:i:s");
                    $ktcuob->ktcuob_verification_result = 1;
                    $ktcuob->ktcuob_cudo_id = $customerdocument->cudo_id;
                    $ktcuob->save();
                }
                else
                {
                    $new_ktcuob = new KTCustomerObligation();
                    $new_ktcuob->ktcuob_cuob_id = $request->obligation;
                    $new_ktcuob->ktcuob_cu_id = $customer->cu_id;
                    $new_ktcuob->ktcuob_cudo_id = $customerdocument->cudo_id;
                    $new_ktcuob->ktcuob_verified_timestamp = date("Y-m-d H:i:s");
                    $new_ktcuob->ktcuob_verification_result = 1;
                    $new_ktcuob->save();
                }
            }

            if (isset($request->insurance) && $request->insurance != "none" && $request->type == 4)
            {
                $ktcuob = KTCustomerInsurance::where("ktcuin_cu_id", $customer->cu_id)->where("ktcuin_cuin_id", $request->insurance)->first();

                if (count($ktcuob) > 0)
                {
                    $ktcuob->ktcuin_verified_timestamp = date("Y-m-d H:i:s");
                    $ktcuob->ktcuin_verification_result = 1;
                    $ktcuob->ktcuin_cudo_id = $customerdocument->cudo_id;
                    $ktcuob->save();
                }
                else
                {
                    $new_ktcuob = new KTCustomerInsurance();
                    $new_ktcuob->ktcuin_cuin_id = $request->insurance;
                    $new_ktcuob->ktcuin_cu_id = $customer->cu_id;
                    $new_ktcuob->ktcuin_cudo_id = $customerdocument->cudo_id;
                    $new_ktcuob->ktcuin_verified_timestamp = date("Y-m-d H:i:s");
                    $new_ktcuob->ktcuin_verification_result = 1;
                    $new_ktcuob->save();
                }
            }
		}

		if ($customer->cu_type == 1)
		{
			return redirect('customers/' . $customer->cu_id . "/edit")->with('message', 'Document is successfully added!');
		}
		elseif ($customer->cu_type == 6)
		{
			return redirect('resellers/' . $customer->cu_id . "/edit")->with('message', 'Document is successfully added!');
		}
		elseif ($customer->cu_type == 2)
		{
			return redirect('serviceproviders/' . $customer->cu_id . "/edit")->with('message', 'Document is successfully added!');
		}
		else
		{
			return redirect('affiliatepartners/' . $customer->cu_id . "/edit")->with('message', 'Document is successfully added!');
		}
	}

	public function downloadDocument(Request $request, $customer_id, $document_id){
		//Get the right document
		$customer_document = CustomerDocument::where("cudo_id", "=", $document_id)->first();

		//Get filepath
		$file = env("SHARED_FOLDER")."/uploads/customer_documents/".$customer_id."/".$customer_document->cudo_filename;

		if(file_exists($file))
		{
			//Download the file
			header('Content-Type: application/octet-stream');
			header("Content-Transfer-Encoding: Binary");
			header("Content-disposition: attachment; filename=\"".basename($file)."\"");
			readfile($file);
		}
	}

	public function archiveDocument(Request $request, $customer_id, $document_id){
		//Get the right document
		$customer_document = CustomerDocument::where("cudo_id", "=", $document_id)->first();

		$customer = Customer::where("cu_id", "=", $customer_id)->first();

		if ($customer_document->cudo_archived == 1)
		{
			$customer_document->cudo_archived = 0;
		}
		else
		{
			$customer_document->cudo_archived = 1;
		}

		$customer_document->save();

		if ($customer->cu_type == 1)
		{
			return redirect('customers/' . $customer->cu_id . "/edit");
		}
		elseif ($customer->cu_type == 6)
		{
			return redirect('resellers/' . $customer->cu_id . "/edit");
		}
		elseif ($customer->cu_type == 2)
		{
			return redirect('serviceproviders/' . $customer->cu_id . "/edit");
		}
		else
		{
			return redirect('affiliatepartners/' . $customer->cu_id . "/edit");
		}
	}
}
