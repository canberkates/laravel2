<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\MoverData;
use Illuminate\Http\Request;

class MoverDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$moverdata = MoverData::with('customer')->get();
	
		return view('moverdata.index', compact("moverdata"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		/*$moda = new MoverData();
	
		$moda->cu_email = "TEST";
	
		$moda->save();*/
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MoverData  $moverData
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		$moverdata = MoverData::query()->where('moda_cu_id', $id)->get()[0];
	
		return view('moverdata.show', compact("moverdata"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MoverData  $moverData
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		// get the mover data
		//$moverdata =  MoverData::query()->where('moda_cu_id', $id)->get()[0];
		$moverdata = MoverData::find($id);
	
		return view('moverdata.edit', compact("moverdata"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MoverData  $moverData
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    
		$mover = MoverData::query()->where('moda_cu_id', $id)->get()[0];
		$customer = Customer::find($id);
	
		$mover->moda_info_email = $request->email;
		$customer->cu_website = 'www.xD.nl';
	
		$mover->save();
		$customer->save();
		
		return redirect('moverdata');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MoverData  $moverData
     * @return \Illuminate\Http\Response
     */
    public function destroy(MoverData $moverData)
    {
        //
    }
}
