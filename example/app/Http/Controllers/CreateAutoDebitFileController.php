<?php

namespace App\Http\Controllers;

use App\Data\AutoDebitType;
use App\Functions\DataIndex;
use App\Functions\DOMDocumentExtended;
use App\Functions\System;
use App\Models\Country;
use App\Models\PaymentCurrency;
use DB;
use Illuminate\Http\Request;

class CreateAutoDebitFileController extends Controller
{
	public function index()
	{
        for ($year = date("Y"); $year >= 2014; $year--)
        {
            $years[] = $year;
        }

        return view('finance.createautodebitfile', [
            'years' => $years,
            'months' => DataIndex::getMonths(),
            'autodebittypes' => AutoDebitType::all()
		]);
	}

	public function filteredIndex(Request $request)
	{
        $request->validate([
            'year' => 'required',
            'month' => 'required',
            'type' => 'required'
        ]);

        $date_from = date("Y-m-d", strtotime($request->year."-".$request->month."-01"));
        $date_to = date("Y-m-t", strtotime($date_from));

        $query = DB::table("invoices")
            ->leftJoin("customers", "in_cu_id", "cu_id")
            ->whereBetween("in_date", [date("Y-m-d", strtotime($date_from)), date("Y-m-d", strtotime($date_to))])
            ->where("in_payment_method", 3)
            ->where("cu_deleted", 0)
            ->orderBy("cu_auto_debit_name", 'asc')
            ->orderBy("in_id", 'asc')
            ->get();

        $i = 0;

        $lines = [];

        foreach($query as $row)
        {
            $i++;

            $query_bali = DB::table("kt_bank_line_invoice_customer_ledger_account")
                ->select("ktbaliinculeac_bali_id","ktbaliinculeac_date","ktbaliinculeac_amount","bali_date")
                ->leftJoin("bank_lines", "ktbaliinculeac_bali_id", "bali_id")
                ->where("ktbaliinculeac_in_id", $row->in_id)
                ->get();

            $amount_paired = 0;

            foreach($query_bali as $row_bali)
            {
                $amount_paired += $row_bali->ktbaliinculeac_amount;
            }

            $amount_left = $row->in_amount_netto_eur - round($amount_paired, 2);

            if($amount_left != 0)
            {
                $lines[$row->cu_id] = [
                    "in_id" => $row->in_id,
                    "in_number" => $row->in_number,
                    "in_currency" => $row->in_currency,
                    "in_amount_netto" => $row->in_amount_netto,
                    "in_amount_netto_eur" => $row->in_amount_netto_eur,
                    "debtor_number" => $row->cu_debtor_number,
                    "date" => $row->in_date,
                    "amount_left" => $amount_left,
                    "amount_paired" => $amount_paired,
                    "cu_auto_debit_name" => $row->cu_auto_debit_name,
                    "cu_auto_debit_city" => $row->cu_auto_debit_city,
                    "cu_auto_debit_co_code" => $row->cu_auto_debit_co_code,
                    "cu_auto_debit_iban" => $row->cu_auto_debit_iban,
                    "cu_auto_debit_bic" => $row->cu_auto_debit_bic,
                    "characteristic" => $row->in_number,
                ];
            }
        }

        foreach (PaymentCurrency::all() as $currency)
        {
            $currency_tokens[$currency->pacu_code] = $currency->pacu_token;
        }

        foreach(Country::all() as $row_countries)
        {
            $countries[$row_countries->co_code] = $row_countries->co_en;
        }

        return view('finance.createautodebitresult', [
            'currencies' => $currency_tokens,
            'countries' => $countries,
            'lines' => $lines,
            'type' => $request->type,
            'year' => $request->year,
            'month' => $request->month
        ]);

	}

    public function createFile(Request $request)
    {
        $request->validate([
            'invoices' => 'required'
        ]);

        $query = DB::table("invoices")
            ->leftJoin("customers", "in_cu_id", "cu_id")
            ->where("in_payment_method", 3)
            ->where("cu_deleted", 0)
            ->whereIn("in_id", array_keys($request->invoices))
            ->orderBy("cu_auto_debit_name", 'asc')
            ->orderBy("in_id", 'asc')
            ->get();

        $lines = [];

        foreach ($query as $row)
        {
            $query_bali = DB::table("kt_bank_line_invoice_customer_ledger_account")
                ->select("ktbaliinculeac_bali_id", "ktbaliinculeac_date", "ktbaliinculeac_amount", "bali_date")
                ->leftJoin("bank_lines", "ktbaliinculeac_bali_id", "bali_id")
                ->where("ktbaliinculeac_in_id", $row->in_id)
                ->get();

            $amount_paired = 0;

            foreach ($query_bali as $row_bali)
            {
                $amount_paired += $row_bali->ktbaliinculeac_amount;
            }

            $amount_left = $row->in_amount_netto_eur - round($amount_paired, 2);

            if ($amount_left != 0)
            {
                if (array_key_exists($row->cu_id, $lines))
                {
                    $lines[$row->cu_id]['amount'] += $amount_left;
                    $lines[$row->cu_id]['characteristic'] .= ", " . $row->in_number;
                } else
                {
                    $lines[$row->cu_id] = [
                        "debtor_number" => $row->cu_debtor_number,
                        "date" => $row->in_date,
                        "amount" => $amount_left,
                        "name" => $row->cu_auto_debit_name,
                        "city" => $row->cu_auto_debit_city,
                        "country" => $row->cu_auto_debit_co_code,
                        "iban" => $row->cu_auto_debit_iban,
                        "bic" => $row->cu_auto_debit_bic,
                        "characteristic" => $row->in_number,
                    ];
                }
            }
        }

        if (!empty($lines))
        {
            $i = 0;

            $xml = new DomDocumentExtended("1.0", "UTF-8");
            $xml->preserveWhiteSpace = false;
            $xml->formatOutput = true;

            $Document = $xml->appendChild($xml->createElement("Document"));
            $Document->setAttribute("xmlns", "urn:iso:std:iso:20022:tech:xsd:pain.008.001.02");
            $Document->setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");

            $CstmrDrctDbtInitn = $Document->appendChild($xml->createElement("CstmrDrctDbtInitn"));

            $GrpHdr = $CstmrDrctDbtInitn->appendChild($xml->createElement("GrpHdr"));
            $GrpHdr->appendChild($xml->createElementText("MsgId", System::getSetting("auto_debit_prefix") . "-" . date("Y_m_d_H_i_s")));
            $GrpHdr->appendChild($xml->createElementText("CreDtTm", date("Y-m-d") . "T" . date("H:i:s") . "Z"));
            $GrpHdr->appendChild($xml->createElementText("NbOfTxs", count($lines)));
            $GrpHdr->appendChild($xml->createElement("InitgPty"))->appendChild($xml->createElementText("Nm", strtoupper(System::getSetting("company_bank_account_name"))));

            $PmtInf = $CstmrDrctDbtInitn->appendChild($xml->createElement("PmtInf"));
            $PmtInf->appendChild($xml->createElementText("PmtInfId", System::getSetting("auto_debit_prefix") . "-" . date("Y_m_d_H_i_s") . "-1"));
            $PmtInf->appendChild($xml->createElementText("PmtMtd", "DD"));

            $PmtTpInf = $PmtInf->appendChild($xml->createElement("PmtTpInf"));
            $PmtTpInf->appendChild($xml->createElement("SvcLvl"))->appendChild($xml->createElementText("Cd", "SEPA"));
            $PmtTpInf->appendChild($xml->createElement("LclInstrm"))->appendChild($xml->createElementText("Cd", "B2B"));
            $PmtTpInf->appendChild($xml->createElementText("SeqTp", $request->type));

            $PmtInf->appendChild($xml->createElementText("ReqdColltnDt", date("Y-m-d")));

            $Cdtr = $PmtInf->appendChild($xml->createElement("Cdtr"));
            $Cdtr->appendChild($xml->createElementText("Nm", strtoupper(System::getSetting("company_bank_account_name"))));

            $PstlAdr = $Cdtr->appendChild($xml->createElement("PstlAdr"));
            $PstlAdr->appendChild($xml->createElementText("Ctry", System::getSetting("company_co_code")));
            $PstlAdr->appendChild($xml->createElementText("AdrLine", System::getSetting("company_street")));
            $PstlAdr->appendChild($xml->createElementText("AdrLine", System::getSetting("company_zipcode") . " " . System::getSetting("company_city")));

            $PmtInf->appendChild($xml->createElement("CdtrAcct"))->appendChild($xml->createElement("Id"))->appendChild($xml->createElementText("IBAN", str_replace(" ", "", System::getSetting("company_iban"))));
            $PmtInf->appendChild($xml->createElement("CdtrAgt"))->appendChild($xml->createElement("FinInstnId"))->appendChild($xml->createElementText("BIC", System::getSetting("company_bic")));
            $PmtInf->appendChild($xml->createElement("UltmtCdtr"))->appendChild($xml->createElementText("Nm", strtoupper(System::getSetting("company_bank_account_name"))));
            $PmtInf->appendChild($xml->createElementText("ChrgBr", "SLEV"));

            $CdtrSchmeId = $PmtInf->appendChild($xml->createElement("CdtrSchmeId"))->appendChild($xml->createElement("Id"))->appendChild($xml->createElement("PrvtId"))->appendChild($xml->createElement("Othr"));
            $CdtrSchmeId->appendChild($xml->createElementText("Id", System::getSetting("auto_debit_collector_id")));
            $CdtrSchmeId->appendChild($xml->createElement("SchmeNm"))->appendChild($xml->createElementText("Prtry", "SEPA"));

            foreach ($lines as $line)
            {
                if ($line['amount'] != 0)
                {
                    $i++;

                    $DrctDbtTxInf = $PmtInf->appendChild($xml->createElement("DrctDbtTxInf"));
                    $DrctDbtTxInf->appendChild($xml->createElement("PmtId"))->appendChild($xml->createElementText("EndToEndId", System::getSetting("auto_debit_prefix") . "-" . date("Y_m_d_H_i_s") . "-1-" . $i));
                    $DrctDbtTxInf->appendChild($xml->createElementText("InstdAmt", System::numberFormat($line['amount'], 2, ".", "")))->setAttribute("Ccy", "EUR");

                    $DrctDbtTx = $DrctDbtTxInf->appendChild($xml->createElement("DrctDbtTx"))->appendChild($xml->createElement("MndtRltdInf"));
                    $DrctDbtTx->appendChild($xml->createElementText("MndtId", $line['debtor_number']));
                    $DrctDbtTx->appendChild($xml->createElementText("DtOfSgntr", $line['date']));
                    $DrctDbtTx->appendChild($xml->createElementText("AmdmntInd", "false"));

                    $DrctDbtTxInf->appendChild($xml->createElement("DbtrAgt"))->appendChild($xml->createElement("FinInstnId"))->appendChild($xml->createElementText("BIC", $line['bic']));

                    $Dbtr = $DrctDbtTxInf->appendChild($xml->createElement("Dbtr"));
                    $Dbtr->appendChild($xml->createElementText("Nm", strtoupper($line['name'])));

                    $PstlAdr = $Dbtr->appendChild($xml->createElement("PstlAdr"));
                    $PstlAdr->appendChild($xml->createElementText("Ctry", $line['country']));
                    $PstlAdr->appendChild($xml->createElementText("AdrLine", $line['city']));

                    $DrctDbtTxInf->appendChild($xml->createElement("DbtrAcct"))->appendChild($xml->createElement("Id"))->appendChild($xml->createElementText("IBAN", str_replace(" ", "", $line['iban'])));
                    $DrctDbtTxInf->appendChild($xml->createElement("UltmtDbtr"))->appendChild($xml->createElementText("Nm", strtoupper($line['name'])));
                    $DrctDbtTxInf->appendChild($xml->createElement("Purp"))->appendChild($xml->createElementText("Cd", "AREN"));
                    $DrctDbtTxInf->appendChild($xml->createElement("RmtInf"))->appendChild($xml->createElementText("Ustrd", $line['characteristic']));
                }
            }

            $file = $request->year . "-" . $request->month . "_" . date("YmdHis");

            $xml->save(env("SHARED_FOLDER") . "uploads/auto_debit_files/" . $file . ".xml");
        }

        return view('finance.createautodebitdownload', [
            'filename' => $file
        ]);
    }

    public function downloadFile($filename){
        $file = env("SHARED_FOLDER") . "uploads/auto_debit_files/" . urldecode($filename) . ".xml";

        return response()->download($file);

    }

}
