<?php

namespace App\Http\Controllers;

use App\Data\ClaimReason;
use App\Data\CustomerMarketType;
use App\Data\CustomerServices;
use App\Data\CustomerStatusType;
use App\Data\DebtorStatus;
use App\Data\DeliveryLead;
use App\Data\DestinationType;
use App\Data\GatheredCompaniesType;
use App\Data\MoverStatus;
use App\Data\NewsletterSlot;
use App\Data\PaymentMethod;
use App\Data\PaymentReminder;
use App\Data\PlannedCallReason;
use App\Data\RejectionReason;
use App\Data\Survey2Source;
use App\Data\WebsiteValidateString;
use App\Data\WebsiteValidateType;
use App\Functions\DataIndex;
use App\Functions\Mail;
use App\Functions\RequestData;
use App\Imports\CustomersUpdateImport;
use App\Models\ApplicationUser;
use App\Models\BankLine;
use App\Functions\System;
use App\Models\ContactPerson;
use App\Models\Country;
use App\Models\Customer;
use App\Models\CustomerCachedTab;
use App\Models\CustomerChange;
use App\Models\CustomerCredit;
use App\Models\CustomerReviewScore;
use App\Models\GatheredCompany;
use App\Models\GatheredCompanyCountryWhitelisted;
use App\Models\Invoice;
use App\Models\KTBankLineInvoiceCustomerLedgerAccount;
use App\Data\Spaces;
use App\Functions\Data;
use App\Functions\Mover;
use App\Functions\Synchronise;
use App\Models\CustomerOffice;
use App\Models\Insurance;
use App\Models\KTCustomerInsurance;
use App\Models\KTCustomerMembership;
use App\Models\KTCustomerObligation;
use App\Models\KTCustomerPortal;
use App\Models\KTCustomerPortalCountry;
use App\Models\KTCustomerPortalRegion;
use App\Models\KTRequestCustomerPortal;
use App\Models\KTServiceProviderNewsletterBlockSent;
use App\Models\MatchStatistic;
use App\Models\Membership;
use App\Models\MobilityexFetchedData;
use App\Models\MoverData;
use App\Models\MoverRequest;
use App\Models\MoverVolumeCalculatorItem;
use App\Models\MoverVolumeCalculatorRoom;
use App\Models\Newsletter;
use App\Models\NewsletterHistory;
use App\Models\Obligation;
use App\Models\PauseHistory;
use App\Models\PaymentCurrency;
use App\Models\PaymentRate;
use App\Models\PlannedCall;
use App\Models\Portal;
use App\Models\QualityScoreCalculation;
use App\Models\Region;
use App\Models\RegionRecognition;
use App\Models\Report;
use App\Models\ReportUsageHistory;
use App\Models\RequestDelivery;
use App\Models\ServiceProviderAdvertorialBlock;
use App\Models\SireloLink;
use App\Models\SireloReviewInvite;
use App\Models\State;
use App\Models\Survey1Customer;
use App\Models\Survey2;
use App\Models\SystemSetting;
use App\Models\User;
use App\Models\VolumeCalculator;
use App\Models\Website;
use App\Models\WebsiteReview;
use Carbon\Carbon;
use CarpCai\AddressParser\Parser;
use Cassandra\Custom;
use DateTime;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use IntlDateFormatter;
use Log;
use Maatwebsite\Excel\Facades\Excel;
use Propaganistas\LaravelPhone\PhoneNumber;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class AdminController extends Controller
{
    public function index()
    {
        return view('admin.index');
    }

    public function phpinfo()
    {
        return view('admin.phpinfo');
    }

    public function debugbar()
    {
        \Debugbar::enable();

        return redirect('/');
    }


    public function testingEmails()
    {
        $mail = new Mail();
        $system = new System();
        /**
         * TGB Emails
         */


        $ktrecupo = DB::table("kt_request_customer_portal")
            ->leftJoin("kt_customer_portal", "ktrecupo_ktcupo_id", "ktcupo_id")
            ->leftJoin("requests", "ktrecupo_re_id", "re_id")
            ->leftJoin("volume_calculator", "voca_id", "re_voca_id")
            ->leftJoin("customers", "ktrecupo_cu_id", "cu_id")
            ->where("ktrecupo_id", 1835083)
            ->where("cu_deleted", 0)
            ->first();

        $claim = DB::table("claims")
            ->leftJoin("kt_request_customer_portal","cl_ktrecupo_id","ktrecupo_id")
            ->leftJoin("kt_customer_portal","ktrecupo_ktcupo_id","ktcupo_id")
            ->leftJoin("requests","ktrecupo_re_id","re_id")
            ->leftJoin("customers","ktrecupo_cu_id","cu_id")
            ->leftJoin("mover_data","ktrecupo_cu_id","moda_cu_id")
            ->leftJoin("portals","ktrecupo_po_id","po_id")
            ->where("cl_status",0)
            ->where("cu_deleted",0)
            ->orderBy("cl_id", "DESC")
            ->first();

        $customer_change = CustomerChange::findOrFail(79);

        $customer = ApplicationUser::with("customer")->where([['apus_cu_id', '=', $customer_change->cuch_cu_id]])->first();

        $survey_row = Survey1Customer::leftJoin("surveys_1", "sucu_su_id", "su_id")
            ->leftJoin("customers", "sucu_cu_id", "cu_id")
            ->leftJoin("requests", "su_re_id", "re_id")
            ->leftJoin("volume_calculator", "voca_id", "re_voca_id")
            ->leftJoin("kt_request_customer_portal", "su_re_id", "ktrecupo_re_id")
            ->whereIn("sucu_contacted", [0,3])
            ->where("sucu_mail", 0)
            ->where("ktrecupo_free", 0)
            ->where("cu_deleted", 0)
            ->orderBy("sucu_id", "DESC")
            ->first();

        $survey_row = System::databaseToArray($survey_row);

        $invoices[] = [
            "id" => 1,
            "number" => 12345,
            "date" => "2021-06-22",
            "expiration_date" => "2021-06-30",
            "expiration_date_time" => strtotime("2021-06-30"),
            "days_overdue" => $system->dateDifference("2021-06-28", date("Y-m-d")),
            "payment_reminder" => 1,
            "payment_reminder_date" => "2021-06-30",
            "payment_reminder_date_time" => strtotime("2021-06-30"),
            "amount_netto" => 200,
            "amount_paid" => 0,
            "amount_left" => 200 - 0
        ];

        $list_of_requests = [];
        $requests_kweerie = \App\Models\Request::leftJoin("kt_request_customer_portal", "re_id", "ktrecupo_re_id")->where("re_status", 1)->orderBy("re_id", "DESC")->groupBy("re_id")->limit(5)->get();
        $requests_kweerie = System::databaseToArray($requests_kweerie);

        foreach ($requests_kweerie as $rk_row)
        {
            $amount_matched = KTRequestCustomerPortal::selectRaw("COUNT(*) as `amount_matched`")
                ->where("ktrecupo_re_id", $rk_row['re_id'])
                ->first()->amount_matched;

            $list_of_requests[$rk_row['re_id']] = [
                //'country_from' => $data->country($row->re_co_code_from, $customer->cu_la_code),
                //'country_to' => $data->country($row->re_co_code_to, $customer->cu_la_code),
                'moving_size' => Mover::movingSizeText($rk_row['re_moving_size'], "EN"),
                'moving_date' => date("W", strtotime($rk_row['re_moving_date'])) . " / " . date("Y", strtotime($rk_row['re_moving_date'])),
                'amount_matched' => $amount_matched,
                'request' => $rk_row
            ];
        }

        $company_data = [
            "company_name" => System::getSetting("company_name"),
            "street" => System::getSetting("company_street"),
            "zipcode" => System::getSetting("company_zipcode"),
            "city" => System::getSetting("company_city"),
            "country" => System::getSetting("company_co_code"),
            "vat" => System::getSetting("company_vat"),
            "coc" => System::getSetting("company_coc"),
            "bank" => System::getSetting("company_bank"),
            "iban" => System::getSetting("company_iban"),
            "bic" => System::getSetting("company_bic"),
            "telephone" => System::getSetting("company_telephone"),
            "fax" => System::getSetting("company_fax"),
            "email" => System::getSetting("company_email"),
            "website" => System::getSetting("company_website")
        ];

        $debtor_data = [
            "id" => $ktrecupo->cu_id,
            "debtor_number" => $ktrecupo->cu_debtor_number,
            "company_name" => $ktrecupo->cu_company_name_legal,
            "attn" => $ktrecupo->cu_attn,
            "email" => ((!empty($ktrecupo->cu_email_bi)) ? $ktrecupo->cu_email_bi : $ktrecupo->cu_email),
            "street_1" => $ktrecupo->cu_street_1,
            "street_2" => $ktrecupo->cu_street_2,
            "zipcode" => $ktrecupo->cu_zipcode,
            "city" => $ktrecupo->cu_city,
            "country" => $ktrecupo->cu_co_code,
            "payment_term" => $ktrecupo->cu_payment_term,
            "vat_number" => $ktrecupo->cu_vat_number
        ];

        $fields = [
            "request_delivery_value" => 'arjandeconinck@live.nl',
            "request_delivery_extra" => '',
            "match_type" => (($ktrecupo->ktrecupo_type == 3)?2:1),
            "cu_id" => 2,
            "cu_re_id" => 12345,
            "language" => "EN",
            "request" => $system->databaseToArray($ktrecupo),
            "matched_to" => 4,
            "email" => "arjandeconinck@live.nl",
            "password" => "RANDOMPASS",
            "link" => "https://mover.triglobal.info",
            "from" => "erp",
            "claim" => $system->databaseToArray($claim),
            "customer_changes" => $system->databaseToArray($customer_change),
            "reason" => "This is just a sample text, why this change is denied.",
            "customer_email" => "arjandeconinck@live.nl",
            "survey" => $survey_row,
            "kt_request_customer_portal" => $system->databaseToArray($ktrecupo),
            "expiration_date" => "2021-06-28",
            "invoice_number" => 12345,
            "amount" => 200.00,
            'invoices' => $invoices,
            'amount_left' => 220.00,
            'amount_left_eur' => 200.00,
            "currency" => "EUR",
            'list_of_requests' => $list_of_requests,
            'to_email' => "arjandeconinck@live.nl",
            "payment_method" => ucfirst("Paypal"),
            "email_to" => "arjandeconinck@live.nl",
            "receipt_number" => 123,
            "receipt_date" => "2021-06-30",
            "customer_name" => "ABC Movers",
            "street_1" => "Spuiboulevard 240",
            "street_2" => "",
            "zipcode" => "3311 GR",
            "city" => "Dordrecht",
            "country" => "NL",
            "vat_number" => "12345678",
            "company_data" => $company_data,
            "debtor_data" => $debtor_data,
            "invoice_names" => implode(", ", ["12345"]),
            "origin" => "Sirelo.co.uk",
            'date_requested' => date("Y-m-d", strtotime("2021-05-20")),
            'total_days' => 13,
            'end_date' => "2021-06-28",
            "full_name" => "Arjan de Coninck",
            "username" => "arjandeconinck@live.nl",
            "portal" => "MP",
            "remaining_balance" => "€" . " " . number_format(200, 2),
            "last_4_digits_cc" => "1234",
            "balance" => "€" . " " . number_format(205, 2),
            "name" => "Arjan",
            "remark" => "Test remark",
            "rejection_reason" => "Not valid, this is a sample text.",
            'company_name' => "ABC Movers",
        ];




        echo "<h1>"."INVOICE_ALMOST_OVERDUE emails"."</h1>";
        echo "<hr>";

        echo "<center><h3>EN</h3></center>";
        echo $mail->send("invoice_almost_overdue", "EN", $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>NL</h3></center>";
        echo $mail->send("invoice_almost_overdue", "NL", $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>DE</h3></center>";
        echo $mail->send("invoice_almost_overdue", "DE", $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>FR</h3></center>";
        echo $mail->send("invoice_almost_overdue", "FR", $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>ES</h3></center>";
        echo $mail->send("invoice_almost_overdue", "ES", $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>IT</h3></center>";
        echo $mail->send("invoice_almost_overdue", "IT", $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>DK</h3></center>";
        echo $mail->send("invoice_almost_overdue", "DK", $fields, null, null, null, true);
        echo "<hr>";


        echo "<h1>"."PAYMENT_REMINDER #1"."</h1>";
        echo "<hr>";

        echo "<center><h3>EN</h3></center>";
        echo $mail->send("payment_reminder", "EN", ['payment_reminder_mail' => 1, 'company_data'=>$company_data,"debtor_data" => $debtor_data] + $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>NL</h3></center>";
        echo $mail->send("payment_reminder", "NL", ['payment_reminder_mail' => 1, 'company_data'=>$company_data,"debtor_data" => $debtor_data] + $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>DE</h3></center>";
        echo $mail->send("payment_reminder", "DE", ['payment_reminder_mail' => 1, 'company_data'=>$company_data,"debtor_data" => $debtor_data] + $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>FR</h3></center>";
        echo $mail->send("payment_reminder", "FR", ['payment_reminder_mail' => 1, 'company_data'=>$company_data,"debtor_data" => $debtor_data] + $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>ES</h3></center>";
        echo $mail->send("payment_reminder", "ES", ['payment_reminder_mail' => 1, 'company_data'=>$company_data,"debtor_data" => $debtor_data] + $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>IT</h3></center>";
        echo $mail->send("payment_reminder", "IT", ['payment_reminder_mail' => 1, 'company_data'=>$company_data,"debtor_data" => $debtor_data] + $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>DK</h3></center>";
        echo $mail->send("payment_reminder", "DK", ['payment_reminder_mail' => 1, 'company_data'=>$company_data,"debtor_data" => $debtor_data] + $fields, null, null, null, true);
        echo "<hr>";



        echo "<h1>"."PAYMENT_REMINDER - Demand for payment"."</h1>";
        echo "<hr>";

        echo "<center><h3>EN</h3></center>";
        echo $mail->send("payment_reminder", "EN", ['payment_reminder_mail' => 2, 'company_data'=>$company_data,"debtor_data" => $debtor_data] + $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>NL</h3></center>";
        echo $mail->send("payment_reminder", "NL", ['payment_reminder_mail' => 2, 'company_data'=>$company_data,"debtor_data" => $debtor_data] + $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>DE</h3></center>";
        echo $mail->send("payment_reminder", "DE", ['payment_reminder_mail' => 2, 'company_data'=>$company_data,"debtor_data" => $debtor_data] + $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>FR</h3></center>";
        echo $mail->send("payment_reminder", "FR", ['payment_reminder_mail' => 2, 'company_data'=>$company_data,"debtor_data" => $debtor_data] + $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>ES</h3></center>";
        echo $mail->send("payment_reminder", "ES", ['payment_reminder_mail' => 2, 'company_data'=>$company_data,"debtor_data" => $debtor_data] + $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>IT</h3></center>";
        echo $mail->send("payment_reminder", "IT", ['payment_reminder_mail' => 2, 'company_data'=>$company_data,"debtor_data" => $debtor_data] + $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>DK</h3></center>";
        echo $mail->send("payment_reminder", "DK", ['payment_reminder_mail' => 2, 'company_data'=>$company_data,"debtor_data" => $debtor_data] + $fields, null, null, null, true);
        echo "<hr>";





        echo "<h1>"."PAYMENT_REMINDER - Notice of default"."</h1>";
        echo "<hr>";

        echo "<center><h3>EN</h3></center>";
        echo $mail->send("payment_reminder", "EN", ['payment_reminder_mail' => 4, 'company_data'=>$company_data,"debtor_data" => $debtor_data] + $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>NL</h3></center>";
        echo $mail->send("payment_reminder", "NL", ['payment_reminder_mail' => 4, 'company_data'=>$company_data,"debtor_data" => $debtor_data] + $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>DE</h3></center>";
        echo $mail->send("payment_reminder", "DE", ['payment_reminder_mail' => 4, 'company_data'=>$company_data,"debtor_data" => $debtor_data] + $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>FR</h3></center>";
        echo $mail->send("payment_reminder", "FR", ['payment_reminder_mail' => 4, 'company_data'=>$company_data,"debtor_data" => $debtor_data] + $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>ES</h3></center>";
        echo $mail->send("payment_reminder", "ES", ['payment_reminder_mail' => 4, 'company_data'=>$company_data,"debtor_data" => $debtor_data] + $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>IT</h3></center>";
        echo $mail->send("payment_reminder", "IT", ['payment_reminder_mail' => 4, 'company_data'=>$company_data,"debtor_data" => $debtor_data] + $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>DK</h3></center>";
        echo $mail->send("payment_reminder", "DK", ['payment_reminder_mail' => 4, 'company_data'=>$company_data,"debtor_data" => $debtor_data] + $fields, null, null, null, true);
        echo "<hr>";



        echo "<h1>"."PAYMENT_REMINDER - Transfer to debt collection agency"."</h1>";
        echo "<hr>";

        echo "<center><h3>EN</h3></center>";
        echo $mail->send("payment_reminder", "EN", ['payment_reminder_mail' => 5, 'company_data'=>$company_data,"debtor_data" => $debtor_data] + $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>NL</h3></center>";
        echo $mail->send("payment_reminder", "NL", ['payment_reminder_mail' => 5, 'company_data'=>$company_data,"debtor_data" => $debtor_data] + $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>DE</h3></center>";
        echo $mail->send("payment_reminder", "DE", ['payment_reminder_mail' => 5, 'company_data'=>$company_data,"debtor_data" => $debtor_data] + $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>FR</h3></center>";
        echo $mail->send("payment_reminder", "FR", ['payment_reminder_mail' => 5, 'company_data'=>$company_data,"debtor_data" => $debtor_data] + $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>ES</h3></center>";
        echo $mail->send("payment_reminder", "ES", ['payment_reminder_mail' => 5, 'company_data'=>$company_data,"debtor_data" => $debtor_data] + $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>IT</h3></center>";
        echo $mail->send("payment_reminder", "IT", ['payment_reminder_mail' => 5, 'company_data'=>$company_data,"debtor_data" => $debtor_data] + $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>DK</h3></center>";
        echo $mail->send("payment_reminder", "DK", ['payment_reminder_mail' => 5, 'company_data'=>$company_data,"debtor_data" => $debtor_data] + $fields, null, null, null, true);
        echo "<hr>";



    }

    public function testArjan() {
        dd($_SERVER);
    }

    public function testArjanEmails()
    {
        $mail = new Mail();
        $system = new System();
        /**
         * TGB Emails
         */


        $ktrecupo = DB::table("kt_request_customer_portal")
            ->leftJoin("kt_customer_portal", "ktrecupo_ktcupo_id", "ktcupo_id")
            ->leftJoin("requests", "ktrecupo_re_id", "re_id")
            ->leftJoin("volume_calculator", "voca_id", "re_voca_id")
            ->leftJoin("customers", "ktrecupo_cu_id", "cu_id")
            ->where("ktrecupo_id", 1835083)
            ->where("cu_deleted", 0)
            ->first();

        $claim = DB::table("claims")
            ->leftJoin("kt_request_customer_portal","cl_ktrecupo_id","ktrecupo_id")
            ->leftJoin("kt_customer_portal","ktrecupo_ktcupo_id","ktcupo_id")
            ->leftJoin("requests","ktrecupo_re_id","re_id")
            ->leftJoin("customers","ktrecupo_cu_id","cu_id")
            ->leftJoin("mover_data","ktrecupo_cu_id","moda_cu_id")
            ->leftJoin("portals","ktrecupo_po_id","po_id")
            ->where("cl_status",0)
            ->where("cu_deleted",0)
            ->orderBy("cl_id", "DESC")
            ->first();

        $customer_change = CustomerChange::findOrFail(79);

        $customer = ApplicationUser::with("customer")->where([['apus_cu_id', '=', $customer_change->cuch_cu_id]])->first();

        $survey_row = Survey1Customer::leftJoin("surveys_1", "sucu_su_id", "su_id")
            ->leftJoin("customers", "sucu_cu_id", "cu_id")
            ->leftJoin("requests", "su_re_id", "re_id")
            ->leftJoin("volume_calculator", "voca_id", "re_voca_id")
            ->leftJoin("kt_request_customer_portal", "su_re_id", "ktrecupo_re_id")
            ->whereIn("sucu_contacted", [0,3])
            ->where("sucu_mail", 0)
            ->where("ktrecupo_free", 0)
            ->where("cu_deleted", 0)
            ->orderBy("sucu_id", "DESC")
            ->first();

        $survey_row = System::databaseToArray($survey_row);

        $invoices[] = [
            "id" => 1,
            "number" => 12345,
            "date" => "2021-06-22",
            "expiration_date" => "2021-06-30",
            "expiration_date_time" => strtotime("2021-06-30"),
            "days_overdue" => $system->dateDifference("2021-06-28", date("Y-m-d")),
            "payment_reminder" => 1,
            "payment_reminder_date" => "2021-06-30",
            "payment_reminder_date_time" => strtotime("2021-06-30"),
            "amount_netto" => 200,
            "amount_paid" => 0,
            "amount_left" => 200 - 0
        ];

        $list_of_requests = [];
        $requests_kweerie = \App\Models\Request::leftJoin("kt_request_customer_portal", "re_id", "ktrecupo_re_id")->where("re_status", 1)->orderBy("re_id", "DESC")->groupBy("re_id")->limit(5)->get();
        $requests_kweerie = System::databaseToArray($requests_kweerie);

        foreach ($requests_kweerie as $rk_row)
        {
            $amount_matched = KTRequestCustomerPortal::selectRaw("COUNT(*) as `amount_matched`")
                ->where("ktrecupo_re_id", $rk_row['re_id'])
                ->first()->amount_matched;

            $list_of_requests[$rk_row['re_id']] = [
                //'country_from' => $data->country($row->re_co_code_from, $customer->cu_la_code),
                //'country_to' => $data->country($row->re_co_code_to, $customer->cu_la_code),
                'moving_size' => Mover::movingSizeText($rk_row['re_moving_size'], "EN"),
                'moving_date' => date("W", strtotime($rk_row['re_moving_date'])) . " / " . date("Y", strtotime($rk_row['re_moving_date'])),
                'amount_matched' => $amount_matched,
                'request' => $rk_row
            ];
        }

        $company_data = [
            "company_name" => System::getSetting("company_name"),
            "street" => System::getSetting("company_street"),
            "zipcode" => System::getSetting("company_zipcode"),
            "city" => System::getSetting("company_city"),
            "country" => System::getSetting("company_co_code"),
            "vat" => System::getSetting("company_vat"),
            "coc" => System::getSetting("company_coc"),
            "bank" => System::getSetting("company_bank"),
            "iban" => System::getSetting("company_iban"),
            "bic" => System::getSetting("company_bic"),
            "telephone" => System::getSetting("company_telephone"),
            "fax" => System::getSetting("company_fax"),
            "email" => System::getSetting("company_email"),
            "website" => System::getSetting("company_website")
        ];

        $debtor_data = [
            "id" => $ktrecupo->cu_id,
            "debtor_number" => $ktrecupo->cu_debtor_number,
            "company_name" => $ktrecupo->cu_company_name_legal,
            "attn" => $ktrecupo->cu_attn,
            "email" => ((!empty($ktrecupo->cu_email_bi)) ? $ktrecupo->cu_email_bi : $ktrecupo->cu_email),
            "street_1" => $ktrecupo->cu_street_1,
            "street_2" => $ktrecupo->cu_street_2,
            "zipcode" => $ktrecupo->cu_zipcode,
            "city" => $ktrecupo->cu_city,
            "country" => $ktrecupo->cu_co_code,
            "payment_term" => $ktrecupo->cu_payment_term,
            "vat_number" => $ktrecupo->cu_vat_number
        ];

        $fields = [
            "request_delivery_value" => 'arjandeconinck@live.nl',
            "request_delivery_extra" => '',
            "match_type" => (($ktrecupo->ktrecupo_type == 3)?2:1),
            "cu_id" => 2,
            "cu_re_id" => 12345,
            "language" => "EN",
            "request" => $system->databaseToArray($ktrecupo),
            "matched_to" => 4,
            "email" => "arjandeconinck@live.nl",
            "password" => "RANDOMPASS",
            "link" => "https://mover.triglobal.info",
            "from" => "erp",
            "claim" => $system->databaseToArray($claim),
            "customer_changes" => $system->databaseToArray($customer_change),
            "reason" => "This is just a sample text, why this change is denied.",
            "customer_email" => "arjandeconinck@live.nl",
            "survey" => $survey_row,
            "kt_request_customer_portal" => $system->databaseToArray($ktrecupo),
            "expiration_date" => "2021-06-28",
            "invoice_number" => 12345,
            "amount" => 200.00,
            'invoices' => $invoices,
            'amount_left' => 220.00,
            'amount_left_eur' => 200.00,
            "currency" => "EUR",
            'list_of_requests' => $list_of_requests,
            'to_email' => "arjandeconinck@live.nl",
            "payment_method" => ucfirst("Paypal"),
            "email_to" => "arjandeconinck@live.nl",
            "receipt_number" => 123,
            "receipt_date" => "2021-06-30",
            "customer_name" => "ABC Movers",
            "street_1" => "Spuiboulevard 240",
            "street_2" => "",
            "zipcode" => "3311 GR",
            "city" => "Dordrecht",
            "country" => "NL",
            "vat_number" => "12345678",
            "company_data" => $company_data,
            "debtor_data" => $debtor_data,
            "payment_reminder" => 1,
            "payment_reminder_mail" => 1,
            "invoice_names" => implode(", ", ["12345"]),
            "origin" => "Sirelo.co.uk",
            'date_requested' => date("Y-m-d", strtotime("2021-05-20")),
            'total_days' => 13,
            'end_date' => "2021-06-28",
            "full_name" => "Arjan de Coninck",
            "username" => "arjandeconinck@live.nl",
            "portal" => "MP",
            "remaining_balance" => "€" . " " . number_format(200, 2),
            "last_4_digits_cc" => "1234",
            "balance" => "€" . " " . number_format(205, 2),
            "name" => "Arjan",
            "remark" => "Test remark",
            "rejection_reason" => "Not valid, this is a sample text.",
            'company_name' => "ABC Movers"
        ];




        echo "<h1>"."INVOICE_ALMOST_OVERDUE emails"."</h1>";
        echo "<hr>";

        echo "<center><h3>EN</h3></center>";
        echo $mail->send("invoice_almost_overdue", "EN", $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>NL</h3></center>";
        echo $mail->send("invoice_almost_overdue", "NL", $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>DE</h3></center>";
        echo $mail->send("invoice_almost_overdue", "DE", $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>FR</h3></center>";
        echo $mail->send("invoice_almost_overdue", "FR", $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>ES</h3></center>";
        echo $mail->send("invoice_almost_overdue", "ES", $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>IT</h3></center>";
        echo $mail->send("invoice_almost_overdue", "IT", $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>DK</h3></center>";
        echo $mail->send("invoice_almost_overdue", "DK", $fields, null, null, null, true);
        echo "<hr>";





        echo "<h1>"."PAYMENT_REMINDER"."</h1>";
        echo "<hr>";

        echo "<center><h3>EN</h3></center>";
        echo $mail->send("payment_reminder", "EN", $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>NL</h3></center>";
        echo $mail->send("payment_reminder", "NL", $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>DE</h3></center>";
        echo $mail->send("payment_reminder", "DE", $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>FR</h3></center>";
        echo $mail->send("payment_reminder", "FR", $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>ES</h3></center>";
        echo $mail->send("payment_reminder", "ES", $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>IT</h3></center>";
        echo $mail->send("payment_reminder", "IT", $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>DK</h3></center>";
        echo $mail->send("payment_reminder", "DK", $fields, null, null, null, true);
        echo "<hr>";






    }

    public function listActiveCustomerStatuses()
    {
        $customerstatustypes = CustomerStatusType::all();

        $active_customers = Data::activeCustomers();

        $customers = Customer::leftJoin("mover_data", "cu_id", "moda_cu_id")->whereIn("cu_id", array_keys($active_customers))->get();
        echo "<table>";
        foreach ($customers as $cust)
        {
            echo "<tr>";
            echo "<td>" . $cust->cu_company_name_business . "</td>";
            echo "<td>" . $customerstatustypes[$cust->moda_crm_status] . "</td>";
            echo "</tr>";
        }
        echo "</table>";


    }

    public function listWhosMissing()
    {
        $customerstatustypes = CustomerStatusType::all();

        $active_customers = Data::activeCustomers();

        $mover_data = MoverData::leftJoin("customers", "cu_id", "moda_cu_id")->where("moda_crm_status", 1)->get();

        $moverdata = [];

        foreach ($mover_data as $md)
        {
            $moverdata[$md->cu_id] = $md->cu_company_name_business;
        }

        foreach ($active_customers as $id => $ac)
        {
            //unset($active_customers)

            unset($moverdata[$id]);

        }

        echo "<table>";
        foreach ($moverdata as $md)
        {
            echo "<tr>";
            echo "<td>" . $md . "</td>";
            echo "</tr>";
        }
        echo "</table>";

    }

    public function fixMatchPositionAndPrice()
    {

        DB::enableQueryLog();

        $timestamp_of_code_change = "2021-01-21 11:06:20";

        $matched_leads = KTRequestCustomerPortal::join("quality_score_calculation", "qsc_re_id", "ktrecupo_re_id")
            ->where("ktrecupo_timestamp", ">", "2020-12-20 00:00:00")
            ->orderBy("ktrecupo_timestamp")
            ->get();

        foreach ($matched_leads->where("ktrecupo_timestamp", "<", $timestamp_of_code_change) as $lead)
        {
            //Empty values
            $lead->ktrecupo_match_position = null;
            $lead->ktrecupo_average_match = null;
            $lead->ktrecupo_formula_price = null;
            $lead->save();

            $new = unserialize(base64_decode($lead->qsc_new_calc));

            $ranked_list = [];

            $main_portal = key($new["customers"]);

            foreach ($new['customers'] as $portal => $portals)
            {
                foreach ($portals as $customer)
                {
                    if ($customer['checked'] && $main_portal == $portal)
                    {
                        $ranked_list[$customer['cu_id']] = $customer['formula_price'];
                    }
                }
            }

            arsort($ranked_list);

            foreach ($new['customers'] as $portal => $portals)
            {


                foreach ($portals as $customer)
                {

                    if ($customer['cu_id'] == $lead->ktrecupo_cu_id && $customer['checked'] && $main_portal == $portal)
                    {
                        echo "<pre>";
                        echo $lead->ktrecupo_re_id;
                        echo "<br>";
                        print_r($customer);
                        echo "</pre>";
                        echo "<br>";
                        print_r($ranked_list);
                        echo "</pre>";

                        $lead->ktrecupo_match_position = array_search($customer['cu_id'], array_keys($ranked_list)) + 1;
                        $lead->ktrecupo_average_match = $customer['average_match_30_days'];

                        $formula_price = (1 + $customer['correction_bonus']) * $customer['claim_adjusted_lead_price'];
                        $lead->ktrecupo_formula_price = $formula_price;
                        $lead->save();

                    }
                }
            }


        }

        foreach ($matched_leads->where("ktrecupo_timestamp", ">=", $timestamp_of_code_change) as $lead)
        {
            //Empty values
            $lead->ktrecupo_match_position = null;
            $lead->ktrecupo_average_match = null;
            $lead->ktrecupo_formula_price = null;
            $lead->save();

            $new = unserialize(base64_decode($lead->qsc_new_calc));

            $ranked_list = [];

            foreach ($new['customers'] as $customer)
            {
                if ($customer->checked && $customer->po_id == $new['main_portal'])
                {
                    $ranked_list[$customer->cu_id] = $customer->formula_price;
                }
            }

            arsort($ranked_list);


            foreach ($new['customers'] as $customer)
            {

                if ($customer->cu_id == $lead->ktrecupo_cu_id && $customer->checked && $customer->po_id == $new['main_portal'])
                {
                    echo "NEW MATCH";
                    echo "<pre>";
                    echo $lead->ktrecupo_re_id;
                    echo "<br>";
                    print_r($customer);
                    echo "</pre>";
                    echo "<br>";
                    print_r($ranked_list);
                    echo "</pre>";

                    $lead->ktrecupo_match_position = array_search($customer->cu_id, array_keys($ranked_list)) + 1;
                    $lead->ktrecupo_average_match = $customer->average_match_30_days;

                    $formula_price = (1 + $customer->correction_bonus) * $customer->claim_adjusted_lead_price;
                    $lead->ktrecupo_formula_price = $formula_price;
                    $lead->save();

                }
            }

        }

    }

    public function fixMatchPositionAndPriceInMatchStatistics()
    {

        DB::enableQueryLog();

        $score_calculation = QualityScoreCalculation::all();

        //Oude score calculations
        foreach ($score_calculation as $lead)
        {
            $new = unserialize(base64_decode($lead->qsc_new_calc));

            if (is_array($new['customers']))
            {

                $checked_list = [];
                $remaining_list = [];

                $main_portal = key($new["customers"]);

                foreach ($new['customers'] as $portal => $portals)
                {
                    foreach ($portals as $customer)
                    {
                        if ($main_portal == $portal)
                        {
                            if ($customer['checked'])
                            {
                                $checked_list[$customer['cu_id']] = $customer['formula_price'];
                            } else
                            {
                                $remaining_list[$customer['cu_id']] = $customer['formula_price'];
                            }
                        }
                    }
                }

                //Sort customers by percentage
                arsort($checked_list);
                arsort($remaining_list);

                foreach ($new['customers'] as $portal => $portals)
                {

                    foreach ($portals as $ktcupo_id => $customer)
                    {

                        if ($main_portal == $portal)
                        {


                            if ($customer['checked'])
                            {
                                $ranking = array_search($customer['cu_id'], array_keys($checked_list)) + 1;

                            } else
                            {
                                $ranking = array_search($customer['cu_id'], array_keys($remaining_list)) + 1 + count($checked_list);
                            }

                            $formula_price = (1 + $customer['correction_bonus']) * $customer['claim_adjusted_lead_price'];


                            try
                            {
                                DB::table('match_statistics')->updateOrInsert([
                                    'mast_re_id' => $lead->qsc_re_id,
                                    'mast_cu_id' => $customer['cu_id']
                                ], [
                                    'mast_matched' => intval($customer['checked']),
                                    'mast_podium' => ($customer['matching_percentage_after_podium'] == 100 ? 1 : 0),
                                    'mast_match_position' => $ranking,
                                    'mast_average_match' => $customer['average_match_30_days'],
                                    'mast_match_percentage_before_podium' => $customer['matching_percentage'],
                                    'mast_match_percentage_after_podium' => $customer['matching_percentage_after_podium'],
                                    'mast_correction_bonus' => $customer['correction_bonus'],
                                    'mast_ktcupo_id' => $ktcupo_id,
                                    'mast_lead_price_netto' => $customer['lead_price'],
                                    'mast_final_price' => $customer['formula_price'],
                                    'mast_formula_price' => $formula_price
                                ]);

                            } catch (\Exception $e)
                            {
                                echo "ohnee!" . $lead->qsc_re_id;
                            }
                        }

                    }
                }

            } else
            {
                $checked_list = [];
                $remaining_list = [];

                $new = unserialize(base64_decode($lead->qsc_new_calc));

                foreach ($new['customers'] as $customer)
                {
                    if ($customer->po_id == $new['main_portal'])
                    {
                        if ($customer->checked)
                        {

                            $checked_list[$customer->cu_id] = $customer->formula_price;
                        } else
                        {
                            $remaining_list[$customer->cu_id] = $customer->formula_price;
                        }
                    }

                }

                arsort($checked_list);
                arsort($remaining_list);


                foreach ($new['customers'] as $customer)
                {

                    if ($customer->po_id == $new['main_portal'])
                    {

                        if ($customer->checked)
                        {
                            $ranking = array_search($customer->cu_id, array_keys($checked_list)) + 1;

                        } else
                        {
                            $ranking = array_search($customer->cu_id, array_keys($remaining_list)) + 1 + count($checked_list);
                        }


                        $formula_price = (1 + $customer->correction_bonus) * $customer->claim_adjusted_lead_price;

                        try
                        {
                            DB::table('match_statistics')->updateOrInsert([
                                'mast_re_id' => $lead->qsc_re_id,
                                'mast_cu_id' => $customer->cu_id
                            ], [
                                'mast_matched' => intval($customer->checked),
                                'mast_podium' => ($customer->matching_percentage_after_podium == 100 ? 1 : 0),
                                'mast_match_position' => $ranking,
                                'mast_average_match' => $customer->average_match_30_days,
                                'mast_match_percentage_before_podium' => $customer->matching_percentage,
                                'mast_match_percentage_after_podium' => $customer->matching_percentage_after_podium,
                                'mast_correction_bonus' => $customer->correction_bonus,
                                'mast_ktcupo_id' => $customer->ktcupo_id,
                                'mast_lead_price_netto' => $customer->lead_price,
                                'mast_final_price' => $customer->formula_price,
                                'mast_formula_price' => $formula_price
                            ]);
                        } catch (\Exception $e)
                        {
                            echo "ohnee!" . $lead->qsc_re_id;
                        }


                    }
                }

            }
        }

    }

    public function fixLeadPrices()
    {

        DB::enableQueryLog();

        $score_calculation = QualityScoreCalculation::all();

        //Oude score calculations
        foreach ($score_calculation as $lead)
        {
            $new = unserialize(base64_decode($lead->qsc_new_calc));

            if (is_array($new['customers']))
            {

            } else
            {
                $checked_list = [];
                $remaining_list = [];

                $new = unserialize(base64_decode($lead->qsc_new_calc));

                foreach ($new['customers'] as $customer)
                {
                    if ($customer->po_id == $new['main_portal'])
                    {
                        if ($customer->checked)
                        {

                            $checked_list[$customer->cu_id] = $customer->formula_price;
                        } else
                        {
                            $remaining_list[$customer->cu_id] = $customer->formula_price;
                        }
                    }

                }

                arsort($checked_list);
                arsort($remaining_list);


                foreach ($new['customers'] as $customer)
                {

                    if ($customer->po_id == $new['main_portal'])
                    {

                        if ($customer->checked)
                        {
                            $ranking = array_search($customer->cu_id, array_keys($checked_list)) + 1;

                        } else
                        {
                            $ranking = array_search($customer->cu_id, array_keys($remaining_list)) + 1 + count($checked_list);
                        }


                        $formula_price = (1 + $customer->correction_bonus) * $customer->claim_adjusted_lead_price;

                        try
                        {
                            DB::table('match_statistics')->updateOrInsert([
                                'mast_re_id' => $lead->qsc_re_id,
                                'mast_cu_id' => $customer->cu_id
                            ], [
                                'mast_lead_price_netto' => $customer->claim_adjusted_lead_price,
                            ]);
                        } catch (\Exception $e)
                        {
                            echo "ohnee!" . $lead->qsc_re_id;
                        }


                    }
                }

            }
        }

    }

    public function getRegionsforCustomers()
    {
        $customers = MoverData::select("cu_city", "cu_co_code", "cu_id")->leftJoin("customers", "moda_cu_id", "cu_id")->where("cu_type", 1)->where("cu_deleted", 0)->whereNull("moda_reg_id")->inRandomOrder()->get();

        $ajax = new AjaxController();

        foreach($customers as $customer){
            $myRequest = new \Illuminate\Http\Request();
            $myRequest->setMethod('POST');

            $myRequest->request->add(['from_to' => "from"]);
            $myRequest->request->add(['city' => $customer->cu_city]);
            $myRequest->request->add(['destination_type' => 1]);
            $myRequest->request->add(['country' => $customer->cu_co_code]);

            $result = json_decode($ajax->findSuggestedRegion($myRequest), true);

            if($result[0]['re_reg_id_from']){
                DB::table('mover_data')
                    ->where('moda_cu_id', $customer->cu_id)
                    ->update(
                        [
                            'moda_reg_id' => $result[0]['re_reg_id_from']
                        ]
                    );
            }else{
                $myRequest->from_to = "to";
                $result_to = json_decode($ajax->findSuggestedRegion($myRequest), true);

                if($result_to[0]['re_reg_id_from'])
                {
                    DB::table('mover_data')
                        ->where('moda_cu_id', $customer->cu_id)
                        ->update(
                            [
                                'moda_reg_id' => $result_to[0]['re_reg_id_from']
                            ]
                        );
                }
            }
        }


    }



    public function showMatchExclusions()
    {
        $exclude_db = SystemSetting::where("syse_setting", "match_exclusions")->first()->syse_value;
        $excluded_from_each_other = System::unserialize($exclude_db);



        return view('admin.match_exclusions', [
            'exclusions' => $excluded_from_each_other,
            'customers' => Customer::select("cu_id", "cu_company_name_business", "cu_co_code")->where("cu_deleted", 0)->get()
        ]);

    }


    public function updateMatchExclusions(Request $request)
    {


        $data = $request->except(['_token', '_method' ,'new', 'new_name', 'name']);

        $data[$request->new_name] = $request->new;

        $ss = SystemSetting::where("syse_setting", "match_exclusions")->first();
        $ss->syse_value = System::serialize($data);
        $ss->save();

        $exclude_db = SystemSetting::where("syse_setting", "match_exclusions")->first()->syse_value;
        $excluded_from_each_other = System::unserialize($exclude_db);

        return view('admin.match_exclusions', [
            'exclusions' => $excluded_from_each_other,
            'customers' => Customer::select("cu_id", "cu_company_name_business", "cu_co_code")->where("cu_deleted", 0)->get()
        ]);

    }

    public function getRegionsforCustomersByZipcode($destination_type, $country, $zipcode, $city)
    {
        $region = 0;

        $regions = Region::select("reg_id")
            ->where("reg_co_code", $country)
            ->where("reg_destination_type", $destination_type)
            ->where("reg_deleted", 0)
            ->get();

        if(count($regions) == 1)
        {
            $row = $regions[0];

            $region = $row->reg_id;
        }
        else
        {
            if(!empty($zipcode))
            {
                $regions = RegionRecognition::select("rere_reg_id")
                    ->where("rere_co_code", $country)
                    ->where("rere_destination_type", $destination_type);

                if($country == "GB" || $country == "CA" || $country == "BR")
                {
                    $zipcode = substr($zipcode, 0, 2);
                    $regions->whereRaw("rere_zipcode LIKE '{$zipcode}%'");
                }
                elseif($country == "PT")
                {
                    $zipcode = substr($zipcode, 0, 4);
                    $regions->whereRaw("rere_zipcode LIKE '{$zipcode}%'");
                }
                else
                {
                    $zipcode = preg_replace("/[^0-9]/", "", $zipcode);
                    $regions->where("rere_zipcode", $zipcode);

                }

                $regions = $regions
                    ->first();

                if(!empty($regions))
                {
                    $region = $regions->rere_reg_id;
                }
            }
            elseif(!isset($region) && !empty($city) && $country != "US")
            {
                $regions = RegionRecognition::select("rere_reg_id")
                    ->where("rere_co_code", $country)
                    ->where("rere_destination_type", $destination_type)
                    ->where("rere_city", preg_replace("/[^a-zA-Z -]/", "", $city))
                    ->first();

                if(!empty($regions))
                {
                    $region = $regions->rere_reg_id;
                }
            }
        }
        return $region;
    }

    public function testPaymentUnit(){
        $system = new System();
        $mover = new Mover();
        $dataindex = new DataIndex();
        $mail = new Mail();

        $qu_customers = Customer::select("cu_id", "cu_company_name_legal", "cu_pacu_code", "cu_credit_limit", "cu_last_payment_notification", "cu_enforce_credit_limit")
            ->leftJoin("kt_customer_portal", "cu_id", "ktcupo_cu_id")
            ->where("cu_deleted", 0)
            ->where("ktcupo_status", 1)
            ->whereRaw("(`customers`.`cu_payment_method` IN ('4', '5', '6') OR `customers`.`cu_enforce_credit_limit` = 1)")
            ->get();


        $query_customers = $system->databaseToArray($qu_customers);
        $customers = [];

        foreach($query_customers as $row_customers)
        {
            $customers[$row_customers['cu_id']] = $row_customers;

            $customers[$row_customers['cu_id']]['avg_price'] = 0;
            $customers[$row_customers['cu_id']]['per_day'] = 0;
            $customers[$row_customers['cu_id']]['days_left'] = 0;
        }

        if(!empty($customers))
        {
            $balances = $mover->customerBalances(array_keys($customers));
            $balances_2 = $mover->customerBalancesFC(array_keys($customers));

            $outstanding_requests = $mover->multipleCustomersOutstandingRequests(array_keys($customers));

            $qu_ktrecupo = KTRequestCustomerPortal::selectRaw("ktrecupo_cu_id, COUNT(*) AS `requests`, SUM(`ktrecupo_amount_netto`) AS `amount`")
                ->where("ktrecupo_type", 1)
                ->whereIn("ktrecupo_cu_id", array_keys($customers))
                ->whereRaw("`ktrecupo_timestamp` BETWEEN NOW() - INTERVAL 7 DAY AND NOW()")
                ->groupBy("ktrecupo_cu_id")
                ->get();

            $query_ktrecupo = $system->databaseToArray($qu_ktrecupo);

            foreach($query_ktrecupo as $row_ktrecupo)
            {
                $cu_id = $row_ktrecupo['ktrecupo_cu_id'];

                $customers[$cu_id]['avg_price'] = round($row_ktrecupo['amount'] / $row_ktrecupo['requests'], 2);
                $customers[$cu_id]['per_day'] = round($row_ktrecupo['amount'] / 7, 2);
            }

            foreach($customers as $cu_id => $customer)
            {
                $customer['balance'] = round($system->useOr($balances_2[$cu_id]['balance'], 0) + $system->useOr($outstanding_requests[$cu_id]['amount'], 0), 2);
                $customer['to_spend'] = round($customer['balance'] + $customer['cu_credit_limit'], 2);

                if($customer['per_day'] > 0)
                {
                    $customer['days_left'] = round($customer['to_spend'] / $customer['per_day']);
                }
                if($customer['to_spend'] < $customer['avg_price'])
                {
                    //Get payment method of the customer
                    $customer_payment_method_query = Customer::select("cu_payment_method", "cu_company_name_business", "cu_la_code", "cu_email", "cu_prepayment_ran_out_mail_timestamp", "cu_credit_limit_increase_timestamp")
                        ->where("cu_id", $cu_id)
                        ->first();
                    $company = $system->databaseToArray($customer_payment_method_query);

                    //If payment method is Prepayment (4), then send mail
                    if($company['cu_payment_method'] == 4 && !empty($company['cu_credit_limit_increase_timestamp']))
                    {
                        $date_now = date("Y-m-d H:i:s");
                        $date_sent = $customer['cu_prepayment_ran_out_mail_timestamp'];

                        $date1 = new DateTime(date('Y-m-d', strtotime($date_sent)));
                        $date2 = new DateTime(date('Y-m-d', strtotime($date_now)));

                        //Calculate days difference
                        $days_difference = $date1->diff($date2)->days;

                       print_r($customer);

                    }


                }
            }
        }





    }

    public function fixPlannedCalls(){
        $active_customers = Data::activeCustomers();
        $plannedCalls = PlannedCall::leftJoin("customers", "plca_cu_id", "cu_id")->leftJoin("mover_data", "moda_cu_id", "cu_id")->whereIn("plca_cu_id", array_keys($active_customers))->groupBy("plca_cu_id")->get();

        foreach($plannedCalls as $call){
            //Check if customer already has an open six month check up
            $checkup_call = PlannedCall::where("plca_cu_id", $call->plca_cu_id )->where("plca_reason", PlannedCallReason::SIX_MONTHS_FOLLOW_UP)->where("plca_status", 0)->first();

            if($checkup_call){continue;}

            //Has no 6month call, plan a new one
            $future_customer_calls = PlannedCall::where("plca_cu_id", $call->plca_cu_id )->orderBy("plca_planned_date")->where("plca_status", 1)->first();

            $planned_call = new PlannedCall();
            $planned_call->plca_cu_id = $call->plca_cu_id;
            $planned_call->plca_us_id = $call->moda_owner;
            $planned_call->plca_type = 1;
            $planned_call->plca_status = 0;
            $planned_call->plca_reason = PlannedCallReason::SIX_MONTHS_FOLLOW_UP;
            $planned_call->plca_reason_extra = "6 month check up";
            $planned_call->plca_criteria_met = 0;
            $planned_call->plca_planned_date = date("Y-m-d H:i:s", strtotime($future_customer_calls->plca_planned_date." +180 day"));
            $planned_call->save();

            dd($planned_call);



        }


    }

    private function getActiveCustomersWithOwners(){





    }

    public function testLorenzo()
    {
        //self::fixMatchPositionAndPrice();
        //self::fixMatchPositionAndPriceInMatchStatistics();
        //self::fixLeadPrices();
        //self::getRegionsforCustomers();
        //self::testPaymentUnit();
        //self::fixPlannedCalls();
        //self::getActiveCustomersWithOwners();

        /*$customers = CustomerCachedTab::leftJoin("customers", "cu_id", "cucata_cu_id")
            ->leftJoin("mover_data", "moda_cu_id", "cu_id")
            ->leftJoin("users", "moda_owner", "us_id")
            ->select("cucata_cu_id")->distinct()
            ->select("customers.*", "mover_data.*", "users.*")
            ->where("cucata_int_nat", 1)
            ->orWhere("cucata_int", 1)
            ->orWhere("cucata_nat", 1)
            ->orWhere("cucata_pause", 1)
            ->orWhere("cucata_credit_hold", 1)
            ->orWhere("cucata_prepayment_credit_hold", 1)
            ->orWhere("cucata_debt_collector", 1)
            ->orWhere("cucata_leads_store", 1)
            ->orWhere("cucata_conversion_tools", 1)
            ->get();*/

        $lead = DeliveryLead::createFromRequest(\App\Models\Request::where("re_id", 1050320)->first());
        $lead2 = DeliveryLead::createFromMoverRequest(MoverRequest::where("more_id", 40)->first());

        dd($lead, $lead2);


        //Return view
        return view('admin.test_lorenzo', [
            "datas" => $customers
        ]);

        exit;

        $customers = MoverData::select("cu_city", "cu_co_code", "cu_id")->leftJoin("customers", "moda_cu_id", "cu_id")->where("cu_type", 1)->where("cu_deleted", 0)->whereNull("moda_reg_id")->inRandomOrder()->get();
        foreach($customers as $customer){
            $region = self::getRegionsforCustomersByZipcode(1, $customer->cu_co_code, $customer->cu_zipcode, $customer->cu_city);

            if($region)
            {
                DB::table('mover_data')
                    ->where('moda_cu_id', $customer->cu_id)
                    ->update(
                        [
                            'moda_reg_id' => $region
                        ]
                    );
            }
        }

        return;

        DB::enableQueryLog();

        $query_1 = DB::table("customers")
            ->join("contact_persons", "cope_cu_id", "cu_id")
            ->join("customer_offices", "cuof_cu_id", "cu_id")
            ->where("cope_mobile", "!=", "")
            ->where("cuof_id", ">", "2650")
            ->get();


        $query_2 = DB::table("customers")
            ->join("contact_persons", function ($join) {
                $join->on("cope_cu_id", "cu_id")
                    ->where("cope_mobile", "!=", "");
            })
            ->join("customer_offices", function ($join) {
                $join->on("cuof_cu_id", "cu_id")
                    ->where("cuof_id", ">", "2650");
            })
            ->get();


        $contact_persons = DB::table("contact_persons")->where("cope_mobile", "!=", "");
        $customer_offices = DB::table("customer_offices")->where("cuof_id", ">", "2650");

        $query_3 = DB::table("customers")
            ->joinSub($contact_persons, "contact_persons", function ($join) {
                $join->on("cope_cu_id", "cu_id");
            })
            ->joinSub($customer_offices, "customer_offices", function ($join) {
                $join->on("cuof_cu_id", "cu_id");
            })
            ->get();


        dd($query_1, $query_2, $query_3, DB::getQueryLog());


    }

    private static function importCustomerReviewScores($platform)
    {

        if ($platform === 'google')
        {

            // Import file
            $importFile = 'C:\Users\Wilco\Documents\projects\testing\import_google.json';

        } elseif ($platform === 'klantenvertellen')
        {

            // Import file
            $importFile = 'C:\Users\Wilco\Documents\projects\testing\import_kv.json';
        }

        if (file_exists($importFile))
        {

            $fileContents = file_get_contents($importFile);
            $fileData = json_decode($fileContents, true);

            if (!empty($fileData))
            {

                // Loop import and save in db with address
                foreach ($fileData as $customerID => $customerData)
                {

                    // Save in DB
                    DB::table("customer_review_scores")
                        ->updateOrInsert([
                            'curesc_cu_id' => $customerID,
                            'curesc_platform' => 'google',
                        ], [
                            'curesc_cu_id' => $customerID,
                            'curesc_score' => $customerData['score'] ?? 0,
                            'curesc_amount' => $customerData['amount'] ?? 0,
                            'curesc_name' => $customerData['name'] ?? '',
                            'curesc_telephone' => $customerData['telephone'] ?? '',
                            'curesc_address' => $customerData['address'] ?? '',
                            'curesc_url' => $customerData['url'] ?? '',
                            'curesc_timestamp' => date("Y-m-d H:i:s", strtotime($customerData['timestamp'])),
                            'curesc_platform' => $platform
                        ]);
                }
            }

            // Clear import
            file_put_contents($importFile, '');
        }
    }

    public function testWilco()
    {

        echo public_path();

    }

    public function CalcOneToNine($last_digit, string &$solution): void
    {
        if ($last_digit <= 3)
        {
            for ($x = 0; $x < $last_digit; $x++)
            {
                $solution .= "I";
            }
        } elseif ($last_digit == 4)
        {
            $solution .= "IV";
        } elseif ($last_digit == 5)
        {
            $solution .= "V";
        } elseif ($last_digit >= 6 && $last_digit < 9)
        {
            $solution .= "V";
            for ($i = 5; $i < $last_digit; $i++)
            {
                $solution .= "I";
            }
        } elseif ($last_digit == 9)
        {
            $solution .= "IX";
        }
    }

    /**
     * @param $last_2_digits
     * @param string $solution
     * @return string
     */
    public function CalcTenToThirthyNine($last_2_digits, string &$solution): void
    {
        if ($last_2_digits <= 39)
        {
            for ($x = 0; $x < floor($last_2_digits / 10); $x++)
            {
                $solution .= "X";
            }
        } elseif ($last_2_digits >= 40 && $last_2_digits < 50)
        {
            $solution .= "XL";
        }
    }

    public function getLastDates()
    {
        //File of the history of the café planner
        $previous_couples_file = env("SHARED_FOLDER") . "cafe_planner_history.txt";

        //Set empty array
        $array_previous = [];

        //Check if file exists
        if (file_exists($previous_couples_file))
        {
            //Get previous dates from the text file
            $array_previous = json_decode(file_get_contents($previous_couples_file), true);
        }

        //Sort array ASC
        krsort($array_previous);

        return $array_previous;
    }

    public function getLowestDates($array)
    {
        uasort($array, function ($a, $b) {
            return $a['adjusted_dates'] <=> $b['adjusted_dates'];
        });

        return $array[0]['adjusted_dates'];
    }

    public function shuffle_assoc($list)
    {
        if (!is_array($list)) return $list;

        $keys = array_keys($list);
        shuffle($keys);
        $random = [];
        foreach ($keys as $key)
        {
            $random[$key] = $list[$key];
        }

        return $random;
    }


    public function datedWithTheOther($id_1, $id_2, $array)
    {
        if (array_key_exists($id_1, $array[$id_2]['done_with']))
        {
            return true;
        }

        return false;
    }

    public function cafePlanner()
    {

        $users_query = User::where("us_is_deleted", 0)->where("us_cafe_planner", 1)->get();

        $array_file = env("SHARED_FOLDER") . "cafe_planner.txt";

        $start = false;
        //Check if file exists, if there is no file, then make one with standard array
        if (!file_exists($array_file))
        {
            $start = true;
            $array_to_put = [];
            foreach ($users_query as $user)
            {
                $array_to_put[$user->us_id] = [
                    "name" => $user->us_name,
                    "count_of_dates" => 0,
                    "adjusted_dates" => 0,
                    "done_with" => []
                ];
            }

            //Save array in the file
            file_put_contents($array_file, json_encode($array_to_put));
        }
        $array = json_decode(file_get_contents($array_file), true);

        if (!$start)
        {
            $adjusted_dates_default = self::getLowestDates($array);
            foreach ($users_query as $user)
            {
                if (array_key_exists($user->us_id, $array))
                {
                    if ($user->us_is_deleted)
                    {
                        //Remove deleted users from array if exists
                        unset($array[$user->us_id]);
                    }
                } else
                {
                    if ($user->us_is_deleted == 0)
                    {
                        $array[$user->us_id] = [
                            "name" => $user->us_name,
                            "count_of_dates" => 0,
                            "adjusted_dates" => $adjusted_dates_default,
                            "done_with" => []
                        ];
                    }
                }
            }

            file_put_contents($array_file, json_encode($array));
        }

        uasort($array, function ($a, $b) {
            return $a['name'] <=> $b['name'];
        });

        $array_previous = self::getLastDates();

        //Return view
        return view('admin.cafeplanner_exclude', [
            "users" => $array,
            "last_dates" => $array_previous
        ]);
    }

    public function cafePlannerFiltered(Request $request)
    {

        $array_file = env("SHARED_FOLDER") . "cafe_planner.txt";

        //Get array from the existing file
        $array = json_decode(file_get_contents($array_file), true);

        //Make backup of the array
        $array_before = $array;

        foreach ($request->exclude as $id)
        {
            unset($array[$id]);
        }

        //Shuffle array
        $array = self::shuffle_assoc($array);

        //Sort the random array by count of dates column
        uasort($array, function ($a, $b) {
            return $a['adjusted_dates'] <=> $b['adjusted_dates'];
        });


        //Define variables
        $persons_in_date = [];
        $persons_in_date_ids = [];
        $total_elements = count($array);
        $total_dates = 0;
        $count = 0;
        $all_dates_done = false;
        $last_element = false;

        foreach ($array as $id => $arr)
        {
            $count++;
            $date_found = false;

            //Set variable true if last element of array is reached
            if ($count === $total_elements)
                $last_element = true;


            //Skip foreach when there are already 2 dates
            if ($total_dates == 2)
                break;

            //Check if person is not dating already
            if (in_array($id, $persons_in_date_ids))
                continue;

            $count_sub_array = 0;

            foreach ($array as $sub_id => $sub_array)
            {
                //Already 2 dates or date is found? Then break the sub-foreach
                if ($total_dates == 2 || $date_found)
                    break;

                $count_sub_array++;

                //Check if your not matching the same ID, not dating already and didn't date with the person
                if ($sub_id != $id && !self::datedWithTheOther($id, $sub_id, $array) && !in_array($sub_id, $persons_in_date_ids))
                {
                    //FOUND A DATE!
                    $date_found = true;

                    //Add persons to the dating array
                    $persons_in_date_ids[] = $id;
                    $persons_in_date_ids[] = $sub_id;
                    $total_dates++;

                    continue;
                }

                //All dates are done
                if ($last_element && $count_sub_array === $total_elements)
                    $all_dates_done = true;
            }
        }

        $array_previous = self::getLastDates();

        if (!$all_dates_done && count($persons_in_date_ids) == 4)
        {
            $persons_in_date = [
                "date_1_person_1" => $persons_in_date_ids[0],
                "date_1_person_2" => $persons_in_date_ids[1],
                "date_2_person_1" => $persons_in_date_ids[2],
                "date_2_person_2" => $persons_in_date_ids[3],
            ];
        } elseif (count($persons_in_date_ids) == 2)
        {
            $persons_in_date = [
                "date_1_person_1" => $persons_in_date_ids[0],
                "date_1_person_2" => $persons_in_date_ids[1],
            ];
        }

        if (count($persons_in_date) >= 2)
            $all_dates_done = false;

        //Return view
        return view('admin.cafeplanner', [
            "persons_in_date" => $persons_in_date,
            "persons" => $array_before,
            "previous" => $array_previous,
            "all_dates_done" => (($all_dates_done) ? 1 : 0)
        ]);
    }

    public function cafePlannerSubmit(Request $request)
    {

        $array_file = env("SHARED_FOLDER") . "cafe_planner.txt";

        if (file_exists($array_file))
        {
            $array = json_decode(file_get_contents($array_file), true);

            //Date 1:
            $array[$request->date_1_person_1]["adjusted_dates"]++;
            $array[$request->date_1_person_1]["count_of_dates"]++;
            $array[$request->date_1_person_1]["done_with"][$request->date_1_person_2] = $request->date_1_person_2;
            $array[$request->date_1_person_2]["adjusted_dates"]++;
            $array[$request->date_1_person_2]["count_of_dates"]++;
            $array[$request->date_1_person_2]["done_with"][$request->date_1_person_1] = $request->date_1_person_1;

            if (isset($request->date_2_person_1))
            {
                //Date 2:
                $array[$request->date_2_person_1]["adjusted_dates"]++;
                $array[$request->date_2_person_1]["count_of_dates"]++;
                $array[$request->date_2_person_1]["done_with"][$request->date_2_person_2] = $request->date_2_person_2;
                $array[$request->date_2_person_2]["adjusted_dates"]++;
                $array[$request->date_2_person_2]["count_of_dates"]++;
                $array[$request->date_2_person_2]["done_with"][$request->date_2_person_1] = $request->date_2_person_1;
            }

            file_put_contents($array_file, json_encode($array));
        }

        $previous_couples_file = env("SHARED_FOLDER") . "cafe_planner_history.txt";

        $array_previous = [];

        if (file_exists($previous_couples_file))
        {
            $array_previous = json_decode(file_get_contents($previous_couples_file), true);
        }

        $array_previous[] = $array[$request->date_1_person_1]['name'] . " & " . $array[$request->date_1_person_2]['name'];

        if (isset($request->date_2_person_1))
        {
            $array_previous[] = $array[$request->date_2_person_1]['name'] . " & " . $array[$request->date_2_person_2]['name'];

        }

        file_put_contents($previous_couples_file, json_encode($array_previous));

        return redirect("/cafe_planner");
    }

    public function reportsUsage()
    {

        $reports_usage = [];

        $reports = Report::all();

        foreach ($reports as $rep)
        {
            $times_used = ReportUsageHistory::selectRaw("COUNT(*) as `count`")->where("reushi_rep_id", $rep->rep_id)->first()->count;

            $reports_usage[$rep->rep_name] = [
                "rep_id" => $rep->rep_id,
                "rep_name" => $rep->rep_name,
                "times_used" => $times_used,
            ];
        }

        ksort($reports_usage);

        return view('admin.reports_usage', [
            "reports_usage" => $reports_usage
        ]);
    }

    public function seperateCombinedPortals()
    {

        //Get all combined portals
        $combinedportals = Portal::whereNull("po_co_code")->where("po_id", "!=", 66)->where("po_destination_type", 1)->where("po_id", 56)->get();

        foreach ($combinedportals as $combinedportal)
        {
            $regions = Region::where("reg_po_id", $combinedportal->po_id)->where("reg_destination_type", $combinedportal->po_destination_type)->get();

            foreach ($regions as $region)
            {

                //SKIP SOUTH KOREAN BS
                if ($region->reg_co_code == "KR")
                {
                    continue;
                }

                $new_portal = $this->getOrCreatePortal($region, $combinedportal);
                $new_region = $this->getOrCreateRegion($region, $new_portal);
                $this->extractNewCustomerPortal($region, $new_portal, $new_region);
            }

        }

    }

    /**
     * @param \Eloquent $region
     * @param \Eloquent $combinedportal
     * @return Portal|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getOrCreatePortal($region, $combinedportal)
    {
        //Check if this country already has a separate portal
        $existing_portal = Portal::where("po_co_code", $region->reg_co_code)->first();
        if (empty($existing_portal))
        {
            //Create new Portal for this country if not already existing
            $new_portal = new Portal();

            $new_portal->po_portal = $combinedportal->po_destination_type == DestinationType::INTMOVING ? "INTMOVING-" . (strtoupper($region->reg_co_code)) : "NATMOVING-" . (strtoupper($region->reg_co_code));
            $new_portal->po_destination_type = $combinedportal->po_destination_type;
            $new_portal->po_co_code = $region->reg_co_code;
            $new_portal->po_pacu_code = $combinedportal->po_pacu_code;
            $new_portal->po_default_website = 32;

            $new_portal->save();
        } else
        {
            $new_portal = $existing_portal;
        }

        return $new_portal;
    }

    /**
     * @param \Eloquent $region
     * @param $new_portal
     * @return Region|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getOrCreateRegion($region, $new_portal)
    {
        $existing_region = Region::where("reg_co_code", $region->reg_co_code)->where("reg_po_id", $new_portal->po_id)->first();
        //Check if this region is already linked to this (new) portal
        if (empty($existing_region))
        {
            //Create new Region
            $new_region = new Region();

            $new_region->reg_po_id = $new_portal->po_id;
            $new_region->reg_co_code = $region->reg_co_code;
            $new_region->reg_parent = $region->reg_parent;
            $new_region->reg_name = $region->reg_name;
            $new_region->reg_destination_type = $region->reg_destination_type;
            $new_region->reg_deleted = $region->reg_deleted;

            $new_region->save();
        } else
        {
            $new_region = $existing_region;
        }

        return $new_region;
    }

    /**
     * @param \Eloquent $region
     * @param $new_portal
     * @param $new_region
     */
    public function extractNewCustomerPortal($region, $new_portal, $new_region): void
    {
        //Check in which CustomerPortalsRegions this region is currently used
        $customer_portal_regions = KTCustomerPortalRegion::where("ktcupore_reg_id", $region->reg_id)->get();

        foreach ($customer_portal_regions as $customer_portal_region)
        {
            $customer_portal = KTCustomerPortal::where("ktcupo_id", $customer_portal_region->ktcupore_ktcupo_id)->first();

            $new_customer_portal = $this->copyCustomerPortal($new_portal, $customer_portal);

            //Set Correct Origin
            $this->createOriginRegion($new_region, $new_customer_portal);

            //Set Destinations
            $this->createDestinationRegions($customer_portal_region, $new_customer_portal);

            //Copy Request Deliveries
            $this->copyRequestDeliveries($customer_portal, $new_customer_portal);

            //Copy Payment Rates
            $this->copyPaymentRates($customer_portal, $new_customer_portal);
        }
    }

    /**
     * @param $new_portal
     * @param $customer_portal
     * @return KTCustomerPortal
     */
    public function copyCustomerPortal($new_portal, $customer_portal): KTCustomerPortal
    {
        //Copy Customer Portal
        $new_customer_portal = new KTCustomerPortal();

        //Set to new portal!
        $new_customer_portal->ktcupo_po_id = $new_portal->po_id;
        $new_customer_portal->ktcupo_cu_id = $customer_portal->ktcupo_cu_id;
        $new_customer_portal->ktcupo_request_type = $customer_portal->ktcupo_request_type;
        $new_customer_portal->ktcupo_description = $customer_portal->ktcupo_description;
        $new_customer_portal->ktcupo_status = $customer_portal->ktcupo_status;
        $new_customer_portal->ktcupo_member_since = $customer_portal->ktcupo_member_since;
        $new_customer_portal->ktcupo_terminated_on = $customer_portal->ktcupo_terminated_on;
        $new_customer_portal->ktcupo_free_trial = 0;

        $new_customer_portal->ktcupo_moving_size_1 = $customer_portal->ktcupo_moving_size_1;
        $new_customer_portal->ktcupo_moving_size_2 = $customer_portal->ktcupo_moving_size_2;
        $new_customer_portal->ktcupo_moving_size_3 = $customer_portal->ktcupo_moving_size_3;
        $new_customer_portal->ktcupo_moving_size_4 = $customer_portal->ktcupo_moving_size_4;
        $new_customer_portal->ktcupo_moving_size_5 = $customer_portal->ktcupo_moving_size_5;
        $new_customer_portal->ktcupo_moving_size_6 = $customer_portal->ktcupo_moving_size_6;
        $new_customer_portal->ktcupo_moving_size_7 = $customer_portal->ktcupo_moving_size_7;
        $new_customer_portal->ktcupo_moving_size_8 = $customer_portal->ktcupo_moving_size_8;
        $new_customer_portal->ktcupo_moving_size_9 = $customer_portal->ktcupo_moving_size_9;
        $new_customer_portal->ktcupo_moving_size_10 = $customer_portal->ktcupo_moving_size_10;

        $new_customer_portal->ktcupo_min_volume_m3 = $customer_portal->ktcupo_min_volume_m3;
        $new_customer_portal->ktcupo_min_volume_ft3 = $customer_portal->ktcupo_min_volume_ft3;
        $new_customer_portal->ktcupo_export_moves = $customer_portal->ktcupo_export_moves;
        $new_customer_portal->ktcupo_import_moves = $customer_portal->ktcupo_import_moves;
        $new_customer_portal->ktcupo_business_only = $customer_portal->ktcupo_business_only;

        $new_customer_portal->ktcupo_max_requests_month = $customer_portal->ktcupo_max_requests_month;
        $new_customer_portal->ktcupo_max_requests_month_left = $customer_portal->ktcupo_max_requests_month_left;
        $new_customer_portal->ktcupo_max_requests_day = $customer_portal->ktcupo_max_requests_day;
        $new_customer_portal->ktcupo_max_requests_day_left = $customer_portal->ktcupo_max_requests_day_left;

        $new_customer_portal->ktcupo_daily_average = $customer_portal->ktcupo_daily_average;
        $new_customer_portal->ktcupo_requests_bucket = $customer_portal->ktcupo_requests_bucket;
        $new_customer_portal->ktcupo_monthly_capping_triglobal = $customer_portal->ktcupo_monthly_capping_triglobal;
        $new_customer_portal->ktcupo_monthly_capping_client = $customer_portal->ktcupo_monthly_capping_client;

        $new_customer_portal->save();

        return $new_customer_portal;
    }

    /**
     * @param $new_region
     * @param KTCustomerPortal $new_customer_portal
     */
    public function createOriginRegion($new_region, KTCustomerPortal $new_customer_portal): void
    {
        $new_customer_portal_region = new KTCustomerPortalRegion();

        $new_customer_portal_region->ktcupore_ktcupo_id = $new_customer_portal->ktcupo_id;
        $new_customer_portal_region->ktcupore_reg_id = $new_region->reg_id;

        $new_customer_portal_region->save();
    }

    /**
     * @param \Eloquent $customer_portal_region
     * @param KTCustomerPortal $new_customer_portal
     */
    public function createDestinationRegions($customer_portal_region, KTCustomerPortal $new_customer_portal): void
    {
        $customer_portal_countries = KTCustomerPortalCountry::where("ktcupoco_ktcupo_id", $customer_portal_region->ktcupore_ktcupo_id)->get();
        //Set Correct Destinations
        foreach ($customer_portal_countries as $customer_portal_country)
        {

            //Save new Destinations
            $new_customer_portal_country = new KTCustomerPortalCountry();

            $new_customer_portal_country->ktcupoco_ktcupo_id = $new_customer_portal->ktcupo_id;
            $new_customer_portal_country->ktcupoco_co_code = $customer_portal_country->ktcupoco_co_code;

            $new_customer_portal_country->save();
        }
    }

    /**
     * @param $customer_portal
     * @param KTCustomerPortal $new_customer_portal
     */
    public function copyRequestDeliveries($customer_portal, KTCustomerPortal $new_customer_portal): void
    {
        //Copy Request Deliveries
        $deliveries = RequestDelivery::where("rede_ktcupo_id", $customer_portal->ktcupo_id)->get();
        foreach ($deliveries as $delivery)
        {
            $new_delivery = new RequestDelivery();

            $new_delivery->rede_customer_type = $delivery->rede_customer_type;
            $new_delivery->rede_ktcupo_id = $new_customer_portal->ktcupo_id;
            $new_delivery->rede_type = $delivery->rede_type;
            $new_delivery->rede_value = $delivery->rede_value;
            $new_delivery->rede_extra = $delivery->rede_extra;

            $new_delivery->save();
        }
    }

    /**
     * @param $customer_portal
     * @param KTCustomerPortal $new_customer_portal
     */
    public function copyPaymentRates($customer_portal, KTCustomerPortal $new_customer_portal): void
    {
        //Copy Payment Rates
        $paymentrates = PaymentRate::where("para_ktcupo_id", $customer_portal->ktcupo_id)->get();
        foreach ($paymentrates as $paymentrate)
        {
            $new_payment_rate = new PaymentRate();

            $new_payment_rate->para_ktcupo_id = $new_customer_portal->ktcupo_id;
            $new_payment_rate->para_date = $paymentrate->para_date;
            $new_payment_rate->para_rate = $paymentrate->para_rate;
            $new_payment_rate->para_discount_type = $paymentrate->para_discount_type;
            $new_payment_rate->para_remark = $paymentrate->para_remark;

            $new_payment_rate->save();
        }
    }

    // Een functie die Arjan heeft gebouwd voor de zipcodes in Amerika

    public function currentStatesCustomersToID()
    {
        $customers = Customer::where("cu_state", "!=", "")->get();

        foreach ($customers as $customer)
        {
            $state_id = State::where("st_zipcode", $customer->cu_state)->where("st_co_code", $customer->cu_co_code)->first()->st_id;

            $customer->cu_state = $state_id;
            $customer->save();
        }

        echo "done";
    }

    // Get state id by city and country
    public function getStateIdByCity($city, $countryCode)
    {

        $system = new System();

        // Default empty stateId
        $stateId = '';

        // Look for a customer that has a state and has the given city
        $customer = Customer::where('cu_city', $city)->where('cu_co_code', $countryCode)->where('cu_state', '!=', '')->first();

        // If customer state is found
        if (!empty($customer->cu_state))
        {

            // Set state id from customer
            $stateId = $customer->cu_state;
        } else
        {

            // Look for a customer office that has a state and has the given city
            $customerOffice = CustomerOffice::where('cuof_city', $city)->where('cuof_co_code', $countryCode)->whereNotNull('cuof_state')->first();

            if (!empty($customerOffice->cuof_state))
            {

                $stateId = $customerOffice->cuof_state;
            }
        }

        if (empty($stateId))
        {

            // Try to get the state from the api
            $city_location = json_decode(@file_get_contents("http://api.geonames.org/searchJSON?q=" . urlencode($city) . "&country=" . $countryCode . "&maxRows=1&username=triglobal&lang=" . $countryCode));

            // If there is data
            if (!empty($city_location->geonames))
            {

                // Get the state by name
                $state = State::where('st_name', '=', $city_location->geonames[0]->adminName1)->first();

                if (!empty($state))
                {

                    $stateId = $state->st_id;

                } else
                {

                    // If the state does not exist yet. Create a new state
                    $state = new State();
                    $state->st_name = $city_location->geonames[0]->adminName1;
                    $state->st_key = $system->getSeoKey($state->st_name);
                    $state->st_co_code = $countryCode;
                    $state->save();

                    $stateId = $state->st_id;
                }
            }
        }

        return !empty($stateId) ? $stateId : false;
    }

    // The script to gather the states
    public function cityToStates()
    {

        // Change if necessary
        $countryCode = 'US';

        // Default counters
        $countCustomers = 0;
        $countCustomerOffices = 0;
        $countCustomerOfficesCountryCodes = 0;

        // Get customers in country that have no state yet
        $customers = Customer::where("cu_co_code", $countryCode)->whereNull('cu_state')->get();

        if (!empty($customers))
        {

            foreach ($customers as $customer)
            {

                if (!empty($customer->cu_city))
                {

                    $state = self::getStateIdByCity(trim($customer->cu_city), $countryCode);

                    if (!empty($state))
                    {

                        $customer->cu_state = $state;
                        $customer->save();
                        $countCustomers++;
                    }
                }

                // Check for offices from this customer
                $offices = CustomerOffice::where('cuof_co_code', $countryCode)->whereNull('cuof_state')->get();

                // If there are offices that have state on null
                if (!empty($offices))
                {

                    // Loop these offices
                    foreach ($offices as $office)
                    {

                        // City can't be empty
                        if (!empty($office->cuof_city))
                        {

                            // Try to get state
                            $state = self::getStateIdByCity(trim($office->cuof_city), $countryCode);

                            // If we get a state
                            if (!empty($state))
                            {

                                // Set state
                                $office->cuof_state = $state;

                                $countCustomerOffices++;

                                // If the country code is empty
                                if (empty($office->cuof_co_code))
                                {

                                    // Repair the country code
                                    $office->cuof_co_code = $countryCode;

                                    $countCustomerOfficesCountryCodes++;
                                }

                                $office->save();
                            }
                        }
                    }
                }
            }
        }

        echo 'Assigned ' . $countCustomers . ' states to customers';
        echo PHP_EOL;
        echo 'Assigned ' . $countCustomerOffices . ' states to offices';
        echo PHP_EOL;
        echo 'Assigned ' . $countCustomerOfficesCountryCodes . ' countries to offices';

        exit;

    }

    public function manualSync()
    {

        if (!isset($_GET['sirelo']))
        {

            echo 'no sirelo provided';

            return;
        }

        $query = DB::table("websites")
            ->where('we_website', 'Sirelo.' . $_GET['sirelo'])
            ->get();

        // Get all memberships
        $memberships = Membership::all();

        // Get all sirelo hyperlinks
        $sirelo_hyperlinks = SireloLink::all();

        // Get all obligations
        $customer_obligations = Obligation::all();

        // Get all insurances
        $customer_insurances = Insurance::all();

        // Get all states
        $states = State::all();

        // Empty array for the logos
        $logos = [];

        // Loop websites that match the criteria
        foreach ($query as $row)
        {

            // Get pro and con categories by language of website
            $pro_con_categories = Data::getProConCategories(null, $row->we_sirelo_la_code);

            echo "<h3>" . $row->we_website . "</h3>";

            // Prepare arrays
            $options = [
                "national_country_code" => 'US',
                "top_mover_months" => $row->we_sirelo_top_mover_months
            ];
            $cities = [];
            $customers = [];
            $customer_offices = [];
            $customer_memberships = [];
            $kt_customer_obligations = [];
            $kt_customer_insurances = [];
            $customers_to_countries = [];
            $reviews = [];

            // Select all customers from the current website (country_code)
            $customers_query = Customer::leftJoin("mover_data", "moda_cu_id", "cu_id")
                ->leftJoin('states', 'cu_state', 'st_id')
                ->where("cu_type", 1)
                ->where("cu_city", "!=", "")
                ->where("cu_co_code", 'US')
                ->where("cu_deleted", 0)
                ->where("moda_disable_sirelo_export", 0)
                ->get();

            // Loop all the customers from the current website (country_code)
            foreach ($customers_query as $row_customers)
            {

                // Build state key
                $state_key = System::getSeoKey($row_customers->st_key);

                // Build city key
                $city_key = System::getSeoKey($row_customers->cu_city);

                // Build customer key
                $customer_key = $city_key . System::getSeoKey($row_customers->cu_company_name_business);

                // For the websites that go with a region
                if ($row->we_states_enabled)
                {

                    // Add the state key in front of the customer key
                    $customer_key = $state_key . $customer_key;
                }

                // New temp customer
                $temp_customer = [$city_key];

                // Check if this customer (company_name + city) exists
                // We don't want double customers
                if (!array_key_exists($customer_key, $customers))
                {
                    // Check if this city exists
                    if (!array_key_exists($city_key, $cities))
                    {
                        // City does not exist in the cities array yet
                        // Add city to the cities array with amount 1
                        $cities[$city_key] = [
                            "city" => $row_customers->cu_city,
                            "amount" => 1,
                        ];

                    } else
                    {
                        // City does exist in the cities array
                        // Increment city by 1
                        $cities[$city_key]['amount']++;
                    }

                    // If this customer has a state and the state wasn't set yet.
                    if (!empty($row_customers->cu_state) && !isset($cities[$city_key]['state']))
                    {

                        // Add state to this city
                        $cities[$city_key]['state'] = $row_customers->cu_state;
                    }

                    // Build customer
                    $customers[$customer_key] = [
                        "city_key" => $city_key,
                        "forward_1" => $row_customers->moda_sirelo_forward_1,
                        "forward_2" => $row_customers->moda_sirelo_forward_2,
                        "origin_id" => $row_customers->cu_id,
                        "top_mover" => 0,
                        "disable_top_mover" => $row_customers->moda_sirelo_disable_top_mover,
                        "review_partner" => $row_customers->moda_review_partner,
                        "claimed" => Mover::isLoggedInOnce($row_customers->cu_id), // If user is logged in once, assume the company is claimed
                        "company_name" => $row_customers->cu_company_name_business,
                        "legal_name" => $row_customers->cu_company_name_legal,
                        "coc" => $row_customers->cu_coc,
                        "logo" => "",
                        "description" => $row_customers->cu_description,
                        "international_moves" => $row_customers->moda_international_moves,
                        "national_moves" => $row_customers->moda_national_moves,
                        "long_distance_moving_europe" => $row_customers->moda_long_distance_moving_europe,
                        "excess_baggage" => $row_customers->moda_excess_baggage,
                        "man_and_van" => $row_customers->moda_man_and_van,
                        "car_and_vehicle_transport" => $row_customers->moda_car_and_vehicle_transport,
                        "piano_transport" => $row_customers->moda_piano_transport,
                        "pet_transport" => $row_customers->moda_pet_transport,
                        "art_transport" => $row_customers->moda_art_transport,
                        "established" => $row_customers->moda_established,
                        "employees" => $row_customers->moda_employees,
                        "trucks" => $row_customers->moda_trucks,
                        "street" => $row_customers->cu_street_1,
                        "zipcode" => $row_customers->cu_zipcode,
                        "city" => $row_customers->cu_city,
                        "country" => $row_customers->cu_co_code,
                        "email" => $row_customers->moda_contact_email,
                        "telephone" => $row_customers->moda_contact_telephone,
                        "website" => $row_customers->cu_website,
                        "not_operational" => $row_customers->moda_not_operational,
                        "type_of_mover" => $row_customers->cu_type_of_mover,
                        "public_liability_insurance" => $row_customers->cu_public_liability_insurance,
                        "goods_in_transit_insurance" => $row_customers->cu_goods_in_transit_insurance,
                        "rating" => 0,
                        "reviews" => 0,
                        "recommendation" => 0,
                        "recommendations" => 0,
                        "rating_recent" => 0,
                        "reviews_recent" => 0,
                        "recommendation_recent" => 0,
                        "recommendations_recent" => 0,
                        "pros_cons" => ["pro" => [], "con" => []]
                    ];

                    // If the customer has a logo
                    if (!empty($row_customers->cu_logo))
                    {
                        $pathToFile = env('SHARED_FOLDER') . 'uploads/logos/' . $row_customers->cu_logo;

                        if (is_file($pathToFile))
                        {
                            $logo = substr(System::getSeoKey($row_customers->cu_company_name_business, $row->we_sirelo_la_code), 0, -1) . "-" . substr($city_key, 0, -1) . "." . pathinfo($row_customers->cu_logo, PATHINFO_EXTENSION);

                            $customers[$customer_key]['logo'] = $logo;
                            $logos[$row_customers->cu_logo] = $logo;
                        }
                    }
                }

                // Get current customer offices and add in array $customer_offices[]
                $customer_offices_query = CustomerOffice::where("cuof_cu_id", $row_customers->cu_id)
                    ->get();

                // Check if there are any offices
                if (count($customer_offices_query) > 0)
                {
                    // Loop customer offices
                    foreach ($customer_offices_query as $row_customer_offices)
                    {
                        //Check if this city exists, add to the cities amount
                        $office_city_key = System::getSeoKey($row_customer_offices->cuof_city, $row->we_sirelo_la_code);

                        // If city key is not the same as the office city key
                        if (!in_array($office_city_key, $temp_customer))
                        {
                            // Check if city key is not present yet
                            if (!isset($cities[$office_city_key]))
                            {
                                // Create city in cities
                                $cities[$office_city_key] = [
                                    "city" => $row_customer_offices->cuof_city,
                                    "amount" => 1,
                                ];

                                // City does exist already
                            } else
                            {
                                // Add 1 to amount of this city
                                $cities[$office_city_key]['amount']++;
                            }

                            // If there is a state and the state wasn't set yet
                            if (!empty($row_customer_offices->cuof_state) && !isset($cities[$office_city_key]['state']))
                            {

                                // Add state to this city
                                $cities[$office_city_key]['state'] = $row_customer_offices->cuof_state;
                            }
                        }

                        // Add office city key to the temp customer array
                        // for the next office
                        $temp_customer[] = $office_city_key;

                        // Add customer office
                        $customer_offices[] = [
                            "customer_key" => $customer_key,
                            "city_key" => System::getSeoKey($row_customer_offices->cuof_city),
                            "street" => $row_customer_offices->cuof_street_1,
                            "zipcode" => $row_customer_offices->cuof_zipcode,
                            "city" => $row_customer_offices->cuof_city,
                            "email" => $row_customer_offices->cuof_email,
                            "telephone" => $row_customer_offices->cuof_telephone,
                            "website" => $row_customer_offices->cuof_website
                        ];
                    }
                }

                //Get customer memberships and add in array $customer_memberships[]
                $customer_memberships = KTCustomerMembership::where("ktcume_cu_id", $row_customers->cu_id)->get();

                foreach ($customer_memberships as $row_customer_memberships)
                {
                    $customer_memberships[] = [
                        "customer_key" => $customer_key,
                        "ktcume_cu_id" => $row_customer_memberships->ktcume_cu_id,
                        "ktcume_me_id" => $row_customer_memberships->ktcume_me_id
                    ];
                }

                //Get customer memberships and add in array $customer_memberships[]
                $kt_customer_obligations_query = KTCustomerObligation::where("ktcuob_cu_id", $row_customers->cu_id)->get();

                foreach ($kt_customer_obligations_query as $row_customer_obligations)
                {
                    $kt_customer_obligations[] = [
                        "customer_key" => $customer_key,
                        "ktcuob_id" => $row_customer_obligations->ktcuob_id,
                        "ktcuob_cuob_id" => $row_customer_obligations->ktcuob_cuob_id,
                        "ktcuob_cu_id" => $row_customer_obligations->ktcuob_cu_id,
                        "ktcuob_cudo_id" => $row_customer_obligations->ktcuob_cudo_id,
                        "ktcuob_verified_timestamp" => $row_customer_obligations->ktcuob_verified_timestamp,
                        "ktcuob_verification_result" => $row_customer_obligations->ktcuob_verification_result
                    ];
                }

                //Get customer insurances and add in array $customer_insurances[]
                $kt_customer_insurances_query = KTCustomerInsurance::where("ktcuin_cu_id", $row_customers->cu_id)->get();

                foreach ($kt_customer_insurances_query as $row_customer_insurances)
                {
                    $kt_customer_insurances[] = [
                        "customer_key" => $customer_key,
                        "ktcuin_id" => $row_customer_insurances->ktcuin_id,
                        "ktcuin_cuob_id" => $row_customer_insurances->ktcuin_cuob_id,
                        "ktcuin_cu_id" => $row_customer_insurances->ktcuin_cu_id,
                        "ktcuin_cudo_id" => $row_customer_insurances->ktcuin_cudo_id,
                        "ktcuin_verified_timestamp" => $row_customer_insurances->ktcuin_verified_timestamp,
                        "ktcuin_verification_result" => $row_customer_insurances->ktcuin_verification_result,
                        "ktcuin_amount" => $row_customer_insurances->ktcuin_amount
                    ];
                }

                $mover_get_country = KTCustomerPortalCountry::selectRaw("DISTINCT(`ktcupoco_co_code`)")
                    ->leftJoin("kt_customer_portal", "ktcupoco_ktcupo_id", "ktcupo_id")
                    ->leftJoin("payment_rates", "ktcupo_id", "para_ktcupo_id")
                    ->where("ktcupo_cu_id", $row_customers->cu_id)
                    ->where("ktcupo_status", 1)
                    ->whereRaw("`payment_rates`.`para_date` <= NOW()")
                    ->get();

                foreach ($mover_get_country as $row_mover_data_country)
                {
                    $customers_to_countries[] = [
                        "customer_key" => $customer_key,
                        "country_code" => $row_mover_data_country->ktcupoco_co_code
                    ];
                }

                foreach (["past", "recent"] as $type)
                {
                    if ($type == "past")
                    {
                        $timestamp = "<";
                    } elseif ($type == "recent")
                    {
                        $timestamp = ">=";
                    }

                    $surveys = Survey2::leftJoin("requests", "su_re_id", "re_id")
                        ->leftJoin("website_reviews", "su_were_id", "were_id")
                        ->whereRaw("`surveys_2`.`su_submitted_timestamp` " . $timestamp . " '" . date("Y-m", strtotime("-" . $row->we_sirelo_top_mover_months . " months")) . "-01 00:00:00'")
                        ->whereRaw("(
                        `surveys_2`.`su_mover` = '" . $row_customers['cu_id'] . "' OR
                        `surveys_2`.`su_other_mover` = '" . $row_customers['cu_id'] . "'
                    )")
                        ->where("su_submitted", 1)
                        ->where("su_show_on_website", ">", 0)
                        ->orderBy("su_submitted_timestamp", "DESC")
                        ->get();

                    $surveys = System::databaseToArray($surveys);

                    foreach ($surveys as $row_surveys)
                    {
                        if ($row_surveys['su_show_on_website'] == 1)
                        {
                            if ($row_surveys['su_source'] == 1)
                            {
                                $prefix = "re";
                                $author = $row_surveys[$prefix . '_full_name'];
                            } elseif ($row_surveys['su_source'] == 2)
                            {
                                $prefix = "were";
                                $author = $row_surveys[$prefix . '_name'];
                            }

                            if ($row_surveys['su_source'] == 1 || ($row_surveys['su_source'] == 2 && $row_surveys['were_anonymous'] == 1))
                            {
                                $exp = explode(" ", preg_replace("/&([a-z])[a-z]+;/i", "$1", htmlentities(System::rusToLat($author))));

                                $author = "";

                                foreach ($exp as $letter)
                                {
                                    $author .= ((isset($letter[0])) ? strtoupper($letter[0]) . "." : "");
                                }
                            }

                            $reviews[$row_surveys['su_id']] = [
                                "customer_key" => $customer_key,
                                "source" => $row_surveys['su_source'],
                                "author" => $author,
                                "date" => date("Y-m-d", strtotime($row_surveys['su_submitted_timestamp'])),
                                "rating" => $row_surveys['su_rating'],
                                "rating_motivation" => $row_surveys['su_rating_motivation'],
                                "recommend" => $row_surveys['su_recommend_mover'],
                                "pro_1" => $row_surveys['su_pro_1'],
                                "pro_2" => $row_surveys['su_pro_2'],
                                "con_1" => $row_surveys['su_con_1'],
                                "con_2" => $row_surveys['su_con_2'],
                                "city_from" => $row_surveys[$prefix . '_city_from'],
                                "country_from" => $row_surveys[$prefix . '_co_code_from'],
                                "city_to" => $row_surveys[$prefix . '_city_to'],
                                "country_to" => $row_surveys[$prefix . '_co_code_to'],
                                "mover_comment_date" => date("Y-m-d", strtotime($row_surveys['su_mover_comment_timestamp'])),
                                "mover_comment" => $row_surveys['su_mover_comment']
                            ];

                            $customers[$customer_key]['rating'] += $row_surveys['su_rating'];
                            $customers[$customer_key]['reviews']++;

                            if ($type == "recent")
                            {
                                $customers[$customer_key]['rating_recent'] += $row_surveys['su_rating'];
                                $customers[$customer_key]['reviews_recent']++;
                            }

                            if ($row_surveys['su_recommend_mover'] > 0)
                            {
                                $customers[$customer_key]['recommendations']++;

                                if ($type == "recent")
                                {
                                    $customers[$customer_key]['recommendations_recent']++;
                                }

                                if ($row_surveys['su_recommend_mover'] == 1)
                                {
                                    $customers[$customer_key]['recommendation']++;

                                    if ($type == "recent")
                                    {
                                        $customers[$customer_key]['recommendation_recent']++;
                                    }
                                }
                            }

                            //Loop trough all pros & cons of review
                            foreach (["pro_1", "pro_2", "con_1", "con_2"] as $pro_con_type)
                            {
                                $category_id = $row_surveys['su_' . $pro_con_type . '_category'];
                                $category_type = substr($pro_con_type, 0, 3);

                                if ($category_id > 0)
                                {
                                    if (!array_key_exists($category_id, $customers[$customer_key]['pros_cons'][$category_type]))
                                    {
                                        $customers[$customer_key]['pros_cons'][$category_type][$category_id] = [
                                            "name" => $pro_con_categories[$category_id],
                                            "amount" => 1
                                        ];
                                    } else
                                    {
                                        $customers[$customer_key]['pros_cons'][$category_type][$category_id]['amount']++;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            foreach ($customers as $key => $customer)
            {
                if ($customers[$key]['reviews'] > 0)
                {
                    $customers[$key]['rating'] = round($customers[$key]['rating'] / $customers[$key]['reviews'], 2);
                }

                if ($customers[$key]['reviews_recent'] > 0)
                {
                    $customers[$key]['rating_recent'] = round($customers[$key]['rating_recent'] / $customers[$key]['reviews_recent']);
                }

                if ($customers[$key]['recommendations'] > 0)
                {
                    $customers[$key]['recommendation'] = round(($customers[$key]['recommendation'] / $customers[$key]['recommendations']) * 100);
                }

                if ($customers[$key]['recommendations_recent'] > 0)
                {
                    $customers[$key]['recommendation_recent'] = round(($customers[$key]['recommendation_recent'] / $customers[$key]['recommendations_recent']) * 100);
                }

                // If this customer is a top mover
                // If customer's top mover is not disabled
                if ($customers[$key]['rating_recent'] >= 4 &&
                    $customers[$key]['reviews_recent'] >= 12 &&
                    $customers[$key]['recommendation_recent'] >= 80 &&
                    !$customers[$key]['disable_top_mover']
                )
                {
                    $customers[$key]['top_mover'] = 1;
                }

                //Sort and limit both types
                foreach ($customers[$key]['pros_cons'] as $category_type => $category_data)
                {
                    //Sort by amount
                    usort($customers[$key]['pros_cons'][$category_type], function ($a, $b) {
                        return strnatcasecmp($b['amount'], $a['amount']);
                    });

                    //Limit by checking recommendations
                    if ($category_type == "con" && $customers[$key]['recommendations'] > 0)
                    {
                        if ($customers[$key]['recommendation'] >= 85)
                        {
                            $limit = 1;
                        } elseif ($customers[$key]['recommendation'] >= 70)
                        {
                            $limit = 2;
                        } else
                        {
                            $limit = 3;
                        }
                    } else
                    {
                        $limit = 3;
                    }

                    //Limit the amount per category
                    $customers[$key]['pros_cons'][$category_type] = array_slice($customers[$key]['pros_cons'][$category_type], 0, $limit, true);
                }
            }

            $postData = [
                'options' => $options,
                'states' => $states,
                'cities' => $cities,
                'customers' => $customers,
                'customer_offices' => $customer_offices,
                'customer_memberships' => $customer_memberships,
                'customers_to_countries' => $customers_to_countries,
                'reviews' => $reviews,
                'memberships' => $memberships,
                'sirelo_hyperlinks' => $sirelo_hyperlinks,
                'customer_obligations' => $customer_obligations,
                'customer_insurances' => $customer_insurances,
                'kt_customer_obligations' => $kt_customer_obligations,
                'kt_customer_insurances' => $kt_customer_insurances,
            ];

            // CAUTION !!!
            $urlToPostTo = $row->we_sirelo_url . '/wp-content/themes/sirelo/import/import.php';

            $postData = json_encode(['sirelo' => $postData]);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $urlToPostTo);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json',
                'Content-Length: ' . strlen($postData),
            ]);
            $import = curl_exec($ch);
            curl_close($ch);

            echo 'finished doing stuff';
        }
    }


    public function testNewReviewScore_1()
    {

        $customers = Customer::whereIn("cu_id", [1, 2, 1577, 6563, 17222, 6205])->get();
        $customers = Customer::where("cu_co_code", "NL")->where("cu_deleted", 0)->get();
        $total_total_reviews = 0;
        foreach ($customers as $customer)
        {

            $cu_id = $customer->cu_id;

            $surveys =
                DB::table('surveys_2')
                    ->leftJoin("website_reviews", "su_were_id", "were_id")
                    ->where('su_submitted', "=", 1)
                    ->where('su_show_on_website', "=", 1)
                    ->where(function ($query) use ($cu_id) {
                        $query
                            ->where('su_mover', '=', $cu_id)
                            ->orWhere('su_other_mover', '=', $cu_id);
                    })
                    ->get();

            $array = [];
            $average_age = 0;
            $average_source = 0;
            $new_rating = 0;

            $dividend_age = 0;
            $divisor_age = 0;

            $dividend_source = 0;
            $divisor_source = 0;

            $total_review_amount = 0;
            $total_review_score = 0;
            $total_reviews = 0;

            foreach ($surveys as $survey)
            {
                $total_reviews++;

                $date1 = $survey->su_submitted_timestamp;
                $date2 = date("Y-m-d H:i:s");

                $ts1 = strtotime($date1);
                $ts2 = strtotime($date2);

                $year1 = date('Y', $ts1);
                $year2 = date('Y', $ts2);

                $month1 = date('m', $ts1);
                $month2 = date('m', $ts2);

                $months = (($year2 - $year1) * 12) + ($month2 - $month1);

                if ($months > 60)
                {
                    $months = 60;
                }

                if ($months > 24)
                {
                    $weight_percentage_age = round(((60 - $months) / 100 * 2.857) * 100);

                } else
                {
                    $weight_percentage_age = 100;
                }

                $dividend_age += ($weight_percentage_age * $survey->su_rating);
                $divisor_age += $weight_percentage_age;

                if ($survey->su_source == Survey2Source::REQUEST)
                {
                    $weight_percentage_source = 100;
                } else
                {
                    //Website review
                    if ($survey->were_verified && $survey->su_submitted_timestamp > "2020-10-05 00:00:00")
                    {
                        $weight_percentage_source = 100;
                    } elseif ($survey->were_verified)
                    {
                        $weight_percentage_source = 90;
                    } else
                    {
                        $weight_percentage_source = 70;
                    }


                }
                $dividend_source += ($weight_percentage_source * $survey->su_rating);
                $divisor_source += $weight_percentage_source;


                //if ($customer->cu_id == 5839) {


                $correction = round((1 / 100) * ($weight_percentage_source * $weight_percentage_age));

                if ($correction < 1)
                {
                    $correction = 1;
                }


                $review_amount_correction = (1 / 100) * $correction;
                $review_score = ($survey->su_rating / 100) * $correction;

                $review_after_correction = $review_score / $review_amount_correction;

                $total_review_amount += $review_amount_correction;
                $total_review_score += $review_score;

                //ARRAY TO DEBUG:
                $array[$survey->su_id] = [
                    'rating' => $survey->su_rating,
                    'weight_percentage_age' => $weight_percentage_age,
                    'weight_percentage_source' => $weight_percentage_source,
                    'correction' => $correction,
                    'review_amount_correction' => $review_amount_correction,
                    'review_score' => $review_score,
                    'rating_after_correction' => $review_after_correction,
                    'months' => $months
                ];
                //}


            }

            //echo round($total_review_score / $total_review_amount, 2);
            $new_rating = (round($total_review_score / $total_review_amount, 2) * 2);

            if (is_nan($new_rating))
            {
                $new_rating = 0;
            }

            $average_age = $dividend_age / $divisor_age;
            $average_source = $dividend_source / $divisor_source;


            /**
             * Calculate old review score:
             */
            $result =
                DB::table('surveys_2')
                    ->select(DB::raw('COUNT(*) AS reviews, SUM(`su_rating`) AS rating'))
                    ->where('su_submitted', "=", 1)
                    ->where('su_show_on_website', "=", 1)
                    ->where(function ($query) use ($cu_id) {
                        $query
                            ->where('su_mover', '=', $cu_id)
                            ->orWhere('su_other_mover', '=', $cu_id);
                    })
                    ->first();

            $rating = 0;

            if ($result->reviews > 0)
            {
                $rating = round($result->rating / $result->reviews, 2);
            }


            if ((System::numberFormat(($rating * 2), 2, ".", ",") - System::numberFormat($new_rating, 2, ".", "")) > 0.5 || (System::numberFormat(($rating * 2), 2, ".", ",") - System::numberFormat($new_rating, 2, ".", "")) < -0.5)
            {
                echo "<b>" . $customer->cu_company_name_business . " (" . $customer->cu_id . ")</b><br />";
                echo "Old rating = " . System::numberFormat(($rating * 2), 2, ".", ",") . "<br />";
                echo "New rating = " . System::numberFormat($new_rating, 2, ".", "") . "<br />";
                //echo "Total reviews = ".$total_reviews."<br />";
                echo "<br /><br />";
            }


            $total_total_reviews += $total_reviews;
        }

        echo "TOTAL REVIEWS = " . $total_total_reviews;


    }

    public function testNewReviewScore_2()
    {

        $customers = Customer::whereIn("cu_id", [1, 2, 1577, 6563, 17222, 6205])->get();
        $customers = Customer::where("cu_co_code", "NL")->where("cu_deleted", 0)->get();
        $total_total_reviews = 0;
        foreach ($customers as $customer)
        {

            $cu_id = $customer->cu_id;

            $surveys =
                DB::table('surveys_2')
                    ->leftJoin("website_reviews", "su_were_id", "were_id")
                    ->where('su_submitted', "=", 1)
                    ->where('su_show_on_website', "=", 1)
                    ->where(function ($query) use ($cu_id) {
                        $query
                            ->where('su_mover', '=', $cu_id)
                            ->orWhere('su_other_mover', '=', $cu_id);
                    })
                    ->get();

            $array = [];
            $average_age = 0;
            $average_source = 0;
            $new_rating = 0;

            $dividend_age = 0;
            $divisor_age = 0;

            $dividend_source = 0;
            $divisor_source = 0;

            $total_review_amount = 0;
            $total_review_score = 0;
            $total_reviews = 0;

            foreach ($surveys as $survey)
            {
                $total_reviews++;

                $date1 = $survey->su_submitted_timestamp;
                $date2 = date("Y-m-d H:i:s");

                $ts1 = strtotime($date1);
                $ts2 = strtotime($date2);

                $year1 = date('Y', $ts1);
                $year2 = date('Y', $ts2);

                $month1 = date('m', $ts1);
                $month2 = date('m', $ts2);

                $months = (($year2 - $year1) * 12) + ($month2 - $month1);

                if ($months > 60)
                {
                    $months = 60;
                }

                if ($months > 24)
                {
                    $weight_percentage_age = round(((60 - $months) / 100 * 2.857) * 100);

                } else
                {
                    $weight_percentage_age = 100;
                }

                $dividend_age += ($weight_percentage_age * $survey->su_rating);
                $divisor_age += $weight_percentage_age;

                if ($survey->su_source == Survey2Source::REQUEST)
                {
                    $weight_percentage_source = 100;
                } else
                {
                    //Website review
                    if ($survey->were_verified && $survey->su_submitted_timestamp > "2020-10-05 00:00:00")
                    {
                        $weight_percentage_source = 100;
                    } elseif ($survey->were_verified)
                    {
                        $weight_percentage_source = 90;
                    } else
                    {
                        $weight_percentage_source = 70;
                    }


                }

                $dividend_source += ($weight_percentage_source * $survey->su_rating);
                $divisor_source += $weight_percentage_source;


                //if ($customer->cu_id == 5839) {


                $correction = round((1 / 100) * ($weight_percentage_source * $weight_percentage_age));

                if ($correction < 1)
                {
                    $correction = 1;
                }


                $review_amount_correction = (1 / 100) * $correction;
                $review_score = ($survey->su_rating / 100) * $correction;

                $review_after_correction = $review_score / $review_amount_correction;

                $total_review_amount += $review_amount_correction;
                $total_review_score += $review_score;

                //ARRAY TO DEBUG:
                $array[$survey->su_id] = [
                    'rating' => $survey->su_rating,
                    'weight_percentage_age' => $weight_percentage_age,
                    'weight_percentage_source' => $weight_percentage_source,
                    'correction' => $correction,
                    'review_amount_correction' => $review_amount_correction,
                    'review_score' => $review_score,
                    'rating_after_correction' => $review_after_correction,
                    'months' => $months
                ];
                //}


            }

            //echo round($total_review_score / $total_review_amount, 2);
            $new_rating = (round($total_review_score / $total_review_amount, 2) * 2);

            if (is_nan($new_rating))
            {
                $new_rating = 0;
            }

            $average_age = $dividend_age / $divisor_age;
            $average_source = $dividend_source / $divisor_source;


            /**
             * Calculate old review score:
             */
            $result =
                DB::table('surveys_2')
                    ->select(DB::raw('COUNT(*) AS reviews, SUM(`su_rating`) AS rating'))
                    ->where('su_submitted', "=", 1)
                    ->where('su_show_on_website', "=", 1)
                    ->where(function ($query) use ($cu_id) {
                        $query
                            ->where('su_mover', '=', $cu_id)
                            ->orWhere('su_other_mover', '=', $cu_id);
                    })
                    ->first();

            $rating = 0;

            if ($result->reviews > 0)
            {
                $rating = round($result->rating / $result->reviews, 2);
            }


            if ((System::numberFormat(($rating * 2), 2, ".", ",") - System::numberFormat($new_rating, 2, ".", "")) > 1 || (System::numberFormat(($rating * 2), 2, ".", ",") - System::numberFormat($new_rating, 2, ".", "")) < -1)
            {
                echo "<b>" . $customer->cu_company_name_business . " (" . $customer->cu_id . ")</b><br />";
                echo "Old rating = " . System::numberFormat(($rating * 2), 2, ".", ",") . "<br />";
                echo "New rating = " . System::numberFormat($new_rating, 2, ".", "") . "<br />";
                //echo "Total reviews = ".$total_reviews."<br />";
                echo "<br /><br />";
            }


            $total_total_reviews += $total_reviews;
        }

    }

    public function allSireloEmails()
    {
        $mail = new Mail();
        $system = new System();

        /**
         * Sirelo Emails
         */

        echo "<h1>" . "SIRELO EMAILS" . "</h1>";
        echo "<hr>";

        $row = \App\Models\Request::leftJoin("kt_request_customer_portal", "ktrecupo_re_id", "re_id")
            ->leftJoin("customers", "ktrecupo_cu_id", "cu_id")
            ->where("re_id", 833905)
            ->limit(1)
            ->orderBy("re_id", "DESC")
            ->first();

        $row = System::databaseToArray($row);

        $survey_row = Survey2::leftJoin("requests", "su_re_id", "re_id")
            ->leftJoin("website_reviews", "su_were_id", "were_id")
            ->leftJoin("website_forms", "were_wefo_id", "wefo_id")
            ->leftJoin("customers", "cu_id", "su_mover")
            ->whereNotNull("su_mover")
            ->orderBy("su_id", "desc")
            ->limit(1)
            ->first();

        $website_review_row = Survey2::leftJoin("requests", "su_re_id", "re_id")
            ->leftJoin("website_reviews", "su_were_id", "were_id")
            ->leftJoin("website_forms", "were_wefo_id", "wefo_id")
            ->leftJoin("customers", "cu_id", "su_mover")
            ->where("su_id", 539909)
            ->where("cu_deleted", 0)
            ->orderBy("su_id", "desc")
            ->limit(1)
            ->first();

        $website_review_row = System::databaseToArray($website_review_row);


        $row['cu_la_code'] = "EN";
        $row['re_la_code'] = "EN";

        $fields = [
            "request" => $row,
            "pd_link" => "https://sirelo.org",
            "survey" => $website_review_row,
            'telephone_number' => '06123'
        ];


        echo "<center><h3>survey_2_invite</h3></center>";
        echo $mail->send("survey_2_invite", $row['re_la_code'], $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>survey_2_reminder</h3></center>";
        echo $mail->send("survey_2_reminder", $row['re_la_code'], $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>survey_1_invite</h3></center>";
        echo $mail->send("survey_1_invite", $row['re_la_code'], $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>survey_confirm</h3></center>";
        echo $mail->send("survey_confirm", $row['re_la_code'], $fields, null, null, null, true);
        echo "<hr>";


        $customers_query = Customer::leftJoin("mover_data", "cu_id", "moda_cu_id")
            ->where("cu_deleted", 0)
            ->limit(3)
            ->get();

        $customers = [];

        $customers_query = System::databaseToArray($customers_query);

        foreach ($customers_query as $cust) {
            $sirelo_data = [
                "sirelo_valid" => 1,
                "sirelo_page" => Data::getSireloCustomerPage($cust['cu_id']),
                "rating" => number_format((2 * 2), 1, ",", ""),
                "reviews" => 30
            ];

            $customers[] = ["type" => "post"] + $cust + $sirelo_data;
        }

        $fields = [
            "request" => System::databaseToArray($row),
            "pd_link" => "https://sirelo.org",
            "customers" => $customers,
            "telephone_number" => "06123"
        ];

        echo "<center><h3>request_date_too_far</h3></center>";
        echo $mail->send("request_date_too_far", $row['re_la_code'], $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>request_match</h3></center>";
        echo $mail->send("request_match", $row['re_la_code'], $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>request_rematch</h3></center>";
        echo $mail->send("request_rematch", $row['re_la_code'], $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>request_wrong_telephone_number</h3></center>";
        echo $mail->send("request_wrong_telephone_number", $row['re_la_code'], $fields, null, null, null, true);
        echo "<hr>";


        $voca_row = VolumeCalculator::where("voca_id", 19)->first();
        $voca_row = System::databaseToArray($voca_row);

        $website_review_row = WebsiteReview::leftJoin("website_forms", "were_wefo_id", "wefo_id")
            ->leftJoin("websites", "wefo_we_id", "we_id")
            ->orderBy("were_id", "DESC")
            ->first();

        $website_review_row = System::databaseToArray($website_review_row);


        $fields = [
            "to" => $row['cu_email'],
            "website_review" => $website_review_row,
            "customer_name" => "ABC Movers",
            "company_name" => $row['cu_company_name_business'],
            "from_name" => 'Sirelo',
            "sirelo_link" => Data::getSireloCustomerPage($row['cu_id']),
            "register_company_link" => Data::getSireloFieldByCountry($row['cu_co_code'], "we_register_company_location"),
            "claim_company_link" => Data::getSireloFieldByCountry($row['cu_co_code'], "we_register_company_location") . "?customer=" . System::getSeoKey($row['cu_city']) . System::getSeoKey($row['cu_company_name_business']),
            "name" => $row['cu_company_name_business'],
            "customer_page" => Data::getSireloCustomerPage($row['cu_id']),
            "survey" => $system->databaseToArray($survey_row),
            "email" => "arjandeconinck@ive.nl",
            "volume_calculator" => $voca_row['voca_volume_calculator'],
            "total_volume_m3" => $_POST['total_volume_m3'],
            "total_volume_ft3" => $_POST['total_volume_ft3'],
            "total_volume_m3_ft3" => ((!empty($voca_row['voca_volume_m3']) && !empty($_POST['voca_volume_ft3'])) ? $_POST['voca_volume_m3'] . " m<sup>3</sup>" . " / " . $_POST['voca_volume_ft3'] . " ft<sup>3</sup>" : ""),
            "website_url" => "https://sirelo.org",
            "website_name" => "Sirelo.org",
            "quote_form_location" => "https://sirelo.org",
            "vc_token" => "123123123",
            "pd_link" => "https://sirelo.org",
            "request" => $row,
            "telephone_number" => "06123"
        ];

        echo "<center><h3>request_submit</h3></center>";
        echo $mail->send("request_submit", "EN", $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>sirelo_profile_changed</h3></center>";
        echo $mail->send("sirelo_profile_changed", $row['cu_la_code'], $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>sirelo_profile_created</h3></center>";
        echo $mail->send("sirelo_profile_created", $row['cu_la_code'], $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>sirelo_review_comment</h3></center>";
        echo $mail->send("sirelo_review_comment", $row['cu_la_code'], $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>website_review_accept</h3></center>";
        echo $mail->send("website_review_accept", $row['cu_la_code'], $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>website_review_reject</h3></center>";
        echo $mail->send("website_review_reject", $row['cu_la_code'], $fields, null, null, null, true);
        echo "<hr>";

        echo "<center><h3>website_review_submit</h3></center>";
        echo $mail->send("website_review_submit", $row['cu_la_code'], $fields, null, null, null, true);
        echo "<hr>";


        //Mail::send("volume_calculator", LANGUAGE, $fields);

        echo "<center><h3>volume_calculator</h3></center>";
        echo $mail->send("volume_calculator", $row['cu_la_code'], $fields, null, null, null, true);
        echo "<hr>";

        /*echo $mail->send("sirelo_review_published", $row['cu_la_code'], $fields, null,null,null,true);
        echo "<hr>";*/

        $rir = SireloReviewInvite::leftJoin("customers", "sirein_cu_id", "cu_id")
            ->where("sirein_sent", 1)
            ->where("cu_deleted", 0)
            ->orderBy("sirein_timestamp", "ASC")
            ->first();
        $rir = $system->databaseToArray($rir);

        $subject = str_replace("[name]", $rir['sirein_name'], $rir['sirein_subject']);
        $body = str_replace("[name]", $rir['sirein_name'], nl2br($rir['sirein_content']));

        $body = preg_replace("/\[button\](.*)\[\/button\]/", $mail->buttonNew(Data::getSireloCustomerPage($rir['cu_id']) . Data::getSireloFieldByCountry($rir['cu_co_code'], "we_sirelo_customer_review_slug"), "$1"), $body);

        $fields = [
            "sirein_id" => $rir['sirein_id'],
            "email" => $rir['sirein_email'],
            "country" => $rir['cu_co_code'],
            "subject" => $subject,
            "body" => $body,
        ];

        echo "<center><h3>sirelo_review_invite</h3></center>";
        echo $mail->send("sirelo_review_invite", Data::getSireloFieldByCountry($rir['cu_co_code'], "we_sirelo_la_code"), $fields, null, null, null, true);

    }

}

//php artisan tinker
//$controller = app()->make( 'App\Http\Controllers\AdminController');
//app()->call([$controller, 'cityToStates']);
