<?php

namespace App\Http\Controllers;

use App\Data\CreditDebitInvoiceType;
use App\Data\CreditDebitType;
use App\Data\InvoiceAmount;
use App\Data\InvoicePeriod;
use App\Data\PaymentMethod;
use App\Data\RequestCustomerPortalType;
use App\Data\YesNo;
use App\Functions\DataIndex;
use App\Functions\Mail;
use App\Functions\Mover;
use App\Functions\System;
use App\Functions\Translation;
use App\Models\Country;
use App\Models\CreditDebitInvoiceLine;
use App\Models\Customer;
use App\Models\Invoice;
use App\Models\InvoiceLine;
use App\Models\KTCustomerSubscription;
use App\Models\LedgerAccount;
use App\Models\PaymentCurrency;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Log;

class CreateInvoicesController extends Controller
{
	public function index()
	{
	    //Get the first letters
        $letters = [
            "Letters" => [],
            "Numbers" => []
        ];

        $letters_array = DB::table("customers")
            ->selectRaw("UPPER(SUBSTRING(`cu_company_name_business`, 1, 1)) AS `letter`")
            ->selectRaw("COUNT(*) AS `nr_of_customers`")
            ->where("cu_deleted", 0)
            ->groupBy(DB::raw("SUBSTRING(`cu_company_name_business`, 1, 1)"))
            ->orderBy("letter", 'asc')
            ->get();

        foreach($letters_array as $letter_row)
        {
            $letter = $letter_row->letter;
            if(preg_match('/^[0-9]/', $letter))
            {
                $key = "Numbers";
            }
            elseif(preg_match('/^[a-zA-Z]/', $letter))
            {
                $key = "Letters";
            }
            else
            {
                $key = "Others";
            }


            $letters[$key][$letter] = "{$letter} ({$letter_row->nr_of_customers})";
        }

        for ($year = date("Y"); $year >= 2018; $year--)
        {
            $years[] = $year;
        }

        return view('finance.createinvoices', [
            'years' => $years,
            'months' => DataIndex::getMonths(),
            'yesno' => YesNo::all(),
            'letters' => $letters,
            'customers' => Customer::select("cu_id", "cu_company_name_business")->whereIn("cu_type", [1,2,4,6])->where("cu_deleted", 0)->orderby("cu_company_name_business")->get(),
            'invoiceperiods' => InvoicePeriod::all(),
            'paymentmethods' => PaymentMethod::all(),
            'invoiceamounts' => InvoiceAmount::all(),
            'creditdebitinvoicetypes' => CreditDebitInvoiceType::all()
		]);
	}

	public function previewInvoices(Request $request)
	{
        $request->validate([
            'year' => 'required',
            'period_from' => 'required',
            'period_to' => 'required',
            'month' => 'required'
        ]);

        $system = new System();
        $translate = new Translation();

        $collected_translation = json_decode($translate->getMultiple(
            [
                "invoice_received_requests",
                "invoice_requests",
                "invoice_free_requests",
                "invoice_no_claim_discount",
                "request_type_val_1",
                "request_type_val_2",
                "request_type_val_3",
                "request_type_val_4",
                "request_type_val_5",
                "invoice_payment_method_1",
                "invoice_payment_method_2",
                "invoice_payment_method_3",
                "invoice_payment_method_4",
                "invoice_payment_method_5",
                "invoice_payment_method_6",
                "invoice_free_requests_previous_invoice",
                "invoice_paid_requests_previous_invoice",
                "invoice_miscellaneous",
                "yes",
                "no"
            ]
        ), true);

        $single_month = $request->single_month == 1;
        $period_from = $request->period_from;
        $period_to = $request->period_to;
        $only_credit_debit = $request->credit_debit_lines >= 1;

        define("CUSTOMER_REQUESTS", "R");
        define("QUESTION_REQUESTS", "S");
        define("CREDIT_DEBIT_LINES", "C");

        define("KTRECUPO", 'ktrecupo');
        define("KTRECUQU", 'ktrecuqu');
        define("REQUESTS", 're');
        define("QUESTIONS", 'qu');

        if(!empty($request->customer))
        {
            $filter_customers = ([$request->customer]);
        }
        else
        {
            $filter_customers = ([]);
        }

        $posted_letters = $request->first_letters ?? [];

        $first_letters = [];
        foreach($posted_letters as $letter)
        {
            $first_letters[] = "`cu_company_name_legal` LIKE '".$letter."%'";
        }


        $eu_countries = ['AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'EL', 'ES', 'FI', 'FR', 'GB', 'HR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK'];

        $customers_array = DB::table("customers")
            ->select("moda_no_claim_discount", "moda_no_claim_discount_percentage")
            ->selectRaw("customers.*")
            ->leftJoin("mover_data", "cu_id", "moda_cu_id")
            ->where("cu_invoice_period", $request->invoice_period)
            ->where("cu_deleted", 0);


        if ($request->unions == 1)
        {
            $customers_array->whereIn("cu_co_code", $eu_countries);
        }
        elseif ($request->unions == 2)
        {
            $customers_array->whereNotIn("cu_co_code", $eu_countries);
        }

        if(!empty($filter_customers))
        {
            $customers_array->whereIn("cu_id", $filter_customers);
        }
        if(!empty($first_letters))
        {
            $customers_array->whereRaw(implode(" OR ", $first_letters));
        }
        if(!empty($request->payment_method))
        {
            $customers_array->where("cu_payment_method", $request->payment_method);
        }

        $customers_array = $customers_array->get();

        $customers_array = $system->databaseToArray($customers_array);

        $customers = [];
        $customer_ids = [];

        foreach($customers_array as $cu_row)
        {
            $cu_row[CUSTOMER_REQUESTS] = [];
            $cu_row[QUESTION_REQUESTS] = [];
            $cu_row[CREDIT_DEBIT_LINES] = [];
            $cu_row["subscriptions"] = [];

            $subscriptions = KTCustomerSubscription::leftJoin("subscriptions", "sub_id", "ktcusu_sub_id")
                ->where("ktcusu_cu_id", $cu_row['cu_id'])
                ->where("ktcusu_active", 1)
                ->where("ktcusu_invoiced", 0)
                ->get();
            if (count($subscriptions) > 0) {
                $cu_row['subscriptions'] = "HAS SUBSCRIPTIONS";
            }


            $cu_id = $cu_row['cu_id'];
            $customers[$cu_id] = $cu_row;
            $customer_ids[$cu_id] = $cu_id;
        }

        //Check if not empty
        if(empty($customers))
        {
            return redirect()->back()->withErrors([ 'errors' => "No customers found with these settings!"]);
        }

        //Gather general things
        //=> Get non deleted portals
        $available_portals = DB::table("portals")
            ->select("po_id", "po_pacu_code", "po_default_website")
            ->get();

        $available_portals = $system->queryToArray($available_portals, 'po_id');

        //=> Get websites
        $available_websites = DB::table("websites")
            ->select("we_id", "we_mail_from_name")
            ->get();

        $available_websites = $system->queryToArray($available_websites, 'we_id');

        $crdeinli = DB::table("credit_debit_invoice_lines")
            ->whereIn("crdeinli_cu_id", $customer_ids)
            ->where("crdeinli_invoiced", 0);

        if ($request->credit_debit_lines == CreditDebitInvoiceType::ONLY_CREDIT_LINES) {
            $crdeinli->where("crdeinli_type", CreditDebitType::CREDIT);
        }elseif($request->credit_debit_lines == CreditDebitInvoiceType::ONLY_DEBIT_LINES) {
            $crdeinli->where("crdeinli_type", CreditDebitType::DEBIT);
        }

        $crdeinli = $crdeinli->orderBy("crdeinli_timestamp",  'asc')
            ->get();

        $crdeinli = $system->databaseToArray($crdeinli);

        self::gatherIntoCustomers(
            $customers, CREDIT_DEBIT_LINES, $crdeinli, 'crdeinli', $additional_data
        );

        if(!$only_credit_debit)
        {
            $ktrecupo = DB::table("kt_request_customer_portal")
                ->whereIn("ktrecupo_cu_id", $customer_ids)
                ->whereIn("ktrecupo_po_id", array_keys($available_portals))
                ->whereIn("ktrecupo_type", [RequestCustomerPortalType::PAID,  RequestCustomerPortalType::LEADS_STORE])
                ->where("ktrecupo_invoiced", 0);

            if($single_month)
            {
                $ktrecupo->whereBetween("ktrecupo_timestamp", [$period_from." 00:00:00", $period_to." 23:59:59"]);
            }
            else
            {
                $ktrecupo->where("ktrecupo_timestamp", "<=" ,$period_to." 23:59:59");
            }

            $ktrecupo = $ktrecupo->orderBy("ktrecupo_timestamp", 'asc')
                ->get();

            $ktrecupo = $system->databaseToArray($ktrecupo);

            self::gatherIntoCustomers($customers, CUSTOMER_REQUESTS, $ktrecupo, 'ktrecupo', $additional_data);

            $ktrecuqu = DB::table("kt_request_customer_question")
                ->whereIn("ktrecuqu_cu_id", $customer_ids)
                ->where("ktrecuqu_sent", 1)
                ->where("ktrecuqu_hide", 0)
                ->where("ktrecuqu_invoiced", 0);

            if($single_month)
            {
                $ktrecuqu->whereBetween("ktrecuqu_sent_timestamp", [$period_from." 00:00:00", $period_to." 23:59:59"]);
            }
            else
            {
                $ktrecuqu->where("ktrecuqu_sent_timestamp", "<=" , $period_to." 23:59:59");
            }

            $ktrecuqu = $ktrecuqu->orderBy("ktrecuqu_sent_timestamp", 'asc')
                ->get();

            $ktrecuqu = $system->databaseToArray($ktrecuqu);

            self::gatherIntoCustomers($customers, QUESTION_REQUESTS, $ktrecuqu, 'ktrecuqu', $additional_data);
        }

        //Gather ktre's, requests & questions
        $found_extras = [];
        foreach([
                    'kt_request_customer_portal' => KTRECUPO,
                    'kt_request_customer_question' => KTRECUQU,
                    'requests' => REQUESTS,
                    'questions' => QUESTIONS
                ]
                as $table => $key)
        {
            if(!empty($additional_data[$key]))
            {
                $query = DB::table($table)
                    ->whereIn("{$key}_id", $additional_data[$key])
                    ->get();

                $query = $system->databaseToArray($query);

                foreach($query as $row)
                {
                    $found_extras[$key][$row[$key.'_id']] = $row;
                    self::checkForAdditionalData($additional_data, $row, $key);
                }
            }
        }

        //Filter out customers
        foreach($customers as $cu_id => &$customer)
        {
            if(empty($customer[CUSTOMER_REQUESTS]) && empty($customer[QUESTION_REQUESTS]) && empty($customer[CREDIT_DEBIT_LINES]) && empty($customer["subscriptions"]))
            {
                unset($customers[$cu_id]);
                unset($customer_ids[$cu_id]);
            }
            unset($customer);
        }

        if(empty($customers))
        {
            return redirect()->back()->withErrors([ 'errors' => "No invoices for these customers found!"]);
        }

        //Get customer balances (for prepayment customers)
        $prepayment_customers = [];
        foreach($customers as $cu_id => &$customer)
        {
            if($customer['cu_payment_method'] == 4)
            {
                $prepayment_customers[] = $cu_id;
                $customer['balance'] = 0;
            }
            unset($customer);
        }

        //Get customer statuses
        $customer_statuses = $system->getMultipleCustomerStatuses(array_keys($customers));

        //Prepare general invoice data
        $ledger_accounts = LedgerAccount::all();
        $currencies = PaymentCurrency::all();

        $request_fields_ktrecupo = ['ktrecupo_id', 'ktrecupo_cu_re_id', 'ktrecupo_timestamp', 'ktrecupo_free'];
        $request_fields_ktrecuqu = ['ktrecuqu_id', 'ktrecuqu_cu_re_id', 'ktrecuqu_sent_timestamp', 'ktrecuqu_free'];

        //Build the invoices
        $preview_invoices = [];

        foreach($customers as $cu_id => $customer)
        {

            //Build the debtor data
            $additional_information = $request->additional_information ?? "";

            //Calculation vars
            $invoice_period_from = null;
            $cu_la_code = $customer['cu_la_code'];
            $cu_pacu_code = $customer['cu_pacu_code'];
            $requests = [];

            $soap_failed = false;

            //Build the lines
            $invoice_lines = 0;
            $invoice_amount = 0;
            $invoice_amount_discount = 0;
            $invoice_amount_gross = 0;
            $invoice_amount_eur = 0;
            $invoice_amount_discount_eur = 0;
            $invoice_amount_gross_eur = 0;

            $subscriptions = KTCustomerSubscription::leftJoin("subscriptions", "sub_id", "ktcusu_sub_id")
                ->where("ktcusu_cu_id", $cu_id)
                ->where("ktcusu_active", 1)
                ->where("ktcusu_invoiced", 0)
                ->get();
            if (count($subscriptions) > 0) {
                foreach ($subscriptions as $sub)
                {
                    $invoice_lines++;
                }
            }

            //=> ktrecupo lines
            foreach($customer[CUSTOMER_REQUESTS] as $ktrecupo_row)
            {
                //General stuff
                $invoice_lines++;
                if($invoice_period_from === null)
                {
                    $invoice_period_from = date("Y-m", strtotime($ktrecupo_row['ktrecupo_timestamp']))."-01";
                }

                //Site key / group
                $portal = $available_portals[$ktrecupo_row['ktrecupo_po_id']];
                $po_pacu_code = $portal['po_pacu_code'];
                $request_row = $found_extras[REQUESTS][$ktrecupo_row['ktrecupo_re_id']];
                $we_mail = $available_websites[$portal['po_default_website']]['we_mail_from_name'];
                $site_key = $we_mail." - ".lcfirst($collected_translation["invoice_received_requests"][$cu_la_code])
                    ." (".$collected_translation["request_type_val_".$request_row['re_request_type']][$cu_la_code].")";

                //Calculate amounts
                $amount = System::getAvailableCurrencyConvert($currencies, $ktrecupo_row['ktrecupo_amount'], (($ktrecupo_row['ktrecupo_type'] == 5) ? $cu_pacu_code : $po_pacu_code), $cu_pacu_code);
                $amount_discount = System::getAvailableCurrencyConvert($currencies, $ktrecupo_row['ktrecupo_amount_discount'], (($ktrecupo_row['ktrecupo_type'] == 5) ? $cu_pacu_code : $po_pacu_code), $cu_pacu_code);
                $amount_gross = System::getAvailableCurrencyConvert($currencies, $ktrecupo_row['ktrecupo_amount_netto'], (($ktrecupo_row['ktrecupo_type'] == 5) ? $cu_pacu_code : $po_pacu_code), $cu_pacu_code);
                $amount_eur = System::getAvailableCurrencyConvert($currencies, $ktrecupo_row['ktrecupo_amount'], (($ktrecupo_row['ktrecupo_type'] == 5) ? $cu_pacu_code : $po_pacu_code), "EUR");
                $amount_discount_eur = System::getAvailableCurrencyConvert($currencies, $ktrecupo_row['ktrecupo_amount_discount'], (($ktrecupo_row['ktrecupo_type'] == 5) ? $cu_pacu_code : $po_pacu_code), "EUR");
                $amount_gross_eur = System::getAvailableCurrencyConvert($currencies, $ktrecupo_row['ktrecupo_amount_netto'], (($ktrecupo_row['ktrecupo_type'] == 5) ? $cu_pacu_code : $po_pacu_code), "EUR");

                if($ktrecupo_row['ktrecupo_free'])
                {
                    $invoice_lines++;
                }
                else
                {
                    //Add to total amounts
                    $invoice_amount += $amount;
                    $invoice_amount_discount += $amount_discount;
                    $invoice_amount_gross += $amount_gross;
                    $invoice_amount_eur += $amount_eur;
                    $invoice_amount_discount_eur += $amount_discount_eur;
                    $invoice_amount_gross_eur += $amount_gross_eur;
                }

                //Add to list of requests
                self::addToRequests(
                    $requests, $site_key, $ktrecupo_row, $request_row,
                    $request_fields_ktrecupo, $cu_la_code,
                    $collected_translation
                );

                unset($ktrecupo_row);
            }

            //=> ktrecuqu lines
            foreach($customer[QUESTION_REQUESTS] as $ktrecuqu_row)
            {
                //General stuff
                $invoice_lines++;
                if($invoice_period_from === null)
                {
                    $invoice_period_from = date("Y-m", strtotime($ktrecuqu_row['ktrecuqu_sent_timestamp']))."-01";
                }

                //Site key / group
                $question_row = $found_extras[QUESTIONS][$ktrecuqu_row['ktrecuqu_qu_id']];
                $site_key =  $collected_translation["invoice_received_requests"][$cu_la_code]." (".$question_row['qu_name'].")";

                //Calculate amounts
                $amount = System::getAvailableCurrencyConvert($currencies, $ktrecuqu_row['ktrecuqu_amount'], $ktrecuqu_row['ktrecuqu_currency'], $cu_pacu_code);
                $amount_eur = System::getAvailableCurrencyConvert($currencies, $ktrecuqu_row['ktrecuqu_amount'], $ktrecuqu_row['ktrecuqu_currency'], "EUR");



                if($ktrecuqu_row['ktrecuqu_free'])
                {
                    $invoice_lines++;
                }
                else
                {
                    //Add to total amounts
                    $invoice_amount += $amount;
                    $invoice_amount_gross += $amount;
                    $invoice_amount_eur += $amount_eur;
                    $invoice_amount_gross_eur += $amount_eur;
                }

                //Add to list of requests
                self::addToRequests(
                    $requests, $site_key, $ktrecuqu_row,
                    $found_extras[REQUESTS][$ktrecuqu_row['ktrecuqu_re_id']] ?? [],
                    $request_fields_ktrecuqu, $cu_la_code,
                    $collected_translation
                );

                unset($ktrecuqu_row);
            }

            //=> credit_debit_lines
            foreach($customer[CREDIT_DEBIT_LINES] as $crdeinli_row)
            {
                //General stuff
                $invoice_lines++;
                $type = $crdeinli_row['crdeinli_type'] == 1 ? "free" : "paid";

                //Calculate amounts
                $amount = System::getAvailableCurrencyConvert($currencies, $crdeinli_row['crdeinli_amount'], $crdeinli_row['crdeinli_currency'], $cu_pacu_code);
                $amount_eur = System::getAvailableCurrencyConvert($currencies, $crdeinli_row['crdeinli_amount'], $crdeinli_row['crdeinli_currency'], "EUR");

                if(!empty($crdeinli_row['crdeinli_ktrecupo_id']))
                {
                    $ktrecupo_row = $found_extras[KTRECUPO][$crdeinli_row['crdeinli_ktrecupo_id']];
                    $request_row = $found_extras[REQUESTS][$ktrecupo_row['ktrecupo_re_id']];

                    $site_key = $collected_translation["invoice_".$type."_requests_previous_invoice"][$cu_la_code].
                        " (".$collected_translation["request_type_val_".$request_row['re_request_type']][$cu_la_code].")";

                    self::addToRequests(
                        $requests, $site_key, $ktrecupo_row, $request_row,
                        $request_fields_ktrecupo, $cu_la_code,
                        $collected_translation
                    );
                }

                if(!empty($crdeinli_row['crdeinli_ktrecuqu_id']))
                {
                    $ktrecuqu_row = $found_extras[KTRECUQU][$crdeinli_row['crdeinli_ktrecuqu_id']];
                    $question_row = $found_extras[QUESTIONS][$ktrecuqu_row['ktrecuqu_qu_id']];
                    $request_row = $found_extras[REQUESTS][$ktrecuqu_row['ktrecuqu_re_id']];

                    $site_key = $collected_translation["invoice_".$type."_requests_previous_invoice"][$cu_la_code].
                        " (".$question_row['qu_name'].")";

                    self::addToRequests(
                        $requests, $site_key, $ktrecuqu_row, $request_row,
                        $request_fields_ktrecuqu, $cu_la_code,
                        $collected_translation
                    );
                }

                //Add to total amounts
                $invoice_amount += $amount;
                $invoice_amount_gross += $amount;
                $invoice_amount_eur += $amount_eur;
                $invoice_amount_gross_eur += $amount_eur;

                unset($crdeinli_row);
            }

            $invoice_amount_no_claim = 0;

            if($customer['moda_no_claim_discount'] && !empty($requests))
            {
                $invoice_amount_no_claim = round($invoice_amount_gross * ($customer['moda_no_claim_discount_percentage'] / 100), 2);
                $invoice_amount_gross -= $invoice_amount_no_claim;
            }

            $invoice_subscriptions_block = [];
            $invoice_subscriptions_total = 0;
            $invoice_subscriptions_total_eur = 0;

            $subscriptions = KTCustomerSubscription::leftJoin("subscriptions", "sub_id", "ktcusu_sub_id")
                ->where("ktcusu_cu_id", $customer['cu_id'])
                ->where("ktcusu_active", 1)
                ->where("ktcusu_invoiced", 0)
                ->get();

            if (count($subscriptions) > 0) {
                foreach($subscriptions as $subscription)
                {
                    $pacu_token = PaymentCurrency::where("pacu_code", $subscription->ktcusu_pacu_code)->first()->pacu_token;
                    $invoice_subscriptions_block[] = [
                        "description" => $subscription->sub_name." (".$subscription->ktcusu_start_date."/".$subscription->ktcusu_end_date.")",
                        "price" => $subscription->ktcusu_price_this_month,
                        "currency" => $subscription->ktcusu_pacu_code,
                        "price_excl" => $pacu_token." ".$system->numberFormat($subscription->ktcusu_price_this_month, 2, ",", ".")
                    ];

                    $invoice_subscriptions_total = $invoice_subscriptions_total + $subscription->ktcusu_price_this_month;
                    $invoice_subscriptions_total_eur = $invoice_subscriptions_total_eur + $system->currencyConvert($subscription->ktcusu_price_this_month, $subscription->ktcusu_pacu_code,"EUR");
                }


                $invoice_amount_gross += round($invoice_subscriptions_total, 2);
                $invoice_amount += round($invoice_subscriptions_total, 2);
                $invoice_subscriptions_total = $pacu_token." ".System::numberFormat($invoice_subscriptions_total,2, ",", ".");
            }

            //Calculate netto
            $ledger_vat = ($ledger_accounts->where("leac_number", $customer['cu_leac_number'])->first()->leac_vat ?? false);
            $invoice_amount_vat = ($invoice_amount_gross / 100) * $ledger_vat;
            $invoice_amount_netto = $invoice_amount_gross + $invoice_amount_vat;

            if(($_POST['invoice_amount'] == 100 && $invoice_amount_netto > 100) || ($_POST['invoice_amount'] == 0 && $invoice_amount_netto == 0) || $_POST['invoice_amount'] == "")
            {
                $token = ($currencies->where("pacu_code", $customer['cu_pacu_code'])->first()->pacu_token ?? "?")." ";
                $disabled = empty($customer['cu_leac_number']) ? ["disabled" => "disabled"] : [];
                $valid = empty($disabled) ? 1 : 0;


                if ($request->unions != 2 && env("APP_ENV") == "production") {
                    /*
                     * START VAT CHECK
                     */

                    //Check if customer has a VAT number in their account
                    if (!empty($customer['cu_vat_number']))
                    {
                        //Replace spaces in VAT number
                        $complete_vat = str_replace(" ", "", $customer['cu_vat_number']);

                        //Get VAT number and Country Code exploded e.g. VAT NUMBER = NL0123456789 -> Country code = NL & VAT number = 0123456789
                        $vat_number = substr($complete_vat, 2);
                        $country_code = substr($complete_vat, 0, 2);

                        //If country code is in the list of allowed countries
                        if (in_array($country_code, $eu_countries))
                        {
                            //Set params to send to SOAP
                            $params = new \stdClass();
                            $params->countryCode = $country_code;
                            $params->vatNumber = $vat_number;

                            try {
                                //Set SoapClient
                                $client = new \SoapClient("http://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl");

                                $response = $client->checkVat($params);

                                //Set valid = 2 when the VAT number is invalid
                                if ($response->valid !== true)
                                {
                                    $valid = 2;
                                }
                            }
                            catch(\SoapFault $fault) {
                                $soap_failed = true;
                            }
                        }
                    }
                    /*
                     * END VAT CHECK
                     */
                }

                $preview_invoices[] = [
                    "cu_id" => $cu_id,
                    "valid" => $valid,
                    "cu_company_name_legal" => $customer['cu_company_name_legal'],
                    "status" => $customer_statuses[$cu_id],
                    "invoice_lines" => $invoice_lines,
                    "amount" => $token.System::numberFormat($invoice_amount, 2),
                    "discount" => $token.System::numberFormat($invoice_amount_discount, 2),
                    "amount_exc" => $token.System::numberFormat($invoice_amount_gross, 2),
                    "amount_incl" => $token.System::numberFormat($invoice_amount_netto, 2),
                    "preview" => [
                        "cu_id" => $cu_id,
                        "period_from" => $period_from,
                        "period_to" => $period_to,
                        "single_month" => $single_month ? 1 : 0,
                        "invoice_date" => $request->invoice_date,
                        "only_credit_debit_invoice_lines" => $request->credit_debit_lines,
                        "additional_information" => $additional_information
                    ]
                ];
            }
        }

        return view('finance.createinvoices', [
            "invoices" => $preview_invoices,
            "request" => $request,
            'soap_failed' => $soap_failed
        ]);

	}

    public function createInvoices(Request $request)
    {
        Log::debug("Creating invoices:");
        Log::debug($request);

        $request->validate([
            'period_from' => 'required',
            'period_to' => 'required',
            'invoices' => 'required'
        ]);

        $system = new System();
        $translate = new Translation();

        $collected_translation = json_decode($translate->getMultiple(
            [
                "invoice_received_requests",
                "invoice_requests",
                "invoice_free_requests",
                "invoice_no_claim_discount",
                "invoice_no_claim_discount_with_price",
                "invoice_no_claim_discount_without_percentage",
                "request_type_val_1",
                "request_type_val_2",
                "request_type_val_3",
                "request_type_val_4",
                "request_type_val_5",
                "invoice_payment_method_1",
                "invoice_payment_method_2",
                "invoice_payment_method_3",
                "invoice_payment_method_4",
                "invoice_payment_method_5",
                "invoice_payment_method_6",
                "invoice_free_requests_previous_invoice",
                "invoice_paid_requests_previous_invoice",
                "invoice_miscellaneous",
                "subscriptions",
                "yes",
                "no"
            ]
        ), true);

        $single_month = $request->single_month == 1;
        $period_from = $request->period_from;
        $period_to = $request->period_to;
        $only_credit_debit = $request->credit_debit_lines >= 1;

        define("CUSTOMER_REQUESTS", "R");
        define("QUESTION_REQUESTS", "S");
        define("CREDIT_DEBIT_LINES", "C");

        define("KTRECUPO", 'ktrecupo');
        define("KTRECUQU", 'ktrecuqu');
        define("REQUESTS", 're');
        define("QUESTIONS", 'qu');

        $filter_customers = array_keys($request->invoices);

        $customers_array = DB::table("customers")
            ->select("moda_no_claim_discount", "moda_no_claim_discount_percentage")
            ->selectRaw("customers.*")
            ->leftJoin("mover_data", "cu_id", "moda_cu_id")
            ->where("cu_invoice_period", $request->invoice_period)
            ->where("cu_deleted", 0)
            ->whereIn("cu_id", $filter_customers)
            ->get();

        $customers_array = $system->databaseToArray($customers_array);

        $customers = [];
        $customer_ids = [];

        foreach($customers_array as $cu_row)
        {
            $cu_row[CUSTOMER_REQUESTS] = [];
            $cu_row[QUESTION_REQUESTS] = [];
            $cu_row[CREDIT_DEBIT_LINES] = [];

            $cu_id = $cu_row['cu_id'];
            $customers[$cu_id] = $cu_row;
            $customer_ids[$cu_id] = $cu_id;
        }

        //Check if not empty
        if(empty($customers))
        {
            return redirect()->back()->withErrors([ 'errors' => "No customers found with these settings!"]);
        }


        //Gather general things
        //=> Get non deleted portals
        $available_portals = DB::table("portals")
            ->select("po_id", "po_pacu_code", "po_default_website")
            ->get();

        $available_portals = $system->queryToArray($available_portals, 'po_id');

        //=> Get websites
        $available_websites = DB::table("websites")
            ->select("we_id", "we_mail_from_name")
            ->get();

        $available_websites = $system->queryToArray($available_websites, 'we_id');

        $crdeinli = DB::table("credit_debit_invoice_lines")
            ->whereIn("crdeinli_cu_id", $customer_ids)
            ->where("crdeinli_invoiced", 0);

        if ($request->credit_debit_lines == CreditDebitInvoiceType::ONLY_CREDIT_LINES) {
            $crdeinli->where("crdeinli_type", CreditDebitType::CREDIT);
        }elseif($request->credit_debit_lines == CreditDebitInvoiceType::ONLY_DEBIT_LINES) {
            $crdeinli->where("crdeinli_type", CreditDebitType::DEBIT);
        }

        $crdeinli = $crdeinli
            ->orderBy("crdeinli_timestamp",  'asc')
            ->get();

        $crdeinli = $system->databaseToArray($crdeinli);

        self::gatherIntoCustomers(
            $customers, CREDIT_DEBIT_LINES, $crdeinli, 'crdeinli', $additional_data
        );

        if(!$only_credit_debit)
        {
            $ktrecupo = DB::table("kt_request_customer_portal")
                ->leftJoin("kt_customer_portal", "ktcupo_id", "ktrecupo_ktcupo_id")
                ->whereIn("ktrecupo_cu_id", $customer_ids)
                ->whereIn("ktrecupo_po_id", array_keys($available_portals))
                ->whereIn("ktrecupo_type", [1,5])
                ->where("ktrecupo_invoiced", 0);

            if($single_month)
            {
                $ktrecupo->whereBetween("ktrecupo_timestamp", [$period_from." 00:00:00", $period_to." 23:59:59"]);
            }
            else
            {
                $ktrecupo->where("ktrecupo_timestamp", "<=" ,$period_to." 23:59:59");
            }

            $ktrecupo = $ktrecupo->orderBy("ktrecupo_timestamp", 'asc')
                ->get();

            $ktrecupo = $system->databaseToArray($ktrecupo);

            self::gatherIntoCustomers($customers, CUSTOMER_REQUESTS, $ktrecupo, 'ktrecupo', $additional_data);

            $ktrecuqu = DB::table("kt_request_customer_question")
                ->whereIn("ktrecuqu_cu_id", $customer_ids)
                ->where("ktrecuqu_sent", 1)
                ->where("ktrecuqu_hide", 0)
                ->where("ktrecuqu_invoiced", 0);

            if($single_month)
            {
                $ktrecuqu->whereBetween("ktrecuqu_sent_timestamp", [$period_from." 00:00:00", $period_to." 23:59:59"]);
            }
            else
            {
                $ktrecuqu->where("ktrecuqu_sent_timestamp", "<=" , $period_to." 23:59:59");
            }

            $ktrecuqu = $ktrecuqu->orderBy("ktrecuqu_sent_timestamp", 'asc')
                ->get();

            $ktrecuqu = $system->databaseToArray($ktrecuqu);

            self::gatherIntoCustomers($customers, QUESTION_REQUESTS, $ktrecuqu, 'ktrecuqu', $additional_data);
        }

        //Gather ktre's, requests & questions
        $found_extras = [];
        foreach
            (
                [
                    'kt_request_customer_portal' => KTRECUPO,
                    'kt_request_customer_question' => KTRECUQU,
                    'requests' => REQUESTS,
                    'questions' => QUESTIONS
                ]
                as $table => $key
            )
            {
                if(!empty($additional_data[$key]))
                {
                    $query = DB::table($table)
                        ->whereIn("{$key}_id", $additional_data[$key])
                        ->get();

                    $query = $system->databaseToArray($query);

                    foreach($query as $row)
                    {
                        $found_extras[$key][$row[$key.'_id']] = $row;
                        self::checkForAdditionalData($additional_data, $row, $key);
                    }
                }
            }

        //Filter out customers
        foreach($customers as $cu_id => &$customer)
        {
            $subscriptions = KTCustomerSubscription::leftJoin("subscriptions", "sub_id", "ktcusu_sub_id")
                ->where("ktcusu_cu_id", $cu_id)
                ->where("ktcusu_active", 1)
                ->where("ktcusu_invoiced", 0)
                ->get();

            if(empty($customer[CUSTOMER_REQUESTS]) && empty($customer[QUESTION_REQUESTS]) && empty($customer[CREDIT_DEBIT_LINES]) && count($subscriptions) == 0)
            {
                unset($customers[$cu_id]);
                unset($customer_ids[$cu_id]);
            }
            unset($customer);
        }

        if(empty($customers))
        {
            return redirect()->back()->withErrors([ 'errors' => "No invoices for these customers found!"]);
        }

        //Get customer balances (for prepayment customers)
        $prepayment_customers = [];
        foreach($customers as $cu_id => &$customer)
        {
            if($customer['cu_payment_method'] == 4)
            {
                $prepayment_customers[] = $cu_id;
                $customer['balance'] = 0;
            }
            unset($customer);
        }

        //Prepare general invoice data
        $ledger_accounts = LedgerAccount::all();
        $currencies = PaymentCurrency::all();

        $company_data = [
            "company_name" => System::getSetting("company_name"),
            "street" => System::getSetting("company_street"),
            "zipcode" => System::getSetting("company_zipcode"),
            "city" => System::getSetting("company_city"),
            "country" => System::getSetting("company_co_code"),
            "vat" => System::getSetting("company_vat"),
            "coc" => System::getSetting("company_coc"),
            "bank" => System::getSetting("company_bank"),
            "iban" => System::getSetting("company_iban"),
            "bic" => System::getSetting("company_bic"),
            "telephone" => System::getSetting("company_telephone"),
            "fax" => System::getSetting("company_fax"),
            "email" => System::getSetting("company_email"),
            "website" => System::getSetting("company_website")
        ];

        $company_iban = System::getSetting("company_iban");
        $company_bank_account_name = System::getSetting("company_bank_account_name");


        $request_fields_ktrecupo = ['ktrecupo_id', 'ktrecupo_cu_re_id', 'ktrecupo_timestamp', 'ktrecupo_free'];
        $request_fields_ktrecuqu = ['ktrecuqu_id', 'ktrecuqu_cu_re_id', 'ktrecuqu_sent_timestamp', 'ktrecuqu_free'];

        //Build the invoices
        foreach($customers as $cu_id => $customer)
        {

            //Get a full lock (no read/write access except this session)
            DB::Raw("LOCK TABLES `invoices` WRITE;");

            //Get highest number
            $year = date("Y", strtotime($request->invoice_date));
            $subyear =  substr($year, 2, 2);

            $number_row = DB::table("invoices")
                ->select("in_number")
                ->where("in_old", 0)
                ->whereRaw("in_number LIKE '".$subyear."-%'")
                ->orderBy("in_number", 'desc')
                ->first();

            if(!empty($number_row))
            {
                $in_number = $number_row->in_number;
                $in_number++;
            }
            else
            {
                $in_number = date("y", strtotime($request->invoice_date))."-0001";
            }

            $new_invoice = new Invoice();

            $new_invoice->in_timestamp = Carbon::now()->toDateTimeString();
            $new_invoice->in_cu_id = $cu_id;
            $new_invoice->in_la_code = $customer['cu_la_code'];
            $new_invoice->in_number = $in_number;
            $new_invoice->in_date = $request->invoice_date;
            $new_invoice->in_expiration_date = date("Y-m-d", strtotime("+".$customer['cu_payment_term']." days"));

            $new_invoice->save();

            //Unlock the table
            DB::Raw("UNLOCK TABLES;");

            if(!$new_invoice->in_id)
            {
                return redirect()->back()->withErrors([ 'errors' => "Something went wrong.. Unable to insert invoice into database."]);
            }

            //Build the debtor data
            $fs = $customer['cu_use_billing_address'] == 1 ? "_bi" : "";
            $debtor_data = [
                "id" => $customer['cu_id'],
                "debtor_number" => $customer['cu_debtor_number'],
                "company_name" => $customer['cu_company_name_legal'],
                "attn" => $customer['cu_attn'],
                "email" => ((!empty($customer['cu_email_bi'])) ? $customer['cu_email_bi'] : $customer['cu_email']),
                "street_1" => $customer['cu_street_1'.$fs],
                "street_2" => $customer['cu_street_2'.$fs],
                "zipcode" => $customer['cu_zipcode'.$fs],
                "city" => $customer['cu_city'.$fs],
                "country" => $customer['cu_co_code'.$fs],
                "vat_number" => $customer['cu_vat_number']
            ];
            unset($fs);
            $additional_information = $request->additional_information ?? "";

            //Calculation vars
            $invoice_period_from = null;
            $cu_la_code = $customer['cu_la_code'];
            $cu_pacu_code = $customer['cu_pacu_code'];
            $requests = [];
            $no_claim_rows = [];

            //Build the lines
            $invoice_lines = 0;
            $invoice_amount = 0;
            $invoice_amount_discount = 0;
            $invoice_amount_gross = 0;
            $invoice_amount_eur = 0;
            $invoice_amount_discount_eur = 0;
            $invoice_amount_gross_eur = 0;
            $invoice_amount_no_claim = 0;
            $invoice_amount_no_claim_eur = 0;

            //=> ktrecupo lines
            foreach($customer[CUSTOMER_REQUESTS] as $ktrecupo_row)
            {
                //General stuff
                $invoice_lines++;
                if($invoice_period_from === null)
                {
                    $invoice_period_from = date("Y-m", strtotime($ktrecupo_row['ktrecupo_timestamp']))."-01";
                }

                //Site key / group
                $portal = $available_portals[$ktrecupo_row['ktrecupo_po_id']];
                $po_pacu_code = $portal['po_pacu_code'];
                $request_row = $found_extras[REQUESTS][$ktrecupo_row['ktrecupo_re_id']];
                $we_mail = $available_websites[$portal['po_default_website']]['we_mail_from_name'];
                //$site_key = $we_mail." - ".lcfirst($collected_translation["invoice_received_requests"][$cu_la_code])
                //    ." (".$collected_translation["request_type_val_".$request_row['re_request_type']][$cu_la_code].")";
                $group = str_replace("[", "", $ktrecupo_row['ktcupo_description']);
                $site_key = str_replace("]", "", $group);

                //Calculate amounts
                $amount = System::getAvailableCurrencyConvert($currencies, $ktrecupo_row['ktrecupo_amount'], (($ktrecupo_row['ktrecupo_type'] == 5) ? $cu_pacu_code : $po_pacu_code), $cu_pacu_code);
                $amount_discount = System::getAvailableCurrencyConvert($currencies, $ktrecupo_row['ktrecupo_amount_discount'], (($ktrecupo_row['ktrecupo_type'] == 5) ? $cu_pacu_code : $po_pacu_code), $cu_pacu_code);
                $amount_gross = System::getAvailableCurrencyConvert($currencies, $ktrecupo_row['ktrecupo_amount_netto'], (($ktrecupo_row['ktrecupo_type'] == 5) ? $cu_pacu_code : $po_pacu_code), $cu_pacu_code);
                $amount_eur = System::getAvailableCurrencyConvert($currencies, $ktrecupo_row['ktrecupo_amount'], (($ktrecupo_row['ktrecupo_type'] == 5) ? $cu_pacu_code : $po_pacu_code), "EUR");
                $amount_discount_eur = System::getAvailableCurrencyConvert($currencies, $ktrecupo_row['ktrecupo_amount_discount'], (($ktrecupo_row['ktrecupo_type'] == 5) ? $cu_pacu_code : $po_pacu_code), "EUR");
                $amount_gross_eur = System::getAvailableCurrencyConvert($currencies, $ktrecupo_row['ktrecupo_amount_netto'], (($ktrecupo_row['ktrecupo_type'] == 5) ? $cu_pacu_code : $po_pacu_code), "EUR");


                self::insertInvoiceLine(
                    $new_invoice->in_id,
                    $ktrecupo_row['ktrecupo_id'],
                    null,
                    null,
                    $site_key,
                    $collected_translation["invoice_requests"][$cu_la_code],
                    0,
                    (($ktrecupo_row['ktrecupo_type'] == 5) ? $cu_pacu_code : $po_pacu_code),
                    $customer['cu_leac_number'],
                    $ktrecupo_row['ktrecupo_amount'],
                    $ktrecupo_row['ktrecupo_amount_discount'],
                    $ktrecupo_row['ktrecupo_amount_netto'],
                    $amount, $amount_discount, $amount_gross,
                    $amount_eur, $amount_discount_eur, $amount_gross_eur
                );

                if ($ktrecupo_row['ktrecupo_free_percentage'] > 0 && $ktrecupo_row['ktrecupo_free_percentage'] < 1)
                {
                    //From DB
                    $no_claim_ktrecupo_amount = round($ktrecupo_row['ktrecupo_amount'] * $ktrecupo_row['ktrecupo_free_percentage'], 4);
                    $no_claim_amount_netto = round($ktrecupo_row['ktrecupo_amount_netto'] * $ktrecupo_row['ktrecupo_free_percentage'], 4);

                    //From DB to Customer currency
                    $no_claim_amount = round($amount * $ktrecupo_row['ktrecupo_free_percentage'], 4);
                    $no_claim_gross = round($amount_gross * $ktrecupo_row['ktrecupo_free_percentage'], 4);

                    //From DB to EURO
                    $no_claim_amount_eur = round($amount_eur * $ktrecupo_row['ktrecupo_free_percentage'], 4);
                    $no_claim_amount_gross_eur = round($amount_gross_eur * $ktrecupo_row['ktrecupo_free_percentage'], 4);

                    $pacu_token = PaymentCurrency::select("pacu_token")->where("pacu_code", (($ktrecupo_row['ktrecupo_type'] == 5) ? $cu_pacu_code : $po_pacu_code))->first()->pacu_token;

                    self::insertInvoiceLine(
                        $new_invoice->in_id,
                        $ktrecupo_row['ktrecupo_id'],
                        null,
                        null,
                        $collected_translation["invoice_no_claim_discount_without_percentage"][$cu_la_code],
                        sprintf($collected_translation["invoice_no_claim_discount_with_price"][$cu_la_code], $ktrecupo_row['ktrecupo_free_percentage'] * 100, $pacu_token." ".$ktrecupo_row['ktrecupo_amount_netto']),
                        1,
                        (($ktrecupo_row['ktrecupo_type'] == 5) ? $cu_pacu_code : $po_pacu_code),
                        $customer['cu_leac_number'],
                        -$no_claim_ktrecupo_amount,
                        0,
                        -$no_claim_amount_netto,
                        -$no_claim_amount, 0, -$no_claim_gross,
                        -$no_claim_amount_eur, 0, -$no_claim_amount_gross_eur
                    );

                    $invoice_amount_no_claim += $no_claim_gross;
                    $invoice_amount_no_claim_eur += $no_claim_amount_gross_eur;

                    $no_claim_gross_converted = $no_claim_gross;

                    $no_claim_group = sprintf($collected_translation["invoice_no_claim_discount_with_price"][$cu_la_code], $ktrecupo_row['ktrecupo_free_percentage'] * 100, $pacu_token." ".$ktrecupo_row['ktrecupo_amount_netto']);
                    if (!isset($no_claim_rows[$no_claim_group])) {
                        $no_claim_rows[$no_claim_group] = [
                            "count" => 0,
                            "price" => $pacu_token." -".System::numberFormat($ktrecupo_row['ktrecupo_amount_netto'] * $ktrecupo_row['ktrecupo_free_percentage'], 4, ",", "."),
                            "price_excl" => 0,
                            "po_pacu_code" => (($ktrecupo_row['ktrecupo_type'] == 5) ? $cu_pacu_code : $po_pacu_code)
                        ];
                    }

                    $no_claim_rows[$no_claim_group]["count"]++;
                    $no_claim_rows[$no_claim_group]["price_excl"] = $no_claim_rows[$no_claim_group]["price_excl"] + $ktrecupo_row['ktrecupo_amount_netto'] * $ktrecupo_row['ktrecupo_free_percentage'];
                }

                DB::table('kt_request_customer_portal')
                    ->where('ktrecupo_id', $ktrecupo_row['ktrecupo_id'])
                    ->update(
                        [
                            'ktrecupo_invoiced' => 1
                        ]
                    );

                if($ktrecupo_row['ktrecupo_free'])
                {
                    $invoice_lines++;

                    //Add a negative line since it's free
                    self::insertInvoiceLine(
                        $new_invoice->in_id,
                        $ktrecupo_row['ktrecupo_id'],
                        null,
                        null,
                        $site_key,
                        $collected_translation["invoice_free_requests"][$cu_la_code],
                        0,
                        $po_pacu_code,
                        $customer['cu_leac_number'],
                        -$ktrecupo_row['ktrecupo_amount'],
                        -$ktrecupo_row['ktrecupo_amount_discount'],
                        -$ktrecupo_row['ktrecupo_amount_netto'],
                        -$amount, -$amount_discount, -$amount_gross,
                        -$amount_eur, -$amount_discount_eur, -$amount_gross_eur
                    );
                }
                else
                {
                    //Add to total amounts
                    $invoice_amount += $amount;
                    $invoice_amount_discount += $amount_discount;
                    $invoice_amount_gross += $amount_gross;
                    $invoice_amount_eur += $amount_eur;
                    $invoice_amount_discount_eur += $amount_discount_eur;
                    $invoice_amount_gross_eur += $amount_gross_eur;
                }

                //Add to list of requests
                self::addToRequests(
                    $requests, $site_key, $ktrecupo_row, $request_row,
                    $request_fields_ktrecupo, $cu_la_code,
                    $collected_translation
                );

                unset($ktrecupo_row);
            }

            //=> ktrecuqu lines
            foreach($customer[QUESTION_REQUESTS] as $ktrecuqu_row)
            {
                //General stuff
                $invoice_lines++;
                if($invoice_period_from === null)
                {
                    $invoice_period_from = date("Y-m", strtotime($ktrecuqu_row['ktrecuqu_sent_timestamp']))."-01";
                }

                //Site key / group
                $question_row = $found_extras[QUESTIONS][$ktrecuqu_row['ktrecuqu_qu_id']];
                $site_key =  $collected_translation["invoice_received_requests"][$cu_la_code]." (".$question_row['qu_name'].")";

                //Calculate amounts
                $amount = System::getAvailableCurrencyConvert($currencies, $ktrecuqu_row['ktrecuqu_amount'], $ktrecuqu_row['ktrecuqu_currency'], $cu_pacu_code);
                $amount_eur = System::getAvailableCurrencyConvert($currencies, $ktrecuqu_row['ktrecuqu_amount'], $ktrecuqu_row['ktrecuqu_currency'], "EUR");

                //Discounts are non-existent
                self::insertInvoiceLine(
                    $new_invoice->in_id,
                    null,
                    $ktrecuqu_row['ktrecuqu_id'],
                    null,
                    $site_key,
                    $collected_translation["invoice_requests"][$cu_la_code],
                    0,
                    $cu_pacu_code,
                    $customer['cu_leac_number'],
                    $amount, 0, $amount,
                    $amount, 0, $amount,
                    $amount_eur, 0, $amount_eur
                );

                DB::table('kt_request_customer_question')
                    ->where('ktrecuqu_id', $ktrecuqu_row['ktrecuqu_id'])
                    ->update(
                        [
                            'ktrecuqu_invoiced' => 1
                        ]
                    );

                if($ktrecuqu_row['ktrecuqu_free'])
                {
                    $invoice_lines++;

                    //Add a negative line since it's free
                    self::insertInvoiceLine(
                        $new_invoice->in_id,
                        null,
                        $ktrecuqu_row['ktrecuqu_id'],
                        null,
                        $site_key,
                        $collected_translation["invoice_free_requests"][$cu_la_code],
                        0,
                        $cu_pacu_code,
                        $customer['cu_leac_number'],
                        -$amount, 0, -$amount,
                        -$amount, 0, -$amount,
                        -$amount_eur, 0, -$amount_eur
                    );
                }
                else
                {
                    //Add to total amounts
                    $invoice_amount += $amount;
                    $invoice_amount_gross += $amount;
                    $invoice_amount_eur += $amount_eur;
                    $invoice_amount_gross_eur += $amount_eur;
                }

                //Add to list of requests
                self::addToRequests(
                    $requests, $site_key, $ktrecuqu_row,
                    $found_extras[REQUESTS][$ktrecuqu_row['ktrecuqu_re_id']] ?? [],
                    $request_fields_ktrecuqu, $cu_la_code,
                    $collected_translation
                );

                unset($ktrecuqu_row);
            }

            //=> credit_debit_lines
            foreach($customer[CREDIT_DEBIT_LINES] as $crdeinli_row)
            {
                //General stuff
                $invoice_lines++;
                $type = $crdeinli_row['crdeinli_type'] == 1 ? "free" : "paid";

                //Calculate amounts
                $amount = System::getAvailableCurrencyConvert($currencies, $crdeinli_row['crdeinli_amount'], $crdeinli_row['crdeinli_currency'], $cu_pacu_code);
                $amount_eur = System::getAvailableCurrencyConvert($currencies, $crdeinli_row['crdeinli_amount'], $crdeinli_row['crdeinli_currency'], "EUR");

                self::insertInvoiceLine(
                    $new_invoice->in_id,
                    $crdeinli_row['crdeinli_ktrecupo_id'],
                    $crdeinli_row['crdeinli_ktrecuqu_id'],
                    $crdeinli_row['crdeinli_id'],
                    $collected_translation["invoice_miscellaneous"][$cu_la_code],
                    $crdeinli_row['crdeinli_description'],
                    0,
                    $cu_pacu_code,
                    $crdeinli_row['crdeinli_leac_number'],
                    $amount, 0, $amount,
                    $amount, 0, $amount,
                    $amount_eur, 0, $amount_eur
                );

                DB::table('credit_debit_invoice_lines')
                    ->where('crdeinli_id', $crdeinli_row['crdeinli_id'])
                    ->update(
                        [
                            'crdeinli_invoiced' => 1
                        ]
                    );

                if(!empty($crdeinli_row['crdeinli_ktrecupo_id']))
                {
                    $ktrecupo_row = $found_extras[KTRECUPO][$crdeinli_row['crdeinli_ktrecupo_id']];
                    $request_row = $found_extras[REQUESTS][$ktrecupo_row['ktrecupo_re_id']];

                    $site_key = $collected_translation["invoice_".$type."_requests_previous_invoice"][$cu_la_code].
                        " (".$collected_translation["request_type_val_".$request_row['re_request_type']][$cu_la_code].")";

                    self::addToRequests(
                        $requests, $site_key, $ktrecupo_row, $request_row,
                        $request_fields_ktrecupo, $cu_la_code,
                        $collected_translation
                    );
                }

                if(!empty($crdeinli_row['crdeinli_ktrecuqu_id']))
                {
                    $ktrecuqu_row = $found_extras[KTRECUQU][$crdeinli_row['crdeinli_ktrecuqu_id']];
                    $question_row = $found_extras[QUESTIONS][$ktrecuqu_row['ktrecuqu_qu_id']];
                    $request_row = $found_extras[REQUESTS][$ktrecuqu_row['ktrecuqu_re_id']];

                    $site_key = $collected_translation["invoice_".$type."_requests_previous_invoice"][$cu_la_code].
                        " (".$question_row['qu_name'].")";

                    self::addToRequests(
                        $requests, $site_key, $ktrecuqu_row, $request_row,
                        $request_fields_ktrecuqu, $cu_la_code,
                        $collected_translation
                    );
                }

                //Add to total amounts
                $invoice_amount += $amount;
                $invoice_amount_gross += $amount;
                $invoice_amount_eur += $amount_eur;
                $invoice_amount_gross_eur += $amount_eur;

                unset($crdeinli_row);
            }

            //$invoice_amount_no_claim = 0;
            //$invoice_amount_no_claim_eur = 0;

            /*if($customer['moda_no_claim_discount'] && !empty($requests))
            {
                //$invoice_amount_no_claim = round($invoice_amount_gross * ($customer['moda_no_claim_discount_percentage'] / 100), 2);
                $invoice_amount_gross -= $invoice_amount_no_claim;

                //$invoice_amount_no_claim_eur = round($invoice_amount_gross_eur * ($customer['moda_no_claim_discount_percentage'] / 100), 2);
                $invoice_amount_gross_eur -= $invoice_amount_no_claim_eur;
            }*/

            if($customer['moda_no_claim_discount'] && !empty($requests))
            {

                $credit_debit_invoice_line = new CreditDebitInvoiceLine();

                $credit_debit_invoice_line->crdeinli_timestamp = Carbon::now()->toDateTimeString();
                $credit_debit_invoice_line->crdeinli_cu_id = $customer['cu_id'];
                $credit_debit_invoice_line->crdeinli_ktrecupo_id = null;
                $credit_debit_invoice_line->crdeinli_ktrecuqu_id = null;
                $credit_debit_invoice_line->crdeinli_leac_number = $customer['cu_leac_number'];
                $credit_debit_invoice_line->crdeinli_description = sprintf($collected_translation["invoice_no_claim_discount"][$customer['cu_la_code']], $customer['moda_no_claim_discount_percentage']);
                $credit_debit_invoice_line->crdeinli_type = CreditDebitType::CREDIT;
                $credit_debit_invoice_line->crdeinli_currency = $customer['cu_pacu_code'];
                $credit_debit_invoice_line->crdeinli_amount = -$invoice_amount_no_claim;
                $credit_debit_invoice_line->crdeinli_invoiced = 1;

                $credit_debit_invoice_line->save();

                /*self::insertInvoiceLine(
                    $new_invoice->in_id,
                    $credit_debit_invoice_line->crdeinli_id,
                    null,
                    null,
                    "",
                    sprintf($collected_translation["invoice_no_claim_discount"][$customer['cu_la_code']], $customer['moda_no_claim_discount_percentage']),
                    1,
                    $customer['cu_pacu_code'],
                    $customer['cu_leac_number'],
                    -$invoice_amount_no_claim,
                    0,
                    -$invoice_amount_no_claim,
                    -$invoice_amount_no_claim,
                    0,
                    -$invoice_amount_no_claim,
                    -$invoice_amount_no_claim_eur,
                    0,
                    -$invoice_amount_no_claim_eur
                );*/
            }

            $total_no_claim_discount = 0;
            $total_no_claim_discount_eur = 0;

            if(!empty($no_claim_rows)) {
                foreach ($no_claim_rows as $group => $row) {
                    $total_no_claim_discount += $system->currencyConvert($row['price_excl'], $row['po_pacu_code'], $customer['cu_pacu_code']);
                    $total_no_claim_discount_eur += $system->currencyConvert($row['price_excl'], $row['po_pacu_code'], "EUR");
                }
            }

            $invoice_amount_gross -= round($total_no_claim_discount, 2);
            $invoice_amount_gross_eur -= round($total_no_claim_discount_eur, 2);

            //PART FOR SUBSCRIPTIONS
            $invoice_subscriptions_block = [];
            $invoice_subscriptions_total = 0;
            $invoice_subscriptions_total_eur = 0;

            $subscriptions = KTCustomerSubscription::leftJoin("subscriptions", "sub_id", "ktcusu_sub_id")
                ->where("ktcusu_cu_id", $customer['cu_id'])
                ->where("ktcusu_active", 1)
                ->where("ktcusu_invoiced", 0)
                ->get();

            if (count($subscriptions) > 0) {
                foreach($subscriptions as $subscription)
                {
                    $pacu_token = PaymentCurrency::where("pacu_code", $subscription->ktcusu_pacu_code)->first()->pacu_token;
                    $invoice_subscriptions_block[] = [
                        "description" => $subscription->sub_name." (".$subscription->ktcusu_start_date."/".$subscription->ktcusu_end_date.")",
                        "price" => $subscription->ktcusu_price_this_month,
                        "currency" => $subscription->ktcusu_pacu_code,
                        "price_excl" => $pacu_token." ".$system->numberFormat($subscription->ktcusu_price_this_month, 2, ",", ".")
                    ];

                    $invoice_subscriptions_total = $invoice_subscriptions_total + $subscription->ktcusu_price_this_month;
                    $invoice_subscriptions_total_eur = $invoice_subscriptions_total_eur + $system->currencyConvert($subscription->ktcusu_price_this_month, $subscription->ktcusu_pacu_code,"EUR");

                    self::insertInvoiceLine(
                        $new_invoice->in_id,
                        null,
                        null,
                        null,
                        $collected_translation["subscriptions"][$cu_la_code],
                        $subscription->sub_name." (".$subscription->ktcusu_start_date."/".$subscription->ktcusu_end_date.")",
                        0,
                        $customer['cu_pacu_code'],
                        $customer['cu_leac_number'],
                        $subscription->ktcusu_price_this_month,
                        0,
                        $subscription->ktcusu_price_this_month,
                        $subscription->ktcusu_price_this_month,
                        0,
                        $subscription->ktcusu_price_this_month,
                        $system->currencyConvert($subscription->ktcusu_price_this_month, $subscription->ktcusu_pacu_code,"EUR"),
                        0,
                        $system->currencyConvert($subscription->ktcusu_price_this_month, $subscription->ktcusu_pacu_code,"EUR")
                    );

                    /*$credit_debit_invoice_line = new CreditDebitInvoiceLine();

                    $credit_debit_invoice_line->crdeinli_timestamp = Carbon::now()->toDateTimeString();
                    $credit_debit_invoice_line->crdeinli_cu_id = $customer['cu_id'];
                    $credit_debit_invoice_line->crdeinli_ktrecupo_id = null;
                    $credit_debit_invoice_line->crdeinli_ktrecuqu_id = null;
                    $credit_debit_invoice_line->crdeinli_leac_number = $customer['cu_leac_number'];
                    $credit_debit_invoice_line->crdeinli_description = sprintf($collected_translation["invoice_no_claim_discount"][$customer['cu_la_code']], $customer['moda_no_claim_discount_percentage']);
                    $credit_debit_invoice_line->crdeinli_type = CreditDebitType::CREDIT;
                    $credit_debit_invoice_line->crdeinli_currency = $customer['cu_pacu_code'];
                    $credit_debit_invoice_line->crdeinli_amount = -$invoice_amount_no_claim;
                    $credit_debit_invoice_line->crdeinli_invoiced = 1;

                    $credit_debit_invoice_line->save();*/

                    DB::table('kt_customer_subscription')
                        ->where('ktcusu_id', $subscription->ktcusu_id)
                        ->update(
                            [
                                'ktcusu_invoiced' => 1
                            ]
                        );
                }


                $invoice_amount_gross += round($invoice_subscriptions_total, 2);
                $invoice_amount += round($invoice_subscriptions_total, 2);
                $invoice_amount_eur += round($invoice_subscriptions_total_eur, 2);
                $invoice_amount_gross_eur += round($invoice_subscriptions_total_eur, 2);

                $invoice_subscriptions_total = $pacu_token." ".System::numberFormat($invoice_subscriptions_total,2, ",", ".");


                /*$invoice_id, $ktrecupo_id, $ktrecuqu_id, $crdeinli_id, $group, $description, $no_claim_discount, $currency, $ledger_account_number,
                               $amount, $amount_discount, $amount_netto,
                               $amount_customer, $amount_discount_customer, $amount_netto_customer,
                               $amount_eur, $amount_discount_eur, $amount_netto_eur)*/


            }

            //Calculate netto
            $ledger_vat = ($ledger_accounts->where("leac_number", $customer['cu_leac_number'])->first()->leac_vat ?? false);
            $invoice_amount_vat = ($invoice_amount_gross / 100) * $ledger_vat;
            $invoice_amount_netto = $invoice_amount_gross + $invoice_amount_vat;
            $invoice_amount_vat_eur = ($invoice_amount_gross_eur / 100) * $ledger_vat;
            $invoice_amount_netto_eur = $invoice_amount_gross_eur + $invoice_amount_vat_eur;

            //Payment message
            $payment_method_message = $collected_translation["invoice_payment_method_".$customer['cu_payment_method']][$cu_la_code];
            if($customer['cu_payment_method'] == PaymentMethod::BANK_TRANSFER)
            {
                $payment_text = sprintf($payment_method_message, $customer['cu_payment_term'], $company_data['iban'], $company_bank_account_name);
            }
            elseif($customer['cu_payment_method'] == PaymentMethod::CREDIT_CARD || $customer['cu_payment_method'] == PaymentMethod::AUTO_DEBIT)
            {
                $payment_text = sprintf($collected_translation["invoice_payment_method_" . $customer['cu_payment_method']][$customer['cu_la_code']], date("d-m-Y", strtotime("+". $customer['cu_payment_term']." days")));
            }
            elseif($customer['cu_payment_method'] == PaymentMethod::PREPAYMENT || $customer['cu_payment_method'] == PaymentMethod::PREPAYMENT_AUTO_CHARGE_CREDIT_CARD || $customer['cu_payment_method'] == PaymentMethod::PREPAYMENT_AUTO_DIRECT_DEBIT)
            {
                $token = $currencies->where("pacu_code", $customer['cu_pacu_code'])->first()->pacu_token;
                $balance = Mover::customerBalancesNew($customer['cu_id']);
                $payment_text = sprintf(
                    $payment_method_message,
                    $token." ".System::numberFormat($balance, 2),
                    $token." ".System::numberFormat($balance - $invoice_amount_netto, 2)
                );
            }
            else
            {
                $payment_text = $payment_method_message;
            }

            DB::table('invoices')
                ->where('in_id', $new_invoice->in_id)
                ->update(
                    [
                        'in_period_from' => isset($invoice_period_from) ? $invoice_period_from : $period_from,
                        'in_period_to' => $period_to,
                        'in_additional_information' => $additional_information,
                        'in_payment_method' => $customer['cu_payment_method'],
                        'in_payment_text' => $payment_text,
                        'in_company_data' => System::serialize($company_data),
                        'in_debtor_data' => System::serialize($debtor_data),
                        'in_lines' => $invoice_lines,
                        'in_currency' => $cu_pacu_code,
                        'in_amount' => $invoice_amount,
                        'in_amount_discount' => $invoice_amount_discount,
                        'in_amount_no_claim' => round($total_no_claim_discount * 100) / 100,
                        'in_amount_gross' => $invoice_amount_gross,
                        'in_amount_netto' => $invoice_amount_netto,
                        'in_amount_vat' => $invoice_amount_vat,
                        'in_amount_eur' => $invoice_amount_eur,
                        'in_amount_discount_eur' => $invoice_amount_discount_eur,
                        'in_amount_no_claim_eur' => round($total_no_claim_discount_eur * 100) / 100,
                        'in_amount_gross_eur' => $invoice_amount_gross_eur,
                        'in_amount_vat_eur' => $invoice_amount_vat_eur,
                        'in_amount_netto_eur' => $invoice_amount_netto_eur,
                        'in_vat_percentage' => $ledger_vat,
                        'in_requests' => System::serialize($requests),
                        'in_id' => $new_invoice->in_id
                    ]
                );

            //Create & send the invoice
            self::createInvoicePDF($customer, $new_invoice->in_id, $request->email, $no_claim_rows);
        }

        return view('finance.createinvoicescompleted', [
            'customers' => $customers,
            'email' => $request->email
        ]);

    }

    public function completedInvoices(){

        return response()->download("xD");

    }

    /**
     * Gather all entries into the customers var.
     * @param array $customers The customers array
     * @param string $cu_key The customer sub key (which array the entry should be placed in)
     * @param array $query The query
     * @param string $cu_field The field in the row to get the customer
     * @param array $additional_data Array to store IDs in
     */
    function gatherIntoCustomers(&$customers, $cu_key, $query, $cu_field, &$additional_data)
    {
        foreach($query as $row)
        {
            $customers[$row[$cu_field.'_cu_id']][$cu_key][] = $row;
            self::checkForAdditionalData($additional_data, $row, $cu_field);
        }
    }

    function checkForAdditionalData(&$additional_data, &$row, $key)
    {
        foreach([KTRECUPO, KTRECUQU, REQUESTS, QUESTIONS] as $type)
        {
            $type_key = "{$key}_{$type}_id";
            if(isset($row[$type_key]))
            {
                $value = $row[$type_key];
                if(!empty($value) && $value != 0)
                {
                    $additional_data[$type][$value] = $value;
                }
            }
        }
    }

    function addToRequests(&$requests, $site_key, $row, $request_row, $fields, $cu_la_code, $collected_translation)
    {
        $full_name = $request_row['re_full_name'] ?? "";
        $requests[$site_key][$row[$fields[0]]] = [
            "id" => $row[$fields[1]],
            "received" => $row[$fields[2]],
            "name" => $full_name,
            "country_from" => $request_row['re_co_code_from'] ?? "",
            "city_from" => $request_row['re_city_from'] ?? "",
            "country_to" => $request_row['re_co_code_to'] ?? "",
            "city_to" => $request_row['re_city_to'] ?? "",
            "free" => $collected_translation[(($row[$fields[3]] == 1) ? "yes" : "no")][$cu_la_code]
        ];
    }

    function insertInvoiceLine($invoice_id, $ktrecupo_id, $ktrecuqu_id, $crdeinli_id, $group, $description, $no_claim_discount, $currency, $ledger_account_number,
                               $amount, $amount_discount, $amount_netto,
                               $amount_customer, $amount_discount_customer, $amount_netto_customer,
                               $amount_eur, $amount_discount_eur, $amount_netto_eur)
    {

        $invoice_line = new InvoiceLine();

        $invoice_line->inli_in_id = $invoice_id;
        $invoice_line->inli_ktrecupo_id = $ktrecupo_id;
        $invoice_line->inli_ktrecuqu_id = $ktrecuqu_id;
        $invoice_line->inli_crdeinli_id = $crdeinli_id;
        $invoice_line->inli_group = $group;
        $invoice_line->inli_description = $description;
        $invoice_line->inli_no_claim_discount = $no_claim_discount;
        $invoice_line->inli_currency = $currency;
        $invoice_line->inli_amount = $amount;
        $invoice_line->inli_amount_discount = $amount_discount;
        $invoice_line->inli_amount_netto = $amount_netto;
        $invoice_line->inli_amount_customer = $amount_customer;
        $invoice_line->inli_amount_discount_customer = $amount_discount_customer;
        $invoice_line->inli_amount_netto_customer = $amount_netto_customer;
        $invoice_line->inli_amount_eur = $amount_eur;
        $invoice_line->inli_amount_discount_eur = $amount_discount_eur;
        $invoice_line->inli_amount_netto_eur = $amount_netto_eur;
        $invoice_line->inli_leac_number = $ledger_account_number;

        $invoice_line->save();

    }

    function createInvoicePDF(&$customer, $in_id, $send_mail, $no_claim_rows = [])
    {
        $system = new System();
        $mail = new Mail();

        $row_invoice = DB::table("invoices")
            ->leftJoin("mover_data", "moda_cu_id", "in_cu_id")
            ->where("in_id", $in_id)
            ->get();

        $row_invoice = $system->databaseToArray($row_invoice[0]);

        $invoice_lines = [];
        $invoice_totals = [];

        $query_invoice_lines = DB::table("invoice_lines")
            ->where("inli_in_id", $in_id)
            ->get();

        $query_invoice_lines = $system->databaseToArray($query_invoice_lines);

        foreach($query_invoice_lines as $row_invoice_lines)
        {
            if ($row_invoice_lines['inli_no_claim_discount']) {
                continue;
            }

            $group = (empty($row_invoice_lines['inli_group']) ? "&nbsp;" : $row_invoice_lines['inli_group']);
            $group = str_replace("[", "", $group);
            $group = str_replace("]", "", $group);
            $desc = $row_invoice_lines['inli_description'];

            if(!array_key_exists($group, $invoice_lines))
            {
                $invoice_lines[$group] = [];
            }

            if(!array_key_exists($desc, $invoice_lines[$group]))
            {
                $invoice_lines[$group][$desc] = [];
            }

            $price = (string)$row_invoice_lines['inli_amount'];
            $discount = (string)$row_invoice_lines['inli_amount_discount'];

            if(!array_key_exists($price, $invoice_lines[$group][$desc]))
            {
                $invoice_lines[$group][$desc][$price] = [];
            }

            if(!array_key_exists($discount, $invoice_lines[$group][$desc][$price]))
            {
                $invoice_lines[$group][$desc][$price][$discount] = [
                    "quantity" => 0,
                    "amount" => 0,
                    "amount_discount" => 0,
                    "amount_netto" => 0,
                ];
            }

            $invoice_lines[$group][$desc][$price][$discount]['quantity']++;
            $invoice_lines[$group][$desc][$price][$discount]['currency'] = $row_invoice_lines['inli_currency'];
            $invoice_lines[$group][$desc][$price][$discount]['amount'] += $row_invoice_lines['inli_amount'];
            $invoice_lines[$group][$desc][$price][$discount]['amount_discount'] += $row_invoice_lines['inli_amount_discount'];
            $invoice_lines[$group][$desc][$price][$discount]['amount_netto'] += $row_invoice_lines['inli_amount_netto'];

            if(!array_key_exists($group, $invoice_totals))
            {
                $invoice_totals[$group]['local'] = 0;

                if($row_invoice_lines['inli_currency'] != $row_invoice['in_currency'])
                {
                    $invoice_totals[$group][$row_invoice['in_currency']] = 0;
                }
            }

            $invoice_totals[$group]['local'] += $row_invoice_lines['inli_amount_netto'];

            if($row_invoice_lines['inli_currency'] != $row_invoice['in_currency'])
            {
                $invoice_totals[$group][$row_invoice['in_currency']] += $row_invoice_lines['inli_amount_netto_customer'];
            }
        }

        $row_invoice['in_cu_leac'] = $customer['cu_leac_number'];

        $invoice_no_claim_discount_block = [];

        if (!empty($no_claim_rows))
        {
            $pacu_token_customer = PaymentCurrency::select("pacu_token")->where("pacu_code", $customer['cu_pacu_code'])->first()->pacu_token;

            foreach ($no_claim_rows as $group => $row)
            {
                $invoice_no_claim_discount_block[] = [
                    "group" => $group,
                    "count" => $row['count'],
                    "price" => $row['price'],
                    //"price_excl" => $pacu_token_customer." -".number_format($row['price_excl'], 2, ",", "."),
                    "price_excl" => $pacu_token_customer." -".$system->numberFormat($system->currencyConvert($row['price_excl'], $row['po_pacu_code'], $customer['cu_pacu_code']) , 2, ",", "."),
                ];

            }
        }

        $fields = [
            "invoice" => $row_invoice,
            "invoice_lines" => $invoice_lines,
            "invoice_totals" => $invoice_totals,

            //No claim discount data
            "invoice_no_claim_discount_block" => $invoice_no_claim_discount_block,
            "no_claim_currency" => $customer['cu_pacu_code'],
            "no_claim_discount_total" => (($customer['cu_pacu_code'] == "EUR") ? round($row_invoice['in_amount_no_claim_eur'], 2) : round($row_invoice['in_amount_no_claim'], 2) ),
        ];

        $html = $mail->send("invoice", $row_invoice['in_la_code'], $fields, null, null, null, true);
        System::createPDF("invoices/".date("Y", strtotime($row_invoice['in_date']))."/", "invoice_".$row_invoice['in_number'], $html);

        //TODO: Enable once we figure out how to process this
        /*if( $_POST['mover_statistics'] ) {

            $customer_id = $customer['cu_id'];

            $fileLocation = SHARED_FOLDER."uploads/test/stats_".$customer_id.'.pdf';

            if( file_exists( $fileLocation ) ) {

                unlink( $fileLocation );
            }

            $url = ( ! empty( $_SERVER['HTTPS'] ) ? 'https://' : 'http://' ).$_SERVER['SERVER_NAME'].'/customers/generate_statistics/?';

            // Add cu id to the url
            $url .= 'cu_id='.$customer_id;

            $url .= ( isset( $_POST['date_from'] ) ? '&from='.$_POST['date_from'] : '' );
            $url .= ( isset( $_POST['date_to'] ) ? '&to='.$_POST['date_to'] : '' );

            $config = [

                'margin'	=> 0,
                'height'	=> 1120,
                'width'		=> 800,
                'dpi'		=> 300,
                'delay'		=> 3000,
                'url'       => urlencode( $url ),
                'force'     => 1,
                'auth_user' => 'pdfaccess', // in htpasswd
                'auth_pass' => 'onheusbejegend', // in htpasswd (md5)
            ];

            // Create statistics
            System::createPDF( 'test/', 'stats_'.$customer_id, '' , $config );
        }*/

        if($send_mail == 1)
        {
            $mail->send("invoice", $row_invoice['in_la_code'], $fields, "uploads/invoices/".date("Y", strtotime($row_invoice['in_date']))."/invoice_".$row_invoice['in_number'].".pdf");
        }

    }

}
