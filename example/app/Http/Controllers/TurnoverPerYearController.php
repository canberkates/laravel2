<?php

namespace App\Http\Controllers;

use App\Data\TurnoverDimensions;
use App\Data\TurnoverType;
use App\Functions\DataIndex;
use App\Models\LedgerAccount;
use App\Models\Portal;
use DB;
use Illuminate\Http\Request;
use Log;

class TurnoverPerYearController extends Controller
{
	public function index()
	{
		for ($year = date("Y"); $year >= 2014; $year--)
		{
			$years[] = $year;
		}

		$dimensions = TurnoverDimensions::all();

		return view('finance.turnoverperyear', [
			'years' => $years,
			'dimensions' => $dimensions,
			'selected_year' => "",
			'selected_dimension' => "",
			'date_filter' => [],
			'filtered_data' => []
		]);
	}

	public function filteredIndex(Request $request)
	{
		$request->validate([
			'year' => 'required',
			'dimension' => 'required'
		]);

		$total_months = [];
		$years = [];

		for ($year = date("Y"); $year >= 2014; $year--)
		{
			$years[] = $year;
		}

		$turnoverdimensions = TurnoverDimensions::all();

		$year = $request->year;
		$dimension = $request->dimension;

		if ($dimension == TurnoverDimensions::LEDGER_ACCOUNT)
		{
			$ledgers = LedgerAccount::where("leac_type", 1)->get();


			$total_total = 0;
			$amount_months = [];
			$total_dimensions = [];

			foreach (DataIndex::getMonths() as $month => $month_name)
			{
				$total_months[$month] = 0;
			}

			foreach ($ledgers as $ledger)
			{
				$total_dimensions[] = $ledger;
				$total_dimension = 0;
				$amount_months[$ledger->leac_number] = [$ledger->leac_number . ' - ' . $ledger->leac_name];

				Log::debug($amount_months[$ledger->leac_number]);

				foreach (DataIndex::getMonths() as $month => $month_name)
				{
					$amount_months[$ledger->leac_number][$month] = 0;
				}

				$results = DB::table("invoices")
					->selectRaw("MONTH(`in_date`) AS `month`")
					->selectRaw("SUM(`inli_amount_netto_eur`) AS `amount`")
					->leftJoin("invoice_lines", "in_id", "inli_in_id")
					->whereRaw("YEAR(`invoices`.`in_date`) = '{$year}'")
					->where("inli_leac_number", $ledger->leac_number)
					->groupBy("month")
					->get();


				foreach ($results as $row)
				{
					$amount_months[$ledger->leac_number][$row->month] += $row->amount;
				}

				foreach (DataIndex::getMonths() as $month => $month_name)
				{

					$total_dimension += $amount_months[$ledger->leac_number][$month];
					$total_months[$month] += $amount_months[$ledger->leac_number][$month];
					$total_total += $amount_months[$ledger->leac_number][$month];
				}

				$total_dimensions[$ledger->leac_number . ' - ' . $ledger->leac_name] = $total_dimension;

			}

			return view('finance.turnoverperyear', [
				'years' => $years,
				'dimensions' => $turnoverdimensions,
				'selected_year' => $year,
				'selected_dimension' => $dimension,
				'total_total' => $total_total,
				'total_months' => $total_months,
				'amount_months' => $amount_months,
				'total_dimensions' => $total_dimensions
			]);

		}

		if ($dimension == TurnoverDimensions::PORTAL)
		{
			$portals = Portal::with("country")->get();

			$total_total = 0;
			$amount_months = [];
			$total_dimensions = [];

			foreach (DataIndex::getMonths() as $month => $month_name)
			{
				$total_months[$month] = 0;
			}

			foreach ($portals as $portal)
			{
				$total_dimensions[] = $portal;
				$total_dimension = 0;
				$amount_months[$portal->po_portal] = [$portal->po_portal . ($portal->country ? ' ('.$portal->country->co_en.')' : '')];

				foreach (DataIndex::getMonths() as $month => $month_name)
				{
					$amount_months[$portal->po_portal][$month] = 0;
				}

				$results = DB::table("invoices")
					->selectRaw("MONTH(`in_date`) AS `month`")
					->selectRaw("SUM(`inli_amount_netto_eur`) AS `amount`")
					->leftJoin("invoice_lines", "in_id", "inli_in_id")
					->leftJoin("kt_request_customer_portal", "inli_ktrecupo_id", "ktrecupo_id")
					->where("ktrecupo_po_id", $portal->po_id)
					->whereRaw("YEAR(`invoices`.`in_date`) = '{$year}'")
					->groupBy("month")
					->get();

				foreach ($results as $row)
				{
					$amount_months[$portal->po_portal][$row->month] += $row->amount;
				}

				foreach (DataIndex::getMonths() as $month => $month_name)
				{

					$total_dimension += $amount_months[$portal->po_portal][$month];
					$total_months[$month] += $amount_months[$portal->po_portal][$month];
					$total_total += $amount_months[$portal->po_portal][$month];
				}

				$total_dimensions[$portal->po_portal . ($portal->country ? ' ('.$portal->country->co_en.')' : '')] = $total_dimension;
			}

			return view('finance.turnoverperyear', [
				'years' => $years,
				'dimensions' => $turnoverdimensions,
				'selected_year' => $year,
				'selected_dimension' => $dimension,
				'total_total' => $total_total,
				'total_months' => $total_months,
				'amount_months' => $amount_months,
				'total_dimensions' => $total_dimensions
			]);

		}

		if ($dimension == TurnoverDimensions::TURNOVER_TYPE)
		{
			$turnovertypes = TurnoverType::all();

			$total_total = 0;
			$amount_months = [];
			$total_dimensions = [];

			foreach (DataIndex::getMonths() as $month => $month_name)
			{
				$total_months[$month] = 0;
			}

			foreach ($turnovertypes as $id => $turnovertype)
			{
				$total_dimensions[] = $turnovertype;
				$total_dimension = 0;
				$amount_months[$id] = [$turnovertype];

				foreach (DataIndex::getMonths() as $month => $month_name)
				{
					$amount_months[$id][$month] = 0;
				}

				$results = DB::table("invoices")
					->selectRaw("MONTH(`in_date`) AS `month`")
					->selectRaw("SUM(`inli_amount_netto_eur`) AS `amount`")
					->leftJoin("invoice_lines", "in_id", "inli_in_id")
					->leftJoin("ledger_accounts", "inli_leac_number", "leac_number")
					->where("leac_turnover_type", $id)
					->whereRaw("YEAR(`invoices`.`in_date`) = '{$year}'")
					->groupBy("month")
					->get();

				foreach ($results as $row)
				{
					$amount_months[$id][$row->month] += $row->amount;
				}

				foreach (DataIndex::getMonths() as $month => $month_name)
				{

					$total_dimension += $amount_months[$id][$month];
					$total_months[$month] += $amount_months[$id][$month];
					$total_total += $amount_months[$id][$month];
				}

				$total_dimensions[$turnovertype] = $total_dimension;
			}

			return view('finance.turnoverperyear', [
				'years' => $years,
				'dimensions' => $turnoverdimensions,
				'selected_year' => $year,
				'selected_dimension' => $dimension,
				'total_total' => $total_total,
				'total_months' => $total_months,
				'amount_months' => $amount_months,
				'total_dimensions' => $total_dimensions
			]);

		}
	}
}
