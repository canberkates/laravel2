<?php

namespace App\Http\Controllers;

use App\Functions\System;
use App\Models\BankLine;
use App\Models\Country;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Log;

class ImportBankLinesController extends Controller
{
    public function index()
    {
        $last_bank_line = DB::table("bank_lines")
            ->where("bali_source", 1)
            ->orderBy("bali_date", 'desc')
            ->first();

        if ($last_bank_line)
        {
            $last_date = $last_bank_line->bali_date;
            $next_date = date("Y-m-d", strtotime($last_bank_line->bali_date . " +1 weekday"));

            $last_statement_number = $last_bank_line->bali_statement_number;

            $number = explode("-", $last_statement_number);

            $next_statement_number = $number[0]."-".($number[1]+1);

        } else
        {
            $last_date = "-";
            $next_date = date("Y-m-d", strtotime("-1 weekday"));

            $last_statement_number = "-";
            $next_statement_number = date("y") . "-001";
        }

        return view('finance.importbanklines', [
            'next_statement_number' => $next_statement_number,
            'last_statement_number' => $last_statement_number,
            'next_date' => $next_date,
            'last_date' => $last_date
        ]);
    }

    public function import(Request $request)
    {
        $request->validate([
            'statement_number' => 'required',
            'date' => 'required',
            'xml' => 'required'
        ]);

        Log::debug($request);

        $query_statement_number = DB::table("bank_lines")
            ->where("bali_statement_number", $request->statement_number)
            ->where("bali_source", 1)
            ->first();

        if (!empty($query_statement_number))
        {
            return redirect()->back()->withErrors(['errors' => "The bank lines for the entered statement number already exist."]);
        }

        $query_date = DB::table("bank_lines")
            ->where("bali_date", $request->date)
            ->where("bali_source", 1)
            ->first();

        if (!empty($query_date))
        {
            return redirect()->back()->withErrors(['errors' => "The bank lines for the entered date already exist."]);
        }

        if (date("Y-m-d") <= date("Y-m-d", strtotime($request->date)))
        {
            return redirect()->back()->withErrors(['errors' => "The entered date is today or in the future."]);
        }

        if (!$request->hasFile('xml'))
        {
            return redirect()->back()->withErrors(['errors' => "The uploaded XML file could not be read. Please notfiy IT."]);
        }

        $xml = @simplexml_load_file($request->file('xml'));

        $xml = $xml->BkToCstmrStmt->Stmt;

        if (date("Y-m-d", strtotime($xml->FrToDt->FrDtTm)) != $request->date || date("Y-m-d", strtotime($xml->FrToDt->ToDtTm)) != $request->date)
        {
            return redirect()->back()->withErrors(['errors' => "The entered date does not match the date specified in the uploaded file."]);
        } elseif ($xml->Acct->Id->IBAN != str_replace(" ", "", System::getSetting("company_iban")) || $xml->Acct->Svcr->FinInstnId->BIC != System::getSetting("company_bic"))
        {
            return redirect()->back()->withErrors(['errors' => "The IBAN or BIC entered in the system's settings do not match the IBAN or BIC specified in the uploaded file."]);
        } elseif ($xml->Acct->Ccy != "EUR")
        {
            return redirect()->back()->withErrors(['errors' => "The entered currency does not match the system's currency."]);
        } elseif ($xml->TxsSummry->TtlNtries->NbOfNtries == 0 || $xml->TxsSummry->TtlNtries->Sum == 0)
        {
            return redirect()->back()->withErrors(['errors' => "There are no entries found in the imported file."]);
        } elseif ($xml->TxsSummry->TtlNtries->NbOfNtries != count($xml->Ntry))
        {
            return redirect()->back()->withErrors(['errors' => "The total amount of entries does not match the specified total amount of entries. Please notify IT!"]);
        } else
        {
            $date_correct = true;

            foreach ($xml->Ntry as $entry)
            {
                if ($entry->BookgDt->Dt != $request->date)
                {
                    $date_correct = false;
                }
            }

            if (!$date_correct)
            {
                return redirect()->back()->withErrors(['errors' => "The are one or more entries found with an incorrect date. Please notify IT!"]);
            } else
            {
                DB::beginTransaction();

                try {
                    foreach ($xml->Ntry as $entry)
                    {
                        $array = [];
                        if ($entry->CdtDbtInd == "CRDT")
                        {
                            $credit_debit = "debit";

                            $amount = $entry->Amt->__toString();

                            $cr_db = "Dbtr";
                        } elseif ($entry->CdtDbtInd == "DBIT")
                        {
                            $credit_debit = "credit";

                            $amount = -$entry->Amt->__toString();

                            $cr_db = "Cdtr";
                        }

                        $debtor_details = [];
                        $bank_details = [];

                        if (!empty($entry->NtryDtls->TxDtls->RltdPties->{$cr_db}->Nm))
                        {
                            $debtor_details[] = $entry->NtryDtls->TxDtls->RltdPties->{$cr_db}->Nm;
                        }

                        if (!empty($entry->NtryDtls->TxDtls->RltdPties->{$cr_db}->PstlAdr))
                        {
                            foreach ($entry->NtryDtls->TxDtls->RltdPties->{$cr_db}->PstlAdr->children() as $key => $value)
                            {
                                if ($key == "Ctry")
                                {
                                    $debtor_details[] = Country::where("co_code", $value)->first()->co_en;
                                } else
                                {
                                    $debtor_details[] = $value;
                                }
                            }
                        }

                        if (!empty($entry->NtryDtls->TxDtls->RltdPties->{$cr_db . "Acct"}->Id))
                        {
                            foreach ($entry->NtryDtls->TxDtls->RltdPties->{$cr_db . "Acct"}->Id->children() as $key => $value)
                            {
                                if ($key == "IBAN")
                                {
                                    $bank_details[] = "IBAN: " . $value;
                                } elseif ($key == "Othr" && $value->SchmeNm->Prtry == "PrtryAcct")
                                {
                                    $bank_details[] = "Bank account: " . $value->Id;
                                }
                            }
                        }

                        if (!empty($entry->NtryDtls->TxDtls->RltdAgts->{$cr_db . "Agt"}->FinInstnId->BIC))
                        {
                            $bank_details[] = "BIC: " . $entry->NtryDtls->TxDtls->RltdAgts->{$cr_db . "Agt"}->FinInstnId->BIC;
                        }

                        if ($entry->CdtDbtInd == "CRDT" && !empty($entry->NtryDtls->TxDtls->AmtDtls->InstdAmt->Amt))
                        {
                            $currency = $entry->NtryDtls->TxDtls->AmtDtls->InstdAmt->Amt['Ccy'];
                            $amount_fc = $entry->NtryDtls->TxDtls->AmtDtls->InstdAmt->Amt;
                        } else
                        {
                            $currency = "EUR";
                            $amount_fc = $amount;
                        }

                        $information = "";

                        if (!empty($entry->NtryDtls->TxDtls->RmtInf->Ustrd))
                        {
                            $information = implode("\r\n", (array)$entry->NtryDtls->TxDtls->RmtInf->Ustrd);
                        } elseif (!empty($entry->NtryDtls->Btch->PmtInfId))
                        {
                            $information = "Batch ID: " . $entry->NtryDtls->Btch->PmtInfId . "\r\n";
                            $information .= "Entries: " . $entry->NtryDtls->Btch->NbOfTxs;
                        }

                        /*$bank_line = new BankLine();

                        $bank_line->bali_statement_number = $request->statement_number;
                        $bank_line->bali_date = date("Y-m-d", strtotime($entry->BookgDt->Dt));
                        $bank_line->bali_reference = $entry->NtryRef;
                        $bank_line->bali_credit_debit = $credit_debit;
                        $bank_line->bali_debtor_details = implode("\r\n", $debtor_details);
                        $bank_line->bali_bank_details = implode("\r\n", $bank_details);
                        $bank_line->bali_amount = $amount;
                        $bank_line->bali_currency = $currency;
                        $bank_line->bali_amount_fc = $amount_fc;
                        $bank_line->bali_information = $information;
                        $bank_line->bali_source = 1;
                        $bank_line->bali_file_name = $request->xml;

                        $bank_line->save();*/

                        $array = [
                            "bali_statement_number" => $request->statement_number,
                            "bali_date" => date("Y-m-d", strtotime($entry->BookgDt->Dt)),
                            "bali_reference" => $entry->NtryRef,
                            "bali_credit_debit" => $credit_debit,
                            "bali_debtor_details" => implode("\r\n", $debtor_details),
                            "bali_bank_details" => implode("\r\n", $bank_details),
                            "bali_amount" => $amount,
                            "bali_currency" => $currency,
                            "bali_amount_fc" => $amount_fc,
                            "bali_information" => $information,
                            "bali_source" => 1,
                            "bali_file_name" => $request->xml,
                        ];

                        $last_currency = $currency;

                        DB::table("bank_lines")->insert($array);
                    }

                    DB::commit();
                } catch (\Exception $e) {
                    // something went wrong
                    DB::rollback();

                    return Redirect::to("/finance/importbanklines")->withErrors(['Error with Currency in the imported file', 'Please contact IT, there is a bank line with a new currency which we dont have in our System (Currency: '.$last_currency.'). This currency needs to be added to our system.']);
                }

                //move_uploaded_file($request->file('xml'), env("SHARED_FOLDER") . "uploads/bank_lines/" . $request->xml);
                //TODO: upload and store XML file

                return view('finance.importbanklinessuccess', [
                    'amount' => $xml->TxsSummry->TtlNtries->NbOfNtries,
                    'creditsum' => System::numberFormat($xml->TxsSummry->TtlCdtNtries->Sum->__toString(), 2),
                    'debitsum' => System::numberFormat($xml->TxsSummry->TtlDbtNtries->Sum->__toString(), 2),
                    'sumsum' => System::numberFormat(($xml->TxsSummry->TtlDbtNtries->Sum->__toString() - $xml->TxsSummry->TtlCdtNtries->Sum->__toString()), 2),
                    'statement_number' => $request->statement_number
                ]);

            }
        }
    }

}

