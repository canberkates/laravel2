<?php

namespace App\Http\Controllers;

use App\Models\Question;
use App\Models\SireloReviewInvite;
use App\Models\Survey1;
use App\Models\Survey1Customer;
use App\Models\Survey2;
use App\Models\WebsiteReview;
use DB;
use Illuminate\Http\Request;
use Log;

class AnonymizeController extends Controller
{
	public function index()
	{
		return view('anonymize.index',
			[
			    "selected" => null
			]);

	}

	public function filteredIndex(Request $request)
	{

        return view('anonymize.index',
            [
                "selected" => true,
                "requests" => self::getRequests($request),
                'websitereviews' => self::getWebsiteReviews($request),
                'surveys2' => self::getSurveyReviews($request),
                'questions' => self::getQuestions($request),
                'surveys1' => self::getSurvey1($request),
                'reactiontimes' => self::getSurvey1Customers($request),
                'sireloinvites' => self::getSireloInvites($request),
                'request' => $request
            ]);
	}

    public function anonymize(Request $request)
    {
        foreach(self::getRequests($request) as $req)
        {
            $getrequest = \App\Models\Request::findOrFail($req->re_id);

            $getrequest->re_street_from = "";
            $getrequest->re_zipcode_from = "";
            $getrequest->re_google_maps_from = "";
            $getrequest->re_street_to = "";
            $getrequest->re_zipcode_to = "";
            $getrequest->re_google_maps_to = "";
            $getrequest->re_gender = "";
            $getrequest->re_company_name = (empty($req->re_company_name) ? "" : "[REMOVED]");
            $getrequest->re_first_name = "";
            $getrequest->re_family_name = "";
            $getrequest->re_full_name = "";
            $getrequest->re_telephone1 = "";
            $getrequest->re_telephone2 = "";
            $getrequest->re_internal_remarks = "GDPR Anonymized";
            $getrequest->re_email =  hash("sha256", $req->re_email);
            $getrequest->re_remarks = (empty($req->re_remarks) ? "" : "[REMOVED]");
            $getrequest->re_ip_address = hash("sha256", $req->re_ip_address);
            $getrequest->re_anonymized = 1;

            $getrequest->save();
        }

        foreach(self::getWebsiteReviews($request) as $wr){

            $websitereview = WebsiteReview::findOrFail($wr->were_id);

            $websitereview->were_name = "";
            $websitereview->were_email = hash("sha256", $websitereview->were_email);
            $websitereview->were_ip_address = hash("sha256", $websitereview->were_ip_address);
            $websitereview->were_anonymized = 1;

            $websitereview->save();
        }

        foreach(self::getSurveyReviews($request) as $survey_review){

            $survey2 = Survey2::findOrFail($survey_review->su_id);

            $survey2->su_why_mover = "";
            $survey2->su_rating_motivation = "";
            $survey2->su_mover_comment = "";
            $survey2->su_checked_remarks = "";
            $survey2->su_con_1 = "";
            $survey2->su_con_2 = "";
            $survey2->su_con_1_category = NULL;
            $survey2->su_con_2_category = NULL;
            $survey2->su_pro_1 = "";
            $survey2->su_pro_2 = "";
            $survey2->su_pro_1_category = NULL;
            $survey2->su_pro_2_category = NULL;
            $survey2->su_show_on_website = 0;
            $survey2->su_anonymized = 1;

            $survey2->save();
        }

        foreach(self::getQuestions($request) as $q){

            $question = Question::findOrFail($q->ktrecuqu_id);

            $question->ktrecuqu_answer_1 = "";
            $question->ktrecuqu_answer_2 = "";
            $question->ktrecuqu_anonymized = 1;

            $question->save();
        }

        foreach(self::getSurvey1($request) as $s1){

            $survey1 = Survey1::findOrFail($s1->su_id);

            $survey1->su_reminder_old = (empty($s1->su_reminder) ? "" : "[REMOVED]");
            $survey1->su_checked_remarks = (empty($s1->su_checked_remarks) ? "" : "[REMOVED]");
            $survey1->su_anonymized = 1;

            $survey1->save();
        }

        foreach(self::getSurvey1Customers($request) as $s1c){

            $survey1customer = Survey1Customer::findOrFail($s1c->sucu_id);

            $survey1customer->sucu_remark = (empty($s1c->sucu_remark) ? "" : "[REMOVED]");
            $survey1customer->sucu_anonymized = 1;

            $survey1customer->save();
        }

        foreach(self::getSireloInvites($request) as $si){

            $sireloinvite = SireloReviewInvite::findOrFail($si->sirein_id);

            $sireloinvite->sirein_email = hash("sha256", $si->sirein_email);
            $sireloinvite->sirein_name = "";
            $sireloinvite->sirein_subject = (empty($si->sirein_subject) ? "" : "[REMOVED]");
            $sireloinvite->sirein_content = (empty($si->sirein_content) ? "" : "[REMOVED]");
            $sireloinvite->sirein_anonymized = 1;

            $sireloinvite->save();

        }

        return view('anonymize.index',
            [
                "selected" => true,
                "requests" => self::getRequests($request),
                'websitereviews' => self::getWebsiteReviews($request),
                'surveys2' => self::getSurveyReviews($request),
                'questions' => self::getQuestions($request),
                'surveys1' => self::getSurvey1($request),
                'reactiontimes' => self::getSurvey1Customers($request),
                'sireloinvites' => self::getSireloInvites($request)
            ]);
    }

    function getRequests($request)
    {
        return DB::table("requests")
            ->where("re_email", $request->email)
            ->get();
    }

    function getWebsiteReviews($request)
    {
        return DB::table("surveys_2")
            ->leftJoin("website_reviews", "su_were_id", "were_id")
            ->where("were_email", $request->email)
            ->get();

    }

    function getSurveyReviews($request)
    {
        return DB::table("surveys_2")
            ->leftJoin("requests", "su_re_id", "re_id")
            ->where("re_email", $request->email)
            ->get();
    }


    function getQuestions($request)
    {
        return DB::table("kt_request_customer_question")
            ->leftJoin("requests", "re_id", "ktrecuqu_re_id")
            ->where("re_email", $request->email)
            ->get();
    }

    function getSurvey1($request)
    {
        return DB::table("surveys_1")
            ->leftJoin("requests", "re_id", "su_re_id")
            ->where("re_email", $request->email)
            ->get();
    }


    function getSurvey1Customers($request)
    {
        return DB::table("survey_1_customers")
            ->leftJoin("surveys_1", "su_id", "sucu_su_id")
            ->leftJoin("requests", "re_id", "su_re_id")
            ->where("re_email", $request->email)
            ->get();
    }


    function getSireloInvites($request)
    {
        return DB::table("sirelo_review_invites")
            ->where("sirein_email", $request->email)
            ->get();

    }

}
