<?php

namespace App\Http\Controllers;

use App\Data\BankTransactionMutationType;
use App\Functions\System;
use App\Models\Customer;
use App\Models\LedgerAccount;
use App\Models\PaymentCurrency;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Log;

class BankTransactionsSummaryController extends Controller
{
	public function index()
	{
        foreach(LedgerAccount::all() as $ledger){
            $ledgeraccounts[$ledger->leac_number] = $ledger->leac_number . " (".$ledger->leac_name.")";
        }
        
		return view('finance.banktransactionssummary', [
			'selected_period' => "",
			'selected_statement' => "",
            'selected_mutation' => "",
            'selected_customer' => "",
            'selected_ledger' => "",
            'selected_pair' => 0,
            'mutationtypes' => BankTransactionMutationType::all(),
            'customers' => Customer::all(),
            'ledgeraccounts' => $ledgeraccounts
		]);
	}
	
	public function filteredIndex(Request $request)
	{
	    Log::debug($request);
        
        $invoices = DB::table("kt_bank_line_invoice_customer_ledger_account")
            ->select("ktbaliinculeac_cu_id","ktbaliinculeac_id","in_id","in_number","cu_id","cu_company_name_legal", "ktbaliinculeac_leac_number","bali_date", "bali_reference", "bali_credit_debit", "bali_debtor_details", "ktbaliinculeac_amount", "bali_id", "bali_statement_number", "bali_information", "ktbaliinculeac_currency", "ktbaliinculeac_amount_fc" )
            ->leftJoin("bank_lines","ktbaliinculeac_bali_id", "bali_id")
            ->leftJoin("invoices","ktbaliinculeac_in_id", "in_id")
            ->leftJoin("customers","ktbaliinculeac_cu_id", "cu_id")
            ->whereNotNull("ktbaliinculeac_bali_id")
            ->whereBetween("bali_date", System::betweenDates($request->date));
        
        if(!empty($request->statement_number)){
            $invoices->where("bali_statement_number", $request->statement_number);
        }
        
        if($request->mutation == 1){
            $invoices->whereNotNUll("ktbaliinculeac_cu_id");
        }else if($request->mutation == 2){
            $invoices->whereNull("ktbaliinculeac_cu_id");
        }
        
        if(!empty($request->customer)){
            $invoices->where("ktbaliinculeac_cu_id", $request->customer);
        }
        
        if(!empty($request->ledger)){
            $invoices->where("ktbaliinculeac_leac_number", $request->ledger);
        }
        
        if(Arr::exists($request, 'not_paired')){
            Log::debug("hehee");
            $invoices->whereNull("ktbaliinculeac_in_id");
        }
        
        $invoices->whereRaw("(
            `customers`.`cu_deleted` = '0' OR
            `customers`.`cu_deleted` IS NULL
	    )");
        
        $invoices = $invoices
            ->get();
	    
        $sum = $invoices->sum("ktbaliinculeac_amount");
        
        foreach(LedgerAccount::all() as $ledger){
            $ledgeraccounts[$ledger->leac_number] = $ledger->leac_number . " (".$ledger->leac_name.")";
        }
        
        $currencies = PaymentCurrency::all();
        
        foreach ($currencies as $currency)
        {
            $currency_tokens[$currency->pacu_code] = $currency->pacu_token;
        }
        
        return view('finance.banktransactionssummary', [
            'selected_period' => $request->date,
            'selected_statement' => $request->statement_number,
            'selected_mutation' => $request->mutation,
            'selected_customer' => $request->customer,
            'selected_ledger' => $request->ledger,
            'selected_pair' => Arr::exists($request, 'not_paired') ? 1 : 0,
            'mutationtypes' => BankTransactionMutationType::all(),
            'customers' => Customer::where("cu_deleted", 0)->get(),
            'ledgeraccounts' => $ledgeraccounts,
            'currencies' => $currency_tokens,
            'invoices' => $invoices,
            'sum' => $sum
        ]);
		
	}
 
 
	
}

