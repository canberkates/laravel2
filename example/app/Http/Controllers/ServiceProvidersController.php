<?php

namespace App\Http\Controllers;

use App\Data\ContactPersonDepartment;
use App\Data\ContactPersonDepartmentRole;
use App\Data\CustomerDocumentType;
use App\Data\CustomerEditForms;
use App\Data\CustomerRemarkDepartment;
use App\Data\CustomerRemarkDirection;
use App\Data\CustomerRemarkMedium;
use App\Data\CustomerStatusReason;
use App\Data\CustomerStatusStatus;
use App\Data\CustomerType;
use App\Data\DebtorStatus;
use App\Data\InvoicePeriod;
use App\Data\NewsletterSlot;
use App\Data\PaymentMethod;
use App\Data\PaymentReminder;
use App\Data\PaymentReminderStatus;
use App\Data\RequestStatus;
use App\Data\RequestType;
use App\Data\YesNo;
use App\Functions\System;
use App\Models\AdyenCardDetails;
use App\Models\Cache;
use App\Models\Country;
use App\Models\Customer;
use App\Models\CustomerChange;
use App\Models\CustomerDocument;
use App\Models\Invoice;
use App\Models\KTBankLineInvoiceCustomerLedgerAccount;
use App\Models\KTRequestCustomerQuestion;
use App\Models\Language;
use App\Models\LedgerAccount;
use App\Models\PaymentCurrency;
use App\Models\ServiceProviderData;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class ServiceProvidersController extends Controller
{
	public function index()
	{
		$system = new System();

		$customer_changes = CustomerChange::where([["cuch_approve_deny", "=", "0"]])->get();

		$cache = Cache::where("ca_name", "=", "customer_count")->first();

		$cached_count = $system->unserialize($cache->ca_value);
		$last_timestamp_updated = $cache->ca_timestamp;

		return view('serviceproviders.index', [
			'customer_changes_count' => count($customer_changes),
			'cached_count' => $cached_count,
			'last_timestamp_updated' => $last_timestamp_updated
		]);
	}

	public function edit($id, $request_date_filter = null, $invoice_paid_filter = null)
	{
		// get the customer and relations
		$customer = Customer::with(['serviceproviderdata', 'statuses.user', 'remarks.user', 'remarks.status', 'documents.user', 'contactpersons.application_user', 'customerquestions.questions'])->findOrFail($id);

        //Check if collegue has the right URL
        $url_check = System::customerTypeToURL($customer->cu_id, $customer->cu_type);

        if ($url_check['redirect'] == true)
        {
            return redirect($url_check['url']);
        }

		//Show different view for deleted customers
		if($customer->cu_deleted == 1){
			return view('serviceproviders.deleted',
				[
					'customer_id' => $customer->cu_id
				]);
		}

		$customer_questions = null;
		$request_date_filter_data = null;

		if ($request_date_filter != null)
		{
			$customer_questions = KTRequestCustomerQuestion::with(["request.countryfrom", "request.countryto", "question"])
				->join("requests", "ktrecuqu_re_id", "re_id")
				->join("questions", "ktrecuqu_qu_id", "qu_id")
				->where("ktrecuqu_cu_id", $id)
				->where("ktrecuqu_hide", 0)
				->whereBetween("ktrecuqu_timestamp", [date("Y-m-d H:i:s", strtotime($request_date_filter[0])), date("Y-m-d H:i:s", strtotime($request_date_filter[1]))])
				->orderBy("ktrecuqu_timestamp", 'asc')
				->get();

			$request_date_filter_data = date("Y/m/d", strtotime($request_date_filter[0]))." - ".date("Y/m/d", strtotime($request_date_filter[1]));
		}

		//Create useful objects for methods
		$system = new System;

		//Get all credit cards
		$creditcards = AdyenCardDetails::where("adcade_cu_id", "=", $id)->where("adcade_removed", "=", 0)->orderBy("adcade_enabled", "desc")->get();

		$this->getContactPersonLogin($customer);
		//$this->calculateInvoiceData($customer, $system);

		$documents = CustomerDocument::where("cudo_archived", "=", 0)->where("cudo_cu_id", "=", $customer->cu_id)->get();
		$documents_archived = CustomerDocument::where("cudo_archived", "=", 1)->where("cudo_cu_id", "=", $customer->cu_id)->get();

		$invoices = Invoice::where("in_cu_id", "=", $customer->cu_id)->get();
		$amount_paired_per_invoice = [];
		$payment_dates_per_invoice = [];

		foreach ($invoices as $invoice)
		{
			$bank_lines = KTBankLineInvoiceCustomerLedgerAccount::leftJoin("bank_lines", "ktbaliinculeac_bali_id", "bali_id")
				->where("ktbaliinculeac_in_id", "=", $invoice->in_id)
				->get();

			$amount_paired_per_invoice[$invoice->in_id] = 0;
			$payment_dates_per_invoice[$invoice->in_id] = [];

			foreach ($bank_lines as $bali)
			{
				$amount_paired_per_invoice[$invoice->in_id] += $bali->ktbaliinculeac_amount;

				$payment_dates_per_invoice[$invoice->in_id][] = (($bali->bali_date == 0) ? $bali->ktbaliinculeac_date : $bali->bali_date);
			}
		}
		$currency_tokens = [];

		$currencies = PaymentCurrency::all();

		foreach ($currencies as $currency)
		{
			$currency_tokens[$currency->pacu_code] = $currency->pacu_token;
		}

		return view('serviceproviders.edit',
			[
				//Customer info
				'customer' => $customer,
				'statuses' => $customer->statuses,
				'remarks' => $customer->remarks,
				'documents' => $documents,
				'documents_archived' => $documents_archived,
				'offices' => $customer->offices,
				'questions' => $customer->customerquestions,
				'newsletterblocks' => $customer->customernewsletterblocks,
				'advertorialblocks' => $customer->customeradvertorialblocks,

				//Moverinfo
				'moverdata' => $customer->moverdata,
				'invoices' => $customer->invoices,
				'customerrequests' => $customer_questions,
				'creditcards' => $creditcards,
				"request_date_filter" => $request_date_filter_data,

				//Lists
				'users' => User::where("id", "!=", 0)->where("us_is_deleted", "=", 0)->orderBy("us_name")->get(),
				'invoiceperiods' => InvoicePeriod::all(),
				'countries' => Country::all(),
				'movertypes' => CustomerType::all(),
				'debtorstatuses' => DebtorStatus::all(),
				'paymentreminderstatuses' => PaymentReminderStatus::all(),
				'paymentreminder' => PaymentReminder::all(),
				'paymentmethods' => PaymentMethod::all(),
				'paymentcurrencies' => PaymentCurrency::all(),
				'ledgeraccounts' => LedgerAccount::all(),
				'languages' => Language::where("la_iframe_only", 0)->get(),
				'contactpersondepartments' => ContactPersonDepartment::all(),
				'contactpersondepartmentroles' => ContactPersonDepartmentRole::all(),
				'requesttypes' => RequestType::all(),
				'customerstatusstatuses' => CustomerStatusStatus::all(),
				'customerstatusreasons' => CustomerStatusReason::all(),
				'customerremarkdepartments' => CustomerRemarkDepartment::all(),
				'customerremarkmediums' => CustomerRemarkMedium::all(),
				'customerremarkdirections' => CustomerRemarkDirection::all(),
				'customerdocumenttypes' => CustomerDocumentType::all(),
				'questionstatuses' => [0 => 'Inactive', 1 => 'Active', 2 => 'Pause'],
				'yesno' => YesNo::all(),
				'requeststatus' => RequestStatus::all(),
				"invoices_paid_filter" => $invoice_paid_filter,
				"amount_paired_per_invoice" => $amount_paired_per_invoice,
				"payment_dates_per_invoice" => $payment_dates_per_invoice,
				"currency_tokens" => $currency_tokens,
				"newsletterslots" => NewsletterSlot::all()
			]);
	}

	public function update(Request $request, $id)
	{
		$customer = Customer::find($id);
		$service_provider_data = ServiceProviderData::where("seprda_cu_id", $id)->first();

		//If General form is posted
		if ($request->form_name == CustomerEditForms::GENERAL)
		{
			$request->validate([
				'la_code' => 'required',
				'company_name_business' => 'required',
				'company_name_legal' => 'required',
				'attn' => 'required',
				'attn_email' => 'required',
				'lead_email' => 'required'
			]);

			$customer->cu_company_name_business = $request->company_name_business;
			$customer->cu_company_name_legal = $request->company_name_legal;
			$customer->cu_email = $request->lead_email;
			$customer->cu_email_bi = $request->billing_email;

            if ((empty($customer->cu_description_la_code) || $customer->cu_description != $request->description) && !empty($request->description)) {
                $description_la_code = System::getGoogleTranslationLanguage($request->description);

                if (!empty($description_la_code)) {
                    $customer->cu_description_la_code = $description_la_code;
                }
            }

			$customer->cu_description = $request->description;
			$customer->cu_attn = $request->attn;
			$customer->cu_attn_email = $request->attn_email;
            $service_provider_data->seprda_lead_form = ($request->sub_type_lead_form === 'on');
            $service_provider_data->seprda_email_marketing = ($request->sub_type_email_marketing === 'on');
            $service_provider_data->seprda_sirelo_advertising = ($request->sub_type_sirelo_advertising === 'on');
			$customer->cu_la_code = $request->la_code;
			$customer->cu_coc = $request->coc;

			//if a new logo was uploaded
			if($request->logo)
			{
				$allowed_types = ['image/jpeg', 'image/png'];

                $image_size = getimagesize($_FILES['logo']['tmp_name']);
				$image_width = $image_size[0];
				$image_height = $image_size[1];

				if($_FILES['logo']['size'] < 1)
				{
					return redirect()->back()->withErrors([ 'logo' => 'The logo has not been saved, because the file is empty.']);
				}
				elseif($_FILES['logo']['size'] > 10000000)
				{
					return redirect()->back()->withErrors([ 'logo' => 'The logo has not been saved, because the file is bigger than 10MB.']);
				}
				elseif(!in_array($_FILES['logo']['type'], $allowed_types))
				{
					return redirect()->back()->withErrors([ 'logo' => 'The logo has not been saved, because the file type is not valid.']);
				}
				/*elseif($image_width < 100 || $image_height < 50)
				{
					return redirect()->back()->withErrors([ 'logo' => 'The logo has not been saved, because the image is too small. The image has to be min. 100 pixels wide and 50 pixels high.']);
				}
				elseif($image_width > 600 || $image_height > 200)
				{
					return redirect()->back()->withErrors([ 'logo' => 'The logo has not been saved, because the image is too big. The image has to be max. 600 pixels wide and 200 pixels high.']);
				}*/

				$system = new System();

                $img = Image::make($request->file('logo'));

                if ($img->width() > 600 || $img->height() > 200) {
                    $img->resize(600, 200, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }

                $file_location = config("system.paths.shared") . 'uploads/logos/';
                $new_logo = $request->file('logo');
                $new_logo_name = $system->hashLogoName($request->curesc_id, $new_logo->getClientOriginalExtension());

                $img->save( $file_location.$new_logo_name );


                // Update the logo name in the db
                $customer->cu_logo = $new_logo_name;

			}

			$customer->save();
            $service_provider_data->save();

		} else if ($request->form_name == CustomerEditForms::ADDRESSES)
		{
			if ($request->use_billing_address !== 'on')
			{
				$request->validate([
					'street_1' => 'required',
					'zipcode' => 'required',
					'city' => 'required',
					'country' => 'required',
					'telephone' => 'required',
					'website' => 'required'
				]);
			}

			if(($request->int_telephone == 0 && !empty($request->telephone)) || ($request->int_telephone_bi == 0 && !empty($request->telephone_bi)))
			{
				return redirect()->back()->withErrors(['phone' => 'This phone number is incorrect!']);
			}

			$customer->cu_street_1 = $request->street_1;
			$customer->cu_street_2 = $request->street_2;
			$customer->cu_zipcode = $request->zipcode;
			$customer->cu_city = $request->city;
			$customer->cu_telephone = $request->int_telephone;
			$customer->cu_website = $request->website;
			$customer->cu_co_code = $request->country;

			$customer->cu_use_billing_address = ($request->use_billing_address === 'on');

			if($request->use_billing_address === 'on')
			{

				$customer->cu_street_1_bi = $request->street_1_bi;
				$customer->cu_street_2_bi = $request->street_2_bi;
				$customer->cu_zipcode_bi = $request->zipcode_bi;
				$customer->cu_city_bi = $request->city_bi;
				$customer->cu_telephone_bi = $request->int_telephone_bi;
				$customer->cu_co_code_bi = $request->country_bi;
				$customer->cu_website_bi = $request->website_bi;
			}

			$customer->save();
		} else if ($request->form_name == CustomerEditForms::FINANCE)
		{
			$old_credit_hold_value = $customer->cu_credit_hold;

			$customer->cu_sales_manager = $request->sales_manager;
			$customer->cu_account_manager = $request->account_manager;
			$customer->cu_debt_manager = $request->debt_manager;
			$customer->cu_payment_reminder_status = $request->payment_reminder_status;
			$customer->cu_debtor_status = $request->debtor_status;
			$customer->cu_payment_method = $request->payment_method;
			$customer->cu_pacu_code = $request->payment_currency;
			$customer->cu_payment_term = $request->payment_term;
			$customer->cu_leac_number = $request->ledger_account;
			$customer->cu_vat_number = $request->vat_number;
			$customer->cu_finance_remarks = $request->finance_remark;
			$customer->cu_invoice_period = $request->invoice_period;

			$customer->cu_debt_collector = (($request->debt_collector === 'on' && $request->credit_hold_hidden == 1) ? 1 : 0);
			$customer->cu_credit_hold = ($request->credit_hold_hidden == 1);
			$customer->cu_finance_lock = ($request->finance_lock === 'on');

			if ($old_credit_hold_value == 0 && $request->credit_hold === 'on')
			{
				$customer->cu_credit_hold_timestamp = date("Y-m-d H:i:s");
			}

			$customer->save();
		}

		return redirect('serviceproviders/' . $id . '/edit')->with('message', 'Service Provider successfully updated!');
	}

	public function getContactPersonLogin($customer): void
	{
		foreach ($customer->contactpersons as $contactperson)
		{
			if (!empty($contactperson->application_user))
			{
                if ($_SERVER['SERVER_NAME'] == "laravel.localhost") {
                    //LOCAL
                    $start_url = "http://service-providers.system";
                }
                elseif ($_SERVER['SERVER_NAME'] == "erp2.triglobal-test-back.nl") {
                    //TEST
                    $start_url = "http://service-providers.triglobal-test-back.nl";
                }
                else {
                    //LIVE
                    $start_url = "http://service-providers.triglobal.info";
                }

				$contactperson['login_url'] = $start_url."/erp.php/?id=" . md5($contactperson->application_user->apus_id) . "&login_key=" . md5($contactperson->application_user->apus_username) . "@" . md5($contactperson->application_user->apus_name);
			}
		}
	}

	public function calculateInvoiceData($customer, System $system): void
	{
		foreach ($customer->invoices as $invoice)
		{
			//Overdue
			$overdue = $system->dateDifference($invoice->in_expiration_date, date("Y-m-d"));

			if ($overdue < 0)
			{
				$overdue = 0;
			}

			$invoice->days_overdue = $overdue;

			$bank_lines = DB::table("kt_bank_line_invoice_customer_ledger_account")
				->select("ktbaliinculeac_bali_id", "ktbaliinculeac_date", "ktbaliinculeac_amount", "bali_date")
				->join("bank_lines", "ktbaliinculeac_bali_id", "=", "bali_id")
				->where('ktbaliinculeac_in_id', "=", $invoice->in_id)
				->get();

			$amount_paired = 0;
			$payment_dates = [];

			foreach ($bank_lines as $row_bali)
			{
				$amount_paired += $row_bali->ktbaliinculeac_amount;

				$payment_dates[] = (($row_bali->bali_date == 0) ? $row_bali->ktbaliinculeac_date : $row_bali->bali_date);
			}

			$invoice->payment_dates = implode(",", $payment_dates);
			$invoice->amount_paired = $amount_paired;

			//Reminder & method
			$invoice->payment_reminder = PaymentReminder::name($invoice->in_payment_reminder);
			$invoice->payment_method = PaymentMethod::name($invoice->in_payment_method);

			$invoice->currency_token_eur = $system->paymentCurrencyToken("EUR");
			$invoice->currency_token_fc = $system->paymentCurrencyToken($invoice->in_currency);
		}
	}

	public function confirmSendInvoice($customer_id, $invoice_id)
	{
		$invoice = Invoice::findOrFail($invoice_id);

		$debtor_data = unserialize(base64_decode($invoice->in_debtor_data));

		return view('invoice.confirm', ['invoice' => $invoice, 'debtor_data' => $debtor_data, 'redirect' => 'serviceproviders']);
	}

	public function getRequests(Request $request, $customer_id)
	{
		$system = new System();
		$dates = $system->betweenDates($request->date);

		return self::edit($customer_id, $dates);

	}

	public function getInvoices(Request $request, $customer_id)
	{
		return self::edit($customer_id, null, $request->paid);
	}
}
