<?php

namespace App\Http\Controllers;

use App\Models\FieldChange;
use App\Models\KTCustomerPortal;
use App\Models\Membership;
use App\Models\StatusHistory;
use Illuminate\Support\Facades\DB;
use Venturecraft\Revisionable\Revision;

class OldHistorySyncController extends Controller
{
	public function index()
	{
		//self::fieldChangesToRevisionTable();
		//self::statusHistoryToRevisionTable();
	}
	
	public function fieldChangesToRevisionTable(){
		$fieldChanges = FieldChange::where('fich_timestamp', ">", '2019-10-10 11:50:00')->get();
		//$this->CustomerAndMoverData($fieldChanges);
		//$this->MembershipData($fieldChanges);
		//$this->CustomerPortalRegions($fieldChanges);
	}
	
	public function statusHistoryToRevisionTable(){
		$statusHistory = StatusHistory::whereNotNull('sthi_ktcupo_id')->where('sthi_timestamp', ">", '2019-10-10 11:50:00')->get();
		
		//$this->CustomerPortalStatuses($statusHistory);
		
	}
	
	
	public function getModel(){
		return [
			"customers" => "App\Models\Customer",
			"mover_data" => "App\Models\MoverData",
			"kt_customer_membership" => "App\Models\KTCustomerMembership",
			"kt_customer_portal_region" => "App\Models\KTCustomerPortalRegion",
			"kt_customer_portal_country" => "App\Models\KTCustomerPortalCountry",
			"kt_customer_portal_region_destination" => "App\Models\KTCustomerPortalRegionDestination",
		];
	}
	
	public function getMembershipName($id){
		return Membership::where("me_id", $id)->first()->me_name;
	}
	
	public function getCustomerPortalName($id){
		$ktcupo = KTCustomerPortal::with("portal")->where("ktcupo_id", $id)->first();
		$name = $ktcupo->portal->po_portal. " - ".$ktcupo->ktcupo_description;
		return $name;
	}
	
	
	/**
	 * @param $fieldChanges
	 */
	public function CustomerAndMoverData($fieldChanges): void
	{
		//Customer and Mover Data
		foreach ($fieldChanges as $fieldChange)
		{
			if ($fieldChange->fich_table != "customers" && $fieldChange->fich_table != "mover_data")
			{
				continue;
			}
			$changes = unserialize(base64_decode($fieldChange->fich_changes));
			
			//Log::debug($fieldChange);
			//Log::debug($changes);
			
			foreach ($changes as $field_name => $change)
			{
				DB::table((new Revision())->getTable())->insert([
					[
						'revisionable_type' => self::getModel()[$fieldChange->fich_table],
						'revisionable_id' => $fieldChange->fich_item_id,
						'key' => $field_name,
						'old_value' => $change[0],
						'new_value' => $change[1],
						'user_id' => ($fieldChange->fich_us_id ?? null),
						'created_at' => $fieldChange->fich_timestamp,
						'updated_at' => $fieldChange->fich_timestamp,
					]
				]);
			}
		}
	}
	
	/**
	 * @param $fieldChanges
	 */
	public function MembershipData($fieldChanges): void
	{
		//Customer and Mover Data
		foreach ($fieldChanges as $fieldChange)
		{
			if ($fieldChange->fich_table != "kt_customer_membership")
			{
				continue;
			}
			$changes = unserialize(base64_decode($fieldChange->fich_changes));
			
			foreach ($changes as $field_name => $change)
			{
				
				DB::table((new Revision())->getTable())->insert([
					[
						'revisionable_type' => self::getModel()[$fieldChange->fich_table],
						'revisionable_id' => $fieldChange->fich_item_id,
						'key' => self::getMembershipName($field_name),
						'old_value' => $change[0],
						'new_value' => $change[1],
						'user_id' => ($fieldChange->fich_us_id ?? null),
						'created_at' => $fieldChange->fich_timestamp,
						'updated_at' => $fieldChange->fich_timestamp,
					]
				]);
			}
		}
	}
	
	public function CustomerPortalRegions($fieldChanges): void
	{
		//Customer portal region data
		foreach ($fieldChanges as $fieldChange)
		{
			if ($fieldChange->fich_table != "kt_customer_portal_region" && $fieldChange->fich_table != "kt_customer_portal_country" && $fieldChange->fich_table != "kt_customer_portal_region_destination" )
			{
				continue;
			}
			$changes = unserialize(base64_decode($fieldChange->fich_changes));
			
			foreach ($changes as $field_name => $change)
			{
				DB::table((new Revision())->getTable())->insert([
					[
						'revisionable_type' => self::getModel()[$fieldChange->fich_table],
						'revisionable_id' => $fieldChange->fich_item_id,
						'key' => $field_name,
						'old_value' => $change[0],
						'new_value' => $change[1],
						'user_id' => ($fieldChange->fich_us_id ?? null),
						'created_at' => $fieldChange->fich_timestamp,
						'updated_at' => $fieldChange->fich_timestamp
					]
				]);
			}
		}
	}
	
	/**
	 * @param $fieldChanges
	 */
	public function CustomerPortalStatuses($statuses): void
	{
		//Customer and Mover Data
		foreach ($statuses as $status)
		{
			DB::table((new Revision())->getTable())->insert([
				[
					'revisionable_type' => "App\Models\KTCustomerPortal",
					'revisionable_id' => $status->sthi_cu_id,
					'key' => self::getCustomerPortalName($status->sthi_ktcupo_id),
					'old_value' => $status->sthi_status_from,
					'new_value' => $status->sthi_status_to,
					'user_id' => ($status->sthi_us_id ?? null),
					'created_at' => $status->sthi_timestamp,
					'updated_at' => $status->sthi_timestamp,
				]
			]);
		}
	}
}
