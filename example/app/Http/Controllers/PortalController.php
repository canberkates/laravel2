<?php

namespace App\Http\Controllers;

use App\Data\CustomerPairStatus;
use App\Data\RequestType;
use App\Functions\System;
use App\Models\Country;
use App\Models\Portal;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PortalController extends Controller
{
	public function index()
	{
		return view('portals.index',
			[
				'portals' => Portal::with("country")->orderBy("po_portal", "asc")->get(),
				'requesttypes' => RequestType::all(),
				'statuses' => CustomerPairStatus::all(),

				'selected_portal' => [],
				'selected_requesttype' => [],
				'selected_status' => [],
				'selected_date' => Carbon::now()->format('Y-m-d'),

				'customers' => []
			]);
	}

	public function filteredindex(Request $request)
	{
		$system = new System();
		Log::debug($request);

		$customers = DB::table("customers")
			->leftJoin("kt_customer_portal", "cu_id", "ktcupo_cu_id")
			->leftJoin("portals", "ktcupo_po_id", "po_id")
			->where("ktcupo_po_id", $_POST['portal']);

		if(!empty($request->requesttype)){
			$customers->where("ktcupo_request_type", $request->requesttype);
		}

		if(!empty($request->status)){
			$customers->where("ktcupo_status", $request->status);
		}

		$customers = $customers
			->where("cu_deleted", 0)
			->orderBy("cu_company_name_business", 'asc')
			->get();

		foreach($customers as $customer){
			$payment_rate = DB::table("payment_rates")
				->where("para_ktcupo_id", $customer->ktcupo_id)
				->whereRaw("`para_date` <= '".date("Y-m-d", strtotime($request->payment_rate))."'")
				->orderBy("para_date", 'desc')
				->limit(1)
				->first();

			if($customer->ktcupo_free_trial == 1)
			{
				$rate_since = "-";
				$rate = "Free trial";
				$rate_discount = "-";
			}
			elseif(!empty($payment_rate))
			{
				$rate_since = $payment_rate->para_date;
				$rate = $system->paymentCurrencyToken($customer->po_pacu_code)." ".number_format($payment_rate->para_rate, 2);
				$rate_discount = (($payment_rate->para_discount_type == 1) ? $system->paymentCurrencyToken($customer->po_pacu_code)." " : "").(($payment_rate->para_discount_type == 0) ? "-" : $payment_rate->para_discount_rate).(($payment_rate->para_discount_type == 2) ? "%" : "");
			}
			else
			{
				$rate_since = "-";
				$rate = "-";
				$rate_discount = "-";
			}

			$customer->rate_since = $rate_since;
			$customer->rate = $rate;
			$customer->rate_discount = $rate_discount;
		}

		$countries = [];

		foreach(Country::all() as $country){
			$countries[$country->co_code] = $country->co_en;
		}

		Log::debug($countries);

		return view('portals.index',
			[
				'portals' => Portal::all(),
				'requesttypes' => RequestType::all(),
				'statuses' => CustomerPairStatus::all(),

				'selected_portal' => ($request->portal ?? []),
				'selected_requesttype' => ($request->requesttype ?? []),
				'selected_status' => ($request->status ?? []),
				'selected_date' => ($request->payment_rate ?? []),

				'customers' => $customers,
				'countries' => $countries
			]);
	}


    public function getPortalPrices()
    {
        return view('portals.prices',
            [
                'portals' => Portal::leftJoin("countries", "po_co_code", "co_code")->get()
            ]);
    }


    public function storePortalPrices(Request $request)
    {
        $portals = Portal::all();

        foreach($portals as $portal){
            $portal->po_minimum_price_int = $request->minimum_price_int[$portal->po_id];
            $portal->po_maximum_price_int = $request->maximum_price_int[$portal->po_id];
            $portal->po_minimum_price_nat_local = $request->minimum_price_nat_local[$portal->po_id];
            $portal->po_maximum_price_nat_local = $request->maximum_price_nat_local[$portal->po_id];
            $portal->po_minimum_price_nat_long_distance = $request->minimum_price_nat_long_distance[$portal->po_id];
            $portal->po_maximum_price_nat_long_distance = $request->maximum_price_nat_long_distance[$portal->po_id];
            $portal->save();
        }
        return view('portals.prices',
            [
                'portals' => Portal::leftJoin("countries", "po_co_code", "co_code")->get()
            ]);
    }

}
