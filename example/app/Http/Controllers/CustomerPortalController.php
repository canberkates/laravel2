<?php

namespace App\Http\Controllers;

use App\Data\CustomerPairStatus;
use App\Data\CustomerPortalForms;
use App\Data\CustomerRequestDeliveryType;
use App\Data\DestinationType;
use App\Data\FreeTrialStopType;
use App\Data\MovingSize;
use App\Data\NatPaymentType;
use App\Data\PortalType;
use App\Data\RequestType;
use App\Data\RequestTypeToMovingSizes;
use App\Data\RoomAmounts;
use App\Data\YesNo;
use App\Functions\System;
use App\Models\Country;
use App\Models\Customer;
use App\Models\CustomerOffice;
use App\Models\KTCustomerPortal;
use App\Models\KTCustomerPortalCountry;
use App\Models\KTCustomerPortalRegion;
use App\Models\KTCustomerPortalRegionDestination;
use App\Models\MacroRegion;
use App\Models\MoverData;
use App\Models\PaymentCurrency;
use App\Models\Portal;
use App\Models\Region;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Venturecraft\Revisionable\Revision;

class CustomerPortalController extends Controller
{
	public function edit($customer_id, $portal_id)
	{
		$system = new System();

		//Get customer and through the relation of the customer, get the correct contact person
		$portal = KTCustomerPortal::with(["customer", "portal.regions", "customerportalregions", "customerportalcountries", "customerportalregionsdestination"])->findOrFail($portal_id);

        $customer = Customer::where("cu_id", $portal->ktcupo_cu_id)->first();

        //Check if this user has the rights to edit
        if (Gate::denies('customer-restrictions', $customer)) {
            abort(403);
        }

		//Origins
		$regions_origin = [];
		$checked_origin = [];

		foreach($portal->customerportalregions as $checked_regions)
		{
			$checked_origin[$checked_regions['ktcupore_reg_id']] = 1;
		}

		foreach($portal->portal->regions->sortBy("reg_name") as $row_regions)
		{
			if($row_regions->reg_deleted){continue;}
			$regions_origin[$row_regions['reg_co_code']][$row_regions['reg_parent']][$row_regions['reg_id']] = [$row_regions['reg_name'], in_array($row_regions['reg_id'], array_keys($checked_origin))];
            ksort($regions_origin[$row_regions['reg_co_code']]);
        }

		//Destinations
		if($portal->portal->po_destination_type == DestinationType::INTMOVING)
		{
			$macro_regions = [];
			$regions_destination = [];
			$checked_destination = [];

			$requests_amount_per_block = [];

			foreach($portal->customerportalcountries as $checked_countries)
			{
				$checked_destination[$checked_countries['ktcupoco_co_code']] = 1;
			}

			foreach(MacroRegion::all() as $macro_region)
			{
				$macro_regions[$macro_region->mare_id] = [$macro_region->mare_name, $macro_region->mare_continent_name];
			}

			foreach(Country::all() as $country)
			{
                /*$request_amount_per_block_query = \App\Models\Request::join("regions", "re_reg_id_to", "reg_id")
                    ->join("countries", "reg_co_code", "co_code")
                    ->whereIn("re_reg_id_from", array_keys($checked_origin))
                    ->where("co_mare_id", $country->co_mare_id)
                    ->whereRaw("`re_timestamp` BETWEEN NOW() - INTERVAL 30 DAY AND NOW()")
                    ->get();

                $requests_amount_per_block[$macro_regions[$country->co_mare_id][0]] = $request_amount_per_block_query->count();*/

                $regions_destination[$macro_regions[$country->co_mare_id][1]][$macro_regions[$country->co_mare_id][0]][$country['co_code']] = [$country['co_en'], in_array($country['co_code'], array_keys($checked_destination))];
			}
		}
		else if($portal->portal->po_destination_type == DestinationType::NATMOVING)
		{
			//Origins
			$regions_destination = [];
			$checked_destination = [];

            $requests_amount_per_block = [];

            foreach($portal->customerportalregionsdestination as $checked_regions)
			{
				$checked_destination[$checked_regions['ktcuporede_reg_id']] = 1;
			}

			foreach($portal->portal->regions->sortBy('reg_name') as $row_regions)
			{
                /*$request_amount_per_block_query = \App\Models\Request::join("regions", "re_reg_id_to", "reg_id")
                    ->whereIn("re_reg_id_from", array_keys($checked_origin))
                    ->where("reg_parent", $row_regions['reg_parent'])
                    ->whereRaw("`re_timestamp` BETWEEN NOW() - INTERVAL 30 DAY AND NOW()")
                    ->get();

                $requests_amount_per_block[$row_regions['reg_parent']] = $request_amount_per_block_query->count();*/

				$regions_destination[$row_regions['reg_co_code']][$row_regions['reg_parent']][$row_regions['reg_id']] = [$row_regions['reg_name'], in_array($row_regions['reg_id'], array_keys($checked_destination))];
			}

		}
		$portal_rev_types = ["App\Models\KTCustomerPortal"];
		$portal_region_change_types = ["App\Models\KTCustomerPortalCountry", "App\Models\KTCustomerPortalRegion", "App\Models\KTCustomerPortalRegionDestination"];

		$portal_revisions = Revision::whereIn("revisionable_type", $portal_rev_types)->where("revisionable_id", "=", $portal_id)->get();
		$portal_region_revisions = Revision::whereIn("revisionable_type", $portal_region_change_types)->where("revisionable_id", "=", $portal_id)->get();

		$countries = [];

		foreach(Country::orderBy("co_en")->get() as $country)
		{
			$countries[$country->co_code] = $country->co_en;
		}

		$region_names = [];

		foreach(Region::orderBy("reg_name")->get() as $region)
		{
			$region_names[$region->reg_id] = $region->reg_name;
		}

		$types = RequestTypeToMovingSizes::name($portal->ktcupo_request_type);

		$moving_sizes = [];

		foreach($types as $type)
		{
			$moving_sizes[$type] = MovingSize::name($type);
		}

		$payment_currency = $system->paymentCurrencyToken($portal->portal->po_pacu_code);

		$capping_method = MoverData::select("moda_capping_method")->where("moda_cu_id", $customer_id)->first()->moda_capping_method;

		return view('customerportal.edit',
			[
				'portal' => $portal,
				'origins' => $regions_origin,
				'destinations' => $regions_destination,
				'statusupdates' => $portal->statusupdates,
				'requestdeliveries' => $portal->requestdeliveries,
				'paymentrates' => $portal->paymentrates,
				'freetrials' => $portal->freetrials,
				'portalstatuses' => CustomerPairStatus::all(),
				'requestdeliverytypes' => CustomerRequestDeliveryType::all(),
				'stoptypes' => FreeTrialStopType::all(),
				'paymentcurrencies' => PaymentCurrency::all(),
				'country_codes' => $countries,
				'region_names' => $region_names,
				'portal_history' => $portal_revisions,
				'portal_region_history' => $portal_region_revisions,
				'moving_sizes' => $moving_sizes,
				'payment_currency' => $payment_currency,
				'requesttypes' => RequestType::all(),
				'roomamounts' => RoomAmounts::all(),
                'pricetypes' => NatPaymentType::all(),
				'yesno' => YesNo::all(),
                'capping_method' => $capping_method,
                'requests_amount_per_block' => $requests_amount_per_block
			]);
	}

	public function update(Request $request, $customerportal_id)
	{
	    $system = new System();
		$customerportal = KTCustomerPortal::with("portal", "customer")->find($customerportal_id);

		//If General form is posted
		if($request->form_name == CustomerPortalForms::GENERAL)
		{
			$customerportal->ktcupo_description = $request->description;
			$customerportal->ktcupo_status = $request->status;

			$customerportal->save();
		}
		else if($request->form_name == CustomerPortalForms::FILTERS_REQUESTS)
		{

		    if($request->ktcupo_enable_room_sizes === 'on'){

		        //If using room_size, disable all moving sizes

		        for($i = 1; $i <= 10; $i++)
                {
                    $prop = "ktcupo_moving_size_".$i;
                    $customerportal->$prop = 0;
                }

                $customerportal->ktcupo_room_size_1 = ($request->ktcupo_room_size_1 === 'on');
                $customerportal->ktcupo_room_size_2 = ($request->ktcupo_room_size_2 === 'on');
                $customerportal->ktcupo_room_size_3 = ($request->ktcupo_room_size_3 === 'on');
                $customerportal->ktcupo_room_size_4 = ($request->ktcupo_room_size_4 === 'on');
            }else{
                //If not using room_size, disable all room_sizes
                for($i = 1; $i <= 4; $i++)
                {
                    $prop = "ktcupo_room_size_".$i;
                    $customerportal->$prop = 0;
                }

                $customerportal->ktcupo_moving_size_1 = ($request->ktcupo_moving_size_1 === 'on');
                $customerportal->ktcupo_moving_size_2 = ($request->ktcupo_moving_size_2 === 'on');
                $customerportal->ktcupo_moving_size_3 = ($request->ktcupo_moving_size_3 === 'on');
                $customerportal->ktcupo_moving_size_4 = ($request->ktcupo_moving_size_4 === 'on');
                $customerportal->ktcupo_moving_size_5 = ($request->ktcupo_moving_size_5 === 'on');
                $customerportal->ktcupo_moving_size_6 = ($request->ktcupo_moving_size_6 === 'on');
                $customerportal->ktcupo_moving_size_7 = ($request->ktcupo_moving_size_7 === 'on');
                $customerportal->ktcupo_moving_size_8 = ($request->ktcupo_moving_size_8 === 'on');
                $customerportal->ktcupo_moving_size_9 = ($request->ktcupo_moving_size_9 === 'on');
                $customerportal->ktcupo_moving_size_10 = ($request->ktcupo_moving_size_10 === 'on');
            }


			$customerportal->ktcupo_room_sizes_enabled = ($request->ktcupo_enable_room_sizes === 'on');
			$customerportal->ktcupo_export_moves = ($request->export_moves === 'on');
			$customerportal->ktcupo_import_moves = ($request->import_moves === 'on');
			$customerportal->ktcupo_nat_local_moves = ($request->local_moves === 'on');
			$customerportal->ktcupo_nat_long_distance_moves = ($request->long_distance_moves === 'on');

			$customerportal->save();
		}
		else if($request->form_name == CustomerPortalForms::FILTERS_CAPPINGS)
		{
			if($request->monthly_capping_triglobal < $request->monthly_capping_client && $request->monthly_capping_triglobal != "" && $request->monthly_capping_client != "")
			{
				$request->max_requests_month = $request->monthly_capping_triglobal;
			}
			elseif($request->monthly_capping_client < $request->monthly_capping_triglobal && $request->monthly_capping_client != "" && $request->monthly_capping_triglobal != "")
			{
				$request->max_requests_month = $request->monthly_capping_client;
			}
			else
			{
				if($request->monthly_capping_triglobal != "" && $request->monthly_capping_client == "")
					$request->max_requests_month = $request->monthly_capping_triglobal;

				if($request->monthly_capping_client != "" && $request->monthly_capping_triglobal == "")
					$request->max_requests_month = $request->monthly_capping_client;
			}

			if($request->monthly_capping_triglobal == $request->monthly_capping_client)
				$request->max_requests_month = $request->monthly_capping_triglobal;

			if($request->max_requests_month == "")
				$request->max_requests_month = 0;

			$difference_between_capping_and_left = $customerportal->ktcupo_max_requests_month - $customerportal->ktcupo_max_requests_month_left;

			$request->max_requests_month_left = $request->max_requests_month - $difference_between_capping_and_left;

			if ($customerportal->ktcupo_max_requests_month == 0)
			{
				$request->max_requests_month_left = $request->max_requests_month;
			}

			if ($request->max_requests_month_left < 0)
			{
				$request->max_requests_month_left = 0;
			}

			$customerportal->ktcupo_monthly_capping_triglobal = $request->monthly_capping_triglobal;
			$customerportal->ktcupo_monthly_capping_client = $request->monthly_capping_client;
			$customerportal->ktcupo_max_requests_month = $request->max_requests_month;
			$customerportal->ktcupo_max_requests_month_left = $request->max_requests_month_left;

			$difference_between_capping_day_and_left = $customerportal->ktcupo_max_requests_day - $customerportal->ktcupo_max_requests_day_left;

			$customerportal->ktcupo_max_requests_day = $request->max_requests_day;

			if (($request->max_requests_day - $difference_between_capping_day_and_left) < 0)
			{
				$customerportal->ktcupo_max_requests_day_left = 0;
			}
			else
			{
				$customerportal->ktcupo_max_requests_day_left = $request->max_requests_day - $difference_between_capping_day_and_left;
			}

			$customerportal->ktcupo_daily_average = $request->max_requests_month / 30.5;

			$customerportal->save();
		}
		else if($request->form_name == CustomerPortalForms::FREE_TRIAL){
			$customerportal->ktcupo_free_trial = ($request->free_trial === 'on');
			$customerportal->save();

		}
		else if($request->form_name == CustomerPortalForms::ORIGINS)
		{
            $available_values = DB::table("regions")
                ->select("reg_id")
                ->where("reg_destination_type", $customerportal->portal->po_destination_type)
                ->get();

            $available_values = $system->databaseToArray($available_values);

		    //Update these values with the history in mind
            $current_values = DB::table('kt_customer_portal_region')
                ->where('ktcupore_ktcupo_id', $customerportal_id)
                ->get()
                ->keyBy("ktcupore_reg_id");

            $current_values = $system->databaseToArray($current_values);

            //Arrays for keeping track
            $insert = [];
            $delete = [];

            //Loop through available values
            foreach($available_values as $available_value)
            {
                $found_value = $available_value['reg_id'];
                $current_set = isset($current_values[$found_value]);
                if(isset($request->origins[$found_value]))
                {
                    if(!$current_set)
                    {
                        $insert[$found_value] = $found_value;
                    }
                }
                elseif($current_set)
                {
                    $delete[$found_value] = $found_value;
                }
            }

            //Execute possible deletes/inserts
            if(!empty($insert))
            {
                foreach($insert as $insert_value)
                {
                    if($customerportal->ktcupo_cu_id == 18216){break;}
                    DB::table((new Revision())->getTable())->insert([
                        [
                            'revisionable_type' => "App\Models\KTCustomerPortalRegion",
                            'revisionable_id' => $customerportal->ktcupo_id,
                            'key' => $insert_value,
                            'old_value' => 0,
                            'new_value' => 1,
                            'user_id' => \Auth::id(),
                            'created_at' => Carbon::now()->toDateTimeString(),
                            'updated_at' => Carbon::now()->toDateTimeString()
                        ]
                    ]);
                }
            }

           if(!empty($delete))
            {
                foreach($delete as $delete_value)
                {
                    KTCustomerPortalRegion::where("ktcupore_ktcupo_id", $customerportal->ktcupo_id)->where("ktcupore_reg_id", $delete_value)->delete();

                    if($customerportal->ktcupo_cu_id == 18216){continue;}

                    DB::table((new Revision())->getTable())->insert([
                        [
                            'revisionable_type' => "App\Models\KTCustomerPortalRegion",
                            'revisionable_id' => $customerportal->ktcupo_id,
                            'key' => $delete_value,
                            'old_value' => 1,
                            'new_value' => 0,
                            'user_id' => \Auth::id(),
                            'created_at' => Carbon::now()->toDateTimeString(),
                            'updated_at' => Carbon::now()->toDateTimeString()
                        ]
                    ]);
                }
            }


			DB::table('kt_customer_portal_region')->where('ktcupore_ktcupo_id', '=', $customerportal_id)->delete();

			if($request->origins)
			{
				foreach($request->origins as $id => $origin)
				{
					$ktcupore = new KTCustomerPortalRegion();
					$ktcupore->ktcupore_ktcupo_id = $customerportal_id;
					$ktcupore->ktcupore_reg_id = $id;
					$ktcupore->save();
				}
			}


		}
		else if($request->form_name == CustomerPortalForms::DESTINATIONS)
		{
			if($customerportal->portal->po_destination_type == DestinationType::NATMOVING)
			{

                $available_values = DB::table("regions")
                    ->select("reg_id")
                    ->where("reg_destination_type", $customerportal->portal->po_destination_type)
                    ->get();

                $available_values = $system->databaseToArray($available_values);

                //Update these values with the history in mind
                $current_values = DB::table('kt_customer_portal_region_destination')
                    ->where('ktcuporede_ktcupo_id', $customerportal_id)
                    ->get()
                    ->keyBy("ktcuporede_reg_id");

                $current_values = $system->databaseToArray($current_values);

                //Arrays for keeping track
                $insert = [];
                $delete = [];

                //Loop through available values
                foreach($available_values as $available_value)
                {
                    $found_value = $available_value['reg_id'];
                    $current_set = isset($current_values[$found_value]);
                    if(isset($request->destinations[$found_value]))
                    {
                        if(!$current_set)
                        {
                            $insert[$found_value] = $found_value;
                        }
                    }
                    elseif($current_set)
                    {
                        $delete[$found_value] = $found_value;
                    }
                }

                //Execute possible deletes/inserts
                if(!empty($insert))
                {
                    foreach($insert as $insert_value)
                    {
                        if($customerportal->ktcupo_cu_id == 18216){break;}
                        DB::table((new Revision())->getTable())->insert([
                            [
                                'revisionable_type' => "App\Models\KTCustomerPortalRegionDestination",
                                'revisionable_id' => $customerportal->ktcupo_id,
                                'key' => $insert_value,
                                'old_value' => 0,
                                'new_value' => 1,
                                'user_id' => \Auth::id(),
                                'created_at' => Carbon::now()->toDateTimeString(),
                                'updated_at' => Carbon::now()->toDateTimeString()
                            ]
                        ]);
                    }
                }

                if(!empty($delete))
                {
                    foreach($delete as $delete_value)
                    {
                        KTCustomerPortalRegionDestination::where("ktcuporede_ktcupo_id", $customerportal->ktcupo_id)->where("ktcuporede_reg_id", $delete_value)->delete();

                        if($customerportal->ktcupo_cu_id == 18216){continue;}

                        DB::table((new Revision())->getTable())->insert([
                            [
                                'revisionable_type' => "App\Models\KTCustomerPortalRegionDestination",
                                'revisionable_id' => $customerportal->ktcupo_id,
                                'key' => $delete_value,
                                'old_value' => 1,
                                'new_value' => 0,
                                'user_id' => \Auth::id(),
                                'created_at' => Carbon::now()->toDateTimeString(),
                                'updated_at' => Carbon::now()->toDateTimeString()
                            ]
                        ]);
                    }
                }

                DB::table('kt_customer_portal_region_destination')->where('ktcuporede_ktcupo_id', '=', $customerportal_id)->delete();

				if($request->destinations)
				{
					foreach($request->destinations as $id => $destination)
					{
						$ktcuporede = new KTCustomerPortalRegionDestination();

						$ktcuporede->ktcuporede_ktcupo_id = $customerportal_id;
						$ktcuporede->ktcuporede_reg_id = $id;

						$ktcuporede->save();
					}
				}
			}
			else if($customerportal->portal->po_destination_type == DestinationType::INTMOVING)
			{

                $available_values = DB::table("countries")
                    ->select("co_code")
                    ->get();

                $available_values = $system->databaseToArray($available_values);

                //Update these values with the history in mind
                $current_values = DB::table('kt_customer_portal_country')
                    ->where('ktcupoco_ktcupo_id', $customerportal->ktcupo_id)
                    ->get()
                    ->keyBy("ktcupoco_co_code");

                $current_values = $system->databaseToArray($current_values);

                //Arrays for keeping track
                $insert = [];
                $delete = [];

                //Loop through available values
                foreach($available_values as $available_value)
                {
                    $found_value = $available_value['co_code'];
                    $current_set = isset($current_values[$found_value]);
                    if(isset($request->destinations[$found_value]))
                    {
                        if(!$current_set)
                        {
                            $insert[$found_value] = $found_value;
                        }
                    }
                    elseif($current_set)
                    {
                        $delete[$found_value] = $found_value;
                    }
                }

                //Execute possible deletes/inserts
                if(!empty($insert))
                {
                    foreach($insert as $insert_value)
                    {
                        if($customerportal->ktcupo_cu_id == 18216){break;}

                        DB::table((new Revision())->getTable())->insert([
                            [
                                'revisionable_type' => "App\Models\KTCustomerPortalCountry",
                                'revisionable_id' => $customerportal->ktcupo_id,
                                'key' => $insert_value,
                                'old_value' => 0,
                                'new_value' => 1,
                                'user_id' => \Auth::id(),
                                'created_at' => Carbon::now()->toDateTimeString(),
                                'updated_at' => Carbon::now()->toDateTimeString()
                            ]
                        ]);
                    }
                }

               if(!empty($delete))
                {
                    foreach($delete as $delete_value)
                    {
                        KTCustomerPortalCountry::where("ktcupoco_ktcupo_id", $customerportal->ktcupo_id)->where("ktcupoco_co_code", $delete_value)->delete();
                        if($customerportal->ktcupo_cu_id == 18216){continue;}

                        DB::table((new Revision())->getTable())->insert([
                            [
                                'revisionable_type' => "App\Models\KTCustomerPortalCountry",
                                'revisionable_id' => $customerportal->ktcupo_id,
                                'key' => $delete_value,
                                'old_value' => 1,
                                'new_value' => 0,
                                'user_id' => \Auth::id(),
                                'created_at' => Carbon::now()->toDateTimeString(),
                                'updated_at' => Carbon::now()->toDateTimeString()
                            ]
                        ]);
                    }
                }


                DB::table('kt_customer_portal_country')->where('ktcupoco_ktcupo_id', '=', $customerportal_id)->delete();

				if($request->destinations)
				{
					foreach($request->destinations as $co_code => $destination)
					{
						$ktcupoco = new KTCustomerPortalCountry();
						$ktcupoco->ktcupoco_ktcupo_id = $customerportal_id;
						$ktcupoco->ktcupoco_co_code = $co_code;

						$ktcupoco->save();
					}
				}


			}
		}

		if ($customerportal->customer->cu_type == 6)
        {
            return redirect('resellers/'.$customerportal->ktcupo_cu_id.'/customerportal/'.$customerportal_id.'/edit')->with('message', 'Customer Portal is successfully updated!');
        }
		else {
            return redirect('customers/'.$customerportal->ktcupo_cu_id.'/customerportal/'.$customerportal_id.'/edit')->with('message', 'Customer Portal is successfully updated!');
        }

	}

	public function create($customer_id)
	{
		$customer = Customer::findOrFail($customer_id);
		$requesttypes = RequestType::all();

		return view('customerportal.create', [
			'customer' => $customer,
			'portals' => Portal::with("country")->get(),
			'requesttypes' => $requesttypes,
            'portal_types' => PortalType::all()
		]);
	}

	public function store(Request $request)
	{
	    if ($request->portal_type == 1) {
            $request->validate([
                'portal_type' => 'required',
                'portal' => 'required',
                'request_type' => 'required'
            ]);
        }
        else {
            $request->validate([
                'portal_type' => 'required',
                'portal' => 'required',
            ]);
        }

        $system = new System();

        // Validate the request...
		$customer = Customer::findOrFail($request->customer_id);

		//Create new contact person
		$customerportal = new KTCustomerPortal();
		$customerportal->ktcupo_cu_id = $request->customer_id;
		$customerportal->ktcupo_po_id = $request->portal;
		$customerportal->ktcupo_type = $request->portal_type;
		$customerportal->ktcupo_request_type = $request->request_type;
		$customerportal->ktcupo_description = $request->description;

		$requestTypeTomovingSizes = RequestTypeToMovingSizes::all()[$request->request_type];

		foreach($requestTypeTomovingSizes as $moving_size)
		{
			$customerportal['ktcupo_moving_size_'.$moving_size] = 1;
		}

		//calculate daily average
		$customerportal->ktcupo_daily_average = 9999 / 30.5;
		$customerportal->ktcupo_monthly_capping_triglobal = 9999;
		$customerportal->ktcupo_monthly_capping_client = 9999;

		//Set to correct customer and save
		$customerportal->customer()->associate($customer);
		$customerportal->save();

        if($customerportal->portal->po_destination_type == DestinationType::INTMOVING && $request->portal_type == 2)
        {
            //Origins
            $regions_to_insert = Region::where("reg_po_id", $customerportal->portal->po_id)->get();

            foreach ($regions_to_insert as $reg) {
                DB::table("kt_customer_portal_region")
                    ->insert([
                        'ktcupore_ktcupo_id' => $customerportal->ktcupo_id,
                        'ktcupore_reg_id' => $reg->reg_id
                    ]);
            }

            //Destinations
            $available_values = DB::table("countries")
                ->select("co_code")
                ->get();

            foreach ($available_values as $val) {
                DB::table("kt_customer_portal_country")
                    ->insert([
                        'ktcupoco_ktcupo_id' => $customerportal->ktcupo_id,
                        'ktcupoco_co_code' => $val->co_code
                    ]);
            }
        }

        if($customerportal->portal->po_destination_type == DestinationType::NATMOVING && $request->portal_type == 2)
        {
            $available_values = DB::table("regions")
                ->select("reg_id")
                ->where("reg_destination_type", $customerportal->portal->po_destination_type)
                ->where("reg_co_code", $customerportal->portal->po_co_code)
                ->get();

            $available_values = $system->databaseToArray($available_values);

            //Arrays for keeping track
            $insert = [];

            //Loop through available values
            foreach($available_values as $available_value)
            {
                $ktcuporede = new KTCustomerPortalRegionDestination();

                $ktcuporede->ktcuporede_ktcupo_id = $customerportal->ktcupo_id;
                $ktcuporede->ktcuporede_reg_id = $available_value['reg_id'];

                $ktcuporede->save();
            }
        }

		if ($customer->cu_type == 6)
        {
            return redirect('resellers/'.$request->customer_id.'/edit')->with('message', 'Customer Portal is successfully added!');
        }
		else
        {
            return redirect('customers/'.$request->customer_id.'/edit')->with('message', 'Customer Portal is successfully added!');
        }

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param \App\MoverData $moverData
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($office_id)
	{
		$office = CustomerOffice::find($office_id);

		$cu_id = $office->cuof_cu_id;

		$customer = Customer::where("cu_id", $cu_id)->first();

		$office->delete();

		if ($customer->cu_type == 6)
        {
            return redirect('resellers/'.$cu_id."/edit");
        }
		else {
            return redirect('customers/'.$cu_id."/edit");
        }
	}

	public function selectEUCountries()
	{
		$countries = DB::table("countries")
			->select("*")
			->leftJoin("continents", "con_id", "co_con_id")
			->where("in_eu", "=", 1)
			->get();

		echo json_encode($countries);
	}

	public function selectSchengenArea()
	{
		$countries = DB::table("countries")
			->select("*")
			->leftJoin("continents", "con_id", "co_con_id")
			->where("in_schengen_area", "=", 1)
			->get();

		echo json_encode($countries);
	}
}
