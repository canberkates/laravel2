<?php

namespace App\Http\Controllers;

use App\Data\Device;
use App\Data\MatchMistakeType;
use App\Data\MatchRating;
use App\Data\MovingSize;
use App\Data\RejectionReason;
use App\Data\RequestStatus;
use App\Data\RequestType;
use App\Models\Country;
use App\Models\User;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use App\Functions\System;
use Log;

class RequestRateController extends Controller
{
	public function index()
	{
		$statuses = RequestStatus::all();
		unset($statuses[0]);

		$rated_requests = \App\Models\Request::where("re_show_this_feedback", 0)->get();

		return view('requests.rate',
			[
				'users' => User::where("us_is_deleted", 0)->get(),
				'statuses' => $statuses,
				'rated_requests' => $rated_requests,
				'ratings' => MatchRating::all(),

				'selected_status' => [],
				'selected_employee' => [],
				'selected_rating' => [],
				'selected_date' => Carbon::now()->format('Y/m/d') . " - ".Carbon::now()->format('Y/m/d'),

				'requests' => [],
				'portalrequests' => []

			]);
	}

	public function filteredindex(Request $request)
	{
		$portalrequests = [];
		$requests = [];
		$to_be_rated = [];

		if($request->status == RequestStatus::MATCHED){

			$portalrequests = DB::table("requests")
				->leftJoin("kt_request_customer_portal", "re_id", "ktrecupo_re_id")
				->where("re_status", 1)
				->whereBetween( 'ktrecupo_timestamp', System::betweenDates( $request->date ) )
				->where("ktrecupo_rematch", 0);

			if (!empty($request->employee)) {
				$portalrequests->where("ktrecupo_us_id", $request->employee);
			}

			if ($request->rating != "") {
				if ($request->rating == 0)
				{
					$portalrequests->where("re_match_rating", 0);
				}

				if ($request->rating == 1)
				{
					//Good
					$portalrequests->where("re_match_rating", 1);
				}

				if ($request->rating == 2)
				{
					//Minor
					$portalrequests->whereRaw("`re_match_rating` IN (2,4,5,10,12,13,15,16)");
				}

				if ($request->rating == 3)
				{
					//Major
					$portalrequests->whereRaw("`re_match_rating` IN (3,6,7,8,9,11,14,17)");
				}
				if ($request->rating == 4)
				{
					//Remarks
					$portalrequests->whereRaw("`re_match_rating` IN (18)");
				}
			}

			$portalrequests = $portalrequests
				->groupBy("ktrecupo_re_id")
				->orderBy("ktrecupo_timestamp", 'asc')
				->get();

			foreach($portalrequests as $req){
				$req->matchedby = User::where("id",  $req->ktrecupo_us_id)->first()->us_name;
				$req->country_from = Country::where("co_code", $req->re_co_code_from)->first()->co_en;
				$req->country_to = Country::where("co_code",$req->re_co_code_to)->first()->co_en;

				$to_be_rated[] = $req->re_id;
			}

			Log::debug($portalrequests);

		}else{
			$requests = \App\Models\Request::with(["rejectedby", "countryfrom", "countryto"])->where("re_status", 2);

			if( ! empty( $request->date ) ) {
				$requests->whereBetween( 're_timestamp', System::betweenDates( $request->date ) );
			}

			if ( ! empty( $request->employee ) ) {
				$requests->where("re_rejection_us_id",  $request->employee);
			}

			if ($request->rating != "") {
				if ($request->rating == 0)
				{
					$requests->where("re_match_rating", 0);
				}

				if ($request->rating == 1)
				{
					//Good
					$requests->where("re_match_rating", 1);
				}

				if ($request->rating == 2)
				{
					//Minor
					$requests->whereRaw("`re_match_rating` IN (2,4,5,10,12,13,15,16)");
				}

				if ($request->rating == 3)
				{
					//Major
					$requests->whereRaw("`re_match_rating` IN (3,6,7,8,9,11,14,17)");
				}
				if ($request->rating == 4)
				{
					//Remarks
					$requests->whereRaw("`re_match_rating` IN (18)");
				}
			}

			$requests = $requests->get();

			foreach($requests as $req){
				$to_be_rated[] = $req->re_id;
			}
		}

		$statuses = RequestStatus::all();
		unset($statuses[0]);


		return view('requests.rate',
			[
				'users' => User::where("us_is_deleted", 0)->get(),
				'ratings' => MatchRating::all(),
				'requesttypes' => RequestType::all(),
				'devices' => Device::all(),
				'matchmistaketypes' => MatchMistakeType::all(),
				'rejectionreasons' => RejectionReason::all(),
				'movingsizes' => MovingSize::all(),
				'statuses' => $statuses,

				'selected_status' => ($request->status ?? []),
				'selected_employee' =>  ($request->employee ?? []),
				'selected_rating' =>  ($request->rating ?? []),
				'selected_date' => ($request->date ?? ""),

				'portalrequests' => $portalrequests,
				'requests' => $requests,
				'toberated' => implode(",", $to_be_rated)
			]);
	}

    public function syncFeedback()
    {
        DB::table('requests')
            ->where("re_show_this_feedback", 0)
            ->update(
                [
                    're_show_this_feedback' => 1
                ]
            );

        return redirect("/requests/rate/");
    }
}
