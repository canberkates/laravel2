<?php

namespace App\Http\Controllers;

use App\Data\ClaimReason;
use App\Data\ContactPersonDepartment;
use App\Data\ContactPersonDepartmentRole;
use App\Data\CustomerDocumentType;
use App\Data\CustomerPairStatus;
use App\Data\CustomerRemarkDepartment;
use App\Data\CustomerRemarkDirection;
use App\Data\CustomerRemarkMedium;
use App\Data\CustomerStatusReason;
use App\Data\CustomerStatusSource;
use App\Data\CustomerStatusStatus;
use App\Data\EmptyYesNo;
use App\Data\InvoicePeriod;
use App\Data\ISO;
use App\Data\MoverCappingMethod;
use App\Data\MovingSize;
use App\Data\PaymentReminder;
use App\Data\QualityScoreOverride;
use App\Data\RequestCustomerPortalType;
use App\Data\RequestType;
use App\Data\Survey1Contacted;
use App\Data\Survey2Source;
use App\Data\CustomerEditForms;
use App\Data\CustomerType;
use App\Data\DebtorStatus;
use App\Data\PaymentMethod;
use App\Data\PaymentReminderStatus;
use App\Data\TypeOfMover;
use App\Data\YesNo;
use App\Functions\Mover;
use App\Functions\Sirelo;
use App\Functions\SireloCustomer;
use App\Functions\System;
use App\Models\AdyenCardDetails;
use App\Models\AffiliatePartnerData;
use App\Models\Cache;
use App\Models\Country;
use App\Models\Customer;
use App\Models\CustomerChange;
use App\Models\CustomerDocument;
use App\Models\CustomerStatus;
use App\Models\Invoice;
use App\Models\KTBankLineInvoiceCustomerLedgerAccount;
use App\Models\KTCustomerMembership;
use App\Models\KTCustomerObligation;
use App\Models\KTRequestCustomerPortal;
use App\Models\Language;
use App\Models\LedgerAccount;
use App\Models\Membership;
use App\Models\MoverData;
use App\Models\MoverPermit;
use App\Models\Obligation;
use App\Models\PaymentCurrency;
use App\Models\Survey2;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Log;
use Venturecraft\Revisionable\Revision;


class ResellersController extends Controller
{
    public function index()
    {
        $system = new System();
        $customer_changes = CustomerChange::where([["cuch_approve_deny", "=", "0"]])->get();

        $cache = Cache::where("ca_name", "=", "customer_count")->first();

        $cached_count = $system->unserialize($cache->ca_value);
        $last_timestamp_updated = $cache->ca_timestamp;

        return view('resellers.index', [
            //'customers' => $customers,
            'customer_changes_count' => count($customer_changes),
            'cached_count' => $cached_count,
            'last_timestamp_updated' => $last_timestamp_updated
        ]);
    }

    public function prospectindex()
    {
        $system = new System();
        $cache = Cache::where("ca_name", "=", "customer_count")->first();

        $cached_count = $system->unserialize($cache->ca_value);

        return view('resellers.prospects.index', [
            //'customers' => $customers,
            'cached_count' => $cached_count
        ]);
    }

    public function edit($id, $request_date_filter = null, $invoice_paid_filter = null, $surveys2_filter = null)
    {
        // get the customer and relations
        $customer = Customer::with(['customerportals.portal', 'statuses.user', 'remarks.user', 'remarks.status', 'documents.user', 'contactpersons.application_user'])->findOrFail($id);

        //Check if collegue has the right URL
        $url_check = System::customerTypeToURL($customer->cu_id, $customer->cu_type);

        if ($url_check['redirect'] == true)
        {
            return redirect($url_check['url']);
        }

        //Show different view for deleted customers
        if ($customer->cu_deleted == 1)
        {
            return view('resellers.deleted',
                [
                    'customer_id' => $customer->cu_id
                ]);
        }

        //Create useful objects for methods
        $sireloCustomer = new SireloCustomer;
        $sirelo = new Sirelo;
        $system = new System;
        $mover = new Mover;
        $claim_rates_90_special_agreements = "";
        $claim_rates_60_special_agreements = "";
        $claim_rates_30_special_agreements = "";

        //Create 2 dropdown with different stepsizes
        $claim_dropdown = $system->dropdownOptions(0, 100, 5);
        $no_claim_dropdown = $system->dropdownOptions(0, 30, 5);

        //Calculate past claimrates
        $claim_rates[90] = $mover->calcReclamationRate(90, $id);
        $claim_rates[60] = $mover->calcReclamationRate(60, $id);
        $claim_rates[30] = $mover->calcReclamationRate(30, $id);

        foreach([90,60,30] as $i)
        {
            if ($claim_rates[$i]['total_claimed_requests'] > 0)
            {

                $one_percent = $claim_rates[$i]['requests'] / 100.0;
                $claimed_percentage = $claim_rates[$i]['special_agreement_claims'] / $one_percent;

                //$claim_rates[$i]['special_agreement_claims']

                //$claim_rates_{$days}_special_agreements = number_format($claimed_percentage, 2, ".","")."%" ;
                ${"claim_rates_".$i."_special_agreements"} = "Special Agreements: ".number_format($claimed_percentage, 2, ".","")."% (".$claim_rates[$i]['special_agreement_claims']." claimed out of ".$claim_rates[$i]['requests']." requests)";
            }
        }

        //Get credits now and in 2 weeks
        $amount_of_credits['now'] = $mover->getAmountOfCredits($id, "NOW()");
        $amount_of_credits['2weeks'] = $mover->getAmountOfCredits($id, "NOW() + INTERVAL 14 DAY");

        //Seperate portals by status
        $portals['active'] = $customer->customerportals->where("ktcupo_status", "=", 1);
        $portals['paused'] = $customer->customerportals->where("ktcupo_status", "=", 2);
        $portals['inactive'] = $customer->customerportals->where("ktcupo_status", "=", 0);

        $request_deliveries = DB::table("kt_customer_portal")
            ->rightJoin("request_deliveries",  "ktcupo_id" , "rede_ktcupo_id")
            ->where("ktcupo_cu_id", $id)
            ->get();

        //Get all reviews by this mover
        $reviews_1 = null;
        $reviews_2 = null;

        if ($surveys2_filter != null)
        {
            $published = $surveys2_filter['published'];



            if (!empty($surveys2_filter['date']) && $surveys2_filter['date'] != null && ($published == "0" || $published == "1"))
            {
                $reviews_1 = DB::table("surveys_1")
                    ->leftJoin("survey_1_customers", "su_id", "sucu_su_id")
                    ->leftJoin("requests", "re_id", "su_re_id")
                    ->whereBetween("su_timestamp", [date("Y-m-d H:i:s", strtotime($surveys2_filter['date'][0])), date("Y-m-d H:i:s", strtotime($surveys2_filter['date'][1]))])
                    ->where("sucu_cu_id", "=", $id)
                    ->get();

                $reviews_2 = Survey2::with(['request', 'website_review'])
                    ->where("su_mover", "=", $id)
                    ->whereBetween("su_timestamp", [date("Y-m-d H:i:s", strtotime($surveys2_filter['date'][0])), date("Y-m-d H:i:s", strtotime($surveys2_filter['date'][1]))])
                    ->where("su_show_on_website", "=", $published)
                    ->get();


            } elseif (!empty($surveys2_filter['date']) && $surveys2_filter['date'] != null)
            {
                $reviews_1 = DB::table("surveys_1")
                    ->leftJoin("survey_1_customers", "su_id", "sucu_su_id")
                    ->leftJoin("requests", "re_id", "su_re_id")
                    ->whereBetween("su_timestamp", [date("Y-m-d H:i:s", strtotime($surveys2_filter['date'][0])), date("Y-m-d H:i:s", strtotime($surveys2_filter['date'][1]))])
                    ->where("sucu_cu_id", "=", $id)
                    ->get();

                $reviews_2 = Survey2::with(['request', 'website_review'])
                    ->where("su_mover", "=", $id)
                    ->whereBetween("su_timestamp", [date("Y-m-d H:i:s", strtotime($surveys2_filter['date'][0])), date("Y-m-d H:i:s", strtotime($surveys2_filter['date'][1]))])
                    ->get();

            } else
            {

                $reviews_1 = DB::table("surveys_1")
                    ->leftJoin("survey_1_customers", "su_id", "sucu_su_id")
                    ->leftJoin("requests", "re_id", "su_re_id")
                    ->where("sucu_cu_id", "=", $id)
                    ->get();

                if ($surveys2_filter['published'] == "both")
                {
                    $reviews_2 = Survey2::with(['request', 'website_review'])
                        ->where("su_mover", "=", $id)
                        ->get();
                }
                else
                {
                    $reviews_2 = Survey2::with(['request', 'website_review'])
                        ->where("su_mover", "=", $id)
                        ->where("su_show_on_website", "=", $published)
                        ->get();
                }
            }


            $survey_date_filter_data = null;

            if (!empty($surveys2_filter['date']))
                $survey_date_filter_data = date("Y/m/d", strtotime($surveys2_filter['date'][0])) . " - " . date("Y/m/d", strtotime($surveys2_filter['date'][1]));

            $surveys2_filter = [
                "published" => $published,
                "date" => $survey_date_filter_data
            ];
        } else
        {
            $surveys2_filter = [
                "published" => "",
                "date" => ""
            ];
        }

        $revisionslist = [];
        foreach( $customer->customerportals as $portall){
            $revisionslist[$portall->ktcupo_id] = $portall->ktcupo_id;
        }

        //Get all credit cards
        $creditcards = AdyenCardDetails::where("adcade_cu_id", "=", $id)->where("adcade_removed", "=", 0)->orderBy("adcade_enabled", "desc")->get();

        $this->getContactPersonLogin($customer);
        $memberships = $this->getCustomerMemberships($customer);

        $customer_rev_types = ["App\\Models\\Customer", "App\\Models\\MoverData", "App\\Models\\KTCustomerMembership"];
        $portal_rev_types = ["App\Models\KTCustomerPortal"];

        $customer_revisions = Revision::whereIn("revisionable_type", $customer_rev_types)->where("revisionable_id", "=", $customer->cu_id)->get();
        $portal_revisions = Revision::whereIn("revisionable_type", $portal_rev_types)->whereIn("revisionable_id", $revisionslist)->get();

        //$customerportalrequests = KTRequestCustomerPortal::with(["request.countryfrom", "request.countryto", "portal"])->where("ktrecupo_cu_id", $id)->orderBy("ktrecupo_cu_re_id", 'desc')->limit(90)->get();
        $documents = CustomerDocument::where("cudo_archived", "=", 0)->where("cudo_cu_id", "=", $customer->cu_id)->get();
        $documents_archived = CustomerDocument::where("cudo_archived", "=", 1)->where("cudo_cu_id", "=", $customer->cu_id)->get();

        $customerportalrequests = null;
        $request_date_filter_data = null;

        if ($request_date_filter != null)
        {
            $customerportalrequests = KTRequestCustomerPortal::with(["request.countryfrom", "request.countryto", "portal"])->where("ktrecupo_cu_id", $id)->whereBetween("ktrecupo_timestamp", [date("Y-m-d H:i:s", strtotime($request_date_filter[0])), date("Y-m-d H:i:s", strtotime($request_date_filter[1]))])->orderBy("ktrecupo_cu_re_id", 'desc')->get();

            $request_date_filter_data = date("Y/m/d", strtotime($request_date_filter[0])) . " - " . date("Y/m/d", strtotime($request_date_filter[1]));
        }

        $int_moverpermits = [];
        $nat_moverpermits = [];

        $mover_permits = MoverPermit::all();

        foreach ($mover_permits as $mp)
        {
            if ($mp->mope_national_permit)
            {
                //National permit
                $nat_moverpermits[$mp->mope_co_code] = $mp->mope_name;
            }

            if ($mp->mope_international_permit)
            {
                //International permit
                $int_moverpermits[$mp->mope_co_code] = $mp->mope_name;
            }
        }

        $invoices = Invoice::where("in_cu_id", "=", $customer->cu_id)->get();
        $amount_paired_per_invoice = [];
        $payment_dates_per_invoice = [];

        $invoice_totals['total_amount_gross'] = 0;
        $invoice_totals['total_amount_netto'] = 0;
        $invoice_totals['total_amount_paid'] = 0;

        foreach ($invoices as $invoice)
        {
            $bank_lines = KTBankLineInvoiceCustomerLedgerAccount::leftJoin("bank_lines", "ktbaliinculeac_bali_id", "bali_id")
                ->where("ktbaliinculeac_in_id", "=", $invoice->in_id)
                ->get();

            $amount_paired_per_invoice[$invoice->in_id] = 0;
            $payment_dates_per_invoice[$invoice->in_id] = [];

            foreach ($bank_lines as $bali)
            {
                $amount_paired_per_invoice[$invoice->in_id] += $bali->ktbaliinculeac_amount;

                $payment_dates_per_invoice[$invoice->in_id][] = (($bali->bali_date == 0) ? $bali->ktbaliinculeac_date : $bali->bali_date);
            }

            $invoice_totals['total_amount_gross'] += $invoice->in_amount_gross_eur;
            $invoice_totals['total_amount_netto'] += $invoice->in_amount_netto_eur;
            $invoice_totals['total_amount_paid'] += $amount_paired_per_invoice[$invoice->in_id];

            $invoice->days_overdue = $system->dateDifference($invoice->in_expiration_date, date("Y-m-d"));
        }
        $currency_tokens = [];

        $currencies = PaymentCurrency::all();

        foreach ($currencies as $currency)
        {
            $currency_tokens[$currency->pacu_code] = $currency->pacu_token;
        }

        $customer_statuses = CustomerStatus::where("cust_cu_id", "=", $customer->cu_id)->get();

        $customer_status_sources = CustomerStatusSource::all();
        $customer_status_reasons = CustomerStatusReason::all();

        $customer_status_expand = [];

        if ($customer->moverdata->moda_checked_sirelo){
            $sirelo_bucket_check = User::where("id", $customer->moverdata->moda_checked_sirelo)->first()->us_name;
        }else{
            $sirelo_bucket_check = "";
        }

        foreach ($customer_statuses as $cs)
        {

            $extra_information_table = '<table class="table">';

            $colWidth = '160';

            $extra_information_table .= "
				<tr>
					<td width='" . $colWidth . "'><b>Number of leads:</b></td>
					<td>" . $cs->cust_number_of_leads . "</td>
				</tr>
				<tr>
					<td width='" . $colWidth . "'><b>Capping of leads:</b></td>
					<td>" . $cs->cust_capping_of_leads . "</td>
				</tr>
				<tr>
					<td width='" . $colWidth . "'><b>Price per lead:</b></td>
					<td>" . $cs->cust_price_per_lead . "</td>
				</tr>
			";

            if ($cs->cust_status == 1 || $cs->cust_status == 3)
            {
                $extra_information_table .= "
					<tr>
						<td width='" . $colWidth . "'><b>Booking source:</b></td>
						<td>" . ($cs->cust_booking_source != 0 ? $customer_status_sources[$cs->cust_booking_source] : "") . "</td>
					</tr>
				";
            }

            if ($cs->cust_status == 2 || $cs->cust_status == 9)
            {
                $extra_information_table .= "
					<tr>
						<td width='" . $colWidth . "'><b>Previous amount of leads:</b></td>
						<td>" . $cs->cust_previous_leads . "</td>
					</tr>
                    <tr>
						<td width='" . $colWidth . "'><b>Previous amount of capping:</b></td>
						<td>" . $cs->cust_previous_capping . "</td>
					</tr>
                    <tr>
						<td width='" . $colWidth . "'><b>Previous price per lead:</b></td>
						<td>" . $cs->cust_previous_price . "</td>
					</tr>
				";
            }

            if ($cs->cust_status == 4 || $cs->cust_status == 5 || $cs->cust_status == 8)
            {
                $extra_information_table .= "
					<tr>
						<td width='" . $colWidth . "'><b>Reason:</b></td>
						<td>" . ($cs->cust_reason != 0 ? $customer_status_reasons[$cs->cust_reason] : "") . "</td>
					</tr>
				";
            }

            if ($cs->cust_status == 8)
            {
                $extra_information_table .= "
					<tr>
						<td width='" . $colWidth . "'><b>Lifetime (in months):</b></td>
						<td>" . $cs->cust_lifetime . "</td>
					</tr>
				";
            }

            $extra_information_table .= '</table>';


            $details = str_replace("\"", "'", $extra_information_table);

            $customer_status_expand[$cs->cust_id] = $details;
        }

        $results = [];
        $chart_labels = [];

        for($i = 60; $i >= 0; $i--)
        {
            $chart_labels[] = date("Y-m-d", strtotime("-".$i." days"));
            $results[date("Y-m-d", strtotime("-".$i." days"))] = 0;
        }

        $requests = DB::table("requests")
            ->leftJoin("kt_request_customer_portal", "re_id", "ktrecupo_re_id")
            ->leftJoin("portals", "ktrecupo_po_id", "po_id")
            ->where("ktrecupo_cu_id", $customer->cu_id)
            ->whereBetween("ktrecupo_timestamp", [date("Y-m-d H:i:s", strtotime(date("Y-m-d", strtotime("-60 days"))." 00:00:00")), date("Y-m-d H:i:s", strtotime(date("Y-m-d", strtotime("-0 days"))." 23:59:59"))])
            ->orderBy("ktrecupo_timestamp", 'asc')
            ->get();

        $customerstatus = $system->getStatus($customer->cu_id);

        foreach($requests as $row_requests)
        {
            $results[date("Y-m-d", strtotime($row_requests->ktrecupo_timestamp))]++;
        }

        $obligations = Obligation::where("cuob_co_code", $customer->cu_co_code)->get();

        $permits_obligations_verified = [];

        $customer_obligations = KTCustomerObligation::where("ktcuob_cu_id", $customer->cu_id)->get();

        foreach ($customer_obligations as $co)
        {
            $permits_obligations_verified[$co->ktcuob_cuob_id] = [
                'verified' => $co->ktcuob_verification_result,
                'verified_timestamp' => date("Y-m-d", strtotime($co->ktcuob_verified_timestamp))
            ];
        }

        return view('resellers.edit',
            [
                //Customer info
                'customer' => $customer,
                'statuses' => $customer_statuses,
                'customer_status_expand' => $customer_status_expand,
                'remarks' => $customer->remarks,
                'documents' => $documents,
                'documents_archived' => $documents_archived,
                'offices' => $customer->offices,
                'portals' => $portals,
                'history' => $customer_revisions,
                'portal_history' => $portal_revisions,
                'chart_labels' => json_encode($chart_labels),
                'chart_data' => json_encode(array_values($results)),

                //Moverinfo
                'sirelopage' => $sireloCustomer->getPage($id),
                'sirelorating' => $sireloCustomer->getReviews($id),
                'sirelourl' => $sirelo->getSireloFieldByCountry($customer->cu_co_code, "we_sirelo_customer_location"),
                'moverdata' => $customer->moverdata,
                'claim_dropdown' => $claim_dropdown,
                'no_claim_dropdown' => $no_claim_dropdown,
                'claim_rates' => $claim_rates,
                'credits' => $amount_of_credits,
                'memberships' => $memberships,
                'memberships_all' => Membership::all(),
                'surveys2' => $reviews_2,
                'surveys1' => $reviews_1,
                'contacted' => Survey1Contacted::all(),
                'emptyyesno' => EmptyYesNo::all(),
                'yesno' => YesNo::all(),
                'movingsizes' => MovingSize::all(),
                'invoices' => $invoices,
                'invoice_totals' => $invoice_totals,
                'customerportalrequests' => $customerportalrequests,
                'creditcards' => $creditcards,
                'customerstatus' => $customerstatus,
                'hasdeliveries' => count($request_deliveries),

                //Lists
                'users' => User::where("id", "!=", 0)->orderBy("us_name")->get(),
                'claimreasons' => ClaimReason::all(),
                'countries' => Country::all(),
                'movertypes' => CustomerType::all(),
                'debtorstatuses' => DebtorStatus::all(),
                'paymentreminderstatuses' => PaymentReminderStatus::all(),
                'paymentreminder' => PaymentReminder::all(),
                'paymentmethods' => PaymentMethod::all(),
                'paymentcurrencies' => PaymentCurrency::all(),
                'ledgeraccounts' => LedgerAccount::all(),
                'languages' => Language::where("la_iframe_only", 0)->get(),
                'contactpersondepartments' => ContactPersonDepartment::all(),
                'contactpersondepartmentroles' => ContactPersonDepartmentRole::all(),
                'qualityscoreoverrides' => QualityScoreOverride::all(),
                'cappingmethod' => MoverCappingMethod::all(),
                'requesttypes' => RequestType::all(),
                'customerstatusstatuses' => CustomerStatusStatus::all(),
                'customerstatusreasons' => CustomerStatusReason::all(),
                'customerremarkdepartments' => CustomerRemarkDepartment::all(),
                'customerremarkmediums' => CustomerRemarkMedium::all(),
                'customerremarkdirections' => CustomerRemarkDirection::all(),
                'customerdocumenttypes' => CustomerDocumentType::all(),
                'survey2sources' => Survey2Source::all(),
                'portalstatuses' => CustomerPairStatus::all(),
                'typeofmovers' => TypeOfMover::all(),
                'requestcustomerportaltypes' => RequestCustomerPortalType::all(),
                'invoiceperiods' => InvoicePeriod::all(),
                'moverpermits' => MoverPermit::all(),
                "intmoverpermits" => $int_moverpermits,
                "natmoverpermits" => $nat_moverpermits,
                "request_date_filter" => $request_date_filter_data,
                "invoices_paid_filter" => $invoice_paid_filter,
                "amount_paired_per_invoice" => $amount_paired_per_invoice,
                "payment_dates_per_invoice" => $payment_dates_per_invoice,
                "currency_tokens" => $currency_tokens,
                "surveys2_filter" => $surveys2_filter,
                "sirelo_bucked_user" => $sirelo_bucket_check,
                "iso" => ISO::all(),

                //Claim rates
                'claim_rates_90_special_agreements' => $claim_rates_90_special_agreements,
                'claim_rates_60_special_agreements' => $claim_rates_60_special_agreements,
                'claim_rates_30_special_agreements' => $claim_rates_30_special_agreements,

                'obligations' => $obligations,

                'permits_obligations_verified' => $permits_obligations_verified
            ]);
    }

    public function update(Request $request, $id)
    {
        $customer = Customer::find($id);

        //If General form is posted
        if ($request->form_name == CustomerEditForms::GENERAL)
        {
            $request->validate([
                'la_code' => 'required',
                'company_name_business' => 'required',
                'company_name_legal' => 'required',
            ]);

            $customer->cu_company_name_business = $request->company_name_business;
            $customer->cu_company_name_legal = $request->company_name_legal;
            $customer->cu_la_code = $request->la_code;
            $customer->cu_email = $request->lead_email;
            $customer->cu_email_bi = $request->billing_email;

            if ((empty($customer->cu_description_la_code) || $customer->cu_description != $request->description) && !empty($request->description)) {
                $description_la_code = System::getGoogleTranslationLanguage($request->description);

                if (!empty($description_la_code)) {
                    $customer->cu_description_la_code = $description_la_code;
                }
            }

            $customer->cu_description = $request->description;
            $customer->cu_attn = $request->attn;
            $customer->cu_attn_email = $request->attn_email;
            $customer->cu_coc = $request->coc;

            //if a new logo was uploaded
            if ($request->logo)
            {
                $allowed_types = ['image/jpeg', 'image/png'];

                $image_size = getimagesize($_FILES['logo']['tmp_name']);
                $image_width = $image_size[0];
                $image_height = $image_size[1];

                if ($_FILES['logo']['size'] < 1)
                {
                    return redirect()->back()->withErrors(['logo' => 'The logo has not been saved, because the file is empty.']);
                } elseif ($_FILES['logo']['size'] > 51200)
                {
                    return redirect()->back()->withErrors(['logo' => 'The logo has not been saved, because the file is bigger than 10MB.']);
                } elseif (!in_array($_FILES['logo']['type'], $allowed_types))
                {
                    return redirect()->back()->withErrors(['logo' => 'The logo has not been saved, because the file type is not valid.']);
                } elseif ($image_width < 100 || $image_height < 50)
                {
                    return redirect()->back()->withErrors(['logo' => 'The logo has not been saved, because the image is too small. The image has to be min. 100 pixels wide and 50 pixels high.']);
                } elseif ($image_width > 600 || $image_height > 200)
                {
                    return redirect()->back()->withErrors(['logo' => 'The logo has not been saved, because the image is too big. The image has to be max. 600 pixels wide and 200 pixels high.']);
                }

                $system = new System();

                // The old logo name
                $old_logo_name = $customer->cu_logo;

                // The new logo file
                $new_logo = $request->file('logo');

                // Create the new logo name
                $new_logo_name = $system->hashLogoName($id, $new_logo->getClientOriginalExtension());

                // If the mover had a logo already
                if (!empty($old_logo_name))
                {

                    // Build path to old logo
                    $path_to_old_logo = config("system.paths.shared") . 'uploads/logos/' . $old_logo_name;

                    // Check if the file exists
                    if (file_exists($path_to_old_logo))
                    {

                        // Destroy the old logo from our erp
                        unlink($path_to_old_logo);
                    }
                }

                // Build new file location
                $file_location = config("system.paths.shared") . 'uploads/logos/';

                // Place the logo on our erp
                $request->file('logo')->move($file_location, $new_logo_name);

                // Update the logo name in the db
                $customer->cu_logo = $new_logo_name;
            }

            $customer->save();

        } else if ($request->form_name == CustomerEditForms::ADDRESSES)
        {
            $request->validate([
                'street_1' => 'required',
                'city' => 'required',
                'country' => 'required'
            ]);

            if(($request->int_telephone != 0 || empty($request->telephone)) && ($request->int_telephone_bi != 0 || empty($request->telephone_bi)))
            {
                $customer->cu_telephone = $request->int_telephone;
            }

            $customer->cu_street_1 = $request->street_1;
            $customer->cu_street_2 = $request->street_2;
            $customer->cu_zipcode = $request->zipcode;
            $customer->cu_city = $request->city;
            $customer->cu_co_code = $request->country;

            $website_url = $request->website;
            if (!empty($website_url)){
                if (strpos($website_url, 'http') === false) {
                    $website_url = "http://".$website_url;
                }
            }

            $customer->cu_website = $website_url;

            $customer->cu_use_billing_address = ($request->use_billing_address === 'on');

            if ($request->use_billing_address === 'on')
            {
                $request->validate([
                    'street_1_bi' => 'required',
                    'city_bi' => 'required',
                    'country_bi' => 'required'
                ]);

                $customer->cu_street_1_bi = $request->street_1_bi;
                $customer->cu_street_2_bi = $request->street_2_bi;
                $customer->cu_zipcode_bi = $request->zipcode_bi;
                $customer->cu_city_bi = $request->city_bi;
                $customer->cu_telephone_bi = $request->int_telephone_bi;
                $customer->cu_co_code_bi = $request->country_bi;

                $website_url = $request->website_bi;
                if (!empty($website_url)){
                    if (strpos($website_url, 'http') === false) {
                        $website_url = "http://".$website_url;
                    }
                }
                $customer->cu_website_bi = $website_url;
            }

            $customer->save();
        } else if ($request->form_name == CustomerEditForms::FINANCE)
        {
            $old_credit_hold_value = $customer->cu_credit_hold;

            $customer->cu_sales_manager = $request->sales_manager;
            $customer->cu_account_manager = $request->account_manager;
            $customer->cu_debt_manager = $request->debt_manager;
            $customer->cu_payment_reminder_status = $request->payment_reminder_status;
            $customer->cu_debtor_status = $request->debtor_status;
            $customer->cu_payment_method = $request->payment_method;
            $customer->cu_skip_for_overdue_cronjob = ($request->skip_for_overdue_cronjob === 'on');

            if (!empty($request->credit_limit))
            {
                $credit_limit = str_replace(",", ".", $request->credit_limit);

                if ($credit_limit < 0)
                {
                    $credit_limit = $credit_limit * -1;
                }
            } else
            {
                $credit_limit = 0;
            }

            //Default set credit limit = 0 when customer is prepayment (also credit limit field will be disabled)
            if ($request->payment_method == 4)
            {
                $credit_limit = 0;
            }

            $customer->cu_credit_limit = $credit_limit;

            $customer->cu_pacu_code = $request->payment_currency;
            $customer->cu_payment_term = $request->payment_term;
            $customer->cu_leac_number = $request->ledger_account;
            $customer->cu_vat_number = $request->vat_number;
            $customer->cu_finance_remarks = $request->finance_remark;
            $customer->cu_invoice_period = $request->invoice_period;

            $customer->cu_debt_collector = (($request->debt_collector === 'on' && $request->credit_hold_hidden == 1) ? 1 : 0);
            $customer->cu_credit_hold = ($request->credit_hold_hidden == 1);
            $customer->cu_finance_lock = ($request->finance_lock === 'on');

            if ($old_credit_hold_value == 0 && $request->credit_hold_hidden == 1)
            {
                $customer->cu_credit_hold_timestamp = date("Y-m-d H:i:s");
            }

            $customer->save();
        }
        else if ($request->form_name == CustomerEditForms::AUTO_DEBIT)
        {
            $customer->cu_auto_debit_name = $request->auto_debit_name;
            $customer->cu_auto_debit_city = $request->auto_debit_city;
            $customer->cu_auto_debit_co_code = $request->auto_debit_co_code;
            $customer->cu_auto_debit_iban = $request->auto_debit_iban;
            $customer->cu_auto_debit_bic = $request->auto_debit_bic;

            $customer->save();
        }
        else if ($request->form_name == CustomerEditForms::SIRELO)
        {
            $moverdata = MoverData::query()->where('moda_cu_id', $id)->get()[0];

            //Sirelo checkboxes
            $moverdata->moda_disable_sirelo_export = ($request->disable_sirelo_export === 'on');
            $moverdata->moda_sirelo_disable_top_mover = ($request->disable_sirelo_top_mover === 'on');
            $moverdata->moda_sirelo_disable_request_form = ($request->disable_request_form === 'on');
            $moverdata->moda_sirelo_review_verification = ($request->sirelo_review_verification === 'on');

            //Sirelofields
            $moverdata->moda_sirelo_widget_location = $request->widget_location;
            $moverdata->moda_review_email = $request->review_communication_email;
            $moverdata->moda_info_email = $request->general_info_email;
            $moverdata->moda_contact_email = $request->contact_email;
            $moverdata->moda_contact_telephone = $request->contact_telephone;

            //Movetypes
            $moverdata->moda_international_moves = ($request->international_moves === 'on');
            $moverdata->moda_national_moves = ($request->national_moves === 'on');
            $moverdata->moda_excess_baggage = ($request->excess_baggage === 'on');
            $moverdata->moda_man_and_van = ($request->man_and_van === 'on');
            $moverdata->moda_car_and_vehicle_transport = ($request->car_and_vehicle_transport === 'on');
            $moverdata->moda_piano_transport = ($request->piano_transport === 'on');
            $moverdata->moda_pet_transport = ($request->pet_transport === 'on');
            $moverdata->moda_art_transport = ($request->art_transport === 'on');

            //Statistics
            $moverdata->moda_established = $request->established;
            $moverdata->moda_employees = $request->employees;
            $moverdata->moda_trucks = $request->trucks;

            //Checked for sirelo
            if($request->sirelo_bucket === 'on')
            {
                $moverdata->moda_checked_sirelo = \Auth::id();
                $moverdata->moda_checked_timestamp = Carbon::now()->toDateTimeString();
            }else{
                $moverdata->moda_checked_sirelo = null;
                $moverdata->moda_checked_timestamp = null;
            }


            //Customer fields
            $customer->cu_type_of_mover = $request->type_of_mover;
            $customer->cu_public_liability_insurance = ($request->public_liability === 'on');
            $customer->cu_goods_in_transit_insurance = ($request->goods_in_transit === 'on');
            $customer->cu_public_liability_insurance_value = (($request->public_liability === 'on') ? $request->public_liability_value : "");
            $customer->cu_goods_in_transit_insurance_value = (($request->goods_in_transit === 'on') ? $request->goods_in_transit_value : "");
            $customer->cu_international_permit = ($request->international_permit === 'on');
            $customer->cu_national_permit = ($request->national_permit === 'on');



            //Save Customer and Mover Data
            $customer->save();
            $moverdata->save();
        } else if ($request->form_name == CustomerEditForms::MOVER_PORTAL)
        {
            $moverdata = MoverData::query()->where('moda_cu_id', $id)->get()[0];

            $moverdata->moda_hide_lead_partner_functions = ($request->moda_hide_lead_partner_functions === 'on');
            $moverdata->moda_hide_response_chart = ($request->moda_hide_response_chart === 'on');

            $moverdata->moda_activate_lead_pick = ($request->moda_activate_lead_pick === 'on');
            $moverdata->moda_lead_pick_import = ($request->moda_lead_pick_import === 'on');

            $moverdata->moda_activate_load_exchange = ($request->moda_activate_load_exchange === 'on');
            $moverdata->moda_load_exchange_email = $request->moda_load_exchange_email;

            $moverdata->moda_activate_premium_leads = ($request->moda_activate_premium_leads === 'on');

            $moverdata->moda_premium_profile_summary = $request->moda_premium_profile_summary;
            //$moverdata->moda_iso = $request->moda_iso;
            $moverdata->moda_premium_profile_payment_terms = $request->moda_premium_profile_payment_terms;

            //iso
            if(isset($request->iso) && in_array("moda_quality_management_iso", $request->iso))
            {
                $moverdata->moda_quality_management_iso = 1;
            }
            else
            {
                $moverdata->moda_quality_management_iso = 0;
            }

            if(isset($request->iso) && in_array("moda_environmental_management_iso", $request->iso))
            {
                $moverdata->moda_environmental_management_iso = 1;
            }
            else
            {
                $moverdata->moda_environmental_management_iso = 0;
            }

            if(isset($request->iso) && in_array("moda_occupational_health_and_safety_iso", $request->iso))
            {
                $moverdata->moda_occupational_health_and_safety_iso = 1;
            }
            else
            {
                $moverdata->moda_occupational_health_and_safety_iso = 0;
            }

            if(isset($request->iso) && in_array("moda_information_security_management_iso", $request->iso))
            {
                $moverdata->moda_information_security_management_iso = 1;
            }
            else
            {
                $moverdata->moda_information_security_management_iso = 0;
            }

            //payment methods
            if(isset($request->payment_methods) && in_array("moda_payment_method_cash", $request->payment_methods))
            {
                $moverdata->moda_payment_method_cash = 1;
            }
            else
            {
                $moverdata->moda_payment_method_cash = 0;
            }

            if(isset($request->payment_methods) && in_array("moda_payment_method_cheque", $request->payment_methods))
            {
                $moverdata->moda_payment_method_cheque = 1;
            }
            else
            {
                $moverdata->moda_payment_method_cheque = 0;
            }
            if(isset($request->payment_methods) && in_array("moda_payment_method_bank_transfer", $request->payment_methods))
            {
                $moverdata->moda_payment_method_bank_transfer = 1;
            }
            else
            {
                $moverdata->moda_payment_method_bank_transfer = 0;
            }
            if(isset($request->payment_methods) && in_array("moda_payment_method_auto_debit", $request->payment_methods))
            {
                $moverdata->moda_payment_method_auto_debit = 1;
            }
            else
            {
                $moverdata->moda_payment_method_auto_debit = 0;
            }
            if(isset($request->payment_methods) && in_array("moda_payment_method_credit_card", $request->payment_methods))
            {
                $moverdata->moda_payment_method_credit_card = 1;
            }
            else
            {
                $moverdata->moda_payment_method_credit_card = 0;
            }

            if(isset($request->payment_methods) && in_array("moda_payment_method_paypal", $request->payment_methods))
            {
                $moverdata->moda_payment_method_paypal = 1;
            }
            else
            {
                $moverdata->moda_payment_method_paypal = 0;
            }

            $moverdata->save();
        } else if ($request->form_name == CustomerEditForms::SETTINGS)
        {
            $moverdata = MoverData::query()->where('moda_cu_id', $id)->get()[0];

            Log::debug($request);

            $moverdata->moda_special_agreements_checkbox = (($request->moda_special_agreements_checkbox === "on") ? 1 : 0);
            $moverdata->moda_special_agreements = $request->special_agreements;
            $moverdata->moda_no_claim_discount = (($request->moda_no_claim_discount === "on") ? 1 : 0);

            if ($request->moda_no_claim_discount === 'on')
            {
                $moverdata->moda_max_claim_percentage = 0;
                $moverdata->moda_no_claim_discount_percentage = $request->no_claim_discount;
                Log::debug("Setting no claim discount percentage :(");
            } else
            {
                $moverdata->moda_max_claim_percentage = $request->claim_percentage;
            }

            $moverdata->moda_quality_score_override = $request->quality_score_override;

            $moverdata->moda_capping_method = $request->capping_method;

            if ($request->capping_method == 1)
            {
                $moverdata->moda_capping_monthly_limit = $request->leads_monthly_limit;
                $moverdata->moda_capping_monthly_limit_left = $request->leads_monthly_limit_left;
                $moverdata->moda_capping_monthly_limit_modifier = $request->leads_monthly_limit_modifier;

                $moverdata->moda_capping_daily_average = ($request->leads_monthly_limit + $request->leads_monthly_limit_modifier) / 30.5;
            } else if ($request->capping_method == 2)
            {
                $moverdata->moda_capping_monthly_limit = $request->spend_monthly_limit;
                $moverdata->moda_capping_monthly_limit_left = $request->spend_monthly_limit_left;
                $moverdata->moda_capping_monthly_limit_modifier = $request->spend_monthly_limit_modifier;

                $moverdata->moda_capping_daily_average = ($request->spend_monthly_limit + $request->spend_monthly_limit_modifier) / 30.5;
            }

            $moverdata->save();
        } else if ($request->form_name == CustomerEditForms::MEMBERSHIPS)
        {
            self::saveMembershipsWithHistory($request, $id);

            DB::table('kt_customer_membership')->where('ktcume_cu_id', '=', $id)->delete();

            if ($request->memberships)
            {
                foreach ($request->memberships as $membership_id => $destination)
                {
                    $ktcume = new KTCustomerMembership();

                    $ktcume->ktcume_cu_id = $id;
                    $ktcume->ktcume_me_id = $membership_id;

                    $ktcume->save();
                }
            }
        }else if ($request->form_name == CustomerEditForms::OBLIGATIONS)
        {
            foreach ($request->permit as $cuob_id => $verified)
            {
                $ktcustomerobligation = KTCustomerObligation::where("ktcuob_cuob_id", $cuob_id)->where("ktcuob_cu_id", $customer->cu_id)->first();

                if (count($ktcustomerobligation) > 0)
                {
                    if ($verified == 0)
                    {
                        DB::table("kt_customer_obligations")->where("ktcuob_cu_id", $customer->cu_id)->where("ktcuob_cuob_id", $cuob_id)->delete();
                    }

                    if ($verified == 1)
                    {
                        $ktcustomerobligation->ktcuob_verification_result = 0;
                        $ktcustomerobligation->ktcuob_verified_timestamp = date("Y-m-d H:i:s");
                        $ktcustomerobligation->save();
                    }
                }
                else
                {
                    if ($verified == 1)
                    {
                        $ktcuob = new KTCustomerObligation();
                        $ktcuob->ktcuob_cuob_id = $cuob_id;
                        $ktcuob->ktcuob_cu_id = $customer->cu_id;
                        $ktcuob->ktcuob_verification_result = 0;
                        $ktcuob->ktcuob_verified_timestamp = date("Y-m-d H:i:s");
                        $ktcuob->save();
                    }
                }
            }
        }

        return redirect('resellers/' . $id . '/edit')->with('message', 'Mover successfully updated!');
    }

    public function create()
    {
        $customer_types = CustomerType::all();
        unset($customer_types[5]);

        return view('resellers.create',
            [
                'users' => User::where("id", "!=", 0),
                'countries' => Country::all(),
                'movertypes' => $customer_types,
                'debtorstatuses' => DebtorStatus::all(),
                'paymentreminderstatuses' => PaymentReminderStatus::all(),
                'paymentmethods' => PaymentMethod::all(),
                'paymentcurrencies' => PaymentCurrency::all(),
                'ledgeraccounts' => LedgerAccount::all(),
                'languages' => Language::where("la_iframe_only", 0)->get()
            ]);
    }

    public function store(Request $request)
    {
        $confirmed = false;

        if (Arr::exists($request, 'confirmed')) {
            $confirmed = true;
        }

        if ($confirmed == false)
        {
            $request->validate([
                'type' => 'required',
                'la_code' => 'required',
                'company_name_business' => 'required',
                'company_name_legal' => 'required',
                //'attn' => 'required',
                //'attn_email' => 'required',
                'email' => 'email',
                //'street_1' => 'required',
                //'zipcode' => 'required',
                //'city' => 'required',
                'country' => 'required',
                //'telephone' => 'required',
                'website' => 'required'
            ]);

            if(($request->int_telephone == 0 && !empty($request->telephone)) || ($request->int_telephone_bi == 0 && !empty($request->telephone_bi)))
            {
                return redirect()->back()->withErrors(['phone' => 'This phone number is incorrect!'])->withInput();
            }

            //Set Bool for creating (double company)
            $can_create = true;

            $double_company_message_html = "Companies have been found with the same fields: <br /><br />";
            $double_company_message_html .= "<ul>";

            /**
             * Start of IF-statements of checking if its a double company or no
             */
            $cn_business = Customer::select("cu_company_name_business")
                ->whereRaw('`cu_company_name_business` LIKE "%'.$request->company_name_business.'%"')
                ->where("cu_deleted", 0)
                ->limit(1)
                ->get();

            if(count($cn_business) > 0){
                $cn_business_name = Customer::select("cu_company_name_business")
                    ->whereRaw('`cu_company_name_business` LIKE "%'.$request->company_name_business.'%"')
                    ->where("cu_deleted", 0)
                    ->limit(1)
                    ->first()->cu_company_name_business;

                //Set can create false
                $can_create = false;
                //message
                $double_company_message_html .= "<li>Company name Business (Filled in): ".$request->company_name_business."</li><br />";
            }

            $cn_legal = Customer::select("cu_company_name_legal")
                ->whereRaw('`cu_company_name_legal` LIKE "%'.$request->company_name_legal.'%"')
                ->where("cu_deleted", 0)
                ->limit(1)
                ->get();

            if(count($cn_legal) > 0)
            {
                $cn_legal_name = Customer::select("cu_company_name_legal")
                    ->whereRaw('`cu_company_name_legal` LIKE "%'.$request->company_name_legal.'%"')
                    ->where("cu_deleted", 0)
                    ->limit(1)
                    ->first()->cu_company_name_legal;

                //Set can create false
                $can_create = false;

                //message
                $double_company_message_html .= "<li>Company name Legal (Filled in): ".$request->company_name_legal."</li><br />";
            }

            if(!empty($request->leads_email))
            {
                $email = Customer::select("cu_email")
                    ->where("cu_email", $request->leads_email)
                    ->where("cu_deleted", 0)
                    ->limit(1)
                    ->get();

                if(count($email) > 0)
                {
                    //Set can create false
                    $can_create = false;

                    //message
                    $double_company_message_html .= "<li>Email: ".$request->leads_email."</li><br />";
                }
            }

            if (!empty($request->zipcode))
            {
                $zipcode = Customer::select("cu_zipcode")
                    ->where("cu_zipcode", $request->zipcode)
                    ->where("cu_deleted", 0)
                    ->limit(1)
                    ->get();

                if(count($zipcode) > 0)
                {
                    //Set can create false
                    $can_create = false;

                    //message
                    $double_company_message_html .= "<li>Zipcode: ".$request->zipcode."</li><br />";
                }
            }

            if (!empty($request->coc))
            {
                $coc = Customer::select("cu_coc")
                    ->where("cu_coc", $request->coc)
                    ->where("cu_deleted", 0)
                    ->limit(1)
                    ->get();

                if(count($coc) > 0)
                {
                    //Set can create false
                    $can_create = false;

                    //message
                    $double_company_message_html .= "<li>CoC: ".$request->coc."</li><br />";
                }
            }

            if (!empty($request->load_exchange_email))
            {
                $domain = explode("@", $request->load_exchange_email);

                //Skip populair domains
                if ((strpos($domain[1], 'outlook') !== false)
                    || (strpos($domain[1], 'gmail') !== false)
                    || (strpos($domain[1], 'hotmail') !== false)
                    || (strpos($domain[1], 'yahoo') !== false)
                    || (strpos($domain[1], 'wanadoo') !== false)
                    || (strpos($domain[1], 'orange') !== false)
                    || (strpos($domain[1], 'live') !== false)
                    || (strpos($domain[1], 'msn') !== false)
                ) {
                    $can_create = true;
                }
                else {
                    $load_exchange_email = MoverData::select("moda_load_exchange_email")
                        ->whereRaw("moda_load_exchange_email LIKE '%".$domain[1]."%'")
                        ->limit(1)
                        ->get();

                    if(count($load_exchange_email) > 0)
                    {
                        //Set can create false
                        $can_create = false;

                        $double_company = Customer::select("cu_company_name_business")
                            ->leftJoin("mover_data", "cu_id", "moda_cu_id")
                            ->whereRaw("moda_load_exchange_email LIKE '%".$domain[1]."%'")
                            ->where("cu_deleted", 0)
                            ->first()->cu_company_name_business;

                        //message
                        $double_company_message_html .= "<li>Load Exchange E-mail: ".$request->load_exchange_email."</li><br />";
                    }
                }
            }

            if (!empty($request->review_email))
            {
                $domain = explode("@", $request->review_email);

                //Skip populair domains
                if ((strpos($domain[1], 'outlook') !== false)
                    || (strpos($domain[1], 'gmail') !== false)
                    || (strpos($domain[1], 'hotmail') !== false)
                    || (strpos($domain[1], 'yahoo') !== false)
                    || (strpos($domain[1], 'wanadoo') !== false)
                    || (strpos($domain[1], 'orange') !== false)
                    || (strpos($domain[1], 'live') !== false)
                    || (strpos($domain[1], 'msn') !== false)
                ) {
                    $can_create = true;
                }
                else {

                    $contact_email = MoverData::select("moda_contact_email")
                        ->whereRaw("moda_contact_email LIKE '%".$domain[1]."%'")
                        ->limit(1)
                        ->get();

                    if(count($contact_email) > 0)
                    {
                        //Set can create false
                        $can_create = false;

                        $double_company = Customer::select("cu_company_name_business")
                            ->leftJoin("mover_data", "cu_id", "moda_cu_id")
                            ->whereRaw("moda_contact_email LIKE '%".$domain[1]."%'")
                            ->where("cu_deleted", 0)
                            ->first()->cu_company_name_business;

                        //message
                        $double_company_message_html .= "<li>Contact E-mail: ".$request->review_email."</li><br />";
                    }
                }
            }

            $double_company_message_html .= "</ul>";

            if ($can_create == false)
            {
                $double_company_message_html .= "Name filled in: <b>".$request->company_name_business."</b><br /><br />";

                if(isset($cn_business_name) && !empty($cn_business_name))
                {
                    $double_company_message_html .= "(Business)Name of double company: <b>".$cn_business_name."</b><br /><br />";
                }
                elseif(isset($cn_legal_name) && !empty($cn_legal_name))
                {
                    $double_company_message_html .= "(Legal)Name of double company: <b>".$cn_legal_name."</b><br /><br />";
                }
                elseif(isset($double_company) && !empty($double_company))
                {
                    $double_company_message_html .= "(Business)Name of double company: <b>".$double_company."</b><br /><br />";
                }

                return view('resellers.confirm_doubles', [
                    'request' => $request,
                    'double_company_message_html' => $double_company_message_html
                ]);
            }
        }



        // Create a User using User model
        $customer = new Customer;

        $customer->cu_type = $request->type;
        $customer->cu_la_code = $request->la_code;
        $customer->cu_company_name_business = $request->company_name_business;
        $customer->cu_company_name_legal = $request->company_name_legal;
        $customer->cu_attn = $request->attn;
        $customer->cu_attn_email = $request->attn_email;
        $customer->cu_coc = $request->coc;
        $customer->cu_street_1 = $request->street_1;
        $customer->cu_street_2 = $request->street_2;
        $customer->cu_city = $request->city;
        $customer->cu_zipcode = $request->zipcode;
        $customer->cu_email = $request->leads_email;
        $customer->cu_co_code = $request->country;
        $customer->cu_telephone = $request->int_telephone;

        $website_url = $request->website;
        if (!empty($website_url)){
            if (strpos($website_url, 'http') === false) {
                $website_url = "http://".$website_url;
            }
        }

        $customer->cu_website = $website_url;
        $customer->cu_street_1_bi = $request->street_1_bi;
        $customer->cu_street_2_bi = $request->street_2_bi;
        $customer->cu_city_bi = $request->city_bi;
        $customer->cu_zipcode_bi = $request->zipcode_bi;
        $customer->cu_email_bi = $request->email_bi;

        if ((empty($customer->cu_description_la_code) || $customer->cu_description != $request->description) && !empty($request->description)) {
            $description_la_code = System::getGoogleTranslationLanguage($request->description);

            if (!empty($description_la_code)) {
                $customer->cu_description_la_code = $description_la_code;
            }
        }

        $customer->cu_description = $request->description;
        if($request->int_telephone_bi != 0)
        {
            $customer->cu_telephone_bi = $request->int_telephone_bi;
        }

        $website_url = $request->website_bi;
        if (!empty($website_url)){
            if (strpos($website_url, 'http') === false) {
                $website_url = "http://".$website_url;
            }
        }

        $customer->cu_website_bi = $website_url;
        $customer->cu_co_code_bi = $request->country_bi;
        $customer->cu_created_timestamp = date("Y-m-d H:i:s");
        $customer->cu_debtor_number = DB::table('customers')->max('cu_debtor_number') + 1;
        $customer->cu_use_billing_address = ($request->use_billing_address === 'on');

        $customer->save();

        if($request->type == 1)
        {
            $moverdata = new MoverData();

            $moverdata->moda_cu_id = $customer->cu_id;
            $moverdata->moda_contact_email = $request->leads_email;
            $moverdata->moda_load_exchange_email = $request->load_exchange_email;
            $moverdata->moda_review_email = $request->review_email;
            $moverdata->moda_info_email = $request->general_info;
            $moverdata->moda_contact_telephone = $request->telephone;
            $moverdata->moda_max_claim_percentage = 10;
            $moverdata->moda_activate_lead_pick = 1;

            $moverdata->save();
        }
        elseif($request->type == 3)
        {
            $afpada = new AffiliatePartnerData();
            $afpada->afpada_cu_id = $customer->cu_id;
            $afpada->save();
        }

        return redirect('resellers/' . $customer->cu_id . "/edit");

    }

    /**
     * @param $customer
     */
    public function getContactPersonLogin($customer): void
    {
        foreach ($customer->contactpersons as $contactperson)
        {
            if (!empty($contactperson->application_user))
            {
                if ($_SERVER['SERVER_NAME'] == "laravel.localhost") {
                    //LOCAL
                    $start_url = "http://mover.system";
                }
                elseif ($_SERVER['SERVER_NAME'] == "erp2.triglobal-test-back.nl") {
                    //TEST
                    $start_url = "https://mover.triglobal-test-back.nl";
                }
                else {
                    //LIVE
                    $start_url = "https://mover.triglobal.info";
                }

                $contactperson['login_url'] = $start_url."/erp.php/?id=" . md5($contactperson->application_user->apus_id) . "&login_key=" . md5($contactperson->application_user->apus_username) . "@" . md5($contactperson->application_user->apus_name);
            }
        }
    }

    /**
     * @param $customeres
     * @param System $system
     */
    public function calculateInvoiceData($customer, System $system): void
    {
        foreach ($customer->invoices as $invoice)
        {

            //Overdue
            $overdue = $system->dateDifference($invoice->in_expiration_date, date("Y-m-d"));

            if ($overdue < 0)
            {
                $overdue = 0;
            }

            $invoice->days_overdue = $overdue;

            $bank_lines = DB::table("kt_bank_line_invoice_customer_ledger_account")
                ->select("ktbaliinculeac_bali_id", "ktbaliinculeac_date", "ktbaliinculeac_amount", "bali_date")
                ->join("bank_lines", "ktbaliinculeac_bali_id", "=", "bali_id")
                ->where('ktbaliinculeac_in_id', "=", $invoice->in_id)
                ->get();

            $amount_paired = 0;
            $payment_dates = [];

            foreach ($bank_lines as $row_bali)
            {
                $amount_paired += $row_bali->ktbaliinculeac_amount;

                $payment_dates[] = (($row_bali->bali_date == 0) ? $row_bali->ktbaliinculeac_date : $row_bali->bali_date);
            }

            $invoice->payment_dates = implode(",", $payment_dates);
            $invoice->amount_paired = $amount_paired;

            //Reminder & method
            $invoice->payment_reminder = PaymentReminder::name($invoice->in_payment_reminder);
            $invoice->payment_method = PaymentMethod::name($invoice->in_payment_method);

            $invoice->currency_token_eur = $system->paymentCurrencyToken("EUR");
            $invoice->currency_token_fc = $system->paymentCurrencyToken($invoice->in_currency);
        }
    }

    /**
     * @param $customer
     * @return array
     */
    public function getCustomerMemberships($customer): array
    {
        $memberships = [];
        foreach ($customer->memberships as $member)
        {
            $memberships[] = $member->me_id;
        }

        return $memberships;
    }

    public function saveMembershipsWithHistory(Request $request, $cu_id)
    {

        $memberships = KTCustomerMembership::with("membership")->where("ktcume_cu_id", $cu_id)->get();

        $old_ids = [];
        $new_ids = [];
        $removed = [];
        $added = [];

        foreach ($memberships as $membership)
        {
            $old_ids[] = $membership->ktcume_me_id;
        }

        if ($request->memberships)
        {
            foreach ($request->memberships as $membership_id => $destination)
            {
                $new_ids[] = $membership_id;
            }
        }

        foreach ($memberships as $membership)
        {
            if (!in_array($membership->ktcume_me_id, $new_ids))
            {
                $removed[$membership->ktcume_me_id] = $membership->membership->me_name;
            }
        }

        if ($request->memberships)
        {
            foreach ($request->memberships as $membership_id => $destination)
            {
                if (!in_array($membership_id, $old_ids))
                {
                    $added[$membership_id] = Membership::where("me_id", $membership_id)->first()->me_name;
                }
            }
        }

        foreach ($removed as $remove_id => $name)
        {
            DB::table((new Revision())->getTable())->insert([
                [
                    'revisionable_type' => "App\Models\KTCustomerMembership",
                    'revisionable_id' => $cu_id,
                    'user_id' => \Auth::id(),
                    'key' => $name,
                    'old_value' => "On",
                    'new_value' => "Off",
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'updated_at' => Carbon::now()->toDateTimeString()
                ]
            ]);
        }

        foreach ($added as $remove_id => $name)
        {
            DB::table((new Revision())->getTable())->insert([
                [
                    'revisionable_type' => "App\Models\KTCustomerMembership",
                    'revisionable_id' => $cu_id,
                    'user_id' => \Auth::id(),
                    'key' => $name,
                    'old_value' => "Off",
                    'new_value' => "On",
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'updated_at' => Carbon::now()->toDateTimeString()
                ]
            ]);
        }
    }

    public function confirmSendInvoice($customer_id, $invoice_id)
    {
        $invoice = Invoice::findOrFail($invoice_id);

        $debtor_data = unserialize(base64_decode($invoice->in_debtor_data));

        return view('invoice.confirm', ['invoice' => $invoice, 'debtor_data' => $debtor_data, 'redirect' => 'customers']);
    }

    public function getRequests(Request $request, $customer_id)
    {
        $system = new System();
        $dates = $system->betweenDates($request->date);

        return self::edit($customer_id, $dates);
    }

    public function getInvoices(Request $request, $customer_id)
    {
        return self::edit($customer_id, null, $request->paid);
    }

    public function getSurveys(Request $request, $customer_id)
    {
        $system = new System();
        $dates = null;

        if (!empty($request->date))
            $dates = $system->betweenDates($request->date);

        $data = [
            "published" => $request->published,
            "date" => $dates
        ];

        return self::edit($customer_id, null, null, $data);
    }

    public function incorrectCustomers()
    {
        return view('resellers.incorrect_customers');
    }
}
