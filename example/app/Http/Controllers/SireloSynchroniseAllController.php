<?php

namespace App\Http\Controllers;

use App\Data\Spaces;
use App\Functions\Data;
use App\Functions\Mover;
use App\Functions\SireloCustomer;
use App\Functions\Synchronise;
use App\Functions\System;
use App\Models\ApplicationUser;
use App\Models\Customer;
use App\Models\CustomerOffice;
use App\Models\Insurance;
use App\Models\KTCustomerGatheredReview;
use App\Models\KTCustomerInsurance;
use App\Models\KTCustomerMembership;
use App\Models\KTCustomerObligation;
use App\Models\KTCustomerPortalCountry;
use App\Models\Membership;
use App\Models\Obligation;
use App\Models\SireloLink;
use App\Models\Survey2;
use App\Models\Website;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Functions\DataIndex;
use Illuminate\Support\Facades\Log;

class SireloSynchroniseAllController extends Controller
{

    public function index()
    {
        return view('synchroniseall.index');
    }

    public function update(Request $request)
    {
        // The countries that don't need to get exported to the bucket
        $exclude_countries = ['NL', 'IT', 'DE', 'ES', 'FR', 'AT', 'ZA', 'AU', 'UK', 'US'];

        // Get all bucket websites
        // Which have a ftp server
        $query = Website::where("we_sirelo_bucket", 1)->get();

        // Get memberships
        $memberships = Membership::whereNotIn("me_id", [26,27])->get();

        $spaces = new Spaces('srl');

        // Get all insurances
        $customer_insurances = Insurance::all();

        $sirelocustomer = new SireloCustomer();

        // Array for saving we_ids which we syncing
        $website_ids = [];

        // Loop websites
        foreach ($query as $row) {
            $website_ids[$row->we_id] = $row->we_id;
            // If we only want an answer back
            if (!isset($request->only_answer)) {
                // Get + and - points from this customer
                $pro_con_categories = Data::getProConCategories(null, $row->we_sirelo_la_code);

                // Create empty arrays
                $cities = [];
                $customers = [];
                $customer_offices = [];
                $customer_memberships = [];
                $kt_customer_insurances = [];
                $reviews = [];
                $logos = [];
                $collectedIds = [];

                // Select all customers from countries
                $query_customers = Customer::leftJoin("mover_data", "cu_id", "moda_cu_id")
                    ->where("cu_type", 1)
                    ->where("cu_city", "!=", "")
                    ->where("cu_deleted", 0)
                    ->where("moda_disable_sirelo_export", 0)
                    ->whereRaw("cu_co_code NOT IN (" . System::escapedImplode($exclude_countries) . ")")
                    ->where("moda_international_moves", 1)
                    ->get();

                // Loop customers from bucket
                foreach ($query_customers as $row_customers) {

                    $collectedIds[] = $row_customers->cu_id;

                    // Create seo city key from city
                    $city_key = System::getSeoKey($row_customers->cu_city);

                    // Create customer key from city key and company business name
                    $customer_key = $city_key . System::getSeoKey($row_customers->cu_company_name_business);

                    // Store country code
                    $country_code = $row_customers->cu_co_code;

                    // New temp customer
                    $temp_customer = [$city_key];

                    // Check if cities country code exists
                    if (!isset($cities[$country_code])) {
                        // Create cities country code
                        $cities[$country_code] = [];
                    }

                    // Check if city key is not present yet
                    if (!isset($cities[$country_code][$city_key])) {
                        // Set city in this country to 1
                        $cities[$country_code][$city_key] = [
                            'city' => $row_customers->cu_city,
                            'amount' => 1,
                            'country_code' => $country_code
                        ];

                        // City key exists
                    } else {

                        // Add 1 to amount;
                        $cities[$country_code][$city_key]['amount']++;
                    }

                    // Customer does not exist yet
                    if (!isset($customers[$customer_key])) {
                        // Get data from application users
                        $query_user = ApplicationUser::where("apus_cu_id", $row_customers->cu_id)->get();

                        // Setup new customer data
                        $customers[$customer_key] = [
                            "city_key" => $city_key,
                            "forward_1" => $row_customers->moda_sirelo_forward_1,
                            "forward_2" => $row_customers->moda_sirelo_forward_2,
                            "origin_id" => $row_customers->cu_id,
                            "top_mover" => 0,
                            "disable_top_mover" => $row_customers->moda_sirelo_disable_top_mover,
                            "disable_request_form_on_customer_page" => $row_customers->moda_sirelo_disable_request_form,
                            "review_partner" => $row_customers->moda_review_partner,
                            "claimed" => ((count($query_user) > 0) ? 1 : 0),
                            "company_name" => $row_customers->cu_company_name_business,
                            "legal_name" => $row_customers->cu_company_name_legal,
                            "coc" => $row_customers->cu_coc,
                            "logo" => "",
                            "description" => $row_customers->cu_description,
                            'default_language' => $row_customers->cu_la_code,
                            "international_moves" => $row_customers->moda_international_moves,
                            "national_moves" => $row_customers->moda_national_moves,
                            "long_distance_moving_europe" => $row_customers->moda_long_distance_moving_europe,
                            "excess_baggage" => $row_customers->moda_excess_baggage,
                            "man_and_van" => $row_customers->moda_man_and_van,
                            "car_and_vehicle_transport" => $row_customers->moda_car_and_vehicle_transport,
                            "piano_transport" => $row_customers->moda_piano_transport,
                            "pet_transport" => $row_customers->moda_pet_transport,
                            "art_transport" => $row_customers->moda_art_transport,
                            "established" => $row_customers->moda_established,
                            "employees" => $row_customers->moda_employees,
                            "trucks" => $row_customers->moda_trucks,
                            "street" => $row_customers->cu_street_1,
                            "zipcode" => $row_customers->cu_zipcode,
                            "city" => $row_customers->cu_city,
//                            "state" => $row_customers->cu_state,
                            "country" => $row_customers->cu_co_code,
                            "email" => $row_customers->moda_contact_email,
                            "telephone" => $row_customers->moda_contact_telephone,
                            "website" => $row_customers->cu_website,
                            "not_operational" => $row_customers->moda_not_operational,
                            "type_of_mover" => $row_customers->cu_type_of_mover,
                            "public_liability_insurance" => $row_customers->cu_public_liability_insurance,
                            "goods_in_transit_insurance" => $row_customers->cu_goods_in_transit_insurance,
                            "rating" => 0,
                            "reviews" => 0,
                            "recommendation" => 0,
                            "recommendations" => 0,
                            "rating_recent" => 0,
                            "reviews_recent" => 0,
                            "recommendation_recent" => 0,
                            "recommendations_recent" => 0,
                            "pros_cons" => ["pro" => [], "con" => []]
                        ];

                        // Logo is not empty
                        if (!empty($row_customers->cu_logo)) {
                            $pathToFile = env('SHARED_FOLDER'). 'uploads/logos/' . $row_customers->cu_logo;

                            if (is_file($pathToFile)) {
                                // Get logo
                                $logo = substr(System::getSeoKey($row_customers->cu_company_name_business, $row->we_sirelo_la_code), 0, -1) . "-" . substr($city_key, 0, -1) . "." . pathinfo($row_customers->cu_logo, PATHINFO_EXTENSION);

                                // Set logo
                                $customers[$customer_key]['logo'] = $logo;

                                // Add to logos array
                                $logos[$row_customers->cu_logo] = $logo;
                            }
                        }
                    }

                    // Get customer offices and add in array $customer_offices[]
                    $query_customer_offices = CustomerOffice::where("cuof_cu_id", $row_customers->cu_id)
                        ->where(function ($query) use ($country_code) {
                            $query
                                ->where('cuof_co_code', '')
                                ->orWhere('cuof_co_code', $country_code);
                        })
                        ->get();

                    // Check if there are any
                    if (count($query_customer_offices) > 0) {

                        // Loop offices from this customer
                        foreach ($query_customer_offices as $row_customer_offices) {

                            // Create new city key
                            $office_city_key = System::getSeoKey($row_customer_offices->cuof_city);

                            // If city key is not the same as the office city key
                            if (!in_array($office_city_key, $temp_customer)) {
                                // Check if city key is not present yet
                                if (!isset($cities[$country_code][$office_city_key])) {
                                    $cities[$country_code][$office_city_key] = [

                                        'city' => $row_customer_offices->cuof_city,
                                        'amount' => 1,
                                        'country_code' => $country_code
                                    ];

                                    // City key exists
                                } else {
                                    // Add 1 to amount;
                                    $cities[$country_code][$office_city_key]['amount']++;
                                }
                            }

                            // Temp customer
                            $temp_customer[] = $office_city_key;

                            // Setup customer office data
                            $customer_offices[] = [
                                "customer_key" => $customer_key,
                                "city_key" => $office_city_key,
                                "street" => $row_customer_offices->cuof_street_1,
                                "zipcode" => $row_customer_offices->cuof_zipcode,
                                "city" => $row_customer_offices->cuof_city,
                                "country" => $row_customer_offices->cuof_co_code ?? '',
                                "email" => $row_customer_offices->cuof_email,
                                "telephone" => $row_customer_offices->cuof_telephone,
                                "website" => $row_customer_offices->cuof_website,
                                "country_code" => $country_code
                            ];
                        }
                    }

                    // Get customer memberships and add in array $customer_memberships[]
                    $query_customer_memberships = KTCustomerMembership::where("ktcume_cu_id", $row_customers->cu_id)->get();

                    // If there are customer memberships
                    if (count($query_customer_memberships) > 0) {
                        // Loop customer memberships
                        foreach ($query_customer_memberships as $row_customer_memberships) {
                            // Store customer memberships
                            $customer_memberships[] = [
                                "customer_key" => $customer_key,
                                "ktcume_cu_id" => $row_customer_memberships->ktcume_cu_id,
                                "ktcume_me_id" => $row_customer_memberships->ktcume_me_id
                            ];
                        }
                    }

                    //Get customer insurances and add in array $customer_insurances[]
                    $kt_customer_insurances_query = KTCustomerInsurance::where("ktcuin_cu_id", $row_customers->cu_id)->get();

                    foreach ($kt_customer_insurances_query as $row_customer_insurances) {
                        $kt_customer_insurances[] = [
                            "customer_key" => $customer_key,
                            "ktcuin_id" => $row_customer_insurances->ktcuin_id,
                            "ktcuin_cuob_id" => $row_customer_insurances->ktcuin_cuob_id,
                            "ktcuin_cu_id" => $row_customer_insurances->ktcuin_cu_id,
                            "ktcuin_cudo_id" => $row_customer_insurances->ktcuin_cudo_id,
                            "ktcuin_verified_timestamp" => $row_customer_insurances->ktcuin_verified_timestamp,
                            "ktcuin_verification_result" => $row_customer_insurances->ktcuin_verification_result,
                            "ktcuin_amount" => $row_customer_insurances->ktcuin_amount
                        ];
                    }

                    // Loop past and recent
                    foreach (["past", "recent"] as $type) {
                        // If type is past
                        if ($type == "past") {
                            $timestamp = "<";
                        } // If type is recent
                        elseif ($type == "recent") {
                            $timestamp = ">=";
                        }

                        // Get surveys
                        $query_surveys = Survey2::leftJoin("requests", "su_re_id", "re_id")
                            ->leftJoin("website_reviews", "su_were_id", "were_id")
                            ->whereRaw("`surveys_2`.`su_submitted_timestamp` " . $timestamp . " '" . date("Y-m", strtotime("-" . $row->we_sirelo_top_mover_months . " months")) . "-01 00:00:00'")
                            ->where("su_submitted", 1)
                            ->whereRaw("(
								`surveys_2`.`su_mover` = '" . $row_customers->cu_id . "' OR
								`surveys_2`.`su_other_mover` = '" . $row_customers->cu_id . "'
							)")
                            ->where("su_show_on_website", ">", 0)
                            ->orderBy("su_submitted_timestamp", "DESC")
                            ->get();

                        $query_surveys = System::databaseToArray($query_surveys);

                        // Loop surveys
                        foreach ($query_surveys as $row_surveys) {
                            // If the survey is shown on the website
                            if ($row_surveys['su_show_on_website'] == 1) {
                                // If survey came from request
                                if ($row_surveys['su_source'] == 1) {
                                    $prefix = "re";
                                    $author = $row_surveys[$prefix . '_full_name'];
                                } // If survey came from website review form
                                elseif ($row_surveys['su_source'] == 2) {
                                    $prefix = "were";
                                    $author = $row_surveys[$prefix . '_name'];
                                }

                                if ($row_surveys['su_source'] == 1 || ($row_surveys['su_source'] == 2 && $row_surveys['were_anonymous'] == 1)) {
                                    $exp = explode(" ", preg_replace("/&([a-z])[a-z]+;/i", "$1", htmlentities(System::rusToLat($author))));

                                    $author = "";

                                    foreach ($exp as $letter) {
                                        $author .= ((isset($letter[0])) ? strtoupper($letter[0]) . "." : "");
                                    }
                                }

                                $reviews[$row_surveys['su_id']] = [
                                    "customer_key" => $customer_key,
                                    "source" => $row_surveys['su_source'],
                                    "author" => $author,
                                    "date" => date("Y-m-d", strtotime($row_surveys['su_submitted_timestamp'])),
                                    "rating" => $row_surveys['su_rating'],
                                    "rating_motivation" => $row_surveys['su_rating_motivation'],
                                    "recommend" => $row_surveys['su_recommend_mover'],
                                    "pro_1" => $row_surveys['su_pro_1'],
                                    "pro_2" => $row_surveys['su_pro_2'],
                                    "con_1" => $row_surveys['su_con_1'],
                                    "con_2" => $row_surveys['su_con_2'],
                                    "city_from" => $row_surveys[$prefix . '_city_from'],
                                    "country_from" => $row_surveys[$prefix . '_co_code_from'],
                                    "city_to" => $row_surveys[$prefix . '_city_to'],
                                    "country_to" => $row_surveys[$prefix . '_co_code_to'],
                                    "mover_comment_date" => date("Y-m-d", strtotime($row_surveys['su_mover_comment_timestamp'])),
                                    "mover_comment" => $row_surveys['su_mover_comment']
                                ];

                                $customers[$customer_key]['rating'] += $row_surveys['su_rating'];
                                $customers[$customer_key]['reviews']++;

                                if ($type == "recent") {
                                    $customers[$customer_key]['rating_recent'] += $row_surveys['su_rating'];
                                    $customers[$customer_key]['reviews_recent']++;
                                }

                                if ($row_surveys['su_recommend_mover'] > 0) {
                                    $customers[$customer_key]['recommendations']++;

                                    if ($type == "recent") {
                                        $customers[$customer_key]['recommendations_recent']++;
                                    }

                                    if ($row_surveys['su_recommend_mover'] == 1) {
                                        $customers[$customer_key]['recommendation']++;

                                        if ($type == "recent") {
                                            $customers[$customer_key]['recommendation_recent']++;
                                        }
                                    }
                                }

                                //Loop trough all pros & cons of review
                                foreach (["pro_1", "pro_2", "con_1", "con_2"] as $pro_con_type) {
                                    $category_id = $row_surveys['su_' . $pro_con_type . '_category'];
                                    $category_type = substr($pro_con_type, 0, 3);

                                    if ($category_id > 0) {
                                        if (!array_key_exists($category_id, $customers[$customer_key]['pros_cons'][$category_type])) {
                                            $customers[$customer_key]['pros_cons'][$category_type][$category_id] = [
                                                "name" => $pro_con_categories[$category_id],
                                                "amount" => 1
                                            ];
                                        } else {
                                            $customers[$customer_key]['pros_cons'][$category_type][$category_id]['amount']++;
                                        }
                                    }
                                }
                            }

                            if ($row_surveys['su_mail'] == 0) {
                                DB::table('surveys_2')
                                    ->where('su_id', $row_surveys['su_id'])
                                    ->update(['su_mail' => 1]);
                            }
                        }
                    }
                }

                foreach ($customers as $key => $customer) {
                    if ($customers[$key]['reviews'] > 0) {
                        $rating_reviews = $sirelocustomer->getReviews($customers[$key]['origin_id']);

                        $customers[$key]['rating'] = $rating_reviews['rating'];

                        //$customers[$key]['rating'] = round($customers[$key]['rating'] / $customers[$key]['reviews'], 2);
                    }

                    if ($customers[$key]['reviews_recent'] > 0) {
                        $customers[$key]['rating_recent'] = round($customers[$key]['rating_recent'] / $customers[$key]['reviews_recent']);
                    }

                    if ($customers[$key]['recommendations'] > 0) {
                        $customers[$key]['recommendation'] = round(($customers[$key]['recommendation'] / $customers[$key]['recommendations']) * 100);
                    }

                    if ($customers[$key]['recommendations_recent'] > 0) {
                        $customers[$key]['recommendation_recent'] = round(($customers[$key]['recommendation_recent'] / $customers[$key]['recommendations_recent']) * 100);
                    }

                    // If this customer is a top mover
                    // If customer's top mover is not disabled
                    if ($customers[$key]['rating_recent'] >= 4 && $customers[$key]['reviews_recent'] >= 12 && $customers[$key]['recommendation_recent'] >= 80 && !$customer[$key]['disable_top_mover']) {
                        $customers[$key]['top_mover'] = 1;
                    }

                    //Sort and limit both types
                    foreach ($customers[$key]['pros_cons'] as $category_type => $category_data) {
                        //Sort by amount
                        usort($customers[$key]['pros_cons'][$category_type], function ($a, $b) {
                            return strnatcasecmp($b['amount'], $a['amount']);
                        });

                        //Limit by checking recommendations
                        if ($category_type == "con" && $customers[$key]['recommendations'] > 0) {
                            if ($customers[$key]['recommendation'] >= 85) {
                                $limit = 1;
                            } elseif ($customers[$key]['recommendation'] >= 70) {
                                $limit = 2;
                            } else {
                                $limit = 3;
                            }
                        } else {
                            $limit = 3;
                        }

                        //Limit the amount per category
                        $customers[$key]['pros_cons'][$category_type] = array_slice($customers[$key]['pros_cons'][$category_type], 0, $limit, true);
                    }
                }

                // Get scores for all the processed customers
                $scores = KTCustomerGatheredReview::select( 'ktcugare_cu_id', 'ktcugare_platform', 'ktcugare_score', 'ktcugare_amount', 'ktcugare_url', 'ktcugare_timestamp_updated' )->whereIn( 'ktcugare_cu_id', $collectedIds )->get();

                $scores = ( ! empty( $scores ) ? System::databaseToArray( $scores ) : [] );

                $postData = [
                    'cities' => $cities,
                    'customers' => $customers,
                    'customer_offices' => $customer_offices,
                    'customer_memberships' => $customer_memberships,
                    'customer_insurances' => $customer_insurances,
                    'kt_customer_insurances' => $kt_customer_insurances,
                    'reviews' => $reviews,
                    'memberships' => $memberships,
                    'sirelo_customer_scores' => $scores,
                ];

                $urlToPostTo = $row->we_sirelo_url . '/wp-content/themes/sirelo/import/import.php';

                $postData = json_encode(array('sirelo' => $postData));

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $urlToPostTo);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($postData),
                ));
                $import = curl_exec($ch);
                curl_close($ch);

                // If we got a response
                if (!empty($import)) {
                    // Decode response
                    $locations = json_decode(trim($import), true);

                    // Get sirelo main id
                    $sirelo_id = Website::where("we_sirelo_bucket", 1)->first()->we_id;

                    if (!empty($locations) && is_array($locations)) {
                        // Loop locations based on country code
                        foreach ($locations as $country_code => $lData) {
                            // If country code is a falsy value
                            // The main sirelo site gives a 0 as country code
                            if (!$country_code)
                                continue;

                            if (!empty($lData['customer_location'])) {
                                DB::table('website_subdomains')
                                    ->where('wesu_we_id', $sirelo_id)
                                    ->where('wesu_country_code', $country_code)
                                    ->update(
                                        [
                                            "wesu_customer_location" => $lData['customer_location']
                                        ]
                                    );
                            }

                            if (!empty($lData['survey_location'])) {
                                DB::table('website_subdomains')
                                    ->where('wesu_we_id', $sirelo_id)
                                    ->where('wesu_country_code', $country_code)
                                    ->update(
                                        [
                                            "wesu_survey_location" => $lData['survey_location']
                                        ]
                                    );
                            }

                        }
                    }
                }

                /*if ((isset($request->sync_logos) && $request->sync_logos) || System::isCron()) {

                    // Loop logos
                    foreach ($logos as $logoKey => $logoValue) {
                        // Set default
                        $putImage = false;

                        // Image doesnt exist on spaces yet
                        if (!$spaces->exists('logos/' . $logoValue)) {
                            $putImage = true;

                        } else {
                            $erpSize = filesize(env('SHARED_FOLDER') . 'uploads/logos/' . $logoKey);
                            $spaSize = $spaces->getSize('logos/' . $logoValue);

                            // If sizes are not the same. overwrite the old images
                            if ($erpSize != $spaSize) {
                                $putImage = true;
                            }
                        }

                        if ($putImage) {
                            //  write or overwrite image
                            $spaces->put(env('SHARED_FOLDER') . 'uploads/logos/' . $logoKey, 'logos/' . $logoValue);
                        }
                    }

                }*/

                // You can disable logo sync with this
                $dontDoLogos = false;

                // If skipping is not set or is cron
                if ((!(isset($request->logo_skip)) || System::isCron()) && !$dontDoLogos) {
                    try
                    {
                        // Loop all logos
                        foreach ($logos as $logoKey => $logoValue) {
                            // Set default
                            $putImage = false;

                            // Image doesnt exist on spaces yet
                            if (!$spaces->exists('logos/' . $logoValue)) {
                                $putImage = true;
                            } else {
                                // Set path to file
                                $pathToFile = env('SHARED_FOLDER') . 'uploads/logos/' . $logoKey;
                                $erpSize = filesize($pathToFile);
                                $spaSize = $spaces->getSize('logos/' . $logoValue);

                                // If sizes are not the same. overwrite the old images
                                if ($erpSize != $spaSize) {

                                    $putImage = true;
                                }
                            }

                            if ($putImage) {
                                //  write or overwrite image
                                $spaces->put(env('SHARED_FOLDER') . 'uploads/logos/' . $logoKey, 'logos/' . $logoValue);
                            }
                        }
                    }catch (\Exception $e){
                        System::sendMessage([4186, 3653], "Sirelo SYNC (Bucket) - Spaces API crash", "Spaces API is failing :(");
                    }
                }
            }

        }

        echo '<hr />';

        if (!isset($request->only_answer) || System::isCron()) {
            // Send emails
            Synchronise::sendEmails($website_ids);
        }


        return view('synchroniseall.index');
    }

}
