<?php

namespace App\Http\Controllers;

use App\Functions\DataIndex;
use App\Functions\System;
use App\Models\LedgerAccount;
use DB;
use Illuminate\Http\Request;

class CostJournalEntryController extends Controller
{
	public function index()
	{
		
		return view('finance.costjournalentry', [
			'selected_date' => "",
			'date_filter' => [],
			'filtered_data' => []
		]);
	}
	
	public function filteredIndex(Request $request)
	{
		$request->validate([
			'date' => 'required'
		]);
		
		$total = 0;
		$amounts = [];
		
		$ledgers = LedgerAccount::whereIn("leac_type", [2,3])
			->where("leac_journal_entry", 1)
			->get();
		
		foreach($ledgers as $leac)
		{
			$amounts[$leac->leac_number] = 0;
			
			$bank_lines = DB::table("bank_lines")
				->leftJoin("kt_bank_line_invoice_customer_ledger_account","bali_id","ktbaliinculeac_bali_id")
				->whereBetween("bali_date", System::betweenDates($request->date))
				->where("ktbaliinculeac_leac_number", $leac->leac_number)
				->get();
			
			foreach($bank_lines as $bank_line)
			{
				$amounts[$leac->leac_number] += $bank_line->ktbaliinculeac_amount;
			}
			
			$total += $amounts[$leac->leac_number];
		}
		
		
		return view('finance.costjournalentry', [
			'selected_date' => $request->date,
			'ledgers' => $ledgers,
			'total' => $total,
			'amount_deb' => $amounts
			
		]);
		
	}
	
}
