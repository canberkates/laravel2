<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\Insurance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InsuranceController extends Controller
{
    public function index()
    {
        return view('insurance.index',
            [
                'insurances' => Insurance::all(),
            ]);
    }

    public function create()
    {
        return view('insurance.create', ['countries' => Country::all()]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $insurance = new Insurance();

        $insurance->cuin_name = $request->name;
        $insurance->cuin_remark = $request->remark;

        $insurance->save();

        return redirect('admin/insurances');
    }

    public function edit($insurance_id)
    {
        $insurance = Insurance::where("cuin_id", $insurance_id)->first();

        /*$sirelo_websites = Website::whereRaw("we_sirelo_co_code IS NOT NULL")->get();

        $sirelo_links = Website::leftJoin("sirelo_hyperlinks", "we_sirelo_co_code", "sihy_co_code")->where("sihy_cuob_id", $obligation->cuob_id)->get();

        $websites = [];
        $sirelo_websites_list = [];

        foreach ($sirelo_links as $link)
        {
            $sirelo_websites_list[$link->sihy_co_code] = [
                'website_name' => $link->we_website,
                'link' => $link->sihy_link
            ];
        }

        foreach ($sirelo_websites as $website)
        {
            if (array_key_exists($website->we_sirelo_co_code, $sirelo_websites_list))
            {
                $websites[$website->we_sirelo_co_code] = [
                    'website_name' => $website->we_website,
                    'link' => $sirelo_websites_list[$website->we_sirelo_co_code]['link'],
                ];
            }
            else
            {
                $websites[$website->we_sirelo_co_code] = [
                    'website_name' => $website->we_website,
                    'link' => ""
                ];
            }
        }*/

        return view('insurance.edit',
            [
                'insurance' => $insurance,
            ]);

    }

    public function update(Request $request, $insurance_id)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $insurance = Insurance::findOrFail($insurance_id);

        //Save basics
        $insurance->cuin_name = $request->name;
        $insurance->cuin_remark = $request->remark;

        $insurance->save();

        //Loop through every sirelo link post
        /*foreach ($request->sirelo as $co_code => $link)
        {
            $sireloLink = SireloLink::where("sihy_co_code", $co_code)->where("sihy_cuob_id", $obligation_id)->first();

            //check if link already exists
            if (count($sireloLink) > 0)
            {
                //Update hyperlink
                $sireloLink->sihy_link = $link;
                $sireloLink->sihy_timestamp = date("Y-m-d H:i:s");
                $sireloLink->save();
            }
            else
            {
                //Create hyperlink
                $newLink = new SireloLink();

                $newLink->sihy_co_code = $co_code;
                $newLink->sihy_cuob_id = $obligation_id;
                $newLink->sihy_link = ((empty($link)) ? "" : $link);
                $newLink->sihy_timestamp = date("Y-m-d H:i:s");

                $newLink->save();
            }
        }*/

        return redirect('admin/insurances');
    }

    public function delete(Request $request)
    {
        DB::table("kt_customer_insurances")
            ->where("ktcuin_cuin_id", $request->id)
            ->delete();

        DB::table("customer_insurances")
            ->where("cuin_id", $request->id)
            ->delete();
    }

}
