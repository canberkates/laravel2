<?php

namespace App\Http\Controllers;

use App\Functions\System;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClaimsCheckController extends Controller
{
	public function index()
	{
		return view('claimscheck.index',
			[
				'totals'=> null,
				'userclaims' => [],
                'date' => null
			]
		);
	}
	
	public function filteredindex(Request $request)
	{
		$total_accepted = 0;
		$total_rejected = 0;
		
		foreach(range(0, 23) as $hour)
		{
			${"total_hour_".$hour} = 0;
		}
		
		$total_total = 0;
		
		$all_users = [];
		foreach(User::all() as $user){
			$all_users[$user->id] = $user->us_name;
		}
		
		$user_claims = [];
		
		foreach($all_users as $user_id => $username)
		{
			$accepted = 0;
			$rejected = 0;
			
			foreach(range(0, 23) as $hour)
			{
				${"hours_".$hour} = 0;
			}
			
			$total = 0;
			
			$claims = DB::table("claims")
				->whereIn("cl_status", ["1","2"])
				->whereBetween("cl_timestamp",System::betweenDates($request->date))
				->where("cl_processed_us_id",$user_id)
				->get();
			
			foreach($claims as $row)
			{
				if($row->cl_status == 1)
				{
					$accepted++;
				}
				elseif($row->cl_status == 2)
				{
					$rejected++;
				}
				
				${"hours_".date("G", strtotime($row->cl_processed_timestamp))}++;
				
				$total++;
			}
			
			if($total > 0)
			{
				$user_claims[$user_id]['name'] = $username;
				$user_claims[$user_id]['accepted'] = $accepted;
				$user_claims[$user_id]['rejected'] = $rejected;
				$user_claims[$user_id]['total'] = $total;
				
				$total_accepted += $accepted;
				$total_rejected += $rejected;
				
				foreach(range(0, 23) as $hour)
				{
					$user_claims[$user_id]['hours'][$hour] = ${"hours_".$hour};
					${"total_hour_".$hour} += ${"hours_".$hour};
				}
				
				$total_total += $total;
			}
		}
		
		$totals['accepted'] = $total_accepted;
		$totals['rejected'] = $total_rejected;
		$totals['total'] = $total_total;
		
		foreach(range(0, 23) as $hour)
		{
			$totals['hours'][$hour] = ${"total_hour_".$hour};
		}
		
		return view('claimscheck.index',
			[
				'totals' => $totals,
				'userclaims' => $user_claims,
				'date' => $request->date,
                'filtered' => true
			]
		);
		
	}
	
}
