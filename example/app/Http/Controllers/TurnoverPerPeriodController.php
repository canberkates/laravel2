<?php

namespace App\Http\Controllers;

use App\Data\TurnoverDimensions;
use App\Data\TurnoverType;
use App\Functions\DataIndex;
use App\Functions\System;
use App\Models\LedgerAccount;
use App\Models\Portal;
use DB;
use Illuminate\Http\Request;

class TurnoverPerPeriodController extends Controller
{
	public function index()
	{
		$dimensions = TurnoverDimensions::all();
		
		return view('finance.turnoverperperiod', [
			'dimensions' => $dimensions,
			'selected_date' => "",
			'selected_dimension' => "",
			'date_filter' => [],
			'filtered_data' => []
		]);
	}
	
	public function filteredIndex(Request $request)
	{
		$request->validate([
			'date' => 'required',
			'dimension' => 'required'
		]);
		
		$turnoverdimensions = TurnoverDimensions::all();
		
		$dimension = $request->dimension;
		
		$total_turnover = 0;
		
		if ($dimension == TurnoverDimensions::LEDGER_ACCOUNT)
		{
			$ledgers = LedgerAccount::where("leac_type", 1)->get();
			
			$total_dimensions = [];
			
			foreach ($ledgers as $ledger)
			{
				$total_dimension[$ledger->leac_number . ' - ' . $ledger->leac_name] = [$ledger->leac_number . ' - ' . $ledger->leac_name];
				
				$results = DB::table("invoices")
					->selectRaw("SUM(`inli_amount_netto_eur`) AS `turnover`")
					->leftJoin("invoice_lines", "in_id", "inli_in_id")
					->whereBetween("in_date", System::betweenDates($request->date))
					->where("inli_leac_number", $ledger->leac_number)
					->first();
				
				if($results)
				{
					$total_turnover += $results->turnover;
					$total_dimensions[$ledger->leac_number . ' - ' . $ledger->leac_name] = $results->turnover;
				}
				
			}
			
			$no_claim = DB::table("invoices")
				->selectRaw("SUM(`inli_amount_netto_eur`) AS `no_claim`")
				->leftJoin("invoice_lines", "in_id", "inli_in_id")
				->whereBetween("in_date", System::betweenDates($request->date))
				->where("inli_no_claim_discount", 1)
				->first();
			
			return view('finance.turnoverperperiod', [
				'dimensions' => $turnoverdimensions,
				'selected_date' => $request->date,
				'selected_dimension' => $dimension,
				'total_turnover' => $total_turnover,
				'total_dimensions' => $total_dimensions,
				'no_claim' => ($no_claim->no_claim ?? 0)
			]);
			
		}
		
		if ($dimension == TurnoverDimensions::PORTAL)
		{
			$portals = Portal::with("country")->get();
			
			$total_dimensions = [];
			
			foreach ($portals as $portal)
			{
				$total_dimension[$portal->po_portal . ($portal->country ? ' ('.$portal->country->co_en.')' : '')] = $portal->po_portal . ($portal->country ? ' ('.$portal->country->co_en.')' : '');
				
				$results = DB::table("invoices")
					->selectRaw("SUM(`inli_amount_netto_eur`) AS `turnover`")
					->leftJoin("invoice_lines", "in_id", "inli_in_id")
					->leftJoin("kt_request_customer_portal", "inli_ktrecupo_id", "ktrecupo_id")
					->where("ktrecupo_po_id", $portal->po_id)
					->whereBetween("in_date",  System::betweenDates($request->date))
					->first();
				
				if($results)
				{
					$total_turnover += $results->turnover;
					$total_dimensions[$portal->po_portal . ($portal->country ? ' ('.$portal->country->co_en.')' : '')] = $results->turnover;
				}
				
			}
			
			$no_claim = DB::table("invoices")
				->selectRaw("SUM(`inli_amount_netto_eur`) AS `no_claim`")
				->leftJoin("invoice_lines", "in_id", "inli_in_id")
				->whereBetween("in_date", System::betweenDates($request->date))
				->where("inli_no_claim_discount", 1)
				->first();
			
			return view('finance.turnoverperperiod', [
				'dimensions' => $turnoverdimensions,
				'selected_date' => $request->date,
				'selected_dimension' => $dimension,
				'total_turnover' => $total_turnover,
				'total_dimensions' => $total_dimensions,
				'no_claim' => ($no_claim->no_claim ?? 0)
			]);
			
		}
		
		if ($dimension == TurnoverDimensions::TURNOVER_TYPE)
		{
			$turnovertypes = TurnoverType::all();
			
			$total_dimensions = [];
			
			foreach ($turnovertypes as $id => $turnovertype)
			{
				$total_dimension[$turnovertype] = $turnovertype;
				
				$results = DB::table("invoices")
					->selectRaw("SUM(`inli_amount_netto_eur`) AS `turnover`")
					->leftJoin("invoice_lines", "in_id", "inli_in_id")
					->leftJoin("ledger_accounts", "inli_leac_number", "leac_number")
					->where("leac_turnover_type", $id)
					->whereBetween("in_date",  System::betweenDates($request->date))
					->first();
				
				if($results)
				{
					$total_turnover += $results->turnover;
					$total_dimensions[$turnovertype] = $results->turnover;
				}
				
			}
			
			$no_claim = DB::table("invoices")
				->selectRaw("SUM(`inli_amount_netto_eur`) AS `no_claim`")
				->leftJoin("invoice_lines", "in_id", "inli_in_id")
				->whereBetween("in_date", System::betweenDates($request->date))
				->where("inli_no_claim_discount", 1)
				->first();
			
			return view('finance.turnoverperperiod', [
				'dimensions' => $turnoverdimensions,
				'selected_date' => $request->date,
				'selected_dimension' => $dimension,
				'total_turnover' => $total_turnover,
				'total_dimensions' => $total_dimensions,
				'no_claim' => ($no_claim->no_claim ?? 0)
			]);
			
		}
	}
}
