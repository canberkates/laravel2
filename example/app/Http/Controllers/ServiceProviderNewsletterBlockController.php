<?php

namespace App\Http\Controllers;

use App\Data\CustomerEditForms;
use App\Data\NewsletterSlot;
use App\Models\Country;
use App\Models\Customer;
use App\Models\KTServiceProviderNewsletterBlockCountryDestination;
use App\Models\KTServiceProviderNewsletterBlockCountryOrigin;
use App\Models\KTServiceProviderNewsletterBlockLanguage;
use App\Models\Language;
use App\Models\MacroRegion;
use App\Models\RequestDelivery;
use App\Models\ServiceProviderNewsletterBlock;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class ServiceProviderNewsletterBlockController extends Controller
{
	public function create($customer_id)
	{
		$customer = Customer::findOrFail($customer_id);

		return view('serviceprovidernewsletterblock.create',
			[
				'customer' => $customer,
				'newsletterslots' => NewsletterSlot::all()
			]);
	}

	public function store(Request $request)
	{
		$request->validate([
			'slot' => 'required'
		]);

		$SPnewsletterblock = new ServiceProviderNewsletterBlock();

		$SPnewsletterblock->seprnebl_cu_id = $request->customer_id;
		$SPnewsletterblock->seprnebl_slot = $request->slot;
		$SPnewsletterblock->seprnebl_description = $request->description;

		$SPnewsletterblock->save();

		return redirect('serviceproviders/' . $request->customer_id .'/edit');
	}

	public function edit($customer_id, $SPnewsletterblock_id)
	{
		// get the customer and relations
		$customer = Customer::with(['customerquestions.questions'])->findOrFail($customer_id);

        if (Gate::denies('customer-restrictions', $customer)) {
            abort(403);
        }

		$SPnewsletterblock = ServiceProviderNewsletterBlock::findOrFail($SPnewsletterblock_id);

		$set_languages = KTServiceProviderNewsletterBlockLanguage::where('ktseprneblla_seprnebl_id', $SPnewsletterblock_id)->get();

		$nebl_languages = [];

		foreach($set_languages as $sl)
		{
			$nebl_languages[] .= $sl->ktseprneblla_la_code;
		}

		$countries = [];

		foreach(MacroRegion::all() as $macro_region)
		{
			$macro_regions[$macro_region->mare_continent_name][$macro_region->mare_id] = [
				'mare_name' => $macro_region->mare_name
			];
		}

		$query_countries = Country::orderBy("co_en", "ASC")->get();

		foreach($query_countries as $row_countries)
		{
			$countries[$row_countries->co_mare_id][$row_countries->co_code] = $row_countries->co_en;
		}

		$origins_checked = [];

		$query_checked = KTServiceProviderNewsletterBlockCountryOrigin::where([["ktseprneblcoor_seprnebl_id", "=", $SPnewsletterblock->seprnebl_id]])->get();

		foreach($query_checked as $row_checked)
		{
			$origins_checked[$row_checked->ktseprneblcoor_co_code] = true;
		}

		$destination_checked = [];

		$query_checked = KTServiceProviderNewsletterBlockCountryDestination::where([["ktseprneblcode_seprnebl_id", "=", $SPnewsletterblock->seprnebl_id]])->get();

		foreach($query_checked as $row_checked)
		{
			$destination_checked[$row_checked->ktseprneblcode_co_code] = true;
		}

		return view('serviceprovidernewsletterblock.edit',
			[
				//Customer info
				'newsletterblock_id' => $SPnewsletterblock_id,
				'newsletterblock' => $SPnewsletterblock,
				'customer' => $customer,
				'languages' => Language::where("la_iframe_only", 0)->orderBy('la_language')->get(),
				'nebl_languages' => $nebl_languages,
				'statuses' => [1 => 'Active', 0 => 'Inactive'],
				'newsletterslots' => NewsletterSlot::all(),
				'countries' => $countries,
				'originschecked' => $origins_checked,
				'destinationschecked' => $destination_checked,
				'macro_regions' => $macro_regions
			]);
	}

	public function update(Request $request, $SPnewsletterblock_id)
	{
		$SPnewsletterblock = ServiceProviderNewsletterBlock::findOrFail($SPnewsletterblock_id);

		if ($request->form_name == CustomerEditForms::GENERAL)
		{
			$request->validate([
				'slot' => 'required',
				'status' => 'required',
			]);

			$SPnewsletterblock->seprnebl_slot = $request->slot;
			$SPnewsletterblock->seprnebl_description = $request->description;
			$SPnewsletterblock->seprnebl_status = $request->status;
			$SPnewsletterblock->seprnebl_international_moves = ((isset($request->international_moves)) ? 1 : 0);
			$SPnewsletterblock->seprnebl_national_moves = ((isset($request->national_moves)) ? 1 : 0);

			//Filters Languages
			$languages = Language::where("la_iframe_only", 0)->get();

			foreach($languages as $row_languages)
			{
				if (isset($request->blocklanguages[$row_languages->la_code]))
				{
					//check if this language is already added to this question
					$newsletterblock_lang = KTServiceProviderNewsletterBlockLanguage::where([["ktseprneblla_seprnebl_id", "=", $SPnewsletterblock->seprnebl_id], ["ktseprneblla_la_code", "=", $row_languages->la_code]])->first();

					if (empty($newsletterblock_lang))
					{
						Log::debug($request);
						$kt_service_provider_newsletter_block_language = new KTServiceProviderNewsletterBlockLanguage();
						$kt_service_provider_newsletter_block_language->ktseprneblla_seprnebl_id = $SPnewsletterblock->seprnebl_id;
						$kt_service_provider_newsletter_block_language->ktseprneblla_la_code = $row_languages->la_code;
						$kt_service_provider_newsletter_block_language->save();
					}
				}
				else
				{
					$lang = KTServiceProviderNewsletterBlockLanguage::where("ktseprneblla_seprnebl_id", "=", $SPnewsletterblock->seprnebl_id)->where("ktseprneblla_la_code", "=", $row_languages->la_code)->first();
					if (!empty($lang))
						$lang->delete();
				}
			}

			//Save question
			$SPnewsletterblock->save();
		}
		elseif ($request->form_name == "Origins")
		{
			DB::table('kt_service_provider_newsletter_block_country_origin')->where('ktseprneblcoor_seprnebl_id', '=',$SPnewsletterblock->seprnebl_id)->delete();

			foreach($request->origins as $id => $origin){
				$ktseprneblcoor = new KTServiceProviderNewsletterBlockCountryOrigin();

				$ktseprneblcoor->ktseprneblcoor_seprnebl_id = $SPnewsletterblock->seprnebl_id;
				$ktseprneblcoor->ktseprneblcoor_co_code = $id;

				$ktseprneblcoor->save();
			}
		}
		elseif ($request->form_name == "Destinations")
		{
			DB::table('kt_service_provider_newsletter_block_country_destination')->where('ktseprneblcode_seprnebl_id', '=', $SPnewsletterblock->seprnebl_id)->delete();

			foreach($request->destinations as $id => $origin){
				$ktseprneblcode = new KTServiceProviderNewsletterBlockCountryDestination();

				$ktseprneblcode->ktseprneblcode_seprnebl_id = $SPnewsletterblock->seprnebl_id;
				$ktseprneblcode->ktseprneblcode_co_code = $id;

				$ktseprneblcode->save();
			}

		}
		elseif ($request->form_name == "Content")
		{
			$request->validate([
				'title' => 'required',
				'blockcontent' => 'required',
			]);

			$SPnewsletterblock->seprnebl_title = $request->title;
			$SPnewsletterblock->seprnebl_content = $request->blockcontent;

			$SPnewsletterblock->save();
		}

		return redirect('serviceproviders/' . $SPnewsletterblock->seprnebl_cu_id . '/newsletterblock/'.$SPnewsletterblock->seprnebl_id.'/edit');
	}

	public function requestDeliveryDelete(Request $request)
	{
		$req_delivery = RequestDelivery::findOrFail($request->id);

		$req_delivery->delete();
	}

	public function createRequestdelivery()
	{
		// get the customer
		return view('customers.create',
			[
				'users' => User::where("id", "!=", 0),
				'countries' => Country::all(),
				'movertypes' => CustomerType::all(),
				'debtorstatuses' => DebtorStatus::all(),
				'paymentreminderstatuses' => PaymentReminderStatus::all(),
				'paymentmethods' => PaymentMethod::all(),
				'paymentcurrencies' => PaymentCurrency::all(),
				'ledgeraccounts' => LedgerAccount::all(),
				'languages' => Language::where("la_iframe_only", 0)->get()
			]);
	}

	public function delete(Request $request)
	{
		$newsletter_block = ServiceProviderNewsletterBlock::findOrFail($request->id);

		DB::table('kt_service_provider_newsletter_block_country_destination')->where('ktseprneblcode_seprnebl_id', '=', $newsletter_block->seprnebl_id)->delete();

		DB::table('kt_service_provider_newsletter_block_country_origin')->where('ktseprneblcoor_seprnebl_id', '=', $newsletter_block->seprnebl_id)->delete();

		DB::table('kt_service_provider_newsletter_block_language')->where('ktseprneblla_seprnebl_id', '=', $newsletter_block->seprnebl_id)->delete();

		$newsletter_block->delete();
	}
}
