<?php

namespace App\Http\Controllers;

use App\Data\BothYesNo;
use App\Data\EmptyYesNo;
use App\Data\Survey2Source;
use App\Data\SurveyType;
use App\Models\Country;
use App\Models\Customer;
use App\Models\FlaggedReview;
use App\Models\KTMoverComment;
use App\Models\Survey2;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Functions\System;

class SurveysController extends Controller
{
	public function index()
	{
		//$open_surveys1 = Survey1::with("survey1customers", "request")->where("su_submitted", 1)->where("su_checked", 0)->get();
		$open_surveys2 = Survey2::with(['request', 'website_review'])->where("su_submitted", 1)->where("su_checked", 0)->get();

		//$survey1['active'] = $open_surveys1->where("su_on_hold", "=", 0);
		//$survey1['on_hold'] = $open_surveys1->where("su_on_hold", "=", 1);

		$survey2['active'] = $open_surveys2->where("su_on_hold", "=", 0);
		$survey2['on_hold'] = $open_surveys2->where("su_on_hold", "=", 1);

		$survey2['flagged_reviews'] = FlaggedReview::join("surveys_2", "su_id", "flre_su_id")
            ->where("flre_status", 0)
            ->get();

		$survey2['mover_comments'] = KTMoverComment::leftJoin("surveys_2", "su_id", "ktmoco_su_id")
            ->leftJoin("customers", "cu_id", "ktmoco_cu_id")
            ->where("ktmoco_read", 0)
            ->get();


		return view('surveys.index',
			[
				//'surveys1' => $survey1,
				'surveys2' => $survey2,
				'survey2sources' => Survey2Source::all()
			]);
	}

	public function flaggedReview($frle_id)
	{
	    $flagged_review = FlaggedReview::leftJoin("surveys_2", "su_id", "flre_su_id")
            ->where("flre_id", $frle_id)
            ->first();

		return view('surveys.flagged_review_confirmation',
			[
				'flagged_review' => $flagged_review,
				'survey2sources' => Survey2Source::all()
			]);
	}

	public function approveFlaggedReview(Request $request, $flre_id)
	{
        $flagged_review = FlaggedReview::where("flre_id", $flre_id)->first();

	    if (isset($request->accept))
        {
            $flagged_review->flre_status = 1;
        }
	    elseif (isset($request->deny))
        {
            $flagged_review->flre_status = 2;
        }

        $flagged_review->save();

	    return redirect('surveys/');
	}

	public function search()
	{
		return view('surveys.search',
			[
				'surveytypes' => SurveyType::all(),
				'bothyesno' => BothYesNo::all(),
				'movers' => Customer::where("cu_deleted", 0)->whereRaw("(cu_type = 1 OR cu_type = 6)")->get(),
				'countries' => Country::all(),
				'ratings' => ["" => "All Ratings"] + [1 => "1 star"] + [2 => "2 stars"] + [3 => "3 stars"] + [4 => "4 stars"] + [5 => "5 stars"],
				'surveys' => [],

				//Give back selected values
				'selected_mover' => -1,
				'selected_date' => null,
				'selected_date_from' => null,
				'selected_date_to' => null,
				'selected_checked' => null,
				'selected_survey_id' => null,
				'selected_website_review_id' => null,
				'selected_request_id' => null,
				'selected_moved' => null,
				'selected_show_on_website' => null,
				'selected_recommended' => null,
				'selected_rating' => null,
				'selected_experience_blog' => null,
				'selected_email' => null
			]);
	}

	public function searchResult(Request $request)
	{
		$surveys = DB::table("surveys_2")
			->leftJoin("requests", "su_re_id", "re_id")
			->leftJoin("website_reviews", "su_were_id", "were_id")
			->leftJoin("customers", "su_mover", "cu_id");

		if( ! empty( $request->date ) ) {

		    $surveys->whereBetween( 'su_submitted_timestamp', System::betweenDates( $request->date ) );
        }

		if(!empty($request->survey_id))
		{
			$surveys->where("su_id", $request->survey_id);
		}

		if(!empty($request->experience_blog))
		{
			$surveys->where("su_experience_blog", $request->experience_blog);
		}

		if($request->proof_files === "0" || $request->proof_files === "1")
		{
		    if ($request->proof_files === "1") {
                $surveys->whereNotNull('were_proof_files');
            }
		    elseif ($request->proof_files === "0") {
                $surveys->whereNull('were_proof_files');
            }
		}

		if(!empty($request->request_id))
		{
			$surveys->where("su_re_id", $request->request_id);
		}

		if(!empty($request->website_review_id)){
			$surveys->where("su_were_id", $request->website_review_id);
		}

		if($request->mover != -1)
		{
			$surveys->whereRaw("
				(
					`su_mover` = ".$request->mover." OR
					`su_other_mover` = ".$request->mover."
				)
			");
		}

		if(!empty($request->moved))
		{
			$surveys->where("su_moved", $request->moved);
		}

		if(!empty($request->recommended))
		{
			$surveys->where("su_recommend_mover", $request->recommended);
		}

		if ($request->show_on_website === null) {
            $surveys->whereIn("su_show_on_website", [0,1,2]);
        }
        else if ($request->show_on_website == 1) {
            $surveys->where("su_show_on_website", 1);
        }
        else {
            $surveys->where("su_show_on_website", 2);

        }

		if(!empty($request->checked))
		{
			$surveys->where("su_checked", $request->checked);
		}

		if(!empty($request->rating))
		{
			$surveys->where("su_rating", $request->rating);
		}

        if(!empty($request->email))
        {
            $surveys->whereRaw("(`re_email` = '".$request->email."' OR `were_email` = '".$request->email."')");
        }

		$surveys = $surveys
			->get();

		return view('surveys.search',
			[
				'surveytypes' => SurveyType::all(),
				'bothyesno' => BothYesNo::all(),
				'movers' => Customer::where("cu_deleted", 0)->whereRaw("(cu_type = 1 OR cu_type = 6)")->get(),
				'countries' => Country::all(),
				'ratings' => ["" => "All Ratings"] + [1 => "1 star"] + [2 => "2 stars"] + [3 => "3 stars"] + [4 => "4 stars"] + [5 => "5 stars"],
				'surveys' => $surveys,
				'surveysources' => Survey2Source::all(),

				//Give back selected values
				'selected_mover' => $request->mover,
				'selected_date' => $request->date,
				'selected_date_from' => $request->date_from,
				'selected_date_to' => $request->date_to,
				'selected_checked' => ($request->checked === null ? null : intval($request->checked)),
				'selected_survey_id' => $request->survey_id,
				'selected_website_review_id' => $request->website_review_id,
				'selected_request_id' => $request->request_id,
				'selected_moved' => ($request->moved === null ? null : intval($request->moved)),
				'selected_show_on_website' => ($request->show_on_website === null ? null : intval($request->show_on_website)),
				'selected_recommended' => ($request->recommended === null ? null : intval($request->recommended)),
				'selected_rating' => $request->rating,
				'selected_experience_blog' => ($request->experience_blog === null ? null : intval($request->experience_blog)),
                'selected_email' => $request->email,
                'selected_proof_files' => $request->proof_files

            ]);
	}

    public function readMoverComment(Request $request)
    {
        $kt_mover_comment = KTMoverComment::where("ktmoco_id", $request->id)->first();
        $kt_mover_comment->ktmoco_read = 1;
        $kt_mover_comment->save();

        return json_encode(['success']);
    }
}
