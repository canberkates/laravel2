<?php

namespace App\Http\Controllers;

use App\Data\BothYesNo;
use App\Models\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class MessagesController extends Controller
{
	public function index()
	{
		$messages = Message::where([["mes_read", "=", 0], ["mes_us_id", "=", Auth::id()]])->get();
		
		return view('messages.index', ['messages' => $messages, 'bothyesno' => BothYesNo::all()]);
	}
	
	public function filteredindex(Request $request)
	{
		$read = 0;
		
		//Extend with:
		if (isset($request->read) && $request->read != "")
		{
			$read = $request->read;
		}
		
		$messages = Message::where([["mes_read", "=", $read], ["mes_us_id", "=", Auth::id()]])->get();
		
		return view('messages.index', ['messages' => $messages, 'bothyesno' => BothYesNo::all()]);
	}
	
	
	public function readallmessages()
	{
		DB::table('messages')
			->where('mes_us_id', Auth::id())
			->update(
				[
					'mes_read' => 1
				]
			);
		
		return Redirect::to('messages');
	}
	
	public function markAsRead(Request $request)
	{
		//Get the message
		$message = Message::findOrFail($request->id);
		
		//Mark as read
		$message->mes_read = 1;
		
		//Save the message
		$message->save();
	}
}
