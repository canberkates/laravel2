<?php

namespace App\Http\Controllers;

use App\Functions\DataIndex;
use App\Functions\System;
use App\Models\LedgerAccount;
use DB;
use Illuminate\Http\Request;

class TurnoverJournalEntryController extends Controller
{
	public function index()
	{
		
		return view('finance.turnoverjournalentry', [
			'selected_date' => "",
			'date_filter' => [],
			'filtered_data' => []
		]);
	}
	
	public function filteredIndex(Request $request)
	{
		$request->validate([
			'date' => 'required'
		]);
		
		$total = 0;
		$total_vat = 0;
		$amounts = [];
		$amounts_vat = [];
		
		$ledgers = LedgerAccount::where("leac_type", 1)->get();
		
		foreach($ledgers as $leac)
		{
			$amounts[$leac->leac_number] = 0;
			$amounts_vat[$leac->leac_number] = 0;
		}
		
		$invoices = DB::table("invoices")
			->leftJoin("invoice_lines","in_id","inli_in_id")
			->select("in_vat_percentage")
			->selectRaw("inli_leac_number AS `number`")
			->selectRaw("SUM(`inli_amount_netto_eur`) AS `amount`")
			->whereBetween("in_date", System::betweenDates($request->date))
			->groupBy("inli_leac_number")
			->get();
		
		foreach($invoices as $invoice)
		{
			$amounts[$invoice->number] = $invoice->amount;
			
			$ledgerVAT = LedgerAccount::where("leac_number", $invoice->number)->first();
			
			if($ledgerVAT->leac_vat > 0)
			{
				$amounts_vat[$invoice->number] = ($invoice->amount / 100) * $invoice->in_vat_percentage;
			}
			
			$total += $amounts[$invoice->number];
			$total_vat += $amounts_vat[$invoice->number];
		}
		
		return view('finance.turnoverjournalentry', [
			'selected_date' => $request->date,
			'ledgers' => $ledgers,
			'total' => $total,
			'total_vat' => $total_vat,
			'amount_vat' => $amounts_vat,
			'amount_deb' => $amounts
			
		]);
		
	}
	
}
