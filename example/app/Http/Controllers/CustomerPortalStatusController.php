<?php

namespace App\Http\Controllers;

use App\Data\CustomerPairStatus;
use App\Functions\System;
use App\Models\KTCustomerPortal;
use App\Models\StatusUpdate;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;

class CustomerPortalStatusController extends Controller
{
	public function create($customer_id, $customerportal_id)
	{
		$customerportal = KTCustomerPortal::with("customer")->find($customerportal_id);

		$system = new System();
		$hour_dropdown = $system->dropdownOptions(0,23,1, false);

		return view('customerportalstatus.create',
			[
				'customer' => $customerportal->customer,
				'portal' => $customerportal,
				'portalstatuses' => CustomerPairStatus::all(),
				'hour_dropdown' => $hour_dropdown,
				'users' => User::where([['us_is_deleted', '=', 0]])->get()
			]);
	}

	public function store(Request $request)
	{
		$request->validate([
			'status' => 'required',
			'date' => 'required',
			'hour' => 'required'
		]);

		$customerportal = KTCustomerPortal::with("customer")->find($request->ktcupo_id);

		//Create new Status Update
		$statusupdate = new StatusUpdate();

		$statusupdate->stup_us_id = Auth::id();
		$statusupdate->stup_ktcupo_id = $request->ktcupo_id;
		$statusupdate->stup_status = $request->status;
		$statusupdate->stup_date = $request->date;
		$statusupdate->stup_hour = $request->hour;
		$statusupdate->stup_description = $request->description;
		if(!empty($request->users))
		{
			$statusupdate->stup_users = implode(',', array_keys($request->users));
		}

		$statusupdate->save();

		if($customerportal->customer->cu_type == 6)
        {
            return redirect('resellers/' . $customerportal->customer->cu_id . '/customerportal/' . $customerportal->ktcupo_id . '/edit');
        }
		else {
            return redirect('customers/' . $customerportal->customer->cu_id . '/customerportal/' . $customerportal->ktcupo_id . '/edit');
        }

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\MoverData $moverData
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($customerportalstatus_id)
	{
		$customerportalstatus = StatusUpdate::find($customerportalstatus_id);
		$customerportalstatus->delete();

		return redirect()->back();
	}
}
