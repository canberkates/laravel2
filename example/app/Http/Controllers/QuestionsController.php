<?php

namespace App\Http\Controllers;

use App\Data\questionExtraType;
use App\Models\Language;
use App\Models\Question;
use Illuminate\Http\Request;

class QuestionsController extends Controller
{
	public function index()
	{
		$questions = Question::all();
		
		return view('question.index',
		[
			'questions' => $questions
		]);
	}
	
	public function edit($question_id)
	{
		$question = Question::findOrFail($question_id);
        
        $un_title = unserialize(base64_decode($question->qu_title));
        $un_question = unserialize(base64_decode($question->qu_question));
        
        $extra_1_questions = unserialize(base64_decode($question->qu_extra_1_question));
        $extra_1_options = unserialize(base64_decode($question->qu_extra_1_options));
        
        $extra_2_questions = unserialize(base64_decode($question->qu_extra_2_question));
        $extra_2_options = unserialize(base64_decode($question->qu_extra_2_options));
		
		return view('question.edit',
			[
				'question' => $question,
				'un_title' => $un_title,
				'un_question' => $un_question,
				'questionextratypes' => questionExtraType::all(),
				'languages' => Language::whereIn("la_code", ["NL", "DK", "DE", "IT", "ES", "FR", "EN"])->get(),
                'extra_1_questions' => $extra_1_questions,
                'extra_1_options' => $extra_1_options,
                'extra_2_questions' => $extra_2_questions,
                'extra_2_options' => $extra_2_options
                
			]);
		
	}
	
	public function update(Request $request, $question_id)
	{
		$request->validate([
			'name' => 'required',
			'titles' => 'required',
            'questions' => 'required',
            'limit' => 'required'
		]);
		
		$question = Question::findOrFail($question_id);
  
		//Basics
        $question->qu_name = $request->name;
        $question->qu_title = base64_encode(serialize($request->titles));
        $question->qu_question = base64_encode(serialize($request->questions));
        $question->qu_class = $request->class;
        $question->qu_limit = $request->limit;
        
        //Extra 1
        $question->qu_extra_1_type = $request->extra_1_type;
        $question->qu_extra_1_question =  base64_encode(serialize($request->extra_1_question));
        $question->qu_extra_1_options =  base64_encode(serialize($request->extra_1_option));
        $question->qu_extra_1_class_front = $request->extra_1_class_front;
        $question->qu_extra_1_class_back = $request->extra_1_class_back;
        
        //Extra 2
        $question->qu_extra_2_type = $request->extra_2_type;
        $question->qu_extra_2_question =  base64_encode(serialize($request->extra_2_question));
        $question->qu_extra_2_options =  base64_encode(serialize($request->extra_2_option));
        $question->qu_extra_2_class_front = $request->extra_2_class_front;
        $question->qu_extra_2_class_back = $request->extra_2_class_back;
        
        $question->save();
		
		return redirect('admin/questions/' . $question_id . '/edit');
	}
	
	public function create()
	{
		
		return view('question.create',
			[
                'questionextratypes' => questionExtraType::all(),
				'languages' => Language::whereIn("la_code", ["NL", "DK", "DE", "IT", "ES", "FR", "EN"])->get()
			]);
		
	}
	
	public function store(Request $request)
	{
        $request->validate([
            'name' => 'required',
            'titles' => 'required',
            'questions' => 'required',
            'limit' => 'required'
        ]);
        
        $question = new Question();
        
        //Basics
        $question->qu_name = $request->name;
        $question->qu_title = base64_encode(serialize($request->titles));
        $question->qu_question = base64_encode(serialize($request->questions));
        $question->qu_class = $request->class ?? "";
        $question->qu_limit = $request->limit;
        
        //Extra 1
        $question->qu_extra_1_type = $request->extra_1_type;
        $question->qu_extra_1_question =  base64_encode(serialize($request->extra_1_question));
        $question->qu_extra_1_options =  base64_encode(serialize($request->extra_1_option));
        $question->qu_extra_1_class_front = $request->extra_1_class_front ?? "";
        $question->qu_extra_1_class_back = $request->extra_1_class_back ?? "";
        
        //Extra 2
        $question->qu_extra_2_type = $request->extra_2_type;
        $question->qu_extra_2_question =  base64_encode(serialize($request->extra_2_question));
        $question->qu_extra_2_options =  base64_encode(serialize($request->extra_2_option));
        $question->qu_extra_2_class_front = $request->extra_2_class_front ?? "";
        $question->qu_extra_2_class_back = $request->extra_2_class_back ?? "";
        
        $question->save();
		
		return redirect('admin/questions/');
		
	}
}
