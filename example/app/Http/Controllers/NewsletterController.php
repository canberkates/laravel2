<?php

namespace App\Http\Controllers;

use App\Data\NewsletterSlot;
use App\Models\Language;
use App\Models\Newsletter;
use Illuminate\Http\Request;

class NewsletterController extends Controller
{
	public function index()
	{
		$newsletters = Newsletter::all();
		
		return view('newsletter.index',
		[
			'newsletters' => $newsletters
		]);
	}
	
	public function edit($newsletter_id)
	{
		$newsletter = Newsletter::findOrFail($newsletter_id);
		
		$singular = unserialize(base64_decode($newsletter->new_subject_singular));
		
		$plural = unserialize(base64_decode($newsletter->new_subject_plural));
		
		$intro = unserialize(base64_decode($newsletter->new_intro));
		
		return view('newsletter.edit',
			[
				'newsletter' => $newsletter,
				'singular' => $singular,
				'plural' => $plural,
				'intro' => $intro,
				'slots' => NewsletterSlot::all(),
				'languages' => Language::whereIn("la_code", ["NL", "DK", "DE", "IT", "ES", "FR", "EN"])->get()
			]);
		
	}
	
	public function update(Request $request, $newsletter_id)
	{
		$request->validate([
			'name' => 'required',
			'delay' => 'required',
			'subject_singular' => 'required',
			'subject_plural' => 'required',
		]);
		
		$newsletter = Newsletter::findOrFail($newsletter_id);
		
		$newsletter->new_name = $request->name;
		$newsletter->new_delay = $request->delay;
		$newsletter->new_subject_singular = base64_encode(serialize($request->subject_singular));
		$newsletter->new_subject_plural = base64_encode(serialize($request->subject_plural));
		$newsletter->new_intro = base64_encode(serialize($request->intro));
		$newsletter->new_slot_1 = $request->slot_1;
		$newsletter->new_slot_2 = $request->slot_2;
		$newsletter->new_slot_3 = $request->slot_3;
		$newsletter->new_slot_4 = $request->slot_4;
		$newsletter->new_slot_5 = $request->slot_5;
		$newsletter->new_slot_6 = $request->slot_6;
		$newsletter->new_slot_7 = $request->slot_7;
		$newsletter->new_slot_8 = $request->slot_8;
		$newsletter->new_slot_9 = $request->slot_9;
		$newsletter->new_slot_10 = $request->slot_10;
		
		$newsletter->save();
		
		return redirect('admin/newsletters/' . $newsletter_id . '/edit');
	}
	
	public function create()
	{
		
		return view('newsletter.create',
			[
				'slots' => NewsletterSlot::all(),
				'languages' => Language::whereIn("la_code", ["NL", "DK", "DE", "IT", "ES", "FR", "EN"])->get()
			]);
		
	}
	
	public function store(Request $request)
	{
		$request->validate([
			'name' => 'required',
			'delay' => 'required',
			'subject_singular' => 'required',
			'subject_plural' => 'required',
		]);
		
		$newsletter = new Newsletter();
		
		$newsletter->new_name = $request->name;
		$newsletter->new_delay = $request->delay;
		$newsletter->new_subject_singular = base64_encode(serialize($request->subject_singular));
		$newsletter->new_subject_plural = base64_encode(serialize($request->subject_plural));
		$newsletter->new_intro = base64_encode(serialize($request->intro));
		$newsletter->new_slot_1 = $request->slot_1;
		$newsletter->new_slot_2 = $request->slot_2;
		$newsletter->new_slot_3 = $request->slot_3;
		$newsletter->new_slot_4 = $request->slot_4;
		$newsletter->new_slot_5 = $request->slot_5;
		$newsletter->new_slot_6 = $request->slot_6;
		$newsletter->new_slot_7 = $request->slot_7;
		$newsletter->new_slot_8 = $request->slot_8;
		$newsletter->new_slot_9 = $request->slot_9;
		$newsletter->new_slot_10 = $request->slot_10;
		$newsletter->new_deleted = 0;
		
		$newsletter->save();
		
		return redirect('admin/newsletters/');
		
	}
}
