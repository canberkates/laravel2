<?php

namespace App\Http\Controllers;

use App\Models\ProConCategory;

class ProsConsController extends Controller
{
	public function index()
	{
		$procons = ProConCategory::all();
		
		return view('procons.index',
			[
				'procons' => $procons,
			]);
	}
	
	
	
}
