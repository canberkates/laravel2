<?php

namespace App\Http\Controllers;

use App\Models\KTUserReport;
use App\Models\Report;
use App\Models\User;
use Auth;
use DB;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Log;
use Spatie\Permission\Models\Permission;

class UserController extends Controller
{
    public function index()
    {
        $users = User::where("us_is_deleted", 0)->get();

        return view('users.index',
            [
                'users' => $users
            ]);
    }

    public function create()
    {
        return view('users.create', [
            'permissions' => Permission::all(),
            'reports' => Report::all()->sortBy("rep_name"),
        ]);
    }

    public function store(Request $request)
    {
        $user = new User();

        $user->name = $request->name;
        $user->email = $request->email;
        $user->us_username = $request->username;

        if($request->password)
        {
            $user->password = Hash::make($request->password);
        }
        $user->us_name = $request->name;
        $user->us_email = $request->email;
        $user->us_date_of_birth = $request->date_of_birth;
        $user->us_telephone_internal = $request->telephone_internal;
        $user->us_telephone_external = $request->telephone_external;
        $user->us_mobile = $request->mobile;

        //Save general block
        $user->us_sales = Arr::exists($request, 'department_sales') ? 1 : 0;
        $user->us_partnerdesk = Arr::exists($request, 'department_partnerdesk') ? 1 : 0;
        $user->us_customerservice = Arr::exists($request, 'department_customerservice') ? 1 : 0;
        $user->us_finance = Arr::exists($request, 'department_finance') ? 1 : 0;
        $user->us_sirelo = Arr::exists($request, 'department_sirelo') ? 1 : 0;

        $user->us_cafe_planner = Arr::exists($request, 'cafe_planner') ? 1 : 0;

        $user->save();

        if($request->permission)
        {
            foreach($request->permission as $permission => $on)
            {
                $user->givePermissionTo(Permission::find($permission));
            }
        }

        if($request->report)
        {
            $reports = Report::all();

            foreach($reports as $id => $rep)
            {
                if(isset($request->report[$rep->rep_id]))
                {
                    $rep_finder = KTUserReport::where("ktusre_us_id", "=", $user->id)->where("ktusre_rep_id", "=", $rep->rep_id)->first();

                    if(count($rep_finder) == 0)
                    {
                        $new_ktusre = new KTUserReport();
                        $new_ktusre->ktusre_us_id = $user->id;
                        $new_ktusre->ktusre_rep_id = $rep->rep_id;
                        $new_ktusre->save();
                    }
                }
            }
        }

        //SET ID the same as US_ID if id=0 (so for new users)
        DB::table("users")
            ->where('id', 0)
            ->update([
                'id' => DB::raw("`us_id`")
            ]);

        return redirect('admin/users/');

    }

    public function edit($user_id)
    {
        $reports_of_user = KTUserReport::select("ktusre_rep_id")->where("ktusre_us_id", "=", $user_id)->get();
        $user_reports = [];
        foreach($reports_of_user as $rep)
        {
            array_push($user_reports, $rep->ktusre_rep_id);
        }

        return view('users.edit',
            [
                'user' => User::find($user_id),
                'permissions' => Permission::all(),
                'reports' => Report::all()->sortBy("rep_name"),
                'user_reports' => $user_reports
            ]);

    }

    public function update(Request $request, $user_id)
    {
        DB::table('model_has_permissions')->where('model_id', '=', $user_id)->where('model_type', '=', 'App\\Models\\User')->delete();

        $user = User::findOrFail($user_id);

        if($request->permission)
        {
            foreach($request->permission as $permission => $on)
            {
                $user->givePermissionTo(Permission::find($permission));
            }
        }

        if($request->report)
        {
            $reports = Report::all();

            Log::debug($reports);

            foreach($reports as $id => $rep)
            {
                if(isset($request->report[$rep->rep_id]))
                {
                    $rep_finder = KTUserReport::where("ktusre_us_id", "=", $user_id)->where("ktusre_rep_id", "=", $rep->rep_id)->first();

                    if(count($rep_finder) == 0)
                    {
                        $new_ktusre = new KTUserReport();
                        $new_ktusre->ktusre_us_id = $user->id;
                        $new_ktusre->ktusre_rep_id = $rep->rep_id;
                        $new_ktusre->save();
                    }
                }
                else
                {
                    DB::table("kt_user_report")->where("ktusre_us_id", "=", $user_id)->where("ktusre_rep_id", "=", $rep->rep_id)->delete();
                }
            }
        }
        else
        {
            DB::table("kt_user_report")->where("ktusre_us_id", "=", $user_id)->delete();
        }

        $user->name = $request->username;
        $user->email = $request->username;
        $user->us_username = $request->username;

        if($request->password)
        {
            $user->password = Hash::make($request->password);
        }
        $user->us_name = $request->name;
        $user->us_email = $request->email;
        $user->us_date_of_birth = $request->date_of_birth;
        $user->us_telephone_internal = $request->telephone_internal;
        $user->us_telephone_external = $request->telephone_external;
        $user->us_mobile = $request->mobile;

        //Save general block
        $user->us_sales = Arr::exists($request, 'department_sales') ? 1 : 0;
        $user->us_partnerdesk = Arr::exists($request, 'department_partnerdesk') ? 1 : 0;
        $user->us_customerservice = Arr::exists($request, 'department_customerservice') ? 1 : 0;
        $user->us_finance = Arr::exists($request, 'department_finance') ? 1 : 0;
        $user->us_sirelo = Arr::exists($request, 'department_sirelo') ? 1 : 0;

        $user->us_cafe_planner = Arr::exists($request, 'cafe_planner') ? 1 : 0;

        $user->save();

        return redirect('admin/users/'.$user_id.'/edit');
    }

    public function changePassword(Request $request)
    {
        // Get current user
        $user = User::findOrFail($request->us_id);
        $success = true;

        // If current password is not empty
        if($request->current_password != '')
        {

            // If current password is correct
            if(Hash::check($request->current_password, $user->password))
            {

                // If the length is 7 or more
                if(strlen($request->new_password) >= 7)
                {

                    // If new password and the new password confirm are the same
                    if($request->new_password == $request->new_password_confirm)
                    {

                        $bool = false;

                        //Define the file of most used passwords
                        $filename = env('SHARED_FOLDER')."/most_used_passwords_100k.txt";
                        $contents = file($filename);

                        //Loop through the file with most used passwords
                        foreach($contents as $line) {
                            if ($request->new_password == trim($line)){
                                //There is a match!
                                $bool = true;
                            }
                        }

                        if ($bool)
                        {
                            $message = "You can't use this password. This password is in the most used password list. Please use another password.";
                        }
                        else {
                            // Set the new password to the user
                            $user->password = Hash::make($request->new_password);

                            // Save it
                            $user->save();

                            // Set the message
                            $message = 'Your password has been changed! You will be logged out within 3 seconds..';
                        }
                    }
                    else
                    {

                        $success = false;
                        $message = "The new passwords don't match.";
                    }
                }
                else
                {

                    $success = false;
                    $message = 'The minimum length is 7 characters.';
                }
            }
            else
            {

                $success = false;
                $message = 'Current password is not correct.';
            }
        }
        else
        {

            $success = false;
            $message = 'Fill in your current password.';
        }

        return response()->json([
            'message' => $message,
            'success' => $success,
        ]);
    }

    public function forcePasswordChange()
    {
        return view('users.force_change_password',
            [
                'user' => User::find(Auth::user()->id)
            ]);

    }

    public function forcePasswordChangePost(Request $request, $user_id)
    {
        $user = User::findOrFail($user_id);

        if($request->password)
        {
            if(strlen($request->password) >= 7)
            {
                //Define the file of most used passwords
                $filename = env('SHARED_FOLDER')."/most_used_passwords_100k.txt";
                $contents = file($filename);

                //Loop through the file with most used passwords
                foreach($contents as $line) {
                    if ($request->password == trim($line)){
                        //There is a match!
                        return redirect()->back()->withInput()->withErrors([ 'password'=> "You can't use this password. This password is in the most used password list. Please use another password."]);
                    }
                }

                //Set password
                $user->password = Hash::make($request->password);
                $user->us_force_change_password = 1;
            }
            else {
                return redirect()->back()->withInput()->withErrors([ 'password'=> "The minimum length is 7 characters."]);
            }
        }

        $user->save();

        return redirect('/dashboard');
    }
}
