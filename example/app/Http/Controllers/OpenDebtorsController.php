<?php

namespace App\Http\Controllers;

use App\Data\CreditDebitType;
use App\Data\DebtorStatus;
use App\Data\PaymentMethod;
use App\Data\PaymentReminder;
use App\Functions\System;
use App\Models\PaymentCurrency;
use App\Models\User;
use DateTime;
use DB;
use Illuminate\Http\Request;
use Log;

class OpenDebtorsController extends Controller
{
	public function index()
	{

		return view('finance.opendebtors', [
			'state_per' => ""
		]);
	}

	public function filteredIndex(Request $request)
	{
		$request->validate([
			'state_per' => 'required'
		]);

		Log::debug("OPEN DEBTOR START");
		$system = new System();

		$customers = [];

		$query_customers = DB::table("customers")
			->select("cu_id", "cu_company_name_legal", "cu_use_billing_address", "cu_co_code",
			"cu_city", "cu_co_code_bi", "cu_city_bi", "cu_debtor_status", "cu_credit_hold", "cu_credit_hold_timestamp", "cu_debt_manager",
			"cu_payment_method", "cu_credit_card_amex")
			->where("cu_deleted", 0)
			->whereIn("cu_type", [1,2,4,6])
			->orderBy("cu_company_name_legal", 'asc')
			->get();

		foreach($query_customers as $row_customers)
		{
		    if ($row_customers->cu_id == 3269){
		        Log::debug("CU ID 3268");
            }
			$customers[$row_customers->cu_id] = [
				"customer" => $system->databaseToArray($row_customers),
				"status" => "",
				"invoices" => [],
				"data" => [
					"balance" => 0,
					"0" => 0,
					"0-30" => 0,
					"30+" => 0
				]
			];
		}

		$query_invoices = DB::table("invoices")
			->select("in_id", "in_cu_id", "in_expiration_date", "in_payment_reminder",
			"in_payment_reminder_date", "in_amount_netto_eur")
			->where("in_date", "<=" , date("Y-m-d", strtotime($request->state_per)))
			->whereIn("in_cu_id", array_keys($customers))
			->get();

		foreach($query_invoices as $row_invoices)
		{
			if($row_invoices->in_expiration_date > date("Y-m-d", strtotime($request->state_per)))
			{
				$row_invoices->key = "0";
			}
			elseif($row_invoices->in_expiration_date >= date("Y-m-d", strtotime($request->state_per." -30 days")) && $row_invoices->in_expiration_date <= date("Y-m-d", strtotime($request->state_per)))
			{
				$row_invoices->key = "0-30";
			}
			elseif($row_invoices->in_expiration_date < date("Y-m-d", strtotime($request->state_per." -30 days")))
			{
				$row_invoices->key = "30+";
			}
			else
			{
				$row_invoices->key = "";
			}

			$row_invoices->amount = $row_invoices->in_amount_netto_eur;

			$customers[$row_invoices->in_cu_id]['data']['balance'] -= $row_invoices->in_amount_netto_eur;
			$customers[$row_invoices->in_cu_id]['data'][$row_invoices->key] -= $row_invoices->in_amount_netto_eur;
			$customers[$row_invoices->in_cu_id]['invoices'][$row_invoices->in_id] = $system->databaseToArray($row_invoices);
		}

		Log::debug("CUSTOMERDATA");
        Log::debug($customers[3269]);

		$query_bank_lines = DB::table("kt_bank_line_invoice_customer_ledger_account")
			->select("ktbaliinculeac_cu_id", "ktbaliinculeac_in_id", "ktbaliinculeac_amount")
			->leftJoin("bank_lines","ktbaliinculeac_bali_id","bali_id")
			->whereIn("ktbaliinculeac_cu_id", array_keys($customers))
			->whereRaw("
			(
				(
					`ktbaliinculeac_bali_id` IS NOT NULL AND
					`bali_date` <= '".date("Y-m-d", strtotime($request->state_per))."'
				)
				OR
				(
					`ktbaliinculeac_bali_id` IS NULL AND
					`ktbaliinculeac_date` <= '".date("Y-m-d", strtotime($request->state_per))."'
				)
			)")
			->get();

		foreach($query_bank_lines as $row_bank_lines)
		{
			$customers[$row_bank_lines->ktbaliinculeac_cu_id]['data']['balance'] += $row_bank_lines->ktbaliinculeac_amount;

			if($row_bank_lines->ktbaliinculeac_in_id != null && isset($customers[$row_bank_lines->ktbaliinculeac_cu_id]['invoices'][$row_bank_lines->ktbaliinculeac_in_id]))
			{
				$customers[$row_bank_lines->ktbaliinculeac_cu_id]['data'][$customers[$row_bank_lines->ktbaliinculeac_cu_id]['invoices'][$row_bank_lines->ktbaliinculeac_in_id]['key']] += $row_bank_lines->ktbaliinculeac_amount;
				$customers[$row_bank_lines->ktbaliinculeac_cu_id]['invoices'][$row_bank_lines->ktbaliinculeac_in_id]['amount'] -= $row_bank_lines->ktbaliinculeac_amount;
			}
			else
			{
				$customers[$row_bank_lines->ktbaliinculeac_cu_id]['data']['0'] += $row_bank_lines->ktbaliinculeac_amount;
			}
		}

        Log::debug("CUSTOMERDATA2");
        Log::debug($customers[3269]);

		$totals = [
			"total_outstanding_invoices" => 0,
			"total_amount_0" => 0,
			"total_amount_0_30" => 0,
			"total_amount_30_plus" => 0,
			"total_balance" => 0,
		];



		foreach($customers as $cu_id => $customer)
		{
			$outstanding_invoices = 0;
			$reminder = 0;
			$reminder_date = "";

			foreach($customer['invoices'] as $invoice)
			{
				if(round($invoice['amount'], 2) != 0)
				{
					$outstanding_invoices++;

					if($invoice['in_payment_reminder'] > $reminder)
					{
						$reminder = $invoice['in_payment_reminder'];
						$reminder_date = $invoice['in_payment_reminder_date'];
					}
				}
			}

			$balance = round($customer['data']['balance'], 2);
			$amount_0 = round($customer['data']['0'], 2);
			$amount_0_30 = round($customer['data']['0-30'], 2);
			$amount_30_plus = round($customer['data']['30+'], 2);

			//These are not printed
			if($balance == 0 || $outstanding_invoices < $request->min_outstanding){
                if ($cu_id == 3269){
                    Log::debug("CU ID 3269 UNSET ");
                }
				unset($customers[$cu_id]);
				continue;
			}

			$customers[$cu_id]['customer']['days_difference'] = "";
			//Calculate how many days a customer is on credithold --> If customer is on credithold
			if ($customer['customer']['cu_credit_hold'] == 1 && $customer['customer']['cu_credit_hold_timestamp'] != "0000-00-00 00:00:00")
			{
				$credithold_timestamp = $customer['customer']['cu_credit_hold_timestamp'];
				$now_timestamp = date("Y-m-d H:i:s");

				$date1 = new DateTime(date('Y-m-d', strtotime($credithold_timestamp)));
				$date2 = new DateTime(date('Y-m-d', strtotime($now_timestamp)));

				$customers[$cu_id]['customer']['days_difference'] = $date1->diff($date2)->days;

			}


			$query_customer_remark = DB::table("customer_remarks")
				->where("cure_cu_id",$customer['customer']['cu_id'])
				->where("cure_department",2)
				->orderBy("cure_timestamp", 'desc')
				->first();

			$customers[$cu_id]['customer']['remark']['timestamp'] = "";
			$customers[$cu_id]['customer']['remark']['text'] = "-";

			if(!empty($query_customer_remark))
			{
				$remarktext = $query_customer_remark->cure_text;
				if($query_customer_remark->cure_status_id != 0)
				{
					$get_status_info = DB::table("customer_statuses")
						->where("cust_id", $query_customer_remark->cure_status_id)
						->first();

					$remarktext = $system->customerRemarkTable($get_status_info);
				}
				$customers[$cu_id]['customer']['remark']['timestamp'] = date("Y-m-d", strtotime($query_customer_remark->cure_timestamp));
				$customers[$cu_id]['customer']['remark']['text'] = $remarktext;
			}

			$debt_manager = User::find($customer['customer']['cu_debt_manager']);

			$payment_reminder = PaymentReminder::name($reminder);

			if($reminder > 0){
				$payment_reminder .= "({$reminder_date})";
			}


			$customers[$cu_id]['customer']['status'] = $system->getCustomerStatus($customer['customer']['cu_id']);
			$customers[$cu_id]['customer']['debt_manager'] = $debt_manager ? $debt_manager->us_name : "";
			$customers[$cu_id]['customer']['debtor_status'] = DebtorStatus::name($customer['customer']['cu_debtor_status']);
			$customers[$cu_id]['customer']['payment_method'] = PaymentMethod::name($customer['customer']['cu_payment_method']);
			$customers[$cu_id]['customer']['outstanding_invoices'] = $outstanding_invoices;
			$customers[$cu_id]['customer']['payment_reminder'] = $payment_reminder;

			$totals['total_outstanding_invoices'] += $outstanding_invoices;
			$totals['total_amount_0'] += $amount_0;
			$totals['total_amount_0_30'] += $amount_0_30;
			$totals['total_amount_30_plus'] += $amount_30_plus;
			$totals['total_balance'] += $balance;

		}

        Log::debug("CUSTOMERDATA3");
        Log::debug($customers[3269]);

		$currency_tokens = [];

		$currencies = PaymentCurrency::all();

		foreach ($currencies as $currency)
		{
			$currency_tokens[$currency->pacu_code] = $currency->pacu_token;
		}

		return view('finance.opendebtors', [
			'selected_date' => $request->date,
			'currency_tokens' => $currency_tokens,
			'state_per' => $request->state_per,
			'customers' => $customers,
			'totals' => $totals,
			'credit_debit_types' => CreditDebitType::all()
		]);

	}

	public function getInvoices(Request $request)
	{
		$system = new System();

		$exp = explode('&state_per=', $request->id);
		$id = $exp[0];
		$state_per = $exp[1];

		$query = DB::table('invoices')
			->where('in_cu_id',$id)
			->where('in_date', '<=' , date('Y-m-d', strtotime($state_per)))
			->get();

		Log::debug($query);

		$html = '';
		$html .=  '<table class="table display">';
		$html .=  '<thead>';
		$html .=  '<tr>';
		$html .=  '<th>Invoice number</th>';
		$html .=  '<th>Invoice date</th>';
		$html .=  '<th>Expiration date</th>';
		$html .=  '<th>Days overdue</th>';
		$html .=  '<th>Payment method</th>';
		$html .=  '<th>Amount incl. VAT FC</th>';
		$html .=  '<th>Amount paid</th>';
		$html .=  '<th>Amount left</th>';
		$html .=  '<th>Payment date(s)</th>';
		$html .=  '</tr>';
		$html .=  '</thead>';
		$html .=  '<tbody>';

		foreach($query as $row)
		{
			$query_bali = DB::table('kt_bank_line_invoice_customer_ledger_account')
				->select('ktbaliinculeac_bali_id', 'ktbaliinculeac_date', 'ktbaliinculeac_amount', 'bali_date')
				->leftJoin('bank_lines', 'ktbaliinculeac_bali_id', 'bali_id')
				->where('ktbaliinculeac_in_id', $row->in_id)
				->whereRaw('(
					(
						`ktbaliinculeac_bali_id` IS NOT NULL AND
						`bali_date` <= "'.date('Y-m-d', strtotime($state_per)).'"
					)
					OR
					(
						`ktbaliinculeac_bali_id` IS NULL AND
						`ktbaliinculeac_date` <= "'.date('Y-m-d', strtotime($state_per)).'"
					)
				)')
				->get();


			$amount_paired = 0;
			$payment_dates = [];

			foreach($query_bali as $row_bali)
			{
				$amount_paired += $row_bali->ktbaliinculeac_amount;

				$payment_dates[] = (($row_bali->bali_date == 0) ? $row_bali->ktbaliinculeac_date : $row_bali->bali_date);
			}

			$paymentcurrencies = PaymentCurrency::all();

			if($row->in_amount_netto_eur != round($amount_paired, 2))
			{
				$html .=  '<tr>';
				$html .=  '<td>'.$row->in_number.'</td>';
				$html .=  '<td>'.$row->in_date.'</td>';
				$html .=  '<td>'.$row->in_expiration_date.'</td>';
				$html .=  '<td>'.(($system->dateDifference($row->in_expiration_date, $state_per) > 0) ? $system->dateDifference($row->in_expiration_date, $state_per) : 0).'</td>';
				$html .=  '<td>'.PaymentMethod::name($row->in_payment_method).'</td>';
				$html .=  '<td>'.$paymentcurrencies->where('pacu_code', $row->in_currency)->first()->pacu_token.' '.System::numberFormat($row->in_amount_netto, 2).'</td>';
				$html .=  '<td>€ '.System::numberFormat($amount_paired, 2).'</td>';
				$html .=  '<td>€ '.System::numberFormat($row->in_amount_netto_eur - $amount_paired, 2).'</td>';
				$html .=  '<td>'.((!empty($payment_dates)) ? implode(', ', $payment_dates) : '-').'</td>';
				$html .=  '</tr>';
			}
		}
		$html .= '</tbody>';
		$html .= '</table>';

		return json_encode($html);
	}


}

