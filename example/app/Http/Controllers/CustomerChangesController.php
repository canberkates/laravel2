<?php

namespace App\Http\Controllers;

use App\Functions\Data;
use App\Functions\Mail;
use App\Functions\Sirelo;
use App\Functions\System;
use App\Models\ApplicationUser;
use App\Models\CustomerChange;
use App\Models\MoverData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CustomerChangesController extends Controller
{
	public function index(Request $request)
	{
		$customer_changes_approval = CustomerChange::where([["cuch_approve_deny", "=", 0]])->get();

		$customer_changes_processed = CustomerChange::where(function ($query) {
			$query
				->where('cuch_approve_deny', '=', 1)
				->orWhere('cuch_approve_deny', '=', 2);
		})->get();

		return view('customerchanges.index', ['customer_changes_approval' => $customer_changes_approval, 'customer_changes_processed' => $customer_changes_processed]);
	}

	public function approveConfirmation($id)
	{
		$customer_change = CustomerChange::findOrFail($id);

		$yesno_from = "";
		$yesno_to = "";

		if ($customer_change->cuch_field_to_change == "Logo")
		{

		}
		elseif (($customer_change->cuch_from_value == "0" || $customer_change->cuch_from_value == "1") && ($customer_change->cuch_to_value == "0" || $customer_change->cuch_to_value == "1"))
		{
			if ($customer_change->cuch_from_value == "0")
			{
				$yesno_from = "No";
			}
			else {
				$yesno_from = "Yes";
			}

			if ($customer_change->cuch_to_value == "0")
			{
				$yesno_to = "No";
			}
			else {
				$yesno_to = "Yes";
			}
		}

		return view('customerchanges.approve_confirmation', ['customer_change' => $customer_change, 'yesno_from' => $yesno_from, 'yesno_to' => $yesno_to]);
	}

	public function denyConfirmation($id)
	{
		$customer_change = CustomerChange::findOrFail($id);

		if ($customer_change->cuch_from_value == 1)
		{
			$from = "Yes";
		}
		elseif ($customer_change->cuch_from_value == 0)
		{
			$from = "No";
		}
		else {
			$from = $customer_change->cuch_from_value;
		}

		if ($customer_change->cuch_to_value == 1)
		{
			$to = "Yes";
		}
		elseif ($customer_change->cuch_to_value == 0)
		{
			$to = "No";
		}
		else {
			$to = $customer_change->cuch_to_value;
		}

		return view('customerchanges.deny_confirmation', ['customer_change' => $customer_change, 'from' => $from, 'to' => $to]);
	}

	public function approveChange(Request $request, $id)
	{
		$customer_change = CustomerChange::findOrFail($id);

		if ($customer_change->cuch_field_to_change == "Established")
		{

			DB::table($customer_change->cuch_table_to_change)
				->where($customer_change->cuch_column_of_cu_id, $customer_change->cuch_cu_id)
				->update(
					[
						$customer_change->cuch_column_to_change => DB::raw("DATE_FORMAT(STR_TO_DATE('".$request->to_value."','%Y'),'%Y/01/01')")
					]
				);
		}
		elseif($customer_change->cuch_field_to_change == "Logo")
		{
			rename(env("SHARED_FOLDER")."uploads/temp/logos/".$customer_change->cuch_to_value, env("SHARED_FOLDER")."uploads/logos/".$customer_change->cuch_to_value);

			if(strpos($customer_change->cuch_from_value, 'placeholder') !== false)
			{
				unlink(env("SHARED_FOLDER")."uploads/logos/".$customer_change->cuch_from_value);
			}

			DB::table($customer_change->cuch_table_to_change)
				->where($customer_change->cuch_column_of_cu_id, $customer_change->cuch_cu_id)
				->update(
					[
						$customer_change->cuch_column_to_change => $customer_change->cuch_to_value
					]
				);

		}
		elseif($customer_change->cuch_field_to_change == "City")
		{
		    if ($customer_change->customer->cu_city != $request->to_value)
            {
                $data = new Data();

                $customer_change->customer->cu_city = $request->to_value;

                $data->updateSireloKeyAndForwards($customer_change->customer,  true);
            }

            DB::table($customer_change->cuch_table_to_change)
                ->where($customer_change->cuch_column_of_cu_id, $customer_change->cuch_cu_id)
                ->update(
                    [
                        $customer_change->cuch_column_to_change => $request->to_value
                    ]
                );
		}
		elseif($customer_change->cuch_field_to_change == "Description")
		{
		    if ($customer_change->customer->cu_description != $request->to_value && !empty($request->to_value))
            {
                if ((empty($customer_change->customer->cu_description_la_code) || $customer_change->customer->cu_description != $request->to_value) && !empty($request->to_value)) {
                    $description_la_code = System::getGoogleTranslationLanguage($request->to_value);

                    if (!empty($description_la_code)) {
                        $customer_change->customer->cu_description_la_code = $description_la_code;
                    }
                }
            }

            DB::table($customer_change->cuch_table_to_change)
                ->where($customer_change->cuch_column_of_cu_id, $customer_change->cuch_cu_id)
                ->update(
                    [
                        $customer_change->cuch_column_to_change => $request->to_value
                    ]
                );
		}
		else
		{
		    DB::table($customer_change->cuch_table_to_change)
                ->where($customer_change->cuch_column_of_cu_id, $customer_change->cuch_cu_id)
                ->update(
                    [
                        $customer_change->cuch_column_to_change => $request->to_value
                    ]
                );

		}


		$customer_change->cuch_approve_deny = 1;
		$customer_change->cuch_to_value = $request->to_value;
		$customer_change->cuch_timestamp_approve_deny = date("Y-m-d H:i:s");
		$customer_change->save();

		return redirect('customerchanges/');
	}

	public function denyChange(Request $request, $id)
	{
		$system = new System();
		$customer_change = CustomerChange::findOrFail($id);

		$customer_change->cuch_approve_deny = 2;
		$customer_change->cuch_reason_deny = $request->reason;
		$customer_change->cuch_timestamp_approve_deny = date("Y-m-d H:i:s");

		if($customer_change->cuch_field_to_change == "Logo")
		{
			unlink(env("SHARED_FOLDER")."uploads/temp/logos/".$customer_change->cuch_to_value);
		}

		if ($request->send_mail == 'on')
		{
			$mail = new Mail();

			$customer = ApplicationUser::with("customer")->where([['apus_cu_id', '=', $customer_change->cuch_cu_id]])->first();

			$fields = [
				"customer_changes" => $system->databaseToArray($customer_change),
				"reason" => $customer_change->cuch_reason_deny,
				"customer_email" => $customer->apus_username
			];

			//Send a mail
			$mail->send("customer_changes_denied", $customer['cu_la_code'], $fields);
		}

		$customer_change->save();

		return redirect('customerchanges/');
	}
}
