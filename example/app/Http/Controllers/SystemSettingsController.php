<?php

namespace App\Http\Controllers;

use App\Data\BothYesNo;
use App\Models\Country;
use App\Models\SystemSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Functions\DataIndex;
use Log;

class SystemSettingsController extends Controller
{
    public function index()
    {
        $get_groups = SystemSetting::groupBy("syse_group")->orderBy("syse_group", "asc")->get();
        $groups = [];
        
        foreach($get_groups as $group){
            $groups[$group->syse_group];
        }
        
        $settings = SystemSetting::orderBy("syse_group", "asc")->orderBy("syse_name", "asc")->get();
    
        foreach(Country::orderBy("co_en")->get() as $country)
        {
            $countries[$country->co_code] = $country->co_en;
        }
        
        foreach($settings as $setting){
    
            if($setting->syse_setting == "company_co_code")
            {
                $options = ["" => ""] + $countries;
            }
            elseif($setting->syse_setting == "default_sales_manager")
            {
                $options = ["" => ""] + DataIndex::salesUsers();
            }
            elseif($setting->syse_setting == "default_finance_manager")
            {
                $options = ["" => ""] + DataIndex::financeUsers();
            }
            elseif($setting->syse_setting == "default_partnerdesk_manager")
            {
                $options = ["" => ""] + DataIndex::partnerdeskUsers();
            }
            elseif($setting->syse_setting == "mover_survey")
            {
                $options = BothYesNo::all();
            }
            else
            {
                $options = [];
            }
            
            $setting->options = $options;
    
            $groups[$setting->syse_group][] = $setting;
    
        }
        
        Log::debug(json_encode($groups));
        
        
        return view('systemsettings.index',
            [
                'systemsettings' => $groups
            ]);
    }

    public function update(Request $request)
    {
        foreach($request->all() as $setting => $value){
            DB::table('system_settings')
                ->where("syse_setting", $setting)
                ->update(
                    [
                        'syse_value' => $value
                    ]
                );
        }
       
        return redirect()->back();
    }

}
