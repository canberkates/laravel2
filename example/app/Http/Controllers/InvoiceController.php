<?php

namespace App\Http\Controllers;

use App\Data\CreditDebitInvoiceType;
use App\Data\CreditDebitType;
use App\Data\PaymentMethod;
use App\Functions\Mail;
use App\Functions\Mover;
use App\Functions\System;
use App\Functions\Translation;
use App\Models\Invoice;
use App\Models\InvoiceLine;
use App\Models\KTCustomerSubscription;
use App\Models\LedgerAccount;
use App\Models\PaymentCurrency;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class InvoiceController extends Controller
{
    public function sendInvoice(Request $request, $customer_id, $invoice_id)
    {
        $invoice = Invoice::where("in_id", "=", $invoice_id)->first();

        $invoice_lines_query = InvoiceLine::where("inli_in_id", "=", $invoice_id)
            ->get();

        $invoice_lines = [];
        $invoice_totals = [];

        foreach ($invoice_lines_query as $row_invoice_lines)
        {
            $group = (empty($row_invoice_lines->inli_group) ? "&nbsp;" : $row_invoice_lines->inli_group);
            $desc = $row_invoice_lines->inli_description;

            if (!array_key_exists($group, $invoice_lines))
            {
                $invoice_lines[$group] = [];
            }

            if (!array_key_exists($desc, $invoice_lines[$group]))
            {
                $invoice_lines[$group][$desc] = [];
            }

            $price = (string)$row_invoice_lines->inli_amount;
            $discount = (string)$row_invoice_lines->inli_amount_discount;

            if (!array_key_exists($price, $invoice_lines[$group][$desc]))
            {
                $invoice_lines[$group][$desc][$price] = [];
            }

            if (!array_key_exists($discount, $invoice_lines[$group][$desc][$price]))
            {
                $invoice_lines[$group][$desc][$price][$discount] = [
                    "quantity" => 0,
                    "amount" => 0,
                    "amount_discount" => 0,
                    "amount_netto" => 0,
                ];

            }

            $invoice_lines[$group][$desc][$price][$discount]['quantity']++;
            $invoice_lines[$group][$desc][$price][$discount]['currency'] = $row_invoice_lines->inli_currency;
            $invoice_lines[$group][$desc][$price][$discount]['amount'] += $row_invoice_lines->inli_amount;
            $invoice_lines[$group][$desc][$price][$discount]['amount_discount'] += $row_invoice_lines->inli_amount_discount;
            $invoice_lines[$group][$desc][$price][$discount]['amount_netto'] += $row_invoice_lines->inli_amount_netto;

            if (!array_key_exists($group, $invoice_totals))
            {
                $invoice_totals[$group]['local'] = 0;

                if ($row_invoice_lines->inli_currency != $invoice->in_currency)
                {
                    $invoice_totals[$group][$invoice->in_currency] = 0;
                }
            }

            $invoice_totals[$group]['local'] += $row_invoice_lines->inli_amount_netto;

            if ($row_invoice_lines->inli_currency != $invoice->in_currency)
            {
                $invoice_totals[$group][$invoice->in_currency] += $row_invoice_lines->inli_amount_netto_customer;
            }
        }

        $system = new System();
        $mail = new Mail();

        if (!empty($request->email))
        {
            $debtor_data = unserialize(base64_decode($invoice->in_debtor_data));;
            $debtor_data['email'] = $request->email;
            $invoice->in_debtor_data = base64_encode(serialize($debtor_data));
        }

        $fields = [
            "invoice" => $system->databaseToArray($invoice),
            "invoice_lines" => $invoice_lines,
            "invoice_totals" => $invoice_totals
        ];

        $mail->send("invoice", $invoice->in_la_code, $fields, "uploads/invoices/" . date("Y", strtotime($invoice->in_date)) . "/invoice_" . $invoice->in_number . ".pdf");

        return redirect($request->redirect_url . '/' . $customer_id . '/edit/');
    }

    public function viewInvoice(Request $request)
    {
        $invoice = Invoice::where("in_id", "=", $request->invoice_id)->first();

        $invoice_lines_query = InvoiceLine::where("inli_in_id", "=", $invoice->in_id)
            ->get();

        $invoice_lines = [];
        $invoice_totals = [];

        foreach ($invoice_lines_query as $row_invoice_lines)
        {
            $group = (empty($row_invoice_lines->inli_group) ? "&nbsp;" : $row_invoice_lines->inli_group);
            $desc = $row_invoice_lines->inli_description;

            if (!array_key_exists($group, $invoice_lines))
            {
                $invoice_lines[$group] = [];
            }

            if (!array_key_exists($desc, $invoice_lines[$group]))
            {
                $invoice_lines[$group][$desc] = [];
            }

            $price = (string)$row_invoice_lines->inli_amount;
            $discount = (string)$row_invoice_lines->inli_amount_discount;

            if (!array_key_exists($price, $invoice_lines[$group][$desc]))
            {
                $invoice_lines[$group][$desc][$price] = [];
            }

            if (!array_key_exists($discount, $invoice_lines[$group][$desc][$price]))
            {
                $invoice_lines[$group][$desc][$price][$discount] = [
                    "quantity" => 0,
                    "amount" => 0,
                    "amount_discount" => 0,
                    "amount_netto" => 0,
                ];

            }

            $invoice_lines[$group][$desc][$price][$discount]['quantity']++;
            $invoice_lines[$group][$desc][$price][$discount]['currency'] = $row_invoice_lines->inli_currency;
            $invoice_lines[$group][$desc][$price][$discount]['amount'] += $row_invoice_lines->inli_amount;
            $invoice_lines[$group][$desc][$price][$discount]['amount_discount'] += $row_invoice_lines->inli_amount_discount;
            $invoice_lines[$group][$desc][$price][$discount]['amount_netto'] += $row_invoice_lines->inli_amount_netto;

            if (!array_key_exists($group, $invoice_totals))
            {
                $invoice_totals[$group]['local'] = 0;

                if ($row_invoice_lines->inli_currency != $invoice->in_currency)
                {
                    $invoice_totals[$group][$invoice->in_currency] = 0;
                }
            }

            $invoice_totals[$group]['local'] += $row_invoice_lines->inli_amount_netto;

            if ($row_invoice_lines->inli_currency != $invoice->in_currency)
            {
                $invoice_totals[$group][$invoice->in_currency] += $row_invoice_lines->inli_amount_netto_customer;
            }
        }

        Log::debug("Totals:");
        Log::debug($invoice_totals);

        $system = new System();
        $mail = new Mail();

        $fields = [
            "invoice" => $system->databaseToArray($invoice),
            "invoice_lines" => $invoice_lines,
            "invoice_totals" => $invoice_totals
        ];

        echo "<iframe srcdoc=\"" . str_replace("\"", "'", $mail->send("invoice", $invoice->in_la_code, $fields, null, null, null, true)) . "\" width=\"100%\" height=\"690\" frameborder=\"0\"></iframe>";

    }

    public function previewInvoice(Request $request)
    {
        Log::debug("Preview Invoice:");
        Log::debug($request);

        $request->validate([
            'cu_id' => 'required',
            'period_from' => 'required',
            'period_to' => 'required',
            'invoice_date' => 'required',
            'single_month' => 'required',
            'only_credit_debit_invoice_lines' => 'required'
        ]);


        $translate = new Translation();
        $system = new System();
        $mail = new Mail();

        $collected_translation = json_decode($translate->getMultiple(
            [
                "invoice_received_requests",
                "invoice_requests",
                "invoice_free_requests",
                "invoice_no_claim_discount",
                "invoice_no_claim_discount_with_price",
                "invoice_no_claim_discount_without_percentage",
                "request_type_val_1",
                "request_type_val_2",
                "request_type_val_3",
                "request_type_val_4",
                "request_type_val_5",
                "invoice_payment_method_1",
                "invoice_payment_method_2",
                "invoice_payment_method_3",
                "invoice_payment_method_4",
                "invoice_payment_method_5",
                "invoice_payment_method_6",
                "invoice_free_requests_previous_invoice",
                "invoice_paid_requests_previous_invoice",
                "invoice_miscellaneous",
                "yes",
                "no"
            ]
        ), true);

        $row_customer = DB::table("customers")
            ->select("customers.*", "moda_no_claim_discount", "moda_no_claim_discount_percentage")
            ->leftJoin("mover_data", "cu_id", "moda_cu_id")
            ->where("cu_id", $request->cu_id)
            ->where("cu_deleted", 0)
            ->first();

        $preview_invoice_lines = [];

        $request_customer_portal = DB::table("kt_request_customer_portal")
            ->leftJoin("kt_customer_portal", "ktcupo_id", "ktrecupo_ktcupo_id")
            ->leftJoin("requests", "ktrecupo_re_id", "re_id")
            ->leftJoin("portals", "ktrecupo_po_id", "po_id")
            ->leftJoin("websites", "po_default_website", "we_id")
            ->where("ktrecupo_cu_id", $row_customer->cu_id)
            ->whereIn("ktrecupo_type", [1,5])
            ->where("ktrecupo_invoiced", 0);

        if ($request->single_month == 1)
        {
            $request_customer_portal->whereBetween("ktrecupo_timestamp", [date("Y-m-d H:i:s", strtotime($request->period_from . " 00:00:00")), date("Y-m-d H:i:s", strtotime($request->period_to . " 23:59:59"))]);
        } elseif ($request->single_month == 0)
        {
            $request_customer_portal->where("ktrecupo_timestamp", "<=", date("Y-m-d H:i:s", strtotime($request->period_to . " 23:59:59")));
        }

        $request_customer_portal = $request_customer_portal->orderBy("ktrecupo_timestamp", 'asc')
            ->get();

        $request_customer_question = DB::table("kt_request_customer_question")
            ->leftJoin("requests", "ktrecuqu_re_id", "re_id")
            ->leftJoin("questions", "ktrecuqu_qu_id", "qu_id")
            ->where("ktrecuqu_cu_id", $row_customer->cu_id)
            ->where("ktrecuqu_sent", 1)
            ->where("ktrecuqu_hide", 0)
            ->where("ktrecuqu_invoiced", 0);

        if ($request->single_month == 1)
        {
            $request_customer_question->whereBetween("ktrecuqu_sent_timestamp", [date("Y-m-d H:i:s", strtotime($request->period_from . " 00:00:00")), date("Y-m-d H:i:s", strtotime($request->period_to . " 23:59:59"))]);
        } elseif ($request->single_month == 0)
        {
            $request_customer_question->where("ktrecuqu_sent_timestamp", "<=", date("Y-m-d H:i:s", strtotime($request->period_to . " 23:59:59")));
        }

        $request_customer_question = $request_customer_question->orderBy("ktrecuqu_sent_timestamp", 'asc')
            ->get();


        $credit_debit_invoice_lines = DB::table("credit_debit_invoice_lines")
            ->leftJoin("kt_request_customer_portal", "crdeinli_ktrecupo_id", "ktrecupo_id")
            ->leftJoin("kt_request_customer_question", "crdeinli_ktrecuqu_id", "ktrecuqu_id")
            ->leftJoin("requests", DB::raw("`kt_request_customer_portal`.`ktrecupo_re_id` = `requests`.`re_id` OR `kt_request_customer_question`.`ktrecuqu_re_id`"), DB::raw("requests.re_id"))
            ->leftJoin("questions", "ktrecuqu_qu_id", "qu_id")
            ->where("crdeinli_cu_id", $row_customer->cu_id);

        if ($request->only_credit_debit_invoice_lines == CreditDebitInvoiceType::ONLY_CREDIT_LINES) {
            $credit_debit_invoice_lines->where("crdeinli_type", CreditDebitType::CREDIT);
        }elseif($request->only_credit_debit_invoice_lines == CreditDebitInvoiceType::ONLY_DEBIT_LINES) {
            $credit_debit_invoice_lines->where("crdeinli_type", CreditDebitType::DEBIT);
        }

        $credit_debit_invoice_lines = $credit_debit_invoice_lines
            ->where("crdeinli_invoiced", 0)
            ->get();

        $subscriptions = KTCustomerSubscription::leftJoin("subscriptions", "sub_id", "ktcusu_sub_id")
            ->where("ktcusu_cu_id", $row_customer->cu_id)
            ->where("ktcusu_active", 1)
            ->where("ktcusu_invoiced", 0)
            ->get();

        if (((count($request_customer_portal) || count($request_customer_question)) && $request->only_credit_debit_invoice_lines == 0) || count($credit_debit_invoice_lines) || count($subscriptions))
        {
            $increment_query = DB::select("SHOW TABLE STATUS LIKE 'invoices'");
            $in_id = $increment_query[0]->Auto_increment;

            //Get highest number
            $year = date("Y", strtotime($request->invoice_date));
            $subyear = substr($year, 2, 2);

            $row_in_number = DB::table("invoices")
                ->select("in_number")
                ->where("in_old", 0)
                ->whereRaw("in_number LIKE '" . $subyear . "-%'")
                ->orderBy("in_number", 'desc')
                ->first();

            if (!empty($row_in_number))
            {
                $in_number = $row_in_number->in_number;

                $in_number++;
            } else
            {
                $in_number = date("y", strtotime($request->invoice_date)) . "-0001";
            }

            $company_data = [
                "company_name" => System::getSetting("company_name"),
                "street" => System::getSetting("company_street"),
                "zipcode" => System::getSetting("company_zipcode"),
                "city" => System::getSetting("company_city"),
                "country" => System::getSetting("company_co_code"),
                "vat" => System::getSetting("company_vat"),
                "coc" => System::getSetting("company_coc"),
                "bank" => System::getSetting("company_bank"),
                "iban" => System::getSetting("company_iban"),
                "bic" => System::getSetting("company_bic"),
                "telephone" => System::getSetting("company_telephone"),
                "fax" => System::getSetting("company_fax"),
                "email" => System::getSetting("company_email"),
                "website" => System::getSetting("company_website")
            ];

            if ($row_customer->cu_use_billing_address == 1)
            {
                $debtor_data = [
                    "id" => $row_customer->cu_id,
                    "debtor_number" => $row_customer->cu_debtor_number,
                    "company_name" => $row_customer->cu_company_name_legal,
                    "attn" => $row_customer->cu_attn,
                    "email" => $row_customer->cu_email_bi ? $row_customer->cu_email_bi : $row_customer->cu_email,
                    "street_1" => $row_customer->cu_street_1_bi,
                    "street_2" => $row_customer->cu_street_2_bi,
                    "zipcode" => $row_customer->cu_zipcode_bi,
                    "city" => $row_customer->cu_city_bi,
                    "country" => $row_customer->cu_co_code_bi,
                    "vat_number" => $row_customer->cu_vat_number
                ];
            } else
            {
                $debtor_data = [
                    "id" => $row_customer->cu_id,
                    "debtor_number" => $row_customer->cu_debtor_number,
                    "company_name" => $row_customer->cu_company_name_legal,
                    "attn" => $row_customer->cu_attn,
                    "email" => ((!empty($row_customer->cu_email_bi)) ? $row_customer->cu_email_bi : $row_customer->cu_email),
                    "street_1" => $row_customer->cu_street_1,
                    "street_2" => $row_customer->cu_street_2,
                    "zipcode" => $row_customer->cu_zipcode,
                    "city" => $row_customer->cu_city,
                    "country" => $row_customer->cu_co_code,
                    "vat_number" => $row_customer->cu_vat_number
                ];
            }

            $invoice_lines = 0;
            $invoice_amount = 0;
            $invoice_amount_discount = 0;
            $invoice_amount_gross = 0;
            $invoice_amount_no_claim = 0;
            $invoice_amount_eur = 0;
            $invoice_amount_discount_eur = 0;
            $invoice_amount_gross_eur = 0;
            $invoice_amount_no_claim_eur = 0;

            $requests = [];

            $no_claim_rows = [];

            if ($request->only_credit_debit_invoice_lines == 0)
            {
                foreach ($request_customer_portal as $row_request_customer_portal)
                {
                    if (!isset($invoice_period_from))
                    {
                        $invoice_period_from = date("Y-m-d", strtotime(date("Y-m", strtotime($row_request_customer_portal->ktrecupo_timestamp)) . "-01"));
                    }

                    $preview_invoice_lines[] = [
                        "inli_in_id" => $in_id,
                        "inli_ktrecupo_id" => $row_request_customer_portal->ktrecupo_id,
                        "inli_group" => $row_request_customer_portal->ktcupo_description,
                        //"inli_group" => $row_request_customer_portal->po_pacu_code." (".PaymentCurrency::where("pacu_code", $row_request_customer_portal->po_pacu_code)->first()->pacu_token.")",
                        //"inli_group" => "Aanvragen (".PaymentCurrency::where("pacu_code", $row_request_customer_portal->po_pacu_code)->first()->pacu_token.")",
                        "inli_description" => $collected_translation["invoice_requests"][$row_customer->cu_la_code],
                        "inli_currency" => (($row_request_customer_portal->ktrecupo_type == 5) ? $row_customer->cu_pacu_code : $row_request_customer_portal->po_pacu_code),
                        "inli_amount" => $row_request_customer_portal->ktrecupo_amount,
                        "inli_amount_discount" => $row_request_customer_portal->ktrecupo_amount_discount,
                        "inli_amount_netto" => $row_request_customer_portal->ktrecupo_amount_netto,
                        "inli_amount_customer" => $system->currencyConvert($row_request_customer_portal->ktrecupo_amount, $row_request_customer_portal->po_pacu_code, $row_customer->cu_pacu_code),
                        "inli_amount_discount_customer" => $system->currencyConvert($row_request_customer_portal->ktrecupo_amount_discount, $row_request_customer_portal->po_pacu_code, $row_customer->cu_pacu_code),
                        "inli_amount_netto_customer" => $system->currencyConvert($row_request_customer_portal->ktrecupo_amount_netto, $row_request_customer_portal->po_pacu_code, $row_customer->cu_pacu_code),
                        "inli_amount_eur" => $system->currencyConvert($row_request_customer_portal->ktrecupo_amount, $row_request_customer_portal->po_pacu_code, "EUR"),
                        "inli_amount_discount_eur" => $system->currencyConvert($row_request_customer_portal->ktrecupo_amount_discount, $row_request_customer_portal->po_pacu_code, "EUR"),
                        "inli_amount_netto_eur" => $system->currencyConvert($row_request_customer_portal->ktrecupo_amount_netto, $row_request_customer_portal->po_pacu_code, "EUR"),
                        "inli_leac_number" => $row_customer->cu_leac_number
                    ];

                    if ($row_request_customer_portal->ktrecupo_free_percentage > 0 && $row_request_customer_portal->ktrecupo_free_percentage < 1)
                    {
                        //From DB
                        $no_claim_ktrecupo_amount = round($row_request_customer_portal->ktrecupo_amount * $row_request_customer_portal->ktrecupo_free_percentage,4 );
                        $no_claim_amount_netto = round($row_request_customer_portal->ktrecupo_amount_netto * $row_request_customer_portal->ktrecupo_free_percentage, 4);

                        //From DB to Customer currency
                        $no_claim_amount = round($row_request_customer_portal->ktrecupo_amount * $row_request_customer_portal->ktrecupo_free_percentage, 4);
                        $no_claim_gross = round($row_request_customer_portal->ktrecupo_amount_netto * $row_request_customer_portal->ktrecupo_free_percentage, 4);

                        //From DB to EURO
                        $no_claim_amount_eur = round($row_request_customer_portal->ktrecupo_amount * $row_request_customer_portal->ktrecupo_free_percentage, 4);
                        $no_claim_amount_gross_eur = round($row_request_customer_portal->ktrecupo_amount_netto * $row_request_customer_portal->ktrecupo_free_percentage, 4);

                        $pacu_token = PaymentCurrency::select("pacu_token")->where("pacu_code", $row_request_customer_portal->po_pacu_code)->first()->pacu_token;

                        /*$array_after_inv[] = [
                            "inli_in_id" => $in_id,
                            "inli_ktrecupo_id" => $row_request_customer_portal->ktrecupo_id,
                            "inli_group" => $collected_translation["invoice_no_claim_discount_without_percentage"][$row_customer->cu_la_code],
                            "inli_description" => sprintf($collected_translation["invoice_no_claim_discount_with_price"][$row_customer->cu_la_code], $row_request_customer_portal->ktrecupo_free_percentage * 100, $pacu_token." ".$row_request_customer_portal->ktrecupo_amount_netto),
                            "inli_no_claim_discount" => 1,
                            "inli_currency" => $row_request_customer_portal->po_pacu_code,
                            "inli_amount" => -$no_claim_ktrecupo_amount,
                            "inli_amount_discount" => 0,
                            "inli_amount_netto" => -$no_claim_amount_netto,
                            "inli_amount_customer" => -$system->currencyConvert($no_claim_amount, $row_request_customer_portal->po_pacu_code, $row_customer->cu_pacu_code, 4),
                            "inli_amount_discount_customer" => 0,
                            "inli_amount_netto_customer" => -$system->currencyConvert($no_claim_gross, $row_request_customer_portal->po_pacu_code, $row_customer->cu_pacu_code, 4),
                            "inli_amount_eur" => -$system->currencyConvert($no_claim_amount_eur, $row_request_customer_portal->po_pacu_code, "EUR", 4),
                            "inli_amount_discount_eur" => 0,
                            "inli_amount_netto_eur" => -$system->currencyConvert($no_claim_amount_gross_eur, $row_request_customer_portal->po_pacu_code, "EUR", 4),
                            "inli_leac_number" => $row_customer->cu_leac_number
                        ];*/

                        //$invoice_amount_no_claim += $system->currencyConvert($no_claim_gross, $row_request_customer_portal->po_pacu_code, $row_customer->cu_pacu_code);
                        //$invoice_amount_no_claim_eur += $system->currencyConvert($no_claim_amount_gross_eur, $row_request_customer_portal->po_pacu_code, "EUR");
                        //$invoice_amount_no_claim += $system->currencyConvert($no_claim_gross, $row_request_customer_portal->po_pacu_code, $row_customer->cu_pacu_code);
                        //$invoice_amount_no_claim_eur += $system->currencyConvert($no_claim_amount_gross_eur, $row_request_customer_portal->po_pacu_code, "EUR");

                        //$invoice_amount_gross -= $system->currencyConvert($no_claim_gross, $row_request_customer_portal->po_pacu_code, $row_customer->cu_pacu_code);
                        //$invoice_amount_gross_eur -= $system->currencyConvert($no_claim_amount_gross_eur, $row_request_customer_portal->po_pacu_code, "EUR");

                        $no_claim_gross_converted = $system->currencyConvert($no_claim_gross, $row_request_customer_portal->po_pacu_code, $row_customer->cu_pacu_code);


                        $no_claim_group = sprintf($collected_translation["invoice_no_claim_discount_with_price"][$row_customer->cu_la_code], $row_request_customer_portal->ktrecupo_free_percentage * 100, $pacu_token." ".$row_request_customer_portal->ktrecupo_amount_netto);

                        if (!isset($no_claim_rows[$no_claim_group])) {
                            $no_claim_rows[$no_claim_group] = [
                                "count" => 0,
                                "price" => $pacu_token." -".System::numberFormat($no_claim_gross, 4),
                                "price_excl" => 0,
                                "po_pacu_code" => $row_request_customer_portal->po_pacu_code
                            ];
                        }

                        $no_claim_rows[$no_claim_group]["count"]++;
                        //$no_claim_rows[$no_claim_group]["price_excl"] = $no_claim_rows[$no_claim_group]["price_excl"] + $no_claim_gross_converted;
                        $no_claim_rows[$no_claim_group]["price_excl"] = $no_claim_rows[$no_claim_group]["price_excl"] + $no_claim_gross;

                        /*self::insertInvoiceLine(
                            $new_invoice->in_id,
                            $ktrecupo_row['ktrecupo_id'],
                            null,
                            null,
                            $site_key,
                            $collected_translation["invoice_requests"][$cu_la_code],
                            1,
                            $po_pacu_code,
                            $customer['cu_leac_number'],
                            -$no_claim_ktrecupo_amount,
                            0,
                            -$no_claim_amount_netto,
                            -$no_claim_amount, 0, -$no_claim_gross,
                            -$no_claim_amount_eur, 0, -$no_claim_amount_gross_eur
                        );*/
                    }

                    $invoice_lines++;
                    $invoice_amount += $system->currencyConvert($row_request_customer_portal->ktrecupo_amount, (($row_request_customer_portal->ktrecupo_type == 5) ? $row_customer->cu_pacu_code : $row_request_customer_portal->po_pacu_code), $row_customer->cu_pacu_code);
                    $invoice_amount_discount += $system->currencyConvert($row_request_customer_portal->ktrecupo_amount_discount, (($row_request_customer_portal->ktrecupo_type == 5) ? $row_customer->cu_pacu_code : $row_request_customer_portal->po_pacu_code), $row_customer->cu_pacu_code);
                    $invoice_amount_gross += $system->currencyConvert($row_request_customer_portal->ktrecupo_amount_netto, (($row_request_customer_portal->ktrecupo_type == 5) ? $row_customer->cu_pacu_code : $row_request_customer_portal->po_pacu_code), $row_customer->cu_pacu_code);
                    $invoice_amount_eur += $system->currencyConvert($row_request_customer_portal->ktrecupo_amount, (($row_request_customer_portal->ktrecupo_type == 5) ? $row_customer->cu_pacu_code : $row_request_customer_portal->po_pacu_code), "EUR");
                    $invoice_amount_discount_eur += $system->currencyConvert($row_request_customer_portal->ktrecupo_amount_discount, (($row_request_customer_portal->ktrecupo_type == 5) ? $row_customer->cu_pacu_code : $row_request_customer_portal->po_pacu_code), "EUR");
                    $invoice_amount_gross_eur += $system->currencyConvert($row_request_customer_portal->ktrecupo_amount_netto, (($row_request_customer_portal->ktrecupo_type == 5) ? $row_customer->cu_pacu_code : $row_request_customer_portal->po_pacu_code), "EUR");

                    if ($row_request_customer_portal->ktrecupo_free_percentage == 1)
                    {
                        $preview_invoice_lines[] = [
                            "inli_in_id" => $in_id,
                            "inli_ktrecupo_id" => $row_request_customer_portal->ktrecupo_id,
                            "inli_group" => $row_request_customer_portal->ktcupo_description,
                            //"inli_group" => $row_request_customer_portal->we_mail_from_name . " - " . lcfirst($collected_translation["invoice_received_requests"][$row_customer->cu_la_code]) . " (" . $collected_translation["request_type_val_" . $row_request_customer_portal->re_request_type][$row_customer->cu_la_code] . ")",
                            "inli_description" => $collected_translation["invoice_free_requests"][$row_customer->cu_la_code],
                            "inli_currency" => $row_request_customer_portal->po_pacu_code,
                            "inli_amount" => -$row_request_customer_portal->ktrecupo_amount,
                            "inli_amount_discount" => -$row_request_customer_portal->ktrecupo_amount_discount,
                            "inli_amount_netto" => -$row_request_customer_portal->ktrecupo_amount_netto,
                            "inli_amount_customer" => -$system->currencyConvert($row_request_customer_portal->ktrecupo_amount, $row_request_customer_portal->po_pacu_code, $row_customer->cu_pacu_code),
                            "inli_amount_discount_customer" => -$system->currencyConvert($row_request_customer_portal->ktrecupo_amount_discount, $row_request_customer_portal->po_pacu_code, $row_customer->cu_pacu_code),
                            "inli_amount_netto_customer" => -$system->currencyConvert($row_request_customer_portal->ktrecupo_amount_netto, $row_request_customer_portal->po_pacu_code, $row_customer->cu_pacu_code),
                            "inli_amount_eur" => -$system->currencyConvert($row_request_customer_portal->ktrecupo_amount, $row_request_customer_portal->po_pacu_code, "EUR"),
                            "inli_amount_discount_eur" => -$system->currencyConvert($row_request_customer_portal->ktrecupo_amount_discount, $row_request_customer_portal->po_pacu_code, "EUR"),
                            "inli_amount_netto_eur" => -$system->currencyConvert($row_request_customer_portal->ktrecupo_amount_netto, $row_request_customer_portal->po_pacu_code, "EUR"),
                            "inli_leac_number" => $row_customer->cu_leac_number
                        ];

                        $invoice_lines++;
                        $invoice_amount -= $system->currencyConvert($row_request_customer_portal->ktrecupo_amount, $row_request_customer_portal->po_pacu_code, $row_customer->cu_pacu_code);
                        $invoice_amount_discount -= $system->currencyConvert($row_request_customer_portal->ktrecupo_amount_discount, $row_request_customer_portal->po_pacu_code, $row_customer->cu_pacu_code);
                        $invoice_amount_gross -= $system->currencyConvert($row_request_customer_portal->ktrecupo_amount_netto, $row_request_customer_portal->po_pacu_code, $row_customer->cu_pacu_code);
                        $invoice_amount_eur -= $system->currencyConvert($row_request_customer_portal->ktrecupo_amount, $row_request_customer_portal->po_pacu_code, "EUR");
                        $invoice_amount_discount_eur -= $system->currencyConvert($row_request_customer_portal->ktrecupo_amount_discount, $row_request_customer_portal->po_pacu_code, "EUR");
                        $invoice_amount_gross_eur -= $system->currencyConvert($row_request_customer_portal->ktrecupo_amount_netto, $row_request_customer_portal->po_pacu_code, "EUR");
                    }

                    $group = str_replace("[", "", $row_request_customer_portal->ktcupo_description);
                    $group = str_replace("]", "", $group);

                    if (!array_key_exists($group, $requests))
                    {
                        $requests[$group] = [];
                    }

                    $requests[$group][$row_request_customer_portal->ktrecupo_id] = [
                        "id" => $row_request_customer_portal->ktrecupo_cu_re_id,
                        "received" => $row_request_customer_portal->ktrecupo_timestamp,
                        "name" => $row_request_customer_portal->re_full_name,
                        "country_from" => $row_request_customer_portal->re_co_code_from,
                        "city_from" => $row_request_customer_portal->re_city_from,
                        "country_to" => $row_request_customer_portal->re_co_code_to,
                        "city_to" => $row_request_customer_portal->re_city_to,
                        "free" => $collected_translation[(($row_request_customer_portal->ktrecupo_free) ? "yes" : "no")][$row_customer->cu_la_code]
                    ];
                }

                foreach ($request_customer_question as $row_request_customer_question)
                {
                    if (!isset($invoice_period_from))
                    {
                        $invoice_period_from = date("Y-m-d", strtotime(date("Y-m", strtotime($row_request_customer_question->ktrecuqu_sent_timestamp)) . "-01"));
                    }

                    $preview_invoice_lines[] = [
                        "inli_in_id" => $in_id,
                        "inli_ktrecuqu_id" => $row_request_customer_question->ktrecuqu_id,
                        "inli_group" => $collected_translation["invoice_received_requests"][$row_customer->cu_la_code] . " (" . $row_request_customer_question->qu_name . ")",
                        "inli_description" => $collected_translation["invoice_requests"][$row_customer->cu_la_code],
                        "inli_currency" => $row_customer->cu_pacu_code,
                        "inli_amount" => $system->currencyConvert($row_request_customer_question->ktrecuqu_amount, $row_request_customer_question->ktrecuqu_currency, $row_customer->cu_pacu_code),
                        "inli_amount_discount" => 0,
                        "inli_amount_netto" => $system->currencyConvert($row_request_customer_question->ktrecuqu_amount, $row_request_customer_question->ktrecuqu_currency, $row_customer->cu_pacu_code),
                        "inli_amount_customer" => $system->currencyConvert($row_request_customer_question->ktrecuqu_amount, $row_request_customer_question->ktrecuqu_currency, $row_customer->cu_pacu_code),
                        "inli_amount_discount_customer" => 0,
                        "inli_amount_netto_customer" => $system->currencyConvert($row_request_customer_question->ktrecuqu_amount, $row_request_customer_question->ktrecuqu_currency, $row_customer->cu_pacu_code),
                        "inli_amount_eur" => $system->currencyConvert($row_request_customer_question->ktrecuqu_amount, $row_request_customer_question->ktrecuqu_currency, "EUR"),
                        "inli_amount_discount_eur" => 0,
                        "inli_amount_netto_eur" => $system->currencyConvert($row_request_customer_question->ktrecuqu_amount, $row_request_customer_question->ktrecuqu_currency, "EUR"),
                        "inli_leac_number" => $row_customer->cu_leac_number
                    ];

                    $invoice_lines++;
                    $invoice_amount += $system->currencyConvert($row_request_customer_question->ktrecuqu_amount, $row_request_customer_question->ktrecuqu_currency, $row_customer->cu_pacu_code);
                    $invoice_amount_gross += $system->currencyConvert($row_request_customer_question->ktrecuqu_amount, $row_request_customer_question->ktrecuqu_currency, $row_customer->cu_pacu_code);
                    $invoice_amount_eur += $system->currencyConvert($row_request_customer_question->ktrecuqu_amount, $row_request_customer_question->ktrecuqu_currency, "EUR");
                    $invoice_amount_gross_eur += $system->currencyConvert($row_request_customer_question->ktrecuqu_amount, $row_request_customer_question->ktrecuqu_currency, "EUR");

                    if ($row_request_customer_question->ktrecuqu_free)
                    {
                        $preview_invoice_lines[] = [
                            "inli_in_id" => $in_id,
                            "inli_ktrecuqu_id" => $row_request_customer_question->ktrecuqu_id,
                            "inli_group" => $collected_translation["invoice_received_requests"][$row_customer->cu_la_code] . " (" . $row_request_customer_question->qu_name . ")",
                            "inli_description" =>$collected_translation["invoice_free_requests"][$row_customer->cu_la_code],
                            "inli_currency" => $row_customer->cu_pacu_code,
                            "inli_amount" => -$system->currencyConvert($row_request_customer_question->ktrecuqu_amount, $row_request_customer_question->ktrecuqu_currency, $row_customer->cu_pacu_code),
                            "inli_amount_discount" => 0,
                            "inli_amount_netto" => -$system->currencyConvert($row_request_customer_question->ktrecuqu_amount, $row_request_customer_question->ktrecuqu_currency, $row_customer->cu_pacu_code),
                            "inli_amount_customer" => -$system->currencyConvert($row_request_customer_question->ktrecuqu_amount, $row_request_customer_question->ktrecuqu_currency, $row_customer->cu_pacu_code),
                            "inli_amount_discount_customer" => 0,
                            "inli_amount_netto_customer" => -$system->currencyConvert($row_request_customer_question->ktrecuqu_amount, $row_request_customer_question->ktrecuqu_currency, $row_customer->cu_pacu_code),
                            "inli_amount_eur" => -$system->currencyConvert($row_request_customer_question->ktrecuqu_amount, $row_request_customer_question->ktrecuqu_currency, "EUR"),
                            "inli_amount_discount_eur" => 0,
                            "inli_amount_netto_eur" => -$system->currencyConvert($row_request_customer_question->ktrecuqu_amount, $row_request_customer_question->ktrecuqu_currency, "EUR"),
                            "inli_leac_number" => $row_customer->cu_leac_number
                        ];

                        $invoice_lines++;
                        $invoice_amount -= $system->currencyConvert($row_request_customer_question->ktrecuqu_amount, $row_request_customer_question->ktrecuqu_currency, $row_customer->cu_pacu_code);
                        $invoice_amount_gross -= $system->currencyConvert($row_request_customer_question->ktrecuqu_amount, $row_request_customer_question->ktrecuqu_currency, $row_customer->cu_pacu_code);
                        $invoice_amount_eur -= $system->currencyConvert($row_request_customer_question->ktrecuqu_amount, $row_request_customer_question->ktrecuqu_currency, "EUR");
                        $invoice_amount_gross_eur -= $system->currencyConvert($row_request_customer_question->ktrecuqu_amount, $row_request_customer_question->ktrecuqu_currency, "EUR");
                    }

                    if (!array_key_exists($collected_translation["invoice_received_requests"][$row_customer->cu_la_code] . " (" . $row_request_customer_question->qu_name . ")", $requests))
                    {
                        $requests[$collected_translation["invoice_received_requests"][$row_customer->cu_la_code] . " (" . $row_request_customer_question->qu_name . ")"] = [];
                    }

                    $requests[$collected_translation["invoice_received_requests"][$row_customer->cu_la_code] . " (" . $row_request_customer_question->qu_name . ")"][$row_request_customer_question->ktrecuqu_id] = [
                        "id" => $row_request_customer_question->ktrecuqu_cu_re_id,
                        "received" => $row_request_customer_question->ktrecuqu_sent_timestamp,
                        "name" => $row_request_customer_question->re_full_name,
                        "country_from" => $row_request_customer_question->re_co_code_from,
                        "city_from" => $row_request_customer_question->re_city_from,
                        "country_to" => $row_request_customer_question->re_co_code_to,
                        "city_to" => $row_request_customer_question->re_city_to,
                        "free" => $collected_translation[(($row_request_customer_question->ktrecuqu_free) ? "yes" : "no")][$row_customer->cu_la_code]
                    ];
                }
            }

            foreach ($credit_debit_invoice_lines as $row_credit_debit_invoice_lines)
            {
                $preview_invoice_lines[] = [
                    "inli_in_id" => $in_id,
                    "inli_ktrecupo_id" => $row_credit_debit_invoice_lines->crdeinli_ktrecupo_id,
                    "inli_ktrecuqu_id" => $row_credit_debit_invoice_lines->crdeinli_ktrecuqu_id,
                    "inli_crdeinli_id" => $row_credit_debit_invoice_lines->crdeinli_id,
                    "inli_group" => $collected_translation["invoice_miscellaneous"][$row_customer->cu_la_code],
                    "inli_description" => $row_credit_debit_invoice_lines->crdeinli_description,
                    "inli_currency" => $row_customer->cu_pacu_code,
                    "inli_amount" => $system->currencyConvert($row_credit_debit_invoice_lines->crdeinli_amount, $row_credit_debit_invoice_lines->crdeinli_currency, $row_customer->cu_pacu_code),
                    "inli_amount_discount" => 0,
                    "inli_amount_netto" => $system->currencyConvert($row_credit_debit_invoice_lines->crdeinli_amount, $row_credit_debit_invoice_lines->crdeinli_currency, $row_customer->cu_pacu_code),
                    "inli_amount_customer" => $system->currencyConvert($row_credit_debit_invoice_lines->crdeinli_amount, $row_credit_debit_invoice_lines->crdeinli_currency, $row_customer->cu_pacu_code),
                    "inli_amount_discount_customer" => 0,
                    "inli_amount_netto_customer" => $system->currencyConvert($row_credit_debit_invoice_lines->crdeinli_amount, $row_credit_debit_invoice_lines->crdeinli_currency, $row_customer->cu_pacu_code),
                    "inli_amount_eur" => $system->currencyConvert($row_credit_debit_invoice_lines->crdeinli_amount, $row_credit_debit_invoice_lines->crdeinli_currency, "EUR"),
                    "inli_amount_discount_eur" => 0,
                    "inli_amount_netto_eur" => $system->currencyConvert($row_credit_debit_invoice_lines->crdeinli_amount, $row_credit_debit_invoice_lines->crdeinli_currency, "EUR"),
                    "inli_leac_number" => $row_credit_debit_invoice_lines->crdeinli_leac_number
                ];

                $invoice_lines++;
                $invoice_amount += $system->currencyConvert($row_credit_debit_invoice_lines->crdeinli_amount, $row_credit_debit_invoice_lines->crdeinli_currency, $row_customer->cu_pacu_code);
                $invoice_amount_gross += $system->currencyConvert($row_credit_debit_invoice_lines->crdeinli_amount, $row_credit_debit_invoice_lines->crdeinli_currency, $row_customer->cu_pacu_code);
                $invoice_amount_eur += $system->currencyConvert($row_credit_debit_invoice_lines->crdeinli_amount, $row_credit_debit_invoice_lines->crdeinli_currency, "EUR");
                $invoice_amount_gross_eur += $system->currencyConvert($row_credit_debit_invoice_lines->crdeinli_amount, $row_credit_debit_invoice_lines->crdeinli_currency, "EUR");

                if ($row_credit_debit_invoice_lines->crdeinli_type == 1)
                {
                    $type = "free";
                } elseif ($row_credit_debit_invoice_lines->crdeinli_type == 2)
                {
                    $type = "paid";
                }

                if (!empty($row_credit_debit_invoice_lines->crdeinli_ktrecupo_id))
                {
                    if (!array_key_exists($collected_translation["invoice_" . $type . "_requests_previous_invoice"][$row_customer->cu_la_code] . " (" . $collected_translation["request_type_val_" . $row_credit_debit_invoice_lines->re_request_type][$row_customer->cu_la_code] . ")", $requests))
                    {
                        $requests[$collected_translation["invoice_" . $type . "_requests_previous_invoice"][$row_customer->cu_la_code] . " (" . $collected_translation["request_type_val_" . $row_credit_debit_invoice_lines->re_request_type][$row_customer->cu_la_code] . ")"] = [];
                    }

                    $requests[$collected_translation["invoice_" . $type . "_requests_previous_invoice"][$row_customer->cu_la_code] . " (" . $collected_translation["request_type_val_" . $row_credit_debit_invoice_lines->re_request_type][$row_customer->cu_la_code] . ")"][$row_credit_debit_invoice_lines->ktrecupo_id] = [
                        "id" => $row_credit_debit_invoice_lines->ktrecupo_cu_re_id,
                        "received" => $row_credit_debit_invoice_lines->ktrecupo_timestamp,
                        "name" => $row_credit_debit_invoice_lines->re_full_name,
                        "country_from" => $row_credit_debit_invoice_lines->re_co_code_from,
                        "city_from" => $row_credit_debit_invoice_lines->re_city_from,
                        "country_to" => $row_credit_debit_invoice_lines->re_co_code_to,
                        "city_to" => $row_credit_debit_invoice_lines->re_city_to,
                        "free" => $collected_translation[(($row_credit_debit_invoice_lines->ktrecupo_free) ? "yes" : "no")][$row_customer->cu_la_code]
                    ];
                }

                if (!empty($row_credit_debit_invoice_lines->crdeinli_ktrecuqu_id))
                {
                    if (!array_key_exists($collected_translation["invoice_" . $type . "_requests_previous_invoice"][$row_customer->cu_la_code] . " (" . $row_credit_debit_invoice_lines->qu_name . ")", $requests))
                    {
                        $requests[$collected_translation["invoice_" . $type . "_requests_previous_invoice"][$row_customer->cu_la_code] . " (" . $row_credit_debit_invoice_lines->qu_name . ")"] = [];
                    }

                    $requests[$collected_translation["invoice_" . $type . "_requests_previous_invoice"][$row_customer->cu_la_code] . " (" . $row_credit_debit_invoice_lines->qu_name . ")"][$row_credit_debit_invoice_lines->ktrecuqu_id] = [
                        "id" => $row_credit_debit_invoice_lines->ktrecuqu_cu_re_id,
                        "received" => $row_credit_debit_invoice_lines->ktrecuqu_sent_timestamp,
                        "name" => $row_credit_debit_invoice_lines->re_full_name,
                        "country_from" => $row_credit_debit_invoice_lines->re_co_code_from,
                        "city_from" => $row_credit_debit_invoice_lines->re_city_from,
                        "country_to" => $row_credit_debit_invoice_lines->re_co_code_to,
                        "city_to" => $row_credit_debit_invoice_lines->re_city_to,
                        "free" =>$collected_translation[(($row_credit_debit_invoice_lines->ktrecuqu_free) ? "yes" : "no")][$row_customer->cu_la_code]
                    ];
                }
            }

            /*if ($row_customer->moda_no_claim_discount && !empty($requests))
            {
                //$invoice_amount_no_claim = round($invoice_amount_gross * ($row_customer->moda_no_claim_discount_percentage / 100), 2);
                //$invoice_amount_no_claim_eur = round($invoice_amount_gross_eur * ($row_customer->moda_no_claim_discount_percentage / 100), 2);

                $invoice_amount_gross -= $invoice_amount_no_claim;
                $invoice_amount_gross_eur -= $invoice_amount_no_claim_eur;
            }*/

            /*if (!empty($array_after_inv))
            {
                foreach ($array_after_inv as $item)
                {
                    $preview_invoice_lines[] = [
                        "inli_in_id" => $item['inli_in_id'],
                        "inli_ktrecupo_id" => $item['inli_ktrecupo_id'],
                        "inli_group" => $item['inli_group'],
                        "inli_description" => $item['inli_description'],
                        "inli_no_claim_discount" => $item['inli_no_claim_discount'],
                        "inli_currency" => $item['inli_currency'],
                        "inli_amount" => $item['inli_amount'],
                        "inli_amount_discount" => $item['inli_amount_discount'],
                        "inli_amount_netto" => $item['inli_amount_netto'],
                        "inli_amount_customer" => $item['inli_amount_customer'],
                        "inli_amount_discount_customer" => $item['inli_amount_discount_customer'],
                        "inli_amount_netto_customer" => $item['inli_amount_netto_customer'],
                        "inli_amount_eur" => $item['inli_amount_eur'],
                        "inli_amount_discount_eur" => $item['inli_amount_discount_eur'],
                        "inli_amount_netto_eur" => $item['inli_amount_netto_eur'],
                        "inli_leac_number" => $item['inli_leac_number']
                    ];
                }
            }*/

            $total_no_claim_discount = 0;
            $total_no_claim_discount_eur = 0;

            if(!empty($no_claim_rows)) {
                foreach ($no_claim_rows as $group => $row) {
                    $total_no_claim_discount += $system->currencyConvert($row['price_excl'], $row['po_pacu_code'], $row_customer->cu_pacu_code);
                    $total_no_claim_discount_eur += $system->currencyConvert($row['price_excl'], $row['po_pacu_code'], "EUR");
                }
            }

            $invoice_amount_gross -= round($total_no_claim_discount, 2);
            $invoice_amount_gross_eur -= round($total_no_claim_discount_eur, 2);

            //PART FOR SUBSCRIPTIONS

            $invoice_subscriptions_block = [];
            $invoice_subscriptions_total = 0;
            $invoice_subscriptions_total_eur = 0;

            $subscriptions = KTCustomerSubscription::leftJoin("subscriptions", "sub_id", "ktcusu_sub_id")
                ->where("ktcusu_cu_id", $row_customer->cu_id)
                ->where("ktcusu_active", 1)
                ->where("ktcusu_invoiced", 0)
                ->get();

            if (count($subscriptions) > 0) {
                foreach($subscriptions as $subscription)
                {
                    $pacu_token = PaymentCurrency::where("pacu_code", $subscription->ktcusu_pacu_code)->first()->pacu_token;
                    $invoice_subscriptions_block[] = [
                        "description" => $subscription->sub_name." (".$subscription->ktcusu_start_date."/".$subscription->ktcusu_end_date.")",
                        "price" => $subscription->ktcusu_price_this_month,
                        "currency" => $subscription->ktcusu_pacu_code,
                        "price_excl" => $pacu_token." ".$system->numberFormat($subscription->ktcusu_price_this_month, 2, ",", ".")
                    ];

                    $invoice_subscriptions_total = $invoice_subscriptions_total + $subscription->ktcusu_price_this_month;
                    $invoice_subscriptions_total_eur = $invoice_subscriptions_total_eur + $system->currencyConvert($subscription->ktcusu_price_this_month, $subscription->ktcusu_pacu_code,"EUR");
                }


                $invoice_amount_gross += round($invoice_subscriptions_total, 2);
                $invoice_amount += round($invoice_subscriptions_total, 2);
                $invoice_amount_eur += round($invoice_subscriptions_total_eur, 2);
                $invoice_amount_gross_eur += round($invoice_subscriptions_total_eur, 2);

                $invoice_subscriptions_total = $pacu_token." ".System::numberFormat($invoice_subscriptions_total,2, ",", ".");
            }


            $invoice_amount_vat = (($invoice_amount_gross / 100) * LedgerAccount::where("leac_number", $row_customer->cu_leac_number)->first()->leac_vat);
            $invoice_amount_netto = $invoice_amount_gross + $invoice_amount_vat;
            $invoice_amount_vat_eur = (($invoice_amount_gross_eur / 100) * LedgerAccount::where("leac_number", $row_customer->cu_leac_number)->first()->leac_vat);
            $invoice_amount_netto_eur = $invoice_amount_gross_eur + $invoice_amount_vat_eur;

            if ($row_customer->cu_payment_method == PaymentMethod::BANK_TRANSFER)
            {
                $payment_text = sprintf($collected_translation["invoice_payment_method_" . $row_customer->cu_payment_method][$row_customer->cu_la_code], $row_customer->cu_payment_term, System::getSetting("company_iban"), System::getSetting("company_bank_account_name"));
            }
            elseif($row_customer->cu_payment_method == PaymentMethod::CREDIT_CARD || $row_customer->cu_payment_method == PaymentMethod::AUTO_DEBIT)
            {
                $payment_text = sprintf($collected_translation["invoice_payment_method_" . $row_customer->cu_payment_method][$row_customer->cu_la_code], date("d-m-Y", strtotime("+". $row_customer->cu_payment_term." days")));
            }
            elseif ($row_customer->cu_payment_method == PaymentMethod::PREPAYMENT || $row_customer->cu_payment_method == PaymentMethod::PREPAYMENT_AUTO_CHARGE_CREDIT_CARD || $row_customer->cu_payment_method == PaymentMethod::PREPAYMENT_AUTO_DIRECT_DEBIT)
            {
                $balance = Mover::customerBalancesNew($row_customer->cu_id);

                $payment_text = sprintf($collected_translation["invoice_payment_method_" . $row_customer->cu_payment_method][$row_customer->cu_la_code], $system->paymentCurrencyToken($row_customer->cu_pacu_code) . " " . System::numberFormat($balance, 2), $system->paymentCurrencyToken($row_customer->cu_pacu_code) . " " . System::numberFormat(($balance - $invoice_amount_netto), 2));
            } else
            {
                $payment_text =$collected_translation["invoice_payment_method_" . $row_customer->cu_payment_method][$row_customer->cu_la_code];
            }

            $invoice = [
                "in_id" => $in_id,
                "in_timestamp" => date("Y-m-d H:i:s"),
                "in_cu_id" => $row_customer->cu_id,
                "in_la_code" => $row_customer->cu_la_code,
                "in_number" => $in_number,
                "in_date" => $request->invoice_date,
                "in_expiration_date" => date("Y-m-d", strtotime("+" . $row_customer->cu_payment_term . " days")),
                "in_period_from" => ((isset($invoice_period_from)) ? $invoice_period_from : $request->period_from),
                "in_period_to" => $request->period_to,
                "in_additional_information" => $request->additional_information ?? "",
                "in_payment_method" => $row_customer->cu_payment_method,
                "in_payment_text" => $payment_text,
                "in_company_data" => $system->serialize($company_data),
                "in_debtor_data" => $system->serialize($debtor_data),
                "in_lines" => $invoice_lines,
                "in_currency" => $row_customer->cu_pacu_code,
                "in_amount" => $invoice_amount,
                "in_amount_discount" => $invoice_amount_discount,
                "in_amount_no_claim" => round($total_no_claim_discount * 100) / 100,
                "in_amount_gross" => $invoice_amount_gross,
                "in_amount_vat" => $invoice_amount_vat,
                "in_amount_netto" => $invoice_amount_netto,
                "in_amount_eur" => $invoice_amount_eur,
                "in_amount_discount_eur" => $invoice_amount_discount_eur,
                "in_amount_no_claim_eur" => round($total_no_claim_discount_eur * 100) / 100,
                "in_amount_gross_eur" => $invoice_amount_gross_eur,
                "in_amount_vat_eur" => $invoice_amount_vat_eur,
                "in_amount_netto_eur" => $invoice_amount_netto_eur,
                "in_vat_percentage" => LedgerAccount::where("leac_number", $row_customer->cu_leac_number)->first()->leac_vat,
                "in_requests" => System::serialize($requests),
                "in_cu_leac" => $row_customer->cu_leac_number,
                "moda_no_claim_discount_percentage" => $row_customer->moda_no_claim_discount_percentage
            ];

            $invoice_lines = [];
            $invoice_totals = [];

            foreach ($preview_invoice_lines as $key => $preview_invoice_line)
            {
                $group = $preview_invoice_line['inli_group'];
                $group = str_replace("[", "", $group);
                $group = str_replace("]", "", $group);
                $desc = $preview_invoice_line['inli_description'];

                if (!array_key_exists($group, $invoice_lines))
                {
                    $invoice_lines[$group] = [];
                }

                if (!array_key_exists($desc, $invoice_lines[$group]))
                {
                    $invoice_lines[$group][$desc] = [];
                }

                $price = (string)$preview_invoice_line['inli_amount'];
                $discount = (string)$preview_invoice_line['inli_amount_discount'];

                if (!array_key_exists($price, $invoice_lines[$group][$desc]))
                {
                    $invoice_lines[$group][$desc][$price] = [];
                }

                if (!array_key_exists($discount, $invoice_lines[$group][$desc][$price]))
                {
                    $invoice_lines[$group][$desc][$price][$discount] = [
                        "quantity" => 0,
                        "amount" => 0,
                        "amount_discount" => 0,
                        "amount_netto" => 0,
                    ];

                }

                $invoice_lines[$group][$desc][$price][$discount]['quantity']++;
                $invoice_lines[$group][$desc][$price][$discount]['currency'] = $preview_invoice_line['inli_currency'];
                $invoice_lines[$group][$desc][$price][$discount]['amount'] += $preview_invoice_line['inli_amount'];
                $invoice_lines[$group][$desc][$price][$discount]['amount_discount'] += $preview_invoice_line['inli_amount_discount'];
                $invoice_lines[$group][$desc][$price][$discount]['amount_netto'] += $preview_invoice_line['inli_amount_netto'];

                if (!array_key_exists($group, $invoice_totals))
                {
                    $invoice_totals[$group]['local'] = 0;

                    if ($preview_invoice_line['inli_currency'] != $invoice['in_currency'])
                    {
                        $invoice_totals[$group][$invoice['in_currency']] = 0;
                    }
                }

                $invoice_totals[$group]['local'] += $preview_invoice_line['inli_amount_netto'];

                if ($preview_invoice_line['inli_currency'] != $invoice['in_currency'])
                {
                    $invoice_totals[$group][$invoice['in_currency']] += $preview_invoice_line['inli_amount_netto_customer'];
                }
            }

            $invoice_no_claim_discount_block = [];

            if (!empty($no_claim_rows))
            {
                $pacu_token_customer = PaymentCurrency::select("pacu_token")->where("pacu_code", $row_customer->cu_pacu_code)->first()->pacu_token;

                foreach ($no_claim_rows as $group => $row)
                {
                    $invoice_no_claim_discount_block[] = [
                        "group" => $group,
                        "count" => $row['count'],
                        "price" => $row['price'],
                        //"price_excl" => $pacu_token_customer." -".number_format($row['price_excl'], 2, ",", "."),
                        "price_excl" => $pacu_token_customer." -".$system->numberFormat($system->currencyConvert($row['price_excl'], $row['po_pacu_code'], $row_customer->cu_pacu_code) , 2, ",", "."),
                    ];

                }
            }


            $fields = [
                "invoice" => $invoice,
                "invoice_lines" => $invoice_lines,
                "invoice_totals" => $invoice_totals,

                //Subscriptions data
                "invoice_subscriptions_block" => $invoice_subscriptions_block,
                "invoice_subscriptions_total" => $invoice_subscriptions_total,

                //No claim discount data
                "invoice_no_claim_discount_block" => $invoice_no_claim_discount_block,
                "no_claim_currency" => $row_customer->cu_pacu_code,
                "no_claim_discount_total" => (($row_customer->cu_pacu_code == "EUR") ? round($total_no_claim_discount_eur, 2) : round($total_no_claim_discount, 2)),
            ];

            echo "<iframe srcdoc=\"" . str_replace("\"", "'", $mail->send("invoice", $invoice['in_la_code'], $fields, null, null, null, true)) . "\" width=\"100%\" height=\"690\" frameborder=\"0\"></iframe>";
        }
    }
}
