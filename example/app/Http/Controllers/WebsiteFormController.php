<?php

namespace App\Http\Controllers;

use App\Data\VolumeType;
use App\Models\Country;
use App\Models\Language;
use App\Models\Portal;
use App\Models\WebsiteForm;
use Illuminate\Http\Request;

class WebsiteFormController extends Controller
{
	public function edit($website_id, $website_form_id)
	{
		$websiteform = WebsiteForm::find($website_form_id);
        
        foreach(Language::where("la_iframe_only", 0)->get() as $language){
            $languages[$language->la_code] = $language->la_language;
        }
        
        foreach(Country::orderBy("co_en")->get() as $country)
        {
            $countries[$country->co_code] = $country->co_en;
        }
		
		return view('website.websiteform.edit',
			[
				'websiteform' => $websiteform,
                'portals' => Portal::with("country")->get(),
                'volumes' => VolumeType::all(),
                'languages' => Language::whereIn("la_code", ["NL", "DK", "DE", "IT", "ES", "FR", "EN"])->get(),
                'countries' => $countries,
                'website_id' => $website_id,
                'websiteform_id' => $website_form_id
			]);
		
	}
	
	public function update(Request $request, $website_id, $websiteform_id)
	{
	    
	    
        $websiteform = WebsiteForm::findOrFail($websiteform_id);
        
        $websiteform->wefo_we_id = $request->website_id;
        $websiteform->wefo_name = $request->name;
        $websiteform->wefo_po_id = $request->portal;
        $websiteform->wefo_la_code = $request->language;
        $websiteform->wefo_volume_type = $request->volume;
        $websiteform->wefo_analytics_ua_code = $request->analytics;
        $websiteform->wefo_facebook = ($request->facebook === 'on' ? 1 : 0);
        
        $websiteform->save();
        
		return redirect('admin/websites/' . $website_id . '/edit');
	}
	
	public function create($website_id)
	{
	 
		
		return view('website.websiteform.create',
			[
				'portals' => Portal::with("country")->get(),
				'volumes' => VolumeType::all(),
				'languages' => Language::whereIn("la_code", ["NL", "DK", "DE", "IT", "ES", "FR", "EN"])->get(),
                'website_id' => $website_id
			]);
		
	}
	
	public function store(Request $request)
	{
		
		$websiteform = new WebsiteForm();
        
        $websiteform->wefo_we_id = $request->website_id;
        $websiteform->wefo_token = md5(uniqid(time(), true).uniqid($_POST['name'], true));
        $websiteform->wefo_name = $request->name;
        $websiteform->wefo_po_id = $request->portal;
        $websiteform->wefo_la_code = $request->language;
        $websiteform->wefo_volume_type = $request->volume;
        $websiteform->wefo_analytics_ua_code = $request->analytics;
        $websiteform->wefo_facebook = ($request->facebook === 'on' ? 1 : 0);
        
        $websiteform->save();
		
		
		return redirect('admin/websites/'.$request->website_id."/edit");
		
	}
}
