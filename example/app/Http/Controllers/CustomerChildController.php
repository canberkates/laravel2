<?php

namespace App\Http\Controllers;

use App\Data\CustomerChildMoverPortalType;
use App\Data\CustomerChildSireloType;
use App\Data\CustomerChildType;
use App\Functions\Mover;
use App\Models\Country;
use App\Models\Customer;
use App\Models\CustomerOffice;
use App\Models\MoverData;
use App\Models\State;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Log;

class CustomerChildController extends Controller
{
	public function edit($customer_id, $office_id)
	{
        $customer = Customer::findOrFail($customer_id);
        $office = CustomerOffice::find($office_id);

        return view('customerchild.edit',
            [
                'customer' => $customer,
                'customers' =>  Customer::select("cu_id", "cu_company_name_business", "cu_city", "cu_co_code")
                    ->where("cu_deleted", 0)
                    ->where("cu_id", "!=", $customer_id)
                    /*->where(function($query) use ($customer_id, $office_id) {
                        $query->where("cu_parent_id", "!=", $customer_id)
                            ->orWhereNull("cu_parent_id")
                            ->orWhere("cu_id", $office_id);
                    })*/
                    ->where("cu_type", 1)->get(),
                'office' => $office,
                'customerchildtypes' => CustomerChildType::all(),
                'customerchildsirelotypes' => CustomerChildSireloType::all(),
                'customerchildmoverportaltypes' => CustomerChildMoverPortalType::all(),
            ]);
	}

	public function update(Request $request, $office_id)
	{
        $office = CustomerOffice::find($office_id);
        $office->cuof_child_id = $request->child;
        $office->save();

        return redirect('customers/' . $office->cuof_cu_id . "/edit")->with('message', 'Child is successfully updated!');
	}

	public function create($customer_id, Request $request)
	{
		$customer = Customer::leftJoin("mover_data", "moda_cu_id", "cu_id")->findOrFail($customer_id);

		return view('customerchild.create',
			[
				'customer' => $customer,
				'customers' => Customer::select("cu_id", "cu_company_name_business", "cu_city", "cu_co_code")
                                ->where("cu_deleted", 0)
                                ->where("cu_id", "!=", $customer_id)
                                ->where(function($query) use ($customer_id) {
                                    $query->where("cu_parent_id", "!=", $customer_id)
                                        ->orWhereNull("cu_parent_id");
                                })
                                ->where("cu_type", 1)->get(),
                'child' => $request->cu_id,
                'office_id' => $request->office_id,
                'customerchildtypes' => CustomerChildType::all(),
                'customerchildsirelotypes' => CustomerChildSireloType::all(),
                'customerchildmoverportaltypes' => CustomerChildMoverPortalType::all(),

			]);
	}

	public function store(Request $request)
	{
		$request->validate([
			'child' => 'required',
		]);

		$customer = Customer::findOrFail($request->child);
		$customer->cu_parent_id = $request->customer_id;
		$customer->save();

		if(!$request->office_id)
        {
            $customeroffice = new CustomerOffice();

        }else{
            $customeroffice = CustomerOffice::where("cuof_id", $request->office_id)->first();
        }

        $customeroffice->cuof_cu_id = $request->customer_id;
        $customeroffice->cuof_child_id = $request->child;
        $customeroffice->cuof_street_1 = $customer->cu_street_1;
        $customeroffice->cuof_city = $customer->cu_city;
        $customeroffice->cuof_co_code = $customer->cu_co_code;
        $customeroffice->cuof_website = $customer->cu_website;
        $customeroffice->cuof_child_type = $request->child_type;

        $md = MoverData::where("moda_cu_id",$customer->cu_id)->first();


        if($request->show_on_sirelo){
            $customeroffice->cuof_child_sirelo_type = $request->sirelo_type;
            $md->moda_disable_sirelo_export = 0;
        }else{
            $md->moda_disable_sirelo_export = 1;
        }

        $md->save();


        //Set to correct customer and save
        $customeroffice->save();

		if ($customer->cu_type == 1)
		{
			return redirect('customers/' . $request->customer_id . "/edit")->with('message', 'Child is successfully added!');
		}
	}

    public function delete(Request $request)
    {
        $customer = Customer::where("cu_id", $request->id)->first();
        $customer->cu_parent_id = null;
        $customer->save();

        CustomerOffice::where("cuof_child_id", $request->id)->delete();


        DB::table('customer_offices')->where('cuof_id', '=', $request->id)->delete();
    }

    public function updateChildDefaults(Request $request, $customer_id)
    {
        $mover_data = MoverData::where("moda_cu_id", $customer_id)->first();
        $mover_data->moda_default_child_type = $request->child;
        $mover_data->moda_default_child_sirelo_type = $request->child_type;
        $mover_data->moda_default_child_mover_portal_type = $request->sirelo_type;
        $mover_data->save();

        return redirect('customers/' . $customer_id. "/edit")->with('message', 'Child defaults are successfully updated!');
    }

}
