<?php

namespace App\Http\Controllers;

use App\Data\LedgerAccountType;
use App\Data\TurnoverType;
use App\Models\LedgerAccount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LedgerAccountController extends Controller
{
	public function index()
	{
		$ledgeraccounts = LedgerAccount::all();
		
		return view('ledgeraccounts.index',
		[
			'ledgeraccounts' => $ledgeraccounts,
			'ledgeraccounttypes' => LedgerAccountType::all(),
            'turnovertypes' => TurnoverType::all()
		]);
	}
    
    
    public function edit($ledger_id)
    {
        $ledger = LedgerAccount::where('leac_number', $ledger_id)->first();
        
        return view('ledgeraccounts.edit',
            [
                'ledgeraccounttypes' => LedgerAccountType::all(),
                'turnovertypes' => TurnoverType::all(),
                'ledger' => $ledger
            ]);
        
    }
    
    public function update(Request $request, $ledger_id)
    {
        $request->validate([
            'ledgertype' => 'required',
            'name' => 'required'
        ]);
        
        DB::table('ledger_accounts')
            ->where("leac_number", $ledger_id)
            ->update(
                [
                    'leac_type' =>  $request->ledgertype,
                    'leac_name' =>  $request->name,
                    'leac_turnover_type' => $request->turnovertype,
                    'leac_vat' => $request->leac_vat ?? 0.00
                ]
            );
    
        return redirect('admin/ledgeraccounts/' . $ledger_id . '/edit');
    }
    
    
    public function create()
    {
        return view('ledgeraccounts.create',
            [
                'ledgeraccounttypes' => LedgerAccountType::all(),
                'turnovertypes' => TurnoverType::all()
            ]);
        
    }
    
    public function store(Request $request)
    {
        $request->validate([
            'number' => 'required',
            'ledgertype' => 'required',
            'name' => 'required',
            'turnovertype' => 'required'
        ]);
        
        $ledger = new LedgerAccount();
    
        $ledger->leac_number = $request->number;
        $ledger->leac_type = $request->ledgertype;
        $ledger->leac_name = $request->name;
        $ledger->leac_turnover_type = $request->turnovertype;
        $ledger->leac_vat = $request->leac_vat ?? 0.00;
    
        $ledger->save();
        
        return redirect('admin/ledgeraccounts/');
        
    }
	
	
}
