<?php

namespace App\Http\Controllers;

use App\Functions\DataIndex;
use App\Functions\System;
use App\Models\LedgerAccount;
use DB;
use Illuminate\Http\Request;

class BalanceJournalEntryController extends Controller
{
	public function index()
	{
		
		return view('finance.balancejournalentry', [
			'selected_date' => "",
			'date_filter' => [],
			'filtered_data' => []
		]);
	}
	
	public function filteredIndex(Request $request)
	{
		$request->validate([
			'date' => 'required'
		]);
		
		$total = 0;
		$amounts = [];
		
		$ledgers = LedgerAccount::where("leac_type", 3)
			->get();
		
		foreach($ledgers as $leac)
		{
			$amounts[$leac->leac_number] = 0;
			
			$bank_lines = DB::table("kt_bank_line_invoice_customer_ledger_account")
				->where("ktbaliinculeac_leac_number", $leac->leac_number)
				->whereBetween("ktbaliinculeac_date", System::betweenDates($request->date))
				->get();
			
			foreach($bank_lines as $bank_line)
			{
				$amounts[$leac->leac_number] += $bank_line->ktbaliinculeac_amount;
			}
			
			$total += $amounts[$leac->leac_number];
		}
		
		
		return view('finance.balancejournalentry', [
			'selected_date' => $request->date,
			'ledgers' => $ledgers,
			'total' => $total,
			'amount' => $amounts
		]);
		
	}
	
}
