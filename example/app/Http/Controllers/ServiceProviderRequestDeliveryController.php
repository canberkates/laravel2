<?php

namespace App\Http\Controllers;

use App\Data\ServiceProviderRequestDeliveryType;
use App\Models\Customer;
use App\Models\KTCustomerQuestion;
use App\Models\RequestDelivery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ServiceProviderRequestDeliveryController extends Controller
{
	public function create($customer_id, $cuqu_id)
	{
		$customer = Customer::findOrFail($customer_id);
		$kt_customer_question = KTCustomerQuestion::findOrFail($cuqu_id);
		
		return view('serviceproviderrequestdelivery.create',
			[
				'customer' => $customer,
				'kt_customer_question' => $kt_customer_question,
				'requestdeliverytypes' => ServiceProviderRequestDeliveryType::all()
			]);
	}
	
	public function store(Request $request)
	{
		
		$request->validate([
			'type' => 'required',
			'value' => 'required'
		]);
		
		$request_delivery = new RequestDelivery();
		
		$request_delivery->rede_customer_type = 2;
		$request_delivery->rede_ktcuqu_id = $request->ktcuqu_id;
		$request_delivery->rede_type = $request->type;
		$request_delivery->rede_value = $request->value;
		$request_delivery->rede_extra = $request->extra;
		
		$request_delivery->save();
		
		return redirect('serviceproviders/'.$request->customer_id."/question/".$request->ktcuqu_id."/edit");
	}
	
	public function edit($id, $cuqu_id, $rede_id)
	{
		// get the customer and relations
		$customer = Customer::findOrFail($id);
		
		$request_delivery = RequestDelivery::findOrFail($rede_id);
		
		return view('serviceproviderrequestdelivery.edit',
			[
				//Customer info
				'customer' => $customer,
				'requestdeliverytypes' => ServiceProviderRequestDeliveryType::all(),
				'requestdelivery' => $request_delivery
			]);
	}
	
	public function update(Request $request, $rede_id)
	{
		$request_delivery = RequestDelivery::find($rede_id);
		
		Log::debug($request_delivery);
		
		$request->validate([
			'type' => 'required',
			'value' => 'required'
		]);
		
		$request_delivery->rede_type = $request->type;
		$request_delivery->rede_value = $request->value;
		$request_delivery->rede_extra = $request->extra;
		
		$request_delivery->save();
		
		return redirect('serviceproviders/' . $request->customer_id.'/question/'.$request->ktcuqu_id . '/edit');
	}
	
	public function requestDeliveryDelete(Request $request)
	{
		$req_delivery = RequestDelivery::findOrFail($request->id);
		
		$req_delivery->delete();
	}
}
