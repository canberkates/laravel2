<?php

namespace App\Http\Controllers;

use App\Data\AffiliateFormType;
use App\Data\CustomerRequestDeliveryType;
use App\Data\VolumeType;
use App\Models\AffiliatePartnerForm;
use App\Models\Country;
use App\Models\Customer;
use App\Models\Language;
use App\Models\MoverDirectPricingRange;
use App\Models\MoverDirectPricingSettings;
use App\Models\MoverFormSettings;
use App\Models\MoverPriceEstimationSettings;
use App\Models\Portal;
use App\Models\RequestDelivery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Log;

class ConversionToolsController extends Controller
{

	public function edit($customer_id, $form_id)
	{
		//Get customer and through the relation of the customer
        $conversion_form = MoverFormSettings::leftJoin("affiliate_partner_forms", "afpafo_id", "mofose_afpafo_id")->where("mofose_id", $form_id)->first();
        $customer = Customer::where("cu_id", $conversion_form->mofose_cu_id)->first();

        $request_deliveries = RequestDelivery::where("rede_mofose_id", $conversion_form->mofose_id)->get();

        //Check if this user has the rights to edit
        if (Gate::denies('customer-restrictions', $customer)) {
            abort(403);
        }

		return view('conversion_tools.edit',
			[
                'customer' => $customer,
				'conversion_form' => $conversion_form,
				'volumetypes' => VolumeType::all(),
                'languages' => Language::all(),
                'requestdeliveries' => $request_deliveries,
                'requestdeliverytypes' =>  CustomerRequestDeliveryType::all()
			]);
	}


	public function update(Request $request, $customer_id, $form_id)
	{
		$request->validate([
			'form_name' => 'required',
			'volume_type' => 'required',
            'language' => 'required'
		]);

        //Find the mover form
        $mover_form = MoverFormSettings::where("mofose_id", $form_id)->first();
        $mover_form->mofose_email_from = $request->email_from;
        $mover_form->mofose_email_to = $request->email_to;
        $mover_form->save();

		//Find the form
		$affiliate_form = AffiliatePartnerForm::find($mover_form->mofose_afpafo_id);
		$affiliate_form->afpafo_name = $request->form_name;
		$affiliate_form->afpafo_volume_type = $request->volume_type;
		$affiliate_form->afpafo_la_code = $request->language;
		$affiliate_form->save();

		//Redirect to affiliate partners edit
		return redirect('customers/' .$customer_id. '/conversion_tools/'.$form_id.'/edit');
	}


	public function create($customer_id)
	{
		$customer = Customer::findOrFail($customer_id);

		return view('conversion_tools.create',
			[
				'customer' => $customer,
				'volumetypes' => VolumeType::all(),
                'languages' => Language::all()
			]);
	}



    public function delete(Request $request)
    {
        $form = AffiliatePartnerForm::findOrFail($request->id);
        $form->afpafo_deleted = 1;
        $form->save();

        MoverFormSettings::where("mofose_afpafo_id", $request->id)->delete();

    }

	public function store(Request $request, $customer_id)
	{
		$request->validate([
			'form_name' => 'required',
			'volume_type' => 'required',
			'language' => 'required'
		]);


		// Validate the request...
		$customer = Customer::findOrFail($customer_id);

		//Create new form
		$affiliate_form = new AffiliatePartnerForm();

		$affiliate_form->afpafo_token = md5(time());
		$affiliate_form->afpafo_name = $request->form_name;
		$affiliate_form->afpafo_form_type = AffiliateFormType::CONVERSION_TOOLS;
        $affiliate_form->afpafo_volume_type = $request->volume_type;
        $affiliate_form->afpafo_la_code = $request->language;
		$affiliate_form->afpafo_disable_ip_check = 0;
		$affiliate_form->afpafo_hide_pre_form = 0;
		$affiliate_form->afpafo_hide_questions = 0;

		//Set to correct customer and save
		$affiliate_form->customer()->associate($customer);

		$affiliate_form->save();

        $mover_form = new MoverFormSettings();

        $mover_form->mofose_cu_id = $customer->cu_id;
        $mover_form->mofose_afpafo_id = $affiliate_form->afpafo_id;
        $mover_form->mofose_theme_color = "#009fe3";
        $mover_form->mofose_show_vc = 1;
        $mover_form->mofose_email_from = $request->email_from;
        $mover_form->mofose_email_to = $request->email_to;

        $mover_form->save();

        $mover_price_settings = new MoverPriceEstimationSettings();

        $mover_price_settings->mopres_mofose_id = $mover_form->mofose_id;
        $mover_price_settings->mopres_price_estimation_currency = $customer->cu_pacu_code;

        $mover_price_settings->save();

        $mover_direct_pricing_settings = new MoverDirectPricingSettings();

        $mover_direct_pricing_settings->modiprse_mofose_id = $mover_form->mofose_id;
        $mover_direct_pricing_settings->modiprse_currency = $customer->cu_pacu_code;

        $mover_direct_pricing_settings->save();

        $direct_pricing_default_ranges = [
            0 => [1,4,1,1,25],
            1 => [5,10,2,0.8,25],
            2 => [11,15,2,0.75,25],
            3 => [16,35,3,0.75,55],
            4 => [36,50,4,0.75,55],
            5 => [51,70,4,0.75,80]
        ];

        foreach($direct_pricing_default_ranges as $index => $values){
            $mover_direct_pricing_range = new MoverDirectPricingRange();

            $mover_direct_pricing_range->modiprra_modiprse_id = $mover_direct_pricing_settings->modiprse_id;

            $mover_direct_pricing_range->modiprra_from_volume = $values[0];
            $mover_direct_pricing_range->modiprra_to_volume = $values[1];
            $mover_direct_pricing_range->modiprra_number_of_people = $values[2];
            $mover_direct_pricing_range->modiprra_hours_per_m3 = $values[3];
            $mover_direct_pricing_range->modiprra_truck_cost_per_hour = $values[4];

            $mover_direct_pricing_range->save();
        }



		return redirect('customers/' . $customer_id .'/edit');
	}
}
