<?php

namespace App\Http\Controllers;

use App\Data\CustomerEditForms;
use App\Data\ServiceProviderRequestDeliveryType;
use App\Models\Country;
use App\Models\Customer;
use App\Models\KTCustomerQuestion;
use App\Models\KTCustomerQuestionCountryDestination;
use App\Models\KTCustomerQuestionCountryOrigin;
use App\Models\KTCustomerQuestionLanguage;
use App\Models\Language;
use App\Models\MacroRegion;
use App\Models\Question;
use App\Models\RequestDelivery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class ServiceProviderQuestionsController extends Controller
{
	public function create($customer_id)
	{
		$customer = Customer::findOrFail($customer_id);

		return view('serviceproviderquestion.create',
			[
				'customer' => $customer,
				'questions' => Question::all()
			]);
	}

	public function store(Request $request)
	{
		$request->validate([
			'question' => 'required'
		]);

		// Validate the request...
		//Create new form
		$customerquestion = new KTCustomerQuestion();

		$customerquestion->ktcuqu_qu_id = $request->question;
		$customerquestion->ktcuqu_cu_id = $request->customer_id;
		$customerquestion->ktcuqu_description = $request->description;

		//Set to correct customer and save

		$customerquestion->save();


		return redirect('serviceproviders/' . $request->customer_id .'/edit');
	}

	public function edit($customer_id, $question_id)
	{
		// get the customer and relations
		$customer = Customer::with(['customerquestions.questions'])->findOrFail($customer_id);

        if (Gate::denies('customer-restrictions', $customer)) {
            abort(403);
        }

		$question = KTCustomerQuestion::findOrFail($question_id);


		$set_languages = KTCustomerQuestionLanguage::where('ktcuqula_ktcuqu_id', $question_id)->get();



		$question_languages = [];

		foreach($set_languages as $sl)
		{
			$question_languages[] .= $sl->ktcuqula_la_code;
		}

		$request_deliveries = RequestDelivery::where([["rede_customer_type", "=", 2], ["rede_ktcuqu_id", "=", $question->ktcuqu_id]])->get();

		$countries = [];

		foreach(MacroRegion::all() as $macro_region)
		{
			$macro_regions[$macro_region->mare_continent_name][$macro_region->mare_id] = [
				'mare_name' => $macro_region->mare_name
			];
		}

		$query_countries = Country::orderBy("co_en", "ASC")->get();

		foreach($query_countries as $row_countries)
		{
			$countries[$row_countries->co_mare_id][$row_countries->co_code] = $row_countries->co_en;;
		}

		$origins_checked = [];

		$query_checked = KTCustomerQuestionCountryOrigin::where([["ktcuqucoor_ktcuqu_id", "=", $question->ktcuqu_id]])->get();

		foreach($query_checked as $row_checked)
		{
			$origins_checked[$row_checked->ktcuqucoor_co_code] = true;
		}

		$destination_checked = [];

		$query_checked = KTCustomerQuestionCountryDestination::where([["ktcuqucode_ktcuqu_id", "=", $question->ktcuqu_id]])->get();

		foreach($query_checked as $row_checked)
		{
			$destination_checked[$row_checked->ktcuqucode_co_code] = true;
		}

		return view('serviceproviderquestion.edit',
			[
				//Customer info
				'question_id' => $question_id,
				'question' => $question,
				'customer' => $customer,
				'questions' => Question::all(),
				'questionstatuses' => [0 => 'Inactive', 1 => 'Active'],
				'languages' => Language::where("la_iframe_only", 0)->orderBy('la_language')->get(),
				'question_languages' => $question_languages,
				'request_deliveries' => $request_deliveries,
				'countries' => $countries,
				'originschecked' => $origins_checked,
				'destinationschecked' => $destination_checked,
				'requestdeliveries' => ServiceProviderRequestDeliveryType::all(),
				'macro_regions' => $macro_regions
			]);
	}

	public function update(Request $request, $question_id)
	{
		$question = KTCustomerQuestion::findOrFail($question_id);

		if ($request->form_name == CustomerEditForms::GENERAL)
		{
			$request->validate([
				'delay' => 'required',
				'rate' => 'required',
			]);

			$rate = str_replace(",", ".", $request->rate);

			if($rate < 0)
			{
				$rate = $rate * -1;
			}

			$question->ktcuqu_description = $request->description;
			$question->ktcuqu_status = $request->status;
			$question->ktcuqu_delay = $request->delay;
			$question->ktcuqu_rate = $rate;
			$question->ktcuqu_international_moves = ((isset($request->international_moves)) ? 1 : 0);
			$question->ktcuqu_national_moves = ((isset($request->national_moves)) ? 1 : 0);
			$question->ktcuqu_max_requests_month = $request->max_requests_month;
			$question->ktcuqu_max_requests_month_left = $request->max_requests_month_left;
			$question->ktcuqu_max_requests_day = $request->max_requests_day;
			$question->ktcuqu_max_requests_day_left = $request->max_requests_day_left;

			//Filters Languages
			$languages = Language::where("la_iframe_only", 0)->get();

			foreach($languages as $row_languages)
			{
				if (isset($request->newslanguages[$row_languages->la_code]))
				{
					//check if this language is already added to this question
					$question_lang = KTCustomerQuestionLanguage::where([["ktcuqula_ktcuqu_id", "=", $question->ktcuqu_id], ["ktcuqula_la_code", "=", $row_languages->la_code]])->first();

					if (empty($question_lang))
					{
						$kt_customer_question_language = new KTCustomerQuestionLanguage();
						$kt_customer_question_language->ktcuqula_ktcuqu_id = $question->ktcuqu_id;
						$kt_customer_question_language->ktcuqula_la_code = $row_languages->la_code;
						$kt_customer_question_language->save();
					}
				}
				else
				{
					$lang = KTCustomerQuestionLanguage::where("ktcuqula_ktcuqu_id", "=", $question->ktcuqu_id)->where("ktcuqula_la_code", "=", $row_languages->la_code)->first();
					if (!empty($lang))
						$lang->delete();
				}
			}

			//Save question
			$question->save();

            return redirect('serviceproviders/' . $question->ktcuqu_cu_id . '/question/'.$question->ktcuqu_id.'/edit#btabs-alt-static-general');
		}
		elseif ($request->form_name == "Origins")
		{
			DB::table('kt_customer_question_country_origin')->where('ktcuqucoor_ktcuqu_id', '=', $question->ktcuqu_id)->delete();

			foreach($request->origins as $id => $origin){
				$ktcuqucoor = new KTCustomerQuestionCountryOrigin();

				$ktcuqucoor->ktcuqucoor_ktcuqu_id = $question->ktcuqu_id;
				$ktcuqucoor->ktcuqucoor_co_code = $id;

				$ktcuqucoor->save();
			}
            return redirect('serviceproviders/' . $question->ktcuqu_cu_id . '/question/'.$question->ktcuqu_id.'/edit#btabs-alt-static-origins');
		}
		elseif ($request->form_name == "Destinations")
		{
			DB::table('kt_customer_question_country_destination')->where('ktcuqucode_ktcuqu_id', '=', $question->ktcuqu_id)->delete();

			foreach($request->destinations as $id => $origin){
				$ktcuqucoor = new KTCustomerQuestionCountryDestination();

				$ktcuqucoor->ktcuqucode_ktcuqu_id = $question->ktcuqu_id;
				$ktcuqucoor->ktcuqucode_co_code = $id;

				$ktcuqucoor->save();
			}
            return redirect('serviceproviders/' . $question->ktcuqu_cu_id . '/question/'.$question->ktcuqu_id.'/edit#btabs-alt-static-destinations');

		}

        return redirect('serviceproviders/' . $question->ktcuqu_cu_id . '/edit');
	}

	public function requestDeliveryDelete(Request $request)
	{
		$req_delivery = RequestDelivery::findOrFail($request->id);

		$req_delivery->delete();
	}

	public function createRequestdelivery()
	{
		// get the customer
		return view('customers.create',
			[
				'users' => User::where("id", "!=", 0),
				'countries' => Country::all(),
				'movertypes' => CustomerType::all(),
				'debtorstatuses' => DebtorStatus::all(),
				'paymentreminderstatuses' => PaymentReminderStatus::all(),
				'paymentmethods' => PaymentMethod::all(),
				'paymentcurrencies' => PaymentCurrency::all(),
				'ledgeraccounts' => LedgerAccount::all(),
				'languages' => Language::where("la_iframe_only", 0)->get()
			]);
	}

}
