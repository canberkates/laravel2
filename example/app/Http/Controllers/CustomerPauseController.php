<?php

namespace App\Http\Controllers;

use App\Data\CustomerStatusReason;
use App\Data\CustomerStatusStatus;
use App\Functions\Mover;
use App\Functions\System;
use App\Models\Customer;
use App\Models\CustomerRemark;
use App\Models\CustomerStatus;
use App\Models\PauseDay;
use App\Models\PauseHistory;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class CustomerPauseController extends Controller
{

	public function create($customer_id)
	{
		$customer = Customer::findOrFail($customer_id);

        $pause_days_calculate = Mover::getPauseDaysAmounts($customer_id);
        $days_used = $pause_days_calculate['days_total'] - $pause_days_calculate['days_left'];
        $days_total = $pause_days_calculate['days_total'];

		return view('customers.pauses.create', [
			'customer' => $customer,
            'days_used' => $days_used,
            'days_total' => $days_total
		]);
	}

	public function store(Request $request)
	{
		$request->validate([
			'pause_from' => 'required',
			'pause_to' => 'required',
            'remark' => 'required'
		]);

		$system = new System();

        //+1 because the difference calculation isnt inclusive of the last day
        $total_days = $system->dateDifference($request->pause_from, $request->pause_to);

        if($total_days < 1){
            return redirect()->back()->withErrors([ 'email'=> "You're unironically requesting a pause for less than 1 day? 🤠"]);
        }

        $pause_history = $this->CreatePauseHistory($request, $total_days);
        $pause_days = $this->UpdatePauseDays($request->customer_id, $total_days);

        //Send a message to the right people
        $system->sendMessage([1778, 4669, 4677], "A pause was requested through Phoenix!", Auth::user()->us_name." has requested a pause for (".$request->customer_id.") ".$request->customer_name.". The pause is from ".$request->pause_from." - ".$request->pause_to." for a total of ".$total_days." days. They have ".(60 - $request->days_used - $total_days)." days left this year.");
        $customerstatus = $this->CreatePauseCustomerStatus($request);

        //Associate the status with the remark
        $pause_history->ph_start_cust_id = $customerstatus->cust_id;

        //Use Partnerdesk or Finance as type if the correct managers add a status, Default: System
        $setting_department = 0;

        if(System::getSetting('default_partnerdesk_manager') == Auth::id())
        {
            $setting_department = 4;
        }
        elseif(System::getSetting('default_finance_manager') == Auth::id())
        {
            $setting_department = 2;
        }
        $this->CreateCustomerRemark($request, $customerstatus, $setting_department);

        $customerstatus = $this->CreateUnpauseCustomerStatus($request);

        //Associate the status with the remark
        $pause_history->ph_end_cust_id = $customerstatus->cust_id;
        $pause_history->save();

        //Create a Customer Remark
        $this->CreateCustomerRemark($request, $customerstatus, $setting_department);

        return redirect('customers/'.$request->customer_id.'/edit')->with('message', 'Pause has been successfully added!');

	}

    public function edit($customer_id, $pause_id)
    {
        $customer = Customer::findOrFail($customer_id);

        //Check if this user has the rights to edit
        if (Gate::denies('customer-restrictions', $customer)) {
            abort(403);
        }

        $pause = PauseHistory::findOrFail($pause_id);

        $pause_days_calculate = Mover::getPauseDaysAmounts($customer_id);
        $days_used = $pause_days_calculate['days_total'] - $pause_days_calculate['days_left'];
        $days_total = $pause_days_calculate['days_total'];

        return view('customers.pauses.edit', [
            'customer' => $customer,
            'days_used' => $days_used,
            'days_total' => $days_total,
            'pause' => $pause
        ]);
    }

    public function update(Request $request, $customer_id, $pause_id)
    {
        $request->validate([
            'pause_from' => 'required',
            'pause_to' => 'required',
            'remark' => 'required'
        ]);

        $system = new System();

        //+1 because the difference calculation isnt inclusive of the last day
        $total_days = $system->dateDifference($request->pause_from, $request->pause_to) + 1;

        //Get pause & update info
        $pause = PauseHistory::findOrFail($pause_id);

        //Get difference from previous total days and now.
        $difference = $total_days - $pause->ph_total_days;

        $pause->ph_start_date = $request->pause_from;
        $pause->ph_end_date = $request->pause_to;
        $pause->ph_remark = $request->remark;
        $pause->ph_total_days = $total_days;
        $pause->save();

        $this->UpdatePauseDays($pause->ph_cu_id, $difference);

        //Update start customer status
        $start_status = CustomerStatus::find($pause->ph_start_cust_id);
        $start_status->cust_date = $pause->ph_start_date;
        $start_status->cust_remark = $request->remark;
        $start_status->save();

        //Update start customer remark
        $start_remark = CustomerRemark::where("cure_status_id", $pause->ph_start_cust_id)->first();
        $start_remark->cure_timestamp =  $pause->ph_start_date;
        $start_remark->save();

        //Update end customer status
        $end_status = CustomerStatus::find($pause->ph_end_cust_id);
        $end_status->cust_date = $pause->ph_end_date;
        $end_status->cust_remark = $request->remark;
        $end_status->save();

        //Update start customer remark
        $end_remark = CustomerRemark::where("cure_status_id", $pause->ph_end_cust_id)->first();
        $end_remark->cure_timestamp = $pause->ph_end_date;
        $end_remark->save();

        return redirect('customers/'.$customer_id.'/edit')->with('message', 'Pause has successfully been edited!');

    }

	public function delete(Request $request)
	{
        $pause = PauseHistory::findOrFail($request->id);

        $start_cust_id = $pause->ph_start_cust_id;
        $end_cust_id = $pause->ph_end_cust_id;
        $customer_id = $pause->ph_cu_id;

        $this->UpdatePauseDays($pause->ph_cu_id, -$pause->ph_total_days);

        //Delete Pause History
        $pause->delete();

        //Update start customer remark
        $start_remark = CustomerRemark::where("cure_status_id", $start_cust_id)->first();
        $start_remark->delete();

        //Update start customer status
        $start_status = CustomerStatus::find($start_cust_id);
        $start_status->delete();

        //Update start customer remark
        $end_remark = CustomerRemark::where("cure_status_id", $end_cust_id)->first();
        $end_remark->delete();

        //Update end customer status
        $end_status = CustomerStatus::find($end_cust_id);
        $end_status->delete();

        return redirect('customers/'.$customer_id."/edit");
	}

    /**
     * @param Request $request
     * @param $total_days
     * @return PauseHistory
     */
    public function CreatePauseHistory(Request $request, $total_days): PauseHistory
    {
        //Create the DB entry
        $pause_history = new PauseHistory();

        $pause_history->ph_created_timestamp = Carbon::now()->toDateTimeString();
        $pause_history->ph_us_id = \Auth::id();
        $pause_history->ph_cu_id = $request->customer_id;
        $pause_history->ph_start_date = $request->pause_from;
        $pause_history->ph_end_date = $request->pause_to;
        $pause_history->ph_total_days = $total_days;
        $pause_history->ph_remark = $request->remark;

        return $pause_history;
    }

    /**
     * @param Request $request
     * @param $total_days
     */
    public function UpdatePauseDays($customer_id, $total_days)
    {
        //Change amount of pause days for this customer
        $pause_days = PauseDay::where("pada_cu_id", $customer_id)->where("pada_deleted", 0)->first();
        $pause_days->pada_amount_left = $pause_days->pada_amount_left - $total_days;
        $pause_days->save();

        return true;
    }

    /**
     * @param Request $request
     * @return CustomerStatus
     */
    public function CreatePauseCustomerStatus(Request $request): CustomerStatus
    {
        //Create the pause customer statuses
        $customerstatus = new CustomerStatus();

        $customerstatus->cust_cu_id = $request->customer_id;
        $customerstatus->cust_date = $request->pause_from;
        $customerstatus->cust_employee = \Auth::id();
        $customerstatus->cust_status = CustomerStatusStatus::PAUSE;
        $customerstatus->cust_reason = CustomerStatusReason::REQUESTED_BY_MOVER;
        $customerstatus->cust_turnover_netto = 0;
        $customerstatus->cust_remark = $request->remark;

        $customerstatus->save();

        return $customerstatus;
    }

    /**
     * @param Request $request
     * @param CustomerStatus $customerstatus
     * @param int $setting_department
     * @return CustomerRemark
     */
    public function CreateCustomerRemark(Request $request, CustomerStatus $customerstatus, int $setting_department): CustomerRemark
    {
        //Create a Customer Remark
        $customer_remark = New CustomerRemark();
        $customer_remark->cure_cu_id = $request->customer_id;
        $customer_remark->cure_timestamp = $customerstatus->cust_date;
        $customer_remark->cure_department = $setting_department;
        $customer_remark->cure_employee = \Auth::id();
        $customer_remark->cure_status_id = $customerstatus->cust_id;
        $customer_remark->save();

        return $customer_remark;
    }

    /**
     * @param Request $request
     * @return CustomerStatus
     */
    public function CreateUnpauseCustomerStatus(Request $request): CustomerStatus
    {
        $customerstatus = new CustomerStatus();

        $customerstatus->cust_cu_id = $request->customer_id;
        $customerstatus->cust_date = $request->pause_to;
        $customerstatus->cust_employee = \Auth::id();
        $customerstatus->cust_status = CustomerStatusStatus::UNPAUSE;
        $customerstatus->cust_reason = CustomerStatusReason::REQUESTED_BY_MOVER;
        $customerstatus->cust_turnover_netto = 0;
        $customerstatus->cust_remark = $request->remark;

        $customerstatus->save();

        return $customerstatus;
    }


}
