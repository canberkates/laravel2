<?php

namespace App\Http\Controllers;

use App\Data\AdyenPaymentStatus;
use App\Functions\System;
use App\Models\PaymentCurrency;
use DB;
use Illuminate\Http\Request;

class CreditCardPaymentHistoryController extends Controller
{
	public function index()
	{
		return view('finance.creditcardpaymenthistory', [
			'selected_date' => "",
            'selected_statuses' => [],
            'selected_batch_number' => "",
            'adyenpaymentstatus' => AdyenPaymentStatus::all()
		]);
	}
	
	public function filteredIndex(Request $request)
	{
        $request->validate([
			'date' => 'required'
		]);
		
        $system = new System();
        
        $query = DB::table("adyen_payments")
            ->select(	"adpa_id", "adpa_timestamp", "adpa_result_code", "adpa_refusal_reason",
            "adpa_amount", "adpa_pacu_code", "adpa_status", "adpa_changes", "adpa_merchant_reference",
            "adcade_id", "adcade_merchant_reference", "adcade_card_expiry_month", "adcade_card_expiry_year",
            "adcade_card_last_digits", "adcade_enabled", "adcade_removed",
            "cu_id", "cu_company_name_business")
            ->leftJoin("adyen_card_details","adpa_adcade_id","adcade_id")
            ->leftJoin("customers","adcade_cu_id","cu_id")
            ->whereBetween("adpa_timestamp", System::betweenDates($request->date));
        
        if(!empty($request->status) && is_array($request->status))
        {
            $query->whereIn("adpa_status", $request->status);
        }
        
        $payments = $query->get();
        
		foreach($payments as $id => $row)
        {
            $changes = $system->unserialize($row->adpa_changes);
            $batch_number = $request->batch_number ?? null;
            if (!empty($changes))
            {
                if ($batch_number !== null)
                {
                    $found = false;
                    foreach ($changes as $change)
                    {
                        if (strpos($change, 'by batch #' . $batch_number) !== false)
                        {
                            $found = true;
                            break;
                        }
                    }
                    
                    if (!$found)
                    {
                        unset($payments[$id]);
                        continue;
                    }
                }
    
                $html = '';
                $html .= "<table class=\"table display\">";
                $html .= "<thead>";
                $html .= "<tr>";
                $html .= "<th>Timestamp</th>";
                $html .= "<th>Subject</th>";
                $html .= "<th>Type</th>";
                $html .= "<th>Additional data</th>";
                $html .= "</tr>";
                $html .= "</thead>";
                $html .= "<tbody>";
                
                foreach ($changes as $change)
                {
    
                    $matches = [];
                    //Match it.
                    // Format:  [{$timestamp}][{$subject}][{$type}] (Optional extra data)
                    // Example: [2016-05-05 22:12:12][Chargeback][Confirmed] by batch #25
                    preg_match('/\[((\w|\-|\s|\:)+)\]\[(\w+)\]\[(\w+)\]\s?(.*)/', $change, $matches);
    
                    if (empty($matches))
                    {
                        $html .=  '<tr>';
                        $html .=  '<td>Unable to parse line</td>';
                        $html .=  '<td></td>';
                        $html .=  '<td>Full line:</td>';
                        $html .=  '<td>$change</td>';
                        $html .=  '</tr>';
                    } else
                    {
                        $html .=  '<tr>';
                        $html .=  '<td>'.$matches[1].'</td>';
                        $html .=  '<td>'.$matches[3].'</td>';
                        $html .=  '<td>'.$matches[4].'</td>';
                        $html .=  '<td>'.$matches[5].'</td>';
                        $html .=  '</tr>';
                    }
                }
    
                $html .= '</tbody>';
                $html .= '</table>';
    
                $payments[$id]->details = $html;
            }
            else
            {
                if ($batch_number !== null)
                {
                    unset($payments[$id]);
                    continue;
                }
    
                $payments[$id]->details = "";
            }
    
            if($row->adpa_status == AdyenPaymentStatus::CONFIRMED && $row->adpa_result_code == "Refused")
            {
                $payments[$id]->style = 'color: #FF0000;';
            }
            elseif(($row->adpa_status == AdyenPaymentStatus::WAITING_FOR_CONFIRMATION || $row->adpa_status == AdyenPaymentStatus::CONFIRMED) && $row->adpa_result_code == "Authorised")
            {
                $payments[$id]->style = 'color: #22A200;';
            }else{
                $payments[$id]->style = 'color: #000000;';
            }
        }
        
        $currency_tokens = [];
        foreach (PaymentCurrency::all() as $currency)
        {
            $currency_tokens[$currency->pacu_code] = $currency->pacu_token;
        }
        
        return view('finance.creditcardpaymenthistory', [
			'selected_date' => $request->date,
			'selected_statuses' => $request->status ?? [],
            'selected_batch_number' => $request->batch_number,
            'currencies' => $currency_tokens,
            'payments' => $payments,
            'adyenpaymentstatus' => AdyenPaymentStatus::all()
		]);
		
	}
	
}
