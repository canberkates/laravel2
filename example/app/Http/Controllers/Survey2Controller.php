<?php

namespace App\Http\Controllers;

use App\Data\Device;
use App\Data\EmptyYesNo;
use App\Data\RequestSource;
use App\Data\Spaces;
use App\Data\Survey2Source;
use App\Data\SurveyMoved;
use App\Data\SurveyStatus;
use App\Data\YesNo;
use App\Functions\System;
use App\Models\Country;
use App\Models\Customer;
use App\Models\Language;
use App\Models\ProConCategory;
use App\Models\Survey2;
use App\Models\WebsiteReview;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Survey2Controller extends Controller
{
	public function edit($customer_id, $survey_id)
	{
		$system = new System();
		$ratingoptions = $system->dropdownOptions(1, 5, 1, false);

		//Get customer and through the relation of the customer, get the correct contact person
		$surveys2 = Survey2::with(['customer.moverdata', 'request.requestcustomerportal.portal', 'request.websiteform.website', 'request.affiliateform', 'request.countryfrom', 'request.countryto', 'website_review.websiteform.website', 'website_review.portal.country'])->where("su_submitted", 1)->findOrFail($survey_id);

		$notices = [];
        $reviews_from_cookies = "";
        $user_agents_last_10_reviews = [];

		if ($surveys2->su_mover)
		{

			if ($surveys2->customer->moverdata->moda_sirelo_review_verification)
			{
				$notices['top']['verification'] = "<strong>ATTENTION:</strong> Each review for this company has to be verified before publishing – please send the verification e-mail!";
			}

			$one_day_reviews = DB::table("surveys_2")
				->selectRaw("COUNT(*) as count")
				->where("su_mover", $surveys2->su_mover)
				->whereRaw("su_submitted_timestamp BETWEEN NOW() - INTERVAL 24 HOUR AND NOW()")
				->first();

			$seven_day_reviews = DB::table("surveys_2")
				->selectRaw("COUNT(*) as count")
				->where("su_mover", $surveys2->su_mover)
				->whereRaw("su_submitted_timestamp BETWEEN NOW() - INTERVAL 7 DAY AND NOW()")
				->first();

			if ($one_day_reviews->count > 2)
			{
				$notices['top']['last_24_hours'] = "<strong>ATTENTION:</strong> This company received more than 2 reviews in the last 24 hours.";
			}

			if ($seven_day_reviews->count > 5)
			{
				$notices['top']['last_7_days'] = "<strong>ATTENTION:</strong> This company received more than 5 reviews in the last 7 days.";
			}
		}
		//Get survey status
		if ($surveys2->su_submitted)
		{
			$survey_status = SurveyStatus::SUBMITTED;
		} elseif ($surveys2->su_opened)
		{
			$survey_status = SurveyStatus::OPENED;
		} else
		{
			$survey_status = SurveyStatus::SENT;
		}

		$proof_files = null;
		if ($surveys2->su_source == Survey2Source::WEBSITE_REVIEW)
		{

			$double_website_reviews = DB::table("website_reviews")
				->select("were_id", "were_wefo_id", "were_email", "were_ip_address", "su_id", "su_timestamp", "su_show_on_website", "su_checked", "cu_id", "cu_company_name_business")
				->leftJoin("surveys_2", "were_id", "su_were_id")
				->leftJoin("customers", "su_mover", "cu_id")
				->where("were_id", "!=", $surveys2->su_were_id)
                ->where("su_submitted",  1)
				->whereRaw("
				(
					(
						`website_reviews`.`were_email` = '" . $surveys2->website_review->were_email . "' AND
						`website_reviews`.`were_email` != ''
					)
					OR
					(
						`website_reviews`.`were_ip_address` = '" . $surveys2->website_review->were_ip_address . "' AND
						`website_reviews`.`were_ip_address` != ''
					)
				)")
				->orderBy("su_submitted_timestamp", 'desc')
				->limit(30)
				->get();

			$double_request_reviews = DB::table("requests")
				->select("re_id", "re_email", "su_id", "su_re_id", "su_timestamp", "su_show_on_website", "su_checked", "cu_id", "cu_company_name_business")
				->rightJoin("surveys_2", "su_re_id", "re_id")
				->leftJoin("customers", "su_mover", "cu_id")
				->whereNotNull("su_re_id")
				->where("su_submitted",  1)
				->whereRaw("
					(
						`requests`.`re_email` = '" . $surveys2->website_review->were_email . "' AND
						`requests`.`re_email` != ''
					)
				")
				->get();


			if (count($double_website_reviews) > 0 || count($double_request_reviews) > 0)
			{
				$notices['double'][] = "<strong>ATTENTION:</strong> This review has been submitted multiple times with the following data";


				foreach ($double_website_reviews as $row_double)
				{
					$email_double = strtolower(trim($row_double->were_email)) == strtolower(trim($surveys2->website_review->were_email));
					$ip_double = $row_double->were_ip_address == $surveys2->website_review->were_ip_address;

					if ($row_double->su_checked == 0)
					{
						$status = "Open";
					} elseif ($row_double->su_show_on_website == 1)
					{
						$status = "Published";
					} else
					{
						$status = "Not published";
					}

					$notices['double'][] = "<strong>[" . $status . "]</strong> Website review <strong>" . $row_double->were_id . "</strong> via website <strong>" . $system->getWebsiteForm($row_double->were_wefo_id) . "</strong> on <strong>" . $row_double->su_timestamp . "</strong> for customer <strong>" . ((!empty($row_double->cu_id)) ? $row_double->cu_company_name_business : "Unknown") . "</strong>. <strong>[<span" . (($email_double) ? " style=\"color: #FF0000;\"" : "") . ">" . $row_double->were_email . "</span> / <span" . (($ip_double) ? " style=\"color: #FF0000;\"" : "") . ">" . $row_double->were_ip_address . "</span>] <a target='_blank' href=". url( 'customers/0/survey2/'.$row_double->su_id .'/edit' ) . "  title=\"View review\"><i class='fa fa-eye'></i></a> </strong>";
				}


				foreach ($double_request_reviews as $row_double_req)
				{
					$email_double = strtolower(trim($row_double_req->re_email)) == strtolower(trim($surveys2->request->re_email));

					if ($row_double_req->su_checked == 0)
					{
						$status = "Open";
					} elseif ($row_double_req->su_show_on_website == 1)
					{
						$status = "Published";
					} else
					{
						$status = "Not published";
					}

					$notices['double'][] = "<strong>[" . $status . "]</strong> Request review <strong>" . $row_double_req->re_id . "</strong> via request on <strong>" . $row_double_req->su_timestamp . "</strong> for customer <strong>" . ((!empty($row_double_req->cu_id)) ? $row_double_req->cu_company_name_business : "Unknown") . "</strong>. <strong>[<span" . (($email_double) ? " style=\"color: #FF0000;\"" : "") . ">" . $row_double_req->re_email . "</span>] <a target='_blank' href=". url( 'customers/0/survey2/'.$row_double_req->su_id.'/edit' ) . "  title=\"View review\"><i class='fa fa-eye'></i></a> </strong>";
				}

			}

			if ($surveys2->website_review->were_reviewsubmitted_cookie && !empty($surveys2->website_review->were_cookie_previous_timestamp) && $surveys2->website_review->were_cookie_previous_timestamp != "null") {

                $reviews_from_cookies .= "<p style='color:red; font-size:16px;'><b>";

                $reviews_from_cookies .= "The cookies tells us that the sender of this review has submitted review(s) before: ";


                $were_prev_timestamp = WebsiteReview::where('were_id', $surveys2->website_review->were_id)->first()->were_cookie_previous_timestamp;

                $counter = 0;
                //while
                while ($were_prev_timestamp) {
                    $result = Survey2::leftJoin("website_reviews", "were_id", 'su_were_id')->where("were_cookie_timestamp", $were_prev_timestamp)->first();

                    if($result) {
                        $counter++;
                        if ($counter > 1)
                        {
                            $reviews_from_cookies .= ", ";
                        }

                        $reviews_from_cookies .= "<a target='_blank' href='/customers/".$result->su_mover."/survey2/".$result->su_id."/edit'>Review #".$counter."</a>";

                        $were_prev_timestamp = $result->were_cookie_previous_timestamp;
                    }else{
                        $were_prev_timestamp = false;
                    }

                }

                $reviews_from_cookies .= "</b></p>";
            }

			//Part to get the last 10 reviews of the customer, and loop through the user agents
            $last_10_reviews = Survey2::leftJoin("website_reviews", "were_id", "su_were_id")
                ->where("su_mover", $surveys2->su_mover)
                ->where("su_id", "!=", $survey_id)
                ->where("su_source", 2)
                ->orderBy("su_timestamp", "DESC")
                ->limit(10)
                ->get();

			foreach($last_10_reviews as $review){
			    if (!empty($review->were_user_agent)){
                    $user_agents_last_10_reviews[] = $review->were_user_agent." - ".Device::name($review->were_device);
                }
            }


            $fileNumber = 1;
			foreach (json_decode($surveys2->website_review->were_proof_files) as $fileName) {
			    $file_info = self::showSpacesFile($fileName, $fileNumber);

			    $proof_files[$file_info['name']] = $file_info['url'];

			    $fileNumber++;
            }
		}

		if ($surveys2->su_source == Survey2Source::REQUEST)
		{

			$query_double_requests = DB::table("requests")
				->select("re_id", "re_email", "re_ip_address", "su_id", "su_timestamp", "su_show_on_website", "su_checked", "cu_id", "cu_company_name_business")
				->rightJoin("surveys_2", "su_re_id", "re_id")
				->leftJoin("customers", "su_mover", "cu_id")
                ->where("su_submitted",  1)
				->where("re_id", "!=", $surveys2->su_re_id)
				->whereRaw("
					(
						`requests`.`re_email` = '" . $surveys2->request->re_email . "' AND
						`requests`.`re_email` != ''
					)
				")
				->orderBy("su_submitted_timestamp", 'desc')
				->limit(30)
				->get();

			$query_double_reviews = DB::table("website_reviews")
				->select("were_id", "were_wefo_id", "were_email", "su_id", "su_timestamp", "su_show_on_website", "su_checked", "cu_id", "cu_company_name_business")
				->leftJoin("surveys_2", "were_id", "su_were_id")
				->leftJoin("customers", "su_mover", "cu_id")
                ->where("su_submitted",  1)
				->whereRaw("
					(
						`website_reviews`.`were_email` = '" . $surveys2->request->re_email . "' AND
						`website_reviews`.`were_email` != ''
					)
				")
				->orderBy("su_submitted_timestamp", 'desc')
				->limit(30)
				->get();

			if (count($query_double_requests) > 0 || count($query_double_reviews) > 0)
			{
				$notices['double'][] = "<strong>ATTENTION:</strong> This review has been submitted multiple times with the following data:";

				if (count($query_double_requests) > 0)
				{
					foreach ($query_double_requests as $row_double_req)
					{
						$email_double = strtolower(trim($row_double_req->re_email)) == strtolower(trim($surveys2->request->re_email));

						if ($row_double_req->su_checked == 0)
						{
							$status = "Open";
						} elseif ($row_double_req->su_show_on_website == 1)
						{
							$status = "Published";
						} else
						{
							$status = "Not published";
						}

						$notices['double'][] = "<strong>[" . $status . "]</strong> Request review <strong>" . $row_double_req->re_id . "</strong> via request on <strong>" . $row_double_req->su_timestamp . "</strong> for customer <strong>" . ((!empty($row_double_req->cu_id)) ? $row_double_req->cu_company_name_business : "Unknown") . "</strong>. <strong>[<span" . (($email_double) ? " style=\"color: #FF0000;\"" : "") . ">" . $row_double_req->re_email . "</span>] <a target='_blank' href=". url( 'customers/0/survey2/'.$row_double_req->su_id .'/edit' ) . "  title=\"View review\"><i class='fa fa-eye'></i></a> </strong>";
					}
				}

				if (count($query_double_reviews) > 0)
				{
					foreach ($query_double_reviews as $row_double)
					{
						$email_double = strtolower(trim($row_double->were_email)) == strtolower(trim($surveys2->website_review->were_email));

						if ($row_double->su_checked == 0)
						{
							$status = "Open";
						} elseif ($row_double->su_show_on_website == 1)
						{
							$status = "Published";
						} else
						{
							$status = "Not published";
						}

						$notices['double'][] .= "<strong>[" . $status . "]</strong> Website review <strong>" . $row_double->were_id . "</strong> via website <strong>" . $system->getWebsiteForm($row_double->were_wefo_id) . "</strong> on <strong>" . $row_double->su_timestamp . "</strong> for customer <strong>" . ((!empty($row_double->cu_id)) ? $row_double->cu_company_name_business : "Unknown") . "</strong>. <strong>[<span" . (($email_double) ? " style=\"color: #FF0000;\"" : "") . ">" . $row_double->were_email . "</span>]  <a target='_blank' href=". url( 'customers/0/survey2/'.$row_double->su_id .'/edit' ) . "  title=\"View review\"><i class='fa fa-eye'></i></a> </strong>";
					}
				}
			}
		}

		$show = $surveys2->su_show_on_website;

		if (!empty($surveys2->request))
		{
			$requestlanguage = Language::where("la_code", "=", $surveys2->request->re_la_code)->first()->la_language;
			$requestsource = RequestSource::name($surveys2->request->re_source);
			$device = Device::name($surveys2->request->re_device);
		} else
		{
			$requestlanguage = '';
			$requestsource = '';
			$device = Device::name($surveys2->website_review->were_device);
		}

		if (empty($surveys2->su_mover))
		{
			$customers = Customer::with("country")->whereRaw("(cu_type = 1 OR cu_type = 6)")->where("cu_deleted", "=", "0")->get();
		} else
		{
			$customers = [];
		}

		if ($surveys2->su_source == Survey2Source::REQUEST)
		{
			if (!empty($surveys2->request->re_ip_address_country) && ($surveys2->request->re_ip_address_country != $surveys2->request->re_co_code_from && $surveys2->request->re_ip_address_country != $surveys2->request->re_co_code_to))
			{
				$notices['ip_address'] = "<strong>ATTENTION:</strong> The IP address is not located in the origin or destination country";
			}

			if (!$system->validatePhone($surveys2->request->re_telephone1, $surveys2->request->re_co_code_from, $surveys2->request->re_co_code_to))
			{
				$notices['phone_1'] = "INVALID";
			}
			if (!$system->validatePhone($surveys2->request->re_telephone2, $surveys2->request->re_co_code_from, $surveys2->request->re_co_code_to))
			{
				$notices['phone_2'] = "INVALID";
			}

			$bounced_mail = $system->getPostmarkBounce($surveys2->request->re_email, $surveys2->su_submitted_timestamp);

			if ($bounced_mail)
			{
				$notices['email_bounce'] = "<strong>ATTENTION:</strong> Mail from <strong>" . $bounced_mail['From'] . "</strong> to <strong>" . $bounced_mail['Email'] . "</strong> bounced on <strong>" . date("Y-m-d H:i:s", strtotime($bounced_mail['BouncedAt'])) . "</strong> with subject <strong>" . $bounced_mail['Subject'] . "</strong>.";
			}

		} else
		{
			if (!empty($surveys2->website_review->were_ip_address_country) && ($surveys2->website_review->were_ip_address_country != $surveys2->website_review->were_co_code_from && $surveys2->website_review->were_ip_address_country != $surveys2->website_review->were_co_code_to))
			{
				$notices['ip_address'] = "<strong>ATTENTION:</strong> The IP address is not located in the origin or destination country";
			}

			$bounced_mail = $system->getPostmarkBounce($surveys2->website_review->were_email, $surveys2->su_submitted_timestamp);

			if ($bounced_mail)
			{
				$notices['email_bounce'] = "<strong>ATTENTION:</strong> Mail from <strong>" . $bounced_mail[0]['From'] . "</strong> to <strong>" . $bounced_mail[0]['Email'] . "</strong> bounced on <strong>" . date("Y-m-d H:i:s", strtotime($bounced_mail[0]['BouncedAt'])) . "</strong> with subject <strong>" . $bounced_mail[0]['Subject'] . "</strong>.";
			}
		}

		Log::debug($surveys2);

		foreach(Country::all() as $country){
			$countries[$country->co_code] = $country->co_en;
		}

        if ($surveys2->su_source == Survey2Source::REQUEST)
        {
            $times_ip_used_last_year = count(Survey2::leftJoin("requests", "su_re_id", "re_id")->where("re_ip_address", $surveys2->request->re_ip_address)->get());
        }
        else {
            $times_ip_used_last_year = count(Survey2::leftJoin("website_reviews", "were_id", "su_were_id")->where("were_ip_address", $surveys2->website_review->were_ip_address)->get());
        }

        $first_review = false;

        if (!empty($surveys2->customer->cu_company_name_business))
        {
            $survey_searcher = Survey2::where("su_mover", "=", $surveys2->su_mover)->first();
            if (count($survey_searcher) == 0)
            {
                $first_review = true;
            }
        }

        $first_review = false;
        $submitted_reviews_before = false;

        if ($surveys2->su_reviewsubmitted_cookie == 1)
        {
            $submitted_reviews_before = true;
        }

        $received_last_30_days = count(Survey2::where("su_mover", $surveys2->su_mover)->whereRaw("su_submitted_timestamp BETWEEN NOW() - INTERVAL 30 DAY AND NOW()")->get());
        $received_last_112_days = count(Survey2::where("su_mover", $surveys2->su_mover)->whereRaw("su_submitted_timestamp BETWEEN NOW() - INTERVAL 112 DAY AND NOW()")->get());

        $avg_30_days_per_day = round($received_last_30_days / 30, 2);
        $avg_112_days_per_day = round($received_last_112_days / 112, 2);

		return view('survey2.edit',
			[
				'survey' => $surveys2,
				'surveysource' => Survey2Source::name($surveys2->su_source),
				'surveystatus' => SurveyStatus::name($survey_status),
				'moved' => SurveyMoved::name($surveys2->su_moved),
				'experienceblog' => YesNo::name($surveys2->su_experience_blog),
				'recommend' => YesNo::all(),
				'ratingoptions' => $ratingoptions,
				'procategories' => ProConCategory::pro()->get(),
				'concategories' => ProConCategory::con()->get(),
				'showonwebsite' => $show,
				'countries' => Country::all(),
				'requestsource' => $requestsource,
				'requestlanguage' => $requestlanguage,
				'device' => $device,
				'customers' => $customers,
				'notices' => $notices,
				'ip_countries' => $countries,
                'bounced_mail' => $bounced_mail,
                'proof_files' => $proof_files,

                //Statistics etc
                'times_ip_used_last_year' => $times_ip_used_last_year,
                'first_review' => $first_review,
                'avg_30_days_per_day' => $avg_30_days_per_day,
                'avg_112_days_per_day' => $avg_112_days_per_day,
                'received_last_30_days' => $received_last_30_days,
                'received_last_112_days' => $received_last_112_days,
                'submitted_reviews_before' => $submitted_reviews_before,
                'reviews_from_cookies' => $reviews_from_cookies,
                'user_agents_last_10_reviews' => $user_agents_last_10_reviews

			]);
	}

	public function showSpacesFile($fileName, $fileNumber) {

        $my_space = Spaces(env("SPACES_KEY"), env("SPACES_SECRET"))->space("tgb-secure", env("SPACES_REGION"));

        $path = "proof/".$fileName;

        $spaces_url = $my_space->signedURL($path, "15 minutes");

        $ext = pathinfo($spaces_url, PATHINFO_EXTENSION);
        $ext_splitter = explode("?", $ext);

        return [
            'name' => 'File '.$fileNumber." (".strtoupper($ext_splitter[0]).")",
            'url' => $spaces_url
        ];
    }

	public function update(Request $request, $survey2_id)
	{
		$survey_2 = Survey2::findOrFail($survey2_id);
        if (Arr::exists($request, 'save_mover_comment')) {
            if ($request->mover_comment)
            {
                $survey_2->su_mover_comment = $request->mover_comment;
            }
            elseif (empty($request->mover_comment) && !empty($survey_2->su_mover_comment))
            {
                $survey_2->su_mover_comment_timestamp = null;
                $survey_2->su_mover_comment = null;
            }

            $survey_2->save();

            DB::table("kt_mover_comments")
                ->where("ktmoco_su_id", $survey_2->su_id)
                ->update(
                    [
                        "ktmoco_read" => 1
                    ]
                );

            return redirect('surveys')->with('message', 'Mover comment successfully changed!');
        }

		//Save WebsiteReview-based survey
		if ($survey_2->su_source == Survey2Source::WEBSITE_REVIEW)
		{
			$website_review = WebsiteReview::query()->where('were_id', "=", $survey_2->su_were_id)->first();

			//Save Move
			$website_review->were_co_code_from = $request->country_from;
			$website_review->were_city_from = $request->city_from;
			$website_review->were_co_code_to = $request->country_to;
			$website_review->were_city_to = $request->city_to;

			//Save contact details
			$website_review->were_name = $request->full_name;
			$website_review->were_email = $request->email;
			$website_review->were_verified = ($request->contact_verified === 'on');

			/*if (Arr::exists($request, 'publish') && $request->contact_verified === 'on')
			{
				$survey_2->su_submitted_timestamp = date("Y-m-d H:i:s");
			}*/

			$website_review->save();
		}

		if ($survey_2->su_moved == YesNo::YES)
		{
			if (empty($survey_2->su_mover))
			{
			    if(!empty($request->mover) && $request->mover != -1)
                {
                    $survey_2->su_mover = $request->mover;
                }

			    if(!empty($request->other_mover) && $request->other_mover != -1){
                    $survey_2->su_mover = $request->other_mover;
                }
			}

			//Save rating and recommendation
			$survey_2->su_rating = $request->rating;
			$survey_2->su_rating_motivation = $request->rating_motivation;
			$survey_2->su_recommend_mover = $request->recommend_mover;
		}

		//Save pros
		$survey_2->su_pro_1 = $request->pro_1;
		$survey_2->su_pro_1_category = $request->pro_1_category;
		$survey_2->su_pro_2 = $request->pro_2;
		$survey_2->su_pro_2_category = $request->pro_2_category;

		//Save cons
		$survey_2->su_con_1 = $request->con_1;
		$survey_2->su_con_1_category = $request->con_1_category;
		$survey_2->su_con_2 = $request->con_2;
		$survey_2->su_con_2_category = $request->con_2_category;

		//Save remark and e-mail
		$survey_2->su_checked_remarks = $request->remark;
		$survey_2->su_send_email = $request->send_email;
		$survey_2->su_checked = 1;

		if (Arr::exists($request, 'publish'))
		{
			/*if($survey_2->su_on_hold == YesNo::YES)
			{
				$survey_2->su_submitted_timestamp = date("Y-m-d H:i:s");
			}*/

            if ($survey_2->su_show_on_website != EmptyYesNo::YES && $survey_2->su_accepted_timestamp == "0000-00-00 00:00:00") {
                $survey_2->su_accepted_timestamp = date("Y-m-d H:i:s");
            }

			if ($survey_2->su_mover || $request->mover > 0)
			{
				$survey_2->su_show_on_website = EmptyYesNo::YES;
				$survey_2->su_send_email = $request->send_email;
			}

            if ($request->mover_comment)
            {
                $survey_2->su_mover_comment = $request->mover_comment;
            }
            elseif (empty($request->mover_comment) && !empty($survey_2->su_mover_comment))
            {
                $survey_2->su_mover_comment_timestamp = null;
                $survey_2->su_mover_comment = null;
            }

			$survey_2->su_on_hold = YesNo::NO;
			$survey_2->su_on_hold_by = null;
			$survey_2->su_checked = YesNo::YES;

			$survey_2->save();

			return redirect('surveys')->with('message', 'Survey successfully published!');
		}
		elseif (Arr::exists($request, 'reject'))
		{
			$survey_2->su_show_on_website = EmptyYesNo::NO;
			$survey_2->su_on_hold = YesNo::NO;
			$survey_2->su_on_hold_by = null;
			$survey_2->su_checked = YesNo::YES;

			$survey_2->save();

			return redirect('surveys')->with('message', 'Survey successfully rejected!');
		}
		elseif (Arr::exists($request, 'place_on_hold'))
		{
			$survey_2->su_show_on_website = EmptyYesNo::EMPTY;
			$survey_2->su_on_hold = YesNo::YES;
			$survey_2->su_on_hold_by = Auth::id();
			$survey_2->su_checked = YesNo::NO;

			$survey_2->save();

			return redirect('surveys')->with('message', 'Survey successfully put on hold!');
		}
	}

}
