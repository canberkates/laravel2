<?php

namespace App\Http\Controllers;

use App\Data\Device;
use App\Data\RequestSource;
use App\Data\Survey2Source;
use App\Data\SurveyMoved;
use App\Data\SurveyStatus;
use App\Data\YesNo;
use App\Functions\System;
use App\Models\Country;
use App\Models\Customer;
use App\Models\Language;
use App\Models\ProConCategory;
use App\Models\Survey1;
use App\Models\Survey2;
use App\Models\WebsiteReview;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class Survey1Controller extends Controller
{
	public function edit($customer_id, $survey_id)
	{
		//Get customer and through the relation of the customer, get the correct contact person
		$surveys_1 = Survey1::with("survey1customers", "request")->where("su_id", "=", $survey_id)->get();
		
		Log::debug($surveys_1);
		
		//Get survey status
		if ($surveys_1->su_submitted)
		{
			$survey_status = 3;
		} elseif ($surveys_1->su_opened)
		{
			$survey_status = 2;
		} else
		{
			$survey_status = 1;
		}
		
		$system = new System();
		$ratingoptions = $system->dropdownOptions(1, 5, 1, false);
		
		$show = $surveys_1->su_show_on_website == 2 ? 0 : $surveys_1->su_show_on_website;
		
		if (!empty($surveys_1->request))
		{
			$requestlanguage = Language::where("la_code", "=", $surveys_1->request->re_la_code)->first()->la_language;
			$requestsource = RequestSource::name($surveys_1->request->re_source);
			$device = Device::name($surveys_1->request->re_device);
			
		} else
		{
			$requestlanguage = '';
			$requestsource = '';
			$device = Device::name($surveys_1->website_review->were_device);
		}
		
		
		if (empty($surveys_1->su_mover))
		{
			$customers = Customer::select('cu_company_name_business', 'cu_id')->where("cu_type", "=", "1")->where("cu_deleted", "=", "0")->get();
		} else
		{
			$customers = [];
		}
		
		return view('survey2.edit',
			[
				'survey' => $surveys_1,
				'surveysource' => Survey2Source::name($surveys_1->su_source),
				'surveystatus' => SurveyStatus::name($survey_status),
				'moved' => SurveyMoved::name($surveys_1->su_moved),
				'recommend' => YesNo::all(),
				'ratingoptions' => $ratingoptions,
				'proconcategories' => ProConCategory::all(),
				'showonwebsite' => YesNo::name($show),
				'countries' => Country::all(),
				'requestsource' => $requestsource,
				'requestlanguage' => $requestlanguage,
				'device' => $device,
				'customers' => $customers
			]);
	}
	
	public function survey_edit($survey_id)
	{
		//Get customer and through the relation of the customer, get the correct contact person
		$surveys_1 = Survey1::with("survey1customers.customer", "request")->where("su_id", "=", $survey_id)->first();
		
		Log::debug($surveys_1);
		
		//Get survey status
		if ($surveys_1->su_submitted)
		{
			$survey_status = 3;
		} elseif ($surveys_1->su_opened)
		{
			$survey_status = 2;
		} else
		{
			$survey_status = 1;
		}
		
		$system = new System();
		$ratingoptions = $system->dropdownOptions(1, 5, 1, false);
		
		$show = $surveys_1->su_show_on_website == 2 ? 0 : $surveys_1->su_show_on_website;
		
		if (!empty($surveys_1->request))
		{
			$requestlanguage = Language::where("la_code", "=", $surveys_1->request->re_la_code)->first()->la_language;
			$requestsource = RequestSource::name($surveys_1->request->re_source);
			$device = Device::name($surveys_1->request->re_device);
			
		} else
		{
			$requestlanguage = '';
			$requestsource = '';
			$device = Device::name($surveys_1->website_review->were_device);
		}
		
		
		if (empty($surveys_1->su_mover))
		{
			$customers = Customer::select('cu_company_name_business', 'cu_id')->where("cu_type", "=", "1")->where("cu_deleted", "=", "0")->get();
		} else
		{
			$customers = [];
		}
		
		return view('survey1.edit',
			[
				'survey' => $surveys_1,
				'surveysource' => Survey2Source::name($surveys_1->su_source),
				'surveystatus' => SurveyStatus::name($survey_status),
				'moved' => SurveyMoved::name($surveys_1->su_moved),
				'recommend' => YesNo::all(),
				'ratingoptions' => $ratingoptions,
				'proconcategories' => ProConCategory::all(),
				'showonwebsite' => YesNo::name($show),
				'countries' => Country::all(),
				'requestsource' => $requestsource,
				'requestlanguage' => $requestlanguage,
				'device' => $device,
				'customers' => $customers
			]);
	}
	
	public function update(Request $request, $survey2_id)
	{
		$survey_2 = Survey2::find($survey2_id);
		
		//Save WebsiteReview-based survey
		$survey_2->su_rating = $request->rating;
		$survey_2->su_rating_motivation = $request->rating_motivation;
		$survey_2->su_recommend_mover = $request->recommend_mover;
		
		//Save pros
		$survey_2->su_pro_1 = $request->pro_1;
		$survey_2->su_pro_1_category = $request->pro_1_category;
		$survey_2->su_pro_2 = $request->pro_2;
		$survey_2->su_pro_2_category = $request->pro_2_category;
		
		//Save cons
		$survey_2->su_con_1 = $request->con_1;
		$survey_2->su_con_1_category = $request->con_1_category;
		$survey_2->su_con_2 = $request->con_2;
		$survey_2->su_con_2_category = $request->con_2_category;
		
		//Save remark and e-mail
		$survey_2->su_checked_remarks = $request->remark;
		$survey_2->su_send_email = $request->send_email;
		$survey_2->su_checked = 1;
		
		if($survey_2->su_were_id)
		{
			$website_review =  WebsiteReview::query()->where('were_id', "=", $survey_2->su_were_id)->first();
		
			//Save Move
			$website_review->were_co_code_from = $request->country_from;
			$website_review->were_city_from = $request->city_from;
			$website_review->were_co_code_to = $request->country_to;
			$website_review->were_city_to = $request->city_to;
			
			//Save contact details
			$website_review->were_name = $request->full_name;
			$website_review->were_email = $request->email;
			$website_review->were_verified = ($request->contact_verified === 'on');
			
			$website_review->save();
		}
		
		$survey_2->save();
		
		return redirect('customers/' . $survey_2->su_mover . '/edit');
	}
	
}
