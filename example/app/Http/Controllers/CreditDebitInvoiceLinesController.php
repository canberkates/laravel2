<?php

namespace App\Http\Controllers;

use App\Data\CreditDebitType;
use App\Functions\System;
use App\Models\CreditDebitInvoiceLine;
use App\Models\Customer;
use App\Models\LedgerAccount;
use App\Models\PaymentCurrency;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Log;

class CreditDebitInvoiceLinesController extends Controller
{
	public function index()
	{

		return view('finance.creditdebitinvoicelines', [
			'selected_date' => "",
			'customers' => Customer::select("cu_id", "cu_company_name_legal")->whereIn("cu_type", [1,2,4,6])->where("cu_deleted", "=", "0")->get()
		]);
	}

	public function filteredIndex(Request $request)
	{
		$request->validate([
			'date' => 'required'
		]);

		$credit_debit_lines = DB::table("credit_debit_invoice_lines")
			->leftJoin("customers","crdeinli_cu_id", "cu_id")
			->leftJoin("ledger_accounts","crdeinli_leac_number", "leac_number")
			->where("cu_deleted", 0)
			->whereBetween("crdeinli_timestamp", System::betweenDates($request->date));

		if(!empty($request->mover)){
			$credit_debit_lines->where("crdeinli_cu_id", $request->mover);
		}

		$credit_debit_lines = $credit_debit_lines
			->orderBy("crdeinli_timestamp", 'asc')
			->get();

		$currency_tokens = [];

		$currencies = PaymentCurrency::all();

		foreach ($currencies as $currency)
		{
			$currency_tokens[$currency->pacu_code] = $currency->pacu_token;
		}


		return view('finance.creditdebitinvoicelines', [
			'selected_date' => $request->date,
			'credit_debit_lines' => $credit_debit_lines,
			'currency_tokens' => $currency_tokens,
			'credit_debit_types' => CreditDebitType::all(),
			'customers' => Customer::select("cu_id", "cu_company_name_legal")->whereIn("cu_type", [1,2,4,6])->where("cu_deleted", "=", "0")->get()
		]);

	}

    public function addIndex()
    {
        $currency_tokens = [];

        foreach (PaymentCurrency::all() as $currency)
        {
            $currency_tokens[$currency->pacu_code] =  $currency->pacu_name." (".$currency->pacu_token.")";
        }

        foreach(LedgerAccount::where("leac_type", 1)->get() as $ledger){
            $ledgeraccounts[$ledger->leac_number] = $ledger->leac_number . " (".$ledger->leac_name.")";
        }


        return view('finance.addcreditdebitinvoiceline', [
            'credit_debit_types' => CreditDebitType::all(),
            'customers' => Customer::select("cu_id", "cu_company_name_business")->whereIn("cu_type", [1,2,4,6])->where("cu_deleted", "=", "0")->get(),
            'ledgeraccounts' => $ledgeraccounts,
            'currencies' => $currency_tokens
        ]);

    }

    public function addLine(Request $request)
    {
        $request->validate([
            'customer' => 'required',
            'ledger' => 'required',
            'type' => 'required',
            'currency' => 'required',
            'amount' => 'required'
        ]);

        $amount = str_replace(",", ".", $request->amount);

        if($amount < 0)
        {
            $amount = $amount * -1;
        }

        $amount = (($request->type == CreditDebitType::CREDIT) ? -$amount : $amount);


        $line = new CreditDebitInvoiceLine();

        $line->crdeinli_timestamp = Carbon::now()->toDateTimeString();
        $line->crdeinli_cu_id = $request->customer;
        $line->crdeinli_leac_number = $request->ledger;
        $line->crdeinli_description = $request->description;
        $line->crdeinli_type = $request->type;
        $line->crdeinli_currency = $request->currency;
        $line->crdeinli_amount = $amount;

        $line->save();

        return view('finance.creditdebitinvoicelines', [
            'selected_date' => "",
            'customers' => Customer::select("cu_id", "cu_company_name_legal")->whereIn("cu_type", [1,2,4,6])->where("cu_deleted", "=", "0")->get()
        ])->withErrors([ 'errors' => "You have successfully added the invoice line!"]);
    }

    public function delete(Request $request)
    {
        Log::debug($request);
        $line = CreditDebitInvoiceLine::find($request->id);

        $line->delete();
    }

}
