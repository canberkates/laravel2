<?php

namespace App\Http\Controllers;

use App\Functions\System;
use App\Models\Customer;
use App\Models\LedgerAccount;
use DB;
use Illuminate\Http\Request;
use Log;

class JournalEntrySummaryController extends Controller
{
	public function index()
	{
        foreach(LedgerAccount::all() as $ledger){
            $ledgeraccounts[$ledger->leac_number] = $ledger->leac_number . " (".$ledger->leac_name.")";
        }
        
        return view('finance.journalentrysummary', [
            'selected_period' => "",
            'selected_customer' => "",
            'selected_ledger' => "",
            'customers' => Customer::all(),
            'ledgeraccounts' => $ledgeraccounts
        ]);
		
	}
	
	public function filteredIndex(Request $request)
	{
	    Log::debug($request);
        
        $request->validate([
            'date' => 'required'
        ]);
        
        $total = 0.00;
        
        $bank_line_query = DB::table("kt_bank_line_invoice_customer_ledger_account")
            ->select("ktbaliinculeac_id", "ktbaliinculeac_date", "bali_id", "bali_date", "in_id", "in_number", "cu_id", "cu_company_name_legal", "ktbaliinculeac_leac_number", "ktbaliinculeac_amount")
            ->leftJoin("invoices", "ktbaliinculeac_in_id", "in_id")
            ->leftJoin("customers", "ktbaliinculeac_cu_id", "cu_id")
            ->leftJoin("bank_lines", "bali_id", "ktbaliinculeac_bali_id")
            ->whereBetween("bali_date", System::betweenDates($request->date));
        
        if(!empty($request->customer))
        {
            $bank_line_query->where("ktbaliinculeac_cu_id",$request->customer);
        }
        if(!empty($request->ledger))
        {
            $bank_line_query->where("ktbaliinculeac_leac_number", $request->ledger);
        }
        
        $bank_line_query = $bank_line_query->get();
        
        $bank_lines = [];
        
        foreach($bank_line_query as $line){
            $bank_lines[$line->ktbaliinculeac_id] = $line;
            $total += $line->ktbaliinculeac_amount;
        }
        
        $journal_line_query = DB::table("kt_bank_line_invoice_customer_ledger_account")
            ->select("ktbaliinculeac_id", "ktbaliinculeac_date", "bali_id", "bali_date", "in_id", "in_number", "cu_id", "cu_company_name_legal", "ktbaliinculeac_leac_number", "ktbaliinculeac_amount")
            ->leftJoin("invoices", "ktbaliinculeac_in_id", "in_id")
            ->leftJoin("customers", "ktbaliinculeac_cu_id", "cu_id")
            ->leftJoin("bank_lines", "bali_id", "ktbaliinculeac_bali_id");
        
        if(!empty($request->date))
        {
            $journal_line_query->whereBetween("ktbaliinculeac_date", System::betweenDates($request->date));
            $journal_line_query->whereRaw("(`bali_date` BETWEEN '".date("Y-m-d", strtotime($request->date[0]))."' AND '".date("Y-m-d", strtotime($request->date[1]))."' OR bali_date IS NULL)");
            
        }
        
        if(!empty($request->customer))
        {
            $journal_line_query->where("ktbaliinculeac_cu_id", $request->customer);
        }
        if(!empty($request->ledger))
        {
            $journal_line_query->where("ktbaliinculeac_leac_number", $request->ledger);
        }
        
        $journal_line_query = $journal_line_query->get();
        
        $journal_lines = [];
        
        foreach($journal_line_query as $line){
            $journal_lines[$line->ktbaliinculeac_id] = $line;
            $total += $line->ktbaliinculeac_amount;
        }
        
        $combined = array_unique(array_merge($bank_lines,$journal_lines), SORT_REGULAR);
        
        foreach(LedgerAccount::all() as $ledger){
            $ledgeraccounts[$ledger->leac_number] = $ledger->leac_number . " (".$ledger->leac_name.")";
        }
        
        return view('finance.journalentrysummary',[
            'selected_period' => $request->date,
            'selected_customer' => $request->customer,
            'selected_ledger' => $request->ledger,
            'customers' => Customer::all(),
            'ledgeraccounts' => $ledgeraccounts,
            "total" => $total,
            "banklines"=> $combined
        ]);
		
	}
	
}
