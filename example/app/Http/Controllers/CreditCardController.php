<?php

namespace App\Http\Controllers;

use Adyen\AdyenException;
use App\Functions\Adyen;
use App\Functions\AdyenTransaction;
use App\Models\AdyenCardDetails;
use App\Models\Customer;
use App\Data\ContactPersonFunction;
use App\Models\Language;
use DateTime;
use DateTimeZone;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Log;

class CreditcardController extends Controller
{
    public function edit($customer_id, $contact_person_id)
    {
        //Get customer and through the relation of the customer, get the correct contact person
        $customer = Customer::findOrFail($customer_id);
        $contactperson = $customer->contactpersons()->findOrFail($contact_person_id);


        return view('creditcard.edit',
            [
                'contactperson' => $contactperson,
                'contactpersonfunctions' => ContactPersonFunction::all(),
                'customer' => $customer,
                'languages' => Language::where("la_iframe_only", 0)->get()
            ]);
    }

    public function show($customer_id, $contact_person_id)
    {

    }

    public function update(Request $request, $cope_id)
    {

    }

    public function updateCard(Request $request, $customer_id)
    {
        $message = null;
        if (Arr::exists($request, 'enable'))
        {
            AdyenTransaction::disableAllCards($customer_id);
            DB::table('adyen_card_details')
                ->where('adcade_id', $request->credit_card_id)
                ->update(
                    [
                        'adcade_enabled' => 1
                    ]
                );
            $message = "Enabled a single credit card, disabled the rest";

            Log::debug("Enabled a single credit card, disabled the rest");
            Log::debug($request);
        } else if (Arr::exists($request, 'update'))
        {

            $card_details = AdyenCardDetails::where("adcade_id", $request->credit_card_id)
                ->leftJoin("customers", "adcade_cu_id", "cu_id")
                ->first();

            $card_details->updateCardAlias = $card_details->adcade_card_alias;

            //Trigger a card detail update
            $updated = AdyenTransaction::process($card_details, $request);
            $message = $updated;
            Log::debug("Process: ".$updated);

        } else if (Arr::exists($request, 'remove'))
        {
            $remove = AdyenTransaction::disableRecurringContract($request->credit_card_id);
            $message = $remove;

        }

        $customer = Customer::findOrFail($customer_id);

        $cards = $customer->creditcards->where("adcade_removed", 0);

        foreach ($cards as $card)
        {
            $card->has_expired = AdyenTransaction::expiredCard($card->adcade_card_expiry_year, $card->adcade_card_expiry_month);
        }

        $timestamp = (new DateTime('now', new DateTimeZone('UTC')))->format("Y-m-d\TH:i:s\Z");

        return view('creditcard.create',
            [
                'customer' => $customer,
                'creditcards' => $cards,
                'timestamp' => $timestamp,
                'message' => $message
            ]);

    }

    public function create($customer_id)
    {
        $customer = Customer::findOrFail($customer_id);

        $cards = $customer->creditcards->where("adcade_removed", 0);

        foreach ($cards as $card)
        {
            $card->has_expired = AdyenTransaction::expiredCard($card->adcade_card_expiry_year, $card->adcade_card_expiry_month);
        }

        $timestamp = (new DateTime('now', new DateTimeZone('UTC')))->format("Y-m-d\TH:i:s\Z");

        Log::debug($cards);

        return view('creditcard.create',
            [
                'customer' => $customer,
                'creditcards' => $cards,
                'timestamp' => $timestamp,
            ]);
    }

    public function store(Request $request)
    {
        $new_card = $request['adyen-encrypted-data'];

        $row = DB::table("customers")
            ->selectRaw("COUNT(`adcade_id`) AS `adcade_cards`")
            ->select("cu_id", "cu_company_name_business", "cu_email", "cu_pacu_code")
            ->leftJoin("adyen_card_details", "cu_id", "adcade_cu_id")
            ->where("cu_id", $request->customer_id)
            ->groupBy("cu_id")
            ->first();

        Log::debug("Adding a card: ".json_encode($request));


        $row->cu_email = explode(",", $row->cu_email)[0];

        $oldCardList = AdyenTransaction::recurringDetails($row->cu_id);
        $oldCardAliases = [];

        Log::debug("Recurring details OLD: ".json_encode($oldCardList));


        if ($oldCardList instanceof AdyenException)
        {
            $adyenerror = explode(" in ", $oldCardList);

            return redirect()->back()->withErrors(['Adyen Exception' => $adyenerror[0]]);
        } else
        {
            foreach ($oldCardList['details'] as $i => $card)
            {
                $oldCardAliases[] = [$i => $card['RecurringDetail']['alias']];
            }
        }

        $result = AdyenTransaction::addCreditCard($row, $new_card);

        if ($result instanceof AdyenException)
        {
            $adyenerror = explode(" in ", $result);

            return redirect()->back()->withErrors(['Adyen Exception' => $adyenerror[0]]);
        }

        Log::debug("Adyen result: ".json_encode($result));

        //Check if authorised
        $result_code = $result['resultCode'];
        switch ($result_code)
        {
            case "Authorised":
                break;
            case "Refused":
                return redirect()->back()->withErrors(['Adyen Exception' => "Contract/Card denied: {$result['refusalReason']}"]);

            case "Error":
            case "Received":
            case "Cancelled":
            default:
                Log::debug("Unexpected Adyen Result");
                Log::debug($result);

                return redirect()->back()->withErrors(['Adyen Exception' => "Unknown resultCode {$result_code}, unable to create contract?"]);
        }

        //Give Adyen some time to process
        sleep(2);

        $newCardList = AdyenTransaction::recurringDetails($row->cu_id);
        $newCardAliases = [];

        Log::debug("Recurring details NEW: ".json_encode($newCardList));


        if ($newCardList instanceof AdyenException)
        {
            $adyenerror = explode(" in ", $newCardList);

            return redirect()->back()->withErrors(['Adyen Exception' => $adyenerror[0]]);
        } else
        {
            Log::debug("Has new cards");
            foreach ($newCardList['details'] as $i => $card)
            {
                $newCardAliases[] = [$i => $card['RecurringDetail']['alias']];
            }
        }

        if ($oldCardAliases)
        {
            Log::debug("Has old cards");

            $last_added_card_index = array_diff_key($newCardAliases, $oldCardAliases);
            if(empty($last_added_card_index)){
                Log::debug("Creditcardlists:");
                Log::debug($newCardAliases);
                Log::debug($oldCardAliases);
                Log::debug(array_diff_key($newCardAliases, $oldCardAliases));

                return redirect()->back()->withErrors(['Adyen Exception' => "Error in old cards and new cards!"]);
            }
            $reset = reset($last_added_card_index);
            $last_added_card = $newCardList['details'][array_key_first($reset)];
        } else
        {
            $last_added_card = $newCardList['details'][0];
        }

        Log::debug("New Card List:");
        Log::debug($newCardAliases);
        Log::debug("Old Card List:");
        Log::debug($oldCardAliases);
        Log::debug("Last added:");
        Log::debug($last_added_card);

        //Disable all active creditcards
        AdyenTransaction::disableAllCards($row->cu_id);
        AdyenTransaction::insertCreditCardInfo($last_added_card, $row);

        return redirect()->back()->with('message', 'Successfully added the credit card!');
    }
}
