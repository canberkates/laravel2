<?php

namespace App\Http\Controllers;

use App\Data\GatheredCompaniesType;
use App\Data\Survey2Source;
use App\Data\WebsiteValidateString;
use App\Data\WebsiteValidateType;
use App\Functions\DataIndex;
use App\Functions\Mail;
use App\Functions\System;
use App\Models\Country;
use App\Models\Customer;
use App\Models\CustomerGatheredReview;
use App\Models\CustomerOffice;
use App\Models\CustomerReviewScore;
use App\Models\GatheredCompany;
use App\Models\GatheredCompanyCountryWhitelisted;
use App\Models\GatheredRelatedSearch;
use App\Models\Insurance;
use App\Models\KTCustomerGatheredReview;
use App\Models\KTCustomerInsurance;
use App\Models\KTCustomerMembership;
use App\Models\KTCustomerObligation;
use App\Models\Language;
use App\Models\Membership;
use App\Models\MoverData;
use App\Models\Obligation;
use App\Models\SireloCities;
use App\Models\SireloCustomerOffices;
use App\Models\SireloCustomers;
use App\Models\SireloLink;
use App\Models\SireloSetting;
use App\Models\State;
use App\Models\Survey2;
use App\Models\User;
use DB;
use Illuminate\Http\Request;
use Log;
use function App\Console\Commands\encrypt;


class APIController extends Controller
{
    private $system;

    function __construct()
    {
        $this->system = new System();
    }

    public function getSireloCityInfo(Request $request)
    {
        $city = SireloCities::where('key', $request->key)->where('country_code', $_POST['co_code'])->first();

        return json_encode($city);
    }

    public function getAllCitiesInCountry(Request $request)
    {
        $cities = SireloCities::where('country_code', $request->co_code)->get();

        return json_encode($cities);
    }

    public function getAmountOfCustomersPerSirelo(Request $request)
    {
        $amount = SireloCustomers::selectRaw("COUNT(*) as amount")->where('country', $request->co_code)->first();

        return !empty($amount) ? json_encode($amount->amount) : json_encode("0");
    }

    public function getCityInStateInfo(Request $request)
    {
        $city = SireloCities::select("states.st_key", "sirelo_cities.*")->leftJoin("states", "state", "st_id")->where('key', $request->city)->where("st_key", $request->state)->where('country_code', $request->co_code)->first();

        return json_encode($city);
    }

    public function getAmountOfCitiesNearby(Request $request)
    {
        $cities = unserialize($request->cities);

        $nearby = SireloCities::selectRaw("sum(amount) as amount")->whereIn('city', $cities)->where("country_code", $request->co_code)->first();

        return json_encode($nearby);
    }

    public function getNearbyCitySearch(Request $request)
    {
        $nearby = SireloCities::select("nearby")->where("key", $request->search)->first();
        Log::debug($nearby->nearby);
        return json_encode($nearby->nearby);
    }

    public function getCitySearch(Request $request)
    {
        $cities = SireloCities::select("key", "city")->where('city', 'like', '%' . $request->search . '%')->where("country_code", $request->co_code)->orderBy("city", "asc")->limit(15)->get();

        return json_encode($this->system->databaseToArray($cities));
    }

    public function getKeyByCityName(Request $request)
    {
        $city = SireloCities::select("key")->where('city', $request->city)->first();

        return json_encode($city->key);
    }

    public function getCountries(Request $request)
    {
        $countries = Country::selectRaw("co_code as country_code")->selectRaw("co_" . $request->language . " as country")->whereNotIn("co_code", json_decode($request->exclude_countries))->orderBy("co_" . $request->language)->get();

        return json_encode($countries);
    }

    public function getCountryName(Request $request)
    {
        $country_name = Country::select("co_" . $request->la_code)->where("co_code", $request->co_code)->get();

        //Set this so Sirelo can handle the data
        $array[0] = new \stdClass();
        $array[0]->name = $country_name[0]['co_' . $request->la_code];

        return json_encode($array);
    }

    public function getCustomer(Request $request)
    {
        $customer = SireloCustomers::where("key", $request->key)->first();

        return json_encode($customer);
    }

    public function getCustomerForward(Request $request)
    {
        $customer = SireloCustomers::select("key")
            ::where(function ($query) use ($request) {
                $query
                    ->where('forward_1', $request->key)
                    ->orWhere('forward_2', $request->key);
            })->first();

        return json_encode($customer);
    }

    public function getCustomersInCity(Request $request)
    {
        $city_customers = SireloCustomers::select("key", "top_mover", "company_name", "logo", "national_moves", "international_moves", "city", "rating", "reviews")->selectRaw("(`rating` - IF(`rating` >= 3.5, 1 / `reviews` * 20 ,20)) AS `corrected_rating`, 0 AS `office`")->where("country", $request->co_code)->whereRaw($request->where)->get();

        return json_encode($city_customers);
    }

    public function getCustomerInsurances(Request $request)
    {
        $query_customer_insurances = KTCustomerInsurance::where("ktcuin_cu_id", $request->cu_id)->get();

        // If there are customer insurances
        if ($query_customer_insurances->count())
        {
            // Loop customer insurances
            foreach ($query_customer_insurances as $row_customer_insurance)
            {
                $kt_customer_insurances[$row_customer_insurance->ktcuin_id] = [
                    "ktcuin_id" => $row_customer_insurance->ktcuin_id,
                    "verified_timestamp" => $row_customer_insurance->ktcuin_verified_timestamp,
                    "verified_result" => $row_customer_insurance->ktcuin_verification_result
                ];
            }
        }

        return json_encode($kt_customer_insurances);
    }

    public function getCustomerMemberships(Request $request)
    {
        $customer_key_query = MoverData::select("moda_sirelo_key")->where("moda_cu_id", $request->cu_id)->first();

        $query_customer_memberships = KTCustomerMembership::leftJoin("memberships", "ktcume_me_id", "me_id")->where("ktcume_cu_id", $request->cu_id)->get();

        // If there are customer memberships
        if ($query_customer_memberships->count())
        {

            // Loop customer memberships
            foreach ($query_customer_memberships as $row_customer_memberships)
            {

                // Store customer memberships
                $customer_memberships[] = [
                    "customer_key" => $customer_key_query->moda_sirelo_key,
                    "me_name" => $row_customer_memberships->me_name,
                    "me_id" => $row_customer_memberships->me_id,
                ];
            }
        }

        return json_encode($customer_memberships);
    }

    public function getCustomerObligations(Request $request)
    {
        $query_customer_obligations = KTCustomerObligation::leftJoin("customer_documents", "cudo_id", "ktcuob_cudo_id")->where("ktcuob_cu_id", $request->cu_id)->get();

        if ($query_customer_obligations->count())
        {
            foreach ($query_customer_obligations as $row_customer_obligations)
            {
                $kt_customer_obligations[$row_customer_obligations->ktcuob_cuob_id] = [
                    "verified_timestamp" => $row_customer_obligations->ktcuob_verified_timestamp,
                    "verified_result" => $row_customer_obligations->ktcuob_verification_result,
                    "number" => ((!empty($row_customer_obligations->cudo_number)) ? $row_customer_obligations->cudo_number : "")
                ];
            }
        }

        return json_encode($kt_customer_obligations);
    }

    public function getNearbyCustomers(Request $request)
    {
        if($request->city_nearby)
        {
            $nearby_customers = SireloCustomers::select("*")
                ->selectRaw("(`rating` - IF(`rating` >= 3.5, 1 / `reviews` * 20 , 20)) AS `corrected_rating`")
                ->where("key", "!=", $request->customer_key)
                ->where(function ($query) use ($request) {
                    $query
                        ->where('city_key', $request->city_key)
                        ->orWhereIn('city', unserialize($request->city_nearby));
                })
                ->where("rating", ">=", 1)
                ->where("country", $request->co_code)
                ->orderBy("corrected_rating", "desc")
                ->limit(5)
                ->get();

            return json_encode($nearby_customers);

        }else{
            return null;
        }

    }

    public function getCustomerOffices(Request $request)
    {

        $query_customer_offices = CustomerOffice::where("cuof_cu_id", $request->cu_id)->get();

        // Check if there are any
        if ($query_customer_offices->count())
        {

            // Loop offices from this customer
            foreach ($query_customer_offices as $row_customer_offices)
            {

                // Setup customer office data
                $customer_offices[] = [
                    "street" => $row_customer_offices->cuof_street_1,
                    "zipcode" => $row_customer_offices->cuof_zipcode,
                    "city" => $row_customer_offices->cuof_city,
                    "telephone" => $row_customer_offices->cuof_telephone,
                ];
            }
        }

        return json_encode($customer_offices);
    }

    public function getSearchedCustomers(Request $request)
    {

        $customers = SireloCustomers::select("key", "company_name")
            ->where('company_name', 'like', '%' . $request->search . '%')
            ->where("country", $request->co_code)
            ->orderBy("company_name", 'asc')
            ->limit(15)
            ->get();

        return json_encode($customers);
    }

    public function getKeyByCustomerName(Request $request)
    {
        $customer = SireloCustomers::select("key")
            ->where("company_name", $request->customer)
            ->first();

        return json_encode($customer->key);
    }

    public function getCustomerWidget(Request $request)
    {
        $customer = SireloCustomers::where("company_name", $request->customer)
            ->where(function ($query) use ($request) {
                $query
                    ->where('forward_1', $request->key)
                    ->orWhere('forward_2', $request->key);
            })
            ->first();

        return json_encode($customer);
    }

    public function getCustomersMoveToCountry(Request $request)
    {
        $customers_to_country = SireloCustomers::select("*")
            ->selectRaw("(`rating` - IF(`rating` >= 3.5, 1 / `reviews` * 20 , 20)) AS `corrected_rating`")
            ->leftJoin("sirelo_customers_to_countries", "sirelo_customers.key", "sirelo_customers_to_countries.customer_key")
            ->where("rating", ">=", 1)
            ->where("sirelo_customers_to_countries.country_code", $request->top_country)
            ->where("sirelo_customers.country", $request->co_code)
            ->groupBy("sirelo_customers.key")
            ->orderBy("corrected_rating", "desc")
            ->limit(5)
            ->get();

        return json_encode($customers_to_country);
    }


    public function getFilteredCustomers(Request $request)
    {
        $customers = SireloCustomers::select("key", "top_mover", "company_name", "logo", "national_moves", "international_moves", "city", "rating", "reviews")
            ->selectRaw("(`rating` - IF(`rating` >= 3.5, 1 / `reviews` * 20 ,20)) AS `corrected_rating`, 0 AS `office`")
            ->where("country", $request->co_code)
            ->whereRaw($request->where.$request->where_extra)
            ->get();

        return json_encode($customers);
    }


    public function getFilteredCustomerOffices(Request $request)
    {
        $office_customers = SireloCustomerOffices::select("sirelo_customers.key", "top_mover", "company_name", "logo", "national_moves", "international_moves", "sirelo_cities.city", "rating", "reviews")
            ->selectRaw("(`rating` - IF(`rating` >= 3.5, 1 / `reviews` * 20 ,20)) AS `corrected_rating`, 1 AS `office`")
            ->leftJoin("sirelo_customers", "sirelo_customer_offices.customer_key",  "sirelo_customers.key")
            ->leftJoin("sirelo_cities", "sirelo_customer_offices.city_key",  "sirelo_cities.key")
            ->whereRaw($request->where.$request->where_extra)
            ->where("sirelo_customers.country", $request->co_code)
            ->get();

        return json_encode($office_customers);
    }

    public function getInsurances(Request $request)
    {
        $insurances = Insurance::all();

        return json_encode($insurances);
    }


    public function getMemberships(Request $request)
    {
        $memberships = Membership::all();

        return json_encode($memberships);
    }


    public function getObligations(Request $request)
    {
        $obligations = Obligation::where("cuob_co_code", strtoupper($request->co_code))->get();

        return json_encode($obligations);
    }


    public function getCityKeyByKeyOrCity(Request $request)
    {
        $city = SireloCities::select("key")
        ->where(function ($query) use ($request) {
            $query
                ->where('key', $request->sanitized_city)
                ->orWhere('city', $request->city);
        })->first();

        return json_encode($city->key);
    }

    public function getCustomerReviews(Request $request)
    {

        $query_surveys = Survey2::leftJoin("requests", "su_re_id", "re_id")
            ->leftJoin("website_reviews", "su_were_id", "were_id")
            ->where("su_submitted", 1)
            ->where(function ($query) use ($request) {
                $query
                    ->where('su_mover', $request->cu_id)
                    ->orWhere('su_other_mover', $request->cu_id);
            })
            ->where("su_show_on_website", 1)
            ->orderBy("su_submitted_timestamp", 'desc')
            ->get();

        $reviews = [];

        // Loop surveys
        foreach( $query_surveys as $row_surveys ) {

            // If the survey is shown on the website
            if( $row_surveys->su_show_on_website == 1 ) {

                // If survey came from request
                if( $row_surveys->su_source == 1 ) {

                    $prefix = "re";
                    $author = $row_surveys[$prefix.'_full_name'];
                }
                // If survey came from website review form
                elseif( $row_surveys->su_source == 2 ) {

                    $prefix = "were";
                    $author = $row_surveys[$prefix.'_name'];
                }

                if( $row_surveys->su_source == Survey2Source::REQUEST || ( $row_surveys->su_source == Survey2Source::WEBSITE_REVIEW && $row_surveys->were_anonymous == 1 ) ) {

                    $exp = explode(" ", preg_replace("/&([a-z])[a-z]+;/i", "$1", htmlentities(System::rusToLat( $author  ) ) ) );

                    $author = "";

                    foreach( $exp as $letter ) {

                        $author .= ((isset($letter[0])) ? strtoupper($letter[0])."." : "");
                    }
                }

                $reviews[$row_surveys->su_id] = [
                    "customer_key" => "test",
                    "source" => $row_surveys->su_source,
                    "author" => $author,
                    "date" => date("Y-m-d", strtotime($row_surveys->su_submitted_timestamp)),
                    "rating" => $row_surveys->su_rating,
                    "rating_motivation" => $row_surveys->su_rating_motivation,
                    "recommend" => $row_surveys->su_recommend_mover,
                    "pro_1" => $row_surveys->su_pro_1,
                    "pro_2" => $row_surveys->su_pro_2,
                    "con_1" => $row_surveys->su_con_1,
                    "con_2" => $row_surveys->su_con_2,
                    "city_from" => $row_surveys[$prefix.'_city_from'],
                    "country_from" => $row_surveys[$prefix.'_co_code_from'],
                    "city_to" => $row_surveys[$prefix.'_city_to'],
                    "country_to" => $row_surveys[$prefix.'_co_code_to'],
                    "mover_comment_date" => date("Y-m-d", strtotime($row_surveys->su_mover_comment_timestamp)),
                    "mover_comment" => $row_surveys->su_mover_comment
                ];

            }
        }

        return json_encode($reviews);
    }


    public function checkStateAndCityKey(Request $request)
    {
        $customer = SireloCustomers::select("key")->where("key", $request->key)->first();

        return json_encode($customer->key);
    }


    public function getSireloHyperlinks(Request $request)
    {

        $hyperlinks = SireloLink::where("sihy_co_code", $request->co_code);

        if(isset($request->membership_id)){
            $hyperlinks->where("sihy_me_id", $request->membership_id);
        }
        if(isset($request->obligation_id)){
            $hyperlinks->where("sihy_cuob_id", $request->obligation_id);
        }

        $hyperlinks = $hyperlinks->get();

        return json_encode($hyperlinks);
    }

    public function getSireloSetting(Request $request)
    {
        $setting = SireloSetting::where("sise_name", $request->setting)->first();

        return json_encode($setting->sise_value);
    }

    public function getStateInfo(Request $request)
    {
        $state = State::select("states.*")->selectRaw("SUM(`sirelo_cities`.`amount`) AS `amountTotal`")
            ->leftJoin("sirelo_cities", "sirelo_cities.state", "st_id")
            ->where("st_key", $request->state)
            ->where("st_co_code", $request->co_code)
            ->first();

        return json_encode($state);
    }

    public function checkStateIsCity(Request $request)
    {
        $customer = SireloCities::select("st_key")
            ->leftJoin("states", "sirelo_cities.state", "st_id")
            ->where("sirelo_cities.key", $request->state)
            ->first();

        return json_encode($customer->st_key);
    }

    public function getTopCities(Request $request)
    {
        $cities = SireloCities::where("country_code", $request->co_code)
            ->orderBy("amount", "desc")
            ->limit(5)
            ->get();

        return json_encode($cities);
    }


    public function getTopReviews(Request $request)
    {

        //TODO: deze vreselijke query fixen?
        $reviews = DB::select("SELECT * FROM (
				SELECT  `moda_sirelo_key` as `customer_key`, `cu_company_name_business` as `company_name`, `cu_city` as `city`, `su_rating` as `review_rating`, DATE(`su_submitted_timestamp`) as `date`, `su_rating_motivation` as `rating_motivation` FROM `mover_data`
					LEFT JOIN `sirelo_customers` on moda_sirelo_key = `sirelo_customers`.`key`
                    LEFT JOIN `surveys_2` on su_mover = sirelo_customers.origin_id
					LEFT JOIN `customers` on moda_cu_id = cu_id
                WHERE su_submitted = 1 AND su_show_on_website = 1 AND cu_co_code = '".$request->co_code."'
				ORDER BY `date` DESC
				LIMIT 10
			) tmp_table
			GROUP BY `customer_key`
			ORDER BY `date` DESC
			LIMIT 3");


        return json_encode($reviews);
    }


    public function getStates(Request $request)
    {
        $states = State::select("st_id", "st_name", "st_key")
            ->where("st_co_code", $request->co_code)
            ->orderBy("st_name")
            ->get();

        return json_encode($states);
    }


    public function getCitiesInState(Request $request)
    {
        $states = SireloCities::select("key", "city", "amount")
            ->where("state", $request->st_id)
            ->orderBy("city")
            ->get();

        return json_encode($states);
    }

    public function checkAutomaticallyProcessing($curesc_id, $response) : void {
        /**
         * Check if its possible to process automatically
         */

        //Get right record for this check
        $curesc_row = CustomerReviewScore::where("curesc_id", $curesc_id)->first();

        if (count($curesc_row) == 1) {
            $customer = Customer::leftJoin("mover_data", "cu_id", "moda_cu_id")->where("cu_id", $curesc_row->curesc_cu_id)->first();
            $ajaxcontroller = new AjaxController();

            //Check if we don't found Google data, if website ERP is good and if Sirelo Disabled Export functionality is off
            if (!empty($customer->cu_street_1) && !empty($customer->cu_zipcode) && !empty($customer->cu_city) && $curesc_row->curesc_processed == 0 && empty($curesc_row->curesc_name) && (strpos($response->html_string, 'Current ERP URL is good') !== false || strpos($response->html_string, 'Current ERP URL is the best one') !== false) && strpos($response->html_string, 'No Google URL scraped') !== false && $customer->moda_disable_sirelo_export == 0) {
                //Its possible to process this record automatically, but only if they got a valid sirelo email and a valid Sirelo phone number

                //Make new post request
                $website_validate_request = new Request();
                $website_validate_request->url_erp = $customer->cu_website;
                $website_validate_request->url_other = $curesc_row->curesc_customer_website ?? '';
                $website_validate_request->email_general = $customer->cu_email;
                $website_validate_request->email_sirelo = $customer->moda_contact_email;

                //Post the request to a function to validate the email domain
                $website_validate_response = json_decode($ajaxcontroller->validateEmailDomain($website_validate_request));

                //Check if email of Sirelo is valid
                if ($website_validate_response->email_sirelo) {

                    //Create post request for function to validate phone numbers
                    $phone_validate_request = new Request();
                    $phone_validate_request->phone_general = $customer->cu_telephone;
                    $phone_validate_request->phone_sirelo = $customer->moda_contact_telephone;
                    $phone_validate_request->phone_scraped = $customer->curesc_telephone;
                    $phone_validate_request->co_code = $customer->cu_co_code;

                    //Send post to function which validates the phone numbers and save to variable
                    $phone_validate_response = json_decode($ajaxcontroller->validatePhoneNumbers($phone_validate_request));

                    //Check if we got something back from the function
                    if ($phone_validate_response != null) {
                        //Check if Sirelo number is valid
                        if( $phone_validate_response->converted_phone_sirelo != 'not_valid' ) {
                            //Find mover data row
                            $moverdata = MoverData::where("moda_cu_id", $customer->cu_id)->first();

                            //Check if there is a moverdata row found
                            if (count($moverdata) > 0) {
                                //Found, save Sirelo phone number in right format
                                $moverdata->moda_contact_telephone = $phone_validate_response->converted_phone_sirelo;
                                $moverdata->save();
                            }

                            //This record can be validated automatically, update processed and timestamp of processing
                            $curesc_row->curesc_processed = 1;
                            $curesc_row->curesc_processed_by = 0;
                            $curesc_row->curesc_processed_timestamp = date("Y-m-d H:i:s");
                        }
                    }
                }
            }
        }
    }

    public function newCustomerCreateNewGoogleRecordAndGetID($customerID, $gathered_company, $customerData, $processed, $type, $platform, $street, $zipcode, $city, $co_code, $website_validation_html) {
        //Create Google record
        DB::table( "customer_review_scores" )
            ->insert( [
                'curesc_cu_id' => null,
                'curesc_gaco_id' => $gathered_company->gaco_id,
                'curesc_type' => $type,
                'curesc_score' => str_replace( ',', '.', $customerData['score'] ) ?? 0,
                'curesc_amount' => $customerData['amount'] ?? 0,
                'curesc_name' => trim($customerData['name']) ?? '',
                'curesc_telephone' => trim($customerData['telephone']) ?? '',
                'curesc_address' => trim($customerData['address']) ?? '',
                'curesc_street' => trim($street) ?? '',
                'curesc_zipcode' => trim($zipcode) ?? '',
                'curesc_city' => trim($city) ?? '',
                'curesc_customer_website' => trim($customerData['website']) ?? '',
                'curesc_url' => trim($customerData['url']) ?? '',
                'curesc_search_url' => trim($customerData['search_url']),
                'curesc_timestamp' => date("Y-m-d H:i:s", strtotime( "+2 HOURS", strtotime($customerData['timestamp']) )),
                'curesc_platform' => $platform,
                'curesc_processed' => $processed,
                'curesc_skip_counter' => 0,
                'curesc_description' => $customerData['description'] ?? '',
                'curesc_permanently_closed' => $customerData['not_operational'] ?? 0,
                'curesc_new_company_website_validation_html' => $website_validation_html ?? 0,
            ] );

        //Get inserted curecs_id
        $curesc_id = CustomerReviewScore::where("curesc_gaco_id", $gathered_company->gaco_id)->first()->curesc_id;

        //Insert Google reviews to DB table
        DB::table("customer_gathered_reviews")
            ->insert( [
                'cugare_curesc_id' => $curesc_id,
                'cugare_cu_id' => null,
                'cugare_platform' => $platform,
                'cugare_score' => str_replace( ',', '.', $customerData['score'] ) ?? 0,
                'cugare_original_score' => str_replace( ',', '.', $customerData['score'] ) ?? 0,
                'cugare_original_range' => 5,
                'cugare_amount' => $customerData['amount'] ?? 0,
                'cugare_url' => $customerData['search_url'] ?? '',
                'cugare_accepted' => 0,
            ] );

        /**
         * The next part is for saving all review platforms which we got back from Google
         */
        //Loop through all review platforms which we got back from Google
        foreach($customerData['review_results'] as $review_platform => $result)
        {
            //Strip gathered score to score and range. (Example: Score is 4/5, then score is 4, range is 5)
            $score = str_replace( ',', '.', $result['score'] );
            $score = preg_split("#/#", $score);
            $original_score = $score[0];
            $original_range = $score[1];

            //If score has % sign, then original range is 100
            if (strpos($result['score'], '%') !== false) {
                $original_range = 100;
            }

            //If original range is not 10, then calculate score to a range of 10
            if ($original_range != 10) {
                $score[0] = System::numberFormat(($original_score / $original_range) * 10, 1, ".", "");
            }

            //Insert the review row for this platform
            DB::table("customer_gathered_reviews")
                ->insert( [
                    'cugare_curesc_id' => $curesc_id,
                    'cugare_cu_id' => null,
                    'cugare_platform' => $result['name'],
                    'cugare_score' => $score[0] ?? 0,
                    'cugare_original_score' => $original_score ?? 0,
                    'cugare_original_range' => $original_range ?? 0,
                    'cugare_amount' => $result['amount'] ?? 0,
                    'cugare_url' => $result['url'] ?? '',
                    'cugare_accepted' => 0,
                ] );
        }

        /**
         * The next part is for saving the related moving companies to the database
         */
        //Loop through all saved related moving companies
        foreach ($customerData['related_moving_companies'] as $related_company) {
            //Check if company is already in the DB
            $related_comp_finder = GatheredCompany::where("gaco_name", $related_company['name'])->first();

            //IF there is something found, then don't add
            if (count($related_comp_finder) == 0) {
                //No company with this name found in the DB, so insert a new record to this table
                $related_comp_new = new GatheredCompany();
                $related_comp_new->gaco_name = $related_company['name'];
                $related_comp_new->gaco_title = $related_company['kind_of_company'];
                $related_comp_new->gaco_searched_city = trim($city);
                $related_comp_new->gaco_searched_co_code = $co_code;
                $related_comp_new->gaco_searched_customer = trim($customerData['name']);
                $related_comp_new->save();
            }
        }

        /**
         * The next part is for saving the related moving companies to the database
         */
        //Loop through all related search results
        foreach ($customerData['related_search_results'] as $related_search) {
            //Check if there is already a row with the same search result
            $related_search_finder = GatheredRelatedSearch::where("garese_related_search", $related_search['search_result'])->first();

            //Check if there is a row found in the DB
            if (count($related_search_finder) == 0) {
                //No result found, so insert a new row and insert the search result
                $related_search_new = new GatheredRelatedSearch();
                $related_search_new->garese_related_search = $related_search['search_result'];
                $related_search_new->garese_searched_city = $city;
                $related_search_new->garese_searched_co_code = $co_code;
                $related_search_new->garese_searched_customer = $customerData['name'];
                $related_search_new->save();
            }
        }

        //Add record to fetched google history table
        //self::addToFetchedGoogleHistoryTable($curesc_id, $customer->cu_id, 1, 1, (($skipcounter) ? 1 : 0),0,0);

        //End of this function. Returning back the inserted record ID
        return $curesc_id;
    }

    public function createNewGoogleRecordAndGetID($customerID, $customer, $customerData, $processed, $skipcounter, $platform, $street, $zipcode, $city, $accept_reviews, $final_url_erp, $final_url_other) {
        //Create Google record

        DB::table( "customer_review_scores" )
            ->insert( [
                'curesc_cu_id' => $customerID,
                'curesc_score' => str_replace( ',', '.', $customerData['score'] ) ?? 0,
                'curesc_amount' => $customerData['amount'] ?? 0,
                'curesc_name' => trim($customerData['name']) ?? '',
                'curesc_telephone' => trim($customerData['telephone']) ?? '',
                'curesc_address' => trim($customerData['address']) ?? '',
                'curesc_street' => trim($street) ?? '',
                'curesc_zipcode' => trim($zipcode) ?? '',
                'curesc_city' => trim($city) ?? '',
                'curesc_customer_website' => trim($customerData['website']) ?? '',
                'curesc_url' => trim($customerData['url']) ?? '',
                'curesc_search_url' => trim($customerData['search_url']),
                'curesc_timestamp' => date("Y-m-d H:i:s", strtotime( "+2 HOURS", strtotime($customerData['timestamp']) )),
                'curesc_platform' => $platform,
                'curesc_processed' => $processed,
                'curesc_skip_counter' => $skipcounter,
                'curesc_description' => $customerData['description'] ?? '',
                'curesc_permanently_closed' => $customerData['not_operational'] ?? 0,
                'curesc_final_url_erp' => $final_url_erp,
                'curesc_final_url_other' => $final_url_other
            ] );

        //Get inserted curecs_id
        $curesc_id = CustomerReviewScore::where("curesc_cu_id", $customerID)->first()->curesc_id;

        //Insert Google reviews to DB table
        DB::table("customer_gathered_reviews")
            ->insert( [
                'cugare_curesc_id' => $curesc_id,
                'cugare_cu_id' => $customerID,
                'cugare_platform' => $platform,
                'cugare_score' => str_replace( ',', '.', $customerData['score'] ) ?? 0,
                'cugare_original_score' => str_replace( ',', '.', $customerData['score'] ) ?? 0,
                'cugare_original_range' => 5,
                'cugare_amount' => $customerData['amount'] ?? 0,
                'cugare_url' => $customerData['search_url'] ?? '',
                'cugare_accepted' => (($accept_reviews) ? 1 : 0),
            ] );

        //Can the reviews automatically accepted?
        if ($accept_reviews) {
            //Reviews can be accepted, accept the Google reviews for on Sirelo
            DB::table("kt_customer_gathered_reviews")
                ->insert( [
                    'ktcugare_cu_id' => $customerID,
                    'ktcugare_platform' => $platform,
                    'ktcugare_score' => str_replace( ',', '.', $customerData['score'] ) ?? 0,
                    "ktcugare_original_score" => str_replace( ',', '.', $customerData['score'] ) ?? 0,
                    "ktcugare_original_range" => 5,
                    'ktcugare_amount' => $customerData['amount'] ?? 0,
                    'ktcugare_url' => $customerData['search_url'] ?? '',
                    'ktcugare_timestamp_updated' => date("Y-m-d H:i:s"),
                ] );
        }

        /**
         * The next part is for saving all review platforms which we got back from Google
         */
        //Loop through all review platforms which we got back from Google
        foreach($customerData['review_results'] as $review_platform => $result)
        {
            //Strip gathered score to score and range. (Example: Score is 4/5, then score is 4, range is 5)
            $score = str_replace( ',', '.', $result['score'] );
            $score = preg_split("#/#", $score);
            $original_score = $score[0];
            $original_range = $score[1];

            //If score has % sign, then original range is 100
            if (strpos($result['score'], '%') !== false) {
                $original_range = 100;
            }

            //If original range is not 10, then calculate score to a range of 10
            if ($original_range != 10) {
                $score[0] = System::numberFormat(($original_score / $original_range) * 10, 1, ".", "");
            }

            //Insert the review row for this platform
            DB::table("customer_gathered_reviews")
                ->insert( [
                    'cugare_curesc_id' => $curesc_id,
                    'cugare_cu_id' => $customerID,
                    'cugare_platform' => $result['name'],
                    'cugare_score' => $score[0] ?? 0,
                    'cugare_original_score' => $original_score ?? 0,
                    'cugare_original_range' => $original_range ?? 0,
                    'cugare_amount' => $result['amount'] ?? 0,
                    'cugare_url' => $result['url'] ?? '',
                    'cugare_accepted' => (($accept_reviews) ? 1 : 0),
                ] );

            if ($accept_reviews) {
                //Reviews can be automatically accepted. Insert into table for Sirelo sync
                DB::table("kt_customer_gathered_reviews")
                    ->insert( [
                        'ktcugare_cu_id' => $customerID,
                        'ktcugare_platform' => $result['name'],
                        'ktcugare_score' => $score[0] ?? 0,
                        "ktcugare_original_score" => $original_score ?? 0,
                        "ktcugare_original_range" => $original_range ?? 0,
                        'ktcugare_amount' => $result['amount'] ?? 0,
                        'ktcugare_url' => $result['url'] ?? '',
                        'ktcugare_timestamp_updated' => date("Y-m-d H:i:s"),
                    ] );
            }
        }

        /**
         * The next part is for saving the related moving companies to the database
         */
        //Loop through all saved related moving companies
        foreach ($customerData['related_moving_companies'] as $related_company) {
            //Check if company is already in the DB
            $related_comp_finder = GatheredCompany::where("gaco_name", $related_company['name'])->first();

            //IF there is something found, then don't add
            if (count($related_comp_finder) == 0) {
                //No company with this name found in the DB, so insert a new record to this table
                $related_comp_new = new GatheredCompany();
                $related_comp_new->gaco_name = $related_company['name'];
                $related_comp_new->gaco_title = $related_company['kind_of_company'];
                $related_comp_new->gaco_searched_city = $customer->cu_city;
                $related_comp_new->gaco_searched_co_code = $customer->cu_co_code;
                $related_comp_new->gaco_searched_customer = $customer->cu_company_name_business;
                $related_comp_new->save();
            }
        }

        /**
         * The next part is for saving the related moving companies to the database
         */
        //Loop through all related search results
        foreach ($customerData['related_search_results'] as $related_search) {
            //Check if there is already a row with the same search result
            $related_search_finder = GatheredRelatedSearch::where("garese_related_search", $related_search['search_result'])->first();

            //Check if there is a row found in the DB
            if (count($related_search_finder) == 0) {
                //No result found, so insert a new row and insert the search result
                $related_search_new = new GatheredRelatedSearch();
                $related_search_new->garese_related_search = $related_search['search_result'];
                $related_search_new->garese_searched_city = $customer->cu_city;
                $related_search_new->garese_searched_co_code = $customer->cu_co_code;
                $related_search_new->garese_searched_customer = $customer->cu_company_name_business;
                $related_search_new->save();
            }
        }

        //Add record to fetched google history table
        self::addToFetchedGoogleHistoryTable($curesc_id, $customer->cu_id, 1, 1, (($skipcounter) ? 1 : 0),0,0);

        //End of this function. Returning back the inserted record ID
        return $curesc_id;
    }

    public function updateGoogleRecordAndGetID($curesc_id, $customer, $customerData, $platform, $street, $zipcode, $city, $accept_reviews, $final_url_erp, $final_url_other) {
        //Get the record from DB to update
        $curesc_row = CustomerReviewScore::where("curesc_id", $curesc_id)->first();

        //Check if the record exist
        if(count($curesc_row) == 1) {
            $processed_before = $curesc_row->curesc_processed;

            //Record exists, now we can update the record

            //Save history of previous record into DB column
            $curesc_row->curesc_history = System::serialize($curesc_row);

            //This variable is used to check if a record needs to go back into the validation process
            $processed = $curesc_row->curesc_processed;

            if (trim($curesc_row->curesc_name) != (trim($customerData['name']) ?? '') ||
                trim($curesc_row->curesc_telephone) != (trim($customerData['telephone']) ?? '') ||
                trim($curesc_row->curesc_street) != (trim($street) ?? '') ||
                trim($curesc_row->curesc_zipcode) != (trim($zipcode) ?? '') ||
                trim($curesc_row->curesc_city) != (trim($city) ?? '') ||
                trim($curesc_row->curesc_customer_website) != (trim($customerData['website']) ?? '') ||
                trim($curesc_row->curesc_url) != (trim($customerData['url']) ?? '')
            ) {
                //If name, telephone, or website is changed then put processed on 0, so this record will get back in the validation process
                if (trim($curesc_row->curesc_name) != (trim($customerData['name']) ?? '') ||
                    trim($curesc_row->curesc_telephone) != (trim($customerData['telephone']) ?? '') ||
                    trim($curesc_row->curesc_customer_website) != (trim($customerData['website']) ?? '') ||
                    trim($curesc_row->curesc_url) != (trim($customerData['url']) ?? '')
                ) {
                    //Processed to 0
                    $processed = 0;
                }

                //Check if street, zipcode or city is changed, then based on if the address string is the same or not, it will be put back into the validation process or not
                if (trim($curesc_row->curesc_street) != (trim($street) ?? '') ||
                    trim($curesc_row->curesc_zipcode) != (trim($zipcode) ?? '') ||
                    trim($curesc_row->curesc_city) != (trim($city) ?? '')
                ) {
                    //Street, zipcode or city is changed. Check if the whole address string is also different. If it is different, then put processed to 0
                    if (trim($curesc_row->curesc_address) != trim($customerData['address']) && !empty($customerData['address'])) {
                        if (trim($customerData['address']) != trim($customerData['telephone'])) {
                            $processed = 0;
                        }
                    }
                }

                //Update records
                $curesc_row->curesc_name = trim($customerData['name']) ?? '';
                $curesc_row->curesc_telephone = trim($customerData['telephone']) ?? '';

                if (!empty($customerData['address'])) {
                    $curesc_row->curesc_street = trim($street) ?? '';
                    $curesc_row->curesc_zipcode = trim($zipcode) ?? '';
                    $curesc_row->curesc_city = trim($city) ?? '';
                }

                $curesc_row->curesc_customer_website = trim($customerData['website']) ?? '';
                $curesc_row->curesc_url = trim($customerData['url']) ?? '';
                $curesc_row->curesc_search_url = trim($customerData['search_url']);
            }

            if (!empty($customerData['address'])) {
                //Save address string
                $curesc_row->curesc_address = $customerData['address'] ?? '';
            }

            //Save gathered timestamp, timestamp +2 hours is needed, because Python server is on another timezone
            $curesc_row->curesc_timestamp = date("Y-m-d H:i:s", strtotime( "+2 HOURS", strtotime($customerData['timestamp']) ));

            //If this record was processed, then save new processed based on if something is changed or not
            if ($curesc_row->curesc_processed == 1) {
                $curesc_row->curesc_processed = $processed;
            }

            //Save description
            $curesc_row->curesc_description = $customerData['description'] ?? '';

            //Save permanently closed field (DB field goes to 1 if Google told us a company is permanently closed)
            $curesc_row->curesc_permanently_closed = $customerData['not_operational'];

            //Save final URLS of ERP and Google
            $curesc_row->curesc_final_url_erp = $final_url_erp ?? '';
            $curesc_row->curesc_final_url_other = $final_url_other ?? '';

            //Save whole record
            $curesc_row->save();

            if ($curesc_row->curesc_processed == 1 && $curesc_row->curesc_use_reviews == 1) {
                $accept_reviews = 1;
            }

            /**
             * The next part is to handle/update the reviews
             */
            //Set processed on 1 of the previous scanned records (not on accepted, only processed)
            //Only Google reviews
            DB::table("customer_gathered_reviews")
                ->where("cugare_curesc_id", $curesc_row->curesc_id)
                ->where("cugare_platform", $platform)
                ->update(
                    [
                        "cugare_processed" => 1,
                    ]
                );

            //Add the new record for Google review to the DB
            //Set accepted and processed on 1 if the record is after the update still processed and if we accepted the record last time (use review checked)
            DB::table("customer_gathered_reviews")
                ->insert( [
                    'cugare_curesc_id' => $curesc_row->curesc_id,
                    'cugare_cu_id' => $customer->cu_id,
                    'cugare_platform' => $platform,
                    'cugare_score' => str_replace( ',', '.', $customerData['score'] ) ?? 0,
                    'cugare_original_score' => str_replace( ',', '.', $customerData['score'] ) ?? 0,
                    'cugare_original_range' => 5,
                    'cugare_amount' => $customerData['amount'] ?? 0,
                    'cugare_url' => $customerData['search_url'] ?? '',
                    'cugare_accepted' => (($curesc_row->curesc_processed == 1 && $accept_reviews) ? 1 : 0),
                    'cugare_processed' => (($curesc_row->curesc_processed == 1 && $accept_reviews) ? 1 : 0),
                ] );

            //Getting the Google review record (which is shown on Sirelo)
            $kt_customer_gathered_review = KTCustomerGatheredReview::where("ktcugare_platform", $platform)->where("ktcugare_cu_id", $curesc_row->curesc_cu_id)->get();

            //Check if we have found a record for Google which we show on Sirelo
            if (count($kt_customer_gathered_review) > 0) {
                if ($curesc_row->curesc_processed == 1 && $accept_reviews) {
                    //Update Google review score
                    DB::table("kt_customer_gathered_reviews")
                        ->where("ktcugare_cu_id", $customer->cu_id)
                        ->where("ktcugare_platform", $platform)
                        ->update(
                            [
                                "ktcugare_score" => str_replace( ',', '.', $customerData['score'] ) ?? 0,
                                "ktcugare_original_score" => str_replace( ',', '.', $customerData['score'] ) ?? 0,
                                "ktcugare_original_range" => 5,
                                "ktcugare_amount" => $customerData['amount'] ?? 0,
                                'ktcugare_timestamp_updated' => date("Y-m-d H:i:s")
                            ]
                        );
                }

            }
            else {
                //No Google review found of previous time. Now check if record is processed and accepted last time
                if ($accept_reviews) {
                    //Record is still processed and accepted, now insert Google review score into the table which will be shown on Sirelo
                    DB::table("kt_customer_gathered_reviews")
                        ->insert( [
                            'ktcugare_cu_id' => $customer->cu_id,
                            'ktcugare_platform' => $platform,
                            'ktcugare_score' => str_replace( ',', '.', $customerData['score'] ) ?? 0,
                            "ktcugare_original_score" => str_replace( ',', '.', $customerData['score'] ) ?? 0,
                            "ktcugare_original_range" => 5,
                            'ktcugare_amount' => $customerData['amount'] ?? 0,
                            'ktcugare_url' => $customerData['search_url'] ?? '',
                            'ktcugare_timestamp_updated' => date("Y-m-d H:i:s"),
                        ] );
                }
            }

            /**
             * The next part is for processing the rest of the review platforms which we got back from Google
             */

            //Loop through all 'extra' review platforms which we got back from Google (if we got something back)
            foreach($customerData['review_results'] as $review_platform => $result)
            {
                //Score which we got back is now for example 4/5
                //We have to split the score to original score and range. Score is 4, range is 5.
                $score = str_replace( ',', '.', $result['score'] );
                $score = preg_split("#/#", $score);
                $original_score = $score[0];
                $original_range = $score[1];

                //If score has % in it, then original range is 100
                if (strpos($result['score'], '%') !== false) {
                    $original_range = 100;
                }

                //If range is not 10, then calculate it to 10 (score 4/5 will be 8/10)
                if ($score[1] != 10) {
                    $score[0] = System::numberFormat(($score[0] / $score[1]) * 10, 1, ".", "");
                }

                //Search for review row with this platform for this customer
                $customer_gathered_review = CustomerGatheredReview::where("cugare_platform", $result['name'])->where("cugare_cu_id", $customer->cu_id)->where("cugare_processed", 0)->get();

                //Check if there is already a row with this platform for this customer
                if (count($customer_gathered_review) > 0) {
                    //There is already a row, so update it
                    DB::table("customer_gathered_reviews")
                        ->where("cugare_curesc_id", $curesc_row->curesc_id)
                        ->where("cugare_platform", $result['name'])
                        ->update(
                            [
                                "cugare_score" => $score[0] ?? 0,
                                "cugare_original_score" => $original_score ?? 0,
                                "cugare_original_range" => $original_range ?? 0,
                                "cugare_amount" => $result['amount'] ?? 0,
                            ]
                        );
                }
                else {
                    //No record found, so insert a row with this platform for this customer
                    DB::table("customer_gathered_reviews")
                        ->insert( [
                            'cugare_curesc_id' => $curesc_row->curesc_id,
                            'cugare_cu_id' => $customer->cu_id,
                            'cugare_platform' => $result['name'],
                            'cugare_score' => $score[0] ?? 0,
                            'cugare_original_score' => $original_score ?? 0,
                            'cugare_original_range' => $original_range ?? 0,
                            'cugare_amount' => $result['amount'] ?? 0,
                            'cugare_url' => $result['url'] ?? '',
                            'cugare_accepted' => (($curesc_row->curesc_processed == 1 && $accept_reviews) ? 1 : 0),
                            'cugare_processed' => (($curesc_row->curesc_processed == 1 && $accept_reviews) ? 1 : 0),
                        ] );

                }

                //Search for an accepted row for this review platform and customer
                $kt_customer_gathered_review = KTCustomerGatheredReview::where("ktcugare_platform", $result['name'])->where("ktcugare_cu_id", $curesc_row->curesc_cu_id)->get();

                //Check if there is already a record accepted for this platform and customer
                if (count($kt_customer_gathered_review) > 0) {

                    if ($curesc_row->curesc_processed == 1 && $accept_reviews) {
                        //There is already a row, so update
                        DB::table("kt_customer_gathered_reviews")
                            ->where("ktcugare_cu_id", $customer->cu_id)
                            ->where("ktcugare_platform", $result['name'])
                            ->update(
                                [
                                    "ktcugare_score" => $score[0] ?? 0,
                                    "ktcugare_original_score" => $original_score ?? 0,
                                    "ktcugare_original_range" => $original_range ?? 0,
                                    "ktcugare_amount" => $result['amount'] ?? 0,
                                    'ktcugare_timestamp_updated' => date("Y-m-d H:i:s")
                                ]
                            );
                    }
                }
                else {
                    //No accepted row found, check if main record is processed and accepted
                    if ($curesc_row->curesc_processed == 1 && $accept_reviews) {
                        //Main record is processed and accepted, which means:
                        //Automatically accept the record and insert review in DB
                        DB::table("kt_customer_gathered_reviews")
                            ->insert( [
                                'ktcugare_cu_id' => $customer->cu_id,
                                'ktcugare_platform' => $result['name'],
                                'ktcugare_score' => $score[0] ?? 0,
                                "ktcugare_original_score" => $original_score ?? 0,
                                "ktcugare_original_range" => $original_range ?? 0,
                                'ktcugare_amount' => $result['amount'] ?? 0,
                                'ktcugare_url' => $result['url'] ?? '',
                                'ktcugare_timestamp_updated' => date("Y-m-d H:i:s"),
                            ] );
                    }
                }
            }

            /**
             * The next part is for saving the related moving companies to the database
             */
            //Loop through all saved related moving companies
            foreach ($customerData['related_moving_companies'] as $related_company) {
                //Check if company is already in the DB
                $related_comp_finder = GatheredCompany::where("gaco_name", $related_company['name'])->first();

                //IF there is something found, then don't add
                if (count($related_comp_finder) == 0) {
                    //No company with this name found in the DB, so insert a new record to this table
                    $related_comp_new = new GatheredCompany();
                    $related_comp_new->gaco_name = $related_company['name'];
                    $related_comp_new->gaco_title = $related_company['kind_of_company'];
                    $related_comp_new->gaco_searched_city = $customer->cu_city;
                    $related_comp_new->gaco_searched_co_code = $customer->cu_co_code;
                    $related_comp_new->gaco_searched_customer = $customer->cu_company_name_business;
                    $related_comp_new->save();
                }
            }

            /**
             * The next part is for saving the related moving companies to the database
             */
            //Loop through all related search results
            foreach ($customerData['related_search_results'] as $related_search) {
                //Check if there is already a row with the same search result
                $related_search_finder = GatheredRelatedSearch::where("garese_related_search", $related_search['search_result'])->first();

                //Check if there is a row found in the DB
                if (count($related_search_finder) == 0) {
                    //No result found, so insert a new row and insert the search result
                    $related_search_new = new GatheredRelatedSearch();
                    $related_search_new->garese_related_search = $related_search['search_result'];
                    $related_search_new->garese_searched_city = $customer->cu_city;
                    $related_search_new->garese_searched_co_code = $customer->cu_co_code;
                    $related_search_new->garese_searched_customer = $customer->cu_company_name_business;
                    $related_search_new->save();
                }
            }

            //Add record to fetched google history table
            $changed = 0;
            $unchanged = 0;

            if ($processed_before == 1 && $curesc_row->curesc_processed == 0) {
                $changed = 1;
            }
            else {
                $unchanged = 1;
            }

            self::addToFetchedGoogleHistoryTable($curesc_row->curesc_id, $curesc_row->curesc_cu_id, 1, 0, 0, $changed, $unchanged);

            //Return the ID of the updated row
            return $curesc_row->curesc_id;
        }
    }

    public function addToFetchedGoogleHistoryTable($curesc_id, $cu_id, $fetched, $created, $skipped, $changed, $unchanged) {
        DB::table("fetched_google_history")
            ->insert( [
                'fegohi_curesc_id' => $curesc_id,
                'fegohi_cu_id' => $cu_id,
                'fegohi_timestamp' => date("Y-m-d H:i:s"),
                'fegohi_fetched' => $fetched,
                'fegohi_created' => $created,
                'fegohi_skipped' => $skipped,
                'fegohi_changed' => $changed,
                'fegohi_unchanged' => $unchanged,
            ] );
    }

    public function postScrapeData(Request $request) {

        $json = file_get_contents('php://input');
        $data = json_decode($json, true);
        $system = new System();
        //$system->sendMessage(4186, "python", System::serialize($data));

        $lock_file_location = env("SHARED_FOLDER")."python_cronjob_running.lock";

        //Removing lock file, so the next cronjob now that he's allowed to start
        if (file_exists($lock_file_location)) {
            unlink($lock_file_location);
        }

        //Check if we got any data back from Python
        if( ! empty( $data ) ) {

            //Loop through $data array
            foreach( $data as $platform => $values ) {

                //Check if we got records
                if( ! empty( $values ) ) {

                    // Loop import and save in db with address
                    foreach( $values as $customerID => $customerData ) {
                        if ($customerData['type'] == 'existing') {
                            //Search for the customer of this row
                            $customer = Customer::leftJoin("mover_data", "cu_id", "moda_cu_id")->where("cu_id", $customerID)->first();

                            //Save country code of customer, needed for splitting address if country code is NL
                            $cu_co_code = $customer->cu_co_code;

                            //Make street, zipcode and city empty, because these variables can be filled of the previous loop
                            //These variables are getting filled after the address is splitted or after the API call to the Geocoding API
                            $street = "";
                            $zipcode = "";
                            $city = "";

                            /**
                             * Start block 'Addresses'
                             *
                             * This Part is to strip the address
                             */
                            //Check if address is not empty. If it is empty, then we got no address back from Google (So probably no Google result)
                            if (!empty($customerData['address'])){
                                //Using the old way of splitting addresses only for records from NL
                                if ($cu_co_code == "NL")
                                {
                                    //Explode address string
                                    $address_details = explode(",", $customerData['address']);

                                    //First part of string is street.
                                    $street = trim($address_details[0]);

                                    //Getting total length of zipcode + city string
                                    $length = strlen($address_details[1]);

                                    //First part of exploded string is zipcode
                                    $zipcode = trim(substr($address_details[1], 0, 8));

                                    //Second part of exploded string is city
                                    $city = trim(substr($address_details[1], 8, $length));
                                }
                                else {
                                    //If country code is not NL, than make a call to Geocoding API to get street, zipcode and city

                                    //Start of URL to Geocoding API
                                    $api_maps = 'https://maps.googleapis.com/maps/';

                                    //Get Google API key from DB
                                    $api_key = System::getSetting("google_key");

                                    //Create whole URL for the API call. Encode address string into the URL and add the API key
                                    $url = $api_maps . 'api/geocode/json?address='.urlencode($customerData['address']).'&key='.$api_key;

                                    //Get content from the URL and save in $result variable
                                    $result = json_decode(file_get_contents($url));

                                    //Check if status is OK, if status is OK, then save street, zipcode and city
                                    if ($result->status == "OK"){
                                        //Make some variables empty. These variables can be filled from the previous loop
                                        $result_city = "";
                                        $result_city_backup = "";
                                        $result_street_name = "";
                                        $result_street_number = "";
                                        $result_zipcode = "";
                                        $result_postal_town = "";

                                        //Loop through all address details we got back from the API call
                                        foreach ($result->results[0]->address_components as $key => $value) {
                                            /**
                                             * Save result of Geocoding API in variables:
                                             * locality = City
                                             * administrative_area_level_1 = Backup city --> If locality is empty, then take this one for City
                                             * street_number = Street number
                                             * route = Street name
                                             * postal_code = Zipcode
                                             */
                                            if ($value->types[0] == "locality" || $value->types[1] == "locality") {
                                                $result_city = $value->long_name;
                                            }
                                            elseif ($value->types[0] == "administrative_area_level_1") { //Hoogste administrator level pakken?
                                                $result_city_backup = $value->short_name;
                                            }
                                            elseif ($value->types[0] == "street_number") {
                                                $result_street_number = $value->short_name;
                                            }
                                            elseif ($value->types[0] == "route") {
                                                $result_street_name = $value->long_name;
                                            }
                                            elseif ($value->types[0] == "postal_code") {
                                                $result_zipcode = $value->long_name;
                                            }
                                            elseif ($value->types[0] == "postal_town") { //Hoogste administrator level pakken?
                                                $result_postal_town = $value->long_name;
                                            }
                                        }

                                        //Check if city is empty, if it is, then take the backup city variable as City
                                        if (empty($result_city)) {
                                            $result_city = $result_city_backup;
                                        }

                                        //Following fix is for United Kingdom addresses. Google give back 'postal town' as city, instead of locality
                                        if ($cu_co_code == "GB") {
                                            if (!empty($result_postal_town)) {
                                                $result_city = $result_postal_town;
                                            }
                                        }

                                        //If country is Australia, Canada, United States, or Mexico, than add short name of State before the zipcode. In these countries it is normal to have short name of state before zipcode
                                        if ($cu_co_code == "AU" || $cu_co_code == "CA" || $cu_co_code == "US" || $cu_co_code == "CN" || $cu_co_code == "MY" || $cu_co_code == "MX") {
                                            $zipcode = trim($result_city_backup)." ".trim($result_zipcode);
                                        }
                                        else {
                                            //Else, just take zipcode and trim whitespaces
                                            $zipcode = trim($result_zipcode);
                                        }

                                        //For country France and United States, add street number before street name, for all the other countries add street number after the street name
                                        $street = (($cu_co_code == "FR" || $cu_co_code == "US" || $cu_co_code == "GB") ? trim($result_street_number)." ".trim($result_street_name) : trim($result_street_name)." ".trim($result_street_number));

                                        //Trimming whitespaces from city
                                        $city = trim($result_city);
                                    }
                                }
                            }
                            /**
                             * End block 'Addresses'
                             */

                            //Strip quotes from description
                            $customerData['description'] = trim($customerData['description'], "'");
                            $customerData['description'] = trim($customerData['description'], '"');

                            //Strip all parameters from the gathered customer website
                            $explode_website = explode("/?", $customerData['website'] ?? '');
                            $customerData['website'] = $explode_website[0] ?? '';


                            /**
                             * Start block 'Website URLs Validation'
                             *
                             * Validating URL of ERP and URL of Google and save the final URLs in DB
                             */
                            $response = null;
                            $variables = "";

                            //Instance of AjaxController; So we can easily run validation functions
                            $ajaxcontroller = new AjaxController();

                            //Define Website Validation type array, array which tells what ID belongs to what message
                            $website_validate_types = WebsiteValidateType::all();

                            //Create a new post request to send it to function in the AjaxController to validate websites
                            $request = new Request();
                            $request->url_erp = $customer->cu_website;
                            $request->url_other = $customerData['website'];

                            //Set starting domains
                            $start_domain_erp = $system->getDomainFromURL($customer->cu_website);
                            $start_domain_other = $system->getDomainFromURL($customerData['website']);

                            //Post the request to validate website URLs function and save data in $response variable
                            $response = json_decode($ajaxcontroller->validateWebsiteURLs($request));

                            //Store final ERP URL and final Google URL in variables, will be saved later in code into customer_review_scores table
                            $final_url_erp = $response->final_erp;
                            $final_url_other = $response->final_other;

                            //Get final domains of ERP and Google URL and save them into variables
                            $final_domain_erp = $system->getDomainFromURL($final_url_erp);
                            $final_domain_other = $system->getDomainFromURL($final_url_other);

                            //Get the right type which belongs to the response string
                            $website_validation_type = $website_validate_types[$response->website_validate_string];

                            //Explode all variables which we got back (all variables are about final url and domain.)
                            $variables = explode(",", $response->variables);

                            /**
                             * End block 'Website URLs Validation'
                             */

                            /**
                             * End block 'Website URLs Validation'
                             */

                            /**
                             * Get the existing record if it exists
                             *
                             * Search in the customer_review_scores table for a record with the Customer ID and Platform; Using variable $customerID & $platform.
                             */
                            $curesc = CustomerReviewScore::where("curesc_cu_id", $customerID)->where("curesc_platform", $platform)->first();

                            /**
                             * Check if Google returned data back or an empty row
                             *
                             * Google row is empty when we didn't receive a company name back
                             */
                            if (empty($customerData['name']) && empty($customerData['website'])) {
                                //This row doesn't have Google data

                                //Check if there is already an existing record for this Customer with this Platform
                                if (count($curesc) == 1) {
                                    //There is already a record for this customer
                                    //Increase skip counter
                                    //Put processed = 0, when this record is already skipped 2 other times (skipcounter >= 3)
                                    //Update timestamp
                                    $curesc->curesc_timestamp = date("Y-m-d H:i:s");
                                    $curesc->curesc_skip_counter = $curesc->curesc_skip_counter + 1;

                                    //Check if skip counter is 3 or higher. If it is, then turn this record back in the validation process
                                    if ($curesc->curesc_skip_counter >= 3) {
                                        $curesc->curesc_processed = 0;
                                    }

                                    //Add record to fetched google history table
                                    self::addToFetchedGoogleHistoryTable($curesc->curesc_id, $curesc->curesc_cu_id, 1, 0, 1,0,0);

                                    //Save record
                                    $curesc->save();
                                }
                                else {
                                    //Create a new row and set skipcounter on 1
                                    //Set processed = 1, so we don't see this empty row in the process. We see only this row when its getting skipped for the third time.
                                    //Save the empty row in DB
                                    $curesc_id = self::createNewGoogleRecordAndGetID($customerID, $customer, $customerData, 1, 1, $platform, $street, $zipcode, $city, false, $final_url_erp, $final_url_other);
                                }
                            }
                            //^ Row doesn't have Google data
                            elseif($customerData['name'] == "See results about" && empty($customerData['website'])){
                                if (count($curesc) == 1) {
                                    $curesc->curesc_timestamp = date("Y-m-d H:i:s");
                                    $curesc->curesc_skip_counter = $curesc->curesc_skip_counter + 1;

                                    //Check if skip counter is 3 or higher. If it is, then turn this record back in the validation process
                                    if ($curesc->curesc_skip_counter >= 3) {
                                        $curesc->curesc_processed = 0;
                                    }

                                    //Add record to fetched google history table
                                    self::addToFetchedGoogleHistoryTable($curesc->curesc_id, $curesc->curesc_cu_id, 1, 0, 1,0,0);

                                    //Save record
                                    $curesc->save();
                                }
                                else {
                                    //Create a new row and set skipcounter on 1
                                    //Set processed = 1, so we don't see this empty row in the process. We see only this row when its getting skipped for the third time.
                                    //Save the empty row in DB
                                    $curesc_id = self::createNewGoogleRecordAndGetID($customerID, $customer, $customerData, 1, 1, $platform, $street, $zipcode, $city, false, $final_url_erp, $final_url_other);
                                }
                            }
                            else {
                                //This row has Google data

                                //Check if there is a final Google Domain of ERP or Google
                                if (!empty($final_domain_erp) || !empty($final_domain_other)) {
                                    if ($final_domain_erp == $final_domain_other) {
                                        //Final ERP domain is the same as the final Google domain.
                                        //Reviews can be automatically accepted

                                        //Check if there is an existing record
                                        if (count($curesc) == 1) {
                                            //Update existing record
                                            $curesc_id = self::updateGoogleRecordAndGetID($curesc->curesc_id, $customer, $customerData, $platform, $street, $zipcode, $city, true, $final_url_erp, $final_url_other);
                                        }
                                        else {
                                            //No record exists, so create one and accept the reviews
                                            $curesc_id = self::createNewGoogleRecordAndGetID($customerID, $customer, $customerData, 0, 0, $platform, $street, $zipcode, $city, true, $final_url_erp, $final_url_other);

                                        }
                                    }
                                    //Final domain ERP or Google is not empty and different from eachother
                                    else {
                                        //Final domain ERP is different from the final domain of Google
                                        if(count($curesc) == 1 && empty($final_domain_other)) {
                                            $curesc_id = self::updateGoogleRecordAndGetID($curesc->curesc_id, $customer, $customerData, $platform, $street, $zipcode, $city, false, $final_url_erp, $final_url_other);
                                        }
                                        //Check if there is an existing record and is NOT processed
                                        elseif (count($curesc) == 1 && $curesc->curesc_processed == 0) {
                                            //Update existing record
                                            $curesc_id = self::updateGoogleRecordAndGetID($curesc->curesc_id, $customer, $customerData, $platform, $street, $zipcode, $city, false, $final_url_erp, $final_url_other);
                                        }
                                        //If there is an existing record and the record is already processed
                                        elseif (count($curesc) == 1 && $curesc->curesc_processed == 1) {
                                            //Get domain from last Google Result
                                            $existing_domain = $system->getDomainFromURL($curesc->curesc_customer_website);

                                            if ($existing_domain == $final_domain_other && !empty($existing_domain) && !empty($final_domain_other)) {
                                                //Same domain as previous time, check if checkbox 'use reviews' was ON when validated and run Update function
                                                $curesc_id = self::updateGoogleRecordAndGetID($curesc->curesc_id, $customer, $customerData, $platform, $street, $zipcode, $city, (($curesc->curesc_use_reviews == 1) ? 1 : 0), $final_url_erp, $final_url_other);
                                            }
                                            elseif ($existing_domain == $final_domain_other) {
                                                //No URLs known for Google and existing record
                                                $curesc_id = self::updateGoogleRecordAndGetID($curesc->curesc_id, $customer, $customerData, $platform, $street, $zipcode, $city, (($curesc->curesc_use_reviews == 1) ? 1 : 0), $final_url_erp, $final_url_other);
                                            }
                                            else {
                                                //Update existing record
                                                $curesc_id = self::updateGoogleRecordAndGetID($curesc->curesc_id, $customer, $customerData, $platform, $street, $zipcode, $city, 0, $final_url_erp, $final_url_other);
                                            }
                                        }
                                        else {

                                            $curesc_id = self::createNewGoogleRecordAndGetID($customerID, $customer, $customerData, 0, 0, $platform, $street, $zipcode, $city, 0, $final_url_erp, $final_url_other);
                                        }
                                    }
                                }
                                else {
                                    //No final ERP and Google domains found

                                    if (count($curesc) == 1) {
                                        //Update row
                                        $curesc_id = self::updateGoogleRecordAndGetID($curesc->curesc_id, $customer, $customerData, $platform, $street, $zipcode, $city, 0, $final_url_erp, $final_url_other);

                                    }
                                    else {
                                        //No record exists, so create one
                                        $curesc_id = self::createNewGoogleRecordAndGetID($customerID, $customer, $customerData, 0, 0, $platform, $street, $zipcode, $city, 0, $final_url_erp, $final_url_other);
                                    }
                                }

                            }

                            /**
                             * Next part is to save the variables of 'Website URL validation' into the record
                             * Set counter default on 1, because column name in database starts at 'curesc_website_validate_var_1' and ends with 'curesc_website_validate_var_6'
                             */
                            //Get most updated version of the record
                            $curesc_row_created_or_updated = CustomerReviewScore::where("curesc_cu_id", $customerID)->first();

                            if (count($curesc_row_created_or_updated) == 1) {
                                //Set the right type into DB (This will trigger the message in validation process)
                                $curesc_row_created_or_updated->curesc_website_validate_type = $website_validation_type;

                                $count = 1;

                                $curesc_row_created_or_updated->curesc_website_validate_var_1 = null;
                                $curesc_row_created_or_updated->curesc_website_validate_var_2 = null;
                                $curesc_row_created_or_updated->curesc_website_validate_var_3 = null;
                                $curesc_row_created_or_updated->curesc_website_validate_var_4 = null;
                                $curesc_row_created_or_updated->curesc_website_validate_var_5 = null;
                                $curesc_row_created_or_updated->curesc_website_validate_var_6 = null;

                                //Loop through all variables and save into DB
                                foreach ($variables as $var) {
                                    $curesc_row_created_or_updated->{"curesc_website_validate_var_".$count} = $var;
                                    $count++;
                                }

                                //Save all variables and type column(s)
                                $curesc_row_created_or_updated->save();
                            }

                            /**
                             * Check if its possible to automatically process this record
                             *
                             * $response = Message which belongs to this record after website validation
                             */

                            self::checkAutomaticallyProcessing($curesc_id, $response);
                        }
                        else {
                            //Potential new customer
                            $gathered_company = GatheredCompany::where("gaco_id", $customerID)->first();
                            //Save country code of customer, needed for splitting address if country code is NL
                            $cu_co_code = $gathered_company->gaco_searched_co_code;

                            //Make street, zipcode and city empty, because these variables can be filled of the previous loop
                            //These variables are getting filled after the address is splitted or after the API call to the Geocoding API
                            $street = "";
                            $zipcode = "";
                            $city = "";

                            /**
                             * Start block 'Addresses'
                             *
                             * This Part is to strip the address
                             */
                            //Check if address is not empty. If it is empty, then we got no address back from Google (So probably no Google result)
                            if (!empty($customerData['address']) && trim($customerData['address']) != trim($customerData['telephone'])){
                                //If country code is not NL, than make a call to Geocoding API to get street, zipcode and city

                                //Start of URL to Geocoding API
                                $api_maps = 'https://maps.googleapis.com/maps/';

                                //Get Google API key from DB
                                $api_key = System::getSetting("google_key");

                                //Create whole URL for the API call. Encode address string into the URL and add the API key
                                $url = $api_maps . 'api/geocode/json?address='.urlencode($customerData['address']).'&key='.$api_key;

                                //Get content from the URL and save in $result variable
                                $result = json_decode(file_get_contents($url));

                                //Check if status is OK, if status is OK, then save street, zipcode and city
                                if ($result->status == "OK"){
                                    //Make some variables empty. These variables can be filled from the previous loop
                                    $result_city = "";
                                    $result_city_backup = "";
                                    $result_street_name = "";
                                    $result_street_number = "";
                                    $result_zipcode = "";
                                    $result_postal_town = "";

                                    //Loop through all address details we got back from the API call
                                    foreach ($result->results[0]->address_components as $key => $value) {
                                        /**
                                         * Save result of Geocoding API in variables:
                                         * locality = City
                                         * administrative_area_level_1 = Backup city --> If locality is empty, then take this one for City
                                         * street_number = Street number
                                         * route = Street name
                                         * postal_code = Zipcode
                                         */
                                        if ($value->types[0] == "locality" || $value->types[1] == "locality") {
                                            $result_city = $value->long_name;
                                        }
                                        elseif ($value->types[0] == "administrative_area_level_1") { //Hoogste administrator level pakken?
                                            $result_city_backup = $value->short_name;
                                        }
                                        elseif ($value->types[0] == "street_number") {
                                            $result_street_number = $value->short_name;
                                        }
                                        elseif ($value->types[0] == "route") {
                                            $result_street_name = $value->long_name;
                                        }
                                        elseif ($value->types[0] == "postal_code") {
                                            $result_zipcode = $value->long_name;
                                        }
                                        elseif ($value->types[0] == "postal_town") { //Hoogste administrator level pakken?
                                            $result_postal_town = $value->long_name;
                                        }
                                    }

                                    //Check if city is empty, if it is, then take the backup city variable as City
                                    if (empty($result_city)) {
                                        $result_city = $result_city_backup;
                                    }

                                    //Following fix is for United Kingdom addresses. Google give back 'postal town' as city, instead of locality
                                    if ($cu_co_code == "GB") {
                                        if (!empty($result_postal_town)) {
                                            $result_city = $result_postal_town;
                                        }
                                    }

                                    //If country is Australia, Canada, United States, or Mexico, than add short name of State before the zipcode. In these countries it is normal to have short name of state before zipcode
                                    if ($cu_co_code == "AU" || $cu_co_code == "CA" || $cu_co_code == "US" || $cu_co_code == "CN" || $cu_co_code == "MY" || $cu_co_code == "MX") {
                                        $zipcode = trim($result_city_backup)." ".trim($result_zipcode);
                                    }
                                    else {
                                        //Else, just take zipcode and trim whitespaces
                                        $zipcode = trim($result_zipcode);
                                    }

                                    //For country France and United States, add street number before street name, for all the other countries add street number after the street name
                                    $street = (($cu_co_code == "FR" || $cu_co_code == "US" || $cu_co_code == "GB") ? trim($result_street_number)." ".trim($result_street_name) : trim($result_street_name)." ".trim($result_street_number));

                                    //Trimming whitespaces from city
                                    $city = trim($result_city);
                                }
                            }
                            /**
                             * End block 'Addresses'
                             */

                            //Check if website of customer already exists in our ERP
                            //Strip all parameters from the gathered customer website
                            $explode_website = explode("/?", $customerData['website'] ?? '');
                            $customerData['website'] = $explode_website[0] ?? '';

                            //Strip quotes from description
                            $customerData['description'] = trim($customerData['description'], "'");
                            $customerData['description'] = trim($customerData['description'], '"');

                            /**
                             * Website validation
                             */

                            //Get ERP domain from URL
                            $domain_current = $system->getDomainFromURL($customerData['website']);

                            //Open URL
                            $ch = curl_init($customerData['website']);
                            curl_setopt($ch, CURLOPT_HEADER, false);
                            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
                            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
                            $html = curl_exec($ch);

                            //URL AFTER REDIRECT; This will be the final URL for ERP
                            $redirectURL = trim(curl_getinfo($ch, CURLINFO_EFFECTIVE_URL), "/");

                            curl_close($ch);

                            //Status must be OK
                            if ($html != false)
                            {

                                //Check if there are any redirects in the ERP url
                                if (trim($customerData['website'], "/") == $redirectURL)
                                {
                                    $website_validation_html = '<div class="col-sm-6" style="color:green;">Current URL is the best one</div>';
                                } else
                                {
                                    //Redirect found. Get domain name after redirect
                                    $domain_erp_after_redirect = $system->getDomainFromURL($redirectURL);

                                    //Check if domain is the same as after the redirect
                                    if ($domain_current == $domain_erp_after_redirect)
                                    {
                                        //Same domain
                                        $website_validation_html = '<div class="col-sm-6">There is a redirect in the current URL. The best URL is: ' . $redirectURL . '<span id="validated_url" data-url="' . $redirectURL . '">✍</span></div>';
                                    } else
                                    {
                                        //Another URL
                                        $website_validation_html = '<div class="col-sm-6">There is found a redirect to another domain. Redirected URL is: ' . $redirectURL . '<span id="validated_url" data-url="' . $redirectURL . '">✍</span></div>';
                                    }
                                }
                            } else
                            {
                                $website_validation_html = '<div class="col-sm-6" style="color:red;">Current URL is not working!</div>';
                            }

                            $domain_website = $system->getDomainFromURL($customerData['website']);
                            $domain_website_redirected = $system->getDomainFromURL($redirectURL);

                            $search_for_website = Customer::where("cu_website", "like", "%".$domain_website."%")->first();
                            $search_for_website_redirected = Customer::where("cu_website", "like", "%".$domain_website_redirected."%")->first();

                            if ((count($search_for_website) > 0 || count($search_for_website_redirected) > 0) && !empty($customerData['website'])) {
                                //Customer found in our ERP with this website domain
                                $curesc_id = self::newCustomerCreateNewGoogleRecordAndGetID(null, $gathered_company, $customerData, 0, 4, $platform, $street, $zipcode, $city, $cu_co_code, $website_validation_html);
                            }
                            else {
                                //No customer found in our ERP with this website domain
                                if (empty($customerData['name']) && empty($customerData['website'])) {
                                    //No google data, so type 3
                                    $curesc_id = self::newCustomerCreateNewGoogleRecordAndGetID(null, $gathered_company, $customerData, 0, 3, $platform, $street, $zipcode, $city, $cu_co_code, $website_validation_html);
                                }
                                elseif($customerData['name'] == "See results about" && empty($customerData['website'])) {
                                    //No google data found, so type 3
                                    $curesc_id = self::newCustomerCreateNewGoogleRecordAndGetID(null, $gathered_company, $customerData, 0, 3, $platform, $street, $zipcode, $city, $cu_co_code, $website_validation_html);
                                }
                                else {
                                    //Google data found, so type 2: Potential new customer
                                    $curesc_id = self::newCustomerCreateNewGoogleRecordAndGetID(null, $gathered_company, $customerData, 0, 2, $platform, $street, $zipcode, $city, $cu_co_code, $website_validation_html);
                                }
                            }

                            $gathered_company->gaco_processed = 1;
                            $gathered_company->save();
                        }
                    }
                }
                else {
                    if (strtolower($platform) == "google"){
                        //Empty array from Python; Which means nothing scraped.
                        $finance_users = array_keys(DataIndex::financeUsers());

                        System::sendMessage($finance_users, "Python - No data received", "We didn't receive gathered data back from Python (Google validation). Please contact IT!");
                    }
                }
            }
        }

        //Return success
        return 'success';
    }

    public function gatherCustomersForPython() {

        // Default variables
        $data = [];
        $googleAmount = 10;
        $lock_file_location = env("SHARED_FOLDER")."python_cronjob_running.lock";

        //Check if lock file exists
        if (file_exists($lock_file_location)){
            //Script is already running

            //Set fields, since when does this lock file exists
            $fields = [
                'running_since' => date("Y-m-d H:i:s", filemtime($lock_file_location))
            ];

            //Send email
	        Mail::send("python_already_running", "EN", $fields);

            //Return empty json string
            echo json_encode($data);
        }
        else {

            foreach( ['google' => $googleAmount] as $subject => $amount ) {
                //Get X new customers, based on amount
                $customers = Customer::leftJoin( 'customer_review_scores', 'cu_id', '=', 'curesc_cu_id' )
                    ->leftJoin("mover_data", "moda_cu_id", "cu_id")
                    ->where("moda_not_operational", 0)
                    ->where( 'cu_deleted', '=', '0' )
                    ->whereRaw( '`curesc_cu_id` IS NULL' )
                    ->where("cu_type", 1)
                    ->limit( $amount )
                    ->orderByRaw('FIELD(cu_co_code, "NL", "DE", "FR", "ES", "US")')
                    ->get();

                //If there are no new customers anymore, than find rows to scan again
                if(count($customers) == 0) {
                    //Find existing rows to update, based on oldest timestamp
                    $customers = Customer::leftJoin( 'customer_review_scores', 'cu_id', '=', 'curesc_cu_id' )
                        ->leftJoin("mover_data", "cu_id", "moda_cu_id")
                        ->where("moda_not_operational", 0)
                        ->where( 'cu_deleted', '=', '0' )
                        ->where("cu_type", 1)
                        ->where( 'curesc_platform', $subject )
                        ->where("curesc_type", 1)
                        ->where("curesc_customer_deleted", 0)
                        ->whereNull("curesc_wait_timestamp")
                        ->orderBy( 'curesc_timestamp' )
                        ->limit( $amount )
                        ->get();
                }

                //Loop through found customers in one of above queries
                foreach( $customers as $cr ) {
                    //Search for langauge which belongs to this customer
                    $lang = Language::where("la_code", $cr->cu_co_code)->first();

                    if (count($lang) > 0) {
                        //Get locale (nl-NL, en-GB etc.)
                        $lang = $lang->la_locale;
                    }
                    else {
                        //If no language is connected to this customer, than take EN as default
                        $lang = Language::where("la_code", "EN")->first()->la_locale;
                    }

                    $google_search_url = urlencode( $cr->cu_company_name_business.' '.$cr->cu_city );

                    if (!empty($cr->cu_custom_google_search_url)) {
                        $google_search_url = $cr->cu_custom_google_search_url;
                    }

                    //Save everything in $data array
                    $data[$subject][$cr->cu_id] =  [
                        'url' => $google_search_url,
                        'language' => $lang,
                        //'delay' => 1,
                        'delay' => rand(60,120),
                        'type' => 'existing'
                    ];
                }

                /**
                 * This part is for searching 5 potential new (related) companies
                 */

                //Get all whitelisted countries, loop through them and save in variable
                $whitelisted_countries_query = GatheredCompanyCountryWhitelisted::all();
                $whitelisted_countries = [];

                foreach ($whitelisted_countries_query as $country_row) {
                    $whitelisted_countries[] = $country_row->gacocowh_co_code;
                }

                $new_companies = GatheredCompany::join("gathered_companies_list", function($join) {
                        $join->on("gaco_title", "=", "gacoli_title");
                        $join->on("gaco_searched_co_code", "=", "gacoli_co_code");
                    })
                    ->whereIn("gaco_searched_co_code", $whitelisted_countries)
                    ->where("gacoli_type", GatheredCompaniesType::WHITELIST)
                    ->where("gaco_processed", 0)
                    ->limit(5)
                    ->get();

                if (count($new_companies) > 0) {
                    foreach ($new_companies as $nc) {
                        $lang = Language::where("la_code", $nc->gaco_searched_co_code)->first();

                        if (count($lang) > 0) {
                            //Get locale (nl-NL, en-GB etc.)
                            $lang = $lang->la_locale;
                        }
                        else {
                            //If no language is connected to this customer, than take EN as default
                            $lang = Language::where("la_code", "EN")->first()->la_locale;
                        }

                        //Save everything in $data array
                        $data[$subject][$nc->gaco_id] =  [
                            'url' => urlencode( $nc->gaco_name.' '.$nc->gaco_searched_city ),
                            'language' => $lang,
                            //'delay' => 1,
                            'delay' => rand(60,120),
                            'type' => 'new'
                        ];
                    }
                }
            }

            //Put all data in JSON format
            $json_data = json_encode($data);

            //Create Lock file so we can't run this script more than once
            $lockfile = fopen($lock_file_location, "w");
            fwrite($lockfile, $json_data);
            fclose($lockfile);

            //echo json encoded data so Python can read it
            echo $json_data;
        }
    }

    public function postTest() {
        echo "HI";
        return "HI";
    }

    public function userExport(){

        $whitelist = [
            '194.153.99.10',
            '2a0f:f340:2000:100::4',
        ];

        if (!in_array($_SERVER['REMOTE_ADDR'], $whitelist)) {
            //exit;
        }


        $users = [];

        $user_query = User::select("email", "password", "us_authenticator_secret")
            ->where("us_is_deleted", 0)
            ->where("us_export_vpn", 1)
            ->get();

        foreach($user_query as $user){
            $users[] = [
                "user" => $user->email,
                "password" => $user->password,
                "totp" => $user->us_authenticator_secret,
            ];
        }


        $fd = fopen('php://memory','w');
        foreach($users as $user) {
            fputcsv($fd, $user, ';');
        }
        rewind($fd);
        echo self::encrypt(stream_get_contents($fd));



    }

    private function encrypt($data) {
        $pubKeyStr = <<<KEY
-----BEGIN PUBLIC KEY-----
MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAs2ebHkRZFCJhr96icPIy
oobA4FByfvkiSxYkCdj8lVQ2g1GQN0RN1eveg7+WrBCRVqGVzGzWTNxVMsaspDyU
O02e2Iey8YTxmy2X8JiRoSVvqPJZKac+L5hjb2I0XQbB6wwE/F6lwghftE+gQRMX
Qmtg7uCVCFeGr6NqEm7RyggE2oZ1lp07a6o9Kg4xYfVoolnRpHkq2HMKcTrCqT3C
DQx7oWGYn7euLDEHe/edjbi6s6e3Sux3XiLIaPMrsq/o0Ki+3kmf8cSTc16Maqps
tZv6+l2j4Q8Otj3fz5g494QuRJF0l+Dtl48EcRLGaFPLMoBABvfqX1GCj8U2Mb68
OD5Wpol8qxXwuwVSEmXFwkm365Rjc4hV1Br0eah6iMr//+WqJ5JjY5ZKqXUjfhsN
89CjFG4AAmY2E7EG8iED8xmnU0wK2qEXdaP8jGhUoyYXoonfa8JYC0R51EFZsynj
lzLF8stF2chs0YsRVLLrLf2ByDHYUT191uZ/QpHssXc+FirVN+s1RIuX6+7U6M7I
AMxL56fGrCDCK9x+/2l+xQGcIf7dTakrGF9Bu68pdObWQ82PnLcjLRwFtAVqvnzu
lQZz1CygB7GTauzHSqKEJk7QZ8dVPUsbG3NcbNLrQB7oBuL7XykliEU/Sl/CWgC1
HeBa4NmTX1kE70GG/m8RkAkCAwEAAQ==
-----END PUBLIC KEY-----
KEY;

        $sealedData = null;
        $keys = [ $pubKeyStr ];
        $cipher_method = 'aes-256-ctr';
        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length($cipher_method));

        if (false === openssl_seal($data, $sealedData, $ekeys, $keys, $cipher_method, $iv)) {
            throw new \Exception('could not encrypt data');
        }

        return base64_encode($iv) .'::' . base64_encode($ekeys[0]) .'::'.base64_encode($sealedData);
    }

    /**
     * @param             $customerData
     * @param string|null $cu_co_code
     *
     * @return array
     */
    private function stripAddressDetails( $customerData, $cu_co_code ): array {

        //Check if address is not empty. If it is empty, then we got no address back from Google (So probably no Google result)
        if( ! empty( $customerData['address'] ) ) {
            //Using the old way of splitting addresses only for records from NL
            if( $cu_co_code == "NL" ) {
                //Explode address string
                $address_details = explode( ",", $customerData['address'] );

                //First part of string is street.
                $street = trim( $address_details[0] );

                //Getting total length of zipcode + city string
                $length = strlen( $address_details[1] );

                //First part of exploded string is zipcode
                $zipcode = trim( substr( $address_details[1], 0, 8 ) );

                //Second part of exploded string is city
                $city = trim( substr( $address_details[1], 8, $length ) );
            }
            else {
                //If country code is not NL, than make a call to Geocoding API to get street, zipcode and city

                //Start of URL to Geocoding API
                $api_maps = 'https://maps.googleapis.com/maps/';

                //Get Google API key from DB
                $api_key = System::getSetting( "google_key" );

                //Create whole URL for the API call. Encode address string into the URL and add the API key
                $url = $api_maps . 'api/geocode/json?address=' . urlencode( $customerData['address'] ) . '&key=' . $api_key;

                //Get content from the URL and save in $result variable
                $result = json_decode( file_get_contents( $url ) );

                //Check if status is OK, if status is OK, then save street, zipcode and city
                if( $result->status == "OK" ) {
                    //Make some variables empty. These variables can be filled from the previous loop
                    $result_city = "";
                    $result_city_backup = "";
                    $result_street_name = "";
                    $result_street_number = "";
                    $result_zipcode = "";
                    $result_postal_town = "";

                    //Loop through all address details we got back from the API call
                    foreach( $result->results[0]->address_components as $key => $value ) {
                        /**
                         * Save result of Geocoding API in variables:
                         * locality = City
                         * administrative_area_level_1 = Backup city --> If locality is empty, then take this one for City
                         * street_number = Street number
                         * route = Street name
                         * postal_code = Zipcode
                         */
                        if( $value->types[0] == "locality" || $value->types[1] == "locality" ) {
                            $result_city = $value->long_name;
                        }
                        elseif( $value->types[0] == "administrative_area_level_1" ) { //Hoogste administrator level pakken?
                            $result_city_backup = $value->short_name;
                        }
                        elseif( $value->types[0] == "street_number" ) {
                            $result_street_number = $value->short_name;
                        }
                        elseif( $value->types[0] == "route" ) {
                            $result_street_name = $value->long_name;
                        }
                        elseif( $value->types[0] == "postal_code" ) {
                            $result_zipcode = $value->long_name;
                        }
                        elseif( $value->types[0] == "postal_town" ) { //Hoogste administrator level pakken?
                            $result_postal_town = $value->long_name;
                        }
                    }

                    //Check if city is empty, if it is, then take the backup city variable as City
                    if( empty( $result_city ) ) {
                        $result_city = $result_city_backup;
                    }

                    //Following fix is for United Kingdom addresses. Google give back 'postal town' as city, instead of locality
                    if( $cu_co_code == "GB" ) {
                        if( ! empty( $result_postal_town ) ) {
                            $result_city = $result_postal_town;
                        }
                    }

                    //If country is Australia, Canada, United States, or Mexico, than add short name of State before the zipcode. In these countries it is normal to have short name of state before zipcode
                    if( $cu_co_code == "AU" || $cu_co_code == "CA" || $cu_co_code == "US" || $cu_co_code == "CN" || $cu_co_code == "MY" || $cu_co_code == "MX" ) {
                        $zipcode = trim( $result_city_backup ) . " " . trim( $result_zipcode );
                    }
                    else {
                        //Else, just take zipcode and trim whitespaces
                        $zipcode = trim( $result_zipcode );
                    }

                    //For country France and United States, add street number before street name, for all the other countries add street number after the street name
                    $street = ( ( $cu_co_code == "FR" || $cu_co_code == "US" || $cu_co_code == "GB" ) ? trim( $result_street_number ) . " " . trim( $result_street_name ) : trim( $result_street_name ) . " " . trim( $result_street_number ) );

                    //Trimming whitespaces from city
                    $city = trim( $result_city );
                }
            }
        }

        return [$street, $zipcode, $city];
    }/**
 * @param \Illuminate\Database\Eloquent\Model|null $customer
 * @param Request                                  $request
 * @param                                          $customerData
 * @param System                                   $system
 *
 * @return array
 */
    private function websiteValidationCheck( ?\Illuminate\Database\Eloquent\Model $customer, Request $request, $customerData, System $system ): array {

//Instance of AjaxController; So we can easily run validation functions
        $ajaxcontroller = new AjaxController();

        //Define Website Validation type array, array which tells what ID belongs to what message
        $website_validate_types = WebsiteValidateType::all();

        //Create a new post request to send it to function in the AjaxController to validate websites
        $request = new Request();
        $request->url_erp = $customer->cu_website;
        $request->url_other = $customerData;

        //Post the request to validate website URLs function and save data in $response variable
        $response = json_decode( $ajaxcontroller->validateWebsiteURLs( $request ) );

        //Store final ERP URL and final Google URL in variables, will be saved later in code into customer_review_scores table
        $final_url_erp = $response->final_erp;
        $final_url_other = $response->final_other;

        //Get final domains of ERP and Google URL and save them into variables
        $final_domain_erp = $system->getDomainFromURL( $final_url_erp );
        $final_domain_other = $system->getDomainFromURL( $final_url_other );

        //Get the right type which belongs to the response string
        $website_validation_type = $website_validate_types[$response->website_validate_string];

        //Explode all variables which we got back (all variables are about final url and domain.)
        $variables = explode( ",", $response->variables );

        return [$response, $final_url_erp, $final_url_other, $final_domain_erp, $final_domain_other, $website_validation_type, $variables];
    }


}
