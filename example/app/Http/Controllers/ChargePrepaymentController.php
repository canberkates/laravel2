<?php

namespace App\Http\Controllers;

use App\Data\AdyenPaymentStatus;
use App\Functions\AdyenTransaction;
use App\Functions\DataIndex;
use App\Functions\System;
use App\Models\AdyenPayment;
use App\Models\Customer;
use DB;
use Illuminate\Http\Request;
use Log;

class ChargePrepaymentController extends Controller
{
	public function index()
	{
        $query = DB::table("customers")
            ->select("cu_id", "cu_company_name_business")
            ->where("cu_deleted", 0)
            ->get();
        
        foreach($query as $result)
        {
            $customers[$result->cu_id] = $result->cu_company_name_business;
        }
        
        return view('finance.chargeprepayment', [
            'customers' => $customers,
            'index' => 1
		]);
	}
    
    public function chargeConfirmation(Request $request)
    {
        $request->validate([
            'customer' => 'required',
            'creditcard' => 'required',
            'amount' => 'required'
        ]);
        
        $system = new System();
        
        Log::debug($request);
        
        $adyen = DB::table("adyen_card_details")
            ->select("adcade_card_last_digits", "adcade_card_expiry_month", "adcade_card_expiry_year", "cu_company_name_business", "cu_pacu_code")
            ->leftJoin("customers","cu_id","adcade_cu_id")
            ->where("cu_id", $request->customer)
            ->where("adcade_id", $request->creditcard)
            ->first();
        
        if(!$adyen)
        {
            return redirect()->back()->withErrors([ 'error' =>  "Customer / card not found!"]);
        }
    
        $rate = str_replace(",", ".", $request->amount);
    
        if($rate < 0)
        {
            $rate = $rate * -1;
        }
    
        $amount = $system->paymentCurrencyToken($adyen->cu_pacu_code)." ".System::numberFormat($rate, 2);
    
        $results['amount'] = $amount;
        $results['rate'] = $rate;
        $results['customer'] = $adyen->cu_company_name_business;
        $results['cu_id'] = $request->customer;
        $results['adcade_id'] = $request->creditcard;
        $results['lastdigits'] = $adyen->adcade_card_last_digits;
        $results['expiryyear'] = $adyen->adcade_card_expiry_year;
        $results['expirymonth'] = $adyen->adcade_card_expiry_month;
    
    
        return view('finance.chargeprepayment', [
            'index' => 0,
            'results' => $results
        ]);
    }
    
    public function charge(Request $request){
	    
        $result = AdyenTransaction::insertCreditCardPayment($request->adcade_id, $request->amount, "CCCHARGE");
        AdyenTransaction::makeCreditCardPayment($result['adcade'], $result['adpa_id'], $result['adpa_merchant_reference'], $request->amount);
    
        $card_details = AdyenPayment::where("adpa_id", $result['adpa_id'])->first();
    
        $status = AdyenPaymentStatus::name($card_details->adpa_status);
        if($card_details->adpa_status != AdyenPaymentStatus::PROCESSING)
        {
            $customer = Customer::where("cu_id", $request->cu_id)->first();
            $customer->cu_pp_charge_credit_card_times = 0;
            $customer->save();
            
            return redirect()->back()->with('message', "Payment success | Status: {$status} | Result code: {$card_details->adpa_result_code}");
        }
        else
        {
            return redirect()->back()->with('message', "Payment failed | Status: {$status} | Result code: {$card_details->adpa_result_code} | Refusal reason: {$card_details->adpa_refusal_reason}");
        }
    
    }
   
}
