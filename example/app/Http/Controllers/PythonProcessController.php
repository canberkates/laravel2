<?php

namespace App\Http\Controllers;

use App\Data\CustomerMarketType;
use App\Data\CustomerPairStatus;
use App\Data\CustomerServices;
use App\Data\CustomerStatusType;
use App\Data\CustomerType;
use App\Data\DebtorStatus;
use App\Data\PaymentMethod;
use App\Data\PaymentReminderStatus;
use App\Data\RequestType;
use App\Data\WebsiteValidateString;
use App\Functions\Data;
use App\Functions\Mail;
use App\Functions\System;
use App\Models\Country;
use App\Models\Customer;
use App\Models\CustomerGatheredReview;
use App\Models\CustomerRemark;
use App\Models\CustomerReviewScore;
use App\Models\GatheredCompany;
use App\Models\GatheredCompanyCountryWhitelisted;
use App\Models\KTCustomerGatheredReview;
use App\Models\KTCustomerMembership;
use App\Models\KTRequestCustomerPortal;
use App\Models\Language;
use App\Models\LedgerAccount;
use App\Models\Membership;
use App\Models\MobilityexFetchedData;
use App\Models\MoverData;
use App\Models\PaymentCurrency;
use App\Models\Portal;
use App\Models\Region;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Intervention\Image\Facades\Image;

class PythonProcessController extends Controller
{

    public function index() {
        $count_google = CustomerReviewScore::where("curesc_processed", 0)
            ->where("curesc_type", 1)
            ->get()->count();

        $count_related = CustomerReviewScore::where("curesc_processed", 0)
            ->where("curesc_type", 2)
            ->get()->count();

        $count_mobilityex = MobilityexFetchedData::where("mofeda_processed", 0)
            ->whereIn("mofeda_type", [1,2,3])
            ->get()->count();

        return view('python_process.index', [
            'count_google' => $count_google,
            'count_related' => $count_related,
            'count_mobilityex' => $count_mobilityex,
        ]);
    }

    public function processFiltered($co_code, $show_all_results = true, $status = 0)
    {

        $curesc = CustomerReviewScore::leftJoin("customers", "cu_id", "curesc_cu_id")->leftJoin("mover_data", "moda_cu_id", "cu_id")->where("curesc_processed", 0)->where("curesc_skip", 0)->whereNull("curesc_lock_user_id")->where("curesc_type", 1);

        if (!empty($co_code)) {
            $curesc = $curesc->where("cu_co_code", $co_code);
        }

        if (!$show_all_results) {
            $curesc = $curesc->where("curesc_name","!=", "");
        }

        if ($status == 1) {
            $curesc = $curesc->where("moda_crm_status",1);
        }
        elseif($status == 2) {
            $curesc = $curesc->where("moda_crm_status","!=", 1);
        }

        $curesc = $curesc->orderBy("curesc_timestamp", "ASC")->first();

        if (count($curesc) == 0) {
            return Redirect::back()->withErrors(['There is no data to validate for '.Data::country($co_code, "EN")." with ".(($show_all_results) ? "showing all results" : "showing results with found data").(($status != 0) ? (($status == 1) ? " and only partners" : " and only NON-partners") : "")]);
        }

        $curesc_left = CustomerReviewScore::selectRaw("COUNT(*) as `count`")->leftJoin("customers", "cu_id", "curesc_cu_id")->leftJoin("mover_data", "cu_id", "moda_cu_id")->where("curesc_processed", 0)->where("curesc_skip", 0)->whereNull("curesc_lock_user_id")->where("curesc_type", 1);

        if (!empty($co_code)) {
            $curesc_left = $curesc_left->where("cu_co_code", $co_code);
        }

        if (!$show_all_results) {
            $curesc_left = $curesc_left->where("curesc_name","!=", "");
        }

        if ($status == 1) {
            $curesc_left = $curesc_left->where("moda_crm_status",1);
        }
        elseif($status == 2) {
            $curesc_left = $curesc_left->where("moda_crm_status","!=", 1);
        }

        $curesc_left = $curesc_left->first()->count;

        $notices = [];
        $warnings = [];

        $curesc_finder = CustomerReviewScore::leftJoin("customers", "cu_id", "curesc_cu_id")->where("curesc_type", 1)->where("curesc_customer_website", $curesc->curesc_customer_website)->where("curesc_customer_website", "!=", "")->where("curesc_id", "!=", $curesc->curesc_id)->where("cu_deleted", 0)->get();
        $website_ERP_finder = Customer::where("cu_website", $curesc->curesc_customer_website)->where("cu_website", "!=", "")->where("cu_id", "!=", $curesc->curesc_cu_id)->where("cu_deleted", 0)->get();
        if (count($curesc_finder) > 0 || count($website_ERP_finder) > 0) {
            $notices['Website'] = "The website URL is not unique. We got the following customer(s) with the same URL: ";

            $count = 0;
            if (count($curesc_finder) > 0) {
                foreach($curesc_finder as $cf) {
                    $count++;
                    $notices['Website'] .= (($count > 1) ? ", " : "")."<a target='_blank' href='/validate_skipped_data/".$cf->curesc_id."/validate'>".$cf->cu_company_name_business." (".$cf->curesc_cu_id.")</a>";
                }
            }

            if (count($website_ERP_finder) > 0) {
                foreach($website_ERP_finder as $wef) {
                    $count++;
                    $notices['Website'] .= (($count > 1) ? ", " : "")."<a target='_blank' href='/customers/".$wef->cu_id."/edit'>ERP - ".$wef->cu_company_name_business." (".$wef->cu_id.")</a>";
                }
            }
        }

        if (empty($curesc->curesc_address) && empty($curesc->curesc_customer_website))
        {
            $notices[''] = "There is no unique result found on ".ucfirst($curesc->curesc_platform)." for this customer.";
        }

        if ($curesc->moda_disable_sirelo_export) {
            $warnings['Sirelo export disabled'] = 'This company is disabled from the export to Sirelo!';
        }

        if ($curesc->moda_not_operational) {
            $warnings['Not operational'] = 'This company is not operational anymore!';
        }

        $search_url = 'https://www.google.com/search?q='.urlencode( $curesc->cu_company_name_business.' '.$curesc->cu_city );

        $system = new System();

        $domain_erp = str_replace("www.", "", $system->getDomainFromURL($curesc->cu_website));
        $domain_other = str_replace("www.", "", $system->getDomainFromURL($curesc->curesc_customer_website));

        if (!empty($domain_erp) && !empty($domain_other) && $domain_erp != $domain_other)
        {
            $notices['Other domains'] = "The ERP and Google URL have different domains. Please check if its the same company!";
        }

        $coc_link = null;

        if($curesc->cu_co_code == "NL") {
            if (!empty($curesc->cu_coc)) {
                $coc_link = "https://www.kvk.nl/zoeken/handelsregister/?handelsnaam=&kvknummer=".$curesc->cu_coc."&straat=&postcode=&huisnummer=&plaats=&hoofdvestiging=1&rechtspersoon=1&nevenvestiging=1&zoekvervallen=0&zoekuitgeschreven=1&start=0";
            }
            else {
                $address = $curesc->cu_street_1;

                // Find a match and store it in $result.
                if ( preg_match('/([^\d]+)\s?(.+)/i', $address, $result) )
                {
                    $streetName = $result[1];
                    $streetNumber = $result[2];

                    $coc_link = "https://www.kvk.nl/zoeken/handelsregister/?handelsnaam=&kvknummer=&straat=".$streetName."&postcode=&huisnummer=".$streetNumber."&plaats=".$curesc->cu_city."&hoofdvestiging=1&rechtspersoon=1&nevenvestiging=1&zoekvervallen=0&zoekuitgeschreven=1&start=0";
                }
            }
        }

        //Check if this customer was partner (or is partner)
        $did_we_send_leads_last_3_years = false;

        $ktrecupo = KTRequestCustomerPortal::where("ktrecupo_cu_id", $curesc->cu_id)->whereRaw("`ktrecupo_timestamp` BETWEEN NOW() - INTERVAL 3 YEAR AND NOW()")->where("ktrecupo_type", 1)->first();
        if (count($ktrecupo) > 0)
        {
            $did_we_send_leads_last_3_years = true;
        }

        $reviews = [];
        $reviews_of_customer = CustomerGatheredReview::where("cugare_cu_id", $curesc->cu_id)->where("cugare_accepted", 0)->get();
        foreach ($reviews_of_customer as $row_reviews){
            $reviews[$row_reviews->cugare_platform] = [
                'score' => $row_reviews->cugare_score,
                'amount' => $row_reviews->cugare_amount,
                'url' => $row_reviews->cugare_url,
            ];
        }

        $website_validate_message = null;

        $website_validate_strings = WebsiteValidateString::all();

        if (!empty($curesc->curesc_website_validate_type)) {
            $var_counter = 0;
            for ($x = 1; $x <= 6; $x++) {
                if(!empty($curesc->{"curesc_website_validate_var_".$x})) {
                    $var_counter++;
                }
            }

            if ($var_counter == 1) {
                $website_validate_message = sprintf($website_validate_strings[$curesc->curesc_website_validate_type], $curesc->curesc_website_validate_var_1);
            }
            elseif($var_counter == 2) {
                $website_validate_message = sprintf($website_validate_strings[$curesc->curesc_website_validate_type], $curesc->curesc_website_validate_var_1, $curesc->curesc_website_validate_var_2);
            }
            elseif($var_counter == 3) {
                $website_validate_message = sprintf($website_validate_strings[$curesc->curesc_website_validate_type], $curesc->curesc_website_validate_var_1, $curesc->curesc_website_validate_var_2, $curesc->curesc_website_validate_var_3);
            }
            elseif($var_counter == 4) {
                $website_validate_message = sprintf($website_validate_strings[$curesc->curesc_website_validate_type], $curesc->curesc_website_validate_var_1, $curesc->curesc_website_validate_var_2, $curesc->curesc_website_validate_var_3, $curesc->curesc_website_validate_var_4);
            }
            elseif($var_counter == 5) {
                $website_validate_message = sprintf($website_validate_strings[$curesc->curesc_website_validate_type], $curesc->curesc_website_validate_var_1, $curesc->curesc_website_validate_var_2, $curesc->curesc_website_validate_var_3, $curesc->curesc_website_validate_var_4, $curesc->curesc_website_validate_var_5);
            }
            elseif($var_counter == 6) {
                $website_validate_message = sprintf($website_validate_strings[$curesc->curesc_website_validate_type], $curesc->curesc_website_validate_var_1, $curesc->curesc_website_validate_var_2, $curesc->curesc_website_validate_var_3, $curesc->curesc_website_validate_var_4, $curesc->curesc_website_validate_var_5, $curesc->curesc_website_validate_var_6);
            }
        }

        $curesc->curesc_lock_user_id = Auth::user()->id;
        $curesc->curesc_lock_timestamp = date("Y-m-d H:i:s");

        $curesc->save();

        return view('python_process.process',
            [
                'to_be_processed' => $curesc,
                'left' => $curesc_left,
                'notices' => $notices,
                'warnings' => $warnings,
                'search_url' => $search_url,
                'coc_link' => $coc_link,
                'reviews'  => $reviews,
                'did_we_send_leads_last_3_years' => $did_we_send_leads_last_3_years,
                'crm_statuses' => CustomerStatusType::all(),
                'show_all_results' => $show_all_results,
                'status' => $status,
                'co_code' => $co_code,
                'website_validate_message' => $website_validate_message,
                'unserialized_history' => System::unserialize($curesc->curesc_history)
            ]);
    }

    public function validateSkippedDataOpen($curesc_id)
    {
        $curesc = CustomerReviewScore::leftJoin("customers", "cu_id", "curesc_cu_id")->leftJoin("mover_data", "moda_cu_id", "cu_id")->where("curesc_id", $curesc_id)->where("curesc_type", 1)->first();

        $warnings = [];
        $notices = [];

        $curesc_finder = CustomerReviewScore::leftJoin("customers", "cu_id", "curesc_cu_id")->where("curesc_type", 1)->where("curesc_customer_website", $curesc->curesc_customer_website)->where("curesc_customer_website", "!=", "")->where("curesc_id", "!=", $curesc->curesc_id)->where("cu_deleted", 0)->get();
        $website_ERP_finder = Customer::where("cu_website", $curesc->curesc_customer_website)->where("cu_website", "!=", "")->where("cu_id", "!=", $curesc->curesc_cu_id)->where("cu_deleted", 0)->get();
        if (count($curesc_finder) > 0 || count($website_ERP_finder) > 0) {
            $notices['Website'] = "The website URL is not unique. We got the following customer(s) with the same URL: ";

            $count = 0;
            if (count($curesc_finder) > 0) {
                foreach($curesc_finder as $cf) {
                    $count++;
                    $notices['Website'] .= (($count > 1) ? ", " : "")."<a target='_blank' href='/validate_skipped_data/".$cf->curesc_id."/validate'>".$cf->cu_company_name_business." (".$cf->curesc_cu_id.")</a>";
                }
            }

            if (count($website_ERP_finder) > 0) {
                foreach($website_ERP_finder as $wef) {
                    $count++;
                    $notices['Website'] .= (($count > 1) ? ", " : "")."<a target='_blank' href='/customers/".$wef->cu_id."/edit'>ERP - ".$wef->cu_company_name_business." (".$wef->cu_id.")</a>";
                }
            }
        }

        if (empty($curesc->curesc_address) && empty($curesc->curesc_customer_website))
        {
            $notices[''] = "There is no unique result found on ".ucfirst($curesc->curesc_platform)." for this customer. Please, handle this customer manually and check everything.";
        }


        if ($curesc->moda_disable_sirelo_export) {
            $warnings['Sirelo export disabled'] = 'This company is disabled from the export to Sirelo!';
        }

        if ($curesc->moda_not_operational) {
            $warnings['Not operational'] = 'This company is not operational anymore!';
        }

        $system = new System();

        $domain_erp = str_replace("www.", "", $system->getDomainFromURL($curesc->cu_website));
        $domain_other = str_replace("www.", "", $system->getDomainFromURL($curesc->curesc_customer_website));

        if (!empty($domain_erp) && !empty($domain_other) && $domain_erp != $domain_other)
        {
            $notices['Other domains'] = "The ERP and Google URL have different domains. Please check if its the same company!";
        }

        $search_url = 'https://www.google.com/search?q='.urlencode( $curesc->cu_company_name_business.' '.$curesc->cu_city );

        $coc_link = null;

        if($curesc->cu_co_code == "NL") {
            if (!empty($curesc->cu_coc)) {
                $coc_link = "https://www.kvk.nl/zoeken/handelsregister/?handelsnaam=&kvknummer=".$curesc->cu_coc."&straat=&postcode=&huisnummer=&plaats=&hoofdvestiging=1&rechtspersoon=1&nevenvestiging=1&zoekvervallen=0&zoekuitgeschreven=1&start=0";
            }
            else {
                $address = $curesc->cu_street_1;

                // Find a match and store it in $result.
                if ( preg_match('/([^\d]+)\s?(.+)/i', $address, $result) )
                {
                    $streetName = $result[1];
                    $streetNumber = $result[2];

                    $coc_link = "https://www.kvk.nl/zoeken/handelsregister/?handelsnaam=&kvknummer=&straat=".$streetName."&postcode=&huisnummer=".$streetNumber."&plaats=".$curesc->cu_city."&hoofdvestiging=1&rechtspersoon=1&nevenvestiging=1&zoekvervallen=0&zoekuitgeschreven=1&start=0";
                }
            }
        }

        //Check if this customer was partner (or is partner)
        $did_we_send_leads_last_3_years = false;

        $ktrecupo = KTRequestCustomerPortal::where("ktrecupo_cu_id", $curesc->cu_id)->whereRaw("`ktrecupo_timestamp` BETWEEN NOW() - INTERVAL 3 YEAR AND NOW()")->where("ktrecupo_type", 1)->first();
        if (count($ktrecupo) > 0)
        {
            $did_we_send_leads_last_3_years = true;
        }

        $reviews = [];
        $reviews_of_customer = CustomerGatheredReview::where("cugare_cu_id", $curesc->cu_id)->where("cugare_accepted", 0)->get();
        foreach ($reviews_of_customer as $row_reviews){
            $reviews[$row_reviews->cugare_platform] = [
                'score' => $row_reviews->cugare_score,
                'amount' => $row_reviews->cugare_amount,
                'url' => $row_reviews->cugare_url,
            ];
        }

        $website_validate_message = null;

        $website_validate_strings = WebsiteValidateString::all();

        if (!empty($curesc->curesc_website_validate_type)) {
            $var_counter = 0;
            for ($x = 1; $x <= 6; $x++) {
                if(!empty($curesc->{"curesc_website_validate_var_".$x})) {
                    $var_counter++;
                }
            }

            if ($var_counter == 0) {
                $website_validate_message = $website_validate_strings[$curesc->curesc_website_validate_type];
            }
            elseif ($var_counter == 1) {
                $website_validate_message = sprintf($website_validate_strings[$curesc->curesc_website_validate_type], $curesc->curesc_website_validate_var_1);
            }
            elseif($var_counter == 2) {
                $website_validate_message = sprintf($website_validate_strings[$curesc->curesc_website_validate_type], $curesc->curesc_website_validate_var_1, $curesc->curesc_website_validate_var_2);
            }
            elseif($var_counter == 3) {
                $website_validate_message = sprintf($website_validate_strings[$curesc->curesc_website_validate_type], $curesc->curesc_website_validate_var_1, $curesc->curesc_website_validate_var_2, $curesc->curesc_website_validate_var_3);
            }
            elseif($var_counter == 4) {
                $website_validate_message = sprintf($website_validate_strings[$curesc->curesc_website_validate_type], $curesc->curesc_website_validate_var_1, $curesc->curesc_website_validate_var_2, $curesc->curesc_website_validate_var_3, $curesc->curesc_website_validate_var_4);
            }
            elseif($var_counter == 5) {
                $website_validate_message = sprintf($website_validate_strings[$curesc->curesc_website_validate_type], $curesc->curesc_website_validate_var_1, $curesc->curesc_website_validate_var_2, $curesc->curesc_website_validate_var_3, $curesc->curesc_website_validate_var_4, $curesc->curesc_website_validate_var_5);
            }
            elseif($var_counter == 6) {
                $website_validate_message = sprintf($website_validate_strings[$curesc->curesc_website_validate_type], $curesc->curesc_website_validate_var_1, $curesc->curesc_website_validate_var_2, $curesc->curesc_website_validate_var_3, $curesc->curesc_website_validate_var_4, $curesc->curesc_website_validate_var_5, $curesc->curesc_website_validate_var_6);
            }
        }

        return view('python_process.process',
            [
                'to_be_processed' => $curesc,
                'notices' => $notices,
                'warnings' => $warnings,
                'reviews' => $reviews,
                'skip' => true,
                'search_url' => $search_url,
                'did_we_send_leads_last_3_years' => $did_we_send_leads_last_3_years,
                'coc_link' => $coc_link,
                'crm_statuses' => CustomerStatusType::all(),
                'website_validate_message' => $website_validate_message,
                'unserialized_history' => System::unserialize($curesc->curesc_history)

            ]);
    }

    public function validateSkippedDataProcess(Request $request, $curesc_id)
    {
        return self::process_next($request, "SKIP");
    }

    public function validateSkippedData()
    {
        $data = new Data();

        $curesc_rows = CustomerReviewScore::leftJoin("customers", "cu_id", "curesc_cu_id")->where("curesc_skip", 1)->where("curesc_type", 1)->get();

        return view('python_process.validate_skipped',
            [
                'curesc_rows' => $curesc_rows,
            ]);
    }

    public function validateGatheredData($none_left = false)
    {
        $countries = [];
        $data = new Data();

        $query_for_countries = CustomerReviewScore::select("cu_co_code")->leftJoin("customers", "cu_id", "curesc_cu_id")->where("curesc_type", 1)->where("curesc_processed", 0)->where("curesc_skip", 0)->whereNull("curesc_lock_user_id")->groupBy("cu_co_code")->get();

        foreach ($query_for_countries as $country)
        {
            $count_left = CustomerReviewScore::selectRaw("COUNT(*) as `counter`")->leftJoin("customers", "cu_id", "curesc_cu_id")->where("curesc_type", 1)->where("curesc_processed", 0)->where("curesc_skip", 0)->where("cu_co_code", $country->cu_co_code)->whereNull("curesc_lock_user_id")->first();

            $countries[$country->cu_co_code] = $data->country($country->cu_co_code, "EN")." (".$count_left->counter.")";
        }

        asort($countries);
        $skipped_count = CustomerReviewScore::where("curesc_skip", 1)->where("curesc_type", 1)->get()->count();

        return view('python_process.validate_data_filter',
            [
                'countries' => $countries,
                'skipped_count' => $skipped_count,
                'none_left' => $none_left,
            ]);
    }

    public function validateGatheredDataFiltered(Request $request)
    {
        $show_all_results = (($request->show_all_results == 'on') ? true : false);
        $status = $request->status;
        $co_code = $request->country;

        $curesc = CustomerReviewScore::leftJoin("customers", "cu_id", "curesc_cu_id")->leftJoin("mover_data", "moda_cu_id", "cu_id")->where("curesc_type", 1)->where("curesc_processed", 0);

        if (!empty($co_code)) {
            $curesc = $curesc->where("cu_co_code", $co_code);
        }

        if (!$show_all_results) {
            $curesc = $curesc->where("curesc_name","!=", "");
        }

        if ($status == 1) {
            $curesc = $curesc->where("moda_crm_status",1);
        }
        elseif ($status == 2) {
            $curesc = $curesc->where("moda_crm_status", "!=",1);
        }

        $curesc = $curesc->first();

        if (count($curesc) == 0) {
            return Redirect::back()->withErrors(['There is no data to validate for '.Data::country($request->country, "EN")." with ".(($show_all_results) ? "showing all results" : "showing results with found data").(($status != 0) ? (($status == 1) ? " and only partners" : " and only NON-partners") : "")]);
        }

        return self::processFiltered($request->country, $show_all_results, $status);
    }

    public function process_next(Request $request, $skip = null)
    {
        if (Arr::exists($request, 'go_to_next')) {
            if ($skip != null) {
                return redirect("/validate_skipped_data");
            }
            else {
                $curesc_finder = CustomerReviewScore::leftJoin("customers", "cu_id", "curesc_cu_id")->leftJoin("mover_data", "cu_id", "moda_cu_id")->where("curesc_type", 1)->where("curesc_processed", 0)->where("curesc_skip", 0)->whereNull("curesc_lock_user_id");

                if (!empty($request->co_code)) {
                    $curesc_finder = $curesc_finder->where("cu_co_code", $request->co_code);
                }

                if(!$request->show_all_results) {
                    $curesc_finder = $curesc_finder->where("curesc_name","!=", "");
                }

                if ($request->status == 1) {
                    $curesc_finder = $curesc_finder->where("moda_crm_status",1);
                }
                elseif ($request->status == 2) {
                    $curesc_finder = $curesc_finder->where("moda_crm_status", "!=",1);
                }

                $curesc_finder = $curesc_finder->first();


                if ($curesc_finder == 0){
                    return self::validateGatheredData(true);
                }

                return self::processFiltered($request->co_code, $request->show_all_results, $request->status);
            }
        }

        $curesc = CustomerReviewScore::leftJoin("customers", "cu_id", "curesc_cu_id")->where("curesc_id", $request->curesc_id)->first();

        if (Arr::exists($request, 'failed_to_delete_next')) {
            $curesc->curesc_processed = 1;
            $curesc->curesc_skip = 0;
            $curesc->curesc_use_reviews = 0;
            $curesc->curesc_processed_by = Auth::user()->us_id;
            $curesc->curesc_processed_timestamp = date("Y-m-d H:i:s");
            $curesc->curesc_lock_user_id = null;
            $curesc->curesc_lock_timestamp = null;
            $curesc->save();

            if ($skip != null) {
                return redirect("/validate_skipped_data");
            }
            else {
                $curesc_finder = CustomerReviewScore::leftJoin("customers", "cu_id", "curesc_cu_id")->leftJoin("mover_data", "cu_id", "moda_cu_id")->where("curesc_type", 1)->where("curesc_processed", 0)->where("curesc_skip", 0);

                if (!empty($request->co_code)) {
                    $curesc_finder = $curesc_finder->where("cu_co_code", $request->co_code);
                }

                if(!$request->show_all_results) {
                    $curesc_finder = $curesc_finder->where("curesc_name","!=", "");
                }

                if ($request->status == 1) {
                    $curesc_finder = $curesc_finder->where("moda_crm_status",1);
                }
                elseif ($request->status == 2) {
                    $curesc_finder = $curesc_finder->where("moda_crm_status", "!=",1);
                }

                $curesc_finder = $curesc_finder->first();

                if ($curesc_finder == 0){
                    return self::validateGatheredData(true);
                }

                return self::processFiltered($request->co_code, $request->show_all_results, $request->status);
            }
        }

        $customer = Customer::where("cu_id", $curesc->curesc_cu_id)->first();
        if ($customer->cu_type == CustomerType::MOVER || $customer->cu_type == CustomerType::LEAD_RESELLER) {
            $mover_data = MoverData::where("moda_cu_id", $curesc->curesc_cu_id)->first();
        }

        if (Arr::exists($request, 'skip_btn')) {
            $curesc->curesc_remarks = $request->remarks;
            $curesc->curesc_skip = 1;
            $curesc->curesc_lock_user_id = null;
            $curesc->curesc_lock_timestamp = null;
            $curesc->save();

            $curesc_finder = CustomerReviewScore::leftJoin("customers", "cu_id", "curesc_cu_id")->leftJoin("mover_data", "cu_id", "moda_cu_id")->where("curesc_type", 1)->where("curesc_processed", 0)->where("curesc_skip", 0)->whereNull("curesc_lock_user_id");

            if (!empty($request->co_code)) {
                $curesc_finder = $curesc_finder->where("cu_co_code", $request->co_code);
            }

            if(!$request->show_all_results) {
                $curesc_finder = $curesc_finder->where("curesc_name","!=", "");
            }

            if ($request->status == 1) {
                $curesc_finder = $curesc_finder->where("moda_crm_status",1);
            }
            elseif ($request->status == 2) {
                $curesc_finder = $curesc_finder->where("moda_crm_status", "!=",1);
            }

            $curesc_finder = $curesc_finder->first();

            if ($curesc_finder == 0){
                return self::validateGatheredData(true);
            }

            return self::processFiltered($request->co_code, $request->show_all_results, $request->status);

        }

        if(!empty($request->remarks)) {

            // Create a User using User model
            $customerremark = new CustomerRemark();

            $customerremark->cure_timestamp = Carbon::now()->toDateTimeString();
            $customerremark->cure_employee = Auth::user()->us_id;
            $customerremark->cure_text = $request->remarks;
            $customerremark->cure_cu_id = $request->cu_id;

            //Set to correct customer and save
            $customerremark->save();
        }

        $something_changed = false;

        $data = new Data();
        if (!empty($request->website)) {
            if($customer->cu_website != $request{$request->website.'_website'}) {
                $customer->cu_website = $request{$request->website.'_website'};
                $something_changed = true;
            }
        }

        if (!empty($request->company_name_business)) {
            if ($customer->cu_company_name_business != $request{$request->company_name_business.'_company_name_business'}){
                $customer->cu_company_name_business = $request{$request->company_name_business.'_company_name_business'};

                //Make sirelo forward thing
                $data->updateSireloKeyAndForwards($customer,  true);
                $something_changed = true;
            }
        }

        if (!empty($request->company_name_legal)) {
            if ($customer->cu_company_name_legal != $request{$request->company_name_legal.'_company_name_legal'}){
                $customer->cu_company_name_legal = $request{$request->company_name_legal.'_company_name_legal'};
                $something_changed = true;
            }
        }

        if (!empty($request->street)) {
            if ($customer->cu_street_1 != $request{$request->street.'_street'}){
                $customer->cu_street_1 = $request{$request->street.'_street'};
                $something_changed = true;
            }
        }

        if (!empty($request->zipcode)) {
            if ($customer->cu_zipcode != $request{$request->zipcode.'_zipcode'}) {
                $customer->cu_zipcode = $request{$request->zipcode.'_zipcode'};
                $something_changed = true;
            }
        }

        if (!empty($request->city)) {
            if ($customer->cu_city != $request{$request->city.'_city'}) {
                $customer->cu_city = $request{$request->city.'_city'};

                //Sirelo forward thing
                $data->updateSireloKeyAndForwards($customer,  true);
                $something_changed = true;
            }
        }

        if (!empty($request->coc)) {
            if ($customer->cu_coc != $request->coc) {
                $customer->cu_coc = $request->coc;
                $something_changed = true;
            }
        }

        if (!empty($request->telephone_main)) {
            if ($customer->cu_telephone != $request{$request->telephone_main.'_telephone_main'}) {
                $customer->cu_telephone = $request{$request->telephone_main.'_telephone_main'};
                $something_changed = true;
            }
        }

        if ($customer->cu_type == CustomerType::MOVER || $customer->cu_type == CustomerType::LEAD_RESELLER) {
            if (!empty($request->telephone_sirelo)) {
                if ($mover_data->moda_contact_telephone != $request{$request->telephone_sirelo . '_telephone_sirelo'}) {
                    $mover_data->moda_contact_telephone = $request{$request->telephone_sirelo . '_telephone_sirelo'};
                    $something_changed = true;
                }
            }

            if (!empty($request->email_sirelo)) {
                if ($mover_data->moda_contact_email != $request->email_sirelo) {
                    $mover_data->moda_contact_email = $request->email_sirelo;
                    $something_changed = true;
                }
            }

            if ($mover_data->moda_disable_sirelo_export) {
                $mover_data->moda_disable_sirelo_export = ($request->disable_sirelo_exp === 'on');
            }
        }

        if (!empty($request->email_general)) {
            if ($customer->cu_email != $request->email_general) {
                $customer->cu_email = $request->email_general;
                $something_changed = true;
            }
        }

        /*if (!empty($request->description)) {
            if ($customer->cu_description != $request{$request->description.'_description'}) {
                $customer->cu_description = $request{$request->description.'_description'};
                $something_changed = true;
            }
        }*/

        if (!empty($request->remarks)) {
            $curesc->curesc_remarks = $request->remarks;
        }

        $curesc->curesc_use_reviews = ($request->use_reviews === 'on');

        if ($request->use_reviews === 'on') {
            $curesc->curesc_accepted = 1;

            $cugare = CustomerGatheredReview::where("cugare_cu_id", $customer->cu_id)->where("cugare_processed", 0)->get();

            foreach ($cugare as $row_cugare) {
                $ktcugare_finder = KTCustomerGatheredReview::where("ktcugare_cu_id", $row_cugare->cugare_cu_id)->where("ktcugare_platform", $row_cugare->cugare_platform)->first();

                if (count($ktcugare_finder) > 0) {
                    $ktcugare_finder->ktcugare_score = $row_cugare->cugare_score;
                    $ktcugare_finder->ktcugare_amount = $row_cugare->cugare_amount;
                    $ktcugare_finder->ktcugare_original_score = $row_cugare->cugare_original_score;
                    $ktcugare_finder->ktcugare_original_range = $row_cugare->cugare_original_range;
                    $ktcugare_finder->ktcugare_url = $row_cugare->cugare_url;
                    $ktcugare_finder->ktcugare_timestamp_updated = date("Y-m-d H:i:s");
                    $ktcugare_finder->save();
                }
                else {
                    $ktcugare_new = new KTCustomerGatheredReview();
                    $ktcugare_new->ktcugare_cu_id = $row_cugare->cugare_cu_id;
                    $ktcugare_new->ktcugare_platform = $row_cugare->cugare_platform;
                    $ktcugare_new->ktcugare_score = $row_cugare->cugare_score;
                    $ktcugare_new->ktcugare_amount = $row_cugare->cugare_amount;
                    $ktcugare_new->ktcugare_original_score = $row_cugare->cugare_original_score;
                    $ktcugare_new->ktcugare_original_range = $row_cugare->cugare_original_range;
                    $ktcugare_new->ktcugare_url = $row_cugare->cugare_url;
                    $ktcugare_new->ktcugare_timestamp_updated = date("Y-m-d H:i:s");
                    $ktcugare_new->save();
                }

                $row_cugare->cugare_accepted = 1;
                $row_cugare->cugare_processed = 1;
                $row_cugare->save();
            }
        }

        //Send email when something has been changed
        if ($something_changed) {
            $curesc->curesc_mail = 1;
        }
        else {
            $curesc->curesc_mail = 3; //3 = DONT SEND EMAIL
        }

        if (Arr::exists($request, 'skip_and_save_btn')) {
            $curesc->curesc_skip = 1;
            $curesc->curesc_processed = 0;
        } else {
            $curesc->curesc_skip = 0;
            $curesc->curesc_processed = 1;
        }

        $curesc->curesc_processed_by = Auth::user()->us_id;
        $curesc->curesc_processed_timestamp = date("Y-m-d H:i:s");
        $curesc->curesc_lock_user_id = null;
        $curesc->curesc_lock_timestamp = null;

        $curesc->curesc_processed_counter = $curesc->curesc_processed_counter + 1;

        if ($curesc->curesc_skip_counter >= 3) {
            //Set wait timestamp
            $today = date("Y-m-d H:i:s");
            $wait_timestamp = date('Y-m-d H:i:s', strtotime('+6 month', strtotime($today)));
            $curesc->curesc_wait_timestamp = $wait_timestamp;
        }

        $curesc->curesc_skip_counter = 0;

        if ($customer->cu_type == CustomerType::MOVER || $customer->cu_type == CustomerType::LEAD_RESELLER) {
            $mover_data->save();
        }

        $customer->save();
        $curesc->save();

        if ($skip != null) {
            return redirect("/validate_skipped_data");
        }
        else {
            $curesc_finder = CustomerReviewScore::leftJoin("customers", "cu_id", "curesc_cu_id")->leftJoin("mover_data", "cu_id", "moda_cu_id")->where("curesc_type", 1)->where("curesc_processed", 0)->where("curesc_skip", 0)->whereNull("curesc_lock_user_id");

            if (!empty($request->co_code)) {
                $curesc_finder = $curesc_finder->where("cu_co_code", $request->co_code);
            }

            if (!$request->show_all_results) {
                $curesc_finder = $curesc_finder->where("curesc_name","!=", "");
            }

            if ($request->status == 1) {
                $curesc_finder = $curesc_finder->where("moda_crm_status",1);
            }
            elseif ($request->status == 2) {
                $curesc_finder = $curesc_finder->where("moda_crm_status", "!=",1);
            }

            $curesc_finder = $curesc_finder->first();
            if ($curesc_finder == 0){
                return self::validateGatheredData(true);
            }

            return self::processFiltered($request->co_code, $request->show_all_results, $request->status);
        }
    }

    public function validateRelatedMovingCompanies($co_code = null)
    {
        $entries = CustomerReviewScore::leftJoin("gathered_companies", "curesc_gaco_id", "gaco_id")
            ->where("curesc_type", 2)
            ->where("curesc_processed", 0)
            ->where("curesc_skip", 0)
            ->whereRaw("(curesc_lock_user_id is NULL OR curesc_lock_user_id = '".Auth::user()->us_id."')");

        if (!empty($co_code)) {
            $entries = $entries->where("gaco_searched_co_code", $co_code);
        }

        $entries = $entries->get();

        $countries = [];

        $countries_query = CustomerReviewScore::leftJoin("gathered_companies", "gaco_id", "curesc_gaco_id")
            ->leftJoin("countries", "co_code", "gaco_searched_co_code")
            ->where("curesc_type", 2)
            ->where("curesc_processed", 0)
            ->groupBy("gaco_searched_co_code")
            ->get();

        foreach ($countries_query as $country) {
            $countries[$country->co_code] = $country->co_en;
        }

        return view('python_process.related_moving_companies',
            [
                'entries' => $entries,
                'selected_co_code' => ((!empty($co_code)) ? $co_code : ""),
                'countries' => $countries
            ]);
    }

    public function createRelatedMovingCompany($curesc_id, $co_code = null)
    {
        $related_company = CustomerReviewScore::leftJoin("gathered_companies", "gaco_id", "curesc_gaco_id")
            ->where("curesc_type", 2)
            ->where("curesc_processed", 0)
            ->where("curesc_skip", 0)
            ->whereRaw("(curesc_lock_user_id is NULL OR curesc_lock_user_id = '".Auth::user()->us_id."')")
            ->where("curesc_id", $curesc_id)
            ->first();

        if (count($related_company) == 0) {
            //return back with message that company is not available anymore
            return redirect()->back()->withErrors([ 'msg' => 'It looks like someone else opened this record earlier.']);
        }
        else {
            $related_company->curesc_lock_user_id = Auth::user()->us_id;
            $related_company->curesc_lock_timestamp = date("Y-m-d H:i:s");
            $related_company->save();
        }

        return view('python_process.related_moving_companies_create',
            [
                'curesc' => $related_company,
                'selected_co_code' => ((!empty($co_code)) ? $co_code : ""),
                'countries' => Country::all(),
                'debtorstatuses' => DebtorStatus::all(),
                'paymentreminderstatuses' => PaymentReminderStatus::all(),
                'paymentmethods' => PaymentMethod::all(),
                'paymentcurrencies' => PaymentCurrency::all(),
                'ledgeraccounts' => LedgerAccount::all(),
                'languages' => Language::where("la_iframe_only", 0)->get(),
                "customerservices" => CustomerServices::all(),
                'users' => User::where("id", "!=", 0)->where("us_is_deleted", 0)->orderBy("us_name")->get(),
                "customerstatustypes" => CustomerStatusType::all(),
                "customermarkettypes" => CustomerMarketType::all(),
                'regions' => Region::join("countries", "reg_co_code", "co_code")->where("reg_destination_type", 1)->where("reg_deleted", 0)->groupBy("reg_co_code", "reg_parent")->get(),
                'memberships' => Membership::orderBy("me_name", "ASC")->get()
            ]);
    }

    public function storeRelatedMovingCompany(Request $request)
    {
        $curesc = CustomerReviewScore::where("curesc_id", $request->curesc_id)->first();

        if (Arr::exists($request, 'nothing')) {
            $curesc->curesc_type = 5;
            $curesc->curesc_processed = 1;
            $curesc->curesc_processed_timestamp = date("Y-m-d H:i:s");
            $curesc->curesc_processed_by = Auth::user()->us_id;
            $curesc->save();

            return self::validateRelatedMovingCompanies($request->co_code);
        }

        if (Arr::exists($request, 'back')) {
            $curesc->curesc_lock_user_id = null;
            $curesc->curesc_lock_timestamp = null;
            $curesc->save();

            return self::validateRelatedMovingCompanies($request->co_code);
        }

        $request->validate([
            'la_code' => 'required',
            'company_name_business' => 'required',
            'company_name_legal' => 'required',
            'attn_email' => 'required',
            'leads_email' => 'required',
            'review_email' => 'required',
            'load_exchange_email' => 'required',
            'general_info' => 'required',
            'city' => 'required',
            'country' => 'required',
            'website' => 'required',
        ]);

        // Create a User using User model
        $customer = new Customer;
        $system = new System();

        if ($request->logo)
        {
            $allowed_types = ['image/jpeg', 'image/png'];

            $image_size = getimagesize($_FILES['logo']['tmp_name']);
            $image_width = $image_size[0];
            $image_height = $image_size[1];

            if ($_FILES['logo']['size'] < 1)
            {
                return redirect()->back()->withErrors(['logo' => 'The logo has not been saved, because the file is empty.'])->withInput($request->all());
            } elseif ($_FILES['logo']['size'] > 10000000)
            {
                return redirect()->back()->withErrors(['logo' => 'The logo has not been saved, because the file is bigger than 10MB. Your size: '.$_FILES['logo']['size']])->withInput($request->all());
            } elseif (!in_array($_FILES['logo']['type'], $allowed_types))
            {
                return redirect()->back()->withErrors(['logo' => 'The logo has not been saved, because the file type is not valid.'])->withInput($request->all());
            } /*elseif ($image_width < 100 || $image_height < 50)
            {
                return redirect()->back()->withErrors(['logo' => 'The logo has not been saved, because the image is too small. The image has to be min. 100 pixels wide and 50 pixels high.'])->withInput(Input::all());
            } elseif ($image_width > 600 || $image_height > 200)
            {
                return redirect()->back()->withErrors(['logo' => 'The logo has not been saved, because the image is too big. The image has to be max. 600 pixels wide and 200 pixels high.'])->withInput(Input::all());
            }*/

            $img = Image::make($request->file('logo'));

            if ($img->width() > 600 || $img->height() > 200) {
                $img->resize(600, 200, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }

            $file_location = config("system.paths.shared") . 'uploads/logos/';
            $new_logo = $request->file('logo');
            $new_logo_name = System::hashLogoName($request->curesc_id, $new_logo->getClientOriginalExtension());

            $img->save( $file_location.$new_logo_name );


            // Update the logo name in the db
            $customer->cu_logo = $new_logo_name;
        }

        $skipped = $curesc->curesc_skipped;

        $customer->cu_type = 1;
        $customer->cu_la_code = $request->la_code;
        $customer->cu_company_name_business = $request->company_name_business;
        $customer->cu_company_name_legal = $request->company_name_legal;
        $customer->cu_attn = $request->attn;
        $customer->cu_attn_email = $request->attn_email;
        $customer->cu_coc = $request->coc;
        $customer->cu_street_1 = $request->street_1;
        $customer->cu_street_2 = $request->street_2;
        $customer->cu_city = $request->city;
        $customer->cu_zipcode = $request->zipcode;
        $customer->cu_email = $request->leads_email;
        $customer->cu_co_code = $request->country;
        $customer->cu_telephone = $request->telephone;

        //IF country is US, than try to find the right state and save it
        if ($request->country == "US" && !empty($request->city)) {
            $admincontroller = new AdminController();

            $stateId = $admincontroller->getStateIdByCity($request->city, $request->country);

            if (!empty($stateId)) {
                $customer->cu_state = $stateId;
            }
        }

        $customer->cu_sales_manager = null;
        $customer->cu_account_manager = null;

        $customer->cu_website = $request->website;

        $customer->cu_description = $request->description;
        $customer->cu_created_timestamp = date("Y-m-d H:i:s");
        $customer->cu_debtor_number = DB::table('customers')->max('cu_debtor_number') + 1;
        $customer->cu_use_billing_address = 0;




        $customer->save();

        $moverdata = new MoverData();

        $moverdata->moda_cu_id = $customer->cu_id;
        $moverdata->moda_contact_email = $request->leads_email;
        $moverdata->moda_load_exchange_email = $request->load_exchange_email;
        $moverdata->moda_review_email = $request->review_email;
        $moverdata->moda_info_email = $request->general_info;
        $moverdata->moda_contact_telephone = $request->telephone;
        $moverdata->moda_max_claim_percentage = 10;
        $moverdata->moda_activate_lead_pick = 1;
        //$moverdata->moda_international_moves = 1;

        $moverdata->moda_reg_id = $request->fields_region;
        $moverdata->moda_market_type = $request->fields_market;
        $moverdata->moda_services = $request->fields_services;
        $moverdata->moda_crm_status = $request->fields_status;
        $moverdata->moda_owner = $request->fields_owner;

        if ($request->fields_market == 1) {
            //INT
            $moverdata->moda_international_moves = 1;
        }
        elseif($request->fields_market == 2) {
            //NAT
            $moverdata->moda_national_moves = 1;
        }
        elseif($request->fields_market == 3) {
            //Both
            $moverdata->moda_international_moves = 1;
            $moverdata->moda_national_moves = 1;
        }

        $moverdata->save();

        $curesc->curesc_type = 1;
        $curesc->curesc_cu_id = $customer->cu_id;
        $curesc->curesc_processed = 1;
        $curesc->curesc_skip = 0;
        $curesc->curesc_processed_counter = 1;
        $curesc->curesc_processed_timestamp = date("Y-m-d H:i:s");
        $curesc->curesc_processed_by = Auth::user()->us_id;

        $curesc->curesc_accepted = 1;
        $curesc->curesc_use_reviews = 1;

        $curesc->curesc_mail = 4;

        $cugare = CustomerGatheredReview::where("cugare_curesc_id", $curesc->curesc_id)->where("cugare_processed", 0)->get();

        foreach ($cugare as $row_cugare) {
            $ktcugare_new = new KTCustomerGatheredReview();
            $ktcugare_new->ktcugare_cu_id = $customer->cu_id;
            $ktcugare_new->ktcugare_platform = $row_cugare->cugare_platform;
            $ktcugare_new->ktcugare_score = $row_cugare->cugare_score;
            $ktcugare_new->ktcugare_amount = $row_cugare->cugare_amount;
            $ktcugare_new->ktcugare_original_score = $row_cugare->cugare_original_score;
            $ktcugare_new->ktcugare_original_range = $row_cugare->cugare_original_range;
            $ktcugare_new->ktcugare_url = $row_cugare->cugare_url;
            $ktcugare_new->ktcugare_timestamp_updated = date("Y-m-d H:i:s");
            $ktcugare_new->save();

            $row_cugare->cugare_cu_id = $customer->cu_id;
            $row_cugare->cugare_accepted = 1;
            $row_cugare->cugare_processed = 1;
            $row_cugare->save();
        }

        if (!empty($request->associations)) {
            foreach ($request->associations as $memb_id) {
                $ktcume = new KTCustomerMembership();
                $ktcume->ktcume_cu_id = $customer->cu_id;
                $ktcume->ktcume_me_id = $memb_id;
                $ktcume->save();
            }
        }

        $curesc->save();

        //Before returning to list, open new tab with Customer Edit of the new customer
        $customer_edit_url = "https://erp2.triglobal.info/customers/".$customer->cu_id."/edit";

        echo "<script type='text/javascript'>window.open('".$customer_edit_url."', '_blank')</script>";

        return self::validateRelatedMovingCompanies($request->co_code);
    }

    public function validateRelatedMovingCompaniesFiltered(Request $request)
    {
        $entries = CustomerReviewScore::leftJoin("gathered_companies", "gaco_id", "curesc_gaco_id")
            ->leftJoin("gathered_companies_list", "gacoli_title", "gaco_title")
            ->where("curesc_type", 2)
            ->where("curesc_processed", 0)
            ->where("curesc_skip", 0)
            ->whereRaw("(curesc_lock_user_id is NULL OR curesc_lock_user_id = '".Auth::user()->us_id."')");

        if (!empty($request->country)) {
            $entries = $entries->where("gaco_searched_co_code", $request->country);

            if (!empty($request->categories)) {
                $entries = $entries->whereIn("gacoli_id", $request->categories);
            }
        }

        $entries = $entries->groupBy("curesc_id")->get();

        $countries = [];

        $countries_query = CustomerReviewScore::leftJoin("gathered_companies", "gaco_id", "curesc_gaco_id")
            ->leftJoin("countries", "co_code", "gaco_searched_co_code")
            ->where("curesc_type", 2)
            ->where("curesc_processed", 0)
            ->groupBy("gaco_searched_co_code")
            ->get();

        foreach ($countries_query as $country) {
            $countries[$country->co_code] = $country->co_en;
        }

        $main_categories = [];


        foreach ($request->categories_hidden as $key => $id) {

            $c = explode(",", $id);
            $category_name = DB::table("gathered_companies_list")
                ->where("gacoli_id", $c[0])
                ->first()->gacoli_title;

            $main_categories[$id] = $category_name;
        }

        return view('python_process.related_moving_companies',
            [
                'entries' => $entries,
                'selected_co_code' => $request->country,
                'countries' => $countries,
                'hidden_categories' => $request->categories_hidden,
                'categories' => $request->categories,
                'main_categories' => $main_categories
            ]);
    }

    public function settingsRelatedMovingCompaniesCountries()
    {
        $countries = [];
        $countries_enabled = [];

        $country_query = GatheredCompany::select("co_en", "co_code", "gaco_country_status")
            ->leftJoin("countries", "co_code", "gaco_searched_co_code")
            /*->where("gaco_whitelisted", 0)
            ->where("gaco_blacklisted", 0)*/
            ->groupBy("gaco_searched_co_code")
            ->orderBy("co_en", "ASC")
            ->get();

        $countries_enabled_query = GatheredCompanyCountryWhitelisted::all();

        foreach ($countries_enabled_query as $active) {
            $countries_enabled[] = $active->gacocowh_co_code;
        }

        foreach ($country_query as $country) {
            $countries[$country->co_code] = [
                'country' => $country->co_en,
                'status' => ((in_array($country->co_code, $countries_enabled)) ? 1 : 0)
            ];
        }


        return view('python_process.related_moving_companies_settings_countries',
            [
                'countries' => $countries,
            ]);
    }

    public function settingsRelatedMovingCompaniesWhiteAndBlacklist()
    {
        $countries = [];

        $country_query = GatheredCompany::select("co_en", "co_code", "gacoli_type")
            ->leftJoin("countries", "co_code", "gaco_searched_co_code")
            ->leftJoin("gathered_companies_list", "gacoli_title", "gaco_title")
            ->whereNotNull("gacoli_type")
            ->groupBy("gaco_searched_co_code")
            ->orderBy("co_en", "ASC")
            ->get();

        foreach ($country_query as $country) {
            $countries[$country->co_code] = [
                'country' => $country->co_en,
                'white_black' => $country->gacoli_type,
            ];
        }

        return view('python_process.related_moving_companies_settings_current_white_and_blacklist',
            [
                'countries' => $countries,
            ]);
    }

    public function settingsRelatedMovingCompaniesWhiteAndBlacklistFiltered(Request $request)
    {
        $countries = [];

        $country_query = GatheredCompany::select("co_en", "co_code", "gacoli_type")
            ->leftJoin("countries", "co_code", "gaco_searched_co_code")
            ->leftJoin("gathered_companies_list", "gacoli_title", "gaco_title")
            //->whereRaw("(gaco_whitelisted = 1 OR gaco_blacklisted = 1)")
            ->whereNotNull("gacoli_type")
            ->groupBy("gaco_searched_co_code")
            ->orderBy("co_en", "ASC")
            ->get();

        foreach ($country_query as $country) {
            $countries[$country->co_code] = [
                'country' => $country->co_en,
                'white_black' => $country->gacoli_type,
            ];
        }

        $filtered_data = [];

        $titles_query = GatheredCompany::leftJoin("gathered_companies_list", "gacoli_title", "gaco_title")
            ->where("gaco_searched_co_code", $request->country)
            ->where("gaco_title", "!=", "")
            //->where("gaco_whitelisted", 0)
            //->where("gaco_blacklisted", 0)
            //->whereRaw("(gaco_whitelisted = 1 OR gaco_blacklisted = 1)")
            ->whereNotNull("gacoli_type")
            ->groupBy("gaco_title")
            ->orderBy("gaco_title", "ASC")
            ->get();

        foreach ($titles_query as $title) {
            $filtered_data[] = [
                'title' => $title->gaco_title,
                'white_black' => $title->gacoli_type,
            ];
        }

        return view('python_process.related_moving_companies_settings_current_white_and_blacklist',
            [
                'countries' => $countries,
                'filtered_data' => $filtered_data,
                'selected_co_code' => $request->country
            ]);
    }

    public function settingsRelatedMovingCompanies()
    {
        $countries = [];

        $country_query = GatheredCompany::select("co_en", "co_code")
            ->leftJoin("countries", "co_code", "gaco_searched_co_code")
            ->leftJoin("gathered_companies_list", "gacoli_title", "gaco_title")
            //->where("gaco_whitelisted", 0)
            //->where("gaco_blacklisted", 0)
            ->whereNull("gacoli_type")
            ->groupBy("gaco_searched_co_code")
            ->orderBy("co_en", "ASC")
            ->get();

        foreach ($country_query as $country) {
            $countries[$country->co_code] = $country->co_en;
        }

        $filtered_data = [];

        $titles_query = GatheredCompany::leftJoin("gathered_companies_list", "gacoli_title", "gaco_title")
            ->where("gaco_title", "!=", "")
            //->where("gaco_whitelisted", 0)
            //->where("gaco_blacklisted", 0)
            ->whereNull("gacoli_type")
            ->groupBy("gaco_title")
            ->orderBy("gaco_title", "ASC")
            ->get();

        foreach ($titles_query as $title) {
            $filtered_data[] = [
                'title' => $title->gaco_title
            ];
        }

        return view('python_process.related_moving_companies_settings',
            [
                'countries' => $countries,
                'filtered_data' => $filtered_data,
            ]);
    }

    public function settingsWhiteOrBlacklistFiltered(Request $request)
    {
        $countries = [];

        $country_query = GatheredCompany::select("co_en", "co_code")
            ->leftJoin("countries", "co_code", "gaco_searched_co_code")
            ->leftJoin("gathered_companies_list", "gacoli_title", "gaco_title")
            //->where("gaco_whitelisted", 0)
            //->where("gaco_blacklisted", 0)
            ->whereNull("gacoli_type")
            ->groupBy("gaco_searched_co_code")
            ->orderBy("co_en", "ASC")
            ->get();

        foreach ($country_query as $country) {
            $countries[$country->co_code] = $country->co_en;
        }

        $filtered_data = [];

        $titles_query = GatheredCompany::leftJoin("gathered_companies_list", "gacoli_title", "gaco_title")
            ->where("gaco_searched_co_code", $request->country)
            ->where("gaco_title", "!=", "")
            //->where("gaco_whitelisted", 0)
            //->where("gaco_blacklisted", 0)
            ->whereNull("gacoli_type")
            ->groupBy("gaco_title")
            ->orderBy("gaco_title", "ASC")
            ->get();

        foreach ($titles_query as $title) {
            $filtered_data[] = [
                'title' => $title->gaco_title
            ];
        }

        return view('python_process.related_moving_companies_settings',
            [
                'countries' => $countries,
                'filtered_data' => $filtered_data,
                'selected_co_code' => $request->country,
            ]);
    }
}
