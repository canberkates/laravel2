<?php

namespace App\Http\Controllers;

use App\Data\NewsletterSlot;
use App\Data\VolumeType;
use App\Models\Country;
use App\Models\KTWebsiteLanguage;
use App\Models\Language;
use App\Models\Newsletter;
use App\Models\Website;
use Illuminate\Http\Request;

class WebsiteController extends Controller
{
	public function index()
	{
		$websites = Website::all();
		
		return view('website.index',
		[
			'websites' => $websites
		]);
	}
	
	public function edit($website_id)
	{
		$website = Website::with(["websiteforms.portal.country", "websiteforms.language", "subdomains.country", "subdomains.language", "language"])->find($website_id);
        
        foreach(Language::where("la_iframe_only", 0)->get() as $language){
            $languages[$language->la_code] = $language->la_language;
        }
        
        
		
        //Get multiple languages for Sirelo buckets
		if($website->subdomains){
		    foreach($website->subdomains as $id => $domain){
                foreach(KTWebsiteLanguage::where("ktwela_subdomain_id", $domain->wesu_id)->get() as $code){
                    if($domain->languages){
                        $domain->languages = $domain->languages.", ".$languages[$code->ktwela_la_code];
                    }
                    else
                    {
                        $domain->languages = $languages[$code->ktwela_la_code];
                    }
                }
            }
        }
        
        foreach(Country::orderBy("co_en")->get() as $country)
        {
            $countries[$country->co_code] = $country->co_en;
        }
		
		return view('website.edit',
			[
				'website' => $website,
                'volumetypes' => VolumeType::all(),
                'countries' => $countries,
                'languages' => $languages
			]);
		
	}
	
	public function update(Request $request, $website_id)
	{
		
		return redirect('admin/websites/' . $website_id . '/edit');
	}
	
	public function create()
	{
		
		return view('newsletter.create',
			[
				'slots' => NewsletterSlot::all(),
				'languages' => Language::whereIn("la_code", ["NL", "DK", "DE", "IT", "ES", "FR", "EN"])->get()
			]);
		
	}
	
	public function store(Request $request)
	{
		$request->validate([
			'name' => 'required',
			'delay' => 'required',
			'subject_singular' => 'required',
			'subject_plural' => 'required',
		]);
		
		$newsletter = new Newsletter();
		
		$newsletter->new_name = $request->name;
		$newsletter->new_delay = $request->delay;
		$newsletter->new_subject_singular = base64_encode(serialize($request->subject_singular));
		$newsletter->new_subject_plural = base64_encode(serialize($request->subject_plural));
		$newsletter->new_intro = base64_encode(serialize($request->intro));
		$newsletter->new_slot_1 = $request->slot_1;
		$newsletter->new_slot_2 = $request->slot_2;
		$newsletter->new_slot_3 = $request->slot_3;
		$newsletter->new_slot_4 = $request->slot_4;
		$newsletter->new_slot_5 = $request->slot_5;
		$newsletter->new_slot_6 = $request->slot_6;
		$newsletter->new_slot_7 = $request->slot_7;
		$newsletter->new_slot_8 = $request->slot_8;
		$newsletter->new_slot_9 = $request->slot_9;
		$newsletter->new_slot_10 = $request->slot_10;
		$newsletter->new_deleted = 0;
		
		$newsletter->save();
		
		return redirect('admin/newsletters/');
		
	}
}
