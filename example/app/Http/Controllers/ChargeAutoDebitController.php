<?php

namespace App\Http\Controllers;

use App\Data\AutoDebitType;
use App\Functions\DataIndex;
use App\Functions\DOMDocumentExtended;
use App\Functions\System;
use DB;
use Illuminate\Http\Request;

class ChargeAutoDebitController extends Controller
{
	public function index()
	{
        $query = DB::table("customers")
            ->select("cu_id", "cu_company_name_business")
            ->where("cu_deleted", 0)
            ->where("cu_payment_method", 3)
            ->get();
        
        foreach($query as $result)
        {
            $customers[$result->cu_id] = $result->cu_company_name_business;
        }
        
        return view('finance.chargeautodebitfile', [
            'customers' => $customers,
            'months' => DataIndex::getMonths(),
            'autodebittypes' => AutoDebitType::all()
		]);
	}
    
    public function createFile(Request $request)
    {
        $request->validate([
            'customer' => 'required',
            'type' => 'required',
            'amount' => 'required'
        ]);
    
        $row = DB::table("customers")
            ->where("cu_deleted", 0)
            ->where("cu_id", $request->customer)
            ->first();
    
        $lines = [];
        
        if($row)
        {
            $lines[$row->cu_id] = [
                "debtor_number" => $row->cu_debtor_number,
                "date" => date("Y-m-d"),
                "amount" => $request->amount,
                "name" => $row->cu_auto_debit_name,
                "city" => $row->cu_auto_debit_city,
                "country" => $row->cu_auto_debit_co_code,
                "iban" => $row->cu_auto_debit_iban,
                "bic" => $row->cu_auto_debit_bic,
                "characteristic" => $request->description,
            ];
        }
    
        if(!empty($lines))
        {
            $i = 0;
        
            $xml = new DOMDocumentExtended("1.0", "UTF-8");
            $xml->preserveWhiteSpace = false;
            $xml->formatOutput = true;
        
            $Document = $xml->appendChild($xml->createElement("Document"));
            $Document->setAttribute("xmlns", "urn:iso:std:iso:20022:tech:xsd:pain.008.001.02");
            $Document->setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        
            $CstmrDrctDbtInitn = $Document->appendChild($xml->createElement("CstmrDrctDbtInitn"));
        
            $GrpHdr = $CstmrDrctDbtInitn->appendChild($xml->createElement("GrpHdr"));
            $GrpHdr->appendChild($xml->createElementText("MsgId", System::getSetting("auto_debit_prefix")."-".date("Y_m_d_H_i_s")));
            $GrpHdr->appendChild($xml->createElementText("CreDtTm", date("Y-m-d")."T".date("H:i:s")."Z"));
            $GrpHdr->appendChild($xml->createElementText("NbOfTxs", count($lines)));
            $GrpHdr->appendChild($xml->createElement("InitgPty"))->appendChild($xml->createElementText("Nm", strtoupper(System::getSetting("company_bank_account_name"))));
        
            $PmtInf = $CstmrDrctDbtInitn->appendChild($xml->createElement("PmtInf"));
            $PmtInf->appendChild($xml->createElementText("PmtInfId", System::getSetting("auto_debit_prefix")."-".date("Y_m_d_H_i_s")."-1"));
            $PmtInf->appendChild($xml->createElementText("PmtMtd", "DD"));
        
            $PmtTpInf = $PmtInf->appendChild($xml->createElement("PmtTpInf"));
            $PmtTpInf->appendChild($xml->createElement("SvcLvl"))->appendChild($xml->createElementText("Cd", "SEPA"));
            $PmtTpInf->appendChild($xml->createElement("LclInstrm"))->appendChild($xml->createElementText("Cd", "B2B"));
            $PmtTpInf->appendChild($xml->createElementText("SeqTp", $request->type));
        
            $PmtInf->appendChild($xml->createElementText("ReqdColltnDt", date("Y-m-d")));
        
            $Cdtr = $PmtInf->appendChild($xml->createElement("Cdtr"));
            $Cdtr->appendChild($xml->createElementText("Nm", strtoupper(System::getSetting("company_bank_account_name"))));
        
            $PstlAdr = $Cdtr->appendChild($xml->createElement("PstlAdr"));
            $PstlAdr->appendChild($xml->createElementText("Ctry", System::getSetting("company_co_code")));
            $PstlAdr->appendChild($xml->createElementText("AdrLine", System::getSetting("company_street")));
            $PstlAdr->appendChild($xml->createElementText("AdrLine", System::getSetting("company_zipcode")." ".System::getSetting("company_city")));
        
            $PmtInf->appendChild($xml->createElement("CdtrAcct"))->appendChild($xml->createElement("Id"))->appendChild($xml->createElementText("IBAN", str_replace(" ", "", System::getSetting("company_iban"))));
            $PmtInf->appendChild($xml->createElement("CdtrAgt"))->appendChild($xml->createElement("FinInstnId"))->appendChild($xml->createElementText("BIC", System::getSetting("company_bic")));
            $PmtInf->appendChild($xml->createElement("UltmtCdtr"))->appendChild($xml->createElementText("Nm", strtoupper(System::getSetting("company_bank_account_name"))));
            $PmtInf->appendChild($xml->createElementText("ChrgBr", "SLEV"));
        
            $CdtrSchmeId = $PmtInf->appendChild($xml->createElement("CdtrSchmeId"))->appendChild($xml->createElement("Id"))->appendChild($xml->createElement("PrvtId"))->appendChild($xml->createElement("Othr"));
            $CdtrSchmeId->appendChild($xml->createElementText("Id", System::getSetting("auto_debit_collector_id")));
            $CdtrSchmeId->appendChild($xml->createElement("SchmeNm"))->appendChild($xml->createElementText("Prtry", "SEPA"));
        
            foreach($lines as $line)
            {
                if($line['amount'] != 0)
                {
                    $i++;
                
                    $DrctDbtTxInf = $PmtInf->appendChild($xml->createElement("DrctDbtTxInf"));
                    $DrctDbtTxInf->appendChild($xml->createElement("PmtId"))->appendChild($xml->createElementText("EndToEndId", System::getSetting("auto_debit_prefix")."-".date("Y_m_d_H_i_s")."-1-".$i));
                    $DrctDbtTxInf->appendChild($xml->createElementText("InstdAmt", System::numberFormat($line['amount'], 2, ".", "")))->setAttribute("Ccy", "EUR");
                
                    $DrctDbtTx = $DrctDbtTxInf->appendChild($xml->createElement("DrctDbtTx"))->appendChild($xml->createElement("MndtRltdInf"));
                    $DrctDbtTx->appendChild($xml->createElementText("MndtId", $line['debtor_number']));
                    $DrctDbtTx->appendChild($xml->createElementText("DtOfSgntr", $line['date']));
                    $DrctDbtTx->appendChild($xml->createElementText("AmdmntInd", "false"));
                
                    $DrctDbtTxInf->appendChild($xml->createElement("DbtrAgt"))->appendChild($xml->createElement("FinInstnId"))->appendChild($xml->createElementText("BIC", $line['bic']));
                
                    $Dbtr = $DrctDbtTxInf->appendChild($xml->createElement("Dbtr"));
                    $Dbtr->appendChild($xml->createElementText("Nm", strtoupper($line['name'])));
                
                    $PstlAdr = $Dbtr->appendChild($xml->createElement("PstlAdr"));
                    $PstlAdr->appendChild($xml->createElementText("Ctry", $line['country']));
                    $PstlAdr->appendChild($xml->createElementText("AdrLine", $line['city']));
                
                    $DrctDbtTxInf->appendChild($xml->createElement("DbtrAcct"))->appendChild($xml->createElement("Id"))->appendChild($xml->createElementText("IBAN", str_replace(" ", "", $line['iban'])));
                    $DrctDbtTxInf->appendChild($xml->createElement("UltmtDbtr"))->appendChild($xml->createElementText("Nm", strtoupper($line['name'])));
                    $DrctDbtTxInf->appendChild($xml->createElement("Purp"))->appendChild($xml->createElementText("Cd", "AREN"));
                    $DrctDbtTxInf->appendChild($xml->createElement("RmtInf"))->appendChild($xml->createElementText("Ustrd", $line['characteristic']));
                }
            }
        
            $file = "manual_charge_".date("YmdHis");
    
            $xml->save(env("SHARED_FOLDER") . "uploads/auto_debit_files/" . $file . ".xml");
        }
        
        return view('finance.createautodebitdownload', [
            'filename' => $file
        ]);
    }
}
