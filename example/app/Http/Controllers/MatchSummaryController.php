<?php

namespace App\Http\Controllers;

use App\Data\MatchMistakeType;
use App\Data\MatchRating;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MatchSummaryController extends Controller
{
	public function index()
	{
		return view('matchsummary.index',
			[
				'totals'=> null,
				'userratings' => [],
				'date_from' => null,
				'date_to' => null,
				'show_hours' => null
			]
		);
	}

	public function filteredindex(Request $request)
	{

		$matchratings = MatchRating::all();
		unset($matchratings[0]);
		unset($matchratings[4]);

		foreach($matchratings as $key => $value)
		{
			${"total_match_rating_".$key} = 0;
		}


		$total_total = 0;
		$total_matched = 0;
		$total_rejected = 0;
		$total_rated = 0;
		$total_called = 0;
		$total_reached = 0;
		$total_not_reached = 0;
        $total_hours = [];

        foreach(range(0, 23) as $hour)
        {
            ${"total_hour_".$hour} = 0;
        }

		$all_users = [-1 => "System"];
		foreach(User::all() as $user){
			$all_users[$user->id] = $user->us_name;
		}

		$user_ratings = [];

		foreach($all_users as $user_id => $username)
		{
            foreach(range(0, 23) as $hour)
            {
                ${"hours_".$hour} = 0;
            }

			$total = 0;
			$matched = 0;
			$rejected = 0;
			$rated = 0;
			$called = 0;
			$called_reached = 0;
			$called_not_reached = 0;

			foreach($matchratings as $key => $value)
			{
				${"match_ratings_".$key} = 0;
			}

			$query = DB::table("requests")
				->select("re_match_rating", "ktrecupo_timestamp", "re_internal_called")
				->leftJoin("kt_request_customer_portal", "re_id", "ktrecupo_re_id")
				->where("re_status", 1);

			if($user_id == -1)
			{
				$query->whereRaw("ktrecupo_us_id IS NULL");
			}
			else
			{
				$query->where("ktrecupo_us_id", $user_id);
			}

			$query = $query
				->where("ktrecupo_rematch", 0)
				->whereBetween("ktrecupo_timestamp", [date("Y-m-d H:i:s", strtotime($request->date_from." 00:00:00")), date("Y-m-d H:i:s", strtotime($request->date_to." 23:59:59"))])
				->groupBy("ktrecupo_re_id")
				->get();

			foreach($query as $row)
			{
				$total++;
				$matched++;

				if($row->re_match_rating != 0)
				{
					$rated++;

					$key  = MatchMistakeType::name($row->re_match_rating)[1];

					if ($key != 4)
						${"match_ratings_".$key}++;
				}

				if($row->re_internal_called == 1)
				{
					$called++;
					$called_not_reached++;
				}

				if($row->re_internal_called == 2)
				{
					$called++;
					$called_reached++;
				}

				if(!empty($request->show_hours) && $request->show_hours == "on")
				{
					${"hours_".date("G", strtotime($row->ktrecupo_timestamp))}++;
				}
			}

			$query = DB::table("requests")
				->select("re_match_rating", "re_rejection_timestamp", "re_internal_called")
				->where("re_status", 2)
				->where("re_rejection_us_id", $user_id)
				->whereBetween("re_rejection_timestamp", [date("Y-m-d H:i:s", strtotime($request->date_from." 00:00:00")), date("Y-m-d H:i:s", strtotime($request->date_to." 23:59:59"))])
				->groupBy("re_id")
				->get();

			foreach($query as $row)
			{
				$total++;
				$rejected++;

				if($row->re_match_rating != 0)
				{
					$rated++;
					$key  = MatchMistakeType::name($row->re_match_rating)[1];

					if ($key != 4)
						${"match_ratings_".$key}++;
				}

				if($row->re_internal_called == 1)
				{
					$called++;
					$called_not_reached++;
				}

				if($row->re_internal_called == 2)
				{
					$called++;
					$called_reached++;
				}

                if(!empty($request->show_hours) && $request->show_hours == "on")
				{
					${"hours_".date("G", strtotime($row->re_rejection_timestamp))}++;
				}
			}
            if(!empty($request->show_hours) && $request->show_hours == "on")
            {
                foreach(range(0, 23) as $hour)
                {
                    $total_hours['total_hour_'.$hour] += ${"hours_".$hour};
                }
            }
            
			if($total > 0)
			{
				$user_ratings[$user_id]['name'] = $username;
				$user_ratings[$user_id]['total'] = $total;
				$user_ratings[$user_id]['matched'] = $matched;
				$user_ratings[$user_id]['rejected'] = $rejected;
				$user_ratings[$user_id]['rated'] = $rated;
				$user_ratings[$user_id]['called'] = $called;
				$user_ratings[$user_id]['called_reached'] = $called_reached;
				$user_ratings[$user_id]['called_not_reached'] = $called_not_reached;

                foreach(range(0, 23) as $hour)
                {
                    $user_ratings[$user_id]['hours_'.$hour] = ${"hours_".$hour};
                }

				$total_total += $total;
				$total_matched += $matched;
				$total_rejected += $rejected;
				$total_rated += $rated;
				$total_called += $called;
				$total_reached += $called_reached;
				$total_not_reached += $called_not_reached;

				foreach($matchratings as $key => $value)
				{
					if(isset($user_ratings[$user_id][$key])){
						$user_ratings[$user_id]['type'][$key] += ${"match_ratings_".$key};
					}else{
						$user_ratings[$user_id]['type'][$key] = ${"match_ratings_".$key};
					}

					${"total_match_rating_".$key} += ${"match_ratings_".$key};
				}
			}
		}

		$totals['total'] = $total_total;
		$totals['matched'] = $total_matched;
		$totals['rejected'] = $total_rejected;
		$totals['rated'] = $total_rated;
		$totals['good'] =  ${"total_match_rating_1"};
		$totals['minor'] = ${"total_match_rating_2"};
		$totals['major'] =  ${"total_match_rating_3"};
		$totals['called'] = $total_called;
		$totals['reached'] = $total_reached;
		$totals['not_reached'] = $total_not_reached;

		return view('matchsummary.index',
			[
				'totals' => $totals,
				'userratings' => $user_ratings,
				'matchratings' => MatchRating::all(),
				'date_from' => $request->date_from,
				'date_to' => $request->date_to,
				'show_hours' => $request->show_hours,
				'total_hours' => $total_hours,
			]
		);

	}

}
