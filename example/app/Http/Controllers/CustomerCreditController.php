<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\CustomerCredit;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CustomerCreditController extends Controller
{
	
	public function create($customer_id)
	{
		$customer = Customer::findOrFail($customer_id);
		
		return view('customercredit.create',
			[
				'customer' => $customer
			]);
	}
	
	public function store(Request $request)
	{
		$request->validate([
			'amount' => 'required',
			'expiry_date' => 'required'
		]);
		
		// Validate the request...
		//$customer = Customer::findOrFail($request->customer_id);
		
		//Create new contact person
		$customercredit = new CustomerCredit();
		
		$customercredit->cucr_timestamp = Carbon::now()->toDateTimeString();
		$customercredit->cucr_cu_id = $request->customer_id;
		$customercredit->cucr_amount = $request->amount;
		$customercredit->cucr_amount_left = $request->amount;
		$customercredit->cucr_expired = $request->expiry_date;
		$customercredit->cucr_type = 2;
		
		//Set to correct customer and save
		$customercredit->save();
		
		return redirect('customers/' . $request->customer_id .'/edit')->with('message', 'Credits are successfully added!');
	}
}
