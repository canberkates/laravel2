<?php

namespace App\Http\Controllers;

use App\Data\DestinationType;
use App\Data\DiscountType;
use App\Data\GatheredCompaniesType;
use App\Data\PlannedCallReason;
use App\Data\PlannedCallStatus;
use App\Data\PlannedCallType;
use App\Data\RequestType;
use App\Data\Survey2Source;
use App\Data\WebsiteValidateType;
use App\Functions\Data;
use App\Functions\DataIndex;
use App\Functions\DialFire;
use App\Functions\Mail;
use App\Functions\Mover;
use App\Functions\Reporting;
use App\Functions\RequestData;
use App\Functions\System;
use App\Models\Country;
use App\Models\Customer;
use App\Models\FreeTrial;
use App\Models\GatheredCompany;
use App\Models\GatheredCompanyCountryWhitelisted;
use App\Models\KTCustomerPortal;
use App\Models\KTCustomerPortalCountry;
use App\Models\KTCustomerPortalRegion;
use App\Models\KTCustomerPortalRegionDestination;
use App\Models\KTCustomerProgress;
use App\Models\KTRequestCustomerPortal;
use App\Models\MacroRegion;
use App\Models\MatchStatistic;
use App\Models\Message;
use App\Models\MoverData;
use App\Models\PaymentRate;
use App\Models\PlannedCall;
use App\Models\Region;
use App\Models\ReportProgress;
use App\Models\ServiceProviderNewsletterBlock;
use App\Models\SystemSetting;
use App\Models\User;
use App\Models\Website;
use App\Models\WebsiteForm;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use pCloud\File;
use pCloud\Folder;

class AjaxController extends Controller
{
    public function getTranslation(Request $request)
    {
        $system = new System();

        return $system->getGoogleTranslation($request->value);
    }

    public function select2customersearch(Request $request){
       if(strlen($request->search) > 2){
           $customers = Customer::where("cu_company_name_business", "LIKE", "%" . $request->search . "%")->get();

           $result = [];
           $count = 0;

           foreach($customers as $customer){
               $result[$count]["id"] = $customer->cu_id;
               $result[$count]["text"] = $customer->cu_company_name_business . " - ". $customer->cu_city . ", " . $customer->cu_co_code . " (" . $customer->cu_id . ")";
               $count++;
           }

            return json_encode($result);
        }
    }

    public function findSuggestedRegion(Request $request)
    {
        $system = new System();

        if ($request->from_to == "from")
        {
            $regions = $system->findRegionsByOriginCity($request->city, $request->destination_type, $request->country);
        } else
        {
            $regions = $system->findRegionsByDestinationCity($request->city, $request->destination_type, $request->country);
        }

        return json_encode($regions);
    }

    public function getMovingSizes(Request $request)
    {
        $system = new System();

        $result = $system->movingSizes($request->request_type);

        return json_encode($result);

    }

    public function addRequestExtraInformation(Request $request)
    {
        $re_id = $request->request_id;
        $string = $request->string;

        //Update request with extra information
        $re = \App\Models\Request::where("re_id", $re_id)->first();
        $re->re_extra_information_text = $string;
        $re->re_extra_information_status = 1;
        $re->re_extra_information_timestamp = date("Y-m-d H:i:s");
        $re->save();

        $requests_customer = KTRequestCustomerPortal::select("ktrecupo_id", "ktrecupo_re_id")
            ->where("ktrecupo_re_id", $re_id)
            ->get();

        foreach ($requests_customer as $customer_req)
        {
            //Update customer request and set extra information on UNREAD
            $ktrecupo = KTRequestCustomerPortal::where("ktrecupo_id", $customer_req->ktrecupo_id)->first();
            $ktrecupo->ktrecupo_read_extra_information = 0;
            $ktrecupo->save();
        }
    }

    public function getRegions(Request $request)
    {
        $system = new System();

        Log::debug($request);

        return json_encode($system->countryRegions($request->country, $request->destination_type));
    }

    public function updateServiceProviderRequest(Request $request)
    {
        DB::table('kt_request_customer_question')
            ->where('ktrecuqu_id', $request->id)
            ->where('ktrecuqu_sent', 0)
            ->where('ktrecuqu_hide', 0)
            ->update(
                [
                    'ktrecuqu_rejection_status' => $request->rejection_status,
                ]
            );

    }

    public function requestWrongTelephoneNumber(Request $request)
    {
        $system = new System();
        $requestData = new RequestData();
        $mail = new Mail();

        $query = \App\Models\Request::where("re_id", $request->id)
            ->where("re_status", 0)->first();

        $pd_url = Website::where("we_sirelo_co_code",  $query->re_co_code_from)->first()->we_personal_dashboard_url;

        if (empty($pd_url)) {
            $pd_url = Website::where("we_sirelo_url", "https://sirelo.org")->first()->we_personal_dashboard_url;
        }

        Log::debug($request);

        if ($query)
        {

            $pd_link = $pd_url . "?id=" . $query->re_token."&utm_source=personal-dashboard&utm_medium=email";

            $excludes = ['RU'];

            if (in_array($query->re_la_code, $excludes))
            {
                $query->re_la_code = "EN";
            }

            $fields = [
                "request" => $system->databaseToArray($query),
                "email" => $request->email,
                "telephone_number" => $request->telephone_number,
                "pd_link" => $pd_link,
            ];

            if ($request->function == "check")
            {
                return "<iframe srcdoc=\"" . str_replace("\"", "'", $mail->send("request_wrong_telephone_number", $query->re_la_code, $fields, $requestData->requestWebsiteField($query->re_id, "we_mail_from_name"), null, null, true)) . "\" width=\"100%\" height=\"390\" frameborder=\"0\"></iframe>";
            } elseif ($request->function == "send")
            {
                $mail->send("request_wrong_telephone_number", $query->re_la_code, $fields);

                if ($request->telephone == 2)
                {
                    $query->re_telephone_2_mail = 1;
                } elseif ($request->telephone == 1)
                {
                    $query->re_telephone_1_mail = 1;
                }

                $query->save();
            }
        }
    }

    public function confirmSurvey(Request $request)
    {
        $system = new System();
        $requestData = new RequestData();
        $mail = new Mail();

        Log::debug($request);

        //Get website from which Survey was entered
        if ($request->type == Survey2Source::WEBSITE_REVIEW)
        {
            $query = DB::table("surveys_2")
                ->leftJoin("website_reviews", "were_id", "su_were_id")
                ->leftJoin("website_forms", "were_wefo_id", "wefo_id")
                ->leftJoin("websites", "wefo_we_id", "we_id")
                ->leftJoin("customers", "su_mover", "cu_id")
                ->where("su_id", $request->id)
                ->first();

            if ($query->cu_company_name_business == "")
            {
                $customer = DB::table("customers")
                    ->select("cu_company_name_business")
                    ->where("cu_id", $request->mover)
                    ->first();

                $query->cu_company_name_business = $customer->cu_company_name_business;
            }

        }

        if (!empty($query))
        {

            $fields = [
                "survey" => $system->databaseToArray($query),
                "email" => $request->email
            ];

            if ($request->function == "check")
            {
                return "<iframe srcdoc=\"" . str_replace("\"", "'", $mail->send("survey_confirm", $query->wefo_la_code, $fields, $query->we_mail_from_name, null, null, true)) . "\" width=\"100%\" height=\"390\" frameborder=\"0\"></iframe>";
            } elseif ($request->function == "send")
            {
                DB::table('surveys_2')
                    ->where('su_id', $request->id)
                    ->update(
                        [
                            'su_confirmation_email' => 1
                        ]
                    );

                $mail->send("survey_confirm", $query->wefo_la_code, $fields);
            }


        }
    }

    public function addToDialfire(Request $request)
    {
        $dialfire = new DialFire();
        $result = $dialfire->addToDialfire($request);

        return $result;
    }

    public function selectAllSirelos(Request $request)
    {
        $sirelos = [];

        $websiteforms = WebsiteForm::leftJoin("websites", "we_id", "wefo_we_id")->where("we_sirelo", 1)->get();

        foreach ($websiteforms as $website_form)
        {
            $sirelos[$website_form->wefo_id] = $website_form->wefo_id;
        }

        echo json_encode($sirelos);
    }

    public function selectAllNonSirelos(Request $request)
    {
        $sirelos = [];

        $websiteforms = WebsiteForm::leftJoin("websites", "we_id", "wefo_we_id")->where("we_sirelo", 0)->get();

        foreach ($websiteforms as $website_form)
        {
            $sirelos[$website_form->wefo_id] = $website_form->wefo_id;
        }

        echo json_encode($sirelos);
    }

    public function loadGoogleMapsImage(Request $request)
    {
        $parameters = [];

        if (!empty($request->street))
        {
            $parameters[] = $request->street;
        }

        if (!empty($request->zipcode))
        {
            $parameters[] = $request->zipcode;
        }

        if (!empty($request->city))
        {
            $parameters[] = $request->city;
        }

        if (!empty($request->country))
        {
            $country = Country::select("co_en")->where("co_code", "=", $request->country)->first();

            $parameters[] = $country->co_en;
        }

        $parameters = urlencode(implode(",", $parameters));

        //Get the Google Key
        $setting = SystemSetting::where("syse_setting", "=", "google_key")->first();
        $google_key = $setting->syse_value;

        return "<a href=\"https://maps.google.com/?q=" . $parameters . "\" target=\"_blank\"><img src=\"//maps.googleapis.com/maps/api/staticmap?center=" . $parameters . "&markers=size:mid|" . $parameters . "&sensor=TRUE_OR_FALSE&zoom=14&size=" . $request->width . "x" . $request->height . "&key=" . $google_key . "\" /></a>";
    }

    public function customerPortalCalculateLeadAmounts(Request $request)
    {
        $data = new DataIndex();
        $portal = KTCustomerPortal::with(["customer", "portal.regions", "customerportalregions", "customerportalcountries", "customerportalregionsdestination"])->findOrFail($request->portal_id);

        //Moving size
        $moving_sizes = [];

        foreach ($data->movingSizes($portal->ktcupo_request_type) as $size_id => $size_name)
        {
            if ($portal->{'ktcupo_moving_size_' . $size_id} == 1)
            {
                $moving_sizes[$size_id] = $size_name;
            }
        }
        foreach($portal->customerportalregions as $checked_regions)
        {
            $checked_origin[$checked_regions['ktcupore_reg_id']] = 1;
        }

        if($request->destination_type == DestinationType::INTMOVING)
        {

            $macro_regions = [];
            $regions_destination = [];
            $checked_destination = [];


            foreach($portal->customerportalcountries as $checked_countries)
            {
                $checked_destination[$checked_countries['ktcupoco_co_code']] = 1;
            }

            $requests_amount_per_block = [];

            foreach(MacroRegion::all() as $macro_region)
            {
                $macro_regions[$macro_region->mare_id] = [$macro_region->mare_name, $macro_region->mare_continent_name];
            }

            foreach(Country::all() as $country)
            {
                $request_amount_per_block_query = \App\Models\Request::join("regions", "re_reg_id_to", "reg_id")
                    ->join("countries", "reg_co_code", "co_code")
                    ->whereIn("re_moving_size", array_keys($moving_sizes))
                    ->whereRaw("(`re_status` = 1 OR (`re_status` = 2 AND `re_rejection_reason` IN (2,3)))")
                    ->whereIn("re_reg_id_from", array_keys($checked_origin))
                    ->where("co_mare_id", $country->co_mare_id)
                    ->whereRaw("`re_timestamp` BETWEEN NOW() - INTERVAL 30 DAY AND NOW()")
                    ->get();

                $requests_amount_per_block[$macro_regions[$country->co_mare_id][0]] = $request_amount_per_block_query->count();

                $regions_destination[$macro_regions[$country->co_mare_id][1]][$macro_regions[$country->co_mare_id][0]][$country['co_code']] = [$country['co_en'], in_array($country['co_code'], array_keys($checked_destination))];
            }
        }
        else if($request->destination_type == DestinationType::NATMOVING)
        {

            //Origins
            $regions_destination = [];
            $checked_destination = [];

            foreach($portal->customerportalregionsdestination as $checked_regions)
            {
                $checked_destination[$checked_regions['ktcuporede_reg_id']] = 1;
            }

            $requests_amount_per_block = [];

            foreach($portal->portal->regions as $row_regions)
            {

                $request_amount_per_block_query = \App\Models\Request::join("regions", "re_reg_id_to", "reg_id")
                    ->whereIn("re_reg_id_from", array_keys($checked_origin))
                    ->where("reg_parent", $row_regions['reg_parent'])
                    ->whereIn("re_moving_size", array_keys($moving_sizes))
                    ->whereRaw("`re_timestamp` BETWEEN NOW() - INTERVAL 30 DAY AND NOW()")
                    ->get();

                $requests_amount_per_block[$row_regions['reg_parent']] = $request_amount_per_block_query->count();

                $regions_destination[$row_regions['reg_co_code']][$row_regions['reg_parent']][$row_regions['reg_id']] = [$row_regions['reg_name'], in_array($row_regions['reg_id'], array_keys($checked_destination))];
            }


        }

        return json_encode($requests_amount_per_block);

    }

    public function customerPortalSummary(Request $request)
    {
        $ktcupo_row = KTCustomerPortal::leftjoin("portals", "ktcupo_po_id", "po_id")
            ->leftjoin("customers", "ktcupo_cu_id", "cu_id")
            ->leftjoin("mover_data", "ktcupo_cu_id", "moda_cu_id")
            ->leftjoin("countries", "co_code", "po_co_code")
            ->where("ktcupo_id", $request->ktcupo_id)
            ->where("cu_deleted", 0)
            ->first();

        if (count($ktcupo_row) > 0)
        {
            $data = new DataIndex();
            $system = new System();

            $major_error = false;
            $minor_error = false;

            //Output info
            echo "<h4>General</h4>";

            echo "<ul>";

            if ($ktcupo_row->cu_credit_hold == 1)
            {
                echo "<li style=\"color:red\">Warning: Customer is on credit hold</li>";
                $minor_error = true;
            }


            echo "<li>Status: " . $data->customerPairStatuses()[$ktcupo_row->ktcupo_status] . ($ktcupo_row->ktcupo_status != 1 ? " (<span style=\"color:red\">Warning: Not active!</span>)" : "") . "</li>";

            if ($ktcupo_row->ktcupo_status != 1)
            {
                $minor_error = true;
            }

            //Free trial
            echo "<li>Free trial: " . ($ktcupo_row->ktcupo_free_trial == 1 ? "Yes" : "No") . "</li>";

            if ($ktcupo_row->ktcupo_free_trial == 1)
            {
                $free_trial = FreeTrial::select("frtr_finished")
                    ->where("frtr_ktcupo_id", "=", $request->ktcupo_id)
                    ->whereRaw("`frtr_date` <= NOW()")
                    ->orderBy("frtr_date", "desc")
                    ->first();

                if (count($free_trial) == 0 || $free_trial->frtr_finished == 1)
                {
                    $can_receive_requests = false;
                    echo "<li style=\"color:red\">Warning: No free trials are active</li>";
                }
            }

            //Moving size
            $moving_sizes = [];

            foreach ($data->movingSizes($ktcupo_row->ktcupo_request_type) as $size_id => $size_name)
            {
                if ($ktcupo_row['ktcupo_moving_size_' . $size_id] == 1)
                {
                    $moving_sizes[$size_id] = $size_name;
                }
            }
            if (empty($moving_sizes))
            {
                echo "<li style=\"color:red\">Warning: No moving sizes selected</li>";
                $major_error = true;
            } else
            {
                echo "<li>Moving sizes: " . implode(", ", $moving_sizes) . "</li>";
            }

            //Import/Export
            echo "<li>Export: " . ($ktcupo_row->ktcupo_export_moves ? "Yes" : "No") . "</li>";
            echo "<li>Import: " . ($ktcupo_row->ktcupo_import_moves ? "Yes" : "No") . "</li>";

            if ($ktcupo_row->ktcupo_export_moves == 0 && $ktcupo_row->ktcupo_import_moves == 0)
            {
                $major_error = true;

                echo "<li style=\"color:red\">Warning: Neither export nor import moves is selected</li>";
            }

            echo "</ul>";
            echo "<h4>Cappings</h4>";
            echo "<ul>";

            //Cappings
            $capping_method = intval($ktcupo_row->moda_capping_method);
            echo "<li>Capping method: " . $data->moverCappingMethods()[$capping_method] . "</li>";
            switch ($capping_method)
            {
                case 0:
                    echo "<li>Max. requests: " . $ktcupo_row->ktcupo_max_requests_month . " <small>(Month)</small>, " . $ktcupo_row->ktcupo_max_requests_day . " <small>(Day)</small></li>";
                    echo "<li>Daily average: " . $system->numberFormat($ktcupo_row->ktcupo_daily_average, 2) . " <small>(Requests/Month)</small></li>";
                    break;

                case 1:
                    $moda_capping_type = "Max. requests";
                    $moda_capping_unit = "Requests";
                    break;

                case 2:
                    $moda_capping_type = "Monthly spend";
                    $moda_capping_unit = $system->paymentCurrencyToken($ktcupo_row->cu_pacu_code);
                    break;
            }

            if (isset($moda_capping_type) && isset($moda_capping_unit))
            {
                echo "<li>" . $moda_capping_type . " (customer level): " . $ktcupo_row->moda_capping_monthly_limit . " <small>(" . $moda_capping_unit . "/Month)</small></li>";

                if ($ktcupo_row->moda_capping_monthly_limit == 0)
                {
                    echo "<li style=\"color:red\">Warning: {$moda_capping_type} (Customer level) prevents requests from being received.</li>";

                    $minor_error = true;
                }

                echo "<li>Daily average: " . $system->numberFormat($ktcupo_row->moda_capping_daily_average, 2) . " <small>(" . $moda_capping_unit . "/Day)</small></li>";
                echo "<li>Max. requests (portal level): " . $ktcupo_row->ktcupo_max_requests_day . " <small>(Day)</small></li>";
            }


            if (($capping_method === 0 && ($ktcupo_row->ktcupo_max_requests_month == 0)) || $ktcupo_row->ktcupo_max_requests_day == 0)
            {
                echo "<li style=\"color:red\">Warning: Max. requests prevents requests from being received.</li>";

                $minor_error = true;
            }

            echo "</ul>";

            echo "<h4>Payment rate</h4>";
            echo "<ul>";

            //Payment rate
            $payment_row = PaymentRate::selectRaw("para_rate, para_discount_type, para_discount_rate")
                ->where("para_ktcupo_id", "=", $request->ktcupo_id)
                ->whereRaw("`para_date` <= NOW()")
                ->orderBy("para_date", "desc")
                ->first();

            if (count($payment_row) > 0)
            {
                echo "<li>Payment rate: " . $system->paymentCurrencyToken($ktcupo_row->po_pacu_code) . " " . $system->numberFormat($payment_row->para_rate, 2);

                if ($payment_row->para_discount_rate != 0)
                {
                    echo " (Discount: " . (($payment_row->para_discount_type == 1) ? $system->paymentCurrencyToken($ktcupo_row->po_pacu_code) . " " : "") . $system->numberFormat($payment_row->para_discount_rate, 2) . (($payment_row->para_discount_type == 2) ? "%" : "") . ")";
                }

                echo "</li>";
            } else
            {
                echo "<li>No payment rate";

                if ($ktcupo_row->ktcupo_free_trial == 0)
                {
                    echo " (<span style=\"color:red\">Warning: Payment rate required</span>)";

                    $minor_error = true;
                }

                echo "</li>";
            }

            echo "</ul>";

            if ($major_error)
            {
                echo $system->showMessage("Can't receive nor show requests, please review the linked portal settings.");

                return;
            }

            echo "<h4>Origins / Destinations</h4>";
            echo "<ul>";

            //Set group concat length
            DB::statement("SET SESSION group_concat_max_len = 1000000;");

            //Get origin
            $origin_row = KTCustomerPortalRegion::selectRaw("GROUP_CONCAT(`ktcupore_reg_id`) as `regions`")
                ->where("ktcupore_ktcupo_id", "=", $request->ktcupo_id)
                ->first();

            $origin_regions = explode(",", $origin_row->regions);

            if ($origin_regions)
            {
                if (($key = array_search("", $origin_regions)) !== false)
                {
                    unset($origin_regions[$key]);
                }
            }

            if (!empty($origin_regions))
            {
                echo "<li>Origins: " . count($origin_regions);
            } else
            {
                echo "<li style=\"color:red\">No origin regions selected!</li>";

                $major_error = true;
            }

            //Get destination
            if ($ktcupo_row->po_destination_type == 1)
            {
                $destination_countries_query = KTCustomerPortalCountry::selectRaw("DISTINCT `ktcupoco_co_code`")
                    ->where("ktcupoco_ktcupo_id", "=", $request->ktcupo_id)
                    ->get();

                $destination_countries = [];

                foreach ($destination_countries_query as $dc)
                {
                    $destination_countries[$dc->ktcupoco_co_code] = $dc->ktcupoco_co_code;
                }

                if (count($destination_countries) > 0)
                {
                    $destination_regions_query = Region::select("reg_id")
                        ->whereIn("reg_co_code", $destination_countries)
                        ->get();

                    $destination_regions = [];

                    foreach ($destination_regions_query as $dr)
                    {
                        $destination_regions[$dr->reg_id] = $dr->reg_id;
                    }
                } else
                {
                    $destination_regions = [];
                }
            } else
            {

                $destination_row = KTCustomerPortalRegionDestination::selectRaw("GROUP_CONCAT(`ktcuporede_reg_id`) as `regions`")
                    ->where("ktcuporede_ktcupo_id", "=", $request->ktcupo_id)
                    ->first();

                $destination_regions = explode(",", $destination_row->regions);
            }

            if ($destination_regions)
            {
                if (($key = array_search("", $destination_regions)) !== false)
                {
                    unset($destination_regions[$key]);
                }
            }

            if (!empty($destination_regions))
            {
                echo "<li>Destination: " . count((($ktcupo_row->po_destination_type == 1) ? $destination_countries : $destination_regions));
            } else
            {
                echo "<li style=\"color:red\">No destination regions selected!</li>";

                $major_error = true;
            }

            echo "</ul>";

            if ($major_error)
            {
                echo $system->showMessage("Can't receive requests, no origins and/or destinations selected.");

                return;
            }

            echo "<h4>Requests (Last 30 days)</h4>";

            //Get the requests
            if ($minor_error)
            {
                echo $system->showMessage("Can't receive requests (Minor errors). Number of requests in the last 30 days for these settings:", "secondary");
            } else
            {
                echo $system->showMessage("Requests in the last 30 days for these settings:", "secondary");
            }

            $types = [];

            if ($ktcupo_row->ktcupo_export_moves)
            {
                $types['Export'] = [$origin_regions, $destination_regions];
            }

            if ($ktcupo_row->ktcupo_import_moves)
            {
                $types['Import'] = [$destination_regions, $origin_regions];
            }

            echo "<ul>";

            $all_requests = [];

            foreach ($types as $type => $values)
            {
                $requests_query = \App\Models\Request::select("re_id")
                    ->where("re_request_type", $ktcupo_row->ktcupo_request_type)
                    ->whereIn("re_moving_size", array_keys($moving_sizes))
                    ->whereRaw("(`re_status` = 1 OR (`re_status` = 2 AND `re_rejection_reason` IN (2,3)))")
                    ->whereIn("re_reg_id_from", $values[0])
                    ->whereIn("re_reg_id_to", $values[1])
                    ->whereRaw("`re_timestamp` BETWEEN NOW() - INTERVAL 30 DAY AND NOW()")
                    ->whereRaw("(
							(`re_volume_m3` >= '" . $ktcupo_row->ktcupo_min_volume_m3 . "' OR `re_volume_ft3` >= '" . $ktcupo_row->ktcupo_min_volume_ft3 . "')
							OR
							(`re_volume_m3` = 0 OR `re_volume_ft3` = 0)
						)")
                    ->get();

                if ($ktcupo_row->ktcupo_business_only == 1)
                {
                    $requests_query = $requests_query->where("re_business", "=", 1);
                }

                $request_count = 0;
                $requests = [];
                $count_not_matched_5_times = 0;

                foreach ($requests_query as $request_row)
                {
                    $request_count++;
                    $requests[] = $request_row->re_id;
                    $all_requests[] = $request_row->re_id;

                    $count_query = KTRequestCustomerPortal::selectRaw("COUNT(*) as `count`")
                        ->where("ktrecupo_re_id", "=", $request_row->re_id)
                        ->first();

                    $count = $count_query->count;

                    if ($count <= 4)
                        $count_not_matched_5_times++;
                }

                echo "<li>{$type} requests: {$request_count}</li>";

                //Check average match
                if ($type === 'Export')
                {
                    $average_match = "Not available";
                    $total_matches = 0;

                    if ($request_count > 0)
                    {
                        $total_matches_query = KTRequestCustomerPortal::selectRaw("COUNT(*) as `matches`")
                            ->whereIn("ktrecupo_re_id", $requests)
                            ->where("ktrecupo_type", 1)
                            ->first();

                        $total_matches = $total_matches_query->matches;

                        if ($total_matches > 0)
                        {
                            $average_match = $total_matches / $request_count;
                        } else
                        {
                            $average_match = 0;
                        }

                        $average_match = $system->numberFormat($average_match, 2);
                    }

                    echo "<li>{$type} average match: {$average_match}</li><span id='portal_average_match_portal_level' style='display:none;'>{$average_match}</span>";
                }

                echo "<li>{$type} number of leads matched less than 5 times: {$count_not_matched_5_times}</li>";
            }




            $placement_results = [];
            $company_placement_results = [];
            $placement_total_leads = 0;

            $personal_placement_results = [
                1 => [
                    "amount" => 0,
                    "paid" => 0,
                    "avg_match" => 0,
                    "formula_prices" => 0
                ],
                2 => [
                    "amount" => 0,
                    "paid" => 0,
                    "avg_match" => 0,
                    "formula_prices" => 0
                ],
                3 => [
                    "amount" => 0,
                    "paid" => 0,
                    "avg_match" => 0,
                    "formula_prices" => 0
                ],
                4 => [
                    "amount" => 0,
                    "paid" => 0,
                    "avg_match" => 0,
                    "formula_prices" => 0
                ],
                5 => [
                    "amount" => 0,
                    "paid" => 0,
                    "avg_match" => 0,
                    "formula_prices" => 0
                ]
            ];

            $requests_in_lottery = 0;

            foreach ($all_requests as $req)
            {
                $matches = MatchStatistic::selectRaw("match_statistics.*, cu_company_name_business")
                    ->leftJoin("customers", "cu_id","mast_cu_id")
                    ->where("mast_re_id", $req)
                    ->where("mast_capping_limitation", 0)
                    ->get();

                if(count($matches))
                {
                    if(count($matches) > 5)
                    {
                        $requests_in_lottery++;
                    }
                    foreach ($matches as $match)
                    {
                        if($match->mast_match_position >= 3 && $match->mast_match_position <= 5){
                            $match_position = "3..5";
                        }else{
                            $match_position = $match->mast_match_position;
                        }
                        $placement_results[$match_position]["amount"] += 1;
                        $placement_results[$match_position]["paid"] += $match->mast_lead_price_netto;
                        $placement_results[$match_position]["avg_match"] += $match->mast_average_match;
                        $placement_results[$match_position]["formula_prices"] += $match->mast_formula_price;

                        $placement_total_leads++;

                        if($match->mast_matched == 1)
                        {
                            $company_placement_results[$match->mast_cu_id]["amount"] += 1;
                            if(count($matches) > 5){
                                $company_placement_results[$match->mast_cu_id]["amount_lottery"] += 1;
                            }
                            $company_placement_results[$match->mast_cu_id]["formula_price"] += $match->mast_formula_price;
                            $company_placement_results[$match->mast_cu_id]["paid"] += $match->mast_lead_price_netto;
                            $company_placement_results[$match->mast_cu_id]["avg_match"] += $match->mast_average_match;
                            $company_placement_results[$match->mast_cu_id]["customername"] = $match->cu_company_name_business;

                            if($match->mast_cu_id == $ktcupo_row->cu_id)
                            {
                                $personal_placement_results[$match->mast_match_position]["amount"] += 1;
                                $personal_placement_results[$match->mast_match_position]["paid"] += $match->mast_lead_price_netto;
                                $personal_placement_results[$match->mast_match_position]["avg_match"] += $match->mast_average_match;
                                $personal_placement_results[$match->mast_match_position]["formula_prices"] += $match->mast_formula_price;
                            }
                        }
                    }
                }
            }

            if($requests_in_lottery == 0){
                echo "No Request Data for this Portal!";
            }

            echo "<li>Number of total leads in lottery: {$requests_in_lottery}</li>";

            echo "</ul>";

            echo "";

            ksort($placement_results);
            ksort($personal_placement_results);


            echo "<h4 style='color:red;'>Testing purposes only!</h4>";

            echo "<ul>";

            echo "<li>Country: ".$ktcupo_row->co_en."</li>";
            if($ktcupo_row->ktcupo_request_type == RequestType::MOVE){
                if ($ktcupo_row->po_destination_type == DestinationType::INTMOVING)
                {
                    echo "<li>Min Price: " . $system->paymentCurrencyToken($ktcupo_row->po_pacu_code) . " " . $ktcupo_row->po_minimum_price_int . "</li>";
                    echo "<li>Max Price: " . $system->paymentCurrencyToken($ktcupo_row->po_pacu_code) . " " . $ktcupo_row->po_maximum_price_int . "</li>";
                } else
                {
                    echo "<li>Min LOCAL Price: " . $system->paymentCurrencyToken($ktcupo_row->po_pacu_code) . " " . $ktcupo_row->po_minimum_price_nat_local . "</li>";
                    echo "<li>Max LOCAL Price: " . $system->paymentCurrencyToken($ktcupo_row->po_pacu_code) . " " . $ktcupo_row->po_maximum_price_nat_local . "</li>";
                    echo "<li>Min LONG DISTANCE Price: " . $system->paymentCurrencyToken($ktcupo_row->po_pacu_code) . " " . $ktcupo_row->po_minimum_price_nat_long_distance . "</li>";
                    echo "<li>Max LONG DISTANCE Price: " . $system->paymentCurrencyToken($ktcupo_row->po_pacu_code) . " " . $ktcupo_row->po_maximum_price_nat_long_distance . "</li>";
                }
            }

            $winner_amount = 0;
            $losers_amount = 0;

            foreach ($placement_results as $index => $result)
            {
                if($index < 6)
                {
                    $average_paid_winners += $result["paid"] / $result["amount"];
                    $average_formula_winners += $result["formula_prices"] / $result["amount"];
                    $winner_amount++;
                }else{
                    $average_paid_losers +=  $result["paid"] / $result["amount"];
                    $average_formula_losers +=  $result["formula_prices"] / $result["amount"];
                    $losers_amount++;
                }
            }

            echo "<li>Average Top 5 Paid: ". $system->paymentCurrencyToken($ktcupo_row->po_pacu_code) . number_format($average_paid_winners / $winner_amount, 2)." (Formula Price: ".number_format($average_formula_winners / $winner_amount, 2).")</li>";
            echo "<li>Average Losers Paid: ". $system->paymentCurrencyToken($ktcupo_row->po_pacu_code) . number_format($average_paid_losers / $losers_amount, 2)." (Formula Price: ".number_format($average_formula_losers / $losers_amount, 2).")</li>";


            echo "</ul>";

            echo "<h4>Price information for this portal:</h4>";


            echo "<table class='table'>";
            echo "<tr>";
            echo "<th>Position</th>";
            echo "<th>Number of leads</th>";
            echo "<th>Percentage of leads</th>";
            echo "<th>Paid (avg)</th>";
            echo "<th>Average match</th>";
            echo "<th>Formula price</th>";
            echo "</tr>";

            $formula_price_losers = 0;
            $formula_price_losers_amount = 0;


            foreach ($placement_results as $index => $result)
            {
                if($index < 6)
                {
                    echo "<tr>";
                    echo "<td>" . $index . "</td>";
                    echo "<td>" . $result["amount"] . "</td>";
                    echo "<td>" . number_format($result["amount"] / $placement_total_leads * 100, 2) . "%</td>";
                    echo "<td>" . number_format($result["paid"] / $result["amount"], 2) . "</td>";
                    echo "<td>" . number_format($result["avg_match"] / $result["amount"], 2) . "</td>";
                    echo "<td>" . number_format($result["formula_prices"] / $result["amount"], 2) . "</td>";
                    echo "</tr>";
                }else{
                    $formula_price_losers += $result["formula_prices"] / $result["amount"];
                    $formula_price_losers_amount += $result["amount"];
                    $formula_price_losers_divide_amount += 1;
                }
            }
            echo "</table>";

            echo "<ul>";
            echo "<li>Average Formula Price for customers that didn't get the lead: ".number_format($formula_price_losers/$formula_price_losers_divide_amount, 2)." (". number_format($formula_price_losers_amount / $placement_total_leads * 100, 2)."%)</li>";
            echo "</ul>";

            $query_payment_rate = DB::table("payment_rates")
                ->where("para_ktcupo_id", $ktcupo_row->ktcupo_id)
                ->whereRaw("`para_date` <= NOW()");

            //Get correct NAT payment type
            if ($request->re_destination_type == DestinationType::NATMOVING)
            {
                $query_payment_rate->where("para_nat_type", $request->re_nat_type);
            }

            $query_payment_rate = $query_payment_rate
                ->orderBy("para_date", 'desc')
                ->first();

            $rqd = new RequestData();
            $average_match_30_days = ($rqd->calculateAverageMatch($ktcupo_row->cu_id) ?? 0);
            $claim_percentage_30_days = $rqd->getClaimRateOrNoClaimRate($ktcupo_row->cu_id);
            $lead_price = $system->calculatePaymentRate($query_payment_rate);
            $claim_adjusted_lead_price = ((1 - ($claim_percentage_30_days / 100)) * $lead_price);
            $adjusted_average_match = 1 - ($average_match_30_days / 5);
            if ($ktcupo_row->moda_quality_score_override == 1)
            {
                $correction_bonus = 0;
            } else
            {
                $correction_bonus = $adjusted_average_match * 0.625;
            }
            $formula_price = (1 + $correction_bonus) * $claim_adjusted_lead_price;

            echo "<h4>Company information:</h4>";


            echo "<ul>";

            echo "<li>Average match: " . number_format($average_match_30_days , 2). "<span id='portal_avg_match' style='display:none'>".number_format($average_match_30_days , 2)."</span></li>";
            echo "<li>Payment rate: " . $system->paymentCurrencyToken($ktcupo_row->po_pacu_code) . " " . $system->numberFormat($lead_price, 2)."<span id='portal_payment_rate' style='display:none'>".$system->paymentCurrencyToken($ktcupo_row->po_pacu_code) . " " . $system->numberFormat($lead_price, 2)."</span>".' <a href="#" class="view_formula_calculation mb-2" data-ktcupo_id="{{$portal->ktcupo_id}}" data-ktcupo_type="{{$portal->ktcupo_type}}">Calculate</a>'."</li>";
            echo "<li>Claim percentage: " . number_format($claim_percentage_30_days , 2). "% "."<span id='portal_claim_percentage' style='display:none'>".number_format($claim_percentage_30_days , 2)."%</span>"."</li>";
            echo "<li>Claim adjusted lead price: " . $system->paymentCurrencyToken($ktcupo_row->po_pacu_code) . " " . number_format($claim_adjusted_lead_price , 2)."<span id='portal_adjusted_lead_price' style='display:none'>".$system->paymentCurrencyToken($ktcupo_row->po_pacu_code) . " " . number_format($claim_adjusted_lead_price , 2)."</span>". "</li>";
            if ($ktcupo_row->moda_quality_score_override == 1)
            {
                echo "<li>Correction bonus: " . number_format($correction_bonus , 2). " (Quality score is 1)"."<span id='portal_correction_bonus' style='display:none'>".number_format($correction_bonus , 2). " (Quality score is 1)"."</span>"."</li>";
            }else{
                echo "<li>Correction bonus: " . number_format($correction_bonus*100 , 2). "%"."<span id='portal_correction_bonus' style='display:none'>".number_format($correction_bonus*100 , 2). "%"."</span>"."</li>";
            }
            echo "<li>Formula price: <strong>". $system->paymentCurrencyToken($ktcupo_row->po_pacu_code) . number_format($formula_price , 2)."<span id='portal_formula_price' style='display:none'>".$system->paymentCurrencyToken($ktcupo_row->po_pacu_code) . number_format($formula_price , 2)."</span>". "</strong></li>";
            echo "<span id='portal_currency_token' style='display: none;'>".$system->paymentCurrencyToken($ktcupo_row->po_pacu_code)."</span>";
            echo "<span id='portal_pacu_code' style='display: none;'>".$ktcupo_row->po_pacu_code."</span>";

            //Get leads of this customers' portal of last 30 days
            $amount_of_matched_leads = MatchStatistic::selectRaw("COUNT(*) as amount")
                ->leftJoin("requests", "re_id", "mast_re_id")
                ->whereIn("mast_re_id", $all_requests)
                ->where("mast_ktcupo_id", $ktcupo_row->ktcupo_id)
                ->where("mast_matched", 1)
                ->where("mast_capping_limitation", 0)
                ->whereRaw("`re_timestamp` BETWEEN NOW() - INTERVAL 30 DAY AND NOW()")
                ->first();

            $leads_and_not_matched_leads = MatchStatistic::selectRaw("COUNT(*) as amount")
                ->leftJoin("requests", "re_id", "mast_re_id")
                ->whereIn("mast_re_id", $all_requests)
                ->where("mast_ktcupo_id", $ktcupo_row->ktcupo_id)
                ->where("mast_capping_limitation", 0)
                ->whereRaw("`re_timestamp` BETWEEN NOW() - INTERVAL 30 DAY AND NOW()")
                ->first();

            $percentage_receieved =(($amount_of_matched_leads->amount / $leads_and_not_matched_leads->amount) * 100);

            echo "<li>Amount received by this company on this portal in the last 30 days: ".$amount_of_matched_leads->amount." out of ".$leads_and_not_matched_leads->amount." (".number_format($percentage_receieved, 2)."%)</li>";
            echo "</ul>";

            echo "<br>";
            echo "<h4>Price information for leads of <i>this customer</i> in this portal:</h4>";

            echo "<table class='table'>";
            echo "<tr>";
            echo "<th>Position (of customer)</th>";
            echo "<th>Number of leads</th>";
            echo "<th>Paid (avg)</th>";
            echo "<th>Average match</th>";
            echo "<th>Formula price</th>";
            echo "</tr>";

            foreach ($personal_placement_results as $index => $personal_result)
            {
                echo "<tr>";
                echo "<td>" . $index . "</td>";
                echo "<td>" . $personal_result["amount"] . "</td>";
                echo "<td>" . number_format($personal_result["paid"] / $personal_result["amount"], 2) . "</td>";
                echo "<td>" . number_format($personal_result["avg_match"] / $personal_result["amount"], 2) . "</td>";
                echo "<td>" . number_format($personal_result["formula_prices"] / $personal_result["amount"], 2) . "</td>";
                echo "</tr>";
            }
            echo "</table>";

            echo "<h4>The companies that got the leads for this portal:</h4>";

            echo "<table class='table'>";
            echo "<tr>";
            echo "<th>Customer</th>";
            echo "<th># of leads</th>";
            echo "<th># in lottery</th>";
            echo "<th>Average price</th>";
            echo "<th>Average match</th>";
            echo "<th>Average Formula price</th>";
            echo "</tr>";

            usort($company_placement_results, function($a, $b) {
                return $b['amount'] <=> $a['amount'];
            });

            $count = 20;

            foreach ($company_placement_results as $company_results)
            {
                if($count <= 0){break;}
                $count--;

                echo "<tr>";
                echo "<td>" . $company_results["customername"] . "</td>";
                echo "<td>" . $company_results["amount"] . "</td>";
                echo "<td>" . ($company_results["amount_lottery"] ?? 0) . "</td>";
                echo "<td>" . number_format($company_results["paid"] / $company_results["amount"], 2) . "</td>";
                echo "<td>" . number_format($company_results["avg_match"] / $company_results["amount"], 2) . "</td>";
                echo "<td>" . number_format($company_results["formula_price"] / $company_results["amount"], 2) . "</td>";
                echo "</tr>";
            }
            echo "</table>";

        }

    }

    public function customerPortalSummaryCalculateFormula(Request $request)
    {
        $avg_match = str_replace(",", ".", $request->average_match);

        $mover_data = MoverData::where("moda_cu_id", $request->cu_id)->first();

        $rqd = new RequestData();

        $average_match_30_days = $avg_match;
        $claim_percentage_30_days = $rqd->getClaimRateOrNoClaimRate($request->cu_id);
        $lead_price = $request->payment_rate;
        $claim_adjusted_lead_price = ((1 - ($claim_percentage_30_days / 100)) * $lead_price);
        $adjusted_average_match = 1 - ($average_match_30_days / 5);

        if ($mover_data->moda_quality_score_override == 1)
        {
            $correction_bonus = 0;
        } else
        {
            $correction_bonus = $adjusted_average_match * 0.625;
        }
        $formula_price = (1 + $correction_bonus) * $claim_adjusted_lead_price;


        $array = [
            'average_match' => number_format($average_match_30_days , 2),
            'payment_rate' => $request->payment_rate,
            'claim_percentage' => number_format($claim_percentage_30_days , 2)."%",
            'claim_adjusted_lead_price' => number_format($claim_adjusted_lead_price , 2),
            'correction_bonus' => (($mover_data->moda_quality_score_override == 1) ? number_format($correction_bonus , 2). " (Quality score is 1)" : number_format($correction_bonus*100 , 2)."%"),
            'formula_price' => number_format($formula_price , 2)

        ];

        echo json_encode($array);
    }

    public function customerPortalSummaryLeadsStore(Request $request)
    {
        $ktcupo_row = KTCustomerPortal::join("portals", "ktcupo_po_id", "po_id")
            ->join("customers", "ktcupo_cu_id", "cu_id")
            ->join("mover_data", "ktcupo_cu_id", "moda_cu_id")
            ->where("ktcupo_id", $request->ktcupo_id)
            ->where("cu_deleted", 0)
            ->first();


        if (count($ktcupo_row) > 0 && $ktcupo_row->ktcupo_export_moves)
        {
            if ($ktcupo_row->po_destination_type == 1)
            {
                $requests_query = \App\Models\Request::select("re_id")
                    ->whereRaw("(`re_status` = 1 OR (`re_status` = 2 AND `re_rejection_reason` IN (2,3)))")
                    ->where("re_co_code_from", $ktcupo_row->po_co_code)
                    ->whereRaw("`re_timestamp` BETWEEN NOW() - INTERVAL 30 DAY AND NOW()")
                    ->get();

                $request_count = 0;
                $requests = [];
                $count_not_matched_5_times = 0;

                foreach ($requests_query as $request_row)
                {
                    $request_count++;
                    $requests[] = $request_row->re_id;

                    $count_query = KTRequestCustomerPortal::selectRaw("COUNT(*) as `count`")
                        ->where("ktrecupo_re_id", "=", $request_row->re_id)
                        ->first();

                    $count = $count_query->count;

                    if ($count <= 4)
                        $count_not_matched_5_times++;
                }

                echo "EXPORT: " . $count_not_matched_5_times . " more leads were available in the last 30 days. (Not matched 5 times for origin " . $ktcupo_row->po_co_code . ")<br />";
            } elseif ($ktcupo_row->po_destination_type == 2)
            {
                $requests_query = \App\Models\Request::select("re_id")
                    ->whereRaw("(`re_status` = 1 OR (`re_status` = 2 AND `re_rejection_reason` IN (2,3)))")
                    ->whereIn("re_co_code_from", $ktcupo_row->po_co_code)
                    ->whereIn("re_co_code_to", $ktcupo_row->po_co_code)
                    ->whereRaw("`re_timestamp` BETWEEN NOW() - INTERVAL 30 DAY AND NOW()")
                    ->get();

                $request_count = 0;
                $requests = [];
                $count_not_matched_5_times = 0;

                foreach ($requests_query as $request_row)
                {
                    $request_count++;
                    $requests[] = $request_row->re_id;

                    $count_query = KTRequestCustomerPortal::selectRaw("COUNT(*) as `count`")
                        ->where("ktrecupo_re_id", "=", $request_row->re_id)
                        ->first();

                    $count = $count_query->count;

                    if ($count <= 4)
                        $count_not_matched_5_times++;
                }

                echo "EXPORT: " . $count_not_matched_5_times . " more leads were available in the last 30 days. (Not matched 5 times NAT-" . $ktcupo_row->po_co_code . ")<br />";
            }
        }

        if (count($ktcupo_row) > 0 && $ktcupo_row->ktcupo_import_moves)
        {
            if ($ktcupo_row->po_destination_type == 1)
            {
                $requests_query = \App\Models\Request::select("re_id")
                    ->whereRaw("(`re_status` = 1 OR (`re_status` = 2 AND `re_rejection_reason` IN (2,3)))")
                    ->where("re_co_code_to", $ktcupo_row->po_co_code)
                    ->whereRaw("`re_timestamp` BETWEEN NOW() - INTERVAL 30 DAY AND NOW()")
                    ->get();

                $request_count = 0;
                $requests = [];
                $count_not_matched_5_times = 0;

                foreach ($requests_query as $request_row)
                {
                    $request_count++;
                    $requests[] = $request_row->re_id;

                    $count_query = KTRequestCustomerPortal::selectRaw("COUNT(*) as `count`")
                        ->where("ktrecupo_re_id", "=", $request_row->re_id)
                        ->first();

                    $count = $count_query->count;

                    if ($count <= 4)
                        $count_not_matched_5_times++;
                }

                echo "IMPORT: " . $count_not_matched_5_times . " more leads were available in the last 30 days. (Not matched 5 times for destination " . $ktcupo_row->po_co_code . ")<br />";
            }
            /*elseif ($ktcupo_row->po_destination_type == 2)
            {
                $requests_query = \App\Models\Request::select("re_id")
                    ->whereRaw("(`re_status` = 1 OR (`re_status` = 2 AND `re_rejection_reason` IN (2,3)))")
                    ->whereIn("re_co_code_from", $ktcupo_row->po_co_code)
                    ->whereIn("re_co_code_to", $ktcupo_row->po_co_code)
                    ->whereRaw("`re_timestamp` BETWEEN NOW() - INTERVAL 30 DAY AND NOW()")
                    ->get();

                $request_count = 0;
                $requests = [];
                $count_not_matched_5_times = 0;

                foreach ($requests_query as $request_row)
                {
                    $request_count++;
                    $requests[] = $request_row->re_id;

                    $count_query = KTRequestCustomerPortal::selectRaw("COUNT(*) as `count`")
                        ->where("ktrecupo_re_id", "=", $request_row->re_id)
                        ->first();

                    $count = $count_query->count;

                    if ($count <= 4)
                        $count_not_matched_5_times++;
                }

                echo "IMPORT: ".$count_not_matched_5_times." more leads were available in the last 30 days. (Not matched 5 times NAT-".$ktcupo_row->po_co_code.")<br />";
            }*/
        }
    }

    public function viewNewsletterPreviewOrSend(Request $request)
    {
        $sp_newsletter_blocks = ServiceProviderNewsletterBlock::where("seprnebl_id", "=", $request->seprnebl_id)->first();

        if (count($sp_newsletter_blocks) > 0)
        {
            $mail = new Mail();
            $content = "";

            $row_sp_newsletter_blocks = $sp_newsletter_blocks;

            if (!empty($row_sp_newsletter_blocks->seprnebl_title))
            {
                $content .= $mail->table();
                $content .= "<tr><td style=\"" . $mail->styleBar('#2B3C4D') . "\">" . $row_sp_newsletter_blocks->seprnebl_title . "</td></tr>";
                $content .= "</table>";
            }

            $content .= $mail->table("center", "100%", 13, "background: #F0F0F0; padding: 10px;");
            $content .= "<tr><td>";
            $content .= $row_sp_newsletter_blocks->seprnebl_content;
            $content .= "</td></tr></table><br />";

            $us_email = User::where("us_id", "=", Auth::id())->first()->us_email;

            $fields = [
                "request" => [
                    "re_id" => 0,
                    "re_email" => $us_email
                ],
                "subject" => "Subject",
                "intro" => "Intro",
                "content" => $content
            ];

            if (isset($request->mail) && $request->mail == 1)
            {
                $mail->send("newsletter", "EN", $fields);
                echo "<div class='alert alert-success' role='alert'>The preview has been sent to your email successfully!</div>";
            }

            echo "<iframe srcdoc=\"" . str_replace("\"", "'", $mail->send("newsletter", "EN", $fields, null, null, null, true)) . "\" width=\"100%\" height=\"690\" frameborder=\"0\"></iframe>";
        }
    }

    public function getMessagesCount(Request $request)
    {
        return Message::selectRaw("COUNT(*) as `count`")->where("mes_us_id", "=", $request->user_id)->where("mes_read", "=", 0)->first()->count;
    }

    public function getCallsCount(Request $request)
    {
        return PlannedCall::selectRaw("COUNT(*) as `count`")->whereDate("plca_planned_date", "<=", Carbon::now()->toDateString())->where("plca_status", "=", 0)->where("plca_us_id", "=", $request->user_id)->first()->count;
    }

    public function planCall(Request $request)
    {
        $call = new PlannedCall();

        $call->plca_cu_id = $request->customer;
        $call->plca_us_id = $request->user;
        $call->plca_status = PlannedCallStatus::OPEN;
        $call->plca_reason = PlannedCallReason::SIX_MONTHS_FOLLOW_UP;
        $call->plca_planned_date = $request->date;

        $call->save();

        return 1;
    }

    public function getCallNotifications(Request $request)
    {
        $calls = PlannedCall::with("customer")->whereDate("plca_planned_date", "<=", Carbon::now()->toDateString())->where("plca_status", 0)->where("plca_us_id", "=", $request->user_id)->limit(5)->get();

        $return_html = '';

        foreach ($calls as $call)
        {

            if ($call->plca_planned_date < date("Y-m-d"))
            {
                $icon = "fa-exclamation-circle text-warning";
                $word = "had";
            } else
            {
                $word = "have";
            }

            if ($call->plca_type == PlannedCallType::PHONE)
            {
                $type = 'call';
                if (!$icon)
                {
                    $icon = "fa-phone text-info";
                }
            } else
            {
                $type = 'email';
                if (!$icon)
                {
                    $icon = "fa-envelope text-info";
                }
            }


            $return_html .=
                '<li>
                    <a class="text-dark media py-2" href="/calendar/' . $call->plca_id . '/edit">
                        <div class="mx-3">
                            <i class="fa fa-fw ' . $icon . '"></i>
                        </div>
                        <div class="media-body font-size-sm pr-2">
                            <div class="font-w600">You ' . $word . ' a scheduled ' . $type . ' with <i>' . $call->customer->cu_company_name_business . '</i></div>
                            <div class="text-muted font-italic">' . $call->plca_planned_date . '</div>
                        </div>
                    </a>
                </li>';
        }

        return $return_html;


    }

    public function getScreenshot(Request $request, $re_id, $name)
    {
        $file = env('SHARED_FOLDER') . "uploads/premium_leads_screenshots/" . $re_id . "/" . $name;

        if (file_exists($file))
        {
            $image_info = getimagesize($file);

            if ($image_info['mime'] == "image/jpeg" || $image_info['mime'] == "image/png")
            {
                header("Content-Type: " . $image_info['mime']);

                return readfile($file);
            }
        }
    }


    public function getInvoiceFields(Request $request)
    {

        $row = DB::table("invoices")
            ->where("in_id", $request->invoice)
            ->first();

        if (!empty($row))
        {
            $customer_ledger_account = DB::table("kt_bank_line_invoice_customer_ledger_account")
                ->where("ktbaliinculeac_in_id", $row->in_id)
                ->get();

            $amount_left = $row->in_amount_netto_eur;
            $amount_left_FC = $row->in_amount_netto;

            foreach ($customer_ledger_account as $row_bank_line_invoice_customer_ledger_account)
            {
                $amount_left -= $row_bank_line_invoice_customer_ledger_account->ktbaliinculeac_amount;
                $amount_left_FC -= $row_bank_line_invoice_customer_ledger_account->ktbaliinculeac_amount_FC;
            }

            $row->amount_left = $amount_left;
            $row->amount_left_FC = $amount_left_FC;

            echo json_encode($row);
        }
    }

    public function reportProgress(Request $request)
    {
        $rep_id = $request->report_id;
        $repr_id = (!empty($request->repr_id) && $request->repr_id != "null" ? $request->repr_id : null);

        //Get the output
        ob_start();

        Log::debug(Reporting::displayReportStatus($rep_id, $repr_id));
        Log::debug($request);

        $report_row = ReportProgress::leftJoin("users", "repr_us_id", "us_id")->where("repr_id", $repr_id)->first();
        Log::debug($report_row);
        $single_row = Reporting::displaySingleReportStatus(System::databaseToArray($report_row));

        $reports = ob_get_clean();

        //Output results
        $results = ["reports" => $reports];
        if ($single_row !== null)
        {
            ob_start();

            //$results["single-report"] = ob_get_clean();
            $results["single-report"] = $single_row;
        } elseif ($repr_id !== null)
        {
            $results["single-report-deleted"] = true;
        }

        echo json_encode($results);
    }

    public function getMovingSizesJSON(Request $request)
    {
        echo json_encode(DataIndex::movingSizes($request->request_type));
    }

    public function getPortalOptions(Request $request)
    {
        $data = [];

        if (isset($request->continents))
        {
            $data = Data::continents(true);
        } else
        {
            $portal = (isset($request->portal) ? $request->portal : false);

            if ($portal !== false)
            {
                $data = Data::portalRegions($portal, true, false);
            } else if (isset($request->greater_regions))
            {
                $type = $request->type ?? 1;

                $data = Data::greaterRegions(null, $type == 0 ? null : $type, true);
            } else if (isset($request->regions))
            {
                $type = @System::useOr($request->type, 1);

                $data = Data::regions(null, $type == 0 ? null : $type, true);
            } else if (isset($request->countries))
            {
                $data = Data::getCountries(LANGUAGE, true);
                $array_europe = Data::getEUcountries(LANGUAGE);
                $array_schengen_area = Data::getSchengenAreaCountries(LANGUAGE);
                $data['All Schengen Area Members'] = $array_schengen_area;
                $data['All Europe Members'] = $array_europe;
            } else if (isset($request->macroregions))
            {
                $data = Data::getMacroRegions(true);
            }
        }

        echo json_encode($data);
    }

    public function getAdyenCCDetails(Request $request)
    {
        if (!$request->customer)
        {
            die(header("HTTP/1.0 400 No customer"));
        }

        $system = new System();


        $result = ['cards' => []];
        $customer = intval($request->customer);

        $card_details = DB::table("customers")
            ->select("cu_id", "cu_pacu_code", "adyen_card_details.*")
            ->leftJoin("adyen_card_details", "cu_id", "adcade_cu_id")
            ->where("cu_id", $customer)
            ->where("adcade_removed", 0)
            ->get();

        foreach ($card_details as $row)
        {
            $result['cu_id'] = $row->cu_id;
            $result['cu_pacu_code'] = $row->cu_pacu_code;

            $result['cards'][$row->adcade_id] = "Card: {$row->adcade_card_last_digits}" .
                " (Expires: {$row->adcade_card_expiry_year}/{$row->adcade_card_expiry_month})";
        }

        if (empty($result['cards']))
        {
            die(header("HTTP/1.0 400 Can't find any credit cards"));
        }

        $result['pacu_token'] = $system->paymentCurrencyToken($result['cu_pacu_code']);

        echo json_encode($result);
    }

    public function getCustomerFields(Request $request)
    {
        $query = DB::table("customers")
            ->where("cu_id", $request->customer)
            ->where("cu_deleted", 0)
            ->first();

        if (!empty($query))
        {
            echo json_encode($query);
        }
    }

    public function customerLogoRemove(Request $request)
    {
        $customer = Customer::where("cu_id", $request->cu_id)->first();

        if (count($customer) > 0)
        {

            // Remove the logo from the erp
            unlink(env("SHARED_FOLDER") . 'uploads/logos/' . $customer->cu_logo);

            $customer->cu_logo = "";
            $customer->save();
            echo "success";
        } else
        {
            echo "failed";
        }
    }

    public function customerProgressReset(Request $request)
    {
        KTCustomerProgress::where("ktcupr_cu_id", $request->cu_id)->where("ktcupr_type", ">", $request->to)->update(['ktcupr_deleted' => 1]);
    }

    public function validateWebsiteURLs(Request $request)
    {
        $system = new System();

        //Trim slashes at end of the string
        $url_erp = trim($request->url_erp, "/");
        $url_other = trim($request->url_other, "/");

        //Make empty array to send filled in array at end of the function
        $array = [
            'html_string' => '',
            'website_validate_string' => '',
            'variables' => '',
            'final_erp' => '',
            'final_other' => ''
        ];

        if (empty($url_erp) && empty($url_other))
        {
            //If ERP url and Google URL are both empty

            $array = [
                'error' => 'no_urls',
                'html_string' => '<div class="col-sm-12">There is no URLs known for this company. Validate this company manually!</div>',
                'website_validate_string' => '<div class="col-sm-12">There is no URLs known for this company. Validate this company manually!</div>',
                'variables' => "",
                'final_erp' => '',
                'final_other' => ''
            ];
        } elseif (!empty($url_erp) && empty($url_other))
        {
            //If we got an ERP url, but no Google URL known

            //Get ERP domain from URL
            $domain_erp_current = $system->getDomainFromURL($url_erp);

            //Open URL
            $ch = curl_init($url_erp);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            $html = curl_exec($ch);

            //URL AFTER REDIRECT; This will be the final URL for ERP
            $redirectURL = trim(curl_getinfo($ch, CURLINFO_EFFECTIVE_URL), "/");

            curl_close($ch);

            //Status must be OK
            if ($html != false)
            {

                //Check if there are any redirects in the ERP url
                if ($url_erp == $redirectURL)
                {
                    //No redirects
                    $array = [
                        'html_string' => '<div class="col-sm-6" style="color:green;">Current ERP URL is the best one :)<span id="final_domain" style="display:none;" data-domain="' . $system->getDomainFromURL($redirectURL) . '"></span></div><div class="col-sm-6">No Google URL scraped.</div>',
                        'website_validate_string' => '<div class="col-sm-6" style="color:green;">Current ERP URL is the best one :)<span id="final_domain" style="display:none;" data-domain="%s"></span></div><div class="col-sm-6">No Google URL scraped.</div>',
                        'variables' => $system->getDomainFromURL($redirectURL),
                        'final_erp' => $redirectURL,
                        'final_other' => ''
                    ];
                } else
                {
                    //Redirect found. Get domain name after redirect
                    $domain_erp_after_redirect = $system->getDomainFromURL($redirectURL);

                    //Check if domain is the same as after the redirect
                    if ($domain_erp_current == $domain_erp_after_redirect)
                    {
                        //Same domain
                        $array = [
                            'html_string' => '<div class="col-sm-6">There is a redirect in the current ERP url. The best URL is: ' . $redirectURL . '<span id="validated_url_erp" data-url="' . $redirectURL . '">✍</span><span id="final_domain_erp" style="display:none;" data-domain="' . $system->getDomainFromURL($redirectURL) . '"></span></div><div class="col-sm-6">No Google URL.</div>',
                            'website_validate_string' => '<div class="col-sm-6">There is a redirect in the current ERP url. The best URL is: %s<span id="validated_url_erp" data-url="%s">✍</span><span id="final_domain_erp" style="display:none;" data-domain="%s"></span></div><div class="col-sm-6">No Google URL.</div>',
                            'variables' => $redirectURL . "," . $redirectURL . "," . $system->getDomainFromURL($redirectURL),
                            'final_erp' => $redirectURL,
                            'final_other' => ''
                        ];
                    } else
                    {
                        //Another URL
                        $array = [
                            'html_string' => '<div class="col-sm-6">There is found a redirect to another domain. Redirected URL is: ' . $redirectURL . '<span id="validated_url_erp" data-url="' . $redirectURL . '">✍</span><span id="final_domain_erp" style="display:none;" data-domain="' . $system->getDomainFromURL($redirectURL) . '"></span></div><div class="col-sm-6">No Google URL.</div>',
                            'website_validate_string' => '<div class="col-sm-6">There is found a redirect to another domain. Redirected URL is: %s<span id="validated_url_erp" data-url="%s">✍</span><span id="final_domain_erp" style="display:none;" data-domain="%s"></span></div><div class="col-sm-6">No Google URL.</div>',
                            'variables' => $redirectURL . "," . $redirectURL . "," . $system->getDomainFromURL($redirectURL),
                            'final_erp' => $redirectURL,
                            'final_other' => ''
                        ];
                    }
                }
            } else
            {
                //URL IS NOT WORKING
                $array = [
                    'error' => 'url_not_working',
                    'html_string' => '<div class="col-sm-12">Current ERP URL is not working. There is also no URL which we received from Google. Validate this company manually!</div>',
                    'website_validate_string' => '<div class="col-sm-12">Current ERP URL is not working. There is also no URL which we received from Google. Validate this company manually!</div>',
                    'variables' => "",
                    'final_erp' => '',
                    'final_other' => ''
                ];
            }

        } elseif (empty($url_erp) && !empty($url_other))
        {
            //If we got a Google URL, but no ERP url known.

            //Get domain from the Google URL
            $domain_other_current = $system->getDomainFromURL($url_other);

            //Open the Google URL
            $ch = curl_init($url_other);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            $html = curl_exec($ch);

            //Final Google URL (after redirects)
            $redirectURL = trim(curl_getinfo($ch, CURLINFO_EFFECTIVE_URL), "/");

            curl_close($ch);

            //Status must be OK
            if ($html != false)
            {
                //Check if the Google URL is still the same, and not being redirected
                if ($url_other == $redirectURL)
                {
                    //No redirects
                    $array = [
                        'html_string' => '<div class="col-sm-6">No ERP URL known.</div><div class="col-sm-6" style="color:green;">Current Google URL is the best one :)<span id="final_domain_other" style="display:none;" data-domain="' . $system->getDomainFromURL($redirectURL) . '"></span></div>',
                        'website_validate_string' => '<div class="col-sm-6">No ERP URL known.</div><div class="col-sm-6" style="color:green;">Current Google URL is the best one :)<span id="final_domain_other" style="display:none;" data-domain="%s"></span></div>',
                        'variables' => $system->getDomainFromURL($redirectURL),
                        'final_erp' => '',
                        'final_other' => $redirectURL
                    ];
                } else
                {
                    //Redirected

                    //Get domain from the redirected URL
                    $domain_other_after_redirect = $system->getDomainFromURL($redirectURL);

                    //Check if domain is still the same or not
                    if ($domain_other_current == $domain_other_after_redirect)
                    {
                        //Same domain
                        $array = [
                            'html_string' => '<div class="col-sm-6">No ERP URL.</div><div class="col-sm-6">There is a redirect in the scraped Google url. The best URL is: ' . $redirectURL . '<span id="validated_url_other" data-url="' . $redirectURL . '">✍</span><span id="final_domain_other" style="display:none;" data-domain="' . $system->getDomainFromURL($redirectURL) . '"></span></div>',
                            'website_validate_string' => '<div class="col-sm-6">No ERP URL.</div><div class="col-sm-6">There is a redirect in the scraped Google url. The best URL is: %s<span id="validated_url_other" data-url="%s">✍</span><span id="final_domain_other" style="display:none;" data-domain="%s"></span></div>',
                            'variables' => $redirectURL . "," . $redirectURL . "," . $system->getDomainFromURL($redirectURL),
                            'final_erp' => '',
                            'final_other' => $redirectURL
                        ];
                    } else
                    {
                        //Another URL
                        $array = [
                            'html_string' => '<div class="col-sm-6">No ERP URL.</div><div class="col-sm-6">There is found a redirect to another domain. Redirected URL is: ' . $redirectURL . '<span id="validated_url_other" data-url="' . $redirectURL . '">✍</span><span id="final_domain_other" style="display:none;" data-domain="' . $system->getDomainFromURL($redirectURL) . '"></span></div>',
                            'website_validate_string' => '<div class="col-sm-6">No ERP URL.</div><div class="col-sm-6">There is found a redirect to another domain. Redirected URL is: %s<span id="validated_url_other" data-url="%s">✍</span><span id="final_domain_other" style="display:none;" data-domain="%s"></span></div>',
                            'variables' => $redirectURL . "," . $redirectURL . "," . $system->getDomainFromURL($redirectURL),
                            'final_erp' => '',
                            'final_other' => $redirectURL
                        ];
                    }
                }
            } else
            {
                //URL NOT WORKING
                $array = [
                    'error' => 'url_not_working',
                    'html_string' => '<div class="col-sm-12">Scraped Google URL is not working. There is also no URL found in the ERP for this customer. Validate this company manually!</div>',
                    'website_validate_string' => '<div class="col-sm-12">Scraped Google URL is not working. There is also no URL found in the ERP for this customer. Validate this company manually!</div>',
                    'variables' => "",
                    'final_erp' => '',
                    'final_other' => ''
                ];
            }
        } elseif (!empty($url_erp) && !empty($url_other) && $url_erp == $url_other)
        {
            //If we got an ERP and Google URL, and these URLs are exactly the same.

            //Get domain from the URLs
            $domain_current = $system->getDomainFromURL($url_erp);

            //Open the URL
            $ch = curl_init($url_erp);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            $html = curl_exec($ch);

            //URL AFTER REDIRECT
            $redirectURL = trim(curl_getinfo($ch, CURLINFO_EFFECTIVE_URL), "/");

            curl_close($ch);

            //Status must be OK
            if ($html != false)
            {
                //Check if URL has redirects
                if ($url_other == $redirectURL)
                {
                    //No redirects
                    $array = [
                        'html_string' => '<div class="col-sm-12" style="color:green;">ERP and Google URL are the same. The current URLs are the best URLs<span id="final_domain" style="display:none;" data-domain="' . $system->getDomainFromURL($redirectURL) . '"></span></div>',
                        'website_validate_string' => '<div class="col-sm-12" style="color:green;">ERP and Google URL are the same. The current URLs are the best URLs<span id="final_domain" style="display:none;" data-domain="%s"></span></div>',
                        'variables' => $system->getDomainFromURL($redirectURL),
                        'final_erp' => $redirectURL,
                        'final_other' => $redirectURL
                    ];
                } else
                {
                    //URL has redirects

                    //Chec
                    $domain_after_redirect = $system->getDomainFromURL($redirectURL);

                    if ($domain_current == $domain_after_redirect)
                    {
                        //Same domain
                        $array = [
                            'html_string' => '<div class="col-sm-12">ERP and Google URL are the same. There is a redirect in the URL. The best URL is: ' . $redirectURL . '<span id="validated_url_erp" data-url="' . $redirectURL . '">✍</span><span id="final_domain" style="display:none;" data-domain="' . $system->getDomainFromURL($redirectURL) . '"></span></div>',
                            'website_validate_string' => '<div class="col-sm-12">ERP and Google URL are the same. There is a redirect in the URL. The best URL is: %s<span id="validated_url_erp" data-url="%s">✍</span><span id="final_domain" style="display:none;" data-domain="%s"></span></div>',
                            'variables' => $redirectURL . "," . $redirectURL . "," . $system->getDomainFromURL($redirectURL),
                            'final_erp' => $redirectURL,
                            'final_other' => $redirectURL
                        ];
                    } else
                    {
                        //Another URL
                        $array = [
                            'html_string' => '<div class="col-sm-12">ERP and Google URL are the same. There is found a redirect to another domain. Redirected URL is: ' . $redirectURL . '<span id="validated_url_erp" data-url="' . $redirectURL . '">✍</span><span id="final_domain" style="display:none;" data-domain="' . $system->getDomainFromURL($redirectURL) . '"></span></div>',
                            'website_validate_string' => '<div class="col-sm-12">ERP and Google URL are the same. There is found a redirect to another domain. Redirected URL is: %s<span id="validated_url_erp" data-url="%s">✍</span><span id="final_domain" style="display:none;" data-domain="%s"></span></div>',
                            'variables' => $redirectURL . "," . $redirectURL . "," . $system->getDomainFromURL($redirectURL),
                            'final_erp' => $redirectURL,
                            'final_other' => $redirectURL
                        ];
                    }
                }
            } else
            {
                //URL NOT WORKING
                $array = [
                    'error' => 'url_not_working',
                    'html_string' => '<div class="col-sm-12">ERP and Google URL is the same and this URL is not working. Validate this company manually!</div>',
                    'website_validate_string' => '<div class="col-sm-12">ERP and Google URL is the same and this URL is not working. Validate this company manually!</div>',
                    'variables' => "",
                    'final_erp' => '',
                    'final_other' => ''
                ];
            }
        } else
        {
            //We have 2 URLs. ERP and Google URL
            //Validate ERP URL

            $domain_erp_current = $system->getDomainFromURL($url_erp);

            $ch = curl_init($url_erp);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            $html_erp = curl_exec($ch);

            //URL AFTER REDIRECT
            $redirectURL_erp = trim(curl_getinfo($ch, CURLINFO_EFFECTIVE_URL), "/");

            curl_close($ch);

            //Google URL
            $domain_other_current = $system->getDomainFromURL($url_other);

            $ch = curl_init($url_other);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            $html_other = curl_exec($ch);

            //URL AFTER REDIRECT
            $redirectURL_other = trim(curl_getinfo($ch, CURLINFO_EFFECTIVE_URL), "/)");

            curl_close($ch);

            if ($html_erp == false && $html_other != false)
            {
                //Only Other is working
                if ($url_other == $redirectURL_other)
                {
                    //No redirects
                    $array = [
                        'html_string' => '<div class="col-sm-6">ERP URL is not working.</div><div class="col-sm-6" style="color:green;">Current Google URL is the best one :)<span id="final_domain_other" style="display:none;" data-domain="' . $system->getDomainFromURL($redirectURL_other) . '"></span></div>',
                        'website_validate_string' => '<div class="col-sm-6">ERP URL is not working.</div><div class="col-sm-6" style="color:green;">Current Google URL is the best one :)<span id="final_domain_other" style="display:none;" data-domain="%s"></span></div>',
                        'variables' => $system->getDomainFromURL($redirectURL_other),
                        'final_erp' => '',
                        'final_other' => $redirectURL_other
                    ];
                } else
                {
                    $domain_other_after_redirect = $system->getDomainFromURL($redirectURL_other);

                    if (str_replace("www.", "", $domain_other_current) == str_replace("www.", "", $domain_other_after_redirect))
                    {
                        //Same domain
                        $array = [
                            'html_string' => '<div class="col-sm-6">ERP URL is not working.</div><div class="col-sm-6">There is a redirect in the scraped Google url. The best URL is: ' . $redirectURL_other . '<span id="validated_url_other" data-url="' . $redirectURL_other . '">✍</span><span id="final_domain_other" style="display:none;" data-domain="' . $system->getDomainFromURL($redirectURL_other) . '"></span></div>',
                            'website_validate_string' => '<div class="col-sm-6">ERP URL is not working.</div><div class="col-sm-6">There is a redirect in the scraped Google url. The best URL is: %s<span id="validated_url_other" data-url="%s">✍</span><span id="final_domain_other" style="display:none;" data-domain="%s"></span></div>',
                            'variables' => $redirectURL_other . "," . $redirectURL_other . "," . $system->getDomainFromURL($redirectURL_other),
                            'final_erp' => '',
                            'final_other' => $redirectURL_other
                        ];
                    } else
                    {
                        //Another URL
                        $array = [
                            'html_string' => '<div class="col-sm-6">ERP URL is not working.</div><div class="col-sm-6">There is found a redirect to another domain. Redirected URL is: ' . $redirectURL_other . '<span id="validated_url_other" data-url="' . $redirectURL_other . '">✍</span><span id="final_domain_other" style="display:none;" data-domain="' . $system->getDomainFromURL($redirectURL_other) . '"></span></div>',
                            'website_validate_string' => '<div class="col-sm-6">ERP URL is not working.</div><div class="col-sm-6">There is found a redirect to another domain. Redirected URL is: %s<span id="validated_url_other" data-url="%s">✍</span><span id="final_domain_other" style="display:none;" data-domain="%s"></span></div>',
                            'variables' => $redirectURL_other . "," . $redirectURL_other . "," . $system->getDomainFromURL($redirectURL_other),
                            'final_erp' => '',
                            'final_other' => $redirectURL_other
                        ];
                    }
                }
            } elseif ($html_erp != false && $html_other == false)
            {
                //Only ERP is working
                if ($url_erp == $redirectURL_erp)
                {
                    //No redirects
                    $array = [
                        'html_string' => '<div class="col-sm-6" style="color:green;">Current ERP URL is the best one :)<span id="final_domain" style="display:none;" data-domain="' . $system->getDomainFromURL($redirectURL_erp) . '"></span></div><div class="col-sm-6">Google URL is not working.</div>',
                        'website_validate_string' => '<div class="col-sm-6" style="color:green;">Current ERP URL is the best one :)<span id="final_domain" style="display:none;" data-domain="%s"></span></div><div class="col-sm-6">Google URL is not working.</div>',
                        'variables' => $system->getDomainFromURL($redirectURL_erp),
                        'final_erp' => $redirectURL_erp,
                        'final_other' => ''
                    ];
                } else
                {
                    $domain_erp_after_redirect = $system->getDomainFromURL($redirectURL_erp);

                    if (str_replace("www.", "", $domain_erp_current) == str_replace("www.", "", $domain_erp_after_redirect))
                    {
                        //Same domain
                        $array = [
                            'html_string' => '<div class="col-sm-6">There is a redirect in our ERP url. The best URL is: ' . $redirectURL_erp . '<span id="validated_url_erp" data-url="' . $redirectURL_erp . '">✍</span><span id="final_domain_erp" style="display:none;" data-domain="' . $system->getDomainFromURL($redirectURL_erp) . '"></span></div><div class="col-sm-6">Google URL is not working.</div>',
                            'website_validate_string' => '<div class="col-sm-6">There is a redirect in our ERP url. The best URL is: %s<span id="validated_url_erp" data-url="%s">✍</span><span id="final_domain_erp" style="display:none;" data-domain="%s"></span></div><div class="col-sm-6">Google URL is not working.</div>',
                            'variables' => $redirectURL_erp . "," . $redirectURL_erp . "," . $system->getDomainFromURL($redirectURL_erp),
                            'final_erp' => $redirectURL_erp,
                            'final_other' => ''
                        ];
                    } else
                    {
                        //Another URL
                        $array = [
                            'html_string' => '<div class="col-sm-6">There is found a redirect to another domain. Redirected URL is: ' . $redirectURL_erp . '<span id="validated_url_erp" data-url="' . $redirectURL_erp . '">✍</span><span id="final_domain_erp" style="display:none;" data-domain="' . $system->getDomainFromURL($redirectURL_erp) . '"></span></div><div class="col-sm-6">Google URL is not working.</div>',
                            'website_validate_string' => '<div class="col-sm-6">There is found a redirect to another domain. Redirected URL is: %s<span id="validated_url_erp" data-url="%s">✍</span><span id="final_domain_erp" style="display:none;" data-domain="%s"></span></div><div class="col-sm-6">Google URL is not working.</div>',
                            'variables' => $redirectURL_erp . "," . $redirectURL_erp . "," . $system->getDomainFromURL($redirectURL_erp),
                            'final_erp' => $redirectURL_erp,
                            'final_other' => ''
                        ];
                    }
                }
            } elseif ($html_erp == false && $html_other == false)
            {
                //Both URLs are not valid
                $array = [
                    'error' => 'url_not_working',
                    'html_string' => '<div class="col-sm-12">The URL from ERP and Google is not valid. Validate this company manually!</div>',
                    'website_validate_string' => '<div class="col-sm-12">The URL from ERP and Google is not valid. Validate this company manually!</div>',
                    'variables' => "",
                    'final_erp' => '',
                    'final_other' => ''
                ];
            } else
            {
                //Both URLs are valid

                $final_url_erp = '';
                $final_url_other = '';

                if ($redirectURL_erp != $url_erp)
                {
                    $domain_erp_after_redirect = $system->getDomainFromURL($redirectURL_erp);
                    if (str_replace("www.", "", $domain_erp_after_redirect) == str_replace("www.", "", $domain_erp_current))
                    {
                        $final_url_erp = $redirectURL_erp;
                    } else
                    {
                        $final_url_erp = 'company_changed';
                    }
                } else
                {
                    $final_url_erp = $url_erp;
                }

                if ($redirectURL_other != $url_other)
                {
                    $domain_other_after_redirect = $system->getDomainFromURL($redirectURL_other);
                    if (str_replace("www.", "", $domain_other_after_redirect) == str_replace("www.", "", $domain_other_current))
                    {
                        $final_url_other = $redirectURL_other;
                    } else
                    {
                        $final_url_other = 'company_changed';
                    }
                } else
                {
                    $final_url_other = $url_other;
                }

                if ($final_url_erp == 'company_changed' && $final_url_other == 'company_changed')
                {
                    $array = [
                        'html_string' => '<div class="col-sm-12">The domain of the ERP and Google URL has been changed. Please check it manually!</div>',
                        'website_validate_string' => '<div class="col-sm-12">The domain of the ERP and Google URL has been changed. Please check it manually!</div>',
                        'variables' => "",
                        'final_erp' => $redirectURL_erp,
                        'final_other' => $redirectURL_other
                    ];
                } elseif ($final_url_erp == 'company_changed')
                {
                    $array = [
                        'html_string' => '<div class="col-sm-6">The domain has been changed after the redirect. (' . $redirectURL_erp . '<span id="validated_url_erp" data-url="' . $redirectURL_erp . '">✍</span>)<span id="final_domain_erp" style="display:none;" data-domain="' . $system->getDomainFromURL($redirectURL_erp) . '"></span></div><div class="col-sm-6">Final Google URL is (' . $final_url_other . '<span id="validated_url_other" data-url="' . $final_url_other . '">✍</span>)<span id="final_domain_other" style="display:none;" data-domain="' . $system->getDomainFromURL($final_url_other) . '"></span></div>',
                        'website_validate_string' => '<div class="col-sm-6">The domain has been changed after the redirect. (%s<span id="validated_url_erp" data-url="%s">✍</span>)<span id="final_domain_erp" style="display:none;" data-domain="%s"></span></div><div class="col-sm-6">Final Google URL is (%s<span id="validated_url_other" data-url="%s">✍</span>)<span id="final_domain_other" style="display:none;" data-domain="%s"></span></div>',
                        'variables' => $redirectURL_erp . "," . $redirectURL_erp . "," . $system->getDomainFromURL($redirectURL_erp) . "," . $final_url_other . "," . $final_url_other . "," . $system->getDomainFromURL($final_url_other),
                        'final_erp' => $redirectURL_erp,
                        'final_other' => $redirectURL_other
                    ];
                } elseif ($final_url_other == 'company_changed')
                {
                    $array = [
                        'html_string' => '<div class="col-sm-6">Final ERP URL is (' . $final_url_erp . '<span id="validated_url_erp" data-url="' . $final_url_erp . '">✍</span>)<span id="final_domain_erp" style="display:none;" data-domain="' . $system->getDomainFromURL($final_url_erp) . '"></span></div><div class="col-sm-6">The domain has been changed after the redirect. (' . $redirectURL_other . '<span id="validated_url_other" data-url="' . $redirectURL_other . '">✍</span>)<span id="final_domain_other" style="display:none;" data-domain="' . $system->getDomainFromURL($redirectURL_other) . '"></span></div>',
                        'website_validate_string' => '<div class="col-sm-6">Final ERP URL is (%s<span id="validated_url_erp" data-url="%s">✍</span>)<span id="final_domain_erp" style="display:none;" data-domain="%s"></span></div><div class="col-sm-6">The domain has been changed after the redirect. (%s<span id="validated_url_other" data-url="%s">✍</span>)<span id="final_domain_other" style="display:none;" data-domain="%s"></span></div>',
                        'variables' => $final_url_erp . "," . $final_url_erp . "," . $system->getDomainFromURL($final_url_erp) . "," . $redirectURL_other . "," . $redirectURL_other . "," . $system->getDomainFromURL($redirectURL_other),
                        'final_erp' => $redirectURL_erp,
                        'final_other' => $redirectURL_other
                    ];
                } else
                {
                    if ($final_url_erp == $final_url_other)
                    {
                        if ($final_url_erp == $url_erp && $final_url_other == $url_other)
                        {
                            $array = [
                                'html_string' => '<div class="col-sm-12" style="color:green;">Both URLs are good! Final URL of both: ' . $final_url_erp . '<span id="final_domain" style="display:none;" data-domain="' . $system->getDomainFromURL($final_url_erp) . '"></span></div>',
                                'website_validate_string' => '<div class="col-sm-12" style="color:green;">Both URLs are good! Final URL of both: %s<span id="final_domain" style="display:none;" data-domain="%s"></span></div>',
                                'variables' => $final_url_erp . "," . $system->getDomainFromURL($final_url_erp),
                                'final_erp' => $final_url_erp,
                                'final_other' => $final_url_other
                            ];
                        } elseif ($final_url_erp != $url_erp && $final_url_other == $url_other)
                        {
                            $array = [
                                'html_string' => '<div class="col-sm-6">ERP URL is redirected to: ' . $final_url_erp . '<span id="validated_url_erp" data-url="' . $final_url_erp . '">✍</span><span id="final_domain_erp" style="display:none;" data-domain="' . $system->getDomainFromURL($final_url_erp) . '"></span></div><div class="col-sm-6" style="color:green;">Google URL is the best URL.<span id="final_domain_other" style="display:none;" data-domain="' . $system->getDomainFromURL($final_url_other) . '"></span></div>',
                                'website_validate_string' => '<div class="col-sm-6">ERP URL is redirected to: %s<span id="validated_url_erp" data-url="%s">✍</span><span id="final_domain_erp" style="display:none;" data-domain="%s"></span></div><div class="col-sm-6" style="color:green;">Google URL is the best URL.<span id="final_domain_other" style="display:none;" data-domain="%s"></span></div>',
                                'variables' => $final_url_erp . "," . $final_url_erp . "," . $system->getDomainFromURL($final_url_erp) . "," . $system->getDomainFromURL($final_url_other),
                                'final_erp' => $final_url_erp,
                                'final_other' => $final_url_other
                            ];
                        } elseif ($final_url_erp == $url_erp && $final_url_other != $url_other)
                        {
                            $array = [
                                'html_string' => '<div class="col-sm-6" style="color:green;">ERP URL is the best URL.<span id="final_domain_erp" style="display:none;" data-domain="' . $system->getDomainFromURL($final_url_erp) . '"></span></div><div class="col-sm-6">Google URL is redirected to: ' . $final_url_other . '<span id="validated_url_other" data-url="' . $final_url_other . '">✍</span><span id="final_domain_other" style="display:none;" data-domain="' . $system->getDomainFromURL($final_url_other) . '"></span></div>',
                                'website_validate_string' => '<div class="col-sm-6" style="color:green;">ERP URL is the best URL.<span id="final_domain_erp" style="display:none;" data-domain="%s"></span></div><div class="col-sm-6">Google URL is redirected to: %s<span id="validated_url_other" data-url="%s">✍</span><span id="final_domain_other" style="display:none;" data-domain="%s"></span></div>',
                                'variables' => $system->getDomainFromURL($final_url_erp) . "," . $final_url_other . "," . $final_url_other . "," . $system->getDomainFromURL($final_url_other),
                                'final_erp' => $final_url_erp,
                                'final_other' => $final_url_other
                            ];
                        } else
                        {
                            $array = [
                                'html_string' => '<div class="col-sm-6">ERP URL is redirected to: ' . $final_url_erp . '<span id="validated_url_erp" data-url="' . $final_url_erp . '">✍</span><span id="final_domain_erp" style="display:none;" data-domain="' . $system->getDomainFromURL($final_url_erp) . '"></span></div><div class="col-sm-6">Google URL is redirected to: ' . $final_url_other . '<span id="validated_url_other" data-url="' . $final_url_other . '">✍</span><span id="final_domain_other" style="display:none;" data-domain="' . $system->getDomainFromURL($final_url_other) . '"></span></div>',
                                'website_validate_string' => '<div class="col-sm-6">ERP URL is redirected to: %s<span id="validated_url_erp" data-url="%s">✍</span><span id="final_domain_erp" style="display:none;" data-domain="%s"></span></div><div class="col-sm-6">Google URL is redirected to: %s<span id="validated_url_other" data-url="%s">✍</span><span id="final_domain_other" style="display:none;" data-domain="%s"></span></div>',
                                'variables' => $final_url_erp . "," . $final_url_erp . "," . $system->getDomainFromURL($final_url_erp) . "," . $final_url_other . "," . $final_url_other . "," . $system->getDomainFromURL($final_url_other),
                                'final_erp' => $final_url_erp,
                                'final_other' => $final_url_other
                            ];
                        }
                    } else
                    {
                        $domain_final_erp = $system->getDomainFromURL($final_url_erp);
                        $domain_final_other = $system->getDomainFromURL($final_url_other);

                        if (str_replace("www.", "", $domain_final_erp) == str_replace("www.", "", $domain_final_other))
                        {
                            if ($final_url_erp == $url_erp)
                            {
                                $array['html_string'] .= '<div class="col-sm-6">Current ERP URL is good.<span id="final_domain_erp" style="display:none;" data-domain="' . $system->getDomainFromURL($final_url_erp) . '"></span></div>';
                                $array['website_validate_string'] .= '<div class="col-sm-6">Current ERP URL is good.<span id="final_domain_erp" style="display:none;" data-domain="%s"></span></div>';
                                $array['variables'] .= $system->getDomainFromURL($final_url_erp);
                                $array['final_erp'] .= $final_url_erp;
                            } else
                            {
                                $array['html_string'] .= '<div class="col-sm-6">ERP URL is redirected to: ' . $final_url_erp . '<span id="validated_url_erp" data-url="' . $final_url_erp . '">✍</span><span id="final_domain_erp" style="display:none;" data-domain="' . $system->getDomainFromURL($final_url_erp) . '"></span></div>';
                                $array['website_validate_string'] .= '<div class="col-sm-6">ERP URL is redirected to: %s<span id="validated_url_erp" data-url="%s">✍</span><span id="final_domain_erp" style="display:none;" data-domain="%s"></span></div>';
                                $array['variables'] .= $final_url_erp . "," . $final_url_erp . "," . $system->getDomainFromURL($final_url_erp);
                                $array['final_erp'] .= $final_url_erp;
                            }

                            if ($final_url_other == $url_other)
                            {
                                $array['html_string'] .= '<div class="col-sm-6">Current Google URL is good.<span id="final_domain_other" style="display:none;" data-domain="' . $system->getDomainFromURL($final_url_other) . '"></span></div>';
                                $array['website_validate_string'] .= '<div class="col-sm-6">Current Google URL is good.<span id="final_domain_other" style="display:none;" data-domain="%s"></span></div>';
                                $array['variables'] .= "," . $system->getDomainFromURL($final_url_other);
                                $array['final_other'] .= $final_url_other;
                            } else
                            {
                                $array['html_string'] .= '<div class="col-sm-6">Google URL is redirected to: ' . $final_url_other . '<span id="validated_url_other" data-url="' . $final_url_other . '">✍</span><span id="final_domain_other" style="display:none;" data-domain="' . $system->getDomainFromURL($final_url_other) . '"></span></div>';
                                $array['website_validate_string'] .= '<div class="col-sm-6">Google URL is redirected to: %s<span id="validated_url_other" data-url="%s">✍</span><span id="final_domain_other" style="display:none;" data-domain="%s"></span></div>';
                                $array['variables'] .= "," . $final_url_other . "," . $final_url_other . "," . $system->getDomainFromURL($final_url_other);
                                $array['final_other'] .= $final_url_other;
                            }
                        } else
                        {
                            $array['html_string'] .= '<div class="col-sm-12" style="color:red;">ERP final URL (' . $final_url_erp . '<span id="validated_url_erp" data-url="' . $final_url_erp . '">✍</span>) has another domain then the final URL of Google (' . $final_url_other . '<span id="validated_url_other" data-url="' . $final_url_other . '">✍</span>). Please check it manually!</div>';
                            $array['website_validate_string'] .= '<div class="col-sm-12" style="color:red;">ERP final URL (%s<span id="validated_url_erp" data-url="%s">✍</span>) has another domain then the final URL of Google (%s<span id="validated_url_other" data-url="%s">✍</span>). Please check it manually!</div>';
                            $array['variables'] .= $final_url_erp . "," . $final_url_erp . "," . $final_url_other . "," . $final_url_other;
                            $array['final_erp'] .= $final_url_erp;
                            $array['final_other'] .= $final_url_other;
                        }
                    }
                }
            }
        }

        return json_encode($array);
        //echo json_encode($array);
    }

    public function validateEmailDomain(Request $request)
    {
        //$domain_website = trim($request->domain_website, "/");
        //$domain_website = str_replace("www.", "", $domain_website);

        $system = new System();

        $email_general_bool = false;
        $email_sirelo_bool = false;
        $email_mobilityex_general_bool = false;
        $email_mobilityex_sirelo_bool = false;

        $url_erp = strtolower($request->url_erp);
        $url_other = strtolower($request->url_other);
        $email_general = strtolower($request->email_general);
        $email_sirelo = strtolower($request->email_sirelo);
        $email_mobilityex_general = strtolower($request->email_mobilityex_general);
        $email_mobilityex_sirelo = strtolower($request->email_mobilityex_sirelo);

        $domain_erp = str_replace("www.", "", trim($system->getDomainFromURL($url_erp), "/"));
        $domain_other = str_replace("www.", "", trim($system->getDomainFromURL($url_other), "/"));

        /**
         * Uncomment this piece to validate on final domain instead of current erp/other domain
         */
        //$final_domain = trim(str_replace("www.", "", $request->final_domain), "/");
        if (isset($request->only) && $request->only == "new")
        {
            if (!empty($domain_other))
            {
                foreach (explode(",", $email_general) as $email)
                {
                    if (strpos($email, "@" . $domain_other) !== false)
                    {
                        $email_general_bool = true;
                    }
                }

                foreach (explode(",", $email_sirelo) as $email)
                {
                    if (strpos($email, "@" . $domain_other) !== false)
                    {
                        $email_sirelo_bool = true;
                    }
                }

                if (!empty($email_mobilityex_general)) {
                    foreach (explode(",", $email_mobilityex_general) as $email)
                    {
                        if (strpos($email, "@" . $domain_other) !== false)
                        {
                            $email_mobilityex_general_bool = true;
                        }
                    }
                }



                if (!empty($email_mobilityex_sirelo)) {
                    foreach (explode(",", $email_mobilityex_sirelo) as $email)
                    {
                        if (strpos($email, "@" . $domain_other) !== false)
                        {
                            $email_mobilityex_sirelo_bool = true;
                        }
                    }
                }
            }
        } elseif (isset($request->only) && $request->only == "old")
        {
            if (!empty($domain_erp))
            {
                foreach (explode(",", $email_general) as $email)
                {
                    if (strpos($email, "@" . $domain_erp) !== false)
                    {
                        $email_general_bool = true;
                    }
                }

                foreach (explode(",", $email_sirelo) as $email)
                {
                    if (strpos($email, "@" . $domain_erp) !== false)
                    {
                        $email_sirelo_bool = true;
                    }
                }

                if (!empty($email_mobilityex_general)) {
                    foreach (explode(",", $email_mobilityex_general) as $email)
                    {
                        if (strpos($email, "@" . $domain_erp) !== false)
                        {
                            $email_mobilityex_general_bool = true;
                        }
                    }
                }

                if (!empty($email_mobilityex_sirelo)) {
                    foreach (explode(",", $email_mobilityex_sirelo) as $email)
                    {
                        if (strpos($email, "@" . $domain_erp) !== false)
                        {
                            $email_mobilityex_sirelo_bool = true;
                        }
                    }
                }
            }
        } else
        {
            if (!empty($domain_erp))
            {

                foreach (explode(",", $email_general) as $email)
                {
                    if (strpos($email, "@" . $domain_erp) !== false)
                    {
                        $email_general_bool = true;
                    }
                }

                foreach (explode(",", $email_sirelo) as $email)
                {
                    if (strpos($email, "@" . $domain_erp) !== false)
                    {
                        $email_sirelo_bool = true;
                    }
                }

                if (!empty($email_mobilityex_general)) {
                    foreach (explode(",", $email_mobilityex_general) as $email)
                    {
                        if (strpos($email, "@" . $domain_erp) !== false)
                        {
                            $email_mobilityex_general_bool = true;
                        }
                    }
                }

                if (!empty($email_mobilityex_sirelo)) {
                    foreach (explode(",", $email_mobilityex_sirelo) as $email)
                    {
                        if (strpos($email, "@" . $domain_erp) !== false)
                        {
                            $email_mobilityex_sirelo_bool = true;
                        }
                    }
                }
            }

            if (!empty($domain_other))
            {
                foreach (explode(",", $email_general) as $email)
                {
                    if (strpos($email, "@" . $domain_other) !== false)
                    {
                        $email_general_bool = true;
                    }
                }

                foreach (explode(",", $email_sirelo) as $email)
                {
                    if (strpos($email, "@" . $domain_other) !== false)
                    {
                        $email_sirelo_bool = true;
                    }
                }

                if (!empty($email_mobilityex_general)) {
                    foreach (explode(",", $email_mobilityex_general) as $email)
                    {
                        if (strpos($email, "@" . $domain_other) !== false)
                        {
                            $email_mobilityex_general_bool = true;
                        }
                    }
                }

                if (!empty($email_mobilityex_sirelo)) {
                    foreach (explode(",", $email_mobilityex_sirelo) as $email)
                    {
                        if (strpos($email, "@" . $domain_other) !== false)
                        {
                            $email_mobilityex_sirelo_bool = true;
                        }
                    }
                }
            }
        }

        $array = [
            'email_general' => $email_general_bool,
            'email_sirelo' => $email_sirelo_bool,
            'email_mobilityex_general' => $email_mobilityex_general_bool,
            'email_mobilityex_sirelo' => $email_mobilityex_sirelo_bool,
            'domain_erp' =>$domain_erp,
            'domain_other' =>$domain_other,
        ];


        //Return array
        return json_encode($array);
    }

    public function checkForEmailWarning(Request $request)
    {
        $system = new System();

        $warning = false;

        $website = strtolower($request->website);
        $emails = strtolower($request->email);

        $domain_website = str_replace("www.", "", trim($system->getDomainFromURL($website), "/"));

        foreach (explode(",", $emails) as $email)
        {
            $var_email_domain = explode('@', $email);
            $domain_email = array_pop($var_email_domain);

            if ($domain_email != $domain_website) {
                $warning = true;
            }
        }

        $array = [
            'warning' => $warning,
        ];

        //Return array
        return json_encode($array);
    }

    public function validatePhoneNumbers(Request $request)
    {
        $phone_general = $request->phone_general;
        $phone_sirelo = $request->phone_sirelo;
        $phone_scraped = $request->phone_scraped;
        $co_code = $request->co_code;

        $suggested_phone_number_general = null;
        $suggested_phone_number_sirelo = null;
        $suggested_phone_number_scraped_int = null;
        $suggested_phone_number_scraped_nat = null;
        $isValid_general = false;
        $isValid_sirelo = false;
        $isValid_scraped_int = false;
        $isValid_scraped_nat = false;

        //Validation telephone number X country
        $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();

        try
        {
            $swissNumberProto_general = $phoneUtil->parse($phone_general, $co_code);

            $isValid_general = $phoneUtil->isValidNumber($swissNumberProto_general);

            if ($isValid_general)
            {
                $suggested_phone_number_general = $phoneUtil->format($swissNumberProto_general, \libphonenumber\PhoneNumberFormat::INTERNATIONAL);
            }

        } catch (\libphonenumber\NumberParseException $e)
        {
            Log::debug("Telephone API FROM is failed");
        }

        //Validation telephone number X country
        $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();

        try
        {
            $swissNumberProto_sirelo = $phoneUtil->parse($phone_sirelo, $co_code);

            $isValid_sirelo = $phoneUtil->isValidNumber($swissNumberProto_sirelo);

            if ($isValid_sirelo)
            {
                $suggested_phone_number_sirelo = $phoneUtil->format($swissNumberProto_sirelo, \libphonenumber\PhoneNumberFormat::NATIONAL);
            }

        } catch (\libphonenumber\NumberParseException $e)
        {
            Log::debug("Telephone API FROM is failed");
        }

        //Validation telephone number X country
        $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();

        try
        {
            $swissNumberProto_scraped_int = $phoneUtil->parse($phone_scraped, $co_code);

            $isValid_scraped_int = $phoneUtil->isValidNumber($swissNumberProto_scraped_int);

            if ($isValid_scraped_int)
            {
                $suggested_phone_number_scraped_int = $phoneUtil->format($swissNumberProto_scraped_int, \libphonenumber\PhoneNumberFormat::INTERNATIONAL);
            }

        } catch (\libphonenumber\NumberParseException $e)
        {
            Log::debug("Telephone API FROM is failed");
        }

        //Validation telephone number X country
        $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();

        try
        {
            $swissNumberProto_scraped_nat = $phoneUtil->parse($phone_scraped, $co_code);

            $isValid_scraped_nat = $phoneUtil->isValidNumber($swissNumberProto_scraped_nat);

            if ($isValid_scraped_nat)
            {
                $suggested_phone_number_scraped_nat = $phoneUtil->format($swissNumberProto_scraped_nat, \libphonenumber\PhoneNumberFormat::NATIONAL);
            }

        } catch (\libphonenumber\NumberParseException $e)
        {
            Log::debug("Telephone API FROM is failed");
        }

        $array = [
            'converted_phone_general' => (($isValid_general) ? $suggested_phone_number_general : "not_valid"),
            'converted_phone_sirelo' => (($isValid_sirelo) ? $suggested_phone_number_sirelo : "not_valid"),
            'converted_phone_scraped_int' => (($isValid_scraped_int) ? $suggested_phone_number_scraped_int : "not_valid"),
            'converted_phone_scraped_nat' => (($isValid_scraped_nat) ? $suggested_phone_number_scraped_nat : "not_valid"),
        ];

        //Return array
        return json_encode($array);
    }

    public function validatePhoneNumbersAddingCustomer(Request $request)
    {
        $phone = $request->phone;
        $co_code = $request->co_code;

        $suggested_phone_number_general = null;
        $isValid_general = false;

        //Validation telephone number X country
        $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();

        try
        {
            $swissNumberProto_general = $phoneUtil->parse($phone, $co_code);

            $isValid_general = $phoneUtil->isValidNumber($swissNumberProto_general);

            if ($isValid_general)
            {
                $suggested_phone_number_general = $phoneUtil->format($swissNumberProto_general, \libphonenumber\PhoneNumberFormat::INTERNATIONAL);
            }

        } catch (\libphonenumber\NumberParseException $e)
        {
            Log::debug("Telephone API FROM is failed");
        }

        $array = [
            'converted_phone_general' => (($isValid_general) ? $suggested_phone_number_general : "not_valid"),
        ];

        //Return array
        return json_encode($array);
    }

    public function validateWebsite(Request $request)
    {
        $website = $request->website;
        $other_customers_with_same_website = false;
        $customers_list = [];

        if (!empty($website))
        {
            $own_cu_id = $request->cu_id;

            $system = new System();

            $domain = trim(str_replace("www.", "", $system->getDomainFromURL($website)), "/");

            if (!empty($domain))
            {
                $customers = Customer::where("cu_website", "like", "%" . $domain . "%")
                    ->where("cu_deleted", 0)
                    ->where("cu_id", "!=", $own_cu_id)
                    ->get();


                if (count($customers) > 0)
                {

                    foreach ($customers as $customer)
                    {
                        $domain_customer = trim(str_replace("www.", "", $system->getDomainFromURL($customer->cu_website)), "/");

                        if ($domain == $domain_customer)
                        {
                            $other_customers_with_same_website = true;

                            $customers_list[$customer->cu_id] = $customer->cu_company_name_business;
                        }
                    }
                }
            }


        }

        $array = [
            //'domain' => $domain,
            'same_website_as_others' => $other_customers_with_same_website,
            'customers' => $customers_list
        ];

        //Return array
        echo json_encode($array);
    }

    public function saveNewDomainInForm(Request $request)
    {
        $website = $request->website;
        $system = new System();

        return trim(str_replace("www.", "", $system->getDomainFromURL($website)), "/");
    }

    public function gatheredCompaniesPutCountryActive(Request $request)
    {
        //Check if country is already whitelisted
        $check_query = GatheredCompanyCountryWhitelisted::where("gacocowh_co_code", $request->country)->first();

        if (count($check_query) == 0) {
            $set_active = new GatheredCompanyCountryWhitelisted();
            $set_active->gacocowh_co_code = $request->country;
            $set_active->save();
        }

        echo "Success";
    }

    public function gatheredCompaniesPutCountryInactive(Request $request)
    {
        $country_to_be_removed = GatheredCompanyCountryWhitelisted::where("gacocowh_co_code", $request->country)->first();
        $country_to_be_removed->delete();

        echo "Success";
    }

    public function putOnWhitelist(Request $request)
    {
        if ($request->on_or_off == 1)
        {
            DB::table('gathered_companies_list')->where("gacoli_title", $request->title)->delete();

            $countries = GatheredCompany::where("gaco_title", $request->title)->groupBy("gaco_searched_co_code")->get();

            foreach ($countries as $country) {
                DB::table('gathered_companies_list')->insert([
                    'gacoli_co_code' => $country->gaco_searched_co_code,
                    'gacoli_title' => $request->title,
                    'gacoli_type' => GatheredCompaniesType::WHITELIST,
                ]);
            }
        }

        echo "Success";
    }

    public function putOnBlacklist(Request $request)
    {
        if ($request->on_or_off == 1)
        {
            DB::table('gathered_companies_list')->where("gacoli_title", $request->title)->delete();

            $countries = GatheredCompany::where("gaco_title", $request->title)->groupBy("gaco_searched_co_code")->get();

            foreach ($countries as $country) {
                DB::table('gathered_companies_list')->insert([
                    'gacoli_co_code' => $country->gaco_searched_co_code,
                    'gacoli_title' => $request->title,
                    'gacoli_type' => GatheredCompaniesType::BLACKLIST,
                ]);
            }
        }

        echo "Success";
    }

    public function getRelatedMovingCompaniesCategories(Request $request)
    {
        $categories = [];

        $list = DB::table('gathered_companies_list')->where("gacoli_co_code", $request->co_code)->get();

        foreach ($list as $item) {
            $categories[$item->gacoli_id] = $item->gacoli_title;
        }

        echo json_encode($categories);
    }

    public function uploadFileToPcloud(Request $request)
    {
        Log::debug("videoupload start");
        Log::debug($request);
        Log::debug("videoupload end");

        $filename_video = \App\Models\Request::where("re_id", $request->request_id)->first();

        $filename_video = "Premium Lead - " . $filename_video->re_full_name . " - Survey.mp4";

        $pcloud_file = new File();

        $meta = $pcloud_file->upload($request->file('file'));
        $pcloud_file->rename($meta->metadata->fileid, $filename_video);

        $method = "getfilepublink";
        $params = ['fileid' => $meta->metadata->fileid];

        $pcloudrequest = new \pCloud\Request();
        $link = $pcloudrequest->get($method, $params)->link;
        echo $meta->metadata->fileid;
        echo $link;

        Log::debug("START LINK");
        Log::debug($link);
        Log::debug("END LINK");

        DB::table('premium_leads')
            ->where('prle_re_id', $request->request_id)
            ->update(
                [
                    'prle_pcloud_file_id' => $meta->metadata->fileid,
                    'prle_pcloud_public_link' => $link
                ]
            );
    }
}
