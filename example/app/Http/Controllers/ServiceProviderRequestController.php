<?php

namespace App\Http\Controllers;

use App\Data\RejectionReason;
use App\Data\ServiceProviderRejectionStatus;
use App\Functions\System;
use App\Models\Country;
use App\Models\Customer;
use App\Models\KTRequestCustomerQuestion;
use Illuminate\Http\Request;

class ServiceProviderRequestController extends Controller
{
	public function index()
	{
		$requests = KTRequestCustomerQuestion::with(['request', 'customer', 'question'])
			->where("ktrecuqu_sent", 0)
			->where("ktrecuqu_hide", 0)
			->where("ktrecuqu_rejection_status", 0)
			->whereHas('request', function ($query) {
				$query->where('re_status', '=', 2);
			})
			->orderBy("ktrecuqu_timestamp", "asc")
			->get();
		
		$service_providers = Customer::where("cu_type", 2)->where("cu_deleted", 0)->get();
		
		$countries = [];
		
		foreach(Country::all() as $country){
			$countries[$country->co_code] = $country->co_en;
		}
		
		return view('serviceproviderrequest.index',
			[
				'requests' => $requests,
				'customers' => $service_providers,
				'statuses' => ServiceProviderRejectionStatus::all(),
				'rejectionreasons' => RejectionReason::all(),
				'selected_customer' => null,
				'selected_status' => null,
				'selected_date' => null,
				'countries' => $countries,
				'filtered' => false
			]);
	}
	
	public function filteredindex(Request $request)
	{
		$requests = KTRequestCustomerQuestion::with(['request.websiteform.website', 'request.affiliateform', 'customer', 'question'])
			->where("ktrecuqu_sent", 0)
			->where("ktrecuqu_hide", 0);
		
		if($request->status != ""){
			$requests->where("ktrecuqu_rejection_status", $request->status);
		}
		
		if($request->customer){
			$requests->whereHas('customer', function ($query) use ($request) {
				$query->where('cu_id', $request->customer);
			});
		}
		
		if ($request->date){
			$requests->whereBetween('ktrecuqu_timestamp', System::betweenDates($request->date));
		}
		
		$requests = $requests->whereHas('request', function ($query) {
				$query->where('re_status', '=', 2);
			})
			->orderBy("ktrecuqu_timestamp", "asc")
			->get();
		
		
		$service_providers = Customer::where("cu_type", 2)->where("cu_deleted", 0)->get();
		
		$countries = [];
		
		foreach(Country::all() as $country){
			$countries[$country->co_code] = $country->co_en;
		}
		
		return view('serviceproviderrequest.index',
			[
				'requests' => $requests,
				'customers' => $service_providers,
				'statuses' => ServiceProviderRejectionStatus::all(),
				'rejectionreasons' => RejectionReason::all(),
				'selected_customer' => $request->customer,
				'selected_status' => $request->status,
				'selected_date' => $request->date,
				'countries' => $countries,
				'filtered' => true
			]);
	
	}
	
	
	
}
