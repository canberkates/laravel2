<?php

namespace App\Http\Controllers;

use App\Data\CreditDebitType;
use App\Data\YesNo;
use App\Functions\Mail;
use App\Functions\Mover;
use App\Functions\RequestData;
use App\Functions\RequestDeliveries;
use App\Functions\System;
use App\Functions\Translation;
use App\Models\CreditDebitInvoiceLine;
use App\Models\KTRequestCustomerPortal;
use App\Models\VolumeCalculator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CustomerRequestController extends Controller
{
	public function free($ktrecupo_id)
	{
		$system = new System();
		$mover = new Mover();

		$ktrecupo = KTRequestCustomerPortal::with(["request", "customer.moverdata"])->findOrFail($ktrecupo_id);
		$reasons = $system->filteredClaimReasons($ktrecupo->request->re_request_type, $ktrecupo->request->re_destination_type);

		$claim_rate = $mover->calcReclamationRate(90, $ktrecupo->cu_id);

		$notice = "";

		if ($claim_rate['percentage'] > $ktrecupo->customer->moverdata->moda_max_claim_percentage && $claim_rate['requests'] >= 30)
		{
			$notice = "<strong>ATTENTION:</strong> Please notice that the claim rate <strong>" . number_format($claim_rate['percentage'], 2) . "%</strong> is higher than the allowed claim rate <strong>" . $ktrecupo->customer->moverdata->moda_max_claim_percentage . "%</strong> for this company.";
		}

		return view('customerrequestfree.free',
			[
				'yesno' => YesNo::all(),
				'reasons' => $reasons,
				'id' => $ktrecupo_id,
				'notice' => $notice
			]);
	}


	public function freeLead(Request $request, $ktrecupo_id)
	{
		$system = new System();
		$mail = new Mail();
		$request_deliveries = new RequestDeliveries();

		$translate = new Translation();

		$ktrecupo = DB::table("kt_request_customer_portal")
			->leftJoin("kt_customer_portal", "ktrecupo_ktcupo_id", "ktcupo_id")
			->leftJoin("requests", "ktrecupo_re_id", "re_id")
			->leftJoin("portals", "ktrecupo_po_id", "po_id")
			->leftJoin("mover_data", "ktrecupo_cu_id", "moda_cu_id")
			->leftJoin("customers", "ktrecupo_cu_id", "cu_id")
			->where("ktrecupo_id", $ktrecupo_id)
			->where("ktrecupo_type", 1)
			->where("cu_deleted", 0)
			->first();

		if(!$ktrecupo){
			return redirect()->back();
		}

		$mover_capping_change = $ktrecupo->moda_capping_method == 2 ? $ktrecupo->ktrecupo_amount_netto : 1;

		if ($request->free == 1 && $ktrecupo->ktrecupo_free)
		{

				DB::table('kt_customer_portal')
					->where('ktcupo_id', $ktrecupo->ktcupo_id)
					->update(
						[
							'ktcupo_max_requests_month_left' => DB::raw("(`ktcupo_max_requests_month_left` + 1)")
						]
					);

				DB::table('mover_data')
					->where('moda_cu_id', $ktrecupo->cu_id)
					->update(
						[
							'moda_capping_monthly_limit_left' => DB::raw("(`moda_capping_monthly_limit_left` + {$mover_capping_change})")
						]
					);

			if (date("d") == date("d", strtotime($ktrecupo->ktrecupo_timestamp)))
			{
				DB::table('kt_customer_portal')
					->where('ktcupo_id', $ktrecupo->ktcupo_id)
					->update(
						[
							'ktcupo_max_requests_day_left' => DB::raw("(`ktcupo_max_requests_day_left` + 1)")
						]
					);
			}

			DB::table('kt_customer_portal')
				->where('ktcupo_id', $ktrecupo->ktcupo_id)
				->update(
					[
						'ktcupo_max_requests_day_left' => DB::raw("IF(
								`ktcupo_requests_bucket` - 1 > 0,
								`ktcupo_requests_bucket` - 1, 0
							)")
					]
				);

			DB::table('mover_data')
				->where('moda_cu_id', $ktrecupo->cu_id)
				->update(
					[
						'moda_capping_bucket' => DB::raw("IF(
								`moda_capping_bucket` - '{$mover_capping_change}' > 0,
								`moda_capping_bucket` - '{$mover_capping_change}', 0
							)"),
                        'moda_forced_daily_capping_left' => DB::raw("`moda_forced_daily_capping_left` + 1")
                    ]
				);


		} elseif ($request->free == 0 && $ktrecupo->ktrecupo_free)
		{

			if (date("m") == date("m", strtotime($ktrecupo->ktrecupo_timestamp)))
			{
				DB::table('kt_customer_portal')
					->where('ktcupo_id', $ktrecupo->ktcupo_id)
					->update(
						[
							'ktcupo_max_requests_month_left' => DB::raw("(`ktcupo_max_requests_month_left` - 1)")
						]
					);

				DB::table('mover_data')
					->where('moda_cu_id', $ktrecupo->cu_id)
					->update(
						[
							'moda_capping_monthly_limit_left' => DB::raw("(`moda_capping_monthly_limit_left` - {$mover_capping_change})")
						]
					);
			}

			if (date("d") == date("d", strtotime($ktrecupo->ktrecupo_timestamp)))
			{
				DB::table('kt_customer_portal')
					->where('ktcupo_id', $ktrecupo->ktcupo_id)
					->update(
						[
							'ktcupo_max_requests_day_left' => DB::raw("(`ktcupo_max_requests_day_left` - 1)")
						]
					);
			}

			DB::table('kt_customer_portal')
				->where('ktcupo_id', $ktrecupo->ktcupo_id)
				->update(
					[
						'ktcupo_requests_bucket' => DB::raw("`ktcupo_requests_bucket` + 1")
					]
				);

			DB::table('mover_data')
				->where('moda_cu_id', $ktrecupo->cu_id)
				->update(
					[
						'moda_capping_bucket' => DB::raw("`moda_capping_bucket` - '{$mover_capping_change}'"),
						'moda_forced_daily_capping_left' => DB::raw("`moda_forced_daily_capping_left` + 1")
					]
				);

		}

		if ($ktrecupo->ktrecupo_invoiced)
		{
			if ($request->free == 1 && $ktrecupo->ktrecupo_free == 0)
			{
				$cdil = new CreditDebitInvoiceLine();

				$cdil->crdeinli_timestamp = Carbon::now()->toDateTimeString();
				$cdil->crdeinli_cu_id = $ktrecupo->cu_id;
				$cdil->crdeinli_ktrecupo_id = $ktrecupo->ktrecupo_id;
				$cdil->crdeinli_leac_number = $ktrecupo->cu_leac_number;
				$cdil->crdeinli_description = $translate->get("invoice_free_requests_previous_invoice", $ktrecupo->cu_la_code). "(".$translate->get("request_type_val_".$ktrecupo->re_request_type, $ktrecupo->cu_la_code);

				$cdil->crdeinli_type = CreditDebitType::CREDIT;
				$cdil->crdeinli_currency = $ktrecupo->cu_pacu_code;
				$cdil->crdeinli_amount = $system->currencyConvert(-$ktrecupo->ktrecupo_amount_netto, $ktrecupo->po_pacu_code, $ktrecupo->cu_pacu_code);

				$cdil->save();
			} elseif ($request->free == 0 && $ktrecupo->ktrecupo_free)
			{
				$cdil = new CreditDebitInvoiceLine();

				$cdil->crdeinli_timestamp = Carbon::now()->toDateTimeString();
				$cdil->crdeinli_cu_id = $ktrecupo->cu_id;
				$cdil->crdeinli_ktrecupo_id = $ktrecupo->ktrecupo_id;
				$cdil->crdeinli_leac_number = $ktrecupo->cu_leac_number;
				$cdil->crdeinli_description = $translate->get("invoice_paid_requests_previous_invoice", $ktrecupo->cu_la_code). "(".$translate->get("request_type_val_".$ktrecupo->re_request_type, $ktrecupo->cu_la_code);

				$cdil->crdeinli_type = CreditDebitType::DEBIT;
				$cdil->crdeinli_currency = $ktrecupo->cu_pacu_code;
				$cdil->crdeinli_amount = $system->currencyConvert($ktrecupo->ktrecupo_amount_netto, $ktrecupo->po_pacu_code, $ktrecupo->cu_pacu_code);

				$cdil->save();
			}
		}

		DB::table('kt_request_customer_portal')
			->where('ktrecupo_id', $ktrecupo->ktrecupo_id)
			->update(
				[
					'ktrecupo_free' => $request->free,
					'ktrecupo_free_reason' => (($request->free == 1) ? $request->reason : 0),
                    'ktrecupo_free_percentage' => (($request->free == 1) ? 1 : 0)
				]
			);

		if ($request->free == 1 && $request->email == 1)
		{
			foreach ($request_deliveries->receivers(1, $ktrecupo->ktcupo_id, $ktrecupo->cu_email, 1) as $receiver)
			{
				$fields = [
					"request_delivery_value" => $receiver['value'],
					"request_delivery_extra" => $receiver['extra'],
					"kt_request_customer_portal" => $system->databaseToArray($ktrecupo)
				];

				$mail->send("customer_request_free", $ktrecupo->cu_la_code, $fields);
			}
		}


		return redirect('customers/' . $ktrecupo->cu_id . '/edit');
	}

	public function resend($ktrecupo_id)
	{
		$ktrecupo = KTRequestCustomerPortal::with(['request', 'customer'])->findOrFail($ktrecupo_id);

		return view('customerrequestfree.resend',
			[
				'ktrecupo' => $ktrecupo
			]);
	}

	public function resendLead(Request $request, $ktrecupo_id)
	{
		Log::debug($ktrecupo_id);

		$request_data = new RequestData();
		$system = new System();
		$mail = new Mail();
		$request_delivery = new RequestDeliveries();

		$ktrecupo = DB::table("kt_request_customer_portal")
			->leftJoin("kt_customer_portal", "ktrecupo_ktcupo_id", "ktcupo_id")
			->leftJoin("requests", "ktrecupo_re_id", "re_id")
            ->leftJoin("volume_calculator", "voca_id", "re_voca_id")
			->leftJoin("customers", "ktrecupo_cu_id", "cu_id")
			->where("ktrecupo_id", $ktrecupo_id)
			->where("cu_deleted", 0)
			->first();

        $ktrecupo->re_volume_calculator = ((!empty($ktrecupo->re_voca_id)) ? VolumeCalculator::where("voca_id", $ktrecupo->re_voca_id)->first()->voca_volume_calculator : null);

		foreach(RequestDeliveries::receivers($ktrecupo->cu_type, $ktrecupo->ktcupo_id, $ktrecupo->cu_email) as $receiver)
		{
			$count_matched_to = $request_data->requestMatchesDone($ktrecupo->re_id);
			$matched_to = $count_matched_to - 1;

			if ($matched_to <= 2)
				$matched_to = "0-2";

			$fields = [
				"request_delivery_value" => $receiver['value'],
				"request_delivery_extra" => $receiver['extra'],
				"match_type" => (($ktrecupo->ktrecupo_type == 3)?2:1),
				"cu_id" => $ktrecupo->cu_id,
				"cu_re_id" => $ktrecupo->ktrecupo_cu_re_id,
				"language" => $ktrecupo->cu_la_code,
				"request" => $system->databaseToArray($ktrecupo),
				"matched_to" => $matched_to
			];

			if($receiver['type'] == 1)
			{
				$mail->send("customer_request", $ktrecupo->cu_la_code, $fields);
			}
			else
			{
				$request_delivery->send(1, $receiver['type'], $ktrecupo->cu_la_code, $fields);
			}
		}

		return redirect('customers/' . $ktrecupo->cu_id . '/edit');
	}

}
