<?php

namespace App\Http\Controllers;

use App\Functions\Mail;
use App\Models\ApplicationUser;
use App\Models\ContactPerson;
use App\Models\Customer;
use Illuminate\Http\Request;

class ApplicationUserController extends Controller
{
	public function create($customer_id, $contactperson_id)
	{
		$contactperson = ContactPerson::findOrFail($contactperson_id);

		$customer = Customer::findOrFail($contactperson->cope_cu_id);

		if ($customer->cu_type == 1)
		{
			$start_redirect_url = "customers/";
		}
		elseif($customer->cu_type == 6)
		{
			$start_redirect_url = "resellers/";
		}
		elseif($customer->cu_type == 2)
		{
			$start_redirect_url = "serviceproviders/";
		}
		elseif ($customer->cu_type == 3)
		{
			$start_redirect_url = "affiliatepartners/";
		}

		return view('applicationuser.create', ['contactperson' => $contactperson, 'start_redirect_url' => $start_redirect_url]);
	}

	public function store(Request $request)
	{
		$contact_person = ContactPerson::with("customer")->findOrFail($request->cope_id);

		if (empty($contact_person->cope_apus_id))
		{
			$ap_id = "";
			$portal = "";

			if ($contact_person->customer->cu_type == 1 || $contact_person->customer->cu_type == 6)
			{
				$ap_id = 2;
				$portal = "Mover Portal";
			}
			elseif($contact_person->customer->cu_type == 2)
			{
				$ap_id = 5;
				$portal = "Service Providers Portal";
			}
			elseif($contact_person->customer->cu_type == 3)
			{
				$ap_id = 6;
				$portal = "Affiliate Portal";
			}

			//Create a random password
			$random_password = substr(md5(mt_rand()), 0, 8);

			$application_user = new ApplicationUser();

			$application_user->apus_cu_id = $contact_person->customer->cu_id;
			$application_user->apus_username = $contact_person->cope_email;
			$application_user->apus_password = md5(md5($random_password."!R@3#l%0^4&u*1(6)"));
			$application_user->apus_name = $contact_person->cope_full_name;
			$application_user->apus_email = $contact_person->cope_email;

			if (!empty($ap_id))
			{
				$application_user->apus_application_id = $ap_id;
			}

			$application_user->save();

			$contact_person->cope_apus_id = $application_user->apus_id;

			$contact_person->save();

			//Start sending mail block
			$mail = new Mail();

			$fields = [
				"full_name" => $contact_person->cope_full_name,
				"email" => $contact_person->cope_email,
				"username" => $contact_person->cope_email,
				"password" => $random_password,
				"portal" => $portal
			];

			$mail->send("user_add", $contact_person->cope_language, $fields);
			//End sending mail block

			return redirect($request->redirect_url);
		}
	}

	public function delete(Request $request)
	{
		//Get the User
		$app_user = ApplicationUser::findOrFail($request->id);

		//Soft delete this user
		$app_user->apus_deleted = 1;

		//Save the user
		$app_user->save();
	}
}
