<?php

namespace App\Http\Controllers;

use App\Functions\DataIndex;
use App\Functions\System;
use DB;
use Illuminate\Http\Request;

class ICListController extends Controller
{
	public function index()
	{

		return view('finance.iclist', [
			'selected_date' => "",
			'date_filter' => [],
			'filtered_data' => []
		]);
	}

	public function filteredIndex(Request $request)
	{
		$request->validate([
			'date' => 'required'
		]);

		$companies = DB::table("customers")
			->select("cu_id", "cu_company_name_legal", "cu_vat_number", "co_en")
			->selectRaw("SUM(inli_amount_netto_eur) AS `amount`")
			->leftJoin("invoices","cu_id","in_cu_id")
			->leftJoin("invoice_lines","in_id","inli_in_id")
			->leftJoin("countries","co_code","cu_co_code")
			->whereBetween("in_date", System::betweenDates($request->date))
			->whereIn("inli_leac_number", [803000, 803001])
			->whereIn("cu_type", [1, 2, 4, 6])
			->where("cu_deleted", 0)
			->groupBy("cu_id")
			->orderBy("cu_company_name_legal", 'asc')
			->get();

		$total = 0;
		foreach($companies as $company){
			$total += $company->amount;
		}

		return view('finance.iclist', [
			'selected_date' => $request->date,
			'companies' => $companies,
			'total' => $total

		]);

	}

}
