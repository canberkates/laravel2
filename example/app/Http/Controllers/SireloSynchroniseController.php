<?php

namespace App\Http\Controllers;

use App\Data\NewsletterSlot;
use App\Data\Spaces;
use App\Functions\Data;
use App\Functions\Mover;
use App\Functions\SireloCustomer;
use App\Functions\Synchronise;
use App\Functions\System;
use App\Models\Customer;
use App\Models\CustomerOffice;
use App\Models\CustomerReviewScore;
use App\Models\Insurance;
use App\Models\KTCustomerGatheredReview;
use App\Models\KTCustomerInsurance;
use App\Models\KTCustomerMembership;
use App\Models\KTCustomerObligation;
use App\Models\KTCustomerPortalCountry;
use App\Models\Membership;
use App\Models\Obligation;
use App\Models\ServiceProviderAdvertorialBlock;
use App\Models\SireloLink;
use App\Models\State;
use App\Models\Survey2;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Functions\DataIndex;
use Response;

class SireloSynchroniseController extends Controller
{

    public function index()
    {
        $websites = DB::table("websites")
            ->where("we_sirelo", 1)
            ->where("we_sirelo_bucket", 0)
            ->get();

        return view('synchronise.index',
            [
                'websites' => $websites
            ]);
    }

    public function update(Request $request)
    {

        // getting values from form (like $record_num)
        Log::debug("start synchronising");
        // Select all websites
        // we_sirelo = 1
        // we_sirelo_bucket = 0 (only normal sirelo's)
        // we_sirelo_ftp_server must have a value
        $query = DB::table("websites")
            ->where("we_sirelo", 1)
            ->where("we_sirelo_bucket", 0)
            ->get();

        $sirelocustomer = new SireloCustomer();

        // Get all memberships
        $memberships = Membership::whereNotIn("me_id", [26,27])->get();

        // Get all sirelo hyperlinks
        $sirelo_hyperlinks = SireloLink::all();

        // Get all obligations
        $customer_obligations = Obligation::all();

        // Get all insurances
        $customer_insurances = Insurance::all();

        // Get all states
        $states = State::all();

        // Get spaces connection
        $spaces = new Spaces('srl');

        // Empty array for the logos
        $logos = [];

        // Array to save we_ids from websites which we syncing
        $websites_ids = [];

        // Loop websites that match the criteria
        foreach ($query as $row) {

            Log::debug("Sirelo Sync POST-info");
            Log::debug($request);

            // If This website must be synced or this is the cron
            // Cron means all websites are synced
            if (isset($request->websites[$row->we_id])) {
                $websites_ids[$row->we_id] = $row->we_id;

                // Get pro and con categories by language of website
                $pro_con_categories = Data::getProConCategories(null, $row->we_sirelo_la_code);

                echo "<h3>" . $row->we_website . "</h3>";

                Log::debug("Collecting data for Sirelo");

                // Prepare arrays
                $options = [
                    "national_country_code" => $row->we_sirelo_co_code,
                    "top_mover_months" => $row->we_sirelo_top_mover_months
                ];
                $cities = [];
                $customers = [];
                $customer_offices = [];
                $customer_memberships = [];
                $kt_customer_obligations = [];
                $kt_customer_insurances = [];
                $customers_to_countries = [];
                $reviews = [];
                $advertorial_blocks = [];

                // Select all customers from the current website (country_code)
                $customers_query = Customer::leftJoin("mover_data", "moda_cu_id", "cu_id")
                    ->leftJoin( 'states', 'cu_state', 'st_id' )
                    ->where("cu_type", 1)
                    ->where("cu_city", "!=", "")
                    ->where("cu_co_code", $row->we_sirelo_co_code)
                    ->where("cu_deleted", 0)
                    ->where("moda_disable_sirelo_export", 0)
                    ->get();

                $collectedIds = [];

                // Loop all the customers from the current website (country_code)
                foreach ($customers_query as $row_customers) {

                    $collectedIds[] = $row_customers['cu_id'];

                    // Build state key
                    $state_key = System::getSeoKey( $row_customers->st_key );

                    // Build city key
                    $city_key = System::getSeoKey($row_customers->cu_city);

                    // Build customer key
                    $customer_key = $city_key.System::getSeoKey($row_customers->cu_company_name_business);

                    // For the websites that go with a region
                    if( $row->we_states_enabled ) {

                        // Add the state key in front of the customer key
                        $customer_key = $state_key.$customer_key;
                    }

                    // New temp customer
                    $temp_customer = [$city_key];

                    // Check if this customer (company_name + city) exists
                    // We don't want double customers
                    if (!array_key_exists($customer_key, $customers)) {
                        // Check if this city exists
                        if (!array_key_exists($city_key, $cities)) {
                            // City does not exist in the cities array yet
                            // Add city to the cities array with amount 1
                            $cities[$city_key] = [
                                "city" => $row_customers->cu_city,
                                "amount" => 1,
                            ];

                        } else {
                            // City does exist in the cities array
                            // Increment city by 1
                            $cities[$city_key]['amount']++;
                        }

                        // If this customer has a state and the state wasn't set yet.
                        if( ! empty( $row_customers->cu_state ) && ! isset( $cities[$city_key]['state'] ) ) {

                            // Add state to this city
                            $cities[$city_key]['state'] = $row_customers->cu_state;
                        }

                        // Build customer
                        $customers[$customer_key] = [
                            "city_key" => $city_key,
                            "forward_1" => $row_customers->moda_sirelo_forward_1,
                            "forward_2" => $row_customers->moda_sirelo_forward_2,
                            "origin_id" => $row_customers->cu_id,
                            "top_mover" => 0,
                            "disable_top_mover" => $row_customers->moda_sirelo_disable_top_mover,
                            "disable_request_form_on_customer_page" => $row_customers->moda_sirelo_disable_request_form,
                            "review_partner" => $row_customers->moda_review_partner,
                            "claimed" => Mover::isLoggedInOnce($row_customers->cu_id), // If user is logged in once, assume the company is claimed
                            "company_name" => $row_customers->cu_company_name_business,
                            "legal_name" => $row_customers->cu_company_name_legal,
                            "coc" => $row_customers->cu_coc,
                            "logo" => "",
                            "description" => $row_customers->cu_description,
                            "international_moves" => $row_customers->moda_international_moves,
                            "national_moves" => $row_customers->moda_national_moves,
                            "long_distance_moving_europe" => $row_customers->moda_long_distance_moving_europe,
                            "excess_baggage" => $row_customers->moda_excess_baggage,
                            "man_and_van" => $row_customers->moda_man_and_van,
                            "car_and_vehicle_transport" => $row_customers->moda_car_and_vehicle_transport,
                            "piano_transport" => $row_customers->moda_piano_transport,
                            "pet_transport" => $row_customers->moda_pet_transport,
                            "art_transport" => $row_customers->moda_art_transport,
                            "established" => $row_customers->moda_established,
                            "employees" => $row_customers->moda_employees,
                            "trucks" => $row_customers->moda_trucks,
                            "street" => $row_customers->cu_street_1,
                            "zipcode" => $row_customers->cu_zipcode,
                            "city" => $row_customers->cu_city,
                            "country" => $row_customers->cu_co_code,
                            "email" => $row_customers->moda_contact_email,
                            "telephone" => $row_customers->moda_contact_telephone,
                            "website" => $row_customers->cu_website,
                            "not_operational" => $row_customers->moda_not_operational,
                            "type_of_mover" => $row_customers->cu_type_of_mover,
                            "public_liability_insurance" => $row_customers->cu_public_liability_insurance,
                            "goods_in_transit_insurance" => $row_customers->cu_goods_in_transit_insurance,
                            "rating" => 0,
                            "reviews" => 0,
                            "recommendation" => 0,
                            "recommendations" => 0,
                            "rating_recent" => 0,
                            "reviews_recent" => 0,
                            "recommendation_recent" => 0,
                            "recommendations_recent" => 0,
                            "pros_cons" => ["pro" => [], "con" => []]
                        ];

                        // If the customer has a logo
                        if (!empty($row_customers->cu_logo)) {
                            $pathToFile = env('SHARED_FOLDER') . 'uploads/logos/' . $row_customers->cu_logo;

                            if (is_file($pathToFile)) {
                                $logo = substr(System::getSeoKey($row_customers->cu_company_name_business, $row->we_sirelo_la_code), 0, -1) . "-" . substr($city_key, 0, -1) . "." . pathinfo($row_customers->cu_logo, PATHINFO_EXTENSION);

                                $customers[$customer_key]['logo'] = $logo;
                                $logos[$row_customers->cu_logo] = $logo;
                            }
                        }
                    }

                    // Get current customer offices and add in array $customer_offices[]
                    $customer_offices_query = CustomerOffice::where("cuof_cu_id", $row_customers->cu_id)
                        ->where(function ($query) use ($row_customers) {
                            $query
                                ->where('cuof_co_code', '')
                                ->orWhere('cuof_co_code', $row_customers->cu_co_code);
                        })
                        ->get();

                    // Check if there are any offices
                    if (count($customer_offices_query) > 0) {
                        // Loop customer offices
                        foreach ($customer_offices_query as $row_customer_offices) {

                            //Check if this city exists, add to the cities amount
                            $office_city_key = System::getSeoKey($row_customer_offices->cuof_city);

                            // If city key is not the same as the office city key
                            if (!in_array($office_city_key, $temp_customer)) {

                                // Check if city key is not present yet
                                if (!isset($cities[$office_city_key])) {

                                    // Create city in cities
                                    $cities[$office_city_key] = [
                                        "city" => $row_customer_offices->cuof_city,
                                        "amount" => 1,
                                    ];

                                    // City does exist already
                                } else {
                                    // Add 1 to amount of this city
                                    $cities[$office_city_key]['amount']++;
                                }

                                // If there is a state and the state wasn't set yet
                                if( ! empty( $row_customer_offices->cuof_state ) && ! isset( $cities[$office_city_key]['state'] ) ) {

                                    // Add state to this city
                                    $cities[$office_city_key]['state'] = $row_customer_offices->cuof_state;
                                }
                            }

                            // Add office city key to the temp customer array
                            // for the next office
                            $temp_customer[] = $office_city_key;


                            // Add customer office
                            $customer_offices[] = [
                                "customer_key" => $customer_key,
                                "city_key" => $office_city_key,
                                "street" => $row_customer_offices->cuof_street_1,
                                "zipcode" => $row_customer_offices->cuof_zipcode,
                                "city" => $row_customer_offices->cuof_city,
                                "country" => $row_customer_offices->cuof_co_code ?? '',
                                "email" => $row_customer_offices->cuof_email,
                                "telephone" => $row_customer_offices->cuof_telephone,
                                "website" => $row_customer_offices->cuof_website
                            ];
                        }
                    }

                    //Get customer memberships and add in array $customer_memberships[]
                    $customer_memberships_query = KTCustomerMembership::where("ktcume_cu_id", $row_customers->cu_id)->get();

                    foreach ($customer_memberships_query as $row_customer_memberships) {
                        $customer_memberships[] = [
                            "customer_key" => $customer_key,
                            "ktcume_cu_id" => $row_customer_memberships->ktcume_cu_id,
                            "ktcume_me_id" => $row_customer_memberships->ktcume_me_id
                        ];
                    }

                    //Get customer memberships and add in array $customer_memberships[]
                    $kt_customer_obligations_query = KTCustomerObligation::leftJoin("customer_documents", "cudo_id", "ktcuob_cudo_id")->where("ktcuob_cu_id", $row_customers->cu_id)->get();

                    foreach ($kt_customer_obligations_query as $row_customer_obligations) {
                        $kt_customer_obligations[] = [
                            "customer_key" => $customer_key,
                            "ktcuob_id" => $row_customer_obligations->ktcuob_id,
                            "ktcuob_cuob_id" => $row_customer_obligations->ktcuob_cuob_id,
                            "ktcuob_cu_id" => $row_customer_obligations->ktcuob_cu_id,
                            "ktcuob_cudo_id" => $row_customer_obligations->ktcuob_cudo_id,
                            "cudo_number" => ((!empty($row_customer_obligations->cudo_number)) ? $row_customer_obligations->cudo_number : ""),
                            "ktcuob_verified_timestamp" => $row_customer_obligations->ktcuob_verified_timestamp,
                            "ktcuob_verification_result" => $row_customer_obligations->ktcuob_verification_result
                        ];
                    }

                    //Get customer insurances and add in array $customer_insurances[]
                    $kt_customer_insurances_query = KTCustomerInsurance::where("ktcuin_cu_id", $row_customers->cu_id)->get();

                    foreach ($kt_customer_insurances_query as $row_customer_insurances) {
                        $kt_customer_insurances[] = [
                            "customer_key" => $customer_key,
                            "ktcuin_id" => $row_customer_insurances->ktcuin_id,
                            "ktcuin_cuob_id" => $row_customer_insurances->ktcuin_cuob_id,
                            "ktcuin_cu_id" => $row_customer_insurances->ktcuin_cu_id,
                            "ktcuin_cudo_id" => $row_customer_insurances->ktcuin_cudo_id,
                            "ktcuin_verified_timestamp" => $row_customer_insurances->ktcuin_verified_timestamp,
                            "ktcuin_verification_result" => $row_customer_insurances->ktcuin_verification_result,
                            "ktcuin_amount" => $row_customer_insurances->ktcuin_amount
                        ];
                    }

                    $mover_get_country = KTCustomerPortalCountry::selectRaw("DISTINCT(`ktcupoco_co_code`), `po_co_code`")
                        ->leftJoin("kt_customer_portal", "ktcupoco_ktcupo_id", "ktcupo_id")
                        ->leftJoin("portals", "ktcupo_po_id", "po_id")
                        ->leftJoin("payment_rates", "ktcupo_id", "para_ktcupo_id")
                        ->where("ktcupo_cu_id", $row_customers->cu_id)
                        ->where("ktcupo_status", 1)
                        ->whereRaw("`payment_rates`.`para_date` <= NOW()")
                        ->get();

                    foreach ($mover_get_country as $row_mover_data_country) {
                        if ($row->we_sirelo_co_code == $row_mover_data_country->po_co_code) {
                            $customers_to_countries[] = [
                                "customer_key" => $customer_key,
                                "country_code" => $row_mover_data_country->ktcupoco_co_code
                            ];
                        }
                    }

                    foreach (["past", "recent"] as $type) {
                        if ($type == "past") {
                            $timestamp = "<";
                        } elseif ($type == "recent") {
                            $timestamp = ">=";
                        }

                        $surveys = Survey2::leftJoin("requests", "su_re_id", "re_id")
                            ->leftJoin("website_reviews", "su_were_id", "were_id")
                            ->whereRaw("`surveys_2`.`su_submitted_timestamp` " . $timestamp . " '" . date("Y-m", strtotime("-" . $row->we_sirelo_top_mover_months . " months")) . "-01 00:00:00'")
                            ->whereRaw("(
								`surveys_2`.`su_mover` = '" . $row_customers['cu_id'] . "' OR
								`surveys_2`.`su_other_mover` = '" . $row_customers['cu_id'] . "'
							)")
                            ->where("su_submitted", 1)
                            ->where("su_show_on_website", ">", 0)
                            ->orderBy("su_submitted_timestamp", "DESC")
                            ->get();

                        $surveys = System::databaseToArray($surveys);

                        foreach ($surveys as $row_surveys) {
                            if ($row_surveys['su_show_on_website'] == 1) {
                                if ($row_surveys['su_source'] == 1) {
                                    $prefix = "re";
                                    $author = $row_surveys[$prefix . '_full_name'];
                                } elseif ($row_surveys['su_source'] == 2) {
                                    $prefix = "were";
                                    $author = $row_surveys[$prefix . '_name'];
                                }

                                if ($row_surveys['su_source'] == 1 || ($row_surveys['su_source'] == 2 && $row_surveys['were_anonymous'] == 1)) {
                                    $exp = explode(" ", preg_replace("/&([a-z])[a-z]+;/i", "$1", htmlentities(System::rusToLat($author))));

                                    $author = "";

                                    foreach ($exp as $letter) {
                                        $author .= ((isset($letter[0])) ? strtoupper($letter[0]) . "." : "");
                                    }
                                }

                                $reviews[$row_surveys['su_id']] = [
                                    "customer_key" => $customer_key,
                                    "source" => $row_surveys['su_source'],
                                    "author" => $author,
                                    "date" => date("Y-m-d", strtotime($row_surveys['su_submitted_timestamp'])),
                                    "rating" => $row_surveys['su_rating'],
                                    "rating_motivation" => $row_surveys['su_rating_motivation'],
                                    "recommend" => $row_surveys['su_recommend_mover'],
                                    "pro_1" => $row_surveys['su_pro_1'],
                                    "pro_2" => $row_surveys['su_pro_2'],
                                    "con_1" => $row_surveys['su_con_1'],
                                    "con_2" => $row_surveys['su_con_2'],
                                    "city_from" => $row_surveys[$prefix . '_city_from'],
                                    "country_from" => $row_surveys[$prefix . '_co_code_from'],
                                    "city_to" => $row_surveys[$prefix . '_city_to'],
                                    "country_to" => $row_surveys[$prefix . '_co_code_to'],
                                    "mover_comment_date" => date("Y-m-d", strtotime($row_surveys['su_mover_comment_timestamp'])),
                                    "mover_comment" => $row_surveys['su_mover_comment']
                                ];

                                $customers[$customer_key]['rating'] += $row_surveys['su_rating'];
                                $customers[$customer_key]['reviews']++;

                                if ($type == "recent") {
                                    $customers[$customer_key]['rating_recent'] += $row_surveys['su_rating'];
                                    $customers[$customer_key]['reviews_recent']++;
                                }

                                if ($row_surveys['su_recommend_mover'] > 0) {
                                    $customers[$customer_key]['recommendations']++;

                                    if ($type == "recent") {
                                        $customers[$customer_key]['recommendations_recent']++;
                                    }

                                    if ($row_surveys['su_recommend_mover'] == 1) {
                                        $customers[$customer_key]['recommendation']++;

                                        if ($type == "recent") {
                                            $customers[$customer_key]['recommendation_recent']++;
                                        }
                                    }
                                }

                                //Loop trough all pros & cons of review
                                foreach (["pro_1", "pro_2", "con_1", "con_2"] as $pro_con_type) {
                                    $category_id = $row_surveys['su_' . $pro_con_type . '_category'];
                                    $category_type = substr($pro_con_type, 0, 3);

                                    if ($category_id > 0) {
                                        if (!array_key_exists($category_id, $customers[$customer_key]['pros_cons'][$category_type])) {
                                            $customers[$customer_key]['pros_cons'][$category_type][$category_id] = [
                                                "name" => $pro_con_categories[$category_id],
                                                "amount" => 1
                                            ];
                                        } else {
                                            $customers[$customer_key]['pros_cons'][$category_type][$category_id]['amount']++;
                                        }
                                    }
                                }
                            }

                            if ($row->we_sirelo_active == 1 && $row_surveys['su_mail'] == 0) {
                                DB::table('surveys_2')
                                    ->where('su_id', $row_surveys['su_id'])
                                    ->update(['su_mail' => 1]);
                            }
                        }
                    }
                }

                foreach ($customers as $key => $customer) {
                    if ($customers[$key]['reviews'] > 0) {
                        $rating_reviews = $sirelocustomer->getReviews($customers[$key]['origin_id']);

                        $customers[$key]['rating'] = $rating_reviews['rating'];

                        //$customers[$key]['rating'] = round($customers[$key]['rating'] / $customers[$key]['reviews'], 2);
                    }

                    if ($customers[$key]['reviews_recent'] > 0) {
                        $customers[$key]['rating_recent'] = round($customers[$key]['rating_recent'] / $customers[$key]['reviews_recent']);
                    }

                    if ($customers[$key]['recommendations'] > 0) {
                        $customers[$key]['recommendation'] = round(($customers[$key]['recommendation'] / $customers[$key]['recommendations']) * 100);
                    }

                    if ($customers[$key]['recommendations_recent'] > 0) {
                        $customers[$key]['recommendation_recent'] = round(($customers[$key]['recommendation_recent'] / $customers[$key]['recommendations_recent']) * 100);
                    }

                    // If this customer is a top mover
                    // If customer's top mover is not disabled
                    if ($customers[$key]['rating_recent'] >= 4 &&
                        $customers[$key]['reviews_recent'] >= 12 &&
                        $customers[$key]['recommendation_recent'] >= 80 &&
                        !$customers[$key]['disable_top_mover']
                    ) {
                        $customers[$key]['top_mover'] = 1;
                    }

                    //Sort and limit both types
                    foreach ($customers[$key]['pros_cons'] as $category_type => $category_data) {
                        //Sort by amount
                        usort($customers[$key]['pros_cons'][$category_type], function ($a, $b) {
                            return strnatcasecmp($b['amount'], $a['amount']);
                        });

                        //Limit by checking recommendations
                        if ($category_type == "con" && $customers[$key]['recommendations'] > 0) {
                            if ($customers[$key]['recommendation'] >= 85) {
                                $limit = 1;
                            } elseif ($customers[$key]['recommendation'] >= 70) {
                                $limit = 2;
                            } else {
                                $limit = 3;
                            }
                        } else {
                            $limit = 3;
                        }

                        //Limit the amount per category
                        $customers[$key]['pros_cons'][$category_type] = array_slice($customers[$key]['pros_cons'][$category_type], 0, $limit, true);
                    }
                }

                // Get scores for all the processed customers
                $scores = KTCustomerGatheredReview::select( 'ktcugare_cu_id', 'ktcugare_platform', 'ktcugare_score', 'ktcugare_amount', 'ktcugare_url', 'ktcugare_timestamp_updated' )->whereIn( 'ktcugare_cu_id', $collectedIds )->get();

                $scores = ( ! empty( $scores ) ? System::databaseToArray( $scores ) : [] );

                // Get all advertorial blocks for this website
                $advertorial_query = ServiceProviderAdvertorialBlock::leftJoin("kt_service_provider_advertorial_block_website", "ktsepradblwe_sepradbl_id", "sepradbl_id")
                    ->leftJoin("kt_service_provider_advertorial_block_language", "ktsepradblla_sepradbl_id", "sepradbl_id")
                    ->leftJoin("kt_service_provider_advertorial_block_language_content", "ktsepradbllaco_sepradbl_id", "sepradbl_id")
                    ->leftJoin("customers", "cu_id", "sepradbl_cu_id")
                    ->where("sepradbl_status", 1)
                    ->where("ktsepradblwe_we_id", $row->we_id)
                    ->get();

                $slots = NewsletterSlot::all();
                $advertorial_blocks = [];

                foreach ($advertorial_query as $adv) {
                    // Check if its not an empty block
                    if (!empty($adv->ktsepradbllaco_title) && !empty($adv->ktsepradbllaco_content) && !empty($adv->ktsepradbllaco_button)) {
                        // Add Block, so we can see it on Sirelo :)
                        $event_category = $adv->sepradbl_event_category;
                        $event_label = $adv->sepradbl_event_label;

                        $event_category = str_replace("{slot}", $slots[$adv->sepradbl_slot], $event_category);
                        $event_category = str_replace("{company_name}", $adv->cu_company_name_business, $event_category);
                        $event_label = str_replace("{slot}", $slots[$adv->sepradbl_slot], $event_label);
                        $event_label = str_replace("{company_name}", $adv->cu_company_name_business, $event_label);

                        $advertorial_blocks[$adv->ktsepradbllaco_la_code."_".$adv->cu_company_name_business."_".$adv->sepradbl_id] = [
                            'company_name' => $adv->cu_company_name_business,
                            'la_code' => $adv->ktsepradbllaco_la_code,
                            'slot_id' => $adv->sepradbl_slot,
                            'slot' => $slots[$adv->sepradbl_slot],
                            'title' => $adv->ktsepradbllaco_title,
                            'content' => $adv->ktsepradbllaco_content,
                            'button' => $adv->ktsepradbllaco_button,
                            'url' => $adv->ktsepradbllaco_url,
                            'event_category' => $event_category,
                            'event_label' => $event_label,
                            'event_action' => $adv->sepradbl_action,
                        ];
                    }
                }

                Log::debug("Posting to the front server");
                $postData = [
                    'options' => $options,
                    'states' => $states,
                    'cities' => $cities,
                    'customers' => $customers,
                    'customer_offices' => $customer_offices,
                    'customer_memberships' => $customer_memberships,
                    'customers_to_countries' => $customers_to_countries,
                    'reviews' => $reviews,
                    'memberships' => $memberships,
                    'sirelo_hyperlinks' => $sirelo_hyperlinks,
                    'customer_obligations' => $customer_obligations,
                    'customer_insurances' => $customer_insurances,
                    'kt_customer_obligations' => $kt_customer_obligations,
                    'kt_customer_insurances' => $kt_customer_insurances,
                    'sirelo_customer_scores' => $scores,
                    'advertorial_blocks' => $advertorial_blocks,
                ];

                $urlToPostTo = $row->we_sirelo_url . '/wp-content/themes/sirelo/import/import.php';

                $postData = json_encode(['sirelo' => $postData]);

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $urlToPostTo);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_HTTPHEADER, [
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($postData),
                ]);
                $import = curl_exec($ch);
                curl_close($ch);

                $locations = json_decode($import, true);

                if (is_array($locations) && !empty($locations)) {

                    //echo System::message( "notice", "Updating page locations in ERP." );
                    DB::table('websites')
                        ->where('we_id', $row->we_id)
                        ->update([
                            'we_sirelo_customer_location' => $locations['customer_location'],
                            'we_sirelo_customer_review_slug' => $locations['customer_review_slug'],
                            'we_sirelo_partner_program_location' => $locations['partner_program_location'],
                            'we_sirelo_survey_location' => $locations['survey_location'],
                        ]);
                } else {
                    Log::debug("Oops, something went wrong. Error: " . $import);
                }

                Log::debug("Synchronisation finished!");
            } else {
                Log::debug("SYNC NO ISSET!");
            }
        }

        // Send emails
        Synchronise::sendEmails($websites_ids);

        // You can disable logo sync with this
        $dontDoLogos = false;

        // If skipping is not set or is cron
        if ((!(isset($request->logo_skip)) || System::isCron()) && !$dontDoLogos) {

            try
            {
                // Loop all logos
                foreach ($logos as $logoKey => $logoValue) {
                    // Set default
                    $putImage = false;

                    // Image doesnt exist on spaces yet
                    if (!$spaces->exists('logos/' . $logoValue)) {
                        $putImage = true;
                    } else {
                        // Set path to file
                        $pathToFile = env('SHARED_FOLDER') . 'uploads/logos/' . $logoKey;
                        $erpSize = filesize($pathToFile);
                        $spaSize = $spaces->getSize('logos/' . $logoValue);

                        // If sizes are not the same. overwrite the old images
                        if ($erpSize != $spaSize) {

                            $putImage = true;
                        }
                    }

                    if ($putImage) {
                        //  write or overwrite image
                        $spaces->put(env('SHARED_FOLDER') . 'uploads/logos/' . $logoKey, 'logos/' . $logoValue);
                    }
                }
            }catch (\Exception $e){
                System::sendMessage([4186, 3653], "Sirelo SYNC - Spaces API crash", "Spaces API is failing :(");
            }
        }

        $websites = DB::table("websites")
            ->where("we_sirelo", 1)
            ->where("we_sirelo_bucket", 0)
            ->get();

        return view('synchronise.index',
            [
                'websites' => $websites
            ]);
    }
}
