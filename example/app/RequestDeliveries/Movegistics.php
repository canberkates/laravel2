<?php

namespace App\RequestDeliveries;

use App\Data\CustomerRequestDeliveryType;
use App\Data\DeliveryLead;
use App\Functions\DataIndex;
use App\Functions\DOMDocumentExtended;
use App\Functions\System;
use App\Functions\Translation;
use App\Models\Request;
use App\Models\RequestDelivery;
use Exception;
use GuzzleHttp;

class Movegistics implements RequestDeliveryInterface
{
    private $client;

    /**
     * @var Translation
     */
    private $translation;

    public function __construct(GuzzleHttp\Client $client, Translation $translation)
    {
        $this->client = $client;
        $this->translation = $translation;
    }

    /*
     *  This function is called from a controller
     *  This function should build the data and send it over HTTP
     */

    public function send(RequestDelivery $request_delivery, DeliveryLead $lead) : void
    {
        $data = $this->buildData($request_delivery, $lead);
        $this->sendHTTP($data, $request_delivery);
    }

    public function isValidForRequestDelivery(RequestDelivery $request_delivery): bool
    {
        return $request_delivery->rede_type == CustomerRequestDeliveryType::MOVEGISTICS;
    }

    private function buildData(RequestDelivery $request_delivery, DeliveryLead $lead) : string
    {
        $xml = new DomDocumentExtended("1.0", "UTF-8");
        $xml->preserveWhiteSpace = false;
        $xml->formatOutput = true;

        $MoverLead = $xml->appendChild($xml->createElement("MoverLead"));
        $MoverLead->setAttribute("version", "1.0");
        $MoverLead->setAttribute("source", System::getSetting("mail_default_from_name"));

        $MoverLead = $this->buildUserContactData($MoverLead, $xml, $lead);
        $MoverLead = $this->buildMoveDetails($MoverLead, $xml, $lead, $request_delivery);

        return $xml->saveXML();
    }

    private function sendHTTP($data, RequestDelivery $request_delivery)
    {
        $movegistics_url = (env("APP_ENV") == "production" ? $request_delivery->rede_value : "http://movegistics.example.net");

        $client = $this->client;
        $res = $client->request('POST', $movegistics_url,
            [
                'form_params' => ["yourData" => $data]
            ]
        );

        if ($res->getStatusCode() >= 300)
        {
            throw new Exception("Received statscode: " . $res->getStatusCode());
        }

        $expected = "OK";
        if (substr($res->getBody(), 0, strlen($expected)) !== $expected)
        {
            throw new Exception("Unexpected result: " . $res->getBody());
        }

    }

    private function buildUserContactData($MoverLead, DOMDocumentExtended $xml, DeliveryLead $lead) : \DOMElement
    {
        //Split up names
        $split_name['first'] = explode(' ', trim($lead->getFullName()))[0];
        $split_name['family'] = substr(strstr($lead->getFullName(), " "), 1);

        $UserContact = $MoverLead->appendChild($xml->createElement("UserContact"));
        $UserContact->appendChild($xml->createElementText("FirstName", $split_name['first']));
        $UserContact->appendChild($xml->createElementText("LastName", $split_name['family']));
        $UserContact->appendChild($xml->createElementText("EmailAddress", $lead->getEmail()));
        $UserContact->appendChild($xml->createElementText("HomePhone", $lead->getTelephone1()));
        $UserContact->appendChild($xml->createElementText("BestTimeCall", ""));
        $UserContact->appendChild($xml->createElementText("ContactAtWork", ""));
        $UserContact->appendChild($xml->createElementText("Address1", ""));
        $UserContact->appendChild($xml->createElementText("Address2", ""));
        $UserContact->appendChild($xml->createElementText("City", ""));
        $UserContact->appendChild($xml->createElementText("State", ""));
        $UserContact->appendChild($xml->createElementText("Zipcode", ""));
        $UserContact->appendChild($xml->createElementText("Comments", $lead->getRemarks()));

        return $MoverLead;


    }

    private function buildMoveDetails($MoverLead, DOMDocumentExtended $xml, DeliveryLead $lead, RequestDelivery $request_delivery) : \DOMElement
    {
        $MoveDetails = $MoverLead->appendChild($xml->createElement("MoveDetails"));
        $MoveDetails->appendChild($xml->createElementText("MoveType", $this->translation->get("request_type_val_".$lead->getRequestType(), $request_delivery->customerportal->customer->cu_la_code)));
        $MoveDetails->appendChild($xml->createElementText("FromCity",$lead->getCityFrom()));
        $MoveDetails->appendChild($xml->createElementText("FromState", $lead->getRegion("from")));
        $MoveDetails->appendChild($xml->createElementText("FromZip", $lead->getZipcodeFrom()));
        $MoveDetails->appendChild($xml->createElementText("FromCountry", $lead->getCountry("from", $request_delivery->customerportal->customer->cu_la_code)));
        $MoveDetails->appendChild($xml->createElementText("ToCity", $lead->getCityTo()));
        $MoveDetails->appendChild($xml->createElementText("ToState", $lead->getRegion("to")));
        $MoveDetails->appendChild($xml->createElementText("ToZip", $lead->getZipcodeTo()));
        $MoveDetails->appendChild($xml->createElementText("ToCountry",  $lead->getCountry("to", $request_delivery->customerportal->customer->cu_la_code)));
        $MoveDetails->appendChild($xml->createElementText("MoveDate", date("d/m/Y", strtotime($lead->getMovingDate()))));
        $MoveDetails->appendChild($xml->createElementText("MoveSize", 0));

        return $MoverLead;


    }

}
