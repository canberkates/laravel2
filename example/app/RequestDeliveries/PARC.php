<?php

namespace App\RequestDeliveries;

use App\Data\CustomerRequestDeliveryType;
use App\Data\DeliveryLead;
use App\Functions\DataIndex;
use App\Functions\DOMDocumentExtended;
use App\Functions\System;
use App\Functions\Translation;
use App\Models\Request;
use App\Models\RequestDelivery;
use Exception;
use GuzzleHttp;
use SoapClient;

class PARC implements RequestDeliveryInterface{

    /**
     * @var Translation
     */
    private $translation;

    public function __construct(Translation $translation)
    {
        $this->translation = $translation;
    }

    /*
     *  This function is called from a controller
     *  This function should build the data and send it over HTTP
     */

    public function send(RequestDelivery $request_delivery, DeliveryLead $lead) : void
    {
        $data = $this->buildData($request_delivery, $lead);
        $this->sendLead($data, $request_delivery);
    }

    public function isValidForRequestDelivery(RequestDelivery $request_delivery) : bool{
        return $request_delivery->rede_type == CustomerRequestDeliveryType::PARC;
    }

    private function buildData(RequestDelivery $request_delivery, DeliveryLead $lead) : array
    {
        //Split up names
        $split_name['first'] = explode(' ', trim($lead->getFullName()))[0];
        $split_name['family'] = substr(strstr($lead->getFullName(), " "), 1);

        $data = array(
            'bourseId' => "TRIGLOBAL",
            'parcId' => "PARC".$request_delivery->rede_value,
            'dataLead' => array(
                'civility' => 'MR',
                'name' => ((!empty($split_name['family'])) ? $split_name['family'] : $split_name['first']),
                'firstname' => $split_name['first'],
                'phone' => $lead->getTelephone1(),
                'mobilePhone' => $lead->getTelephone2(),
                'mail' => $lead->getEmail(),
                'requestDate' => date("Y-m-d", strtotime($lead->getTimestamp())),
                'requestType' => '1',
                'society' => '',
                'loadingDateStart' => $lead->getMovingDate(),
                'loadingDateEnd' => $lead->getMovingDate(),
                'loadingAddress' => $lead->getStreetFrom(),
                'loadingZipCode' => $lead->getZipcodeFrom(),
                'loadingCity' => $lead->getCityFrom(),
                'loadingCountry' => $lead->getCoCodeFrom(),
                'loadingInfo' => '',
                'loadingElevator' => '0',
                'loadingFloor' => '0',
                'deliveryDateStart' => $lead->getMovingDate(),
                'deliveryDateEnd' => $lead->getMovingDate(),
                'deliveryAddress' => $lead->getStreetTo(),
                'deliveryZipCode' => ((!empty($lead->getZipcodeTo())) ? $lead->getZipcodeTo() : "-"),
                'deliveryCity' => $lead->getCityTo(),
                'deliveryCountry' => $lead->getCoCodeTo(),
                'deliveryInfo' => '',
                'deliveryElevator' => '0',
                'deliveryFloor' => '0',
                'furnitureVolume' => $lead->getVolumeM3(),
                'furnitureList' => array(),
                'service' => '4',
                'comments' => $lead->getRemarks(),
                'webformSID' => '',
                'distributionID' => ''
            )
        );

        return $data;
    }

    private function sendLead(array $data, RequestDelivery $request_delivery)
    {
        $url = (env("APP_ENV") == "production" ? "https://www.logiciel-demenagement-parc.com/import/wsParcImportLead.php?wsdl" : "https://example.com");

        // declare soap client
        $client = new SoapClient($url, array('cache_wsdl' => WSDL_CACHE_NONE));

        // Call wsdl function importLead
        $result = $client->__soapCall("importLead", $data);
    }

}
