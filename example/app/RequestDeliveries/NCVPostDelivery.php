<?php

namespace App\RequestDeliveries;

use App\Data\CustomerRequestDeliveryType;
use App\Data\DeliveryLead;
use App\Functions\System;
use App\Models\Request;
use App\Models\RequestDelivery;
use Exception;
use GuzzleHttp;

class NCVPostDelivery implements RequestDeliveryInterface{

    private $client;

    /**
     * @var System
     */
    private $system;

    public function __construct(GuzzleHttp\Client $client, System $system)
    {
        $this->client = $client;
        $this->system = $system;
    }

    /*
     *  This function is called from a controller
     *  This function should build the data and send it over HTTP
     */

    public function send(RequestDelivery $request_delivery, DeliveryLead $lead) : void
    {
        $data = $this->buildData($request_delivery, $lead);
        $this->sendHTTP($data, $request_delivery);
    }

    public function isValidForRequestDelivery(RequestDelivery $request_delivery) : bool{
        return $request_delivery->rede_type == CustomerRequestDeliveryType::NCV;
    }

    private function buildData(RequestDelivery $request_delivery, DeliveryLead $lead) : array
    {
        //Split up names
        $split_name['first'] = explode(' ', trim($lead->getFullName()))[0];
        $split_name['family'] = substr(strstr($lead->getFullName(), " "), 1);

        $moving_size = 0;

        if ($lead->getMovingSize() == 3) {
            $moving_size = 1;
        }
        elseif ($lead->getMovingSize() == 2) {
            $moving_size = 2;
        }
        elseif ($lead->getMovingSize() == 1) {
            $moving_size = 3;
        }

        $request = [
            "source_id" => 28039,
            "first_name" => $split_name['first'],
            "last_name" => $split_name['family'],
            "email" => $lead->getEmail(),
            "phone1" => $lead->getTelephone1(),
            "ipaddr" => $lead->getIpAddress(),
            "captured" => $lead->getTimestamp(),
            "zip1" => $lead->getZipcodeFrom(),
            "city1" => $lead->getCityFrom(),
            "country1" => $lead->getCoCodeFrom(),
            "zip2" => $lead->getZipcodeTo(),
            "city2" => $lead->getCityTo(),
            "country2" => $lead->getCoCodeTo(),
            "move_date" => $lead->getMovingDate(),
            //"move_size" => System::getTranslation("moving_size_val_".$fields['request']['re_moving_size'], "EN"),
            "move_size" => $moving_size,
            "lead_source" => str_replace("www.", "", $this->system->getSetting("company_website")),
            "test_lead" => 0
        ];

        /*if ($request_delivery->rede_extra)
        {
            $request['extra'] = $request_delivery->rede_extra;
        }*/

        return $request;
    }

    private function sendHTTP(array $data, RequestDelivery $request_delivery)
    {
        $post_url = (env("APP_ENV") == "production" ? $request_delivery->rede_value : "https://example.com");

        $client = $this->client;
        $res = $client->request('POST', $post_url,
            [
                'form_params' => $data
            ]
        );

        if ($res->getStatusCode() >= 300) {
            throw new Exception("Received statuscode: ".$res->getStatusCode());
        }

        $expected = "SUCCESS";
        if (substr( $res->getBody(), 0, strlen($expected) ) !== $expected) {
            throw new Exception("Unexpected result: ".$res->getBody());
        }

    }

}
