<?php

namespace App\RequestDeliveries;

use App\Data\CustomerRequestDeliveryType;
use App\Data\DeliveryLead;
use App\Functions\DataIndex;
use App\Functions\DOMDocumentExtended;
use App\Functions\RequestDeliveries;
use App\Functions\System;
use App\Functions\Translation;
use App\Models\Request;
use App\Models\RequestDelivery;
use DateTime;
use Exception;
use GuzzleHttp;

class P4P implements RequestDeliveryInterface{

    private $client;
    /**
     * @var Translation
     */
    private $translation;

    public function __construct(GuzzleHttp\Client $client, Translation $translation)
    {
        $this->client = $client;
        $this->translation = $translation;
    }

    /*
     *  This function is called from a controller
     *  This function should build the data and send it over HTTP
     */

    public function send(RequestDelivery $request_delivery, DeliveryLead $lead) : void
    {
        $data = $this->buildData($request_delivery, $lead);
        $this->sendHTTP($data);
    }

    public function isValidForRequestDelivery(RequestDelivery $request_delivery) : bool{
        return $request_delivery->rede_type == CustomerRequestDeliveryType::P4P;
    }

    private function buildData(RequestDelivery $request_delivery, DeliveryLead $lead) : array
    {
        //Split up names
        $split_name['first'] = explode(' ', trim($lead->getFullName()))[0];
        $split_name['family'] = substr(strstr($lead->getFullName(), " "), 1);

        $datetime = new DateTime($lead->getMovingDate());

        //US postalcode FROM
        if($lead->getCoCodeFrom() == "US")
        {
            $lead_state_from = substr(DataIndex::getRegion($lead->getRegIdFrom()), 0, 2);
        }

        //US postalcode TO
        if($lead->getCoCodeTo() == "US")
        {
            $lead_state_to = substr(DataIndex::getRegion($lead->getRegIdTo()), 0, 2);
        }

        //CA postalcode FROM
        if($lead->getCoCodeFrom() == "CA")
        {
            $lead_state_from = RequestDeliveries::getPostalCodeCanada($lead->getRegIdFrom());
        }

        //CA postalcode TO
        if($lead->getCoCodeTo() == "CA")
        {
            $lead_state_to = RequestDeliveries::getPostalCodeCanada($lead->getRegIdTo());
        }

        //US Phone number code
        $telephonenumber = $lead->getTelephone1();
        $telephonenumber = str_replace('-', '', $telephonenumber);
        $telephonenumber = str_replace('(', '', $telephonenumber);
        $telephonenumber = str_replace(')', '', $telephonenumber);
        $telephonenumber = str_replace(' ', '', $telephonenumber);
        $telephonenumber = trim($telephonenumber);
        $telephonenumber = stripslashes($telephonenumber);

        if(strlen($telephonenumber) == 10){
            $tel_1 = substr($telephonenumber, 0, 3);
            $tel_2 = substr($telephonenumber, 3, 3);
            $tel_3 = substr($telephonenumber, 6, 4);
        }elseif (strlen($telephonenumber) == 11){
            $tel_1 = substr($telephonenumber, 1, 3);
            $tel_2 = substr($telephonenumber, 4, 3);
            $tel_3 = substr($telephonenumber, 7, 4);
            $tel_int = substr($telephonenumber, 0, 1);
        }elseif (strlen($telephonenumber) == 12){
            $tel_1 = substr($telephonenumber, 2, 3);
            $tel_2 = substr($telephonenumber, 5, 3);
            $tel_3 = substr($telephonenumber, 8, 4);
            $tel_int = substr($telephonenumber, 0, 2);
        }elseif (strlen($telephonenumber) == 13){
            $tel_1 = substr($telephonenumber, 3, 3);
            $tel_2 = substr($telephonenumber, 6, 3);
            $tel_3 = substr($telephonenumber, 9, 4);
            $tel_int = substr($telephonenumber, 0, 3);
        }elseif (strlen($telephonenumber) == 14){
            $tel_1 = substr($telephonenumber, 3, 3);
            $tel_2 = substr($telephonenumber, 6, 3);
            $tel_3 = substr($telephonenumber, 9, 5);
            $tel_int = substr($telephonenumber, 0, 3);
        }


        $request = [

            'provider_id' => $request_delivery->rede_value,
            'mover_id' => "1195_TriG",
            "lead_provider_ref" => $this->getReId($request_delivery, $lead),

            "move_date_year" => $datetime->format('Y'),
            "move_date_month" => $datetime->format('m'),
            "move_date_day" => $datetime->format('d'),

            "lead_name_first" => $split_name['first'],
            "lead_name_last" => $split_name['family'],
            "lead_email" => $lead->getEmail(),

            "lead_phone1" => $tel_1,
            "lead_phone2" => $tel_2,
            "lead_phone3" =>  $tel_3,
            "lead_phone_intl" => $tel_int,

            "lead_from_add1" => $lead->getStreetFrom(),
            "lead_from_city" => $lead->getCityTo(),
            "lead_from_postal" => $lead->getZipcodeTo(),
            "lead_from_state" => $lead_state_from,
            "lead_from_country" => $lead->getCoCodeFrom(),

            "lead_to_add1" => $lead->getStreetTo(),
            "lead_to_city" => $lead->getCityTo(),
            "lead_to_postal" => $lead->getZipcodeTo(),
            "lead_to_state" => $lead_state_to,
            "lead_to_country" => $lead->getCoCodeTo(),

            "lead_lbs" => 7777,
            "lead_notes" => str_replace(["\r\n", "\r", "\n"], " ", $lead->getRemarks())
        ];

        return $request;
    }

    private function sendHTTP(array $data)
    {
        $post_url = (env("APP_ENV") == "production" ? "https://www.p4pmarketing.com/lead_provider/new_lead.php" : "https://example.com");

        $client = $this->client;
        $res = $client->request('POST', $post_url,
            [
                'form_params' => $data
            ]
        );

        if ($res->getStatusCode() >= 300) {
            throw new Exception("Received statuscode: ".$res->getStatusCode());
        }

        if (!preg_match("/^\d+$/", $res->getBody())) {
            throw new Exception("Unexpected result: ".$res->getBody());
        }

    }

    private function getReId(RequestDelivery $request_delivery, DeliveryLead $lead): int
    {
        return $request_delivery->getMatchIdForRequest($lead);
    }

}
