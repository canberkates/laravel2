<?php

namespace App\RequestDeliveries;

use App\Data\CustomerRequestDeliveryType;
use App\Data\DeliveryLead;
use App\Functions\DataIndex;
use App\Functions\DOMDocumentExtended;
use App\Functions\System;
use App\Models\Request;
use App\Models\RequestDelivery;
use Exception;
use GuzzleHttp;

class MoveWare implements RequestDeliveryInterface{

    private $client;

    public function __construct(GuzzleHttp\Client $client)
    {
        $this->client = $client;
    }

    /*
     *  This function is called from a controller
     *  This function should build the data and send it over HTTP
     */

    public function send(RequestDelivery $request_delivery, DeliveryLead $lead) : void
    {
        $data = $this->buildData($request_delivery, $lead);
        $this->sendHTTP($data);
    }

    public function isValidForRequestDelivery(RequestDelivery $request_delivery) : bool{
        return $request_delivery->rede_type == CustomerRequestDeliveryType::MOVEWARE;
    }

    private function buildData(RequestDelivery $request_delivery, DeliveryLead $lead) : array
    {
        $xml = new DomDocumentExtended("1.0", "UTF-8");
        $xml->preserveWhiteSpace = false;
        $xml->formatOutput = true;

        $moveware_url = (env("APP_ENV") == "production" ? "http://www.moveware.com.au/MovewareConnect.xsd" : "https://moveware.aspx.example.com");

        $MWConnect = $xml->appendChild($xml->createElement("MWConnect"));
        $MWConnect->setAttribute("xmlns", $moveware_url);

        $Webquote = $this->buildGeneralData($MWConnect, $xml, $lead, $request_delivery);

        $Removal = $this->buildMoveInformationData($xml, $request_delivery, $lead);
        $Webquote->appendChild($Removal);

        $Removal->appendChild($xml->createElement("SessionID", rand(111111111111111111, 999999999999999999)));

        return [
            "companyname" => System::getSetting("mail_default_from_name"),
            "companyserial" => "76VK-9ZV6-JSB5",
            "procedure" => "setxml",
            "optionalval" => $xml->saveXML()
        ];

    }

    private function sendHTTP(array $data)
    {
        $client = $this->client;
        $res = $client->request('POST', 'https://moveware.aspx.example.com',
            [
                'form_params' => $data
            ]
        );

        if ($res->getStatusCode() >= 300) {
            throw new Exception("Received statuscode: ".$res->getStatusCode());
        }

        $expected = "XML uploaded successfully";
        if (substr( $res->getBody(), 0, strlen($expected) ) !== $expected) {
            throw new Exception("Unexpected result: ".$res->getBody());
        }

    }

    private function getReId(RequestDelivery $request_delivery, DeliveryLead $lead): int
    {
        return $request_delivery->getMatchIdForRequest($lead);
    }

    private function buildMoveInformationData(DOMDocumentExtended $xml, RequestDelivery $request_delivery, DeliveryLead $lead) : \DOMElement
    {
        $Removal = $xml->createElement("Removal");
        $Removal->appendChild($xml->createElementText("Referral", "TriGlobal"));
        $Removal->appendChild($xml->createElementText("Branch", $request_delivery->rede_extra));
        $Removal->appendChild($xml->createElementText("EstMoveDate", date("d/m/Y", strtotime($lead->getMovingDate()))));
        $Removal->appendChild($xml->createElementText("BillerName", System::getSetting("mail_default_from_name")));
        $Removal->appendChild($xml->createElementText("Order", $this->getReId($request_delivery, $lead)));
        $Removal->appendChild($xml->createElementText("InternalNotes", str_replace(["\r\n", "\r", "\n"], " ", $lead->getRemarks())));
        $Dates = $Removal->appendChild($xml->createElement("Dates"));
        $Dates->appendChild($xml->createElementText("type", "Uplift"));
        $Dates->appendChild($xml->createElementText("Date", date("Y-m-d", strtotime($lead->getMovingDate()))));
        $Removal->appendChild($xml->createElementText("ixType", "DD"));

        $Origin = $this->buildOriginAddressData($xml, $lead);
        $Removal->appendChild($Origin);

        $Destination = $this->buildDestinationAddressData($xml, $lead);
        $Removal->appendChild($Destination);

        return $Removal;
    }

    private function buildOriginAddressData(DOMDocumentExtended $xml, DeliveryLead $lead): \DOMElement
    {
        $Origin = $xml->createElement("Origin");

        $Name = $Origin->appendChild($xml->createElement("Name"));
        $Name->appendChild($xml->createElementText("Name", $lead->getFullName()));
        $Name->appendChild($xml->createElementText("Email", $lead->getEmail()));
        $Name->appendChild($xml->createElementText("Phone", $lead->getTelephone1()));
        $Name->appendChild($xml->createElementText("Mobile", $lead->getTelephone2()));

        $Address = $Origin->appendChild($xml->createElement("Address"));
        $Address->appendChild($xml->createElement("Address1", $lead->getStreetFrom()));
        $Address->appendChild($xml->createElement("Suburb", $lead->getCityFrom()));
        $Address->appendChild($xml->createElement("State", DataIndex::getRegion($lead->getRegIdFrom())));

        $CountryCode = $Address->appendChild($xml->createElement("CountryCode"));
        $CountryCode->appendChild($xml->createElement("CountryDesc",  $lead->getCityFrom()));
        $CountryCode->appendChild($xml->createElement("CountryState", DataIndex::getRegion($lead->getRegIdFrom())));
        $CountryCode->appendChild($xml->createElement("CountryCountry", $lead->getCoCodeFrom()));

        $Address->appendChild($xml->createElement("Postcode", $lead->getZipcodeFrom()));
        $Address->appendChild($xml->createElement("Access"));
        $Address->appendChild($xml->createElement("Comments"));

        return $Origin;
    }

    private function buildDestinationAddressData( DOMDocumentExtended $xml, DeliveryLead $lead): \DOMElement
    {
        $Destination = $xml->createElement("Destination");

        $Name = $Destination->appendChild($xml->createElement("Name"));
        $Name->appendChild($xml->createElementText("Name", $lead->getFullName()));
        $Name->appendChild($xml->createElementText("Email", $lead->getEmail()));
        $Name->appendChild($xml->createElementText("Phone", $lead->getTelephone1()));
        $Name->appendChild($xml->createElementText("Mobile", $lead->getTelephone2()));

        $Address = $Destination->appendChild($xml->createElement("Address"));
        $Address->appendChild($xml->createElement("Address1", $lead->getStreetTo()));
        $Address->appendChild($xml->createElement("Suburb", $lead->getCityTo()));
        $Address->appendChild($xml->createElement("State", DataIndex::getRegion($lead->getRegIdTo())));

        $CountryCode = $Address->appendChild($xml->createElement("CountryCode"));
        $CountryCode->appendChild($xml->createElement("CountryDesc", $lead->getCityTo()));
        $CountryCode->appendChild($xml->createElement("CountryState", DataIndex::getRegion($lead->getRegIdTo())));
        $CountryCode->appendChild($xml->createElement("CountryCountry", $lead->getCoCodeTo()));

        $Address->appendChild($xml->createElement("Postcode", $lead->getZipcodeTo()));
        $Address->appendChild($xml->createElement("Access"));
        $Address->appendChild($xml->createElement("Comments"));

        return $Destination;
    }

    private function buildGeneralData($MWConnect, DOMDocumentExtended $xml, DeliveryLead $lead, RequestDelivery $request_delivery) : \DOMElement
    {
        $Details = $MWConnect->appendChild($xml->createElement("Details"));
        $Details->appendChild($xml->createElementText("Date", date("Y-m-d H:i:s:") . ":000"));
        $Details->appendChild($xml->createElementText("Subject", "Quote Request"));
        $Details->appendChild($xml->createElementText("Comments", str_replace(["\r\n", "\r", "\n"], " ", $lead->getRemarks())));

        $Webquote = $MWConnect->appendChild($xml->createElement("MessageType"))->appendChild($xml->createElement("WebQuote"));
        $Webquote->appendChild($xml->createElementText("CompanyToMQID", $request_delivery->rede_value));

        return $Webquote;
    }

}
