<?php

namespace App\RequestDeliveries;

use App\Data\CustomerRequestDeliveryType;
use App\Data\DeliveryLead;
use App\Functions\System;
use App\Models\Request;
use App\Models\RequestDelivery;
use Exception;
use GuzzleHttp;

class Scout24PostDelivery implements RequestDeliveryInterface{

    private $client;

    /**
     * @var System
     */
    private $system;

    public function __construct(GuzzleHttp\Client $client, System $system)
    {
        $this->client = $client;
        $this->system = $system;
    }

    /*
     *  This function is called from a controller
     *  This function should build the data and send it over HTTP
     */

    public function send(RequestDelivery $request_delivery, DeliveryLead $lead) : void
    {
        $data = $this->buildData($request_delivery, $lead);
        $this->sendHTTP($data, $request_delivery);
    }

    public function isValidForRequestDelivery(RequestDelivery $request_delivery) : bool{
        return $request_delivery->rede_type == CustomerRequestDeliveryType::SCOUT24;
    }

    private function buildData(RequestDelivery $request_delivery, DeliveryLead $lead) : array
    {
        $popular_german_streets =
            [
                "strasse 12",
                "Hauptstrasse 27",
                "Bahnhofstrasse 14",
                "Schulstrasse 8",
                "Kirchenstrasse 2",
                "Schnellstrasse 7",
                "Schillerstrasse 16",
                "Goethestrasse 22",
                "Friedhofstrasse 4",
                "Beethovenstrasse 9",
                "Dorfstrasse 5",
                "Gartenstrasse 1"
            ];

        $google_change = false;

        $zipcode_from = $this->checkForGermanZipcode("from", $lead, $popular_german_streets, $google_change);
        $zipcode_to = $this->checkForGermanZipcode("to", $lead, $popular_german_streets, $google_change);

        if($zipcode_from == null || $zipcode_to == null){
            $this->system->sendMessage("3653", "Was not able to find a zipcode for lead ".$lead->getReId().". Zipcode from: ".$zipcode_from.". Zipcode to: ".$zipcode_to, $this->system->serialize($lead));
            exit;
        }

        //Split up names
        $split_name['first'] = explode(' ', trim($lead->getFullName()))[0];
        $split_name['family'] = substr(strstr($lead->getFullName(), " "), 1);

        if( $lead->getVolumeM3() == 0){
            //If complete household
            if($lead->getMovingSize() == 1){
                $lead->setVolumeM3(15);
            }
            //Part of household
            elseif($lead->getMovingSize()== 2){
                $lead->setVolumeM3(8);
            }
            //Small move
            elseif($lead->getMovingSize() == 3){
                $lead->setVolumeM3(4);
            }
        }

        //Add vital information like a fake zipcode and the volume to the request
        $pretext = "";

        if($google_change){
            $pretext .= "Die Straßenangaben der Wohnorte müssen ggf. mit dem Antragssteller geprüft werden. ";
        }

        if($lead->getVolumeM3()) {
            $pretext .= "Volume: ".$lead->getVolumeM3()."(m3). ";
        }

        $requestdata = [
            "type" => "RELOCATION",
            "aff" => $request_delivery->rede_value,
            "consumer" =>
                [
                    "firstName" => $split_name['first'],
                    "lastName" => (empty($split_name['family']) ? $split_name['first'] : $split_name['family']),
                    "salutation" => "MR",
                    "telephone" => $lead->getTelephone1(),
                    "email" => $lead->getEmail(),
                ],
            "additionalInformation" => $pretext.(!empty($lead->getRemarks()) ? "Remark: ".str_replace(["\r\n", "\r", "\n"], " ", $lead->getRemarks()) : ""),
            "payment" => "PRIVATE",

            "from" => [
                "street" => $lead->getStreetFrom(),
                "zipcode" => $zipcode_from,
                "city" => $lead->getCityFrom(),
                "country" => $lead->getCoCodeFrom(),
                "date" =>date('Y-m-d', strtotime( $lead->getMovingDate())),
                'dateType' => "START",
                'area' => (($lead->getVolumeM3() * 3) < 10 ? 10 : round($lead->getVolumeM3() * 3)),

            ],
            "to" => [
                "street" => $lead->getStreetTo(),
                "zipcode" => $zipcode_to,
                "city" => $lead->getCityTo(),
                "country" => $lead->getCoCodeTo(),
                "date" =>date('Y-m-d', strtotime($lead->getMovingDate())),
                'dateType' => "START"
            ]


        ];

        if($lead->getPacking()){
            $requestdata["features"][] = "PACK";
            $requestdata["features"][] = "UNPACK";
        }

        if($lead->getAssembly()){
            $requestdata["features"][] = "FURNITURE_ASSEMBLY";
            $requestdata["features"][] = "FURNITURE_DISASSEMBLY";
        }

        return $requestdata;
    }

    private function sendHTTP(array $data, RequestDelivery $request_delivery)
    {
        $post_url = (env("APP_ENV") == "production" ? "https://api.relocation.immobilienscout24.de/relocationrequest" : "http://example.net");

        $client = $this->client;
        $res = $client->request('POST', $post_url,
            [
                'form_params' => $data
            ]
        );

        if ($res->getStatusCode() >= 300) {
            throw new Exception("Received statuscode: ".$res->getStatusCode());
        }

        $expected = "SUCCESS";
        if (substr( $res->getBody(), 0, strlen($expected) ) !== $expected) {
            throw new Exception("Unexpected result: ".$res->getBody());
        }

    }

    /**
     * @param string $google_search
     * @return array
     */
    private function getGermanZipcode($google_search)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($google_search) . "&country:DE&result_type=postal_code&key=AIzaSyBqW3eoOlVa5S8CIPMb4wO48vnNWExrYCs");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);

        $google_result = json_decode($result, true);

        $last = array_key_last($google_result["results"][0]['address_components']);

        if(in_array("postal_code", $google_result["results"][0]['address_components'][$last]["types"])) {
            return $google_result["results"][0]['address_components'][$last]['short_name'];

        }else{
            return null;
        }
    }

    /**
     *
     * @param array $popular_german_streets
     *
     */
    private function checkForGermanZipcode($direction, DeliveryLead $lead, $popular_german_streets, &$google_change)
    {
        //If not exactly five numbers in zipcode (from or to)
        if (!preg_match('/^\d{5}$/', $lead->{"getZipcode".ucfirst($direction)}())) {

            //First check if the JSON-file has the anwser
            $json_result = $this->checkJSONFile($direction, $lead);
            if($json_result){
                if($direction == "from" && empty($request->re_street_from)){
                    $lead->setStreetFrom($popular_german_streets[0]);
                }
                return $json_result;
            }

            //We are now counting on Google for the zipcode/street
            $google_change = true;
            //Add street_from to possible streetnames
            if (!empty($lead->{"getStreet".ucfirst($direction)}())) {
                array_unshift($popular_german_streets, $lead->{"getStreet".ucfirst($direction)}());

            }

            foreach ($popular_german_streets as $german_street) {
                $google_search = $german_street . " " . $lead->{"getCity".ucfirst($direction)}(). " " . $lead->{"getCoCode".ucfirst($direction)}();
                $result = $this->getGermanZipcode($google_search);
                if (!empty($result)) {
                    if($direction == "from") {
                        $lead->setStreetFrom($german_street);
                    }
                    return $result;
                }
            }

            return null;


        }else{
            return $lead->{"getZipcode".ucfirst($direction)}();
        }

    }

    private function checkJSONFile($direction, DeliveryLead $lead){
        $jsonfile = file_get_contents(env("SHARED_FOLDER")."zipcodes.de.json");
        $json = json_decode($jsonfile);

        foreach($json as $line){
            if(strtolower($line->place) == strtolower($lead->{"getCity".ucfirst($direction)}())){
                return $line->zipcode;
            }
        }


    }

}
