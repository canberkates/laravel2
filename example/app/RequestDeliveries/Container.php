<?php


namespace App\RequestDeliveries;


use Exception;
use ReflectionClass;

class Container
{

    /**
     *
     * @param mixed $class
     * @param array $params
     *
     */
    public function make($class, array $params = [])
    {

        $reflection = new ReflectionClass($class);

        $constructor = $reflection->getConstructor();

        $resolvedParameters = [];

        foreach ($constructor->getParameters() as $parameter) {

            $parameterClass = $parameter->getClass();
            $className = $parameterClass->name;
            $parameterName = $parameter->getName();


            if (null === $parameterClass) {

                if (isset($params[$parameterName])) {
                    $resolvedParameters[$parameterName] = $params[$parameterName];
                } else {
                    throw new Exception(
                        sprintf('Container could not solve %s parameter', $parameterName)
                    );
                }

            } else {

                $resolvedParameters[$parameterName] = $this->make($className);
            }
        }

        dd($resolvedParameters);

        return $reflection->newInstanceArgs($resolvedParameters);
    }
}
