<?php

namespace App\RequestDeliveries;

use App\Data\CustomerRequestDeliveryType;
use App\Data\DeliveryLead;
use App\Functions\DataIndex;
use App\Functions\DOMDocumentExtended;
use App\Functions\Mail;
use App\Functions\RequestData;
use App\Functions\RequestDeliveries;
use App\Functions\System;
use App\Models\Request;
use App\Models\RequestDelivery;
use App\Models\VolumeCalculator;
use DB;
use Exception;
use GuzzleHttp;

class LegacyDeliveryHandler implements RequestDeliveryInterface{

    //TODO: Everytime a new Delivery is implemented, remove it from this list

    private $validDeliveryMethods = [
        CustomerRequestDeliveryType::MAIL_DEFAULT,
        CustomerRequestDeliveryType::MAIL_PLAIN_TEXT,
        CustomerRequestDeliveryType::XML_MAIL,
        CustomerRequestDeliveryType::CSV_ATTACHMENT,
        CustomerRequestDeliveryType::ALLIED_PICKFORDS,
        CustomerRequestDeliveryType::MOVERWORX,
        CustomerRequestDeliveryType::GRANOT,
        CustomerRequestDeliveryType::SALESFORCE,
        CustomerRequestDeliveryType::VOXME,
        CustomerRequestDeliveryType::EMOVER,
        CustomerRequestDeliveryType::PREMIUM_MOVERS,
        CustomerRequestDeliveryType::BEST_GLOBAL_MOVERS,
        CustomerRequestDeliveryType::BIARD_DEMENAGEMENTS,
        CustomerRequestDeliveryType::FAZLAND,
        CustomerRequestDeliveryType::MY_MOVING_LOADS,
        CustomerRequestDeliveryType::REMOVALS_MANAGER,
        CustomerRequestDeliveryType::EUROPEAN_MOVING,
        CustomerRequestDeliveryType::MAIL_JSON,
        CustomerRequestDeliveryType::MOVINGA,
        CustomerRequestDeliveryType::INTERNATIONAL_SEA_AND_AIR_SHIPPING,
        CustomerRequestDeliveryType::SALESFORCE_GINTER,
        CustomerRequestDeliveryType::SALESFORCE_UNIVERSAL,
        CustomerRequestDeliveryType::DEMENGO,
        CustomerRequestDeliveryType::MOVEWARE_SIMPSONS,
        CustomerRequestDeliveryType::MOVEWARE_OSS,
    ];

    public function isValidForRequestDelivery(RequestDelivery $requestDelivery) : bool
    {
        return in_array($requestDelivery->rede_type, $this->validDeliveryMethods);
    }

    /*
     *  This function is called from a controller
     *  This function should send data to the old request delivery API
     */
    public function send(RequestDelivery $request_delivery, DeliveryLead $lead) : void
    {
        $request_data = new RequestData();
        $system = new System();
        $mail = new Mail();
        $request_deliveries = new RequestDeliveries();
        $request = Request::find($lead->getReId());

        $ktrecupo = DB::table("kt_request_customer_portal")
            ->leftJoin("kt_customer_portal", "ktrecupo_ktcupo_id", "ktcupo_id")
            ->leftJoin("requests", "ktrecupo_re_id", "re_id")
            ->leftJoin("volume_calculator", "voca_id", "re_voca_id")
            ->leftJoin("customers", "ktrecupo_cu_id", "cu_id")
            ->where("ktrecupo_id", $request->requestcustomerportals->where("ktrecupo_cu_id", $request_delivery->customerportal->customer->cu_id)->first()->ktrecupo_id)
            ->where("cu_deleted", 0)
            ->first();

        $ktrecupo->re_volume_calculator = ((!empty($ktrecupo->re_voca_id)) ? VolumeCalculator::where("voca_id", $ktrecupo->re_voca_id)->first()->voca_volume_calculator : null);


        foreach(RequestDeliveries::receivers($ktrecupo->cu_type, $ktrecupo->ktcupo_id, $ktrecupo->cu_email) as $receiver)
        {
            //Only send if current request delivery is the same as the found/looped request delivery
            if($request_delivery->rede_id != $receiver['rede_id']){continue;}


            $count_matched_to = $request_data->requestMatchesDone($ktrecupo->re_id);
            $matched_to = $count_matched_to - 1;

            if ($matched_to <= 2)
                $matched_to = "0-2";

            $fields = [
                "request_delivery_value" => $receiver['value'],
                "request_delivery_extra" => $receiver['extra'],
                "match_type" => (($ktrecupo->ktrecupo_type == 3)?2:1),
                "cu_id" => $ktrecupo->cu_id,
                "cu_re_id" => $ktrecupo->ktrecupo_cu_re_id,
                "language" => $ktrecupo->cu_la_code,
                "request" => $system->databaseToArray($ktrecupo),
                "matched_to" => $matched_to
            ];

            if($receiver['type'] == 1)
            {
                $mail->send("customer_request", $ktrecupo->cu_la_code, $fields);
            }
            else
            {
                $request_deliveries->send(1, $receiver['type'], $ktrecupo->cu_la_code, $fields);
            }
        }
    }


}
