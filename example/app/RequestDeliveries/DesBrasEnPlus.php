<?php

namespace App\RequestDeliveries;

use App\Data\CustomerRequestDeliveryType;
use App\Data\DeliveryLead;
use App\Functions\DataIndex;
use App\Functions\DOMDocumentExtended;
use App\Functions\System;
use App\Functions\Translation;
use App\Models\Request;
use App\Models\RequestDelivery;
use Exception;
use GuzzleHttp;

class DesBrasEnPlus implements RequestDeliveryInterface{

    private $client;
    /**
     * @var Translation
     */
    private $translation;

    public function __construct(GuzzleHttp\Client $client, Translation $translation)
    {
        $this->client = $client;
        $this->translation = $translation;
    }

    /*
     *  This function is called from a controller
     *  This function should build the data and send it over HTTP
     */

    public function send(RequestDelivery $request_delivery, DeliveryLead $lead) : void
    {
        $data = $this->buildData($request_delivery, $lead);
        $this->sendHTTP($data, $request_delivery);
    }

    public function isValidForRequestDelivery(RequestDelivery $request_delivery) : bool{
        return $request_delivery->rede_type == CustomerRequestDeliveryType::DES_BRAS_EN_PLUS;
    }

    private function buildData(RequestDelivery $request_delivery, DeliveryLead $lead) : array
    {
        //Split up names
        $split_name['first'] = explode(' ', trim($lead->getFullName()))[0];
        $split_name['family'] = substr(strstr($lead->getFullName(), " "), 1);

        if(empty($split_name['family'])){
            $split_name['family'] = $split_name['first'];
        }

        $lead->setTelephone1(str_replace( "+3", "", $lead->getTelephone1()));
        $lead->setTelephone1(str_replace( " ", "", $lead->getTelephone1()));

        if(strlen($lead->getTelephone1()) === 9){
            $lead->setTelephone1("0".$lead->getTelephone1());
        }

        $request = [
            "date_dem" => date("d/m/Y", strtotime($lead->getMovingDate())),
            "token" => $request_delivery->rede_value,
            "nom" => $split_name['family'],
            "prenom" => $split_name['first'],
            "email" => $lead->getEmail(),
            "telephone" => $lead->getTelephone1(),
            "surface" => $lead->getVolumeM3(),
            "code_postal_depart" => $lead->getZipcodeFrom(),
            "code_postal_arrivee" => $lead->getZipcodeTo(),
        ];

        return $request;
    }

    private function sendHTTP(array $data, RequestDelivery $request_delivery)
    {
        $post_url = (env("APP_ENV") == "production" ? "https://api.leads.desbrasenplus.com/add-leads.php" : "https://example.com");

        $client = $this->client;
        $res = $client->request('POST', $post_url,
            [
                'form_params' => $data
            ]
        );

        if ($res->getStatusCode() >= 300) {
            throw new Exception("Received statuscode: ".$res->getStatusCode());
        }

        /*$expected = "XML uploaded successfully";
        if (substr( $res->getBody(), 0, strlen($expected) ) !== $expected) {
            throw new Exception("Unexpected result: ".$res->getBody());
        }*/

    }

    private function getReId(RequestDelivery $request_delivery, DeliveryLead $lead): int
    {
        return $request_delivery->getMatchIdForRequest($lead);
    }

}
