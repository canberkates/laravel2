<?php

namespace App\RequestDeliveries;

use App\Data\CustomerRequestDeliveryType;
use App\Data\DeliveryLead;
use App\Functions\DataIndex;
use App\Functions\DOMDocumentExtended;
use App\Functions\System;
use App\Functions\Translation;
use App\Models\Request;
use App\Models\RequestDelivery;
use Exception;
use GuzzleHttp;

class HTTPPostDelivery implements RequestDeliveryInterface{

    private $client;
    /**
     * @var Translation
     */
    private $translation;

    public function __construct(GuzzleHttp\Client $client, Translation $translation)
    {
        $this->client = $client;
        $this->translation = $translation;
    }

    /*
     *  This function is called from a controller
     *  This function should build the data and send it over HTTP
     */

    public function send(RequestDelivery $request_delivery, DeliveryLead $lead) : void
    {
        $data = $this->buildData($request_delivery, $lead);
        $this->sendHTTP($data, $request_delivery);
    }

    public function isValidForRequestDelivery(RequestDelivery $request_delivery) : bool{
        return $request_delivery->rede_type == CustomerRequestDeliveryType::HTTP_POST;
    }

    private function buildData(RequestDelivery $request_delivery, DeliveryLead $lead) : array
    {
        //Split up names
        $split_name['first'] = explode(' ', trim($lead->getFullName()))[0];
        $split_name['family'] = substr(strstr($lead->getFullName(), " "), 1);

        $request = [
            "lead_source" => System::getSetting("mail_default_from_name"),
            "lead_id" => $this->getReId($request_delivery, $lead),
            "request_type" => $this->translation->get("request_type_val_" . $lead->getRequestType(), $request_delivery->customerportal->customer->cu_la_code),
            "moving_size" => $this->translation->get("moving_size_val_" . $lead->getMovingSize(), $request_delivery->customerportal->customer->cu_la_code),
            "moving_date" => $lead->getMovingDate(),
            "volume_m3" => $lead->getVolumeM3(),
            "volume_ft3" =>  $lead->getVolumeFt3(),
            "storage" => $this->translation->get("storage_val_" . $lead->getStorage(), $request_delivery->customerportal->customer->cu_la_code),
            "packing" => $this->translation->get("self_company_val_" .$lead->getPacking(), $request_delivery->customerportal->customer->cu_la_code),
            "assembly" => $this->translation->get("self_company_val_" . $lead->getAssembly(), $request_delivery->customerportal->customer->cu_la_code),
            "business" => $this->translation->get(("re_business" == 1  ? "yes" :  "no"), $request_delivery->customerportal->customer->cu_la_code),
            "pickup_street" => $lead->getStreetFrom(),
            "pickup_zipcode" =>  $lead->getZipcodeFrom(),
            "pickup_city" =>  $lead->getCityFrom(),
            "pickup_country" =>  $lead->getCoCodeFrom(),
            "pickup_region" => DataIndex::getRegion($lead->getRegIdFrom()),
            "pickup_residence" => $this->translation->get("residence_val_" . $lead->getResidenceFrom(), $request_delivery->customerportal->customer->cu_la_code),
            "delivery_street" =>  $lead->getStreetTo(),
            "delivery_zipcode" =>  $lead->getZipcodeTo(),
            "delivery_city" =>  $lead->getCityTo(),
            "delivery_country" =>  $lead->getCoCodeTo(),
            "delivery_region" => DataIndex::getRegion( $lead->getRegIdTo()),
            "delivery_residence" => $this->translation->get("residence_val_" . $lead->getResidenceTo(), $request_delivery->customerportal->customer->cu_la_code),
            "company_name" =>  $lead->getCompanyName(),
            "gender" => "",
            "full_name" =>  $lead->getFullName(),
            "first_name" => $split_name['first'],
            "family_name" => $split_name['family'],
            "telephone1" =>  $lead->getTelephone1(),
            "telephone2" => $lead->getTelephone2(),
            "email" => $lead->getEmail(),
            "remarks" => str_replace(["\r\n", "\r", "\n"], " ", $lead->getRemarks())
        ];

        if($lead->getLeadType() == 2){
            $request['item_disposal'] = $lead->getItemDisposal();
            $request['handyman'] = $lead->getHandyman();
            $request['floor_type_from'] = $lead->getFloorTypeFrom();
            $request['walking_distance_from'] = $lead->getWalkingDistanceFrom();
            $request['elevator_available_from'] = $lead->getElevatorAvailableFrom();
            $request['parking_permit_needed_from'] = $lead->getParkingPermitNeededFrom();
            $request['floor_type_to'] = $lead->getFloorTypeTo();
            $request['walking_distance_to'] = $lead->getWalkingDistanceTo();
            $request['elevator_available_to'] = $lead->getElevatorAvailableTo();
            $request['parking_permit_needed_to'] = $lead->getParkingPermitNeededTo();
            $request['contact_method'] = $lead->getContactMethod();
            $request['price_estimation'] = $lead->getPriceEstimationLow() . " - " . $lead->getPriceEstimationHigh();
        }

        if ($request_delivery->rede_extra)
        {
            $request['extra'] = $request_delivery->rede_extra;
        }

        return $request;
    }

    private function sendHTTP(array $data, RequestDelivery $request_delivery)
    {
        $post_url = (env("APP_ENV") == "production" ? $request_delivery->rede_value : "https://example.com");

        $client = $this->client;
        $res = $client->request('POST', $post_url,
            [
                'form_params' => $data
            ]
        );

        if ($res->getStatusCode() >= 300) {
            throw new Exception("Received statuscode: ".$res->getStatusCode());
        }

        /*$expected = "XML uploaded successfully";
        if (substr( $res->getBody(), 0, strlen($expected) ) !== $expected) {
            throw new Exception("Unexpected result: ".$res->getBody());
        }*/

    }

    private function getReId(RequestDelivery $request_delivery, DeliveryLead $lead): int
    {
        return $request_delivery->getMatchIdForRequest($lead);
    }

}
