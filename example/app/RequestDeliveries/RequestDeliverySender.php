<?php


namespace App\RequestDeliveries;


use App\Data\DeliveryLead;
use App\Models\Request;
use App\Models\RequestDelivery;
use App\Providers\RequestDeliveryServiceProvider;

class RequestDeliverySender
{
    private $handlers;
    /**
     * @var LegacyDeliveryHandler
     */

    /**
     * RequestDeliverySender constructor.
     * @param RequestDeliveryInterface[] $handlers
     */
    public function __construct(iterable $handlers)
    {
        $this->handlers = $handlers;
    }

    public function sendRequest(RequestDelivery $requestDelivery, DeliveryLead $lead)
    {
        foreach ($this->handlers as $deliveryHandler)
        {
            if ($deliveryHandler->isValidForRequestDelivery($requestDelivery))
            {
                $deliveryHandler->send($requestDelivery, $lead);
                return;
            }
        }

        throw new \RuntimeException("No Request Delivery Found for type: ".$requestDelivery->rede_type );

    }

}
