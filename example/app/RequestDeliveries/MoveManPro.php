<?php

namespace App\RequestDeliveries;

use App\Data\CustomerRequestDeliveryType;
use App\Data\DeliveryLead;
use App\Functions\System;
use App\Functions\Translation;
use App\Models\Request;
use App\Models\RequestDelivery;
use Exception;
use GuzzleHttp;

class MoveManPro implements RequestDeliveryInterface
{
    private $client;

    /**
     * @var Translation
     */
    private $translation;

    public function __construct(GuzzleHttp\Client $client, Translation $translation)
    {
        $this->client = $client;
        $this->translation = $translation;
    }

    /*
     *  This function is called from a controller
     *  This function should build the data and send it over HTTP
     */

    public function send(RequestDelivery $request_delivery, DeliveryLead $lead) : void
    {
        $request_delivery->getMatchIdForRequest($lead);
        $data = $this->buildData($request_delivery, $lead);
        $this->sendHTTP($data);
    }

    public function isValidForRequestDelivery(RequestDelivery $request_delivery): bool
    {
        return $request_delivery->rede_type == CustomerRequestDeliveryType::MOVEMAN_PRO;
    }

    private function buildData(RequestDelivery $request_delivery, DeliveryLead $lead): array
    {
        return array_merge(
            $this->buildGeneralInformation($request_delivery, $lead),
            $this->buildMoveDetails($lead, $request_delivery)
        );
    }

    private function sendHTTP(array $data)
    {

        $moveman_url = (env("APP_ENV") == "production" ? "http://service.moveman.net/MoveManLiveServerService.asmx/SubmitEnquiriesFull" : "http://service.moveman.example.net");

        $client = $this->client;
        $res = $client->request('POST', $moveman_url,
            [
                'form_params' => $data
            ]
        );

        if ($res->getStatusCode() >= 300)
        {
            throw new Exception("Received statscode: " . $res->getStatusCode());
        }

        $expected = "OK";
        if (substr($res->getBody(), 0, strlen($expected)) !== $expected)
        {
            throw new Exception("Unexpected result: " . $res->getBody());
        }

    }

    /**
     * @param Request $request
     * @param RequestDelivery $request_delivery
     * @return string
     */
    private function buildClientNotes(DeliveryLead $lead, RequestDelivery $request_delivery): string
    {
        $ClientNotes = "Source: " . System::getSetting("mail_default_from_name") . ";";
        $ClientNotes .= "Volume: " . $lead->getVolumeM3() . " m3 / " . $lead->getVolumeFt3() . " ft3;";
        $ClientNotes .= "Storage: " . $this->translation->get("storage_val_" . $lead->getStorage(), $request_delivery->customerportal->customer->cu_la_code) . ";";
        $ClientNotes .= "Packing: " . $this->translation->get("self_company_val_" . $lead->getPacking(), $request_delivery->customerportal->customer->cu_la_code) . ";";
        $ClientNotes .= "Assembly: " . $this->translation->get("self_company_val_" . $lead->getAssembly(), $request_delivery->customerportal->customer->cu_la_code) . ";";
        $ClientNotes .= "Remarks: " . str_replace(["\r\n", "\r", "\n"], " ", $lead->getRemarks()) . ";";

        return $ClientNotes;
    }

    /**
     * @param RequestDelivery $request_delivery
     * @param Request $request
     * @return array
     */
    private function buildGeneralInformation(RequestDelivery $request_delivery, DeliveryLead $lead): array
    {
        if ($request_delivery->request_delivery_value == "split")
        {
            //Split up names
            $split_name['first'] = explode(' ', trim($lead->getFullName()))[0];
            $split_name['family'] = substr(strstr($lead->getFullName(), " "), 1);
        }

        $built_request = [
            "MoverCompanyID" => $request_delivery->request_delivery_value,
            "ClientCompanyName" => $lead->getCompanyName(),
            "ClientTitle" => "",
            "ClientFirstname" => ($request_delivery->request_delivery_value == "split" ? $split_name['first'] : $lead->getFullName()),
            "ClientSurname" => ($request_delivery->request_delivery_value == "split" ? $split_name['family'] : "")
        ];

        return $built_request;
    }

    /**
     * @param array $built_request
     * @param Request $request
     * @param RequestDelivery $request_delivery
     * @return mixed
     */
    private function buildMoveDetails(DeliveryLead $lead, RequestDelivery $request_delivery)
    {
        $ClientNotes = $this->buildClientNotes($lead, $request_delivery);

        return [
            "ClientAddress1" => $lead->getStreetFrom(),
            "ClientAddress2" => $lead->getCityFrom(),
            "ClientAddress3" => $lead->getRegion("from"),
            "ClientAddress4" => $lead->getCountry("from", $request_delivery->customerportal->customer->cu_la_code),
            "ClientPostcode" => $lead->getZipcodeFrom(),
            "ClientTelephone" => $lead->getTelephone1(),
            "ClientMobile" => $lead->getTelephone2(),
            "ClientFax" => "",
            "ClientEmail" => $lead->getEmail(),
            "ClientDCompanyName" => $lead->getCompanyName(),
            "ClientDTitle" => "",
            "ClientDFirstname" => "",
            "ClientDSurname" => $lead->getFullName(),
            "ClientDAddress1" => $lead->getStreetTo(),
            "ClientDAddress2" => $lead->getCityTo(),
            "ClientDAddress3" => $lead->getRegion("to"),
            "ClientDAddress4" => $lead->getCountry("to", $request_delivery->customerportal->customer->cu_la_code),
            "ClientDPostcode" => $lead->getZipcodeTo(),
            "ClientDTelephone" => $lead->getTelephone1(),
            "ClientDMobile" => $lead->getTelephone2(),
            "ClientDFax" => "",
            "ClientDEmail" => $lead->getEmail(),
            "ClientNotes" => $ClientNotes,
            "ClientBuildingType" => $this->translation->get("residence_val_" . $lead->getResidenceFrom(), $request_delivery->customerportal->customer->cu_la_code),
            "ClientAccess" => "",
            "ClientParking" => "",
            "ClientDirections" => "",
            "ClientDBuildingType" => $this->translation->get("residence_val_" . $lead->getResidenceTo(), $request_delivery->customerportal->customer->cu_la_code),
            "ClientDAccess" => "",
            "ClientDParking" => "",
            "ClientDDirections" => "",
            "ClientParentRefNumber" => "",
            "ClientTaxNumber" => "",
            "ClientRemovalPackDate" => "2000-01-01",
            "ClientRemovalDate" => $lead->getMovingDate(),
            "ClientRemovalDeliveryDate" => "2000-01-01",
            "ClientRemovalPeriod" => ""
        ];
    }

}
