<?php


namespace App\RequestDeliveries;


use App\Data\DeliveryLead;
use App\Models\RequestDelivery;

interface RequestDeliveryInterface
{
    public function isValidForRequestDelivery(RequestDelivery $requestDelivery) : bool;
    public function send(RequestDelivery $requestDelivery, DeliveryLead $lead) : void;
}
