<?php
namespace App\Imports;

use App\Data\CustomerMarketType;
use App\Data\CustomerServices;
use App\Data\CustomerStatusType;
use App\Models\ContactPerson;
use App\Models\Customer;
use App\Models\Language;
use App\Models\MoverData;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;

class CustomersUpdateImport implements ToCollection
{
    public function collection(Collection $rows)
    {
        $array_customers = [];
        $array_contact_persons = [];
        $array_both = [];

        foreach ($rows as $row)
        {
            $array_cust = false;

            $company_name = trim($row[0]);
            $postal_code = trim($row[1]);
            $city = trim($row[2]);
            $region = trim($row[3]);
            $country = trim($row[4]);
            $continent = trim($row[5]);
            $market = trim($row[6]);
            $services = trim($row[7]);
            $group = trim($row[8]);
            $associations = trim($row[9]);
            $labels = trim($row[10]);
            $year_or_registration = trim($row[11]);

            $language = trim($row[12]);
            $title = trim($row[13]);
            $first_name = trim($row[14]);
            $last_name = trim($row[15]);
            $phone = trim($row[16]);
            $email = trim($row[17]);
            $status = trim($row[18]);


            //Raul's file:
            /*$company_name =  trim($row[0]);
            $country = trim($row[1]);
            $continent = trim($row[2]);
            $market = trim($row[3]);
            $services = trim($row[4]);
            $associations = trim($row[5]);
            $status = trim($row[6]);
            $category = trim($row[7]);
            $language = trim($row[8]);
            $title = trim($row[9]);
            $first_name = trim($row[10]);
            $last_name = trim($row[11]);
            $email = trim($row[12]);
            $phone = trim($row[13]);*/

            $customer = Customer::whereRaw('(`cu_company_name_legal` = "'.trim($company_name).'" OR `cu_company_name_business` = "'.trim($company_name).'")')->where("cu_deleted", 0)->get();

            if (count($customer) == 1) {
                foreach($customer as $cust) {
                    $mover_data = MoverData::where("moda_cu_id", $cust->cu_id)->first();

                    if (count($mover_data) == 0) {
                        $array_cust = true;
                        $array_customers[] = $row;

                    }
                    else {
                        /*if ($market == "INT") {
                            $market_type = CustomerMarketType::INT;
                        }
                        elseif($market == "NAT") {
                            $market_type = CustomerMarketType::NAT;
                        }
                        else {
                            $market_type = CustomerMarketType::INT_NAT;
                        }

                        if ($services == "Removals") {
                            $services_int = CustomerServices::REMOVALS;
                        }
                        elseif ($services == "Full Package") {
                            $services_int = CustomerServices::FULL_PACKAGE;
                        }
                        else {
                            $services_int = CustomerServices::BAGGAGE;
                        }*/

                        $status_int = "";

                        if($status == "Prospect") {
                            $status_int = CustomerStatusType::PROSPECT;
                        }
                        elseif($status == "Partner") {
                            $status_int = CustomerStatusType::PARTNER;
                        }
                        elseif ($status == "In conversation") {
                            $status_int = CustomerStatusType::IN_CONVERSATION;
                        }
                        elseif ($status == "Waiting list") {
                            $status_int = CustomerStatusType::WAITING_LIST;
                        }
                        elseif ($status == "Excluded") { //WAT MET STATUS CANCELLED?
                            $status_int = CustomerStatusType::EXCLUDED;
                        }
                        elseif ($status == "Cancelled") { //WAT MET STATUS CANCELLED?
                            $status_int = CustomerStatusType::PROSPECT;
                        }


                        //$mover_data->moda_market_type = $market_type;
                        //$mover_data->moda_services = $services_int;
                        if (!empty($status_int)) {
                            $mover_data->moda_crm_status = $status_int;
                            $mover_data->save();
                        }

                        $revision = DB::table("revisions")
                            //->whereRaw("created_at BETWEEN '2020-08-11 12:00:00' AND '2020-10-22 14:00:00'")
                            ->whereRaw("created_at BETWEEN '2020-06-15 13:00:00' AND '2020-10-22 14:00:00'")
                            ->where("revisionable_id", $mover_data->moda_id)
                            ->where("key", "moda_crm_status")
                            ->orderBy("created_at", "DESC")
                            ->limit(1)
                            ->first();

                        if (count($revision) > 0) {
                            $crm_status = $revision->new_value;
                            //$cu_id = $mover_data->moda_cu_id;

                            $mover_data->moda_crm_status = $crm_status;
                            $mover_data->save();

                            /*DB::table('mover_data')
                                ->where('moda_cu_id', $cu_id)
                                ->update(
                                    [
                                        'moda_crm_status' => $crm_status
                                    ]
                                );*/

                        }
                    }
                }
            }
            else {
                //More than 1 customer result
                $array_cust = true;
                $array_customers[] = $row;

            }




            //CONTACT PERSON UPDATE
            /*$contact_persons = ContactPerson::where("cope_email", trim($email))->get();

            if (count($contact_persons) == 0 && $array_cust == true) {
                $array_both[] = $row;
                continue;
            }
            elseif (count($contact_persons) == 0 && $array_cust == false) {
                $array_contact_persons[] = $row;
                continue;
            }
            else {
                foreach ($contact_persons as $contact_person) {
                    $contact_person->cope_first_name = $first_name;
                    $contact_person->cope_last_name = $last_name;
                    $contact_person->cope_full_name = $first_name." ".$last_name;
                    $contact_person->cope_title = $title;
                    $contact_person->cope_telephone = $phone;
                    $contact_person->cope_language = Language::where("la_language", $language)->first()->la_code;
                    $contact_person->save();
                }

                if ($array_cust == true) {
                    $array_customers[] = $row;
                }
            }*/
        }

        //$customers_json = json_encode($array_customers);
        //file_put_contents(env("SHARED_FOLDER")."customers_raul.txt", $customers_json);

        $customers_json = json_encode($array_customers);
        //$contact_persons_json = json_encode($array_contact_persons);
        //$both_json = json_encode($array_both);

        file_put_contents(env("SHARED_FOLDER")."customers_22_10_2020.txt", $customers_json);
        //file_put_contents(env("SHARED_FOLDER")."contact_persons_raul.txt", $contact_persons_json);
        //file_put_contents(env("SHARED_FOLDER")."both_raul.txt", $both_json);
    }
}