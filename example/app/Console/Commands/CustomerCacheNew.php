<?php

namespace App\Console\Commands;

use App\Cronjobs\CronjobController;
use App\Data\CustomerMarketType;
use App\Data\PortalType;
use App\Data\SubscriptionType;
use App\Functions\System;
use App\Models\Country;
use App\Models\Customer;
use App\Models\CustomerStatus;
use App\Models\KTCustomerPortal;
use App\Models\KTCustomerSubscription;
use App\Models\User;
use Carbon\Carbon;
use DateTime;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CustomerCacheNew extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cache:customer_new';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'The cronjob of Customer Cache';

    private $cc;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CronjobController $cc)
    {
        $this->cc = $cc;
        parent::__construct();
    }

    const CRONJOB_ID = 8;
    const CRONJOB_TASK = 'Phoenix';
    const CRONJOB_SUBTASK = 'Customer new cache';
    const CRONJOB_TRIGGER = 20;
    const CRONJOB_INTERVAL = "40 MINUTE";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Set Cronjob check values
        $CRONJOB_USERS = [3653];

        try
        {
            $this->cacheCustomers();
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 0, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, null);

        }catch (\Throwable $e){
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 1, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, $e);
        }


    }

    private function setINTNATCustomers($customers)
    {
        $intnat = $this->getActivePortals(clone $customers, 'both');

        $this->updateCachedTabs($intnat, "int_nat");

        $this->writeToFile("customers_int_nat", $intnat);
    }

    private function setINTCustomers($customers)
    {
        $int = $this->getActivePortals(clone $customers, 'int');

        $this->updateCachedTabs($int, "int");

        $this->writeToFile("customers_int", $int);
    }

    private function setNATCustomers($customers)
    {
        $nat = $this->getActivePortals(clone $customers, 'nat');

        $this->updateCachedTabs($nat, "nat");

        $this->writeToFile("customers_nat", $nat);
    }

    private function setSubscriptionCustomers($customers)
    {
        $subscriptions = KTCustomerSubscription::leftJoin("customers", "cu_id", "ktcusu_cu_id")
            ->where("ktcusu_start_date", "<=", date("Y-m-d"))
            ->where("ktcusu_end_date", ">=", date("Y-m-d"))
            ->get();

        $leads_store = $customers->whereIn("cu_id", $subscriptions->where("ktcusu_sub_id", SubscriptionType::LEADS_STORE)->pluck('ktcusu_cu_id')->toArray());
        $conversion_tools = $customers->whereIn("cu_id", $subscriptions->where("ktcusu_sub_id", SubscriptionType::CONVERSION_TOOLS)->pluck('ktcusu_cu_id')->toArray());

        $this->updateCachedTabs($leads_store, "leads_store");
        $this->updateCachedTabs($conversion_tools, "conversion_tools");


        $this->writeToFile("customers_leads_store", $leads_store);
        $this->writeToFile("customers_conversion_tools", $conversion_tools);
    }

    private function setPauseCustomers($customers)
    {
        $pause = $this->getPausedPortals(clone $customers);

        $this->updateCachedTabs($pause, "pause");

        $this->writeToFile("customers_paused", $pause, "pause");
    }

    private function setCancelledCustomers($customers)
    {
        $status_query = CustomerStatus::selectRaw("DISTINCT `cust_cu_id`")
            ->where("cust_status", "=", 8)
            ->whereIn("cust_cu_id", array_values($customers->pluck('cu_id')->toArray()))
            ->whereRaw("cust_date BETWEEN NOW() - INTERVAL 1 YEAR AND NOW()")
            ->get();

        $ids = $status_query->pluck('cust_cu_id')->toArray();

        if($ids)
        {
            $customers->whereIn("cu_id", array_keys($ids));

            foreach($customers as $customer){
                $query_status = DB::table("customer_statuses")
                    ->select("cust_date")
                    ->where("cust_cu_id", "=", $customer->cu_id)
                    ->where("cust_status", "=", 8)
                    ->whereRaw("`cust_date` BETWEEN NOW() - INTERVAL 1 YEAR AND NOW()")
                    ->orderBy("cust_date", "desc")
                    ->limit(1)
                    ->first();

                $row_status = $query_status;

                $customer->cancelled_on = $row_status->cust_date;
            }

        }

        $this->updateCachedTabs($customers->whereIn("cu_id", array_values($ids)), "cancelled");

        $this->writeToFile("customers_cancelled", $customers->whereIn("cu_id", array_values($ids)), "cancelled");
    }

    private function setCreditHoldCustomers($customers)
    {
        $ch_ids = [];
        $ppch_ids = [];

        foreach($customers as $customer){

            if($customer->cu_credit_hold == 1 && $customer->cu_debt_collector != 1){
                //When customer is on credithold and its not a prepayment customer
                if($customer->cu_payment_method != 4 && $customer->cu_payment_method != 5 && $customer->cu_payment_method != 6)
                {
                    //Set as cc customer
                    $ch_ids[] = $customer->cu_id;
                }
                //When customer is on credithold and it is a prepayment customer
                if(($customer->cu_payment_method == 4 || $customer->cu_payment_method == 5 || $customer->cu_payment_method == 6))
                {
                    $ppch_ids[] = $customer->cu_id;
                }
            }

            $timestamp = $customer->cu_credit_hold_timestamp;
            $now_timestamp = date("Y-m-d H:i:s");

            $date1 = new DateTime(date('Y-m-d', strtotime($timestamp)));
            $date2 = new DateTime(date('Y-m-d', strtotime($now_timestamp)));

            $days_difference = $date1->diff($date2)->days;

            $customer->days_credit_hold = ($timestamp == "0000-00-00 00:00:00" ? "Unknown" : $days_difference);

            $user = User::select("us_name")->where("id", $customer->cu_debt_manager)->first();
            $customer->debt_manager = $user ? $user->us_name : "-";

        }

        $this->updateCachedTabs($customers->whereIn("cu_id", $ch_ids), "credit_hold");
        $this->updateCachedTabs($customers->whereIn("cu_id", $ppch_ids), "prepayment_credit_hold");

        $this->writeToFile("customers_credit_hold", $customers->whereIn("cu_id", $ch_ids), "credit_hold");
        $this->writeToFile("customers_prepayment_credit_hold", $customers->whereIn("cu_id", $ppch_ids), "credit_hold");
    }

    private function setDebtCollectorCustomers($customers)
    {
        $ids = [];

        foreach($customers as $customer){
            if($customer->cu_debt_collector == 1)
            {
                $ids[] = $customer->cu_id;
            }
        }
        $this->updateCachedTabs($customers->whereIn("cu_id", $ids), "debt_collector");

        $this->writeToFile("customers_debt_collector", $customers->whereIn("cu_id", $ids));
    }

    private function updateCachedTabs($customers, $type) {
        $cu_ids = [];

        //Loop through customers and put cu_id in array
        foreach ($customers as $value) {
            $cu_ids[] = $value->cu_id;
        }

        //Set tab active for customers which are in the Customer ID array
        DB::table('customer_cached_tabs')
            ->whereIn('cucata_cu_id', $cu_ids)
            ->update(['cucata_'.$type => 1]);

        //Set tab inactive for customers which are not in the Customer ID array
        DB::table('customer_cached_tabs')
            ->whereNotIn('cucata_cu_id', $cu_ids)
            ->update(['cucata_'.$type => 0]);
    }

    private function writeToFile($filename, $customers, $tabletype = null)
    {
        $file = resource_path()."/views/cache/{$filename}.php";

        $handle = fopen($file, 'w');

        if ( $handle !== false ) {
            fwrite($handle, self::makeTable($customers, $tabletype));
            fclose($handle);
        }
        else{
            System::sendMessage(3653, "Couldn't open file!", $file);
        }

    }

    private function removeCreditHoldCustomers($customers)
    {
        //Only return customers that aren't on debt collection
        return $customers->where("cu_debt_collector", "!=", 1)->where("cu_credit_hold", "!=", 1);
    }

    private function getActivePortals($customers, $filter)
    {
        foreach($customers as $index => $customer){
            $ktcupo_query = KTCustomerPortal::leftJoin("portals", "ktcupo_po_id", "po_id")
                ->select("ktcupo_status", "po_destination_type")
                ->where("ktcupo_cu_id", $customer->cu_id)
                ->where("ktcupo_type", PortalType::CORE)
                ->get();

            $int = $this->getPortalInfo($ktcupo_query, CustomerMarketType::INT);
            $nat = $this->getPortalInfo($ktcupo_query, CustomerMarketType::NAT);

            if($filter == 'both'){
                if(!$nat || !$int)
                {
                    $customers->forget($index);
                }
            }elseif ($filter == 'int'){
                if(!$int)
                {
                    $customers->forget($index);
                }
            }elseif ($filter == 'nat'){
                if(!$nat)
                {
                    $customers->forget($index);
                }
            }


        }
        return $customers;
    }

    private function getPausedPortals($customers)
    {
        foreach($customers as $index => $customer){
            $ktcupo_query = KTCustomerPortal::select("ktcupo_status")
                ->where("ktcupo_cu_id", $customer->cu_id)
                ->whereIn("ktcupo_status", [1,2])
                ->get();

            if(count($ktcupo_query) == 0 || (count($ktcupo_query) != count($ktcupo_query->where("ktcupo_status", 2)))){
                $customers->forget($index);
            }

            $query_status = DB::table("status_updates")
                ->select("stup_date")
                ->leftJoin("kt_customer_portal", "ktcupo_id", "stup_ktcupo_id")
                ->where("ktcupo_cu_id", "=", $customer->cu_id)
                ->where("stup_status", "=", 1)
                ->whereRaw("`stup_date` >= NOW()")
                ->where("stup_processed", "=", 0)
                ->orderBy("stup_date", "asc")
                ->limit(1)
                ->first();

            if($query_status)
            {
                $row_status = $query_status;

                $customer->paused_till = $row_status->stup_date;
            }
            else
            {
                $customer->paused_till  = "-";
            }

            $unpause_date = DB::table("pause_history")
                ->select("ph_end_date")
                ->where("ph_cu_id", "=", $customer->cu_id)
                ->whereRaw("`ph_end_date` >= NOW()")
                ->orderBy("ph_end_date", "asc")
                ->limit(1)
                ->first();

            if($unpause_date)
            {
                $customer->paused_till = $unpause_date->ph_end_date;
            }

            $cu = Customer::select("us_name")->join("users", "cu_account_manager", "us_id")->where("cu_id", "=", $customer->cu_id)->first();

            $customer->account_manager = $cu ? $cu->us_name : "-";
        }
        return $customers;
    }

    private function getPortalInfo($ktcupo_query, $type){
        return count($ktcupo_query->where("ktcupo_status", 1)->where("po_destination_type", $type)) > 0;
    }

    private function makeTable($customers, $table_type = null)
    {
        $countries = Country::All();

        $country_list = [];

        foreach($countries as $country)
        {
            $country_list[$country->co_code] = $country->co_en;
        }

        $headers = ["ID", "Company legal name", "Company business name", "Country", "City", "Email", "Telephone"];

        if($table_type == "pause"){
            $headers[] = "Paused Till";
            $headers[] = "Account Manager";
        }elseif($table_type == "cancelled")
        {
            $headers[] = "Cancelled on";
        }elseif($table_type == "credit_hold")
        {
            $headers[] = "Days credit hold";
            $headers[] = "Debt manager";
        }

        $headers[] = "Actions";

        $order_by = '[[1, "asc"]]';

        $table_html = "<input type='hidden' name='table_count' value='".count($customers)."'>";
        $table_html .= "<table data-order='{$order_by}' class='table table-bordered table-striped table-vcenter js-dataTable-full dataTable no-footer'>";
        $table_html .= "<thead>";
        $table_html .= "<tr>";

        foreach($headers as $column)
        {
            $table_html .= "<th>".$column."</th>";
        }

        $table_html .= "</tr>";
        $table_html .= "</thead>";

        $table_html .= "<tbody>";

        foreach($customers as $customer)
        {
            $table_html .= "<tr>";

            $table_html .= "<td>".$customer->cu_id."</td>";
            $table_html .= "<td>".$customer->cu_company_name_legal."</td>";
            $table_html .= "<td>".$customer->cu_company_name_business."</td>";
            $table_html .= "<td>".((!empty($customer->cu_co_code)) ? $country_list[$customer->cu_co_code] : "")."</td>";
            $table_html .= "<td>".$customer->cu_city."</td>";
            $table_html .= "<td>".str_replace(",", "<br />", $customer->cu_email)."</td>";
            $table_html .= "<td>".$customer->cu_telephone."</td>";

            if($table_type == "pause"){
                $table_html .= "<td>".$customer->paused_till."</td>";
                $table_html .= "<td>".$customer->account_manager."</td>";
            }elseif($table_type == "cancelled"){
                $table_html .= "<td>".$customer->cancelled_on."</td>";
            }elseif($table_type == "credit_hold")
            {
                $table_html .= "<td>".$customer->days_credit_hold."</td>";
                $table_html .= "<td>".$customer->debt_manager."</td>";
            }

            $table_html .= "<td><div class='btn-group'><a href='/customers/{$customer->cu_id}/edit'><button type='button' class='btn btn-sm btn-primary js-tooltip-enabled' data-toggle='tooltip' data-id='".$customer->cu_id."' data-original-title='Edit'><i class='fa fa-pencil-alt'></i></button></a></div> <div class='btn-group'> <button type='button' class='btn btn-sm btn-primary customer_delete js-tooltip-enabled' data-toggle='tooltip' data-id='".$customer->cu_id."' data-original-title='Delete'><i class='fa fa-times'></i></button></div></td>";


            $table_html .= "</tr>";
        }

        $table_html .= "</tbody>";
        $table_html .= "</table>";

        return $table_html;
    }

    private function cacheCustomers(): void
    {
        $customers = DB::table("customers")
            ->where("cu_deleted", 0)
            ->where("cu_type", 1)
            ->orderBy("cu_company_name_business")
            ->get();

        $this->setSubscriptionCustomers($customers);
        $this->setPauseCustomers($customers);
        $this->setCancelledCustomers($customers);
        $this->setCreditHoldCustomers($customers);
        $this->setDebtCollectorCustomers($customers);


        //we don't want customers on credithold at the active customers
        $customers = $this->removeCreditHoldCustomers($customers);

        $this->setINTNATCustomers($customers);
        $this->setINTCustomers($customers);
        $this->setNATCustomers($customers);
    }

}
