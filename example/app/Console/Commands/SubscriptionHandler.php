<?php

namespace App\Console\Commands;


use App\Cronjobs\CronjobController;
use App\Models\KTCustomerSubscription;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SubscriptionHandler extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronjob:subscription_handler';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    private $cc;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CronjobController $cc)
    {
        $this->cc = $cc;
        parent::__construct();
    }


    const CRONJOB_ID = 35;
    const CRONJOB_TASK = 'Finance';
    const CRONJOB_SUBTASK = 'Update Subscription Rows';
    const CRONJOB_TRIGGER = 1;
    const CRONJOB_INTERVAL = "1 DAY";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        //Set Cronjob check values
        $CRONJOB_USERS = [3653];

        try
        {
            $this->handleSubscriptions();
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 0, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, null);

        }catch (\Throwable $e){
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 1, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, $e);
        }

    }

    private function handleSubscriptions(): void
    {
        $to_be_activated_subscriptions = KTCustomerSubscription::where("ktcusu_active", 0)->whereRaw('`ktcusu_start_date` LIKE "' . date('Y-m-d') . '%"')->get();

        foreach ($to_be_activated_subscriptions as $activate)
        {
            echo $activate->ktcusu_start_date . "\n";

            $activate->ktcusu_active = 1;
            $activate->save();
        }

        //$to_be_disabled_subscriptions = KTCustomerSubscription::where("ktcusu_active", 1)->whereRaw('`ktcusu_end_date` LIKE "'.date('Y-m-d').'%"')->get();
        $to_be_disabled_subscriptions = KTCustomerSubscription::where("ktcusu_active", 1)->whereRaw('DATE(ktcusu_end_date) = DATE(NOW() - INTERVAL 1 DAY)')->get();

        foreach ($to_be_disabled_subscriptions as $disable)
        {
            echo $disable->ktcusu_end_date . "\n";

            $disable->ktcusu_active = 2;
            $disable->save();

            if (!empty($disable->ktcusu_cancellation_date))
            {
                $current = strtotime(date("Y-m-d"));
                $date_cancellation = strtotime($disable->ktcusu_cancellation_date);

                $datediff = $date_cancellation - $current;
                $difference = floor($datediff / (60 * 60 * 24));

                if ($difference == -1)
                {
                    //End of subscription, dont make new one.
                    continue;
                }
            }

            $end_date_yearly = date("Y-m-d", strtotime(date("Y-m-d", strtotime(date("Y-m-d"))) . "+1 year"));
            $end_date_monthly = date("Y-m-t", strtotime(date("Y-m-d")));

            $customer_subscription = new KTCustomerSubscription();
            $customer_subscription->ktcusu_cu_id = $disable->ktcusu_cu_id;
            $customer_subscription->ktcusu_sub_id = $disable->ktcusu_sub_id;
            $customer_subscription->ktcusu_start_date = date("Y-m-d");
            $customer_subscription->ktcusu_end_date = (($disable->ktcusu_payment_interval == 1) ? $end_date_monthly : $end_date_yearly);
            $customer_subscription->ktcusu_cancellation_date = $disable->ktcusu_cancellation_date;
            $customer_subscription->ktcusu_price = $disable->ktcusu_price;
            $customer_subscription->ktcusu_price_this_month = $disable->ktcusu_price;
            $customer_subscription->ktcusu_pacu_code = $disable->ktcusu_pacu_code;
            $customer_subscription->ktcusu_payment_interval = $disable->ktcusu_payment_interval;
            $customer_subscription->ktcusu_active = 1;
            $customer_subscription->save();

        }
    }
}
