<?php

namespace App\Console\Commands;

use App\Cronjobs\CronjobController;
use App\Functions\Mail;
use App\Functions\Mover;
use App\Functions\System;
use App\Models\Customer;
use App\Models\CustomerLoadExchange;
use App\Models\CustomerLoadReaction;
use App\Models\CustomerLoadSetting;
use Carbon\Carbon;
use Illuminate\Console\Command;

class LoadExchangeDailyUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronjob:load_exchange_daily_update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    private $cc;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CronjobController $cc)
    {
        $this->cc = $cc;
        parent::__construct();
    }


    const CRONJOB_ID = 14;
    const CRONJOB_TASK = 'Mover Portal';
    const CRONJOB_SUBTASK = 'Send Load Exchange Daily Update Emails';
    const CRONJOB_TRIGGER = 5;
    const CRONJOB_INTERVAL = "1 DAY";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        //Set Cronjob check values
        $CRONJOB_USERS = [3653];

        try
        {
            $this->sendUpdates();
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 0, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, null);

        }catch (\Throwable $e){
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 1, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, $e);
        }

    }

    public function dailyUpdate($customer)
    {
        $system = new System();
        $mover = new Mover();
        $mail = new Mail();

        //Company country filters
        $company_countries_query = CustomerLoadSetting::select("culose_co_code", "culose_type")
            ->where("culose_cu_id", $customer['cu_id'])
            ->get();

        $company_countries = $system->databaseToArray($company_countries_query);

        $countries_from = [];
        $countries_to = [];

        if(count($company_countries) > 0)
        {
            foreach($company_countries as $country)
            {
                if($country['culose_type'] == 1)
                {
                    $countries_from[] = $country['culose_co_code'];
                }
                else
                {
                    $countries_to[] = $country['culose_co_code'];
                }
            }
        }

        $loads_query = CustomerLoadExchange::where("culoex_cu_id", "!=", $customer['cu_id'])
            ->where("culoex_deleted", 0);

        if(!empty($countries_from))
        {
            $loads_query = $loads_query->whereIn("culoex_co_code_from", $countries_from);
        }
        if(!empty($countries_to))
        {
            $loads_query = $loads_query->whereIn("culoex_co_code_to", $countries_to);
        }

        $loads_query = $loads_query
            ->orderBy("culoex_timestamp", "DESC")
            ->get();

        $loads = $system->databaseToArray($loads_query);

        if(count($loads) > 0)
        {
            foreach($loads as $index => $load)
            {
                $quotes_query = CustomerLoadReaction::selectRaw("COUNT(*) as amount")
                    ->where("culore_culoex_id", $load['culoex_id'])
                    ->where("culore_deleted", 0)
                    ->first();

                $quotes = $system->databaseToArray($quotes_query);

                //Set amount of quotes, otherwise zero
                if(count($quotes) > 0)
                {
                    $loads[$index]['quotes'] = $quotes['amount'];
                }
                else
                {
                    $loads[$index]['quotes'] = 0;
                }
            }

            $fields = [
                "to_email" => $mover->getField("moda_load_exchange_email", $customer['cu_id']),
                "name" => $customer['cu_company_name_business'],
                "loads" => $loads
            ];

            if($loads)
            {
                $mail->send("load_exchange_daily_load_update", $customer['cu_la_code'], $fields, null, false, "#7950a0");
            }
        }
    }

    private function sendUpdates(): void
    {
        $system = new System();

        //If date is weekend, don't send daily update
        $expiry_day = date("w");
        if ($expiry_day == 6 || $expiry_day == 0)
        {
            return;
        }

        $query_customer = Customer::leftJoin("mover_data", "moda_cu_id", "cu_id")
            ->where("moda_activate_load_exchange", 1)
            ->where("moda_load_exchange_daily", 1)
            ->where("cu_deleted", 0)
            ->get();

        $customers = $system->databaseToArray($query_customer);

        foreach ($customers as $customer)
        {
            self::dailyUpdate($customer);
        }
    }
}

