<?php
/**
 * Created by PhpStorm.
 * User: Arjan de Coninck
 * Date: 7-5-2020
 * Time: 12:51
 */

namespace App\Console\Commands;

use App\Cronjobs\CronjobController;
use App\Functions\Mail;
use App\Models\EmailBuilderTemplate;
use App\Models\MovingAssistant;
use App\Models\MovingAssistantEmail;
use App\Models\Website;
use Carbon\Carbon;
use Illuminate\Console\Command;

class MovingAssistantEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'moving_assistant:emails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cronjob for sending the emails of the Moving Assistant.';

    private $cc;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CronjobController $cc)
    {
        $this->cc = $cc;
        parent::__construct();
    }


    const CRONJOB_ID = 17;
    const CRONJOB_TASK = 'Sirelo';
    const CRONJOB_SUBTASK = 'Send Move Assistant Emails';
    const CRONJOB_TRIGGER = 3;
    const CRONJOB_INTERVAL = "1 DAY";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Set Cronjob check values
        $CRONJOB_USERS = [3653];

        try
        {
            $this->sendEmails();
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 0, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, null);

        }catch (\Throwable $e){
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 1, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, $e);
        }

    }

    private function sendEmails(): void
    {
        $date_now = date("Y-m-d");

        $mail = new Mail();

        $moving_assistant_emails = MovingAssistantEmail::all();

        foreach ($moving_assistant_emails as $email)
        {
            $email_name = EmailBuilderTemplate::where("embute_id", $email->moasem_embute_id)->first()->embute_name;
            $date_of_moving = date('Y-m-d', strtotime((($email->moasem_days_before_mail >= 0) ? '+' : '-') . str_replace("-", "", $email->moasem_days_before_mail) . ' days', strtotime($date_now)));
            $people_to_mail = MovingAssistant::where("moas_moving_date", $date_of_moving)->where("moas_unsubscribed", 0)->get();

            foreach ($people_to_mail as $person)
            {
                $language = Website::where("we_id", $person->moas_we_id)->first()->we_sirelo_la_code;

                $email_to_send = EmailBuilderTemplate::where("embute_name", $email_name)
                    ->where("embute_language", $language)
                    ->first();

                $mailname = str_replace(".php", "", $email_to_send->embute_filename);


                $unsubscribe_link = Website::where("we_id", $person->moas_we_id)->first()->we_moving_assistant_unsubscribe_location . "?token=" . $person->moas_token;

                $from_name = Website::select("we_mail_from_name")->where("we_id", $person->moas_we_id)->first()->we_mail_from_name;

                if (empty($from_name))
                {
                    $from_name = "Sirelo.org";
                }

                $fields = [
                    'customername' => $person->moas_first_name,
                    'moving_assistant' => true,
                    'unsubscribe_link' => $unsubscribe_link,
                    "from_name" => $from_name,
                    "to" => $person->moas_email
                ];

                echo "Sending email " . $person->moas_email . "\n";

                $mail->sendCustomMail($mailname, $language, $fields);
            }
        }
    }
}
