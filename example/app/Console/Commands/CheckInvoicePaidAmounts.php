<?php

namespace App\Console\Commands;

use App\Cronjobs\CronjobController;
use App\Functions\System;
use Carbon\Carbon;
use DB;
use Illuminate\Console\Command;

class CheckInvoicePaidAmounts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'checker:invoice_amount_paid';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cronjob that checks whether the paid amount on invoices is still the same as registered in the field in the invoice-tabel.';

    private $cc;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CronjobController $cc)
    {
        $this->cc = $cc;
        parent::__construct();
    }

    //Set Cronjob check values
    const CRONJOB_ID = 6;
    const CRONJOB_TASK = 'Finance';
    const CRONJOB_SUBTASK = 'Check Invoice Paid Amounts';
    const CRONJOB_TRIGGER = 3;
    const CRONJOB_INTERVAL = "1 DAY";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        //Set Cronjob check values
        $CRONJOB_USERS = [3653];

        try
        {
            $this->checkAmounts();
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 0, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, null);

        }catch (\Throwable $e){
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 1, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, $e);
        }

    }

    private function checkAmounts(): void
    {
        $system = new System();

        $bank_lines = DB::table("kt_bank_line_invoice_customer_ledger_account")
            ->select("ktbaliinculeac_in_id", "ktbaliinculeac_amount", "in_amount_netto_eur")
            ->leftJoin("invoices", "in_id", "ktbaliinculeac_in_id")
            ->whereNotNull("ktbaliinculeac_in_id")
            ->get();

        $found_invoices = [];


        foreach ($bank_lines as $bank_line)
        {
            if (array_key_exists($bank_line->ktbaliinculeac_in_id, $found_invoices))
            {
                $found_invoices[$bank_line->ktbaliinculeac_in_id]["paid"] += $bank_line->ktbaliinculeac_amount;
            } else
            {
                $found_invoices[$bank_line->ktbaliinculeac_in_id]["amount"] = $bank_line->in_amount_netto_eur;
                $found_invoices[$bank_line->ktbaliinculeac_in_id]["paid"] = $bank_line->ktbaliinculeac_amount;
            }
        }

        foreach ($found_invoices as $index => $invoice)
        {
            $current_invoice = DB::table('invoices')
                ->select("in_amount_netto_eur_paid", "in_cu_id")
                ->where('in_id', $index)
                ->first();

            if (number_format($current_invoice->in_amount_netto_eur_paid, 2) != number_format($invoice["paid"], 2))
            {
                $system->sendMessage(3653, "Invoice Paid Amount not the same :(", "For customer {$current_invoice->in_cu_id} the amount in the DB is " . floatval($current_invoice->in_amount_netto_eur_paid) . " while the calculation says " . floatval($invoice["paid"]));
            }


        }
    }
}
