<?php

namespace App\Console\Commands;

use App\Cronjobs\CronjobController;
use App\Functions\Data;
use App\Functions\Mail;
use App\Functions\System;
use App\Models\KTServiceProviderNewsletterBlockSent;
use App\Models\Newsletter;
use App\Models\NewsletterHistory;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Newsletters extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronjob:newsletters';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    private $cc;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CronjobController $cc)
    {
        $this->cc = $cc;
        parent::__construct();
    }


    const CRONJOB_ID = 18;
    const CRONJOB_TASK = 'Email Marketing';
    const CRONJOB_SUBTASK = 'Send Newsletter Emails';
    const CRONJOB_TRIGGER = 5;
    const CRONJOB_INTERVAL = "60 MINUTE";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Set Cronjob check values
        $CRONJOB_USERS = [3653];

        try
        {
            $this->sendNewsletters();
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 0, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, null);

        }catch (\Throwable $e){
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 1, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, $e);
        }


    }

    /**
     * @return int[]
     */
    private function sendNewsletters(): void
    {
        $system = new System();
        $mail = new Mail();
        $data = new Data();

        $query = Newsletter::all();

        $query = $system->databaseToArray($query);

        foreach ($query as $row)
        {


            //@TODO JOIN TABLE RAW!!!!!


            $query_request = DB::select(DB::raw("SELECT `re_id`, `re_la_code`, `re_co_code_from`, `re_co_code_to`, `re_full_name`, `re_email` FROM `requests` LEFT JOIN `newsletter_history` ON `nehi_new_id` = '" . $row['new_id'] . "' AND `newsletter_history`.`nehi_re_id` = `requests`.`re_id` LEFT JOIN `kt_request_customer_portal` ON `ktrecupo_re_id` = `re_id` LEFT JOIN `customers` ON `ktrecupo_cu_id` = `cu_id` WHERE `newsletter_history`.`nehi_id` IS NULL AND HOUR(`requests`.`re_timestamp`) <= HOUR(NOW()) AND `re_status` = '1' AND `cu_type` != '6' AND `re_timestamp` BETWEEN '" . date("Y-m-d H:i:s", strtotime(date("Y-m-d", strtotime("-" . $row['new_delay'] . " days")) . " 00:00:00")) . "' AND '" . date("Y-m-d H:i:s", strtotime(date("Y-m-d", strtotime("-" . $row['new_delay'] . " days")) . " 23:59:59")) . "' GROUP BY `re_id`"));

            $query_request = $system->databaseToArray($query_request);

            foreach ($query_request as $row_request)
            {
                $slots_taken = 0;
                $content = "";
                $content .= "</td></tr></table>";

                $start = [1, 2];
                $end = [3, 4, 5, 6, 7, 8];
                shuffle($end);

                $result = array_merge($start, $end);

                foreach ($result as $slot)
                {
                    if (!empty($slot) && $slots_taken < 2)
                    {
                        $row_sp_newsletter_blocks = DB::table('service_provider_newsletter_blocks')
                            ->select("seprnebl_id", "seprnebl_cu_id", "seprnebl_title", "seprnebl_content")
                            ->leftJoin("kt_service_provider_newsletter_block_language", "seprnebl_id", "ktseprneblla_seprnebl_id")
                            ->leftJoin("kt_service_provider_newsletter_block_country_origin", "seprnebl_id", "ktseprneblcoor_seprnebl_id")
                            ->leftJoin("kt_service_provider_newsletter_block_country_destination", "seprnebl_id", "ktseprneblcode_seprnebl_id")
                            ->where("seprnebl_status", 1)
                            ->where("seprnebl_slot", $slot)
                            ->where("seprnebl_" . (($row_request['re_co_code_from'] != $row_request['re_co_code_to']) ? "international" : "national") . "_moves", 1)
                            ->where("ktseprneblla_la_code", $row_request['re_la_code'])
                            ->where("ktseprneblcoor_co_code", $row_request['re_co_code_from'])
                            ->where("ktseprneblcode_co_code", $row_request['re_co_code_to'])
                            ->where("seprnebl_cu_id", "!=", 25713)
                            ->orderByRaw("RAND()")
                            ->first();

                        if (count($row_sp_newsletter_blocks) > 0)
                        {
                            $slots_taken++;

                            /*if (!empty($row_sp_newsletter_blocks->seprnebl_title))
                            {
                                $content .= $mail->table();
                                $content .= "<tr><td style=\"" . $mail->styleBar("#2B3C4D") . "\">" . $row_sp_newsletter_blocks->seprnebl_title . "</td></tr>";
                                $content .= "</table>";
                            }*/

                            $content .= "<br />";
                            $content .= Mail::table2( ['max-width' => '640px' /*, 'font-family' => 'Helvetica Neue'*/] );

                            $content .= Mail::TR();
                            $content .= Mail::TD();
                            $content .= Mail::table2( ['max-width' => '640px']);
                            /* if ($slots_taken > 1) {
                                 $content .= "<tr>";
                                 $content .= "<td>&nbsp;</td>";
                                 $content .= "</tr>";
                             }*/


                            if(!empty($row_sp_newsletter_blocks->seprnebl_title)) {
                                $content .= "<tr style='background-color:#fff;'>";
                                $content .= "<td style='padding: 30px 30px 0 30px; font-size:20px; color: #000083; font-weight: 700;'>".$row_sp_newsletter_blocks->seprnebl_title."</td>";
                                $content .= "</tr>";
                            }

                            $content .= "<tr style='background-color:#fff;'><td style='padding: 0 30px 30px 30px; color: #535b65; line-height: 20px;'>";
                            $content .= $row_sp_newsletter_blocks->seprnebl_content;
                            $content .= "</td></tr>";

                            $content .= "</table>";
                            $content .= "</table>";


                            $ktseprneblse = new KTServiceProviderNewsletterBlockSent();
                            $ktseprneblse->ktseprneblse_timestamp = date("Y-m-d H:i:s");
                            $ktseprneblse->ktseprneblse_cu_id = $row_sp_newsletter_blocks->seprnebl_cu_id;
                            $ktseprneblse->ktseprneblse_seprnebl_id = $row_sp_newsletter_blocks->seprnebl_id;
                            $ktseprneblse->ktseprneblse_slot = $slot;
                            $ktseprneblse->save();
                        }
                    }
                }

                /**
                 * START ARTICLES BLOCK
                 */

                $row_article = DB::table('service_provider_newsletter_blocks')
                    ->select("seprnebl_id", "seprnebl_cu_id", "seprnebl_title", "seprnebl_content")
                    ->leftJoin("kt_service_provider_newsletter_block_language", "seprnebl_id", "ktseprneblla_seprnebl_id")
                    ->leftJoin("kt_service_provider_newsletter_block_country_origin", "seprnebl_id", "ktseprneblcoor_seprnebl_id")
                    ->leftJoin("kt_service_provider_newsletter_block_country_destination", "seprnebl_id", "ktseprneblcode_seprnebl_id")
                    ->where("seprnebl_status", 1)
                    ->where("seprnebl_slot", 10)
                    ->where("seprnebl_" . (($row_request['re_co_code_from'] != $row_request['re_co_code_to']) ? "international" : "national") . "_moves", 1)
                    ->where("ktseprneblla_la_code", $row_request['re_la_code'])
                    ->where("ktseprneblcoor_co_code", $row_request['re_co_code_from'])
                    ->where("ktseprneblcode_co_code", $row_request['re_co_code_to'])
                    ->where("seprnebl_cu_id", 25713)
                    ->orderByRaw("RAND()")
                    ->first();

                if (!empty($row_article)) {
                    $content .= "<br />";
                    $content .= Mail::table2( ['max-width' => '640px'] );

                    $content .= Mail::TR();
                    $content .= Mail::TD();
                    $content .= Mail::table2( ['max-width' => '640px']);

                    if(!empty($row_article->seprnebl_title)) {
                        $content .= "<tr style='background-color:#fff;'>";
                        $content .= "<td style='padding: 30px 30px 10px 30px; font-size:20px; color: #000083; font-weight: 700;'>".$row_article->seprnebl_title."</td>";
                        $content .= "</tr>";
                    }

                    $content .= "<tr style='background-color:#fff;'><td style='padding: 0 30px 30px 30px; color: #535b65; line-height: 20px;'>";
                    $content .= $row_article->seprnebl_content;
                    $content .= "</td></tr>";

                    $content .= "</table>";
                    $content .= "</table>";

                    if ($slots_taken > 0 && !empty($content))
                    {
                        $ktseprneblse = new KTServiceProviderNewsletterBlockSent();
                        $ktseprneblse->ktseprneblse_timestamp = date("Y-m-d H:i:s");
                        $ktseprneblse->ktseprneblse_cu_id = $row_article->seprnebl_cu_id;
                        $ktseprneblse->ktseprneblse_seprnebl_id = $row_article->seprnebl_id;
                        $ktseprneblse->ktseprneblse_slot = 10;
                        $ktseprneblse->save();
                    }
                }

                /**
                 * END ARTICLES BLOCK
                 */

                $content .= "</table>";
                $content .= "</table>";

                if ($slots_taken > 0 && !empty($content))
                {
                    if ($slots_taken == 1)
                    {
                        $subject = $system->unserialize($row['new_subject_singular'])[$row_request['re_la_code']];
                    } else
                    {
                        $subject = $system->unserialize($row['new_subject_plural'])[$row_request['re_la_code']];
                    }

                    $subject = str_replace("[slots_taken]", $slots_taken, $subject);

                    $intro = $system->unserialize($row['new_intro'])[$row_request['re_la_code']];

                    $shortcodes = [
                        "name" => $row_request['re_full_name'],
                        "country_from" => $data->country($row_request['re_co_code_from'], $row_request['re_la_code']),
                        "country_to" => $data->country($row_request['re_co_code_to'], $row_request['re_la_code'])
                    ];

                    foreach ($shortcodes as $shortcode => $replace)
                    {
                        $intro = str_replace("[" . $shortcode . "]", $replace, $intro);
                    }

                    $fields = [
                        "request" => $row_request,
                        "subject" => $subject,
                        "intro" => nl2br($intro),
                        "content" => $content
                    ];

                    $mail->send("newsletter", $row_request['re_la_code'], $fields);

                    $newsletter_history = new NewsletterHistory();
                    $newsletter_history->nehi_timestamp = date("Y-m-d H:i:s");
                    $newsletter_history->nehi_new_id = $row['new_id'];
                    $newsletter_history->nehi_re_id = $row_request['re_id'];
                    $newsletter_history->save();
                }
            }
        }

    }
}

