<?php

namespace App\Console\Commands;

use App\Cronjobs\CronjobController;
use App\Functions\Cache;
use App\Functions\System;
use App\Models\Request;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CacheRequestsMonthlyChart extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronjob:cache_requests_monthly_chart';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    private $cc;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CronjobController $cc)
    {
        $this->cc = $cc;
        parent::__construct();
    }

    //Set Cronjob check values
    const CRONJOB_ID = 5;
    const CRONJOB_TASK = 'Phoenix';
    const CRONJOB_SUBTASK = 'Cache Requests for Monthly Chart';
    const CRONJOB_TRIGGER = 5;
    const CRONJOB_INTERVAL = "1 DAY";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Set Cronjob check values
        $CRONJOB_USERS = [3653];

        try
        {
            $this->updateCache();
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 0, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, null);

        }catch (\Throwable $e){
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 1, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, $e);
        }

    }

    private function updateCache(): void
    {
        $results = [];

        //Create an index in the array for the last 24 months | $results['Feb 2017']
        for ($i = 24; $i >= 0; $i--)
        {
            $results[date("M Y", strtotime("first day of -" . $i . " Months"))] = [
                "current" => 0
            ];
        }

        $query_requests = Request::select("re_timestamp")
            ->whereRaw('`re_timestamp` BETWEEN "' . date("Y-m-d H:i:s", strtotime(date("Y-m-01", strtotime("-2 year +1 Month")) . " 00:00:00")) . '" AND "' . date("Y-m-d H:i:s", strtotime(date("Y-m-d", strtotime("today GMT+1")) . " 23:59:59")) . ' "')
            ->get();

        foreach ($query_requests as $row_requests)
        {
            if (array_key_exists(date("M Y", strtotime($row_requests->re_timestamp)), $results))
            {
                $results[date("M Y", strtotime($row_requests->re_timestamp))]['current']++;
            }
        }

        Cache::update("requests_monthly_chart", System::serialize($results));
    }
}

