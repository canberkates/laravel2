<?php

namespace App\Console\Commands;

use App\Cronjobs\CronjobController;
use App\Functions\DataIndex;
use App\Functions\System;
use App\Models\AdyenCardDetails;
use Carbon\Carbon;
use DateTime;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class AdyenCardExpiryCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronjob:adyen_card_expiry_check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    private $cc;
    /**
     * @var System
     */
    private $system;
    /**
     * @var DataIndex
     */
    private $dataIndex;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CronjobController $cc, System $system, DataIndex $dataIndex)
    {
        parent::__construct();
        $this->cc = $cc;
        $this->system = $system;
        $this->dataIndex = $dataIndex;
    }

    //Class constants for Cronjob Info
    const CRONJOB_ID = 1;
    const CRONJOB_TASK = 'Finance';
    const CRONJOB_SUBTASK = 'Credit Card Expiry Check';
    const CRONJOB_TRIGGER = 1;
    const CRONJOB_INTERVAL = "1 MONTH";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Get correct users for this Cronjob
        $CRONJOB_USERS = DataIndex::financeUsers();

        try
        {
            //Find expiring cards
            $this->findCards(
                new DateTime(),
                "credit card has expired",
                function ($row) {
                    DB::table('adyen_card_details')
                        ->where('adcade_id', $row['adcade_id'])
                        ->update(
                            [
                                'adcade_enabled' => 0
                            ]
                        );
                }
            );

            //Find almost expiring cards
            $this->findCards(
                new DateTime('midnight first day of next month'),
                "credit card expires next month"
            );

            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 0, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, null);

        }catch (\Throwable $e){
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 1, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, $e);
        }

    }

    /**
     * Find cards and message Finance about them
     * @param DateTime $date Card date
     * @param String $message The message for finance
     * @param null|callable $callback Possible callback
     */
    public function findCards($date, $message, $callback = null)
    {
        $query = AdyenCardDetails::select(
                "adcade_id",
                "adcade_card_expiry_month",
                "adcade_card_expiry_year",
                "adcade_card_last_digits",
                "cu_id",
                "cu_company_name_business"
            )
            ->leftJoin("customers", "cu_id", "adcade_cu_id")
            ->where("adcade_card_expiry_month", $date->format("m"))
            ->where("adcade_card_expiry_year", $date->format("Y"))
            ->where("adcade_removed", 0)
            ->where("adcade_enabled", 1)
            ->get();

        $query = $this->system->databaseToArray($query);

        foreach($query as $row)
        {
            //Send the message
            $name = $row['cu_company_name_business'];
            $href_query = http_build_query(["cu_id" => $row['cu_id'], "source" => ""]);
            $this->system->sendMessage(
                array_keys($this->dataIndex->financeUsers()),
                "[CC] {$name}'s {$message}",
                "{$name}'s {$message}. <br />".
                "Card ID: {$row['adcade_id']} | Last digits: {$row['adcade_card_last_digits']} | Expires: {$row['adcade_card_expiry_year']}/{$row['adcade_card_expiry_month']} <br />".
                "<a href='/finance/credit_card_edit/?{$href_query}' class='button'>Edit cards</a>"
            );

            //Possible callback
            if($callback !== null)
            {
                $callback($row);
            }
        }
    }
}

