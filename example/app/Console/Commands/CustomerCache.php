<?php

namespace App\Console\Commands;

use App\Cronjobs\CronjobController;
use App\Data\AffiliatePartnerStatus;
use App\Data\CustomerType;
use App\Data\MoverStatus;
use App\Data\ServiceProviderStatus;
use App\Data\SubscriptionType;
use App\Functions\DataIndex;
use App\Functions\System;
use App\Models\Country;
use App\Models\Customer;
use App\Models\CustomerStatus;
use App\Models\KTCustomerPortal;
use App\Models\KTCustomerQuestion;
use App\Models\KTCustomerSubscription;
use App\Models\RequestDelivery;
use App\Models\ServiceProviderNewsletterBlock;
use App\Models\User;
use Carbon\Carbon;
use DateTime;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CustomerCache extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cache:customer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'The cronjob of Customer Cache';

    private $cc;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CronjobController $cc)
    {
        $this->cc = $cc;
        parent::__construct();
    }

    const CRONJOB_ID = 7;
    const CRONJOB_TASK = 'Phoenix';
    const CRONJOB_SUBTASK = 'Customer old cache';
    const CRONJOB_TRIGGER = 20;
    const CRONJOB_INTERVAL = "30 MINUTE";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        //Set Cronjob check values
        $CRONJOB_USERS = [3653];

        try
        {
            $this->createCache();
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 0, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, null);

        }catch (\Throwable $e){
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 1, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, $e);
        }

    }

    private function hasCreditLimitation(&$customers, &$customer, $cu_type)
	{
		//Maps the customer type to their sub type
		$credit_hold_map = [1 => 6, 2 => 4, 6 => 6];
		$credit_hold_prepayment_map = [1 => 9, 2 => 4, 6 => 9];
		$debt_collector_map = [1 => 7, 2 => 5, 6 => 9];

		if($customer->cu_debt_collector == 1)
		{
			$customers[$cu_type][$debt_collector_map[$cu_type]][$customer->cu_id] = $customer;

			return true;
		}

		//When customer is on credithold and its not a prepayment customer
		if($customer->cu_credit_hold == 1 && $customer->cu_payment_method != 4 && $customer->cu_payment_method != 5 && $customer->cu_payment_method != 6)
		{
			$customers[$cu_type][$credit_hold_map[$cu_type]][$customer->cu_id] = $customer;

			return true;
		}

		//When customer is on credithold and is a prepayment customer
		if($customer->cu_credit_hold == 1 && ($customer->cu_payment_method == 4 || $customer->cu_payment_method == 5 || $customer->cu_payment_method == 6))
		{
			$customers[$cu_type][$credit_hold_prepayment_map[$cu_type]][$customer->cu_id] = $customer;

			return true;
		}

		return false;
	}

	private function empty_check($id, $table, $prefix, $field)
	{
		$query = DB::table($table)
			->select($prefix."_id")
			->where($prefix."_".$field."_id", $id)
			->get();

		if (count($query) == 0)
		{
			return true;
		}
		else {
			return false;
		}
	}

	private function makeTable($customers, $type, $sub_type)
	{
		$countries = Country::All();

		$country_list = [];

		foreach($countries as $country)
		{
			$country_list[$country->co_code] = $country->co_en;
		}

		$headers = ["ID", "Company legal name", "Company business name", "Country", "City", "Email", "Telephone"];

		if($type == 1 || $type == 6)
		{
			if($sub_type == 3)
			{
				$headers[] = "Paused till";
				$headers[] = "Account Manager";
			}
			elseif($sub_type == 4)
			{
				$headers[] = "Cancelled on";
			}
			elseif($sub_type == 6)
			{
				$headers[] = "Days credit hold";
                $headers[] = "Debt manager";
            }
			elseif($sub_type == 9)
			{
				$headers[] = "Days credit hold";
				$headers[] = "Debt manager";
			}
		}

		$headers[] = "Actions";

		//$headers[] = "Delete";

		$order_by = '[[1, "asc"]]';

		$table_html = "";
		$table_html .= "<table data-order='{$order_by}' class='table table-bordered table-striped table-vcenter js-dataTable-full dataTable no-footer'>";
		$table_html .= "<thead>";
		$table_html .= "<tr>";

		foreach($headers as $column)
		{
			$table_html .= "<th>".$column."</th>";
		}

		$table_html .= "</tr>";
		$table_html .= "</thead>";

		$table_html .= "<tbody>";

		foreach($customers as $row)
		{
			if($row->cu_type == 1 || $row->cu_type == 6)
			{
				if(isset($row->sub_type) && $row->sub_type == 3)
				{
					$fields[] = "Paused till";
					$fields[] = "Account Manager";

					$query_status = DB::table("status_updates")
						->select("stup_date")
						->leftJoin("kt_customer_portal", "ktcupo_id", "stup_ktcupo_id")
						->where("ktcupo_cu_id", "=", $row->cu_id)
						->where("stup_status", "=", 1)
						->whereRaw("`stup_date` >= NOW()")
						->where("stup_processed", "=", 0)
						->orderBy("stup_date", "asc")
						->limit(1)
						->first();

					if(count($query_status) > 0)
					{
						$row_status = $query_status;

						$paused_till = $row_status->stup_date;
					}
					else
					{
						$paused_till = "-";
					}

                    $unpause_date = DB::table("pause_history")
                        ->select("ph_end_date")
                        ->where("ph_cu_id", "=", $row->cu_id)
                        ->whereRaw("`ph_end_date` >= NOW()")
                        ->orderBy("ph_end_date", "asc")
                        ->limit(1)
                        ->first();

                    if(count($unpause_date) > 0)
                    {
                        $paused_till = $unpause_date->ph_end_date;
                    }

					$account_manager = "";

					$cu = Customer::select("cu_account_manager")->where("cu_id", "=", $row->cu_id)->first();

					$account_manager_id = $cu->cu_account_manager;

					if(!empty($account_manager_id))
					{
						$user = User::findOrFail($account_manager_id);

						$account_manager = $user->us_name;
					}
				}
				elseif(isset($row->sub_type) && $row->sub_type == 4)
				{
					$fields[] = "Cancelled on";

					$query_status = DB::table("customer_statuses")
						->select("cust_date")
						->where("cust_cu_id", "=", $row->cu_id)
						->where("cust_status", "=", 8)
						->whereRaw("`cust_date` BETWEEN NOW() - INTERVAL 1 YEAR AND NOW()")
						->orderBy("cust_date", "desc")
						->limit(1)
						->first();

					$row_status = $query_status;

					$cancelled_on = $row_status->cust_date;
				}
				elseif($row->cu_credit_hold == 1 && $row->cu_debt_collector == 0)
				{
					$fields[] = "Days credit hold";
					if(isset($row->sub_type) && ($row->sub_type == 6 || $row->sub_type == 9))
						$fields[] = "Debt manager";

					$get_customer = Customer::findOrFail($row->cu_id);

					if(count($get_customer) > 0)
					{
						$timestamp = $get_customer->cu_credit_hold_timestamp;
						$now_timestamp = date("Y-m-d H:i:s");

						$date1 = new DateTime(date('Y-m-d', strtotime($timestamp)));
						$date2 = new DateTime(date('Y-m-d', strtotime($now_timestamp)));

						$days_difference = $date1->diff($date2)->days;

						$timestamp = $days_difference;
					}
					else
					{
						$timestamp = "Not known";
					}

					$days_credit_hold = $timestamp;

					$debt_manager = "";

					$debt_manager_id = $get_customer->cu_debt_manager;

					if(!empty($debt_manager_id))
					{
						$user = User::findOrFail($debt_manager_id);

						$debt_manager = $user->us_name;
					}
				}
			}

			$table_html .= "<tr>";

			$table_html .= "<td>".$row->cu_id."</td>";
			$table_html .= "<td>".$row->cu_company_name_legal."</td>";
			$table_html .= "<td>".$row->cu_company_name_business."</td>";
			$table_html .= "<td>".((!empty($row->cu_co_code)) ? $country_list[$row->cu_co_code] : "")."</td>";
			$table_html .= "<td>".$row->cu_city."</td>";
			$table_html .= "<td>".str_replace(",", "<br />", $row->cu_email)."</td>";
			$table_html .= "<td>".$row->cu_telephone."</td>";

			if($row->cu_type == 1 || $row->cu_type == 6)
			{
				if(isset($row->sub_type) && $row->sub_type == 3)
				{
					$table_html .= "<td>".$paused_till."</td>";
					$table_html .= "<td>".$account_manager."</td>";
				}
				elseif(isset($row->sub_type) && $row->sub_type == 4)
				{
					$table_html .= "<td>".$cancelled_on."</td>";
				}
				elseif($row->cu_credit_hold == 1 && $row->cu_debt_collector == 0 && $row->cu_payment_method != 4 && $row->cu_payment_method != 5 && $row->cu_payment_method != 6)
				{
					$table_html .= "<td>".$days_credit_hold."</td>";
                    $table_html .= "<td>".$debt_manager."</td>";
                }
				elseif($row->cu_credit_hold == 1 && $row->cu_debt_collector == 0 && ($row->cu_payment_method == 4 || $row->cu_payment_method == 5 || $row->cu_payment_method == 6 ))
				{
					$table_html .= "<td>".$days_credit_hold."</td>";
					$table_html .= "<td>".$debt_manager."</td>";
				}

			}

			$start_url = "";

			if ($row->cu_type == 1)
			{
				$start_url = "customers";
			}
			elseif($row->cu_type == 6)
            {
                $start_url = "resellers";
            }
			elseif($row->cu_type == 2)
			{
				$start_url = "serviceproviders";
			}
			else
			{
				$start_url = "affiliatepartners";
			}

			//$table_html .= "<td><a href='/{$start_url}/{$row->cu_id}/edit'><button type='button' class='btn btn-sm btn-primary js-tooltip-enabled' data-toggle='tooltip' data-id='".$row->cu_id."' data-original-title='Edit'><i class='fa fa-pencil-alt'></i></button></a></td>";

			//$table_html .= "<td><button type='button' class='btn btn-sm btn-primary customer_delete js-tooltip-enabled' data-toggle='tooltip' data-id='".$row->cu_id."' data-original-title='Delete'><i class='fa fa-times'></i></button></td>";

			$table_html .= "<td><div class='btn-group'><a href='/{$start_url}/{$row->cu_id}/edit'><button type='button' class='btn btn-sm btn-primary js-tooltip-enabled' data-toggle='tooltip' data-id='".$row->cu_id."' data-original-title='Edit'><i class='fa fa-pencil-alt'></i></button></a></div> <div class='btn-group'> <button type='button' class='btn btn-sm btn-primary customer_delete js-tooltip-enabled' data-toggle='tooltip' data-id='".$row->cu_id."' data-original-title='Delete'><i class='fa fa-times'></i></button></div></td>";


			$table_html .= "</tr>";
		}

		$table_html .= "</tbody>";
		$table_html .= "</table>";

		return $table_html;
	}

	private function getTypeOfCustomer($type)
	{
		$array = [
			1 => "customers",
			2 => "service_providers",
			3 => "affiliate_partners",
			4 => "other",
			5 => "iam",
			6 => "resellers"
		];

		return $array[$type];
	}

    /**
     * @return false
     */
    private function createCache(): bool
    {

        //Array prep
        $customers = [];
        $customer_types = CustomerType::all();
        $mover_statuses = MoverStatus::all();
        $service_statuses = ServiceProviderStatus::all();
        $affiliate_statuses = AffiliatePartnerStatus::all();
        $system = new System();

        foreach ($customer_types as $type_id => $type)
        {
            $customers[$type_id] = [];
        }

        foreach ([1 => $mover_statuses, 2 => $service_statuses, 3 => $affiliate_statuses, 6 => $mover_statuses] as $type => $sub_types)
        {
            $arr = [];
            foreach ($sub_types as $index => $name)
            {
                $arr[$index] = [];
            }
            $customers[$type] = $arr;
        }

        $query = DB::table("customers")
            ->selectRaw("*")
            ->leftJoin("affiliate_partner_data", "afpada_cu_id", "cu_id")
            ->where("cu_deleted", 0)
            ->orderBy("cu_company_name_business", 'asc')
            ->get();


        foreach ($query as $customer)
        {
            $found_customers[$customer->cu_id] = $customer;
        }

        define("ACTIVE", 1);
        define("ACTIVE_FT", 2);
        define("PAUSE", 3);

        //Sort them in the correct types
        $movers = [];
        $lead_resellers = [];
        $service_providers = [];

        foreach ($found_customers as $cu_id => $customer)
        {
            $cu_type = intval($customer->cu_type);

            if ($cu_type === 1 || $cu_type === 2 || $cu_type === 6)
            {
                //General checks
                if (!self::hasCreditLimitation($customers, $customer, $cu_type))
                {
                    if ($cu_type === 1)
                    {
                        $movers[$customer->cu_id] = [
                            ACTIVE => false,
                            ACTIVE_FT => false,
                            PAUSE => false
                        ];
                    } elseif ($cu_type === 6)
                    {
                        $lead_resellers[$customer->cu_id] = [
                            ACTIVE => false,
                            ACTIVE_FT => false,
                            PAUSE => false
                        ];
                    } else
                    {
                        $service_providers[$customer->cu_id] = [
                            ACTIVE => false,
                            PAUSE => false
                        ];
                    }

                    continue;
                }
            } elseif ($cu_type === 3)
            {
                $customers[3][$customer->afpada_status][$cu_id] = $customer;
            } else
            {
                //Just add it to the array
                $customers[$cu_type][$cu_id] = $customer;
            }
        }

        if (!empty($movers))
        {
            //Get kt_customer_portals
            $ktcupo_query = KTCustomerPortal::select("ktcupo_id", "ktcupo_cu_id", "ktcupo_status", "ktcupo_free_trial")->whereIn("ktcupo_cu_id", array_keys($movers))->get();

            foreach ($ktcupo_query as $ktcupo_row)
            {
                $cu_id = $ktcupo_row->ktcupo_cu_id;
                if ($ktcupo_row->ktcupo_status == 1)
                {
                    if ($ktcupo_row->ktcupo_free_trial == 0)
                    {
                        $movers[$cu_id][ACTIVE] = true;
                    } else
                    {
                        $movers[$cu_id][ACTIVE_FT] = true;
                    }
                } else if ($ktcupo_row->ktcupo_status == 2)
                {
                    $movers[$cu_id][PAUSE] = true;
                }
            }

            foreach ($movers as $cu_id => $status)
            {
                $add_to_type = null;

                //Check if customer has nat portals
                $int_portals = KTCustomerPortal::leftJoin("portals", "ktcupo_po_id", "po_id")
                    ->where("ktcupo_cu_id", $cu_id)
                    ->where("po_destination_type", 1)
                    ->where("ktcupo_status", 1)
                    ->where("ktcupo_type", 1)
                    ->get();

                $nat_portals = KTCustomerPortal::leftJoin("portals", "ktcupo_po_id", "po_id")
                    ->where("ktcupo_cu_id", $cu_id)
                    ->where("po_destination_type", 2)
                    ->where("ktcupo_status", 1)
                    ->where("ktcupo_type", 1)
                    ->get();

                $support_portals = KTCustomerPortal::leftJoin("portals", "ktcupo_po_id", "po_id")
                    ->where("ktcupo_cu_id", $cu_id)
                    ->where("ktcupo_status", 1)
                    ->where("ktcupo_type", 2)
                    ->get();

                $conversion_tools = KTCustomerSubscription::where("ktcusu_cu_id", $cu_id)
                    ->where("ktcusu_sub_id", SubscriptionType::CONVERSION_TOOLS)
                    ->where("ktcusu_active", 1)
                    ->get();

                if ($status[ACTIVE] && count($nat_portals) > 0 && count($int_portals) == 0)
                {
                    $add_to_type = 10;
                } elseif ($status[ACTIVE] && count($nat_portals) == 0 && count($int_portals) == 0 && count($support_portals) > 0)
                {
                    $add_to_type = 11;
                } elseif (count($conversion_tools) > 0)
                {
                    $add_to_type = 12;
                } elseif ($status[ACTIVE]) //When a customer has INT portals, and maybe got one or more NAT portals.
                {
                    $add_to_type = 1;
                } elseif ($status[ACTIVE_FT])
                {
                    $add_to_type = 2;
                } elseif ($status[PAUSE])
                {
                    $add_to_type = 3;
                }

                $found_customers[$cu_id]->sub_type = $add_to_type;

                if ($add_to_type !== null)
                {
                    $customers[1][$add_to_type][$cu_id] = $found_customers[$cu_id];

                    unset($movers[$cu_id]);
                }
            }

            if (!empty($movers))
            {
                $status_query = CustomerStatus::selectRaw("DISTINCT `cust_cu_id`")
                    ->where("cust_status", "=", 8)
                    ->whereIn("cust_cu_id", array_keys($movers))
                    ->whereRaw("cust_date BETWEEN NOW() - INTERVAL 1 YEAR AND NOW()")
                    ->get();

                foreach ($status_query as $status_row)
                {
                    $cu_id = $status_row->cust_cu_id;

                    $found_customers[$cu_id]->sub_type = 4;

                    $customers[1][4][$cu_id] = $found_customers[$cu_id];

                    unset($movers[$cu_id]);
                }
            }

            //=> The rest is prospect
            foreach ($movers as $cu_id => $status)
            {
                $acc_manager = Customer::where("cu_id", $cu_id)->first()->cu_account_manager;
                if ($acc_manager != 4186)
                {
                    //If Arjan is the account manager, then don't add it to prospect list.
                    //This are the imported customers
                    $customers[1][5][$cu_id] = $found_customers[$cu_id];
                }
            }
        }

        if (!empty($lead_resellers))
        {
            //Get kt_customer_portals
            $ktcupo_query = KTCustomerPortal::select("ktcupo_id", "ktcupo_cu_id", "ktcupo_status", "ktcupo_free_trial")->whereIn("ktcupo_cu_id", array_keys($lead_resellers))->get();

            foreach ($ktcupo_query as $ktcupo_row)
            {
                $cu_id = $ktcupo_row->ktcupo_cu_id;
                if ($ktcupo_row->ktcupo_status == 1)
                {
                    if ($ktcupo_row->ktcupo_free_trial == 0)
                    {
                        $lead_resellers[$cu_id][ACTIVE] = true;
                    } else
                    {
                        $lead_resellers[$cu_id][ACTIVE_FT] = true;
                    }
                } else if ($ktcupo_row->ktcupo_status == 2)
                {
                    $lead_resellers[$cu_id][PAUSE] = true;
                }
            }

            foreach ($lead_resellers as $cu_id => $status)
            {
                $add_to_type = null;

                if ($status[ACTIVE])
                {
                    $add_to_type = 1;
                } elseif ($status[ACTIVE_FT])
                {
                    $add_to_type = 2;
                } elseif ($status[PAUSE])
                {
                    $add_to_type = 3;
                }

                $found_customers[$cu_id]->sub_type = $add_to_type;

                if ($add_to_type !== null)
                {
                    $customers[6][$add_to_type][$cu_id] = $found_customers[$cu_id];

                    unset($lead_resellers[$cu_id]);
                }
            }

            if (!empty($lead_resellers))
            {
                $status_query = CustomerStatus::selectRaw("DISTINCT `cust_cu_id`")
                    ->where("cust_status", "=", 8)
                    ->whereIn("cust_cu_id", array_keys($lead_resellers))
                    ->whereRaw("cust_date BETWEEN NOW() - INTERVAL 1 YEAR AND NOW()")
                    ->get();

                foreach ($status_query as $status_row)
                {
                    $cu_id = $status_row->cust_cu_id;

                    $found_customers[$cu_id]->sub_type = 4;

                    $customers[6][4][$cu_id] = $found_customers[$cu_id];

                    unset($lead_resellers[$cu_id]);
                }
            }

            //=> The rest is prospect
            foreach ($lead_resellers as $cu_id => $status)
            {
                $customers[6][5][$cu_id] = $found_customers[$cu_id];
            }
        }

        if (!empty($service_providers))
        {
            //Service providers
            $service_provider_tables = ["ktcuqu" => "kt_customer_question", "seprnebl" => "service_provider_newsletter_blocks"];
            foreach ($service_provider_tables as $prefix => $table)
            {
                $service_query = DB::table($table)
                    ->select($prefix . "_cu_id", $prefix . "_status")
                    ->whereIn($prefix . "_cu_id", array_keys($service_providers))
                    ->get();


                foreach ($service_query as $service_row_db)
                {

                    $service_row = $system->databaseToArray($service_row_db);

                    $cu_id = $service_row["{$prefix}_cu_id"];
                    $status = intval($service_row["{$prefix}_status"]);
                    if ($status === 1)
                    {
                        $service_providers[$cu_id][ACTIVE] = true;
                    } elseif ($status === 2)
                    {
                        $service_providers[$cu_id][PAUSE] = true;
                    }
                }
            }

            //=> Loop through
            foreach ($service_providers as $cu_id => $status)
            {
                $type = ($status[ACTIVE] ? 1 : ($status[PAUSE] ? 2 : 3));
                $customers[2][$type][$cu_id] = $found_customers[$cu_id];
            }
        }

        $countries = Country::All();

        $country_list = [];

        foreach ($countries as $country)
        {
            $country_list[$country->co_code] = $country->co_en;
        }

        $customers_incorrect = [];

        //Get customers
        $query = DB::table("customers")
            ->selectRaw("`customers`.*")
            ->selectRaw("`mover_data`.*")
            ->leftJoin("kt_customer_portal", "cu_id", "ktcupo_cu_id")
            ->leftJoin("kt_customer_question", "cu_id", "ktcuqu_id")
            ->leftJoin("service_provider_newsletter_blocks", "cu_id", "seprnebl_cu_id")
            ->leftJoin("mover_data", "cu_id", "moda_cu_id")
            ->whereRaw("(
					((`cu_type` = 1 OR `cu_type` = 6) AND `ktcupo_status` = 1) OR
					(`cu_type` = 2 AND (`ktcuqu_status` = 1 OR `seprnebl_status` = 1)) OR
					`cu_type` IN (3, 4)
				)")
            ->where("cu_deleted", "=", 0)
            ->groupBy("cu_id")
            ->orderBy("cu_company_name_business", "asc")
            ->get();

        foreach ($query as $row)
        {
            $errors = [];

            $check_fields = [];

            //Empty checks
            $check_fields['cu_company_name_legal'] = "company legal name";
            $check_fields['cu_company_name_business'] = "company business name";

            if ($row->cu_type == 1 || $row->cu_type == 2 || $cu_type == 6)
            {
                $check_fields['cu_city'] = "city";

                if ($row->cu_use_billing_address == 1)
                {
                    $check_fields['cu_city_bi'] = "city (billing)";
                }
            }

            $check_fields['cu_email'] = "email";
            $check_fields['cu_co_code'] = "country";

            if ($row->cu_use_billing_address == 1)
            {
                $check_fields['cu_email_bi'] = "email (billing)";
                $check_fields['cu_co_code_bi'] = "country (billing)";
            }

            foreach ($check_fields as $column => $name)
            {
                if (empty($row->{$column}))
                {
                    $errors['cu'][] = "Missing " . $name;
                }
            }

            $system->validateEmail($row->cu_email, false, $cu_errors);

            if ($row->cu_use_billing_address == 1)
            {
                $system->validateEmail($row->cu_email_bi, true, $cu_errors);
            }

            if ($row->cu_type == 1 || $row->cu_type == 6)
            {
                if ($row->moda_capping_method == 1 || $row->moda_capping_method == 2)
                {
                    if (($row->moda_capping_monthly_limit + $row->moda_capping_monthly_limit_modifier < 0) || $row->moda_capping_daily_average < 0)
                    {
                        $errors['cu'][] = "Invalid cappings";
                    }
                }

                $query_cupo = DB::table("kt_customer_portal")
                    ->select("*")
                    ->leftJoin("portals", "ktcupo_po_id", "po_id")
                    ->where("ktcupo_cu_id", "=", $row->cu_id)
                    ->where("ktcupo_status", "=", 1)
                    ->get();

                foreach ($query_cupo as $cupo_row)
                {
                    $kt_errors = [];
                    $id = $cupo_row->ktcupo_id;
                    $field = "ktcupo";

                    $free_trial = null;

                    $query_ft = DB::table("free_trials")
                        ->select("frtr_finished")
                        ->where("frtr_ktcupo_id", "=", $id)
                        ->whereRaw("`frtr_date` <= NOW()")
                        ->orderBy("frtr_date", "desc")
                        ->get();

                    foreach ($query_ft as $ft_row)
                    {
                        if ($free_trial !== null)
                        {
                            continue;
                        }

                        $free_trial = ($ft_row->frtr_finished == 0 ? true : false);
                    }

                    //Free trials
                    if ($cupo_row->ktcupo_free_trial == 1 && $cupo_row->ktcupo_type != 2)
                    {
                        if ($free_trial === null)
                        {
                            $kt_errors[] = "No free trials active";
                        } elseif ($free_trial === false)
                        {
                            $kt_errors[] = "Free trial has ended";
                        }
                    } else
                    {
                        $query_payment_rate = DB::table("payment_rates")
                            ->select("para_id")
                            ->where("para_ktcupo_id", "=", $id)
                            ->whereRaw("`para_date` <= NOW()")
                            ->orderBy("para_date", "desc")
                            ->limit(1)
                            ->get();

                        if (count($query_payment_rate) == 0 && $cupo_row->ktcupo_type != 2)
                        {
                            $kt_errors[] = "No payment rate active";
                        }
                    }


                    /**
                     * @@ NEED TO BE CHECKED > MOVING SIZES??????
                     */

                    //Moving size
                    $moving_sizes = [];

                    $dataindex = new DataIndex();

                    foreach ($dataindex->movingSizes($cupo_row->ktcupo_request_type) as $size_id => $size_name)
                    {
                        $name = 'ktcupo_moving_size_' . $size_id;
                        if ($cupo_row->$name == 1)
                        {
                            $moving_sizes[$size_id] = $size_name;
                        }
                    }

                    if (empty($moving_sizes) && $cupo_row->ktcupo_type != 2)
                    {
                        $kt_errors[] = "No moving sizes selected";
                    }

                    //Moves
                    if ($cupo_row->ktcupo_export_moves == 0 && $cupo_row->ktcupo_import_moves == 0)
                    {
                        $kt_errors[] = "No export moves & no import moves";
                    }

                    //Cappings
                    if (($row->moda_capping_method == 0 && ($cupo_row->ktcupo_max_requests_month == 0)) || $cupo_row->ktcupo_max_requests_day == 0 && $cupo_row->ktcupo_type != 2)
                    {
                        $kt_errors[] = "Invalid cappings";
                    }

                    //Origins
                    if (self::empty_check($id, "kt_customer_portal_region", "ktcupore", $field) && $cupo_row->ktcupo_type != 2)
                    {
                        $kt_errors[] = "No origin regions selected";
                    }

                    //Destinations
                    if ($cupo_row->po_destination_type == 1)
                    {
                        if (self::empty_check($id, "kt_customer_portal_country", "ktcupoco", $field))
                        {
                            $kt_errors[] = "No destination countries selected";
                        }
                    } elseif ($cupo_row->po_destination_type == 2)
                    {
                        if (self::empty_check($id, "kt_customer_portal_region_destination", "ktcuporede", $field))
                        {
                            $kt_errors[] = "No destination regions selected";
                        }
                    }

                    $query_rd = RequestDelivery::select("rede_value")
                        ->where("rede_customer_type", "=", 1)
                        ->whereIn("rede_type", [1, 2, 5, 6])
                        ->where("rede_ktcupo_id", "=", $cupo_row->ktcupo_id)
                        ->get();

                    foreach ($query_rd as $rd_row)
                    {
                        System::validateEmail($rd_row->rede_value, false, $kt_errors);

                        if (!empty($rd_row->rede_extra))
                        {
                            System::validateEmail($rd_row->rede_extra, false, $kt_errors);
                        }
                    }

                    foreach ($kt_errors as $error)
                    {
                        $errors['kt'][] = [
                            "id" => $id,
                            "type" => "customer_portal",
                            "field" => $field,
                            "error" => $error
                        ];
                    }
                }
            } elseif ($row->cu_type == 2)
            {
                $query_cuqu = KTCustomerQuestion::where("ktcuqu_cu_id", "=", $row->cu_id)
                    ->where("ktcuqu_status", "=", 1)
                    ->get();

                foreach ($query_cuqu as $cuqu_row)
                {
                    $kt_errors = [];
                    $id = $cuqu_row->ktcuqu_id;
                    $field = "ktcuqu";

                    //Moves
                    if ($cuqu_row->ktcuqu_international_moves == 0 && $cuqu_row->ktcuqu_national_moves == 0)
                    {
                        $kt_errors[] = "No international moves & no national moves";
                    }

                    //Language
                    if (self::empty_check($id, "kt_customer_question_language", "ktcuqula", $field))
                    {
                        $kt_errors[] = "No languages selected";
                    }

                    //Cappings
                    if ($cuqu_row->ktcuqu_max_requests_month == 0 || $cuqu_row->ktcuqu_max_requests_day == 0)
                    {
                        $kt_errors[] = "Invalid cappings";
                    }

                    //Origins
                    if (self::empty_check($id, "kt_customer_question_country_origin", "ktcuqucoor", $field))
                    {
                        $kt_errors[] = "No origin countries selected";
                    }

                    //Destinations
                    if (self::empty_check($id, "kt_customer_question_country_destination", "ktcuqucode", $field))
                    {
                        $kt_errors[] = "No destination countries selected";
                    }

                    $query_rd = RequestDelivery::select("rede_value")
                        ->where("rede_customer_type", "=", 2)
                        ->whereIn("rede_type", [1, 2, 5, 6])
                        ->where("rede_ktcupo_id", "=", $cuqu_row->ktcuqu_id)
                        ->get();

                    foreach ($query_rd as $rd_row)
                    {
                        $system->validateEmail($rd_row->rede_value, false, $kt_errors);

                        if (!empty($rd_row->rede_extra))
                        {
                            $system->validateEmail($rd_row->rede_extra, false, $kt_errors);
                        }
                    }

                    foreach ($kt_errors as $error)
                    {
                        $errors['kt'][] = [
                            "id" => $id,
                            "type" => "customer_question",
                            "field" => $field,
                            "error" => $error
                        ];
                    }
                }

                $query_seprnebl = ServiceProviderNewsletterBlock::where("seprnebl_cu_id", "=", $row->cu_id)
                    ->where("seprnebl_status", "=", 1)
                    ->get();

                foreach ($query_seprnebl as $seprnebl_row)
                {
                    $kt_errors = [];
                    $id = $seprnebl_row->seprnebl_id;
                    $field = "seprnebl";

                    //Moves
                    if ($seprnebl_row->seprnebl_international_moves == 0 && $seprnebl_row->seprnebl_national_moves == 0)
                    {
                        $kt_errors[] = "No international moves & no national moves";
                    }

                    //Language
                    if (self::empty_check($id, "kt_service_provider_newsletter_block_language", "ktseprneblla", $field))
                    {
                        $kt_errors[] = "No languages selected";
                    }

                    //Title
                    if (empty($seprnebl_row->seprnebl_title))
                    {
                        $kt_errors[] = "The newsletter title is empty";
                    }

                    //Content
                    if (empty($seprnebl_row->seprnebl_content))
                    {
                        $kt_errors[] = "The newsletter content is empty";
                    }

                    //Origins
                    if (self::empty_check($id, "kt_service_provider_newsletter_block_country_origin", "ktseprneblcoor", $field))
                    {
                        $kt_errors[] = "No origin countries selected";
                    }

                    //Destinations
                    if (self::empty_check($id, "kt_service_provider_newsletter_block_country_destination", "ktseprneblcode", $field))
                    {
                        $kt_errors[] = "No destination countries selected";
                    }

                    foreach ($kt_errors as $error)
                    {
                        $errors['kt'][] = [
                            "id" => $id,
                            "type" => "newsletter_block",
                            "field" => $field,
                            "error" => $error
                        ];
                    }
                }
            }

            //Add to results
            if (!empty($errors))
            {
                $customers_incorrect[$row->cu_id] = [
                    "row" => $row,
                    "errors" => $errors
                ];
            }
        }
        $order_by = '[[1, "asc"]]';

        $customers_incorrect_html = "<table data-order='{$order_by}' class='table table-bordered table-striped table-vcenter js-dataTable-full dataTable no-footer'>";
        $customers_incorrect_html .= "<thead>";
        $customers_incorrect_html .= "<tr>";
        $customers_incorrect_html .= "<th>ID</th>";
        $customers_incorrect_html .= "<th>Customer</th>";
        $customers_incorrect_html .= "<th>Country</th>";
        $customers_incorrect_html .= "<th>Type</th>";
        $customers_incorrect_html .= "<th>Error</th>";
        $customers_incorrect_html .= "<th>Actions</th>";
        $customers_incorrect_html .= "</tr>";
        $customers_incorrect_html .= "</thead>";
        $customers_incorrect_html .= "<tbody>";

        foreach ($customers_incorrect as $cu_id => $customer)
        {
            $row = $customer['row'];

            foreach ($customer['errors'] as $type => $errors)
            {
                foreach ($errors as $error)
                {
                    if ($type == "kt")
                    {
                        $error_type = $error['type'];
                        $extra = "&" . $error['field'] . "_id=" . $error['id'];
                        $error = $error['error'];

                    } else
                    {
                        $error_type = "customer";
                        $extra = "";
                    }

                    $start_url = "";

                    if ($row->cu_type == 1)
                    {
                        $start_url = "customers";
                    } elseif ($row->cu_type == 6)
                    {
                        $start_url = "resellers";
                    } elseif ($row->cu_type == 2)
                    {
                        $start_url = "serviceproviders";
                    } else
                    {
                        $start_url = "affiliatepartners";
                    }

                    $customers_incorrect_html .= "<tr>";
                    $customers_incorrect_html .= "<td>" . $cu_id . "</td>";
                    $customers_incorrect_html .= "<td>" . $row->cu_company_name_business . "</td>";
                    $customers_incorrect_html .= "<td>" . @$system->useOr($country_list[$row->cu_co_code]) . "</td>";
                    $customers_incorrect_html .= "<td>" . ucfirst(str_replace("_", " ", $error_type)) . "</td>";
                    $customers_incorrect_html .= "<td>" . $error . "</td>";
                    $customers_incorrect_html .= "<td><a href='/{$start_url}/{$row->cu_id}/edit'><button type='button' class='btn btn-sm btn-primary js-tooltip-enabled' data-toggle='tooltip' data-id='" . $row->cu_id . "' data-original-title='Edit'><i class='fa fa-pencil-alt'></i></button></a></td>";
                    $customers_incorrect_html .= "</tr>";
                }
            }
        }

        $customers_incorrect_html .= "</tbody>";
        $customers_incorrect_html .= "</table>";

        //Create Customers Incorrect file
        $customers_incorrect_file = 'resources/views/cache/customers_incorrect.php';
        $customers_incorrect_file_2 = resource_path() . '/views/cache/customers_incorrect.php';

        if (is_readable($customers_incorrect_file) && $handle = fopen($customers_incorrect_file, 'w'))
        {
            fwrite($handle, $customers_incorrect_html);
        } else
        {
            $handle = fopen($customers_incorrect_file_2, 'w');
            fwrite($handle, $customers_incorrect_html);
        }

        //Save in cache
        $count_cache = [];
        $companies = [];

        foreach ($customers as $type => $results)
        {
            if ($type === 1 || $type === 2 || $type == 3 || $type == 6)
            {
                $count_cache[$type] = [];
                foreach ($results as $sub_type => $sub_results)
                {
                    if ($sub_type == 1 && ($type == 1 || $type == 6))
                    {
                        $companies = array_merge($companies, array_keys($sub_results));
                    }
                    if ($sub_type == 3 && ($type == 1 || $type == 6))
                    {
                        $companies = array_merge($companies, array_keys($sub_results));
                    }
                    if ($sub_type == 6 && ($type == 1 || $type == 6))
                    {
                        $companies = array_merge($companies, array_keys($sub_results));

                        DB::table('cache')
                            ->where('ca_name', "customer_gdpr_list")
                            ->update(
                                [
                                    'ca_value' => base64_encode(serialize(($companies)))
                                ]
                            );
                    }

                    $count_cache[$type][$sub_type] = count($sub_results);

                    $customer_files = "resources/views/cache/" . self::getTypeOfCustomer($type) . "_{$sub_type}.php";
                    $customer_files_2 = resource_path() . "/views/cache/" . self::getTypeOfCustomer($type) . "_{$sub_type}.php";

                    if (is_readable($customer_files) && $handle = fopen($customer_files, 'w'))
                    {
                        fwrite($handle, self::makeTable($sub_results, $type, $sub_type));
                    } else
                    {
                        $handle = fopen($customer_files_2, 'w');
                        fwrite($handle, self::makeTable($sub_results, $type, $sub_type));
                    }
                }
            } else
            {
                $count_cache[$type] = count($results);

                $customer_files = "resources/views/cache/" . self::getTypeOfCustomer($type) . "_{$type}.php";
                $customer_files_2 = resource_path() . "/views/cache/" . self::getTypeOfCustomer($type) . "_{$type}.php";

                if (is_readable($customer_files) && $handle = fopen($customer_files, 'w'))
                {
                    fwrite($handle, self::makeTable($results, $type, 0));
                } else
                {
                    $handle = fopen($customer_files_2, 'w');
                    fwrite($handle, self::makeTable($results, $type, 0));
                }
            }
        }

        $deleted_customers_query = Customer::where("cu_deleted", "=", 1)
            ->orderBy("cu_company_name_business", "asc")
            ->get();

        $deleted_customers = [];

        foreach ($deleted_customers_query as $customer)
        {
            $deleted_customers[$customer->cu_id] = $customer;
        }

        $deleted_customers_file = "resources/views/cache/customers_8.php";
        $deleted_customers_file_2 = resource_path() . "/views/cache/customers_8.php";

        if (is_readable($deleted_customers_file) && $handle = fopen($deleted_customers_file, 'w'))
        {
            fwrite($handle, self::makeTable($deleted_customers, 1, 8));
        } else
        {
            $handle = fopen($deleted_customers_file_2, 'w');
            fwrite($handle, self::makeTable($deleted_customers, 1, 8));
        }

        $count_cache[1][8] = count($deleted_customers);
        $count_cache['incorrect'] = count($customers_incorrect);

        DB::table('cache')
            ->where('ca_name', "customer_count")
            ->update(
                [
                    'ca_value' => base64_encode(serialize(($count_cache))),
                    'ca_timestamp' => date("Y-m-d H:i:s")
                ]
            );

        return false;
    }
}
