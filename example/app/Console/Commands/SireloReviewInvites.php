<?php

namespace App\Console\Commands;

use App\Cronjobs\CronjobController;
use App\Functions\Data;
use App\Functions\Mail;
use App\Functions\System;
use App\Models\SireloReviewInvite;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SireloReviewInvites extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronjob:sirelo_review_invites';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    private $cc;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CronjobController $cc)
    {
        $this->cc = $cc;
        parent::__construct();
    }


    const CRONJOB_ID = 31;
    const CRONJOB_TASK = 'Sirelo';
    const CRONJOB_SUBTASK = 'Send Sirelo Review Invites';
    const CRONJOB_TRIGGER = 1;
    const CRONJOB_INTERVAL = "60 MINUTE";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Set Cronjob check values
        $CRONJOB_USERS = [3653];

        try
        {
            $this->sendSireloReviewInvites();
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 0, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, null);

        }catch (\Throwable $e){
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 1, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, $e);
        }


    }

    private function sendSireloReviewInvites(): void
    {
        $system = new System();
        $mail = new Mail();
        $data = new Data();

        $query = SireloReviewInvite::leftJoin("customers", "sirein_cu_id", "cu_id")
            ->where("sirein_sent", 0)
            ->where("cu_deleted", 0)
            ->orderBy("sirein_timestamp", "ASC")
            ->limit(50)
            ->get();

        $query = $system->databaseToArray($query);

        foreach ($query as $row)
        {
            DB::table('sirelo_review_invites')
                ->where('sirein_id', $row['sirein_id'])
                ->update(
                    [
                        'sirein_sent' => 1,
                        'sirein_timestamp_sent' => DB::raw("NOW()")
                    ]
                );

            $subject = str_replace("[name]", $row['sirein_name'], $row['sirein_subject']);
            $body = str_replace("[name]", $row['sirein_name'], nl2br($row['sirein_content']));

            $body = preg_replace("/\[button\](.*)\[\/button\]/", $mail->buttonNew($data->getSireloCustomerPage($row['cu_id']) . $data->getSireloFieldByCountry($row['cu_co_code'], "we_sirelo_customer_review_slug"), "$1"), $body);

            $fields = [
                "sirein_id" => $row['sirein_id'],
                "email" => $row['sirein_email'],
                "country" => $row['cu_co_code'],
                "subject" => $subject,
                "body" => $body,
            ];


            $mail->send("sirelo_review_invite", $data->getSireloFieldByCountry($row['cu_co_code'], "we_sirelo_la_code"), $fields);
        }
    }
}

