<?php

namespace App\Console\Commands;

use App\Cronjobs\CronjobController;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class RestoreMaxRequestsMonth extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronjob:restore_max_requests_month';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    private $cc;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CronjobController $cc)
    {
        $this->cc = $cc;
        parent::__construct();
    }


    const CRONJOB_ID = 28;
    const CRONJOB_TASK = 'Matching';
    const CRONJOB_SUBTASK = 'Restore Max Monthly Requests';
    const CRONJOB_TRIGGER = 1;
    const CRONJOB_INTERVAL = "1 MONTH";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Set Cronjob check values
        $CRONJOB_USERS = [3653];

        try
        {
            $this->restoreMonthlyRequests();
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 0, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, null);

        }catch (\Throwable $e){
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 1, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, $e);
        }


    }

    private function restoreMonthlyRequests(): void
    {
        DB::table('mover_data')
            ->update(
                [
                    'moda_capping_monthly_limit_left' => DB::raw("moda_capping_monthly_limit")
                ]
            );

        DB::table('kt_customer_portal')
            ->update(
                [
                    'ktcupo_max_requests_month_left' => DB::raw("ktcupo_max_requests_month")
                ]
            );

        DB::table('kt_customer_question')
            ->update(
                [
                    'ktcuqu_max_requests_month_left' => DB::raw("ktcuqu_max_requests_month")
                ]
            );

        DB::table('affiliate_partner_data')
            ->update(
                [
                    'afpada_max_requests_month_left' => DB::raw("afpada_max_requests_month")
                ]
            );
    }
}

