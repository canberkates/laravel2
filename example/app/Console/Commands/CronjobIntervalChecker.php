<?php

namespace App\Console\Commands;

use App\Cronjobs\CronjobController;
use App\Functions\DataIndex;
use App\Functions\System;
use App\Functions\Teams;
use App\Models\AdyenCardDetails;
use App\Models\Cronjob;
use Carbon\Carbon;
use DateTime;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CronjobIntervalChecker extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronjob:check_intervals';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    private $cc;
    private $teams;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CronjobController $cc, Teams $teams)
    {
        $this->cc = $cc;
        $this->teams = $teams;
        parent::__construct();
    }

    //Class constants for Cronjob Info
    const CRONJOB_ID = 0;
    const CRONJOB_TASK = 'IT';
    const CRONJOB_SUBTASK = 'Cronjob Interval Checker';
    const CRONJOB_TRIGGER = 1;
    const CRONJOB_INTERVAL = "15 MINUTE";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Get correct users for this Cronjob
        $CRONJOB_USERS = [3653];

        try
        {
            $this->checkCronjobs();
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 0, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, null);

        }catch (\Throwable $e){
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 1, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, $e);
        }

    }

   private function checkCronjobs()
   {
       $system = new System();

       //Get all cronjobs except this one
        $cronjobs = Cronjob::where("cr_id", ">=", 1 )->get();

        foreach($cronjobs as $cronjob){

            $check_date = date("Y-m-d H:i:s", strtotime("- ".$cronjob->cr_interval));
            $last_ran = $cronjob->cr_last_ran;

            if($check_date > $last_ran){

                $content = $cronjob->cr_subtask. " (ID: ".$cronjob->cr_id.") has not ran when we expected it to (every ".$cronjob->cr_interval.")! The last time this cronjob ran is ".$cronjob->cr_last_ran;
                $system->sendMessage(3653, $cronjob->cr_subtask . " didn't run!", $content);
                self::sendTeamsMessage($content);
            }

            if($cronjob->cr_trigger <= $cronjob->cr_error_count){
                $content = $cronjob->cr_subtask. " (ID: ".$cronjob->cr_id.") has crashed and passed its trigger!";
                $system->sendMessage(3653, $cronjob->cr_subtask . " didn't run!", $content);
                self::sendTeamsMessage($content);
            }

        }
   }

   private function sendTeamsMessage($content){
       $webhook_url = "https://triglobalbv.webhook.office.com/webhookb2/467aabaf-ab9f-4575-a8eb-b71837b60355@21938c5d-879a-4eef-996a-ac494c9850a6/IncomingWebhook/2f68d60801d54e18b3d8e56670ed225a/e78f7649-2005-4400-9843-f8c4454feb6a";
       $title = "⛔ A cronjob isn't running! ⛔";
       $text = $content;

       $this->teams->sendMessage($webhook_url, $title, $text);
   }
}

