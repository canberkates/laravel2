<?php

namespace App\Console\Commands;


use App\Cronjobs\CronjobController;
use App\Functions\Reporting;
use App\Functions\System;
use App\Models\ReportProgress;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ReportsClean extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronjob:reports_clean';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    private $cc;
    /**
     * @var System
     */
    private $system;
    /**
     * @var Reporting
     */
    private $reporting;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CronjobController $cc, System $system, Reporting $reporting)
    {
        parent::__construct();

        $this->cc = $cc;
        $this->system = $system;
        $this->reporting = $reporting;
    }

    const CRONJOB_ID = 24;
    const CRONJOB_TASK = 'Phoenix';
    const CRONJOB_SUBTASK = 'Clean Reports';
    const CRONJOB_TRIGGER = 5;
    const CRONJOB_INTERVAL = "1 DAY";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        //Set Cronjob check values
        $CRONJOB_USERS = [3653];

        try
        {
            $this->cleanReports();
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 0, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, null);

        }catch (\Throwable $e){
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 1, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, $e);
        }

    }

    private function cleanReports(): void
    {

        $query = ReportProgress::where("repr_status", 3)
            ->whereRaw("`repr_finish_timestamp` + INTERVAL 24 HOUR < NOW()")
            ->get();

        $query = $this->system->databaseToArray($query);

        foreach ($query as $row)
        {

            //Update the row
            $this->reporting->updateField($row['repr_id'], ["repr_status" => 4], false);

            //Delete the file
            $file_path = env("SHARED_FOLDER") . "uploads/reports/report-result-" . $row['repr_id'] . ".html";
            if (file_exists($file_path))
            {
                unlink($file_path);
            }
        }

        //Check if the reports can be truncated
        $repr_count_query = ReportProgress::selectRaw("COUNT(*) AS `nr`")
            ->where("repr_status", "!=", 4)
            ->first();

        $repr_count = $repr_count_query;

        if ($repr_count['nr'] == 0)
        {
            //Truncate the table
            DB::query("TRUNCATE `report_progress`");

            //Remove report html files
            $html_files = glob(env("SHARED_FOLDER") . "uploads/reports/*");
            if ($html_files)
            {
                foreach ($html_files as $file)
                {
                    if (is_file($file))
                    {
                        unlink($file);
                    }
                }
            }
        }
    }
}

