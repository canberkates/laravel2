<?php

namespace App\Console\Commands;

use App\Cronjobs\CronjobController;
use App\Data\CustomerPairStatus;
use App\Functions\Mail;
use App\Functions\System;
use App\Models\KTCustomerPortal;
use App\Models\PauseHistory;
use Carbon\Carbon;
use DB;
use Illuminate\Console\Command;
use Log;
use Venturecraft\Revisionable\Revision;

class PauseAndUnpauseCustomers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'checker:pauses';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cronjob that checks which companies should be paused and unpaused today';

    private $cc;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CronjobController $cc)
    {
        $this->cc = $cc;
        parent::__construct();
    }


    const CRONJOB_ID = 19;
    const CRONJOB_TASK = 'Matching';
    const CRONJOB_SUBTASK = 'Pause and Unpause Customers';
    const CRONJOB_TRIGGER = 1;
    const CRONJOB_INTERVAL = "1 DAY";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        //Set Cronjob check values
        $CRONJOB_USERS = [3653];

        try
        {
            $this->checkPauses();
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 0, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, null);

        }catch (\Throwable $e){
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 1, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, $e);
        }


    }

    /**
     * @param PauseHistory $pause_customer
     * @param $status
     */
    public function storePauseHistory(PauseHistory $pause_customer, $status): void
    {
        //Also track changes into Revision-table
        $portals = KTCustomerPortal::where("ktcupo_cu_id", $pause_customer->ph_cu_id)
            ->where("ktcupo_status", $status)->get();

        if($status == CustomerPairStatus::ACTIVE){
            $other_status = CustomerPairStatus::PAUSE;
        }elseif($status == CustomerPairStatus::PAUSE){
            $other_status = CustomerPairStatus::ACTIVE;
        }

        if ($portals->count())
        {
            foreach ($portals as $portal)
            {
                DB::table((new Revision())->getTable())->insert([
                    [
                        'revisionable_type' => "App\Models\KTCustomerPortal",
                        'revisionable_id' => $portal->ktcupo_id,
                        'key' => 'ktcupo_status',
                        'old_value' => $status,
                        'new_value' => $other_status,
                        'created_at' => Carbon::now()->toDateTimeString(),
                        'updated_at' => Carbon::now()->toDateTimeString()
                    ]
                ]);
            }
        }
    }

    private function checkPauses(): void
    {
        $system = new System();
        $mail = new Mail();

        $to_be_paused = PauseHistory::join("customers", "cu_id", "ph_cu_id")->where("ph_start_date", date("Y-m-d"))->get();
        $to_be_unpaused = PauseHistory::join("customers", "cu_id", "ph_cu_id")->where("ph_end_date", date("Y-m-d"))->get();

        $system->sendMessage([3653], "Automatic Pause", System::serialize([$to_be_paused, $to_be_unpaused]));

        Log::debug("Pause Cronjob Results:");
        Log::debug($to_be_paused);
        Log::debug($to_be_unpaused);

        //Perform Unpause-code
        foreach ($to_be_unpaused as $unpause_customer)
        {
            $system->sendMessage([1778, 4669, 4677], "Automatic unpause has been triggered", $unpause_customer->cu_company_name_business . " (" . $unpause_customer->ph_cu_id . ") have reached the end of their requested pause! The code should have automatically processed this for you, please check if all their portals are active again.");

            $this->storePauseHistory($unpause_customer, CustomerPairStatus::PAUSE);

            //Get paused portals and activate them
            DB::table('kt_customer_portal')
                ->where("ktcupo_cu_id", $unpause_customer->ph_cu_id)
                ->where("ktcupo_status", CustomerPairStatus::PAUSE)
                ->update(
                    [
                        'ktcupo_status' => CustomerPairStatus::ACTIVE
                    ]
                );

        }

        //Perform Pause-code
        foreach ($to_be_paused as $pause_customer)
        {
            $system->sendMessage([1778, 4669, 4677], "Automatic pause has been triggered", $pause_customer->cu_company_name_business . " (" . $pause_customer->ph_cu_id . ") has requested a pause for today! The code should have automatically processed this for you, please check if all their portals are on pause.");

            $this->storePauseHistory($pause_customer, CustomerPairStatus::ACTIVE);

            //Get active portals and pause them
            DB::table('kt_customer_portal')
                ->where("ktcupo_cu_id", $pause_customer->ph_cu_id)
                ->where("ktcupo_status", CustomerPairStatus::ACTIVE)
                ->update(
                    [
                        'ktcupo_status' => CustomerPairStatus::PAUSE
                    ]
                );

        }

        /**
         * Part for Customers who will be activated in the next 48 hours AND total days of pause longer than 10 days
         */
        $to_be_unpaused_in_48_hours = PauseHistory::join("customers", "cu_id", "ph_cu_id")
            ->where("ph_end_date", date("Y-m-d", strtotime(date("Y-m-d") . ' + 2 DAY')))
            ->where("ph_total_days", ">", 10)
            ->get();

        foreach ($to_be_unpaused_in_48_hours as $unpause_48_hour_customer)
        {
            $fields = [
                'to_email' => $unpause_48_hour_customer->cu_email,
                'date_requested' => date("Y-m-d", strtotime($unpause_48_hour_customer->ph_created_timestamp)),
                'total_days' => $unpause_48_hour_customer->ph_total_days,
                'end_date' => $unpause_48_hour_customer->ph_end_date
            ];

            $mail->send("unpause_in_48_hours", $unpause_48_hour_customer->cu_la_code, $fields);
        }
    }
}
