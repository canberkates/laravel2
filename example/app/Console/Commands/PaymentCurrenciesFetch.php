<?php

namespace App\Console\Commands;

use App\Cronjobs\CronjobController;
use App\Functions\Data;
use App\Functions\DataIndex;
use App\Functions\Mail;
use App\Functions\Mover;
use App\Functions\System;
use App\Models\Cronjob;
use App\Models\PaymentCurrency;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class PaymentCurrenciesFetch extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronjob:fetch_payment_currencies';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    private $cc;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CronjobController $cc)
    {
        $this->cc = $cc;
        parent::__construct();
    }


    const CRONJOB_ID = 20;
    const CRONJOB_TASK = 'Finance';
    const CRONJOB_SUBTASK = 'Fetch Payment Rates';
    const CRONJOB_TRIGGER = 10;
    const CRONJOB_INTERVAL = "60 MINUTE";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $system = new System();

        //Set Cronjob check values
        $CRONJOB_USERS = DataIndex::financeUsers();

        try
        {
            $this->getCurrenciesFromExternalSource(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, $CRONJOB_USERS, self::CRONJOB_INTERVAL, $this->cc, $system);

        } catch (\Throwable $e){
            $system->sendMessage(3653, "Failed to fetch currencies", $e);
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 1, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, $e);
        }
    }

    /**
     * @param int $CRONJOB_ID
     * @param string $CRONJOB_TASK
     * @param string $CRONJOB_SUBTASK
     * @param int $CRONJOB_TRIGGER
     * @param array $CRONJOB_USERS
     * @param string $CRONJOB_INTERVAL
     * @return void
     */
    private function getCurrenciesFromExternalSource(int $CRONJOB_ID, string $CRONJOB_TASK, string $CRONJOB_SUBTASK, int $CRONJOB_TRIGGER, array $CRONJOB_USERS, string $CRONJOB_INTERVAL, CronjobController $cc , System $system): void
    {

        $query = PaymentCurrency::all();
        $payment_currencies = [];

        foreach ($query as $row)
        {
            $payment_currencies[] = $row->pacu_code;
        }

        $result = json_decode(System::getExternal("https://apilayer.net/api/live?access_key=" . $system->getSetting("currencylayer_key") . "&source=EUR&currencies=" . implode(",", $payment_currencies) . "&format=1"), true);

        $this->checkCurrencyResult($result, $cc, $CRONJOB_ID, $CRONJOB_TASK, $CRONJOB_SUBTASK, $CRONJOB_TRIGGER, $CRONJOB_USERS, $CRONJOB_INTERVAL, $system);

    }

    /**
     * @param $result
     * @param CronjobController $cc
     * @param int $CRONJOB_ID
     * @param string $CRONJOB_TASK
     * @param string $CRONJOB_SUBTASK
     * @param int $CRONJOB_TRIGGER
     * @param array $CRONJOB_USERS
     * @param string $CRONJOB_INTERVAL
     * @param System $system
     */
    private function checkCurrencyResult($result, CronjobController $cc, int $CRONJOB_ID, string $CRONJOB_TASK, string $CRONJOB_SUBTASK, int $CRONJOB_TRIGGER, array $CRONJOB_USERS, string $CRONJOB_INTERVAL, System $system): void
    {
        if (isset($result['quotes']))
        {
            foreach ($result['quotes'] as $currency => $rate)
            {
                $currency = substr($currency, 3, 6);
                $payment_currencies = PaymentCurrency::where("pacu_code", $currency)->first();

                //If payment currency is suddenly extremely high or low, log error
                if ($payment_currencies->pacu_rate <= ($rate * .75) || $payment_currencies->pacu_rate >= ($rate * 1.25))
                {
                    $cc->CronjobWatchdog($CRONJOB_ID, $CRONJOB_TASK, $CRONJOB_SUBTASK, $CRONJOB_TRIGGER, 1, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), $CRONJOB_INTERVAL, $result);
                    $system->sendMessage(3653, "Not updating currency because it's extremely high or low", System::serialize($result));
                } else
                {
                    DB::table('payment_currencies')
                        ->where('pacu_code', $currency)
                        ->update(
                            [
                                'pacu_rate' => $rate
                            ]
                        );
                }
            }
            //All went well, reset error count to 0
            $cc->CronjobWatchdog($CRONJOB_ID, $CRONJOB_TASK, $CRONJOB_SUBTASK, $CRONJOB_TRIGGER, 0, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), $CRONJOB_INTERVAL, $result);

        }
        else
        {
            //No response so log error
            $system->sendMessage(3653, "Failed to fetch currencies", System::serialize($result));
            $cc->CronjobWatchdog($CRONJOB_ID, $CRONJOB_TASK, $CRONJOB_SUBTASK, $CRONJOB_TRIGGER, 1, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), $CRONJOB_INTERVAL, $result);
        }
    }

}

