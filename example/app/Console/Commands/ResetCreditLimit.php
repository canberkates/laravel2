<?php

namespace App\Console\Commands;

use App\Cronjobs\CronjobController;
use App\Models\Customer;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ResetCreditLimit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'checker:reset_credit_limit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cronjob that reset the increased credit limit to the old one.';

    private $cc;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CronjobController $cc)
    {
        $this->cc = $cc;
        parent::__construct();
    }

    const CRONJOB_ID = 26;
    const CRONJOB_TASK = 'Finance';
    const CRONJOB_SUBTASK = 'Reset Credit Limit';
    const CRONJOB_TRIGGER = 1;
    const CRONJOB_INTERVAL = "1 DAY";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        //Set Cronjob check values
        $CRONJOB_USERS = [3653];

        try
        {
            $this->resetCreditLimit();
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 0, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, null);

        }catch (\Throwable $e){
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 1, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, $e);
        }

    }

    private function resetCreditLimit(): void
    {
        //Get all customers with increase timestamp today (when it needs to go back to the old Credit Limit)
        $customers = Customer::where("cu_credit_limit_increase_timestamp", date("y-m-d"))->get();

        //Loop through customers
        foreach ($customers as $customer)
        {
            $customer->cu_credit_limit = $customer->cu_credit_limit_original;
            $customer->cu_credit_limit_increase_timestamp = null;
            $customer->cu_credit_limit_original = null;

            $customer->save();
        }
    }
}
