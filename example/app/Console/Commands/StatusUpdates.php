<?php

namespace App\Console\Commands;

use App\Cronjobs\CronjobController;
use App\Functions\DataIndex;
use App\Functions\System;
use App\Models\StatusHistory;
use App\Models\StatusUpdate;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class StatusUpdates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronjob:status_updates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    private $cc;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CronjobController $cc)
    {
        $this->cc = $cc;
        parent::__construct();
    }


    const CRONJOB_ID = 34;
    const CRONJOB_TASK = 'Phoenix';
    const CRONJOB_SUBTASK = 'Update Customer Statuses';
    const CRONJOB_TRIGGER = 1;
    const CRONJOB_INTERVAL = "60 MINUTE";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        //Set Cronjob check values
        $CRONJOB_USERS = [3653];

        try
        {
            $this->updateStatuses();
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 0, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, null);

        }catch (\Throwable $e){
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 1, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, $e);
        }


    }

    private function updateStatuses(): void
    {
        $system = new System();
        $dataindex = new DataIndex();

        $query = StatusUpdate::leftJoin("kt_customer_portal", "stup_ktcupo_id", "ktcupo_id")
            ->leftJoin("customers", "ktcupo_cu_id", "cu_id")
            ->leftJoin("portals", "ktcupo_po_id", "po_id")
            ->where("stup_processed", 0)
            ->where("cu_deleted", 0)
            ->whereRaw("`status_updates`.`stup_date` <= NOW()")
            ->get();

        $query = $system->databaseToArray($query);

        foreach ($query as $row)
        {
            if ((date("Y-m-d") == $row['stup_date'] && $row['stup_hour'] <= date("G")) || $row['stup_date'] < date("Y-m-d"))
            {
                DB::table('kt_customer_portal')
                    ->where('ktcupo_id', $row['ktcupo_id'])
                    ->update(
                        [
                            'ktcupo_status' => $row['stup_status']
                        ]
                    );

                $status_history = new StatusHistory();
                $status_history->sthi_timestamp = date("Y-m-d H:i:s");
                $status_history->sthi_type = 2;
                $status_history->sthi_us_id = $row['stup_us_id'];
                $status_history->sthi_cu_id = $row['cu_id'];
                $status_history->sthi_ktcupo_id = $row['ktcupo_id'];
                $status_history->sthi_status_from = $row['ktcupo_status'];
                $status_history->sthi_status_to = $row['stup_status'];
                $status_history->save();

                if (!empty($row['stup_users']))
                {
                    $users = explode(",", $row['stup_users']);

                    $subject = "Status update";
                    $message = "The status of customer <strong>" . $row['cu_company_name_business'] . " - " . $row['po_portal'] . " (" . $dataindex->requestTypes()[$row['ktcupo_request_type']] . ") - " . $row['ktcupo_description'] . "</strong> has been changed from <strong>" . $dataindex->customerPairStatuses()[$row['ktcupo_status']] . "</strong> to <strong>" . $dataindex->customerPairStatuses()[$row['stup_status']] . "</strong>.";

                    foreach ($users as $user)
                    {
                        $system->sendMessage($user, $subject, $message);
                    }
                }

                DB::table('status_updates')
                    ->where('stup_id', $row['stup_id'])
                    ->update(
                        [
                            'stup_processed' => 1
                        ]
                    );
            }
        }
    }
}

