<?php

namespace App\Console\Commands;

use App\Cronjobs\CronjobController;
use App\Functions\Teams;
use App\Models\User;
use Carbon\Carbon;
use DateTime;
use Illuminate\Console\Command;

class BirthdayChecker extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'checker:birthday';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cronjob which runs every day to send a message to the team channel with birthdays';

    private $cc;
    private $teams;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CronjobController $cc, Teams $teams)
    {
        $this->cc = $cc;
        $this->teams = $teams;
        parent::__construct();
    }

    //Set Cronjob check values
    const CRONJOB_ID = 4;
    const CRONJOB_TASK = 'Organization';
    const CRONJOB_SUBTASK = 'Check for birthdays';
    const CRONJOB_TRIGGER = 3;
    const CRONJOB_INTERVAL = "1 DAY";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Set Cronjob check values
        $CRONJOB_USERS = [3653];

        try
        {
            $this->sendBirthdayMessage();
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 0, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, null);

        }catch (\Throwable $e){
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 1, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, $e);
        }
    }

    private function sendBirthdayMessage(): void
    {

        $date = date("m-d");
        $users = User::whereRaw("`us_date_of_birth` LIKE '%" . $date . "'")
            ->where("us_is_deleted", 0)
            ->where("id", "!=", 4681)
            ->where("id", "!=", 4679)
            ->get();

        if (count($users) > 0)
        {
            $text = "";
            foreach ($users as $user)
            {
                $date = new DateTime($user->us_date_of_birth);
                $year_birthday = $date->format('Y');
                $year_now = date("Y");
                $count = $year_now - $year_birthday;

                $text .= "<span style='color: lawngreen;'>" . $user->us_name . "</span> turned " . (($count > 30) ? "30+" : $count) . " years old! 👏<br />";
            }

            $webhook_url = "https://triglobalbv.webhook.office.com/webhookb2/6584d331-fbe4-473c-861b-8b792ed79d7a@21938c5d-879a-4eef-996a-ac494c9850a6/IncomingWebhook/7239ec19cf964ab682ef13581e0269d8/b0777516-6c48-40f4-801e-47b8a48bc8cd";
            $title = '🎉&#127880; Congratulations! &#127880;🎉';

            $this->teams->sendMessage($webhook_url, $title, $text);
        }
    }
}
