<?php

namespace App\Console\Commands;

use App\Cronjobs\CronjobController;
use App\Data\AdyenPaymentStatus;
use App\Data\PaymentMethod;
use App\Functions\AdyenTransaction;
use App\Functions\Mail;
use App\Functions\Mover;
use App\Functions\System;
use App\Models\AdyenCardDetails;
use App\Models\AdyenPayment;
use App\Models\Customer;
use App\Models\PaymentCurrency;
use Carbon\Carbon;
use Illuminate\Console\Command;

class PrepaymentBatch extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'batch:prepayment';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get all customers with payment method 5 & 6 and check if they have less than 100E balance, if they have, than charge credit card or make auto debit file.';

    private $cc;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CronjobController $cc)
    {
        $this->cc = $cc;
        parent::__construct();
    }


    const CRONJOB_ID = 21;
    const CRONJOB_TASK = 'Finance';
    const CRONJOB_SUBTASK = 'Prepayment Batch';
    const CRONJOB_TRIGGER = 1;
    const CRONJOB_INTERVAL = "1 WEEKDAY";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        //Set Cronjob check values
        $CRONJOB_USERS = [3653];

        try
        {
            $this->checkCustomers();
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 0, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, null);

        }catch (\Throwable $e){
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 1, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, $e);
        }


    }

    private function checkCustomers(): void
    {
        $mail = new Mail();

        $date_now = date("Y-m-d H:i:s");
        $date_3_days_ago = date("Y-m-d", strtotime(date("Y-m-d", strtotime("-3 day"))));

        $customers = Customer::whereIn("cu_payment_method", [PaymentMethod::PREPAYMENT_AUTO_CHARGE_CREDIT_CARD])
            ->where("cu_pp_monthly_budget", "!=", "")
            ->where("cu_pp_charge_threshold", "!=", "")
            ->whereRaw("(`cu_pp_charge_timestamp` NOT BETWEEN '".$date_3_days_ago."' AND '".$date_now."' OR cu_pp_charge_timestamp IS NULL)")
            ->whereRaw("(cu_pp_charge_credit_card_times != 2 OR cu_pp_charge_credit_card_times IS NULL)")
            ->get();

        $mover = new Mover();
        $system = new System();

        foreach ($customers as $customer)
        {
            //Check if customer is active
            if ($system->getStatus($customer->cu_id) != "Active")
            {
                continue;
            }

            //Get current balances of customer
            $balance_total = $mover->customerBalancesFC([$customer->cu_id])[$customer->cu_id];

            if ($balance_total < $customer->cu_pp_charge_threshold)
            {
                if ($customer->cu_payment_method == PaymentMethod::PREPAYMENT_AUTO_CHARGE_CREDIT_CARD)
                {
                    //Charge credit card automatically

                    $adcade_id = AdyenCardDetails::where("adcade_cu_id", $customer->cu_id)
                        ->where("adcade_enabled", 1)
                        ->where("adcade_removed", 0)
                        ->first();

                    $pacu_token = PaymentCurrency::where("pacu_code", $customer->cu_pacu_code)->first()->pacu_token;

                    if (!empty($adcade_id->adcade_id))
                    {
                        $result = AdyenTransaction::insertCreditCardPayment($adcade_id->adcade_id, $customer->cu_pp_monthly_budget, "CCCHARGE");
                        AdyenTransaction::makeCreditCardPayment($result['adcade'], $result['adpa_id'], $result['adpa_merchant_reference'], $customer->cu_pp_monthly_budget);

                        $card_details = AdyenPayment::where("adpa_id", $result['adpa_id'])->first();

                        $last_4_digits_cc = $adcade_id->adcade_card_last_digits;

                        if ($card_details->adpa_status != AdyenPaymentStatus::PROCESSING && $card_details->adpa_status != AdyenPaymentStatus::FAILED && $card_details->adpa_status != 3)
                        {
                            //Payment success
                            $customer->cu_pp_charge_credit_card_times = 0;
                            $customer->cu_pp_charge_timestamp = date("Y-m-d H:i:s");

                            $balance_after_charged = $balance_total + $customer->cu_pp_monthly_budget;

                            //Send mail of successfull charge

                            $fields = [
                                "email" => ((!empty($customer->cu_email_bi)) ? $customer->cu_email_bi : $customer->cu_email),
                                "balance" => $pacu_token . " " . number_format($balance_after_charged, 2),
                                "amount" => $pacu_token . " " . $customer->cu_pp_monthly_budget,
                                "last_4_digits_cc" => $last_4_digits_cc
                            ];

                            $mail->send("prepaymentbatch_charged_successfully", $customer->cu_la_code, $fields, null, false);
                        } else
                        {
                            //Payment failed
                            $customer->cu_pp_charge_timestamp = date("Y-m-d H:i:s");
                            $customer->cu_pp_charge_credit_card_times = $customer->cu_pp_charge_credit_card_times + 1;

                            $remaining_balance = $balance_total;

                            //Send mail of failed charge
                            if ($customer->cu_pp_charge_credit_card_times == 2)
                            {
                                $fields = [
                                    "email" => ((!empty($customer->cu_email_bi)) ? $customer->cu_email_bi : $customer->cu_email),
                                    "remaining_balance" => $pacu_token . " " . number_format($remaining_balance, 2),
                                    "last_4_digits_cc" => $last_4_digits_cc,
                                    "reason" => '"' . $card_details->adpa_refusal_reason . '"'
                                ];

                                $mail->send("prepaymentbatch_charge_declined", $customer->cu_la_code, $fields, null, false);
                            } else
                            {

                                $fields = [
                                    "email" => ((!empty($customer->cu_email_bi)) ? $customer->cu_email_bi : $customer->cu_email),
                                    "remaining_balance" => $pacu_token . " " . number_format($remaining_balance, 2),
                                    "amount" => $pacu_token . " " . $customer->cu_pp_monthly_budget,
                                    "last_4_digits_cc" => $last_4_digits_cc,
                                    "reason" => '"' . $card_details->adpa_refusal_reason . '"'
                                ];

                                $mail->send("prepaymentbatch_charge_failed", $customer->cu_la_code, $fields, null, false);
                            }

                            if ($customer->cu_pp_charge_credit_card_times == 2)
                            {
                                $system->sendMessage([4186, 4560, 4459], "Charge Prepayment Credit card FAILED - (" . $customer->cu_pp_charge_credit_card_times . " time(s))", "This was the last try of the cronjob to charge the CC. Finance has to handle this charge manual: CU ID = " . $customer->cu_id);
                            } else
                            {
                                $system->sendMessage([4186, 4560, 4459], "Charge Prepayment Credit card FAILED - (" . $customer->cu_pp_charge_credit_card_times . " time(s))", "Tried to charge the credit card of a customer (CU ID = " . $customer->cu_id . "), but its failed.");
                            }
                        }
                        $customer->save();
                    } else
                    {
                        //Send message to finance?
                        $system->sendMessage([4186, 4560, 4459], "Charge Prepayment Credit card", "Tried to charge the credit card of a customer (CU ID = " . $customer->cu_id . "), but it looks like this customer has not a credit card.");
                    }
                }
            }
        }
    }
}
