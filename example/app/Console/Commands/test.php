<?php

namespace App\Console\Commands;

use App\Data\WebsiteValidateString;
use App\Data\WebsiteValidateType;
use App\Functions\Data;
use App\Functions\Mover;
use App\Functions\System;
use App\Http\Controllers\AjaxController;
use App\Models\ContactPerson;
use App\Models\Customer;
use App\Models\CustomerGatheredReview;
use App\Models\CustomerReviewScore;
use App\Models\KTCustomerGatheredReview;
use App\Models\KTCustomerProgress;
use App\Models\MobilityexFetchedData;
use App\Models\MoverData;
use App\Models\PauseDay;
use App\Models\PauseHistory;
use Illuminate\Console\Command;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class test extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronjob:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Onze 1e cronjob om wat shit te testen. Lekker zeg. Keurig.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "TEST";
    }

}
