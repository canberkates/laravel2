<?php

namespace App\Console\Commands;


use App\Cronjobs\CronjobController;
use App\Functions\Mail;
use App\Functions\System;
use App\Models\Request;
use Carbon\Carbon;
use DateTime;
use Illuminate\Console\Command;

class RequestsChecker extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronjob:requests_checker';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    private $cc;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CronjobController $cc)
    {
        $this->cc = $cc;
        parent::__construct();
    }


    const CRONJOB_ID = 25;
    const CRONJOB_TASK = 'Matching';
    const CRONJOB_SUBTASK = 'Check if we still receive leads';
    const CRONJOB_TRIGGER = 3;
    const CRONJOB_INTERVAL = "15 MINUTE";


    public function handle()
    {

        //Set Cronjob check values
        $CRONJOB_USERS = [3653];

        try
        {
            $this->sendRequestEmails();
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 0, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, null);

        }catch (\Throwable $e){
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 1, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, $e);
        }


    }

    private function sendRequestEmails(): void
    {
        $system = new System();
        $mail = new Mail();

        $sea_check = false;
        $seo_check = false;
        $affiliates_check = false;

        /*
         * First check if we received any SEO leads in last 30 minutes
         * If not? Then send mail that we didn't receive any SEA/SEO leads in the last 30 minutes.
         */
        $query_seo = Request::select("re_timestamp")
            ->leftJoin("website_forms", "re_wefo_id", "wefo_id")
            ->leftJoin("websites", "wefo_we_id", "we_id")
            ->whereRaw("`re_timestamp` BETWEEN NOW() - INTERVAL 2 HOUR AND NOW()")
            ->where("re_source", 1)
            ->where("we_sirelo", 1)
            ->orderBy("re_timestamp", "DESC")
            ->first();

        $query_seo_last = Request::select("re_timestamp", "we_website")
            ->leftJoin("website_forms", "re_wefo_id", "wefo_id")
            ->leftJoin("websites", "wefo_we_id", "we_id")
            ->where("re_source", 1)
            ->where("we_sirelo", 1)
            ->orderBy("re_timestamp", "DESC")
            ->limit(5)
            ->get();

        $query_seo_last = $system->databaseToArray($query_seo_last);

        if (count($query_seo) == 0)
        {
            $seo_check = true;
        }

        /*
         * First check if we received any SEA leads in last 30 minutes
         * If not? Then send mail that we didn't receive any SEA/SEO leads in the last 30 minutes.
        */
        $query_sea = Request::select("re_timestamp")
            ->leftJoin("website_forms", "re_wefo_id", "wefo_id")
            ->leftJoin("websites", "wefo_we_id", "we_id")
            ->whereRaw("`re_timestamp` BETWEEN NOW() - INTERVAL 2 HOUR AND NOW()")
            ->where("re_source", 1)
            ->where("we_sirelo", 0)
            ->orderBy("re_timestamp", "DESC")
            ->first();

        $query_sea_last = Request::select("re_timestamp", "we_website")
            ->from("requests")
            ->leftJoin("website_forms", "re_wefo_id", "wefo_id")
            ->leftJoin("websites", "wefo_we_id", "we_id")
            ->where("re_source", 1)
            ->where("we_sirelo", 0)
            ->orderBy("re_timestamp", "DESC")
            ->limit(5)
            ->get();

        $query_sea_last = $system->databaseToArray($query_sea_last);

        if (count($query_sea) == 0)
        {
            $sea_check = true;
        }

        /*
         * Check if we received any AFFILIATES leads in last 30 minutes
         * If not? Then send mail that we didn't receive any AFFILIATES leads in the last 30 minutes.
         */
        $query_affiliates = Request::select("re_timestamp")
            ->whereRaw("`re_timestamp` BETWEEN NOW() - INTERVAL 2 HOUR AND NOW()")
            ->where("re_source", 2)
            ->orderBy("re_timestamp", "DESC")
            ->limit(1)
            ->first();

        $query_affiliates_last = Request::select("re_timestamp", "afpafo_name")
            ->leftJoin("affiliate_partner_forms", "re_afpafo_id", "afpafo_id")
            ->where("re_source", 2)
            ->orderBy("re_timestamp", "DESC")
            ->limit(5)
            ->get();

        $query_affiliates_last = $system->databaseToArray($query_affiliates_last);

        if (count($query_affiliates) == 0)
        {
            $affiliates_check = true;
        }

        $time_now = date("H:i");

        if ($seo_check)
        {
            $seo_string = "";

            foreach ($query_seo_last as $last)
            {
                $date = new DateTime($last['re_timestamp']);

                $seo_string .= $last['we_website'] . " (Time: " . $date->format("H:i") . ")<br />";
            }

            $fields = [
                'seo_check' => true,
                'type' => 'SEO',
                'last_leads' => $seo_string
            ];

            if ($time_now >= "07:00" && $time_now <= "23:00")
            {
                $mail->send("requests_checker", "EN", $fields);
            }

        }

        if ($sea_check)
        {
            $sea_string = "";

            foreach ($query_sea_last as $last)
            {
                $date = new DateTime($last['re_timestamp']);

                $sea_string .= $last['we_website'] . " (Time: " . $date->format("H:i") . ")<br />";
            }

            $fields = [
                'sea_check' => true,
                'type' => 'SEA',
                'last_leads' => $sea_string
            ];

            $mail->send("requests_checker", "EN", $fields);
        }

        if ($affiliates_check)
        {
            $affiliate_string = "";

            foreach ($query_affiliates_last as $last)
            {
                $date = new DateTime($last['re_timestamp']);

                $affiliate_string .= $last['afpafo_name'] . " (Time: " . $date->format("H:i") . ")<br />";
            }

            $fields = [
                'affiliates_check' => true,
                'type' => 'AFFILIATE',
                'last_leads' => $affiliate_string
            ];

            $mail->send("requests_checker", "EN", $fields);
        }
    }
}

