<?php

namespace App\Console\Commands;

use App\Cronjobs\CronjobController;
use App\Functions\Mail;
use App\Functions\System;
use App\Models\Customer;
use App\Models\Request;
use App\Models\Survey1;
use Carbon\Carbon;
use Illuminate\Console\Command;

class Survey1Invites extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronjob:survey_1_invites';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    private $cc;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CronjobController $cc)
    {
        $this->cc = $cc;
        parent::__construct();
    }


    const CRONJOB_ID = 36;
    const CRONJOB_TASK = 'Mailing';
    const CRONJOB_SUBTASK = 'Send Survey 1 Invites';
    const CRONJOB_TRIGGER = 3;
    const CRONJOB_INTERVAL = "60 MINUTE";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Set Cronjob check values
        $CRONJOB_USERS = [3653];

        try
        {
            $this->sendInvites();
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 0, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, null);

        }catch (\Throwable $e){
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 1, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, $e);
        }


    }

    private function sendInvites(): void
    {
        $system = new System();
        $mail = new Mail();

        $excluded_customers = [];
        $excl_customers = Customer::select("cu_id")
            ->leftJoin("mover_data", "moda_cu_id", "cu_id")
            ->where("moda_quality_score_override", "-2")
            ->get();

        foreach ($excl_customers as $customer)
        {
            $excluded_customers[$customer->cu_id] = $customer->cu_id;
        }

        $query = Request::leftJoin("kt_request_customer_portal", "ktrecupo_re_id", "re_id")
            ->whereRaw("`re_timestamp` BETWEEN '" . date("Y-m-d H:i:s", strtotime(date("Y-m-d", strtotime("-5 days")) . " 00:00:00")) . "' AND '" . date("Y-m-d H:i:s", strtotime(date("Y-m-d", strtotime("-5 days")) . " 23:59:59")) . "'")
            ->where("re_status", 1)
            ->whereRaw("HOUR(`requests`.`re_timestamp`) <= HOUR(NOW())")
            ->get();

        $query = $system->databaseToArray($query);

        foreach ($query as $row)
        {
            if (!in_array($row['ktrecupo_cu_id'], $excluded_customers))
            {
                $query_survey = Survey1::select("su_id")->where("su_re_id", $row['re_id'])->get();

                if (count($query_survey) == 0)
                {
                    $survey_1 = new Survey1();
                    $survey_1->su_re_id = $row['re_id'];
                    $survey_1->su_timestamp = date("Y-m-d H:i:s");
                    $survey_1->save();

                    $fields = [
                        "request" => $row,
                    ];

                    $mail->send("survey_1_invite", $row['re_la_code'], $fields);
                }
            }
        }
    }
}

