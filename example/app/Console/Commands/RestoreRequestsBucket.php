<?php

namespace App\Console\Commands;


use App\Cronjobs\CronjobController;
use App\Functions\System;
use App\Models\Customer;
use App\Models\KTCustomerPortal;
use App\Models\MoverData;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class RestoreRequestsBucket extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronjob:restore_requests_bucket';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    private $cc;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CronjobController $cc)
    {
        $this->cc = $cc;
        parent::__construct();
    }


    const CRONJOB_ID = 29;
    const CRONJOB_TASK = 'Matching';
    const CRONJOB_SUBTASK = 'Restore Daily Request Buckets';
    const CRONJOB_TRIGGER = 1;
    const CRONJOB_INTERVAL = "1 DAY";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Set Cronjob check values
        $CRONJOB_USERS = [3653];

        try
        {
            $this->restoreRequestBucketValues();
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 0, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, null);

        }catch (\Throwable $e){
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 1, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, $e);
        }


    }

    private function restoreRequestBucketValues(): void
    {
        $system = new System();

        $query_customers = Customer::select("cu_id")
            ->where("cu_deleted", 0)
            ->get();

        $query_customers = $system->databaseToArray($query_customers);

        foreach ($query_customers as $row_customers)
        {
            //When restoring, check if the value of the request bucket is higher than expected

            //First get all customer portals:
            $portals = KTCustomerPortal::select("ktcupo_id", "ktcupo_daily_average", "ktcupo_requests_bucket")
                ->where("ktcupo_cu_id", $row_customers['cu_id'])
                ->get();

            $portals = $system->databaseToArray($portals);

            foreach ($portals as $portal)
            {
                $daily_average = $portal['ktcupo_daily_average'];
                $bucket = $portal['ktcupo_requests_bucket'];

                $new_bucket_value = $bucket > $daily_average ? $bucket - $daily_average : 0;

                DB::table('kt_customer_portal')
                    ->where('ktcupo_id', $portal['ktcupo_id'])
                    ->update(
                        [
                            'ktcupo_requests_bucket' => $new_bucket_value
                        ]
                    );
            }

            //Get mover capping
            $mover_capping = MoverData::select("moda_capping_bucket", "moda_capping_daily_average")
                ->where("moda_cu_id", $row_customers['cu_id'])
                ->first();

            if (count($mover_capping) > 0)
            {

                $daily_average = $mover_capping->moda_capping_daily_average;
                $bucket = $mover_capping->moda_capping_bucket;

                $new_bucket_value = $bucket > $daily_average ? $bucket - $daily_average : 0;

                DB::table('mover_data')
                    ->where('moda_cu_id', $row_customers['cu_id'])
                    ->update(
                        [
                            'moda_capping_bucket' => $new_bucket_value
                        ]
                    );
            }
        }

        $query_customers_check = Customer::select("cu_id")
            ->where("cu_deleted", 0)
            ->get();

        $query_customers_check = $system->databaseToArray($query_customers_check);

        $message = "";
        $bucket_message_cu_ids = "";
        $send_message = false;
        $send_cappingbucket = false;

        foreach ($query_customers_check as $row_customers)
        {
            //First get all customer portals:
            $portals = KTCustomerPortal::select("ktcupo_id", "ktcupo_daily_average", "ktcupo_requests_bucket")
                ->where("ktcupo_cu_id", $row_customers['cu_id'])
                ->get();

            $portals = $system->databaseToArray($portals);

            $portal_message = false;
            $portal_message_text = "";

            foreach ($portals as $portal)
            {
                $bucket = $portal['ktcupo_requests_bucket'];

                if ($bucket != 0)
                {
                    $send_message = true;
                    $portal_message = true;
                    $portal_message_text .= $portal['ktcupo_id'] . ", ";
                }
            }

            //Get mover capping
            $mover_capping = MoverData::select("moda_capping_bucket", "moda_capping_daily_average")
                ->where("moda_cu_id", $row_customers['cu_id'])
                ->first();

            if (count($mover_capping) > 0)
            {
                $bucket_capping = $mover_capping->moda_capping_bucket;

                if ($bucket_capping != 0)
                {
                    $send_message = true;
                    $send_cappingbucket = true;

                    $bucket_message_cu_ids .= $row_customers['cu_id'] . ", ";
                }
            }
        }

        if ($portal_message && !empty($portal_message_text))
        {
            $message .= "Bucket is 0 for portals (ktcupo_id): " . $portal_message_text;

            if ($send_cappingbucket)
            {
                $message .= " - Bucket is 0 for capping bucket (cu_id): " . $bucket_message_cu_ids;
            }
        } else
        {
            if ($send_cappingbucket)
            {
                $message .= "Bucket is 0 for capping bucket (cu_id): " . $bucket_message_cu_ids;
            }
        }


        if (!empty($message) && $send_message)
        {
            //Send message to Lorenzo and Arjan
            $system->sendMessage([4186, 3653], "Restore Request Bucket ALERT", $system->serialize($message));
        }
    }
}

