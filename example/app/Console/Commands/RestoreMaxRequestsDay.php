<?php

namespace App\Console\Commands;

use App\Cronjobs\CronjobController;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class RestoreMaxRequestsDay extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronjob:restore_max_requests_day';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    private $cc;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CronjobController $cc)
    {
        $this->cc = $cc;
        parent::__construct();
    }

    const CRONJOB_ID = 27;
    const CRONJOB_TASK = 'Matching';
    const CRONJOB_SUBTASK = 'Restore Max Daily Requests';
    const CRONJOB_TRIGGER = 1;
    const CRONJOB_INTERVAL = "1 DAY";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Set Cronjob check values
        $CRONJOB_USERS = [3653];

        try
        {
            $this->restoreDailyMaxRequests();
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 0, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, null);

        }catch (\Throwable $e){
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 1, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, $e);
        }


    }

    private function restoreDailyMaxRequests(): void
    {
        DB::table('kt_customer_portal')
            ->update(
                [
                    'ktcupo_max_requests_day_left' => DB::raw("ktcupo_max_requests_day")
                ]
            );

        DB::table('kt_customer_question')
            ->update(
                [
                    'ktcuqu_max_requests_day_left' => DB::raw("ktcuqu_max_requests_day")
                ]
            );

        DB::table('mover_data')
            ->update(
                [
                    'moda_forced_daily_capping_left' => DB::raw("moda_forced_daily_capping")
                ]
            );

        DB::table('affiliate_partner_data')
            ->update(
                [
                    'afpada_max_requests_day_left' => DB::raw("afpada_max_requests_day")
                ]
            );
    }
}

