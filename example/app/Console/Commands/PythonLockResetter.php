<?php

namespace App\Console\Commands;

use App\Cronjobs\CronjobController;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class PythonLockResetter extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronjob:lock_reset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cronjob to remove the locks from records.';

    private $cc;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CronjobController $cc)
    {
        $this->cc = $cc;
        parent::__construct();
    }

    const CRONJOB_ID = 22;
    const CRONJOB_TASK = 'Validation';
    const CRONJOB_SUBTASK = 'Reset Python Locks';
    const CRONJOB_TRIGGER = 3;
    const CRONJOB_INTERVAL = "1 DAY";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        //Set Cronjob check values
        $CRONJOB_USERS = [3653];

        try
        {
            $this->resetLocks();
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 0, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, null);

        }catch (\Throwable $e){
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 1, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, $e);
        }



    }

    private function resetLocks(): void
    {
        DB::table('customer_review_scores')
            ->whereNotNull('curesc_lock_user_id')
            ->update(
                [
                    'curesc_lock_user_id' => null,
                    'curesc_lock_timestamp' => null,
                ]
            );

        DB::table('mobilityex_fetched_data')
            ->whereNotNull('mofeda_lock_user_id')
            ->update(
                [
                    'mofeda_lock_user_id' => null,
                    'mofeda_lock_timestamp' => null,
                ]
            );

        //Remove wait timestamp (lock for gathering customers)

        DB::table('customer_review_scores')
            ->where('curesc_wait_timestamp', "<=", date("Y-m-d") . " 23:59:59")
            ->update(
                [
                    'curesc_wait_timestamp' => null,
                ]
            );
    }
}
