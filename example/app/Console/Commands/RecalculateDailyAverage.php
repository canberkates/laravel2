<?php

namespace App\Console\Commands;

use App\Cronjobs\CronjobController;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class RecalculateDailyAverage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronjob:recalculate_daily_average';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    private $cc;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CronjobController $cc)
    {
        $this->cc = $cc;
        parent::__construct();
    }


    const CRONJOB_ID = 23;
    const CRONJOB_TASK = 'Matching';
    const CRONJOB_SUBTASK = 'Recalculate Daily Average';
    const CRONJOB_TRIGGER = 1;
    const CRONJOB_INTERVAL = "1 MONTH";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Set Cronjob check values
        $CRONJOB_USERS = [3653];

        try
        {
            $this->recalculateDailyAverage();
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 0, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, null);

        }catch (\Throwable $e){
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 1, $CRONJOB_USERSS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, $e);
        }



    }

    private function recalculateDailyAverage(): void
    {
        DB::table('kt_customer_portal')
            ->whereRaw("`ktcupo_daily_average` != (`ktcupo_max_requests_month` / 30.5)")
            ->where("ktcupo_max_requests_month", "!=", 0)
            ->update(
                [
                    'ktcupo_daily_average' => DB::raw("(`ktcupo_max_requests_month` / 30.5)"),
                    'ktcupo_requests_bucket' => 0,
                ]
            );

        DB::table('mover_data')
            ->whereRaw("`moda_capping_daily_average` != ((`moda_capping_monthly_limit` + `moda_capping_monthly_limit_modifier`) / 30.5)")
            ->whereIn("moda_capping_method", [1, 2])
            ->update(
                [
                    'moda_capping_bucket' => 0,
                    'moda_capping_daily_average' => DB::raw("((`moda_capping_monthly_limit` + `moda_capping_monthly_limit_modifier`) / 30.5)")
                ]
            );
    }
}

