<?php

namespace App\Console\Commands;

use App\Cronjobs\CronjobController;
use App\Functions\Mail;
use App\Functions\System;
use App\Models\KTCustomerProgress;
use App\Models\PauseDay;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Log;

class PauseDaysReset extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronjob:reset_pause_days';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cronjob that runs 1 per year to reset the pause days of active customers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    //Set Cronjob check values
    const CRONJOB_ID = 38;
    const CRONJOB_TASK = 'Matching';
    const CRONJOB_SUBTASK = 'Reset pause days for active customers';
    const CRONJOB_TRIGGER = 1;
    const CRONJOB_INTERVAL = "1 YEAR";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cc = new CronjobController();

        //Set Cronjob check values
        $CRONJOB_USERS = [3653];

        try
        {
            $this->resetPauseDays();
            $cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 0, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, null);

        }catch (\Throwable $e){
            $cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 1, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, $e);
        }
    }

    private function resetPauseDays(): void
    {
        $year_now = date("Y");
        $expiration_date = $year_now."-12-31 23:59:59";

        //Mark all expired pauses as deleted
        DB::table("pause_days")
            ->where("pada_expired", "<", $year_now."-01-01 00:00:00")
            ->update(
                [
                    "pada_deleted" => 1
                ]
            );

        $customers = KTCustomerProgress::where("ktcupr_type", 6)
            ->where("ktcupr_deleted", 0)
            ->groupBy("ktcupr_cu_id")
            ->get();

        foreach($customers as $customer){

            //Add 60 pause days for this year to customer
            $add_pause_days = new PauseDay();
            $add_pause_days->pada_cu_id = $customer->ktcupr_cu_id;
            $add_pause_days->pada_timestamp = date("Y-m-d H:i:s");
            $add_pause_days->pada_amount = 60;
            $add_pause_days->pada_amount_left = 60;
            $add_pause_days->pada_start_date = $year_now."-01-01 00:00:00";
            $add_pause_days->pada_expired = $expiration_date;
            $add_pause_days->save();
        }
    }


}
