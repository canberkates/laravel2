<?php

namespace App\Console\Commands;


use App\Cronjobs\CronjobController;
use App\Functions\Mail;
use App\Functions\Mover;
use App\Functions\System;
use App\Models\CustomerLoadExchange;
use App\Models\CustomerLoadReaction;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class LoadExchangeExpiration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronjob:load_exchange_expiration';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    private $cc;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CronjobController $cc)
    {
        $this->cc = $cc;
        parent::__construct();
    }


    const CRONJOB_ID = 15;
    const CRONJOB_TASK = 'Mover Portal';
    const CRONJOB_SUBTASK = 'Send Load Exchange Expiry Emails';
    const CRONJOB_TRIGGER = 5;
    const CRONJOB_INTERVAL = "1 DAY";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        //Set Cronjob check values
        $CRONJOB_USERS = [3653];

        try
        {
            $this->sendExpiredEmails();
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 0, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, null);

        }catch (\Throwable $e){
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 1, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, $e);
        }

    }

    private function sendExpiredEmails(): void
    {
        $system = new System();
        $mover = new Mover();
        $mail = new Mail();

        $soon_deleted_loads_query = CustomerLoadExchange::whereRaw("culoex_date_expiration BETWEEN NOW() AND NOW() + INTERVAL 1 DAY")
            ->where("culoex_deleted", 0)
            ->get();

        $soon_deleted_loads = $system->databaseToArray($soon_deleted_loads_query);

        foreach ($soon_deleted_loads as $load)
        {

            $amount_quotes_query = CustomerLoadReaction::selectRaw("COUNT(*) as amount")
                ->where("culore_culoex_id", $load['culoex_id'])
                ->where("culore_deleted", 0)
                ->first();

            $fields = [
                "to_email" => $mover->getField("moda_load_exchange_email", $load['culoex_cu_id']),
                "name" => $mover->getApplicationUserField("apus_name", $load['culoex_apus_id']),
                "load" => $load,
                "amount_of_quotes" => $amount_quotes_query->amount
            ];

            $mail->send("load_exchange_load_expiration_warning", $mover->getApplicationUserField("cope_language", $load['culoex_apus_id']), $fields, null, false, "#7950a0");
        }

        //DELETE
        $deleted_loads_query = CustomerLoadExchange::whereRaw("culoex_date_expiration < NOW()")
            ->where("culoex_deleted", 0)
            ->get();

        $deleted_loads = $system->databaseToArray($deleted_loads_query);

        foreach ($deleted_loads as $load)
        {

            $fields = [
                "to_email" => $mover->getField("moda_load_exchange_email", $load['culoex_cu_id']),
                "name" => $mover->getApplicationUserField("apus_name", $load['culoex_apus_id']),
                "load" => $load
            ];

            $mail->send("load_exchange_load_expiration", $mover->getApplicationUserField("cope_language", $load['culoex_apus_id']), $fields, null, false, "#7950a0");
            //System::insertHistory("load_exchange_load_expiration", $load['culoex_cu_id'], System::serialize($fields));

            $quote_qu = CustomerLoadReaction::where("culore_culoex_id", $load['culoex_id'])
                ->where("culore_deleted", 0)
                ->get();

            $quote_query = $system->databaseToArray($quote_qu);

            foreach ($quote_query as $quote)
            {
                $amount_quotes_query = CustomerLoadReaction::selectRaw("COUNT(*) as amount")
                    ->where("culore_culoex_id", $load['culoex_id'])
                    ->where("culore_deleted", 0)
                    ->first();

                $my_quotes_query = CustomerLoadReaction::selectRaw("COUNT(*) as amount")
                    ->from("customer_load_reaction")
                    ->where("culore_cu_id", $quote['culore_cu_id'])
                    ->where("culore_deleted", 0)
                    ->first();

                $email_fields = [
                    "to_email" => $mover->getField("moda_load_exchange_email", $quote['culore_cu_id']),
                    "name" => $mover->getApplicationUserField("apus_name", $quote['culore_apus_id']),
                    "quote" => $quote,
                    "load" => $load,
                    "reason" => 2,
                    "amount_of_quotes" => $amount_quotes_query->amount,
                    "my_quotes" => $my_quotes_query->amount
                ];

                $mail->send("load_exchange_load_removed_quotes", $mover->getApplicationUserField("cope_language", $quote['culore_apus_id']), $email_fields, null, false, "#7950a0");
                //System::insertHistory("load_exchange_load_removed_quotes", $quote['culore_cu_id'], System::serialize($email_fields));
            }

            //Set load as deleted
            DB::table('customer_load_exchange')
                ->where('culoex_id', $load['culoex_id'])
                ->update(
                    [
                        'culoex_deleted' => 1
                    ]
                );

            //Set all related quotes as deleted
            DB::table('customer_load_reaction')
                ->where('culore_culoex_id', $load['culoex_id'])
                ->update(
                    [
                        'culore_deleted' => 1
                    ]
                );

        }
    }
}

