<?php

namespace App\Console\Commands;

use App\Cronjobs\CronjobController;
use App\Http\Controllers\SireloSynchroniseController;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Log;

class SireloSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sirelo:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cronjob that syncs the data to the Sirelo servers';

    private $cc;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CronjobController $cc)
    {
        $this->cc = $cc;
        parent::__construct();
    }


    const CRONJOB_ID = 32;
    const CRONJOB_TASK = 'Sirelo';
    const CRONJOB_SUBTASK = 'Sync main Sirelos';
    const CRONJOB_TRIGGER = 3;
    const CRONJOB_INTERVAL = "1 DAY";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        //Set Cronjob check values
        $CRONJOB_USERS = [3653];

        try
        {
            $this->syncMainSirelos();
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 0, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, null);

        }catch (\Throwable $e){
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 1, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, $e);
        }



    }

    private function syncMainSirelos(): void
    {
        Log::debug("Sync cronjob");

        //Set-up request reject POST
        $sync_request = new \Illuminate\Http\Request();
        $sync_request->setMethod('POST');
        $sync_request->request->add(['websites' => [41 => "on", 44 => "on", 45 => "on", 46 => "on", 47 => "on", 48 => "on", 49 => "on", 50 => "on", 51 => "on", 57 => "on", 58 => "on", 59 => "on"]]);

        $ssc = new SireloSynchroniseController();
        $ssc->update($sync_request);
    }

}
