<?php

namespace App\Console\Commands;

use App\Cronjobs\CronjobController;
use App\Functions\System;
use App\Models\Request;
use Carbon\Carbon;
use Illuminate\Console\Command;

class AutomaticMatchCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronjob:automatic_check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks if there are any leads that were missed by the automatic matcher, and thus stuck.';

    private $cc;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CronjobController $cc)
    {
        $this->cc = $cc;
        parent::__construct();
    }

    //Set Cronjob check values
    const CRONJOB_ID = 3;
    const CRONJOB_TASK = 'Matching';
    const CRONJOB_SUBTASK = 'Automatic Match Check';
    const CRONJOB_TRIGGER = 5;
    const CRONJOB_INTERVAL = "15 MINUTE";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Set Cronjob check values
        $CRONJOB_USERS = [3653];

        try{
        //Get requests that are older than 30 minutes but still havent been checked
        $requests = Request::where("re_automatic", 1)->whereRaw("re_timestamp < timestamp(DATE_SUB(NOW(), INTERVAL 30 MINUTE))")->get();

        //Set to 0, put back in normal request list
        foreach($requests as $request){

            $system = new System();
            $system->sendMessage(3653,"Unstuck a request!", $request->re_id);

            $request->re_automatic = 0;
            $request->save();
        }

        $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 0, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, null);

        }catch (\Throwable $e){
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 1, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, $e);
        }
    }
}

