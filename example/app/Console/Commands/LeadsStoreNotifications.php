<?php

namespace App\Console\Commands;

use App\Cronjobs\CronjobController;
use App\Functions\Mail;
use App\Functions\Mover;
use App\Functions\System;
use App\Models\Claim;
use App\Models\KTCustomerPortal;
use App\Models\KTCustomerSubscription;
use App\Models\KTRequestCustomerPortal;
use App\Models\Region;
use App\Models\Request;
use Carbon\Carbon;
use Illuminate\Console\Command;
use pCloud\Exception;

class LeadsStoreNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronjob:leads_store_notifications';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cronjob for sending summaries of available leads for leads store customers.';

    private $cc;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CronjobController $cc)
    {
        $this->cc = $cc;
        parent::__construct();
    }

    const CRONJOB_ID = 13;
    const CRONJOB_TASK = 'Mover Portal';
    const CRONJOB_SUBTASK = 'Send Lead Store Notification Emails';
    const CRONJOB_TRIGGER = 5;
    const CRONJOB_INTERVAL = "1 DAY";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        //Set Cronjob check values
        $CRONJOB_USERS = [3653];

        try
        {
            $this->sendNotifications();
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 0, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, null);

        }catch (\Throwable $e){
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 1, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, $e);
        }


    }


    private function sendNotifications(): void
    {

        $active_leads_store_customers = KTCustomerSubscription::join("customers", function ($join) {
            $join->on("cu_id", "ktcusu_cu_id")
                ->where("cu_deleted", 0)
                ->where("cu_credit_hold", 0);
        })
            ->join("kt_customer_portal", function ($join) {
                $join->on("ktcupo_cu_id", "ktcusu_cu_id")
                    ->where("ktcupo_status", 1);
            })
            ->where("ktcusu_active", 1)
            ->groupBy("ktcusu_cu_id")
            ->get();

        $system = new System();
        $mover = new Mover();
        $mail = new Mail();

        foreach ($active_leads_store_customers as $customer)
        {
            echo $customer->ktcusu_cu_id . "\n";

            $received = [];
            $max_requests = [];

            $active_portals = KTCustomerPortal::select("ktcupo_export_moves", "ktcupo_import_moves", "po_destination_type", "po_co_code")
                ->leftJoin("portals", "po_id", "ktcupo_po_id")
                ->where("ktcupo_cu_id", $customer->cu_id)
                ->where("ktcupo_status", 1)
                ->get();

            $int_regions_exp = [];
            $int_regions_imp = [];
            $nat_regions = [];

            foreach ($active_portals as $portal)
            {
                if ($portal->po_destination_type == 1)
                {
                    //INT
                    $regions = Region::selectRaw("DISTINCT `reg_id`")
                        ->where("reg_co_code", $portal->po_co_code)
                        ->where("reg_destination_type", 1)
                        ->get();

                    if ($portal->ktcupo_export_moves == 1 && $portal->ktcupo_import_moves == 1)
                    {
                        foreach ($regions as $reg)
                        {
                            array_push($int_regions_exp, $reg->reg_id);
                            array_push($int_regions_imp, $reg->reg_id);
                        }
                    } elseif ($portal->ktcupo_export_moves == 1)
                    {
                        foreach ($regions as $reg)
                        {
                            array_push($int_regions_exp, $reg->reg_id);
                        }
                    } elseif ($portal->ktcupo_import_moves == 1)
                    {
                        foreach ($regions as $reg)
                        {
                            array_push($int_regions_imp, $reg->reg_id);
                        }
                    }
                } else
                {
                    //NAT
                    $regions = Region::selectRaw("DISTINCT `reg_id`")
                        ->where("reg_co_code", $portal->po_co_code)
                        ->where("reg_destination_type", 2)
                        ->get();

                    foreach ($regions as $reg)
                    {
                        array_push($nat_regions, $reg->reg_id);
                    }
                }
            }

            $requests_exp = [];
            $requests_imp = [];
            $requests_nat = [];
            $rejected_requests_exp = [];
            $rejected_requests_imp = [];
            $rejected_requests_nat = [];

            if (!empty($int_regions_exp))
            {
                //Get all potential free leads that have not been matched 5 times
                $requests_exp = KTRequestCustomerPortal::selectRaw("re_id, ktrecupo_timestamp, ktrecupo_re_id, re_moving_size, re_moving_date, re_city_from, re_city_to, re_co_code_from, re_co_code_to, COUNT(`ktrecupo_re_id`) AS amount_matched")
                    ->leftJoin("requests", "ktrecupo_re_id", "re_id")
                    ->leftJoin("mover_data", "moda_cu_id", "ktrecupo_cu_id")
                    ->whereIn("re_request_type", [1, 2, 3, 4])
                    ->whereRaw("`ktrecupo_timestamp` BETWEEN NOW() - INTERVAL 1 DAY AND NOW()")
                    ->whereIn("re_reg_id_from", array_values($int_regions_exp))
                    ->where("moda_quality_score_override", "!=", "-2")
                    ->groupBy("ktrecupo_re_id")
                    ->havingRaw("amount_matched < 5")
                    ->get();

                //Get all requests that have no occupancy
                $rejected_requests_exp = Request::selectRaw("re_id, re_rejection_timestamp, re_moving_size, re_moving_date, re_city_from, re_city_to, re_co_code_from, re_co_code_to")
                    ->whereRaw("`re_rejection_timestamp` BETWEEN NOW() - INTERVAL 1 DAY AND NOW()")
                    ->whereIn("re_request_type", [1, 2, 3, 4])
                    ->whereIn("re_reg_id_from", array_values($int_regions_exp))
                    ->whereIn("re_rejection_reason", [2, 3])
                    ->get();
            }

            if (!empty($int_regions_imp))
            {
                //Get all potential free leads that have not been matched 5 times
                $requests_imp = KTRequestCustomerPortal::selectRaw("re_id, ktrecupo_timestamp, ktrecupo_re_id, re_moving_size, re_moving_date, re_city_from, re_city_to, re_co_code_from, re_co_code_to, COUNT(`ktrecupo_re_id`) AS amount_matched")
                    ->leftJoin("requests", "ktrecupo_re_id", "re_id")
                    ->leftJoin("mover_data", "moda_cu_id", "ktrecupo_cu_id")
                    ->whereIn("re_request_type", [1, 2, 3, 4])
                    ->whereRaw("`ktrecupo_timestamp` BETWEEN NOW() - INTERVAL 1 DAY AND NOW()")
                    ->whereIn("re_reg_id_to", array_values($int_regions_imp))
                    ->where("moda_quality_score_override", "!=", "-2")
                    ->groupBy("ktrecupo_re_id")
                    ->havingRaw("amount_matched < 5")
                    ->get();

                //Get all requests that have no occupancy
                $rejected_requests_imp = Request::selectRaw("re_id, re_rejection_timestamp, re_moving_size, re_moving_date, re_city_from, re_city_to, re_co_code_from, re_co_code_to")
                    ->whereRaw("`re_rejection_timestamp` BETWEEN NOW() - INTERVAL 1 DAY AND NOW()")
                    ->whereIn("re_request_type", [1, 2, 3, 4])
                    ->whereIn("re_reg_id_to", array_values($int_regions_imp))
                    ->whereIn("re_rejection_reason", [2, 3])
                    ->get();
            }

            if (!empty($nat_regions))
            {
                //Get all potential free leads that have not been matched 5 times
                $requests_nat = KTRequestCustomerPortal::selectRaw("re_id, ktrecupo_timestamp, ktrecupo_re_id, re_moving_size, re_moving_date, re_city_from, re_city_to, re_co_code_from, re_co_code_to, COUNT(`ktrecupo_re_id`) AS amount_matched")
                    ->leftJoin("requests", "ktrecupo_re_id", "re_id")
                    ->leftJoin("mover_data", "moda_cu_id", "ktrecupo_cu_id")
                    ->whereIn("re_request_type", [1, 2, 3, 4])
                    ->whereRaw("`ktrecupo_timestamp` BETWEEN NOW() - INTERVAL 1 DAY AND NOW()")
                    ->whereIn("re_reg_id_from", array_values($nat_regions))
                    ->whereIn("re_reg_id_to", array_values($nat_regions))
                    ->where("moda_quality_score_override", "!=", "-2")
                    ->groupBy("ktrecupo_re_id")
                    ->havingRaw("amount_matched < 5")
                    ->get();

                //Get all requests that have no occupancy
                $rejected_requests_nat = Request::selectRaw("re_id, re_rejection_timestamp, re_moving_size, re_moving_date, re_city_from, re_city_to, re_co_code_from, re_co_code_to")
                    ->whereRaw("`re_rejection_timestamp` BETWEEN NOW() - INTERVAL 1 DAY AND NOW()")
                    ->whereIn("re_request_type", [1, 2, 3, 4])
                    ->whereIn("re_reg_id_from", array_values($nat_regions))
                    ->whereIn("re_reg_id_to", array_values($nat_regions))
                    ->whereIn("re_rejection_reason", [2, 3])
                    ->get();
            }


            $requests_exp = $system->databaseToArray($requests_exp);
            $requests_imp = $system->databaseToArray($requests_imp);
            $requests_nat = $system->databaseToArray($requests_nat);
            $rejected_requests_exp = $system->databaseToArray($rejected_requests_exp);
            $rejected_requests_imp = $system->databaseToArray($rejected_requests_imp);
            $rejected_requests_nat = $system->databaseToArray($rejected_requests_nat);

            $requests = array_merge($requests_exp, $requests_imp, $requests_nat);

            $rejected_requests = array_merge($rejected_requests_exp, $rejected_requests_imp, $rejected_requests_nat);

            //Get all requests taken by this customer last X days
            $receivedquery = KTRequestCustomerPortal::select("ktrecupo_re_id")
                ->where("ktrecupo_cu_id", $customer->cu_id)
                ->whereRaw("`ktrecupo_timestamp` BETWEEN NOW() - INTERVAL 3 DAY AND NOW()")
                ->get();

            foreach ($receivedquery as $received_row)
            {
                $received[] = $received_row->ktrecupo_re_id;
            }

            if (!empty($int_regions_exp))
            {
                //Get all max matched requests (to be sure)
                $all_max_matched_requests = KTRequestCustomerPortal::selectRaw("*, COUNT(`ktrecupo_re_id`) AS amount_matched")
                    ->leftJoin("requests", "ktrecupo_re_id", "re_id")
                    ->whereRaw("`ktrecupo_timestamp` BETWEEN NOW() - INTERVAL 3 DAY AND NOW()")
                    ->whereIn("re_reg_id_from", array_values($int_regions_exp))
                    ->groupBy("ktrecupo_re_id")
                    ->havingRaw("amount_matched > 4")
                    ->get();

                foreach ($all_max_matched_requests as $all_request)
                {
                    $max_requests[] = $all_request->ktrecupo_re_id;
                }
            }


            if (!empty($int_regions_imp))
            {
                //Get all max matched requests (to be sure)
                $all_max_matched_requests = KTRequestCustomerPortal::selectRaw("*, COUNT(`ktrecupo_re_id`) AS amount_matched")
                    ->leftJoin("requests", "ktrecupo_re_id", "re_id")
                    ->whereRaw("`ktrecupo_timestamp` BETWEEN NOW() - INTERVAL 3 DAY AND NOW()")
                    ->whereIn("re_reg_id_to", array_values($int_regions_imp))
                    ->groupBy("ktrecupo_re_id")
                    ->havingRaw("amount_matched > 4")
                    ->get();

                foreach ($all_max_matched_requests as $all_request)
                {
                    $max_requests[] = $all_request->ktrecupo_re_id;
                }
            }


            if (!empty($nat_regions))
            {
                //Get all max matched requests (to be sure)
                $all_max_matched_requests = KTRequestCustomerPortal::selectRaw("*, COUNT(`ktrecupo_re_id`) AS amount_matched")
                    ->leftJoin("requests", "ktrecupo_re_id", "re_id")
                    ->whereRaw("`ktrecupo_timestamp` BETWEEN NOW() - INTERVAL 3 DAY AND NOW()")
                    ->whereIn("re_reg_id_from", array_values($nat_regions))
                    ->whereIn("re_reg_id_to", array_values($nat_regions))
                    ->groupBy("ktrecupo_re_id")
                    ->havingRaw("amount_matched > 4")
                    ->get();

                foreach ($all_max_matched_requests as $all_request)
                {
                    $max_requests[] = $all_request->ktrecupo_re_id;
                }
            }


            $claimed = [];

            //Get all requests that have been claimed for being invalid
            $claimedquery = Claim::select("ktrecupo_re_id")
                ->leftJoin("kt_request_customer_portal", "cl_ktrecupo_id", "ktrecupo_id")
                ->whereRaw("`cl_timestamp` BETWEEN NOW() - INTERVAL 1 DAY AND NOW()")
                ->where("cl_status", 1)
                ->whereRaw("`cl_reason` NOT IN (0, 3, 4, 5)")
                ->get();

            foreach ($claimedquery as $claimed_row)
            {
                $claimed[] = $claimed_row->ktrecupo_re_id;
            }

            $list_of_requests_ids = [];
            $list_of_requests = [];

            foreach ($requests as $row)
            {
                $amount_matched = KTRequestCustomerPortal::selectRaw("COUNT(*) as `amount_matched`")
                    ->where("ktrecupo_re_id", $row['re_id'])
                    ->first()->amount_matched;

                if ($amount_matched >= 5)
                    continue;

                $hourdiff = round((strtotime(date("Y-m-d H:i:s")) - strtotime($row['ktrecupo_timestamp'])) / 60, 0);

                if (in_array($row['ktrecupo_re_id'], $list_of_requests_ids) || in_array($row['ktrecupo_re_id'], $max_requests) || in_array($row['ktrecupo_re_id'], $received) || in_array($row['ktrecupo_re_id'], $claimed) || !Mover::withinHourRange($row['amount_matched'], $hourdiff))
                {
                    continue;
                } else
                {
                    $list_of_requests_ids[] = $row['ktrecupo_re_id'];

                    $list_of_requests[$row['re_id']] = [
                        //'country_from' => $data->country($row->re_co_code_from, $customer->cu_la_code),
                        //'country_to' => $data->country($row->re_co_code_to, $customer->cu_la_code),
                        'moving_size' => $mover->movingSizeText($row['re_moving_size'], $customer->cu_la_code),
                        'moving_date' => date("W", strtotime($row['re_moving_date'])) . " / " . date("Y", strtotime($row['re_moving_date'])),
                        'amount_matched' => $amount_matched,
                        'request' => $row
                    ];
                }
            }

            foreach ($rejected_requests as $row)
            {
                $amount_matched = KTRequestCustomerPortal::selectRaw("COUNT(*) as `amount_matched`")
                    ->where("ktrecupo_re_id", $row['re_id'])
                    ->first()->amount_matched;

                if ($amount_matched >= 5)
                    continue;

                if (in_array($row['re_id'], $list_of_requests_ids) || in_array($row['re_id'], $received))
                {
                    continue;
                } else
                {
                    $list_of_requests_ids[] = $row['re_id'];

                    $list_of_requests[$row['re_id']] = [
                        //'country_from' => $data->country($row->re_co_code_from, $customer->cu_la_code),
                        //'country_to' => $data->country($row->re_co_code_to, $customer->cu_la_code),
                        'moving_size' => $mover->movingSizeText($row['re_moving_size'], $customer->cu_la_code),
                        'moving_date' => date("W", strtotime($row['re_moving_date'])) . " / " . date("Y", strtotime($row['re_moving_date'])),
                        'amount_matched' => $amount_matched,
                        'request' => $row
                    ];
                }
            }

            if (count($list_of_requests) > 0)
            {

                $fields = [
                    'list_of_requests' => $list_of_requests,
                    'to_email' => $customer->cu_email,
                    'company_name' => $customer->cu_company_name_business
                ];

                $mail->send("leads_store_leads_notification", $customer->cu_la_code, $fields);
            }
        }

    }
}
