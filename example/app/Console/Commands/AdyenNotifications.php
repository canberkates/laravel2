<?php

namespace App\Console\Commands;

use App\Cronjobs\CronjobController;
use App\Functions\AdyenTransaction;
use App\Functions\DataIndex;
use App\Functions\System;
use App\Models\AdyenPayment;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class AdyenNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronjob:adyen_notifications';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    private $cc;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CronjobController $cc)
    {
        $this->cc = $cc;
        parent::__construct();
    }

    //Class constants for Cronjob Info
    const CRONJOB_ID = 2;
    const CRONJOB_TASK = 'Finance';
    const CRONJOB_SUBTASK = 'Adyen Notifications';
    const CRONJOB_TRIGGER = 1;
    const CRONJOB_INTERVAL = "15 MINUTE";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        //Set Cronjob check values
        $CRONJOB_USERS = DataIndex::financeUsers();

        try
        {

            //Check if the file exists
            $original_file = env("SHARED_FOLDER") . 'uploads/temp/adyen_notifications.json';
            $file = env("SHARED_FOLDER") . 'uploads/temp/adyen_notifications.processing.json';

            //Move the file
            if (!file_exists($original_file))
            {
                $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 0, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, null);
                return;
            }
            rename($original_file, $file);

            //Process the notifications
            $fp = fopen($file, 'r');

            if ($fp)
            {
                while (($line = fgets($fp)) !== false)
                {
                    $notification = json_decode($line, true);
                    if (isset($notification['notificationItems']))
                    {
                        foreach ($notification['notificationItems'] as $notificationItem)
                        {
                            if (isset($notificationItem['NotificationRequestItem']))
                            {
                                self::processNotification($notificationItem['NotificationRequestItem']);
                            } else
                            {
                                error_log("[Adyen] NotificationItem without NotificationRequestItem, dumping: " . json_encode($notificationItem));
                                error_log("[Adyen] Full notification: " . json_encode($notification));
                            }
                        }
                    } else
                    {
                        self::processNotification($notification);
                    }
                }
                fclose($fp);

                //Delete the old file
                unlink($file);

                $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 0, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, null);

            } else
            {
                error_log("[Adyen] Unable to read processing json file");
                $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 1, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, "[Adyen] Unable to read processing json file");
            }
        }catch (\Throwable $e){
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 1, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, $e);
        }
    }

    public function handleSettlementReport($report, $file, $batch)
    {
        $system = new System();
        //Check if this report hasn't been parsed yet
        $file_path = env("SHARED_FOLDER").'uploads/adyen_reports/'.$file;
        if(file_exists($file_path))
        {
            return;
        }

        //Check if it isn't a duplicate
        $last_report_file_path = env("SHARED_FOLDER").'uploads/adyen_reports/last_report.json';
        $last_report_json = file_get_contents($last_report_file_path);

        if($last_report_json !== false)
        {
            $last_reports = json_decode($last_report_json, true);
            if(in_array($batch, $last_reports['done']))
            {
                return;
            }

            //Check for duplicate report (adds a 1 or 2 add the end)
            if($batch > ($last_reports['highest'] * 10))
            {
                return;
            }

            if($batch > $last_reports['highest'])
            {
                $last_reports['highest'] = $batch;
            }
            $last_reports['done'][] = $batch;
        }
        else
        {
            $last_reports = [
                "highest" => $batch,
                "done" => [$batch]
            ];
        }
        file_put_contents($last_report_file_path, json_encode($last_reports));

        //Create & set options
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $report);
        curl_setopt($ch, CURLOPT_USERPWD, $system->getSetting('adyen_report_api_user').":".$system->getSetting('adyen_report_api_password'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        //Execute
        $result = curl_exec($ch);

        if($result === false)
        {
            error_log("Failed to CURL to {$report}: ".curl_error($ch));
        }

        curl_close($ch);

        //=> Save the file
        file_put_contents($file_path, $result);


        //Set all items on completed
        $report_rows = explode("\n", $result);
        unset($report_rows[0]);

        foreach($report_rows as $row_value)
        {
            if(!empty($row_value))
            {
                $row = explode("\t", $row_value);
                if(!empty($row[2]))
                {
                    $pspReference = $row[2];
                    switch($row[7])
                    {
                        case "Chargeback":
                            self::updatePaymentStatus($pspReference, 4, "[Chargeback][Confirmed] by batch #{$batch}");
                            break;

                        case "Refunded":
                            self::updatePaymentStatus($pspReference, 5, "[Refund][Confirmed] by batch #{$batch}");
                            break;

                        case "Settled":
                            self::updatePaymentStatus($pspReference, 3, "[Payment][Confirmed] by batch #{$batch}");
                            break;
                    }
                }
            }
        }
    }

    public function getPayment($pspReference)
    {
        $system = new System();

        $query = AdyenPayment::where("adpa_psp_reference", $pspReference)->first();

        $query = $system->databaseToArray($query);

        return $query;
    }

    public function updatePaymentStatus($pspReference, $status, $add_message)
    {
        $system = new System();

        //Get entry
        $escaped_psp = $pspReference;
        $row = self::getPayment($pspReference);
        if($row === false)
        {
            return;
        }

        if($status === null)
        {
            $status = $row['adpa_status'];
        }

        //Add change
        $changes = $system->unserialize($row['adpa_changes']);
        $changes[] = "[".date("Y-m-d H:i:s")."]".$add_message;
        $changes_value = $system->serialize($changes);

        DB::table('adyen_payments')
            ->where('adpa_psp_reference', $escaped_psp)
            ->update(
                [
                    'adpa_status' =>  $status,
                    'adpa_changes' =>  $changes_value
                ]
            );
    }

    public function originalPspReference($notification)
    {
        $system = new System();
        return $system->useOr($notification['originalReference'], $notification['pspReference']);
    }

    public function messageFinance($subject, $message)
    {
        $system = new System();
        $dataindex = new DataIndex();
        $system->sendMessage(array_keys($dataindex->financeUsers()), $subject, $message);
    }

    /**
     * Process an Adyen notification
     * @param array $notification the array
     */
    public function processNotification($notification)
    {
        $system = new System();
        $adyen = new AdyenTransaction();

        switch($notification['eventCode'])
        {
            case "REPORT_AVAILABLE":
                $matches = [];
                if(preg_match('/^settlement_detail_report_batch_(\d+)\.tsv$/', $notification['pspReference'], $matches) === 1)
                {
                    self::handleSettlementReport($notification['reason'], $notification['pspReference'], $matches[1]);
                }
                break;

            case "CANCELLATION":
            case "CANCEL_OR_REFUND":
            case "REFUND":
                self::updatePaymentStatus(self::originalPspReference($notification), 5, "[Refund][Notification] Event: {$notification['eventCode']}");
                break;

            case "CHARGEBACK":
                self::updatePaymentStatus(self::originalPspReference($notification), 4, "[Chargeback][Notification]");
                self::messageFinance(
                    "Payment chargeback",
                    "The payment <strong>{$notification['merchantReference']} (".
                    $system->paymentCurrencyToken($notification['amount']['currency'])." ".
                    $system->numberFormat($adyen->decimalValue($notification['amount']['value']), 2).
                    ")</strong> has been charged back (pspReference: {$notification['pspReference']})"
                );
                break;

            case "CHARGEBACK_REVERSED":
                self::updatePaymentStatus(self::originalPspReference($notification), 3, "[Payment][Notification] Chargeback reversed");
                break;

            case "REFUND_FAILED":
                self::messageFinance("Refund failed", "A refund failed. Futher investagion required. pspReference: {$notification['pspReference']}");
                self::updatePaymentStatus(self::originalPspReference($notification), 3, "[Payment][Notification] Refund failed");
                break;

            case "REFUNDED_REVERSED":
                self::updatePaymentStatus(self::originalPspReference($notification), 3, "[Payment][Notification] Refund reversed");
                break;

            case "NOTIFICATION_OF_CHARGEBACK":
                self::updatePaymentStatus(self::originalPspReference($notification), null, "[Chargeback][Warning]");
                self::messageFinance(
                    "Chargeback warning",
                    "A chargeback has been issued for the payment <strong>{$notification['merchantReference']}</strong> (pspReference: {$notification['pspReference']})"
                );
                break;
        }
    }
}

