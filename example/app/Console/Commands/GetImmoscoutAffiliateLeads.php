<?php

namespace App\Console\Commands;

use App\Cronjobs\CronjobController;
use App\Functions\DataIndex;
use Carbon\Carbon;
use Exception;
use Illuminate\Console\Command;

class GetImmoscoutAffiliateLeads extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronjob:get_immoscout_affiliate_leads';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    private $cc;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CronjobController $cc)
    {
        $this->cc = $cc;
        parent::__construct();
    }

    const CRONJOB_ID = 11;
    const CRONJOB_TASK = 'Affiliate';
    const CRONJOB_SUBTASK = 'Get Immoscout leads';
    const CRONJOB_TRIGGER = 12;
    const CRONJOB_INTERVAL = "5 MINUTE";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Set Cronjob check values
        $CRONJOB_USERS =  DataIndex::financeUsers();

        try
        {
            $this->getLeads();
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 0, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, null);

        }catch (\Throwable $e){
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 1, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, $e);
        }


    }

    public function buildAffiliateAPIPost($result){
        return [
            "moving_date" => $result->WunschterminBis,
            "moving_size" => 1,
            "volume" => $result->Wohnflache_BL / 3,
            "remarks" => $result->Bemerkung_BL,

            "packing" => intval($result->Einpacken),
            "assembly" => intval($result->Moebelmontage_BL),

            "street_from" => $result->Strasse_BL,
            "zipcode_from" => $result->PLZ_BL,
            "city_from" => $result->Ort_BL,
            "country_from" => self::getCountryCodeFromJSON($result->Land_BL),

            "street_to" => $result->Strasse_EL,
            "zipcode_to" => $result->PLZ_EL,
            "city_to" => $result->Ort_EL,
            "country_to" => self::getCountryCodeFromJSON($result->Land_EL),

            "first_name" => $result->Name_A,
            "family_name" => $result->Vorname_A,
            "full_name" => $result->Vorname_A." ".$result->Name_A,
            "telephone1" => $result->Telefon_A,
            "telephone2" => $result->Telefon2_A,
            "email" => $result->Mail_A,


        ];

    }

    public function getCountryCodeFromJSON($country_code){
        $jsonfile = file_get_contents(env("SHARED_FOLDER")."scout24countrycodes.json");
        $json = json_decode($jsonfile);

        foreach($json as $line){
            if(strtolower($line->A) == $country_code){
                return $line->B;
            }
        }
    }

    private function getLeads(): void
    {
        $auth_bearer = "3582d7949a9f82373c355ba86ef307790537d617";
        $affiliate_token = "8ceed007ab3c449f77f1365e67709d21";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://www.umzug-easy.de/api/listrequests/");
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ["Authorization: Bearer {$auth_bearer}"]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);

        $result = json_decode($result);

        if (strpos($result->error, "no new requests found") !== false) {
            echo $result->error;
            return;
        }elseif($result->error){
            throw new Exception($result->error);
        }



        foreach ($result as $lead)
        {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://www.umzug-easy.de/api/request/" . $lead->aid);
            curl_setopt($ch, CURLOPT_POST, FALSE);
            curl_setopt($ch, CURLOPT_HTTPHEADER, ["Authorization: Bearer {$auth_bearer}"]);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $lead_values = curl_exec($ch);
            curl_close($ch);

            $post_values = self::buildAffiliateAPIPost(json_decode($lead_values));

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://affiliate-partner-forms.triglobal.info/api.php?token=" . $affiliate_token);
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/x-www-form-urlencoded']);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_values));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $result = curl_exec($ch);
            curl_close($ch);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://www.umzug-easy.de/api/request/" . $lead->aid);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($ch, CURLOPT_HTTPHEADER, ["Authorization: Bearer {$auth_bearer}"]);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $post = curl_exec($ch);
            curl_close($ch);


            print_r($result);
            print_r($post);
        }
    }

}

