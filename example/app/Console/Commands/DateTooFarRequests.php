<?php

namespace App\Console\Commands;

use App\Cronjobs\CronjobController;
use App\Functions\DataIndex;
use App\Functions\Mail;
use App\Functions\System;
use App\Models\Request;
use App\Models\Website;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class DateTooFarRequests extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronjob:date_too_far_requests';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    private $cc;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CronjobController $cc)
    {
        $this->cc = $cc;
        parent::__construct();
    }

    const CRONJOB_ID = 10;
    const CRONJOB_TASK = 'Mailing';
    const CRONJOB_SUBTASK = 'Send Date Too Far Away Emails';
    const CRONJOB_TRIGGER = 20;
    const CRONJOB_INTERVAL = "60 MINUTE";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Set Cronjob check values
        $CRONJOB_USERS =  DataIndex::financeUsers();

        try
        {
            $this->sendDateTooFarEmail();
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 0, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, null);

        }catch (\Throwable $e){
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 1, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, $e);
        }

    }

    private function sendDateTooFarEmail(): void
    {
        $system = new System();
        $mail = new Mail();

        $qu = Request::leftJoin("volume_calculator", "voca_id", "re_voca_id")
            ->leftJoin("websites", "we_sirelo_co_code", "re_co_code_from")
            ->where("re_status", 2)
            ->where("re_rejection_reason", 7)
            ->where("re_date_too_far_mailed", 0)
            ->whereRaw("`re_moving_date` BETWEEN DATE(NOW() + INTERVAL 14 DAY) AND DATE(NOW() + INTERVAL 6 MONTH) AND HOUR(`requests`.`re_timestamp`) <= HOUR(NOW())")
            ->get();

        $query = $system->databaseToArray($qu);


        foreach ($query as $row)
        {
            DB::table('requests')
                ->where('re_id', $row['re_id'])
                ->update(
                    [
                        're_date_too_far_mailed' => 1
                    ]
                );

            $row['re_volume_calculator'] = ((!empty($row['voca_volume_calculator'])) ? $row['voca_volume_calculator'] : null);

            if (empty($row['we_personal_dashboard_url'])) {
                $website = Website::where("we_sirelo_url", "https://sirelo.org")->first();

                $row['we_personal_dashboard_url'] = $website->we_personal_dashboard_url;
            }

            $pd_link = $row['we_personal_dashboard_url']."?id=" . $row['re_token'] . "&utm_source=personal-dashboard&utm_medium=email";

            $fields = [
                "request" => $row,
                "pd_link" => $pd_link
            ];

            $mail->send("request_date_too_far", $row['re_la_code'], $fields);
        }
    }
}

