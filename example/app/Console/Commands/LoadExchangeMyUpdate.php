<?php

namespace App\Console\Commands;

use App\Cronjobs\CronjobController;
use App\Functions\Mail;
use App\Functions\Mover;
use App\Functions\System;
use App\Models\Customer;
use App\Models\CustomerLoadExchange;
use App\Models\CustomerLoadReaction;

use Carbon\Carbon;
use Illuminate\Console\Command;

class LoadExchangeMyUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronjob:load_exchange_my_update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    private $cc;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CronjobController $cc)
    {
        $this->cc = $cc;
        parent::__construct();
    }


    const CRONJOB_ID = 16;
    const CRONJOB_TASK = 'Mover Portal';
    const CRONJOB_SUBTASK = 'Send Load Exchange Own Loads Emails';
    const CRONJOB_TRIGGER = 5;
    const CRONJOB_INTERVAL = "1 DAY";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        //Set Cronjob check values
        $CRONJOB_USERS = [3653];

        try
        {
            $this->sendEmails();
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 0, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, null);

        }catch (\Throwable $e){
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 1, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, $e);
        }

    }

    public function myUpdate($customer)
    {

        $system = new System();
        $mail = new Mail();
        $mover = new Mover();

        $loads = CustomerLoadExchange::where("culoex_cu_id", $customer['cu_id'])
            ->where("culoex_deleted", 0)
            ->orderBy("culoex_timestamp", "DESC")
            ->get();

        $loads = $system->databaseToArray($loads);

        $my_quotes = CustomerLoadExchange::leftJoin("customer_load_reaction", "culore_culoex_id", "culoex_id")
            ->where("culore_cu_id", $customer['cu_id'])
            ->where("culoex_deleted", 0)
            ->where("culore_deleted", 0)
            ->orderBy("culore_timestamp", "DESC")
            ->get();

        $my_quotes = $system->databaseToArray($my_quotes);

        if(count($loads) > 0)
        {
            foreach($loads as $index => $load)
            {
                $quotes = CustomerLoadReaction::selectRaw("COUNT(*) as amount")
                    ->where("culore_culoex_id", $load['culoex_id'])
                    ->where("culore_deleted", 0)
                    ->first();

                //Set amount of quotes, otherwise zero
                if(count($quotes) > 0)
                {
                    $loads[$index]['quotes'] = $quotes->amount;
                }
                else
                {
                    $loads[$index]['quotes'] = 0;
                }

            }
        }

        if(count($my_quotes) > 0)
        {
            foreach($my_quotes as $index => $load)
            {
                $quotes = CustomerLoadReaction::selectRaw("COUNT(*) as amount")
                    ->where("culore_culoex_id", $load['culoex_id'])
                    ->where("culore_deleted", 0)
                    ->first();

                //Set amount of quotes, otherwise zero
                if($quotes)
                {
                    $my_quotes[$index]['quotes'] = $quotes->amount;
                }
                else
                {
                    $my_quotes[$index]['quotes'] = 0;
                }

            }
        }

        $fields = [
            "to_email" => $mover->getField("moda_load_exchange_email", $customer['cu_id']),
            "name" => $customer['cu_company_name_business'],
            "loads" => $loads,
            "quotes" => $my_quotes
        ];

        if($loads || $my_quotes)
        {
            $mail->send("load_exchange_daily_my_update", $customer['cu_la_code'], $fields, null, false, "#7950a0");
            //System::insertHistory("load_exchange_daily_my_update", $customer['cu_id'], System::serialize($fields));

        }
    }

    private function sendEmails(): void
    {
        $system = new System();

        $customers_query = Customer::leftJoin("mover_data", "moda_cu_id", "cu_id")
            ->where("cu_deleted", 0)
            ->where("moda_activate_load_exchange", 1)
            ->where("moda_load_exchange_daily", 1)
            ->get();

        $customers = $system->databaseToArray($customers_query);

        foreach ($customers as $customer)
        {
            self::myUpdate($customer);
        }
    }

}

