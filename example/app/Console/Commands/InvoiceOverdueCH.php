<?php

namespace App\Console\Commands;

use App\Cronjobs\CronjobController;
use App\Functions\DataIndex;
use App\Functions\Mail;
use App\Functions\System;
use App\Models\Customer;
use App\Models\Invoice;
use App\Models\KTBankLineInvoiceCustomerLedgerAccount;
use App\Models\PaymentCurrency;
use Carbon\Carbon;
use DateTime;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use IntlDateFormatter;

class InvoiceOverdueCH extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'checker:invoices_overdue_ch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks if invoice has an overdue of 2 days, if they have the overdue, then put customer on credit hold.s';

    private $cc;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CronjobController $cc)
    {
        $this->cc = $cc;
        parent::__construct();
    }


    const CRONJOB_ID = 12;
    const CRONJOB_TASK = 'Finance';
    const CRONJOB_SUBTASK = 'Send Credit Hold Emails';
    const CRONJOB_TRIGGER = 1;
    const CRONJOB_INTERVAL = "1 DAY";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Set Cronjob check values
        $CRONJOB_USERS =  DataIndex::financeUsers();

        try
        {
            $this->sendCreditHoldEmail();
            $this->sendWarningEmail();
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 0, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, null);

        }catch (\Throwable $e){
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 1, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, $e);
        }


    }

    /**
     * @param $mail
     */
    public function sendWarningEmail()
    {
        $mail = new Mail();

        //Exclude following customers for 'expiring in 5 days' email
        //HL Van Transport (226)
        $exclude = [226];

        $customers = Customer::where("cu_credit_hold", 0)
            ->where("cu_deleted", 0)
            ->where("cu_payment_method", 1)
            ->whereNotIn("cu_id", $exclude)
            ->get();

        //Check if invoice will be expired within 5 days, if it will, than send a mail to the customer
        $date_now = date("Y-m-d");

        foreach ($customers as $customer) {
            $invoice_overdue_query = Invoice::where("in_cu_id", $customer->cu_id)
                ->whereRaw("in_expiration_date > '" . $date_now . "'")
                ->get();

            if (count($invoice_overdue_query) > 0) {
                foreach ($invoice_overdue_query as $invoice) {
                    $dStart = new DateTime($date_now);
                    $dEnd = new DateTime($invoice->in_expiration_date);
                    $dDiff = $dStart->diff($dEnd);

                    //IS INVOICE ALREADY PAID?
                    $bank_lines = KTBankLineInvoiceCustomerLedgerAccount::leftJoin("bank_lines", "ktbaliinculeac_bali_id", "bali_id")
                        ->where("ktbaliinculeac_in_id", $invoice->in_id)
                        ->get();

                    $amount_paired = 0;

                    foreach ($bank_lines as $bali) {
                        $amount_paired += $bali->ktbaliinculeac_amount;
                    }

                    //Check if expiration date is within 5 days and check if invoice is already paid
                    if ($dDiff->format('%r%a') == 5 && round($amount_paired, 2) != round($invoice->in_amount_netto_eur,2)) {
                        if ($customer->cu_skip_for_overdue_cronjob == 1)
                        {
                            $customer->cu_skip_for_overdue_cronjob = 0;
                            $customer->save();
                        }
                        else {
                            //Invoice will be expired within 5 days
                            $fc_currency = PaymentCurrency::where("pacu_code", $invoice->in_currency)->first()->pacu_token;

                            $amount = $fc_currency . " " . $invoice->in_amount_netto;

                            if (!empty($customer->cu_email_bi))
                            {
                                $email = $customer->cu_email_bi;
                            }
                            else {
                                $email = $customer->cu_email;
                            }

                            $date = DateTime::createFromFormat("Y-m-d", $invoice->in_expiration_date);
                            $day = $date->format("d");
                            $month = $date->format("m");
                            $year = $date->format("y");

                            $fmt = new IntlDateFormatter(
                                $customer->cu_la_code,
                                IntlDateFormatter::GREGORIAN,
                                IntlDateFormatter::NONE
                            );

                            $exp_date = $fmt->format(mktime(null, null, null, $month, $day, $year));

                            $fields = [
                                "expiration_date" => $exp_date,
                                "invoice_number" => $invoice->in_number,
                                "amount" => $amount,
                                "email" => $email
                            ];

                            //Send mail
                            $mail->send("invoice_almost_overdue", $customer->cu_la_code, $fields);
                        }
                    }
                }
            }
        }
    }

    /**
     * @param $system
     * @param $mail
     */
    public function sendCreditHoldEmail()
    {
        $system = new System();
        $mail = new Mail();

        $customers = [];
        $date_2_days_ago = date("Y-m-d", strtotime(date("Y-m-d", strtotime("-2 day"))));

        $invoices = Invoice::whereRaw("in_expiration_date = '" . $date_2_days_ago . "'")
            ->leftJoin("customers", "cu_id", "in_cu_id")
            ->where("cu_payment_method", "!=", 4)
            ->where("cu_payment_method", "!=", 5)
            ->where("cu_payment_method", "!=", 6)
            ->get();

        foreach ($invoices as $invoice) {
            $bank_lines = KTBankLineInvoiceCustomerLedgerAccount::leftJoin("bank_lines", "ktbaliinculeac_bali_id", "bali_id")
                ->where("ktbaliinculeac_in_id", "=", $invoice->in_id)
                ->get();
            //TODO: Code rekent nu ook half afbetaalde invoices als betaald.
            if (count($bank_lines) == 0) {
                //Not Paid
                if (!isset($customers[$invoice->in_cu_id])) {
                    $customers[$invoice->in_cu_id] = $invoice->in_cu_id;
                }
            }
        }

        foreach ($customers as $cu_id => $customer) {
            $row = Customer::where("cu_id", $cu_id)->first();
            $invoices = [];

            $amount_left = 0;
            $amount_left_eur = 0;

            $invoices_query = Invoice::where("in_cu_id", $cu_id)->get();

            foreach ($invoices_query as $row_invoices) {
                $query_bank_line_invoice_customer_ledger_account = DB::table("kt_bank_line_invoice_customer_ledger_account")
                    ->select("ktbaliinculeac_amount")
                    ->where("ktbaliinculeac_in_id", $row_invoices->in_id)
                    ->get();

                $amount_paired = 0;
                $amount_paired_fc = 0;

                foreach ($query_bank_line_invoice_customer_ledger_account as $row_bank_line_invoice_customer_ledger_account) {
                    $amount_paired += $row_bank_line_invoice_customer_ledger_account->ktbaliinculeac_amount;
                    $amount_paired_fc += $system->currencyConvert($row_bank_line_invoice_customer_ledger_account->ktbaliinculeac_amount, "EUR", $row->cu_pacu_code);
                }

                if ($row_invoices->in_amount_netto_eur != round($amount_paired, 2)) {
                    if ($row_invoices->in_expiration_date >= date("Y-m-d")) {
                        continue;
                    }
                    $amount_netto = (($row->cu_pacu_code == "EUR") ? $row_invoices->in_amount_netto_eur : $system->currencyConvert($row_invoices->in_amount_netto, $row_invoices->in_currency, $row->cu_pacu_code));

                    $invoices[] = [
                        "id" => $row_invoices->in_id,
                        "number" => $row_invoices->in_number,
                        "date" => $row_invoices->in_date,
                        "expiration_date" => $row_invoices->in_expiration_date,
                        "expiration_date_time" => strtotime($row_invoices->in_expiration_date),
                        "days_overdue" => $system->dateDifference($row_invoices->in_expiration_date, date("Y-m-d")),
                        "payment_reminder" => $row_invoices->in_payment_reminder,
                        "payment_reminder_date" => $row_invoices->in_payment_reminder_date,
                        "payment_reminder_date_time" => strtotime($row_invoices->in_payment_reminder_date),
                        "amount_netto" => $amount_netto,
                        "amount_paid" => $amount_paired_fc,
                        "amount_left" => $amount_netto - $amount_paired_fc
                    ];

                    $amount_left += $amount_netto - $amount_paired_fc;
                    $amount_left_eur += $row_invoices->in_amount_netto_eur - $amount_paired;
                }
            }

            if ($row->cu_skip_for_overdue_cronjob == 1)
            {
                $row->cu_skip_for_overdue_cronjob = 0;
                $row->save();
            }
            else {
                if ($row->cu_credit_hold == 0 && $amount_left_eur > 0)
                {
                    //Set customer credit hold
                    $row->cu_credit_hold = 1;
                    $row->cu_credit_hold_timestamp = date("Y-m-d H:i:s");
                    $row->save();

                    if (!empty($row->cu_email_bi))
                    {
                        $email = $row->cu_email_bi;
                    }
                    else {
                        $email = $row->cu_email;
                    }

                    $fields = [
                        'email' => $email,
                        'invoices' => $invoices,
                        'amount_left' => $amount_left,
                        'amount_left_eur' => $amount_left_eur,
                        "currency" => $row->cu_pacu_code,
                    ];

                    //Send email
                    $mail->send("invoice_overdue_ch", $row->cu_co_code, $fields);
                }
            }
        }
    }
}
