<?php

namespace App\Console\Commands;

use App\Cronjobs\CronjobController;
use App\Functions\Mail;
use App\Functions\System;
use App\Models\Request;
use App\Models\Survey2;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class Survey2Invites extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronjob:survey_2_invites';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    private $cc;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CronjobController $cc)
    {
        $this->cc = $cc;
        parent::__construct();
    }


    const CRONJOB_ID = 37;
    const CRONJOB_TASK = 'Mailing';
    const CRONJOB_SUBTASK = 'Send Survey 2 Invites';
    const CRONJOB_TRIGGER = 3;
    const CRONJOB_INTERVAL = "60 MINUTE";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Set Cronjob check values
        $CRONJOB_USERS = [3653];

        try
        {
            $this->sendInvites();
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 0, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, null);

        }catch (\Throwable $e){
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 1, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, $e);
        }
    }

    private function sendInvites(): void
    {
        $system = new System();
        $mail = new Mail();

        $query = Request::leftJoin("kt_request_customer_portal", "ktrecupo_re_id", "re_id")
            ->leftJoin("customers", "ktrecupo_cu_id", "cu_id")
            ->where("re_status", 1)
            ->where("cu_type", "!=", 6)
            ->whereRaw("`re_moving_date` = '" . date("Y-m-d", strtotime("-14 days")) . "' AND	HOUR(`requests`.`re_timestamp`) <= HOUR(NOW())")
            ->get();

        $query = $system->databaseToArray($query);

        foreach ($query as $row)
        {
            $query_survey = Survey2::select("su_id")
                ->where("su_re_id", $row['re_id'])
                ->get();

            if (count($query_survey) == 0)
            {
                $survey_2 = new Survey2();
                $survey_2->su_source = 1;
                $survey_2->su_re_id = $row['re_id'];
                $survey_2->su_timestamp = date("Y-m-d H:i:s");
                $survey_2->save();

                $fields = [
                    "request" => $row,
                ];

                $mail->send("survey_2_invite", $row['re_la_code'], $fields);
            }
        }

        $query = Survey2::leftJoin("requests", "su_re_id", "re_id")
            ->where("su_submitted", 1)
            ->where("su_moved", 3)
            ->where("su_new_moving_date", date("Y-m-d", strtotime("-14 days")))
            ->whereRaw("HOUR(`requests`.`re_timestamp`) <= HOUR(NOW())")
            ->get();

        $query = $system->databaseToArray($query);

        foreach ($query as $row)
        {
            DB::table('surveys_2')
                ->where('su_id', $row['su_id'])
                ->update(
                    [
                        'su_timestamp' => DB::raw("NOW()"),
                        'su_opened' => "",
                        "su_opened_timestamp" => "",
                        "su_submitted" => "",
                        "su_submitted_timestamp" => "",
                        "su_sent" => DB::raw("(`su_sent` + 1)"),
                        "su_moved" => "",
                        "su_new_moving_date" => "",
                        "su_checked" => "",
                        "su_checked_remarks" => "",
                    ]
                );

            $fields = [
                "request" => $row
            ];

            $mail->send("survey_2_invite", $row['re_la_code'], $fields);
        }

        //Get all unsubmitted surveys of 7 days ago and check if reminder is not sent yet.
        $query = Survey2::leftJoin("requests", "su_re_id", "re_id")
            ->where("su_submitted", 0)
            ->where("su_reminder", 0)
            ->whereRaw("`su_timestamp` BETWEEN '" . date("Y-m-d H:i:s", strtotime("-7 days" . " 00:00:00")) . "' AND '" . date("Y-m-d H:i:s", strtotime("-7 days" . " 23:59:59")) . "'")
            ->whereRaw("HOUR(`requests`.`re_timestamp`) <= HOUR(NOW())")
            ->get();

        $query = $system->databaseToArray($query);

        foreach ($query as $row)
        {
            DB::table('surveys_2')
                ->where('su_id', $row['su_id'])
                ->update(
                    [
                        'su_timestamp' => DB::raw("NOW()"),
                        'su_opened' => "",
                        "su_opened_timestamp" => "",
                        "su_submitted" => "",
                        "su_submitted_timestamp" => "",
                        "su_sent" => DB::raw("(`su_sent` + 1)"),
                        "su_moved" => "",
                        "su_new_moving_date" => "",
                        "su_checked" => "",
                        "su_checked_remarks" => "",
                        "su_reminder" => 1,
                    ]
                );

            $fields = [
                "request" => $row
            ];

            //Send reminder mail.
            $mail->send("survey_2_reminder", $row['re_la_code'], $fields);
        }
    }
}

