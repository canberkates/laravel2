<?php

namespace App\Console\Commands;

use App\Cronjobs\CronjobController;
use App\Functions\Mail;
use App\Functions\RequestData;
use App\Functions\RequestDeliveries;
use App\Functions\System;
use App\Models\KTRequestCustomerQuestion;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ServiceProviderRequests extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronjob:service_provider_requests';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    private $cc;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CronjobController $cc)
    {
        $this->cc = $cc;
        parent::__construct();
    }


    const CRONJOB_ID = 30;
    const CRONJOB_TASK = 'Service Provider';
    const CRONJOB_SUBTASK = 'Send Service Provider Requests';
    const CRONJOB_TRIGGER = 1;
    const CRONJOB_INTERVAL = "1 HOUR";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Set Cronjob check values
        $CRONJOB_USERS = [3653];

        try
        {
            $this->sendServiceProviderRequests();
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 0, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, null);

        }catch (\Throwable $e){
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 1, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, $e);
        }

    }

    private function sendServiceProviderRequests(): void
    {
        $system = new System();
        $mail = new Mail();
        $requestdata = new RequestData();
        $requestdeliveries = new RequestDeliveries();

        $query = KTRequestCustomerQuestion::leftJoin("kt_customer_question", "ktrecuqu_ktcuqu_id", "ktcuqu_id")
            ->leftJoin("requests", "ktrecuqu_re_id", "re_id")
            ->leftJoin("kt_request_customer_portal", "ktrecuqu_re_id", "ktrecupo_re_id")
            ->leftJoin("customers", "ktrecuqu_cu_id", "cu_id")
            ->where("ktrecuqu_sent", 0)
            ->where("ktrecuqu_hide", 0)
            ->where("cu_type", 2)
            ->where("cu_deleted", 0)
            ->whereRaw("(
			(
				`requests`.`re_status` = '1' AND
				`kt_request_customer_portal`.`ktrecupo_timestamp` <= TIMESTAMP(NOW() - INTERVAL `kt_customer_question`.`ktcuqu_delay` HOUR)
			)
			OR
			(
				`requests`.`re_status` = '2' AND
				`kt_request_customer_question`.`ktrecuqu_timestamp` <= TIMESTAMP(NOW() - INTERVAL `kt_customer_question`.`ktcuqu_delay` HOUR)
			)
		)")
            ->groupBy("ktrecuqu_id")
            ->get();

        $query = $system->databaseToArray($query);

        foreach ($query as $row)
        {
            if ($row['re_status'] == 1 || ($row['re_status'] == 2 && $row['ktrecuqu_rejection_status'] == 1))
            {
                DB::table('kt_request_customer_question')
                    ->where('ktrecuqu_id', $row['ktrecuqu_id'])
                    ->update(
                        [
                            'ktrecuqu_rejection_status' => 0,
                            'ktrecuqu_sent' => 1,
                            'ktrecuqu_sent_timestamp' => DB::raw("NOW()"),
                        ]
                    );

                foreach ($requestdeliveries->receivers(2, $row['ktcuqu_id'], $row['cu_email']) as $receiver)
                {
                    $fields = [
                        "request_delivery_value" => $receiver['value'],
                        "request_delivery_extra" => $receiver['extra'],
                        "cu_re_id" => $row['ktrecuqu_cu_re_id'],
                        "origin" => $requestdata->requestWebsiteField($row['re_id'], "we_mail_from_name"),
                        "request" => $row
                    ];

                    if ($receiver['type'] == 1)
                    {
                        $mail->send("service_provider_request", $row['cu_la_code'], $fields);
                    } else
                    {
                        $requestdeliveries->send(2, $receiver['type'], $row['cu_la_code'], $fields);
                    }
                }
            } elseif ($row['re_status'] == 2 && $row['ktrecuqu_rejection_status'] == 2)
            {
                DB::table('kt_request_customer_question')
                    ->where('ktrecuqu_id', $row['ktrecuqu_id'])
                    ->update(
                        [
                            'ktrecuqu_rejection_status' => 0,
                            'ktrecuqu_hide' => 1
                        ]
                    );

                if (date("m") == date("m", strtotime($row['ktrecuqu_timestamp'])))
                {
                    DB::table('kt_customer_question')
                        ->where('ktcuqu_id', $row['ktcuqu_id'])
                        ->update(
                            [
                                'ktcuqu_max_requests_month_left' => DB::raw("(`ktcuqu_max_requests_month_left` + 1)")
                            ]
                        );
                }

                if (date("d") == date("d", strtotime($row['ktrecuqu_timestamp'])))
                {
                    DB::table('kt_customer_question')
                        ->where('ktcuqu_id', $row['ktcuqu_id'])
                        ->update(
                            [
                                'ktcuqu_max_requests_day_left' => DB::raw("(`ktcuqu_max_requests_day_left` + 1)")
                            ]
                        );
                }
            }
        }
    }
}

