<?php

namespace App\Console\Commands;

use App\Cronjobs\CronjobController;
use App\Http\Controllers\SireloSynchroniseAllController;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Log;

class SireloSyncAll extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sirelo:syncall';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cronjob that syncs the data to the Sirelo.org servers';

    private $cc;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CronjobController $cc)
    {
        $this->cc = $cc;
        parent::__construct();
    }

    const CRONJOB_ID = 33;
    const CRONJOB_TASK = 'Sirelo';
    const CRONJOB_SUBTASK = 'Sync Sirelo.org';
    const CRONJOB_TRIGGER = 2;
    const CRONJOB_INTERVAL = "1 DAY";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Set Cronjob check values
        $CRONJOB_USERS = [3653];

        try
        {
            $this->syncSireloOrg();
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 0, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, null);

        }catch (\Throwable $e){
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 1, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, $e);
        }


    }

    private function syncSireloOrg(): void
    {
        //Set-up request reject POST
        $sync_request = new \Illuminate\Http\Request();
        $sync_request->setMethod('POST');

        $ssc = new SireloSynchroniseAllController();
        $ssc->update($sync_request);
    }

}
