<?php

namespace App\Console\Commands;

use App\Cronjobs\CronjobController;
use App\Functions\DataIndex;
use App\Functions\Mail;
use App\Functions\Mover;
use App\Functions\System;
use App\Models\Customer;
use App\Models\KTRequestCustomerPortal;
use Carbon\Carbon;
use DateTime;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CustomerFinanceChecks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronjob:customer_finance_checks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    private $cc;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CronjobController $cc)
    {
        $this->cc = $cc;
        parent::__construct();
    }

    const CRONJOB_ID = 9;
    const CRONJOB_TASK = 'Finance';
    const CRONJOB_SUBTASK = 'Check Prepayment Customer Balance';
    const CRONJOB_TRIGGER = 20;
    const CRONJOB_INTERVAL = "5 MINUTE";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        //Set Cronjob check values
        $CRONJOB_USERS =  DataIndex::financeUsers();

        try
        {
            $this->checkBalance();
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 0, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, null);

        }catch (\Throwable $e){
            $this->cc->CronjobWatchdog(self::CRONJOB_ID, self::CRONJOB_TASK, self::CRONJOB_SUBTASK, self::CRONJOB_TRIGGER, 1, $CRONJOB_USERS, Carbon::now()->toDateTimeString(), self::CRONJOB_INTERVAL, $e);
        }

    }

    private function checkBalance(): void
    {
        $system = new System();
        $mover = new Mover();
        $dataindex = new DataIndex();
        $mail = new Mail();

        $qu_customers = Customer::select("cu_id", "cu_company_name_legal", "cu_pacu_code", "cu_credit_limit", "cu_last_payment_notification", "cu_enforce_credit_limit", "cu_debt_manager")
            ->leftJoin("kt_customer_portal", "cu_id", "ktcupo_cu_id")
            ->where("cu_credit_hold", 0)
            ->where("cu_deleted", 0)
            ->where("ktcupo_status", 1)
            ->get();

        $query_customers = $system->databaseToArray($qu_customers);
        $customers = [];

        foreach ($query_customers as $row_customers)
        {
            $customers[$row_customers['cu_id']] = $row_customers;

            $customers[$row_customers['cu_id']]['avg_price'] = 0;
            $customers[$row_customers['cu_id']]['per_day'] = 0;
            $customers[$row_customers['cu_id']]['days_left'] = 0;
        }

        if (!empty($customers))
        {
            $finance_users = array_keys($dataindex->financeUsers());
            $balances = $mover->customerBalancesFC(array_keys($customers));

            $qu_ktrecupo = KTRequestCustomerPortal::selectRaw("ktrecupo_cu_id, COUNT(*) AS `requests`, SUM(`ktrecupo_amount_netto`) AS `amount`")
                ->where("ktrecupo_type", 1)
                ->whereIn("ktrecupo_cu_id", array_keys($customers))
                ->whereRaw("`ktrecupo_timestamp` BETWEEN NOW() - INTERVAL 7 DAY AND NOW()")
                ->groupBy("ktrecupo_cu_id")
                ->get();

            $query_ktrecupo = $system->databaseToArray($qu_ktrecupo);

            foreach ($query_ktrecupo as $row_ktrecupo)
            {
                $cu_id = $row_ktrecupo['ktrecupo_cu_id'];

                $customers[$cu_id]['avg_price'] = round($row_ktrecupo['amount'] / $row_ktrecupo['requests'], 2);
                $customers[$cu_id]['per_day'] = round($row_ktrecupo['amount'] / 7, 2);
            }

            foreach ($customers as $cu_id => $customer)
            {
                $customer['balance'] = round($system->useOr($balances[$cu_id], 0), 2);
                $customer['to_spend'] = round($customer['balance'] + $customer['cu_credit_limit'], 2);

                if ($customer['per_day'] > 0)
                {
                    $customer['days_left'] = round($customer['to_spend'] / $customer['per_day']);
                }

                if ($customer['to_spend'] < 0)
                {
                    //Get payment method of the customer
                    $customer_payment_method_query = Customer::select("cu_payment_method", "cu_company_name_business", "cu_la_code", "cu_email", "cu_prepayment_ran_out_mail_timestamp", "cu_credit_limit_increase_timestamp")
                        ->where("cu_id", $cu_id)
                        ->first();
                    $company = $system->databaseToArray($customer_payment_method_query);

                    //If payment method is Prepayment (4), then send mail
                    if ($company['cu_payment_method'] == 4 && empty($company['cu_credit_limit_increase_timestamp']))
                    {
                        $date_now = date("Y-m-d H:i:s");
                        $date_sent = $customer['cu_prepayment_ran_out_mail_timestamp'];

                        $date1 = new DateTime(date('Y-m-d', strtotime($date_sent)));
                        $date2 = new DateTime(date('Y-m-d', strtotime($date_now)));

                        //Calculate days difference
                        $days_difference = $date1->diff($date2)->days;

                        //if the mail has been sent 14 days ago (or more)
                        if ($days_difference >= 14 || $company['cu_prepayment_ran_out_mail_timestamp'] == null)
                        {
                            $fields = [
                                "company_name" => $company['cu_company_name_business'],
                                "email" => $company['cu_email']
                            ];

                            //Send mail
                            $mail->send("prepayment_ran_out", $customer['cu_la_code'], $fields);

                            //Update sent mail timestamp
                            DB::table('customers')
                                ->where('cu_id', $cu_id)
                                ->update(
                                    [
                                        'cu_prepayment_ran_out_mail_timestamp' => date("Y-m-d H:i:s")
                                    ]
                                );

                            $system->sendMessage(3653, "Prepayment ran out (balance < 0)", System::serialize($fields));
                        }

                    }

                    $customer_row = Customer::where("cu_id", $cu_id)->first();
                    $customer_row->cu_credit_hold = 1;
                    $customer_row->cu_credit_hold_timestamp = date("Y-m-d H:i:s");
                    $customer_row->save();

                    $subject = (($customer['cu_enforce_credit_limit'] == 1) ? "Enforced" : "Prepayment") . " credit limit has been reached";
                    $message = "Customer <strong>" . $customer['cu_company_name_legal'] . "</strong> has been changed to credit hold, because the credit limit <strong>(" . $system->paymentCurrencyToken($customer['cu_pacu_code']) . " " . $system->numberFormat($customer['cu_credit_limit'], 2) . ")</strong> has been reached. The current balance (incl. not invoiced) is <strong>" . $system->paymentCurrencyToken($customer['cu_pacu_code']) . " " . $system->numberFormat($customer['balance'], 2) . "</strong>.";


                    if (!empty($customer['cu_debt_manager'])) {
                        //Add debt manager to $finance_users array
                        $finance_users[] = $customer['cu_debt_manager'];
                    }

                    $system->sendMessage($finance_users, $subject, $message);

                } elseif ($customer['per_day'] > 0 && $customer['days_left'] <= 4 && $system->dateDifference($customer['cu_last_payment_notification'], "today") > 7)
                {
                    DB::table('customers')
                        ->where('cu_id', $cu_id)
                        ->update(
                            [
                                'cu_last_payment_notification' => date("Y-m-d H:i:s")
                            ]
                        );

                    $currency = $system->paymentCurrencyToken($customer['cu_pacu_code']) . " ";

                    $subject = (($customer['cu_enforce_credit_limit'] == 1) ? "Enforced" : "Prepayment") . " credit limit is going to run out";
                    $message = "Customer <strong>" . $customer['cu_company_name_legal'] . "</strong> is going to run out of credit soon (approx. 4 days). Amount left to spend: <strong>" . $currency . $system->numberFormat($customer['to_spend'], 2) . "</strong> (Average spend per day: " . $currency . $system->numberFormat($customer['per_day']) . ").";

                    $system->sendMessage($finance_users, $subject, $message);
                }
            }
        }
    }
}

