<?php

namespace App\Providers;

use App\Data\CustomerRestrictions;
use App\Models\Customer;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Session;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

		// Implicitly grant "Super Admin" role all permissions
		// This works in the app by using gate-related functions like auth()->user->can() and @can()
		Gate::before(function ($user, $ability) {
			return $user->hasRole('admin') ? true : null;
		});

        Gate::define("customer-restrictions", function (User $user, Customer $customer) {
            $customerRestrictions = CustomerRestrictions::all();

            if (isset($customerRestrictions[$user->id])) {
                if (!in_array($customer->cu_co_code, $customerRestrictions[$user->id])) {
                    return false;
                }
            }

            return true;
        });
    }
}
