<?php


namespace App\Providers;


use Illuminate\Console\Events\CommandFinished;

final class AddFinishedToOutput
{
    public function handle(CommandFinished $event) : void{
        $event->output->write('
   __      _    _                                            _    _
  / /  ___| | _| | _____ _ __    __ _  _____      _____ _ __| | _| |_
 / /  / _ \ |/ / |/ / _ \ \'__|  / _` |/ _ \ \ /\ / / _ \ \'__| |/ / __|
/ /__|  __/   <|   <  __/ |    | (_| |  __/\ V  V /  __/ |  |   <| |_
\____/\___|_|\_\_|\_\___|_|     \__, |\___| \_/\_/ \___|_|  |_|\_\\__|
                                |___/
        ');
    }
}
