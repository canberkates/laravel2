<?php


namespace App\Providers;


use App\Data\RequestType;
use App\Models\Claim;
use App\Models\CustomerReviewScore;
use App\Models\MobilityexFetchedData;
use App\Models\Request;
use App\Models\Survey2;
use App\Models\User;
use DB;
use Illuminate\Support\ServiceProvider;
use Log;
use Schema;
use View;

class MenuAmountsServiceProvider extends ServiceProvider
{
    public function boot()
    {
        // Get all users with heartbeat
        $users = User::select("id", "us_name", "us_telephone_internal")->where("us_is_deleted", 0)->orderBy("us_name", "asc")->get();

        $last_actives_query = DB::table("sessions")->select("user_id", "last_activity")->get();
        $last_actives = [];

        // Loop last activities
        foreach( $last_actives_query as $activity ) {

            // Store last activities from laravel
            $last_actives[$activity->user_id] = $activity->last_activity;
        }

        // Loop users
        if (Schema::hasColumn('requests', 're_automatic'))
        {
            // Get amount of requests where status is 0 and request type is normal
            $open_requests_count = Request::selectRaw('count(*) as `count`')
                ->where("re_status", "=", 0)
                ->where("re_automatic", "=", 0)
                ->whereIn("re_request_type", [RequestType::MOVE, RequestType::BAGGAGE, RequestType::TRANSPORT, RequestType::OFFICE_MOVE])
                ->first();
        }
        // Get amount of requests where status is 0 and request type is normal
        $open_premium_leads_count = Request::selectRaw('count(*) as `count`')
            ->where("re_status", "=", 0)
            ->where("re_request_type", "=", 5)
            ->first();

        // Get amount of claims where status is 0
        $open_claims_count = Claim::selectRaw('count(*) as `count`')
            ->where("cl_status", "=", 0)
            ->first();

        // Get amount of surveys that aren't checked yet
        $open_surveys_count = Survey2::selectRaw('count(*) as `count`')
            ->where("su_submitted", "=", 1)
            ->where("su_checked", "=", 0)
            ->where("su_on_hold", "=", 0)
            ->first();

        //Get amount of open Google validation records
        $count_google_related = CustomerReviewScore::where("curesc_processed", 0)
            ->whereIn("curesc_type", [1])
            ->get()->count();

        //Get amount of open MobilityEx validation records
        /*$count_mobilityex = MobilityexFetchedData::where("mofeda_processed", 0)
            ->whereIn("mofeda_type", [1,2,3])
            ->get()->count();*/

        //Total open validation rows
        //$open_validation_count = $count_google_related + $count_mobilityex;

        // Build globaldata array
        $data = [
            'menu_open_requests' => $open_requests_count->count,
            'menu_open_premium_leads' => $open_premium_leads_count->count,
            'menu_open_claims' => $open_claims_count->count,
            'menu_open_surveys' => $open_surveys_count->count,
            'menu_open_validation' => $count_google_related
        ];

        // Share the globaldata array with all views
        View::share( 'globaldata', $data );

    }
}
