<?php

namespace App\Providers;

use App\RequestDeliveries\DesBrasEnPlus;
use App\RequestDeliveries\HTTPPostDelivery;
use App\RequestDeliveries\JSONPostDelivery;
use App\RequestDeliveries\LegacyDeliveryHandler;
use App\RequestDeliveries\Movegistics;
use App\RequestDeliveries\MoveManPro;
use App\RequestDeliveries\MoveWare;
use App\RequestDeliveries\NCVPostDelivery;
use App\RequestDeliveries\P4P;
use App\RequestDeliveries\PARC;
use App\RequestDeliveries\RequestDeliverySender;
use App\RequestDeliveries\Scout24PostDelivery;
use Illuminate\Support\ServiceProvider;

class RequestDeliveryServiceProvider extends ServiceProvider
{

    public const TAG = "requestDelivery";

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

        //TODO: Everytime a new Delivery is implemented, add the class to this list
        $classes = [
            MoveWare::class,
            MoveManPro::class,
            Movegistics::class,
            DesBrasEnPlus::class,
            HTTPPostDelivery::class,
            LegacyDeliveryHandler::class,
            JSONPostDelivery::class,
            NCVPostDelivery::class,
            Scout24PostDelivery::class,
            P4P::class,
            PARC::class
        ];

        $this->app->tag($classes, static::TAG);

        $this->app->bind(RequestDeliverySender::class, function (){
            return new RequestDeliverySender(
                $this->app->tagged(RequestDeliveryServiceProvider::TAG)
            );
        });

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
