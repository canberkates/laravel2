<?php


namespace App\Providers;


use Illuminate\Console\Events\CommandStarting;

final class AddLogoToOutput
{
    public function handle(CommandStarting $event) : void{
        $event->output->write('
 _ __   __ _  __ _ _ __   _ __   ___ _ __ ___   ___ _ __
| \'_ \ / _` |/ _` | \'__| | \'_ \ / _ \ \'_ ` _ \ / _ \ \'_ \
| |_) | (_| | (_| | |    | | | |  __/ | | | | |  __/ | | |
| .__/ \__,_|\__,_|_|    |_| |_|\___|_| |_| |_|\___|_| |_|
| |
|_|
        ');
    }
}
