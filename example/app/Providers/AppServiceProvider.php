<?php

namespace App\Providers;

use App\Data\RequestType;
use App\Extensions\LifetimeSessionHandler;
use App\Functions\Translation;
use App\Models\Claim;
use App\Models\Message;
use App\Models\Request;
use App\Models\Survey2;
use DB;
use Illuminate\Session\DatabaseSessionHandler;
use Illuminate\Support\Facades\App;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Log;
use View;
use Illuminate\Support\ServiceProvider;
use Blade;
use URL;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        error_reporting(E_ERROR | E_PARSE);

        // Custom blade directive for pushonce
        // Use @pushonce('scripts:datatables')
        //     @pushonce('{stack}:{groupName}')
        Blade::directive('pushonce', function ($expression) {
            [$pushName, $pushSub] = explode(':', trim(substr($expression, 1, -1)));
            $key = '__pushonce_' . str_replace('-', '_', $pushName) . '_' . str_replace('-', '_', $pushSub);

            return "<?php if(! isset(\$__env->{$key})): \$__env->{$key} = 1; \$__env->startPush('{$pushName}'); ?>";
        });

        // Custom blade directive for the endpushonce
        Blade::directive('endpushonce', function () {
            return '<?php $__env->stopPush(); endif; ?>';
        });

        // Blade directive for the translate function
		Blade::directive( 'translate', function( $expression ){

			$expression = str_replace( "'", '', $expression );
			$expression = str_replace( "\"", '', $expression );

			[$key, $language] = explode(', ', $expression);

			$translation = new Translation();

			return $translation->get(  trim( $key  ),trim( $language ) );
		});

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RepositoryServiceProvider::class);
        $this->app->register(TaggingServiceProvider::class);
    }
}
