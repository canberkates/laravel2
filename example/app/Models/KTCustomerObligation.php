<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\KTCustomerMembership
 *
 * @property-read \App\Models\Obligation $obligation
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerMembership newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerMembership newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerMembership query()
 * @mixin \Eloquent
 * @property int $ktcuob_id
 * @property int $ktcuob_cuob_id
 * @property int $ktcuob_cu_id
 * @property int|null $ktcuob_cudo_id
 * @property string $ktcuob_verified_timestamp
 * @property int $ktcuob_verification_result
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerObligation whereKtcuobCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerObligation whereKtcuobCudoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerObligation whereKtcuobCuobId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerObligation whereKtcuobId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerObligation whereKtcuobVerificationResult($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerObligation whereKtcuobVerifiedTimestamp($value)
 */
class KTCustomerObligation extends Model
{
    protected $table = 'kt_customer_obligations';
    protected $primaryKey = 'ktcuob_id';
    public $timestamps = false;

    public function obligation()
    {
        return $this->belongsTo('App\Models\Obligation', 'ktcuob_cuob_id', 'cuob_id');
    }

}
