<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Chat
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat query()
 * @mixin \Eloquent
 * @property int $ch_id
 * @property int|null $ch_us_id_from
 * @property int|null $ch_us_id_to
 * @property string $ch_timestamp
 * @property int $ch_read
 * @property string $ch_message
 * @property-read \App\Models\User|null $sentfrom
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat whereChId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat whereChMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat whereChRead($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat whereChTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat whereChUsIdFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat whereChUsIdTo($value)
 */
class Chat extends Model
{
    protected $table = 'chat';
	protected $primaryKey = 'ch_id';
	public $timestamps = false;
	
	public function sentfrom(){
		return $this->belongsTo('App\Models\User', 'ch_us_id_from', 'id');
	}
}
