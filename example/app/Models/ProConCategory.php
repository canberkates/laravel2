<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ProConCategory
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProConCategory con()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProConCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProConCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProConCategory pro()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProConCategory query()
 * @mixin \Eloquent
 * @property int $prcoca_id
 * @property int $prcoca_type
 * @property string $prcoca_name
 * @property string $prcoca_category
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProConCategory wherePrcocaCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProConCategory wherePrcocaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProConCategory wherePrcocaName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProConCategory wherePrcocaType($value)
 */
class ProConCategory extends Model
{
    protected $table = 'pro_con_categories';
	protected $primaryKey = 'prcoca_id';
	public $timestamps = false;
	
	public function scopePro($query)
	{
		return $query->where('prcoca_type', '=', 1);
	}
	
	public function scopeCon($query)
	{
		return $query->where('prcoca_type', '=', 2);
	}
}
