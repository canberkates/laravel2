<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Application
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Application newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Application newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Application query()
 * @mixin \Eloquent
 * @property int $ap_id
 * @property string $ap_name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Application whereApId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Application whereApName($value)
 */
class Application extends Model
{
   	protected $table = 'applications';
	protected $primaryKey = 'ap_id';
	public $timestamps = false;
}
