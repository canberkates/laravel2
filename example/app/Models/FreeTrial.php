<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\FreeTrial
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FreeTrial newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FreeTrial newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FreeTrial query()
 * @mixin \Eloquent
 * @property int $frtr_id
 * @property int|null $frtr_ktcupo_id
 * @property int|null $frtr_us_id
 * @property string $frtr_date
 * @property int $frtr_stop_type
 * @property string $frtr_stop_date
 * @property int $frtr_stop_requests
 * @property int $frtr_stop_requests_left
 * @property string|null $frtr_remark
 * @property int $frtr_finished
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FreeTrial whereFrtrDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FreeTrial whereFrtrFinished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FreeTrial whereFrtrId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FreeTrial whereFrtrKtcupoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FreeTrial whereFrtrRemark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FreeTrial whereFrtrStopDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FreeTrial whereFrtrStopRequests($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FreeTrial whereFrtrStopRequestsLeft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FreeTrial whereFrtrStopType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FreeTrial whereFrtrUsId($value)
 */
class FreeTrial extends Model
{
    protected $table = 'free_trials';
	protected $primaryKey = 'frtr_id';
	public $timestamps = false;
}
