<?php

namespace App\Models;

use Fico7489\Laravel\RevisionableUpgrade\Traits\RevisionableUpgradeTrait;
use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\AffiliatePartnerFormStep
 *
 * @property int $afpafost_id
 * @property int $afpafost_afpafo_id
 * @property string $afpafost_date
 * @property int $afpafost_step
 * @property int $afpafost_visits
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerFormStep newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerFormStep newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerFormStep query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerFormStep whereAfpafostAfpafoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerFormStep whereAfpafostDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerFormStep whereAfpafostId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerFormStep whereAfpafostStep($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerFormStep whereAfpafostVisits($value)
 * @mixin \Eloquent
 */
class AffiliatePartnerFormStep extends Model
{
    protected $table = 'affiliate_partner_form_steps';
	protected $primaryKey = 'afpafost_id';
	public $timestamps = false;

}
