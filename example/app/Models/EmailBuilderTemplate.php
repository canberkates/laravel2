<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\EmailBuilderTemplate
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmailBuilderTemplate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmailBuilderTemplate newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmailBuilderTemplate query()
 * @mixin \Eloquent
 * @property int $embute_id
 * @property string $embute_timestamp
 * @property string $embute_language
 * @property string $embute_name
 * @property string $embute_description
 * @property string $embute_filename
 * @property string $embute_from_email
 * @property string|null $embute_to_cc
 * @property string|null $embute_to_bcc
 * @property string|null $embute_subject
 * @property string|null $embute_content_html
 * @property int $embute_created_by
 * @property int $embute_last_updated_by
 * @property string $embute_last_updated_timestamp
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmailBuilderTemplate whereEmbuteContentHtml($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmailBuilderTemplate whereEmbuteCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmailBuilderTemplate whereEmbuteDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmailBuilderTemplate whereEmbuteFilename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmailBuilderTemplate whereEmbuteFromEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmailBuilderTemplate whereEmbuteId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmailBuilderTemplate whereEmbuteLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmailBuilderTemplate whereEmbuteLastUpdatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmailBuilderTemplate whereEmbuteLastUpdatedTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmailBuilderTemplate whereEmbuteName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmailBuilderTemplate whereEmbuteSubject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmailBuilderTemplate whereEmbuteTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmailBuilderTemplate whereEmbuteToBcc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmailBuilderTemplate whereEmbuteToCc($value)
 */
class EmailBuilderTemplate extends Model
{
    protected $table = 'email_builder_templates';
    protected $primaryKey = 'embute_id';
    public $timestamps = false;


}
