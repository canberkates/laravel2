<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SireloCustomersToCountries
 *
 * @property string $customer_key
 * @property int $origin_id
 * @property string $country_code
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomersToCountries newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomersToCountries newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomersToCountries query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomersToCountries whereCountryCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomersToCountries whereCustomerKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomersToCountries whereOriginId($value)
 * @mixin \Eloquent
 */
class SireloCustomersToCountries extends Model
{
    protected $table = 'sirelo_customers_to_countries';
    public $timestamps = false;

}
