<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\KTServiceProviderAdvertorialBlockLanguageContent
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderAdvertorialBlockLanguageContent newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderAdvertorialBlockLanguageContent newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderAdvertorialBlockLanguageContent query()
 * @mixin \Eloquent
 * @property int $ktsepradbllaco_id
 * @property int $ktsepradbllaco_sepradbl_id
 * @property string $ktsepradbllaco_la_code
 * @property string|null $ktsepradbllaco_title
 * @property string|null $ktsepradbllaco_content
 * @property string|null $ktsepradbllaco_button
 * @property string $ktsepradbllaco_url
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderAdvertorialBlockLanguageContent whereKtsepradbllacoButton($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderAdvertorialBlockLanguageContent whereKtsepradbllacoContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderAdvertorialBlockLanguageContent whereKtsepradbllacoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderAdvertorialBlockLanguageContent whereKtsepradbllacoLaCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderAdvertorialBlockLanguageContent whereKtsepradbllacoSepradblId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderAdvertorialBlockLanguageContent whereKtsepradbllacoTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderAdvertorialBlockLanguageContent whereKtsepradbllacoUrl($value)
 */
class KTServiceProviderAdvertorialBlockLanguageContent extends Model
{
    protected $table = 'kt_service_provider_advertorial_block_language_content';
    protected $primaryKey = 'ktsepradbllaco_id';
    public $timestamps = false;
}
