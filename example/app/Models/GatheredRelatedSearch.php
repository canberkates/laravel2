<?php

namespace App\Models;

use Fico7489\Laravel\RevisionableUpgrade\Traits\RevisionableUpgradeTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\GatheredRelatedSearch
 *
 * @property-read \App\Models\Customer $customer
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatheredRelatedSearch newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatheredRelatedSearch newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatheredRelatedSearch query()
 * @property int $garese_id
 * @property string $garese_searched_customer
 * @property string $garese_searched_city
 * @property string $garese_searched_co_code
 * @property string $garese_related_search
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatheredRelatedSearch whereGareseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatheredRelatedSearch whereGareseRelatedSearch($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatheredRelatedSearch whereGareseSearchedCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatheredRelatedSearch whereGareseSearchedCoCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatheredRelatedSearch whereGareseSearchedCustomer($value)
 * @mixin \Eloquent
 */
class GatheredRelatedSearch extends Model
{
    protected $table = 'gathered_related_search';
    protected $primaryKey = 'garese_id';
    public $timestamps = false;


}
