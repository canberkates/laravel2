<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Obligation
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Membership newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Membership newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Membership query()
 * @mixin \Eloquent
 * @property int $cuob_id
 * @property int $cuob_type
 * @property string $cuob_co_code
 * @property string $cuob_name
 * @property string $cuob_remark
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Obligation whereCuobCoCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Obligation whereCuobId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Obligation whereCuobName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Obligation whereCuobRemark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Obligation whereCuobType($value)
 */
class Obligation extends Model
{
    protected $table = 'customer_obligations';
    protected $primaryKey = 'cuob_id';
    public $timestamps = false;


}
