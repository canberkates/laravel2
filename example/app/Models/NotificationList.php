<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\NotificationList
 *
 * @property int $noli_id
 * @property string $noli_name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotificationList newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotificationList newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotificationList query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotificationList whereNoliId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotificationList whereNoliName($value)
 * @mixin \Eloquent
 */
class NotificationList extends Model
{
    protected $table = 'notification_lists';
    protected $primaryKey = 'noli_id';
    public $timestamps = false;
}