<?php

namespace App\Models;

use Fico7489\Laravel\RevisionableUpgrade\Traits\RevisionableUpgradeTrait;
use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\Portal
 *
 * @property-read \App\Models\Country $country
 * @property-read \App\Models\KTCustomerPortal $customerportal
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Region[] $regions
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Portal newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Portal newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Portal query()
 * @mixin \Eloquent
 * @property int $po_id
 * @property string $po_portal
 * @property int $po_destination_type
 * @property string|null $po_co_code
 * @property string $po_pacu_code
 * @property int|null $po_default_website
 * @property-read int|null $regions_count
 * @property-read int|null $revision_history_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Portal wherePoCoCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Portal wherePoDefaultWebsite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Portal wherePoDestinationType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Portal wherePoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Portal wherePoPacuCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Portal wherePoPortal($value)
 * @property float|null $po_minimum_price_int
 * @property float|null $po_maximum_price_int
 * @property float|null $po_minimum_price_nat_local
 * @property float|null $po_maximum_price_nat_local
 * @property float|null $po_minimum_price_nat_long_distance
 * @property float|null $po_maximum_price_nat_long_distance
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Portal wherePoMaximumPriceInt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Portal wherePoMaximumPriceNatLocal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Portal wherePoMaximumPriceNatLongDistance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Portal wherePoMinimumPriceInt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Portal wherePoMinimumPriceNatLocal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Portal wherePoMinimumPriceNatLongDistance($value)
 */
class Portal extends Model
{
    protected $table = 'portals';
	protected $primaryKey = 'po_id';
	public $timestamps = false;
	
	use RevisionableTrait;
	use RevisionableUpgradeTrait;
	
	public function customerportal()
	{
		return $this->hasOne('App\Models\KTCustomerPortal', 'ktcupo_po_id', 'po_id');
	}
	
	public function regions(){
		return $this->hasMany('App\Models\Region', 'reg_po_id', 'po_id');
	}
	
	public function country(){
		return $this->hasOne('App\Models\Country', 'co_code', 'po_co_code');
	}
}
