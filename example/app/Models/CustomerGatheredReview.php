<?php

namespace App\Models;

use Fico7489\Laravel\RevisionableUpgrade\Traits\RevisionableUpgradeTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\CustomerGatheredReview
 *
 * @property-read \App\Models\Customer $customer
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerGatheredReview newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerGatheredReview newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerGatheredReview query()
 * @property int $cugare_id
 * @property int $cugare_curesc_id
 * @property int $cugare_cu_id
 * @property string $cugare_platform
 * @property float $cugare_score
 * @property int $cugare_amount
 * @property string|null $cugare_url
 * @property int $cugare_accepted
 * @property int $cugare_processed
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerGatheredReview whereCugareAccepted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerGatheredReview whereCugareAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerGatheredReview whereCugareCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerGatheredReview whereCugareCurescId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerGatheredReview whereCugareId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerGatheredReview whereCugarePlatform($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerGatheredReview whereCugareProcessed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerGatheredReview whereCugareScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerGatheredReview whereCugareUrl($value)
 * @mixin \Eloquent
 * @property float|null $cugare_original_score
 * @property int|null $cugare_original_range
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerGatheredReview whereCugareOriginalRange($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerGatheredReview whereCugareOriginalScore($value)
 */
class CustomerGatheredReview extends Model
{
    protected $table = 'customer_gathered_reviews';
    protected $primaryKey = 'cugare_id';
    public $timestamps = false;

    //customer relation
    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'cugare_cu_id', 'cu_id');
    }

}
