<?php

namespace App\Models;

use Fico7489\Laravel\RevisionableUpgrade\Traits\RevisionableUpgradeTrait;
use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\MoverData
 *
 * @property-read \App\Models\Customer $customer
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData query()
 * @mixin \Eloquent
 * @property int $moda_id
 * @property int $moda_cu_id
 * @property int|null $moda_disable_sirelo_export
 * @property int|null $moda_review_partner
 * @property int|null $moda_sirelo_widget_bypass
 * @property string|null $moda_sirelo_widget_location
 * @property int $moda_sirelo_review_verification
 * @property string|null $moda_sirelo_key
 * @property string|null $moda_sirelo_forward_1
 * @property string|null $moda_sirelo_forward_2
 * @property string|null $moda_info_email
 * @property string|null $moda_review_email
 * @property string|null $moda_contact_email
 * @property string|null $moda_contact_telephone
 * @property string|null $moda_load_exchange_email
 * @property int $moda_pause_days
 * @property int|null $moda_international_moves
 * @property int|null $moda_national_moves
 * @property int|null $moda_long_distance_moving_europe
 * @property int|null $moda_excess_baggage
 * @property int|null $moda_man_and_van
 * @property int|null $moda_car_and_vehicle_transport
 * @property int|null $moda_piano_transport
 * @property int|null $moda_pet_transport
 * @property int|null $moda_art_transport
 * @property string|null $moda_established
 * @property int|null $moda_employees
 * @property int|null $moda_trucks
 * @property int|null $moda_special_agreements_checkbox
 * @property string|null $moda_special_agreements
 * @property int|null $moda_max_claim_percentage
 * @property int|null $moda_no_claim_discount
 * @property int|null $moda_no_claim_discount_percentage
 * @property int|null $moda_hide_lead_partner_functions
 * @property int|null $moda_hide_response_chart
 * @property int|null $moda_activate_lead_pick
 * @property int|null $moda_lead_pick_import
 * @property int|null $moda_activate_load_exchange
 * @property int|null $moda_load_exchange_notification
 * @property int|null $moda_load_exchange_daily
 * @property int $moda_activate_premium_leads
 * @property int $moda_quality_score
 * @property int|null $moda_quality_score_override
 * @property int|null $moda_capping_limitation
 * @property int|null $moda_capping_method
 * @property float|null $moda_capping_monthly_limit
 * @property float|null $moda_capping_monthly_limit_left
 * @property float|null $moda_capping_monthly_limit_modifier
 * @property float|null $moda_capping_daily_average
 * @property float|null $moda_capping_bucket
 * @property string|null $moda_sirelo_invite_subject
 * @property string|null $moda_sirelo_invite_content
 * @property int|null $moda_sirelo_disable_top_mover
 * @property int|null $moda_iam2017
 * @property int $moda_iam2018
 * @property int $moda_christmas_credits
 * @property int|null $moda_checked_sirelo
 * @property string|null $moda_checked_timestamp
 * @property int $moda_widget_check
 * @property int|null $moda_iso
 * @property int $moda_information_security_management_iso
 * @property int $moda_occupational_health_and_safety_iso
 * @property int $moda_environmental_management_iso
 * @property int $moda_quality_management_iso
 * @property string|null $moda_premium_profile_summary
 * @property string|null $moda_premium_profile_payment_terms
 * @property int $moda_payment_method_cash
 * @property int $moda_payment_method_cheque
 * @property int $moda_payment_method_bank_transfer
 * @property int $moda_payment_method_auto_debit
 * @property int $moda_payment_method_credit_card
 * @property int $moda_payment_method_paypal
 * @property int $moda_not_operational
 * @property string|null $moda_not_operational_timestamp
 * @property int|null $moda_reg_id
 * @property int|null $moda_market_type
 * @property int|null $moda_services
 * @property int|null $moda_crm_status
 * @property int|null $moda_owner
 * @property-read int|null $revision_history_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaActivateLeadPick($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaActivateLoadExchange($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaActivatePremiumLeads($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaArtTransport($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaCappingBucket($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaCappingDailyAverage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaCappingLimitation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaCappingMethod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaCappingMonthlyLimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaCappingMonthlyLimitLeft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaCappingMonthlyLimitModifier($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaCarAndVehicleTransport($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaCheckedSirelo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaCheckedTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaChristmasCredits($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaContactEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaContactTelephone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaCrmStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaDisableSireloExport($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaEmployees($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaEnvironmentalManagementIso($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaEstablished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaExcessBaggage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaHideLeadPartnerFunctions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaHideResponseChart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaIam2017($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaIam2018($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaInfoEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaInformationSecurityManagementIso($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaInternationalMoves($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaIso($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaLeadPickImport($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaLoadExchangeDaily($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaLoadExchangeEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaLoadExchangeNotification($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaLongDistanceMovingEurope($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaManAndVan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaMarketType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaMaxClaimPercentage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaNationalMoves($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaNoClaimDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaNoClaimDiscountPercentage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaNotOperational($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaNotOperationalTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaOccupationalHealthAndSafetyIso($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaOwner($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaPauseDays($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaPaymentMethodAutoDebit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaPaymentMethodBankTransfer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaPaymentMethodCash($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaPaymentMethodCheque($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaPaymentMethodCreditCard($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaPaymentMethodPaypal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaPetTransport($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaPianoTransport($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaPremiumProfilePaymentTerms($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaPremiumProfileSummary($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaQualityManagementIso($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaQualityScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaQualityScoreOverride($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaRegId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaReviewEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaReviewPartner($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaServices($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaSireloDisableTopMover($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaSireloForward1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaSireloForward2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaSireloInviteContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaSireloInviteSubject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaSireloKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaSireloReviewVerification($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaSireloWidgetBypass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaSireloWidgetLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaSpecialAgreements($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaSpecialAgreementsCheckbox($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaTrucks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaWidgetCheck($value)
 * @property int $moda_forced_daily_capping
 * @property int $moda_forced_daily_capping_left
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaForcedDailyCapping($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaForcedDailyCappingLeft($value)
 * @property int|null $moda_source
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaSource($value)
 * @property int $moda_sirelo_disable_request_form
 * @property int|null $moda_child_sirelo_type
 * @property int|null $moda_parent_sirelo_type
 * @property-read \App\Models\User|null $owner
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaChildSireloType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaParentSireloType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData whereModaSireloDisableRequestForm($value)
 */
class MoverData extends Model
{
	protected $table = 'mover_data';
	protected $primaryKey = 'moda_id';
	public $timestamps = false;

	use RevisionableTrait;
	use RevisionableUpgradeTrait;

	public function customer()
	{
		return $this->belongsTo('App\Models\Customer', 'moda_cu_id', 'cu_id');
	}

	public function owner(){
	    return $this->belongsTo('App\Models\User', 'moda_owner', 'us_id');
    }
}
