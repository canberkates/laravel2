<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Insurance
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Insurance newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Insurance newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Insurance query()
 * @mixin \Eloquent
 * @property int $cuin_id
 * @property string $cuin_name
 * @property string $cuin_remark
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Insurance whereCuinId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Insurance whereCuinName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Insurance whereCuinRemark($value)
 */
class Insurance extends Model
{
    protected $table = 'customer_insurances';
    protected $primaryKey = 'cuin_id';
    public $timestamps = false;


}
