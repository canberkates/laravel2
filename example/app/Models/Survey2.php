<?php

namespace App\Models;

use Fico7489\Laravel\RevisionableUpgrade\Traits\RevisionableUpgradeTrait;
use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\Survey2
 *
 * @property-read \App\Models\Customer $customer
 * @property-read \App\Models\Request $request
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @property-read \App\Models\WebsiteReview $website_review
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 query()
 * @mixin \Eloquent
 * @property int $su_id
 * @property int $su_source
 * @property int|null $su_re_id
 * @property int $su_on_hold
 * @property int|null $su_on_hold_by
 * @property int|null $su_were_id
 * @property string $su_timestamp
 * @property int $su_opened
 * @property string $su_opened_timestamp
 * @property int $su_submitted
 * @property string $su_submitted_timestamp
 * @property int $su_sent
 * @property int $su_moved
 * @property string $su_new_moving_date
 * @property int|null $su_mover
 * @property string $su_why_mover
 * @property int|null $su_other_mover
 * @property string $su_other_mover_description
 * @property string $su_volume
 * @property string $su_budget
 * @property int $su_rating
 * @property string $su_rating_motivation
 * @property string|null $su_rating_motivation_original
 * @property int $su_recommend_mover
 * @property string $su_pro_1
 * @property int|null $su_pro_1_category
 * @property string $su_pro_2
 * @property int|null $su_pro_2_category
 * @property string $su_con_1
 * @property int|null $su_con_1_category
 * @property string $su_con_2
 * @property int|null $su_con_2_category
 * @property int $su_show_on_website
 * @property int $su_send_email
 * @property string $su_mover_comment_timestamp
 * @property string $su_mover_comment
 * @property int $su_checked
 * @property string $su_checked_remarks
 * @property int $su_mail
 * @property int $su_reminder
 * @property int $su_experience_blog
 * @property int $su_confirmation_email
 * @property int $su_anonymized
 * @property-read int|null $revision_history_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 whereSuAnonymized($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 whereSuBudget($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 whereSuChecked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 whereSuCheckedRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 whereSuCon1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 whereSuCon1Category($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 whereSuCon2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 whereSuCon2Category($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 whereSuConfirmationEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 whereSuExperienceBlog($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 whereSuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 whereSuMail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 whereSuMoved($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 whereSuMover($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 whereSuMoverComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 whereSuMoverCommentTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 whereSuNewMovingDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 whereSuOnHold($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 whereSuOnHoldBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 whereSuOpened($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 whereSuOpenedTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 whereSuOtherMover($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 whereSuOtherMoverDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 whereSuPro1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 whereSuPro1Category($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 whereSuPro2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 whereSuPro2Category($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 whereSuRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 whereSuRatingMotivation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 whereSuRatingMotivationOriginal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 whereSuReId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 whereSuRecommendMover($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 whereSuReminder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 whereSuSendEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 whereSuSent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 whereSuShowOnWebsite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 whereSuSource($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 whereSuSubmitted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 whereSuSubmittedTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 whereSuTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 whereSuVolume($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 whereSuWereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey2 whereSuWhyMover($value)
 */
class Survey2 extends Model
{
    protected $table = 'surveys_2';
	protected $primaryKey = 'su_id';
	public $timestamps = false;
	
	use RevisionableTrait;
	use RevisionableUpgradeTrait;
	
	public function customer()
	{
		return $this->belongsTo('App\Models\Customer', 'su_mover', 'cu_id');
	}
	
	public function request()
	{
		return $this->belongsTo('App\Models\Request', 'su_re_id', 're_id');
	}
	
	public function website_review()
	{
		return $this->belongsTo('App\Models\WebsiteReview', 'su_were_id', 'were_id');
	}
}
