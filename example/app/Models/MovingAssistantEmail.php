<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\MovingAssistantEmail
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovingAssistantEmail newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovingAssistantEmail newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovingAssistantEmail query()
 * @mixin \Eloquent
 * @property-read \App\Models\EmailBuilderTemplate $email
 * @property int $moasem_id
 * @property string $moasem_timestamp
 * @property int $moasem_embute_id
 * @property int $moasem_days_before_mail
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovingAssistantEmail whereMoasemDaysBeforeMail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovingAssistantEmail whereMoasemEmbuteId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovingAssistantEmail whereMoasemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovingAssistantEmail whereMoasemTimestamp($value)
 */
class MovingAssistantEmail extends Model
{
    protected $table = 'moving_assistant_email';
    protected $primaryKey = 'moasem_id';
    public $timestamps = false;

    public function email()
    {
        return $this->belongsTo('App\Models\EmailBuilderTemplate', 'moasem_embute_id', 'embute_id');
    }
}
