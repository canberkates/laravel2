<?php

namespace App\Models;

use Fico7489\Laravel\RevisionableUpgrade\Traits\RevisionableUpgradeTrait;
use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\MoverPriceEstimationSettings
 *
 * @property int $mopres_id
 * @property int $mopres_mofose_id
 * @property string|null $mopres_price_estimation_currency
 * @property int|null $mopres_price_estimation_starting_price
 * @property int|null $mopres_price_estimation_price_per_km
 * @property int|null $mopres_price_estimation_price_per_m2
 * @property int|null $mopres_price_estimation_price_per_minute
 * @property int|null $mopres_price_estimation_price_range_down
 * @property int|null $mopres_price_estimation_price_range_up
 * @property int|null $mopres_price_estimation_minimum_volume
 * @property-read \App\Models\MoverFormSettings $customer
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @property-read int|null $revision_history_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverPriceEstimationSettings newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverPriceEstimationSettings newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverPriceEstimationSettings query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverPriceEstimationSettings whereMopresId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverPriceEstimationSettings whereMopresMofoseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverPriceEstimationSettings whereMopresPriceEstimationCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverPriceEstimationSettings whereMopresPriceEstimationMinimumVolume($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverPriceEstimationSettings whereMopresPriceEstimationPricePerKm($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverPriceEstimationSettings whereMopresPriceEstimationPricePerM2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverPriceEstimationSettings whereMopresPriceEstimationPricePerMinute($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverPriceEstimationSettings whereMopresPriceEstimationPriceRangeDown($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverPriceEstimationSettings whereMopresPriceEstimationPriceRangeUp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverPriceEstimationSettings whereMopresPriceEstimationStartingPrice($value)
 * @mixin \Eloquent
 * @property int $mopres_price_estimation_price_per_m3
 * @property int $mopres_price_estimation_price_per_hour
 * @property int $mopres_price_estimation_maximum_volume
 * @property int $mopres_volume_type
 * @property int $mopres_volume_format
 * @property string $mopres_main_color
 * @property string $mopres_secondary_color
 * @property string $mopres_google_autocomplete_country
 * @property int $mopres_show_quote_button
 * @property string $mopres_quote_button_url
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverPriceEstimationSettings whereMopresGoogleAutocompleteCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverPriceEstimationSettings whereMopresMainColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverPriceEstimationSettings whereMopresPriceEstimationMaximumVolume($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverPriceEstimationSettings whereMopresPriceEstimationPricePerHour($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverPriceEstimationSettings whereMopresPriceEstimationPricePerM3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverPriceEstimationSettings whereMopresQuoteButtonUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverPriceEstimationSettings whereMopresSecondaryColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverPriceEstimationSettings whereMopresShowQuoteButton($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverPriceEstimationSettings whereMopresVolumeFormat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverPriceEstimationSettings whereMopresVolumeType($value)
 */
class MoverPriceEstimationSettings extends Model
{
	protected $table = 'mover_price_estimation_settings';
	protected $primaryKey = 'mopres_id';
	public $timestamps = false;

	use RevisionableTrait;
	use RevisionableUpgradeTrait;

	public function customer()
	{
		return $this->hasOne('App\Models\MoverFormSettings', 'mofose_id', 'mopres_mofose_id');
	}


}
