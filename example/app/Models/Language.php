<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Language
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Language newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Language newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Language query()
 * @mixin \Eloquent
 * @property string $la_code
 * @property string $la_language
 * @property string $la_language_local
 * @property string $la_iso
 * @property string $la_locale
 * @property int $la_iframe_only
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Language whereLaCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Language whereLaIframeOnly($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Language whereLaIso($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Language whereLaLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Language whereLaLanguageLocal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Language whereLaLocale($value)
 */
class Language extends Model
{
	protected $table = 'languages';
	//protected $primaryKey = 'pacu_code';
	public $timestamps = false;
}
