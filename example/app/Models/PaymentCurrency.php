<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PaymentCurrency
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentCurrency newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentCurrency newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentCurrency query()
 * @mixin \Eloquent
 * @property string $pacu_code
 * @property string $pacu_name
 * @property string $pacu_token
 * @property float $pacu_rate
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentCurrency wherePacuCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentCurrency wherePacuName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentCurrency wherePacuRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentCurrency wherePacuToken($value)
 */
class PaymentCurrency extends Model
{
	protected $table = 'payment_currencies';
	//protected $primaryKey = 'pacu_code';
	public $timestamps = false;
}
