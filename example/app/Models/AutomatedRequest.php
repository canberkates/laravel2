<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Adyen
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Adyen newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Adyen newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Adyen query()
 * @mixin \Eloquent
 * @property int $aure_id
 * @property int $aure_re_id
 * @property string $aure_timestamp
 * @property int $aure_decision
 * @property string $aure_details
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AutomatedRequest whereAureDecision($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AutomatedRequest whereAureDetails($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AutomatedRequest whereAureId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AutomatedRequest whereAureReId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AutomatedRequest whereAureTimestamp($value)
 */
class AutomatedRequest extends Model
{
    protected $table = 'automated_requests';
	protected $primaryKey = 'aure_id';
	public $timestamps = false;
}
