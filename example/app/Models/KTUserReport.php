<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\KTUserReport
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTUserReport newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTUserReport newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTUserReport query()
 * @mixin \Eloquent
 * @property int $ktusre_id
 * @property int $ktusre_us_id
 * @property int $ktusre_rep_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTUserReport whereKtusreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTUserReport whereKtusreRepId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTUserReport whereKtusreUsId($value)
 */
class KTUserReport extends Model
{
    protected $table = 'kt_user_report';
	protected $primaryKey = 'ktusre_id';
	public $timestamps = false;
}
