<?php

namespace App\Models;

use Fico7489\Laravel\RevisionableUpgrade\Traits\RevisionableUpgradeTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\CustomerOffice
 *
 * @property-read \App\Models\Customer $customer
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerOffice newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerOffice newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CustomerOffice onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerOffice query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CustomerOffice withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CustomerOffice withoutTrashed()
 * @mixin \Eloquent
 * @property int $cuof_id
 * @property int|null $cuof_cu_id
 * @property string $cuof_street_1
 * @property string|null $cuof_street_2
 * @property string $cuof_city
 * @property string|null $cuof_zipcode
 * @property string|null $cuof_state
 * @property string $cuof_co_code
 * @property string|null $cuof_email
 * @property string|null $cuof_telephone
 * @property string|null $cuof_website
 * @property string|null $deleted_at
 * @property-read int|null $revision_history_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerOffice whereCuofCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerOffice whereCuofCoCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerOffice whereCuofCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerOffice whereCuofEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerOffice whereCuofId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerOffice whereCuofState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerOffice whereCuofStreet1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerOffice whereCuofStreet2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerOffice whereCuofTelephone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerOffice whereCuofWebsite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerOffice whereCuofZipcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerOffice whereDeletedAt($value)
 * @property int|null $cuof_child_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerOffice whereCuofChildId($value)
 * @property int|null $cuof_child_type
 * @property int|null $cuof_child_sirelo_type
 * @property int|null $cuof_child_mover_portal_type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerOffice whereCuofChildMoverPortalType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerOffice whereCuofChildSireloType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerOffice whereCuofChildType($value)
 */
class CustomerOffice extends Model
{
    protected $table = 'customer_offices';
	protected $primaryKey = 'cuof_id';
	public $timestamps = false;
	
	use RevisionableTrait;
	use RevisionableUpgradeTrait;
	
	//enable this if you want use methods that gets information about creating
	protected $revisionCreationsEnabled = true;
	
	//customer relation
	public function customer()
	{
		return $this->belongsTo('App\Models\Customer', 'cuof_cu_id', 'cu_id');
	}
	
}
