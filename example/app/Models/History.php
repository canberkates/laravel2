<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\History
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\History newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\History newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\History query()
 * @mixin \Eloquent
 * @property int $hi_id
 * @property int $hi_ap_id
 * @property int|null $hi_us_id
 * @property int|null $hi_apus_id
 * @property string $hi_timestamp
 * @property string $hi_task
 * @property string $hi_key
 * @property string $hi_data
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\History whereHiApId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\History whereHiApusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\History whereHiData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\History whereHiId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\History whereHiKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\History whereHiTask($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\History whereHiTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\History whereHiUsId($value)
 */
class History extends Model
{
    protected $table = 'history';
	protected $primaryKey = 'hi_id';
	public $timestamps = false;
}
