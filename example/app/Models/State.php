<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\State
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\State newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\State newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\State query()
 * @mixin \Eloquent
 * @property int $st_id
 * @property string $st_name
 * @property string|null $st_key
 * @property string|null $st_zipcode
 * @property string $st_co_code
 * @property int|null $st_order
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\State whereStCoCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\State whereStId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\State whereStKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\State whereStName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\State whereStOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\State whereStZipcode($value)
 */
class State extends Model
{
    protected $table = 'states';
	protected $primaryKey = 'st_id';
	public $timestamps = false;
}
