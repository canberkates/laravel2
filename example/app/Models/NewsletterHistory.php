<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\NewsletterHistory
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsletterHistory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsletterHistory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsletterHistory query()
 * @mixin \Eloquent
 * @property int $nehi_id
 * @property string $nehi_timestamp
 * @property int $nehi_new_id
 * @property int $nehi_re_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsletterHistory whereNehiId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsletterHistory whereNehiNewId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsletterHistory whereNehiReId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsletterHistory whereNehiTimestamp($value)
 */
class NewsletterHistory extends Model
{
    protected $table = 'newsletter_history';
	protected $primaryKey = 'nehi_id';
	public $timestamps = false;
}
