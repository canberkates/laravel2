<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\WebsiteSubdomain
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteSubdomain newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteSubdomain newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteSubdomain query()
 * @mixin \Eloquent
 * @property int $wesu_id
 * @property string $wesu_name
 * @property string $wesu_url
 * @property int $wesu_we_id
 * @property string $wesu_country_code
 * @property string $wesu_default_language
 * @property string $wesu_customer_location
 * @property string $wesu_survey_location
 * @property-read \App\Models\Country $country
 * @property-read \App\Models\Language $language
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteSubdomain whereWesuCountryCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteSubdomain whereWesuCustomerLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteSubdomain whereWesuDefaultLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteSubdomain whereWesuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteSubdomain whereWesuName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteSubdomain whereWesuSurveyLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteSubdomain whereWesuUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteSubdomain whereWesuWeId($value)
 * @property string|null $wesu_register_company_location
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteSubdomain whereWesuRegisterCompanyLocation($value)
 */
class WebsiteSubdomain extends Model
{
    protected $table = 'website_subdomains';
	protected $primaryKey = 'wesu_id';
	public $timestamps = false;
    
    public function country()
    {
        return $this->hasOne('App\Models\Country', 'co_code', 'wesu_country_code');
    }
    
    public function language()
    {
        return $this->hasOne('App\Models\Language', 'la_code', 'wesu_default_language');
    }
}
