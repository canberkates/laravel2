<?php

namespace App\Models;

use Fico7489\Laravel\RevisionableUpgrade\Traits\RevisionableUpgradeTrait;
use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\AffiliatePartnerForm
 *
 * @property-read \App\Models\Customer $customer
 * @property-read \App\Models\Portal $portal
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerForm newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerForm newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerForm query()
 * @mixin \Eloquent
 * @property int $afpafo_id
 * @property int|null $afpafo_cu_id
 * @property string $afpafo_token
 * @property string $afpafo_name
 * @property int|null $afpafo_po_id
 * @property int $afpafo_form_type
 * @property int $afpafo_disable_ip_check
 * @property string|null $afpafo_la_code
 * @property int $afpafo_volume_type
 * @property int $afpafo_hide_pre_form
 * @property int $afpafo_hide_questions
 * @property float $afpafo_rate
 * @property string|null $afpafo_custom_css
 * @property string|null $afpafo_analytics_ua_code
 * @property string|null $afpafo_conversion_code
 * @property int $afpafo_tracking_clientid
 * @property string|null $afpafo_tracking_domains
 * @property int $afpafo_deleted
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @property-read int|null $revision_history_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerForm whereAfpafoAnalyticsUaCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerForm whereAfpafoConversionCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerForm whereAfpafoCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerForm whereAfpafoCustomCss($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerForm whereAfpafoDeleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerForm whereAfpafoDisableIpCheck($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerForm whereAfpafoFormType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerForm whereAfpafoHidePreForm($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerForm whereAfpafoHideQuestions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerForm whereAfpafoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerForm whereAfpafoLaCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerForm whereAfpafoName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerForm whereAfpafoPoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerForm whereAfpafoRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerForm whereAfpafoToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerForm whereAfpafoTrackingClientid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerForm whereAfpafoTrackingDomains($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerForm whereAfpafoVolumeType($value)
 * @property-read \App\Models\MoverFormSettings $conversion_tools
 */
class AffiliatePartnerForm extends Model
{
    protected $table = 'affiliate_partner_forms';
	protected $primaryKey = 'afpafo_id';
	public $timestamps = false;

	use RevisionableTrait;
	use RevisionableUpgradeTrait;

	//enable this if you want use methods that gets information about creating
	protected $revisionCreationsEnabled = true;

	public function portal()
	{
		return $this->belongsTo('App\Models\Portal', 'afpafo_po_id', 'po_id');
	}

	public function customer()
	{
		return $this->belongsTo('App\Models\Customer', 'afpafo_cu_id', 'cu_id');
	}

	public function moverform()
	{
		return $this->hasOne('App\Models\MoverFormSettings', 'mofose_afpafo_id', 'afpafo_id');
	}
}
