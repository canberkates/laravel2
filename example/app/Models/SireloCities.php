<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SireloCities
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer query()
 * @mixin \Eloquent
 * @property string $key
 * @property string $city
 * @property string $country_code
 * @property int $amount
 * @property int|null $state
 * @property string $nearby
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCities whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCities whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCities whereCountryCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCities whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCities whereNearby($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCities whereState($value)
 */

class SireloCities extends Model
{
    protected $table = 'sirelo_cities';
    protected $primaryKey = 'sici_key';
    public $timestamps = false;
}
