<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\QualityScoreCalculation
 *
 * @property int $qsc_id
 * @property int $qsc_re_id
 * @property string $qsc_old_calc
 * @property string $qsc_new_calc
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QualityScoreCalculation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QualityScoreCalculation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QualityScoreCalculation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QualityScoreCalculation whereQscId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QualityScoreCalculation whereQscNewCalc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QualityScoreCalculation whereQscOldCalc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QualityScoreCalculation whereQscReId($value)
 * @mixin \Eloquent
 */
class QualityScoreCalculation extends Model
{
    protected $table = 'quality_score_calculation';
	protected $primaryKey = 'qsc_id';
	public $timestamps = false;
}
