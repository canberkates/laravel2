<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



/**
 * App\Models\MoverDirectPricingSettings
 *
 * @property int $modiprse_id
 * @property int $modiprse_mofose_id
 * @property string $modiprse_currency
 * @property float $modiprse_price_per_mover_per_hour
 * @property float $modiprse_price_per_km
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverDirectPricingSettings newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverDirectPricingSettings newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverDirectPricingSettings query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverDirectPricingSettings whereModiprseCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverDirectPricingSettings whereModiprseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverDirectPricingSettings whereModiprseMofoseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverDirectPricingSettings whereModiprsePricePerKm($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverDirectPricingSettings whereModiprsePricePerMoverPerHour($value)
 * @mixin \Eloquent
 * @property float $modiprse_price_range_down
 * @property float $modiprse_price_range_up
 * @property int $modiprse_price_round_off
 * @property int $modiprse_minimum_price
 * @property float $modiprse_packing_cost
 * @property float $modiprse_assembly_cost
 * @property float $modiprse_storage_cost_per_m3_per_day
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverDirectPricingSettings whereModiprseAssemblyCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverDirectPricingSettings whereModiprseMinimumPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverDirectPricingSettings whereModiprsePackingCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverDirectPricingSettings whereModiprsePriceRangeDown($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverDirectPricingSettings whereModiprsePriceRangeUp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverDirectPricingSettings whereModiprsePriceRoundOff($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverDirectPricingSettings whereModiprseStorageCostPerM3PerDay($value)
 */
class MoverDirectPricingSettings extends Model
{
	protected $table = 'mover_direct_pricing_settings';
	protected $primaryKey = 'modiprse_id';
    public $timestamps = false;

}
