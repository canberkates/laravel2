<?php

namespace App\Models;

use Fico7489\Laravel\RevisionableUpgrade\Traits\RevisionableUpgradeTrait;
use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\Customer
 *
 * @property-read \App\Models\AffiliatePartnerData                                                       $affiliatedata
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ContactPerson[]                   $contactpersons
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\AdyenCardDetails[]                $creditcards
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ServiceProviderNewsletterBlock[]  $customernewsletterblocks
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ServiceProviderAdvertorialBlock[] $customeradvertorialblocks
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\KTRequestCustomerPortal[]         $customerportalrequests
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\KTCustomerPortal[]                $customerportals
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\KTCustomerQuestion[]              $customerquestions
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CustomerDocument[]                $documents
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\AffiliatePartnerForm[]            $forms
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Invoice[]                         $invoices
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Membership[]                      $memberships
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\KTCustomerSubscription[]          $customersubscriptions
 * @property-read \App\Models\MoverData                                                                  $moverdata
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CustomerOffice[]                  $offices
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CustomerRemark[] $remarks
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CustomerStatus[] $statuses
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Survey2[] $surveys2
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer query()
 * @mixin \Eloquent
 * @property int $cu_id
 * @property string $cu_debtor_number
 * @property int $cu_type
 * @property int|null $cu_sales_manager
 * @property int|null $cu_account_manager
 * @property int|null $cu_debt_manager
 * @property string|null $cu_la_code
 * @property string|null $cu_logo
 * @property string|null $cu_description
 * @property string|null $cu_company_name_legal
 * @property string|null $cu_company_name_business
 * @property string|null $cu_attn
 * @property string|null $cu_attn_email
 * @property string|null $cu_coc
 * @property string|null $cu_street_1
 * @property string|null $cu_street_2
 * @property string|null $cu_city
 * @property string|null $cu_zipcode
 * @property string|null $cu_state
 * @property string|null $cu_co_code
 * @property string|null $cu_email
 * @property string|null $cu_telephone
 * @property string|null $cu_fax
 * @property string $cu_website
 * @property int $cu_use_billing_address
 * @property string|null $cu_street_1_bi
 * @property string|null $cu_street_2_bi
 * @property string|null $cu_city_bi
 * @property string|null $cu_zipcode_bi
 * @property string|null $cu_state_bi
 * @property string|null $cu_co_code_bi
 * @property string|null $cu_email_bi
 * @property string|null $cu_telephone_bi
 * @property string|null $cu_fax_bi
 * @property string|null $cu_website_bi
 * @property int $cu_debtor_status
 * @property int $cu_finance_lock
 * @property int $cu_credit_hold
 * @property string $cu_credit_hold_timestamp
 * @property int $cu_debt_collector
 * @property string $cu_pacu_code
 * @property int $cu_invoice_period
 * @property int $cu_payment_reminder_status
 * @property int $cu_payment_method
 * @property int|null $cu_pp_monthly_budget
 * @property int|null $cu_pp_charge_threshold
 * @property string|null $cu_pp_charge_timestamp
 * @property int|null $cu_pp_charge_credit_card_times
 * @property int $cu_credit_card_amex
 * @property int $cu_payment_term
 * @property string $cu_last_payment_notification
 * @property string|null $cu_prepayment_ran_out_mail_timestamp
 * @property string $cu_prepayment_mail_timestamp
 * @property float $cu_credit_limit
 * @property float|null $cu_credit_limit_original
 * @property string|null $cu_credit_limit_increase_timestamp
 * @property int $cu_enforce_credit_limit
 * @property int|null $cu_leac_number
 * @property string|null $cu_vat_number
 * @property string|null $cu_finance_remarks
 * @property string|null $cu_auto_debit_name
 * @property string|null $cu_auto_debit_city
 * @property string|null $cu_auto_debit_co_code
 * @property string|null $cu_auto_debit_iban
 * @property string|null $cu_auto_debit_bic
 * @property string $cu_partner_last_contact
 * @property string $cu_created_timestamp
 * @property string|null $cu_revenues_share
 * @property int|null $cu_show_revenues_in_affiliate_portal
 * @property int|null $cu_type_of_mover
 * @property int|null $cu_public_liability_insurance
 * @property string|null $cu_public_liability_insurance_value
 * @property int|null $cu_goods_in_transit_insurance
 * @property string|null $cu_goods_in_transit_insurance_value
 * @property int $cu_national_permit
 * @property int $cu_international_permit
 * @property int|null $cu_created_us_id
 * @property int $cu_deleted
 * @property int $cu_skip_for_overdue_cronjob
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PlannedCall[] $contactlogs
 * @property-read int|null $contactlogs_count
 * @property-read int|null $contactpersons_count
 * @property-read \App\Models\Country $country
 * @property-read int|null $creditcards_count
 * @property-read int|null $customernewsletterblocks_count
 * @property-read int|null $customerportalrequests_count
 * @property-read int|null $customerportals_count
 * @property-read int|null $customerquestions_count
 * @property-read int|null $documents_count
 * @property-read int|null $forms_count
 * @property-read int|null $invoices_count
 * @property-read int|null $memberships_count
 * @property-read int|null $offices_count
 * @property-read int|null $remarks_count
 * @property-read int|null $revision_history_count
 * @property-read int|null $statuses_count
 * @property-read int|null $surveys2_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuAccountManager($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuAttn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuAttnEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuAutoDebitBic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuAutoDebitCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuAutoDebitCoCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuAutoDebitIban($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuAutoDebitName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuCityBi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuCoCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuCoCodeBi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuCoc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuCompanyNameBusiness($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuCompanyNameLegal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuCreatedTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuCreatedUsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuCreditCardAmex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuCreditHold($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuCreditHoldTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuCreditLimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuCreditLimitIncreaseTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuCreditLimitOriginal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuDebtCollector($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuDebtManager($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuDebtorNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuDebtorStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuDeleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuEmailBi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuEnforceCreditLimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuFax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuFaxBi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuFinanceLock($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuFinanceRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuGoodsInTransitInsurance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuGoodsInTransitInsuranceValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuInternationalPermit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuInvoicePeriod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuLaCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuLastPaymentNotification($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuLeacNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuNationalPermit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuPacuCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuPartnerLastContact($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuPaymentMethod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuPaymentReminderStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuPaymentTerm($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuPpChargeCreditCardTimes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuPpChargeThreshold($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuPpChargeTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuPpMonthlyBudget($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuPrepaymentMailTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuPrepaymentRanOutMailTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuPublicLiabilityInsurance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuPublicLiabilityInsuranceValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuRevenuesShare($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuSalesManager($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuShowRevenuesInAffiliatePortal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuSkipForOverdueCronjob($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuStateBi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuStreet1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuStreet1Bi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuStreet2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuStreet2Bi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuTelephone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuTelephoneBi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuTypeOfMover($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuUseBillingAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuVatNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuWebsite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuWebsiteBi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuZipcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuZipcodeBi($value)
 * @property-read int|null $customersubscriptions_count
 * @property int|null $cu_parent_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuParentId($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Request[] $leads
 * @property-read int|null $leads_count
 * @property string|null $cu_custom_google_search_url
 * @property-read int|null $customeradvertorialblocks_count
 * @property-read \App\Models\ServiceProviderData $serviceproviderdata
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCuCustomGoogleSearchUrl($value)
 */

class Customer extends Model
{
	protected $table = 'customers';
	protected $primaryKey = 'cu_id';
	public $timestamps = false;

	use RevisionableTrait;
	use RevisionableUpgradeTrait;

	//enable this if you want use methods that gets information about creating
	protected $revisionCreationsEnabled = true;

	public function moverdata()
	{
		return $this->hasOne('App\Models\MoverData', 'moda_cu_id', 'cu_id');
	}

	public function affiliatedata()
	{
		return $this->hasOne('App\Models\AffiliatePartnerData', 'afpada_cu_id', 'cu_id');
	}

	public function serviceproviderdata()
	{
		return $this->hasOne('App\Models\ServiceProviderData', 'seprda_cu_id', 'cu_id');
	}

	public function forms()
	{
		return $this->hasMany('App\Models\AffiliatePartnerForm', 'afpafo_cu_id', 'cu_id');
	}

	public function moverforms()
	{
		return $this->hasMany('App\Models\MoverFormSettings', 'mofose_cu_id', 'cu_id');
	}

	public function contactpersons(){
		return $this->hasMany('App\Models\ContactPerson', 'cope_cu_id', 'cu_id');
	}

	public function statuses(){
		return $this->hasMany('App\Models\CustomerStatus', 'cust_cu_id', 'cu_id');
	}

	public function remarks(){
		return $this->hasMany('App\Models\CustomerRemark', 'cure_cu_id', 'cu_id');
	}

	public function documents(){
		return $this->hasMany('App\Models\CustomerDocument', 'cudo_cu_id', 'cu_id');
	}

    public function offices(){
        return $this->hasMany('App\Models\CustomerOffice', 'cuof_cu_id', 'cu_id');
    }

    public function customerquestions(){
		return $this->hasMany('App\Models\KTCustomerQuestion', 'ktcuqu_cu_id', 'cu_id');
	}

	public function customernewsletterblocks(){
		return $this->hasMany('App\Models\ServiceProviderNewsletterBlock', 'seprnebl_cu_id', 'cu_id');
	}

	public function customeradvertorialblocks(){
		return $this->hasMany( 'App\Models\ServiceProviderAdvertorialBlock', 'sepradbl_cu_id', 'cu_id');
	}

	public function customerportals(){
		return $this->hasMany('App\Models\KTCustomerPortal', 'ktcupo_cu_id');
	}

	public function customersubscriptions(){
		return $this->hasMany('App\Models\KTCustomerSubscription', 'ktcusu_cu_id');
	}

	public function surveys2(){
		return $this->hasMany('App\Models\Survey2', 'su_mover', 'cu_id');
	}

	public function customerportalrequests(){
		return $this->hasMany('App\Models\KTRequestCustomerPortal', 'ktrecupo_cu_id', 'cu_id');
	}

	public function invoices(){
		return $this->hasMany('App\Models\Invoice', 'in_cu_id', 'cu_id');
	}

	public function memberships()
	{
		return $this->belongsToMany('App\Models\Membership', 'kt_customer_membership', 'ktcume_cu_id', 'ktcume_me_id');
	}

	public function creditcards(){
		return $this->hasMany('App\Models\AdyenCardDetails', 'adcade_cu_id', 'cu_id');
	}

	public function country()
	{
		return $this->hasOne('App\Models\Country', 'co_code', 'cu_co_code');
	}

    public function contactlogs()
    {
        return $this->hasMany('App\Models\PlannedCall', 'plca_cu_id', 'cu_id');
    }

    public function leads(){
	    return $this->hasManyThrough('App\Models\Request', 'App\Models\KTRequestCustomerPortal',
            'ktrecupo_cu_id', 're_id', 'cu_id', 'ktrecupo_re_id');
    }

    public function getNextReID(): int
    {
        $request_id = KTRequestCustomerPortal::select("ktrecupo_cu_re_id")->where("ktrecupo_cu_id", $this->cu_id)->orderByDesc("ktrecupo_cu_re_id")->first();

        if($request_id == null){

        }else{
            $request_id = KTRequestCustomerPortal::select("ktrecupo_cu_re_id")->where("ktrecupo_cu_id", $this->cu_id)->orderByDesc("ktrecupo_cu_re_id")->first()->ktrecupo_cu_re_id + 1;
        }

        return $request_id;
    }
}
