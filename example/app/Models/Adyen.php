<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Adyen
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Adyen newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Adyen newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Adyen query()
 * @mixin \Eloquent
 * @property int $ad_id
 * @property string $ad_timestamp
 * @property int|null $ad_cu_id
 * @property string $ad_psp_reference
 * @property string $ad_merchant_reference
 * @property string $ad_event_code
 * @property string $ad_payment_method
 * @property string $ad_currency
 * @property float $ad_amount
 * @property float|null $ad_amount_eur
 * @property int $ad_success
 * @property string $ad_data
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Adyen whereAdAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Adyen whereAdAmountEur($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Adyen whereAdCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Adyen whereAdCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Adyen whereAdData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Adyen whereAdEventCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Adyen whereAdId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Adyen whereAdMerchantReference($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Adyen whereAdPaymentMethod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Adyen whereAdPspReference($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Adyen whereAdSuccess($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Adyen whereAdTimestamp($value)
 */
class Adyen extends Model
{
    protected $table = 'adyen';
	protected $primaryKey = 'ad_id';
	public $timestamps = false;
}
