<?php

namespace App\Models;

use Fico7489\Laravel\RevisionableUpgrade\Traits\RevisionableUpgradeTrait;
use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\MoverPortalLoginAttempt
 *
 * @property-read \App\Models\Customer $customer
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData query()
 * @mixin \Eloquent
 * @property int $mopoloat_id
 * @property int|null $mopoloat_cope_id
 * @property string|null $mopoloat_email
 * @property string $mopoloat_ip_address
 * @property string $mopoloat_timestamp
 * @property int $mopoloat_deleted
 * @property-read int|null $revision_history_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverPortalLoginAttempt whereMopoloatCopeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverPortalLoginAttempt whereMopoloatDeleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverPortalLoginAttempt whereMopoloatEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverPortalLoginAttempt whereMopoloatId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverPortalLoginAttempt whereMopoloatIpAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverPortalLoginAttempt whereMopoloatTimestamp($value)
 */
class MoverPortalLoginAttempt extends Model
{
	protected $table = 'mover_portal_login_attempts';
	protected $primaryKey = 'mopoloat_id';
	public $timestamps = false;
	
	use RevisionableTrait;
	use RevisionableUpgradeTrait;
}
