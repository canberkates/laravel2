<?php

namespace App\Models;

use Fico7489\Laravel\RevisionableUpgrade\Traits\RevisionableUpgradeTrait;
use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\KTCustomerSubscription
 *
 * @property int $ktcusu_id
 * @property int $ktcusu_cu_id
 * @property int $ktcusu_sub_id
 * @property string $ktcusu_start_date
 * @property string|null $ktcusu_end_date
 * @property string|null $ktcusu_cancellation_date
 * @property float $ktcusu_price
 * @property float $ktcusu_price_this_month
 * @property string $ktcusu_pacu_code
 * @property int $ktcusu_payment_interval
 * @property int $ktcusu_active
 * @property int $ktcusu_invoiced
 * @property-read \App\Models\Customer $customer
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @property-read int|null $revision_history_count
 * @property-read \App\Models\Subscription $subscription
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerSubscription newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerSubscription newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerSubscription query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerSubscription whereKtcusuActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerSubscription whereKtcusuCancellationDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerSubscription whereKtcusuCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerSubscription whereKtcusuEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerSubscription whereKtcusuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerSubscription whereKtcusuInvoiced($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerSubscription whereKtcusuPacuCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerSubscription whereKtcusuPaymentInterval($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerSubscription whereKtcusuPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerSubscription whereKtcusuPriceThisMonth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerSubscription whereKtcusuStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerSubscription whereKtcusuSubId($value)
 * @mixin \Eloquent
 */
class KTCustomerSubscription extends Model
{
    protected $table = 'kt_customer_subscription';
    protected $primaryKey = 'ktcusu_id';
    public $timestamps = false;

    use RevisionableTrait;
    use RevisionableUpgradeTrait;

    //enable this if you want use methods that gets information about creating
    protected $revisionCreationsEnabled = true;

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'ktcusu_cu_id', 'cu_id');
    }

    public function subscription()
    {
        return $this->belongsTo('App\Models\Subscription', 'ktcusu_sub_id', 'sub_id');
    }

}
