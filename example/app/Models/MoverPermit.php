<?php

namespace App\Models;

use Fico7489\Laravel\RevisionableUpgrade\Traits\RevisionableUpgradeTrait;
use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\MoverData
 *
 * @property-read \App\Models\Customer $customer
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData query()
 * @mixin \Eloquent
 * @property int $mope_id
 * @property string $mope_co_code
 * @property int $mope_national_permit
 * @property int $mope_international_permit
 * @property string $mope_name
 * @property string $mope_en
 * @property-read \App\Models\Country $country
 * @property-read int|null $revision_history_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverPermit whereMopeCoCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverPermit whereMopeEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverPermit whereMopeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverPermit whereMopeInternationalPermit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverPermit whereMopeName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverPermit whereMopeNationalPermit($value)
 */
class MoverPermit extends Model
{
	protected $table = 'mover_permits';
	protected $primaryKey = 'mope_id';
	public $timestamps = false;
	
	use RevisionableTrait;
	use RevisionableUpgradeTrait;
	
	public function country()
	{
		return $this->hasOne('App\Models\Country', 'mope_co_code', 'co_code');
	}
}
