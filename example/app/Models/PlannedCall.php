<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PlannedCall
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PlannedCall newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PlannedCall newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PlannedCall query()
 * @mixin \Eloquent
 * @property int $plca_id
 * @property int|null $plca_cu_id
 * @property int|null $plca_us_id
 * @property int $plca_status
 * @property int $plca_reason
 * @property string|null $plca_reason_extra
 * @property string $plca_comment
 * @property string $plca_handled_timestamp
 * @property int $plca_criteria_met
 * @property string $plca_planned_date
 * @property int|null $plca_cure_id
 * @property string $plca_cancel_reason
 * @property-read \App\Models\Customer|null $customer
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PlannedCall wherePlcaCancelReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PlannedCall wherePlcaComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PlannedCall wherePlcaCriteriaMet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PlannedCall wherePlcaCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PlannedCall wherePlcaCureId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PlannedCall wherePlcaHandledTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PlannedCall wherePlcaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PlannedCall wherePlcaPlannedDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PlannedCall wherePlcaReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PlannedCall wherePlcaReasonExtra($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PlannedCall wherePlcaStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PlannedCall wherePlcaUsId($value)
 * @property int $plca_type
 * @property string $plca_person_spoken_to
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PlannedCall wherePlcaPersonSpokenTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PlannedCall wherePlcaType($value)
 */

class PlannedCall extends Model
{
    protected $table = 'planned_calls';
	protected $primaryKey = 'plca_id';
	public $timestamps = false;

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'plca_us_id', 'id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'plca_cu_id', 'cu_id');
    }

}
