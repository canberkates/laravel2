<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CustomerLoadExchange
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadExchange newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadExchange newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadExchange query()
 * @mixin \Eloquent
 * @property int $culoex_id
 * @property string $culoex_timestamp
 * @property int|null $culoex_cu_id
 * @property int $culoex_apus_id
 * @property float $culoex_m3
 * @property string|null $culoex_pacu_code
 * @property int $culoex_vehicle_transport
 * @property string|null $culoex_co_code_from
 * @property string $culoex_city_from
 * @property string|null $culoex_co_code_to
 * @property string $culoex_city_to
 * @property int $culoex_date_type
 * @property string|null $culoex_date_load_flexible
 * @property string|null $culoex_date_unload_flexible
 * @property string|null $culoex_no_date_remark
 * @property string $culoex_date_expiration
 * @property int $culoex_type
 * @property string $culoex_remark
 * @property string|null $culoex_reference
 * @property int|null $culoex_deleted_reason
 * @property string|null $culoex_deleted_reason_remark
 * @property int $culoex_deleted
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadExchange whereCuloexApusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadExchange whereCuloexCityFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadExchange whereCuloexCityTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadExchange whereCuloexCoCodeFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadExchange whereCuloexCoCodeTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadExchange whereCuloexCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadExchange whereCuloexDateExpiration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadExchange whereCuloexDateLoadFlexible($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadExchange whereCuloexDateType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadExchange whereCuloexDateUnloadFlexible($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadExchange whereCuloexDeleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadExchange whereCuloexDeletedReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadExchange whereCuloexDeletedReasonRemark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadExchange whereCuloexId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadExchange whereCuloexM3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadExchange whereCuloexNoDateRemark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadExchange whereCuloexPacuCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadExchange whereCuloexReference($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadExchange whereCuloexRemark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadExchange whereCuloexTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadExchange whereCuloexType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadExchange whereCuloexVehicleTransport($value)
 */
class CustomerLoadExchange extends Model
{
    protected $table = 'customer_load_exchange';
	protected $primaryKey = 'culoex_id';
	public $timestamps = false;
}
