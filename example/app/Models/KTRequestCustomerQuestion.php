<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\KTRequestCustomerQuestion
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerQuestion newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerQuestion newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerQuestion query()
 * @mixin \Eloquent
 * @property int $ktrecuqu_id
 * @property int|null $ktrecuqu_re_id
 * @property int|null $ktrecuqu_ktcuqu_id
 * @property int|null $ktrecuqu_cu_id
 * @property int|null $ktrecuqu_cu_re_id
 * @property int|null $ktrecuqu_qu_id
 * @property string $ktrecuqu_timestamp
 * @property int $ktrecuqu_rejection_status
 * @property int $ktrecuqu_sent
 * @property string $ktrecuqu_sent_timestamp
 * @property string $ktrecuqu_answer_1
 * @property string $ktrecuqu_answer_2
 * @property string|null $ktrecuqu_currency
 * @property float $ktrecuqu_amount
 * @property int $ktrecuqu_hide
 * @property int $ktrecuqu_free
 * @property int $ktrecuqu_invoiced
 * @property int $ktrecuqu_anonymized
 * @property-read \App\Models\Customer|null $customer
 * @property-read \App\Models\KTCustomerQuestion|null $customerquestion
 * @property-read \App\Models\Question|null $question
 * @property-read \App\Models\Request|null $request
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerQuestion whereKtrecuquAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerQuestion whereKtrecuquAnonymized($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerQuestion whereKtrecuquAnswer1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerQuestion whereKtrecuquAnswer2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerQuestion whereKtrecuquCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerQuestion whereKtrecuquCuReId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerQuestion whereKtrecuquCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerQuestion whereKtrecuquFree($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerQuestion whereKtrecuquHide($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerQuestion whereKtrecuquId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerQuestion whereKtrecuquInvoiced($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerQuestion whereKtrecuquKtcuquId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerQuestion whereKtrecuquQuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerQuestion whereKtrecuquReId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerQuestion whereKtrecuquRejectionStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerQuestion whereKtrecuquSent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerQuestion whereKtrecuquSentTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerQuestion whereKtrecuquTimestamp($value)
 */
class KTRequestCustomerQuestion extends Model
{
    protected $table = 'kt_request_customer_question';
	protected $primaryKey = 'ktrecuqu_id';
	public $timestamps = false;
	
	public function request()
	{
		return $this->belongsTo('App\Models\Request', 'ktrecuqu_re_id', 're_id');
	}
	
	public function customer()
	{
		return $this->belongsTo('App\Models\Customer', 'ktrecuqu_cu_id', 'cu_id');
	}
	
	public function question()
	{
		return $this->belongsTo('App\Models\Question', 'ktrecuqu_qu_id', 'qu_id');
	}
	
	public function customerquestion()
	{
		return $this->belongsTo('App\Models\KTCustomerQuestion', 'ktrecuqu_ktcuqu_id', 'ktcuqu_id');
	}
	
	
	
}
