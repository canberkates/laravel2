<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CustomerChange
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerChange newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerChange newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerChange query()
 * @mixin \Eloquent
 * @property int $cuch_id
 * @property int|null $cuch_cu_id
 * @property int|null $cuch_apus_id
 * @property string $cuch_table_to_change
 * @property string $cuch_column_to_change
 * @property string $cuch_column_of_cu_id
 * @property string $cuch_field_to_change
 * @property string $cuch_from_value
 * @property string $cuch_to_value
 * @property string $cuch_timestamp
 * @property int $cuch_approve_deny
 * @property string $cuch_timestamp_approve_deny
 * @property string $cuch_reason_deny
 * @property-read \App\Models\Customer|null $customer
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerChange whereCuchApproveDeny($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerChange whereCuchApusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerChange whereCuchColumnOfCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerChange whereCuchColumnToChange($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerChange whereCuchCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerChange whereCuchFieldToChange($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerChange whereCuchFromValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerChange whereCuchId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerChange whereCuchReasonDeny($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerChange whereCuchTableToChange($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerChange whereCuchTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerChange whereCuchTimestampApproveDeny($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerChange whereCuchToValue($value)
 */
class CustomerChange extends Model
{
    protected $table = 'customer_changes';
	protected $primaryKey = 'cuch_id';
	public $timestamps = false;
	
	public function customer()
	{
		return $this->belongsTo('App\Models\Customer', 'cuch_cu_id', 'cu_id');
	}
}
