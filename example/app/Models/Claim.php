<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Claim
 *
 * @property-read \App\Models\KTRequestCustomerPortal $customerrequestportal
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Claim newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Claim newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Claim query()
 * @mixin \Eloquent
 * @property int $cl_id
 * @property int|null $cl_ktrecupo_id
 * @property string $cl_timestamp
 * @property int $cl_status
 * @property string $cl_processed_timestamp
 * @property int|null $cl_processed_us_id
 * @property int $cl_reason
 * @property string $cl_remarks
 * @property string $cl_rejection_reason
 * @property-read \App\Models\User|null $claimprocessedbyuser
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Claim whereClId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Claim whereClKtrecupoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Claim whereClProcessedTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Claim whereClProcessedUsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Claim whereClReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Claim whereClRejectionReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Claim whereClRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Claim whereClStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Claim whereClTimestamp($value)
 */
class Claim extends Model
{
    protected $table = 'claims';
	protected $primaryKey = 'cl_id';
	public $timestamps = false;



    public function customerrequestportal(){
        return $this->belongsTo('App\Models\KTRequestCustomerPortal', 'cl_ktrecupo_id', 'ktrecupo_id');
    }

    public function claimprocessedbyuser(){
        return $this->belongsTo('App\Models\User', 'cl_processed_us_id', 'us_id');
    }
}
