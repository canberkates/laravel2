<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SystemSetting
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SystemSetting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SystemSetting newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SystemSetting query()
 * @mixin \Eloquent
 * @property string $syse_setting
 * @property string $syse_group
 * @property string $syse_name
 * @property string $syse_type
 * @property string $syse_value
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SystemSetting whereSyseGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SystemSetting whereSyseName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SystemSetting whereSyseSetting($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SystemSetting whereSyseType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SystemSetting whereSyseValue($value)
 */
class SystemSetting extends Model
{
    protected $table = 'system_settings';
	protected $primaryKey = 'syse_setting';
	public $timestamps = false;
}
