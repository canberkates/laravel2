<?php

namespace App\Models;

use DB;
use Fico7489\Laravel\RevisionableUpgrade\Traits\RevisionableUpgradeTrait;
use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\Request
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Claim[] $claims
 * @property-read \App\Models\Country $countryfrom
 * @property-read \App\Models\Country $countryto
 * @property-read \App\Models\KTRequestCustomerPortal $requestcustomerportal
 * @property-read \App\Models\WebsiteForm $websiteform
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request query()
 * @mixin \Eloquent
 * @property int $re_id
 * @property string $re_token
 * @property string $re_timestamp
 * @property int $re_automatic
 * @property int $re_automatic_checked
 * @property int $re_status
 * @property int $re_double
 * @property int $re_spam
 * @property int $re_on_hold
 * @property int|null $re_on_hold_by
 * @property int $re_rematch_hide
 * @property int $re_matched_before
 * @property string $re_rejection_timestamp
 * @property int|null $re_rejection_us_id
 * @property int $re_rejection_reason
 * @property int $re_recover_reason
 * @property int $re_date_too_far_mailed
 * @property int $re_match_rating
 * @property string $re_match_rating_remarks
 * @property int|null $re_match_rating_read
 * @property int|null $re_show_this_feedback
 * @property int $re_po_id
 * @property int $re_source
 * @property int $re_exclusive_match
 * @property int $re_type
 * @property int|null $re_wefo_id
 * @property int|null $re_afpafo_id
 * @property string $re_la_code
 * @property int $re_sirelo_customer
 * @property int $re_destination_type
 * @property int $re_request_type
 * @property int $re_moving_size
 * @property string $re_moving_date
 * @property float $re_volume_m3
 * @property float $re_volume_ft3
 * @property string $re_volume_calculator
 * @property int|null $re_voca_id
 * @property int $re_storage
 * @property int $re_packing
 * @property int $re_assembly
 * @property int $re_business
 * @property string $re_street_from
 * @property string $re_zipcode_from
 * @property string $re_city_from
 * @property string|null $re_co_code_from
 * @property int|null $re_reg_id_from
 * @property int $re_residence_from
 * @property string $re_google_maps_from
 * @property string $re_street_to
 * @property string $re_zipcode_to
 * @property string $re_city_to
 * @property string|null $re_co_code_to
 * @property int|null $re_reg_id_to
 * @property int $re_residence_to
 * @property string $re_google_maps_to
 * @property string $re_company_name
 * @property int|null $re_gender
 * @property string $re_first_name
 * @property string $re_family_name
 * @property string $re_full_name
 * @property string $re_telephone1
 * @property string $re_telephone2
 * @property string $re_email
 * @property string $re_remarks
 * @property int $re_internal_called
 * @property string $re_internal_called_timestamp
 * @property string $re_internal_remarks
 * @property int|null $re_score
 * @property int $re_device
 * @property string $re_ip_address
 * @property string|null $re_ip_address_country
 * @property int $re_anonymized
 * @property string|null $re_reply_received
 * @property string|null $re_extra_information_text
 * @property int|null $re_extra_information_status
 * @property string|null $re_extra_information_timestamp
 * @property string $re_platform_source
 * @property string $re_category
 * @property int $re_telephone_1_mail
 * @property int $re_telephone_2_mail
 * @property-read \App\Models\AffiliatePartnerForm $affiliateform
 * @property-read int|null $claims_count
 * @property-read \App\Models\User $onholdby
 * @property-read \App\Models\PremiumLead $premiumlead
 * @property-read \App\Models\User $rejectedby
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @property-read int|null $revision_history_count
 * @property-read \App\Models\VolumeCalculator $volumecalculator
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReAfpafoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReAnonymized($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReAssembly($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReAutomatic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReAutomaticChecked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReBusiness($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReCityFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReCityTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReCoCodeFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReCoCodeTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReCompanyName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReDateTooFarMailed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReDestinationType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReDevice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReDouble($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReExclusiveMatch($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReExtraInformationStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReExtraInformationText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReExtraInformationTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReFamilyName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReFullName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReGoogleMapsFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReGoogleMapsTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReInternalCalled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReInternalCalledTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReInternalRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReIpAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReIpAddressCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReLaCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReMatchRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReMatchRatingRead($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReMatchRatingRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReMatchedBefore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReMovingDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReMovingSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReOnHold($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReOnHoldBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereRePacking($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereRePlatformSource($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereRePoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReRecoverReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReRegIdFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReRegIdTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReRejectionReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReRejectionTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReRejectionUsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReRematchHide($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReReplyReceived($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReRequestType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReResidenceFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReResidenceTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReShowThisFeedback($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReSireloCustomer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReSource($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReSpam($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReStorage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReStreetFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReStreetTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReTelephone1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReTelephone1Mail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReTelephone2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReTelephone2Mail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReVocaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReVolumeCalculator($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReVolumeFt3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReVolumeM3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReWefoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReZipcodeFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReZipcodeTo($value)
 * @property int|null $re_nat_type
 * @property int $re_pd_validate
 * @property string $re_gclid
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReGclid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReNatType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereRePdValidate($value)
 * @property int|null $re_room_size
 * @property int $re_room_size_more
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReRoomSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Request whereReRoomSizeMore($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\KTRequestCustomerPortal[] $requestcustomerportals
 * @property-read int|null $requestcustomerportals_count
 */
class Request extends Model
{
    protected $table = 'requests';
	protected $primaryKey = 're_id';
	public $timestamps = false;
    protected $dontKeepRevisionOf = ['re_automatic', 're_volume_calculator'];

	use RevisionableTrait;
	use RevisionableUpgradeTrait;

	public function requestcustomerportals()
	{
		return $this->hasMany('App\Models\KTRequestCustomerPortal', 'ktrecupo_re_id', 're_id');
	}

	public function requestcustomerportal()
	{
		return $this->hasOne('App\Models\KTRequestCustomerPortal', 'ktrecupo_re_id', 're_id');
	}

	public function websiteform()
	{
		return $this->hasOne('App\Models\WebsiteForm', 'wefo_id', 're_wefo_id');
	}

	public function affiliateform()
	{
		return $this->hasOne('App\Models\AffiliatePartnerForm', 'afpafo_id', 're_afpafo_id');
	}

	public function countryfrom()
	{
		return $this->hasOne('App\Models\Country', 'co_code', 're_co_code_from');
	}

	public function countryto()
	{
		return $this->hasOne('App\Models\Country', 'co_code', 're_co_code_to');
	}

	public function rejectedby()
	{
		return $this->hasOne('App\Models\User', 'id', 're_rejection_us_id');
	}

	public function onholdby()
	{
		return $this->hasOne('App\Models\User', 'id', 're_on_hold_by');
	}

	public function claims()
	{
		return $this->hasMany('App\Models\Claim', 'co_code', 're_id');
	}

	public function premiumlead()
	{
		return $this->hasOne('App\Models\PremiumLead', 'prle_re_id', 're_id');
	}

	public function volumecalculator()
	{
		return $this->hasOne('App\Models\VolumeCalculator', 'voca_id', 're_voca_id');
	}

	public function getFromRegion(): string
    {
        return $this->getRegion("from");
    }

	public function getToRegion(): string
    {
        return $this->getRegion("to");
    }

    private function getRegion($direction): string
    {
        $region = Region::select("reg_co_code", "reg_parent", "reg_name")->where("reg_id", $this->{"re_reg_id_".$direction})->first();

        if(!empty($region))
        {
            if($region->reg_co_code == "US" && !empty($region->reg_parent))
            {
                if (!empty($region->reg_name))
                {
                    return $region->reg_parent.", ".$region->reg_name;
                }
                return $region->reg_parent;

            }
            return ((!empty($region->reg_name)) ? $region->reg_name : $region->reg_parent);
        }
        return false;
    }

    //TODO: ook een getFromCountry en getToCountry bouwen
    public function getCountry($direction, $language = null): string
    {
        $country_column = "co_".strtolower($language);

        $country_code = Country::select($country_column)->where("co_code", $this->{"re_co_code_".$direction})->first();

        if(!empty($country_code))
        {
            return $country_code->$country_column;
        }
        else
        {
            return false;
        }
    }
}
