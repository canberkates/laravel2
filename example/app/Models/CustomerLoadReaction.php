<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CustomerLoadReaction
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadReaction newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadReaction newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadReaction query()
 * @mixin \Eloquent
 * @property int $culore_id
 * @property string $culore_timestamp
 * @property int|null $culore_culoex_id
 * @property int|null $culore_cu_id
 * @property string|null $culore_pacu_code
 * @property float $culore_price
 * @property string|null $culore_load_date_flexible
 * @property string|null $culore_unload_date_flexible
 * @property int $culore_apus_id
 * @property string $culore_remark
 * @property int $culore_confirmed
 * @property string|null $culore_confirmed_remark
 * @property int $culore_deleted
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadReaction whereCuloreApusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadReaction whereCuloreConfirmed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadReaction whereCuloreConfirmedRemark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadReaction whereCuloreCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadReaction whereCuloreCuloexId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadReaction whereCuloreDeleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadReaction whereCuloreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadReaction whereCuloreLoadDateFlexible($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadReaction whereCulorePacuCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadReaction whereCulorePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadReaction whereCuloreRemark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadReaction whereCuloreTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadReaction whereCuloreUnloadDateFlexible($value)
 */
class CustomerLoadReaction extends Model
{
    protected $table = 'customer_load_reaction';
	protected $primaryKey = 'culore_id';
	public $timestamps = false;
}
