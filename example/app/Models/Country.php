<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Country
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country query()
 * @mixin \Eloquent
 * @property string $co_code
 * @property string|null $co_code_3
 * @property int $co_con_id
 * @property string $co_en
 * @property string $co_nl
 * @property string $co_de
 * @property string $co_dk
 * @property string $co_fr
 * @property string $co_ru
 * @property string $co_es
 * @property string $co_it
 * @property string $co_pt
 * @property string $co_pl
 * @property int $in_eu
 * @property int $in_schengen_area
 * @property int|null $co_mare_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereCoCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereCoCode3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereCoConId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereCoDe($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereCoDk($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereCoEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereCoEs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereCoFr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereCoIt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereCoMareId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereCoNl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereCoPl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereCoPt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereCoRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereInEu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereInSchengenArea($value)
 * @property string|null $co_currency
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereCoCurrency($value)
 */
class Country extends Model
{
	protected $table = 'countries';
	//protected $primaryKey = 'pacu_code';
	public $timestamps = false;
}
