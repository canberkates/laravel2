<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\RequestGallery
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestGallery newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestGallery newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestGallery query()
 * @mixin \Eloquent
 * @property int $rega_id
 * @property string $rega_re_token
 * @property string $rega_filename
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestGallery whereRegaFilename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestGallery whereRegaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestGallery whereRegaReToken($value)
 */
class RequestGallery extends Model
{
    protected $table = 'request_gallery';
	protected $primaryKey = 'rega_id';
	public $timestamps = false;
}
