<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\KTServiceProviderNewsletterBlockCountryDestination
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderNewsletterBlockCountryDestination newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderNewsletterBlockCountryDestination newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderNewsletterBlockCountryDestination query()
 * @mixin \Eloquent
 * @property int $ktseprneblcode_id
 * @property int $ktseprneblcode_seprnebl_id
 * @property string $ktseprneblcode_co_code
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderNewsletterBlockCountryDestination whereKtseprneblcodeCoCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderNewsletterBlockCountryDestination whereKtseprneblcodeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderNewsletterBlockCountryDestination whereKtseprneblcodeSeprneblId($value)
 */
class KTServiceProviderNewsletterBlockCountryDestination extends Model
{
    protected $table = 'kt_service_provider_newsletter_block_country_destination';
	protected $primaryKey = 'ktseprneblcode_id';
	public $timestamps = false;
}
