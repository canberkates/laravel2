<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ServiceProviderAdvertorialBlock
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ServiceProviderAdvertorialBlock newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ServiceProviderAdvertorialBlock newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ServiceProviderAdvertorialBlock query()
 * @mixin \Eloquent
 * @property int $sepradbl_id
 * @property int $sepradbl_cu_id
 * @property int $sepradbl_slot
 * @property string $sepradbl_description
 * @property int $sepradbl_status
 * @property string $sepradbl_event_category
 * @property string $sepradbl_event_label
 * @property string $sepradbl_action
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ServiceProviderAdvertorialBlock whereSepradblAction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ServiceProviderAdvertorialBlock whereSepradblCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ServiceProviderAdvertorialBlock whereSepradblDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ServiceProviderAdvertorialBlock whereSepradblEventCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ServiceProviderAdvertorialBlock whereSepradblEventLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ServiceProviderAdvertorialBlock whereSepradblId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ServiceProviderAdvertorialBlock whereSepradblSlot($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ServiceProviderAdvertorialBlock whereSepradblStatus($value)
 */
class ServiceProviderAdvertorialBlock extends Model
{
    protected $table = 'service_provider_advertorial_blocks';
    protected $primaryKey = 'sepradbl_id';
    public $timestamps = false;
}
