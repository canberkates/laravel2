<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\MatchStatistic
 *
 * @property int $mast_re_id
 * @property int $mast_cu_id
 * @property int $mast_matched
 * @property int $mast_podium
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MatchStatistic newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MatchStatistic newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MatchStatistic query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MatchStatistic whereMastCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MatchStatistic whereMastMatched($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MatchStatistic whereMastPodium($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MatchStatistic whereMastReId($value)
 * @mixin \Eloquent
 * @property int|null $mast_match_position
 * @property float|null $mast_average_match
 * @property float|null $mast_match_percentage_before_podium
 * @property float|null $mast_match_percentage_after_podium
 * @property float|null $mast_amount_netto
 * @property float|null $mast_final_price
 * @property float|null $mast_formula_price
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MatchStatistic whereMastAmountNetto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MatchStatistic whereMastAverageMatch($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MatchStatistic whereMastFinalPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MatchStatistic whereMastFormulaPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MatchStatistic whereMastMatchPercentageAfterPodium($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MatchStatistic whereMastMatchPercentageBeforePodium($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MatchStatistic whereMastMatchPosition($value)
 * @property int|null $mast_ktcupo_id
 * @property float|null $mast_correction_bonus
 * @property float|null $mast_lead_price_netto
 * @property int $mast_capping_limitation
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MatchStatistic whereMastCappingLimitation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MatchStatistic whereMastCorrectionBonus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MatchStatistic whereMastKtcupoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MatchStatistic whereMastLeadPriceNetto($value)
 */
class MatchStatistic extends Model
{
    protected $table = 'match_statistics';
	public $timestamps = false;

    protected $primaryKey = ['mast_re_id', 'mast_cu_id'];
    public $incrementing = false;

    protected $guarded = [];

}
