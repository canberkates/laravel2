<?php

namespace App\Models;

use Fico7489\Laravel\RevisionableUpgrade\Traits\RevisionableUpgradeTrait;
use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\CustomerCachedTab
 *
 * @property-read \App\Models\Customer $customer
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerCachedTab newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerCachedTab newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerCachedTab query()
 * @mixin \Eloquent
 * @property int $cucata_id
 * @property int $cucata_cu_id
 * @property int $cucata_int_nat
 * @property int $cucata_int
 * @property int $cucata_nat
 * @property int $cucata_leads_store
 * @property int $cucata_conversion_tools
 * @property int $cucata_pause
 * @property int $cucata_cancelled
 * @property int $cucata_credit_hold
 * @property int $cucata_prepayment_credit_hold
 * @property int $cucata_debt_collector
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerCachedTab whereCucataCancelled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerCachedTab whereCucataConversionTools($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerCachedTab whereCucataCreditHold($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerCachedTab whereCucataCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerCachedTab whereCucataDebtCollector($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerCachedTab whereCucataId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerCachedTab whereCucataInt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerCachedTab whereCucataIntNat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerCachedTab whereCucataLeadsStore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerCachedTab whereCucataNat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerCachedTab whereCucataPause($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerCachedTab whereCucataPrepaymentCreditHold($value)
 */

class CustomerCachedTab extends Model
{
    protected $table = 'customer_cached_tabs';
    protected $primaryKey = 'cucata_id';
    public $timestamps = false;

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'cucata_cu_id', 'cu_id');
    }

}
