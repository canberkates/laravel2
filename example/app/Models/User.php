<?php

namespace App\Models;

use Fico7489\Laravel\RevisionableUpgrade\Traits\RevisionableUpgradeTrait;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Venturecraft\Revisionable\RevisionableTrait;


/**
 * App\Models\User
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CustomerRemark[] $remarks
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CustomerStatus[] $statuses
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $us_id
 * @property string $us_heartbeat
 * @property string $us_session_id
 * @property string $us_username
 * @property string $us_password
 * @property int $us_bypass_authenticator
 * @property string $us_authenticator_secret
 * @property string $us_name
 * @property string $us_email
 * @property string $us_date_of_birth
 * @property string|null $us_telephone_internal
 * @property string|null $us_telephone_external
 * @property string|null $us_mobile
 * @property int $us_sales
 * @property int $us_partnerdesk
 * @property int $us_customerservice
 * @property int $us_finance
 * @property int $us_sirelo
 * @property int $us_is_deleted
 * @property int $us_force_change_password
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read int|null $remarks_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @property-read int|null $revision_history_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @property-read int|null $statuses_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User role($roles, $guard = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUsAuthenticatorSecret($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUsBypassAuthenticator($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUsCustomerservice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUsDateOfBirth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUsEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUsFinance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUsForceChangePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUsHeartbeat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUsIsDeleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUsMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUsName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUsPartnerdesk($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUsPassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUsSales($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUsSessionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUsSirelo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUsTelephoneExternal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUsTelephoneInternal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUsUsername($value)
 * @property int $us_cafe_planner
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUsCafePlanner($value)
 * @property int $us_export_vpn
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUsExportVpn($value)
 */
class User extends Authenticatable
{
    use Notifiable;
	use HasRoles;
	
	use RevisionableTrait;
	use RevisionableUpgradeTrait;
	
	//enable this if you want use methods that gets information about creating
	protected $revisionCreationsEnabled = true;
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
	
	public function statuses(){
		return $this->hasMany('App\Models\CustomerStatus', 'cust_employee', 'id');
	}
	
	public function remarks(){
		return $this->hasMany('App\Models\CustomerRemark', 'cure_employee', 'id');
	}
}
