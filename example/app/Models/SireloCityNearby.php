<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\AdyenPayment
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdyenPayment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdyenPayment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdyenPayment query()
 * @mixin \Eloquent
 * @property string $key
 * @property string $city
 * @property string $country_code
 * @property string $nearby
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCityNearby whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCityNearby whereCountryCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCityNearby whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCityNearby whereNearby($value)
 */
class SireloCityNearby extends Model
{
    protected $table = 'sirelo_cities_nearby';
	public $timestamps = false;
}
