<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\KTPortalRegion
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTPortalRegion newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTPortalRegion newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTPortalRegion query()
 * @mixin \Eloquent
 * @property int $ktpore_id
 * @property int $ktpore_po_id
 * @property int $ktpore_reg_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTPortalRegion whereKtporeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTPortalRegion whereKtporePoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTPortalRegion whereKtporeRegId($value)
 */
class KTPortalRegion extends Model
{
    protected $table = 'kt_portal_region';
	protected $primaryKey = 'ktpore_id';
	public $timestamps = false;
}
