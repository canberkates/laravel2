<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\InvoiceLine
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InvoiceLine newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InvoiceLine newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InvoiceLine query()
 * @mixin \Eloquent
 * @property int $inli_id
 * @property int|null $inli_in_id
 * @property int|null $inli_ktrecupo_id
 * @property int|null $inli_ktrecuqu_id
 * @property int|null $inli_crdeinli_id
 * @property string $inli_group
 * @property string $inli_description
 * @property int $inli_no_claim_discount
 * @property string $inli_currency
 * @property float $inli_amount
 * @property float $inli_amount_discount
 * @property float $inli_amount_netto
 * @property float $inli_amount_customer
 * @property float $inli_amount_discount_customer
 * @property float $inli_amount_netto_customer
 * @property float $inli_amount_eur
 * @property float $inli_amount_discount_eur
 * @property float $inli_amount_netto_eur
 * @property int $inli_leac_number
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InvoiceLine whereInliAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InvoiceLine whereInliAmountCustomer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InvoiceLine whereInliAmountDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InvoiceLine whereInliAmountDiscountCustomer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InvoiceLine whereInliAmountDiscountEur($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InvoiceLine whereInliAmountEur($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InvoiceLine whereInliAmountNetto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InvoiceLine whereInliAmountNettoCustomer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InvoiceLine whereInliAmountNettoEur($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InvoiceLine whereInliCrdeinliId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InvoiceLine whereInliCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InvoiceLine whereInliDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InvoiceLine whereInliGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InvoiceLine whereInliId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InvoiceLine whereInliInId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InvoiceLine whereInliKtrecupoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InvoiceLine whereInliKtrecuquId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InvoiceLine whereInliLeacNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InvoiceLine whereInliNoClaimDiscount($value)
 */
class InvoiceLine extends Model
{
    protected $table = 'invoice_lines';
	protected $primaryKey = 'inli_id';
	public $timestamps = false;
}
