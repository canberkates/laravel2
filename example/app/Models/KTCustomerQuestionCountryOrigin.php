<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\KTCustomerQuestionCountryOrigin
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerQuestionCountryOrigin newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerQuestionCountryOrigin newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerQuestionCountryOrigin query()
 * @mixin \Eloquent
 * @property int $ktcuqucoor_id
 * @property int $ktcuqucoor_ktcuqu_id
 * @property string $ktcuqucoor_co_code
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerQuestionCountryOrigin whereKtcuqucoorCoCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerQuestionCountryOrigin whereKtcuqucoorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerQuestionCountryOrigin whereKtcuqucoorKtcuquId($value)
 */
class KTCustomerQuestionCountryOrigin extends Model
{
    protected $table = 'kt_customer_question_country_origin';
	protected $primaryKey = 'ktcuqucoor_id';
	public $timestamps = false;
}
