<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Survey1
 *
 * @property-read \App\Models\Request $request
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Survey1Customer[] $survey1customers
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey1 newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey1 newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey1 query()
 * @mixin \Eloquent
 * @property int $su_id
 * @property int $su_re_id
 * @property int $su_on_hold
 * @property int|null $su_on_hold_by
 * @property string $su_timestamp
 * @property int $su_opened
 * @property string $su_opened_timestamp
 * @property int $su_submitted
 * @property string $su_submitted_timestamp
 * @property int $su_reminder
 * @property string $su_reminder_old
 * @property int $su_checked
 * @property string $su_checked_remarks
 * @property int $su_trustpilot
 * @property string $su_trustpilot_timestamp
 * @property int $su_anonymized
 * @property-read int|null $survey1customers_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey1 whereSuAnonymized($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey1 whereSuChecked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey1 whereSuCheckedRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey1 whereSuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey1 whereSuOnHold($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey1 whereSuOnHoldBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey1 whereSuOpened($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey1 whereSuOpenedTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey1 whereSuReId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey1 whereSuReminder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey1 whereSuReminderOld($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey1 whereSuSubmitted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey1 whereSuSubmittedTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey1 whereSuTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey1 whereSuTrustpilot($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey1 whereSuTrustpilotTimestamp($value)
 */
class Survey1 extends Model
{
    protected $table = 'surveys_1';
	protected $primaryKey = 'su_id';
	public $timestamps = false;
	
	public function request()
	{
		return $this->belongsTo('App\Models\Request', 'su_re_id', 're_id');
	}
	
	public function survey1customers(){
		return $this->hasMany('App\Models\Survey1Customer', 'sucu_su_id', 'su_id');
	}
}
