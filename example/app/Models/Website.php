<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Website
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Website newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Website newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Website query()
 * @mixin \Eloquent
 * @property int $we_id
 * @property string $we_website
 * @property string $we_mail_from_name
 * @property int $we_sirelo
 * @property int $we_sirelo_active
 * @property string|null $we_sirelo_co_code
 * @property string|null $we_sirelo_la_code
 * @property string $we_sirelo_url
 * @property int $we_sirelo_bucket
 * @property int $we_sirelo_top_mover_months
 * @property string $we_sirelo_ftp_server
 * @property string $we_sirelo_ftp_username
 * @property string $we_sirelo_ftp_password
 * @property string $we_sirelo_ftp_folder
 * @property string $we_sirelo_customer_location
 * @property string $we_sirelo_customer_review_slug
 * @property string $we_sirelo_partner_program_location
 * @property string $we_privacy_policy_url
 * @property string $we_terms_of_use_url
 * @property string $we_sirelo_survey_location
 * @property string|null $we_sirelo_request_form_location
 * @property int $we_states_enabled
 * @property-read \App\Models\Language $language
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\WebsiteSubdomain[] $subdomains
 * @property-read int|null $subdomains_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\WebsiteForm[] $websiteforms
 * @property-read int|null $websiteforms_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Website whereWeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Website whereWeMailFromName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Website whereWePrivacyPolicyUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Website whereWeSirelo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Website whereWeSireloActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Website whereWeSireloBucket($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Website whereWeSireloCoCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Website whereWeSireloCustomerLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Website whereWeSireloCustomerReviewSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Website whereWeSireloFtpFolder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Website whereWeSireloFtpPassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Website whereWeSireloFtpServer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Website whereWeSireloFtpUsername($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Website whereWeSireloLaCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Website whereWeSireloPartnerProgramLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Website whereWeSireloRequestFormLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Website whereWeSireloSurveyLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Website whereWeSireloTopMoverMonths($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Website whereWeSireloUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Website whereWeStatesEnabled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Website whereWeTermsOfUseUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Website whereWeWebsite($value)
 * @property string|null $we_personal_dashboard_url
 * @property int $we_personal_dashboard_on
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Website whereWePersonalDashboardOn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Website whereWePersonalDashboardUrl($value)
 * @property string|null $we_register_company_location
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Website whereWeRegisterCompanyLocation($value)
 * @property string|null $we_moving_assistant_unsubscribe_location
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Website whereWeMovingAssistantUnsubscribeLocation($value)
 */
class Website extends Model
{
    protected $table = 'websites';
	protected $primaryKey = 'we_id';
	public $timestamps = false;
    
    public function websiteforms(){
        return $this->hasMany('App\Models\WebsiteForm', 'wefo_we_id', 'we_id');
    }
    
    public function subdomains(){
        return $this->hasMany('App\Models\WebsiteSubdomain', 'wesu_we_id', 'we_id');
    }
    
    public function language(){
        return $this->hasOne('App\Models\Language', 'la_code', 'we_sirelo_la_code');
    }
    
}
