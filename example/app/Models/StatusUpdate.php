<?php

namespace App\Models;

use Fico7489\Laravel\RevisionableUpgrade\Traits\RevisionableUpgradeTrait;
use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\StatusUpdate
 *
 * @property-read \App\Models\KTCustomerPortal $customerportal
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StatusUpdate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StatusUpdate newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StatusUpdate query()
 * @mixin \Eloquent
 * @property int $stup_id
 * @property int|null $stup_ktcupo_id
 * @property int|null $stup_us_id
 * @property string $stup_date
 * @property int $stup_hour
 * @property int $stup_status
 * @property string|null $stup_description
 * @property string $stup_users
 * @property int $stup_processed
 * @property-read int|null $revision_history_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StatusUpdate whereStupDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StatusUpdate whereStupDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StatusUpdate whereStupHour($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StatusUpdate whereStupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StatusUpdate whereStupKtcupoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StatusUpdate whereStupProcessed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StatusUpdate whereStupStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StatusUpdate whereStupUsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StatusUpdate whereStupUsers($value)
 */
class StatusUpdate extends Model
{
    protected $table = 'status_updates';
	protected $primaryKey = 'stup_id';
	public $timestamps = false;
	
	use RevisionableTrait;
	use RevisionableUpgradeTrait;
	
	//enable this if you want use methods that gets information about creating
	protected $revisionCreationsEnabled = true;
	
	public function customerportal()
	{
		return $this->belongsTo('App\Models\KTCustomerPortal', 'ktcupo_id', 'stup_ktcupo_id');
	}
}
