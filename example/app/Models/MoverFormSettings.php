<?php

namespace App\Models;

use Fico7489\Laravel\RevisionableUpgrade\Traits\RevisionableUpgradeTrait;
use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\MoverData
 *
 * @property-read \App\Models\Customer $customer
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverData query()
 * @mixin \Eloquent
 * @property int $mofose_id
 * @property int $mofose_cu_id
 * @property int $mofose_afpafo_id
 * @property string $mofose_theme_color
 * @property int $mofose_from_use_google_api
 * @property int $mofose_from_ask_residence
 * @property int $mofose_from_ask_elevator
 * @property int $mofose_from_ask_parking_permit
 * @property int $mofose_from_ask_walking_distance
 * @property int $mofose_to_use_google_api
 * @property int $mofose_to_ask_residence
 * @property int $mofose_to_ask_elevator
 * @property int $mofose_to_ask_parking_permit
 * @property int $mofose_to_ask_walking_distance
 * @property int $mofose_international_form
 * @property int $mofose_show_vc
 * @property int $mofose_ask_flexible_moving_date
 * @property int $mofose_ask_storage
 * @property int $mofose_ask_handyman
 * @property int $mofose_ask_disposal_of_items
 * @property int $mofose_send_emails
 * @property string|null $mofose_header
 * @property string|null $mofose_footer
 * @property string|null $mofose_email_from
 * @property string|null $mofose_email_to
 * @property-read \App\Models\AffiliatePartnerForm $affiliatepartnerform
 * @property-read int|null $revision_history_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverFormSettings whereMofoseAfpafoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverFormSettings whereMofoseAskDisposalOfItems($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverFormSettings whereMofoseAskFlexibleMovingDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverFormSettings whereMofoseAskHandyman($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverFormSettings whereMofoseAskStorage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverFormSettings whereMofoseCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverFormSettings whereMofoseEmailFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverFormSettings whereMofoseEmailTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverFormSettings whereMofoseFooter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverFormSettings whereMofoseFromAskElevator($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverFormSettings whereMofoseFromAskParkingPermit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverFormSettings whereMofoseFromAskResidence($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverFormSettings whereMofoseFromAskWalkingDistance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverFormSettings whereMofoseFromUseGoogleApi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverFormSettings whereMofoseHeader($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverFormSettings whereMofoseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverFormSettings whereMofoseInternationalForm($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverFormSettings whereMofoseSendEmails($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverFormSettings whereMofoseShowVc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverFormSettings whereMofoseThemeColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverFormSettings whereMofoseToAskElevator($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverFormSettings whereMofoseToAskParkingPermit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverFormSettings whereMofoseToAskResidence($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverFormSettings whereMofoseToAskWalkingDistance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverFormSettings whereMofoseToUseGoogleApi($value)
 * @property int $mofose_ask_packing
 * @property int $mofose_ask_assembly
 * @property int $mofose_voca_enabled
 * @property string|null $mofose_voca_email_subject
 * @property string|null $mofose_voca_email_content
 * @property int $mofose_voca_show_quote_button
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverFormSettings whereMofoseAskAssembly($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverFormSettings whereMofoseAskPacking($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverFormSettings whereMofoseVocaEmailContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverFormSettings whereMofoseVocaEmailSubject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverFormSettings whereMofoseVocaEnabled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverFormSettings whereMofoseVocaShowQuoteButton($value)
 * @property int $mofose_show_direct_pricing
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverFormSettings whereMofoseShowDirectPricing($value)
 * @property string $mofose_font_color
 * @property string $mofose_background_color
 * @property string|null $mofose_thank_you_url
 * @property string|null $mofose_title
 * @property string|null $mofose_subtext
 * @property int $mofose_show_moving_sizes
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverFormSettings whereMofoseBackgroundColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverFormSettings whereMofoseFontColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverFormSettings whereMofoseShowMovingSizes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverFormSettings whereMofoseSubtext($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverFormSettings whereMofoseThankYouUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverFormSettings whereMofoseTitle($value)
 */
class MoverFormSettings extends Model
{
	protected $table = 'mover_form_settings';
	protected $primaryKey = 'mofose_id';
	public $timestamps = false;

	use RevisionableTrait;
	use RevisionableUpgradeTrait;

	public function customer()
	{
		return $this->hasOne('App\Models\Customer', 'mofose_cu_id', 'cu_id');
	}

	public function affiliatepartnerform()
	{
		return $this->hasOne('App\Models\AffiliatePartnerForm', 'mofose_afpafo_id', 'afpafo_id');
	}
}
