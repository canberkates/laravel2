<?php

namespace App\Models;

use Fico7489\Laravel\RevisionableUpgrade\Traits\RevisionableUpgradeTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\CustomerReviewScore
 *
 * @property-read \App\Models\Customer $customer
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactPerson newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactPerson newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactPerson query()
 * @property int $curesc_cu_id
 * @property string $curesc_platform
 * @property float $curesc_score
 * @property int $curesc_amount
 * @property string $curesc_name
 * @property string $curesc_telephone
 * @property string $curesc_address
 * @property string|null $curesc_street
 * @property string|null $curesc_zipcode
 * @property string|null $curesc_city
 * @property string $curesc_customer_website
 * @property string $curesc_url
 * @property string|null $curesc_search_url
 * @property int $curesc_skip
 * @property int $curesc_processed
 * @property string|null $curesc_timestamp
 * @property int $curesc_use_reviews
 * @property int|null $curesc_accepted
 * @property string|null $curesc_description
 * @property string|null $curesc_remarks
 * @property int $curesc_mail
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescAccepted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescCustomerWebsite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescMail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescPlatform($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescProcessed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescSearchUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescSkip($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescTelephone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescUseReviews($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescZipcode($value)
 * @mixin \Eloquent
 * @property int|null $curesc_processed_by
 * @property string|null $curesc_processed_timestamp
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescProcessedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescProcessedTimestamp($value)
 * @property int $curesc_id
 * @property int|null $curesc_website_validate_type
 * @property string|null $curesc_website_validate_var_1
 * @property string|null $curesc_website_validate_var_2
 * @property string|null $curesc_website_validate_var_3
 * @property string|null $curesc_website_validate_var_4
 * @property string|null $curesc_website_validate_var_5
 * @property string|null $curesc_website_validate_var_6
 * @property int $curesc_skip_counter
 * @property int|null $curesc_lock_user_id
 * @property string|null $curesc_lock_timestamp
 * @property string|null $curesc_history
 * @property int $curesc_processed_counter
 * @property string|null $curesc_final_url_erp
 * @property string|null $curesc_final_url_other
 * @property int $curesc_permanently_closed
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescFinalUrlErp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescFinalUrlOther($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescHistory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescLockTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescLockUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescPermanentlyClosed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescProcessedCounter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescSkipCounter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescWebsiteValidateType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescWebsiteValidateVar1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescWebsiteValidateVar2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescWebsiteValidateVar3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescWebsiteValidateVar4($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescWebsiteValidateVar5($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescWebsiteValidateVar6($value)
 * @property int $curesc_customer_deleted
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescCustomerDeleted($value)
 * @property int|null $curesc_gaco_id
 * @property int $curesc_type
 * @property string|null $curesc_wait_timestamp
 * @property string|null $curesc_new_company_website_validation_html
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescGacoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescNewCompanyWebsiteValidationHtml($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerReviewScore whereCurescWaitTimestamp($value)
 */
class CustomerReviewScore extends Model
{
    protected $table = 'customer_review_scores';
    protected $primaryKey = 'curesc_id';
    public $timestamps = false;

    //customer relation
    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'curesc_cu_id', 'cu_id');
    }

}
