<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PaymentRate
 *
 * @property-read \App\Models\KTCustomerPortal $customerportal
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentRate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentRate newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentRate query()
 * @mixin \Eloquent
 * @property int $para_id
 * @property int|null $para_ktcupo_id
 * @property string $para_date
 * @property float $para_rate
 * @property int $para_discount_type
 * @property float $para_discount_rate
 * @property string|null $para_remark
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentRate whereParaDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentRate whereParaDiscountRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentRate whereParaDiscountType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentRate whereParaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentRate whereParaKtcupoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentRate whereParaRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentRate whereParaRemark($value)
 * @property int $para_type
 * @property int|null $para_nat_type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentRate whereParaNatType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentRate whereParaType($value)
 */
class PaymentRate extends Model
{
    protected $table = 'payment_rates';
	protected $primaryKey = 'para_id';
	public $timestamps = false;
	
	public function customerportal()
	{
		return $this->belongsTo('App\Models\KTCustomerPortal', 'para_ktcupo_id', 'ktcupo_id');
	}
}
