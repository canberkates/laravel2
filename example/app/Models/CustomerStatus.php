<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CustomerStatus
 *
 * @property-read \App\Models\Customer $customer
 * @property-read \App\Models\CustomerRemark $remark
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerStatus newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerStatus newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerStatus query()
 * @mixin \Eloquent
 * @property int $cust_id
 * @property int|null $cust_cu_id
 * @property string $cust_date
 * @property int|null $cust_employee
 * @property int $cust_status
 * @property int $cust_reason
 * @property float|null $cust_turnover_netto
 * @property string|null $cust_number_of_leads
 * @property string|null $cust_capping_of_leads
 * @property string|null $cust_price_per_lead
 * @property int|null $cust_booking_source
 * @property string|null $cust_lifetime
 * @property string|null $cust_previous_leads
 * @property string|null $cust_previous_capping
 * @property string|null $cust_previous_price
 * @property string|null $cust_remark
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerStatus whereCustBookingSource($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerStatus whereCustCappingOfLeads($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerStatus whereCustCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerStatus whereCustDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerStatus whereCustEmployee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerStatus whereCustId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerStatus whereCustLifetime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerStatus whereCustNumberOfLeads($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerStatus whereCustPreviousCapping($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerStatus whereCustPreviousLeads($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerStatus whereCustPreviousPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerStatus whereCustPricePerLead($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerStatus whereCustReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerStatus whereCustRemark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerStatus whereCustStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerStatus whereCustTurnoverNetto($value)
 */
class CustomerStatus extends Model
{
    protected $table = 'customer_statuses';
	protected $primaryKey = 'cust_id';
	public $timestamps = false;
	
	public function customer()
	{
		return $this->belongsTo('App\Models\Customer', 'cust_cu_id', 'cu_id');
	}
	
	public function user()
	{
		return $this->belongsTo('App\Models\User', 'cust_employee', 'id');
	}
	
	public function remark()
	{
		return $this->belongsTo('App\Models\CustomerRemark', 'cust_id', 'cure_status_id');
	}
}
