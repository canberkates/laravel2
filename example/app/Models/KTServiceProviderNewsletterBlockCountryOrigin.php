<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\KTServiceProviderNewsletterBlockCountryOrigin
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderNewsletterBlockCountryOrigin newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderNewsletterBlockCountryOrigin newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderNewsletterBlockCountryOrigin query()
 * @mixin \Eloquent
 * @property int $ktseprneblcoor_id
 * @property int $ktseprneblcoor_seprnebl_id
 * @property string $ktseprneblcoor_co_code
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderNewsletterBlockCountryOrigin whereKtseprneblcoorCoCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderNewsletterBlockCountryOrigin whereKtseprneblcoorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderNewsletterBlockCountryOrigin whereKtseprneblcoorSeprneblId($value)
 */
class KTServiceProviderNewsletterBlockCountryOrigin extends Model
{
    protected $table = 'kt_service_provider_newsletter_block_country_origin';
	protected $primaryKey = 'ad_id';
	public $timestamps = false;
}
