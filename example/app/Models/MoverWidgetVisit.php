<?php

namespace App\Models;

use Fico7489\Laravel\RevisionableUpgrade\Traits\RevisionableUpgradeTrait;
use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\MoverWidgetVisit
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverWidgetVisit newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverWidgetVisit newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverWidgetVisit query()
 * @mixin \Eloquent
 * @pro
 */
class MoverWidgetVisit extends Model
{
    protected $table = 'mover_widget_visits';
    protected $primaryKey = 'mowivi_id';
    public $timestamps = false;

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'mowivi_cu_id', 'cu_id');
    }
}
