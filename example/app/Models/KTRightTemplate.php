<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\KTRightTemplate
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRightTemplate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRightTemplate newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRightTemplate query()
 * @mixin \Eloquent
 * @property int $ktrite_id
 * @property int $ktrite_ri_id
 * @property int $ktrite_te_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRightTemplate whereKtriteId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRightTemplate whereKtriteRiId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRightTemplate whereKtriteTeId($value)
 */
class KTRightTemplate extends Model
{
    protected $table = 'kt_rights_templates';
	protected $primaryKey = 'ktrite_id';
	public $timestamps = false;
}
