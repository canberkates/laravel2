<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\FlaggedReview
 *
 * @property-read \App\Models\Customer $customer
 * @property-read \App\Models\Survey1 $survey
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey1Customer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey1Customer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey1Customer query()
 * @mixin \Eloquent
 * @property int $flre_id
 * @property int $flre_su_id
 * @property string|null $flre_timestamp
 * @property int $flre_status
 * @property string|null $flre_personal_information
 * @property string|null $flre_spam
 * @property string|null $flre_offensive_language
 * @property string|null $flre_conflicts
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FlaggedReview whereFlreConflicts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FlaggedReview whereFlreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FlaggedReview whereFlreOffensiveLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FlaggedReview whereFlrePersonalInformation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FlaggedReview whereFlreSpam($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FlaggedReview whereFlreStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FlaggedReview whereFlreSuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FlaggedReview whereFlreTimestamp($value)
 */
class FlaggedReview extends Model
{
    protected $table = 'flagged_reviews';
	protected $primaryKey = 'flre_id';
	public $timestamps = false;

	public function survey()
	{
		return $this->belongsTo('App\Models\Survey2', 'flre_su_id', 'su_id');
	}
}
