<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\BankLine
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BankLine newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BankLine newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BankLine query()
 * @mixin \Eloquent
 * @property int $bali_id
 * @property string $bali_statement_number
 * @property string $bali_date
 * @property string|null $bali_timestamp
 * @property string $bali_reference
 * @property string $bali_credit_debit
 * @property string $bali_debtor_details
 * @property int|null $bali_debtor_cu_id
 * @property int|null $bali_debtor_in_id
 * @property string|null $bali_event_code
 * @property int $bali_source
 * @property string|null $bali_original_reference
 * @property string $bali_adyen_data
 * @property string $bali_bank_details
 * @property float $bali_amount
 * @property string|null $bali_currency
 * @property float $bali_amount_fc
 * @property string $bali_information
 * @property string $bali_file_name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BankLine whereBaliAdyenData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BankLine whereBaliAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BankLine whereBaliAmountFc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BankLine whereBaliBankDetails($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BankLine whereBaliCreditDebit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BankLine whereBaliCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BankLine whereBaliDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BankLine whereBaliDebtorCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BankLine whereBaliDebtorDetails($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BankLine whereBaliDebtorInId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BankLine whereBaliEventCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BankLine whereBaliFileName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BankLine whereBaliId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BankLine whereBaliInformation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BankLine whereBaliOriginalReference($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BankLine whereBaliReference($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BankLine whereBaliSource($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BankLine whereBaliStatementNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BankLine whereBaliTimestamp($value)
 */
class BankLine extends Model
{
    protected $table = 'bank_lines';
	protected $primaryKey = 'bali_id';
	public $timestamps = false;
}
