<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\KTAdyenPaymentInvoice
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTAdyenPaymentInvoice newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTAdyenPaymentInvoice newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTAdyenPaymentInvoice query()
 * @mixin \Eloquent
 * @property int $ktadpain_id
 * @property int $ktadpain_adpa_id
 * @property int $ktadpain_in_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTAdyenPaymentInvoice whereKtadpainAdpaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTAdyenPaymentInvoice whereKtadpainId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTAdyenPaymentInvoice whereKtadpainInId($value)
 */
class KTAdyenPaymentInvoice extends Model
{
    protected $table = 'kt_adyen_payments_invoices';
	protected $primaryKey = 'ktadpain_id';
	public $timestamps = false;
}
