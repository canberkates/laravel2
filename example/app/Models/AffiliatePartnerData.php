<?php

namespace App\Models;

use Fico7489\Laravel\RevisionableUpgrade\Traits\RevisionableUpgradeTrait;
use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\AffiliatePartnerData
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerData newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerData newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerData query()
 * @mixin \Eloquent
 * @property int $afpada_id
 * @property int|null $afpada_cu_id
 * @property int $afpada_status
 * @property int $afpada_max_requests_month
 * @property int $afpada_max_requests_month_left
 * @property int $afpada_max_requests_day
 * @property int $afpada_max_requests_day_left
 * @property float $afpada_price_per_lead
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @property-read int|null $revision_history_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerData whereAfpadaCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerData whereAfpadaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerData whereAfpadaMaxRequestsDay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerData whereAfpadaMaxRequestsDayLeft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerData whereAfpadaMaxRequestsMonth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerData whereAfpadaMaxRequestsMonthLeft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerData whereAfpadaPricePerLead($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerData whereAfpadaStatus($value)
 * @property string|null $afpada_revenues
 * @property string|null $afpada_revenues_calculated_timestamp
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerData whereAfpadaRevenues($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerData whereAfpadaRevenuesCalculatedTimestamp($value)
 */
class AffiliatePartnerData extends Model
{
    protected $table = 'affiliate_partner_data';
	protected $primaryKey = 'afpada_id';
	public $timestamps = false;
	
	use RevisionableTrait;
	use RevisionableUpgradeTrait;
	
	//enable this if you want use methods that gets information about creating
	protected $revisionCreationsEnabled = true;
}
