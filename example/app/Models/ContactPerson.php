<?php

namespace App\Models;

use Fico7489\Laravel\RevisionableUpgrade\Traits\RevisionableUpgradeTrait;
use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\ContactPerson
 *
 * @property-read \App\Models\ApplicationUser $application_user
 * @property-read \App\Models\Customer $customer
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactPerson newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactPerson newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactPerson query()
 * @mixin \Eloquent
 * @property int $cope_id
 * @property int|null $cope_cu_id
 * @property int|null $cope_apus_id
 * @property int $cope_gender
 * @property string|null $cope_title
 * @property string|null $cope_full_name
 * @property string|null $cope_first_name
 * @property string|null $cope_last_name
 * @property int|null $cope_function
 * @property string|null $cope_function_remark
 * @property int|null $cope_department
 * @property int|null $cope_department_role
 * @property string|null $cope_language
 * @property string|null $cope_email
 * @property string|null $cope_telephone
 * @property string|null $cope_mobile
 * @property string|null $cope_remark
 * @property int $cope_deleted
 * @property-read int|null $revision_history_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactPerson whereCopeApusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactPerson whereCopeCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactPerson whereCopeDeleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactPerson whereCopeDepartment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactPerson whereCopeDepartmentRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactPerson whereCopeEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactPerson whereCopeFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactPerson whereCopeFullName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactPerson whereCopeFunction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactPerson whereCopeFunctionRemark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactPerson whereCopeGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactPerson whereCopeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactPerson whereCopeLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactPerson whereCopeLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactPerson whereCopeMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactPerson whereCopeRemark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactPerson whereCopeTelephone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactPerson whereCopeTitle($value)
 */
class ContactPerson extends Model
{
	protected $table = 'contact_persons';
	protected $primaryKey = 'cope_id';
	public $timestamps = false;
	
	use RevisionableTrait;
	use RevisionableUpgradeTrait;
	
	//enable this if you want use methods that gets information about creating
	protected $revisionCreationsEnabled = true;
	
	public function customer()
	{
		return $this->belongsTo('App\Models\Customer', 'cope_cu_id', 'cu_id');
	}
	
	public function application_user()
	{
		return $this->hasOne('App\Models\ApplicationUser', 'apus_id', 'cope_apus_id');
	}
}
