<?php

namespace App\Models;

use Fico7489\Laravel\RevisionableUpgrade\Traits\RevisionableUpgradeTrait;
use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\RevisionableTrait;


/**
 * App\Models\KTMoverVolumeCalculatorItem
 *
 * @property int $ktmovocait_cu_id
 * @property int $ktmovocait_movocait_id
 * @property float $ktmovocait_custom_volume_m3
 * @property float $ktmovocait_custom_weight_kg
 * @property int $ktmovocait_custom_room
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @property-read int|null $revision_history_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTMoverVolumeCalculatorItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTMoverVolumeCalculatorItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTMoverVolumeCalculatorItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTMoverVolumeCalculatorItem whereKtmovocaitCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTMoverVolumeCalculatorItem whereKtmovocaitCustomRoom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTMoverVolumeCalculatorItem whereKtmovocaitCustomVolumeM3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTMoverVolumeCalculatorItem whereKtmovocaitCustomWeightKg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTMoverVolumeCalculatorItem whereKtmovocaitMovocaitId($value)
 * @mixin \Eloquent
 * @property float|null $ktmovocait_custom_packing_costs
 * @property float|null $ktmovocait_custom_unpacking_costs
 * @property float|null $ktmovocait_custom_assembly_costs
 * @property float|null $ktmovocait_custom_disassembly_costs
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTMoverVolumeCalculatorItem whereKtmovocaitCustomAssemblyCosts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTMoverVolumeCalculatorItem whereKtmovocaitCustomDisassemblyCosts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTMoverVolumeCalculatorItem whereKtmovocaitCustomPackingCosts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTMoverVolumeCalculatorItem whereKtmovocaitCustomUnpackingCosts($value)
 */
class KTMoverVolumeCalculatorItem extends Model
{
	protected $table = 'kt_mover_volume_calculator_item';
	protected $primaryKey = 'movocaro_id';
	public $timestamps = false;

	use RevisionableTrait;
	use RevisionableUpgradeTrait;


}
