<?php

namespace App\Models;

use Fico7489\Laravel\RevisionableUpgrade\Traits\RevisionableUpgradeTrait;
use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\Subscription
 *
 * @property int $sub_id
 * @property string $sub_name
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @property-read int|null $revision_history_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription whereSubId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription whereSubName($value)
 * @mixin \Eloquent
 */
class Subscription extends Model
{
    protected $table = 'subscriptions';
    protected $primaryKey = 'sub_id';
    public $timestamps = false;

    use RevisionableTrait;
    use RevisionableUpgradeTrait;

}
