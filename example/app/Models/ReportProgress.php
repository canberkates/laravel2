<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ReportProgress
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReportProgress newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReportProgress newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReportProgress query()
 * @mixin \Eloquent
 * @property int $repr_id
 * @property int|null $repr_us_id
 * @property int $repr_rep_id
 * @property string $repr_comment
 * @property string $repr_create_timestamp
 * @property string $repr_start_timestamp
 * @property string $repr_finish_timestamp
 * @property int $repr_status
 * @property int $repr_waiting
 * @property string $repr_progress_array
 * @property string $repr_posted_values
 * @property int $repr_mysql_thread
 * @property int $repr_read
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReportProgress whereReprComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReportProgress whereReprCreateTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReportProgress whereReprFinishTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReportProgress whereReprId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReportProgress whereReprMysqlThread($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReportProgress whereReprPostedValues($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReportProgress whereReprProgressArray($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReportProgress whereReprRead($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReportProgress whereReprRepId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReportProgress whereReprStartTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReportProgress whereReprStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReportProgress whereReprUsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReportProgress whereReprWaiting($value)
 */
class ReportProgress extends Model
{
    protected $table = 'report_progress';
	protected $primaryKey = 'repr_id';
	public $timestamps = false;
}
