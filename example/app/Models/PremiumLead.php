<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PremiumLead
 *
 * @property int $prle_id
 * @property int $prle_re_id
 * @property int $prle_status
 * @property string|null $prle_preferred_appointment_timestamp
 * @property string|null $prle_planned_appointment_timestamp
 * @property string|null $prle_planned_timestamp
 * @property int|null $prle_planned_by
 * @property int|null $prle_residence_type_from
 * @property string|null $prle_inside_elevator_from
 * @property string|null $prle_obstacles_for_moving_van_from
 * @property string|null $prle_parking_restrictions_and_permits_from
 * @property string|null $prle_walking_distance_to_house_from
 * @property int|null $prle_residence_type_to
 * @property string|null $prle_inside_elevator_to
 * @property string|null $prle_obstacles_for_moving_van_to
 * @property string|null $prle_parking_restrictions_and_permits_to
 * @property string|null $prle_walking_distance_to_house_to
 * @property string|null $prle_list_of_all_items
 * @property string|null $prle_special_items
 * @property string|null $prle_extra_info_moving_date
 * @property int|null $prle_packing_by_mover
 * @property string|null $prle_packing_by_mover_remarks
 * @property int|null $prle_unpacking_by_mover
 * @property string|null $prle_unpacking_by_mover_remarks
 * @property int|null $prle_disassembling_by_mover
 * @property string|null $prle_disassembling_by_mover_remarks
 * @property int|null $prle_assembling_by_mover
 * @property string|null $prle_assembling_by_mover_remarks
 * @property string|null $prle_handyman_service
 * @property int|null $prle_storage
 * @property string|null $prle_storage_remarks
 * @property int|null $prle_insurance
 * @property string|null $prle_insurance_remarks
 * @property string|null $prle_insurance_amount
 * @property string|null $prle_screenshots
 * @property int|null $prle_survey_executor
 * @property int $prle_appointment_email_sent
 * @property string|null $prle_pcloud_public_link
 * @property int|null $prle_pcloud_file_id
 * @property-read \App\Models\Request $request
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiumLead newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiumLead newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiumLead query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiumLead wherePrleAppointmentEmailSent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiumLead wherePrleAssemblingByMover($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiumLead wherePrleAssemblingByMoverRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiumLead wherePrleDisassemblingByMover($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiumLead wherePrleDisassemblingByMoverRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiumLead wherePrleExtraInfoMovingDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiumLead wherePrleHandymanService($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiumLead wherePrleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiumLead wherePrleInsideElevatorFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiumLead wherePrleInsideElevatorTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiumLead wherePrleInsurance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiumLead wherePrleInsuranceAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiumLead wherePrleInsuranceRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiumLead wherePrleListOfAllItems($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiumLead wherePrleObstaclesForMovingVanFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiumLead wherePrleObstaclesForMovingVanTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiumLead wherePrlePackingByMover($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiumLead wherePrlePackingByMoverRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiumLead wherePrleParkingRestrictionsAndPermitsFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiumLead wherePrleParkingRestrictionsAndPermitsTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiumLead wherePrlePcloudFileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiumLead wherePrlePcloudPublicLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiumLead wherePrlePlannedAppointmentTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiumLead wherePrlePlannedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiumLead wherePrlePlannedTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiumLead wherePrlePreferredAppointmentTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiumLead wherePrleReId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiumLead wherePrleResidenceTypeFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiumLead wherePrleResidenceTypeTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiumLead wherePrleScreenshots($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiumLead wherePrleSpecialItems($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiumLead wherePrleStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiumLead wherePrleStorage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiumLead wherePrleStorageRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiumLead wherePrleSurveyExecutor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiumLead wherePrleUnpackingByMover($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiumLead wherePrleUnpackingByMoverRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiumLead wherePrleWalkingDistanceToHouseFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiumLead wherePrleWalkingDistanceToHouseTo($value)
 * @mixin \Eloquent
 */
class PremiumLead extends Model
{
    protected $table = 'premium_leads';
	protected $primaryKey = 'prle_id';
	public $timestamps = false;
	
	public function request()
	{
		return $this->belongsTo('App\Models\Request', 'prle_re_id', 're_id');
	}
}
