<?php

namespace App\Models;

use Fico7489\Laravel\RevisionableUpgrade\Traits\RevisionableUpgradeTrait;
use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\SireloLink
 *
 * @property-read \App\Models\Customer $customer
 * @property-read \App\Models\Request $request
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @property-read \App\Models\WebsiteReview $website_review
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloLink newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloLink newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloLink query()
 * @mixin \Eloquent
 * @property int $sihy_id
 * @property string $sihy_co_code
 * @property int|null $sihy_me_id
 * @property int|null $sihy_cuob_id
 * @property string $sihy_link
 * @property string $sihy_timestamp
 * @property-read int|null $revision_history_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloLink whereSihyCoCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloLink whereSihyCuobId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloLink whereSihyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloLink whereSihyLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloLink whereSihyMeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloLink whereSihyTimestamp($value)
 */
class SireloLink extends Model
{
    protected $table = 'sirelo_hyperlinks';
    protected $primaryKey = 'sihy_id';
    public $timestamps = false;

    use RevisionableTrait;
    use RevisionableUpgradeTrait;
  
}
