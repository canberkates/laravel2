<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\MacroRegion
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MacroRegion newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MacroRegion newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MacroRegion query()
 * @mixin \Eloquent
 * @property int $mare_id
 * @property string $mare_name
 * @property string|null $mare_continent_name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MacroRegion whereMareContinentName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MacroRegion whereMareId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MacroRegion whereMareName($value)
 */
class MacroRegion extends Model
{
    protected $table = 'macro_regions';
	protected $primaryKey = 'mare_id';
	public $timestamps = false;
}
