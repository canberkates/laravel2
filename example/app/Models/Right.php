<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Right
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Right newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Right newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Right query()
 * @mixin \Eloquent
 * @property int $ri_id
 * @property int $ri_ap_id
 * @property string $ri_right
 * @property string $ri_name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Right whereRiApId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Right whereRiId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Right whereRiName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Right whereRiRight($value)
 */
class Right extends Model
{
    protected $table = 'rights';
	protected $primaryKey = 'ri_id';
	public $timestamps = false;
}
