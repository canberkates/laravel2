<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\RequestUpdate
 *
 * @property int $reup_id
 * @property int $reup_re_id
 * @property string $reup_timestamp
 * @property string|null $reup_key
 * @property string $reup_value
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestUpdate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestUpdate newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestUpdate query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestUpdate whereReupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestUpdate whereReupKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestUpdate whereReupReId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestUpdate whereReupTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestUpdate whereReupValue($value)
 * @mixin \Eloquent
 */
class RequestUpdate extends Model
{
    protected $table = 'request_updates';
    protected $primaryKey = 'reup_id';
    public $timestamps = false;
}
