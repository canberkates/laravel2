<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\AffiliatePartnerFormVisit
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerFormVisit newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerFormVisit newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerFormVisit query()
 * @mixin \Eloquent
 * @property int $afpafovi_id
 * @property int|null $afpafovi_afpafo_id
 * @property string $afpafovi_date
 * @property int $afpafovi_visits
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerFormVisit whereAfpafoviAfpafoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerFormVisit whereAfpafoviDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerFormVisit whereAfpafoviId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerFormVisit whereAfpafoviVisits($value)
 */
class AffiliatePartnerFormVisit extends Model
{
    protected $table = 'affiliate_partner_form_visits';
	protected $primaryKey = 'afpafovi_id';
	public $timestamps = false;
}
