<?php
/**
 * Created by PhpStorm.
 * User: Arjan de Coninck
 * Date: 31-3-2020
 * Time: 15:02
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
/**
 * App\Models\SireloSetting
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloSetting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloSetting newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloSetting query()
 * @mixin \Eloquent
 * @property int $sise_id
 * @property string $sise_timestamp
 * @property string $sise_name
 * @property string $sise_description
 * @property string $sise_value
 * @property string $sise_type
 * @property string $sise_updated_timestamp
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloSetting whereSiseDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloSetting whereSiseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloSetting whereSiseName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloSetting whereSiseTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloSetting whereSiseType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloSetting whereSiseUpdatedTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloSetting whereSiseValue($value)
 */
class SireloSetting extends Model
{
    protected $table = 'sirelo_settings';
    protected $primaryKey = 'sise_id';
    public $timestamps = false;
}
