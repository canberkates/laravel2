<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Region
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region query()
 * @mixin \Eloquent
 * @property int $reg_id
 * @property int|null $reg_po_id
 * @property string $reg_co_code
 * @property string $reg_parent
 * @property string $reg_name
 * @property int $reg_destination_type
 * @property int $reg_deleted
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region whereRegCoCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region whereRegDeleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region whereRegDestinationType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region whereRegId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region whereRegName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region whereRegParent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region whereRegPoId($value)
 */
class Region extends Model
{
    protected $table = 'regions';
	protected $primaryKey = 'reg_id';
	public $timestamps = false;
}
