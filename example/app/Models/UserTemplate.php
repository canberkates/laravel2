<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UserTemplate
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserTemplate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserTemplate newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserTemplate query()
 * @mixin \Eloquent
 * @property int $te_id
 * @property string $te_name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserTemplate whereTeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserTemplate whereTeName($value)
 */
class UserTemplate extends Model
{
    protected $table = 'user_templates';
	protected $primaryKey = 'te_id';
	public $timestamps = false;
}
