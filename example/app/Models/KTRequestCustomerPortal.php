<?php

namespace App\Models;

use Fico7489\Laravel\RevisionableUpgrade\Traits\RevisionableUpgradeTrait;
use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\KTRequestCustomerPortal
 *
 * @property-read \App\Models\Customer $customer
 * @property-read \App\Models\Portal $portal
 * @property-read \App\Models\Request $request
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerPortal newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerPortal newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerPortal query()
 * @mixin \Eloquent
 * @property int $ktrecupo_id
 * @property int|null $ktrecupo_re_id
 * @property int|null $ktrecupo_ktcupo_id
 * @property int|null $ktrecupo_cu_id
 * @property int|null $ktrecupo_cu_re_id
 * @property int|null $ktrecupo_po_id
 * @property string $ktrecupo_timestamp
 * @property int|null $ktrecupo_us_id
 * @property int $ktrecupo_type
 * @property int $ktrecupo_rematch
 * @property float $ktrecupo_amount
 * @property float $ktrecupo_amount_eur
 * @property int $ktrecupo_discount_type
 * @property float $ktrecupo_discount_rate
 * @property float $ktrecupo_amount_discount
 * @property float $ktrecupo_amount_netto
 * @property float $ktrecupo_amount_netto_eur
 * @property int $ktrecupo_free
 * @property float $ktrecupo_free_percentage
 * @property int $ktrecupo_free_reason
 * @property int $ktrecupo_invoiced
 * @property int|null $ktrecupo_read_extra_information
 * @property-read \App\Models\User $matchedby
 * @property-read int|null $revision_history_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerPortal whereKtrecupoAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerPortal whereKtrecupoAmountDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerPortal whereKtrecupoAmountEur($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerPortal whereKtrecupoAmountNetto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerPortal whereKtrecupoAmountNettoEur($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerPortal whereKtrecupoCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerPortal whereKtrecupoCuReId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerPortal whereKtrecupoDiscountRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerPortal whereKtrecupoDiscountType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerPortal whereKtrecupoFree($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerPortal whereKtrecupoFreePercentage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerPortal whereKtrecupoFreeReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerPortal whereKtrecupoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerPortal whereKtrecupoInvoiced($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerPortal whereKtrecupoKtcupoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerPortal whereKtrecupoPoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerPortal whereKtrecupoReId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerPortal whereKtrecupoReadExtraInformation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerPortal whereKtrecupoRematch($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerPortal whereKtrecupoTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerPortal whereKtrecupoType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerPortal whereKtrecupoUsId($value)
 * @property float $ktrecupo_amount_customer
 * @property float $ktrecupo_amount_netto_customer
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerPortal whereKtrecupoAmountCustomer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerPortal whereKtrecupoAmountNettoCustomer($value)
 * @property int|null $ktrecupo_match_position
 * @property float|null $ktrecupo_average_match
 * @property float|null $ktrecupo_formula_price
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerPortal whereKtrecupoAverageMatch($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerPortal whereKtrecupoFormulaPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTRequestCustomerPortal whereKtrecupoMatchPosition($value)
 * @property-read \App\Models\KTCustomerPortal|null $customerPortal
 */
class KTRequestCustomerPortal extends Model
{
    protected $table = 'kt_request_customer_portal';
	protected $primaryKey = 'ktrecupo_id';
	public $timestamps = false;

	use RevisionableTrait;
	use RevisionableUpgradeTrait;

	public function request()
	{
		return $this->belongsTo('App\Models\Request', 'ktrecupo_re_id', 're_id');
	}

	public function portal(){
		return $this->belongsTo('App\Models\Portal', 'ktrecupo_po_id', 'po_id');
	}

	public function customerPortal(){
		return $this->belongsTo('App\Models\KTCustomerPortal', 'ktrecupo_ktcupo_id', 'ktcupo_id');
	}

	public function customer(){
		return $this->belongsTo('App\Models\Customer', 'ktrecupo_cu_id', 'cu_id');
	}

	public function matchedby(){
		return $this->hasOne('App\Models\User', 'id', 'ktrecupo_us_id');
	}


}
