<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CustomerCreditTransaction
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerCreditTransaction newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerCreditTransaction newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerCreditTransaction query()
 * @mixin \Eloquent
 * @property int $cucrtr_id
 * @property int|null $cucrtr_apus_id
 * @property string $cucrtr_timestamp
 * @property int|null $cucrtr_cu_id
 * @property int $cucrtr_amount
 * @property int $cucrtr_ktrecupo_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerCreditTransaction whereCucrtrAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerCreditTransaction whereCucrtrApusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerCreditTransaction whereCucrtrCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerCreditTransaction whereCucrtrId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerCreditTransaction whereCucrtrKtrecupoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerCreditTransaction whereCucrtrTimestamp($value)
 */
class CustomerCreditTransaction extends Model
{
    protected $table = 'customer_credit_transactions';
	protected $primaryKey = 'cucrtr_id';
	public $timestamps = false;
}
