<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\KTWebsiteLanguage
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTWebsiteLanguage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTWebsiteLanguage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTWebsiteLanguage query()
 * @mixin \Eloquent
 * @property int $ktwela_id
 * @property int|null $ktwela_website_id
 * @property int|null $ktwela_subdomain_id
 * @property string $ktwela_la_code
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTWebsiteLanguage whereKtwelaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTWebsiteLanguage whereKtwelaLaCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTWebsiteLanguage whereKtwelaSubdomainId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTWebsiteLanguage whereKtwelaWebsiteId($value)
 */
class KTWebsiteLanguage extends Model
{
    protected $table = 'kt_website_languages';
	protected $primaryKey = 'ktwela_id';
	public $timestamps = false;
}
