<?php

namespace App\Models;

use Fico7489\Laravel\RevisionableUpgrade\Traits\RevisionableUpgradeTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\KTCustomerGatheredReview
 *
 * @property-read \App\Models\Customer $customer
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerGatheredReview newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerGatheredReview newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerGatheredReview query()
 * @property int $ktcugare_id
 * @property int $ktcugare_cu_id
 * @property string $ktcugare_platform
 * @property float $ktcugare_score
 * @property int $ktcugare_amount
 * @property string|null $ktcugare_url
 * @property string $ktcugare_timestamp_updated
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerGatheredReview whereKtcugareAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerGatheredReview whereKtcugareCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerGatheredReview whereKtcugareId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerGatheredReview whereKtcugarePlatform($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerGatheredReview whereKtcugareScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerGatheredReview whereKtcugareTimestampUpdated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerGatheredReview whereKtcugareUrl($value)
 * @mixin \Eloquent
 * @property float|null $ktcugare_original_score
 * @property int|null $ktcugare_original_range
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerGatheredReview whereKtcugareOriginalRange($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerGatheredReview whereKtcugareOriginalScore($value)
 */
class KTCustomerGatheredReview extends Model
{
    protected $table = 'kt_customer_gathered_reviews';
    protected $primaryKey = 'ktcugare_id';
    public $timestamps = false;

    //customer relation
    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'ktcugare_cu_id', 'cu_id');
    }

}
