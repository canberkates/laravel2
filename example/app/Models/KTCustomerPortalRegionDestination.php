<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\KTCustomerPortalRegionDestination
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortalRegionDestination newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortalRegionDestination newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortalRegionDestination query()
 * @mixin \Eloquent
 * @property int $ktcuporede_id
 * @property int|null $ktcuporede_ktcupo_id
 * @property int $ktcuporede_reg_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortalRegionDestination whereKtcuporedeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortalRegionDestination whereKtcuporedeKtcupoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortalRegionDestination whereKtcuporedeRegId($value)
 */
class KTCustomerPortalRegionDestination extends Model
{
    protected $table = 'kt_customer_portal_region_destination';
	protected $primaryKey = 'ktcuporede_id';
	public $timestamps = false;
}
