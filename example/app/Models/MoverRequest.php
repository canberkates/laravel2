<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\MoverRequest
 *
 * @property int $more_id
 * @property string $more_token
 * @property int $more_cu_more_id
 * @property int $more_cu_id
 * @property int $more_afpafo_id
 * @property string $more_timestamp
 * @property int $more_status
 * @property int $more_spam
 * @property string $more_la_code
 * @property string $more_moving_date
 * @property float $more_volume_m3
 * @property float $more_volume_ft3
 * @property int|null $more_voca_id
 * @property string|null $more_special_items
 * @property int $more_packing
 * @property int $more_assembly
 * @property int $more_storage
 * @property int $more_handyman
 * @property int $more_item_disposal
 * @property string|null $more_service_remarks
 * @property string|null $more_street_from
 * @property string|null $more_zipcode_from
 * @property string $more_city_from
 * @property string $more_co_code_from
 * @property int $more_floor_type_from
 * @property int $more_walking_distance_from
 * @property int $more_elevator_available_from
 * @property int $more_parking_permit_needed_from
 * @property string|null $more_street_to
 * @property string|null $more_zipcode_to
 * @property string $more_city_to
 * @property string $more_co_code_to
 * @property int $more_floor_type_to
 * @property int $more_walking_distance_to
 * @property int $more_elevator_available_to
 * @property int $more_parking_permit_needed_to
 * @property string $more_full_name
 * @property string $more_telephone_1
 * @property string $more_email
 * @property int $more_contact_method
 * @property string|null $more_device
 * @property string|null $more_ip_address
 * @property int $more_anonymized
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest whereMoreAfpafoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest whereMoreAnonymized($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest whereMoreAssembly($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest whereMoreCityFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest whereMoreCityTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest whereMoreCoCodeFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest whereMoreCoCodeTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest whereMoreContactMethod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest whereMoreCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest whereMoreCuMoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest whereMoreDevice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest whereMoreElevatorAvailableFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest whereMoreElevatorAvailableTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest whereMoreEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest whereMoreFloorTypeFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest whereMoreFloorTypeTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest whereMoreFullName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest whereMoreHandyman($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest whereMoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest whereMoreIpAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest whereMoreItemDisposal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest whereMoreLaCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest whereMoreMovingDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest whereMorePacking($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest whereMoreParkingPermitNeededFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest whereMoreParkingPermitNeededTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest whereMoreServiceRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest whereMoreSpam($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest whereMoreSpecialItems($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest whereMoreStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest whereMoreStorage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest whereMoreStreetFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest whereMoreStreetTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest whereMoreTelephone1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest whereMoreTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest whereMoreToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest whereMoreVocaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest whereMoreVolumeFt3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest whereMoreVolumeM3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest whereMoreWalkingDistanceFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest whereMoreWalkingDistanceTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest whereMoreZipcodeFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverRequest whereMoreZipcodeTo($value)
 * @mixin \Eloquent
 */

class MoverRequest extends Model
{
    protected $table = 'mover_requests';
	protected $primaryKey = 'more_id';
	public $timestamps = false;
}
