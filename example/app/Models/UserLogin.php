<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UserLogin
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserLogin newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserLogin newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserLogin query()
 * @mixin \Eloquent
 * @property int $uslo_id
 * @property int|null $uslo_us_id
 * @property int|null $uslo_apus_id
 * @property int $uslo_ap_id
 * @property string $uslo_timestamp
 * @property string $uslo_ip_address
 * @property string $uslo_user_agent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserLogin whereUsloApId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserLogin whereUsloApusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserLogin whereUsloId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserLogin whereUsloIpAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserLogin whereUsloTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserLogin whereUsloUsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserLogin whereUsloUserAgent($value)
 */
class UserLogin extends Model
{
    protected $table = 'user_logins';
	protected $primaryKey = 'uslo_id';
	public $timestamps = false;
}
