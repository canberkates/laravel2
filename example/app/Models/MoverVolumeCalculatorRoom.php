<?php

namespace App\Models;

use Fico7489\Laravel\RevisionableUpgrade\Traits\RevisionableUpgradeTrait;
use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\RevisionableTrait;


/**
 * App\Models\MoverVolumeCalculatorRoom
 *
 * @property int $movocaro_id
 * @property string $movocaro_room
 * @property string $movocaro_svg
 * @property string $movocaro_la_code
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @property-read int|null $revision_history_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverVolumeCalculatorRoom newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverVolumeCalculatorRoom newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverVolumeCalculatorRoom query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverVolumeCalculatorRoom whereMovocaroId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverVolumeCalculatorRoom whereMovocaroLaCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverVolumeCalculatorRoom whereMovocaroRoom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverVolumeCalculatorRoom whereMovocaroSvg($value)
 * @mixin \Eloquent
 */
class MoverVolumeCalculatorRoom extends Model
{
	protected $table = 'mover_volume_calculator_room';
	protected $primaryKey = 'movocaro_id';
	public $timestamps = false;

	use RevisionableTrait;
	use RevisionableUpgradeTrait;


}
