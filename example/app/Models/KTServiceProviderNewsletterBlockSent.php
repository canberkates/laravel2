<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\KTServiceProviderNewsletterBlockSent
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderNewsletterBlockSent newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderNewsletterBlockSent newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderNewsletterBlockSent query()
 * @mixin \Eloquent
 * @property int $ktseprneblse_id
 * @property string $ktseprneblse_timestamp
 * @property int $ktseprneblse_cu_id
 * @property int $ktseprneblse_seprnebl_id
 * @property int $ktseprneblse_slot
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderNewsletterBlockSent whereKtseprneblseCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderNewsletterBlockSent whereKtseprneblseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderNewsletterBlockSent whereKtseprneblseSeprneblId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderNewsletterBlockSent whereKtseprneblseSlot($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderNewsletterBlockSent whereKtseprneblseTimestamp($value)
 */
class KTServiceProviderNewsletterBlockSent extends Model
{
    protected $table = 'kt_service_provider_newsletter_block_sent';
    protected $primaryKey = 'ktseprneblse_id';
    public $timestamps = false;
}
