<?php

namespace App\Models;

use Fico7489\Laravel\RevisionableUpgrade\Traits\RevisionableUpgradeTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\GatheredCompany
 *
 * @property-read \App\Models\Customer $customer
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatheredCompany newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatheredCompany newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatheredCompany query()
 * @property int $gaco_id
 * @property string $gaco_searched_customer
 * @property string $gaco_searched_city
 * @property string $gaco_searched_co_code
 * @property string $gaco_name
 * @property string|null $gaco_title
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatheredCompany whereGacoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatheredCompany whereGacoName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatheredCompany whereGacoSearchedCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatheredCompany whereGacoSearchedCoCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatheredCompany whereGacoSearchedCustomer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatheredCompany whereGacoTitle($value)
 * @mixin \Eloquent
 * @property int $gaco_whitelisted
 * @property int $gaco_blacklisted
 * @property int $gaco_processed
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatheredCompany whereGacoBlacklisted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatheredCompany whereGacoProcessed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatheredCompany whereGacoWhitelisted($value)
 * @property int $gaco_country_status
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatheredCompany whereGacoCountryStatus($value)
 */
class GatheredCompany extends Model
{
    protected $table = 'gathered_companies';
    protected $primaryKey = 'gaco_id';
    public $timestamps = false;


}
