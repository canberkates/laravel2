<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\KTCustomerQuestionLanguage
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerQuestionLanguage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerQuestionLanguage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerQuestionLanguage query()
 * @mixin \Eloquent
 * @property int $ktcuqula_id
 * @property int $ktcuqula_ktcuqu_id
 * @property string $ktcuqula_la_code
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerQuestionLanguage whereKtcuqulaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerQuestionLanguage whereKtcuqulaKtcuquId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerQuestionLanguage whereKtcuqulaLaCode($value)
 */
class KTCustomerQuestionLanguage extends Model
{
    protected $table = 'kt_customer_question_language';
	protected $primaryKey = 'ktcuqula_id';
	public $timestamps = false;
}
