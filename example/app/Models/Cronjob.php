<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Cronjob
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cronjob newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cronjob newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cronjob query()
 * @mixin \Eloquent
 * @property int $cr_id
 * @property string $cr_task
 * @property string $cr_subtask
 * @property int $cr_trigger
 * @property int $cr_error_count
 * @property string $cr_interval
 * @property string $cr_last_ran
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cronjob whereCrErrorCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cronjob whereCrId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cronjob whereCrInterval($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cronjob whereCrLastRan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cronjob whereCrSubtask($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cronjob whereCrTask($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cronjob whereCrTrigger($value)
 */


class Cronjob extends Model
{

    //Makes all fields mass-fillable by using Laravel Upsert
    protected $guarded = [];

    protected $table = 'cronjobs';
	protected $primaryKey = 'cr_id';
	public $timestamps = false;
}
