<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\KTCustomerQuestion
 *
 * @property-read \App\Models\Question $questions
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerQuestion newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerQuestion newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerQuestion query()
 * @mixin \Eloquent
 * @property int $ktcuqu_id
 * @property int $ktcuqu_cu_id
 * @property int $ktcuqu_qu_id
 * @property string $ktcuqu_description
 * @property int $ktcuqu_status
 * @property int $ktcuqu_delay
 * @property float $ktcuqu_rate
 * @property int $ktcuqu_international_moves
 * @property int $ktcuqu_national_moves
 * @property int $ktcuqu_max_requests_month
 * @property int $ktcuqu_max_requests_month_left
 * @property int $ktcuqu_max_requests_week
 * @property int $ktcuqu_max_requests_week_left
 * @property int $ktcuqu_max_requests_day
 * @property int $ktcuqu_max_requests_day_left
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerQuestion whereKtcuquCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerQuestion whereKtcuquDelay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerQuestion whereKtcuquDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerQuestion whereKtcuquId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerQuestion whereKtcuquInternationalMoves($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerQuestion whereKtcuquMaxRequestsDay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerQuestion whereKtcuquMaxRequestsDayLeft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerQuestion whereKtcuquMaxRequestsMonth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerQuestion whereKtcuquMaxRequestsMonthLeft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerQuestion whereKtcuquMaxRequestsWeek($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerQuestion whereKtcuquMaxRequestsWeekLeft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerQuestion whereKtcuquNationalMoves($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerQuestion whereKtcuquQuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerQuestion whereKtcuquRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerQuestion whereKtcuquStatus($value)
 */
class KTCustomerQuestion extends Model
{
    protected $table = 'kt_customer_question';
	protected $primaryKey = 'ktcuqu_id';
	public $timestamps = false;
	
	public function questions(){
		return $this->hasOne('App\Models\Question', 'qu_id', 'ktcuqu_qu_id');
	}
	
}
