<?php

namespace App\Models;

use Fico7489\Laravel\RevisionableUpgrade\Traits\RevisionableUpgradeTrait;
use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\KTMoverComment
 *
 * @property-read \App\Models\Customer $customer
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTMoverComment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTMoverComment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTMoverComment query()
 * @mixin \Eloquent
 * @property int $ktmoco_id
 * @property int $ktmoco_cu_id
 * @property int $ktmoco_su_id
 * @property int $ktmoco_type
 * @property int $ktmoco_read
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTMoverComment whereKtmocoCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTMoverComment whereKtmocoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTMoverComment whereKtmocoRead($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTMoverComment whereKtmocoSuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTMoverComment whereKtmocoType($value)
 */
class KTMoverComment extends Model
{
    protected $table = 'kt_mover_comments';
    protected $primaryKey = 'ktmoco_id';
    public $timestamps = false;

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'ktmoco_cu_id', 'cu_id');
    }

}
