<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\RegionRecognition
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegionRecognition newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegionRecognition newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegionRecognition query()
 * @mixin \Eloquent
 * @property int $rere_id
 * @property string $rere_co_code
 * @property int $rere_destination_type
 * @property string $rere_zipcode
 * @property string $rere_city
 * @property int $rere_reg_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegionRecognition whereRereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegionRecognition whereRereCoCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegionRecognition whereRereDestinationType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegionRecognition whereRereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegionRecognition whereRereRegId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegionRecognition whereRereZipcode($value)
 */
class RegionRecognition extends Model
{
    protected $table = 'region_recognitions';
	protected $primaryKey = 'rere_id';
	public $timestamps = false;
}
