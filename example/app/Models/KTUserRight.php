<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\KTUserRight
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTUserRight newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTUserRight newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTUserRight query()
 * @mixin \Eloquent
 * @property int $ktusri_id
 * @property int $ktusri_us_id
 * @property int $ktusri_ri_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTUserRight whereKtusriId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTUserRight whereKtusriRiId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTUserRight whereKtusriUsId($value)
 */
class KTUserRight extends Model
{
    protected $table = 'kt_user_right';
	protected $primaryKey = 'ktusri_id';
	public $timestamps = false;
}
