<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\AffiliatePartnerRequest
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerRequest newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerRequest newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerRequest query()
 * @mixin \Eloquent
 * @property int $afpare_id
 * @property int|null $afpare_cu_id
 * @property int|null $afpare_re_id
 * @property int $afpare_status
 * @property int $afpare_rejection_reason
 * @property string|null $afpare_currency
 * @property float $afpare_amount
 * @property float $afpare_amount_eur
 * @property-read \App\Models\Request|null $request
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerRequest whereAfpareAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerRequest whereAfpareAmountEur($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerRequest whereAfpareCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerRequest whereAfpareCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerRequest whereAfpareId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerRequest whereAfpareReId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerRequest whereAfpareRejectionReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliatePartnerRequest whereAfpareStatus($value)
 */
class AffiliatePartnerRequest extends Model
{
    protected $table = 'affiliate_partner_requests';
	protected $primaryKey = 'afpare_id';
	public $timestamps = false;
	
	public function request()
	{
		return $this->belongsTo('App\Models\Request', 'afpare_re_id', 're_id');
	}
}
