<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\KTCustomerPortalCountry
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortalCountry newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortalCountry newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortalCountry query()
 * @mixin \Eloquent
 * @property int $ktcupoco_id
 * @property int|null $ktcupoco_ktcupo_id
 * @property string $ktcupoco_co_code
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortalCountry whereKtcupocoCoCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortalCountry whereKtcupocoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortalCountry whereKtcupocoKtcupoId($value)
 */
class KTCustomerPortalCountry extends Model
{
    protected $table = 'kt_customer_portal_country';
	protected $primaryKey = 'ktcupoco_id';
	public $timestamps = false;
}
