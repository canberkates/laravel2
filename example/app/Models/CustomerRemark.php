<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CustomerRemark
 *
 * @property-read \App\Models\Customer $customer
 * @property-read \App\Models\CustomerStatus $status
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerRemark newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerRemark newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerRemark query()
 * @mixin \Eloquent
 * @property int $cure_id
 * @property int|null $cure_cu_id
 * @property string $cure_timestamp
 * @property string $cure_contact_date
 * @property int|null $cure_department
 * @property int|null $cure_employee
 * @property int|null $cure_medium
 * @property int|null $cure_direction
 * @property string $cure_text
 * @property int|null $cure_status_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerRemark whereCureContactDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerRemark whereCureCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerRemark whereCureDepartment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerRemark whereCureDirection($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerRemark whereCureEmployee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerRemark whereCureId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerRemark whereCureMedium($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerRemark whereCureStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerRemark whereCureText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerRemark whereCureTimestamp($value)
 */
class CustomerRemark extends Model
{
    protected $table = 'customer_remarks';
	protected $primaryKey = 'cure_id';
	public $timestamps = false;
	
	public function customer()
	{
		return $this->belongsTo('App\Models\Customer', 'cure_cu_id', 'cu_id');
	}
	
	public function user()
	{
		return $this->belongsTo('App\Models\User', 'cure_employee', 'id');
	}
	
	public function status()
	{
		return $this->belongsTo('App\Models\CustomerStatus', 'cure_status_id', 'cust_id');
	}
}
