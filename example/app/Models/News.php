<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\News
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News query()
 * @mixin \Eloquent
 * @property int $ne_id
 * @property string $ne_timestamp
 * @property string $ne_title
 * @property string $ne_content
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereNeContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereNeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereNeTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereNeTitle($value)
 */
class News extends Model
{
    protected $table = 'news';
	protected $primaryKey = 'ne_id';
	public $timestamps = false;
}
