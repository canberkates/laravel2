<?php

namespace App\Models;

use App\Data\DeliveryLead;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\RequestDelivery
 *
 * @property-read \App\Models\KTCustomerPortal $customerportal
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestDelivery newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestDelivery newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestDelivery query()
 * @mixin \Eloquent
 * @property int $rede_id
 * @property int $rede_customer_type
 * @property int|null $rede_ktcupo_id
 * @property int|null $rede_ktcuqu_id
 * @property int $rede_type
 * @property string $rede_value
 * @property string|null $rede_extra
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestDelivery whereRedeCustomerType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestDelivery whereRedeExtra($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestDelivery whereRedeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestDelivery whereRedeKtcupoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestDelivery whereRedeKtcuquId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestDelivery whereRedeType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestDelivery whereRedeValue($value)
 */
class RequestDelivery extends Model
{
    protected $table = 'request_deliveries';
	protected $primaryKey = 'rede_id';
	public $timestamps = false;

	public function customerportal()
	{
		return $this->belongsTo('App\Models\KTCustomerPortal', 'rede_ktcupo_id', 'ktcupo_id');
	}

	public function getMatchIdForRequest(DeliveryLead $lead){
        $match = $this->customerportal->requestcustomerportal->where("ktrecupo_re_id", "=", $lead->getReId())->first();
        return $match->ktrecupo_cu_re_id;
    }
}
