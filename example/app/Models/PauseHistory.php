<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Claim
 *
 * @property-read \App\Models\KTRequestCustomerPortal $customerrequestportal
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Claim newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Claim newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Claim query()
 * @mixin \Eloquent
 * @property int $ph_id
 * @property string $ph_created_timestamp
 * @property int|null $ph_us_id
 * @property int|null $ph_apus_id
 * @property int $ph_cu_id
 * @property string $ph_start_date
 * @property string $ph_end_date
 * @property int $ph_total_days
 * @property string|null $ph_remark
 * @property int $ph_start_cust_id
 * @property int $ph_end_cust_id
 * @property-read \App\Models\ApplicationUser $application_user
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PauseHistory wherePhApusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PauseHistory wherePhCreatedTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PauseHistory wherePhCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PauseHistory wherePhEndCustId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PauseHistory wherePhEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PauseHistory wherePhId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PauseHistory wherePhRemark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PauseHistory wherePhStartCustId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PauseHistory wherePhStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PauseHistory wherePhTotalDays($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PauseHistory wherePhUsId($value)
 */
class PauseHistory extends Model
{
    protected $table = 'pause_history';
	protected $primaryKey = 'ph_id';
	public $timestamps = false;

    public function application_user()
    {
        return $this->hasOne('App\Models\ApplicationUser', 'apus_id', 'ph_apus_id');
    }

    public function user()
    {
        return $this->hasOne('App\Models\User', 'us_id', 'ph_us_id');
    }
}
