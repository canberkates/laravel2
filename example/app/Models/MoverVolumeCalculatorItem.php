<?php

namespace App\Models;

use Fico7489\Laravel\RevisionableUpgrade\Traits\RevisionableUpgradeTrait;
use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\RevisionableTrait;


/**
 * App\Models\MoverVolumeCalculatorItem
 *
 * @property int $movocait_id
 * @property int $movocait_movocaro_id
 * @property string $movocait_name
 * @property float $movocait_volume_m3
 * @property float $movocait_weight_kg
 * @property int $movocait_custom
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @property-read int|null $revision_history_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverVolumeCalculatorItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverVolumeCalculatorItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverVolumeCalculatorItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverVolumeCalculatorItem whereMovocaitCustom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverVolumeCalculatorItem whereMovocaitId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverVolumeCalculatorItem whereMovocaitMovocaroId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverVolumeCalculatorItem whereMovocaitName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverVolumeCalculatorItem whereMovocaitVolumeM3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverVolumeCalculatorItem whereMovocaitWeightKg($value)
 * @mixin \Eloquent
 * @property float $movocait_packing_costs
 * @property float $movocait_unpacking_costs
 * @property float $movocait_assembly_costs
 * @property float $movocait_disassembly_costs
 * @property string $movocait_la_code
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverVolumeCalculatorItem whereMovocaitAssemblyCosts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverVolumeCalculatorItem whereMovocaitDisassemblyCosts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverVolumeCalculatorItem whereMovocaitLaCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverVolumeCalculatorItem whereMovocaitPackingCosts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverVolumeCalculatorItem whereMovocaitUnpackingCosts($value)
 */
class MoverVolumeCalculatorItem extends Model
{
	protected $table = 'mover_volume_calculator_item';
	//protected $primaryKey = 'ktmovocait_cu_id';
	public $timestamps = false;

	use RevisionableTrait;
	use RevisionableUpgradeTrait;


}
