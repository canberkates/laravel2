<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\KTCustomerPortalRegion
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortalRegion newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortalRegion newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortalRegion query()
 * @mixin \Eloquent
 * @property int $ktcupore_id
 * @property int|null $ktcupore_ktcupo_id
 * @property int $ktcupore_reg_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortalRegion whereKtcuporeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortalRegion whereKtcuporeKtcupoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortalRegion whereKtcuporeRegId($value)
 */
class KTCustomerPortalRegion extends Model
{
    protected $table = 'kt_customer_portal_region';
	protected $primaryKey = 'ktcupore_id';
	public $timestamps = false;
}
