<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Message
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message query()
 * @mixin \Eloquent
 * @property int $mes_id
 * @property int|null $mes_us_id
 * @property string $mes_timestamp
 * @property int $mes_read
 * @property string $mes_subject
 * @property string $mes_message
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereMesId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereMesMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereMesRead($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereMesSubject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereMesTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereMesUsId($value)
 */
class Message extends Model
{
    protected $table = 'messages';
	protected $primaryKey = 'mes_id';
	public $timestamps = false;
}
