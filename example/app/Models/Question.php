<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Question
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\KTCustomerQuestion[] $customerquestions
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question query()
 * @mixin \Eloquent
 * @property int $qu_id
 * @property string $qu_name
 * @property string $qu_title
 * @property string $qu_question
 * @property string $qu_class
 * @property int $qu_limit
 * @property int $qu_extra_1_type
 * @property string $qu_extra_1_question
 * @property string $qu_extra_1_options
 * @property string $qu_extra_1_class_front
 * @property string $qu_extra_1_class_back
 * @property int $qu_extra_2_type
 * @property string $qu_extra_2_question
 * @property string $qu_extra_2_options
 * @property string $qu_extra_2_class_front
 * @property string $qu_extra_2_class_back
 * @property-read int|null $customerquestions_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question whereQuClass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question whereQuExtra1ClassBack($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question whereQuExtra1ClassFront($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question whereQuExtra1Options($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question whereQuExtra1Question($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question whereQuExtra1Type($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question whereQuExtra2ClassBack($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question whereQuExtra2ClassFront($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question whereQuExtra2Options($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question whereQuExtra2Question($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question whereQuExtra2Type($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question whereQuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question whereQuLimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question whereQuName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question whereQuQuestion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question whereQuTitle($value)
 */
class Question extends Model
{
    protected $table = 'questions';
	protected $primaryKey = 'qu_id';
	public $timestamps = false;
	
	public function customerquestions(){
		return $this->hasMany('App\Models\KTCustomerQuestion', 'ktcuqu_cu_id', 'cu_id');
	}
}
