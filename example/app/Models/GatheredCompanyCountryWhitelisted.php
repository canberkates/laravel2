<?php

namespace App\Models;

use Fico7489\Laravel\RevisionableUpgrade\Traits\RevisionableUpgradeTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\GatheredCompanyCountryWhitelisted
 *
 * @property-read \App\Models\Customer $customer
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatheredCompanyCountryWhitelisted newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatheredCompanyCountryWhitelisted newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatheredCompanyCountryWhitelisted query()
 * @mixin \Eloquent
 */
class GatheredCompanyCountryWhitelisted extends Model
{
    protected $table = 'gathered_companies_countries_whitelist';
    protected $primaryKey = 'gacocowh_id';
    public $timestamps = false;


}
