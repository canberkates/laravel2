<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Invoice
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice query()
 * @mixin \Eloquent
 * @property int $in_id
 * @property string $in_timestamp
 * @property int|null $in_cu_id
 * @property string|null $in_la_code
 * @property string $in_number
 * @property string $in_date
 * @property string $in_expiration_date
 * @property int $in_payment_reminder
 * @property string $in_payment_reminder_date
 * @property string $in_period_from
 * @property string $in_period_to
 * @property string $in_additional_information
 * @property int $in_payment_method
 * @property string $in_payment_text
 * @property string $in_company_data
 * @property string $in_debtor_data
 * @property int $in_lines
 * @property string|null $in_currency
 * @property float $in_amount
 * @property float $in_amount_discount
 * @property float $in_amount_no_claim
 * @property float $in_amount_gross
 * @property float $in_amount_vat
 * @property float $in_amount_netto
 * @property float $in_amount_eur
 * @property float $in_amount_discount_eur
 * @property float $in_amount_no_claim_eur
 * @property float $in_amount_gross_eur
 * @property float $in_amount_vat_eur
 * @property float $in_amount_netto_eur
 * @property float $in_amount_netto_eur_paid
 * @property float $in_vat_percentage
 * @property string $in_requests
 * @property int $in_old
 * @property-read \App\Models\Customer|null $customer
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\InvoiceLine[] $invoice_lines
 * @property-read int|null $invoice_lines_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice whereInAdditionalInformation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice whereInAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice whereInAmountDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice whereInAmountDiscountEur($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice whereInAmountEur($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice whereInAmountGross($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice whereInAmountGrossEur($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice whereInAmountNetto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice whereInAmountNettoEur($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice whereInAmountNettoEurPaid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice whereInAmountNoClaim($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice whereInAmountNoClaimEur($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice whereInAmountVat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice whereInAmountVatEur($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice whereInCompanyData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice whereInCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice whereInCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice whereInDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice whereInDebtorData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice whereInExpirationDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice whereInId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice whereInLaCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice whereInLines($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice whereInNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice whereInOld($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice whereInPaymentMethod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice whereInPaymentReminder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice whereInPaymentReminderDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice whereInPaymentText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice whereInPeriodFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice whereInPeriodTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice whereInRequests($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice whereInTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice whereInVatPercentage($value)
 */
class Invoice extends Model
{
    protected $table = 'invoices';
	protected $primaryKey = 'in_id';
	public $timestamps = false;
	
	public function customer()
	{
		return $this->belongsTo('App\Models\Customer', 'in_cu_id', 'cu_id');
	}
	
	public function invoice_lines(){
		return $this->hasMany('App\Models\InvoiceLine', 'inli_in_id', 'in_id');
	}
}
