<?php

namespace App\Models;

use Fico7489\Laravel\RevisionableUpgrade\Traits\RevisionableUpgradeTrait;
use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\KTBankLineInvoiceCustomerLedgerAccount
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTBankLineInvoiceCustomerLedgerAccount newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTBankLineInvoiceCustomerLedgerAccount newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTBankLineInvoiceCustomerLedgerAccount query()
 * @mixin \Eloquent
 * @property int $ktbaliinculeac_id
 * @property int|null $ktbaliinculeac_bali_id
 * @property string $ktbaliinculeac_date
 * @property int|null $ktbaliinculeac_in_id
 * @property int|null $ktbaliinculeac_cu_id
 * @property int|null $ktbaliinculeac_leac_number
 * @property float $ktbaliinculeac_amount
 * @property string|null $ktbaliinculeac_currency
 * @property float|null $ktbaliinculeac_amount_FC
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @property-read int|null $revision_history_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTBankLineInvoiceCustomerLedgerAccount whereKtbaliinculeacAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTBankLineInvoiceCustomerLedgerAccount whereKtbaliinculeacAmountFC($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTBankLineInvoiceCustomerLedgerAccount whereKtbaliinculeacBaliId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTBankLineInvoiceCustomerLedgerAccount whereKtbaliinculeacCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTBankLineInvoiceCustomerLedgerAccount whereKtbaliinculeacCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTBankLineInvoiceCustomerLedgerAccount whereKtbaliinculeacDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTBankLineInvoiceCustomerLedgerAccount whereKtbaliinculeacId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTBankLineInvoiceCustomerLedgerAccount whereKtbaliinculeacInId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTBankLineInvoiceCustomerLedgerAccount whereKtbaliinculeacLeacNumber($value)
 */
class KTBankLineInvoiceCustomerLedgerAccount extends Model
{
    protected $table = 'kt_bank_line_invoice_customer_ledger_account';
	protected $primaryKey = 'ktbaliinculeac_id';
	public $timestamps = false;
    
    use RevisionableTrait;
    use RevisionableUpgradeTrait;
    
    //enable this if you want use methods that gets information about creating
    protected $revisionCreationsEnabled = true;
}
