<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\LedgerAccount
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LedgerAccount newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LedgerAccount newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LedgerAccount query()
 * @mixin \Eloquent
 * @property int $leac_number
 * @property int $leac_type
 * @property string $leac_name
 * @property int $leac_turnover_type
 * @property float $leac_vat
 * @property int $leac_journal_entry
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LedgerAccount whereLeacJournalEntry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LedgerAccount whereLeacName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LedgerAccount whereLeacNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LedgerAccount whereLeacTurnoverType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LedgerAccount whereLeacType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LedgerAccount whereLeacVat($value)
 */
class LedgerAccount extends Model
{
	protected $table = 'ledger_accounts';
	//protected $primaryKey = 'pacu_code';
	public $timestamps = false;
}
