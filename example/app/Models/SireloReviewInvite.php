<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SireloReviewInvite
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloReviewInvite newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloReviewInvite newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloReviewInvite query()
 * @mixin \Eloquent
 * @property int $sirein_id
 * @property int|null $sirein_cu_id
 * @property string $sirein_timestamp
 * @property int $sirein_sent
 * @property string $sirein_timestamp_sent
 * @property string $sirein_email
 * @property string $sirein_name
 * @property string $sirein_subject
 * @property string $sirein_content
 * @property int $sirein_anonymized
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloReviewInvite whereSireinAnonymized($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloReviewInvite whereSireinContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloReviewInvite whereSireinCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloReviewInvite whereSireinEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloReviewInvite whereSireinId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloReviewInvite whereSireinName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloReviewInvite whereSireinSent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloReviewInvite whereSireinSubject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloReviewInvite whereSireinTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloReviewInvite whereSireinTimestampSent($value)
 */
class SireloReviewInvite extends Model
{
    protected $table = 'sirelo_review_invites';
	protected $primaryKey = 'sirein_id';
	public $timestamps = false;
}
