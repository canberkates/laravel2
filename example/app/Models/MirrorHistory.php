<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\MirrorHistory
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MirrorHistory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MirrorHistory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MirrorHistory query()
 * @mixin \Eloquent
 * @property int $mihi_id
 * @property string $mihi_start_timestamp
 * @property string $mihi_finish_timestamp
 * @property int $mihi_success
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MirrorHistory whereMihiFinishTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MirrorHistory whereMihiId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MirrorHistory whereMihiStartTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MirrorHistory whereMihiSuccess($value)
 */
class MirrorHistory extends Model
{
    protected $table = 'mirror_history';
	protected $primaryKey = 'mihi_id';
	public $timestamps = false;
}
