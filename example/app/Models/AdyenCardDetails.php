<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\AdyenCardDetails
 *
 * @property-read \App\Models\Customer $customer
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdyenCardDetails newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdyenCardDetails newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdyenCardDetails query()
 * @mixin \Eloquent
 * @property int $adcade_id
 * @property int|null $adcade_cu_id
 * @property string $adcade_merchant_reference
 * @property int $adcade_card_expiry_month
 * @property int $adcade_card_expiry_year
 * @property string $adcade_card_last_digits
 * @property string $adcade_card_alias
 * @property int $adcade_enabled
 * @property int $adcade_removed
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdyenCardDetails whereAdcadeCardAlias($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdyenCardDetails whereAdcadeCardExpiryMonth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdyenCardDetails whereAdcadeCardExpiryYear($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdyenCardDetails whereAdcadeCardLastDigits($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdyenCardDetails whereAdcadeCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdyenCardDetails whereAdcadeEnabled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdyenCardDetails whereAdcadeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdyenCardDetails whereAdcadeMerchantReference($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdyenCardDetails whereAdcadeRemoved($value)
 */
class AdyenCardDetails extends Model
{
    protected $table = 'adyen_card_details';
	protected $primaryKey = 'adcade_id';
	public $timestamps = false;
	
	public function customer()
	{
		return $this->belongsTo('App\Models\Customer', 'adcade_cu_id', 'cu_id');
	}
}
