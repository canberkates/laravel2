<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CustomerLoadSetting
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadSetting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadSetting newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadSetting query()
 * @mixin \Eloquent
 * @property int $culose_id
 * @property int $culose_cu_id
 * @property string $culose_co_code
 * @property int $culose_type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadSetting whereCuloseCoCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadSetting whereCuloseCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadSetting whereCuloseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerLoadSetting whereCuloseType($value)
 */
class CustomerLoadSetting extends Model
{
    protected $table = 'customer_load_settings';
	protected $primaryKey = 'culose_id';
	public $timestamps = false;
}
