<?php

namespace App\Models;

use Fico7489\Laravel\RevisionableUpgrade\Traits\RevisionableUpgradeTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\MobilityexFetchedData
 *
 * @property-read \App\Models\Customer $customer
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerGatheredReview newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerGatheredReview newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerGatheredReview query()
 * @mixin \Eloquent
 * @property int $mofeda_id
 * @property int $mofeda_type
 * @property int|null $mofeda_mobilityex_id
 * @property int|null $mofeda_cu_id
 * @property string $mofeda_timestamp
 * @property string|null $mofeda_company_name_legal
 * @property string|null $mofeda_company_name_business
 * @property string|null $mofeda_email
 * @property string|null $mofeda_website
 * @property string|null $mofeda_address
 * @property string|null $mofeda_street
 * @property string|null $mofeda_zipcode
 * @property string|null $mofeda_city
 * @property string|null $mofeda_telephone
 * @property string|null $mofeda_established
 * @property int $mofeda_moving_type
 * @property int $mofeda_relocation_type
 * @property string|null $mofeda_member_associations
 * @property string|null $mofeda_contact_persons
 * @property string|null $mofeda_quality
 * @property string|null $mofeda_moving_capabilities
 * @property int $mofeda_processed
 * @property int $mofeda_skipped
 * @property int $mofeda_accepted
 * @property int|null $mofeda_processed_by
 * @property int $mofeda_mail
 * @property string|null $mofeda_mobilityex_url
 * @property int|null $mofeda_website_validate_type
 * @property string|null $mofeda_website_validate_var_1
 * @property string|null $mofeda_website_validate_var_2
 * @property string|null $mofeda_website_validate_var_3
 * @property string|null $mofeda_website_validate_var_4
 * @property string|null $mofeda_website_validate_var_5
 * @property string|null $mofeda_website_validate_var_6
 * @property string|null $mofeda_final_url_erp
 * @property string|null $mofeda_final_url_mobilityex
 * @property int|null $mofeda_lock_user_id
 * @property string|null $mofeda_lock_timestamp
 * @property int $mofeda_processed_counter
 * @property string|null $mofeda_remarks
 * @property string|null $mofeda_history
 * @property string|null $mofeda_processed_timestamp
 * @property int $mofeda_customer_deleted
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaAccepted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaCompanyNameBusiness($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaCompanyNameLegal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaContactPersons($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaCustomerDeleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaEstablished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaFinalUrlErp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaFinalUrlMobilityex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaHistory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaLockTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaLockUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaMail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaMemberAssociations($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaMobilityexId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaMobilityexUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaMovingCapabilities($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaMovingType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaProcessed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaProcessedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaProcessedCounter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaProcessedTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaQuality($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaRelocationType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaSkipped($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaTelephone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaWebsite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaWebsiteValidateType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaWebsiteValidateVar1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaWebsiteValidateVar2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaWebsiteValidateVar3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaWebsiteValidateVar4($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaWebsiteValidateVar5($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaWebsiteValidateVar6($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaZipcode($value)
 * @property string|null $mofeda_customers_json
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaCustomersJson($value)
 * @property int|null $mofeda_original_type
 * @property string|null $mofeda_co_code
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaCoCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaOriginalType($value)
 * @property string|null $mofeda_what_happend
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MobilityexFetchedData whereMofedaWhatHappend($value)
 */
class MobilityexFetchedData extends Model
{
    protected $table = 'mobilityex_fetched_data';
    protected $primaryKey = 'mofeda_id';
    public $timestamps = false;

}
