<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\AdyenPayment
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdyenPayment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdyenPayment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdyenPayment query()
 * @mixin \Eloquent
 * @property int $adpa_id
 * @property int|null $adpa_adcade_id
 * @property string $adpa_timestamp
 * @property string $adpa_psp_reference
 * @property string $adpa_merchant_reference
 * @property string $adpa_result_code
 * @property string $adpa_refusal_reason
 * @property float $adpa_amount
 * @property string|null $adpa_pacu_code
 * @property int $adpa_status
 * @property string $adpa_changes
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdyenPayment whereAdpaAdcadeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdyenPayment whereAdpaAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdyenPayment whereAdpaChanges($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdyenPayment whereAdpaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdyenPayment whereAdpaMerchantReference($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdyenPayment whereAdpaPacuCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdyenPayment whereAdpaPspReference($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdyenPayment whereAdpaRefusalReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdyenPayment whereAdpaResultCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdyenPayment whereAdpaStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdyenPayment whereAdpaTimestamp($value)
 */
class AdyenPayment extends Model
{
    protected $table = 'adyen_payments';
	protected $primaryKey = 'adpa_id';
	public $timestamps = false;
}
