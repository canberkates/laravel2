<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\MoverDirectPricingRange
 *
 * @property int $modiprra_id
 * @property int $modiprra_modiprse_id
 * @property int $modiprra_from_km
 * @property int $modiprra_to_km
 * @property int $modiprra_number_of_people
 * @property float $modiprra_hours_per_m3
 * @property float $modiprra_truck_cost_per_hour
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverDirectPricingRange newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverDirectPricingRange newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverDirectPricingRange query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverDirectPricingRange whereModiprraFromKm($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverDirectPricingRange whereModiprraHoursPerM3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverDirectPricingRange whereModiprraId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverDirectPricingRange whereModiprraModiprseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverDirectPricingRange whereModiprraNumberOfPeople($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverDirectPricingRange whereModiprraToKm($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverDirectPricingRange whereModiprraTruckCostPerHour($value)
 * @mixin \Eloquent
 * @property int $modiprra_from_volume
 * @property int $modiprra_to_volume
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverDirectPricingRange whereModiprraFromVolume($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverDirectPricingRange whereModiprraToVolume($value)
 */
class MoverDirectPricingRange extends Model
{
	protected $table = 'mover_direct_pricing_range';
	protected $primaryKey = 'modiprra_id';

    public $timestamps = false;
}
