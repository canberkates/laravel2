<?php

namespace App\Models;

use Fico7489\Laravel\RevisionableUpgrade\Traits\RevisionableUpgradeTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\FetchedGoogleHistory
 *
 * @property-read \App\Models\Customer $customer
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FetchedGoogleHistory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FetchedGoogleHistory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FetchedGoogleHistory query()
 * @mixin \Eloquent
 * @property int $fegohi_id
 * @property int $fegohi_curesc_id
 * @property int $fegohi_cu_id
 * @property string $fegohi_timestamp
 * @property int $fegohi_fetched
 * @property int $fegohi_created
 * @property int $fegohi_skipped
 * @property int $fegohi_changed
 * @property int $fegohi_unchanged
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FetchedGoogleHistory whereFegohiChanged($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FetchedGoogleHistory whereFegohiCreated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FetchedGoogleHistory whereFegohiCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FetchedGoogleHistory whereFegohiCurescId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FetchedGoogleHistory whereFegohiFetched($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FetchedGoogleHistory whereFegohiId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FetchedGoogleHistory whereFegohiSkipped($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FetchedGoogleHistory whereFegohiTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FetchedGoogleHistory whereFegohiUnchanged($value)
 */
class FetchedGoogleHistory extends Model
{
    protected $table = 'fetched_google_history';
    protected $primaryKey = 'fegohi_id';
    public $timestamps = false;

    //customer relation
    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'fegohi_cu_id', 'cu_id');
    }

}
