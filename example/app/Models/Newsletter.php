<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Newsletter
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Newsletter newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Newsletter newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Newsletter query()
 * @mixin \Eloquent
 * @property int $new_id
 * @property string $new_name
 * @property int $new_delay
 * @property string $new_subject_singular
 * @property string $new_subject_plural
 * @property string $new_intro
 * @property int $new_slot_1
 * @property int $new_slot_2
 * @property int $new_slot_3
 * @property int $new_slot_4
 * @property int $new_slot_5
 * @property int $new_slot_6
 * @property int $new_slot_7
 * @property int $new_slot_8
 * @property int $new_slot_9
 * @property int $new_slot_10
 * @property int $new_deleted
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Newsletter whereNewDelay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Newsletter whereNewDeleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Newsletter whereNewId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Newsletter whereNewIntro($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Newsletter whereNewName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Newsletter whereNewSlot1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Newsletter whereNewSlot10($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Newsletter whereNewSlot2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Newsletter whereNewSlot3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Newsletter whereNewSlot4($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Newsletter whereNewSlot5($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Newsletter whereNewSlot6($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Newsletter whereNewSlot7($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Newsletter whereNewSlot8($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Newsletter whereNewSlot9($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Newsletter whereNewSubjectPlural($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Newsletter whereNewSubjectSingular($value)
 */
class Newsletter extends Model
{
    protected $table = 'newsletters';
	protected $primaryKey = 'new_id';
	public $timestamps = false;
}
