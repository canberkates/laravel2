<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CustomerGDPR
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerGDPR newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerGDPR newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerGDPR query()
 * @mixin \Eloquent
 * @property int $cugdpr_id
 * @property int $cugdpr_cu_id
 * @property int $cugdpr_apus_id
 * @property string $cugdpr_timestamp
 * @property string $cugdpr_name
 * @property string $cugdpr_email
 * @property int $cugdpr_version
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerGDPR whereCugdprApusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerGDPR whereCugdprCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerGDPR whereCugdprEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerGDPR whereCugdprId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerGDPR whereCugdprName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerGDPR whereCugdprTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerGDPR whereCugdprVersion($value)
 */
class CustomerGDPR extends Model
{
    protected $table = 'customer_gdpr';
	protected $primaryKey = 'cugdpr_id';
	public $timestamps = false;
}
