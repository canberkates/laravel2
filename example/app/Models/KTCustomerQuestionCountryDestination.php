<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\KTCustomerQuestionCountryDestination
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerQuestionCountryDestination newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerQuestionCountryDestination newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerQuestionCountryDestination query()
 * @mixin \Eloquent
 * @property int $ktcuqucode_id
 * @property int $ktcuqucode_ktcuqu_id
 * @property string $ktcuqucode_co_code
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerQuestionCountryDestination whereKtcuqucodeCoCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerQuestionCountryDestination whereKtcuqucodeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerQuestionCountryDestination whereKtcuqucodeKtcuquId($value)
 */
class KTCustomerQuestionCountryDestination extends Model
{
    protected $table = 'kt_customer_question_country_destination';
	protected $primaryKey = 'ktcuqucode_id';
	public $timestamps = false;
}
