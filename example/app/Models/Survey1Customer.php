<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Survey1Customer
 *
 * @property-read \App\Models\Customer $customer
 * @property-read \App\Models\Survey1 $survey
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey1Customer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey1Customer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey1Customer query()
 * @mixin \Eloquent
 * @property int $sucu_id
 * @property int $sucu_su_id
 * @property int|null $sucu_cu_id
 * @property int $sucu_contacted
 * @property int $sucu_quote_received
 * @property string $sucu_remark
 * @property int $sucu_mail
 * @property int $sucu_anonymized
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey1Customer whereSucuAnonymized($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey1Customer whereSucuContacted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey1Customer whereSucuCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey1Customer whereSucuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey1Customer whereSucuMail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey1Customer whereSucuQuoteReceived($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey1Customer whereSucuRemark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Survey1Customer whereSucuSuId($value)
 */
class Survey1Customer extends Model
{
    protected $table = 'survey_1_customers';
	protected $primaryKey = 'sucu_id';
	public $timestamps = false;
	
	public function customer()
	{
		return $this->belongsTo('App\Models\Customer', 'sucu_cu_id', 'cu_id');
	}
	
	public function survey()
	{
		return $this->belongsTo('App\Models\Survey1', 'sucu_su_id', 'su_id');
	}
}
