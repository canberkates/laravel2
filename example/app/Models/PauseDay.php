<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Claim
 *
 * @property-read \App\Models\KTRequestCustomerPortal $customerrequestportal
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Claim newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Claim newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Claim query()
 * @mixin \Eloquent
 * @property int $pada_id
 * @property string $pada_timestamp
 * @property int $pada_cu_id
 * @property int $pada_amount
 * @property int $pada_amount_left
 * @property string $pada_start_date
 * @property string $pada_expired
 * @property int $pada_deleted
 * @property-read \App\Models\Customer $customer
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PauseDay wherePadaAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PauseDay wherePadaAmountLeft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PauseDay wherePadaCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PauseDay wherePadaDeleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PauseDay wherePadaExpired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PauseDay wherePadaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PauseDay wherePadaStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PauseDay wherePadaTimestamp($value)
 */
class PauseDay extends Model
{
    protected $table = 'pause_days';
    protected $primaryKey = 'pada_id';
    public $timestamps = false;

    public function customer()
    {
        return $this->hasOne('App\Models\Customer', 'cu_id', 'pada_cu_id');
    }
}
