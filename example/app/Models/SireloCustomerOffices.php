<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SireloCustomerOffices
 *
 * @property string $customer_key
 * @property string $city_key
 * @property string $country_code
 * @property string $street
 * @property string $zipcode
 * @property string $city
 * @property string $email
 * @property string $telephone
 * @property string $website
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomerOffices newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomerOffices newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomerOffices query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomerOffices whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomerOffices whereCityKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomerOffices whereCountryCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomerOffices whereCustomerKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomerOffices whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomerOffices whereStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomerOffices whereTelephone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomerOffices whereWebsite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomerOffices whereZipcode($value)
 * @mixin \Eloquent
 */
class SireloCustomerOffices extends Model
{
    protected $table = 'sirelo_customer_offices';
    public $timestamps = false;

}
