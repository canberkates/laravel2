<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Cache
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cache newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cache newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cache query()
 * @mixin \Eloquent
 * @property string $ca_name
 * @property string $ca_timestamp
 * @property string $ca_value
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cache whereCaName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cache whereCaTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cache whereCaValue($value)
 */
class Cache extends Model
{
    protected $table = 'cache';
	//protected $primaryKey = 'ad_id';
	public $timestamps = false;
}
