<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\KTCustomerQuestion
 *
 * @property-read \App\Models\Question $questions
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerQuestion newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerQuestion newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerQuestion query()
 * @mixin \Eloquent
 * @property int $ktcupr_id
 * @property int $ktcupr_cu_id
 * @property int $ktcupr_type
 * @property int|null $ktcupr_closed_reason
 * @property string|null $ktcupr_timestamp
 * @property int|null $ktcupr_us_id
 * @property int|null $ktcupr_cure_id
 * @property int $ktcupr_deleted
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerProgress whereKtcuprClosedReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerProgress whereKtcuprCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerProgress whereKtcuprCureId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerProgress whereKtcuprDeleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerProgress whereKtcuprId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerProgress whereKtcuprTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerProgress whereKtcuprType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerProgress whereKtcuprUsId($value)
 */
class KTCustomerProgress extends Model
{
    protected $table = 'kt_customer_progress';
	protected $primaryKey = 'ktcupr_id';
	public $timestamps = false;

}
