<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Membership
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Membership newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Membership newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Membership query()
 * @mixin \Eloquent
 * @property int $me_id
 * @property string $me_name
 * @property string $me_location
 * @property string $me_website
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Membership whereMeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Membership whereMeLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Membership whereMeName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Membership whereMeWebsite($value)
 */
class Membership extends Model
{
    protected $table = 'memberships';
	protected $primaryKey = 'me_id';
	public $timestamps = false;
	
	
}
