<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\KTCustomerMembership
 *
 * @property-read \App\Models\Membership $membership
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerMembership newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerMembership newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerMembership query()
 * @mixin \Eloquent
 * @property int $ktcume_id
 * @property int $ktcume_cu_id
 * @property int $ktcume_me_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerMembership whereKtcumeCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerMembership whereKtcumeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerMembership whereKtcumeMeId($value)
 * @property string|null $ktcume_link
 * @property string|null $ktcume_exp_date
 * @property string|null $ktcume_updated_timestamp
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerMembership whereKtcumeExpDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerMembership whereKtcumeLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerMembership whereKtcumeUpdatedTimestamp($value)
 */
class KTCustomerMembership extends Model
{
    protected $table = 'kt_customer_membership';
	protected $primaryKey = 'ktcume_id';
	public $timestamps = false;
	
	public function membership()
	{
		return $this->belongsTo('App\Models\Membership', 'ktcume_me_id', 'me_id');
	}
	
}
