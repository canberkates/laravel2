<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\KTNotificationUser
 *
 * @property int $ktnous_id
 * @property int $ktnous_us_id
 * @property int $ktnous_noli_id
 * @property-read \App\Models\NotificationList $notificationlist
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTNotificationUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTNotificationUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTNotificationUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTNotificationUser whereKtnousId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTNotificationUser whereKtnousNoliId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTNotificationUser whereKtnousUsId($value)
 * @mixin \Eloquent
 */
class KTNotificationUser extends Model
{
    protected $table = 'kt_notification_user';
    protected $primaryKey = 'ktnous_id';
    public $timestamps = false;
    
    public function notificationlist()
    {
        return $this->belongsTo('App\Models\NotificationList', 'ktnous_noli_id', 'noli_id');
    }
    
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'ktnous_us_id', 'us_id');
    }



}