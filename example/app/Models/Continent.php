<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Continent
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Continent newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Continent newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Continent query()
 * @mixin \Eloquent
 * @property int $con_id
 * @property string $con_name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Continent whereConId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Continent whereConName($value)
 */
class Continent extends Model
{
	protected $table = 'continents';
	protected $primaryKey = 'con_id';
	public $timestamps = false;
}
