<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\MovingAssistant
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovingAssistant newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovingAssistant newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovingAssistant query()
 * @mixin \Eloquent
 * @property int $moas_id
 * @property string $moas_token
 * @property string $moas_timestamp
 * @property string $moas_email
 * @property string $moas_first_name
 * @property string $moas_moving_date
 * @property int $moas_we_id
 * @property int $moas_unsubscribed
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovingAssistant whereMoasEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovingAssistant whereMoasFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovingAssistant whereMoasId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovingAssistant whereMoasMovingDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovingAssistant whereMoasTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovingAssistant whereMoasToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovingAssistant whereMoasUnsubscribed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MovingAssistant whereMoasWeId($value)
 */
class MovingAssistant extends Model
{
    protected $table = 'moving_assistant';
    protected $primaryKey = 'moas_id';
    public $timestamps = false;

}
