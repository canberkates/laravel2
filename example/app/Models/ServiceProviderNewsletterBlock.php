<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ServiceProviderNewsletterBlock
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ServiceProviderNewsletterBlock newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ServiceProviderNewsletterBlock newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ServiceProviderNewsletterBlock query()
 * @mixin \Eloquent
 * @property int $seprnebl_id
 * @property int|null $seprnebl_cu_id
 * @property int $seprnebl_slot
 * @property string $seprnebl_description
 * @property int $seprnebl_status
 * @property int $seprnebl_international_moves
 * @property int $seprnebl_national_moves
 * @property string $seprnebl_title
 * @property string $seprnebl_content
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ServiceProviderNewsletterBlock whereSeprneblContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ServiceProviderNewsletterBlock whereSeprneblCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ServiceProviderNewsletterBlock whereSeprneblDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ServiceProviderNewsletterBlock whereSeprneblId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ServiceProviderNewsletterBlock whereSeprneblInternationalMoves($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ServiceProviderNewsletterBlock whereSeprneblNationalMoves($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ServiceProviderNewsletterBlock whereSeprneblSlot($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ServiceProviderNewsletterBlock whereSeprneblStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ServiceProviderNewsletterBlock whereSeprneblTitle($value)
 */
class ServiceProviderNewsletterBlock extends Model
{
    protected $table = 'service_provider_newsletter_blocks';
	protected $primaryKey = 'seprnebl_id';
	public $timestamps = false;
}
