<?php

namespace App\Models;

use Fico7489\Laravel\RevisionableUpgrade\Traits\RevisionableUpgradeTrait;
use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\KTCustomerPortal
 *
 * @property-read \App\Models\Customer $customer
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\KTCustomerPortalCountry[] $customerportalcountries
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\KTCustomerPortalRegion[] $customerportalregions
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\KTCustomerPortalRegionDestination[] $customerportalregionsdestination
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\FreeTrial[] $freetrials
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PaymentRate[] $paymentrates
 * @property-read \App\Models\Portal $portal
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\RequestDelivery[] $requestdeliveries
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\StatusUpdate[] $statusupdates
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortal newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortal newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortal query()
 * @mixin \Eloquent
 * @property int $ktcupo_id
 * @property int|null $ktcupo_cu_id
 * @property int|null $ktcupo_po_id
 * @property int $ktcupo_type
 * @property int $ktcupo_request_type
 * @property string|null $ktcupo_description
 * @property int $ktcupo_status
 * @property string $ktcupo_member_since
 * @property string $ktcupo_terminated_on
 * @property int $ktcupo_free_trial
 * @property int $ktcupo_moving_size_1
 * @property int $ktcupo_moving_size_2
 * @property int $ktcupo_moving_size_3
 * @property int $ktcupo_moving_size_4
 * @property int $ktcupo_moving_size_5
 * @property int $ktcupo_moving_size_6
 * @property int $ktcupo_moving_size_7
 * @property int $ktcupo_moving_size_8
 * @property int $ktcupo_moving_size_9
 * @property int $ktcupo_moving_size_10
 * @property float $ktcupo_min_volume_m3
 * @property float $ktcupo_min_volume_ft3
 * @property int $ktcupo_export_moves
 * @property int $ktcupo_import_moves
 * @property int $ktcupo_business_only
 * @property int $ktcupo_max_requests_month
 * @property int $ktcupo_max_requests_month_left
 * @property int $ktcupo_max_requests_day
 * @property int $ktcupo_max_requests_day_left
 * @property float $ktcupo_daily_average
 * @property float $ktcupo_requests_bucket
 * @property int|null $ktcupo_monthly_capping_triglobal
 * @property int|null $ktcupo_monthly_capping_client
 * @property-read int|null $customerportalcountries_count
 * @property-read int|null $customerportalregions_count
 * @property-read int|null $customerportalregionsdestination_count
 * @property-read int|null $freetrials_count
 * @property-read int|null $paymentrates_count
 * @property-read int|null $requestdeliveries_count
 * @property-read int|null $revision_history_count
 * @property-read int|null $statusupdates_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortal whereKtcupoBusinessOnly($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortal whereKtcupoCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortal whereKtcupoDailyAverage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortal whereKtcupoDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortal whereKtcupoExportMoves($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortal whereKtcupoFreeTrial($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortal whereKtcupoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortal whereKtcupoImportMoves($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortal whereKtcupoMaxRequestsDay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortal whereKtcupoMaxRequestsDayLeft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortal whereKtcupoMaxRequestsMonth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortal whereKtcupoMaxRequestsMonthLeft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortal whereKtcupoMemberSince($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortal whereKtcupoMinVolumeFt3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortal whereKtcupoMinVolumeM3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortal whereKtcupoMonthlyCappingClient($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortal whereKtcupoMonthlyCappingTriglobal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortal whereKtcupoMovingSize1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortal whereKtcupoMovingSize10($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortal whereKtcupoMovingSize2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortal whereKtcupoMovingSize3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortal whereKtcupoMovingSize4($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortal whereKtcupoMovingSize5($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortal whereKtcupoMovingSize6($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortal whereKtcupoMovingSize7($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortal whereKtcupoMovingSize8($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortal whereKtcupoMovingSize9($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortal whereKtcupoPoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortal whereKtcupoRequestType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortal whereKtcupoRequestsBucket($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortal whereKtcupoStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortal whereKtcupoTerminatedOn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortal whereKtcupoType($value)
 * @property int $ktcupo_room_sizes_enabled
 * @property int $ktcupo_room_size_1
 * @property int $ktcupo_room_size_2
 * @property int $ktcupo_room_size_3
 * @property int $ktcupo_room_size_4
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortal whereKtcupoRoomSize1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortal whereKtcupoRoomSize2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortal whereKtcupoRoomSize3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortal whereKtcupoRoomSize4($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortal whereKtcupoRoomSizesEnabled($value)
 * @property int $ktcupo_nat_local_moves
 * @property int $ktcupo_nat_long_distance_moves
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\KTRequestCustomerPortal[] $requestcustomerportal
 * @property-read int|null $requestcustomerportal_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortal whereKtcupoNatLocalMoves($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerPortal whereKtcupoNatLongDistanceMoves($value)
 */
class KTCustomerPortal extends Model
{
    protected $table = 'kt_customer_portal';
	protected $primaryKey = 'ktcupo_id';
	public $timestamps = false;

	use RevisionableTrait;
	use RevisionableUpgradeTrait;

	//enable this if you want use methods that gets information about creating
	protected $revisionCreationsEnabled = true;

	public function portal()
	{
		return $this->belongsTo('App\Models\Portal', 'ktcupo_po_id', 'po_id');
	}

	public function customer()
	{
		return $this->belongsTo('App\Models\Customer', 'ktcupo_cu_id', 'cu_id');
	}

	public function statusupdates()
	{
		return $this->hasMany('App\Models\StatusUpdate', 'stup_ktcupo_id', 'ktcupo_id');
	}

	public function requestdeliveries()
	{
		return $this->hasMany('App\Models\RequestDelivery', 'rede_ktcupo_id', 'ktcupo_id');
	}

	public function paymentrates()
	{
		return $this->hasMany('App\Models\PaymentRate', 'para_ktcupo_id', 'ktcupo_id');
	}

	public function freetrials()
	{
		return $this->hasMany('App\Models\FreeTrial', 'frtr_ktcupo_id', 'ktcupo_id');
	}

	public function customerportalregions(){
		return $this->hasMany('App\Models\KTCustomerPortalRegion', 'ktcupore_ktcupo_id', 'ktcupo_id');
	}

	public function customerportalregionsdestination(){
		return $this->hasMany('App\Models\KTCustomerPortalRegionDestination', 'ktcuporede_ktcupo_id', 'ktcupo_id');
	}

	public function customerportalcountries(){
		return $this->hasMany('App\Models\KTCustomerPortalCountry', 'ktcupoco_ktcupo_id', 'ktcupo_id');
	}

	public function requestcustomerportal(){
		return $this->hasMany('App\Models\KTRequestCustomerPortal', 'ktrecupo_ktcupo_id', 'ktcupo_id');
	}


}
