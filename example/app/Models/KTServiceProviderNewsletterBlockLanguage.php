<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\KTServiceProviderNewsletterBlockLanguage
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderNewsletterBlockLanguage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderNewsletterBlockLanguage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderNewsletterBlockLanguage query()
 * @mixin \Eloquent
 * @property int $ktseprneblla_id
 * @property int $ktseprneblla_seprnebl_id
 * @property string $ktseprneblla_la_code
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderNewsletterBlockLanguage whereKtseprnebllaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderNewsletterBlockLanguage whereKtseprnebllaLaCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderNewsletterBlockLanguage whereKtseprnebllaSeprneblId($value)
 */
class KTServiceProviderNewsletterBlockLanguage extends Model
{
    protected $table = 'kt_service_provider_newsletter_block_language';
	protected $primaryKey = 'ktseprneblla_id';
	public $timestamps = false;
}
