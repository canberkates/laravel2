<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\KTServiceProviderAdvertorialBlockLanguage
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderAdvertorialBlockLanguage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderAdvertorialBlockLanguage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderAdvertorialBlockLanguage query()
 * @mixin \Eloquent
 * @property int $ktsepradblla_id
 * @property int $ktsepradblla_sepradbl_id
 * @property string $ktsepradblla_la_code
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderAdvertorialBlockLanguage whereKtsepradbllaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderAdvertorialBlockLanguage whereKtsepradbllaLaCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderAdvertorialBlockLanguage whereKtsepradbllaSepradblId($value)
 */
class KTServiceProviderAdvertorialBlockLanguage extends Model
{
    protected $table = 'kt_service_provider_advertorial_block_language';
    protected $primaryKey = 'ktsepradblla_id';
    public $timestamps = false;
}
