<?php

namespace App\Models;

use Fico7489\Laravel\RevisionableUpgrade\Traits\RevisionableUpgradeTrait;
use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\ServiceProviderData
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ServiceProviderData newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ServiceProviderData newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ServiceProviderData query()
 * @mixin \Eloquent
 * @property int $seprda_id
 * @property int $seprda_cu_id
 * @property int $seprda_lead_form
 * @property int $seprda_email_marketing
 * @property int $seprda_sirelo_advertising
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @property-read int|null $revision_history_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ServiceProviderData whereSeprdaCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ServiceProviderData whereSeprdaEmailMarketing($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ServiceProviderData whereSeprdaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ServiceProviderData whereSeprdaLeadForm($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ServiceProviderData whereSeprdaSireloAdvertising($value)
 */
class ServiceProviderData extends Model
{
    protected $table = 'service_provider_data';
    protected $primaryKey = 'seprda_id';
    public $timestamps = false;

    use RevisionableTrait;
    use RevisionableUpgradeTrait;

    //enable this if you want use methods that gets information about creating
    protected $revisionCreationsEnabled = true;
}
