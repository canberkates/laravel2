<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\KTReportTemplate
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTReportTemplate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTReportTemplate newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTReportTemplate query()
 * @mixin \Eloquent
 * @property int $ktrete_id
 * @property int $ktrete_re_id
 * @property int $ktrete_te_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTReportTemplate whereKtreteId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTReportTemplate whereKtreteReId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTReportTemplate whereKtreteTeId($value)
 */
class KTReportTemplate extends Model
{
    protected $table = 'kt_reports_templates';
	protected $primaryKey = 'ktrete_id';
	public $timestamps = false;
}
