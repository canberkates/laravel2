<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ApplicationUser
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ApplicationUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ApplicationUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ApplicationUser query()
 * @mixin \Eloquent
 * @property int $apus_id
 * @property string $apus_heartbeat
 * @property string|null $apus_session_id
 * @property string|null $apus_session_token
 * @property int $apus_cu_id
 * @property string $apus_username
 * @property string $apus_password
 * @property string $apus_name
 * @property string $apus_email
 * @property int $apus_right
 * @property int|null $apus_application_id
 * @property int $apus_force_change_password
 * @property int $apus_blocked
 * @property int $apus_deleted
 * @property-read \App\Models\Customer $customer
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ApplicationUser whereApusApplicationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ApplicationUser whereApusBlocked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ApplicationUser whereApusCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ApplicationUser whereApusDeleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ApplicationUser whereApusEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ApplicationUser whereApusForceChangePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ApplicationUser whereApusHeartbeat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ApplicationUser whereApusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ApplicationUser whereApusName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ApplicationUser whereApusPassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ApplicationUser whereApusRight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ApplicationUser whereApusSessionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ApplicationUser whereApusSessionToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ApplicationUser whereApusUsername($value)
 */
class ApplicationUser extends Model
{
    protected $table = 'application_users';
	protected $primaryKey = 'apus_id';
	public $timestamps = false;
	
	public function customer()
	{
		return $this->hasOne('App\Models\Customer', 'cu_id', 'cope_cu_id');
	}
}
