<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\MoverSurvey
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverSurvey newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverSurvey newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverSurvey query()
 * @mixin \Eloquent
 * @property int $mosu_id
 * @property string $mosu_timestamp
 * @property int|null $mosu_cu_id
 * @property string $mosu_name
 * @property int $mosu_rate_leads
 * @property int $mosu_rate_triglobal
 * @property int $mosu_rate_satisfaction
 * @property string $mosu_increase_satisfaction
 * @property int $mosu_recommend
 * @property string $mosu_remarks
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverSurvey whereMosuCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverSurvey whereMosuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverSurvey whereMosuIncreaseSatisfaction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverSurvey whereMosuName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverSurvey whereMosuRateLeads($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverSurvey whereMosuRateSatisfaction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverSurvey whereMosuRateTriglobal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverSurvey whereMosuRecommend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverSurvey whereMosuRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MoverSurvey whereMosuTimestamp($value)
 */
class MoverSurvey extends Model
{
    protected $table = 'mover_surveys';
	protected $primaryKey = 'mosu_id';
	public $timestamps = false;
}
