<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\StatusHistory
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StatusHistory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StatusHistory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StatusHistory query()
 * @mixin \Eloquent
 * @property int $sthi_id
 * @property string $sthi_timestamp
 * @property int $sthi_type
 * @property int|null $sthi_us_id
 * @property int|null $sthi_cu_id
 * @property int|null $sthi_ktcupo_id
 * @property int $sthi_status_from
 * @property int $sthi_status_to
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StatusHistory whereSthiCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StatusHistory whereSthiId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StatusHistory whereSthiKtcupoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StatusHistory whereSthiStatusFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StatusHistory whereSthiStatusTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StatusHistory whereSthiTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StatusHistory whereSthiType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StatusHistory whereSthiUsId($value)
 */
class StatusHistory extends Model
{
    protected $table = 'status_history';
	protected $primaryKey = 'sthi_id';
	public $timestamps = false;
}
