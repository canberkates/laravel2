<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\WebsiteReview
 *
 * @property-read \App\Models\Country $countryfrom
 * @property-read \App\Models\Country $countryto
 * @property-read \App\Models\Portal $portal
 * @property-read \App\Models\WebsiteForm $websiteform
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteReview newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteReview newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteReview query()
 * @mixin \Eloquent
 * @property int $were_id
 * @property int $were_po_id
 * @property int $were_wefo_id
 * @property string $were_co_code_from
 * @property string $were_city_from
 * @property string $were_co_code_to
 * @property string $were_city_to
 * @property string $were_name
 * @property string $were_email
 * @property int|null $were_verified
 * @property int $were_anonymous
 * @property int $were_send_contact_details_to_mover
 * @property int $were_device
 * @property string|null $were_user_agent
 * @property string $were_ip_address
 * @property string|null $were_ip_address_country
 * @property int $were_anonymized
 * @property int $were_reviewsubmitted_cookie
 * @property string|null $were_cookie_timestamp
 * @property string|null $were_cookie_previous_timestamp
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteReview whereWereAnonymized($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteReview whereWereAnonymous($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteReview whereWereCityFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteReview whereWereCityTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteReview whereWereCoCodeFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteReview whereWereCoCodeTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteReview whereWereCookiePreviousTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteReview whereWereCookieTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteReview whereWereDevice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteReview whereWereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteReview whereWereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteReview whereWereIpAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteReview whereWereIpAddressCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteReview whereWereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteReview whereWerePoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteReview whereWereReviewsubmittedCookie($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteReview whereWereSendContactDetailsToMover($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteReview whereWereUserAgent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteReview whereWereVerified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteReview whereWereWefoId($value)
 * @property string|null $were_proof_files
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteReview whereWereProofFiles($value)
 */
class WebsiteReview extends Model
{
    protected $table = 'website_reviews';
	protected $primaryKey = 'were_id';
	public $timestamps = false;
	
	public function websiteform()
	{
		return $this->hasOne('App\Models\WebsiteForm', 'wefo_id', 'were_wefo_id');
	}
	
	public function portal()
	{
		return $this->hasOne('App\Models\Portal', 'po_id', 'were_po_id');
	}
	
	public function countryfrom()
	{
		return $this->hasOne('App\Models\Country', 'co_code', 'were_co_code_from');
	}
	
	public function countryto()
	{
		return $this->hasOne('App\Models\Country', 'co_code', 'were_co_code_to');
	}
}
