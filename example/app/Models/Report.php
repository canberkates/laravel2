<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Report
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Report newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Report newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Report query()
 * @mixin \Eloquent
 * @property int $rep_id
 * @property string $rep_report
 * @property string $rep_name
 * @property string|null $rep_description
 * @property int $rep_heavy
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Report whereRepDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Report whereRepHeavy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Report whereRepId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Report whereRepName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Report whereRepReport($value)
 */
class Report extends Model
{
    protected $table = 'reports';
	protected $primaryKey = 'rep_id';
	public $timestamps = false;
}
