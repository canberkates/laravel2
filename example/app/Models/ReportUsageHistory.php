<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ReportUsageHistory
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReportUsageHistory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReportUsageHistory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReportUsageHistory query()
 * @mixin \Eloquent
 * @property int $reushi_id
 * @property string $reushi_timestamp
 * @property int $reushi_us_id
 * @property int $reushi_rep_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReportUsageHistory whereReushiId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReportUsageHistory whereReushiRepId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReportUsageHistory whereReushiTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReportUsageHistory whereReushiUsId($value)
 */
class ReportUsageHistory extends Model
{
    protected $table = 'report_usage_history';
    protected $primaryKey = 'reushi_id';
    public $timestamps = false;
}
