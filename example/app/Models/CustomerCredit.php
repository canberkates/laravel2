<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CustomerCredit
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerCredit newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerCredit newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerCredit query()
 * @mixin \Eloquent
 * @property int $cucr_id
 * @property string $cucr_timestamp
 * @property int|null $cucr_cu_id
 * @property int $cucr_amount
 * @property int $cucr_amount_left
 * @property string $cucr_expired
 * @property int $cucr_type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerCredit whereCucrAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerCredit whereCucrAmountLeft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerCredit whereCucrCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerCredit whereCucrExpired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerCredit whereCucrId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerCredit whereCucrTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerCredit whereCucrType($value)
 */
class CustomerCredit extends Model
{
    protected $table = 'customer_credit';
	protected $primaryKey = 'cucr_id';
	public $timestamps = false;
}
