<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CustomerDocument
 *
 * @property-read \App\Models\Customer $customer
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerDocument newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerDocument newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerDocument query()
 * @mixin \Eloquent
 * @property int $cudo_id
 * @property string $cudo_timestamp
 * @property int|null $cudo_cu_id
 * @property int|null $cudo_us_id
 * @property string $cudo_filename
 * @property int $cudo_type
 * @property string|null $cudo_description
 * @property int $cudo_archived
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerDocument whereCudoArchived($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerDocument whereCudoCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerDocument whereCudoDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerDocument whereCudoFilename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerDocument whereCudoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerDocument whereCudoTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerDocument whereCudoType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerDocument whereCudoUsId($value)
 * @property string|null $cudo_number
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerDocument whereCudoNumber($value)
 */
class CustomerDocument extends Model
{
    protected $table = 'customer_documents';
	protected $primaryKey = 'cudo_id';
	public $timestamps = false;
	
	public function customer()
	{
		return $this->belongsTo('App\Models\Customer', 'cudo_cu_id', 'cu_id');
	}
	
	public function user()
	{
		return $this->belongsTo('App\Models\User', 'cudo_us_id', 'id');
	}
}
