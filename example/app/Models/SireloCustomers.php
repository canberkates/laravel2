<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SireloCustomers
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer query()
 * @mixin \Eloquent
 * @property string $key
 * @property string $city_key
 * @property string $forward_1
 * @property string $forward_2
 * @property int $origin_id
 * @property int $top_mover
 * @property int $review_partner
 * @property int $claimed
 * @property string $company_name
 * @property string $legal_name
 * @property string $logo
 * @property string $description
 * @property string|null $coc
 * @property string $language
 * @property int $national_moves
 * @property int $international_moves
 * @property int $long_distance_moving_europe
 * @property int $excess_baggage
 * @property int $man_and_van
 * @property int $car_and_vehicle_transport
 * @property int $piano_transport
 * @property int $pet_transport
 * @property int $art_transport
 * @property string $established
 * @property int $employees
 * @property int $trucks
 * @property string $street
 * @property string $zipcode
 * @property string $city
 * @property string $country
 * @property string $email
 * @property string $telephone
 * @property string $website
 * @property float $rating
 * @property int $reviews
 * @property int $recommendation
 * @property int $recommendations
 * @property string $pros_cons
 * @property int $not_operational
 * @property int $type_of_mover
 * @property int $public_liability_insurance
 * @property int $goods_in_transit_insurance
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomers whereArtTransport($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomers whereCarAndVehicleTransport($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomers whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomers whereCityKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomers whereClaimed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomers whereCoc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomers whereCompanyName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomers whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomers whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomers whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomers whereEmployees($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomers whereEstablished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomers whereExcessBaggage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomers whereForward1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomers whereForward2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomers whereGoodsInTransitInsurance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomers whereInternationalMoves($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomers whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomers whereLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomers whereLegalName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomers whereLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomers whereLongDistanceMovingEurope($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomers whereManAndVan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomers whereNationalMoves($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomers whereNotOperational($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomers whereOriginId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomers wherePetTransport($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomers wherePianoTransport($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomers whereProsCons($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomers wherePublicLiabilityInsurance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomers whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomers whereRecommendation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomers whereRecommendations($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomers whereReviewPartner($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomers whereReviews($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomers whereStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomers whereTelephone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomers whereTopMover($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomers whereTrucks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomers whereTypeOfMover($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomers whereWebsite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SireloCustomers whereZipcode($value)
 */

class SireloCustomers extends Model
{
    protected $table = 'sirelo_customers';
    protected $primaryKey = 'sicuda_key';
    public $timestamps = false;
}
