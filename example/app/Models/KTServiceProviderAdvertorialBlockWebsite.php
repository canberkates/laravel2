<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\KTServiceProviderAdvertorialBlockWebsite
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderAdvertorialBlockWebsite newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderAdvertorialBlockWebsite newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderAdvertorialBlockWebsite query()
 * @mixin \Eloquent
 * @property int $ktsepradblwe_id
 * @property int $ktsepradblwe_sepradbl_id
 * @property int $ktsepradblwe_we_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderAdvertorialBlockWebsite whereKtsepradblweId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderAdvertorialBlockWebsite whereKtsepradblweSepradblId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTServiceProviderAdvertorialBlockWebsite whereKtsepradblweWeId($value)
 */
class KTServiceProviderAdvertorialBlockWebsite extends Model
{
    protected $table = 'kt_service_provider_advertorial_block_website';
    protected $primaryKey = 'ktsepradblwe_id';
    public $timestamps = false;
}
