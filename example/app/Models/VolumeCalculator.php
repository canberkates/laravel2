<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\VolumeCalculator
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VolumeCalculator newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VolumeCalculator newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VolumeCalculator query()
 * @mixin \Eloquent
 * @property int $voca_id
 * @property string|null $voca_token
 * @property int|null $voca_we_id
 * @property string $voca_la_code
 * @property int $voca_type
 * @property string $voca_timestamp
 * @property string $voca_email
 * @property float|null $voca_volume_m3
 * @property float|null $voca_volume_ft3
 * @property string $voca_volume_calculator
 * @property int $voca_mail_sent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VolumeCalculator whereVocaEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VolumeCalculator whereVocaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VolumeCalculator whereVocaLaCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VolumeCalculator whereVocaMailSent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VolumeCalculator whereVocaTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VolumeCalculator whereVocaToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VolumeCalculator whereVocaType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VolumeCalculator whereVocaVolumeCalculator($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VolumeCalculator whereVocaVolumeFt3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VolumeCalculator whereVocaVolumeM3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VolumeCalculator whereVocaWeId($value)
 * @property int|null $voca_afpafo_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VolumeCalculator whereVocaAfpafoId($value)
 */
class VolumeCalculator extends Model
{
    protected $table = 'volume_calculator';
    protected $primaryKey = 'voca_id';
    public $timestamps = false;
}

