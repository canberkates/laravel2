<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\KTCustomerObligation
 *
 * @property-read \App\Models\Insurance $obligation
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerObligation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerObligation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerObligation query()
 * @mixin \Eloquent
 * @property int $ktcuin_id
 * @property int $ktcuin_cuin_id
 * @property int $ktcuin_cu_id
 * @property int|null $ktcuin_cudo_id
 * @property string $ktcuin_verified_timestamp
 * @property int $ktcuin_verification_result
 * @property int|null $ktcuin_amount
 * @property-read \App\Models\Insurance $insurance
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerInsurance whereKtcuinAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerInsurance whereKtcuinCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerInsurance whereKtcuinCudoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerInsurance whereKtcuinCuinId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerInsurance whereKtcuinId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerInsurance whereKtcuinVerificationResult($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KTCustomerInsurance whereKtcuinVerifiedTimestamp($value)
 */
class KTCustomerInsurance extends Model
{
    protected $table = 'kt_customer_insurances';
    protected $primaryKey = 'ktcuin_id';
    public $timestamps = false;

    public function insurance()
    {
        return $this->belongsTo('App\Models\Insurance', 'ktcuin_cuin_id', 'cuin_id');
    }

}
