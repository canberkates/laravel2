<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\FieldChange
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FieldChange newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FieldChange newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FieldChange query()
 * @mixin \Eloquent
 * @property int $fich_id
 * @property string $fich_timestamp
 * @property int|null $fich_us_id
 * @property string $fich_source
 * @property string $fich_table
 * @property int $fich_item_id
 * @property string $fich_changes
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FieldChange whereFichChanges($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FieldChange whereFichId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FieldChange whereFichItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FieldChange whereFichSource($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FieldChange whereFichTable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FieldChange whereFichTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FieldChange whereFichUsId($value)
 */
class FieldChange extends Model
{
    protected $table = 'field_changes';
	protected $primaryKey = 'fich_id';
	public $timestamps = false;
}
