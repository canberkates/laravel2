<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\WebsiteForm
 *
 * @property-read \App\Models\Website $website
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteForm newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteForm newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteForm query()
 * @mixin \Eloquent
 * @property int $wefo_id
 * @property int|null $wefo_we_id
 * @property string $wefo_token
 * @property string $wefo_name
 * @property int|null $wefo_po_id
 * @property string|null $wefo_la_code
 * @property int $wefo_volume_type
 * @property string $wefo_analytics_ua_code
 * @property int $wefo_facebook
 * @property-read \App\Models\Language $language
 * @property-read \App\Models\Portal $portal
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteForm whereWefoAnalyticsUaCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteForm whereWefoFacebook($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteForm whereWefoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteForm whereWefoLaCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteForm whereWefoName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteForm whereWefoPoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteForm whereWefoToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteForm whereWefoVolumeType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteForm whereWefoWeId($value)
 */
class WebsiteForm extends Model
{
    protected $table = 'website_forms';
	protected $primaryKey = 'wefo_id';
	public $timestamps = false;
	
	public function website()
	{
		return $this->hasOne('App\Models\Website', 'we_id', 'wefo_we_id');
	}
    
    public function portal()
    {
        return $this->hasOne('App\Models\Portal', 'po_id', 'wefo_po_id');
    }
    
    public function language()
    {
        return $this->hasOne('App\Models\Language', 'la_code', 'wefo_la_code');
    }
    
}
