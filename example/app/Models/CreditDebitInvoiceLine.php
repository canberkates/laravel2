<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CreditDebitInvoiceLine
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CreditDebitInvoiceLine newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CreditDebitInvoiceLine newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CreditDebitInvoiceLine query()
 * @mixin \Eloquent
 * @property int $crdeinli_id
 * @property string $crdeinli_timestamp
 * @property int|null $crdeinli_cu_id
 * @property int|null $crdeinli_ktrecupo_id
 * @property int|null $crdeinli_ktrecuqu_id
 * @property int $crdeinli_leac_number
 * @property string $crdeinli_description
 * @property int $crdeinli_type
 * @property string $crdeinli_currency
 * @property float $crdeinli_amount
 * @property int $crdeinli_invoiced
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CreditDebitInvoiceLine whereCrdeinliAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CreditDebitInvoiceLine whereCrdeinliCuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CreditDebitInvoiceLine whereCrdeinliCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CreditDebitInvoiceLine whereCrdeinliDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CreditDebitInvoiceLine whereCrdeinliId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CreditDebitInvoiceLine whereCrdeinliInvoiced($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CreditDebitInvoiceLine whereCrdeinliKtrecupoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CreditDebitInvoiceLine whereCrdeinliKtrecuquId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CreditDebitInvoiceLine whereCrdeinliLeacNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CreditDebitInvoiceLine whereCrdeinliTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CreditDebitInvoiceLine whereCrdeinliType($value)
 */
class CreditDebitInvoiceLine extends Model
{
    protected $table = 'credit_debit_invoice_lines';
	protected $primaryKey = 'crdeinli_id';
	public $timestamps = false;
}
