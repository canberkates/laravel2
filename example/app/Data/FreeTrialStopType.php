<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 13:34
 */

namespace App\Data;


class FreeTrialStopType
{
	const DATE = 1;
	const REQUESTS = 2;
	const DATE_OR_REQUESTS = 3;
	
	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::DATE => 'Date',
			self::REQUESTS => 'Requests',
			self::DATE_OR_REQUESTS => 'Date or requests'
		];
	}
}