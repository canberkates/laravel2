<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 11:01
 */

namespace App\Data;


class MovingSize
{
	const REMOVAL_HOUSEHOLD_COMPLETE = 1;
	const REMOVAL_HOUSEHOLD_PART = 2;
	const REMOVAL_HOUSEHOLD_SMALL = 3;
	const BAGGAGE_BOX_PLUS = 4;
	const PET = 5;
	const CAR = 6;
	const COMMERCIAL_SHIPPING = 7;
	const OFFICE_MOVE = 8;
	const BAGGAGE_BOXES_ONLY = 9;
	const PIANO = 10;

	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}

	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::REMOVAL_HOUSEHOLD_COMPLETE => 'Removal – Household (Complete)',
			self::REMOVAL_HOUSEHOLD_PART => 'Removal – Household (Part)',
			self::REMOVAL_HOUSEHOLD_SMALL => 'Removal – Household (Small)',
			self::BAGGAGE_BOX_PLUS => 'Baggage – Baggage (Box plus)',
			self::BAGGAGE_BOXES_ONLY => 'Baggage – Boxes Only',
			self::PET => 'Pet',
			self::CAR => 'Car',
			self::COMMERCIAL_SHIPPING => 'Commercial shipping',
			self::OFFICE_MOVE => 'Office move',
			self::PIANO => 'Piano'
		];
	}
}
