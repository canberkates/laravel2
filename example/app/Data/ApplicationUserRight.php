<?php
/**
 * Created by PhpStorm.
 * User: Lorenzo
 * Date: 09/01/2019
 * Time: 15:47
 */

namespace App\Data;


class ApplicationUserRight
{
	const STANDARD = 1;
	const FINANCE = 2;
	const ADMINISTRATOR = 3;

	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}

	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::STANDARD => "Standard",
			self::FINANCE => "Finance",
			self::ADMINISTRATOR => "Administrator"
		];
	}

}
