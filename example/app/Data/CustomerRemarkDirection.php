<?php
/**
 * Created by PhpStorm.
 * User: Lorenzo
 * Date: 09/01/2019
 * Time: 15:47
 */

namespace App\Data;


class CustomerRemarkDirection
{
	const NOT_AVAILABLE = 0;
	const INCOMING = 1;
	const OUTGOING = 2;
	
	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::NOT_AVAILABLE => "N/A",
			self::INCOMING => "Incoming",
			self::OUTGOING => "Outgoing",
		];
	}
	
}