<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 11:28
 */

namespace App\Data;


class TypeOfMover
{
	const EMPTY = 0;
	const BETWEEN_0_AND_20_TO_3RD_PARTIES = 1;
	const BETWEEN_20_AND_70_TO_3RD_PARTIES = 2;
	const MORE_THAN_70_TO_3RD_PARTIES = 3;
	const ALL_MOVES_OUTSOURCED_TO_3RD_PARTIES = 4;
	
	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::EMPTY => '',
			self::BETWEEN_0_AND_20_TO_3RD_PARTIES => 'Between 0-20% of the moves are carried out by 3rd parties',
			self::BETWEEN_20_AND_70_TO_3RD_PARTIES => 'Between 20-70% of the moves are carried out by 3rd parties',
			self::MORE_THAN_70_TO_3RD_PARTIES => 'More than 70% of the moves are carried out by 3rd parties',
			self::ALL_MOVES_OUTSOURCED_TO_3RD_PARTIES => 'All moves are carried out by 3rd parties',
		];
	}
}