<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 11:27
 */

namespace App\Data;


class RequestTypeToMovingSizes
{
	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}

	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
        return [
            1 => [1, 2, 3],
            2 => [4, 9],
            3 => [5, 6, 7, 10],
            4 => [8],
            5 => [1, 2, 3]
        ];
	}
}
