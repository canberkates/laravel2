<?php
/**
 * Created by PhpStorm.
 * User: Lorenzo
 * Date: 09/01/2019
 * Time: 15:39
 */

namespace App\Data;


class PaymentReminderStatus{
	
	const REGULAR = 0;
	const ONLY_FRIENDLY = 1;
	
	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::REGULAR => 'Regular',
			self::ONLY_FRIENDLY => 'Only Friendly'
		];
	}
	
}