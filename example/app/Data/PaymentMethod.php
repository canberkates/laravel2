<?php
/**
 * Created by PhpStorm.
 * User: Lorenzo
 * Date: 09/01/2019
 * Time: 15:39
 */

namespace App\Data;


class PaymentMethod{
	
	const BANK_TRANSFER = 1;
	const CREDIT_CARD = 2;
	const AUTO_DEBIT = 3;
	const PREPAYMENT = 4;
	const PREPAYMENT_AUTO_CHARGE_CREDIT_CARD = 5;
	const PREPAYMENT_AUTO_DIRECT_DEBIT = 6;

	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::BANK_TRANSFER => 'Bank transfer',
			self::CREDIT_CARD => 'Credit card',
			self::AUTO_DEBIT => 'Auto debit',
			self::PREPAYMENT => 'Prepayment: wait for partner to pay',
			self::PREPAYMENT_AUTO_CHARGE_CREDIT_CARD => 'Prepayment: Auto charge Credit Card',
			self::PREPAYMENT_AUTO_DIRECT_DEBIT => 'Prepayment: Auto Direct Debit'
		];
	}
	
}