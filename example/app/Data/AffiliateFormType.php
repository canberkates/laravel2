<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 13:34
 */

namespace App\Data;


class AffiliateFormType
{
	const IFRAME = 1;
	const API = 2;
	const BANNER = 3;
	const CONVERSION_TOOLS = 4;

	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}

	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::IFRAME => 'iFrame',
			self::API => 'API',
			self::BANNER => 'Banner',
			self::CONVERSION_TOOLS => 'Conversion Tools Form'
		];
	}
}
