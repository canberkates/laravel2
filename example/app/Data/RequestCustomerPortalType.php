<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 12:05
 */

namespace App\Data;


class RequestCustomerPortalType
{
	const PAID = 1;
	const FREE_TRIAL = 2;
	const SIRELO = 3;
	const FREE_LEADS = 4;
	const LEADS_STORE = 5;

	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}

	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::PAID => 'Paid',
			self::FREE_TRIAL => 'Free trial',
			self::SIRELO => 'Sirelo',
			self::FREE_LEADS => 'Free leads',
			self::LEADS_STORE => 'Leads Store'
		];
	}
}
