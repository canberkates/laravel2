<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 11:35
 */

namespace App\Data;


class SurveyStatus
{
	const SENT = 1;
	const OPENED = 2;
	const SUBMITTED = 3;
	
	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::SENT => 'Sent',
			self::OPENED => 'Opened',
			self::SUBMITTED => 'Submitted'
		];
	}
}