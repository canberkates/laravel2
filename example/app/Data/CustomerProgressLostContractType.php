<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 11:28
 */

namespace App\Data;

class CustomerProgressLostContractType
{

    const ACCREDITATIONS = 1;
    const AMOUNT_OF_LEADS = 2;
    const ASSET_LIGHT = 3;
    const BUSINESS_MODEL_MOVER = 4;
    const CLAIMS = 5;
    const COMPETITION = 6;
    const DOCUMENTS = 7;
    const EXPERIENCE = 8;
    const MINIMUM_CAPPING = 9;
    const NO_OCCUPANCY_AREA_FULL = 10;
    const NO_SERVICE_AVAILABLE = 11;
    const NO_WEBSITE = 12;
    const ONLINE_REPUTATION = 13;
    const PAYMENT_METHOD = 14;
    const PREVIOUS_FINANCIAL_REASONS = 15;
    const PRICE = 16;
    const ROUTES  = 17;
    const SIRELO = 18;
    const TRIAL_PERIOD = 19;
    const UNREACHABLE = 20;
    const BUSINESS_MODEL_TRIGLOBAL = 21;
    const NO_CURRENT_NEED = 22;
    const PAUSE = 23;


	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}

	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
            //They reject Us
            self::ACCREDITATIONS => "Accreditations",
            self::AMOUNT_OF_LEADS => "Amount of leads",
            self::ASSET_LIGHT => "Asset light",
            self::BUSINESS_MODEL_MOVER =>"Business model (mover)",
            self::CLAIMS => "Claims",
            self::COMPETITION => "Competition",
            self::DOCUMENTS => "Documents",
            self::EXPERIENCE => "Experience",
            self::MINIMUM_CAPPING =>"Minimum capping",
            self::NO_OCCUPANCY_AREA_FULL => "No occupancy (area is full)",
            self::NO_SERVICE_AVAILABLE => "No service available",
            self::NO_WEBSITE =>  "No website",
            self::ONLINE_REPUTATION =>  "Online reputation",
            self::PAYMENT_METHOD => "Payment method",
            self::PREVIOUS_FINANCIAL_REASONS => "Previous financial reasons",
			self::PRICE => "Price",
			self::ROUTES => "Routes",
			self::SIRELO => "Sirelo",
			self::TRIAL_PERIOD => "Trial period",
			self::UNREACHABLE => "Unreachable",
			self::BUSINESS_MODEL_TRIGLOBAL => "Business Model - TriGlobal",
			self::NO_CURRENT_NEED => "No current need",
			self::PAUSE => "Pause",

		];
	}
}
