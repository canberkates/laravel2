<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 11:28
 */

namespace App\Data;


class ClaimReason
{
	const TRIGLOBALS_MISTAKE = -1;
	const AGREEMENT_WITH_PARTNERDESK = 0;
	const UNABLE_TO_REACH = 1;
	const NO_RELOCATION = 2;
	const ALREADY_RECEIVED_VIA_TRIGLOBAL = 3;
	const ALREADY_RECEIVED_VIA_OWN_WEBSITE = 4;
	const ALREADY_RECEIVED_VIA_OTHER_GENERATOR = 5;
	const MOVE_DATE_NOT_BETWEEN_2_WEEKS_AND_9_MONTHS = 6;
	const MOVE_DATE_NOT_BETWEEN_1_WEEK_AND_9_MONTHS = 7;
	const MOVE_DATE_NOT_BETWEEN_2_DAYS_AND_9_MONTHS = 8;
	const COVID_19 = 9;

	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::TRIGLOBALS_MISTAKE => "TriGlobal's mistake",
			self::AGREEMENT_WITH_PARTNERDESK => 'Special agreement with partnerdesk',
			self::UNABLE_TO_REACH => 'Unable to reach',
			self::NO_RELOCATION => 'No relocation',
			self::ALREADY_RECEIVED_VIA_TRIGLOBAL => "Already received via TriGlobal",
			self::ALREADY_RECEIVED_VIA_OWN_WEBSITE => 'Already received via own website',
			self::ALREADY_RECEIVED_VIA_OTHER_GENERATOR => 'Have received before via other lead generator',
			self::MOVE_DATE_NOT_BETWEEN_2_WEEKS_AND_9_MONTHS => 'The move date is not scheduled between 2 weeks and 9 months',
			self::MOVE_DATE_NOT_BETWEEN_1_WEEK_AND_9_MONTHS => 'The move date is not scheduled between 1 week and 9 months',
			self::MOVE_DATE_NOT_BETWEEN_2_DAYS_AND_9_MONTHS => 'The move date is not scheduled between 2 days and 9 months',
			self::COVID_19 => 'COVID-19 Special Terms'
		];
	}
}