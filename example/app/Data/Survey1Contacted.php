<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 12:22
 */

namespace App\Data;


class Survey1Contacted
{
	const EMPTY = 0;
	const YES_WITHIN_48H = 1;
	const YES_AFTER_48H = 2;
	const NO = 3;
	
	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::EMPTY => 'Empty',
			self::YES_WITHIN_48H => 'Yes, within 48 hours',
			self::YES_AFTER_48H => 'Yes, after 48 hours',
			self::NO => 'No'
		];
	}
}