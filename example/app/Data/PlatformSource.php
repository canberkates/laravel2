<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 11:48
 */

namespace App\Data;


class PlatformSource
{
	const GOOGLE = 'g';
	const FACEBOOK = 'f';
	const BING = 'b';
	const AFFILIATE = 'a';
	const GOOGLE_DISPLAY = 'd';

	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}

	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::GOOGLE => 'Google',
			self::FACEBOOK => 'Facebook',
			self::BING => 'Bing',
			self::AFFILIATE => 'Affiliate',
			self::GOOGLE_DISPLAY => 'Google Display'
		];
	}
}
