<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 11-3-2019
 * Time: 12:05
 */

namespace App\Data;


class ContactPersonDepartment
{
	const UNKNOWN = 0;
	const GENERAL_MANAGEMENT = 1;
	const MARKETING_AND_SALES = 2;
	const FINANCE = 3;
	const FLEET_MANAGEMENT = 4;
	
	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::UNKNOWN => "Unknown",
			self::GENERAL_MANAGEMENT => "General management",
			self::MARKETING_AND_SALES => "Marketing and Sales",
			self::FINANCE => "Finance",
			self::FLEET_MANAGEMENT => "Fleet management"
		];
	}
	
}