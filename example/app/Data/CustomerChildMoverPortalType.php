<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 11-3-2019
 * Time: 12:05
 */

namespace App\Data;


class CustomerChildMoverPortalType
{
	const SINGLE_ACCESS = 1;
	const ME_AND_PARENTS_ACCESS = 2;
	const PARENTS_ONLY = 3;

	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}

	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::SINGLE_ACCESS => "Only I have access to my Mover Portal",
			self::ME_AND_PARENTS_ACCESS => "My Parent & I have access to my Mover Portal",
			self::PARENTS_ONLY => "Only my parent have access to my Mover Portal",
		];
	}

}
