<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 11:42
 */

namespace App\Data;


class CustomerPairStatus
{
	const INACTIVE = 0;
	const ACTIVE = 1;
	const PAUSE = 2;
	
	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::INACTIVE => 'Inactive',
			self::ACTIVE => 'Active',
			self::PAUSE => 'Pause'
		];
	}
}