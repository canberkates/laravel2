<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 13:26
 */

namespace App\Data;


class NewsletterSlot
{
	const HEALTH_INSURANCE = 1;
	const CURRENCY_EXCHANGE = 2;
	const DORMER = 3;
	const INSULTATION = 4;
	const FRAMES = 5;
	const VISA_ADVICE = 6;
	const TAX = 7;
	const PERSONAL_PROPERTY_INSURANCE = 8;
	const REAL_ESTATE = 9;
	const INTERESTING_ARTICLES = 10;

	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}

	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::HEALTH_INSURANCE => 'Health Insurance',
			self::CURRENCY_EXCHANGE => 'Currency Exchange',
			self::DORMER => 'Dormer',
			self::INSULTATION => 'Insultation',
			self::FRAMES => 'Frames',
			self::VISA_ADVICE => 'Visa Advice',
			self::TAX => 'Tax',
			self::PERSONAL_PROPERTY_INSURANCE => 'Personal Property Insurance',
			self::REAL_ESTATE => 'Real Estate',
			self::INTERESTING_ARTICLES => 'Interesting Articles'
		];
	}
}
