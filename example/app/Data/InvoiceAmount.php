<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 11:28
 */

namespace App\Data;


class InvoiceAmount
{
	const ALL_INVOICES = "";
	const HUNDRED = "100";
	const ZERO = "0";
	
	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::ALL_INVOICES => 'All invoices',
			self::HUNDRED => '> 100',
			self::ZERO => '= 0'
		];
	}
}
