<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 11:28
 */

namespace App\Data;


class RequestUpdateFields
{
    const TELEPHONE = "re_telephone1";
    const CITY_FROM = "re_city_from";
    const CITY_TO = "re_city_to";
    const STREET_FROM = "re_street_from";
    const ZIPCODE_FROM = "re_zipcode_from";
    const ZIPCODE_TO = "re_zipcode_to";
    const STREET_TO = "re_street_to";
    const COUNTRY_FROM = "re_co_code_from";
    const COUNTRY_TO = "re_co_code_to";
    const MOVING_DATE = "re_moving_date";
    const REGION_FROM = "re_reg_id_from";
    const REGION_TO = "re_reg_id_to";
    const MOVING_SIZE = "re_moving_size";
    const STORAGE = "re_storage";
    const BUSINESS = "re_business";
    const ASSEMBLY = "re_assembly";
    const PACKING = "re_packing";
    const DOUBLE = "re_double";
    const SPAM = "re_spam";
    const ON_HOLD = "re_on_hold";
    const REQUEST_TYPE = "re_request_type";
    const FULL_NAME = "re_full_name";
    const EMAIL = "re_email";
    const VOLUME_M3 = "re_volume_m3";
    const REMARKS = "re_remarks";

    /**
     * @param int $id The id (see constants)
     * @return null|string Result
     * @see SurveyType::REQUEST
     * @see SurveyType::EXPERIENCE
     */
    public static function name($id) {
        return self::all()[$id] ?? null;
    }

    /**
     * Get all types
     * @return string[]
     */
    public static function all() {
        return [
            self::TELEPHONE => 'Telephone',
            self::STREET_FROM=> 'Street From',
            self::STREET_TO=> 'Street To',
            self::ZIPCODE_FROM=> 'Zipcode From',
            self::ZIPCODE_TO=> 'Zipcode To',
            self::CITY_FROM=> 'City From',
            self::CITY_TO=> 'City To',
            self::COUNTRY_FROM=> 'Country From',
            self::COUNTRY_TO=> 'Country To',
            self::MOVING_DATE=> 'Moving Date',
            self::REGION_FROM=> 'Region From',
            self::REGION_TO => 'Region To',
            self::MOVING_SIZE => 'Moving Size',
            self::STORAGE => 'Storage',
            self::BUSINESS => 'Business',
            self::ASSEMBLY => 'Assembly',
            self::PACKING => 'Packing',
            self::DOUBLE => 'Double',
            self::SPAM => 'Spam',
            self::ON_HOLD => 'On hold',
            self::REQUEST_TYPE => 'Request Type',
            self::FULL_NAME => 'Full Name',
            self::EMAIL => 'Email',
            self::VOLUME_M3 => 'Volume m3',
            self::REMARKS => 'Remarks',
        ];
    }
}
