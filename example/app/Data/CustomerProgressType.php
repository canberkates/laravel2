<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 11:28
 */

namespace App\Data;

class CustomerProgressType
{
	const PROSPECT = 0;
	const FIRST_CONTACT = 1;
	const WORKING_ON = 2;
	const PROPOSAL_SENT = 3;
	const CONTRACT_SENT = 4;
	const LOST = 5;
	const WON = 6;
	const CANCELLED = 7;

	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}

	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::PROSPECT => "Prospect",
			self::FIRST_CONTACT => "First contact",
            self::WORKING_ON => "Working on",
            self::PROPOSAL_SENT => "Proposal sent",
            self::CONTRACT_SENT => "Contract sent",
            self::LOST => "Lost",
            self::WON => "Won",
            self::CANCELLED => "Cancelled"
		];
	}
}
