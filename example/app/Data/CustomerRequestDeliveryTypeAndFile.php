<?php
/**
 * Created by PhpStorm.
 * User: Lorenzo
 * Date: 09/01/2019
 * Time: 15:47
 */

namespace App\Data;


class CustomerRequestDeliveryTypeAndFile
{
	const MAIL_DEFAULT = 1;
	const MAIL_PLAIN_TEXT = 2;
	const HTTP_POST = 3;
	const JSON_POST = 4;
	const XML_MAIL = 5;
	const CSV_ATTACHMENT = 6;
	const MOVEMAN_PRO = 7;
	const MOVEWARE = 8;
	const ALLIED_PICKFORDS = 9;
	const MOVERWORX = 10;
	const MOVEGISTICS = 11;
	const GRANOT = 12;
	const SALESFORCE = 13;
	const VOXME = 14;
	const EMOVER = 15;
	const PREMIUM_MOVERS = 16;
	const BEST_GLOBAL_MOVERS = 17;
	const BIARD_DEMENAGEMENTS = 18;
	const FAZLAND = 19;
	const MY_MOVING_LOADS = 20;
	const REMOVALS_MANAGER = 21;
	const EUROPEAN_MOVING = 22;
	const MAIL_JSON = 23;
	const MOVINGA = 24;
	const INTERNATIONAL_SEA_AND_AIR_SHIPPING = 25;
	const SALESFORCE_GINTER = 26;
	const SALESFORCE_UNIVERSAL = 27;
	const DEMENGO = 28;
	const MOVEWARE_SIMPSONS = 29;
	const MOVEWARE_OSS = 30;
	const PARC = 31;
	const P4P = 32;
	const SCOUT24 = 33;
	const NCV = 34;
	const DES_BRAS_EN_PLUS = 35;

	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}

	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::MAIL_DEFAULT =>  ["Mail (Default)", null],
			self::MAIL_PLAIN_TEXT => ["Mail (Plain text)", "sendPlainTextMail"],
			self::HTTP_POST => ["HTTP POST", "sendHTTP"],
			self::JSON_POST =>  ["JSON POST", "sendJSON"],
			self::XML_MAIL => ["XML (Mail)", "sendXML"],
			self::CSV_ATTACHMENT => ["CSV (Attachment)", "sendCSVAttachment"],
			self::MOVEMAN_PRO => ["MoveMan Pro", "sendMoveManPro"],
			self::MOVEWARE => ["Moveware", "sendMoveWare"],
			self::ALLIED_PICKFORDS => ["Allied Pickfords", "sendAlliedPickfords"],
			self::MOVERWORX => ["MoverworX", "sendMoverworX"],
			self::MOVEGISTICS => ["Movegistics", "sendMovegistics"],
			self::GRANOT => ["Granot", "sendGranot"],
			self::SALESFORCE => ["Salesforce", "sendSalesforce"],
			self::VOXME => ["Voxme", "sendVoxme"],
			self::EMOVER => ["eMover", "sendeMover"],
			self::PREMIUM_MOVERS => ["Premium Movers", "sendPremiumMovers"],
			self::BEST_GLOBAL_MOVERS => ["Best Global Movers", "sendBestGlobalMovers"],
			self::BIARD_DEMENAGEMENTS => ["Biard Déménagements", "sendBiardDemenagements"],
			self::FAZLAND => ["Fazland", "sendFazland"],
			self::MY_MOVING_LOADS => ["MyMovingLoads", "sendMyMovingLoads"],
			self::REMOVALS_MANAGER => ["Removals Manager", "sendRemovalsManager"],
			self::EUROPEAN_MOVING => ["European Moving", "sendEuropeanMoving"],
			self::MAIL_JSON => ["Mail (JSON)", "sendJSONMail"],
			self::MOVINGA => ["Movinga", "sendMovinga"],
			self::INTERNATIONAL_SEA_AND_AIR_SHIPPING => ["International Sea & Air Shipping", "sendInternationalSeasAndAirShipping"],
			self::SALESFORCE_GINTER => ["Salesforce Ginter", "sendSalesforceGinter"],
			self::SALESFORCE_UNIVERSAL => ["Salesforce Universal", "sendSalesforceUniversal"],
			self::DEMENGO => ["Demengo", "sendDemengo"],
			self::MOVEWARE_SIMPSONS => ["Moveware Simpsons (HTTP POST)", "sendSimpsons"],
			self::MOVEWARE_OSS => ["Moveware OSS", "sendMovewareOSS"],
			self::PARC => ["PARC", "sendPARC"],
			self::P4P => ["P4P", "sendP4P"],
			self::SCOUT24 => ["Scout 24", "sendScout24"],
			self::NCV => ["NCV", "sendNCV"],
			self::DES_BRAS_EN_PLUS => ["Des Bras en Plus", "sendDesBrasEnPlus"],
		];
	}

}
