<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 12:05
 */

namespace App\Data;


class SubscriptionType
{
	const LEADS_STORE = 1;
	const CONVERSION_TOOLS = 2;

	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}

	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::LEADS_STORE => 'Leads Store',
			self::CONVERSION_TOOLS => 'Conversion Tools',
		];
	}
}
