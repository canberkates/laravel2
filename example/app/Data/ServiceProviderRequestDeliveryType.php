<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 01/04/2019
 * Time: 15:47
 */

namespace App\Data;


class ServiceProviderRequestDeliveryType
{
	const MAIL_DEFAULT = 1;
	const MAIL_PLAIN_TEXT = 2;
	const HTTP_POST = 3;
	const JSON_POST = 4;
	const XML_MAIL = 5;
	const CSV_ATTACHMENT = 6;
	const SALESFORCE = 7;
	const SALESFORCE_FREEDOM_HEALTH_INSURANCE = 8;
	const TORFX = 9;
	const TORFXOZ = 10;


	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}

	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::MAIL_DEFAULT =>  "Mail (Default)",
			self::MAIL_PLAIN_TEXT => "Mail (Plain text)",
			self::HTTP_POST => "HTTP POST",
			self::JSON_POST =>  "JSON POST",
			self::XML_MAIL => "XML (Mail)",
			self::CSV_ATTACHMENT => "CSV (Attachment)",
			self::SALESFORCE => "Salesforce",
			self::SALESFORCE_FREEDOM_HEALTH_INSURANCE => "Salesforce (Freedom health insurance)",
			self::TORFX => "Tor FX",
			self::TORFXOZ => "Tor FX OZ"
		];
	}

}
