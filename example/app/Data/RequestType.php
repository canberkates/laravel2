<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 11:27
 */

namespace App\Data;


class RequestType
{
	const MOVE = 1;
	const BAGGAGE = 2;
	const TRANSPORT = 3;
	const OFFICE_MOVE = 4;
	const PREMIUM = 5;
	
	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::MOVE => 'Move',
			self::BAGGAGE => 'Baggage',
			self::TRANSPORT => 'Transport',
			self::OFFICE_MOVE => 'Office Move',
			self::PREMIUM => 'Premium'
		];
	}
}
