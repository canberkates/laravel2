<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 10:43
 */

namespace App\Data;


class ReportProgressStatus
{
	const QUEUED = 0;
	const IN_PROGRESS = 1;
	const FINAL_SECTION = 2;
	const FINISHED = 3;
	const DELETED = 4;
	const FAILED = 5;
	
	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::QUEUED => 'Queued',
			self::IN_PROGRESS => 'In progress',
			self::FINAL_SECTION => 'Final section',
			self::FINISHED => 'Finished',
			self::DELETED => 'Deleted',
			self::FAILED => 'Failed',
		];
	}
}