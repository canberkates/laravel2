<?php
/**
 * Created by PhpStorm.
 * User: Lorenzo
 * Date: 09/01/2019
 * Time: 15:47
 */

namespace App\Data;


class CustomerType
{
	const MOVER = 1;
	const SERVICE_PROVIDER = 2;
	const AFFILIATE_PARTNER = 3;
	const OTHER = 4;
	const IAM = 5;
	const LEAD_RESELLER = 6;

	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}

	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::MOVER => "Mover",
			self::SERVICE_PROVIDER => "Service provider",
			self::AFFILIATE_PARTNER => "Affiliate partner",
			self::OTHER => "Other",
			self::LEAD_RESELLER => "Lead reseller"
		];
	}

}
