<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 11:39
 */

namespace App\Data;


class RequestResidence
{
	const EMPTY = 0;
	const GROUND_FLOOR = 1;
	const HOUSE_MULTIPLE_FLOORS = 2;
	const TWO_STOREY_FLAT = 3;
	const LOWER_GROUND_FLOOR = 4;
	const FIRST_FLOOR = 5;
	const SECOND_FLOOR = 6;
	const THIRD_FLOOR = 7;
	const FOURTH_FLOOR = 8;
	const FIFTH_FLOOR = 9;
	const SIXTH_FLOOR = 10;
	const HIGHER_THAN_SIXTH_FLOOR = 11;
	
	
	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::EMPTY => '-',
			self::GROUND_FLOOR => "Ground floor",
			self::HOUSE_MULTIPLE_FLOORS => 'House (Multiple floors)',
			self::TWO_STOREY_FLAT => 'Two-storey flat',
			self::LOWER_GROUND_FLOOR => 'Lower ground floor',
			self::FIRST_FLOOR => "1. floor",
			self::SECOND_FLOOR => "2. floor",
			self::THIRD_FLOOR =>  "3. floor",
			self::FOURTH_FLOOR => "4. floor",
			self::FIFTH_FLOOR => "5. floor",
			self::SIXTH_FLOOR => "6. floor",
			self::HIGHER_THAN_SIXTH_FLOOR => "Higher than 6. floor"
		];
	}
}