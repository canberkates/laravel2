<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 11:03
 */

namespace App\Data;


class RequestInternalCalled
{
	const NOT_CALLED = 0;
	const CALLED_CLIENT_DID_NOT_PICK_UP_THE_PHONE = 1;
	const CALLED_TALKED_TO_THE_CLIENT = 2;
	
	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::NOT_CALLED => 'Not called',
			self::CALLED_CLIENT_DID_NOT_PICK_UP_THE_PHONE => 'Called, client did not pick up the phone',
			self::CALLED_TALKED_TO_THE_CLIENT => 'Called, talked to the client'
		];
	}
}