<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 11:40
 */

namespace App\Data;


class MoverCappingMethod
{
	const PORTAL_LEVEL = 0;
	const MONTHLY_LEADS = 1;
	const MONTHLY_SPEND = 2;
	
	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::PORTAL_LEVEL => 'Portal level',
			self::MONTHLY_LEADS => 'Monthly leads',
			self::MONTHLY_SPEND => 'Monthly spend'
		];
	}
}