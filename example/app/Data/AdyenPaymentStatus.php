<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 11:28
 */

namespace App\Data;

class AdyenPaymentStatus
{
	const FAILED = -1;
	const QUEUED = 0;
	const PROCESSING = 1;
	const WAITING_FOR_CONFIRMATION = 2;
	const CONFIRMED = 3;
	const CHARGEBACK = 4;
	const REFUNDED_CANCELLED = 5;
	
	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::FAILED => "Failed",
			self::QUEUED => "Queued",
			self::PROCESSING => "Processing",
			self::WAITING_FOR_CONFIRMATION =>  "Waiting for confirmation",
			self::CONFIRMED => "Confirmed / Refused?",
			self::CHARGEBACK => "Chargeback",
			self::REFUNDED_CANCELLED => "Refunded / Cancelled"
		];
	}
}
