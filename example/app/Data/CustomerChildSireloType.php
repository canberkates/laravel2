<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 11-3-2019
 * Time: 12:05
 */

namespace App\Data;


class CustomerChildSireloType
{
	const SELF = 1;
	const SELF_AND_GROUP = 2;
	const GROUP_ONLY = 3;

	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}

	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::SELF => "Show own reviews",
			self::SELF_AND_GROUP => "Show own reviews and group reviews separately",
			self::GROUP_ONLY => "Show all combined group reviews"
		];
	}

}
