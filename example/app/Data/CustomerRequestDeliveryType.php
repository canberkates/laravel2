<?php
/**
 * Created by PhpStorm.
 * User: Lorenzo
 * Date: 09/01/2019
 * Time: 15:47
 */

namespace App\Data;


class CustomerRequestDeliveryType
{
	const MAIL_DEFAULT = 1;
	const MAIL_PLAIN_TEXT = 2;
	const HTTP_POST = 3;
	const JSON_POST = 4;
	const XML_MAIL = 5;
	const CSV_ATTACHMENT = 6;
	const MOVEMAN_PRO = 7;
	const MOVEWARE = 8;
	const ALLIED_PICKFORDS = 9;
	const MOVERWORX = 10;
	const MOVEGISTICS = 11;
	const GRANOT = 12;
	const SALESFORCE = 13;
	const VOXME = 14;
	const EMOVER = 15;
	const PREMIUM_MOVERS = 16;
	const BEST_GLOBAL_MOVERS = 17;
	const BIARD_DEMENAGEMENTS = 18;
	const FAZLAND = 19;
	const MY_MOVING_LOADS = 20;
	const REMOVALS_MANAGER = 21;
	const EUROPEAN_MOVING = 22;
	const MAIL_JSON = 23;
	const MOVINGA = 24;
	const INTERNATIONAL_SEA_AND_AIR_SHIPPING = 25;
	const SALESFORCE_GINTER = 26;
	const SALESFORCE_UNIVERSAL = 27;
	const DEMENGO = 28;
	const MOVEWARE_SIMPSONS = 29;
	const MOVEWARE_OSS = 30;
	const PARC = 31;
	const P4P = 32;
	const SCOUT24 = 33;
	const NCV = 34;
	const DES_BRAS_EN_PLUS = 35;
	const ATLAS_MOVEWARE = 36;

	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}

	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::MAIL_DEFAULT =>  "Mail (Default)",
			self::MAIL_PLAIN_TEXT => "Mail (Plain text)",
			self::HTTP_POST => "HTTP POST",
			self::JSON_POST =>  "JSON POST",
			self::XML_MAIL => "XML (Mail)",
			self::CSV_ATTACHMENT => "CSV (Attachment)",
			self::MOVEMAN_PRO => "MoveMan Pro",
			self::MOVEWARE => "Moveware",
			self::ALLIED_PICKFORDS => "Allied Pickfords",
			self::MOVERWORX => "MoverworX",
			self::MOVEGISTICS => "Movegistics",
			self::GRANOT => "Granot",
			self::SALESFORCE => "Salesforce",
			self::VOXME => "Voxme",
			self::EMOVER => "eMover",
			self::PREMIUM_MOVERS => "Premium Movers",
			self::BEST_GLOBAL_MOVERS => "Best Global Movers",
			self::BIARD_DEMENAGEMENTS => "Biard Déménagements",
			self::FAZLAND => "Fazland",
			self::MY_MOVING_LOADS => "MyMovingLoads",
			self::REMOVALS_MANAGER => "Removals Manager",
			self::EUROPEAN_MOVING => "European Moving",
			self::MAIL_JSON => "Mail (JSON)",
			self::MOVINGA => "Movinga",
			self::INTERNATIONAL_SEA_AND_AIR_SHIPPING => "International Sea & Air Shipping",
			self::SALESFORCE_GINTER => "Salesforce Ginter",
			self::SALESFORCE_UNIVERSAL => "Salesforce Universal",
			self::DEMENGO => "Demengo",
			self::MOVEWARE_SIMPSONS => "Moveware Simpsons (HTTP POST)",
			self::MOVEWARE_OSS => "Moveware OSS",
			self::PARC => "PARC",
			self::P4P => "P4P",
			self::SCOUT24 => "Scout 24",
			self::NCV => "NCV",
			self::DES_BRAS_EN_PLUS => "Des Bras en Plus",
			self::ATLAS_MOVEWARE => "Atlas Moveware",
		];
	}

}
