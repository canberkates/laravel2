<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 11:28
 */

namespace App\Data;

class AutomatedDecision
{
	const AUTOMATIC_MATCH = 1;
	const AUTOMATIC_REJECT = 2;
	const MANUAL = 3;

	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}

	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::AUTOMATIC_MATCH => "Automatic Match",
			self::AUTOMATIC_REJECT => "Automatic Reject",
            self::MANUAL => "Manual"
		];
	}
}
