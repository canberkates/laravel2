<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 11-3-2019
 * Time: 12:05
 */

namespace App\Data;


class CustomerChildType
{
	const BRANCHE = 1;
	const BRAND = 2;
	const INDIVIDUAL_COMPANY = 3;

	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}

	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::BRANCHE => "Branche",
			self::BRAND => "Brand",
			self::INDIVIDUAL_COMPANY => "Individual Company",
		];
	}

}
