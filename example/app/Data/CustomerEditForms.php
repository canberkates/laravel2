<?php
/**
 * Created by PhpStorm.
 * User: Lorenzo
 * Date: 09/01/2019
 * Time: 15:39
 */

namespace App\Data;


class CustomerEditForms{

	const GENERAL = "General";
	const ADDRESSES = "Addresses";
	const FINANCE = "Finance";
	const AUTO_DEBIT = "Auto debit";
	const SIRELO = "Sirelo";
	const MOVER_PORTAL = "Mover Portal";
	const SETTINGS = "Settings";
	const MEMBERSHIPS = "Memberships";
	const OBLIGATIONS = "Obligations";
	const INSURANCES = "Insurances";
	const FIELDS = "Fields";
	const EXTERNAL_REVIEWS = "External Reviews";
	const CHILD_DEFAULTS = "Child Defaults";

	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}

	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::GENERAL => 'General',
			self::ADDRESSES => 'Addresses',
			self::FINANCE => 'Finance',
			self::SIRELO => 'Sirelo',
			self::MOVER_PORTAL => 'Mover Portal',
			self::SETTINGS => 'Settings',
			self::MEMBERSHIPS => 'Memberships',
			self::INSURANCES => 'Insurances',
			self::OBLIGATIONS => 'Obligations',
			self::FIELDS => 'Fields',
	        self::AUTO_DEBIT => "Auto debit",
	        self::EXTERNAL_REVIEWS => "External Reviews",
	        self::CHILD_DEFAULTS => "Child Defaults"
        ];
	}

}
