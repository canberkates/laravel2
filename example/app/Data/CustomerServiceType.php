<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 11:28
 */

namespace App\Data;


class CustomerServiceType
{
	const SIRELO = 1;
	const LEADS = 2;
	const PREMIUM_LEADS = 3;
	const LOAD_EXCHANGE = 4;

	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}

	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::SIRELO => 'Sirelo',
			self::LEADS => 'Leads',
			self::PREMIUM_LEADS => 'Premium Leads',
			self::LOAD_EXCHANGE => 'Load Exchange',
		];
	}
}
