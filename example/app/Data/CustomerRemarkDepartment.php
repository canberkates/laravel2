<?php
/**
 * Created by PhpStorm.
 * User: Lorenzo
 * Date: 09/01/2019
 * Time: 15:47
 */

namespace App\Data;


class CustomerRemarkDepartment
{
	const OTHER = 0;
	const SALES = 1;
	const FINANCE = 2;
	const CUSTOMERSERVICE = 3;
	const PARTNERDESK = 4;
	const MARKETING = 5;
	const AFFILIATE_SERVICE_PROVIDER = 6;
	const MANAGEMENT = 7;
	
	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::OTHER => "Other",
			self::SALES => "Sales",
			self::FINANCE => "Finance",
			self::CUSTOMERSERVICE => "Customerservice",
			self::PARTNERDESK => "Partnerdesk",
			self::MARKETING => "Marketing",
			self::AFFILIATE_SERVICE_PROVIDER => "Affiliate/Service Provider",
			self::MANAGEMENT => "Management"
		];
	}
	
}