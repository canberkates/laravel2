<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 11:28
 */

namespace App\Data;


class BankLineSource
{
	const BANK_LINE = 1;
	const ADYEN = 2;
	const PAYPAL_ADYEN = 3;
	
	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::BANK_LINE => 'Bank line',
			self::ADYEN => 'Adyen',
			self::PAYPAL_ADYEN => 'Paypal (Adyen)'
		];
	}
}
