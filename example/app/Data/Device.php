<?php
/**
 * Created by PhpStorm.
 * User: Lorenzo
 * Date: 09/01/2019
 * Time: 15:47
 */

namespace App\Data;


class Device
{
	const UNKNOWN = 0;
	const COMPUTER = 1;
	const TABLET = 2;
	const MOBILE = 3;
	
	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::UNKNOWN => "Unknown",
			self::COMPUTER => "Computer",
			self::TABLET => "Tablet",
			self::MOBILE => "Mobile"
		];
	}
	
}