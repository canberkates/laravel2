<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 11:28
 */

namespace App\Data;

class RecoverReason
{
	const USER_CONTACT = 1;
	const AUTOMATIC_MISTAKE = 2;
	const OTHER = 3;

	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}

	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::USER_CONTACT => "Contact with the user",
			self::AUTOMATIC_MISTAKE => "Automatic matching made a mistake",
            self::OTHER => "Other"
		];
	}
}
