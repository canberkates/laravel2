<?php
/**
 * Created by PhpStorm.
 * User: Lorenzo
 * Date: 09/01/2019
 * Time: 15:47
 */

namespace App\Data;


class AutoDebitType
{
	const FIRST = "FRST";
	const RECURRING = "RCUR";
	const FINAL = "FNAL";
	const ONE_OFF = "OOFF";
	
	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::FIRST => "First",
			self::RECURRING => "Recurring",
			self::FINAL => "Final",
			self::ONE_OFF => "One off"
		];
	}
	
}
