<?php
/**
 * Created by PhpStorm.
 * User: Lorenzo
 * Date: 09/01/2019
 * Time: 15:47
 */

namespace App\Data;


class ContactPersonFunction
{
	const OTHER = 0;
	const OWNER = 1;
	const CEO = 2;
	const MANAGER_SALES_MARKETING = 3;
	const SALES_REPRESENTATIVE = 4;
	const MANAGER_FINANCE = 5;
	const FINANCE_EMPLOYEE = 6;
	const PLANNER = 7;
	
	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::OTHER => "Other",
			self::OWNER => "Owner",
			self::CEO => "CEO",
			self::MANAGER_SALES_MARKETING => "Manager Sales/Marketing",
			self::SALES_REPRESENTATIVE => "Sales representative",
			self::MANAGER_FINANCE => "Manager Finance",
			self::FINANCE_EMPLOYEE => "Finance Employee",
			self::PLANNER => "Planner",
		];
	}
	
}