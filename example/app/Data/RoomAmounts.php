<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 11:01
 */

namespace App\Data;


class RoomAmounts
{
	const ONE = 1;
	const TWO = 2;
	const THREE = 3;
	const FOUR_PLUS = 4;

	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}

	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
            self::FOUR_PLUS => '4+',
            self::THREE => '3',
            self::TWO => '2',
			self::ONE => '1',
		];
	}
}
