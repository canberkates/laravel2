<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 11:28
 */

namespace App\Data;


class CreditDebitInvoiceType
{
    const NO = 0;
    const YES_BOTH = 1;
    const ONLY_CREDIT_LINES = 2;
    const ONLY_DEBIT_LINES = 3;

    /**
     * @param int $id The id (see constants)
     * @return null|string Result
     * @see SurveyType::REQUEST
     * @see SurveyType::EXPERIENCE
     */
    public static function name($id) {
        return self::all()[$id] ?? null;
    }

    /**
     * Get all types
     * @return string[]
     */
    public static function all() {
        return [
            self::NO => 'No',
            self::YES_BOTH => 'Yes, both',
            self::ONLY_CREDIT_LINES => 'Yes, only credit lines',
            self::ONLY_DEBIT_LINES => 'Yes, only debit lines'
        ];
    }
}
