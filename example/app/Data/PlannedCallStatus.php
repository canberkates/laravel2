<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 11:27
 */

namespace App\Data;


class PlannedCallStatus
{
	const OPEN = 0;
	const COMPLETED = 1;
	const RESCHEDULED = 2;
	const CANCELLED = 3;

	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}

	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::OPEN => 'Open',
			self::COMPLETED => 'Completed',
			self::RESCHEDULED => 'Rescheduled',
			self::CANCELLED => 'Cancelled'
		];
	}
}
