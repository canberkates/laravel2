<?php


namespace App\Data;

use App;
use Aws\S3\S3Client;
use League\Flysystem\Adapter\Local;
use League\Flysystem\AdapterInterface;
use League\Flysystem\AwsS3v3\AwsS3Adapter;
use League\Flysystem\Filesystem;
use League\Flysystem\FileNotFoundException;

class Spaces
{
    public $filesystem;

    public function __construct($bucket, $public = true)
    {
        // If the environment is local or test
        if (App::environment('local') || App::environment('test'))
        {
            // Get local adapter
            // We don't want to connect to spaces when testing
            $adapter = new Local(env('SHARED_FOLDER') . '/uploads/local/' . $bucket . '/');
        } else
        {
            // Build the connection with spaces
            $client = new S3Client([
                'endpoint' => 'https://' . env('SPACES_REGION') . '.digitaloceanspaces.com/',
                'version' => 'latest',
                'credentials' => [
                    'key' => env('SPACES_KEY'),
                    'secret' => env('SPACES_SECRET')
                ],
                'region' => env('SPACES_REGION'),
            ]);

            // Get the spaces adapter
            $adapter = new AwsS3Adapter($client, $bucket);
        }

        // Create the connection
        $this->filesystem = new Filesystem($adapter, [
            // Set default visibility of the files
            'visibility' => ($public ? AdapterInterface::VISIBILITY_PUBLIC : AdapterInterface::VISIBILITY_PRIVATE)
        ]);

        return $this->filesystem;
    }

    public function write($file, $to = '')
    {

        // Get content of the file
        $contents = file_get_contents($file);

        // If the location where to write to is empty
        if (empty ($to))
            // Write to the root directory with the same name
            $to = basename($file);

        // If the location where we want to write to exists on spaces
        if (!$this->exists($to))
        {

            // Upload the file to spaces
            $this->filesystem->write($to, $contents);
        }
    }

    public function put( $file, $to ) {

        // Get content of the file
        $contents = file_get_contents($file);

        // If the location where to write to is empty
        if (empty ($to))
            // Write to the root directory with the same name
            $to = basename($file);

        // Upload the file to spaces
        $this->filesystem->put($to, $contents);
    }

    public function read( $file ) {
        if ($this->exists($file)) {
            return $this->filesystem->read($file);
        }

        return false;
    }

    public function exists($file)
    {
        // If the file exist on spaces
        return $this->filesystem->has($file);
    }

    public function list($dir = '', $recursive = false)
    {
        // List all files in the given directory
        return $this->filesystem->listContents($dir, $recursive);
    }

    public function move($from, $to)
    {
        // Check if the file that you want to move exists
        // Check if the file does not exist at the place where you want to move it to
        if ($this->exists($from) && !$this->exists($to))
            $this->filesystem->rename($from, $to);
    }

    public function getSize( $file ) {

        try {

            return $this->filesystem->getSize( $file );

        }catch( FileNotFoundException $e ) {

            return 0;
        }
    }
}
