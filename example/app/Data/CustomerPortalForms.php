<?php
/**
 * Created by PhpStorm.
 * User: Lorenzo
 * Date: 09/01/2019
 * Time: 15:39
 */

namespace App\Data;


class CustomerPortalForms{
	
	const GENERAL = "General";
	const FILTERS_REQUESTS = "Filters - Request";
	const FILTERS_CAPPINGS = "Filters - Cappings";
	const ORIGINS = "Origins";
	const DESTINATIONS = "Destinations";
	const FREE_TRIAL = "Free Trial";
	
	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::GENERAL => 'General',
			self::FILTERS_REQUESTS => "Filters - Request",
			self::FILTERS_CAPPINGS => "Filters - Cappings",
			self::ORIGINS => "Origins",
			self::DESTINATIONS => "Destinations",
			self::FREE_TRIAL => "Free Trial"
		];
	}
	
}