<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 5-8-2019
 * Time: 11:28
 */

namespace App\Data;


class PremiumLeadStatus
{
	const MAKE_APPOINTMENT = 0;
	const PLANNED_FOR_SURVEY = 1;
	const READY_FOR_MATCHING = 2;
	
	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::MAKE_APPOINTMENT => 'Make Appointment',
			self::PLANNED_FOR_SURVEY => 'Planned for Survey',
			self::READY_FOR_MATCHING => 'Ready for Matching'
		];
	}
}
