<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 11:42
 */

namespace App\Data;


class CustomerDocumentType
{
	const AGREEMENT = 1;
	const DOCUMENT = 2;
	const CREDIT_CARD_DETAILS = 3;
	const INSURANCE = 4;
	const CHAMBER_OF_COMMERCE = 5;
	const PERMIT_LICENSE = 6;
	
	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::AGREEMENT => 'Agreement',
			self::DOCUMENT => 'Document',
			self::CREDIT_CARD_DETAILS => 'Credit card details',
			self::INSURANCE => 'Insurance',
			self::CHAMBER_OF_COMMERCE => 'Chamber of Commerce',
			self::PERMIT_LICENSE => 'Permit/License'
		];
	}
}