<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 15-8-2019
 * Time: 11:36
 */

namespace App\Data;

class SurveyExecutor
{
	const TRIGLOBAL = 1;
	const BUZZSURVEY = 2;
	
	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::TRIGLOBAL => 'TriGlobal',
			self::BUZZSURVEY => 'BuzzSurvey'
		];
	}
}
