<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 11:46
 */

namespace App\Data;


class StatusHistoryType
{
	const MANUAL = 1;
	const STATUS_UPDATE = 2;
	const FREE_TRIAL_STOP_DATE = 3;
	const FREE_TRIAL_STOP_REQUESTS = 4;
	
	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::MANUAL => 'Manual',
			self::STATUS_UPDATE => 'Status update',
			self::FREE_TRIAL_STOP_DATE => 'Free trial stop - date',
			self::FREE_TRIAL_STOP_REQUESTS => 'Free trial stop - requests'
		];
	}
}