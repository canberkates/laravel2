<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 11:03
 */

namespace App\Data;


class RejectionReason
{
	const OTHER = 0;
	const ONLY_TRANSPORT = 1;
	const NO_REGION_OCCUPANCY_MOVE = 2;
	const NO_REGION_OCCUPANCY_BAGGAGE = 3;
	const INVALID_CONTACT_DETAILS = 4;
	const INVALID_ADDRESS_DETAILS = 5;
	const SAME_LEAD_HAS_BEEN_SENT_TOO_RECENT = 6;
	const MOVING_DATE_TOO_FAR_AWAY = 7;
	const NO_RELOCATION = 8;
	const DOMESTIC_MOVE = 9;
	const DOUBLE_SUBMIT_OF_AFFILIATE = 10;
	const DOUBLE_SUBMIT_OF_OWN_WEBSITE = 11;
	const AFFILIATE_LEAD_BUT_ALREADY_RECEIVED_VIA_OWN_WEBSITE = 12;
	const AFFILIATE_LEAD_BUT_ALREADY_RECEIVED_VIA_OTHER_AFFILIATE = 13;
	const OWN_LEAD_BUT_ALREADY_RECEIVED_VIA_AFFILIATE = 14;
	const OWN_LEAD_BUT_ALREADY_RECEIVED_VIA_OTHER_OWN_WEBSITE = 15;
	const OWN_TEST_LEAD = 16;
	const THIRD_PARTY_TEST_LEAD = 17;
	const EMPTY_LEAD = 18;
	const SPAM_LEAD = 19;
	const FAKE_LEAD = 20;
	const DOUBLE_SUBMIT_WITHIN_24_HOURS = 21;
	const DOUBLE_SUBMIT_BETWEEN_1_AND_30_DAYS = 22;
	
	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::OTHER =>  ["Other" , 1],
			self::ONLY_TRANSPORT => ["Only transport", 1],
			self::NO_REGION_OCCUPANCY_MOVE => ["No region occupancy - Move", 1],
			self::NO_REGION_OCCUPANCY_BAGGAGE => ["No region occupancy - Baggage", 1],
			self::INVALID_CONTACT_DETAILS => ["Invalid contact details", 1],
			self::INVALID_ADDRESS_DETAILS => ["Invalid address details", 1],
			self::SAME_LEAD_HAS_BEEN_SENT_TOO_RECENT => ["Same lead has been sent too recent", 0],
			self::MOVING_DATE_TOO_FAR_AWAY => ["Moving date too far away", 1],
			self::NO_RELOCATION => ["No relocation", 1],
			self::DOMESTIC_MOVE => ["Domestic move", 1],
			self::DOUBLE_SUBMIT_OF_AFFILIATE => ["Double submit of affiliate", 0],
			self::DOUBLE_SUBMIT_OF_OWN_WEBSITE => ["Double submit of own website", 0],
			self::AFFILIATE_LEAD_BUT_ALREADY_RECEIVED_VIA_OWN_WEBSITE => ["Affiliate lead but already received via own website", 0],
			self::AFFILIATE_LEAD_BUT_ALREADY_RECEIVED_VIA_OTHER_AFFILIATE => ["Affiliate lead but already received via other affiliate", 0],
			self::OWN_LEAD_BUT_ALREADY_RECEIVED_VIA_AFFILIATE => ["Own lead but already received via affiliate", 0],
			self::OWN_LEAD_BUT_ALREADY_RECEIVED_VIA_OTHER_OWN_WEBSITE => ["Own lead but already received via other own website", 0],
			self::OWN_TEST_LEAD => ["Own test lead", 1],
			self::THIRD_PARTY_TEST_LEAD => ["3rd party test lead", 1],
			self::EMPTY_LEAD => ["Empty lead", 1],
			self::SPAM_LEAD => ["Spam lead", 1],
			self::FAKE_LEAD => ["Fake lead", 1],
			self::DOUBLE_SUBMIT_WITHIN_24_HOURS => ["Double submit (within 24 hours)", 1],
			self::DOUBLE_SUBMIT_BETWEEN_1_AND_30_DAYS => ["Double submit (between 1 and 30 days)", 1]
		];
	}
}