<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 11:28
 */

namespace App\Data;


class CustomerMarketType
{
	const INT = 1;
	const NAT = 2;
	const INT_NAT = 3;

	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}

	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::INT => 'INT',
			self::NAT => 'NAT',
			self::INT_NAT => 'INT + NAT'
		];
	}
}
