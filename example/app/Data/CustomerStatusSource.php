<?php
/**
 * Created by PhpStorm.
 * User: Lorenzo
 * Date: 09/01/2019
 * Time: 15:47
 */

namespace App\Data;


class CustomerStatusSource
{
	const SALES_INBOX = 1;
	const COLD_CALL = 2;
	const CALLED_IN = 3;
	const CONFERENCE = 4;
	const SIRELO = 5;
	const MAILING_CAMPAIGN = 6;
	const REACTIVATION_PROJECT = 7;
	const REFERRAL = 8;
	
	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::SALES_INBOX => "Sales inbox",
			self::COLD_CALL => "Cold call",
			self::CALLED_IN => "Called in",
			self::CONFERENCE => "Conference",
			self::SIRELO => "Sirelo",
			self::MAILING_CAMPAIGN => "Mailing campaing",
			self::REACTIVATION_PROJECT => "Reactivation project",
			self::REFERRAL => "Referral"
		];
	}
	
}