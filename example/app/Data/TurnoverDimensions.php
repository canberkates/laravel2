<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 11:28
 */

namespace App\Data;


class TurnoverDimensions
{
	const LEDGER_ACCOUNT = 1;
	const PORTAL = 2;
	const TURNOVER_TYPE = 3;
	
	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::LEDGER_ACCOUNT => 'Ledger account',
			self::PORTAL => 'Portal',
			self::TURNOVER_TYPE => 'Turnover type'
		];
	}
}