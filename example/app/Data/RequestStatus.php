<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 11:01
 */

namespace App\Data;


class RequestStatus
{
	const OPEN = 0;
	const MATCHED = 1;
	const REJECTED = 2;
	
	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::OPEN => 'Open',
			self::MATCHED => 'Matched',
			self::REJECTED => 'Rejected'
		];
	}
}