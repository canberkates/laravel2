<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 5-8-2019
 * Time: 13:28
 */

namespace App\Data;


class PremiumLeadResidenceType
{
	const EMPTY = 0;
	const APARTMENT_FLAT = 1;
	const DETACHED_HOUSE = 2;
	const SEMI_DETACHED_HOUSE = 3;
	const TERRACED_HOUSE = 4;
	const COTTAGE_BUNGALOW = 5;
	
	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::EMPTY => '-',
			self::APARTMENT_FLAT => 'Apartment/Flat',
			self::DETACHED_HOUSE => 'Detached house',
			self::SEMI_DETACHED_HOUSE => 'Semi-detached house',
			self::TERRACED_HOUSE => 'Terraced house',
			self::COTTAGE_BUNGALOW => 'Cottage/Bungalow'
		];
	}
}
