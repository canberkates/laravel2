<?php
/**
 * Created by PhpStorm.
 * User: Lorenzo
 * Date: 09/01/2019
 * Time: 15:47
 */

namespace App\Data;


class DebtorStatus
{
	const EMPTY = 0;
	const POTENTIAL_WRITE_OFF = 1;
	const DEBT_WRITTEN_OFF = 2;
	
	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::EMPTY => "[Empty]",
			self::POTENTIAL_WRITE_OFF => "Potential write-off",
			self::DEBT_WRITTEN_OFF => "Debt written-off"
		];
	}
	
	
}