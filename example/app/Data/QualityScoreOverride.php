<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 11:39
 */

namespace App\Data;


class QualityScoreOverride
{
	const DISABLED = 0;
	const QUALITY_SCORE_1 = 1;
	const QUALITY_SCORE_2 = 2;
	const QUALITY_SCORE_3 = 3;
	const QUALITY_SCORE_4 = 4;
	const QUALITY_SCORE_5 = 5;
	const QUALITY_SCORE_6 = 6;
	const QUALITY_SCORE_7 = 7;
	const QUALITY_SCORE_8 = 8;
	const QUALITY_SCORE_9 = 9;
	const QUALITY_SCORE_10 = 10;
	const EXCLUDED_ONLY_TAKE_OPEN_SPOTS = -1;
	const EXCLUDED_ONLY_MATCH_POSSIBLE = -2;


	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}

	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::DISABLED => 'Disabled',
			self::QUALITY_SCORE_1 => "1 (No bonus for a low Average Match)",
			/*self::QUALITY_SCORE_2 => 2,
			self::QUALITY_SCORE_3 => 3,
			self::QUALITY_SCORE_4 => 4,
			self::QUALITY_SCORE_5 => 5,
			self::QUALITY_SCORE_6 => 6,
			self::QUALITY_SCORE_7 => 7,
			self::QUALITY_SCORE_8 => 8,
			self::QUALITY_SCORE_9 => 9,
			self::QUALITY_SCORE_10 => 10,*/
			self::EXCLUDED_ONLY_TAKE_OPEN_SPOTS => "Excluded (only take open spots)",
			self::EXCLUDED_ONLY_MATCH_POSSIBLE => 'Excluded (only match possible)'
		];
	}
}
