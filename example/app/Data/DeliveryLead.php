<?php

namespace App\Data;

use App\Models\Country;
use App\Models\MoverRequest;
use App\Models\Region;
use App\Models\Request;

class DeliveryLead
{

    private function __construct() {}

    private $lead_type;
    private $re_id;
    private $more_id;
    private $timestamp;
    private $source;
    private $la_code;
    private $destination_type;
    private $request_type;
    private $moving_size;
    private $room_size;
    private $room_size_more;
    private $moving_date;
    private $volume_m3;
    private $volume_ft3;
    private $volume_calculator;
    private $voca_id;
    private $storage;
    private $packing;
    private $assembly;
    private $business;
    private $handyman;
    private $item_disposal;
    private $street_from;
    private $zipcode_from;
    private $city_from;
    private $co_code_from;
    private $reg_id_from;
    private $residence_from;
    private $floor_type_from;
    private $walking_distance_from;
    private $elevator_available_from;
    private $parking_permit_needed_from;
    private $street_to;
    private $zipcode_to;
    private $city_to;
    private $co_code_to;
    private $reg_id_to;
    private $residence_to;
    private $floor_type_to;
    private $walking_distance_to;
    private $elevator_available_to;
    private $parking_permit_needed_to;
    private $company_name;
    private $first_name;
    private $last_name;
    private $full_name;
    private $telephone1;
    private $telephone2;
    private $email;
    private $remarks;
    private $ip_address;
    private $contact_method;
    private $price_estimation_low;
    private $price_estimation_high;


    public static function createFromRequest(Request $request){

        $lead = new DeliveryLead();

        $lead->lead_type = 1;
        $lead->re_id = $request->re_id;
        $lead->timestamp = $request->re_timestamp;
        $lead->source = $request->re_source;
        $lead->la_code = $request->re_la_code;
        $lead->destination_type = $request->re_destination_type;
        $lead->request_type = $request->re_request_type;
        $lead->moving_size = $request->re_moving_size;
        $lead->room_size = $request->re_room_size;
        $lead->room_size_more = $request->re_room_size_more;
        $lead->moving_date = $request->re_moving_date;
        $lead->volume_m3 = $request->re_volume_m3;
        $lead->volume_ft3 = $request->re_volume_ft3;
        $lead->volume_calculator = $request->re_volume_calculator;
        $lead->voca_id = $request->re_voca_id;
        $lead->storage = $request->re_storage;
        $lead->packing = $request->re_packing;
        $lead->assembly = $request->re_assembly;
        $lead->business = $request->re_business;
        $lead->street_from = $request->re_street_from;
        $lead->zipcode_from = $request->re_zipcode_from;
        $lead->city_from = $request->re_city_from;
        $lead->co_code_from = $request->re_co_code_from;
        $lead->reg_id_from = $request->re_reg_id_from;
        $lead->residence_from = $request->re_residence_from;
        $lead->street_to = $request->re_street_to;
        $lead->zipcode_to = $request->re_zipcode_to;
        $lead->city_to = $request->re_city_to;
        $lead->co_code_to = $request->re_co_code_to;
        $lead->reg_id_to = $request->re_reg_id_to;
        $lead->residence_to = $request->re_residence_to;
        $lead->company_name = $request->re_company_name;
        $lead->first_name = $request->re_first_name;
        $lead->last_name = $request->re_family_name;
        $lead->full_name = $request->re_full_name;
        $lead->telephone1 = $request->re_telephone1;
        $lead->telephone2 = $request->re_telephone2;
        $lead->email = $request->re_email;
        $lead->remarks = $request->re_remarks;
        $lead->ip_address = $request->re_ip_address;

        return $lead;
    }

    public static function createFromMoverRequest(MoverRequest $mover_request){
        $lead = new DeliveryLead();

        $lead->lead_type = 2;
        $lead->more_id = $mover_request->more_id;
        $lead->timestamp = $mover_request->more_timestamp;
        $lead->la_code = $mover_request->more_la_code;
        $lead->moving_date = $mover_request->more_moving_date;
        $lead->volume_m3 = $mover_request->more_volume_m3;
        $lead->volume_ft3 = $mover_request->more_volume_ft3;
        $lead->voca_id = $mover_request->more_voca_id;
        $lead->storage = $mover_request->more_storage;
        $lead->packing = $mover_request->more_packing;
        $lead->assembly = $mover_request->more_assembly;
        $lead->handyman = $mover_request->more_handyman;
        $lead->item_disposal = $mover_request->more_item_disposal;
        $lead->street_from = $mover_request->more_street_from;
        $lead->zipcode_from = $mover_request->more_zipcode_from;
        $lead->city_from = $mover_request->more_city_from;
        $lead->co_code_from = $mover_request->more_co_code_from;
        $lead->floor_type_from = $mover_request->more_floor_type_from;
        $lead->walking_distance_from = $mover_request->more_walking_distance_from;
        $lead->elevator_available_from = $mover_request->more_elevator_available_from;
        $lead->parking_permit_needed_from = $mover_request->more_parking_permit_needed_from;
        $lead->street_to = $mover_request->more_street_to;
        $lead->zipcode_to = $mover_request->more_zipcode_to;
        $lead->city_to = $mover_request->more_city_to;
        $lead->co_code_to = $mover_request->more_co_code_to;
        $lead->floor_type_to = $mover_request->more_floor_type_to;
        $lead->walking_distance_to = $mover_request->more_walking_distance_to;
        $lead->elevator_available_to = $mover_request->more_elevator_available_to;
        $lead->parking_permit_needed_to = $mover_request->more_parking_permit_needed_to;
        $lead->full_name = $mover_request->more_full_name;
        $lead->telephone1 = $mover_request->more_telephone_1;
        $lead->email = $mover_request->more_email;
        $lead->remarks = $mover_request->more_service_remarks;
        $lead->ip_address = $mover_request->more_ip_address;
        $lead->contact_method = $mover_request->more_contact_method;
        $lead->price_estimation_low = $mover_request->more_price_estimation_low;
        $lead->price_estimation_high = $mover_request->more_price_estimation_high;

        return $lead;
    }



    /**
     * @return mixed
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @return mixed
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @return mixed
     */
    public function getLaCode()
    {
        return $this->la_code;
    }

    /**
     * @return mixed
     */
    public function getDestinationType()
    {
        return $this->destination_type;
    }

    /**
     * @return mixed
     */
    public function getRequestType()
    {
        return $this->request_type;
    }

    /**
     * @return mixed
     */
    public function getMovingSize()
    {
        return $this->moving_size;
    }

    /**
     * @return mixed
     */
    public function getRoomSize()
    {
        return $this->room_size;
    }

    /**
     * @return mixed
     */
    public function getRoomSizeMore()
    {
        return $this->room_size_more;
    }

    /**
     * @return mixed
     */
    public function getMovingDate()
    {
        return $this->moving_date;
    }

    /**
     * @return mixed
     */
    public function getVolumeM3()
    {
        return $this->volume_m3;
    }

    /**
     * @return mixed
     */
    public function getVolumeFt3()
    {
        return $this->volume_ft3;
    }

    /**
     * @return mixed
     */
    public function getVolumeCalculator()
    {
        return $this->volume_calculator;
    }

    /**
     * @return mixed
     */
    public function getVocaId()
    {
        return $this->voca_id;
    }

    /**
     * @return mixed
     */
    public function getStorage()
    {
        return $this->storage;
    }

    /**
     * @return mixed
     */
    public function getPacking()
    {
        return $this->packing;
    }

    /**
     * @return mixed
     */
    public function getAssembly()
    {
        return $this->assembly;
    }

    /**
     * @return mixed
     */
    public function getBusiness()
    {
        return $this->business;
    }

    /**
     * @return mixed
     */
    public function getStreetFrom()
    {
        return $this->street_from;
    }

    /**
     * @return mixed
     */
    public function getZipcodeFrom()
    {
        return $this->zipcode_from;
    }

    /**
     * @return mixed
     */
    public function getCityFrom()
    {
        return $this->city_from;
    }

    /**
     * @return mixed
     */
    public function getCoCodeFrom()
    {
        return $this->co_code_from;
    }

    /**
     * @return mixed
     */
    public function getRegIdFrom()
    {
        return $this->reg_id_from;
    }

    /**
     * @return mixed
     */
    public function getResidenceFrom()
    {
        return $this->residence_from;
    }

    /**
     * @return mixed
     */
    public function getStreetTo()
    {
        return $this->street_to;
    }

    /**
     * @return mixed
     */
    public function getZipcodeTo()
    {
        return $this->zipcode_to;
    }

    /**
     * @return mixed
     */
    public function getCityTo()
    {
        return $this->city_to;
    }

    /**
     * @return mixed
     */
    public function getCoCodeTo()
    {
        return $this->co_code_to;
    }

    /**
     * @return mixed
     */
    public function getRegIdTo()
    {
        return $this->reg_id_to;
    }

    /**
     * @return mixed
     */
    public function getResidenceTo()
    {
        return $this->residence_to;
    }

    /**
     * @return mixed
     */
    public function getCompanyName()
    {
        return $this->company_name;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * @return mixed
     */
    public function getFullName()
    {
        return $this->full_name;
    }

    /**
     * @return mixed
     */
    public function getTelephone1()
    {
        return $this->telephone1;
    }

    /**
     * @return mixed
     */
    public function getTelephone2()
    {
        return $this->telephone2;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getRemarks()
    {
        return $this->remarks;
    }

    /**
     * @return mixed
     */
    public function getIpAddress()
    {
        return $this->ip_address;
    }

    /**
     * @return mixed
     */
    public function getHandyman()
    {
        return $this->handyman;
    }


    /**
     * @return mixed
     */
    public function getItemDisposal()
    {
        return $this->item_disposal;
    }

    /**
     * @return mixed
     */
    public function getFloorTypeFrom()
    {
        return $this->floor_type_from;
    }

    /**
     * @return mixed
     */
    public function getWalkingDistanceFrom()
    {
        return $this->walking_distance_from;
    }

    /**
     * @return mixed
     */
    public function getElevatorAvailableFrom()
    {
        return $this->elevator_available_from;
    }

    /**
     * @return mixed
     */
    public function getParkingPermitNeededFrom()
    {
        return $this->parking_permit_needed_from;
    }

    /**
     * @return mixed
     */
    public function getFloorTypeTo()
    {
        return $this->floor_type_to;
    }

    /**
     * @return mixed
     */
    public function getWalkingDistanceTo()
    {
        return $this->walking_distance_to;
    }

    /**
     * @return mixed
     */
    public function getElevatorAvailableTo()
    {
        return $this->elevator_available_to;
    }

    /**
     * @return mixed
     */
    public function getParkingPermitNeededTo()
    {
        return $this->parking_permit_needed_to;
    }

    /**
     * @return mixed
     */
    public function getContactMethod()
    {
        return $this->contact_method;
    }

    /**
     * @return mixed
     */
    public function getPriceEstimationLow()
    {
        return $this->price_estimation_low;
    }

    /**
     * @return mixed
     */
    public function getPriceEstimationHigh()
    {
        return $this->price_estimation_high;
    }

    /**
     * @return mixed
     */
    public function getReId()
    {
        return $this->re_id;
    }

    /**
     * @return mixed
     */
    public function getMoreId()
    {
        return $this->more_id;
    }

    public function getRegion($direction): string
    {
        $region = Region::select("reg_co_code", "reg_parent", "reg_name")->where("reg_id", $this->{"getRegId".ucfirst($direction)}())->first();

        if(!empty($region))
        {
            if($region->reg_co_code == "US" && !empty($region->reg_parent))
            {
                if (!empty($region->reg_name))
                {
                    return $region->reg_parent.", ".$region->reg_name;
                }
                return $region->reg_parent;

            }
            return ((!empty($region->reg_name)) ? $region->reg_name : $region->reg_parent);
        }
        return false;
    }

    public function getCountry($direction, $language = null): string
    {
        $country_column = "co_".strtolower($language);

        $country_code = Country::select($country_column)->where("co_code", $this->{"getCoCode".ucfirst($direction)}())->first();

        if(!empty($country_code))
        {
            return $country_code->$country_column;
        }
        else
        {
            return false;
        }
    }

    /**
     * @param mixed $street_from
     */
    public function setStreetFrom($street_from): void
    {
        $this->street_from = $street_from;
    }

    /**
     * @param mixed $volume_m3
     */
    public function setVolumeM3($volume_m3): void
    {
        $this->volume_m3 = $volume_m3;
    }

    /**
     * @return mixed
     */
    public function getLeadType()
    {
        return $this->lead_type;
    }

    /**
     * @param mixed $telephone1
     */
    public function setTelephone1($telephone1): void
    {
        $this->telephone1 = $telephone1;
    }
}
