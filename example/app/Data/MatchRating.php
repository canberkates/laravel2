<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 11:05
 */

namespace App\Data;


class MatchRating
{
	const UNRATED = 0;
	const GOOD = 1;
	const MINOR_MISTAKE = 2;
	const MAJOR_MISTAKE = 3;
	const REMARKS = 4;
	
	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::UNRATED => 'Unrated',
			self::GOOD => 'Good',
			self::MINOR_MISTAKE => 'Minor mistake',
			self::MAJOR_MISTAKE => 'Major mistake',
			self::REMARKS => 'Remarks'
		];
	}
}