<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 11:28
 */

namespace App\Data;


class CustomerStatusType
{
	const PARTNER = 1;
	const PROSPECT = 2;
	const IN_CONVERSATION = 3;
	const WAITING_LIST = 4;
	const DEBT_COLLECTOR = 5;
	const EXCLUDED = 6;

	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}

	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::PARTNER => 'Partner',
			self::PROSPECT => 'Prospect',
			self::IN_CONVERSATION => 'In Conversation',
			self::WAITING_LIST => 'Waiting List',
			self::DEBT_COLLECTOR => 'Debt Collector',
			self::EXCLUDED => 'Excluded',
		];
	}
}
