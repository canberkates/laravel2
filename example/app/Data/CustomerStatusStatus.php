<?php
/**
 * Created by PhpStorm.
 * User: Lorenzo
 * Date: 09/01/2019
 * Time: 15:47
 */

namespace App\Data;


class CustomerStatusStatus
{
	const NEW_BOOKING = 1;
	const UPSELL = 2;
	const REACTIVATION = 3;
	const PAUSE = 4;
	const UNPAUSE = 5;
	const CREDITHOLD = 6;
	const OUT_OF_CREDITHOLD = 7;
	const CANCELLATION = 8;
	const DOWNSELL = 9;
	
	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::NEW_BOOKING => "New booking",
			self::UPSELL => "Upsell",
			self::REACTIVATION => "Reactivation",
			self::PAUSE => "Pause",
			self::UNPAUSE => "Unpause",
			self::CREDITHOLD => "Credithold",
			self::OUT_OF_CREDITHOLD => "Out of credithold",
			self::CANCELLATION => "Cancellation",
			self::DOWNSELL => "Downsell"
		];
	}
	
}