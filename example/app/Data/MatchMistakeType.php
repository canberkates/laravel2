<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 11:16
 */

namespace App\Data;


class MatchMistakeType
{
	const UNRATED = 0;
	const GOOD = 1;
	const MINOR_MISTAKE = 2;
	const MAJOR_MISTAKE = 3;
	const ADDRESS_DETAILS_1 = 4;
	const ADDRESS_DETAILS_2 = 5;
	const INVALID_PHONE_NUMBER = 6;
	const INVALID_EMAIL_ADDRESS = 7;
	const MOVING_SIZE_BAGGAGE_1 = 8;
	const MOVING_SIZE_BAGGAGE_2 = 9;
	const MOVING_SIZE_MOVE = 10;
	const BOXES_ONLY_BAGGAGE = 16;
	const SHOULD_HAVE_MATCHED_REJECTED = 11;
	const WRONG_REJECTION_REASON = 12;
	const SHOULD_HAVE_CALLED_PUT_ON_HOLD = 13;
	const A_TRANSPORT = 17;
	const OTHER_MAJOR = 14;
	const OTHER_MINOR = 15;
	const REMARK = 18;
	
	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::UNRATED => ['Unrated', 0],
			self::GOOD => ['Good', 1],
			self::MINOR_MISTAKE => ["Minor mistake", 2],
			self::MAJOR_MISTAKE => ["Major mistake", 3],
			self::ADDRESS_DETAILS_1 => ["Address details", 2],
			self::ADDRESS_DETAILS_2 => ["Address details", 2],
			self::INVALID_PHONE_NUMBER => ["Invalid phone number", 3],
			self::INVALID_EMAIL_ADDRESS => ["Invalid email address", 3],
			self::MOVING_SIZE_BAGGAGE_1 => ["Moving size (Baggage)", 3],
			self::MOVING_SIZE_BAGGAGE_2 => ["Moving size (Baggage)", 3],
			self::MOVING_SIZE_MOVE => ["Moving size (Move)", 2],
			self::BOXES_ONLY_BAGGAGE => ["Boxes only - Baggage", 2],
			self::SHOULD_HAVE_MATCHED_REJECTED => ["Should have matched/rejected", 3],
			self::WRONG_REJECTION_REASON => ["Wrong rejection reason", 2],
			self::SHOULD_HAVE_CALLED_PUT_ON_HOLD => ["Should have called / put on hold", 2],
			self::A_TRANSPORT => ["A transport", 3],
			self::OTHER_MAJOR => ["Other Major", 3],
			self::OTHER_MINOR => ["Other Minor", 2],
			self::REMARK => ["Remark", 4]
		];
	}
}