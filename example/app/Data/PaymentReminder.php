<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 11:43
 */

namespace App\Data;


class PaymentReminder
{
	const NONE = 0;
	const PAYMENT_REMINDER = 1;
	const DEMAMD_FOR_PAYMENT_1 = 2;
	const DEMAMD_FOR_PAYMENT_2 = 3;
	const NOTICE_OF_DEFAULT = 4;
	const COLLECTION_AGENCY = 5;
	
	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::NONE => 'None',
			self::PAYMENT_REMINDER => 'Payment reminder',
			self::DEMAMD_FOR_PAYMENT_1 => 'Demand for payment #1',
			self::DEMAMD_FOR_PAYMENT_2 => 'Demand for payment #2',
			self::NOTICE_OF_DEFAULT => 'Notice of default',
			self::COLLECTION_AGENCY => 'Collection agency'
		];
	}
}