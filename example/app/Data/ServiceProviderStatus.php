<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 10:57
 */

namespace App\Data;


class ServiceProviderStatus
{
	const SP_ACTIVE = 1;
	const SP_PAUSE = 2;
	const SP_INACTIVE = 3;
	const SP_CREDIT_HOLD = 4;
	const SP_DEBT_COLLECTOR = 5;
	
	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::SP_ACTIVE => 'SP - Active',
			self::SP_PAUSE => 'SP - Pause',
			self::SP_INACTIVE => 'SP - Inactive',
			self::SP_CREDIT_HOLD => 'SP - Credit hold',
			self::SP_DEBT_COLLECTOR => 'SP - Debt collector'
		];
	}
}