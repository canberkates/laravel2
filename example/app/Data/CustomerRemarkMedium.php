<?php
/**
 * Created by PhpStorm.
 * User: Lorenzo
 * Date: 09/01/2019
 * Time: 15:47
 */

namespace App\Data;


class CustomerRemarkMedium
{
	const OTHER = 0;
	const PHONE = 1;
	const EMAIL = 2;
	const MEETING = 3;
	const SOCIAL_MEDIA = 4;
	
	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::OTHER => "Other",
			self::PHONE => "Phone",
			self::EMAIL => "E-mail",
			self::MEETING => "Meeting",
			self::SOCIAL_MEDIA => "Social media"
		];
	}
	
}