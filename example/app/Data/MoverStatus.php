<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 10:50
 */

namespace App\Data;


class MoverStatus
{
	const MV_ACTIVE = 1;
	const MV_FREE_TRIAL = 2;
	const MV_PAUSE = 3;
	const MV_CANCELLED = 4;
	const MV_PROSPECT = 5;
	const MV_CREDIT_HOLD = 6;
	const MV_DEBT_COLLECTOR = 7;
	const MV_DELETED = 8;
	
	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::MV_ACTIVE => 'MV - Active',
			self::MV_FREE_TRIAL => 'MV - Free trial',
			self::MV_PAUSE => 'MV - Pause',
			self::MV_CANCELLED => 'MV - Cancelled',
			self::MV_PROSPECT => 'MV - Prospect',
			self::MV_CREDIT_HOLD => 'MV - Credit hold',
			self::MV_DEBT_COLLECTOR => 'MV - Debt collector',
			self::MV_DELETED => 'MV - Deleted'
		];
	}
}