<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 20-8-2019
 * Time: 10:50
 */

namespace App\Data;


class ISO
{
	const ISO_9001_QUALITY_MANAGEMENT = 1;
	const ISO_14001_ENVIRONMENTAL_MANAGEMENT = 2;
	const ISO_45001_OCCUPATIONAL_HEALTH_AND_SAFETY = 3;
	const ISO_27001_INFORMATION_SECURITY_MANAGEMENT = 4;
	
	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::ISO_9001_QUALITY_MANAGEMENT => 'ISO 9001 - Quality management',
			self::ISO_14001_ENVIRONMENTAL_MANAGEMENT => 'ISO 14001 - Environmental management',
			self::ISO_45001_OCCUPATIONAL_HEALTH_AND_SAFETY => 'ISO 45001 - Occupational health & safety',
			self::ISO_27001_INFORMATION_SECURITY_MANAGEMENT => 'ISO 27001 - Information security management'
		];
	}
}
