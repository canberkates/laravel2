<?php
/**
 * Created by PhpStorm.
 * User: Arjan de Coninck
 * Date: 30-3-2020
 * Time: 09:36
 */

namespace App\Data;


class PaymentType
{
    const POSTPAYMENT = 1;
    const PREPAYMENT_WAIT_FOR_PARTNER = 2;
    const PREPAYMENT_CHARGE_MONTHLY_BUDGET = 3;

    /**
     * @param int $id The id (see constants)
     * @return null|string Result
     * @see SurveyType::REQUEST
     * @see SurveyType::EXPERIENCE
     */
    public static function name($id) {
        return self::all()[$id] ?? null;
    }

    /**
     * Get all types
     * @return string[]
     */
    public static function all() {
        return [
            self::POSTPAYMENT => ["Post-payment" , 'post'],
            self::PREPAYMENT_WAIT_FOR_PARTNER => ["Prepayment: wait for partner to pay", 'pre'],
            self::PREPAYMENT_CHARGE_MONTHLY_BUDGET => ["Prepayment: charge monthly budget", 'pre'],
        ];
    }
}