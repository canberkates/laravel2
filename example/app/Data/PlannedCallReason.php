<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 11:27
 */

namespace App\Data;


class PlannedCallReason
{
	const THIRTY_DAY_CHECK = 1;
	const NINETY_DAY_CHECK = 2;
	const SIX_MONTHS_FOLLOW_UP = 3;
	const SALES_FOLLOW_UP = 4;
	const FINANCE_CHECK = 5;
	const CONFERENCE_FOLLOW_UP = 6;
	const REACTIVATION_FOLLOW_UP = 7;
	const COLD_CALL = 8;
	const SIRELO_CALL = 9;

	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 */

	public static function name($id) {
		return self::all()[$id] ?? null;
	}

	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::THIRTY_DAY_CHECK => '30 days check',
			self::NINETY_DAY_CHECK => '90 days check',
			self::SIX_MONTHS_FOLLOW_UP => '6 months follow-up',
			self::SALES_FOLLOW_UP => 'Sales follow-up',
			self::FINANCE_CHECK => 'Finance check',
			self::CONFERENCE_FOLLOW_UP => 'Conference follow-up',
			self::REACTIVATION_FOLLOW_UP => 'Reactivation follow-up',
			self::COLD_CALL => 'Cold call',
			self::SIRELO_CALL => 'Sirelo call'
		];
	}
}
