<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 11-3-2019
 * Time: 12:05
 */

namespace App\Data;


class ContactPersonDepartmentRole
{
	const UNKNOWN = 0;
	const CEO_OWNER = 1;
	const MANAGEMENT = 2;
	const EMPLOYEE = 3;
	
	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::UNKNOWN => "Unknown",
			self::CEO_OWNER => "CEO/Owner",
			self::MANAGEMENT => "Management",
			self::EMPLOYEE => "Employee"
		];
	}
	
}