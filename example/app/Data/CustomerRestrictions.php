<?php
/**
 * Created by PhpStorm.
 * User: Lorenzo
 * Date: 09/01/2019
 * Time: 15:39
 */

namespace App\Data;


class CustomerRestrictions{

    const MARIO = 4710;
    const RESTRICTED_USER = 4686;

    /**
     * @param int $id The id (see constants)
     * @return null|string Result
     * @see SurveyType::REQUEST
     * @see SurveyType::EXPERIENCE
     */
    public static function name($id) {
        return self::all()[$id] ?? null;
    }

    /**
     * Get all types
     * @return string[]
     */
    public static function all() {
        return [
            self::MARIO => ["MX", "PE", "AR", "CL", "CO", "PR"],
            self::RESTRICTED_USER => ["NL"]
        ];
    }

}
