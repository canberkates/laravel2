<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 11:03
 */

namespace App\Data;


class AffiliatePartnerRejectionReason
{
	const OTHER = 0;
	const DOMESTIC_MOVE = 1;
	const DOUBLE_SUBMIT = 2;
	const ALREADY_RECEIVED_AN_IDENTICAL_REQUEST = 3;
	const INVALID_CONTACT_DETAILS = 4;
	const INVALID_ADDRESS_DETAILS = 5;
	const NO_RELOCATION = 6;
	const ALREADY_RECEIVED_VIA_OWN_CHANNELS = 7;
	const FAKE_LEAD = 8;
	const EMPTY_LEAD = 9;
	const SPAM_LEAD = 10;
	const TEST_LEAD = 11;
	const INVALID_MOVING_DATE = 12;
	const OWN_LEAD_BUT_ALREADY_RECEIVED_VIA_AFFILIATE = 13;
	const OWN_LEAD_BUT_ALREADY_RECEIVED_VIA_OTHER_OWN_WEBSITE = 14;
	
	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::OTHER =>  "Other",
			self::DOMESTIC_MOVE => "Domestic move",
			self::DOUBLE_SUBMIT => "Double submit",
			self::ALREADY_RECEIVED_AN_IDENTICAL_REQUEST => "Already received an identical request",
			self::INVALID_CONTACT_DETAILS => "Invalid contact details",
			self::INVALID_ADDRESS_DETAILS => "Invalid address details",
			self::NO_RELOCATION => "No relocation",
			self::ALREADY_RECEIVED_VIA_OWN_CHANNELS => "Already received via own channels",
			self::FAKE_LEAD => "Fake lead",
			self::EMPTY_LEAD => "Empty lead",
			self::SPAM_LEAD => "Spam lead",
			self::TEST_LEAD => "Test lead",
			self::INVALID_MOVING_DATE => "Invalid moving date",
			self::OWN_LEAD_BUT_ALREADY_RECEIVED_VIA_AFFILIATE => "Own lead but already received via affiliate",
			self::OWN_LEAD_BUT_ALREADY_RECEIVED_VIA_OTHER_OWN_WEBSITE => "Own lead but already received via other own website",
		];
	}
}