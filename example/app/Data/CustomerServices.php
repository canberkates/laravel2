<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 11:28
 */

namespace App\Data;


class CustomerServices
{
	const REMOVALS = 1;
	const BAGGAGE = 2;
	const FULL_PACKAGE = 3;

	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}

	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::REMOVALS => 'Removals',
			self::BAGGAGE => 'Baggage',
			self::FULL_PACKAGE => 'Full Package'
		];
	}
}
