<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 12:22
 */

namespace App\Data;


class Survey2Source
{
	const REQUEST = 1;
	const WEBSITE_REVIEW = 2;
	
	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}
	
	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::REQUEST => 'Request',
			self::WEBSITE_REVIEW => 'Website review'
		];
	}
}