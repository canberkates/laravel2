<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 11:28
 */

namespace App\Data;


class CustomerSourceType
{
	const REFERRAL = 1;
	const MAILING_CAMPAIGN = 2;
	const PERSONAL_EMAIL = 3;
	const SIRELO = 4;
	const CONFERENCE = 5;
	const COLD_CALL = 6;
	const WEBSITE_FORM_CHAT = 7;
	const CALLED_IN = 8;

	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 * @see SurveyType::REQUEST
	 * @see SurveyType::EXPERIENCE
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}

	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::REFERRAL => 'Referral',
			self::MAILING_CAMPAIGN => 'Mailing Campaign',
			self::PERSONAL_EMAIL => 'Personal Email',
			self::SIRELO => 'Sirelo',
			self::CONFERENCE => 'Conference',
			self::COLD_CALL => 'Cold call',
			self::WEBSITE_FORM_CHAT => 'Website (Form - Chat)',
			self::CALLED_IN => 'Called in'
		];
	}
}
