<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 25-1-2019
 * Time: 13:35
 */

namespace App\Data;


class WebsiteValidateString
{
    const NO_URLS_KNOWN = 1;
    const ERP_BEST_NO_GOOGLE_URL = 2;
    const ERP_REDIRECT_NO_GOOGLE_URL = 3;
    const ERP_REDIRECT_OTHER_DOMAIN_NO_GOOGLE_URL = 4;
    const ERP_NOT_WORKING_NO_GOOGLE_URL = 5;
    const NO_ERP_URL_GOOGLE_BEST = 6;
    const NO_ERP_URL_GOOGLE_REDIRECT = 7;
    const NO_ERP_URL_GOOGLE_REDIRECT_OTHER_DOMAIN = 8;
    const NO_ERP_URL_GOOGLE_NOT_WORKING = 9;
    const ERP_GOOGLE_THE_SAME = 10;
    const ERP_GOOGLE_THE_SAME_REDIRECT = 11;
    const ERP_GOOGLE_THE_SAME_REDIRECT_OTHER_DOMAIN = 12;
    const ERP_GOOGLE_THE_SAME_NOT_WORKING = 13;
    const ERP_NOT_WORKING_GOOGLE_BEST = 14;
    const ERP_NOT_WORKING_GOOGLE_REDIRECT = 15;
    const ERP_NOT_WORKING_GOOGLE_REDIRECT_OTHER_DOMAIN = 16;
    const ERP_BEST_GOOGLE_NOT_WORKING = 17;
    const ERP_REDIRECT_GOOGLE_NOT_WORKING = 18;
    const ERP_REDIRECT_ANOTHER_DOMAIN_GOOGLE_NOT_WORKING = 19;
    const ERP_GOOGLE_NOT_VALID = 20;
    const ERP_GOOGLE_DOMAIN_CHANGED = 21;
    const ERP_DOMAIN_CHANGED_GOOGLE_FINAL = 22;
    const ERP_FINAL_GOOGLE_REDIRECT = 23;
    const BOTH_GOOD_FINAL = 24;
    const ERP_REDIRECT_GOOGLE_BEST = 25;
    const ERP_BEST_GOOGLE_REDIRECT = 26;
    const ERP_REDIRECT_GOOGLE_REDIRECT = 27;
    const ERP_OTHER_DOMAIN_THAN_GOOGLE = 28;
    const ERP_GOOD_GOOGLE_GOOD = 29;
    const ERP_GOOD_GOOGLE_REDIRECT = 30;
    const ERP_REDIRECT_GOOGLE_GOOD = 31;
    const REDIRECTED_BOTH = 32;

    const ERP_BEST_GOOGLE_REDIRECTED = 222;

    /**
     * @param int $id The id (see constants)
     * @return null|string Result
     * @see SurveyType::REQUEST
     * @see SurveyType::EXPERIENCE
     */
    public static function name($id) {
        return self::all()[$id] ?? null;
    }

    /**
     * Get all types
     * @return string[]
     */
    public static function all() {
        return [
            self::NO_URLS_KNOWN => '<div class="col-sm-12">There is no URLs known for this company. Validate this company manually!</div>',
            self::ERP_BEST_NO_GOOGLE_URL => '<div class="col-sm-6" style="color:green;">Current ERP URL is the best one :)<span id="final_domain" style="display:none;" data-domain="%s"></span></div><div class="col-sm-6">No Google URL scraped.</div>',
            self::ERP_REDIRECT_NO_GOOGLE_URL => '<div class="col-sm-6">There is a redirect in the current ERP url. The best URL is: %s<span id="validated_url_erp" data-url="%s">✍</span><span id="final_domain_erp" style="display:none;" data-domain="%s"></span></div><div class="col-sm-6">No Google URL.</div>',
            self::ERP_REDIRECT_OTHER_DOMAIN_NO_GOOGLE_URL => '<div class="col-sm-6">There is found a redirect to another domain. Redirected URL is: %s<span id="validated_url_erp" data-url="%s">✍</span><span id="final_domain_erp" style="display:none;" data-domain="%s"></span></div><div class="col-sm-6">No Google URL.</div>',
            self::ERP_NOT_WORKING_NO_GOOGLE_URL => '<div class="col-sm-12">Current ERP URL is not working. There is also no URL which we received from Google. Validate this company manually!</div>',
            self::NO_ERP_URL_GOOGLE_BEST => '<div class="col-sm-6">No ERP URL known.</div><div class="col-sm-6" style="color:green;">Current Google URL is the best one :)<span id="final_domain_other" style="display:none;" data-domain="%s"></span></div>',
            self::NO_ERP_URL_GOOGLE_REDIRECT => '<div class="col-sm-6">No ERP URL.</div><div class="col-sm-6">There is a redirect in the scraped Google url. The best URL is: %s<span id="validated_url_other" data-url="%s">✍</span><span id="final_domain_other" style="display:none;" data-domain="%s"></span></div>',
            self::NO_ERP_URL_GOOGLE_REDIRECT_OTHER_DOMAIN => '<div class="col-sm-6">No ERP URL.</div><div class="col-sm-6">There is found a redirect to another domain. Redirected URL is: %s<span id="validated_url_other" data-url="%s">✍</span><span id="final_domain_other" style="display:none;" data-domain="%s"></span></div>',
            self::NO_ERP_URL_GOOGLE_NOT_WORKING => '<div class="col-sm-12">Scraped Google URL is not working. There is also no URL found in the ERP for this customer. Validate this company manually!</div>',
            self::ERP_GOOGLE_THE_SAME => '<div class="col-sm-12" style="color:green;">ERP and Google URL are the same. The current URLs are the best URLs<span id="final_domain" style="display:none;" data-domain="%s"></span></div>',
            self::ERP_GOOGLE_THE_SAME_REDIRECT => '<div class="col-sm-12">ERP and Google URL are the same. There is a redirect in the URL. The best URL is: %s<span id="validated_url_erp" data-url="%s">✍</span><span id="final_domain" style="display:none;" data-domain="%s"></span></div>',
            self::ERP_GOOGLE_THE_SAME_REDIRECT_OTHER_DOMAIN => '<div class="col-sm-12">ERP and Google URL are the same. There is found a redirect to another domain. Redirected URL is: %s<span id="validated_url_erp" data-url="%s">✍</span><span id="final_domain" style="display:none;" data-domain="%s"></span></div>',
            self::ERP_GOOGLE_THE_SAME_NOT_WORKING => '<div class="col-sm-12">ERP and Google URL is the same and this URL is not working. Validate this company manually!</div>',
            self::ERP_NOT_WORKING_GOOGLE_BEST => '<div class="col-sm-6">ERP URL is not working.</div><div class="col-sm-6" style="color:green;">Current Google URL is the best one :)<span id="final_domain_other" style="display:none;" data-domain="%s"></span></div>',
            self::ERP_NOT_WORKING_GOOGLE_REDIRECT => '<div class="col-sm-6">ERP URL is not working.</div><div class="col-sm-6">There is a redirect in the scraped Google url. The best URL is: %s<span id="validated_url_other" data-url="%s">✍</span><span id="final_domain_other" style="display:none;" data-domain="%s"></span></div>',
            self::ERP_NOT_WORKING_GOOGLE_REDIRECT_OTHER_DOMAIN => '<div class="col-sm-6">ERP URL is not working.</div><div class="col-sm-6">There is found a redirect to another domain. Redirected URL is: %s<span id="validated_url_other" data-url="%s">✍</span><span id="final_domain_other" style="display:none;" data-domain="%s"></span></div>',
            self::ERP_BEST_GOOGLE_NOT_WORKING => '<div class="col-sm-6" style="color:green;">Current ERP URL is the best one :)<span id="final_domain" style="display:none;" data-domain="%s"></span></div><div class="col-sm-6">Google URL is not working.</div>',
            self::ERP_REDIRECT_GOOGLE_NOT_WORKING => '<div class="col-sm-6">There is a redirect in our ERP url. The best URL is: %s<span id="validated_url_erp" data-url="%s">✍</span><span id="final_domain_erp" style="display:none;" data-domain="%s"></span></div><div class="col-sm-6">Google URL is not working.</div>',
            self::ERP_REDIRECT_ANOTHER_DOMAIN_GOOGLE_NOT_WORKING => '<div class="col-sm-6">There is found a redirect to another domain. Redirected URL is: %s<span id="validated_url_erp" data-url="%s">✍</span><span id="final_domain_erp" style="display:none;" data-domain="%s"></span></div><div class="col-sm-6">Google URL is not working.</div>',
            self::ERP_GOOGLE_NOT_VALID => '<div class="col-sm-12">The URL from ERP and Google is not valid. Validate this company manually!</div>',
            self::ERP_GOOGLE_DOMAIN_CHANGED => '<div class="col-sm-12">The domain of the ERP and Google URL has been changed. Please check it manually!</div>',
            self::ERP_DOMAIN_CHANGED_GOOGLE_FINAL => '<div class="col-sm-6">The domain has been changed after the redirect. (%s<span id="validated_url_erp" data-url="%s">✍</span>)<span id="final_domain_erp" style="display:none;" data-domain="%s"></span></div><div class="col-sm-6">Final Google URL is (%s<span id="validated_url_other" data-url="%s">✍</span>)<span id="final_domain_other" style="display:none;" data-domain="%s"></span></div>',
            self::ERP_FINAL_GOOGLE_REDIRECT => '<div class="col-sm-6">Final ERP URL is (%s<span id="validated_url_erp" data-url="%s">✍</span>)<span id="final_domain_erp" style="display:none;" data-domain="%s"></span></div><div class="col-sm-6">The domain has been changed after the redirect. (%s<span id="validated_url_other" data-url="%s">✍</span>)<span id="final_domain_other" style="display:none;" data-domain="%s"></span></div>',
            self::BOTH_GOOD_FINAL => '<div class="col-sm-12" style="color:green;">Both URLs are good! Final URL of both: %s<span id="final_domain" style="display:none;" data-domain="%s"></span></div>',
            self::ERP_REDIRECT_GOOGLE_BEST => '<div class="col-sm-6">ERP URL is redirected to: %s<span id="validated_url_erp" data-url="%s">✍</span><span id="final_domain_erp" style="display:none;" data-domain="%s"></span></div><div class="col-sm-6" style="color:green;">Google URL is the best URL.<span id="final_domain_other" style="display:none;" data-domain="%s"></span></div>',
            self::ERP_BEST_GOOGLE_REDIRECT => '<div class="col-sm-6" style="color:green;">ERP URL is the best URL.<span id="final_domain_erp" style="display:none;" data-domain="%s"></span></div><div class="col-sm-6">Google URL is redirected to: %s<span id="validated_url_other" data-url="%s">✍</span><span id="final_domain_other" style="display:none;" data-domain="%s"></span></div>',
            self::ERP_REDIRECT_GOOGLE_REDIRECT => '<div class="col-sm-6">ERP URL is redirected to: %s<span id="validated_url_erp" data-url="%s">✍</span><span id="final_domain_erp" style="display:none;" data-domain="%s"></span></div><div class="col-sm-6">Google URL is redirected to: %s<span id="validated_url_other" data-url="%s">✍</span><span id="final_domain_other" style="display:none;" data-domain="%s"></span></div>',
            self::ERP_OTHER_DOMAIN_THAN_GOOGLE => '<div class="col-sm-12" style="color:red;">ERP final URL (%s<span id="validated_url_erp" data-url="%s">✍</span>) has another domain then the final URL of Google (%s<span id="validated_url_other" data-url="%s">✍</span>). Please check it manually!</div>',
            self::ERP_GOOD_GOOGLE_GOOD => '<div class="col-sm-6">Current ERP URL is good.<span id="final_domain_erp" style="display:none;" data-domain="%s"></span></div><div class="col-sm-6">Current Google URL is good.<span id="final_domain_other" style="display:none;" data-domain="%s"></span></div>',
            self::ERP_GOOD_GOOGLE_REDIRECT => '<div class="col-sm-6">Current ERP URL is good.<span id="final_domain_erp" style="display:none;" data-domain="%s"></span></div><div class="col-sm-6">Google URL is redirected to: %s<span id="validated_url_other" data-url="%s">✍</span><span id="final_domain_other" style="display:none;" data-domain="%s"></span></div>',
            self::ERP_REDIRECT_GOOGLE_GOOD => '<div class="col-sm-6">ERP URL is redirected to: %s<span id="validated_url_erp" data-url="%s">✍</span><span id="final_domain_erp" style="display:none;" data-domain="%s"></span></div><div class="col-sm-6">Current Google URL is good.<span id="final_domain_other" style="display:none;" data-domain="%s"></span></div>',
            self::REDIRECTED_BOTH => '<div class="col-sm-6">ERP URL is redirected to: %s<span id="validated_url_erp" data-url="%s">✍</span><span id="final_domain_erp" style="display:none;" data-domain="%s"></span></div><div class="col-sm-6">Google URL is redirected to: %s<span id="validated_url_other" data-url="%s">✍</span><span id="final_domain_other" style="display:none;" data-domain="%s"></span></div>',

        ];
    }
}
