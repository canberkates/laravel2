<?php
/**
 * Created by PhpStorm.
 * User: Lorenzo
 * Date: 09/01/2019
 * Time: 15:47
 */

namespace App\Data;


class CustomerStatusReason
{
	const OTHER = 0;
	const NEEDS_MORE_TURNOVER = 1;
	const QUITS_OTHER_LEAD_GENERATOR = 2;
	const BOOKING_TOO_FEW = 3;
	const REQUEST_QUALITY_IS_BAD = 4;
	const STARTS_WITH_LEAD_GENERATOR = 5;
	const LIMITED_SALES_CAPACITY = 6;
	const HAS_ENOUGH_WORK = 7;
	const NOT_ENOUGH_REQUESTS = 8;
	const BANKRUPTCY = 9;
	const LIMITED_SALES_CAPACITY_2 = 10;
	const LOW_CONVERSION = 11;
	const CLAIMING_DISPUTE = 12;
	const PRICE_DISPUTE = 13;
	const TOO_FEW_LEADS = 14;
	const REQUESTED_BY_MOVER = 15;
	const CONVERSION_ROI = 16;
	const FINANCE_PROCEDURES = 17;
	const INDUSTRY_ISSUE = 18;
	const RULES_POLICIES = 19;
	const SIRELO_DISPUTE = 20;
	const NO_SERVICE_LEADS = 21;
	const COMPETITION = 22;
	const BANKRUPT = 23;
	const UNREACHABLE = 24;
	const NOT_INTERESTED_IN_SERVICE = 25;
	const NOT_SATISFIED_WITH_OUR_PRODUCT = 26;

	/**
	 * @param int $id The id (see constants)
	 * @return null|string Result
	 */
	public static function name($id) {
		return self::all()[$id] ?? null;
	}

	/**
	 * Get all types
	 * @return string[]
	 */
	public static function all() {
		return [
			self::NEEDS_MORE_TURNOVER => "Needs more turnover",
			self::QUITS_OTHER_LEAD_GENERATOR => "Quits other lead generator",
			self::BOOKING_TOO_FEW => "Booking too few",
			self::REQUEST_QUALITY_IS_BAD => "Request quality is bad",
			self::STARTS_WITH_LEAD_GENERATOR => "Starts with lead generator",
			self::LIMITED_SALES_CAPACITY => "Limited sales capacity",
			self::HAS_ENOUGH_WORK => "Has enough work",
			self::NOT_ENOUGH_REQUESTS => "Not enough requests",
			self::BANKRUPTCY => "Bankruptcy",
			self::LIMITED_SALES_CAPACITY_2 => "Limited sales capacity",
			self::LOW_CONVERSION => "Low conversion",
			self::CLAIMING_DISPUTE => "Claiming dispute",
			self::PRICE_DISPUTE => "Price dispute",
			self::TOO_FEW_LEADS => "Too few leads",
            self::REQUESTED_BY_MOVER => "Request by mover",

            self::BANKRUPT => "Bankrupt",
            self::COMPETITION => "Competition",
            self::CONVERSION_ROI => "Conversion & ROI",
            self::FINANCE_PROCEDURES => "Finance Procedures",
            self::INDUSTRY_ISSUE => "Industry issue",
            self::NO_SERVICE_LEADS => "No service (leads)",
            self::NOT_INTERESTED_IN_SERVICE => "Not interested in service",
            self::NOT_SATISFIED_WITH_OUR_PRODUCT => "Not satisfied with our product",
            self::RULES_POLICIES => "Rules & Policies",
            self::SIRELO_DISPUTE => "Sirelo Dispute",
            self::UNREACHABLE => "Unreachable",
            self::OTHER => "Other",

        ];
	}

}
