<?php

namespace App\Repository;

use App\Models\KTRequestCustomerPortal;
use Illuminate\Support\Collection;

class RequestCustomerPortalRepository extends BaseRepository
{

    /**
     * RequestCustomerPortalRepository constructor.
     *
     * @param KTRequestCustomerPortal $model
     */
    public function __construct(KTRequestCustomerPortal $model)
    {
        parent::__construct($model);
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->model->all();
    }

    public function findById($id) : ?KTRequestCustomerPortal{
        return $this->model->find($id);
    }

    public function getCustomerPortalById($id){

    }
}
