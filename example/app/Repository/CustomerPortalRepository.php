<?php

namespace App\Repository;

use App\Models\KTCustomerPortal;
use DB;
use Illuminate\Support\Collection;

class CustomerPortalRepository extends BaseRepository
{

    /**
     * RequestCustomerPortalRepository constructor.
     *
     * @param KTCustomerPortal $model
     */
    public function __construct(KTCustomerPortal $model)
    {
        parent::__construct($model);
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->model->all();
    }

    public function findByReCuPoId($reCuPoId) {

        $customerPortal = KTCustomerPortal::select("kt_customer_portal.*")
            ->join("kt_request_customer_portal", "ktrecupo_ktcupo_id", "ktcupo_id")
            ->where("ktrecupo_id", $reCuPoId)
            ->first();

       return $customerPortal;
    }

}
