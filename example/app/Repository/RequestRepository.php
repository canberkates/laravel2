<?php

namespace App\Repository;

use App\Models\Request;
use DB;
use Illuminate\Support\Collection;

class RequestRepository extends BaseRepository
{

    /**
     * RequestCustomerPortalRepository constructor.
     *
     * @param Request $model
     */
    public function __construct(Request $model)
    {
        parent::__construct($model);
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->model->all();
    }

    public function findByReCuPoId($reCuPoId) {

        $request = Request::select("requests.*")
            ->join("kt_request_customer_portal", "ktrecupo_re_id", "re_id")
            ->where("ktrecupo_id", $reCuPoId)
            ->first();

       return $request;
    }

}
