<?php


namespace App\Cronjobs;


use App\Functions\System;
use App\Functions\Teams;
use App\Models\Cronjob;
use DB;
use Log;

class CronjobController
{

    private $teams;

    public function __construct(Teams $teams){
        $this->teams = $teams;
    }

    public function CronjobWatchdog($id, $task, $subtask, $trigger, $error_count, array $users, $last_ran, $interval, $result){
        $system = new System();

        //Create or update cronjob
        $cronjob = $this->insertOrUpdateCronjob($id, $task, $subtask, $trigger, $error_count, $last_ran, $interval);

        //Always log the results of errors
        if($error_count){
            Log::debug("Result of failed cronjob with ID: ".$id);
            Log::debug($result);
        }

        //Check if error count surpasses the trigger amount
        if($cronjob->cr_trigger <= $cronjob->cr_error_count){

            $content = $cronjob->cr_subtask. " (ID: ".$cronjob->cr_id.") has now failed ".$cronjob->cr_error_count." times. Its trigger value is ".$cronjob->cr_trigger . ". The last time this cronjob ran is ".$cronjob->cr_last_ran;

            $system->sendMessage(3653, $cronjob->cr_subtask . " failed!", $content);
            self::sendTeamsMessage($content);
        }

    }

    private function sendTeamsMessage($content){
        $webhook_url = "https://triglobalbv.webhook.office.com/webhookb2/467aabaf-ab9f-4575-a8eb-b71837b60355@21938c5d-879a-4eef-996a-ac494c9850a6/IncomingWebhook/2f68d60801d54e18b3d8e56670ed225a/e78f7649-2005-4400-9843-f8c4454feb6a";
        $title = '⚠ A cronjob is causing problems! ⚠';
        $text = $content;

        $this->teams->sendMessage($webhook_url, $title, $text);
    }

    private function insertOrUpdateCronjob($id, $task, $subtask, $trigger, $error_count, $last_ran, $interval) : Cronjob
    {

       Cronjob::updateOrCreate(
            ['cr_id' => intval($id)],
            [
                'cr_task' => $task,
                'cr_subtask' => $subtask,
                'cr_trigger' => $trigger,
                'cr_error_count' => ($error_count ? DB::raw( '`cr_error_count` + 1' ) : $error_count ),
                'cr_interval' => $interval,
                'cr_last_ran' => $last_ran,
            ]
        );

       return Cronjob::find($id);
    }
}
