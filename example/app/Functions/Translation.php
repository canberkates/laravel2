<?php
/**
 * Created by PhpStorm.
 * User: Lorenzo
 * Date: 07/02/2019
 * Time: 15:07
 */

namespace App\Functions;

use Illuminate\Support\Facades\Log;

class Translation
{
	public function get($key, $language)
	{

		$data = [
			'token' => 'e$FrA#*aAwg4@nmhGZAbIm^d6DC',
			'key' => $key,
			'language' => $language,
		];

		$data = http_build_query($data, '', '&');

		// Prepare new cURL resource
		$curl = curl_init('https://public.triglobal.info/api/translation.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLINFO_HEADER_OUT, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curl, CURLOPT_ENCODING, "");
		curl_setopt($curl, CURLOPT_MAXREDIRS, 10);
		curl_setopt($curl, CURLOPT_TIMEOUT, 30);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

		// Set HTTP Header for POST request
		curl_setopt($curl, CURLOPT_HTTPHEADER, [
			'Content-Type: application/x-www-form-urlencoded',
			'Content-Length: '.strlen($data)
		]);

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if($err)
		{
			Log::debug($err);
		}
		else
		{
			Log::debug('response: '.$response);
		}

		return $response;
	}

    public function getMultiple($keys)
    {
        $data = [
            'token' => 'e$FrA#*aAwg4@nmhGZAbIm^d6DC',
            'keys' => $keys
        ];

        $data = http_build_query($data, '', '&');

        // Prepare new cURL resource
        $curl = curl_init('https://public.triglobal.info/api/multipletranslation.php');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_ENCODING, "");
        curl_setopt($curl, CURLOPT_MAXREDIRS, 10);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

        // Set HTTP Header for POST request
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Content-Type: application/x-www-form-urlencoded',
            'Content-Length: '.strlen($data)
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if($err)
        {
            Log::debug($err);
        }
        else
        {
            Log::debug('response multiple: '.$response);
        }

        return $response;
    }

    public function getMultipleInLanguage($keys, $language, $array = true)
    {
        $data = [
            'token' => 'e$FrA#*aAwg4@nmhGZAbIm^d6DC',
            'keys' => $keys
        ];

        $data = http_build_query($data, '', '&');

        // Prepare new cURL resource
        $curl = curl_init('https://public.triglobal.info/api/multipletranslation.php');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_ENCODING, "");
        curl_setopt($curl, CURLOPT_MAXREDIRS, 10);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

        // Set HTTP Header for POST request
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Content-Type: application/x-www-form-urlencoded',
            'Content-Length: '.strlen($data)
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if($err)
        {
            Log::debug($err);
        }
        else
        {
            Log::debug('response multiple: '.$response);
        }

        $response = json_decode($response, true);

        foreach($response as $i => $title){
            foreach($title as $index => $translation){
                if($index == $language){
                    $response[$i] = $translation;
                }
            }
        }

        return $response;
    }

    //TODO: Wordt dit nog gebruikt?
   /* public function getTranslationFromMultipleArray($translations, $key, $language)
    {
        return $translations->$key->$language;
    }*/

}
