<?php
/**
 * Created by PhpStorm.
 * User: Lorenzo
 * Date: 07/02/2019
 * Time: 16:08
 */

namespace App\Functions;

use App\Data\ClaimReason;
use App\Data\CustomerPairStatus;
use App\Data\CustomerStatusReason;
use App\Data\CustomerStatusStatus;
use App\Data\CustomerType;
use App\Data\MoverStatus;
use App\Data\MovingSize;
use App\Data\RequestType;
use App\Data\RequestTypeToMovingSizes;
use App\Data\ServiceProviderStatus;
use App\Models\AffiliatePartnerForm;
use App\Models\KTNotificationUser;
use App\Models\Language;
use App\Models\Message;
use App\Models\PaymentCurrency;
use App\Models\SystemSetting;
use Carbon\Carbon;
use DateTime;
use DB;
use function App\Data\RequestTypeToMovingSizes;

class System
{
	public function getSeoKey($key)
	{
		// Replace spaces with dashes.
		$key = str_replace(" ", "-", mb_strtolower(self::rusToLat(trim($key)), "UTF-8"));

		// Replace specifics
		$key = str_replace(["ä", "ü", "ö", "ß"], ["ae", "ue", "oe", "ss"], $key);

		// Remove certain characters
		$key = str_replace(["&", "€", "’"], "", $key);

		// Replace all the html entities
		$key = preg_replace("/&([a-z])[a-z]+;/i", "$1", htmlentities($key));

		$key = preg_replace("/[^a-z0-9_\s-]/", "", $key);
		$key = preg_replace("/-+/", "-", $key);

		return $key."/";
	}

	public function rusToLat($string)
	{
		$chars = [
			"А" => "a", "Б" => "b", "В" => "v", "Г" => "g", "Д" => "d",
			"Е" => "e", "Ё" => "yo", "Ж" => "zh", "З" => "z", "И" => "i",
			"Й" => "j", "К" => "k", "Л" => "l", "М" => "m", "Н" => "n",
			"О" => "o", "П" => "p", "Р" => "r", "С" => "s", "Т" => "t",
			"У" => "u", "Ф" => "f", "Х" => "kh", "Ц" => "ts", "Ч" => "ch",
			"Ш" => "sh", "Щ" => "sch", "Ъ" => "", "Ы" => "y", "Ь" => "",
			"Э" => "e", "Ю" => "yu", "Я" => "ya", "а" => "a", "б" => "b",
			"в" => "v", "г" => "g", "д" => "d", "е" => "e", "ё" => "yo",
			"ж" => "zh", "з" => "z", "и" => "i", "й" => "j", "к" => "k",
			"л" => "l", "м" => "m", "н" => "n", "о" => "o", "п" => "p",
			"р" => "r", "с" => "s", "т" => "t", "у" => "u", "ф" => "f",
			"х" => "kh", "ц" => "ts", "ч" => "ch", "ш" => "sh", "щ" => "sch",
			"ъ" => "", "ы" => "y", "ь" => "", "э" => "e", "ю" => "yu",
			"я" => "ya",
		];

		return strtr($string, $chars);
	}

	public function dropdownOptions($start, $end, $stepsize, $percentage = true){

		$dropdown = [];

		foreach(range($start, $end, $stepsize) as $step)
		{
			$dropdown[$step] = $step.($percentage ? "%" : "");
		}

		return $dropdown;
	}

	/**
	 * Check the date difference (in days) between two dates.
	 * The dates accept almost any type of string (same as DateTime).
	 *
	 * It will return a negative number when the from date is after the to date.
	 * Examples:
	 *  "today"     -> "tomorrow"   : 1
	 *  "tomorrow"  -> "today"      : -1
	 *  "today"     -> "today"      : 0
	 *  "today"     -> "+ 5 days"   : 5
	 *
	 * @param string $date_from From date
	 * @param string $date_to To date
	 * @return float number of days between dates
	 */
	public function dateDifference($date_from, $date_to)
	{
		$ts1 = strtotime($date_from);
		$ts2 = strtotime($date_to);

		$seconds_diff = $ts2 - $ts1;

		return floor($seconds_diff / 3600 / 24);
	}

	public function paymentCurrencyToken($payment_currency_code, $currencies = null)
	{
		if($currencies === null)
		{
			$token = DB::table("payment_currencies")
				->select("pacu_token")
				->where('pacu_code', "=" , $payment_currency_code)
				->first();

			return !empty($token) ? $token->pacu_token : false;
		}
		else
		{
			if(!empty($currencies[$payment_currency_code]))
			{
				return $currencies[$payment_currency_code]["pacu_token"];
			}
			else
			{
				return false;
			}
		}
	}

	public function volumeCalculator($string)
	{
		$volume_calculator = unserialize(base64_decode($string));

		$volume_calculator_html = "";

		if(is_array($volume_calculator))
		{
			foreach($volume_calculator as $category => $items)
			{
				$volume_calculator_html .= "<ul>";
				$volume_calculator_html .= "<li><strong>".ucfirst($category)."</strong></li>";
				$volume_calculator_html .= "<ul>";

				foreach($items as $item => $amount)
				{
				    if(is_array($amount)){continue;}
					$volume_calculator_html .= "<li>".ucfirst($item)." <strong>(".$amount."x)</strong></li>";
				}

				$volume_calculator_html .= "</ul>";
				$volume_calculator_html .= "</ul>";
			}
		}

		return $volume_calculator_html;
	}

	/**
	 * Calculate the actual payment rate for a payment_rate row.
	 * @param float $discount_amount The actual discount amount
	 * @return number the payment rate
	 */
	public function calculatePaymentRate($para_row, &$discount_amount = 0.0)
	{
		$discount_type = intval($para_row->para_discount_type);
		switch($discount_type)
		{
			case 1:
				$discount_amount = $para_row->para_discount_rate;
				break;

			case 2:
				$discount_amount = (($para_row->para_rate / 100) * $para_row->para_discount_rate);
				break;

			default:
				$discount_amount = 0;
		}

		return $para_row->para_rate - $discount_amount;
	}

	/**
	 * Send one or more users an ERP message.
	 * @param int|array $users The user ID or array of user IDs
	 * @param string $subject The subject of the message
	 * @param string $message The message
	 */
	public function sendMessage($users, $subject, $content)
	{
		if(!is_array($users))
		{
			$users = [$users];
		}

		foreach($users as $us_id)
		{
			if(!empty($us_id))
			{
				$message = new Message();

				$message->mes_timestamp = Carbon::now()->toDateTimeString();
				$message->mes_us_id = $us_id;
				$message->mes_subject = $subject;
				$message->mes_message = $content;

				$message->save();
			}
		}
	}

	/**
	 * Get the filtered list of claim reasons.
	 * This removes 2 of the date reasons depending on the request.
	 * @param int $request_type The request type
	 * @param int|null $destination_type The destination type (required for 'request_type=1')
	 * @return array array with reasons
	 */
	public function filteredClaimReasons($request_type, $destination_type = null)
	{
		$claim_reasons = ClaimReason::all();
		$date_reason = self::dateClaimReason($request_type, $destination_type);

		foreach([6, 7, 8] as $option)
		{
			if($date_reason !== $option)
			{
				unset($claim_reasons[$option]);
			}
		}

		return $claim_reasons;
	}

	/**
	 * Get the correct date claim reason based on the request type.
	 * @param int $request_type The request type
	 * @param int|null $destination_type The destination type (required for 'request_type=1')
	 * @return int the reason
	 */
	private function dateClaimReason($request_type, $destination_type = null)
	{
		if($request_type == 2)
		{
			return 8;
		}
		elseif($destination_type == 2)
		{
			return 7;
		}
		else
		{
			return 6;
		}
	}

	public function currencyConvert($value, $currency_from, $currency_to)
	{
		if($currency_from === $currency_to)
		{
			return $value;
		}

		$from = PaymentCurrency::where("pacu_code", $currency_from)->first();

		$to = PaymentCurrency::where("pacu_code", $currency_to)->first();


		if(!empty($from) && !empty($to))
		{
			$value = (($value / $from->pacu_rate) * $to->pacu_rate);

			return round($value, 2);
		}
		else
		{
			return false;
		}
	}

	public function databaseToArray($result){
		return json_decode(json_encode($result), true);
	}

	public function getWebsiteForm($website_form_id)
	{
		$website_form = DB::table("website_forms")
			->select("wefo_name","we_website")
			->leftJoin("websites","wefo_we_id","we_id")
			->where("wefo_id", $website_form_id)
			->first();

		if(!empty($website_form))
		{
			return $website_form->we_website." (".$website_form->wefo_name.")";
		}
		else
		{
			return false;
		}
	}

	public function validatePhone($phonenumber, $country_from, $country_to){
        $results =  json_decode(self::getExternal("https://public.triglobal.info/api/phone/parse.php?phone_number=".urlencode($phonenumber)."&co_codes=".$country_from.",".$country_to), true);

		$valid = false;

		if(!empty($results))
		{
			foreach($results as $country => $fields)
			{
				if(isset($fields['valid']) && $fields['valid'])
				{
					$valid = true;
				}
			}
		}

		return $valid;
	}

	public function getExternal($url, $timeout = 5)
	{
		$user_agent = SystemSetting::where("syse_setting", "company_name")->first()->syse_value;
		$fgc = @file_get_contents($url, 0, stream_context_create(["http" => ["timeout" => $timeout, "header" => "User-Agent: ".$user_agent]]));

		return $fgc;
	}

	public function getPostmarkBounce($email, $timestamp){
		$postmark_token = SystemSetting::where("syse_setting", "postmark_server_token")->first()->syse_value;

		$curl = curl_init();

		if($timestamp == "0000-00-00 00:00:00"){
			$timestamp = "";
		}

		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.postmarkapp.com/bounces?count=10&offset=0&emailFilter=".urlencode($email)."&fromdate=".date("Y-m-d", strtotime($timestamp." -700 day"))."&todate=".date("Y-m-d"),
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_SSL_VERIFYPEER => 0,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"accept: application/json",
				"cache-control: no-cache",
				"postman-token: 562d4e89-4f2b-af5a-872d-96b758c13917",
				"x-postmark-server-token: ".$postmark_token
			),
		));

		$result = curl_exec($curl);

		curl_close($curl);

		return json_decode($result, true)['Bounces'];
	}

	public function getGoogleTranslationLanguage($string)
	{
		$google_token = SystemSetting::where("syse_setting", "google_translate_key")->first()->syse_value;
		$url = "https://www.googleapis.com/language/translate/v2/detect?key=".$google_token."&q=".urlencode(str_replace("\n", "[*]", $string));

        $fgc = json_decode(@file_get_contents($url, 0, stream_context_create(["http" => ["timeout" => 5]])));

        return strtoupper($fgc->data->detections[0][0]->language);
	}

	public function getGoogleTranslation($string, $language = "EN")
	{
		$google_token = SystemSetting::where("syse_setting", "google_translate_key")->first()->syse_value;
		$la_iso = Language::where("la_code", $language)->first()->la_iso;
		$url = "https://www.googleapis.com/language/translate/v2?key=".$google_token."&target=".$la_iso."&q=".urlencode(str_replace("\n", "[*]", $string));

		$google_translate = json_decode(self::getExternal($url), true);

		return str_replace("[*]", "\n", html_entity_decode($google_translate['data']['translations'][0]['translatedText'], ENT_QUOTES));

	}

	/**
	 * Get the first workday (Monday-Friday).
	 * @param string $from Starting from (includes the date if before 12h)
	 * @return DateTime the first work day
	 */
	/**
	 * Get the first workday (Monday-Friday).
	 * @param string $from Starting from (includes the date if before 12h)
	 * @return DateTime the first work day
	 */
	public static function getFirstWorkDay($from = "now")
	{
		$today = new DateTime($from);
		$tomorrow = new DateTime($today->format("Y-m-d"));
		$tomorrow->modify("+1 days");

		//Check if today is viable
		$till_tomorrow = $today->diff($tomorrow);
		//Allow today if it's before 12 o'clock
		if($till_tomorrow->h > 12)
		{
			//Check if weekday
			if($today->format("N") < 6)
			{
				return $today;
			}
		}

		//Try the next working day
		$tomorrow_day = $tomorrow->format("N");
		if($tomorrow_day < 6)
		{
			return $tomorrow;
		}
		else
		{
			$skip_days = 8 - $tomorrow_day;
			$tomorrow->modify("+{$skip_days} days");

			return $tomorrow;
		}
	}

	public function affiliatePartnerForm($affiliate_partner_form_id)
	{
		$affiliate_form = AffiliatePartnerForm::where("afpafo_id", $affiliate_partner_form_id)->first();

		if(!empty($affiliate_form))
		{
			return $affiliate_form->afpafo_name." (".$affiliate_form->afpafo_la_code.")";
		}
		else
		{
			return false;
		}
	}


	public function makeRevisionsReadable($revisions){

		//Log::debug($revisions);

		foreach($revisions as $revision){
			//if($revision->)
		}
	}

	public function findRegionsByOriginCity($city, $destination_type, $country)
	{
		$best_matched = DB::table("requests")
			->select("re_city_from", "re_reg_id_from", "reg_name", "reg_parent", "reg_deleted")
			->selectRaw("COUNT(*) as count")
			->leftJoin("regions", "re_reg_id_from", "reg_id")
			->where("re_status", 1)
			->where("re_city_from", $city)
			->where("re_co_code_from", $country)
			->where("re_destination_type", $destination_type)
			->where("reg_deleted", 0)
			->groupBy("re_reg_id_from")
			->orderBy("count", "DESC")
			->get();

		return $best_matched;
	}

	public function findRegionsByDestinationCity($city, $destination_type, $country)
	{
		$best_matched = DB::table("requests")
			->select("re_city_to", "re_reg_id_to", "reg_name", "reg_parent", "reg_deleted")
			->selectRaw("COUNT(*) as count")
			->leftJoin("regions", "re_reg_id_to", "reg_id")
			->where("re_status", 1)
			->where("re_city_to", $city)
			->where("re_co_code_to", $country)
			->where("re_destination_type", $destination_type)
			->where("reg_deleted", 0)
			->groupBy("re_reg_id_to")
			->orderBy("count", "DESC")
			->get();

		return $best_matched;
	}

	public function movingSizes($request_type = null, $request_type_prefix = false)
	{
		$temp = MovingSize::all();

		if($request_type_prefix)
		{
			foreach($temp as $key => $value)
			{
				$temp[$key] = RequestType::name(self::movingSizeToRequestType()[$key])." - ".$value;
			}
		}

		$moving_sizes = [];

		if($request_type !== null)
		{
			foreach(RequestTypeToMovingSizes::name($request_type) as $key => $value)
			{
				$moving_sizes[$value] = $temp[$value];
			}
		}
		else
		{
			$moving_sizes = $temp;
		}

		return $moving_sizes;
	}

	public static function movingSizeToRequestType()
	{
		return [
			1 => 1,
			2 => 1,
			3 => 1,
			4 => 2,
			5 => 3,
			6 => 3,
			7 => 3,
			8 => 4,
			9 => 2,
            10 => 3
		];
	}

	public function countryRegions($country, $destination_type)
	{

		$result = DB::table("regions")
			->select("reg_id", "reg_name", "reg_parent")
			->where("reg_co_code", $country)
			->where("reg_destination_type", $destination_type)
			->where("reg_deleted", 0)
			->orderBy("reg_parent")
			->get();

		$result = self::databaseToArray($result);

		$data = [];

		foreach($result as $row)
		{
			if(!isset($data[$row["reg_parent"]]))
			{
				$data[$row["reg_parent"]] = [];
			}
			$target_array = &$data[$row["reg_parent"]];

			//Store it
			$target_array[$row["reg_id"]] = $row["reg_name"];


			unset($target_array);
		}

		if(count($data) === 1 && key($data) === "")
		{
			$data = ["All" => $data[""]];
		}


		if(empty($data))
		{
			return ["" => ""];
		}
		else
		{
			return $data;
		}
	}

    /**
     * Make from 1 date string 2 dates
     * @param $date
     * @return array
     */
    public static function betweenDates($date)
    {
        $date = explode( '-', $date );
        return [ date( 'Y-m-d H:i:s', strtotime( trim( $date[0] ).' 00:00:00' ) ), date( 'Y-m-d H:i:s', strtotime( trim( $date[1] ).' 23:59:59' ) )];
    }

    public static function betweenDatesWithoutTime($date)
    {
        $date = explode( '-', $date );
        return [ date( 'Y-m-d', strtotime( trim( $date[0] ) ) ), date( 'Y-m-d', strtotime( trim( $date[1] ) ) )];
    }

    public function queryToArray($query, $key = null, $unset_key = false, $append_to_array = null)
	{
	    $query = self::databaseToArray($query);
		$result = [];
		foreach($query as $row)
		{
			if($append_to_array !== null)
			{
				$row = $row + $append_to_array;
			}

			if($key == null)
			{
				$result[] = $row;
			}
			else
			{
				$key_value = $row[$key];
				if($unset_key)
				{
					unset($row[$key]);
				}
				$result[$key_value] = $row;
			}
		}

		return $result;
	}

	public static function useOr($value, $default = "", $prefix = "", $suffix = "")
	{
		if(empty($value))
		{
			return $default;
		}
		else
		{
			if($prefix === "" && $suffix === "")
			{
				return $value;
			}
			else
			{
				return $prefix.$value.$suffix;
			}
		}
	}

	public static function validateEmail($email, $billing = false, &$errors = [])
	{
		if(!empty($email))
		{
			if(strpos($email, "@") === false || strpos($email, ".") === false)
			{
				$errors[] = "Email".(($billing) ? " (billing)" : "")." is invalid";
			}
			elseif(preg_match("/(\s|;)/", $email))
			{
				$errors[] = "Email".(($billing) ? " (billing)" : "")." contains spaces or ;";
			}
			elseif(substr_count($email, "@") !== (substr_count($email, ",") + 1))
			{
				$errors[] = "Email".(($billing) ? " (billing)" : "")." is not correctly separated";
			}
			else
			{
				foreach(explode(",", $email) as $key => $exp)
				{
					$email_exp = explode("@", $exp);

					if(!filter_var(idn_to_ascii($email_exp[0])."@".idn_to_ascii($email_exp[1]), FILTER_VALIDATE_EMAIL))
					{
						$errors[] = "Email #".($key + 1)." ".(($billing) ? " (business)" : "")." is invalid";
					}
				}
			}
		}

		return $errors;
	}

	public static function escapedImplode($values, $glue = ",", $wrap = "'")
	{
		foreach($values as $index => $value)
		{
			$values[$index] = $wrap.$value.$wrap;
		}

		return implode($glue, $values);
	}

    public static function escapedKeysImplode($values, $glue = ",", $wrap = "'")
    {
        return System::escapedImplode(array_keys($values), $glue, $wrap);
    }

	public static function getSetting($setting)
	{
		$result = SystemSetting::where("syse_setting", "=", $setting)->first();


		if($result->count())
		{
			return $result->syse_value;
		}
		else
		{
			return false;
		}
	}

    public static function serialize($string)
    {
        return base64_encode(serialize($string));
    }

	public static function unserialize($string)
	{
		return unserialize(base64_decode($string));
	}

	public function hashLogoName( $cu_id, $extension ) {

        return md5(uniqid(time(), true).uniqid( $cu_id, true)).'.'.$extension ;
    }

    public static function numberFormat($number, $decimals = 0, $dec_point = ",", $thousands_sep = ".")
	{
		if(empty($number))
		{
			$number = 0;
		}

		return number_format($number, $decimals, $dec_point, $thousands_sep);
	}

	public function showMessage($message, $alert_type = "danger")
	{
		return "<div class='alert alert-".$alert_type."' role='alert'>".$message."</div>";
	}

	public function getStatus($id){
		//Check if active
		$active_checks = [
			["kt_customer_portal", "ktcupo"],
			["kt_customer_question", "ktcuqu"],
			["service_provider_newsletter_blocks", "seprnebl"],
			["affiliate_partner_data", "afpada"]
		];

		$returnstatus = -1;
		$active_found = false;

		foreach($active_checks as $check)
		{
			if($active_found){break;}
			$table = $check[0];
			$prefix = $check[1];

			$builder = DB::table($table)
			->selectRaw("DISTINCT {$prefix}_cu_id")
			->selectRaw("{$prefix}_status");

			if($table === "kt_customer_portal")
			{
				$builder->where("ktcupo_free_trial", 0);
			}

			$builder =
				$builder
					->whereIn("{$prefix}_status", ["1", "2"])
					->where("{$prefix}_cu_id", $id)
					->get();

			foreach($builder as $active_row)
			{
				$row = self::databaseToArray($active_row);

				$status = $row[$prefix.'_status'];
				if($status == 1){
					$returnstatus = 1;
					$active_found = true;
				}

				if($status == 2 && $returnstatus == -1){
					$returnstatus = 2;
				}
			}
		}

		if($returnstatus == -1){$returnstatus=0;}
		return CustomerPairStatus::name($returnstatus);
	}

	public function customerRemarkTable($row)
	{
		//Create a table to display status
		$remarktable = "<table id='remark'>";
		$remarktable .= "<th colspan=\"4\"><strong>Status update</strong></th>";
		$remarktable .= "<tr><td><strong>Status</strong></td><td><strong>Reason</strong></td><td><strong>Netto turnover</strong></td><td><strong>Remark</strong></td></tr>";
		$remarktable .= "<tr><td>".CustomerStatusStatus::name($row->cust_status)."</td>";
		$remarktable .= "<td>".CustomerStatusReason::name($row->cust_reason)."</td>";
		$remarktable .= "<td>€".number_format($row->cust_turnover_netto, 2, ",", ".")."</td>";
		$remarktable .= "<td>".$row->cust_remark."</td></tr>";
		$remarktable .= "</table>";

		return $remarktable;
	}

    public function getCustomerStatus($cu_id)
    {
        return self::getMultipleCustomerStatuses([$cu_id])[$cu_id];
    }

    public function getMultipleCustomerStatuses($cu_ids)
    {
        $result = [];
        $customers = [];
        foreach($cu_ids as $cu_id)
        {
            $result[$cu_id] = false;
        }

        $customers_query = DB::table("customers")
            ->select("cu_id","cu_type","cu_debt_collector", "cu_credit_hold")
            ->whereIn("cu_id", $cu_ids)
            ->where("cu_deleted",0)
            ->get();

        $customers_query = self::databaseToArray($customers_query);

        foreach($customers_query as $customer){
            $customers[$customer['cu_id']] = $customer;
        }

        $portals = DB::table("kt_customer_portal")
            ->distinct()
            ->select("ktcupo_cu_id", "ktcupo_status","ktcupo_free_trial")
            ->whereIn("ktcupo_cu_id", $cu_ids)
            ->get();

        $portals = self::databaseToArray($portals);

        foreach($portals as $row)
        {
            if($row['ktcupo_status'] == 1)
            {
                $key = ($row['ktcupo_free_trial'] == 1 ? 2 : 1);
                $customers[$row['ktcupo_cu_id']][$key] = true;
            }
            elseif($row['ktcupo_status'] == 2)
            {
                $customers[$row['ktcupo_cu_id']][3] = true;
            }
        }

        $statuses = DB::table("customer_statuses")
            ->selectRaw("DISTINCT cust_cu_id")
            ->whereIn("cust_cu_id", array_keys($customers))
            ->where("cust_status", 8)
            ->whereBetween("cust_date", ["NOW() - INTERVAL 1 YEAR", "NOW()"])
            ->get();

        $statuses = self::databaseToArray($statuses);

        foreach($statuses as $status_row)
        {
            $cu_id = $status_row['cust_cu_id'];

            if($customers[$cu_id]['cu_type'] == 1 || $customers[$cu_id]['cu_type'] == 6)
            {
                $customers[$cu_id][4] = true;
            }
        }

        //Check questions
        foreach(['kt_customer_question' => 'ktcuqu', 'service_provider_newsletter_blocks' => 'seprnebl'] as $table => $key)
        {
            $questions = DB::table($table)
                ->select("{$key}_cu_id", "{$key}_status")
                ->whereIn("{$key}_cu_id", array_keys($customers))
                ->get();

            $questions = self::databaseToArray($questions);

            foreach($questions as $row)
            {
                $customers[$row["{$key}_cu_id"]][$row["{$key}_status"]] = true;
            }
        }

        //Build result
        foreach($customers as $cu_id => $customer)
        {
            if($customer['cu_type'] > 2 && $customer['cu_type'] != CustomerType::LEAD_RESELLER)
            {
                $result[$cu_id] = CustomerType::name($customer['cu_type']);
                continue;
            }

            $mover = ($customer['cu_type'] == CustomerType::MOVER || $customer['cu_type'] == CustomerType::LEAD_RESELLER);
            $result_key = $mover ? 5 : 3;

            if($customer['cu_debt_collector'] == 1)
            {
                $result_key = $mover ? 7 : 5;
            }
            elseif($customer['cu_credit_hold'] == 1)
            {
                $result_key = $mover ? 6 : 4;
            }
            else
            {
                foreach([1, 2, 3, 4] as $key)
                {
                    if(isset($customer[$key]))
                    {
                        $result_key = $key;
                        break;
                    }
                }

            }

            $status_text = ($mover ? MoverStatus::name($result_key) : ServiceProviderStatus::name($result_key));

            if($customer['cu_type'] == CustomerType::LEAD_RESELLER){
                $status_text = str_replace("MV -", "LR -", $status_text);
            }

            $result[$cu_id] = $status_text;

        }

        return $result;
    }

    /**
     * Convert an amount to a different currency.
     * This requires the full database set of PaymentCurrency
     *
     * @param array $currencies The currencies
     * @param mixed $amount The amount
     * @param string $currency_from The from code (ex. EUR)
     * @param string $currency_to The to code (ex. USD)
     * @return float the converted value
     */
    public static function getAvailableCurrencyConvert(&$currencies, $amount, $currency_from, $currency_to)
    {
        if($currency_from === $currency_to || $amount === 0)
        {
            return $amount;
        }
        else
        {
            $row_from = $currencies->where("pacu_code", $currency_from)->first();
            $row_to = $currencies->where("pacu_code", $currency_to)->first();

            $value = (($amount / $row_from->pacu_rate) * $row_to->pacu_rate);

            return round($value, 2);
        }
    }

    public static function createPDF($folder, $name, $html, $config = [])
    {
        $folder = env("SHARED_FOLDER")."uploads/".$folder;

        if(!is_dir($folder))
        {
            mkdir($folder, 0755);
        }

        $defaults = [

            'margin'    => 20,
            'zoom'      => 1,
            'width'     => '',
            'height'    => '',
            'dpi'       => 96,
            'delay'     => '',
            'url'       => '', // To overwrite the html with an url
            'force'     => '',
            'auth_user' => '',
            'auth_pass' => '',
        ];

        if( ! empty( $config ) && is_array( $config ) ) {

            foreach( $config as $key => $option ) {

                $defaults[$key] = $option;
            }
        }

        $param = '';

        $param .= '&margin_top='.$defaults['margin'];
        $param .= '&margin_bottom='.$defaults['margin'];
        $param .= '&margin_left='.$defaults['margin'];
        $param .= '&margin_right='.$defaults['margin'];
        $param .= '&zoom=1';
        $param .= '&dpi='.$defaults['dpi'];

        if( ! empty( $defaults['width'] ) ) {

            $param .= '&page_width='.$defaults['width'];
        }

        if( ! empty( $defaults['height'] ) ) {

            $param .= '&page_height='.$defaults['height'];
        }

        if( ! empty( $defaults['delay'] ) ) {

            $param .= '&delay='.$defaults['delay'];
        }

        if( ! empty( $defaults['auth_user'] ) && ! empty( $defaults['auth_pass'] ) ) {

            $param.= '&auth_user='.trim( $defaults['auth_user'] ).'&auth_pass='.trim( $defaults['auth_pass'] );
        }

        if( $defaults['url'] ) {

            $param .= '&document_url='.$defaults['url'];
        }

        if( $defaults['force'] ) {

            $param .= '&force=1';
        }

        // If test mode
        if( env('ENVIRONMENT') == "TEST") {
            $param .= '&test=1';
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://api.pdflayer.com/api/convert?access_key=". System::getSetting("pdflayer_key").$param);
        curl_setopt($ch, CURLOPT_POST, true);

        if( ! $defaults['url'] ) {

            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(["document_html" => $html]));
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $pdf = curl_exec($ch);
        curl_close($ch);

        $response = json_decode($pdf);

        if(is_object($response))
        {
            return [0, $response->error->info];
        }
        else
        {
            file_put_contents($folder.$name.".pdf", $pdf);

            return [1, null];
        }
    }

    public function generateRandomString($length)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function arrayParameter($parameter, $allow_null = false)
	{
		if(is_array($parameter))
		{
			return $parameter;
		}
		else
		{
			if($parameter === null && !$allow_null)
			{
				return [];
			}
			else
			{
				return [$parameter];
			}
		}
	}

    public function endsWith($FullStr, $needle)
    {
        $StrLen = strlen($needle);
        $FullStrEnd = substr($FullStr, strlen($FullStr) - $StrLen);
        return $FullStrEnd == $needle;
    }

    public static function customerTypeToURL($cu_id, $cu_type)
    {
        $CurPageURL = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

        $redirect = false;
        $url = "";

        if ($cu_type == 1)
        {
            if(strpos($CurPageURL, "customers") === false){
                $redirect = true;
                $url = 'customers/' . $cu_id . "/edit";
            }
        }
        elseif($cu_type == 2)
        {
            if(strpos($CurPageURL, "serviceproviders") === false){
                $redirect = true;
                $url = 'serviceproviders/' . $cu_id . "/edit";
            }
        }
        elseif($cu_type == 3)
        {
            if(strpos($CurPageURL, "affiliatepartners") === false){
                $redirect = true;
                $url = 'affiliatepartners/' . $cu_id . "/edit";
            }
        }
        elseif($cu_type == 6)
        {
            if(strpos($CurPageURL, "resellers") === false){
                $redirect = true;
                $url = 'resellers/' . $cu_id . "/edit";
            }
        }

        return ['redirect' => $redirect, 'url' => $url];


    }

    public static function isCron()
    {
        if(php_sapi_name() == "cli" && empty(System::getIP()))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static function getIP()
    {
        if(isset($_SERVER["HTTP_CF_CONNECTING_IP"]))
        {
            $cloudflare = $_SERVER["HTTP_CF_CONNECTING_IP"];
        }
        else
        {
            $cloudflare = null;
        }

        if(isset($_SERVER["HTTP_CLIENT_IP"]))
        {
            $client = $_SERVER["HTTP_CLIENT_IP"];
        }
        else
        {
            $client = null;
        }
        if(isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
        {
            $forward = $_SERVER["HTTP_X_FORWARDED_FOR"];
        }
        else
        {
            $forward = null;
        }

        if(filter_var($cloudflare, FILTER_VALIDATE_IP))
        {
            $ip = $cloudflare;
        }
        elseif(filter_var($client, FILTER_VALIDATE_IP))
        {
            $ip = $client;
        }
        elseif(filter_var($forward, FILTER_VALIDATE_IP))
        {
            $ip = $forward;
        }
        else
        {
            $ip = @$_SERVER['REMOTE_ADDR'];
        }

        return $ip;
    }

    public static function getUserIdsOfNotificationList($name)
    {
        $array = [];
        $users = KTNotificationUser::leftJoin("notification_lists", "ktnous_noli_id", "noli_id")->where("noli_name", $name)->get();

        foreach ($users as $us)
        {
            array_push($array, $us->ktnous_us_id);
        }

        return $array;
    }

    public static function getExternalCurl($url, $timeout = 5)
    {
        $agent = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.79 Safari/537.36';
        $ch = curl_init($url); // initialize curl with given url
        curl_setopt($ch, CURLOPT_USERAGENT, $agent); // set  useragent
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // write the response to a variable
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); // follow redirects if any
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout); // max. seconds to execute
        curl_setopt($ch, CURLOPT_FAILONERROR, 0); // stop when it encounters an error

        $result = @curl_exec($ch);

        $second = curl_init($url); // initialize curl with given url
        curl_setopt($second, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // set  useragent
        curl_setopt($second, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($second, CURLOPT_RETURNTRANSFER, true); // write the response to a variable
        curl_setopt($second, CURLOPT_FOLLOWLOCATION, true); // follow redirects if any
        curl_setopt($second, CURLOPT_CONNECTTIMEOUT, $timeout); // max. seconds to execute
        curl_setopt($second, CURLOPT_FAILONERROR, 0); // stop when it encounters an error

        $result .= @curl_exec($second);


        return $result;
    }

    public static function getDateInFormatPerLanguage($date, $la_code, $year = false) {
        $date = new DateTime($date);

        $month_name = Translation::get("month_".$date->format("n"), $la_code);
        $day = $date->format("d");

        if (substr($day, 0, 1) === '0') {
            $day = substr($day, 1);
        }

        return $day.(($la_code == "ES") ? " de " : " ").$month_name.(($year) ? " ".$date->format("Y"): "");
    }

    public static function getDomainFromURL($url) {
        $parsed = parse_url($url);

        //Return domain and strip www. from domain
        return str_replace("www.", "", $parsed['host']);
    }
}
