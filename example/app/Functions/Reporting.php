<?php
/**
 * Created by PhpStorm.
 * User: Lorenzo
 * Date: 9-11-2079
 * Time: 15:29
 */

namespace App\Functions;

//Defined strings for td calculation
use App\Data\ReportProgressStatus;
use App\Functions\System;
use App\Models\Report;
use App\Models\ReportProgress;
use Carbon\Carbon;
use DB;
use Log;

define("TD_CALC_PER", 1);
define("TD_CALC_AVG", 2);


define("RD_REQUESTS", "r");
define("RD_IMPORT_CUSTOMERS", "i");
define("RD_EXPORT_CUSTOMERS", "e");

define("CU_NAME", "n");
define("CU_PORTALS", "p");

define("CUPO_STATUS", "st");
define("CUPO_PAYMENT_RATE", "pr");
define("CUPO_PACU_CODE", "pc");
define("CUPO_PO_ID", "po");
define("CUPO_MIN_VOLUME_M3", "m3");
define("CUPO_MIN_VOLUME_FT3", "f3");
define("CUPO_REQUEST_TYPE", "rt");
define("CUPO_EXPORT_MOVES", "em");
define("CUPO_IMPORT_MOVES", "im");
define("CUPO_BUSINESS_ONLY", "bo");
define("CUPO_FREE_TRIAL", "ft");
define("CUPO_MOVING_SIZE", "ms");
define("CUPO_FROM_REGIONS", "fr");
define("CUPO_TO_REGIONS", "tr");

//define("CUPO_QUALITY_SCORE", "cqs");
//define("CUPO_QUALITY_SCORE_OVERRIDE", "cqr");

define("MODA_CAPPINGS_METHOD", "c");
define("CUPO_CAPPINGS_DAILY", "d");
define("CUPO_CAPPINGS_MONTHLY", "m");

class Reporting
{
	/**
	 * Queue a report
	 * @param int $report_id ID of the report
	 * @param string $rep_report Name/ID of the report
	 * @param bool $heavy The request report is heavy
	 */
	public static function addToQueue($report_id, $rep_report, $post_values)
	{

		$progress = System::serialize(["[".date("Y-m-d H:i:s")."] Initiating Report..."]);
		$post = System::serialize($post_values);

		$report_progress = new ReportProgress();

        $report_progress->repr_us_id = \Auth::id();
        $report_progress->repr_rep_id = $report_id;
        $report_progress->repr_comment = $post_values['report_comment']; //Post comment?
        $report_progress->repr_create_timestamp = Carbon::now()->toDateTimeString();
        $report_progress->repr_waiting = 0;
        $report_progress->repr_progress_array = $progress;
        $report_progress->repr_posted_values = $post;

        $report_progress->save();

        return $report_progress->repr_id;
	}


	/**
	 * Get progress info
	 * Also checks if the script should be stopped (and will stop it if specified)
	 * @param int $id The ID
	 * @param bool|true $auto_kill Kill the script if needed
	 * @param bool|false $join_user Join the user table
	 * @return array Array with info
	 */
	public static function getProgressInfo($id, $auto_kill = true, $join_user = false)
	{
        $system = new System();
        $task_row = DB::table("report_progress")
            ->leftJoin("reports", "rep_id", "repr_rep_id")
            ->where("repr_id", $id)
            ->first();

		if(!empty($task))
		{
			return false;
		}

		$task_row = $system->databaseToArray($task_row);

		//Put in array
		$progress_data = [
			"id" => $task_row['repr_id'],
			"user" => $task_row['repr_us_id'],
			"report" => $task_row['repr_rep_id'],
			"comment" => $task_row['repr_comment'],
			"created" => $task_row['repr_create_timestamp'],
			"started" => $task_row['repr_start_timestamp'],
			"finished" => $task_row['repr_finish_timestamp'],
			"status" => $task_row['repr_status'],
			"progress" => System::unserialize($task_row['repr_progress_array']),
			"posted" => System::unserialize($task_row['repr_posted_values']),
			"mysql_id" => $task_row['repr_mysql_thread']
		];

		//Add possible user data
		if($join_user)
		{
			$progress_data["user_name"] = $task_row['us_name'];
		}

		//Check if it can continue
		if($progress_data["status"] == 4 && $auto_kill)
		{
			self::updateField($id, [
				"repr_finish_timestamp" => "NOW()"
			]);
			die();
		}

		//Return the data
		return $progress_data;
	}

	/**
	 * Update the status/progress of a report
	 * @param int $id The ID of the report progress
	 * @param array|null $update_fields The fields that need to be updated
	 * @param bool $update_read Defines whether repr_read should be updated on repr_status update
	 */
	public static function updateField($id, $update_fields, $update_read = true)
	{

		//Nothing to update
		if(empty($update_fields))
		{
			return;
		}

		//Add Read if needed
		if(isset($update_fields['repr_status']) && $update_read)
		{
			$update_fields['repr_read'] = 0;
		}

		//Build the SQL
		$array = [];
		foreach($update_fields as $field => $value)
		{
			if($value === "NOW()")
			{
				$value_sql = $value;
			}
			else
			{
				if(is_array($value))
				{
					$value = System::serialize($value);
				}
				$value_sql = $value;
			}

            $array[] = [$field => $value_sql];
		}

		//TODO: test this shit fam
        DB::table('report_progress')
            ->where('repr_id', $id)
            ->update($array[0]);

	}

	/**
	 * Add one or more updates to the array
	 * @param int $id The ID of the report
	 * @param array $progress_array The array with the old updates
	 * @param array|string $new_update New updates
	 * @param bool|true $auto_update Update the database
	 * @param bool|false $finishing_status Update the status to finishing up.
	 */
	public static function addProgress($id, &$progress_array, $new_update, $auto_update = true, $finishing_status = false)
	{
		if(!is_array($new_update))
		{
			$new_update = [$new_update];
		}

		foreach($new_update as $update)
		{
			$progress_array[] = "[".date("Y-m-d H:i:s")."] ".$update;
		}

		if($auto_update)
		{
			$values = [
				"repr_progress_array" => $progress_array
			];
			if($finishing_status)
			{
				$values["repr_status"] = 2;
			}
			Reporting::updateField($id, $values);
		}
	}


	/**
	 * Check if the report should be stopped and will kill it
	 * @param int $id Report progress ID
	 * @param bool $heavy Report is heavy & should check
	 */
	public static function checkIfStopped($id)
	{
        $report_status = DB::table("report_progress")
            ->select("repr_status")
            ->where("repr_id", $id)
            ->first();

		if($report_status->repr_status == ReportProgressStatus::DELETED)
		{
			Reporting::updateField($id, ["repr_finish_timestamp" => "NOW()"]);
		}
	}

	/**
	 * Finish a report
	 * This will save the gathered output in a file and update the database.
	 * @param int $id The ID of the report progress
	 */
	public static function finish($id)
	{
		//The resulting HTML
		$result = ob_get_clean();

		//Check for errors
		global $warning_log;
		if($warning_log["warnings"] > 0)
		{
			ob_start();

			//TODO: Errors teruggeven
			/*echo System::message("error", "<strong>ATTENTION:</strong> ".$warning_log["warnings"]." errors occured during the generation of this report. <strong>Please notify IT!</strong>");

			echo "<p style=\"display:none\">";
			echo json_encode($warning_log);
			echo "</p>";*/

			$result = ob_get_clean().$result;
		}

		//Save it to a file
		file_put_contents(env('SHARED_FOLDER')."uploads/reports/report-result-".$id.".html", $result);

		//Update the database
		Reporting::updateField($id, [
			"repr_finish_timestamp" => "NOW()",
			"repr_status" => 3
		]);
	}

	/**
	 * Echo the currently running reports
	 * Optionally return a certain row for more info
	 * @param int $report_id The report ID
	 * @param int|null $repr_id The ID of a report progress
	 */
	public static function displayReportStatus($report_id, $repr_id = null)
	{
		$return_row = null;

        $report_query = DB::table("report_progress")
            ->select("us_name", "repr_id", "repr_comment", "repr_create_timestamp", "repr_start_timestamp", "repr_finish_timestamp", "repr_status",  "repr_progress_array")
            ->leftJoin("users", "repr_us_id", "us_id")
            ->where("repr_rep_id", $report_id)
            ->where("repr_status", "!=", ReportProgressStatus::DELETED)
            ->whereRaw("ADDDATE(`repr_create_timestamp`, INTERVAL 24 HOUR) > NOW()")
            ->get();

		$html = "<div>";
		$rows_printed = 0;
		if(!empty($report_query))
		{
			foreach($report_query as $report_row)
			{
				if($report_row->repr_id == $repr_id)
				{
					$return_row = $report_row;
					continue;
				}
				$rows_printed++;

				//TODO: Form creation
                $html .= "<div class='col-sm-3'>";
				$html .= "Report #".$report_row->repr_id."(".DataIndex::reportProgressStatus()[$report_row->repr_status].")";
				$html .= "<p>";
				$html .= "Created by: ".$report_row->us_name."<br />";
				$html .= "Comment: ".$report_row->repr_comment."<br />";

				if($report_row->repr_status != 0)
				{
					$html .= "Started at: ".$report_row->repr_start_timestamp."<br />";
				}

				if($report_row->repr_status >= 3)
				{
					$html .= "Finished at: ".$report_row->repr_finish_timestamp."<br />";
				}

				$html .= "</p>";
				$html .= "<p>";
				$html .= "<a href=\"?repr_id=".$report_row->repr_id."\" class=\"button\">View</a>";

				if($report_row->repr_status == 0 || $report_row->repr_status == 1)
				{
					$html .= " <a href=\"/reports/report_progress/?repr_id=".$report_row->repr_id."&action=cancel\" class=\"button\">Cancel</a>";
				}
				elseif($report_row->repr_status >= 3)
				{
					$html .= " <a href=\"/reports/report_progress/?repr_id=".$report_row->repr_id."&action=delete\" class=\"button\">Delete</a>";
				}
				$html .= "</p>";

				$html .= "</div>";

			}
		}
		if($rows_printed === 0)
		{
			$html .= "There are currently no other reports.";
		}

		$html .= "</div>";

		if($return_row === null)
		{
			$html .= "<div style=\"position: absolute; bottom: 0; left: 0; width: 100%; height: 2px;\"><div class=\"report-loading-bar\" style=\"background: #6FB600; width: 0%; height: 100%;\"></div></div>";
		}

		//Build the form
        /*
		Form::createForm("report-progress", [
			"style" => [
				"type" => "html",
				"html" => "<style>#report-progress fieldset fieldset { display: inline-block; margin-right: 10px; } #report-progress fieldset fieldset p { margin-bottom: 5px; }</style>"
			],
			"reports_start" => [
				"type" => "fieldset_start",
				"label" => "Other reports"
			],
			"reports_html" => [
				"type" => "html",
				"html" => $html
			],
			"reports_end" => [
				"type" => "fieldset_end"
			]
		]);
        */

		return $html;
		//return \App\Functions\System::databaseToArray($return_row);
	}

	/**
	 * Display the info about a single report_progress row
	 * @param array $report_row Row with data for that single one
	 */
	public static function displaySingleReportStatus($report_row)
	{
	    if(empty($report_row['us_name']))
        {
            return "";
        }

	    $html = "<div style=\"position: absolute; bottom: 0; left: 0; width: 100%; height: 2px;\"><div class=\"report-loading-bar\" style=\"background: #6FB600; width: 0%; height: 100%;\"></div></div>";
		$html .= "<div id=\"single-report-status\" data-status=\"".$report_row['repr_status']."\">";
		$html .= "<p>";
		$html .= "Created by: ".$report_row['us_name']."<br />";
		$html .= "Comment: ".$report_row['repr_comment']."<br />";

		if($report_row['repr_status'] != 0)
		{
			$html .= "Started at: ".$report_row['repr_start_timestamp']."<br />";
		}

		if($report_row['repr_status'] >= 3)
		{
			$html .= "Finished at: ".$report_row['repr_finish_timestamp']."<br />";
		}

		$html .= "</p>";

		$html .= "<p>Progress:</p>";
		$html .= "<ul>";
		$progress_array = System::unserialize($report_row['repr_progress_array']);

		if(empty($progress_array))
		{
			$progress_array[] = "No data yet";
		}
		else
		{
			foreach($progress_array as $progress)
			{
				$html .= '<li>'.$progress.'</li>';
			}
			if($report_row['repr_status'] >= 3)
			{
				$html .= "<li>[{$report_row['repr_finish_timestamp']}] Finished</li>";
			}
		}
		$html .= "</ul>";
		$html .= "</div>";



		/*Form::createForm("single-report-progress", [
			"single_start" => [
				"type" => "fieldset_start",
				"label" => "Report #".$report_row['repr_id']." (".DataIndex::reportProgressStatus()[$report_row['repr_status']].")"
			],
			"single_html" => [
				"type" => "html",
				"html" => $html
			],
			"single_end" => [
				"type" => "fieldset_end"
			]
		]);*/

		return $html;
	}

	/**
	 * Cancel a report
	 * @param int $repr_id The report progress id
	 * @param int $mysql_id The ID of the MySQL thread
	 * @param int $current_status The current status of the report
	 */
	public static function cancelReport($repr_id, $mysql_id, $current_status)
	{
		//Update the row
		$report_progress = ReportProgress::where("repr_id", $repr_id)->first();
		$report_progress->repr_status = 4;
		$report_progress->save();

		//Check if query running
		if($current_status == 1)
		{
			$mysql_thread_id = intval($mysql_id);

            $processlist = DB::select("SHOW PROCESSLIST;");

            foreach($processlist as $process_row)
			{
				if(intval($process_row['ID']) === $mysql_thread_id)
				{
					//Kill it
                    DB::raw("KILL ".$mysql_thread_id);
					break;
				}
			}
		}
	}

	/**
	 * Output the HTML file available for a report
	 * @param int $repr_id The ID of the report
	 */
	public static function outputReportResult($repr_id)
	{
		$file = env("SHARED_FOLDER")."uploads/reports/report-result-".$repr_id.".html";
		if(file_exists($file))
		{
			//echo "<p class=\"table-loading-message\"><img src=\"/frontend/images/icon_loading.gif\" /></p>";
			//echo "<div class=\"table-loading-wrapper\" style=\"display: none;\">";
			include $file;
			//echo "</div>";
		}
		else
		{
			//echo System::message("error", "The report you are looking for has disappeared.. Woops!");
		}
	}

	/**
	 * PHP executed for a Report script
	 * @param int $repr_id ID
	 * @return array Array containing PDO & data
	 */
	public static function includeScript($repr_id)
	{
		ignore_user_abort(true);

		$mysql_id = DB::select("SELECT CONNECTION_ID()");

		//Get the info about this script
		$report_data = Reporting::getProgressInfo($repr_id);
		$_POST = $report_data['posted'];

		//Update to indicate start
		Reporting::updateField($repr_id, [
			"repr_status" => 1,
			"repr_waiting" => 0,
			"repr_start_timestamp" => "NOW()",
			"repr_mysql_thread" => $mysql_id
		]);

		return [
			"data" => $report_data
		];
	}


	/**
	 * Include function for report modules.
	 *
	 * Info about the return data:
	 * - single_report_status_html | HTML for the single report if selected
	 * - reports_status_html | HTML for the other reports
	 * - form_disabled | Whether the form should be disabled
	 * - submit_button | Value for the submit button
	 * - form_buttons | Buttons at the end of the form
	 * - form_comment_section | Section for the form with the comment field in it
	 * - rep_id | ID of this report
	 * - rep_heavy | Report is heavy
	 *
	 * And some more data regarding whether the report should be printed
	 * - repr_available | Boolean
	 * - repr_id | ID or null of the report
	 *
	 * @param string $rep_report Report string (Ex. region_occupancy)
	 * @return array Results
	 */
	public static function includeModule($rep_report)
	{
		?>
		<h2><?php $report_data = Data::getReportTitle($rep_report); ?></h2>
		<?php
			$rep_description_query = TGB_QueryBuilder::select("rep_description")
				->from("reports")
				->whereEqual("rep_report", $rep_report)
				->collectQuery();

			$rep_description = $rep_description_query[0]['rep_description'];

			if (!empty($rep_description))
			{
				echo "<p><b>Description:</b> ".$rep_description."</p>";
			}
		?>
		<script src="/frontend/js/reports.js"></script>
		<script>
			var report = "<?php echo $rep_report; ?>";
			var repr_id = <?php echo(isset($_GET['repr_id']) ? '"'.$_GET['repr_id'].'"' : "null"); ?>;
		</script>
		<?php
		System::updateMessage("reports");

		$form_buttons = [
			[
				"label" => "Back",
				"url" => "reports/reports/"
			]
		];
		$form_disabled = "";
		if(!isset($submit_button))
		{
			$submit_button = "Queue report";
		}

		//Get report
		$repr_id = null;
		$rep_id = $report_data['rep_id'];
		$repr_available = false;
		if(isset($_GET['repr_id']))
		{
			$repr_id = intval($_GET['repr_id']);
			$_SERVER['REQUEST_METHOD'] = "POST";
			$report_status_data = Reporting::getProgressInfo($repr_id, false, true);
			if(!$report_status_data || $report_status_data['status'] == 4)
			{
				System::redirect("/reports/".$rep_report."/");
			}
			$_POST = $report_status_data['posted'];
			$submit_button = null;
			$form_disabled = "disabled";
			$new_form_buttons = [
				[
					"label" => "New report",
					"url" => "reports/".$rep_report."/"
				],
				[
					"label" => "Clone report",
					"url" => "reports/report_clone/?repr_id={$repr_id}"
				]
			];

			if($report_status_data['status'] == 0 || $report_status_data['status'] == 1)
			{
				$new_form_buttons[] = [
					"label" => "Cancel report",
					"url" => "reports/report_progress/?repr_id=".$repr_id."&action=cancel"
				];
			}
			elseif($report_status_data['status'] >= 3)
			{
				$new_form_buttons[] = [
					"label" => "Delete report",
					"url" => "reports/report_progress/?repr_id=".$repr_id."&action=delete"
				];
				$repr_available = ($report_status_data['status'] == 3);
			}

			$form_buttons = array_merge($new_form_buttons, $form_buttons);
		}

		//Get the HTML for the report status
		ob_start();
		$single_report_data_row = Reporting::displayReportStatus($rep_id, $repr_id);
		$reports_status_html = ob_get_clean();

		$single_report_status_html = "";
		if($single_report_data_row !== null)
		{
			ob_start();
			Reporting::displaySingleReportStatus($single_report_data_row);
			$single_report_status_html = ob_get_clean();
		}

		//Comment section for in the form
		$form_comment_section = [
			"report_comment_start" => [
				"label" => "Report comment",
				"type" => "fieldset_start"
			],
			"report_comment" => [
				"label" => "Comment",
				"type" => "text",
				"disabled" => $form_disabled,
				"suffix" => "<label class=\"required\">*</label>",
			],
			"report_comment_end" => [
				"type" => "fieldset_end"
			]
		];

		return [
			"single_report_status_html" => $single_report_status_html,
			"reports_status_html" => $reports_status_html,
			"form_disabled" => $form_disabled,
			"form_buttons" => $form_buttons,
			"form_comment_section" => $form_comment_section,
			"submit_button" => $submit_button,
			"rep_id" => $rep_id,
			"rep_heavy" => ($report_data['rep_heavy'] == 1 ? true : false),
			"repr_available" => $repr_available,
			"repr_id" => $repr_id
		];
	}



	/**
	 * Create a numberformatted <td> with a certain calculation
	 * @param array $values Array with values used in the calculation
	 * @param int $method The Method (Use the defined TD_CALC_*)
	 * @param string $prefix Possible prefix
	 * @param string $suffix Possible suffix
	 * @return string The TD
	 */
	public static function calculationTableTD($values, $method, $prefix = "", $suffix = "", $th = false)
	{
		$td = ($th ? "th" : "td");

		return "<".$td.">".$prefix.System::numberFormat(self::calculate($values, $method), 2).$suffix."</".$td.">";
	}

	public static function calculate($values, $method)
	{
		$result = 0;
		$value1 = $values[0];
		$value2 = $values[1];

		//Check if caluclation is possible
		if($value1 != 0 && $value2 != 0)
		{
			//Use the correct method
			if($method === TD_CALC_PER)
			{
				$value3 = 100;
				if(isset($values[2]))
				{
					$value3 = $values[2];
				}

				$result = ($value1 / $value2) * $value3;
			}
			elseif($method === TD_CALC_AVG)
			{
				$result = ($value1 / $value2);
			}
		}

		return $result;
	}

	/**
	 * Get the posted array. Returns an empty array if not posted or not an array.
	 * @param string $key The key in $_POST
	 * @param bool|true $remove_empty_strings Should remove all empty strings
	 * @return array The array
	 */
	public static function getPostedArray($key, $remove_empty_strings = true)
	{
		//Check if posted and an array
		if(!isset($_POST[$key]) || !is_array($_POST[$key]))
		{
			return [];
		}

		$arr = $_POST[$key];
		if($remove_empty_strings)
		{
			foreach($arr as $index => $value)
			{
				if(empty($value))
				{
					unset($arr[$index]);
				}
			}
		}

		return $arr;
	}

	/**
	 * Get the payment rates for multiple ktcupos
	 * @param array $ktcupo_ids Array containing the IDs
	 * @return array Resulting array with ID => Rate (Rate => null = not found)
	 */
	public static function paymentRates($ktcupo_ids)
	{
	    $system = new System();
		//Check if empty
		if(empty($ktcupo_ids))
		{
			return $ktcupo_ids;
		}

		//Create new array, ID => Rate
		$ktcupo_to_rate = [];
		foreach($ktcupo_ids as $ktcupo_id)
		{
			$ktcupo_to_rate[$ktcupo_id] = null;
		}

        $payment_rate_query = DB::table("payment_rates")
            ->whereIn("para_ktcupo_id", $ktcupo_ids)
            ->whereRaw("`para_date` <= NOW()")
            ->orderBy("para_date", 'desc')
            ->get();

		foreach($payment_rate_query as $payment_row)
		{
			//Check if already set
			$ktcupo_id = $payment_row->para_ktcupo_id;

			if($ktcupo_to_rate[$ktcupo_id] !== null)
			{
				continue;
			}

			$ktcupo_to_rate[$ktcupo_id] = $system->calculatePaymentRate($payment_row);
		}

		//Return the values
		return $ktcupo_to_rate;
	}

	/**
	 * Confirm the free trials of multiple ktcupos
	 * @param array $ktcupo_ids Array containing the IDs
	 * @return array Resulting array with ID => Free trial (Free trial => null = not found)
	 */
	public static function freeTrials($ktcupo_ids)
	{
		//Check if empty
		if(empty($ktcupo_ids))
		{
			return $ktcupo_ids;
		}

		//Create new array, ID => Rate
		$ktcupo_to_free_trial = [];
		foreach($ktcupo_ids as $ktcupo_id)
		{
			$ktcupo_to_free_trial[$ktcupo_id] = null;
		}

        $query_free_trial = DB::table("free_trials")
            ->select("frtr_ktcupo_id", "frtr_finished")
            ->whereIn("frtr_ktcupo_id", $ktcupo_ids)
            ->whereRaw("`frtr_date` <= NOW()")
            ->orderBy("frtr_date","desc")
            ->get();

		foreach($query_free_trial as $free_trial_row)
		{
			//Check if already set
			$ktcupo_id = $free_trial_row->frtr_ktcupo_id;

			if($ktcupo_to_free_trial[$ktcupo_id] !== null)
			{
				continue;
			}

			//Confirm the free trial
			$ktcupo_to_free_trial[$ktcupo_id] = ($free_trial_row->frtr_finished == 0 ? true : false);
		}

		//Return the values
		return $ktcupo_to_free_trial;
	}

	/**
	 * Gather all regions -> regions & all customers on those regions
	 * @param int $id repr_id
	 * @param array $progress Current progress array
	 * @param bool|true $free_trial Include free trials
	 * @param bool|false $cappings Include cappings
	 * @param array $ktcupo_status Status of ktcupo
	 */
	public static function allRegionsToRegions($id, &$progress, $free_trial = true, $cappings = false, $ktcupo_status = [])
	{
	    $system = new System();

		Reporting::addProgress($id, $progress, "Gathering all regions -> regions");

		//Get all regions
		$region_to_name = [];
		$region_results = [];
		$national_regions = [];
		$international_regions = [];

        $region_query = DB::table("regions")
            ->select( "reg_id", "reg_name", "reg_destination_type", "reg_co_code" )
            ->where("reg_co_code", "!=", "US")
            ->orderBy("reg_name", 'asc')
            ->get();

		foreach($region_query as $region_row)
		{
			$region_to_name[$region_row->reg_id] = $region_row->reg_name;
			if($region_row->reg_destination_type == 1)
			{
				$international_regions[] = $region_row->reg_id;
			}
			else
			{
				if(!isset($national_regions[$region_row->reg_co_code]))
				{
					$national_regions[$region_row->reg_co_code] = [];
				}
				$national_regions[$region_row->reg_co_code][] = $region_row->reg_id;
			}
		}

		//Set international regions
		foreach($international_regions as $reg_id)
		{
			foreach($international_regions as $reg_id_to)
			{
				$region_results[$reg_id][$reg_id_to] = [
					RD_REQUESTS => 0,
					RD_IMPORT_CUSTOMERS => [],
					RD_EXPORT_CUSTOMERS => []
				];
			}
		}


		//Set national regions
		foreach($national_regions as $country => $reg_ids)
		{
			foreach($reg_ids as $reg_id)
			{
				foreach($reg_ids as $reg_id_to)
				{
					$region_results[$reg_id][$reg_id_to] = [
						RD_REQUESTS => 0,
						RD_IMPORT_CUSTOMERS => [],
						RD_EXPORT_CUSTOMERS => []
					];
				}
			}
		}

		//Get all countries -> Regions
		Reporting::checkIfStopped($id);
		Reporting::addProgress($id, $progress, "Gathering customers");

		//Get all customers
		$customer_results = [];
		$ktcupo_free_trials = [];
		$ktcupo_to_customer = [];

        $customer_query_builder = DB::table("customers")
            ->select(
                "cu_id", "cu_company_name_business",
                "po_id", "po_destination_type", "po_pacu_code",
                "ktcupo_id", "ktcupo_min_volume_m3", "ktcupo_min_volume_ft3",
                "ktcupo_export_moves", "ktcupo_import_moves", "ktcupo_business_only", "ktcupo_request_type",
                "ktcupo_moving_size_1", "ktcupo_moving_size_2", "ktcupo_moving_size_3", "ktcupo_moving_size_4",
                "ktcupo_moving_size_5", "ktcupo_moving_size_6", "ktcupo_moving_size_7", "ktcupo_moving_size_8"
            )
            ->leftJoin("kt_customer_portal", "cu_id", "ktcupo_cu_id")
            ->leftJoin("portals", "po_id", "ktcupo_po_id")
            ->leftJoin("mover_data", "cu_id", "moda_cu_id")
            ->whereRaw("(cu_type = 1 OR cu_type = 6)")
            ->where("cu_credit_hold", 0)
            ->where("cu_deleted", 0)
            ->whereIn("ktcupo_status", $ktcupo_status)
            ->where("po_portal", "!=", "INTMOVING-US")
            ->where("po_portal", "!=", "NATMOVING-US");

        if($free_trial)
        {
            $customer_query_builder->addSelect("ktcupo_free_trial");
        }
        else
        {
            $customer_query_builder->where("ktcupo_free_trial", 0);
        }

        if($cappings)
        {
            $customer_query_builder->addSelect("ktcupo_max_requests_month","ktcupo_max_requests_day","moda_capping_method");
        }

        if(count($ktcupo_status) !== 1)
        {
            $customer_query_builder->addSelect("ktcupo_status");
        }

        $customer_query_builder = $customer_query_builder->get();

        $customer_query = $system->databaseToArray($customer_query_builder);

		foreach($customer_query as $customer_row)
		{
			//Add to results
			$cu_id = $customer_row['cu_id'];
			if(!isset($customer_results[$cu_id]))
			{
				$customer_results[$cu_id] = [
					CU_NAME => $customer_row['cu_company_name_business'],
					CU_PORTALS => []
				];
			}

			//Add portal
			$ktcupo_id = $customer_row['ktcupo_id'];
			$ktcupo_data = [
				CUPO_PAYMENT_RATE => 0.0,
				CUPO_PACU_CODE => $customer_row['po_pacu_code'],
				CUPO_PO_ID => $customer_row['po_id'],
				CUPO_MIN_VOLUME_M3 => $customer_row['ktcupo_min_volume_m3'],
				CUPO_MIN_VOLUME_FT3 => $customer_row['ktcupo_min_volume_ft3'],
				CUPO_REQUEST_TYPE => $customer_row['ktcupo_request_type'],
				CUPO_EXPORT_MOVES => $customer_row['ktcupo_export_moves'],
				CUPO_IMPORT_MOVES => $customer_row['ktcupo_import_moves'],
				CUPO_BUSINESS_ONLY => $customer_row['ktcupo_business_only'],
				CUPO_MOVING_SIZE => [
					1 => (bool)$customer_row['ktcupo_moving_size_1'],
					2 => (bool)$customer_row['ktcupo_moving_size_2'],
					3 => (bool)$customer_row['ktcupo_moving_size_3'],
					4 => (bool)$customer_row['ktcupo_moving_size_4'],
					5 => (bool)$customer_row['ktcupo_moving_size_5'],
					6 => (bool)$customer_row['ktcupo_moving_size_6'],
					7 => (bool)$customer_row['ktcupo_moving_size_7'],
					8 => (bool)$customer_row['ktcupo_moving_size_8']
				],
				//CUPO_QUALITY_SCORE => $customer_row['ktcupo_quality_score'],
				//CUPO_QUALITY_SCORE_OVERRIDE => $currency_row['ktcupo_quality_score_override'],
				CUPO_FROM_REGIONS => [],
				CUPO_TO_REGIONS => []
			];

			if($free_trial)
			{
				$ktcupo_data[CUPO_FREE_TRIAL] = $customer_row['ktcupo_free_trial'];
			}

			if($cappings)
			{
				$ktcupo_data[CUPO_CAPPINGS_DAILY] = $customer_row['ktcupo_max_requests_day'];
				$ktcupo_data[CUPO_CAPPINGS_MONTHLY] = $customer_row['ktcupo_max_requests_month'];
				$ktcupo_data[MODA_CAPPINGS_METHOD] = $customer_row['moda_capping_method'];
			}

			if(count($ktcupo_status) !== 1)
			{
				$ktcupo_data[CUPO_STATUS] = $customer_row['ktcupo_status'];
			}

			$customer_results[$cu_id][CU_PORTALS][$ktcupo_id] = $ktcupo_data;
			$ktcupo_to_customer[$ktcupo_id] = $cu_id;

			if($free_trial && $customer_row['ktcupo_free_trial'] == 1)
			{
				$ktcupo_free_trials[$ktcupo_id] = $ktcupo_id;
			}
		}

		Reporting::checkIfStopped($id);
		Reporting::addProgress($id, $progress, "Gathering customers -> regions");

        $cu_from_query = DB::table("kt_customer_portal_region")
            ->select( "ktcupore_ktcupo_id", "ktcupore_reg_id")
            ->whereIn("ktcupore_ktcupo_id", array_keys($ktcupo_to_customer))
            ->get();

		foreach($cu_from_query as $cu_from_row)
		{
			$reg_id = $cu_from_row->ktcupore_reg_id;
			if(!isset($region_results[$reg_id]))
			{
				continue;
			}

			$ktcupo_id = $cu_from_row->ktcupore_ktcupo_id;
			$cu_id = $ktcupo_to_customer[$ktcupo_id];

			$customer_results[$cu_id][CU_PORTALS][$ktcupo_id][CUPO_FROM_REGIONS][] = $reg_id;
		}

        $cu_from_query = DB::table("kt_customer_portal_country")
            ->select( "ktcupoco_ktcupo_id", "reg_id")
			->leftJoin("regions","reg_co_code","ktcupoco_co_code")
            ->where("reg_destination_type",1)
            ->whereIn("ktcupoco_ktcupo_id", array_keys($ktcupo_to_customer))
            ->get();

		foreach($cu_from_query as $cu_to_row)
		{
			$reg_id = $cu_to_row->reg_id;
			if(!isset($region_results[$reg_id]))
			{
				continue;
			}

			$ktcupo_id = $cu_to_row->ktcupoco_ktcupo_id;
			$cu_id = $ktcupo_to_customer[$ktcupo_id];

			$customer_results[$cu_id][CU_PORTALS][$ktcupo_id][CUPO_TO_REGIONS][] = $reg_id;
		}

        $cu_to_nat_query = DB::table("kt_customer_portal_region_destination")
            ->select( "ktcuporede_ktcupo_id", "ktcuporede_reg_id")
            ->whereIn("ktcuporede_ktcupo_id",array_keys($ktcupo_to_customer))
            ->get();

		foreach($cu_to_nat_query as $cu_to_nat_row)
		{
			$ktcupo_id = $cu_to_nat_row->ktcuporede_ktcupo_id;
			$cu_id = $ktcupo_to_customer[$ktcupo_id];
			$reg_id = $cu_to_nat_row->ktcuporede_reg_id;
			$customer_results[$cu_id][CU_PORTALS][$ktcupo_id][CUPO_TO_REGIONS][] = $reg_id;
		}

		Reporting::checkIfStopped($id);
		Reporting::addProgress($id, $progress, "Checking free trials");


		//Get the free trials
		$free_trials = Reporting::freeTrials($ktcupo_free_trials);
		foreach($free_trials as $ktcupo_id => $is_free_trial)
		{
			if($is_free_trial === null || $is_free_trial === false)
			{
				//Invalid free trial
				unset($customer_results[$ktcupo_to_customer[$ktcupo_id]][CU_PORTALS][$ktcupo_id]);
				unset($ktcupo_to_customer[$ktcupo_id]);
			}
		}

		Reporting::addProgress($id, $progress, "Gathering & converting payment rates");

		//Get the payment rates
		$payment_rates = Reporting::paymentRates(array_keys($ktcupo_to_customer));
		foreach($payment_rates as $ktcupo_id => $rate)
		{
			if($rate === null)
			{
				$rate = 10;
			}
			$customer_results[$ktcupo_to_customer[$ktcupo_id]][CU_PORTALS][$ktcupo_id][CUPO_PAYMENT_RATE] = $rate;
		}

		//Get the currencies
		$currencies = [];

        $currencies_query = DB::table("payment_currencies")
            ->select( "pacu_code", "pacu_rate")
            ->get();

		foreach($currencies_query as $currency_row)
		{
			$currencies[$currency_row->pacu_code] = $currency_row->pacu_rate;
		}

		//=> Convert the rates
		foreach($customer_results as $cu_id => &$cu_data)
		{
			foreach($cu_data[CU_PORTALS] as $ktcupo_id => &$ktcupo_data)
			{
				$old_value = $ktcupo_data[CUPO_PAYMENT_RATE];
				$new_value = (($old_value / $currencies[$ktcupo_data[CUPO_PACU_CODE]]) * $currencies["EUR"]);
				$ktcupo_data[CUPO_PAYMENT_RATE] = round($new_value, 2);
				unset($ktcupo_data);
			}
			unset($cu_data);
		}

		Reporting::checkIfStopped($id);
		Reporting::addProgress($id, $progress, "Map customers on regions");

		//Map to region results
		foreach($customer_results as $cu_id => $cu_data)
		{
			foreach($cu_data[CU_PORTALS] as $ktcupo_id => $ktcupo_data)
			{
				$types = [
					0 => [CUPO_EXPORT_MOVES, CUPO_FROM_REGIONS, CUPO_TO_REGIONS, RD_EXPORT_CUSTOMERS],
					1 => [CUPO_IMPORT_MOVES, CUPO_TO_REGIONS, CUPO_FROM_REGIONS, RD_IMPORT_CUSTOMERS]
				];
				foreach($types as $type)
				{
					if($ktcupo_data[$type[0]])
					{
						foreach($ktcupo_data[$type[1]] as $from_reg_id)
						{
							$reg_data = &$region_results[$from_reg_id];
							foreach($ktcupo_data[$type[2]] as $to_reg_id)
							{
								$region_results[$from_reg_id][$to_reg_id][$type[3]][$ktcupo_id] = $cu_id;
							}
							unset($reg_data);
						}
					}
				}
			}
		}

		Reporting::checkIfStopped($id);
		Reporting::addProgress($id, $progress, "Done mapping customers on regions");


		$all_results = [
			"customer_results" => &$customer_results,
			"region_to_name" => &$region_to_name,
			"region_results" => &$region_results
		];

		return $all_results;
	}

	public function getAllGeneratedReportsByReportId($report_id, $exclude = null)
    {
        $repr_id = $exclude;

        $return_row = null;
        $html = "";
		$rows_printed = 0;
		$rep_name = Report::select("rep_report")->where("rep_id", $report_id)->first()->rep_report;

        $report_query = DB::table("report_progress")
            ->select("us_name", "repr_id", "repr_comment", "repr_create_timestamp", "repr_start_timestamp", "repr_finish_timestamp", "repr_status",  "repr_progress_array")
            ->leftJoin("users", "repr_us_id", "us_id")
            ->where("repr_rep_id", $report_id)
            ->where("repr_status", "!=", ReportProgressStatus::DELETED)
            ->whereRaw("ADDDATE(`repr_create_timestamp`, INTERVAL 24 HOUR) > NOW()")
            ->get();

        $html .= "<div class='row'>";
        if (count($report_query) > 0)
        {
            foreach ($report_query as $report_row)
            {
                if($repr_id != null && $report_row->repr_id == $repr_id)
				{
					$return_row = $report_row;
					continue;
				}
                $rows_printed++;

                $html .= "<div class='col-sm-4'>";
				$html .= "Report #".$report_row->repr_id."(".DataIndex::reportProgressStatus()[$report_row->repr_status].")";
				$html .= "<p>";
				$html .= "Created by: ".$report_row->us_name."<br />";
				$html .= "Comment: ".$report_row->repr_comment."<br />";

				if($report_row->repr_status != 0)
				{
					$html .= "Started at: ".$report_row->repr_start_timestamp."<br />";
				}

				if($report_row->repr_status >= 3)
				{
					$html .= "Finished at: ".$report_row->repr_finish_timestamp."<br />";
				}

				$html .= "</p>";
				$html .= "<p>";
				$html .= "<a href=\"/reports/".$report_id."/view/".$rep_name."/".$report_row->repr_id."/view\" class=\"btn btn-primary\">View</a>";

				if($report_row->repr_status == 0 || $report_row->repr_status == 1)
				{
					$html .= " <a href=\"/reports/report_progress/".$report_row->repr_id."/cancel\" class=\"btn btn-warning\">Cancel</a>";
				}
				elseif($report_row->repr_status >= 3)
				{
					$html .= " <a href=\"/reports/report_progress/".$report_row->repr_id."/delete\" class=\"btn btn-danger\">Delete</a>";
				}
				$html .= "</p>";

				$html .= "</div>";
            }
        }

        if($rows_printed === 0)
		{
			$html .= "There are currently no other reports.";
		}

		$html .= "</div>";

		return $html;
    }
}
