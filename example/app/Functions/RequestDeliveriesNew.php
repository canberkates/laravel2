<?php


namespace App\Functions;

use App\Data\CustomerRequestDeliveryTypeAndFile;
use App\RequestDeliveries\Deliveries\HTTPDelivery;
use App\RequestDeliveries\Deliveries\PlainTextEmailDelivery;
use App\RequestDeliveries\Deliveries\SalesForceDelivery;
use App\RequestDeliveries\MoveWare;
use GuzzleHttp\Client;

/**
 * Class RequestDeliveriesNew
 * @package App\Functions
 */
class RequestDeliveriesNew
{
    /**
     * @param $delivery_type
     * @param string $language
     * @param null $fields
     */
    public function send($delivery_type, $language = "EN", $fields = null)
    {
        //Get standard lead translations for deliveries
        $translation = new Translation();
        $translations = $translation->getMultipleInLanguage(
            [
                "moving_details",
                "request_type",
                "moving_size",
                "request_type_val_" . $fields['request']['re_request_type'],
                "moving_size_val_" . $fields['request']['re_moving_size'],
                "moving_date",
                "volume_m3",
                "volume_ft3",
                "storage",
                "packing",
                "assembly",
                "business",
                "storage_val_" . $fields['request']['re_storage'],
                "self_company_val_" . $fields['request']['re_packing'],
                "self_company_val_" . $fields['request']['re_assembly'],
                "moving_from",
                "street",
                "zipcode",
                "city",
                "country",
                "region",
                "residence",
                "contact_details",
                "company_name",
                "first_name",
                "family_name",
                "telephone1",
                "telephone2",
                "email",
                "remarks",
                "customer_request_subject_" . $fields['match_type'],
            ],
            $language
        );

        //Calls the dynamic method as set in the CustomerRequestDeliveryTypeAndFile Class
        $call = CustomerRequestDeliveryTypeAndFile::name($delivery_type)[1];
        self::$call($language, $fields, $translations);


        $client = new Client();
        $send = new MoveWare($client);
        $this->getContainer()->get(MoveWare::class);
    }

    /**
     * @param $language
     * @param $fields
     * @param $translations
     */
    private function sendHTTP($fields, $language, $translations)
    {
        $http = new HTTPDelivery();
        $http->sendRequest($fields, $language, $translations);
    }

    /**
     * @param $language
     * @param $fields
     * @param $translations
     */
    private function sendSalesForce($fields, $language, $translations)
    {
        $salesforce = new SalesForceDelivery();
        $salesforce->sendRequest($fields, $language, $translations);
    }

    /**
     * @param $language
     * @param $fields
     * @param $translations
     */
    private function sendPlainTextEmail($fields, $language, $translations)
    {
        $plaintext = new PlainTextEmailDelivery();
        $plaintext->sendRequest($fields, $language, $translations);
    }
}
