<?php


namespace App\Functions;


use App\Models\Language;
use App\Models\Portal;
use DB;
use Log;

class PortalData
{
	public function nameAndCountryCode($po_id)
	{
		$portal = Portal::find($po_id);
		
		if(!empty($portal))
		{
			return $portal->po_portal.((!empty($portal->po_co_code)) ? " (".self::country($portal->po_co_code).")" : "");
		}
		else
		{
			return false;
		}
	}
	
	public function country($country_code, $language = 'en')
	{
		$country_column = "co_".strtolower($language);
		
		$result = DB::table("countries")
			->select($country_column)
			->where("co_code", $country_code)
			->first();
		
		if(!empty($result))
		{
			return $result->$country_column;
		}
		else
		{
			return false;
		}
	}
	
	public function getLanguage($la_code)
	{
		$language = Language::where("la_code", $la_code)->first();
		
		if(!empty($language))
		{
			return $language->la_language;
		}
		else
		{
			return false;
		}
	}
	
}