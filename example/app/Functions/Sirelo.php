<?php
/**
 * Created by PhpStorm.
 * User: Lorenzo
 * Date: 07/02/2019
 * Time: 15:47
 */

namespace App\Functions;


use Illuminate\Support\Facades\DB;

class Sirelo
{
	public function getSireloFieldByCountry( $country, $field )
	{
		$sirelo_field =
			DB::table('websites')
				->select($field)
				->where('we_sirelo', 1)
				->where('we_sirelo_co_code', $country)
				->first();
		
		if( empty( $sirelo_field ) ) {
			
			// The fields that are different in the subdomains table
			$subdomain_fields = [
				
				'we_sirelo_url'					=> 'wesu_url',
				'we_sirelo_customer_location'	=> 'wesu_customer_location',
				'we_sirelo_la_code'				=> 'wesu_default_language',
				'we_sirelo_survey_location'		=> 'wesu_survey_location',
				
				// Get website name from main site
				'we_website'					=> 'we_website',
				
				// Get mail from name from main site
				'we_mail_from_name'				=> 'we_mail_from_name',
			];
			
			// If the key exists
			if( array_key_exists( $field, $subdomain_fields ) ) {
				
				// Change field
				$field = $subdomain_fields[$field];
				
				// Search for the different field in the subdomains table
				$sirelo_field =
					DB::table('website_subdomains')
						->select($field)
						->join("websites", "wesu_we_id", "=", "we_id")
						->where('wesu_country_code', $country)
						->where('we_sirelo', 1)
						->first();
				
			}
		}
		
		if( ! empty( $sirelo_field ) ) {
			// Return field
			return $sirelo_field->$field;
			
		}else{
			
			return false;
		}
	}
}