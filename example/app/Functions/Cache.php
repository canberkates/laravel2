<?php
/**
 * Created by PhpStorm.
 * User: Arjan
 * Date: 26/06/2019
 * Time: 14:36
 */

namespace App\Functions;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Cache
{
	public static function get($name)
	{
		$result = \App\Models\Cache::where("ca_name", "=", $name)->first();
		
		if(count($result) > 0)
		{
			return [
				"timestamp" => $result->ca_timestamp,
				"value" => $result->ca_value
			];
		}
	}

	public static function update($name, $value)
    {
        $cache = \App\Models\Cache::select("ca_name")
            ->where("ca_name", $name)
            ->first();

        if(count($cache) == 0)
        {
            $cache = new \App\Models\Cache();
            $cache->ca_name = $name;
            $cache->save();
        }

        DB::table('cache')
            ->where('ca_name', $name)
            ->update([
                'ca_value' => $value,
                'ca_timestamp' => date("Y-m-d H:i:s")
            ]);

    }
}