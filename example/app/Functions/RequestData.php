<?php
/**
 * Created by PhpStorm.
 * User: Lorenzo
 * Date: 19/03/2019
 * Time: 10:54
 */

namespace App\Functions;


use App\Data\CustomerType;
use App\Data\DestinationType;
use App\Data\MoverCappingMethod;
use App\Data\NatPaymentType;
use App\Data\QualityScoreOverride;
use App\Data\RejectionReason;
use App\Data\RequestSource;
use App\Data\RequestStatus;
use App\Models\AffiliatePartnerForm;
use App\Models\Country;
use App\Models\Customer;
use App\Models\KTCustomerPortal;
use App\Models\KTRequestCustomerPortal;
use App\Models\MatchStatistic;
use App\Models\MoverData;
use App\Models\QualityScoreCalculation;
use App\Models\Region;
use App\Models\Request;
use App\Models\SystemSetting;
use Exception;
use Illuminate\Support\Facades\DB;
use Log;

class RequestData
{
    public function getQuestions($request)
    {
        $system = new System();
        $language = "EN";
        //Decide if it's a national or international move
        if ($request->re_co_code_from != $request->re_co_code_to)
        {
            $where_field = "ktcuqu_international_moves";
        } else
        {
            $where_field = "ktcuqu_national_moves";
        }

        $question = DB::table("kt_customer_question")
            ->select("*")
            ->join("customers", "ktcuqu_cu_id", "=", "cu_id")
            ->join("questions", "ktcuqu_qu_id", "=", "qu_id")
            ->join("kt_customer_question_language", "ktcuqu_id", "=", "ktcuqula_ktcuqu_id")
            ->join("kt_customer_question_country_origin", "ktcuqu_id", "=", "ktcuqucoor_ktcuqu_id")
            ->join("kt_customer_question_country_destination", "ktcuqu_id", "=", "ktcuqucode_ktcuqu_id")
            ->where('ktcuqu_status', "=", 1)
            ->where($where_field, "=", 1)
            ->where('ktcuqu_max_requests_month_left', ">", 0)
            ->where('ktcuqu_max_requests_day_left', ">", 0)
            ->where('cu_credit_hold', "=", 0)
            ->where('cu_deleted', "=", 0)
            ->where('qu_limit', ">", 0)
            ->where('ktcuqula_la_code', $request->re_la_code)
            ->where('ktcuqucoor_co_code', $request->re_co_code_from)
            ->where('ktcuqucode_co_code', $request->re_co_code_to)
            ->orderBy("qu_name", "asc")
            ->groupBy("qu_id")
            ->get();

        foreach ($question as $row)
        {
            $double_1 = DB::table("kt_request_customer_question")
                ->select("ktrecuqu_id")
                ->where("ktrecuqu_re_id", $request->re_id)
                ->where("ktrecuqu_qu_id", $row->qu_id)
                ->get();

            $double_2 = DB::table("kt_request_customer_question")
                ->select("ktrecuqu_id")
                ->leftJoin("requests", "ktrecuqu_re_id", "re_id")
                ->where("ktrecuqu_qu_id", $row->qu_id)
                ->where("ktrecuqu_hide", 0)
                ->where("re_id", "!=", $request->re_id)
                ->whereRaw("`re_timestamp` > NOW() - INTERVAL 3 MONTH")
                ->where("re_email", $request->re_email)
                ->where("re_email", "!=", "")
                ->get();


            if (!$double_2->count())
            {
                $extra_1_options = explode(";", $system->unserialize($row->qu_extra_1_options)[$language]);
                $extra_2_options = explode(";", $system->unserialize($row->qu_extra_2_options)[$language]);

                $questions[$row->qu_id] = [
                    "name" => $row->qu_name,
                    "title" => $system->unserialize($row->qu_title)[$language],
                    "question" => $system->unserialize($row->qu_question)[$language],
                    "class" => $row->qu_class,
                    "extra_1_type" => $row->qu_extra_1_type,
                    "extra_1_question" => $system->unserialize($row->qu_extra_1_question)[$language],
                    "extra_1_options" => (($row->qu_extra_1_type == 2) ? ["" => "- " . "Choose" . " -"] + array_combine($extra_1_options, $extra_1_options) : ""),
                    "extra_1_class" => $row->qu_extra_1_class_back,
                    "extra_2_type" => $row->qu_extra_2_type,
                    "extra_2_question" => $system->unserialize($row->qu_extra_2_question)[$language],
                    "extra_2_options" => (($row->qu_extra_2_type == 2) ? ["" => "- " . "Choose" . " -"] + array_combine($extra_2_options, $extra_2_options) : ""),
                    "extra_2_class" => $row->qu_extra_2_class_back,
                    "double" => ((sizeof($double_1)) ? 1 : 0)
                ];
            }
        }

        return $questions ?? [];
    }

    /**
     * Get the number of matches done for a request.
     * @param int $request_id The request ID
     * @return int number of matches
     */
    public function requestMatchesDone($request_id)
    {
        return self::multipleRequestsMatchesDone([$request_id])[$request_id];
    }

    /**
     * Get the number of matches done for one or more requests.
     * @param array $request_ids Array containing the re_ids
     * @return array re_id => matches
     */
    public function multipleRequestsMatchesDone($request_ids)
    {
        if (empty($request_ids))
        {
            return [];
        }

        $amount = DB::table("kt_request_customer_portal")
            ->select("ktrecupo_re_id")
            ->selectRaw("COUNT(*) AS `matches`")
            ->where('ktrecupo_free', "=", 0)
            ->whereIn("ktrecupo_re_id", $request_ids)
            ->groupBy("ktrecupo_re_id")
            ->get();

        $result = [];
        foreach ($request_ids as $request_id)
        {
            $result[$request_id] = false;
        }
        foreach ($amount as $row)
        {
            $result[$row->ktrecupo_re_id] = $row->matches;
        }

        return $result;
    }

    /**
     * Get the number of matches left for a request.
     * @param int $request_id The request ID
     * @return int|false
     */
    public function requestMatchesLeft($request_id)
    {
        return self::multipleRequestsMatchesLeft([$request_id])[$request_id];
    }

    public function multipleRequestsMatchesLeft($request_ids)
    {
        if (empty($request_ids))
        {
            return [];
        }

        $matches_per_request = SystemSetting::where("syse_setting", "matches_per_request")->first()->syse_value;
        $matches_done = self::multipleRequestsMatchesDone($request_ids);

        $amount = DB::table("requests")
            ->select("re_id")
            ->whereIn("re_id", $request_ids)
            ->get();

        $result = [];
        foreach ($request_ids as $request_id)
        {
            $result[$request_id] = false;
        }

        foreach ($amount as $row)
        {
            $re_id = $row->re_id;
            $result[$re_id] = $matches_per_request - $matches_done[$re_id] ?? 0;
        }

        return $result;
    }

    public function getRegions($country, $destination_type)
    {

        $regions =
            DB::table("regions")
                ->select("reg_id", "reg_name", "reg_parent")
                ->where('reg_co_code', "=", $country)
                ->where('reg_destination_type', "=", $destination_type)
                ->where('reg_deleted', "=", 0)
                ->get();

        return $regions;
    }

    public function matchLeadBetter(Request $request)
    {
        $system = new System();
        $mover = new Mover();

        $matches_left = $this->requestMatchesLeft($request->re_id);

        //Lower amount of matches left by 1 as the lead will be 100% be matched to the Sirelo Customer
        if ($request->re_sirelo_customer)
        {
            $matches_left--;
        }

        $exclude_db = SystemSetting::where("syse_setting", "match_exclusions")->first()->syse_value;
        $excluded_from_each_other = System::unserialize($exclude_db);

        //Exclude everone that this lead was matched to already
        $customer_exclude = $this->ExcludeCustomers($request->re_id);

        if (isset($request->re_sirelo_customer))
        {
            $customer_exclude[] = $request->re_sirelo_customer;
        }

        //Exclude Resellers except LeadGrab (22642)
        if($request->re_source == RequestSource::AFFILIATE){
            $affiliate_partner_id = AffiliatePartnerForm::where("afpafo_id", $request->re_afpafo_id)->first()->afpafo_cu_id;
            $resellers = Customer::where("cu_type", 6)->where("cu_id", "!=", 22642)->get();
            foreach($resellers as $reseller){
                $customer_exclude[] = $reseller->cu_id;
            }
        }

        $query_customers = DB::table("customers")
            ->select("cu_id", "cu_company_name_business", "moda_capping_method", "moda_capping_daily_average", "moda_capping_bucket" , "moda_capping_monthly_limit_left", "moda_forced_daily_capping" , "moda_forced_daily_capping_left" ,"ktcupo_daily_average" , "ktcupo_max_requests_month_left", "ktcupo_max_requests_day_left","ktcupo_requests_bucket","moda_capping_monthly_limit_left", "ktcupo_id", "ktcupo_free_trial", "moda_quality_score", "moda_quality_score_override", "po_id")
            ->leftjoin("kt_customer_portal", "cu_id", "ktcupo_cu_id")
            ->leftjoin("kt_customer_portal_region", "ktcupo_id", "ktcupore_ktcupo_id")
            ->leftjoin("kt_customer_portal_country", "ktcupo_id", "ktcupoco_ktcupo_id")
            ->leftjoin("kt_customer_portal_region_destination", "ktcupo_id", "ktcuporede_ktcupo_id")
            ->leftjoin("regions", "ktcupo_po_id", "reg_po_id")
            ->leftjoin("portals", "ktcupo_po_id", "po_id")
            ->leftjoin("mover_data", "cu_id", "moda_cu_id")
            ->where("cu_credit_hold", 0)
            ->where("cu_deleted", 0)
            ->where("ktcupo_request_type", $request->re_request_type)
            ->where("ktcupo_status", 1)
            ->where("po_destination_type", $request->re_destination_type);

        if($request->re_room_size){
            //Roomsizes above 4 are matched to portals with room size 4+
            $convert = $request->re_room_size > 4 ? 4 : $request->re_room_size;

            $query_customers
                ->where(function ($query) use ($request, $convert ) {
                    $query
                        ->where("ktcupo_moving_size_" . $request->re_moving_size, 1)
                        ->orWhere("ktcupo_room_size_" . $convert, 1);
                });

        }else{
            $query_customers->where("ktcupo_moving_size_" . $request->re_moving_size, 1);
        }

        //Do not sell affiliate leads to resellers, exception for MoverDB
        if ($request->re_source == RequestSource::AFFILIATE && $affiliate_partner_id != 12637)
        {
            $query_customers->where("cu_type", CustomerType::MOVER);
        } else
        {
            $query_customers->whereIn("cu_type", [CustomerType::MOVER, CustomerType::LEAD_RESELLER]);
        }

        //Exclude customers
        if ($customer_exclude)
        {
            $query_customers->whereNotIn("cu_id", $customer_exclude);
        }

        //If this lead has already been matched once, filter out all customers that can only get a zero-matched lead
        if ($this->requestMatchesDone($request->re_id) > 0)
        {
            $query_customers->where("moda_quality_score_override", "!=", QualityScoreOverride::EXCLUDED_ONLY_MATCH_POSSIBLE);
        }

        //Depending on INT or NAT, find portals with these regions active
        if ($request->re_destination_type == DestinationType::INTMOVING)
        {

            //Find a portal with these regions as EXPORT or as IMPORT
            $query_customers->where(function ($query) use ($request) {

                //Find a portal with these regions based on EXPORT
                $query->orwhere(function ($query) use ($request) {
                    $query
                        ->where('ktcupore_reg_id', '=', $request->re_reg_id_from)
                        ->where('ktcupoco_co_code', '=', $request->re_co_code_to)
                        ->Where('ktcupo_export_moves', '=', 1);
                });

                //Find a portal with these regions based on IMPORT
                $query->orwhere(function ($query) use ($request) {
                    $query
                        ->where('ktcupore_reg_id', '=', $request->re_reg_id_to)
                        ->where('ktcupoco_co_code', '=', $request->re_co_code_from)
                        ->Where('ktcupo_import_moves', '=', 1);
                });
            });
        } elseif ($request->re_destination_type == DestinationType::NATMOVING)
        {

            //Find a portal with these regions as EXPORT or as IMPORT
            $query_customers->where(function ($query) use ($request) {

                //Find a portal with these regions based on EXPORT
                $query->orwhere(function ($query) use ($request) {
                    $query
                        ->where('ktcupore_reg_id', '=', $request->re_reg_id_from)
                        ->where('ktcuporede_reg_id', '=', $request->re_reg_id_to)
                        ->Where('ktcupo_export_moves', '=', 1);
                });

                //Find a portal with these regions based on IMPORT
                $query->orwhere(function ($query) use ($request) {
                    $query
                        ->where('ktcupore_reg_id', '=', $request->re_reg_id_to)
                        ->where('ktcuporede_reg_id', '=', $request->re_reg_id_from)
                        ->Where('ktcupo_import_moves', '=', 1);
                });
            });

            $parent_from = Region::select("reg_parent")->where("reg_id", $request->re_reg_id_from)->first()->reg_parent;
            $parent_to = Region::select("reg_parent")->where("reg_id", $request->re_reg_id_to)->first()->reg_parent;

            if($parent_from == $parent_to){
                $query_customers->where("ktcupo_nat_local_moves", 1);
            }else{
                $query_customers->where("ktcupo_nat_long_distance_moves", 1);
            }
        }

        //DISABLE REMOVING CAPPING CUSTOMERS IN QUERY

        //Check the capping method and corresponding capping values
       /* $query_customers->where(function ($query) use ($request) {

            //Check the capping on PORTAL-level
            $query->orwhere(function ($query) use ($request) {
                $query
                    ->where('moda_capping_method', '=', MoverCappingMethod::PORTAL_LEVEL)
                    ->where('ktcupo_max_requests_month_left', '>', 0)
                    ->where('ktcupo_max_requests_day_left', '>', 0)
                    ->whereRaw('ktcupo_requests_bucket < ((`ktcupo_daily_average` + SQRT(`ktcupo_daily_average`)) * 1.05)');
            });

            //Check the capping on CUSTOMER-level
            $query->orwhere(function ($query) use ($request) {
                $query
                    ->whereIn('moda_capping_method', [MoverCappingMethod::MONTHLY_LEADS, MoverCappingMethod::MONTHLY_SPEND])
                    ->where('ktcupo_max_requests_day_left', '>', 0)
                    ->where('moda_capping_monthly_limit_left', '>', 0)
                    ->whereRaw('moda_capping_bucket < ((`moda_capping_daily_average` + SQRT(`moda_capping_daily_average`)) * 1.05)');
            });
        });*/

        //Check whether the selected portals do the correct export or import
        $query_customers->where(function ($query) use ($request) {

            $query->orwhere(function ($query) use ($request) {
                $query
                    ->where('reg_id', '=', $request->re_reg_id_from)
                    ->where('ktcupo_export_moves', '=', 1);
            });

            $query->orwhere(function ($query) use ($request) {
                $query
                    ->where('reg_id', '=', $request->re_reg_id_to)
                    ->where('ktcupo_import_moves', '=', 1);
            });
        });

        $query_customers = $query_customers
            ->groupBy("cu_id")
            ->orderBy("cu_company_name_business")
            ->get();

        //Log::debug($query_customers);

        $monthly = $query_customers->whereIn('moda_capping_method', [MoverCappingMethod::MONTHLY_LEADS, MoverCappingMethod::MONTHLY_SPEND]);

        DB::beginTransaction();

        foreach($monthly as $month){
            if(
                $month->moda_capping_bucket > (($month->moda_capping_daily_average + sqrt($month->moda_capping_daily_average)) * 1.05) ||
                $month->ktcupo_max_requests_day_left <= 0 ||
                $month->moda_capping_monthly_limit_left <= 0
            )
            {
                try
                {
                    //Save capped customer in match statistics
                    $ms = new MatchStatistic();
                    $ms->mast_re_id = $request->re_id;
                    $ms->mast_cu_id = $month->cu_id;
                    $ms->mast_matched = 0;
                    $ms->mast_podium = 0;
                    $ms->mast_ktcupo_id = $month->ktcupo_id;
                    $ms->mast_capping_limitation = 1;
                    $ms->save();
                } catch (\Exception $e)
                {
                    DB::rollBack();
                }

                //Remove from possible matches
                $query_customers = $query_customers->where("cu_id", "!=", $month->cu_id);
            }
        }

        DB::commit();

        $portal_level = $query_customers->where('moda_capping_method', '=', MoverCappingMethod::PORTAL_LEVEL);

        DB::beginTransaction();

        foreach($portal_level as $portal_customer){
            if(
                $portal_customer->ktcupo_requests_bucket > (($portal_customer->ktcupo_daily_average + sqrt($portal_customer->ktcupo_daily_average)) * 1.05) ||
                $portal_customer->ktcupo_max_requests_month_left <= 0 ||
                $portal_customer->ktcupo_max_requests_day_left <= 0
            )
            {
                try
                {
                    //Save capped customer in match statistics
                    $ms = new MatchStatistic();
                    $ms->mast_re_id = $request->re_id;
                    $ms->mast_cu_id = $portal_customer->cu_id;
                    $ms->mast_matched = 0;
                    $ms->mast_podium = 0;
                    $ms->mast_ktcupo_id = $portal_customer->ktcupo_id;
                    $ms->mast_capping_limitation = 2;
                    $ms->save();
                } catch (\Exception $e)
                {
                    DB::rollBack();
                }

                //Remove from possible matches
                $query_customers = $query_customers->where("cu_id", "!=", $portal_customer->cu_id);
            }
        }

        DB::commit();

        //Check forced daily capping
        $daily_capping = $query_customers->where("moda_forced_daily_capping_left", "<=", 0);

        DB::beginTransaction();

        foreach($daily_capping as $daily){
            try
            {
                //Save capped customer in match statistics
                $ms = new MatchStatistic();
                $ms->mast_re_id = $request->re_id;
                $ms->mast_cu_id = $daily->cu_id;
                $ms->mast_matched = 0;
                $ms->mast_podium = 0;
                $ms->mast_ktcupo_id = $daily->ktcupo_id;
                $ms->mast_capping_limitation = 3;
                $ms->save();
            } catch (\Exception $e)
            {
                DB::rollBack();
            }

            //Remove from possible matches
            $query_customers = $query_customers->where("cu_id", "!=", $daily->cu_id);
        }

        DB::commit();

        //The actual list of customers we will be working with
        $total_customers = 0;
        $total_checked = 0;

        $match_summary = [
            "customers" => [],
            "total_prices" => 0,
            "total_prices_after_podium" => 0,
            "correction_bonus" => 0.625,
            "formula_price_correction" => 5,
            "lowest_formula_price_export" => 100,
            "lowest_formula_price_import" => 100,
            "podium" => [],
            "main_portal" => null,
        ];

        $average_matches = [];  //List of collected average matches
        $podium_size = 2; //Size of the guaranteed podium places
        $all_checked = false; //Is set true if lead gets matched to all available customers

        //Remove customers that can't afford this lead
        $query_customers = self::removeInsufficientFundsCustomers($query_customers, $request, $system);

        $query_customers = $mover->setCustomersOnCreditHoldBeforeMatch($query_customers, $request);

        //Remove all companies that arent allowed to match each other
        foreach($excluded_from_each_other as $ids)
        {
            $query_customers = self::removeDoubleCompanies($query_customers, $ids);
        }

        //Get the average matches
        foreach ($query_customers as $index => $customer)
        {
            $average_match_30_days = self::calculateAverageMatch($customer->cu_id);
            if ($average_match_30_days)
            {
                array_push($average_matches, $average_match_30_days);
            }
        }

        //Remove Open Spots Only and Only Match Possible customers for now
        $open_spots_only_customers = [];
        $only_match_possible_customers = [];
        $query_customers = self::removeCustomersWithQualityScoreOverride($query_customers,$open_spots_only_customers, $only_match_possible_customers);

        $match_summary['customers'] = $query_customers;

        foreach ($query_customers as $customer)
        {
            $query_payment_rate = DB::table("payment_rates")
                ->where("para_ktcupo_id", $customer->ktcupo_id)
                ->whereRaw("`para_date` <= NOW()");

            //Get correct NAT payment type
            if ($request->re_destination_type == DestinationType::NATMOVING)
            {
                $query_payment_rate->where("para_nat_type", $request->re_nat_type);
            }

            $query_payment_rate = $query_payment_rate
                ->orderBy("para_date", 'desc')
                ->first();


            /* If this customer should receive the lead, calculate the following things:
             *
             *      1. Average match of mover in last 30 days
             *      2. Price the customer pays for the lead: $actual_payment_rate
             *      3. Claim percentage (or no-claim discount) of last 30 days
             *      4. Adjusted Lead Price
             *      5. Adjusted Average Match
             *      6. Formule Price
             *      7. Top X positions
             *      8. Percentages for remaining movers
             *
            */

            $average_match_30_days = self::calculateAverageMatch($customer->cu_id, true, $average_matches);
            $claim_percentage_30_days = self::getClaimRateOrNoClaimRate($customer->cu_id);
            //$lead_price = $system->calculatePaymentRate($query_payment_rate);
            //Get price without discount
            $lead_price = $query_payment_rate->para_rate;
            $claim_adjusted_lead_price = ((1 - ($claim_percentage_30_days / 100)) * $lead_price);
            $adjusted_average_match = 1 - ($average_match_30_days / 5);
            if ($customer->moda_quality_score_override == 1)
            {
                $correction_bonus = 0;
            } else
            {
                $correction_bonus = $adjusted_average_match * $match_summary["correction_bonus"];
            }
            $formula_price = (1 + $correction_bonus) * $claim_adjusted_lead_price;

            $customer->average_match_30_days = $average_match_30_days;
            $customer->lead_price = $lead_price;
            $customer->claim_percentage_30_days = $claim_percentage_30_days;
            $customer->claim_adjusted_lead_price = $claim_adjusted_lead_price;
            $customer->adjusted_average_match = $adjusted_average_match;
            $customer->correction_bonus = $correction_bonus;
            $customer->formula_price = $formula_price;
            $customer->final_price = $formula_price;
            $customer->matching_percentage = 0;
            $customer->matching_percentage_after_podium = 0;

            $match_summary["total_prices"] += $formula_price;

            $total_customers++;

        }

        //Determine the main portal
        $main_portal = DB::table("portals")
            ->select("po_id")
            ->leftJoin("regions", "po_id", "reg_po_id")
            ->where("reg_id", $request->re_reg_id_from)
            ->first()
            ->po_id;

        $match_summary["main_portal"] = $main_portal;

        $amount_of_main_portal_customers = $match_summary['customers']->where("po_id", $main_portal)->count();

        $use_guaranteed_podium = $amount_of_main_portal_customers > $matches_left;


        //There are more than 5 customers in the main portal, so we will calculate a guaranteed podium place
        if ($use_guaranteed_podium)
        {
            //Set the lowest formula price
            foreach ($match_summary["customers"]->where("po_id", $main_portal) as $customer)
            {
                if($customer->formula_price < $match_summary["lowest_formula_price_export"]){
                    $match_summary["lowest_formula_price_export"] = $customer->formula_price;
                }
            }

            //Create a new calculation for the formula price
            foreach ($match_summary["customers"]->where("po_id", $main_portal) as $customer)
            {
                $match_summary["total_prices"] -= $customer->formula_price;

                $price_after_division = $customer->formula_price / $match_summary["lowest_formula_price_export"];
                $customer->final_price = pow($price_after_division, $match_summary["formula_price_correction"]);

                $match_summary["total_prices"] += $customer->final_price;
            }

            foreach ($match_summary["customers"]->where("po_id", "!=", $main_portal) as $customer)
            {
                $match_summary["total_prices"] -= $customer->final_price;
            }


            foreach ($match_summary["customers"] as $customer)
            {
                //There are more than 5 customers in the main portal, so they get a fair percentage
                if ($customer->po_id == $main_portal)
                {
                    $customer->matching_percentage = ($customer->final_price / $match_summary["total_prices"] * 100);
                }
                //Import portal customers get 0% chance
                else
                {
                    $customer->matching_percentage = 0;
                }
            }

        }
        //There are 5 or less customers in the MAIN portal
        else
        {
            //Take away points from the total quality score if the current customer will have a 100% chance of being selected
            //This happens because there are 5 or less customers to match to
            //Or there are 5 or less customers in the main portal, who will always get priority over other portals (import)

            //Determine the lowest price of the current selection
            foreach ($match_summary["customers"]->where("po_id", "!=", $main_portal) as $customer)
            {
                if($customer->formula_price < $match_summary["lowest_formula_price_import"]){
                    $match_summary["lowest_formula_price_import"] = $customer->formula_price;
                }
            }

            //Create a new calculation for the formula price
            foreach ($match_summary["customers"]->where("po_id", "!=", $main_portal) as $customer)
            {
                $match_summary["total_prices"] -= $customer->formula_price;

                $price_after_division = $customer->formula_price / $match_summary["lowest_formula_price_import"];
                $customer->final_price = pow($price_after_division, $match_summary["formula_price_correction"]);

                $match_summary["total_prices"] += $customer->final_price;
            }


            foreach ($match_summary["customers"] as $customer)
            {

                //There are less customers than matches left, so everybody gets the lead
                if ($total_customers <= $matches_left)
                {
                    $customer->matching_percentage = 100;
                    $customer->matching_percentage_after_podium = 100;
                    $customer->checked = 1;

                    $total_checked++;
                    $all_checked = true;
                }
                //There are less customers than matches left for the main portal, so everybody in the main portal gets the lead
                elseif (count($match_summary["customers"]) > 1 && $amount_of_main_portal_customers <= $matches_left && $customer->po_id == $main_portal)
                {
                    $customer->matching_percentage = 100;
                    $customer->matching_percentage_after_podium = 100;
                    $customer->checked = 1;
                    $match_summary["total_prices"] -= $customer->final_price;
                    $total_checked++;
                }
                //Everybody in the import portal gets a fair percentage
                else
                {
                    $customer->matching_percentage = ($customer->final_price / $match_summary["total_prices"] * 100);
                }
            }
        }

        //Guaranteed podium is more than 5 customers in the main portal
        if ($use_guaranteed_podium)
        {
            //Determine guaranteed podium position
            foreach ($match_summary["customers"]->where("po_id", $main_portal) as $customer)
            {
                //Set an impossibly high percentage as lowest, so it will always be overwritten with an actual value
                $lowest = ["id" => -1, "percentage" => 101];

                if (sizeof($match_summary["podium"]) < $podium_size)
                {
                    $match_summary["podium"][$customer->cu_id] = $customer;
                } else
                {
                    foreach ($match_summary["podium"] as $podium)
                    {
                        if ($podium->matching_percentage < $lowest["percentage"])
                        {
                            $lowest = ["id" => $podium->cu_id, "percentage" => $podium->matching_percentage];
                        }
                    }

                    if ($customer->matching_percentage > $lowest["percentage"])
                    {
                        unset($match_summary["podium"][$lowest["id"]]);
                        $match_summary["podium"][$customer->cu_id] = $customer;
                    }
                }

            }


            //Loop through podium and remove the percentages from leads_divided_by_average_match_total
            $match_summary["total_prices_after_podium"] = $match_summary["total_prices"];
            foreach ($match_summary["podium"] as $cu_id => $customer)
            {
                $match_summary["total_prices_after_podium"] -= $customer->final_price;
            }

            $from = 0;
            $to = 0;

            //calculate remaining percentages
            foreach ($match_summary["customers"]->where("po_id", $main_portal) as $customer)
            {

                if (array_key_exists($customer->cu_id, $match_summary["podium"]))
                {
                    $customer->matching_percentage = 100;
                    $customer->matching_percentage_after_podium = 100;
                    $customer->checked = 1;

                    $total_checked++;

                } else
                {
                    $customer_percentage = ($customer->final_price / $match_summary["total_prices_after_podium"]) * 100;
                    $to += $customer_percentage;

                    $customer->matching_percentage_after_podium = $customer_percentage;
                    $customer->from = $from;
                    $customer->to = $to;

                    $from += $customer_percentage;
                }
            }

        }
        //There is no guaranteed podium, so calculate percentages
        elseif(!$all_checked)
        {
            $from = 0;
            $to = 0;

            //calculate remaining percentages
            foreach ($match_summary["customers"]->where("po_id", "!=" ,$main_portal) as $customer)
            {
                if ($amount_of_main_portal_customers != 5)
                {
                    $customer_percentage = ($customer->final_price / $match_summary["total_prices"]) * 100;
                    $to += $customer_percentage;

                    $customer->matching_percentage_after_podium = $customer_percentage;
                    $customer->from = $from;
                    $customer->to = $to;

                    $from += $customer_percentage;
                }
            }

        }


        //Randomly choose customers
        $i = 0;
        $max_checked = (($total_customers < $matches_left) ? $total_customers : $matches_left) - $total_checked;

        while ($i < $max_checked)
        {
            $random = rand(0, 100);

            foreach ($match_summary["customers"] as $customer)
            {
                if (isset($customer->from) && $customer->checked == 0 && $random >= round($customer->from) && $random <= round($customer->to))
                {
                    $customer->checked = 1;
                    $i++;

                    break;
                }
            }
        }

        //Add the potential open spot customers
        $match_summary = $this->AddOpenSpotsCustomers($open_spots_only_customers, $match_summary, $total_customers, $matches_left);

        //Add the potential solo customers
        $match_summary = $this->AddSoloCustomers($only_match_possible_customers, $match_summary, $total_customers);

        //Log the entire calculation
        $already_exists = QualityScoreCalculation::select("qsc_id")->where("qsc_re_id", $request->re_id)->first();

        if (!$already_exists)
        {
            $qsc = new QualityScoreCalculation();
            $qsc->qsc_re_id = $request->re_id;
            $qsc->qsc_new_calc = System::serialize($match_summary);
            $qsc->save();
        }


        $arrayify = [];

        foreach ($match_summary["customers"] as $customer)
        {
            $arrayify[$customer->cu_id] = $system->databaseToArray($customer);
        }

        $match_summary["customers"] = $arrayify;

        return $match_summary;

    }

    /**
     * @param $request_id
     * @param array $customers_exclude
     * @return array
     */
    public function ExcludeCustomers($request_id): array
    {
        $customers_exclude = [];

        $query_request_customer_portal = DB::table("kt_request_customer_portal")
            ->select("ktrecupo_cu_id")
            ->where('ktrecupo_re_id', "=", $request_id)
            ->get();

        if ($query_request_customer_portal->count())
        {
            foreach ($query_request_customer_portal as $row_request_customer_portal)
            {
                $customers_exclude[] = $row_request_customer_portal->ktrecupo_cu_id;
            }
        }

        return $customers_exclude;
    }


    public function getMatchedTo($request_id)
    {
        return KTRequestCustomerPortal::with("customer")->where("ktrecupo_re_id", $request_id)->get();
    }


    public static function getCreditsForRequest($moving_size)
    {
        switch ($moving_size)
        {
            case 1:
            case 2:
                $creditAmount = 6;
                break;
            case 3:
                $creditAmount = 4;
                break;
            default:
                $creditAmount = 2;
                break;
        }

        return $creditAmount;
    }

    public function getRequestsDouble(Request $request)
    {
        $system = new System();

        $notices = [];

        $request_ids = [];

        $double_requests = DB::table("requests")
            ->select("re_id", "re_timestamp", "re_source", "re_wefo_id", "re_afpafo_id", "re_status", "re_rejection_reason", "re_co_code_from", "re_co_code_to", "re_email", "re_ip_address")
            ->where("re_id", "!=", $request->re_id)
            ->whereRaw("
				(
					(
						`re_email` = '" . $request->re_email . "' AND
						`re_email` != ''
					)
					OR
					(
						`re_ip_address` = '" . $request->re_ip_address . "' AND
						`re_ip_address` != ''
					)
				)")
            ->orderBy("re_timestamp", 'desc')
            ->limit(30)
            ->get();

        if ($double_requests->count())
        {
            $notices['double'][] = "<strong>ATTENTION:</strong> This request has been submitted multiple times with the following data:";

            foreach ($double_requests as $double)
            {
                $request_ids[] = $double->re_id;

                if ($double->re_source == RequestSource::TRIGLOBAL)
                {
                    $source = "Website <strong>" . $system->getWebsiteForm($double->re_wefo_id) . "</strong>";
                } elseif ($double->re_source == RequestSource::AFFILIATE)
                {
                    $source = "Affiliate partner <strong>" . $system->affiliatePartnerForm($double->re_afpafo_id) . "</strong>";
                }

                $email_double = strtolower(trim($double->re_email)) == strtolower(trim($request->re_email));
                $ip_double = $double->re_ip_address == $request->re_ip_address;

                $double_country_from_query = Country::where("co_code", $double->re_co_code_from)->first();
                $double_country_from = $double_country_from_query ? $double_country_from_query->co_en : "Unknown";

                $double_country_to_query = Country::where("co_code", $double->re_co_code_to)->first();
                $double_country_to = $double_country_to_query ? $double_country_to_query->co_en : "Unknown";

                $notices['double'][] = "<strong>[" . RequestStatus::name($double->re_status) . (($double->re_status == RequestStatus::REJECTED) ? " - " . RejectionReason::name($double->re_rejection_reason)[0] : "") . "]</strong>
 				Request <strong>" . $double->re_id . "</strong>
 				via " . $source . " on <strong>" . $double->re_timestamp . "</strong>
 				from <strong>" . $double_country_from . "</strong>
 				to <strong>" . $double_country_to . "</strong>
 				 <strong>[<span" . (($email_double) ? " style=\"color: #FF0000;\"" : "") . ">" . $double->re_email . "</span> / <span" . (($ip_double) ? " style=\"color: #FF0000;\"" : "") . ">" . $double->re_ip_address . "</span>]</strong>
 				  <a target='_blank'  href=" . url('requests/' . $double->re_id) . "  title=\"View lead\"><i class='fa fa-eye'></i></a>";
            }
        }

        $double_requests_new = DB::table("requests")
            ->select("re_id", "re_timestamp", "re_source", "re_wefo_id", "re_afpafo_id", "re_status", "re_rejection_reason", "re_co_code_from", "re_co_code_to", "re_email", "re_ip_address")
            ->where("re_id", "!=", $request->re_id)
            ->whereRaw("
				(
                    (
                        `re_full_name` = '" . filter_var(ucwords(strtolower($request->re_full_name)), FILTER_SANITIZE_STRING) . "' AND
                        `re_co_code_from` = '" . $request->re_co_code_from . "' AND
                        `re_co_code_to` = '" . $request->re_co_code_to . "'
                    )
				)")
            ->whereRaw("`re_timestamp` > NOW() - INTERVAL 30 DAY")
            ->orderBy("re_timestamp", 'desc')
            ->limit(30)
            ->get();

        if ($double_requests_new->count())
        {
            if (!$double_requests->count())
            {
                $notices['double'][] = "<strong>ATTENTION:</strong> This request has been submitted multiple times with the following data:";
            }

            foreach ($double_requests_new as $double)
            {
                if (in_array($double->re_id, $request_ids))
                {
                    continue;
                }
                $request_ids[] = $double->re_id;

                $notices['double'][] = "<strong>[" . RequestStatus::name($double->re_status) . (($double->re_status == RequestStatus::REJECTED) ? " - " . RejectionReason::name($double->re_rejection_reason)[0] : "") . "]</strong>
 				Request <strong>" . $double->re_id . "</strong>
 				could be double, because there is already a request with the same
 				<span style='color:#FF0000;'>full name, country from AND country to</span>
 				in the last 30 days.
 				  <a target='_blank'  href=" . url('requests/' . $double->re_id) . "  title=\"View lead\"><i class='fa fa-eye'></i></a>";
            }
        }

        //TODO: hey Arjan, als één van de values hier een '/' is dan klapt de boel eruit
        $double_requests_new2 = DB::table("requests")
            ->select("re_id", "re_timestamp", "re_source", "re_wefo_id", "re_afpafo_id", "re_status", "re_rejection_reason", "re_co_code_from", "re_co_code_to", "re_email", "re_ip_address")
            ->where("re_id", "!=", $request->re_id)
            ->whereRaw("
				(
					(
                        `re_street_from` = '" . filter_var(ucwords(strtolower($request->re_street_from)), FILTER_SANITIZE_STRING) . "' AND
                        `re_zipcode_from` = '" . filter_var(ucwords(strtolower($request->re_zipcode_from)), FILTER_SANITIZE_STRING) . "' AND
                        `re_city_from` = '" . filter_var(ucwords(strtolower($request->re_city_from)), FILTER_SANITIZE_STRING) . "' AND
                        `re_co_code_from` = '" . $request->re_co_code_from . "'
                    )
				)")
            ->whereRaw("`re_timestamp` > NOW() - INTERVAL 30 DAY")
            ->orderBy("re_timestamp", 'desc')
            ->limit(30)
            ->get();

        if ($double_requests_new2->count())
        {
            if (!$double_requests->count() && !$double_requests_new->count())
            {
                $notices['double'][] = "<strong>ATTENTION:</strong> This request has been submitted multiple times with the following data:";
            }

            foreach ($double_requests_new2 as $double)
            {
                if (in_array($double->re_id, $request_ids))
                {
                    continue;
                }
                $request_ids[] = $double->re_id;

                $notices['double'][] = "<strong>[" . RequestStatus::name($double->re_status) . (($double->re_status == RequestStatus::REJECTED) ? " - " . RejectionReason::name($double->re_rejection_reason)[0] : "") . "]</strong>
 				Request <strong>" . $double->re_id . "</strong>
 				could be double, because there is already a request with the same
 				<span style='color:#FF0000;'>origin (street, zipcode, city and country</span>
 				in the last 30 days.
 				  <a target='_blank'  href=" . url('requests/' . $double->re_id) . "  title=\"View lead\"><i class='fa fa-eye'></i></a>";
            }
        }

        return $notices;

    }

    public function checkDestinationType(Request $request)
    {

        $query_portals = DB::table("portals")
            ->select("po_id")
            ->where("po_destination_type", DestinationType::NATMOVING)
            ->whereRaw("(
					`po_co_code` = '" . $request->re_co_code_from . "' AND
					`po_co_code` = '" . $request->re_co_code_to . "'
				)")
            ->first();

        if (($request->re_co_code_from == $request->re_co_code_to) && !empty($query_portals) && $request->re_destination_type == DestinationType::INTMOVING)
        {
            //Change request to national
            $request->re_destination_type = DestinationType::NATMOVING;
            $request->save();
        } elseif (($request->re_co_code_from != $request->re_co_code_to) && $request->re_destination_type == DestinationType::NATMOVING)
        {
            //Change request to international
            $request->re_destination_type = DestinationType::INTMOVING;
            $request->save();
        }

    }

    public function requestCategories()
    {
        $categories = DB::table("requests")
            ->selectRaw('DISTINCT re_category')
            ->where('re_category', '!=', '')
            ->get();


        // Set default array
        $data = [];

        // Loop categories
        foreach ($categories as $category)
        {

            // Set key and value to the category
            // This will be used in forms
            $data[$category->re_category] = $category->re_category;
        }

        return $data;
    }

    public function requestWebsiteField($request_id, $field)
    {

        $request = Request::select("re_source", "re_wefo_id", "re_afpafo_id")->where("re_id", $request_id)->first();

        if (!empty($request))
        {
            if ($request->re_source == RequestSource::TRIGLOBAL)
            {
                $website_form = DB::table("website_forms")
                    ->selectRaw("`websites`." . $field)
                    ->leftJoin("websites", "wefo_we_id", "we_id")
                    ->where("wefo_id", $request->re_wefo_id)
                    ->first();

                if (!empty($website_form))
                {
                    return $website_form->$field;
                } else
                {
                    return false;
                }
            } elseif ($request->re_source == RequestSource::AFFILIATE)
            {
                $affiliate_partner_form = DB::table("affiliate_partner_forms")
                    ->selectRaw("`websites`." . $field)
                    ->leftJoin("portals", "afpafo_po_id", "po_id")
                    ->leftJoin("websites", "po_default_website", "we_id")
                    ->where("afpafo_id", $request->re_afpafo_id)
                    ->first();

                if (!empty($affiliate_partner_form))
                {
                    return $affiliate_partner_form->$field;
                } else
                {
                    return false;
                }
            }
        } else
        {
            return false;
        }
    }

    public function minimumMovingDate($request_type, $destination_type = null)
    {
        $reason_to_days = [6 => 14, 7 => 7, 8 => 2];

        return $reason_to_days[self::dateClaimReason($request_type, $destination_type)];
    }

    private function dateClaimReason($request_type, $destination_type = null)
    {
        if ($request_type == 2)
        {
            return 8;
        } elseif ($destination_type == 2)
        {
            return 7;
        } else
        {
            return 6;
        }
    }

    public function calculateAverageMatch($cu_id, $use_median = false, $average_matches = [])
    {
        $requests = KTRequestCustomerPortal::select("ktrecupo_re_id")
            ->whereBetween("ktrecupo_timestamp", [date("Y-m-d H:i:s", strtotime("-45 day")), date("Y-m-d H:i:s")])
            ->where("ktrecupo_cu_id", $cu_id)
            ->get();

        $ids = [];
        $match_amounts = [];

        //Put all requests in an array
        foreach($requests as $request){
            array_push($ids, $request->ktrecupo_re_id);
        }

        //Get the amount of matches for all these requests
        $match_amounts_query = KTRequestCustomerPortal::selectRaw("count(*) as amount_of_matches, `ktrecupo_re_id`")->whereIn("ktrecupo_re_id", $ids)->groupBy("ktrecupo_re_id")->get();

        //Save the amount in a separate array
        foreach($match_amounts_query as $match_amount_query){
            $match_amounts[$match_amount_query->ktrecupo_re_id] = $match_amount_query->amount_of_matches;
        }

        $total_requests = $requests->count();
        $total_matches = 0;

        //If no requests, return a default
        if ($total_requests < 20)
        {
            if ($use_median)
            {

                foreach ($requests as $request)
                {
                    $total_matches += $match_amounts[$request->ktrecupo_re_id];
                }

                if (!empty($average_matches))
                {

                    $median = self::getMedianofArray($average_matches);
                    $total_matches += ((20 - $total_requests) * $median);

                    return $total_matches / 20;

                } else
                {
                    return 5;
                }

            } else
            {
                return null;
            }
        } //If more than 20 leads, do a normal average match calculation
        else
        {
            foreach ($requests as $request)
            {
                $total_matches += $match_amounts[$request->ktrecupo_re_id];
            }

            return $total_matches / $total_requests;
        }

    }

    public function getClaimRateOrNoClaimRate($cu_id)
    {

        $claim_discount = MoverData::select("moda_no_claim_discount_percentage")->where("moda_cu_id", $cu_id)->where("moda_no_claim_discount", 1)->where("moda_no_claim_discount_percentage", ">", 0)->first();
        if ($claim_discount)
        {
            return $claim_discount->moda_no_claim_discount_percentage;
        } else
        {
            $mover = new Mover();
            $calculations = $mover->calcReclamationRate(90, $cu_id);

            //This is done to avoid punishing customers with a low amount of leads that have all/mostly been claimed, causing them to have an extremely high claim rate.
            if($calculations["percentage"] > 10){
                //The percentage is higher than 10 but the amount is lower than 10, just return 10 for now, as to not punish new customers
                if($calculations["requests"] < 10){
                    return 10;
                }//The customer has a high claim rate over many leads but we will punish this maximally at 50%
                elseif($calculations["percentage"] > 50){
                    return 50;
                }else{
                    return $calculations["percentage"];
                }
            }
            else
            {
                return $calculations["percentage"];
            }
        }

    }

    function getMedianofArray($arr)
    {
        sort($arr);
        $count = count($arr);
        $middleval = floor(($count - 1) / 2);
        if ($count % 2)
        {
            $median = $arr[$middleval];
        } else
        {
            $low = $arr[$middleval];
            $high = $arr[$middleval + 1];
            $median = (($low + $high) / 2);
        }

        return $median;
    }


    function removeInsufficientFundsCustomers($customers, $request, $system)
    {
        foreach($customers->where("moda_capping_method", MoverCappingMethod::MONTHLY_SPEND) as $index => $customer)
        {
            $query_payment_rate = DB::table("payment_rates")
                ->where("para_ktcupo_id", $customer->ktcupo_id)
                ->whereRaw("`para_date` <= NOW()");

            //Get correct NAT payment type
            if ($request->re_destination_type == DestinationType::NATMOVING)
            {
                $query_payment_rate->where("para_nat_type", $request->re_nat_type);
            }

            $query_payment_rate = $query_payment_rate
                ->orderBy("para_date", 'desc')
                ->first();

            if (!empty($query_payment_rate))
            {
                //Check if enough 'balance' left for the lead
                $actual_payment_rate = $system->calculatePaymentRate($query_payment_rate);
                if($actual_payment_rate > $customer->moda_capping_monthly_limit_left){
                    $customers->forget($index);
                }
            }
        }

       return $customers;
    }

    function removeDoubleCompanies($customers, $not_allowed_ids){

        $found = [];
        foreach ($customers as $customer)
        {
            if (in_array($customer->cu_id, $not_allowed_ids))
            {
                $found[] = $customer->cu_id;
            }
        }

        //Unset a random winner
        unset($found[array_rand($found)]);

        //Return the array without the losers in it
        return $customers->whereNotIn("cu_id", $found);


    }


    function removeCustomersWithQualityScoreOverride($customers, &$open_spot_customers, &$only_match_customers){

        $remove_ids = [];

        foreach ($customers as $customer)
        {
           if($customer->moda_quality_score_override == QualityScoreOverride::EXCLUDED_ONLY_TAKE_OPEN_SPOTS){
               $open_spot_customers[] = $customer;
               $remove_ids[] = $customer->cu_id;
           }

            if($customer->moda_quality_score_override == QualityScoreOverride::EXCLUDED_ONLY_MATCH_POSSIBLE){
                $only_match_customers[] = $customer;
                $remove_ids[] = $customer->cu_id;
            }
        }

        //Return the array without the losers in it
        return $customers->whereNotIn("cu_id", $remove_ids);


    }/**
 * @param array $open_spots_only_customers
 * @param array $match_summary
 * @param int $total_customers
 * @param bool|int $matches_left
 * @return array
 */
    public function AddOpenSpotsCustomers($open_spots_only_customers, $match_summary, &$total_customers, $matches_left)
    {
        //Add the optional customers back into the customers array, we want to show them no matter what
        $total_optional_customers = count($open_spots_only_customers);

        foreach ($open_spots_only_customers as $opt_customer)
        {
            $match_summary["customers"][] = $opt_customer;
        }

        //Check if there are any spots for excluded customers
        if ($total_customers < $matches_left && !empty($open_spots_only_customers))
        {
            $optional_matches_left = $matches_left - $total_customers;

            $i = 0;

            while ($i < $optional_matches_left && !empty($open_spots_only_customers))
            {
                //Pick a random winner
                $winning_open_spots_index = array_rand($open_spots_only_customers);
                $winning_open_spots_customer = $open_spots_only_customers[$winning_open_spots_index];
                $match_summary["customers"]->where("cu_id", $winning_open_spots_customer->cu_id)->first()->checked = 1;

                //Unset the winner
                unset($open_spots_only_customers[$winning_open_spots_index]);
                $i++;
            }
        }

        $total_customers += $total_optional_customers;

        return $match_summary;
    }

    /**
     * @param array $only_match_possible_customers
     * @param array $match_summary
     * @param int $total_customers
     * @return array
     */
    public function AddSoloCustomers(array $only_match_possible_customers, array $match_summary, int $total_customers): array
    {
        $total_solo_customers = count($only_match_possible_customers);

        foreach ($only_match_possible_customers as $solo_customer)
        {
            $match_summary["customers"][] = $solo_customer;
        }

        //Check if solo customers are allowed
        if ($total_customers === 0 && $total_solo_customers > 0)
        {
            //Get the main portal or a random one
            //Pick a random winner
            $winning_index = array_rand($only_match_possible_customers);
            $winning_customer = $only_match_possible_customers[$winning_index];
            $match_summary["customers"]->where("cu_id", $winning_customer->cu_id)->first()->checked = 1;
        }

        return $match_summary;
    }


}
