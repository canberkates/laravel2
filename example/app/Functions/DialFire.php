<?php


namespace App\Functions;

use DB;
use Illuminate\Http\Request;
use Log;

define("SUCCESS", "success");
define("DIALFIRE_ID", "dialfire_id");
define("DIALFIRE_TASK", "dialfire_task");


class DialFire
{
	function errorExit($message)
	{
		return json_encode(["error" => $message, "success" => false]);
	}
	
	function validatePhone($telephone, $co_codes, &$valid_phone, &$country_code = null, &$alt_phone = null)
	{
		$system = new System();
		//Verify phone number
		$query = http_build_query([
			"phone_number" => $telephone,
			"co_codes" => implode(",", $co_codes)
		]);
		
		$phone_content = $system->getExternal("https://public.triglobal.info/api/phone/parse.php?".$query);
		
		if(empty($phone_content))
		{
			return self::errorExit("Failed to parse phonenumber(s)...");
		}
		
		$phone_result = json_decode($phone_content, true);
		
		foreach($co_codes as $co_code)
		{
			if(isset($phone_result[$co_code]))
			{
				$phone_code_result = $phone_result[$co_code];
				
				if($phone_code_result["valid"] === true)
				{
					if($valid_phone == null)
					{
						$valid_phone = $phone_code_result["formatted"];
						$country_code = $phone_code_result["countryCode"];
					}
					else if($valid_phone != $phone_code_result["formatted"])
					{
						$alt_phone = $phone_code_result["formatted"];
					}
				}
			}
		}
		
		return $phone_result;
	}
	
	function determineTask($cc)
	{
		//Available task and all countries that should redirect to that task
		$available_tasks = [
			"NL" => [],
			"DK" => [],
			"UK" => ["GB"],
			"AU" => [
				"AS", "CC", "CK", "CX", "FJ", "FM", "KI", "MH", "NC", "NF", "NR", "NU", "NZ", "PF", "PG", "PN", "PW", "SB", "TK", "TO", "TV", "VU", "WF", "WS"
			],
			"DE" => [],
			"AT" => [],
			"CH" => [],
			"BE" => [],
			"CA" => [],
			"ES" => [],
			"IT" => [],
			"US" => [
				"AG", "AI", "AR", "AW", "BB", "BL", "BM", "BO", "BQ", "BR", "BS", "BZ", "CA", "CL", "CO", "CR", "CU", "CW", "DM", "DO", "EC", "FK", "GD", "GF", "GP", "GS", "GT", "GY", "HN",
				"HT", "JM", "KN", "KY", "LC", "MF", "MQ", "MS", "MX", "NI", "PA", "PE", "PM", "PR", "PY", "SH", "SR", "SV", "SX", "TC", "TT", "UM", "UY", "VC", "VE", "VG", "VI"
			],
			"FR" => [],
			"SG" => [
				"AE", "AF", "AM", "AZ", "BD", "BN", "BT", "CN", "GE", "GU", "HK", "ID", "IN", "IO", "IR", "JP", "KG", "KH", "KP", "KR", "KZ", "LA", "LK", "MM", "MN", "MO", "MP", "MV", "MY",
				"NP", "OM", "PH", "PK", "SG", "TH", "TJ", "TL", "TM", "TW", "UZ", "VN"
			]
		];
		
		$default_task = "NL";
		
		if(isset($available_tasks[$cc]))
		{
			return $cc;
		}
		else
		{
			foreach($available_tasks as $key => $countries)
			{
				if(in_array($cc, $countries))
				{
					return $key;
				}
			}
			
			return $default_task;
		}
	}
	
	/**
	 * @param $campaign
	 * @param $task
	 * @param $request
	 * @return array
	 */
	function postCall($campaign, $task, $request)
	{
		$campaign_id = System::getSetting("dialfire_campaign_id_{$campaign}");
		$auth_bearer = System::getSetting("dialfire_auth_bearer_{$campaign}");
		
		//Send the request
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://api.dialfire.com/api/campaigns/{$campaign_id}/tasks/fc_call_{$task}/contacts/create");
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch,		CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request));
		curl_setopt($ch, CURLOPT_HTTPHEADER, ["Content-Type: application/json", "Authorization: Bearer {$auth_bearer}"]);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		curl_close($ch);
		
		Log::debug($result);
		
		if($result === false || empty($result))
		{
			self::errorExit("Failed to contact Dialfire. Try again later!");
		}
		
		$json_result = json_decode($result, true);
		
		if(empty($json_result["data"]["\$id"]))
		{
			error_log("[Dialfire] No ID given | Task: {$campaign}/{$task}) | Request: ".json_encode($request)." | Result: ".$result);
			self::errorExit("Unable to add to Dialfire. <strong>Please notify IT!</strong>");
		}
		
		return [
			SUCCESS => true,
			DIALFIRE_ID => $json_result["data"]["\$id"],
			DIALFIRE_TASK => $task
		];
	}
	
	function addToDialfire(Request $request){
		$portalData = new PortalData();
		
		switch($request->type)
		{
			case "requests":
				if(empty($request->id))
				{
					return self::errorExit("No request ID given");
				}
				
				$row = \App\Models\Request::find($request->id);
				
				//Check if exists & not already added
				if(empty($row))
				{
					return self::errorExit("No Request Found");
				}
				
				$co_codes = [$row['re_co_code_from'], $row['re_co_code_to']];
				
				//Verify phone number
				$valid_phone = null;
				$alt_phone = null;
				$country_code = null;
				self::validatePhone($row['re_telephone1'], $co_codes, $valid_phone, $country_code, $alt_phone);
				
				if($valid_phone === null)
				{
					return self::errorExit("Unable to create a valid phonenumber (try editing it?)");
				}
				
				if($alt_phone == null && !empty($row['re_telephone2']))
				{
					self::validatePhone($row['re_telephone2'], $co_codes, $alt_phone);
				}
				
				
				//Determine the task
				$use_task = self::determineTask($country_code);
				
				//Build request
				$request = [
					"\$phone" => $valid_phone,
					"id" => $row['re_id'],
					"portal" => $portalData->nameAndCountryCode($row['re_po_id']),
					"received_on" => $row['re_timestamp'],
					
					"full_name" => $row['re_full_name'],
					"email" => $row['re_email'],
					"original_phone" => $row['re_telephone1'],
					"alt_phone" => ($alt_phone == null ? "N/A" : $alt_phone),
					
					"country_from" => $portalData->country($row['re_co_code_from']),
					"country_to" => $portalData->country($row['re_co_code_to']),
					"type" => DataIndex::requestTypes()[$row['re_request_type']]." (".DataIndex::movingSizes()[$row['re_moving_size']].")",
					"moving_date" => $row['re_moving_date']
				];
				$result = self::postCall("requests", $use_task, $request);
				
				break;
			
			//Intentional fall-through
			
			case "customers":
				//Get the customer
				if(empty($request->id))
				{
					self::errorExit("No customer ID given");
				}
				
				$row = DB::table("customers")
					->select("cu_company_name_business","cu_co_code","cu_la_code","cu_telephone","cu_telephone_bi")
					->where("cu_id", $request->id)
					->first();
				
				
				//Validate the phone numbers
				$co_codes = [$row['cu_co_code']];
				$valid_phone = null;
				self::validatePhone($row['cu_telephone'], $co_codes, $valid_phone);
				
				if($valid_phone === null)
				{
					self::errorExit("Invalid phonenumber");
				}
				
				//Business phone
				$business_phone = null;
				if(!empty($row['cu_telephone_bi']))
				{
					self::validatePhone($row['cu_telephone_bi'], $co_codes, $business_phone);
				}
				
				//Determine the task
				$use_task = self::determineTask($row['cu_co_code']);
				
				//Build the request
				$request = [
						'$phone' => $valid_phone,
						'cu_id' => $request->id,
						'customer' => $row['cu_company_name_business'],
						'original_phone' => $row['cu_telephone'],
						'original_phone_bi' => System::useOr($row['cu_telephone_bi']),
						'formatted_phone_bi' => System::useOr($business_phone),
						'language' => $portalData->getLanguage($row['cu_la_code'])
					] + [];
				$result = self::postCall($request->type, $use_task, $request);
				
				
				break;
			
			default:
				self::errorExit("Unknown type");
				
				return;
		}
		
		echo json_encode($result);
	}
}
