<?php
/**
 * Created by PhpStorm.
 * User: Lorenzo
 * Date: 07/02/2019
 * Time: 15:07
 */

namespace App\Functions;

use App\Models\SystemSetting;
use Illuminate\Support\Facades\Log;

class Mail
{
    public static $textColor = '#535b65';

    public function send($mail_name, $language_code, $fields, $attachments = null, $show_error = null, $bar_color = null, $html = false)
    {
        $data = [
            'token' => 'e$FrA#*aAwg4@nmhGZAbIm^d6DC',
            'mail_name' => $mail_name,
            'language' => $language_code,
            'show_error' => $show_error,
            'bar_color' => $bar_color,
            'attachments' => $attachments,
            'html' => $html,
            'fields' => $fields
        ];

        if(env("APP_ENV") == "production"){
            $curl = curl_init('https://public.triglobal.info/api/mail.php');
        }else{
            $curl = curl_init('https://public.triglobal-test-back.nl/api/mail.php');
        }

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_ENCODING, "");
        curl_setopt($curl, CURLOPT_MAXREDIRS, 10);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));

        // Set HTTP Header for POST request
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
                'Content-Type: application/x-www-form-urlencoded',
                'Content-Length: '.strlen(http_build_query($data))]
        );

        $response = curl_exec($curl);
        $err = curl_error($curl);

        Log::debug("Processing Mail: ".$mail_name." HTML: ".intval($html));
        if($html){
            return $response;
        }

        curl_close($curl);

        if($err)
        {
            Log::debug($err);
        }
        else
        {
            Log::debug($response);
        }
    }

    public function sendCustomMail($mail_name, $language_code, $fields, $attachments = null, $show_error = null, $bar_color = null, $html = false)
    {
        $data = [
            'token' => 'e$FrA#*aAwg4@nmhGZAbIm^d6DC',
            'mail_name' => $mail_name,
            'language' => $language_code,
            'show_error' => $show_error,
            'bar_color' => $bar_color,
            'attachments' => $attachments,
            'html' => $html,
            'fields' => $fields
        ];

        if(env("APP_ENV") == "production"){
            $curl = curl_init('https://public.triglobal.info/api/custommail.php');
        }else{
            $curl = curl_init('https://public.triglobal-test-back.nl/api/custommail.php');
        }


        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_ENCODING, "");
        curl_setopt($curl, CURLOPT_MAXREDIRS, 10);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));

        // Set HTTP Header for POST request
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
                'Content-Type: application/x-www-form-urlencoded',
                'Content-Length: '.strlen(http_build_query($data))]
        );

        $response = curl_exec($curl);
        $err = curl_error($curl);

        Log::debug("Processing Mail: ".$mail_name." HTML: ".intval($html));
        if($html){
            return $response;
        }

        curl_close($curl);

        if($err)
        {
            Log::debug($err);
        }
        else
        {
            Log::debug($response);
        }
    }

	public function table( $align = "center", $width = "100%", $font_size = 16, $extra_style = "", $styles = [] )
	{
		// Default table styling
		$defaults = [
			'padding' => '0',
			'margin-top' => '0',
			'margin-bottom' => '0',
			'max-width' => '640px',
			'font-family' => 'Arial',
			'font-size' => '16px',
			'margin-left' => '0',
			'margin-right' => '0',
		];

		// Merge default styling with custom styling
		// Latter overrides first
		$styles = array_merge( $defaults, $styles );

		// Start style
		$style = 'style="';

		// Loop styles
		foreach( $styles as $property => $value ) {

			// Add styles in proper format
			$style .= $property.':'.$value.';';
		}

		// End style
		$style .= ( ! empty( $extra_style ) ? $extra_style : '' ).'"';

		return '<table align="'.$align.'" cellpadding="0" cellspacing="0" width="'.$width.'" '.$style.'>';
	}

    // The redefined table
    public static function table2( $style = [], $attributes = [] ) {

        // Get style
        $style = self::getStyle( $style, [] );
        $attributes = self::getAttributes( $attributes, [
            'cellpading' => '0',
            'cellspacing'=> '0',
            'width' => '100%',
            'border' => '0',
            'border-spacing' => '0',
            'margin-left' => '0',
            'margin-right' => '0',
        ] );

        return '<table '.$style.' '.$attributes.'>';
    }

    public static function genTable( $content, $variation = '' )
    {
        // Start output
        $template = '';
        $i = 0;

        // Loop rows
        foreach( $content as $index => $options ) {

            // Set default of skip
            $skip = false;

            // If options is not an array
            if( ! is_array( $options ) )
                continue;

            if( isset( $options['checks'] ) ) {

                // If there is a not check
                if( isset( $options['checks']['not'] ) ) {

                    $notCheckVal = $options['checks']['not']['value'];

                    // Turn into array
                    if( ! is_array( $options['checks']['not']['fields'] ) )
                        $options['checks']['not']['fields'] = [$options['checks']['not']['fields']];

                    // Loop fields to check on
                    foreach( $options['checks']['not']['fields'] as $field ) {

                        // The check
                        if( $notCheckVal == $field )
                            $skip = true;
                    }
                }
                if( isset( $options['checks']['empty'] ) ) {

                    // Turn into array
                    if( ! is_array( $options['checks']['empty']['fields'] ) )
                        $options['checks']['empty']['fields'] = [$options['checks']['empty']['fields']];

                    // Loop fields to check on
                    foreach( $options['checks']['empty']['fields'] as $field ) {

                        // The check
                        if( empty( $field ) )
                            $skip = true;
                    }
                }
            }

            // If we should skip the current row
            if( $skip )
                continue;

            // Set type
            if( ! isset( $options['type'] ) )
                $options['type'] = 'row';

            // If type is heading
            if( $options['type'] == 'heading' ) {

                // Reset i
                $i = 0;

                $template .= self::genTableType( 'heading', $options );
            }
            elseif( $options['type'] == 'empty_row' ) {

                $template .= self::genTableType( 'empty_row' );
            }
            elseif( $options['type'] == 'row' ) {

                if( isset( $options['when_empty'] ) )
                    if( empty( $options['val'] ) )
                        $options['val'] = $options['when_empty'];

                $trStyle = ['background-color' => ( ( $i++ % 2 == 0 ) ? '#F4F5F5' : 'transparent' ) ];
                $tdStyle1 = ['color' => self::$textColor, 'padding' => '10px'];
                $tdStyle2 = ['color' => '#40454c', 'padding' => '10px'];

                // Specific email changes
                if( $variation == 'customer_request' ) {

                    $tdStyle1['padding'] = '0';
                    $tdStyle2['padding'] = '0';
                }

                // If $i is even add bgc
                // after check add 1 to $i
                $template .= self::TR( $trStyle );
                $template .= self::TD( $tdStyle1 );
                $template .= $options['name'].':';
                $template .= '</td>';
                $template .= self::TD( $tdStyle2 );
                $template .= $options['val'];
                $template .= '</td>';
                $template .= '</tr>';
            }
            elseif( $options['type'] == 'remarks' ) {

                // If there are remarks
                if( ! empty( $options['val'] ) ) {

                    // Empty row
                    if( $variation != 'customer_request' )
                        $template .= self::genTableType( 'empty_row' );

                    // Heading row
                    $template .= self::genTableType( 'heading', $options );

                    // Remarks
                    $template .= Mail::TR();
                    $template .= Mail::TD( ['word-break' => 'break-all'], ['colspan' => '2'] );
                    $template .= Mail::P( nl2br( $options['val'] ), ($variation == 'customer_request' ? ['margin-bottom' => '0', 'font-size' => '15px'] : [] ) );
                    $template .= '</td>';
                    $template .= '</tr>';
                }
            }
        }

        return $template;
    }

	public function styleBar($background, $padding = 5, $font_size = 16)
	{
		return "color: #FFFFFF; background: ".$background."; text-align: center; padding: ".$padding."px; font-size: ".$font_size."px; font-weight: bold";
	}

    public static function genTableType( $type, $options = [] )
    {
        $template = '';

        if( $type === 'empty_row' ) {

            $template .= self::TR();
            $template .= self::TD( false, ['colspan' => '2'] );
            $template .= '&nbsp;';
            $template .= '</td>';
            $template .= '</tr>';
        }
        elseif( $type === 'heading' ) {

            $template .= self::TR();
            $template .= self::TD(false, ['colspan' => '2'] );
            $template .= self::tableH2( $options['name'] );
            $template .= '</td>';
            $template .= '</tr>';
        }
        elseif( $type === 'full_row' ) {

            $template .= self::TR();
            $template .= self::TD( [], ['colspan' => '2'] );
            $template .= Mail::P( $options['val'], [] );
            $template .= '</td>';
            $template .= '</tr>';
        }

        return $template;
    }

    public static function blockTitle( $text, $style = [] ) {

        // Default paragraph styling
        $defaults = [
            'font-size'   => '25px',
            'margin'      => '0 0 15px 0',
            'padding'     => '0',
            'color'       => '#13B3EF',
            'font-weight' => 'normal',
            'mso-line-height-rule' => 'exactly',
        ];

        $style = self::getStyle( $style, $defaults );

        // End paragraph
        $p = '<h2 '.$style.'>'.$text.'</h2>';

        return $p;
    }

    public static function H2($text)
    {
        return "<h2 style='font-size: 14px; text-transform: uppercase; border-bottom: 1px solid #999999; margin: 0 0 5px 0; padding: 0; color: #F07D00; mso-line-height-rule:exactly;'>".$text."</h2>";
    }

    public static function tableH2( $text, $style = [] )
    {

        $defaults = [
            'font-size'            => '18px',
            'color'                => '#292D31',
            'font-weight'          => 'bold',
            'margin'               => '15px 0 10px',
            'mso-line-height-rule' => 'exactly',
        ];

        $style = self::getStyle( $style, $defaults );

        return '<h2 '.$style.'>'.$text.'</h2>';
    }

    public static function H3($text)
    {
        return "<h3 style='font-size: 14px; margin: 0 0 5px 0; padding: 0; mso-line-height-rule:exactly;'>".$text."</h3>";
    }

    public static function logoText( $text, $style = [] )
    {

        $defaults = [

            'font-size'	=> '28px',
            'color'		=> '#2B3C4D',
        ];

        $style = self::getStyle( $style, $defaults );

        return '<h1 '.$style.'>'.$text.'</h1>';
    }

    public static function P( $text, $style = [] )
    {
        // Default paragraph styling
        $defaults = [
            'font-size'	=> '16px',
            'margin'	=> '0 0 18px 0',
            'padding'	=> '0',
            'color'		=> self::$textColor,
        ];

        $style = self::getStyle( $style, $defaults );

        // End paragraph
        $p = '<p '.$style.'>'.$text.'</p>';

        return $p;
    }

    public static function link( $link, $text = '', $style = [] ) {

        if( empty( $link ) || empty( $text ) )
            return false;

        // Default paragraph styling
        $defaults = [
            'font-size'	=> '16px',
            'margin'	=> '0 0 0',
            'padding'	=> '0',
            'color'		=> 'inherit',
        ];

        $style = self::getStyle( $style, $defaults );

        $a = '<a href="'.$link.'" '.$style.'>'.$text.'</a>';

        return $a;
    }

    public static function button($link, $text, $arrow = true )
    {
        return "<a href=\"".$link."\" target=\"_blank\" style=\"font-size: 14px; color: #FFFFFF; font-weight: bold; text-decoration: none; background-color: #F07D00; border-top: 12px solid #F07D00; border-bottom: 12px solid #F07D00; border-right: 18px solid #F07D00; border-left: 18px solid #F07D00; display: inline-block;\">".$text.(($arrow == true) ? " &rarr;" : "")."</a>";
    }

    public static function buttonNew($link, $text, $arrow = true )
    {
        return "<a href=\"".$link."\" target=\"_blank\" style=\"font-size: 14px; color: #FFFFFF; font-weight: bold; text-decoration: none; background-color: #3184F9; border-top: 12px solid #3184F9; border-bottom: 12px solid #3184F9; border-right: 18px solid #3184F9; border-left: 18px solid #3184F9; display: inline-block;\">".$text.(($arrow == true) ? " &rarr;" : "")."</a>";
    }

    public static function hr($margin = true)
    {
        return "<p style=\"border-top: 1px solid #999999; margin: 0; padding: 0;\">".(($margin) ? "&nbsp;" : "")."</p>";
    }

    public static function pageBreak()
    {
        return "<div style=\"page-break-before: always;\"></div>";
    }

    public static function styleBorderTable()
    {
        return "color: #333333; border-left: 1px solid #999999; border-top: 1px solid #999999;";
    }

    public static function styleBorderTH($no_wrap = false)
    {
        return "background: #F0F0F0; border-right: 1px solid #999999; border-bottom: 1px solid #999999; padding: 4px; page-break-inside: avoid;".(($no_wrap) ? " white-space: nowrap;" : "");
    }

    public static function styleBorderTD($no_wrap = false, $double_border = false, $text_align = "center")
    {
        return "border-right: 1px solid #999999; border-bottom: ".($double_border ? "3px double" : "1px solid")." #999999; text-align: ".$text_align."; padding: 2px; page-break-inside: avoid;".(($no_wrap) ? " white-space: nowrap;" : "");
    }

    public static function TR( $style = [], $attributes = [] )
    {
        // Get style
        $style = self::getStyle( $style, [] );
        $attributes = self::getAttributes( $attributes, [] );

        return '<tr '.$style.' '.$attributes.'>';
    }

    public static function TD( $style = [], $attributes = [] )
    {
        $style = self::getStyle( $style, [] );
        $attributes = self::getAttributes( $attributes, [] );

        return '<td '.$style.' '.$attributes.'>';
    }

    public static function IMG( $src, $alt = '', $style = [], $attributes = [] )
    {
        // The td style defaults
        $defaults = [];

        // Get style
        $style = self::getStyle( $style, $defaults );

        $attributes = self::getAttributes( $attributes, [] );

        return '<img src="'.$src.'" '.$style.( ! empty( $alt ) ? ' alt="'.$alt.'"' : '' ).' '.$attributes.'>';
    }

    public static function CLEAR()
    {
        return '<div style="clear: both; width: 100%; display: block;"></div>';
    }

    private static function getStyle( $style = [], $defaults = [] )
    {
        // If both are empty
        if( empty( $style ) && empty( $defaults ) )
            return '';

        // Merge default styling with custom styling
        // Latter overrides first
        $styles = ( is_array( $style ) ? array_merge( $defaults, $style ) : $defaults );

        // Start style
        $style = 'style="';

        // Loop styles
        foreach( $styles as $property => $value ) {

            // Add styles in proper format
            $style .= $property.':'.$value.';';
        }

        // End style
        $style .= '"';

        return $style;
    }

    private static function getAttributes( $attributes = [], $defaults = [] )
    {
        // Merge default styling with custom styling
        // Latter overrides first
        $attList = array_merge( $defaults, $attributes );

        if( empty( $attList ) )
            return '';

        // Start style
        $attributes = '';

        // Loop styles
        foreach( $attList as $attr => $value ) {

            // Add styles in proper format
            $attributes .= ' '.$attr.'="'.$value.'" ';
        }

        return $attributes;
    }
}
