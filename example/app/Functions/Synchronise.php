<?php


namespace App\Functions;
use App\Models\CustomerReviewScore;
use App\Models\KTCustomerPortal;
use App\Models\MobilityexFetchedData;
use App\Models\MoverData;
use App\Models\Survey2;
use App\Models\Website;
use Illuminate\Support\Facades\DB;
use Log;
use Session;


class Synchronise
{
    /**
     * Synchronise constructor.
     */
    public function __construct() {
    }

    /**
     *
     */
    public static function sendEmails($websites)
    {
        Session::put('progress', "Sending emails for the new reviews");
        Session::save(); // Remember to call save()

        // Send all the emails for new reviews
        self::sendReviewEmails($websites);

        Session::put('progress', "Sending emails for the new or edited comments on reviews");
        Session::save(); // Remember to call save()

        // Send all the emails for new or edited comments on reviews
        self::sendReviewCommentEmails($websites);

        Session::put('progress', "Sending emails for Python Process");
        Session::save(); // Remember to call save()

        self::sendPythonProcessEmails($websites);

        self::sendMobilityexEmails($websites);

    }

    private static function sendReviewEmails($websites)
    {
        // Default request
        $accepted = 0;
        $rejected = 0;

        DB::enableQueryLog();

        $query = Survey2::leftJoin("requests", "su_re_id", "re_id")
            ->leftJoin("website_reviews", "su_were_id", "were_id")
            ->leftJoin("customers", DB::raw("(IF(`surveys_2`.`su_mover` > '0', `surveys_2`.`su_mover`, `surveys_2`.`su_other_mover`))"), "cu_id")
            ->leftJoin("website_forms", "were_wefo_id", "wefo_id")
            ->leftJoin("websites", "cu_co_code", "we_sirelo_co_code")
            ->leftJoin("website_subdomains", "cu_co_code", "wesu_country_code")
            ->leftJoin("mover_data", "cu_id", "moda_cu_id")
            ->where("su_mail", 1)
            ->where("cu_deleted", 0)
            ->where(function ($query) use ($websites) {
                $query->whereIn('we_id', $websites)
                    ->orWhereIn('wesu_we_id', $websites);
            })
            ->get();

        Log::debug("Syncing reviews!!");
        Log::debug(DB::getQueryLog());


        $query = System::databaseToArray($query);

        // Loop surveys
        foreach( $query as $row ) {

            // Update current survey
            // Set su_mail = 2
            // su_mail = 2 is status of mailed
            DB::table('surveys_2')
                ->where('su_id', $row['su_id'])
                ->update(['su_mail' => 2]);

            // If the submitted date is 7 days from today
            //if( System::dateDifference( $row['su_submitted_timestamp'], "today" ) <= 7 ) {

                // Get from name for the emails
                $from_name = self::getEmailField( 'from_name', $row );

                // Get the language for the emails
                $language = self::getEmailField( 'language', $row );

                // Get the email to for the emails
                $to = self::getEmailField( 'to', $row );

                // Get the name for the email
                $name = self::getEmailField( 'name', $row );

                // If review will be shown on website
                if( $row['su_show_on_website'] == 1 ) {

                    // In this case we send 2 emails
                    // One to the client that send us the review
                    // One to the mover that has a new review

                    // If this customer is a review partner
                    if( $row['moda_review_partner'] == 1 ) {

                        $customer_type = 3;
                    }
                    // This customer is not a review partner
                    else {

                        // Select customer portal by customer id
                        $query_customer_portal = KTCustomerPortal::select("ktcupo_id")
                            ->where("ktcupo_cu_id", $row['cu_id'])
                            ->whereIn("ktcupo_status", [1,2])
                            ->where("ktcupo_free_trial", 0)
                            ->first();

                        // If there is a customer portal
                        if(count($query_customer_portal) > 0) {

                            // Set customer type
                            $customer_type = 2;
                        }
                        else{

                            // Set customer type
                            $customer_type = 1;
                        }
                    }

                    // Get customer page
                    $customer_page = Data::getSireloCustomerPage( $row['cu_id'] );

                    // Set customer name
                    $customer_name = $row['cu_company_name_business'];

                    // Prepare fields for the review accept email
                    $fields = [
                        "from_name"     => $from_name,
                        "to"            => $to,
                        "name"          => $name,
                        "customer_page" => $customer_page,
                        "survey"        => $row,
                        "customer_name" => $customer_name,
                    ];

                    // Send mail to the online user
                    Mail::send("website_review_accept", $language, $fields);

                    // Prepare fields for the review published email
                    $fields = [
                        "from_name"        => $from_name,
                        "customer_type"    => $customer_type,
                        "customer_page"    => $customer_page,
                        "information_page" => ( ( $customer_type == 1 ) ? Data::getSireloFieldByCountry( $row['cu_co_code'], "we_sirelo_partner_program_location" ) : System::getSetting( "mover_location" ) ),
                        "survey"           => $row,
                    ];

                    // Send mail to the mover
                    Mail::send( "sirelo_review_published", $row['cu_la_code'], $fields );

                    // Add one to accepted
                    $accepted++;
                }

                // If su_send_email = 1
                // su_send_email means if we want to send the rejected email or not
                elseif( $row['su_show_on_website'] == 2 && $row['su_send_email'] == 1 ) {

                    // In this case we send 1 email
                    // One to the client about his review being rejected and why

                    // Prepare fields for the review reject email
                    $fields = [
                        "from_name" => $from_name,
                        "to"        => $to,
                        "name"      => $name,
                        "survey"    => $row,
                    ];

                    // Send mail to the online user
                    Mail::send( 'website_review_reject', $language, $fields );

                    // Add one to rejected
                    $rejected++;
                }
            //}
        }

        Session::put('progress', 'Sent '.$accepted.' accepted emails');
        Session::save(); // Remember to call save()

        Session::put('progress', 'Sent '.$rejected.' rejected emails');
        Session::save(); // Remember to call save()
    }

    private static function sendReviewCommentEmails($websites)
    {
        // Set review comment amount to 0
        $reviewComment = 0;

        // Check all surveys with a mover comment
        // Get all survey_2 rows where su_mail = 3
        // su_mail 3 means that there is a comment or edit on a comment from the moving company
        $query = Survey2::leftJoin("requests", "su_re_id", "re_id")
            ->leftJoin("website_reviews", "su_were_id", "were_id")
            ->leftJoin("customers", DB::raw("(IF(`surveys_2`.`su_mover` > '0', `surveys_2`.`su_mover`, `surveys_2`.`su_other_mover`))"), "cu_id")
            ->leftJoin("website_forms", "were_wefo_id", "wefo_id")
            ->leftJoin("websites", "cu_co_code", "we_sirelo_co_code")
            ->leftJoin("website_subdomains", "cu_co_code", "wesu_country_code")

            ->leftJoin("mover_data", "cu_id", "moda_cu_id")
            ->where("su_mail", 3)
            ->where("cu_deleted", 0)
            ->where(function ($query) use ($websites) {
                $query->whereIn('we_id', $websites)
                    ->orWhereIn('wesu_we_id', $websites);
            })
            ->get();

        $query = System::databaseToArray($query);

        // Loop the surveys with a mover comment
        foreach( $query as $row ) {
            // Reset su_mail status to 2
            DB::table('surveys_2')
                ->where('su_id', $row['su_id'])
                ->update(['su_mail' => 2]);

            // Send mail to the online user
            $fields = [
                "from_name"     => self::getEmailField( 'from_name', $row ),
                "to"            => self::getEmailField('to', $row ),
                "name"          => self::getEmailField( 'name', $row ),
                "customer_page" => Data::getSireloCustomerPage( $row['cu_id'] ),
                "survey"        => $row,
            ];

            // Add one to review comment
            $reviewComment++;

            Mail::send("sirelo_review_comment", self::getEmailField( 'language', $row ), $fields );
        }

        Session::put('progress', 'Sent '.$reviewComment.' review comment emails');
        Session::save(); // Remember to call save()
    }

    private static function sendPythonProcessEmails($websites)
    {
        $mail = new Mail();
        $system = new System();
        $sirelocustomer = new SireloCustomer();

        $python_process = 0;

        //Get all changes to mail (Python Process
        //curesc_mail = 0 --> NOT PROCESSED YET
        //curesc_mail = 1 --> SEND EMAIL
        //curesc_mail = 2 --> EMAIL ALREADY SENT
        //curesc_mail = 3 --> DONT SEND EMAIL
        //curesc_mail = 4 --> SEND PROFILE CREATED EMAIL

        $query = CustomerReviewScore::leftJoin("customers", "cu_id", "curesc_cu_id")
            ->leftJoin("websites", "cu_co_code", "we_sirelo_co_code")
            ->leftJoin("website_subdomains", "cu_co_code", "wesu_country_code")
            ->where(function ($query) use ($websites) {
                $query->whereIn('we_id', $websites)
                    ->orWhereIn('wesu_we_id', $websites);
            })
            ->where("curesc_mail", 1)
            ->where("curesc_processed", 1)
            ->get();

        $query = $system->databaseToArray($query);

        // Loop the customer_review_score rows
        foreach( $query as $row ) {
            //Set mail status to 2 (MAIL SENT)
            DB::table('customer_review_scores')
                ->where('curesc_id', $row['curesc_id'])
                ->update(['curesc_mail' => 2]);

            $moverdata_row = MoverData::where("moda_cu_id", $row['cu_id'])->first();
            if (count($moverdata_row) > 0 && $moverdata_row->moda_disable_sirelo_export == 0) {
                $sirelopage = $sirelocustomer->getPage($moverdata_row->moda_cu_id);
                if ($moverdata_row->moda_international_moves == 0 && strpos($sirelopage, 'sirelo.org') !== false) {
                    continue;
                }
                //@TODO als er geen email bekend is, dan info@ toevoegen van huidige domein van de customer.
                if (!empty($row['cu_email'])) {
                    $fields = [
                        "to" => $row['cu_email'],
                        "company_name" => $row['cu_company_name_business'],
                        "from_name" => 'Sirelo',
                        "sirelo_link" => Data::getSireloCustomerPage($row['cu_id']),
                        "register_company_link" => Data::getSireloFieldByCountry( $row['cu_co_code'], "we_register_company_location")
                    ];

                    $python_process++;

                    $mail->send("sirelo_profile_changed", $row['cu_la_code'], $fields);
                }
            }
        }

        $query = CustomerReviewScore::leftJoin("customers", "cu_id", "curesc_cu_id")
            ->leftJoin("websites", "cu_co_code", "we_sirelo_co_code")
            ->leftJoin("website_subdomains", "cu_co_code", "wesu_country_code")
            ->where(function ($query) use ($websites) {
                $query->whereIn('we_id', $websites)
                    ->orWhereIn('wesu_we_id', $websites);
            })
            ->where("curesc_mail", 4)
            ->where("curesc_processed", 1)
            ->get();

        $query = $system->databaseToArray($query);

        // Loop the customer_review_score rows
        foreach( $query as $row ) {
            //Set mail status to 2 (MAIL SENT)
            DB::table('customer_review_scores')
                ->where('curesc_id', $row['curesc_id'])
                ->update(['curesc_mail' => 2]);

            $moverdata_row = MoverData::where("moda_cu_id", $row['cu_id'])->first();
            if (count($moverdata_row) > 0 && $moverdata_row->moda_disable_sirelo_export == 0) {
                $sirelopage = $sirelocustomer->getPage($moverdata_row->moda_cu_id);
                if ($moverdata_row->moda_international_moves == 0 && strpos($sirelopage, 'sirelo.org') !== false) {
                    continue;
                }
                //@TODO als er geen email bekend is, dan info@ toevoegen van huidige domein van de customer.
                if (!empty($row['cu_email'])) {
                    $fields = [
                        "to" => $row['cu_email'],
                        "company_name" => $row['cu_company_name_business'],
                        "from_name" => 'Sirelo',
                        "sirelo_link" => Data::getSireloCustomerPage($row['cu_id']),
                        "claim_company_link" => Data::getSireloFieldByCountry( $row['cu_co_code'], "we_register_company_location")."?customer=".System::getSeoKey($row['cu_city']).System::getSeoKey($row['cu_company_name_business'])
                    ];

                    $python_process++;

                    $mail->send("sirelo_profile_created", $row['cu_la_code'], $fields);
                }
            }
        }

        Session::put('progress', 'Sent '.$python_process.' python process emails');
        Session::save(); // Remember to call save()
    }

    private static function sendMobilityexEmails($websites)
    {
        $mail = new Mail();
        $system = new System();
        $sirelocustomer = new SireloCustomer();

        $python_process = 0;

        //Get all changes to mail (Python Process
        //curesc_mail = 0 --> NOT PROCESSED YET
        //curesc_mail = 1 --> SEND EMAIL
        //curesc_mail = 2 --> EMAIL ALREADY SENT
        //curesc_mail = 3 --> DONT SEND EMAIL

        $query = MobilityexFetchedData::leftJoin("customers", "cu_id", "mofeda_cu_id")
            ->leftJoin("websites", "cu_co_code", "we_sirelo_co_code")
            ->leftJoin("website_subdomains", "cu_co_code", "wesu_country_code")
            ->where(function ($query) use ($websites) {
                $query->whereIn('we_id', $websites)
                    ->orWhereIn('wesu_we_id', $websites);
            })
            ->where("mofeda_mail", 1)
            ->where("mofeda_processed", 1)
            ->get();

        $query = $system->databaseToArray($query);

        // Loop the customer_review_score rows
        foreach( $query as $row ) {
            //Set mail status to 2 (MAIL SENT)
            DB::table('mobilityex_fetched_data')
                ->where('mofeda_id', $row['mofeda_id'])
                ->update(['mofeda_mail' => 2]);

            $moverdata_row = MoverData::where("moda_cu_id", $row['cu_id'])->first();
            if (count($moverdata_row) > 0 && $moverdata_row->moda_disable_sirelo_export == 0) {
                $sirelopage = $sirelocustomer->getPage($moverdata_row->moda_cu_id);
                if ($moverdata_row->moda_international_moves == 0 && strpos($sirelopage, 'sirelo.org') !== false) {
                    continue;
                }
                //@TODO als er geen email bekend is, dan info@ toevoegen van huidige domein van de customer.
                if (!empty($row['cu_email'])) {
                    $fields = [
                        "to" => $row['cu_email'],
                        "company_name" => $row['cu_company_name_business'],
                        "from_name" => 'Sirelo',
                        "sirelo_link" => Data::getSireloCustomerPage($row['cu_id']),
                        "register_company_link" => Data::getSireloFieldByCountry( $row['cu_co_code'], "we_register_company_location")
                    ];

                    $python_process++;

                    $mail->send("sirelo_profile_changed", $row['cu_la_code'], $fields);
                }
            }
        }

        $query = MobilityexFetchedData::leftJoin("customers", "cu_id", "mofeda_cu_id")
            ->leftJoin("websites", "cu_co_code", "we_sirelo_co_code")
            ->leftJoin("website_subdomains", "cu_co_code", "wesu_country_code")
            ->where(function ($query) use ($websites) {
                $query->whereIn('we_id', $websites)
                    ->orWhereIn('wesu_we_id', $websites);
            })
            ->where("mofeda_mail", 4)
            ->where("mofeda_processed", 1)
            ->get();

        $query = $system->databaseToArray($query);

        // Loop the customer_review_score rows
        foreach( $query as $row ) {
            //Set mail status to 2 (MAIL SENT)
            DB::table('mobilityex_fetched_data')
                ->where('mofeda_id', $row['mofeda_id'])
                ->update(['mofeda_mail' => 2]);

            $moverdata_row = MoverData::where("moda_cu_id", $row['cu_id'])->first();
            if (count($moverdata_row) > 0 && $moverdata_row->moda_disable_sirelo_export == 0) {
                $sirelopage = $sirelocustomer->getPage($moverdata_row->moda_cu_id);
                if ($moverdata_row->moda_international_moves == 0 && strpos($sirelopage, 'sirelo.org') !== false) {
                    continue;
                }
                //@TODO als er geen email bekend is, dan info@ toevoegen van huidige domein van de customer.
                if (!empty($row['cu_email'])) {
                    $fields = [
                        "to" => $row['cu_email'],
                        "company_name" => $row['cu_company_name_business'],
                        "from_name" => 'Sirelo',
                        "sirelo_link" => Data::getSireloCustomerPage($row['cu_id']),
                        "claim_company_link" => Data::getSireloFieldByCountry( $row['cu_co_code'], "we_register_company_location")."?customer=".System::getSeoKey($row['cu_city']).System::getSeoKey($row['cu_company_name_business'])
                    ];

                    $python_process++;

                    $mail->send("sirelo_profile_created", $row['cu_la_code'], $fields);
                }
            }
        }

        Session::put('progress', 'Sent '.$python_process.' mobilityex emails');
        Session::save(); // Remember to call save()
    }

    private static function getEmailField( $field, $row )
    {
        // Get source | If source is 1 its true and uses fields from the request
        // If source is not 1 its false and uses the website review fields
        $source = $row['su_source'] == 1;

        if( $field === 'language' ) {

            return ( $source ? $row['re_la_code'] : $row['wefo_la_code'] );
        }
        elseif( $field === 'to' ) {

            return ( $source ? $row['re_email'] : $row['were_email'] );
        }
        elseif( $field === 'name' ) {

            return ( $source ? $row['re_full_name'] : $row['were_name'] );
        }
        elseif( $field === 'from_name' ) {
            return Data::getSireloMailName($row);
        }
        return false;
    }
}
