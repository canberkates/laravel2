<?php


namespace App\Functions;


use App\Data\DestinationType;
use App\Http\Controllers\AdminController;
use App\Models\Continent;
use App\Models\Country;
use App\Models\Customer;
use App\Models\MacroRegion;
use App\Models\MoverData;
use App\Models\Portal;
use App\Models\ProConCategory;
use App\Models\Region;
use App\Models\State;
use App\Models\Website;
use App\Models\WebsiteSubdomain;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Data
{
    /**
	 * Get all active customers.
	 * @param array $additional_fields Any additional customer fields
	 * @param string|null $additional_where additional where statements
	 * @param string|null $order_by order order
	 * @param bool $count_free_trial Should count free trial customers as active
	 * @return array Active customers in a array: cu_id => cu_row
	 */
	public static function activeCustomers($additional_fields = [], $additional_where = null, $order_by = null, $count_free_trial = true)
	{
	    $system = new System();

		$wanted_fields = "`cu_id`";

		foreach($additional_fields as $field)
        {
            $wanted_fields .= ", `".$field."`";
        }

	    $customer_builder = Customer::selectRaw($wanted_fields)
            ->where("cu_deleted", "=", 0);

		if($additional_where !== null)
		{
		    $customer_builder = $customer_builder->whereRaw($additional_where);
		}

		if($order_by !== null)
		{
			$customer_builder = $customer_builder->orderBy($order_by);
		}

		$customer_builder = $customer_builder->get();


		//Get customers
		$customers = [];

		foreach($customer_builder as $row)
		{
			$customers[$row->cu_id] = $row;
		}

		if(empty($customers))
		{
			return $customers;
		}

		$active_customers = [];

		//Check if active
		$active_checks = [
			["kt_customer_portal", "ktcupo"],
			["kt_customer_question", "ktcuqu"],
			["service_provider_newsletter_blocks", "seprnebl"]
		];
		foreach($active_checks as $check)
		{
			$table = $check[0];
			$prefix = $check[1];


			$builder = DB::table($table)
                ->select(DB::raw("DISTINCT {$prefix}_cu_id, {$prefix}_status"));

			if(!$count_free_trial && $table === "kt_customer_portal")
			{
			    $builder = $builder->where("ktcupo_free_trial", 0);
			}

			$builder = $builder->whereIn("{$prefix}_status", ["1", "2"])
                ->whereIn("{$prefix}_cu_id", array_keys($customers))
                ->get();

			$builder = $system->databaseToArray($builder);

			foreach($builder as $active_row)
			{
				$cu_id = $active_row[$prefix."_cu_id"];
				$status = $active_row[$prefix.'_status'];

				if(isset($active_customers[$cu_id]))
				{
					$cu_row = &$active_customers[$cu_id];
					if($cu_row['status'] == 2 && $status == 1)
					{
						$cu_row['status'] = $status;
					}
					unset($cu_row);
				}
				else
				{
					$active_customers[$cu_id] = $customers[$cu_id];
					$active_customers[$cu_id]["status"] = $status;
				}
			}
		}

		//Map to correct order
		$result = [];
		foreach($customers as $cu_id => $row)
		{
			if(isset($active_customers[$cu_id]))
			{
				$result[$cu_id] = $active_customers[$cu_id];
			}
		}

		return $result;
	}

    public static function getProConCategories($type = null, $language = null)
    {
        $categories = DB::table("pro_con_categories");

        if($type !== null){
            $categories->where("prcoca_type", $type);
        }

        $categories = $categories
            ->orderBy("prcoca_name", 'asc')
            ->get();

        $pro_con_categories = [];

        foreach($categories as $row)
        {
            if($language != null)
            {
                $languages = System::unserialize($row->prcoca_category);

                $pro_con_categories[$row->prcoca_id] = ((!empty($languages[$language])) ? $languages[$language] : $languages["EN"]);
            }
            else
            {
                $pro_con_categories[$row->prcoca_id] = $row->prcoca_name;
            }
        }

        return $pro_con_categories;
    }

	public static function portals($destination_type = null, $super_in_group = false)
	{
		$result = Portal::select("po_id", "po_portal", "po_destination_type", "co_en")
            ->leftJoin("countries", "co_code", "po_co_code");

		if($destination_type !== null)
		{
		    $result = $result->where("po_destination_type", $destination_type);
		}

		$result = $result->orderBy("po_destination_type", "asc")
            ->orderBy("po_portal", "asc")
            ->get();

		$portals = [];

		foreach($result as $row)
		{
			if($super_in_group)
			{
				$optgroup_name = DestinationType::all()[$row->po_destination_type];
				if(!isset($portals[$optgroup_name]))
				{
					$portals[$optgroup_name] = [];
				}
				$target_portals = &$portals[$optgroup_name];
			}
			else
			{
				$target_portals = &$portals;
			}

			$target_portals[$row->po_id] = $row->po_portal.((!empty($row->co_en)) ? " (".$row->co_en.")" : "");
			unset($target_portals);
		}

		return $portals;
	}

	public static function customers($type = null, $location_details = false)
	{
		if($location_details)
		{
		    $builder = Customer::select("cu_id", "cu_company_name_business", "cu_city", "cu_co_code");
		}
		else{
		    $builder = Customer::select("cu_id", "cu_company_name_business");
        }


		if($type !== null)
		{
		    $builder = $builder->where("cu_type", $type);
		}

		$builder = $builder->where("cu_deleted", 0)
            ->orderBy("cu_company_name_business", "asc")
            ->get();

		$customers = [];

		foreach($builder as $row)
		{
			$customers[$row->cu_id] = $row->cu_company_name_business.(($location_details) ? " / ".$row->cu_city.((!empty($row->cu_co_code)) ? ", ".DataIndex::getCountry($row->cu_co_code) : "") : "");
		}

		return $customers;
	}

	public static function getSirelos()
	{
		$website = Website::select("we_sirelo_co_code", "we_website")
            ->where("we_sirelo", 1)
            ->orderBy("we_website", "asc")
            ->get();

		$websites = [];

		foreach($website as $row)
		{
			if($row->we_sirelo_co_code == null){
				$websites['XX'] = $row->we_website;
			}else{
				$websites[$row->we_sirelo_co_code] = $row->we_website;
			}
		}

		return $websites;
	}

	/**
	 * Get all continents
	 * @param bool|false $group_in_super Group in super order (Worldwide)
	 * @return array Array with data
	 */
	/*public static function continents($group_in_super = false)
	{
		$continents_query = Continent::select("con_id", "con_name")->orderBy("con_name")->get();

		$continents = [];
		foreach($continents_query as $con)
        {
            $continents[$con->con_id] = $continents->con_name;
        }

		if($group_in_super)
		{
			return ["Worldwide" => $continents];
		}
		else
		{
			return $continents;
		}
	}*/
	public static function continents($group_in_super = false)
	{
        $continents = self::queryData("continents", "con_id", "con_name");

        if($group_in_super)
		{
			return ['Worldwide' => $continents];
		}
		else
		{
			return $continents;
		}
	}


	public static function portalRegions($portal, $group_in_super = false, $group_on_country = true, $language = LANGUAGE)
	{
		$joins = [];
		//Determine the joins
		if($group_in_super && $group_on_country)
		{
			$joins[] = ["countries", "reg_co_code", "co_code"];
		}

		//Get the data
		$data = self::queryData(
			"regions", "reg_id", "reg_name",
			($group_in_super ? ($group_on_country ? "co_en" : "reg_parent") : null),
			$joins,
			["reg_po_id" => $portal]
		);

		if(empty($data))
		{
			return ["" => "No regions found for this portal"];
		}
		else
		{
			return $data;
		}
	}

	public static function greaterRegions($country = null, $destination_type = null, $group_in_super = false, $language = LANGUAGE)
	{
		//Build the SQL
		$builder = Region::selectRaw("reg_id, co_en, (CASE WHEN (`reg_parent` = '') THEN 'All' ELSE `reg_parent` END) as `reg_parent`")
            ->join("countries", "co_code", "reg_co_code");
		if ($country != null)
        {
            $builder->whereIn("co_code", System::arrayParameter($country));
        }

		if ($destination_type != null)
        {
            $builder->whereIn("reg_destination_type", System::arrayParameter($destination_type));
        }


		$builder->groupBy("co_code", "reg_parent");

		//Get the results
		if(!$group_in_super)
		{
			$builder = $builder->get();
			$data = [];
			foreach($builder as $row)
            {
                $data[$row->co_en][$row->reg_id] = $row->reg_parent;
            }

			return $data;
		}
		else
		{
			$results = [];
			$query = $builder
				->orderBy("co_en")
				->orderBy("reg_parent")
				->get();

			foreach($query as $row)
			{
				$results[$row->co_en][$row->reg_id] = $row->reg_parent;
			}

			return $results;
		}
	}

	/*public static function regions($country = null, $destination_type = null, $group_in_super = false, $language = LANGUAGE)
	{
		$builder = Region::select("reg_id", "reg_name");

		if($group_in_super)
        {
            $builder = Region::selectRaw("reg_id, reg_name, co_en");
            $builder = $builder->leftJoin("countries", "reg_co_code", "co_code");
            if($country !== null)
            {
                $builder = $builder->where("reg_co_code", $country);
            }
            if($destination_type !== null)
            {
                $builder = $builder->where("reg_destination_type", $destination_type);
            }
            $builder = $builder->where("reg_deleted", 0);

            $builder = $builder->orderBy("co_en");
        }
		else{
		    if($country !== null)
            {
                $builder = $builder->where("reg_co_code", $country);
            }
            if($destination_type !== null)
            {
                $builder = $builder->where("reg_destination_type", $destination_type);
            }
            $builder = $builder->where("reg_deleted", 0);
        }

		$builder = $builder->orderBy("reg_name")->get();

		//$data = System::databaseToArray($builder);
		$data = [];
		foreach ($builder as $bldr)
        {
            $data[$bldr->reg_id] = $bldr->reg_name;
        }

		if(empty($data))
		{
			return ["" => "No regions found for this country"];
		}
		else
		{
			return $data;
		}
	}*/

	public static function regions($country = null, $destination_type = null, $group_in_super = false, $language = LANGUAGE)
	{
		//Build where
		$where = [];
		if($country !== null)
		{
			$where["reg_co_code"] = $country;
		}
		if($destination_type !== null)
		{
			$where["reg_destination_type"] = $destination_type;
		}
		$where["reg_deleted"] = 0;

		//Get the data
		$data = self::queryData(
			"regions", "reg_id", "reg_name",
			($group_in_super ? "co_en" : null),
			($group_in_super ? [["countries", "reg_co_code", "co_code"]] : []),
			$where
		);

		if(empty($data))
		{
			return ["" => "No regions found for this country"];
		}
		else
		{
			return $data;
		}
	}

	/*public static function regions($country = null, $destination_type = null, $group_in_super = false)
	{
		//Build where
		$where = [];
		if($country !== null)
		{
			$where["reg_co_code"] = $country;
		}
		if($destination_type !== null)
		{
			$where["reg_destination_type"] = $destination_type;
		}
		$where["reg_deleted"] = 0;

		//Get the data
		$data = self::queryData(
			"regions", "reg_id", "reg_name",
			($group_in_super ? "co_en" : null),
			($group_in_super ? [["countries", "reg_co_code", "co_code"]] : []),
			$where
		);

		if(empty($data))
		{
			return ["" => "No regions found for this country"];
		}
		else
		{
			return ["" => ""] + $data;
		}
	}*/

    public static function getCountries($language = LANGUAGE, $super_in_group = false)
    {
        $country_query = Country::select("co_code", "co_en", "mare_continent_name")->leftJoin("macro_regions", "co_mare_id", "mare_id");

        $country = [];

        if($super_in_group)
        {
            $country_query = $country_query->orderBy("mare_continent_name");
        }
        $country_query = $country_query->orderBy("co_en")->get();

        foreach ($country_query as $co)
        {
            $country[$co->mare_continent_name][$co->co_code] = $co->co_en;
        }

        return $country;
    }

    public static function getMacroRegions($group_in_super = false)
    {
        $macro_regions = self::queryData("macro_regions", "mare_id", "mare_name");

        if($group_in_super)
        {
            $array = [];
            $array["Worldwide"] = $macro_regions;

            $macro_regions_query = MacroRegion::all();

            foreach ($macro_regions_query as $mr) {
                $array[$mr->mare_continent_name][$mr->mare_id] = $mr->mare_name;
            }

            return $array;
        }
        else
        {
            return $macro_regions;
        }
    }

	public static function getEUcountries($language = LANGUAGE)
	{
	    $country_query = Country::select("co_code", "co_en")->leftJoin("continents", "con_id", "co_con_id")->where("in_eu", 1)->orderBy("co_en");

	    $country = [];

	    $country_query = $country_query->get();

	    foreach ($country_query as $co)
        {
            $country[$co->co_code] = $co->co_en;
        }

	    return $country;
	}

	public static function getSchengenAreaCountries($language = LANGUAGE)
	{
	    $country_query = Country::select("co_code", "co_en")->leftJoin("continents", "con_id", "co_con_id")->where("in_schengen_area", 1)->orderBy("co_en");

	    $country = [];

	    $country_query = $country_query->get();

	    foreach ($country_query as $co)
        {
            $country[$co->co_code] = $co->co_en;
        }

	    return $country;
	}

	public static function queryData($from_table, $id_column, $name_column, $super_column = null, $joins = [], $where = [])
	{
		//$result = TGB_QueryBuilder::select($id_column, $name_column);
		$result = DB::table($from_table)->select($id_column, $name_column);

		if($super_column !== null)
		{
			//$result->selectColumn($super_column);
			$result = DB::table($from_table)->select($id_column, $name_column, $super_column);
		}

		foreach($joins as $join_data)
		{
			//$result->joinTable($join_data[0], $join_data[1], $join_data[2]);

			$result = $result->leftJoin($join_data[0], $join_data[1], $join_data[2]);
		}

		if(!empty($where))
		{
			foreach($where as $where_column => $where_value)
			{
				//$result->whereEqual($where_column, $where_value);

				$result = $result->where($where_column, $where_value);
			}
		}

		if($super_column !== null)
		{
			//$result->orderBy($super_column);
			$result = $result->orderBy($super_column);
		}

		$result = $result->orderBy($name_column)->get();
		$result = System::databaseToArray($result);
		$data = [];

		foreach($result as $row)
		{
			//Get the correct target array
			if($super_column !== null)
			{
				if(!isset($data[$row[$super_column]]))
				{
					$data[$row[$super_column]] = [];
				}
				$target_array = &$data[$row[$super_column]];
			}
			else
			{
				$target_array = &$data;
			}

			//Store it
			$target_array[$row[$id_column]] = $row[$name_column];


			unset($target_array);
		}

		if($super_column !== null && count($data) === 1 && key($data) === "")
		{
			$data = ["All" => $data[""]];
		}

		return $data;
	}

    /**
     * @param $data|mixed Data from the request and/or customer in an array or the country code as a string
     *
     * @return bool|string
     */
    public static function getSireloMailName( $data )
    {
        // Set default
        $fromName = '';

        // If data is not an array
        if( ! is_array( $data ) && ! empty( $data ) )
            // Assume data is a country code and turn it into an array
            $data = ['cu_co_code' => $data];

        // I cu_co_code is set in data and not empty
        if( isset( $data['cu_co_code'] ) && ! empty( $data['cu_co_code'] ) ) {

            // Get from name based on customer country code
            $fromName = Data::getSireloFieldByCountry( $data['cu_co_code'], 'we_mail_from_name' );

            // If the request country code from is set and not empty
        }elseif( isset( $data['re_co_code_from'] ) && ! empty( $data['re_co_code_from'] ) ) {

            // Get name based on
            $fromName = Data::getSireloFieldByCountry( $data['re_co_code_from'], 'we_mail_from_name' );
        }

        return ( ! empty( $fromName ) ? $fromName : 'Sirelo.org' );
    }

    public static function getSireloFieldByCountry( $country, $field )
    {

        $sirelo_field = Website::select($field)
            ->where("we_sirelo", 1)
            ->where("we_sirelo_co_code", $country)
            ->first();

        // If there is no value
        if(count($sirelo_field) == 0) {
            $sirelo_field = System::databaseToArray($sirelo_field);

            // The fields that are different in the subdomains table
            $subdomain_fields = [

                'we_sirelo_url'					=> 'wesu_url',
                'we_sirelo_customer_location'	=> 'wesu_customer_location',
                'we_sirelo_la_code'				=> 'wesu_default_language',
                'we_sirelo_survey_location'		=> 'wesu_survey_location',
                'we_register_company_location'  => 'wesu_register_company_location',

                // Get website name from main site
                'we_website'					=> 'we_website',

                // Get mail from name from main site
                'we_mail_from_name'				=> 'we_mail_from_name',
            ];

            // If the key exists
            if( array_key_exists( $field, $subdomain_fields ) ) {

                // Change field
                $field = $subdomain_fields[$field];

                // Search for the different field in the subdomains table
                $sirelo_field = WebsiteSubdomain::select($field)
                    ->join("websites", "wesu_we_id", "we_id")
                    ->where("wesu_country_code", $country)
                    ->where("we_sirelo", 1)
                    ->first();

                $sirelo_field = System::databaseToArray($sirelo_field);
            }
        }

        return (count($sirelo_field) > 0 ? $sirelo_field[$field] : false);
    }

    public static function getSireloCustomerPage($cu_id)
    {
        $sirelo_customer = new SireloCustomer();

        return $sirelo_customer->getPage($cu_id);

        /*$sirelo = Customer::select("cu_la_code", "cu_co_code", "cu_city", "cu_company_name_business")
            ->where("cu_id", $cu_id)
            ->where("cu_deleted", 0)
            ->first();

        if(count($sirelo) > 0) {
            $language = self::getSireloFieldByCountry( $sirelo->cu_co_code, "we_sirelo_la_code" );

            return self::getSireloFieldByCountry( $sirelo->cu_co_code, "we_sirelo_customer_location").System::getSeoKey($sirelo->cu_city).System::getSeoKey($sirelo->cu_company_name_business);
        }
        else {
            return false;
        }*/
    }

    /**
     * @param $customer | The new customer
     * @param bool $forwards | If the user wants to set forwards
     * @return bool
     */
    public function updateSireloKeyAndForwards( &$customer, $forwards = false ) {

        $sirelo = new Sirelo();
        $system = new System();
        $admin = new AdminController();

        // Get old customer data
        $customer_now = Customer::where("cu_id", $customer->cu_id)
            ->leftJoin( 'states', 'cu_state', 'st_id' )
            ->first();

        // Old url
        $current_link = $system->getSeoKey( $customer_now->cu_city ).$system->getSeoKey( $customer_now->cu_company_name_business );

        // New url
        $new_link = $system->getSeoKey( $customer->cu_city ).$system->getSeoKey( $customer->cu_company_name_business );

            // Get states enabled true or false
        $statesEnabledOld = $sirelo->getSireloFieldByCountry( $customer_now->cu_co_code, 'we_states_enabled' );

        // If the states are enabled with the old url
        if( $statesEnabledOld ) {

            // If the old customer had a state
            if( ! empty( $customer_now->cu_state ) ) {

                // Get the current link with the state
                $current_link = $customer_now->st_key.$current_link;
            }
        }

        // Get states enabled true or false from new customer
        $statesEnabledNew = $sirelo->getSireloFieldByCountry( $customer->cu_co_code, 'we_states_enabled' );

        // If the states are enabled with the new url
        if( $statesEnabledNew ) {

            // Get state id
            $stateID = $admin->getStateIdByCity( $customer->cu_city, $customer->cu_co_code );

            // If we got a state id
            if( ! empty( $stateID ) ) {

                // Save state id to customer
                $customer->cu_state = $stateID;

                // Get state info by id
                $state = State::where( 'st_id', '=', $stateID )->first();

                // Prepend state key to url
                $new_link = $state->st_key.$new_link;
            }
            else {

                // We now have a customer in a state enabled country without a state
                // Inform admins
                $system->sendMessage( [4186,4559,3653], 'Customer state not found', 'Er is een customer in een land waar states enabled zijn maar deze customer heeft geen state. customerID: '.$customer->cu_id );
            }
        }

        $moverdata = MoverData::where([['moda_cu_id', '=', $customer->cu_id]])->first();

        //Update Sirelo key
        $moverdata->moda_sirelo_key = $new_link;

        if( $forwards ) {

            if( $current_link != $new_link && ! empty( $customer->cu_city ) )
            {
                $moda_sirelo_forward_1 = $moverdata->moda_sirelo_forward_1;

                $moverdata->moda_sirelo_forward_1 = $current_link;

                if($new_link != $moda_sirelo_forward_1 && !empty($moda_sirelo_forward_1))
                {
                    $moverdata->moda_sirelo_forward_2 = $moda_sirelo_forward_1;
                }
            }
        }

        $moverdata->save();

        return true;
    }

    public static function country($country_code, $language = "EN")
    {
        $country_column = "co_".strtolower($language);

        $result = Country::select($country_column)
            ->where("co_code", $country_code)
            ->first();

        $result = System::databaseToArray($result);

        if(count($result) > 0)
        {
            return $result[$country_column];
        }
        else
        {
            return false;
        }
    }

    public static function sireloWidgetCheck($cu_id, $url = null)
    {
        $customer = Customer::select("cu_la_code","cu_co_code","cu_city","cu_company_name_business","moda_sirelo_widget_location","moda_sirelo_forward_1","moda_sirelo_forward_2")
            ->leftJoin("mover_data","cu_id","moda_cu_id")
            ->where("cu_id",$cu_id)
            ->where("cu_deleted",0)
            ->first();

        $system = new System();

        if(count($customer) > 0)
        {
            $row = $system->databaseToArray($customer);

            if($url === null)
            {
                $url = $row['moda_sirelo_widget_location'];
            }

            if(empty($url))
            {

                return false;
            }

            $language = self::getSireloFieldByCountry($row['cu_co_code'], "we_sirelo_la_code");

            $checks = [$system->getSeoKey($row['cu_city'], $language).$system->getSeoKey($row['cu_company_name_business'], $language)];

            if(!empty($row['moda_sirelo_forward_1']))
            {
                $checks[] = $row['moda_sirelo_forward_1'];
            }
            if(!empty($row['moda_sirelo_forward_2']))
            {
                $checks[] = $row['moda_sirelo_forward_2'];
            }

            foreach($checks as $check)
            {
                $curlsearch = $system->getExternalCurl($url, 60);
                if($curlsearch !== false)
                {
                    $curlsearch = preg_replace("/<!--(.|\s)*?>/", "", $curlsearch);

                    $sirelo_url = str_replace(".", "\.", str_replace("/", "\/", self::getSireloFieldByCountry($row['cu_co_code'], "we_sirelo_url")));
                    $sirelo_customer_location = str_replace(".", "\.", str_replace("/", "\/", self::getSireloCustomerPage($cu_id)));
                    $sirelo_customer_review_slug = str_replace("/", "\/", self::getSireloFieldByCountry($row['cu_co_code'], "we_sirelo_customer_review_slug"));

                    $iframe_check = preg_match("/<(iframe|embed)([\w\s=\"():%-;]+)?src=[\"\']".$sirelo_url."\/widget\/".str_replace("/", "\/", $check)."[0-9]-[0-9]\/(\?widget_language=[a-z]{2}_[A-Z]{2})?[\"\']([\w\s=\"():%-;]+)?>.*<\/(iframe|embed)>/s", $curlsearch);
                    $href_check = preg_match("/<a ([\w\s=\"():%-;]+)?href=\"".$sirelo_customer_location."(".$sirelo_customer_review_slug.")?\"([\w\s=\"():%-;]+)?>.*<\/a>/s", $curlsearch);

                    if($iframe_check || $href_check)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        else
        {
            return false;
        }
    }
}
