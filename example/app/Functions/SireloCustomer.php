<?php
/**
 * Created by PhpStorm.
 * User: Lorenzo
 * Date: 07/02/2019
 * Time: 15:07
 */

namespace App\Functions;


use App\Data\Survey2Source;
use App\Models\MoverData;
use Illuminate\Support\Facades\DB;

class SireloCustomer
{
	public function getPage($cu_id){

		$result =  DB::table('customers')
            ->leftJoin( 'states', 'cu_state', 'st_id' )
			->where('cu_id', "=" , $cu_id)
			->where('cu_deleted', "=", 0)
			->first();

		if( ! empty( $result ) ) {

			$sirelo = new Sirelo();
			$language_code = $sirelo->getSireloFieldByCountry($result->cu_co_code, "we_sirelo_la_code");
		}

		$system = new System();

		if( ! empty( $language_code ) ) {

			// Start building page
			$page = $sirelo->getSireloFieldByCountry( $result->cu_co_code, 'we_sirelo_customer_location' );

            // If states enabled and there is a state for this result
            if( $sirelo->getSireloFieldByCountry( $result->cu_co_code, 'we_states_enabled' ) && ! empty( $result->st_key ) ) {

                // Add state key to url
                $page .= $result->st_key;
            }

            $page .= $system->getSeoKey( $result->cu_city );
            $page .= $system->getSeoKey( $result->cu_company_name_business );

			return $page;
		}
		else
		{
			return false;
		}
	}

    public function getReviews_old($cu_id){
        $result =
            DB::table('surveys_2')
            ->select(DB::raw('COUNT(*) AS reviews, SUM(`su_rating`) AS rating'))
            ->where('su_submitted', "=" , 1)
            ->where('su_show_on_website', "=", 1)
            ->where(function ($query) use ($cu_id) {
                $query
                    ->where('su_mover', '=', $cu_id)
                    ->orWhere('su_other_mover', '=', $cu_id);
            })
            ->first();

        $rating = 0;

        if($result->reviews > 0)
        {
            $rating = round($result->rating / $result->reviews, 2);
        }

        return ["rating" => $rating, "reviews" => $result->reviews];

        /*$sirelo_html .= Form::createField("sirelo_rating", [
            "label" => "Rating",
            "type" => "text",
            "value" => "Average score of ".number_format(($rating * 2), 1, ",", "")." out of ".$row_surveys['reviews']." reviews",
            "disabled" => "disabled",
            "suffix" => "<label class=\"desc\">".Data::getSireloCustomerStars($rating)."</label>"
        ]);*/

    }

    public function getReviews($cu_id){
        $surveys =
            DB::table('surveys_2')
                ->leftJoin("website_reviews", "su_were_id", "were_id")
                ->where('su_submitted', "=" , 1)
                ->where('su_show_on_website', "=", 1)
                ->where(function ($query) use ($cu_id) {
                    $query
                        ->where('su_mover', '=', $cu_id)
                        ->orWhere('su_other_mover', '=', $cu_id);
                })
                ->get();

        $array = [];
        $average_age = 0;
        $average_source = 0;
        $new_rating = 0;

        $dividend_age = 0;
        $divisor_age = 0;

        $dividend_source = 0;
        $divisor_source = 0;

        $total_review_amount = 0;
        $total_review_score = 0;
        $total_reviews = 0;

        foreach ($surveys as $survey) {
            $total_reviews++;

            $date1 = $survey->su_submitted_timestamp;
            $date2 = date("Y-m-d H:i:s");

            $ts1 = strtotime($date1);
            $ts2 = strtotime($date2);

            $year1 = date('Y', $ts1);
            $year2 = date('Y', $ts2);

            $month1 = date('m', $ts1);
            $month2 = date('m', $ts2);

            $months = (($year2 - $year1) * 12) + ($month2 - $month1);

            if ($months > 60) {
                $months = 60;
            }

            if ($months > 24) {
                $weight_percentage_age = round(((60-$months) /100*2.857) * 100);

            }
            else {
                $weight_percentage_age = 100;
            }

            $dividend_age += ($weight_percentage_age * $survey->su_rating);
            $divisor_age += $weight_percentage_age;

            if ($survey->su_source == Survey2Source::REQUEST) {
                $weight_percentage_source = 100;
            }
            else {
                //Website review
                if ($survey->were_verified && $survey->su_submitted_timestamp > "2020-10-05 00:00:00") {
                    $weight_percentage_source = 100;
                }
                elseif ($survey->were_verified) {
                    $weight_percentage_source = 90;
                }
                else {
                    $weight_percentage_source = 70;
                }
            }

            $dividend_source += ($weight_percentage_source * $survey->su_rating);
            $divisor_source += $weight_percentage_source;

            $correction = round((1 /100) * ($weight_percentage_source * $weight_percentage_age));

            if ($correction < 1) {
                $correction = 1;
            }

            $review_amount_correction = (1 /100) * $correction;
            $review_score = ($survey->su_rating / 100) * $correction;

            //$review_after_correction = $review_score / $review_amount_correction;

            $total_review_amount += $review_amount_correction;
            $total_review_score += $review_score;
        }

        //echo round($total_review_score / $total_review_amount, 2);
        $new_rating = round($total_review_score / $total_review_amount, 2);

        if (is_nan($new_rating)) {
            $new_rating = 0;
        }

        return ["rating" => $new_rating, "reviews" => $total_reviews];

    }

	public function sireloValid($cu_id)
	{
		//Customer
		$mover_data = MoverData::with("customer")->where("moda_cu_id", $cu_id)->first();

		// If website is disabled for sirelo
		// If international moves is not 1
		if(empty($mover_data) || $mover_data->moda_disable_sirelo_export || ( self::isBucket($mover_data->customer->cu_co_code) && $mover_data->moda_international_moves != 1 ) ) {
			return false;
		}

		// Get sirelo website
		$website = DB::table("websites")
			->select("we_id")
			->where("we_sirelo", 1)
			->where("we_sirelo_active", 1)
			->where("we_sirelo_co_code", $mover_data->customer->cu_co_code)
			->get();

		if( $website->count() <= 0 ) {

			$website = DB::table( 'website_subdomains' )
				->select( 'wesu_id' )
				->where( 'wesu_country_code', $mover_data->customer->cu_co_code )
				->leftJoin( 'websites', 'wesu_we_id', 'we_id' )
				->where( 'we_sirelo_bucket', 1 )
				->get();
		}

		// Return true if website is not empty
		return $website->count() ? 1 : 0;
	}

	public function isBucket( $country_code ) {
		// Get subdomain website
		$website = DB::table( 'website_subdomains' )
			->select( 'wesu_id' )
			->where( 'wesu_country_code', $country_code )
			->get();

		return $website->count() ? 1 : 0;
	}


}
