<?php
/**
 * Created by PhpStorm.
 * User: Lorenzo
 * Date: 07/02/2019
 * Time: 15:07
 */

namespace App\Functions;

use App\Data\CustomerRequestDeliveryType;
use App\Data\CustomerRequestDeliveryTypeAndFile;
use App\Models\RequestDelivery;
use App\RequestDeliveries\HTTPDelivery;
use DateTime;
use ErrorException;
use Illuminate\Support\Facades\Log;
use SoapClient;

class RequestDeliveries
{

    public static function receivers($customer_type, $id, $fallback, $type = null)
    {
        $receivers = [];

        if ($customer_type == 1 || $customer_type == 6)
        {
            $id_column = "ktcupo";
        } elseif ($customer_type == 2)
        {
            $id_column = "ktcuqu";
        }

        //Check for type because leads without a customer_portal (Sirelo, Lead Pick) will otherwise return all Service Provider Deliveries :(
        $rd = RequestDelivery::where("rede_customer_type", $customer_type)
            ->where("rede_" . $id_column . "_id", $id);

        if ($type !== null)
        {
            $rd->where("rede_type", $type);
        }

        $rd = $rd->get();

        if ($rd->count())
        {
            foreach ($rd as $row)
            {
                $receivers[] = [
                    "type" => $row['rede_type'],
                    "value" => $row['rede_value'],
                    "extra" => $row['rede_extra'],
                    "rede_id" => $row['rede_id']
                ];
            }
        } else
        {
            $receivers[] = [
                "type" => 1,
                "value" => $fallback,
                "extra" => ""
            ];
        }

        return $receivers;
    }

    public static function send($customer_type, $type, $language = "EN", $fields = null)
    {
        $data = [
            'token' => 'e$FrA#*aAwg4@nmhGZAbIm^d6DC',
            'customer_type' => $customer_type,
            'type' => $type,
            'request_delivery_language' => $language,
            'fields' => $fields
        ];

        // Prepare new cURL resource
        if (strpos($_SERVER['SERVER_NAME'], 'triglobal.info') !== false || System::isCron())
        {
            $curl = curl_init('https://public.triglobal.info/api/request_delivery.php');
        } else
        {
            $curl = curl_init('https://public.triglobal-test-back.nl/api/request_delivery.php');
        }

        // Prepare new cURL resource
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_ENCODING, "");
        curl_setopt($curl, CURLOPT_MAXREDIRS, 10);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));

        // Set HTTP Header for POST request
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
                'Content-Type: application/x-www-form-urlencoded',
                'Content-Length: ' . strlen(http_build_query($data))]
        );

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err)
        {
            Log::debug($err);
        } else
        {
            Log::debug($response);
        }

    }


    public function sendNew($customer_type, $delivery_type, $language = "EN", $fields = null)
    {
        if (env('APP_ENV') != "production")
        {
            return true;
        }

        //Get standard lead translations for deliveries
        $translation = new Translation();
        $translations = $translation->getMultipleInLanguage(
            [
                "moving_details",
                "request_type",
                "moving_size",
                "request_type_val_" . $fields['request']['re_request_type'],
                "moving_size_val_" . $fields['request']['re_moving_size'],
                "moving_date",
                "volume_m3",
                "volume_ft3",
                "storage",
                "packing",
                "assembly",
                "business",
                "storage_val_" . $fields['request']['re_storage'],
                "self_company_val_" . $fields['request']['re_packing'],
                "self_company_val_" . $fields['request']['re_assembly'],
                "moving_from",
                "street",
                "zipcode",
                "city",
                "country",
                "region",
                "residence",
                "contact_details",
                "company_name",
                "first_name",
                "family_name",
                "telephone1",
                "telephone2",
                "email",
                "remarks",
                "customer_request_subject_" . $fields['match_type'],
            ],
            $language
        );

        //Calls the dynamic method as set in the CustomerRequestDeliveryTypeAndFile Class
        $call = CustomerRequestDeliveryTypeAndFile::name($delivery_type)[1];
        self::$call($language, $fields, $translations);
    }

    private function sendHTTP($language, $fields, $translations)
    {
        $http = new HTTPDelivery();
        $http->sendRequest($fields, $translations);
    }

    public static function sendPlainTextMail($language, $fields, $translations)
    {

        $body = $translations["moving_details"] . "\r\n";
        $body .= $translations["request_type"] . ": " . $translations["request_type_val_" . $fields['request']['re_request_type']] . "\r\n";
        $body .= $translations["moving_size"] . ": " . $translations["moving_size_val_" . $fields['request']['re_moving_size']] . "\r\n";
        $body .= $translations["moving_date"] . ": " . $fields['request']['re_moving_date'] . "\r\n";
        $body .= strip_tags($translations["volume_m3"]) . ": " . (($fields['request']['re_volume_m3'] > 0) ? $fields['request']['re_volume_m3'] : $translations["volume_empty"]) . "\r\n";
        $body .= strip_tags($translations["volume_ft3"]) . ": " . (($fields['request']['re_volume_ft3'] > 0) ? $fields['request']['re_volume_ft3'] : $translations["volume_empty"]) . "\r\n";
        $body .= $translations["storage"] . ": " . $translations["storage_val_" . $fields['request']['re_storage']] . "\r\n";
        $body .= $translations["packing"] . ": " . $translations["self_company_val_" . $fields['request']['re_packing']] . "\r\n";
        $body .= $translations["assembly"] . ": " . $translations["self_company_val_" . $fields['request']['re_assembly']] . "\r\n";
        $body .= $translations["business"] . ": " . $translations[(($fields['request']['re_business'] == 1) ? "yes" : "no")] . "\r\n\r\n";

        $body .= $translations["moving_from"] . "\r\n";
        $body .= $translations["street"] . ": " . $fields['request']['re_street_from'] . "\r\n";
        $body .= $translations["zipcode"] . ": " . $fields['request']['re_zipcode_from'] . "\r\n";
        $body .= $translations["city"] . ": " . $fields['request']['re_city_from'] . "\r\n";
        $body .= $translations["country"] . ": " . Data::country($fields['request']['re_co_code_from'], $language) . "\r\n";
        $body .= $translations["region"] . ": " . DataIndex::getRegion($fields['request']['re_reg_id_from']) . "\r\n";
        $body .= $translations["residence"] . ": " . $translations["residence_val_" . $fields['request']['re_residence_from']] . "\r\n\r\n";

        $body .= $translations["moving_to"] . "\r\n";
        $body .= $translations["street"] . ": " . $fields['request']['re_street_to'] . "\r\n";
        $body .= $translations["zipcode"] . ": " . $fields['request']['re_zipcode_to'] . "\r\n";
        $body .= $translations["city"] . ": " . $fields['request']['re_city_to'] . "\r\n";
        $body .= $translations["country"] . ": " . Data::country($fields['request']['re_co_code_to'], $language) . "\r\n";
        $body .= $translations["region"] . ": " . DataIndex::getRegion($fields['request']['re_reg_id_to']) . "\r\n";
        $body .= $translations["residence"] . ": " . $translations["residence_val_" . $fields['request']['re_residence_to']] . "\r\n\r\n";

        $body .= $translations["contact_details"] . "\r\n";
        $body .= $translations["company_name"] . ": " . $fields['request']['re_company_name'] . "\r\n";
        $body .= $translations["first_name"] . ": " . "" . "\r\n";
        $body .= $translations["family_name"] . ": " . $fields['request']['re_full_name'] . "\r\n";
        $body .= $translations["telephone1"] . ": " . $fields['request']['re_telephone1'] . "\r\n";
        $body .= $translations["telephone2"] . ": " . $fields['request']['re_telephone2'] . "\r\n";
        $body .= $translations["email"] . ": " . $fields['request']['re_email'] . "\r\n\r\n";

        $body .= $translations["remarks"] . ": " . str_replace(["\r\n", "\r", "\n"], " ", $fields['request']['re_remarks']) . "\r\n";

        $subject = sprintf($translations["customer_request_subject_" . $fields['match_type']], $translations["request_type_val_" . $fields['request']['re_request_type']], $fields['cu_re_id']);

        echo $subject;
        echo $body;

        //Mail::sendCustom($fields['request_delivery_value'], $fields['request_delivery_extra'], $subject, $body, System::getSetting("requests_email"));
    }

    public static function sendJSON($language, $fields, $translations)
    {

        $split_name['first'] = explode(' ', trim($fields['request']['re_full_name']))[0];
        $split_name['family'] = substr(strstr($fields['request']['re_full_name'], " "), 1);

        $request = [
            "lead_source" => System::getSetting("mail_default_from_name"),
            "lead_id" => $fields['cu_re_id'],
            "request_type" => $translations["request_type_val_" . $fields['request']['re_request_type']],
            "moving_size" => $translations["moving_size_val_" . $fields['request']['re_moving_size']],
            "moving_date" => $fields['request']['re_moving_date'],
            "volume_m3" => $fields['request']['re_volume_m3'],
            "volume_ft3" => $fields['request']['re_volume_ft3'],
            "storage" => $translations["storage_val_" . $fields['request']['re_storage']],
            "packing" => $translations["self_company_val_" . $fields['request']['re_packing']],
            "assembly" => $translations["self_company_val_" . $fields['request']['re_assembly']],
            "business" => $translations[(($fields['request']['re_business'] == 1) ? "yes" : "no")],
            "pickup_street" => $fields['request']['re_street_from'],
            "pickup_zipcode" => $fields['request']['re_zipcode_from'],
            "pickup_city" => $fields['request']['re_city_from'],
            "pickup_country" => $fields['request']['re_co_code_from'],
            "pickup_region" => DataIndex::getRegion($fields['request']['re_reg_id_from']),
            "pickup_residence" => $translations["residence_val_" . $fields['request']['re_residence_from']],
            "delivery_street" => $fields['request']['re_street_to'],
            "delivery_zipcode" => $fields['request']['re_zipcode_to'],
            "delivery_city" => $fields['request']['re_city_to'],
            "delivery_country" => $fields['request']['re_co_code_to'],
            "delivery_region" => DataIndex::getRegion($fields['request']['re_reg_id_to']),
            "delivery_residence" => $translations["residence_val_" . $fields['request']['re_residence_to']],
            "company_name" => $fields['request']['re_company_name'],
            "gender" => "",
            "full_name" => $fields['request']['re_full_name'],
            "first_name" => $split_name['first'],
            "family_name" => $split_name['family'],
            "telephone1" => $fields['request']['re_telephone1'],
            "telephone2" => $fields['request']['re_telephone2'],
            "email" => $fields['request']['re_email'],
            "remarks" => str_replace(["\r\n", "\r", "\n"], " ", $fields['request']['re_remarks'])
        ];


        print_r($request);

        /*
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fields['request_delivery_value']);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request));
        curl_setopt($ch, CURLOPT_HTTPHEADER, ["Content-Type: application/json"]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        $error = curl_error($ch);
        curl_close($ch);
        */
    }

    public static function sendXML($language, $fields, $translations)
    {

        $split_name['first'] = explode(' ', trim($fields['request']['re_full_name']))[0];
        $split_name['family'] = substr(strstr($fields['request']['re_full_name'], " "), 1);

        $xml = new DomDocumentExtended("1.0", "UTF-8");
        $xml->preserveWhiteSpace = false;
        $xml->formatOutput = true;

        $request = $xml->appendChild($xml->createElement("request"));
        $request->appendChild($xml->createElementText("lead_source", System::getSetting("mail_default_from_name")));
        $request->appendChild($xml->createElementText("lead_id", $fields['cu_re_id']));
        $request->appendChild($xml->createElementText("request_type", $translations["request_type_val_" . $fields['request']['re_request_type']]));
        $request->appendChild($xml->createElementText("moving_size", $translations["moving_size_val_" . $fields['request']['re_moving_size']]));
        $request->appendChild($xml->createElementText("moving_date", date("d-m-Y", strtotime($fields['request']['re_moving_date']))));
        $request->appendChild($xml->createElementText("volume_m3", $fields['request']['re_volume_m3']));
        $request->appendChild($xml->createElementText("volume_ft3", $fields['request']['re_volume_ft3']));
        $request->appendChild($xml->createElementText("storage", $translations["storage_val_" . $fields['request']['re_storage']]));
        $request->appendChild($xml->createElementText("packing", $translations["self_company_val_" . $fields['request']['re_packing']]));
        $request->appendChild($xml->createElementText("assembly", $translations["self_company_val_" . $fields['request']['re_assembly']]));
        $request->appendChild($xml->createElementText("business", $translations[(($fields['request']['re_business'] == 1) ? "yes" : "no")]));
        $request->appendChild($xml->createElementText("pickup_street", $fields['request']['re_street_from']));
        $request->appendChild($xml->createElementText("pickup_zipcode", $fields['request']['re_zipcode_from']));
        $request->appendChild($xml->createElementText("pickup_city", $fields['request']['re_city_from']));
        $request->appendChild($xml->createElementText("pickup_country", $fields['request']['re_co_code_from']));
        $request->appendChild($xml->createElementText("pickup_region", DataIndex::getRegion($fields['request']['re_reg_id_from'])));
        $request->appendChild($xml->createElementText("pickup_residence", $translations["residence_val_" . $fields['request']['re_residence_from']]));
        $request->appendChild($xml->createElementText("delivery_street", $fields['request']['re_street_to']));
        $request->appendChild($xml->createElementText("delivery_zipcode", $fields['request']['re_zipcode_to']));
        $request->appendChild($xml->createElementText("delivery_city", $fields['request']['re_city_to']));
        $request->appendChild($xml->createElementText("delivery_country", $fields['request']['re_co_code_to']));
        $request->appendChild($xml->createElementText("delivery_region", DataIndex::getRegion($fields['request']['re_reg_id_to'])));
        $request->appendChild($xml->createElementText("delivery_residence", $translations["residence_val_" . $fields['request']['re_residence_to']]));
        $request->appendChild($xml->createElementText("company_name", $fields['request']['re_company_name']));
        $request->appendChild($xml->createElementText("gender", ""));
        $request->appendChild($xml->createElementText("full_name", $fields['request']['re_full_name']));
        $request->appendChild($xml->createElementText("first_name", $split_name['first']));
        $request->appendChild($xml->createElementText("family_name", $split_name['family']));
        $request->appendChild($xml->createElementText("telephone1", $fields['request']['re_telephone1']));
        $request->appendChild($xml->createElementText("telephone2", $fields['request']['re_telephone2']));
        $request->appendChild($xml->createElementText("email", $fields['request']['re_email']));
        $request->appendChild($xml->createElementText("remarks", str_replace(["\r\n", "\r", "\n"], " ", $fields['request']['re_remarks'])));

        $volume_calculator = $request->appendChild($xml->createElement("volume_calculator"));

        $volume_calculator_data = System::unserialize($fields['request']['re_volume_calculator']);

        if (is_array($volume_calculator_data))
        {
            foreach ($volume_calculator_data as $category_name => $items)
            {
                $category = $volume_calculator->appendChild($xml->createElement("category"));

                $category->appendChild($xml->createElementText("name", $category_name));

                foreach ($items as $item_name => $amount)
                {
                    $item = $category->appendChild($xml->createElement("item"));

                    $item->appendChild($xml->createElementText("name", $item_name));
                    $item->appendChild($xml->createElementText("amount", $amount));
                }
            }
        }

        $subject = sprintf($translations["customer_request_subject_" . $fields['match_type']], $translations["request_type_val_" . $fields['request']['re_request_type']], $fields['cu_re_id']);

        //Mail::sendCustom($fields['request_delivery_value'], $fields['request_delivery_extra'], $subject, $xml->saveXML(), System::getSetting("requests_email"));

    }

    public static function sendCSVAttachment($language, $fields, $translations)
    {

        $lines = [
            [
                "lead_source",
                "lead_id",
                "request_type",
                "moving_size",
                "moving_date",
                "volume_m3",
                "volume_ft3",
                "storage",
                "packing",
                "assembly",
                "business",
                "pickup_street",
                "pickup_zipcode",
                "pickup_city",
                "pickup_country",
                "pickup_region",
                "pickup_residence",
                "delivery_street",
                "delivery_zipcode",
                "delivery_city",
                "delivery_country",
                "delivery_region",
                "delivery_residence",
                "company_name",
                "gender",
                "full_name",
                "first_name",
                "family_name",
                "telephone1",
                "telephone2",
                "email",
                "remarks"
            ],
            [
                System::getSetting("mail_default_from_name"),
                $fields['cu_re_id'],
                $translations["request_type_val_" . $fields['request']['re_request_type']],
                $translations["moving_size_val_" . $fields['request']['re_moving_size']],
                $fields['request']['re_moving_date'],
                $fields['request']['re_volume_m3'],
                $fields['request']['re_volume_ft3'],
                $translations["storage_val_" . $fields['request']['re_storage']],
                $translations["self_company_val_" . $fields['request']['re_packing']],
                $translations["self_company_val_" . $fields['request']['re_assembly']],
                $translations[(($fields['request']['re_business'] == 1) ? "yes" : "no")],
                $fields['request']['re_street_from'],
                $fields['request']['re_zipcode_from'],
                $fields['request']['re_city_from'],
                $fields['request']['re_co_code_from'],
                DataIndex::getRegion($fields['request']['re_reg_id_from']),
                $translations["residence_val_" . $fields['request']['re_residence_from']],
                $fields['request']['re_street_to'],
                $fields['request']['re_zipcode_to'],
                $fields['request']['re_city_to'],
                $fields['request']['re_co_code_to'],
                DataIndex::getRegion($fields['request']['re_reg_id_to']),
                $translations["residence_val_" . $fields['request']['re_residence_to']],
                $fields['request']['re_company_name'],
                "",
                $fields['request']['re_full_name'],
                "",
                "",
                $fields['request']['re_telephone1'],
                $fields['request']['re_telephone2'],
                $fields['request']['re_email'],
                str_replace(["\r\n", "\r", "\n"], " ", $fields['request']['re_remarks'])
            ]
        ];

        $file_name = env("SHARED_FOLDER") . "uploads/temp/request_" . $fields['cu_re_id'] . ".csv";

        $fp = fopen($file_name, "w");

        fprintf($fp, chr(0xEF) . chr(0xBB) . chr(0xBF));

        foreach ($lines as $line)
        {
            fputcsv($fp, $line);
        }

        fclose($fp);

        $subject = sprintf($translations["customer_request_subject_" . $fields['match_type']], $translations["request_type_val_" . $fields['request']['re_request_type']], $fields['cu_re_id']);

        //Mail::sendCustom($fields['request_delivery_value'], $fields['request_delivery_extra'], $subject, $subject, System::getSetting("requests_email"), $file_name);

        //unlink($file_name);
    }

    public static function sendMoveManPro($language, $fields, $translations)
    {

        if ($fields['request_delivery_value'] == "split")
        {
            $split_name['first'] = explode(' ', trim($fields['request']['re_full_name']))[0];
            $split_name['family'] = substr(strstr($fields['request']['re_full_name'], " "), 1);
        }

        $ClientNotes = "Source: " . System::getSetting("mail_default_from_name") . ";";
        $ClientNotes .= "Volume: " . $fields['request']['re_volume_m3'] . " m3 / " . $fields['request']['re_volume_ft3'] . " ft3;";
        $ClientNotes .= "Storage: " . $translations["storage_val_" . $fields['request']['re_storage']] . ";";
        $ClientNotes .= "Packing: " . $translations["self_company_val_" . $fields['request']['re_packing']] . ";";
        $ClientNotes .= "Assembly: " . $translations["self_company_val_" . $fields['request']['re_assembly']] . ";";
        $ClientNotes .= "Remarks: " . str_replace(["\r\n", "\r", "\n"], " ", $fields['request']['re_remarks']) . ";";

        $request = [
            "MoverCompanyID" => $fields['request_delivery_value'],
            "ClientCompanyName" => $fields['request']['re_company_name'],
            "ClientTitle" => "",
            "ClientFirstname" => ($fields['request_delivery_value'] == "split" ? $split_name['first'] : $fields['request']['re_full_name']),
            "ClientSurname" => ($fields['request_delivery_value'] == "split" ? $split_name['family'] : ""),
            "ClientAddress1" => $fields['request']['re_street_from'],
            "ClientAddress2" => $fields['request']['re_city_from'],
            "ClientAddress3" => DataIndex::getRegion($fields['request']['re_reg_id_from']),
            "ClientAddress4" => Data::country($fields['request']['re_co_code_from'], $language),
            "ClientPostcode" => $fields['request']['re_zipcode_from'],
            "ClientTelephone" => $fields['request']['re_telephone1'],
            "ClientMobile" => $fields['request']['re_telephone2'],
            "ClientFax" => "",
            "ClientEmail" => $fields['request']['re_email'],
            "ClientDCompanyName" => $fields['request']['re_company_name'],
            "ClientDTitle" => "",
            "ClientDFirstname" => "",
            "ClientDSurname" => $fields['request']['re_full_name'],
            "ClientDAddress1" => $fields['request']['re_street_to'],
            "ClientDAddress2" => $fields['request']['re_city_to'],
            "ClientDAddress3" => DataIndex::getRegion($fields['request']['re_reg_id_to']),
            "ClientDAddress4" => Data::country($fields['request']['re_co_code_to'], $language),
            "ClientDPostcode" => $fields['request']['re_zipcode_to'],
            "ClientDTelephone" => $fields['request']['re_telephone1'],
            "ClientDMobile" => $fields['request']['re_telephone2'],
            "ClientDFax" => "",
            "ClientDEmail" => $fields['request']['re_email'],
            "ClientNotes" => $ClientNotes,
            "ClientBuildingType" => $translations["residence_val_" . $fields['request']['re_residence_from']],
            "ClientAccess" => "",
            "ClientParking" => "",
            "ClientDirections" => "",
            "ClientDBuildingType" => $translations["residence_val_" . $fields['request']['re_residence_to']],
            "ClientDAccess" => "",
            "ClientDParking" => "",
            "ClientDDirections" => "",
            "ClientParentRefNumber" => "",
            "ClientTaxNumber" => "",
            "ClientRemovalPackDate" => "2000-01-01",
            "ClientRemovalDate" => $fields['request']['re_moving_date'],
            "ClientRemovalDeliveryDate" => "2000-01-01",
            "ClientRemovalPeriod" => ""
        ];

        dd($request);

        /*$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://service.moveman.net/MoveManLiveServerService.asmx/SubmitEnquiriesFull");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($request));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);



        if(!(strpos($result, 'OK') !== false))
        {
            System::sendMessage([3653, 4186], "RequestDelivery Result (Moveman Pro)", $result);
        }*/
    }

    public static function sendMoveWare($language, $fields, $translations)
    {

        $xml = new DomDocumentExtended("1.0", "UTF-8");
        $xml->preserveWhiteSpace = false;
        $xml->formatOutput = true;

        $MWConnect = $xml->appendChild($xml->createElement("MWConnect"));
        $MWConnect->setAttribute("xmlns", "http://www.moveware.com.au/MovewareConnect.xsd");

        $Details = $MWConnect->appendChild($xml->createElement("Details"));
        $Details->appendChild($xml->createElementText("Date", date("Y-m-d H:i:s:") . ":000"));
        $Details->appendChild($xml->createElementText("Subject", "Quote Request"));
        $Details->appendChild($xml->createElementText("Comments", str_replace(["\r\n", "\r", "\n"], " ", $fields['request']['re_remarks'])));

        $Webquote = $MWConnect->appendChild($xml->createElement("MessageType"))->appendChild($xml->createElement("WebQuote"));
        $Webquote->appendChild($xml->createElementText("CompanyToMQID", $fields['request_delivery_value']));

        $Removal = $Webquote->appendChild($xml->createElement("Removal"));
        $Removal->appendChild($xml->createElementText("Referral", "TriGlobal"));
        $Removal->appendChild($xml->createElementText("Branch", $fields['request_delivery_extra']));
        $Removal->appendChild($xml->createElementText("EstMoveDate", date("d/m/Y", strtotime($fields['request']['re_moving_date']))));
        $Removal->appendChild($xml->createElementText("BillerName", System::getSetting("mail_default_from_name")));
        $Removal->appendChild($xml->createElementText("Order", $fields['cu_re_id']));
        $Removal->appendChild($xml->createElementText("InternalNotes", str_replace(["\r\n", "\r", "\n"], " ", $fields['request']['re_remarks'])));
        $Dates = $Removal->appendChild($xml->createElement("Dates"));
        $Dates->appendChild($xml->createElementText("type", "Uplift"));
        $Dates->appendChild($xml->createElementText("Date", date("Y-m-d", strtotime($fields['request']['re_moving_date']))));
        $Removal->appendChild($xml->createElementText("ixType", "DD"));

        $Origin = $Removal->appendChild($xml->createElement("Origin"));

        $Name = $Origin->appendChild($xml->createElement("Name"));
        $Name->appendChild($xml->createElementText("Name", $fields['request']['re_full_name']));
        $Name->appendChild($xml->createElementText("Email", $fields['request']['re_email']));
        $Name->appendChild($xml->createElementText("Phone", $fields['request']['re_telephone1']));
        $Name->appendChild($xml->createElementText("Mobile", $fields['request']['re_telephone2']));

        $Address = $Origin->appendChild($xml->createElement("Address"));
        $Address->appendChild($xml->createElement("Address1", $fields['request']['re_street_from']));
        $Address->appendChild($xml->createElement("Suburb", $fields['request']['re_city_from']));
        $Address->appendChild($xml->createElement("State", DataIndex::getRegion($fields['request']['re_reg_id_from'])));

        $CountryCode = $Address->appendChild($xml->createElement("CountryCode"));
        $CountryCode->appendChild($xml->createElement("CountryDesc", $fields['request']['re_city_from']));
        $CountryCode->appendChild($xml->createElement("CountryState", DataIndex::getRegion($fields['request']['re_reg_id_from'])));
        $CountryCode->appendChild($xml->createElement("CountryCountry", $fields['request']['re_co_code_from']));

        $Address->appendChild($xml->createElement("Postcode", $fields['request']['re_zipcode_from']));
        $Address->appendChild($xml->createElement("Access"));
        $Address->appendChild($xml->createElement("Comments"));

        $Destination = $Removal->appendChild($xml->createElement("Destination"));

        $Name = $Destination->appendChild($xml->createElement("Name"));
        $Name->appendChild($xml->createElementText("Name", $fields['request']['re_full_name']));
        $Name->appendChild($xml->createElementText("Email", $fields['request']['re_email']));
        $Name->appendChild($xml->createElementText("Phone", $fields['request']['re_telephone1']));
        $Name->appendChild($xml->createElementText("Mobile", $fields['request']['re_telephone2']));

        $Address = $Destination->appendChild($xml->createElement("Address"));
        $Address->appendChild($xml->createElement("Address1", $fields['request']['re_street_to']));
        $Address->appendChild($xml->createElement("Suburb", $fields['request']['re_city_to']));
        $Address->appendChild($xml->createElement("State", DataIndex::getRegion($fields['request']['re_reg_id_to'])));

        $CountryCode = $Address->appendChild($xml->createElement("CountryCode"));
        $CountryCode->appendChild($xml->createElement("CountryDesc", $fields['request']['re_city_to']));
        $CountryCode->appendChild($xml->createElement("CountryState", DataIndex::getRegion($fields['request']['re_reg_id_to'])));
        $CountryCode->appendChild($xml->createElement("CountryCountry", $fields['request']['re_co_code_to']));

        $Address->appendChild($xml->createElement("Postcode", $fields['request']['re_zipcode_to']));
        $Address->appendChild($xml->createElement("Access"));
        $Address->appendChild($xml->createElement("Comments"));

        $Removal->appendChild($xml->createElement("SessionID", rand(111111111111111111, 999999999999999999)));

        $data = [
            "companyname" => System::getSetting("mail_default_from_name"),
            "companyserial" => "76VK-9ZV6-JSB5",
            "procedure" => "setxml",
            "optionalval" => $xml->saveXML()
        ];

        dd($data);


        /*$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://www.moveconnect.com/webtransfer.aspx");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);

        System::sendMessage(3653, "Response MoveWare", System::serialize($result));
        */
    }

    public static function sendAlliedPickfords($language, $fields, $translations)
    {

        $xml = new DomDocumentExtended("1.0", "UTF-8");
        $xml->preserveWhiteSpace = false;
        $xml->formatOutput = true;

        $quoterequest = $xml->appendChild($xml->createElement("quoterequest"));
        $quoterequest->appendChild($xml->createElementCDATA("timestamp", date("YmdHis")));
        $quoterequest->appendChild($xml->createElementCDATA("leadsource", System::getSetting("mail_default_from_name")));
        $quoterequest->appendChild($xml->createElementCDATA("title", ""));
        $quoterequest->appendChild($xml->createElementCDATA("firstname", ""));
        $quoterequest->appendChild($xml->createElementCDATA("surname", $fields['request']['re_full_name']));
        $quoterequest->appendChild($xml->createElementCDATA("phone", $fields['request']['re_telephone1']));
        $quoterequest->appendChild($xml->createElementCDATA("movetype", "International"));
        $quoterequest->appendChild($xml->createElementCDATA("email", $fields['request']['re_email']));
        $quoterequest->appendChild($xml->createElementCDATA("phonetype", "Home"));
        $quoterequest->appendChild($xml->createElementCDATA("howtocontact", "Phone"));
        $quoterequest->appendChild($xml->createElementCDATA("alternatephone1", $fields['request']['re_telephone2']));
        $quoterequest->appendChild($xml->createElementCDATA("fundingmove", (($fields['request']['re_business'] == 1) ? "Company" : "Private")));
        $quoterequest->appendChild($xml->createElementCDATA("companyname", $fields['request']['re_company_name']));
        $quoterequest->appendChild($xml->createElementCDATA("pickuphousetype", $translations["residence_val_" . $fields['request']['re_residence_from']]));
        $quoterequest->appendChild($xml->createElementCDATA("pickupaddress1", $fields['request']['re_street_from']));
        $quoterequest->appendChild($xml->createElementCDATA("pickupsuburb", $fields['request']['re_city_from']));
        $quoterequest->appendChild($xml->createElementCDATA("pickupstate", DataIndex::getRegion($fields['request']['re_reg_id_from'])));
        $quoterequest->appendChild($xml->createElementCDATA("pickuppostcode", $fields['request']['re_zipcode_from']));
        $quoterequest->appendChild($xml->createElementCDATA("movedate", date("d-m-Y", strtotime($fields['request']['re_moving_date']))));
        $quoterequest->appendChild($xml->createElementCDATA("furnishingsamount", $translations["moving_size_val_" . $fields['request']['re_moving_size']]));
        $quoterequest->appendChild($xml->createElementCDATA("deliveryhousetype", $translations["residence_val_" . $fields['request']['re_residence_to']]));
        $quoterequest->appendChild($xml->createElementCDATA("deliveryaddress1", $fields['request']['re_street_to']));
        $quoterequest->appendChild($xml->createElementCDATA("deliverysuburb", $fields['request']['re_city_to']));
        $quoterequest->appendChild($xml->createElementCDATA("deliverystate", DataIndex::getRegion($fields['request']['re_reg_id_to'])));
        $quoterequest->appendChild($xml->createElementCDATA("deliverypostcode", $fields['request']['re_zipcode_to']));
        $quoterequest->appendChild($xml->createElementCDATA("deliverycountry", Data::country($fields['request']['re_co_code_to'], $language)));

        $additional_services = [];

        if ($fields['request']['re_storage'] == 1)
        {
            $additional_services[] = "\"Storage\"";
        }
        if ($fields['request']['re_packing'] == 2)
        {
            $additional_services[] = "\"Packing\"";
        }
        if ($fields['request']['re_assembly'] == 2)
        {
            $additional_services[] = "\"Assembly\"";
        }

        $quoterequest->appendChild($xml->createElementCDATA("additionalservices", implode(", ", $additional_services)));
        $quoterequest->appendChild($xml->createElementCDATA("additionalcomments", str_replace(["\r\n", "\r", "\n"], " ", $fields['request']['re_remarks'])));

        $volume_calculator = System::unserialize($fields['request']['re_volume_calculator']);

        $specific_items = "";

        if (is_array($volume_calculator))
        {
            foreach ($volume_calculator as $category => $items)
            {
                foreach ($items as $item => $amount)
                {
                    $specific_items .= "<b>" . ucfirst($item) . "</b> No Of Items=" . $amount . "<br />";
                }
            }
        }

        $quoterequest->appendChild($xml->createElementCDATA("specificitems", $specific_items));

        $subject = "Lead submitted by: " . System::getSetting("mail_default_from_name");

        dd($subject, $quoterequest);

        //Mail::sendCustom($fields['request_delivery_value'], $fields['request_delivery_extra'], $subject, $xml->saveXML(), System::getSetting("requests_email"));

    }

    public static function sendMoverworX($language, $fields, $translations)
    {

        if ($fields['request_delivery_extra'] == "split")
        {
            $split_name['first'] = explode(' ', trim($fields['request']['re_full_name']))[0];
            $split_name['family'] = substr(strstr($fields['request']['re_full_name'], " "), 1);
        } else
        {
            $split_name['first'] = "";
            $split_name['family'] = "";
        }

        $body = "VOLUME: " . (($fields['request']['re_volume_ft3'] > 0) ? round($fields['request']['re_volume_ft3']) . " cuft" : "0") . "\r\n";
        $body .= "WEIGHT: 0\r\n\r\n";

        $body .= "MOVE DATE: " . date("m/d/Y", strtotime($fields['request']['re_moving_date'])) . "\r\n\r\n";

        $body .= "FROM STREET ADDRESS: " . $fields['request']['re_street_from'] . "\r\n";
        $body .= "CITY: " . $fields['request']['re_city_from'] . "\r\n";
        $body .= "STATE:\r\n";
        $body .= "ZIP: " . $fields['request']['re_zipcode_from'] . "\r\n";
        $body .= "COUNTRY: " . Data::country($fields['request']['re_co_code_from'], $language) . "\r\n";
        $body .= "Stairs:\r\n";
        $body .= "Elevators:\r\n\r\n";

        $body .= "TO STREET ADDRESS: " . $fields['request']['re_street_to'] . "\r\n";
        $body .= "CITY: " . $fields['request']['re_city_to'] . "\r\n";
        $body .= "STATE:\r\n";
        $body .= "ZIP: " . $fields['request']['re_zipcode_to'] . "\r\n";
        $body .= "COUNTRY: " . Data::country($fields['request']['re_co_code_to'], $language) . "\r\n";
        $body .= "Stairs:\r\n";
        $body .= "Elevators:\r\n\r\n";

        $body .= "Customer FIRST NAME: " . ($split_name['first'] != "" ? $split_name['first'] : "") . "\r\n";
        $body .= "Customer LAST NAME: " . ($split_name['family'] != "" ? $split_name['family'] : $fields['request']['re_full_name']) . "\r\n";
        $body .= "Telephone: " . $fields['request']['re_telephone1'] . "\r\n";
        $body .= "Work:\r\n";
        $body .= "Cell: " . $fields['request']['re_telephone2'] . "\r\n";
        $body .= "Pager:\r\n";
        $body .= "Fax:\r\n";
        $body .= "Email: " . $fields['request']['re_email'] . "\r\n";
        $body .= "Contact Preference: No Preference\r\n\r\n";

        $body .= "NOTES: " . str_replace(["\r\n", "\r", "\n"], " ", $fields['request']['re_remarks']) . "\r\n\r\n";

        $body .= "Source: " . str_replace("www.", "", System::getSetting("company_website"));

        $subject = sprintf($translations["customer_request_subject_" . $fields['match_type']], $translations["request_type_val_" . $fields['request']['re_request_type']], $fields['cu_re_id']);

        echo $body;
        echo "<br>";
        echo $subject;

        //Mail::sendCustom($fields['request_delivery_value'], "", $subject, $body, System::getSetting("requests_email"));
    }

    public static function sendMovegistics($language, $fields, $translations)
    {

        $split_name['first'] = explode(' ', trim($fields['request']['re_full_name']))[0];
        $split_name['family'] = substr(strstr($fields['request']['re_full_name'], " "), 1);

        $xml = new DomDocumentExtended("1.0", "UTF-8");
        $xml->preserveWhiteSpace = false;
        $xml->formatOutput = true;

        $MoverLead = $xml->appendChild($xml->createElement("MoverLead"));
        $MoverLead->setAttribute("version", "1.0");
        $MoverLead->setAttribute("source", System::getSetting("mail_default_from_name"));

        $UserContact = $MoverLead->appendChild($xml->createElement("UserContact"));
        $UserContact->appendChild($xml->createElementText("FirstName", $split_name['first']));
        $UserContact->appendChild($xml->createElementText("LastName", $split_name['family']));
        $UserContact->appendChild($xml->createElementText("EmailAddress", $fields['request']['re_email']));
        $UserContact->appendChild($xml->createElementText("HomePhone", $fields['request']['re_telephone1']));
        $UserContact->appendChild($xml->createElementText("BestTimeCall", ""));
        $UserContact->appendChild($xml->createElementText("ContactAtWork", ""));
        $UserContact->appendChild($xml->createElementText("Address1", ""));
        $UserContact->appendChild($xml->createElementText("Address2", ""));
        $UserContact->appendChild($xml->createElementText("City", ""));
        $UserContact->appendChild($xml->createElementText("State", ""));
        $UserContact->appendChild($xml->createElementText("Zipcode", ""));
        $UserContact->appendChild($xml->createElementText("Comments", $fields['request']['re_remarks']));

        $MoveDetails = $MoverLead->appendChild($xml->createElement("MoveDetails"));
        $MoveDetails->appendChild($xml->createElementText("MoveType", $translations["request_type_val_" . $fields['request']['re_request_type']]));
        $MoveDetails->appendChild($xml->createElementText("FromCity", $fields['request']['re_city_from']));
        $MoveDetails->appendChild($xml->createElementText("FromState", DataIndex::getRegion($fields['request']['re_reg_id_from'])));
        $MoveDetails->appendChild($xml->createElementText("FromZip", $fields['request']['re_zipcode_from']));
        $MoveDetails->appendChild($xml->createElementText("FromCountry", Data::country($fields['request']['re_co_code_from'], $language)));
        $MoveDetails->appendChild($xml->createElementText("ToCity", $fields['request']['re_city_to']));
        $MoveDetails->appendChild($xml->createElementText("ToState", DataIndex::getRegion($fields['request']['re_reg_id_to'])));
        $MoveDetails->appendChild($xml->createElementText("ToZip", $fields['request']['re_zipcode_to']));
        $MoveDetails->appendChild($xml->createElementText("ToCountry", Data::country($fields['request']['re_co_code_to'], $language)));
        $MoveDetails->appendChild($xml->createElementText("MoveDate", date("d/m/Y", strtotime($fields['request']['re_moving_date']))));
        $MoveDetails->appendChild($xml->createElementText("MoveSize", 0));

        dd(["yourData" => $xml->saveXML()]);

        /* $ch = curl_init();
         curl_setopt($ch, CURLOPT_URL, $fields['request_delivery_value']);
         curl_setopt($ch, CURLOPT_POST, true);
         curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(["yourData" => $xml->saveXML()]));
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
         $result = curl_exec($ch);
         curl_close($ch);

         System::sendMessage([3653], "RequestDelivery Result (Movegistics)", $result);
         System::sendMessage([3653], "RequestDelivery Result (Movegistics) XML", System::serialize($xml->saveXML()));
        */

    }

    public static function sendGranot($language, $fields, $translations)
    {

        $request = [
            "servtypeid" => 104,
            "leadno" => $fields['cu_re_id'],
            "firstname" => "",
            "lastname" => $fields['request']['re_full_name'],
            "ocity" => $fields['request']['re_city_from'],
            "ostate" => substr(DataIndex::getRegion($fields['request']['re_reg_id_from']), 0, 20),
            "ozip" => $fields['request']['re_zipcode_from'],
            "ocountry" => Data::country($fields['request']['re_co_code_from'], $language),
            "dcity" => $fields['request']['re_city_to'],
            "dstate" => substr(DataIndex::getRegion($fields['request']['re_reg_id_to']), 0, 20),
            "dzip" => $fields['request']['re_zipcode_to'],
            "dcountry" => Data::country($fields['request']['re_co_code_to'], $language),
            "volume" => $fields['request']['re_volume_ft3'],
            "movesize" => $translations["moving_size_val_" . $fields['request']['re_moving_size']],
            "notes" => str_replace(["\r\n", "\r", "\n"], " ", $fields['request']['re_remarks']),
            "movedte" => date("m/d/Y", strtotime($fields['request']['re_moving_date'])),
            "email" => $fields['request']['re_email'],
            "phone1" => $fields['request']['re_telephone1'],
            "phone2" => $fields['request']['re_telephone2']
        ];

        dd(http_build_query($request));

        /*$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://lead.hellomoving.com/LEADSGWHTTP.lidgw?&API_ID=6FD8F4FF31D2&MOVERREF=".$fields['request_delivery_value']);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($request));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);

        if(!(strpos($result, 'OK') !== false))
        {
            System::sendMessage([3653, 4186], "RequestDelivery Result (Granot)", $result);
        }*/


    }

    public static function sendSalesforce($language, $fields, $translations)
    {
        $service = "";

        if ($fields['request']['re_request_type'] == 1)
        {
            if ($fields['request']['re_destination_type'] == 1)
            {
                $service = "Household Goods - International";
            } elseif ($fields['re_destination_type'] == 2)
            {
                $service = "Household Goods - Domestic";
            }
        } elseif ($fields['request']['re_request_type'] == 2)
        {
            $service = "Excess Baggage";
        } elseif ($fields['request']['re_request_type'] == 3)
        {
            if ($fields['request']['re_moving_size'] == 5)
            {
                $service = "Pet Relocation";
            } elseif ($fields['request']['re_moving_size'] == 6)
            {
                $service = "Car Shipping";
            } elseif ($fields['request']['re_moving_size'] == 7)
            {
                $service = "Shipping";
            }
        } elseif ($fields['request']['re_request_type'] == 4)
        {
            $service = "Office Moving";
        }

        $request = [
            "oid" => $fields['request_delivery_value'],
            "retURL" => "http://",
            "recordType" => "01220000000K3SC",
            "lead_source" => System::getSetting("mail_default_from_name"),
            "first_name" => "",
            "last_name" => $fields['request']['re_full_name'],
            "email" => $fields['request']['re_email'],
            "company" => $fields['request']['re_company_name'],
            "city" => $fields['request']['re_city_from'],
            "country" => Data::country($fields['request']['re_co_code_from'], $language),
            "00N20000003ZWmq" => $service,
            "phone" => $fields['request']['re_telephone1']
        ];

        dd(http_build_query($request));

        /*$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($request));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);

        if(!empty($result))
        {
            System::sendMessage([3653, 4186], "RequestDelivery Result (Salesforce)", $result);
        }*/

    }

    public static function sendVoxme($language, $fields, $translations)
    {
        $xml = new DomDocumentExtended("1.0", "UTF-8");
        $xml->preserveWhiteSpace = false;
        $xml->formatOutput = true;

        $MovingData = $xml->appendChild($xml->createElement("MovingData"));
        $MovingData->setAttribute("ID", "10000");

        $GeneralInfo = $MovingData->appendChild($xml->createElement("GeneralInfo"));
        $GeneralInfo->appendChild($xml->createElementText("ClientSalutation", ""));
        $GeneralInfo->appendChild($xml->createElementText("ClientFirstName", ""));
        $GeneralInfo->appendChild($xml->createElementText("Name", $fields['request']['re_full_name']));
        $GeneralInfo->appendChild($xml->createElement("ConsigneeName"));
        $GeneralInfo->appendChild($xml->createElementText("EstimatorName", "NEW LEAD"));
        $GeneralInfo->appendChild($xml->createElementText("AgentJobId", "NEW LEAD - " . date("d M, Y H:i")));
        $GeneralInfo->appendChild($xml->createElementText("SourceOfRequest", System::getSetting("mail_default_from_name")));
        $GeneralInfo->appendChild($xml->createElementText("EstimationDate", date("m/d/Y", strtotime($fields['request']['re_moving_date']))));
        $GeneralInfo->appendChild($xml->createElementText("EMFID", $fields['cu_re_id']));

        $Address = $GeneralInfo->appendChild($xml->createElement("Address"));
        $Address->appendChild($xml->createElementText("Company", $fields['request']['re_company_name']));
        $Address->appendChild($xml->createElementText("Street", $fields['request']['re_street_from']));
        $Address->appendChild($xml->createElementText("City", $fields['request']['re_city_from']));
        $Address->appendChild($xml->createElementText("State", DataIndex::getRegion($fields['request']['re_reg_id_from'])));
        $Address->appendChild($xml->createElementText("Country", Data::country($fields['request']['re_co_code_from'], $language)));
        $Address->appendChild($xml->createElementText("Zip", $fields['request']['re_zipcode_from']));
        $Address->appendChild($xml->createElementText("PrimaryPhone", $fields['request']['re_telephone1']));
        $Address->appendChild($xml->createElementText("SecondaryPhone", $fields['request']['re_telephone2']));
        $Address->appendChild($xml->createElementText("email", $fields['request']['re_email']));
        $Address->appendChild($xml->createElement("fax"));

        $AccessInfo = $Address->appendChild($xml->createElement("AccessInfo"));
        $AccessInfo->appendChild($xml->createElementText("Floor", 0));
        $AccessInfo->appendChild($xml->createElementText("PropertyType", (($fields['request']['re_residence_from'] == 0) ? "Unknown" : $translations["residence_val_" . $fields['request']['re_residence_from']])));
        $AccessInfo->appendChild($xml->createElementText("PropertySize", $translations["moving_size_val_" . $fields['request']['re_moving_size']]));

        $Address->appendChild($xml->createElement("Comment"));

        $Destination = $GeneralInfo->appendChild($xml->createElement("Destination"));
        $Destination->appendChild($xml->createElement("Company"));
        $Destination->appendChild($xml->createElementText("Street", $fields['request']['re_street_to']));
        $Destination->appendChild($xml->createElementText("City", $fields['request']['re_city_to']));
        $Destination->appendChild($xml->createElementText("State", DataIndex::getRegion($fields['request']['re_reg_id_to'])));
        $Destination->appendChild($xml->createElementText("Country", Data::country($fields['request']['re_co_code_to'], $language)));
        $Destination->appendChild($xml->createElementText("Zip", $fields['request']['re_zipcode_to']));
        $Destination->appendChild($xml->createElement("PrimaryPhone"));
        $Destination->appendChild($xml->createElement("SecondaryPhone"));
        $Destination->appendChild($xml->createElement("email"));
        $Destination->appendChild($xml->createElement("fax"));

        $AccessInfo = $Destination->appendChild($xml->createElement("AccessInfo"));
        $AccessInfo->appendChild($xml->createElementText("Floor", 0));
        $AccessInfo->appendChild($xml->createElementText("PropertyType", (($fields['request']['re_residence_to'] == 0) ? "Unknown" : $translations["residence_val_" . $fields['request']['re_residence_to']])));
        $AccessInfo->appendChild($xml->createElement("PropertySize"));

        $Destination->appendChild($xml->createElement("Comment"));

        $Preferences = $GeneralInfo->appendChild($xml->createElement("Preferences"));
        $Preferences->appendChild($xml->createElementText("PreferredLanguage", DataIndex::getLanguage($fields['request']['re_la_code'])));
        $Preferences->appendChild($xml->createElement("PackingTime"));
        $Preferences->appendChild($xml->createElement("VacationDate"));
        $Preferences->appendChild($xml->createElement("ServiceType"));
        $Preferences->appendChild($xml->createElement("Comment"));

        $GeneralInfo->appendChild($xml->createElementText("Comment", str_replace(["\r\n", "\r", "\n"], " ", $fields['request']['re_remarks'])));
        $GeneralInfo->appendChild($xml->createElement("SurveySignature"));

        $file_name = env("SHARED_FOLDER") . "uploads/temp/mf" . $fields['request']['re_full_name'] . "_" . $fields['cu_re_id'] . ".xml";

        $xml->save($file_name);

        $subject = sprintf($translations["customer_request_subject_" . $fields['match_type']], $translations["request_type_val_" . $fields['request']['re_request_type']], $fields['cu_re_id']);

        /*Mail::sendCustom($fields['request_delivery_value'], $fields['request_delivery_extra'], $subject, $subject, System::getSetting("requests_email"), $file_name);

        unlink($file_name);*/
    }

    public static function sendeMover($language, $fields, $translations)
    {

        $request = [
            "Provider" => System::getSetting("mail_default_from_name"),
            "CompanyID" => $fields['request_delivery_value'],
            "PortalID" => $fields['request_delivery_extra'],
            "CustomerName" => $fields['request']['re_full_name'],
            "EMail" => $fields['request']['re_email'],
            "Phones" => $fields['request']['re_telephone1'],
            "JobDate" => date("m/d/Y", strtotime($fields['request']['re_moving_date'])),
            "OrigCity" => $fields['request']['re_city_from'],
            "OrigState" => DataIndex::getRegion($fields['request']['re_reg_id_from']),
            "OrigZip" => $fields['request']['re_zipcode_from'],
            "OrigCountry" => Data::country($fields['request']['re_co_code_from'], $language),
            "DestCity" => $fields['request']['re_city_to'],
            "DestState" => DataIndex::getRegion($fields['request']['re_reg_id_to']),
            "DestZip" => $fields['request']['re_zipcode_to'],
            "DestCountry" => Data::country($fields['request']['re_co_code_to'], $language),
            "EstimatedLbs" => "",
            "Details" => str_replace(["\r\n", "\r", "\n"], " ", $fields['request']['re_remarks'])
        ];

        dd($request);

        /* $ch = curl_init();
         curl_setopt($ch, CURLOPT_URL, "https://moving.emoversoftware.com/submitlead.aspx?".http_build_query($request));
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
         $result = curl_exec($ch);
         curl_close($ch);

         */
    }

    public static function sendPremiumMovers($language, $fields, $translations)
    {
        $request = [
            "username" => "skydreams",
            "password" => "PeNiNcK",
            "firstname" => "",
            "name" => $fields['request']['re_full_name'],
            "gender" => "",
            "type" => (($fields['request']['re_business'] == 1) ? "Zakelijk" : "Particulier"),
            "company_name" => $fields['request']['re_company_name'],
            "address" => $fields['request']['re_street_from'],
            "address_number" => "",
            "postcode" => $fields['request']['re_zipcode_from'],
            "city" => $fields['request']['re_city_from'],
            "to_address" => $fields['request']['re_street_to'],
            "to_address_number" => "",
            "to_postcode" => $fields['request']['re_zipcode_to'],
            "to_city" => $fields['request']['re_city_to'],
            "date_planned" => $fields['request']['re_moving_date'],
            "email" => $fields['request']['re_email'],
            "phone_number" => $fields['request']['re_telephone1'],
            "mobile" => $fields['request']['re_telephone2'],
            "from_type" => $translations["residence_val_" . $fields['request']['re_residence_from']],
            "from_bedrooms" => "",
            "to_type" => $translations["residence_val_" . $fields['request']['re_residence_to']],
            "description" => "",
            "offerteId" => $fields['cu_re_id'],
            "regio" => DataIndex::getRegion($fields['request']['re_reg_id_from']),
            "bron" => System::getSetting("mail_default_from_name"),
            "opdrachtBeschrijving" => $translations["request_type_val_" . $fields['request']['re_request_type']] . " - " . $translations["moving_size_val_" . $fields['request']['re_moving_size']] . "\r\n" . str_replace(["\r\n", "\r", "\n"], " ", $fields['request']['re_remarks']),
            "inpakWerkzaamheden" => $translations["self_company_val_" . $fields['request']['re_packing']],
            "verhuisiftNodig" => "",
            "aanvraagVerstuurdNaar" => ""
        ];

        /* $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fields['request_delivery_value']);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($request));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);

        */
    }

    public static function sendBestGlobalMovers($language, $fields, $translations)
    {
        $body = "This lead was verified by " . System::getSetting("mail_default_from_name") . " on " . date("d M Y") . " at " . date("h:i A") . "\r\n\r\n\r\n";
        $body .= "First Name: " . "" . "\r\n\r\n";
        $body .= "Last Name: " . $fields['request']['re_full_name'] . "\r\n\r\n";
        $body .= "Tel: " . $fields['request']['re_telephone1'] . "\r\n\r\n";
        $body .= "Email: " . $fields['request']['re_email'] . "\r\n\r\n";
        $body .= "Address Line 1: " . $fields['request']['re_street_from'] . "\r\n\r\n";
        $body .= "Address Line 2: " . $fields['request']['re_city_from'] . "\r\n\r\n";
        $body .= "Postcode: " . $fields['request']['re_zipcode_from'] . "\r\n\r\n";
        $body .= "Origin Country: " . $fields['request']['re_co_code_from'] . "\r\n\r\n";
        $body .= "Destination Country: " . $fields['request']['re_co_code_to'] . "\r\n\r\n";
        $body .= "Who is paying for your move?: " . ($fields['request']['re_business'] == 1 ? "Company" : "Myself") . "\r\n\r\n";
        $body .= "Packing service required?: " . ($fields['request']['re_packing'] == 2 ? "Yes" : "No") . "\r\n\r\n";
        $body .= "Destination Address: " . $fields['request']['re_street_to'] . "\r\n\r\n";
        $body .= "Destination City: " . $fields['request']['re_city_to'] . "\r\n\r\n";
        $body .= "Destination Zipcode / Postcode: " . $fields['request']['re_zipcode_to'] . "\r\n\r\n";
        $body .= "Approximate moving date: " . $fields['request']['re_moving_date'] . "\r\n\r\n";
        $body .= "Size of the move - number of rooms: " . $translations["moving_size_val_" . $fields['request']['re_moving_size']] . "\r\n\r\n";
        $body .= "List of items to be shipped: " . str_replace(["\r\n", "\r", "\n"], " ", $fields['request']['re_remarks']) . "\r\n\r\n\r\n";
        $body .= "Kind regards, " . System::getSetting("mail_default_from_name");

        $subject = "Your Qualified Lead from " . System::getSetting("mail_default_from_name");

        dd($body, $subject);

        //Mail::sendCustom($fields['request_delivery_value'], $fields['request_delivery_extra'], $subject, $body, System::getSetting("requests_email"));
    }

    public static function sendBiardDemenagements($language, $fields, $translations)
    {

        echo $fields['request_delivery_value'] . "|Devis|" . "" . "|" .
            $fields['request']['re_full_name'] . "|" . "" . "|" .
            $fields['request']['re_email'] . "|" .
            $fields['request']['re_telephone1'] . "|" .
            $fields['request']['re_telephone2'] . "|" .
            $fields['request']['re_co_code_from'] . "|" .
            $fields['request']['re_zipcode_from'] . "|" .
            $fields['request']['re_city_from'] . "|" .
            $fields['request']['re_co_code_to'] . "|" .
            $fields['request']['re_zipcode_to'] . "|" .
            $fields['request']['re_city_to'] . "|" .
            str_replace(["\r\n", "\r", "\n"], " ", $fields['request']['re_remarks']) . "|Site K|" .
            $fields['request_delivery_extra'] . "||" .
            $translations["self_company_val_" . $fields['request']['re_assembly']] . "|" .
            $fields['request']['re_moving_date'] . "|" . $fields['request']['re_volume_m3'] . " m3 / " .
            $fields['request']['re_volume_ft3'] . " ft3|";

       /* $client = new SoapClient("http://imis-move.fr/noyau/webservice/webserver/format.wsdl");

        $client->mercato($fields['request_delivery_value'] . "|Devis|" . "" . "|" .
            $fields['request']['re_full_name'] . "|" . "" . "|" .
            $fields['request']['re_email'] . "|" .
            $fields['request']['re_telephone1'] . "|" .
            $fields['request']['re_telephone2'] . "|" .
            $fields['request']['re_co_code_from'] . "|" .
            $fields['request']['re_zipcode_from'] . "|" .
            $fields['request']['re_city_from'] . "|" .
            $fields['request']['re_co_code_to'] . "|" .
            $fields['request']['re_zipcode_to'] . "|" .
            $fields['request']['re_city_to'] . "|" .
            str_replace(["\r\n", "\r", "\n"], " ", $fields['request']['re_remarks']) . "|Site K|" .
            $fields['request_delivery_extra'] . "||" .
            $translations["self_company_val_" . $fields['request']['re_assembly']] . "|" .
            $fields['request']['re_moving_date'] . "|" . $fields['request']['re_volume_m3'] . " m3 / " .
            $fields['request']['re_volume_ft3'] . " ft3|");
       */
    }

    public static function sendFazland($language, $fields, $translations)
    {


       /* $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.fazland.com/api/v1/login_check");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(["username" => $fields['request_delivery_value'], "password" => $fields['request_delivery_extra']]));
        curl_setopt($ch, CURLOPT_HTTPHEADER, ["Content-Type: application/json"]);
        curl_setopt($ch, CURLOPT_USERAGENT, str_replace("www.", "", System::getSetting("company_website")));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);

        $token = json_decode($result)->token;

        if(!empty($token))
        {*/
            $data = [
                "date1" => $fields['request']['re_moving_date']
            ];

            if($fields['request']['re_co_code_from'] == "IT" && $fields['request']['re_co_code_to'] == "IT")
            {
                $locality = "Reggio Emilia";
                $service = 17;

                $data['text1'] = $fields['request']['re_city_from'];
                $data['text2'] = $fields['request']['re_city_to'];
            }
            elseif($fields['request']['re_co_code_from'] == "IT" || $fields['request']['re_co_code_to'] == "IT")
            {
                $locality = (($fields['request']['re_co_code_from'] == "IT") ? $fields['request']['re_city_from'] : $fields['request']['re_city_to']);
                $service = 23;

                $data['text1'] = $fields['request']['re_city_from'];
                $data['text2'] = Data::country($fields['request']['re_co_code_from'], $language);
                $data['text3'] = $fields['request']['re_city_to'];
                $data['text4'] = Data::country($fields['request']['re_co_code_to'], $language);
            }
            else
            {
                $locality = "Lampedusa";
                $service = 2289;

                $additional_services = [];

                if($fields['request']['re_storage'] == 1)
                {
                    $additional_services[] = "key7";
                }
                if($fields['request']['re_packing'] == 2)
                {
                    $additional_services[] = "key9";
                }
                if($fields['request']['re_assembly'] == 2)
                {
                    $additional_services[] = "key11";
                }

                $data['text1'] = Data::country($fields['request']['re_co_code_from'], $language);
                $data['text2'] = $fields['request']['re_city_from'];
                $data['text3'] = Data::country($fields['request']['re_co_code_to'], $language);
                $data['text4'] = $fields['request']['re_city_to'];
                $data['text5'] = $translations["moving_size_val_".$fields['request']['re_moving_size']];
                $data['text6'] = $fields['request']['re_volume_m3'];
                $data['choice1'] = $additional_services;
            }

            $data['textarea1'] = str_replace(["\r\n", "\r", "\n"], " ", $fields['request']['re_remarks']);

            $data = [
                "user" => [
                    "first_name" => "",
                    "last_name" => ((!empty($fields['request']['re_full_name'])) ? $fields['request']['re_full_name'] : "-"),
                    "password" => md5(uniqid(time(), true).uniqid($fields['request']['re_email'], true)),
                    "email" => $fields['request']['re_email'],
                    "phone" => $fields['request']['re_telephone1']
                ],
                "quote" => [
                    "contactType" => 2,
                    "locality" => $locality,
                    "service" => $service,
                    "urgency" => 2,
                    "serviceData" => $data
                ]
            ];

            dd($data);
            /*

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://api.fazland.com/api/v2/affiliates/quotes");
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($ch, CURLOPT_HTTPHEADER, ["Content-Type: application/json", "Authorization: Bearer ".$token]);
            curl_setopt($ch, CURLOPT_USERAGENT, str_replace("www.", "", System::getSetting("company_website")));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $curl_result = curl_exec($ch);
            curl_close($ch);
            */

        }

    public static function sendMyMovingLoads($language, $fields, $translations)
    {
        $movingsize = [
            1 => 6,
            2 => 4,
            3 => 2,
            4 => 1
        ];

        $telephone1 = preg_replace("/\s+/", "", $fields['request']['re_telephone1']);
        $email = preg_replace("/\s+/", "", $fields['request']['re_email']);

        if(filter_var($fields['request']['re_ip_address'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4))
        {
            $ip = $fields['request']['re_ip_address'];
        }
        else
        {
            $ip = "213.127.234.15";
        }

        $split_name['first'] = explode(' ', trim($fields['request']['re_full_name']))[0];
        $split_name['family'] = substr(strstr($fields['request']['re_full_name'], " "), 1);

        if(empty($split_name['family']))
        {
            $split_name['family'] = "Johnson";
        }

        $request = [
            "key" => $fields['request_delivery_value'],
            "customer_name" => $split_name['first']." ".$split_name['family'],
            "customer_phone" => $telephone1,
            "customer_email" => $email,
            "ip" => $ip,
            "from_postal_code" => $fields['request']['re_zipcode_from'],
            "from_place" => $fields['request']['re_city_from'],
            "from_country" => $fields['request']['re_co_code_from'],
            "to_postal_code" => $fields['request']['re_zipcode_to'],
            "to_place" => $fields['request']['re_city_to'],
            "to_country" => $fields['request']['re_co_code_to'],
            "size" => $movingsize[$fields['request']['re_request_type']],
            "moving_date" => $fields['request']['re_moving_date'],
        ];

        //Get US postal regions from DB

        //US postalcode FROM
        if($fields['request']['re_co_code_from'] == "US" && empty($fields['request']['re_zipcode_from']))
        {
            $request['from_admin1_code'] = substr(DataIndex::getRegion($fields['request']['re_reg_id_from']), 0, 2);
        }

        //US postalcode TO
        if($fields['request']['re_co_code_to'] == "US" && empty($fields['request']['re_zipcode_to']))
        {
            $request['to_admin1_code'] = substr(DataIndex::getRegion($fields['request']['re_reg_id_to']), 0, 2);
        }

        //CA postalcode FROM
        if($fields['request']['re_co_code_from'] == "CA" && empty($fields['request']['re_zipcode_from']))
        {
            $request['from_admin1_code'] = self::getPostalCodeCanada($fields['request']['re_reg_id_from']);
        }

        //CA postalcode TO
        if($fields['request']['re_co_code_to'] == "CA" && empty($fields['request']['re_zipcode_to']))
        {
            $request['to_admin1_code'] = self::getPostalCodeCanada($fields['request']['re_reg_id_to']);
        }

        //AU postalcode FROM
        if($fields['request']['re_co_code_from'] == "AU" && empty($fields['request']['re_zipcode_from']))
        {
            $request['from_admin1_code'] = self::getPostalCodeAustralia($fields['request']['re_reg_id_from']);
        }

        //AU postalcode TO
        if($fields['request']['re_co_code_to'] == "AU" && empty($fields['request']['re_zipcode_to']))
        {
            $request['to_admin1_code'] = self::getPostalCodeAustralia($fields['request']['re_reg_id_to']);
        }

        //All other empty postal codes FROM
        if(($fields['request']['re_co_code_from'] != "US" || $fields['request']['re_co_code_from'] != "CA") && empty($fields['request']['re_zipcode_from']))
        {
            $tgb_name = DataIndex::getRegion($fields['request']['re_reg_id_from']);

            if(str_word_count($tgb_name) > 1 && strlen($tgb_name) > 10)
            {
                $temp = explode(' ', $tgb_name);

                $result = '';
                foreach($temp as $t)
                    $result .= ucwords($t[0]);

                $new_name = $result;
            }
            else
            {
                $new_name = substr($tgb_name, 0, 10);
            }
            $request['from_admin1_code'] = $new_name;
        }

        //All other empty postal codes TO
        if(($fields['request']['re_co_code_to'] != "US" && $fields['request']['re_co_code_to'] != "CA" && $fields['request']['re_co_code_to'] != "AU") && empty($fields['request']['re_zipcode_to']))
        {
            $tgb_name = DataIndex::getRegion($fields['request']['re_reg_id_to']);

            if(str_word_count($tgb_name) > 1 && strlen($tgb_name) > 10)
            {
                $temp = explode(' ', $tgb_name);

                $result = '';
                foreach($temp as $t)
                    $result .= ucwords($t[0]);

                $new_name = $result;
            }
            else
            {
                $new_name = substr($tgb_name, 0, 10);
            }
            $request['to_admin1_code'] = $new_name;
        }

        dd(http_build_query($request));


        /*$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://www.mymovingloads.com/services/leads/ping?".http_build_query($request));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $ping = curl_exec($ch);
        $ping = (array)json_decode($ping);

        if($ping['status'] != "success")
        {
            System::sendMessage([3653, 4669], "My Moving Load failed to deliver request: ".$fields['request']['re_id'] , System::serialize($request));
            System::sendMessage([3653, 4669], "My Moving Load PING Error (put this in unserialize.com)", System::serialize($ping));
        }

        curl_close($ch);

        $post_lead = curl_init();
        curl_setopt($post_lead, CURLOPT_URL, "https://www.mymovingloads.com/services/leads/post?".http_build_query($request));
        curl_setopt($post_lead, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($post_lead, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($post_lead);
        $result = (array)json_decode($result);
        curl_close($post_lead);

        System::sendMessage(3653, "My Moving Load Insert Result", System::serialize($result));
        */


    }

    public static function sendRemovalsManager($language, $fields, $translations)
    {
        $split_name['first'] = explode(' ', trim($fields['request']['re_full_name']))[0];
        $split_name['family'] = substr(strstr($fields['request']['re_full_name'], " "), 1);

        $request = [
            "clientToken" => $fields['request_delivery_extra'],
            "lead_source" => System::getSetting("mail_default_from_name"),
            "lead_id" => $fields['cu_re_id'],
            "request_type" => $translations["request_type_val_".$fields['request']['re_request_type']],
            "moving_size" => $translations["moving_size_val_".$fields['request']['re_moving_size']],
            "moving_date" => $fields['request']['re_moving_date'],
            "volume_m3" => $fields['request']['re_volume_m3'],
            "volume_ft3" => $fields['request']['re_volume_ft3'],
            "storage" => $translations["storage_val_".$fields['request']['re_storage']],
            "packing" => $translations["self_company_val_".$fields['request']['re_packing']],
            "assembly" => $translations["self_company_val_".$fields['request']['re_assembly']],
            "business" => $translations[(($fields['request']['re_business'] == 1) ? "yes" : "no")],
            "pickup_street" => $fields['request']['re_street_from'],
            "pickup_zipcode" => $fields['request']['re_zipcode_from'],
            "pickup_city" => $fields['request']['re_city_from'],
            "pickup_country" => $fields['request']['re_co_code_from'],
            "pickup_region" => DataIndex::getRegion($fields['request']['re_reg_id_from']),
            "pickup_residence" => $translations["residence_val_".$fields['request']['re_residence_from']],
            "delivery_street" => $fields['request']['re_street_to'],
            "delivery_zipcode" => $fields['request']['re_zipcode_to'],
            "delivery_city" => $fields['request']['re_city_to'],
            "delivery_country" => $fields['request']['re_co_code_to'],
            "delivery_region" => DataIndex::getRegion($fields['request']['re_reg_id_to']),
            "delivery_residence" => $translations["residence_val_".$fields['request']['re_residence_to']],
            "company_name" => $fields['request']['re_company_name'],
            "gender" => $translations["gender_val_".$fields['request']['re_gender']],
            "full_name" => $fields['request']['re_full_name'],
            "first_name" => $split_name['first'],
            "family_name" => $split_name['family'],
            "telephone1" => $fields['request']['re_telephone1'],
            "telephone2" => $fields['request']['re_telephone2'],
            "email" => $fields['request']['re_email'],
            "remarks" => str_replace(["\r\n", "\r", "\n"], " ", $fields['request']['re_remarks'])
        ];

        dd(http_build_query($request));

        /*$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fields['request_delivery_value']);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($request));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        $error = curl_error($ch);
        curl_close($ch);

        if(!(strpos($result, 'successfully') !== false))
        {
            System::sendMessage([3653, 4186], "RequestDelivery Result (RemovalsManager)", System::serialize($result));
            System::sendMessage([3653, 4186], "RequestDelivery Error (RemovalsManager)", System::serialize($error));
        }*/

    }

    public static function sendEuropeanMoving($language, $fields, $translations)
    {

        /*//Get Zoho CRM token
        $url = "https://accounts.zoho.com/oauth/v2/token?grant_type=refresh_token&client_id=1000.SDQLEA9Z8IYHEMV2CO2ELAXM26RDGH&client_secret=c0168d661a02aeebeaadb548486e399775a877de28&refresh_token=1000.2c353c2551ddacd127252bfc480d1c4b.ad4789de0eb2a730fbc55f67da3d1191";

        $ch_token = curl_init();
        curl_setopt($ch_token, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch_token, CURLOPT_POST, true);
        //curl_setopt($ch_token, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch_token, CURLOPT_URL, $url);
        curl_setopt($ch_token, CURLOPT_SSL_VERIFYPEER, false);
        $actual_result = curl_exec($ch_token);
        $error = curl_error($ch_token);
        curl_close($ch_token);

        if(empty($error)){
            $response = json_decode($actual_result, true);
            $token = $response['access_token'];
        }*/


        $family_name = $fields['request']['re_full_name'];
        $first_name = "";
        $phone = substr($fields['request']['re_telephone1'], 0, 29);
        $email = $fields['request']['re_email'];
        $date =  date( "Y-m-d", strtotime($fields['request']['re_timestamp']));
        $moving_size = $fields['request']['re_moving_size'];
        $remark = $fields['request']['re_remarks'];
        $m3 = $fields['request']['re_volume_m3'];
        $ft3 = $fields['request']['re_volume_ft3'];
        $street_from = $fields['request']['re_street_from'];
        $zipcode_from = $fields['request']['re_zipcode_from'];
        $city_from = $fields['request']['re_city_from'];
        $country_from = Data::country($fields['request']['re_co_code_from'], "EN");
        $collection_date = date( "Y-m-d", strtotime($fields['request']['re_moving_date']));
        $street_to = $fields['request']['re_street_to'];
        $zipcode_to = $fields['request']['re_zipcode_to'];
        $city_to = $fields['request']['re_city_to'];
        $country_to = Data::country($fields['request']['re_co_code_to'], "EN");
        $form_language = $fields['request']['re_la_code'];
        $source = $fields['request_delivery_value'];

//OLD XML lead delivery
        $result = <<<HERE
<Leads>
<row no="1">
<FL val="Name"><![CDATA[$first_name]]></FL>
<FL val="Last Name"><![CDATA[$family_name]]></FL>
<FL val="Phone"><![CDATA[$phone]]></FL>
<FL val="Email"><![CDATA[$email]]></FL>
<FL val="Enquiry Date"><![CDATA[$date]]></FL>
<FL val="No Rooms"><![CDATA[$moving_size]]></FL>
<FL val="Load Description"><![CDATA[$remark]]></FL>
<FL val="Approx. Load Size (m3)"><![CDATA[$m3]]></FL>
<FL val="Approx. Load Weight (kg)"><![CDATA[$ft3]]></FL>
<FL val="Lead Source"><![CDATA[$source]]></FL>
<FL val="Lead Type"><![CDATA[European]]></FL>
<FL val="Coll Address1"><![CDATA[$street_from]]></FL>
<FL val="Coll Address2"><![CDATA[]]></FL>
<FL val="Coll City"><![CDATA[$city_from]]></FL>
<FL val="Collection Postcode"><![CDATA[$zipcode_from]]></FL>
<FL val="Collection Country"><![CDATA[$country_from]]></FL>
<FL val="Collection Date(s)"><![CDATA[$collection_date]]></FL>
<FL val="Del Address1"><![CDATA[$street_to]]></FL>
<FL val="Del Address2"><![CDATA[]]></FL>
<FL val="Del City"><![CDATA[$city_to]]></FL>
<FL val="Delivery Postcode"><![CDATA[$zipcode_to]]></FL>
<FL val="Delivery Country"><![CDATA[$country_to]]></FL>
<FL val="Delivery Date(s)"><![CDATA[$collection_date]]></FL>
<FL val="Overseas"><![CDATA[Yes]]></FL>
<FL val="Language"><![CDATA[$form_language]]></FL>
<FL val="Country Assigned"><![CDATA[United Kingdom]]></FL>
</row>
</Leads>
HERE;

        $url = "https://www.zohoapis.com/crm/v2/Leads/upsert";

        $jsonData['duplicate_check_fields'] = ["Email", "Phone"];
        $jsonData['data'] = [[
            'First_Name' => $first_name,
            'Last_Name' => $family_name,
            'Phone' => $phone,
            'Email' => $email,

            'Lead_Source' => $source,
            'Enquiry_Date' => $date,
            'No_Rooms' => $moving_size,
            'APPROX_LOAD_SIZE_m3' => $m3,
            'Territory_Assigned' => $form_language,

            'Coll_Address1' => $street_from,
            'Collection_Postcode' => $zipcode_from,
            'Collection_Country' => $country_from,
            'Coll_City' => $city_from,

            'Del_Address1' => $street_to,
            'Delivery_Postcode' => $zipcode_to,
            'Delivery_Country' => $country_to,
            'Del_City' => $city_to,

            'Collection_Date_s' => $collection_date,
            'Load_Description' => $remark
        ]];

        dd($jsonData);

    /*
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: Zoho-oauthtoken '.$token,
            'Accept: application/json'
        ));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($jsonData));
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $actual_result = curl_exec($ch);
        $error = curl_error($ch);
        curl_close($ch);

        System::insertHistory("European Moving Delivery Result", $fields['request']['re_id'], System::serialize($actual_result));
        System::insertHistory("European Moving Delivery Error",$fields['request']['re_id'], System::serialize($error));

        System::sendMessage(3653, "European Moving Error",  System::serialize($error));
        System::sendMessage(3653, "European Moving Result",  System::serialize($actual_result));
        System::sendMessage(3653, "European Moving JSON: ".$fields['request']['re_id'],  json_encode($jsonData));
    */


    }

    public static function sendJSONMail($language, $fields, $translations)
    {

        $request = [
            "lead_source" => System::getSetting("mail_default_from_name"),
            "lead_id" => $fields['cu_re_id'],
            "request_type" => $translations["request_type_val_".$fields['request']['re_request_type']],
            "moving_size" => $translations["moving_size_val_".$fields['request']['re_moving_size']],
            "moving_date" => $fields['request']['re_moving_date'],
            "volume_m3" => $fields['request']['re_volume_m3'],
            "volume_ft3" => $fields['request']['re_volume_ft3'],
            "storage" => $translations["storage_val_".$fields['request']['re_storage']],
            "packing" => $translations["self_company_val_".$fields['request']['re_packing']],
            "assembly" => $translations["self_company_val_".$fields['request']['re_assembly']],
            "business" => $translations[(($fields['request']['re_business'] == 1) ? "yes" : "no")],
            "pickup_street" => $fields['request']['re_street_from'],
            "pickup_zipcode" => $fields['request']['re_zipcode_from'],
            "pickup_city" => $fields['request']['re_city_from'],
            "pickup_country" => $fields['request']['re_co_code_from'],
            "pickup_region" => DataIndex::getRegion($fields['request']['re_reg_id_from']),
            "pickup_residence" => $translations["residence_val_".$fields['request']['re_residence_from']],
            "delivery_street" => $fields['request']['re_street_to'],
            "delivery_zipcode" => $fields['request']['re_zipcode_to'],
            "delivery_city" => $fields['request']['re_city_to'],
            "delivery_country" => $fields['request']['re_co_code_to'],
            "delivery_region" => DataIndex::getRegion($fields['request']['re_reg_id_to']),
            "delivery_residence" => $translations["residence_val_".$fields['request']['re_residence_to']],
            "company_name" => $fields['request']['re_company_name'],
            "gender" => "",
            "full_name" => $fields['request']['re_full_name'],
            "first_name" => "",
            "family_name" => "",
            "telephone1" => $fields['request']['re_telephone1'],
            "telephone2" => $fields['request']['re_telephone2'],
            "email" => $fields['request']['re_email'],
            "remarks" => str_replace(["\r\n", "\r", "\n"], " ", $fields['request']['re_remarks']),
            "volume_calculator" => System::unserialize($fields['request']['re_volume_calculator'])
        ];

        $subject = sprintf($translations["customer_request_subject_".$fields['match_type']], $translations["request_type_val_".$fields['request']['re_request_type']], $fields['cu_re_id']);

        dd($subject, $request);

        //Mail::sendCustom($fields['request_delivery_value'], $fields['request_delivery_extra'], $subject, json_encode($request), System::getSetting("requests_email"));



    }

    public static function sendMovinga($language, $fields, $translations)
    {

        $split_name['first'] = explode(' ', trim($fields['request']['re_full_name']))[0];
        $split_name['family'] = substr(strstr($fields['request']['re_full_name'], " "), 1);
        if(empty($split_name['family'])){$split_name['family'] = $split_name['first'];}

        $request = [
            "source" => "Sirelo",
            "lead_id" => "TriGlobal ID: ".$fields['cu_re_id'],
            "move_out_date" => $fields['request']['re_moving_date'],
            "phone" => $fields['request']['re_telephone1'],
            "email" => $fields['request']['re_email'],
            "first_name" => $split_name['first'],
            "last_name" => $split_name['family'],
            "additional_info" => str_replace(["\r\n", "\r", "\n"], " ", $fields['request']['re_remarks']." \n Volume (m3): ".$fields['request']['re_volume_m3']),
            "stops" =>
                [
                    [
                        "country" => $fields['request']['re_co_code_from'],
                        "city" => $fields['request']['re_city_from'],
                        "street" => $fields['request']['re_street_from'],
                        "postal_code" => $fields['request']['re_zipcode_from']
                    ],
                    [
                        "country" => $fields['request']['re_co_code_to'],
                        "city" => $fields['request']['re_city_to'],
                        "street" => $fields['request']['re_street_to'],
                        "postal_code" => $fields['request']['re_zipcode_to']
                    ]
                ]
        ];
        dd( json_encode($request));


        /*$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fields['request_delivery_value']);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request));
        curl_setopt($ch, CURLOPT_HTTPHEADER, ["Content-Type: application/json"]);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($ch);

        // Then, after your curl_exec call:
        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $header = substr($response, 0, $header_size);
        $body = substr($response, $header_size);

        curl_close($ch);

        System::sendMessage(3653, "Movinga result: header", System::serialize($header));
        System::sendMessage(3653, "Movinga result: body", System::serialize($body));
        */


    }

    public static function sendInternationalSeasAndAirShipping($language, $fields, $translations)
    {

        $request = [
            "postingtype" => "intlMv",
            "sId" => 133,

            "fromZip" => $fields['request']['re_zipcode_from'],
            "fromCity" => $fields['request']['re_city_from'],
            "fromCountry" => $fields['request']['re_co_code_from'],

            "toZip" => $fields['request']['re_zipcode_to'],
            "toCity" => $fields['request']['re_city_to'],
            "toCountry" => $fields['request']['re_co_code_to'],

            "moveDate" => date("m/d/Y", strtotime($fields['request']['re_moving_date'])),
            "moveSize" => $translations["moving_size_val_".$fields['request']['re_moving_size']],

            "storage" => $translations["storage_val_".$fields['request']['re_storage']],
            "business" => $translations[(($fields['request']['re_business'] == 1) ? "yes" : "no")],
            "companyName" => $fields['request']['re_company_name'],

            "customerName" => $fields['request']['re_full_name'],
            "email" => $fields['request']['re_email'],
            "phone1" => $fields['request']['re_telephone1'],
            "phone2" => $fields['request']['re_telephone2'],

            "comments" => str_replace(["\r\n", "\r", "\n"], " ", $fields['request']['re_remarks']),
        ];

        dd($request);

       /* $curl = curl_init();
        curl_setopt_array($curl, array(

            CURLOPT_URL => $fields['request_delivery_value'].http_build_query($request),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache"
            ),
        ));
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Length: 0'));


        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);*/



    }

    public static function sendSalesforceGinter($language, $fields, $translations)
    {
        $service = "";

        if($fields['request']['re_request_type'] == 1)
        {
            if($fields['request']['re_destination_type'] == 1)
            {
                $service = "Household Goods - International";
            }
            elseif($fields['re_destination_type'] == 2)
            {
                $service = "Household Goods - Domestic";
            }
        }
        elseif($fields['request']['re_request_type'] == 2)
        {
            $service = "Excess Baggage";
        }
        elseif($fields['request']['re_request_type'] == 3)
        {
            if($fields['request']['re_moving_size'] == 5)
            {
                $service = "Pet Relocation";
            }
            elseif($fields['request']['re_moving_size'] == 6)
            {
                $service = "Car Shipping";
            }
            elseif($fields['request']['re_moving_size'] == 7)
            {
                $service = "Shipping";
            }
        }
        elseif($fields['request']['re_request_type'] == 4)
        {
            $service = "Office Moving";
        }

        $request = [
            "oid" => $fields['request_delivery_value'],
            "retURL" => "http://",
            "recordType" => "01220000000K3SC",
            "lead_source" => System::getSetting("mail_default_from_name"),
            "first_name" => "",
            "last_name" => $fields['request']['re_full_name'],
            "email" => $fields['request']['re_email'],
            "company" => $fields['request']['re_company_name'],
            "Volume_m__c" => $fields['request']['re_volume_m3'],
            "Cidade_de_Destino__c" => $fields['request']['re_city_to'],
            "Cidade_de_Origem__c" => $fields['request']['re_city_from'],
            "Address" => $fields['request']['re_street_from'],
            "CEP__c" => $fields['request']['re_zipcode_from'],
            "paisDeDestino__c" => $fields['request']['re_co_code_to'],
            "paisDeOrigem__c" => $fields['request']['re_co_code_from'],
            "00N20000003ZWmq" => $service,
            "MobilePhone" => $fields['request']['re_telephone1'],
            "Phone" => $fields['request']['re_telephone2'],
            "Description" => $fields['request']['re_remarks'],
            "Tipo_de_mudan_a__c" => $service
        ];

        dd($request);

        /*$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($request));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);

        if(!empty($result))
        {
            System::sendMessage([3653, 4186], "RequestDelivery Result (Salesforce Ginter)", $result);
        }*/


    }

    public static function sendSalesforceUniversal($language, $fields, $translations)
    {
        $service = "";

        if($fields['request']['re_request_type'] == 1)
        {
            if($fields['request']['re_destination_type'] == 1)
            {
                $service = "Household Goods - International";
            }
            elseif($fields['re_destination_type'] == 2)
            {
                $service = "Household Goods - Domestic";
            }
        }
        elseif($fields['request']['re_request_type'] == 2)
        {
            $service = "Excess Baggage";
        }
        elseif($fields['request']['re_request_type'] == 3)
        {
            if($fields['request']['re_moving_size'] == 5)
            {
                $service = "Pet Relocation";
            }
            elseif($fields['request']['re_moving_size'] == 6)
            {
                $service = "Car Shipping";
            }
            elseif($fields['request']['re_moving_size'] == 7)
            {
                $service = "Shipping";
            }
        }
        elseif($fields['request']['re_request_type'] == 4)
        {
            $service = "Office Moving";
        }

        $request = [
            "oid" => $fields['request_delivery_value'],
            "retURL" => "http://",
            "recordType" => "01220000000K3SC",
            "lead_source" => System::getSetting("mail_default_from_name"),
            "LeadSource" => System::getSetting("mail_default_from_name"),
            "first_name" => "",
            "last_name" => $fields['request']['re_full_name'],
            "email" => $fields['request']['re_email'],
            "company" => $fields['request']['re_company_name'],
            "Volume_m__c" => $fields['request']['re_volume_m3'],
            "Origin_Street__c" => $fields['request']['re_street_from'],
            "Origin_Zip_code__c" => $fields['request']['re_zipcode_from'],
            "Origin_City__c" => $fields['request']['re_city_from'],
            "Origin_Country__c" => $fields['request']['re_co_code_from'],
            "Destination_City__c" => $fields['request']['re_city_to'],
            "Destination_Zipcode__c" => $fields['request']['re_zipcode_to'],
            "Destination_Country__c" => $fields['request']['re_co_code_to'],
            "00N20000003ZWmq" => $service,
            "MobilePhone" => $fields['request']['re_telephone2'],
            "phone" => $fields['request']['re_telephone1'],
            "Description" => $fields['request']['re_remarks'],
            "Move_Size__c" => $service,
            "Moving_Date__c" => $fields['request']['re_moving_date']
        ];

        dd($request);

        /*$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($request));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);


        if(!empty($result))
        {
            System::sendMessage([3653, 4186], "RequestDelivery Result (Salesforce Ginter)", $result);
        }*/


    }

    public static function sendDemengo($language, $fields, $translations)
    {

        $split_name['first'] = explode(' ', trim($fields['request']['re_full_name']))[0];
        $split_name['family'] = substr(strstr($fields['request']['re_full_name'], " "), 1);

        $request = [
            "source" => System::getSetting("mail_default_from_name"),
            "nom_cli" => $split_name['family'],
            "prenom_cli" => $split_name['first'],
            "tel_cli" => $fields['request']['re_telephone1'],
            "email_cli" => $fields['request']['re_email'],
            "date_depart" => date("d/m/Y", strtotime($fields['request']['re_moving_date'])),
            "adresse_depart" => $fields['request']['re_street_from'],
            "cp_depart" => $fields['request']['re_zipcode_from'],
            "ville_depart" => $fields['request']['re_city_from'],
            "commentaire" => str_replace(["\r\n", "\r", "\n"], " ", $fields['request']['re_remarks']),
            "adresse_arrivee" => $fields['request']['re_street_to'],
            "cp_arrivee" => $fields['request']['re_zipcode_to'],
            "ville_arrivee" => $fields['request']['re_city_to'],
            "reference" => $fields['cu_re_id'],
            "volume_m3" => $fields['request']['re_volume_m3'],
        ];

        dd($request);

       /* $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fields['request_delivery_value']);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($request));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);*/




    }

    public static function sendSimpsons($language, $fields, $translations)
    {

        $split_name['first'] = explode(' ', trim($fields['request']['re_full_name']))[0];
        $split_name['family'] = substr(strstr($fields['request']['re_full_name'], " "), 1);

        if($fields['request']['re_co_code_from'] == "US"){
            $pickup_region = substr(DataIndex::getRegion($fields['request']['re_reg_id_from']), 0, 2);
        }else{
            $pickup_region = DataIndex::getRegion($fields['request']['re_reg_id_from']);
        }

        if($fields['request']['re_co_code_to'] == "US"){
            $delivery_region = substr(DataIndex::getRegion($fields['request']['re_reg_id_to']), 0, 2);
        }else{
            $delivery_region = DataIndex::getRegion($fields['request']['re_reg_id_to']);
        }

        $request = [
            "lead_source" => System::getSetting("mail_default_from_name"),
            "lead_id" => $fields['cu_re_id'],
            "request_type" => $translations["request_type_val_".$fields['request']['re_request_type']],
            "moving_size" => $translations["moving_size_val_".$fields['request']['re_moving_size']],
            "moving_date" => $fields['request']['re_moving_date'],
            "volume_m3" => $fields['request']['re_volume_m3'],
            "volume_ft3" => $fields['request']['re_volume_ft3'],
            "storage" => $translations["storage_val_".$fields['request']['re_storage']],
            "packing" => $translations["self_company_val_".$fields['request']['re_packing']],
            "assembly" => $translations["self_company_val_".$fields['request']['re_assembly']],
            "business" => $translations[(($fields['request']['re_business'] == 1) ? "yes" : "no")],
            "pickup_street" => $fields['request']['re_street_from'],
            "pickup_zipcode" => $fields['request']['re_zipcode_from'],
            "pickup_city" => $fields['request']['re_city_from'],
            "pickup_country" => $fields['request']['re_co_code_from'],
            "pickup_region" => $pickup_region,
            "pickup_residence" => $translations["residence_val_".$fields['request']['re_residence_from']],
            "delivery_street" => $fields['request']['re_street_to'],
            "delivery_zipcode" => $fields['request']['re_zipcode_to'],
            "delivery_city" => $fields['request']['re_city_to'],
            "delivery_country" => $fields['request']['re_co_code_to'],
            "delivery_region" => $delivery_region,
            "delivery_residence" => $translations["residence_val_".$fields['request']['re_residence_to']],
            "company_name" => $fields['request']['re_company_name'],
            "gender" => "",
            "full_name" => $fields['request']['re_full_name'],
            "first_name" => $split_name['first'],
            "family_name" => $split_name['family'],
            "telephone1" => $fields['request']['re_telephone1'],
            "telephone2" => $fields['request']['re_telephone2'],
            "email" => $fields['request']['re_email'],
            "remarks" => str_replace(["\r\n", "\r", "\n"], " ", $fields['request']['re_remarks']),
            "branch" => $fields['request_delivery_extra'],
            "status" => "N"
        ];

        dd($request);

        /*$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fields['request_delivery_value']);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($request));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);

        System::sendMessage(3653, "Simpsons Delivery", System::serialize($request));
        */
    }

    public static function sendMovewareOSS($language, $fields, $translations)
    {
        $xml = new DomDocumentExtended("1.0", "UTF-8");
        $xml->preserveWhiteSpace = false;
        $xml->formatOutput = true;

        $MWConnect = $xml->appendChild($xml->createElement("MWConnect"));
        $MWConnect->setAttribute("xmlns", "http://www.moveware.com.au/MovewareConnect.xsd");

        $Details = $MWConnect->appendChild($xml->createElement("Details"));
        $Details->appendChild($xml->createElementText("Date", date("Y-m-d H:i:s:").":000"));
        $Details->appendChild($xml->createElementText("Subject", "Quote Request"));
        $Details->appendChild($xml->createElementText("Comments", str_replace(["\r\n", "\r", "\n"], " ", $fields['request']['re_remarks'])));

        $Webquote = $MWConnect->appendChild($xml->createElement("MessageType"))->appendChild($xml->createElement("WebQuote"));
        $Webquote->appendChild($xml->createElementText("CompanyToMQID", "06000"));

        $Removal = $Webquote->appendChild($xml->createElement("Removal"));
        $Removal->appendChild($xml->createElementText("Referral", "overseas-moving.com"));
        $Removal->appendChild($xml->createElementText("Branch", $fields['request_delivery_extra']));
        $Removal->appendChild($xml->createElementText("Status", "E"));
        $Removal->appendChild($xml->createElementText("EstMoveDate", date("d/m/Y", strtotime($fields['request']['re_moving_date']))));
        $Removal->appendChild($xml->createElementText("CustCode", $fields['request_delivery_value']));
        $Removal->appendChild($xml->createElementText("Order", $fields['cu_re_id']));
        $Removal->appendChild($xml->createElementText("InternalNotes", str_replace(["\r\n", "\r", "\n"], " ", $fields['request']['re_remarks'])));
        $Dates = $Removal->appendChild($xml->createElement("Dates"));
        $Dates->appendChild($xml->createElementText("type", "Uplift"));
        $Dates->appendChild($xml->createElementText("Date", date("Y-m-d", strtotime($fields['request']['re_moving_date']))));
        $Removal->appendChild($xml->createElementText("ixType", "DD"));

        $Origin = $Removal->appendChild($xml->createElement("Origin"));

        $Name = $Origin->appendChild($xml->createElement("Name"));
        $Name->appendChild($xml->createElementText("Name", $fields['request']['re_full_name']));
        $Name->appendChild($xml->createElementText("Email", $fields['request']['re_email']));
        $Name->appendChild($xml->createElementText("Phone", $fields['request']['re_telephone1']));
        $Name->appendChild($xml->createElementText("Mobile", $fields['request']['re_telephone2']));

        $Address = $Origin->appendChild($xml->createElement("Address"));
        $Address->appendChild($xml->createElement("Address1", $fields['request']['re_street_from']));
        $Address->appendChild($xml->createElement("Suburb", $fields['request']['re_city_from']));
        $Address->appendChild($xml->createElement("State", DataIndex::getRegion($fields['request']['re_reg_id_from'])));

        $CountryCode = $Address->appendChild($xml->createElement("CountryCode"));
        $CountryCode->appendChild($xml->createElement("CountryDesc", $fields['request']['re_city_from']));
        $CountryCode->appendChild($xml->createElement("CountryState", DataIndex::getRegion($fields['request']['re_reg_id_from'])));
        $CountryCode->appendChild($xml->createElement("CountryCountry", $fields['request']['re_co_code_from']));

        $Address->appendChild($xml->createElement("Postcode", $fields['request']['re_zipcode_from']));
        $Address->appendChild($xml->createElement("Access"));
        $Address->appendChild($xml->createElement("Comments"));

        $Destination = $Removal->appendChild($xml->createElement("Destination"));

        $Name = $Destination->appendChild($xml->createElement("Name"));
        $Name->appendChild($xml->createElementText("Name", $fields['request']['re_full_name']));
        $Name->appendChild($xml->createElementText("Email", $fields['request']['re_email']));
        $Name->appendChild($xml->createElementText("Phone", $fields['request']['re_telephone1']));
        $Name->appendChild($xml->createElementText("Mobile", $fields['request']['re_telephone2']));

        $Address = $Destination->appendChild($xml->createElement("Address"));
        $Address->appendChild($xml->createElement("Address1", $fields['request']['re_street_to']));
        $Address->appendChild($xml->createElement("Suburb", $fields['request']['re_city_to']));
        $Address->appendChild($xml->createElement("State", DataIndex::getRegion($fields['request']['re_reg_id_to'])));

        $CountryCode = $Address->appendChild($xml->createElement("CountryCode"));
        $CountryCode->appendChild($xml->createElement("CountryDesc", $fields['request']['re_city_to']));
        $CountryCode->appendChild($xml->createElement("CountryState", DataIndex::getRegion($fields['request']['re_reg_id_to'])));
        $CountryCode->appendChild($xml->createElement("CountryCountry", $fields['request']['re_co_code_to']));

        $Address->appendChild($xml->createElement("Postcode", $fields['request']['re_zipcode_to']));
        $Address->appendChild($xml->createElement("Access"));
        $Address->appendChild($xml->createElement("Comments"));

        $Removal->appendChild($xml->createElement("SessionID", rand(111111111111111111, 999999999999999999)));

        $data = [
            "companyname" => System::getSetting("mail_default_from_name"),
            "companyserial" => "76VK-9ZV6-JSB5",
            "procedure" => "setxml",
            "optionalval" => $xml->saveXML()
        ];

        dd($data);


        /*$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://www.moveconnect.com/webtransfer.aspx");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);*/


    }

    public static function sendPARC($language, $fields, $translations)
    {

        $split_name['first'] = explode(' ', trim($fields['request']['re_full_name']))[0];
        $split_name['family'] = substr(strstr($fields['request']['re_full_name'], " "), 1);

        // define data
        $data = array(
            'bourseId' => "TRIGLOBAL",
            'parcId' => "PARC".$fields['request_delivery_value'],
            'dataLead' => array(
                'civility' => 'MR',
                'name' => ((!empty($split_name['family'])) ? $split_name['family'] : $split_name['first']),
                'firstname' => $split_name['first'],
                'phone' => $fields['request']['re_telephone1'],
                'mobilePhone' => $fields['request']['re_telephone2'],
                'mail' => $fields['request']['re_email'],
                'requestDate' => date("Y-m-d", strtotime($fields['request']['re_timestamp'])),
                'requestType' => '1',
                'society' => '',
                'loadingDateStart' => $fields['request']['re_moving_date'],
                'loadingDateEnd' => $fields['request']['re_moving_date'],
                'loadingAddress' => $fields['request']['re_street_from'],
                'loadingZipCode' => $fields['request']['re_zipcode_from'],
                'loadingCity' => $fields['request']['re_city_from'],
                'loadingCountry' => $fields['request']['re_co_code_from'],
                'loadingInfo' => '',
                'loadingElevator' => '0',
                'loadingFloor' => '0',
                'deliveryDateStart' => $fields['request']['re_moving_date'],
                'deliveryDateEnd' => $fields['request']['re_moving_date'],
                'deliveryAddress' => $fields['request']['re_street_to'],
                'deliveryZipCode' => ((!empty($fields['request']['re_zipcode_to'])) ? $fields['request']['re_zipcode_to'] : "-"),
                'deliveryCity' => $fields['request']['re_city_to'],
                'deliveryCountry' => $fields['request']['re_co_code_to'],
                'deliveryInfo' => '',
                'deliveryElevator' => '0',
                'deliveryFloor' => '0',
                'furnitureVolume' => $fields['request']['re_volume_m3'],
                'furnitureList' => array(),
                'service' => '4',
                'comments' => $fields['request']['re_remarks'],
                'webformSID' => '',
                'distributionID' => ''
            )
        );

        dd($data);

       /* // declare saop client
        $url = "https://www.logiciel-demenagement-parc.com/import/wsParcImportLead.php?wsdl";
        $client = new SoapClient($url, array('cache_wsdl' => WSDL_CACHE_NONE));
        // Call wsdl function importLead
        $result = $client->__soapCall("importLead", $data);

        System::sendMessage(4186, "PARC DATA", System::serialize($data));
        System::sendMessage(4186, "PARC", System::serialize($result));*/

    }

    public static function sendP4P($language, $fields, $translations)
    {

        $split_name['first'] = explode(' ', trim($fields['request']['re_full_name']))[0];
        $split_name['family'] = substr(strstr($fields['request']['re_full_name'], " "), 1);

        $datetime = new DateTime($fields['request']['re_moving_date']);

        //US postalcode FROM
        if($fields['request']['re_co_code_from'] == "US")
        {
            $lead_state_from = substr(DataIndex::getRegion($fields['request']['re_reg_id_from']), 0, 2);
        }

        //US postalcode TO
        if($fields['request']['re_co_code_to'] == "US")
        {
            $lead_state_to = substr(DataIndex::getRegion($fields['request']['re_reg_id_to']), 0, 2);
        }

        //CA postalcode FROM
        if($fields['request']['re_co_code_from'] == "CA")
        {
            $lead_state_from = self::getPostalCodeCanada($fields['request']['re_reg_id_from']);
        }

        //CA postalcode TO
        if($fields['request']['re_co_code_to'] == "CA")
        {
            $lead_state_to = self::getPostalCodeCanada($fields['request']['re_reg_id_to']);
        }

        //US Phone number code
        $telephonenumber = $fields['request']['re_telephone1'];
        $telephonenumber = str_replace('-', '', $telephonenumber);
        $telephonenumber = str_replace('(', '', $telephonenumber);
        $telephonenumber = str_replace(')', '', $telephonenumber);
        $telephonenumber = str_replace(' ', '', $telephonenumber);
        $telephonenumber = trim($telephonenumber);
        $telephonenumber = stripslashes($telephonenumber);

        if(strlen($telephonenumber) == 10){
            $tel_1 = substr($telephonenumber, 0, 3);
            $tel_2 = substr($telephonenumber, 3, 3);
            $tel_3 = substr($telephonenumber, 6, 4);
        }elseif (strlen($telephonenumber) == 11){
            $tel_1 = substr($telephonenumber, 1, 3);
            $tel_2 = substr($telephonenumber, 4, 3);
            $tel_3 = substr($telephonenumber, 7, 4);
            $tel_int = substr($telephonenumber, 0, 1);
        }elseif (strlen($telephonenumber) == 12){
            $tel_1 = substr($telephonenumber, 2, 3);
            $tel_2 = substr($telephonenumber, 5, 3);
            $tel_3 = substr($telephonenumber, 8, 4);
            $tel_int = substr($telephonenumber, 0, 2);
        }elseif (strlen($telephonenumber) == 13){
            $tel_1 = substr($telephonenumber, 3, 3);
            $tel_2 = substr($telephonenumber, 6, 3);
            $tel_3 = substr($telephonenumber, 9, 4);
            $tel_int = substr($telephonenumber, 0, 3);
        }elseif (strlen($telephonenumber) == 14){
            $tel_1 = substr($telephonenumber, 3, 3);
            $tel_2 = substr($telephonenumber, 6, 3);
            $tel_3 = substr($telephonenumber, 9, 5);
            $tel_int = substr($telephonenumber, 0, 3);
        }


        $request = [

            'provider_id' => $fields['request_delivery_value'],
            'mover_id' => "1195_TriG",
            'lead_provider_ref' => $fields['cu_re_id'],

            "move_date_year" => $datetime->format('Y'),
            "move_date_month" => $datetime->format('m'),
            "move_date_day" => $datetime->format('d'),

            "lead_name_first" => $split_name['first'],
            "lead_name_last" => $split_name['family'],
            "lead_email" => $fields['request']['re_email'],

            "lead_phone1" => $tel_1,
            "lead_phone2" => $tel_2,
            "lead_phone3" =>  $tel_3,
            "lead_phone_intl" => $tel_int,

            "lead_from_add1" => $fields['request']['re_street_from'],
            "lead_from_city" => $fields['request']['re_city_from'],
            "lead_from_postal" => $fields['request']['re_zipcode_from'],
            "lead_from_state" => $lead_state_from,
            "lead_from_country" => $fields['request']['re_co_code_from'],

            "lead_to_add1" => $fields['request']['re_street_to'],
            "lead_to_city" => $fields['request']['re_city_to'],
            "lead_to_postal" => $fields['request']['re_zipcode_to'],
            "lead_to_state" => $lead_state_to,
            "lead_to_country" => $fields['request']['re_co_code_to'],

            "lead_lbs" => 7777,
            "lead_notes" => str_replace(["\r\n", "\r", "\n"], " ", $fields['request']['re_remarks'])
        ];

        dd($request);

      /*  $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://www.p4pmarketing.com/lead_provider/new_lead.php");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($request));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        $info = curl_getinfo($ch);

        curl_close($ch);

        // This should be a valid Lead ID
        if (!preg_match("/^\d+$/", $result)) {
            System::sendMessage(3653, "P4P Delivery failed", System::serialize($result));
        }*/



    }

    public static function sendScout24($language, $fields, $translations)
    {

        $popular_german_streets =
            [
                "strasse 12",
                "Hauptstrasse 27",
                "Bahnhofstrasse 14",
                "Schulstrasse 8",
                "Kirchenstrasse 2",
                "Schnellstrasse 7",
                "Schillerstrasse 16",
                "Goethestrasse 22",
                "Friedhofstrasse 4",
                "Beethovenstrasse 9",
                "Dorfstrasse 5",
                "Gartenstrasse 1"
            ];

        $google_change = false;

        $zipcode_from = self::checkForGermanZipcode("from", $fields, $popular_german_streets, $google_change);
        $zipcode_to = self::checkForGermanZipcode("to", $fields, $popular_german_streets, $google_change);

        if($zipcode_from == null || $zipcode_to == null){
            //System::sendMessage("3653", "Was not able to find a zipcode for lead ".$fields['request']['re_id'].". Zipcode from: ".$zipcode_from.". Zipcode to: ".$zipcode_to, System::serialize($fields));
            exit;
        }

        $split_name['first'] = explode(' ', trim($fields['request']['re_full_name']))[0];
        $split_name['family'] = substr(strstr($fields['request']['re_full_name'], " "), 1);

        if( $fields['request']['re_volume_m3'] == 0){
            //If complete household
            if($fields['request']['re_moving_size'] == 1){
                $fields['request']['re_volume_m3'] = 15;
            }
            //Part of household
            elseif($fields['request']['re_moving_size'] == 2){
                $fields['request']['re_volume_m3'] = 8;
            }
            //Small move
            elseif($fields['request']['re_moving_size'] == 3){
                $fields['request']['re_volume_m3'] = 4;
            }
        }

        //Add vital information like a fake zipcode and the volume to the request
        $pretext = "";

        if($google_change){
            $pretext .= "Die Straßenangaben der Wohnorte müssen ggf. mit dem Antragssteller geprüft werden. ";
        }

        if($fields['request']['re_volume_m3']) {
            $pretext .= "Volume: ".$fields['request']['re_volume_m3']."(m3). ";
        }

        $request = [
            "type" => "RELOCATION",
            "aff" => $fields['request_delivery_value'],
            "consumer" =>
                [
                    "firstName" => $split_name['first'],
                    "lastName" => (empty($split_name['family']) ? $split_name['first'] : $split_name['family']),
                    "salutation" => "MR",
                    "telephone" => $fields['request']['re_telephone1'],
                    "email" => $fields['request']['re_email'],
                ],
            "additionalInformation" => $pretext.(!empty($fields['request']['re_remarks']) ? "Remark: ".str_replace(["\r\n", "\r", "\n"], " ", $fields['request']['re_remarks']) : ""),
            "payment" => "PRIVATE",

            "from" => [
                "street" => $fields['request']['re_street_from'],
                "zipcode" => $zipcode_from,
                "city" => $fields['request']['re_city_from'],
                "country" => $fields['request']['re_co_code_from'],
                "date" =>date('Y-m-d', strtotime($fields['request']['re_moving_date'])),
                'dateType' => "START",
                'area' => (($fields['request']['re_volume_m3'] * 3) < 10 ? 10 : round($fields['request']['re_volume_m3'] * 3)),

            ],
            "to" => [
                "street" => $fields['request']['re_street_to'],
                "zipcode" => $zipcode_to,
                "city" => $fields['request']['re_city_to'],
                "country" => $fields['request']['re_co_code_to'],
                "date" =>date('Y-m-d', strtotime($fields['request']['re_moving_date'])),
                'dateType' => "START"
            ]


        ];

        if($fields['request']['re_packing']){
            $request["features"][] = "PACK";
            $request["features"][] = "UNPACK";
        }

        if($fields['request']['re_assembly']){
            $request["features"][] = "FURNITURE_ASSEMBLY";
            $request["features"][] = "FURNITURE_DISASSEMBLY";
        }

        dd($request);


        /*$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.relocation.immobilienscout24.de/relocationrequest");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request));
        curl_setopt($ch, CURLOPT_HTTPHEADER, ["Content-Type: application/json"]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        $info = curl_getinfo($ch);

        curl_close($ch);

        System::sendMessage("3653", "Sent scout24 lead: ".$fields['request']['re_id'], System::serialize($result));
        System::sendMessage("3653", "scout24 Post Values: ".$fields['request']['re_id'], System::serialize($request));

        $scout = (array)json_decode($result);

        if(!array_key_exists("id", $scout))
        {
            System::sendMessage("4203", "Scout response to lead: ".$fields['request']['re_id']." (paste in unserialize.com)", System::serialize($result));
        }*/




    }

    public static function sendNCV($language, $fields, $translations)
    {

        $split_name['first'] = explode(' ', trim($fields['request']['re_full_name']))[0];
        $split_name['family'] = substr(strstr($fields['request']['re_full_name'], " "), 1);

        $moving_size = 0;

        if ($fields['request']['re_moving_size'] == 3) {
            $moving_size = 1;
        }
        elseif ($fields['request']['re_moving_size'] == 2) {
            $moving_size = 2;
        }
        elseif ($fields['request']['re_moving_size'] == 1) {
            $moving_size = 3;
        }

        $request = [
            "source_id" => 28039,
            "first_name" => $split_name['first'],
            "last_name" => $split_name['family'],
            "email" => $fields['request']['re_email'],
            "phone1" => $fields['request']['re_telephone1'],
            "ipaddr" => $fields['request']['re_ip_address'],
            "captured" => $fields['request']['re_timestamp'],
            "zip1" => $fields['request']['re_zipcode_from'],
            "city1" => $fields['request']['re_city_from'],
            "country1" => $fields['request']['re_co_code_from'],
            "zip2" => $fields['request']['re_zipcode_to'],
            "city2" => $fields['request']['re_city_to'],
            "country2" => $fields['request']['re_co_code_to'],
            "move_date" => $fields['request']['re_moving_date'],
            //"move_size" => System::getTranslation("moving_size_val_".$fields['request']['re_moving_size'], "EN"),
            "move_size" => $moving_size,
            "lead_source" => str_replace("www.", "", System::getSetting("company_website")),
            "test_lead" => 0
        ];

        dd($request);

        /*$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fields['request_delivery_value']);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($request));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $error = curl_error($ch);

        System::sendMessage(4186, "NCV", System::serialize($result));
        curl_close($ch);


        if(!(strpos($result, 'SUCCESS') !== false))
        {
            System::sendMessage([3653, 4186], "RequestDelivery Result (NCV)", System::serialize($result));
            System::sendMessage([3653, 4186], "RequestDelivery Error (NCV)", System::serialize($error));
        }*/



    }

    public static function sendDesBrasEnPlus($language, $fields, $translations)
    {

        $split_name['first'] = explode(' ', trim($fields['request']['re_full_name']))[0];
        $split_name['family'] = substr(strstr($fields['request']['re_full_name'], " "), 1);

        if(empty($split_name['family'])){
            $split_name['family'] = $split_name['first'];
        }

        $fields['request']['re_telephone1'] = str_replace( "+3", "", $fields['request']['re_telephone1']);
        $fields['request']['re_telephone1'] = str_replace( " ", "", $fields['request']['re_telephone1']);

        if(strlen($fields['request']['re_telephone1']) === 9){
            $fields['request']['re_telephone1'] = "0".$fields['request']['re_telephone1'];
        }

        $request = [
            "date_dem" => date("d/m/Y", strtotime($fields['request']['re_moving_date'])),
            "token" => $fields['request_delivery_value'],
            "nom" => $split_name['family'],
            "prenom" => $split_name['first'],
            "email" => $fields['request']['re_email'],
            "telephone" => $fields['request']['re_telephone1'],
            "surface" => $fields['request']['re_volume_m3'],
            "code_postal_depart" => $fields['request']['re_zipcode_from'],
            "code_postal_arrivee" => $fields['request']['re_zipcode_to'],
        ];
        dd($request);

       /* $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.leads.desbrasenplus.com/add-leads.php");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($request));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);

        $result = json_decode($result, true);

        System::sendMessage(3653, "Des Bras En Plus delivery: result", System::serialize($result));
        System::sendMessage(3653, "Des Bras En Plus delivery: request", System::serialize($request));
    */

    }




    //Delivery Helper functions
    public static function getGermanZipcode($google_search)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($google_search) . "&country:DE&result_type=postal_code&key=AIzaSyBqW3eoOlVa5S8CIPMb4wO48vnNWExrYCs");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);

        $google_result = json_decode($result, true);

        $last = array_key_last($google_result["results"][0]['address_components']);

        if(in_array("postal_code", $google_result["results"][0]['address_components'][$last]["types"])) {
            return $google_result["results"][0]['address_components'][$last]['short_name'];

        }else{
            return null;
        }
    }

    public static function checkForGermanZipcode($direction, &$fields, $popular_german_streets, &$google_change)
    {
        //If zipcode not exactly five numbers in zipcode (from or to)
        if (!preg_match('/^\d{5}$/', $fields['request']['re_zipcode_'.$direction])) {

            //First check if the JSON-file has the anwser
            $json_result = self::checkJSONFile($direction, $fields);
            if($json_result){
                if($direction == "from" && empty($fields['request']['re_street_from'])){
                    $fields['request']['re_street_from'] = $popular_german_streets[0];
                }
                return $json_result;
            }

            //We are now counting on Google for the zipcode/street
            $google_change = true;
            //Add street_from to possible streetnames
            if (!empty($fields['request']['re_street_'.$direction])) {
                array_unshift($popular_german_streets, $fields['request']['re_street_'.$direction]);

            }

            foreach ($popular_german_streets as $german_street) {
                $google_search = $german_street . " " . $fields['request']['re_city_'.$direction] . " " . $fields['request']['re_co_code_'.$direction];
                $result = self::getGermanZipcode($google_search);
                if (!empty($result)) {
                    if($direction == "from") {
                        $fields['request']['re_street_from'] = $german_street;
                    }
                    return $result;
                }
            }

            return null;


        }else{
            return $fields['request']['re_zipcode_'.$direction];
        }

    }

    public static function checkJSONFile($direction, $fields){
        $jsonfile = file_get_contents(env("SHARED_FOLDER")."zipcodes.de.json");
        $json = json_decode($jsonfile);

        foreach($json as $line){
            if(strtolower($line->place) == strtolower($fields['request']['re_city_'.$direction])){
                return $line->zipcode;
            }
        }


    }

    public static function getPostalCodeCanada($region_id)
    {
        switch($region_id)
        {
            case 3411:
                return "AB";
            case 3412:
                return "BC";
            case 3413:
                return "MB";
            case 3414:
                return "NB";
            case 3415:
                return "NL";
            case 3416:
                return "NT";
            case 3417:
                return "NS";
            case 3418:
                return "PE";
            case 3419:
                return "SK";
            case 3420:
                return "YT";
            case 3421:
                return "QC";
            case 3422:
                return "QC";
            case 3423:
                return "QC";
            case 3424:
                return "ON";
            case 3425:
                return "ON";
            case 3426:
                return "ON";
            case 3427:
                return "ON";
            case 3428:
                return "ON";
        }
    }

    public static function getPostalCodeAustralia($region_id)
    {
        switch($region_id)
        {
            case 3429:
                return "NSW";
            case 3430:
                return "NT";
            case 3431:
                return "QLD";
            case 3432:
                return "SA";
            case 3433:
                return "TAS";
            case 3434:
                return "VIC";
            case 3435:
                return "WA";
            case 3436:
                return "ACT";
        }
    }

}
