<?php
/**
 * Created by PhpStorm.
 * User: Lorenzo
 * Date: 11/02/2019
 * Time: 11:02
 */

namespace App\Functions;


use App\Data\ClaimReason;
use App\Data\CustomerPairStatus;
use App\Data\DestinationType;
use App\Data\PaymentMethod;
use App\Models\ApplicationUser;
use App\Models\ContactPerson;
use App\Models\Customer;
use App\Models\KTBankLineInvoiceCustomerLedgerAccount;
use App\Models\KTRequestCustomerPortal;
use App\Models\PauseDay;
use App\Models\PauseHistory;
use App\Models\User;
use DateTime;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Mover
{
    public function getRequests($wanted_fields, $cu_id, $days)
    {
        $query =
            DB::table('kt_request_customer_portal')
                ->select(DB::raw($wanted_fields))
                ->join("requests", "ktrecupo_re_id", "=", "re_id")
                ->join("portals", "ktrecupo_po_id", "=", "po_id")
                ->leftJoin("claims", "ktrecupo_id", "cl_ktrecupo_id")
                ->where('ktrecupo_cu_id', "=", $cu_id)
                ->whereRaw("ktrecupo_timestamp BETWEEN NOW() - INTERVAL " . $days . " DAY AND NOW()")
                ->orderByRaw("`ktrecupo_timestamp` DESC")
                ->get();


        /*$query_1 = TGB_QueryBuilder::selectRawStatements($wanted_fields)
            ->from("kt_request_customer_portal")
            ->joinTable("requests", "ktrecupo_re_id", "re_id")
            ->joinTable("portals", "ktrecupo_po_id", "po_id")
            ->whereEqual("ktrecupo_cu_id", self::getCustomerId($id))
            ->whereBetweenRaw("ktrecupo_timestamp", $between_start, $between_end)
            ->whereEqual("ktrecupo_read_extra_information", 0)
            ->whereEqual("re_extra_information_status", 1)
            //->orderBy("ktrecupo_timestamp", TGB_QueryBuilder::ORDER_MODE_DESC)
            ->orderByRaw("`re_extra_information_timestamp` DESC, `ktrecupo_timestamp` DESC")
            ->collectQuery();*/

        /*$query_2 = TGB_QueryBuilder::selectRawStatements($wanted_fields)
            ->from("kt_request_customer_portal")
            ->joinTable("requests", "ktrecupo_re_id", "re_id")
            ->joinTable("portals", "ktrecupo_po_id", "po_id")
            ->whereEqual("ktrecupo_cu_id", self::getCustomerId($id))
            ->whereBetweenRaw("ktrecupo_timestamp", $between_start, $between_end)
            //->orderBy("ktrecupo_timestamp", TGB_QueryBuilder::ORDER_MODE_DESC)
            ->orderByRaw("`ktrecupo_timestamp` DESC")
            ->collectQuery();*/

        //$query_merged = array_merge($query_1, $query_2);

        return $query;
    }

    public function calcReclamationRate($days = 90, $id = null)
    {
        //Find all requests for a customer in a certain time period
        $allRequests = self::getRequests("`ktrecupo_id`, `ktrecupo_free`, `ktrecupo_free_reason`, `cl_reason`", $id, $days);

        //Counters for the requests
        $requests = 0;
        $claimed_requests = 0;
        $pending_requests = 0;
        $denied_requests = 0;
        $special_agreement_claims = 0;

        //IDs that should be checked in the free table
        $cl_ids = [];
        //Loop through results
        foreach ($allRequests as $row)
        {
            //Increment the number of requests
            $requests++;

            //Check if free
            $cl_ids[] = $row->ktrecupo_id;

            if ($row->ktrecupo_free && $row->ktrecupo_free_reason != -1 && $row->ktrecupo_free_reason != ClaimReason::AGREEMENT_WITH_PARTNERDESK && $row->ktrecupo_free_reason != ClaimReason::COVID_19) // Special agreement and COVID-19 claims
            {
                $claimed_requests++;
            } elseif ($row->ktrecupo_free && ($row->ktrecupo_free_reason == ClaimReason::AGREEMENT_WITH_PARTNERDESK || $row->ktrecupo_free_reason == 9)) // Special agreements and COVID-19 claims
            {
                $special_agreement_claims++;
            }
        }

        //Find free requests
        if (!empty($cl_ids))
        {
            $free_requests =
                DB::table('claims')
                    ->select(DB::raw("COUNT(*) AS `requests`, cl_status, cl_reason"))
                    ->whereIn("cl_ktrecupo_id", $cl_ids)
                    ->groupBy("cl_status")
                    ->get();

            foreach ($free_requests as $row)
            {
                $found_requests = $row->requests;

                switch (intval($row->cl_status))
                {
                    case 0:
                        if ($row->cl_reason != ClaimReason::AGREEMENT_WITH_PARTNERDESK && $row->cl_reason != ClaimReason::COVID_19 && $row->cl_reason != ClaimReason::TRIGLOBALS_MISTAKE)
                        {
                            $pending_requests += $found_requests;
                        }
                        break;
                    case 2:
                        $denied_requests += $found_requests;
                        break;
                }

                /*if ($row->ktrecupo_free && $row->ktrecupo_free_reason == 0)
                {
                    $special_agreement_claims++;
                }*/
            }
        }

        //Calculate the rate
        $total_claimed = ($claimed_requests + $pending_requests);
        if ($total_claimed == 0)
        {
            $claimed_percentage = 0;
        } else
        {
            $one_percent = $requests / 100.0;
            $claimed_percentage = ($total_claimed) / $one_percent;
        }

        //Return the data
        return [
            "requests" => $requests,
            "claimed_requests" => $claimed_requests,
            "pending_requests" => $pending_requests,
            "denied_requests" => $denied_requests,
            "total_claimed_requests" => $total_claimed,
            "percentage" => $claimed_percentage,
            "special_agreement_claims" => $special_agreement_claims // AND COVID-19 CLAIMS
        ];
    }

    public function getAmountOfCredits($cu_id, $date = "NOW()")
    {
        $system = new System();

        if (!isset($cu_id))
        {
            return 0;
        }

        $credit_query = DB::table('customer_credit')
            ->select(DB::raw("SUM(cucr_amount_left) as amount"))
            ->where("cucr_cu_id", "=", $cu_id)
            ->whereRaw($date . " < `cucr_expired`")
            ->get();

        if (!empty($credit_query))
        {
            return ($credit_query[0]->amount ? $credit_query[0]->amount : 0);
        } else
        {
            return 0;
        }
    }

    public function getPortals($cu_id)
    {
        return DB::table('kt_customer_portal')
            ->select('*')
            ->join("portals", "ktcupo_po_id", "=", "po_id")
            ->where('ktcupo_cu_id', "=", $cu_id)
            ->where('ktcupo_status', "=", 1)
            ->orderBy("po_portal", 'asc')
            ->get();

    }

    public function customerOutstandingRequests($customer_id, $currency = null, $state_per = null)
    {
        $system = new System();

        //Get the count
        $requests = self::multipleCustomersOutstandingRequests([$customer_id], $state_per);
        if (!isset($requests[$customer_id]))
        {
            return 0;
        }
        $customer = $requests[$customer_id];

        //Convert it if needed
        if ($currency !== null && $customer['cu_pacu_code'] != $currency)
        {
            return $system->currencyConvert($customer['amount'], $customer['cu_pacu_code'], $currency);
        } else
        {
            return $customer['amount'];
        }
    }

    public function multipleCustomersOutstandingRequests($customer_ids, $state_per = null)
    {
        $system = new System();

        $customer_query = DB::table('customers')
            ->select("cu_id", "cu_pacu_code", "cu_payment_method", "cu_leac_number")
            ->whereIn("cu_id", $customer_ids)
            ->where('cu_deleted', "=", 0)
            ->get();

        $customer_query = $system->databaseToArray($customer_query);

        $customers = [];
        foreach ($customer_query as $row)
        {
            $row['amount'] = 0.0;
            $customers[$row['cu_id']] = $row;
        }
        if (empty($customers))
        {
            return [];
        }

        $builder = DB::table('kt_request_customer_portal')
            ->select("ktrecupo_cu_id", "ktrecupo_amount_netto", "po_pacu_code")
            ->leftJoin("portals", "po_id", "ktrecupo_po_id");

        if ($state_per !== null)
        {
            $builder->where("ktrecupo_timestamp", "<=", date("Y-m-d", strtotime($state_per)));
        }

        $builder = $builder
            ->whereIn("ktrecupo_cu_id", array_keys($customers))
            ->where("ktrecupo_free", 0)
            ->where("ktrecupo_invoiced", 0)
            ->get();

        $builder = $system->databaseToArray($builder);

        foreach ($builder as $ktrecupo_row)
        {
            $cu_row = &$customers[$ktrecupo_row['ktrecupo_cu_id']];

            $methods = [PaymentMethod::PREPAYMENT, PaymentMethod::PREPAYMENT_AUTO_CHARGE_CREDIT_CARD, PaymentMethod::PREPAYMENT_AUTO_DIRECT_DEBIT];

            //If VAT of 21%, add to the cost
            if (in_array($cu_row['cu_payment_method'], $methods) && $cu_row['cu_leac_number'] == "801000")
            {
                $amount = ($ktrecupo_row['ktrecupo_amount_netto'] * 1.21);
            } else
            {
                $amount = $ktrecupo_row['ktrecupo_amount_netto'];
            }

            $cu_row['amount'] -= $system->currencyConvert($amount, $ktrecupo_row['po_pacu_code'], $cu_row['cu_pacu_code']);

            unset($cu_row);
        }

        return $customers;
    }

    public static function customerBalance($customer_id, $currency = null, $state_per = null, $invoices = null)
    {
        $system = new System();

        //Get the balance
        $balances = self::customerBalances([$customer_id], $state_per, $invoices);

        if (!isset($balances[$customer_id]))
        {
            return 0;
        }
        $customer = $balances[$customer_id];

        //Convert it if needed
        if ($currency !== null && $customer['cu_pacu_code'] != $currency)
        {
            return $system->currencyConvert($customer['balance'], $customer['cu_pacu_code'], $currency);
        } else
        {
            return $customer['balance'];
        }
    }

    public static function customerBalances($customer_ids, $state_per = null, $invoices = null)
    {
        $system = new System();

        $customer_query = DB::table('customers')
            ->select("cu_id", "cu_pacu_code")
            ->whereIn("cu_id", $customer_ids)
            ->where('cu_deleted', "=", 0)
            ->get();

        $customer_query = $system->databaseToArray($customer_query);

        $customers = [];

        foreach ($customer_query as $row)
        {
            $row['balance'] = 0.0;
            $customers[$row['cu_id']] = $row;
        }

        if (empty($customers))
        {
            return [];
        }

        $bank_lines_query = DB::table('kt_bank_line_invoice_customer_ledger_account')
            ->select("ktbaliinculeac_cu_id", "ktbaliinculeac_amount")
            ->leftJoin("bank_lines", "ktbaliinculeac_bali_id", "bali_id")
            ->whereIn("ktbaliinculeac_cu_id", array_keys($customers));

        if ($state_per !== null)
        {
            $bank_lines_query->whereRaw("
			(
				(
					`ktbaliinculeac_bali_id` IS NOT NULL AND
					`bali_date` <= '" . date("Y-m-d", strtotime($state_per)) . "'
				)
				OR
				(
					`ktbaliinculeac_bali_id` = IS NULL AND
					`ktbaliinculeac_date` <= '" . date("Y-m-d", strtotime($state_per)) . "'
				)
			)");
        }

        if ($invoices !== null)
        {
            if ($invoices)
            {
                $bank_lines_query->whereRaw("ktbaliinculeac_in_id IS NOT NULL");
            } else
            {
                $bank_lines_query->whereRaw("ktbaliinculeac_in_id IS NULL");
            }
        }

        $bank_lines_query = $bank_lines_query->get();

        $bank_lines_query = $system->databaseToArray($bank_lines_query);

        foreach ($bank_lines_query as $ktbali_row)
        {
            $customers[$ktbali_row['ktbaliinculeac_cu_id']]['balance'] += $ktbali_row['ktbaliinculeac_amount'];
        }

        //Invoices
        if ($invoices === null || $invoices === true)
        {
            $invoice_query = DB::table('invoices')
                ->select("in_cu_id", "in_amount_netto_eur")
                ->whereIn("in_cu_id", array_keys($customers));

            if ($state_per != null)
            {
                $invoice_query->where("in_date", "<=", date("Y-m-d", strtotime($state_per)));
            }
            $invoice_query = $invoice_query->get();

            $invoice_query = $system->databaseToArray($invoice_query);

            foreach ($invoice_query as $invoice_row)
            {
                $customers[$invoice_row['in_cu_id']]['balance'] -= $invoice_row['in_amount_netto_eur'];
            }
        }

        //=> Convert the currency
        foreach ($customers as &$cu_row)
        {
            if ($cu_row['cu_pacu_code'] !== "EUR" && $cu_row['balance'] != 0)
            {
                $cu_row['balance'] = $system->currencyConvert($cu_row['balance'], "EUR", $cu_row['cu_pacu_code']);
            }

            unset($cu_row);
        }

        return $customers;
    }

    public static function customerBalancesNew($customer_id)
    {
        $system = new System();

        $customer_query = DB::table('customers')
            ->select("cu_id", "cu_pacu_code")
            ->where("cu_id", $customer_id)
            ->where('cu_deleted', "=", 0)
            ->get();

        $customer_query = $system->databaseToArray($customer_query);

        $customers = [];

        foreach ($customer_query as $row)
        {
            $row['balance'] = 0.0;
            $customers[$row['cu_id']] = $row;
        }

        if (empty($customers))
        {
            return 0;
        }

        $bank_lines_query = DB::table('kt_bank_line_invoice_customer_ledger_account')
            ->select("ktbaliinculeac_cu_id", "ktbaliinculeac_amount_FC")
            ->leftJoin("bank_lines", "ktbaliinculeac_bali_id", "bali_id")
            ->whereIn("ktbaliinculeac_cu_id", array_keys($customers))
            ->whereNull("ktbaliinculeac_in_id");

        $bank_lines_query = $bank_lines_query->get();

        $bank_lines_query = $system->databaseToArray($bank_lines_query);

        foreach ($bank_lines_query as $ktbali_row)
        {
            $customers[$ktbali_row['ktbaliinculeac_cu_id']]['balance'] += $ktbali_row['ktbaliinculeac_amount_FC'];
        }

        return $customers[$customer_id]['balance'];
    }

    public static function customerBalancesFC($customer_ids, $state_per = null, $invoices = null)
    {
        foreach ($customer_ids as $id)
        {
            //Get current balances of customer
            $prepaid = self::customerBalancesNew($id);

            $invoices = 0;

            //How many invoices open?
            foreach (self::getCustomerTotalBalances($id) as $data)
            {
                $fully_paid = $data['fully_paid'];
                if (!$fully_paid)
                {
                    $invoices -= ($data['in_amount_netto'] - $data['amount_paired_fc']);
                }
            }

            //Check how many outstanding leads (not paid yet)
            $requests = self::customerOutstandingRequests($id);

            //How many balance left to spend
            $balance_total = $prepaid + $invoices + $requests;

            $customers[$id] = $balance_total;
        }

        return $customers;
    }

    public function setCustomersOnCreditHoldBeforeMatch($all_customers, $request)
    {
        $system = new System();
        $dataindex = new DataIndex();
        $mail = new Mail();

        if(!count($all_customers)){return $all_customers;}

        foreach($all_customers as $index => $current_customer)
        {

            $customer = Customer::select(
                "cu_id",
                "cu_email",
                "cu_company_name_legal",
                "cu_company_name_business",
                "cu_pacu_code",
                "cu_credit_limit",
                "cu_last_payment_notification",
                "cu_enforce_credit_limit",
                "cu_payment_method",
                "cu_la_code",
                "cu_prepayment_ran_out_mail_timestamp",
                "cu_credit_limit_increase_timestamp",
                "ktcupo_id",
                "cu_debt_manager"
            )
                ->leftJoin("kt_customer_portal", "cu_id", "ktcupo_cu_id")
                ->where("cu_id", $current_customer->cu_id)
                ->where("ktcupo_id", $current_customer->ktcupo_id)
                ->where("cu_credit_hold", 0)
                ->where("cu_deleted", 0)
                ->where("ktcupo_status", 1)
                ->where(function ($query) {
                    $query->whereIn("cu_payment_method", [PaymentMethod::PREPAYMENT, PaymentMethod::PREPAYMENT_AUTO_CHARGE_CREDIT_CARD, PaymentMethod::PREPAYMENT_AUTO_DIRECT_DEBIT])
                        ->orWhere("cu_enforce_credit_limit", 1);
                })
                ->first();


            if(!$customer){
                continue;
            }else{
                $customer = $system->databaseToArray($customer);
            }

            $customer['balance'] = self::customerBalancesFC([$customer['cu_id']])[$customer['cu_id']];
            $customer['to_spend'] = round($customer['balance'] + $customer['cu_credit_limit'], 2);

            $query_payment_rate = DB::table("payment_rates")
                ->where("para_ktcupo_id", $customer['ktcupo_id'])
                ->whereRaw("`para_date` <= NOW()");

            //Get correct NAT payment type
            if ($request->re_destination_type == DestinationType::NATMOVING)
            {
                $query_payment_rate->where("para_nat_type", $request->re_nat_type);
            }

            $query_payment_rate = $query_payment_rate
                ->orderBy("para_date", 'desc')
                ->first();

            if (!empty($query_payment_rate))
            {
                //Check if enough 'balance' left for the lead
                $actual_payment_rate = $system->calculatePaymentRate($query_payment_rate);

                if($actual_payment_rate > $customer['to_spend']){

                    //If payment method is Prepayment (4), then send mail
                    if ($customer['cu_payment_method'] == PaymentMethod::PREPAYMENT && empty($customer['cu_credit_limit_increase_timestamp']))
                    {
                        $date_now = date("Y-m-d H:i:s");
                        $date_sent = $customer['cu_prepayment_ran_out_mail_timestamp'];

                        $date1 = new DateTime(date('Y-m-d', strtotime($date_sent)));
                        $date2 = new DateTime(date('Y-m-d', strtotime($date_now)));

                        //Calculate days difference
                        $days_difference = $date1->diff($date2)->days;

                        //if the mail has been sent 14 days ago (or more)
                        if ($days_difference >= 14 || $customer['cu_prepayment_ran_out_mail_timestamp'] == null)
                        {
                            $fields = [
                                "company_name" => $customer['cu_company_name_business'],
                                "email" => $customer['cu_email']
                            ];

                            //Send mail
                            $mail->send("prepayment_ran_out", $customer['cu_la_code'], $fields);


                            //Update sent mail timestamp
                            DB::table('customers')
                                ->where('cu_id', $customer['cu_id'])
                                ->update(
                                    [
                                        'cu_prepayment_ran_out_mail_timestamp' => date("Y-m-d H:i:s")
                                    ]
                                );

                            $system->sendMessage(3653, "Prepayment ran out (match code)", System::serialize($fields));
                        }

                    }

                    DB::table('customers')
                        ->where('cu_id', $customer['cu_id'])
                        ->update(
                            [
                                'cu_credit_hold' => 1,
                                'cu_credit_hold_timestamp' => date("Y-m-d H:i:s")
                            ]
                        );

                    $subject = (($customer['cu_enforce_credit_limit'] == 1) ? "Enforced" : "Prepayment") . " credit limit has been reached";
                    $message = "Customer <strong>" . $customer['cu_company_name_legal'] . "</strong> has been changed to credit hold, because the credit limit <strong>(" . $system->paymentCurrencyToken($customer['cu_pacu_code']) . " " . $system->numberFormat($customer['cu_credit_limit'], 2) . ")</strong> has been reached. The current balance (incl. not invoiced) is <strong>" . $system->paymentCurrencyToken($customer['cu_pacu_code']) . " " . $system->numberFormat($customer['balance'], 2) . "</strong>.";

                    $finance_users = array_keys($dataindex->financeUsers());

                    if (!empty($customer['cu_debt_manager'])) {
                        //Add debt manager to $finance_users array
                        $finance_users[] = $customer['cu_debt_manager'];
                    }

                    $system->sendMessage($finance_users, $subject, $message);

                    $all_customers->forget($index);
                }else{
                    continue;
                }
            }
        }

        return $all_customers;
    }

    public function setCustomersOnCreditHoldAfterMatch($cu_id)
    {
        $system = new System();
        $dataindex = new DataIndex();
        $mail = new Mail();

        $customer = Customer::select(
                "cu_id",
                "cu_email",
                "cu_company_name_legal",
                "cu_company_name_business",
                "cu_pacu_code",
                "cu_credit_limit",
                "cu_last_payment_notification",
                "cu_enforce_credit_limit",
                "cu_payment_method",
                "cu_la_code",
                "cu_prepayment_ran_out_mail_timestamp",
                "cu_credit_limit_increase_timestamp"
            )
            ->leftJoin("kt_customer_portal", "cu_id", "ktcupo_cu_id")
            ->where("cu_id", $cu_id)
            ->where("cu_credit_hold", 0)
            ->where("cu_deleted", 0)
            ->where("ktcupo_status", 1)
            ->where(function ($query) {
                $query->whereIn("cu_payment_method", [PaymentMethod::PREPAYMENT, PaymentMethod::PREPAYMENT_AUTO_CHARGE_CREDIT_CARD, PaymentMethod::PREPAYMENT_AUTO_DIRECT_DEBIT])
                    ->orWhere("cu_enforce_credit_limit", 1);
            })
            ->first();

        if (!$customer)
        {
            return;
        }


        $customer = $system->databaseToArray($customer);

        $customer['avg_price'] = 0;
        $customer['per_day'] = 0;
        $customer['days_left'] = 0;

        $finance_users = array_keys($dataindex->financeUsers());
        $customer['balance'] = self::customerBalancesFC([$cu_id])[$cu_id];

        $leads = KTRequestCustomerPortal::selectRaw("ktrecupo_cu_id, COUNT(*) AS `requests`, SUM(`ktrecupo_amount_netto`) AS `amount`")
            ->where("ktrecupo_type", 1)
            ->where("ktrecupo_cu_id", $cu_id)
            ->whereRaw("`ktrecupo_timestamp` BETWEEN NOW() - INTERVAL 7 DAY AND NOW()")
            ->groupBy("ktrecupo_cu_id")
            ->first();

        $leads = $system->databaseToArray($leads);



        foreach ($leads as $lead)
        {
            $customer['avg_price'] = round($lead['amount'] / $lead['requests'], 2);
            $customer['per_day'] = round($lead['amount'] / 7, 2);
        }

        $customer['to_spend'] = round($customer['balance'] + $customer['cu_credit_limit'], 2);

        if ($customer['per_day'] > 0)
        {
            $customer['days_left'] = round($customer['to_spend'] / $customer['per_day']);
        }

        if ($customer['to_spend'] < $customer['avg_price'])
        {

            //If payment method is Prepayment (4), then send mail
            if ($customer['cu_payment_method'] == PaymentMethod::PREPAYMENT && empty($customer['cu_credit_limit_increase_timestamp']))
            {

                $date_now = date("Y-m-d H:i:s");
                $date_sent = $customer['cu_prepayment_ran_out_mail_timestamp'];

                $date1 = new DateTime(date('Y-m-d', strtotime($date_sent)));
                $date2 = new DateTime(date('Y-m-d', strtotime($date_now)));

                //Calculate days difference
                $days_difference = $date1->diff($date2)->days;

                //if the mail has been sent 14 days ago (or more)
                if ($days_difference >= 14 || $customer['cu_prepayment_ran_out_mail_timestamp'] == null)
                {
                    $fields = [
                        "company_name" => $customer['cu_company_name_business'],
                        "email" => $customer['cu_email']
                    ];

                    //Send mail
                    $mail->send("prepayment_ran_out", $customer['cu_la_code'], $fields);


                    //Update sent mail timestamp
                    DB::table('customers')
                        ->where('cu_id', $cu_id)
                        ->update(
                            [
                                'cu_prepayment_ran_out_mail_timestamp' => date("Y-m-d H:i:s")
                            ]
                        );

                    $system->sendMessage(3653, "Prepayment ran out (match code)", System::serialize($fields));
                }

            }

            //Set customer credit hold
            $get_customer = Customer::where("cu_id", $cu_id)->first();
            $get_customer->cu_credit_hold = 1;
            $get_customer->cu_credit_hold_timestamp = date("Y-m-d H:i:s");
            $get_customer->save();

            $subject = (($customer['cu_enforce_credit_limit'] == 1) ? "Enforced" : "Prepayment") . " credit limit has been reached";
            $message = "Customer <strong>" . $customer['cu_company_name_legal'] . "</strong> has been changed to credit hold, because the credit limit <strong>(" . $system->paymentCurrencyToken($customer['cu_pacu_code']) . " " . $system->numberFormat($customer['cu_credit_limit'], 2) . ")</strong> has been reached. The current balance (incl. not invoiced) is <strong>" . $system->paymentCurrencyToken($customer['cu_pacu_code']) . " " . $system->numberFormat($customer['balance'], 2) . "</strong>.";

            $system->sendMessage($finance_users, $subject, $message);

        }
    }

    public function getCustomerTotalBalancesNew($customer_id)
    {
        $system = new System();

        $invoices = DB::table('invoices')
            ->leftJoin("customers", "in_cu_id", "cu_id")
            ->where("in_cu_id", $customer_id)
            ->where('cu_deleted', "=", 0)
            ->orderBy("in_timestamp", 'desc')
            ->get();

        $invoices = $system->databaseToArray($invoices);

        //Gather data in a single array
        $data = [];
        $invoice_ids = [];
        foreach ($invoices as $row)
        {
            $id = $row['in_id'];
            $invoice_ids[] = $id;

            $row['amount_paired'] = 0;
            $row['amount_paired_fc'] = 0;

            if (number_format($row['in_amount_netto_eur'], 2) == number_format($row['in_amount_netto_eur_paid'], 2))
            {

                $row['fully_paid'] = 1;
            } else
            {
                $row['fully_paid'] = 0;
            }

            $data[$id] = $row;

        }

        //Get the actual payed amounts
        if (!empty($invoice_ids))
        {
            $ledger_accounts = DB::table('kt_bank_line_invoice_customer_ledger_account')
                ->select("ktbaliinculeac_in_id", "ktbaliinculeac_amount", "ktbaliinculeac_amount_FC")
                ->whereIn('ktbaliinculeac_in_id', $invoice_ids)
                ->get();

            foreach ($ledger_accounts as $row)
            {
                $invoice_id = $row->ktbaliinculeac_in_id;
                $invoice = &$data[$invoice_id];

                if (!$data[$invoice_id]['fully_paid'])
                {
                    $invoice['amount_paired'] += $row->ktbaliinculeac_amount;
                    $invoice['amount_paired_fc'] += $row->ktbaliinculeac_amount_FC;
                }
            }
        }

        //Calculate vars
        foreach ($data as $id => &$invoice)
        {
            $days_overdue = $system->dateDifference($invoice['in_expiration_date'], date("Y-m-d"));

            if ($invoice['in_payment_method'] == 3)
            {
                $days_overdue -= 14;
            }

            $invoice['days_overdue'] = ($invoice['fully_paid'] || $days_overdue < 0 ? 0 : $days_overdue);
        }

        return $data;
    }

    public function getCustomerTotalBalances($customer_id)
    {
        $system = new System();

        $invoices = DB::table('invoices')
            ->leftJoin("customers", "in_cu_id", "cu_id")
            ->where("in_cu_id", $customer_id)
            ->where('cu_deleted', "=", 0)
            ->orderBy("in_timestamp", 'desc')
            ->get();

        $invoices = $system->databaseToArray($invoices);

        //Gather data in a single array
        $data = [];
        $invoice_ids = [];
        foreach ($invoices as $row)
        {
            $id = $row['in_id'];
            $row['amount_paired'] = 0;
            $row['amount_paired_fc'] = 0;
            $data[$id] = $row;
            $invoice_ids[] = $id;
        }

        //Get the actual payed amounts
        if (!empty($invoice_ids))
        {

            $ledger_accounts = DB::table('kt_bank_line_invoice_customer_ledger_account')
                ->select("ktbaliinculeac_in_id", "ktbaliinculeac_amount", "ktbaliinculeac_amount_FC")
                ->whereIn('ktbaliinculeac_in_id', $invoice_ids)
                ->get();

            foreach ($ledger_accounts as $row)
            {
                $invoice_id = $row->ktbaliinculeac_in_id;
                $invoice = &$data[$invoice_id];

                $invoice['amount_paired'] += $row->ktbaliinculeac_amount;
                $invoice['amount_paired_fc'] += $row->ktbaliinculeac_amount_FC;
            }
        }

        //Calculate vars
        foreach ($data as $id => &$invoice)
        {
            $fully_paid = ($invoice['in_amount_netto_eur'] == round($invoice['amount_paired'], 2) ? 1 : 0);
            $invoice['fully_paid'] = $fully_paid;
            $days_overdue = $system->dateDifference($invoice['in_expiration_date'], date("Y-m-d"));

            if ($invoice['in_payment_method'] == 3)
            {
                $days_overdue -= 14;
            }

            $invoice['days_overdue'] = ($fully_paid || $days_overdue < 0 ? 0 : $days_overdue);
        }

        return $data;
    }

    public static function isLoggedInOnce($cu_id = null)
    {
        // Get contact persons of this user
        $contactPersons = ContactPerson::select("cope_id")
            ->where("cope_cu_id", $cu_id)
            ->where("cope_deleted", "!=", 1)
            ->get();

        // If contact persons is empty
        // Assume this person has never logged in
        if (count($contactPersons) == 0)
            return false;

        // Get user logins from this user
        $user_logins = ApplicationUser::select("uslo_id")
            ->join("user_logins", "uslo_apus_id", "apus_id")
            ->where("apus_cu_id", $cu_id)
            ->get();

        // If user logins is empty
        // Assume this person has never logged in
        if (count($user_logins) == 0)
            return false;

        return true;
    }

    public static function getUserField($field, $user_id = null)
    {
        if ($user_id != null)
        {
            $us_id = $user_id;
        } else if (array_key_exists('us_id', $_SESSION))
        {
            $us_id = $_SESSION['us_id'];
        } else
        {
            $us_id = "";
        }

        $database = (APPLICATION_ID == 1 ? "" : "application_") . "users";
        $prefix = (APPLICATION_ID == 1 ? "" : "ap") . "us";

        $result = DB::table($database)
            ->select($field)
            ->where($prefix . "_id", $us_id)
            ->first();

        $result = System::databaseToArray($result);

        if (count($result) > 0)
        {
            return $result[$field];
        } else
        {
            return false;
        }
    }

    public static function getCustomerId($cu_id = null)
    {
        return $cu_id === null ? self::getUserField("apus_cu_id") : $cu_id;
    }

    public static function getField($field, $cu_id = null)
    {
        $get_field = Customer::select($field)
            ->leftJoin("mover_data", "cu_id", "moda_cu_id")
            ->where("cu_deleted", 0)
            ->where("cu_id", ($cu_id ? $cu_id : Mover::getCustomerId()))
            ->first();

        $get_field = System::databaseToArray($get_field);

        if (count($get_field) > 0)
        {
            $row = $get_field;

            return $row[$field];
        } else
        {
            return false;
        }
    }

    public static function getApplicationUserField($field, $user_id = null)
    {
        if ($user_id != null)
        {
            $us_id = $user_id;
        } else if (array_key_exists('us_id', $_SESSION))
        {
            $us_id = $_SESSION['us_id'];
        } else
        {
            $us_id = "";
        }

        $result = ApplicationUser::select($field)
            ->leftJoin("contact_persons", "apus_id", "cope_apus_id")
            ->where("apus_id", $us_id)
            ->first();

        $result = System::databaseToArray($result);

        if (count($result) > 0)
        {
            return $result[$field];
        } else
        {
            return false;
        }
    }

    public static function getPauseDaysAmounts($cu_id) {
        $pause_days_query = PauseDay::where("pada_cu_id", $cu_id)
            ->where("pada_deleted", 0)
            ->whereRaw("YEAR(pada_start_date) = YEAR(CURDATE())")
            ->first();

        return [
            'days_total' => $pause_days_query->pada_amount,
            'days_left' => $pause_days_query->pada_amount_left
        ];
    }

    public static function getPauseDaysUsedThisYear($cu_id)
    {
        //Get pauses
        //Calculate how many days left this year
        $days_used_query = PauseHistory::where("ph_cu_id", $cu_id)
            ->where(function ($query) {
                $query->whereRaw("YEAR(ph_start_date) = YEAR(CURDATE())")
                    ->orWhereRaw("YEAR(ph_end_date) = YEAR(CURDATE())");
            })
            ->get();

        $total_days = 0;

        foreach ($days_used_query as $pause_taken)
        {

            //The whole pause falls within this year
            if (date('Y') == substr($pause_taken->ph_start_date, 0, 4) && date('Y') == substr($pause_taken->ph_end_date, 0, 4))
            {
                $total_days += $pause_taken->ph_total_days;
            } elseif (date('Y') == substr($pause_taken->ph_start_date, 0, 4) && date('Y') != substr($pause_taken->ph_end_date, 0, 4))
            {

                //The pause started this year, but ends in the next one
                $format = 'Y-m-d';

                $today = DateTime::createFromFormat($format, $pause_taken->ph_start_date);
                $appt = DateTime::createFromFormat($format, date("Y") . '-12-31');

                $total_days += ($appt->diff($today)->days + 1);
            } elseif (date('Y') != substr($pause_taken->ph_start_date, 0, 4) && date('Y') == substr($pause_taken->ph_end_date, 0, 4))
            {

                //The pause started last year, but ends in this year
                $format = 'Y-m-d';

                $start = DateTime::createFromFormat($format, date("Y") . '-01-01');
                $to = DateTime::createFromFormat($format, $pause_taken->ph_end_date);

                $total_days += $start->diff($to)->days;
            }

        }

        return $total_days;
    }

    public static function withinHourRange($amount, $difference)
    {
        switch ($amount)
        {
            case 0:
            case 1:
            case 2:
                if ($difference > (12 * 60))
                {
                    return false;
                }
                break;
            case 3:
                if ($difference > (8 * 60))
                {
                    return false;
                }
                break;
            case 4:
                if ($difference > (6 * 60))
                {
                    return false;
                }
                break;
        }

        return true;
    }

    public static function movingSizeText($size, $lang)
    {
        switch ($size)
        {

            case 1:
            case 2:
                $sizeText = Translation::get("moving_size_household", $lang);
                break;
            default:
                $sizeText = DataIndex::movingSizes()[$size];
                break;
        }

        return $sizeText;
    }

}
