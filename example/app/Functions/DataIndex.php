<?php


namespace App\Functions;


use App\Data\AffiliatePartnerRejectionReason;
use App\Data\RejectionReason;
use App\Data\RequestTypeToMovingSizes;
use App\Models\Country;
use App\Models\Language;
use App\Models\Region;
use App\Models\User;

class DataIndex
{
	public static function movingSizes($request_type = null, $request_type_prefix = false)
	{
		$temp = [
			1 => "Removal – Household (Complete)",
			2 => "Removal – Household (Part)",
			3 => "Removal – Household (Small)",
			4 => "Baggage – Baggage (Box plus)",
			5 => "Pet",
			6 => "Car",
			7 => "Commercial shipping",
			8 => "Office move",
			9 => "Baggage – Boxes Only",
			10 => "Piano"
		];

		if($request_type_prefix)
		{
			foreach($temp as $key => $value)
			{
				$temp[$key] = self::requestTypes()[self::movingSizeToRequestType()[$key]]." - ".$value;
			}
		}

		$moving_sizes = [];

		if($request_type !== null)
		{
			foreach(RequestTypeToMovingSizes::name($request_type) as $key => $value)
			{
				$moving_sizes[$value] = $temp[$value];
			}
		}
		else
		{
			$moving_sizes = $temp;
		}

		return $moving_sizes;
	}

	public static function requestTypes()
	{
		return [
			1 => "Move",
			2 => "Baggage",
			3 => "Transport",
			4 => "Office move",
			5 => "Premium"
		];
	}

	public static function rejectionReasonToAffiliateRejectionReason(){
        return $affiliate_reasons = [
            RejectionReason::OTHER => AffiliatePartnerRejectionReason::OTHER,
            RejectionReason::ONLY_TRANSPORT => AffiliatePartnerRejectionReason::NO_RELOCATION,
            RejectionReason::NO_REGION_OCCUPANCY_MOVE => AffiliatePartnerRejectionReason::NO_RELOCATION,
            RejectionReason::NO_REGION_OCCUPANCY_BAGGAGE => AffiliatePartnerRejectionReason::NO_RELOCATION,
            RejectionReason::INVALID_CONTACT_DETAILS => AffiliatePartnerRejectionReason::INVALID_CONTACT_DETAILS,
            RejectionReason::INVALID_ADDRESS_DETAILS => AffiliatePartnerRejectionReason::INVALID_ADDRESS_DETAILS,
            RejectionReason::SAME_LEAD_HAS_BEEN_SENT_TOO_RECENT => AffiliatePartnerRejectionReason::ALREADY_RECEIVED_AN_IDENTICAL_REQUEST,
            RejectionReason::MOVING_DATE_TOO_FAR_AWAY => AffiliatePartnerRejectionReason::INVALID_MOVING_DATE,
            RejectionReason::NO_RELOCATION => AffiliatePartnerRejectionReason::NO_RELOCATION,
            RejectionReason::DOMESTIC_MOVE => AffiliatePartnerRejectionReason::DOMESTIC_MOVE,
            RejectionReason::DOUBLE_SUBMIT_OF_AFFILIATE => AffiliatePartnerRejectionReason::DOUBLE_SUBMIT,
            RejectionReason::DOUBLE_SUBMIT_OF_OWN_WEBSITE => AffiliatePartnerRejectionReason::DOUBLE_SUBMIT,
            RejectionReason::AFFILIATE_LEAD_BUT_ALREADY_RECEIVED_VIA_OWN_WEBSITE => AffiliatePartnerRejectionReason::ALREADY_RECEIVED_VIA_OWN_CHANNELS,
            RejectionReason::AFFILIATE_LEAD_BUT_ALREADY_RECEIVED_VIA_OTHER_AFFILIATE => AffiliatePartnerRejectionReason::ALREADY_RECEIVED_VIA_OWN_CHANNELS,
            RejectionReason::OWN_LEAD_BUT_ALREADY_RECEIVED_VIA_AFFILIATE => AffiliatePartnerRejectionReason::OWN_LEAD_BUT_ALREADY_RECEIVED_VIA_OTHER_OWN_WEBSITE,
            RejectionReason::OWN_LEAD_BUT_ALREADY_RECEIVED_VIA_OTHER_OWN_WEBSITE => AffiliatePartnerRejectionReason::OWN_LEAD_BUT_ALREADY_RECEIVED_VIA_AFFILIATE,
            RejectionReason::OWN_TEST_LEAD => AffiliatePartnerRejectionReason::TEST_LEAD,
            RejectionReason::THIRD_PARTY_TEST_LEAD => AffiliatePartnerRejectionReason::TEST_LEAD,
            RejectionReason::EMPTY_LEAD => AffiliatePartnerRejectionReason::EMPTY_LEAD,
            RejectionReason::SPAM_LEAD => AffiliatePartnerRejectionReason::SPAM_LEAD,
            RejectionReason::FAKE_LEAD => AffiliatePartnerRejectionReason::FAKE_LEAD,
            RejectionReason::DOUBLE_SUBMIT_WITHIN_24_HOURS => AffiliatePartnerRejectionReason::DOUBLE_SUBMIT,
            RejectionReason::DOUBLE_SUBMIT_BETWEEN_1_AND_30_DAYS => AffiliatePartnerRejectionReason::ALREADY_RECEIVED_AN_IDENTICAL_REQUEST
        ];

    }

	public static function movingSizeToRequestType()
	{
		return [
			1 => 1,
			2 => 1,
			3 => 1,
			4 => 2,
			5 => 3,
			6 => 3,
			7 => 3,
			8 => 4,
			9 => 2,
            10 => 3
		];
	}

	public static function customerPairStatuses()
	{
		return [
			0 => "Inactive",
			1 => "Active",
			2 => "Pause",
		];
	}

	public static function moverCappingMethods()
	{
		return [
			0 => "Portal level",
			1 => "Monthly leads",
			2 => "Monthly spend"
		];
	}

	public static function getLanguage($la_code)
	{
		$language = Language::where("la_code", "=", $la_code)->first();

		if(count($language) > 0)
		{
			return $language->la_language;
		}
		else
		{
			return false;
		}
	}

	public static function getCountry($co_code)
	{
		$country = Country::where("co_code", "=", $co_code)->first();

		if(count($country) > 0)
		{
			return $country->co_en;
		}
		else
		{
			return false;
		}
	}

	public static function getUserName($us_id)
	{
		$user = User::where("us_id", "=", $us_id)->first();

		if(count($user) > 0)
		{
			return $user->us_name;
		}
		else
		{
			return false;
		}
	}

	public static function getMonths()
	{
		return cal_info(0)['months'];
	}

	public static function salesUsers($deleted = false)
	{
		return self::getTypedUsers("us_sales", $deleted);
	}

    public static function financeUsers( $deleted = false)
    {
        return self::getTypedUsers("us_finance", $deleted);
    }


    public static function partnerdeskUsers( $deleted = false)
    {
        return self::getTypedUsers("us_partnerdesk", $deleted);
    }

	public static function getTypedUsers($required_user_field = null, $deleted = false)
	{
	    $users = [];

		$builder = User::select("us_id", "us_name");
		if (!$deleted)
        {
            $builder = $builder->where("us_is_deleted", $deleted);
        }

		$builder = $builder->orderBy("us_name");

		if($required_user_field !== null)
		{
		    $builder = $builder->where($required_user_field, 1);
		}
		$builder = $builder->get();

		foreach($builder as $user)
        {
            $users[$user->us_id] = $user->us_name;
        }

		return $users;
	}

	public static function customerStatuses()
	{
		return [
			1 => "New booking",
			2 => "Upsell",
			3 => "Reactivation",
			4 => "Pause",
			5 => "Unpause",
			6 => "Credithold",
			7 => "Out of credithold",
			8 => "Cancellation",
			9 => "Downsell"
		];
	}

	public static function getRegion($region_id)
	{
	    $result = Region::select("reg_co_code", "reg_parent", "reg_name")
            ->where("reg_id", $region_id)
            ->first();

		if (count($result) > 0)
        {
            if(!empty($result->reg_name))
			{
				return $result->reg_parent.", ".$result->reg_name;
			}
			else
			{
				return $result->reg_parent;
			}
        }
		else
        {
            return false;
        }
	}

	public static function CombinedReasons()
    {
        return [
            21 => [10,11,21],
			22 => [6,12,13,14,15,22]
        ];
    }

    public static function reportProgressStatus()
	{
		return [
			0 => "Queued",
			1 => "In progress",
			2 => "Final section",
			3 => "Finished",
			4 => "Deleted",
			5 => "Failed"
		];
	}
}
