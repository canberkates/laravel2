<?php


namespace App\Functions;
use DOMDocument;

class DOMDocumentExtended extends DOMDocument
{
    public function createElementText($name, $value)
    {
        $new_child = $this->createElement($name);
        
        $node = dom_import_simplexml($new_child);
        $no = $node->ownerDocument;
        $node->appendChild($no->createTextNode($value));
        
        return $new_child;
    }
    
    public function createElementCDATA($name, $value)
    {
        $new_child = $this->createElement($name);
        
        $node = dom_import_simplexml($new_child);
        $no = $node->ownerDocument;
        $node->appendChild($no->createCDATASection($value));
        
        return $new_child;
    }
}
