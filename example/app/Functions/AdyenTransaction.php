<?php


namespace App\Functions;


use Adyen\AdyenException;
use App\Data\AdyenApiTypes;
use App\Data\YesNo;
use App\Models\AdyenCardDetails;
use App\Models\AdyenPayment;
use Carbon\Carbon;
use DateTime;
use DB;
use Log;

class AdyenTransaction
{
    /**
     * Insert a payment into adyen_payments.
     * @param int $adcade_id The card_details id
     * @param number $charge The amount charged
     * @param string $prefix The merchant reference prefix
     * @return array result details
     */
    public static function insertCreditCardPayment($adcade_id, $charge, $prefix = "", $invoice = null)
    {
        $system = new System();

        $row = DB::table("adyen_card_details")
            ->select("cu_id", "cu_email", "cu_pacu_code", "adcade_id", "adcade_merchant_reference", "adcade_card_alias")
            ->leftJoin("customers", "cu_id", "adcade_cu_id")
            ->where("adcade_id", $adcade_id)
            ->first();

        $currency = ($invoice === null ? $row->cu_pacu_code : $invoice->in_currency);

        $adyen_payment = new AdyenPayment();

        $adyen_payment->adpa_adcade_id = $row->adcade_id;
        $adyen_payment->adpa_timestamp = Carbon::now()->toDateTimeString();
        $adyen_payment->adpa_amount = $charge;
        $adyen_payment->adpa_pacu_code = $currency;
        $adyen_payment->adpa_status = 0;
        $adyen_payment->adpa_changes = System::serialize([]);

        $adyen_payment->save();

        if ($invoice !== null)
        {
            $merchant_reference = "{$prefix}-{$adyen_payment->adpa_id}-{$invoice['in_number']}";
        } else
        {
            //Update merchant reference
            $merchant_reference = "{$prefix}-{$adyen_payment->adpa_id}-" . date("YmdHis");
        }

        DB::table("adyen_payments")
            ->where("adpa_id", $adyen_payment->adpa_id)
            ->update(
                [
                    "adpa_merchant_reference" => $merchant_reference
                ]
            );

        return [
            "adcade" => $system->databaseToArray($row),
            "adpa_id" => $adyen_payment->adpa_id,
            "adpa_merchant_reference" => $merchant_reference
        ];
    }

    /**
     * Make a credit card payment.
     * @param array $adcade The adcade row (see #Adyen::insertCreditCardPayment())
     * @param int $adpa_id The adpa ID
     * @param string $adpa_merchant_reference The adpa merchant reference
     * @param number $charge The amount to be charged
     */
    public static function makeCreditCardPayment($adcade, $adpa_id, $adpa_merchant_reference, $charge)
    {
        self::updatePayment($adpa_id, ['adpa_status' => 1]);

        //Make the payment
        $post = [
            "amount" => [
                "value" => self::decimalValue($charge, true),
                "currency" => $adcade['cu_pacu_code']
            ],

            "reference" => $adpa_merchant_reference,
            "merchantAccount" => env('ADYEN_API_MERCHANT_ACCOUNT'),

            "shopperEmail" => $adcade['cu_email'],
            "shopperReference" => $adcade['adcade_merchant_reference'],
            "selectedRecurringDetailReference" => $adcade['adcade_card_alias'],

            "recurring" => [
                "contract" => "RECURRING,ONECLICK"
            ],
            "shopperInteraction" => "ContAuth"
        ];

        $result = self::apiCall(AdyenApiTypes::PAYMENT, $post);

        //Check if failed
        if ($result == false)
        {
            self::updatePayment($adpa_id, ['adpa_status' => -1, 'adpa_refusal_reason' => 'Unable to contact Adyen']);

            return 'Unable to contact Adyen';
        }

        if (isset($result['status']))
        {
            self::updatePayment($adpa_id, [
                'adpa_status' => -1,
                'adpa_psp_reference' => $result['pspReference'],
                'adpa_refusal_reason' => "{$result['message']} (Type: {$result['errorType']} | Status: {$result['status']} | Error: {$result['errorCode']}"
            ]);

            return "{$result['message']} (Type: {$result['errorType']} | Status: {$result['status']} | Error: {$result['errorCode']}";
        }

        //Processed/Confirmed
        $update = ['adpa_result_code' => $result['resultCode'], 'adpa_psp_reference' => $result['pspReference']];
        switch ($result['resultCode'])
        {
            case "Authorised":
                $update['adpa_status'] = 2;
                break;

            case "Refused":
                $update['adpa_status'] = 3;
                $update['adpa_refusal_reason'] = $result['refusalReason'];
                break;

            case "Error":
            case "Received":
            case "Cancelled":
            default:
                $update['adpa_status'] = -1;
                $update['adpa_refusal_reason'] = "Unknown resultCode";
                Log::debug("[Adyen] Unknown resultCode for payment (adpa: {$adpa_id}): {$result['resultCode']}");
                Log::debug("[Adyen] Json result: " . json_encode($result));
                break;
        }

        //Update entry
        self::updatePayment($adpa_id, $update);
        return $result['resultCode'];
    }

    /**
     * Make a CURL call to an Adyen API endpoint.
     * @param string $path Endpoint path (see defined variables 'ADYEN_{X}')
     * @param array|string $data The JSON data (array or json encoded string)
     * @return array|false the result or false if the call failed (URL couldn't be reached etc)
     */
    public static function apiCall($adyentype, $data)
    {

        $client = new \Adyen\Client();
        $client->setApplicationName("Adyen PHP Api Library Example");
        $client->setUsername(env('ADYEN_API_USER'));
        $client->setPassword(env('ADYEN_API_PASSWORD'));
        $client->setXApiKey(env('ADYEN_API_KEY'));
        if(env('ADYEN_ENVIRONMENT') == "TEST")
        {
            $client->setEnvironment(\Adyen\Environment::TEST);
        }elseif(env('ADYEN_ENVIRONMENT') == 'PRODUCTION'){
            $client->setEnvironment(\Adyen\Environment::LIVE);
        }

        //Decide and process the type of Adyen payment
        if ($adyentype == AdyenApiTypes::PAYMENT)
        {
            $service = new \Adyen\Service\Payment($client);
            try
            {
                return $service->authorise($data);
            } catch (AdyenException $e)
            {
                return $e;
            }
        } elseif ($adyentype == AdyenApiTypes::RECURRING)
        {
            $service = new \Adyen\Service\Recurring($client);

            try
            {
                return $service->listRecurringDetails($data);
            } catch (AdyenException $e)
            {
                return $e;
            }
        } elseif ($adyentype == AdyenApiTypes::RECURRING_DISABLE)
        {
            $service = new \Adyen\Service\Recurring($client);

            try
            {
                Log::debug($data);

                return $service->disable($data);
            } catch (AdyenException $e)
            {
                return $e;
            }
        }
    }

    /**
     * Get the recurring details for a certain shopper.
     * @param string $shopperReference The shopper reference of the customer
     * @return array|false The result
     */
    public static function recurringDetails($shopperReference)
    {
        //Create & post
        $post = [
            "merchantAccount" => env('ADYEN_API_MERCHANT_ACCOUNT'),
            "recurring" => [
                "contract" => "RECURRING"
            ],
            "shopperReference" => $shopperReference
        ];

        return self::apiCall(AdyenApiTypes::RECURRING, $post);
    }

    /**
     * Get the recurring details for a certain shopper.
     * @param string $shopperReference The shopper reference of the customer
     * @return array|false The result
     */
    public static function addCreditCard($row, $new_card)
    {
        //Make the call
        $post = [
            "amount" => [
                "value" => 0,
                "currency" => $row->cu_pacu_code
            ],
            "merchantAccount" => env('ADYEN_API_MERCHANT_ACCOUNT'),
            "reference" => "CARDCHECK-{$row->cu_id}-" . date("YmdHis"),
            "shopperEmail" => $row->cu_email,
            "shopperReference" => $row->cu_id,
            "additionalData" => [
                "card.encrypted.json" => $new_card
            ],
            "recurring" => [
                "contract" => "RECURRING,ONECLICK"
            ]
        ];

        return self::apiCall(AdyenApiTypes::PAYMENT, $post);
    }

    /**
     * Get the recurring details for a certain shopper.
     * @param string $shopperReference The shopper reference of the customer
     * @return array|false The result
     */
    public static function modifyCreditCard($row, $request)
    {
        //Make the call
        $post = [
            "amount" => [
                "value" => 0,
                "currency" => $row->cu_pacu_code
            ],
            "merchantAccount" => env('ADYEN_API_MERCHANT_ACCOUNT'),
            "reference" => "CARDCHECK-{$row->cu_id}-" . date("YmdHis"),
            "shopperEmail" => $row->cu_email,
            "shopperReference" => $row->cu_id,
            "recurring" => [
                "contract" => "RECURRING"
            ]
        ];

        $post["card"] = [
            "expiryMonth" => $request->expiry_month,
            "expiryYear" => $request->expiry_year
        ];
        $post["selectedRecurringDetailReference"] = $row->updateCardAlias;
        $post['shopperInteraction'] = "ContAuth";

        return self::apiCall(AdyenApiTypes::PAYMENT, $post);
    }


    /**
     * Update an Adyen payment.
     * @param number $adpa_id The id
     * @param array $field_to_value Each field with the new value
     */
    public static function updatePayment($adpa_id, $field_to_value)
    {
        DB::table("adyen_payments")
            ->where("adpa_id", $adpa_id)
            ->update(
                $field_to_value
            );
    }

    /**
     * Transform a value provided by Adyen into a decimal value.
     * @param mixed $value the value
     * @param bool|false $reverse reversed effect (will change it into a non decimal value)
     * @return mixed the result
     */
    public static function decimalValue($value, $reverse = false)
    {
        if ($reverse)
        {
            return round($value * 100);
        } else
        {
            return substr_replace($value, ".", -2, 0);
        }
    }

    public static function disableAllCards($cu_id)
    {
        DB::table('adyen_card_details')
            ->where('adcade_cu_id', $cu_id)
            ->update(
                [
                    'adcade_enabled' => 0
                ]
            );
    }

    /**
     * Insert a payment into adyen_payments.
     * @param int $adcade_id The card_details id
     * @param number $charge The amount charged
     * @param string $prefix The merchant reference prefix
     * @return array result details
     */
    public static function insertCreditCardInfo($carddata, $row)
    {
        $details = $carddata['RecurringDetail'];

        $existing_card = DB::table("adyen_card_details")
            ->select("adcade_id")
            ->where("adcade_card_alias", $details['alias'])
            ->where("adcade_cu_id", $row->cu_id)
            ->first();

        if (empty($existing_card))
        {
            $adyen_card_details = new AdyenCardDetails();

            $adyen_card_details->adcade_cu_id = $row->cu_id;
            $adyen_card_details->adcade_merchant_reference = $row->cu_id;
            $adyen_card_details->adcade_card_expiry_month = $details['card']['expiryMonth'];
            $adyen_card_details->adcade_card_expiry_year = $details['card']['expiryYear'];
            $adyen_card_details->adcade_card_last_digits = $details['card']['number'];
            $adyen_card_details->adcade_card_alias = $details['alias'];
            $adyen_card_details->adcade_enabled = YesNo::YES;

            $adyen_card_details->save();
        } else
        {

            DB::table('adyen_card_details')
                ->where('adcade_id', $existing_card->adcade_id)
                ->update(
                    [
                        'adcade_card_expiry_month' => $details['card']['expiryMonth'],
                        'adcade_card_expiry_year' => $details['card']['expiryYear'],
                        'adcade_enabled' => 1,
                        'adcade_removed' => 0
                    ]
                );
        }
    }

    public static function disableRecurringContract($card_id)
    {

        $adyen_card_details = AdyenCardDetails::select("adcade_cu_id", "adcade_merchant_reference", "adcade_card_alias", "adcade_enabled")
            ->where("adcade_id", $card_id)
            ->first();


        DB::table('adyen_card_details')
            ->where('adcade_id', $card_id)
            ->update(
                [
                    'adcade_enabled' => 0,
                    'adcade_removed' => 1
                ]
            );


        //Auto switch to a new one if possible
        if ($adyen_card_details->adcade_enabled == 1)
        {

            DB::table('adyen_card_details')
                ->where('adcade_cu_id', $adyen_card_details->adcade_cu_id)
                ->where('adcade_removed', 0)
                ->orderBy("adcade_card_expiry_year", 'desc')
                ->orderBy("adcade_card_expiry_month", 'desc')
                ->limit(1)
                ->update(
                    [
                        'adcade_enabled' => 1,
                    ]
                );
        }

        Log::debug($adyen_card_details);

        //Update Adyen
        $recurring_detail = self::matchRecurringDetail($adyen_card_details['adcade_merchant_reference'], $adyen_card_details['adcade_card_alias']);
        if ($recurring_detail === false)
        {
            return "Couldn't find the contract!";
        }
        $post = [
            "merchantAccount" => env('ADYEN_API_MERCHANT_ACCOUNT'),
            "shopperReference" => $adyen_card_details['adcade_merchant_reference'],
            "recurringDetailReference" => $recurring_detail['recurringDetailReference']
        ];
        $result = self::apiCall(AdyenApiTypes::RECURRING_DISABLE, $post);

        Log::debug("Adyen delete result:");
        Log::debug($result);

        if ($result === false)
        {
            Log::debug('Unable to contact Adyen...');

            return 'Unable to contact Adyen...';
        }

        if (isset($result['status']))
        {
            Log::debug("Contract modification failed: {$result['message']} (Type: {$result['errorType']} | Status: {$result['status']} | Code: {$result['errorCode']})");

            return "Contract modification failed: {$result['message']} (Type: {$result['errorType']} | Status: {$result['status']} | Code: {$result['errorCode']})";
        }


        if (!isset($result['response']) || ($result['response'] != '[detail-successfully-disabled]' && $result['response'] != '[all-details-successfully-disabled]'))
        {
            Log::debug("Unable to disable Recurring contract");

            return "Unable to disable Recurring contract";
        }

        return 'Successfully removed the contract!';
    }

    public static function matchRecurringDetail($shopper_reference, $alias)
    {
        $details = self::recurringDetails($shopper_reference);
        if ($details === false)
        {
            return false;
        }

        Log::debug($details);

        if (isset($details['details']))
        {
            foreach ($details["details"] as $recurring_detail)
            {
                $found_card_alias = $recurring_detail['RecurringDetail']['alias'];
                if ($found_card_alias == $alias)
                {
                    Log::debug($recurring_detail['RecurringDetail']);

                    return $recurring_detail['RecurringDetail'];
                }
            }
        }

        Log::debug($details);
        Log::debug("[Adyen] Unable to find alias '{$alias}': " . json_encode($details));

        return false;
    }

    /**
     * Check whether a Adyen credit card has expired.
     * @param array $card_row The card row
     * @return bool has expired
     */
    public static function expiredCard($expiry_year, $expiry_month)
    {
        $today = new DateTime('midnight today');
        $expires = new DateTime("midnight {$expiry_year}/{$expiry_month}/01");

        return $today->diff($expires)->invert === 1;
    }

    public static function createRecurringContract($row, $request)
    {
        //Make the call
        $result = self::modifyCreditCard($row, $request);
        Log::debug($result);

        //Check if authorised
        $result_code = $result['resultCode'];
        switch ($result_code)
        {
            case "Authorised":
                return $result;
                break;

            case "Refused":
                Log::debug("Modify Credit Card denied: " . $result['refusalReason']);

                return ["error" => "Contract/Card denied: {$result['refusalReason']}"];

            case "Error":
            case "Received":
            case "Cancelled":
            default:
                Log::debug("[Adyen] Unable to create contract due to unknown resultCode: {$result_code}");
                Log::debug("[Adyen] Json result: " . json_encode($result));

                return ["error" => "Unknown resultCode {$result_code}, unable to create contract?"];
        }
    }


    public static function process($row, $request)
    {
        //TODO: Check if fields filled in & correct?
        $result = self::createRecurringContract($row, $request);
        if (array_key_first($result) === 'error')
        {
            Log::debug("returning false");
            return $result['error'];
        }

        Log::debug("Updating CC");
        Log::debug($result);

        //Get the details
        $card_alias = $result['additionalData']['alias'];

        //Give Adyen some time to process it all...
        sleep(1);

        //=> Find the recurringDetailReference
        $found_recurring_detail = self::matchRecurringDetail($row->cu_id, $card_alias);
        if ($found_recurring_detail === false)
        {
            return "Couldn't find updated Credit Card...";
        }

        //Set all to inactive
        self::disableAllCards($row->cu_id);

        $card_details = AdyenCardDetails::select("adcade_id")
            ->where("adcade_card_alias", $card_alias)
            ->where("adcade_cu_id", $row->cu_id)
            ->first();


        DB::table('adyen_card_details')
            ->where('adcade_id', $card_details->adcade_id)
            ->update(
                [
                    'adcade_card_expiry_month' => $found_recurring_detail['card']['expiryMonth'],
                    'adcade_card_expiry_year' => $found_recurring_detail['card']['expiryYear'],
                    'adcade_enabled' => 1,
                    'adcade_removed' => 0
                ]
            );

        return "Credit card has been updated!";
    }


}
