<?php

if(isset($_GET['env']) && isset($_GET['invoice']) && isset($_GET['date']))
{
	$file = $_GET['env']."uploads/invoices/".date("Y", strtotime($_GET['date']))."/invoice_".$_GET['invoice'].".pdf";
	
	if(file_exists($file))
	{
		header("Content-Type: application/pdf");
		header("Content-Disposition: attachment; filename=".urlencode(basename($file)));
		
		readfile($file);
	}
}
