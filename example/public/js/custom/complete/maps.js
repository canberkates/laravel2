function getMap( $direction, $callback = '' ) {

    // Store map
    var $map = $( '.google_maps_image_' + $direction );

    // Store map width
    var $mapWidth = Math.round( $map.width() );

    // Do the ajax call
    $.ajax({
        type: 'GET',
        url: trailingslashit( $ajaxUrl ) + 'loadgooglemapsimage/',
        data: {
            'street': $( 'input[name="street_' + $direction + '"]' ).val(),
            'zipcode': $( 'input[name="zipcode_' + $direction + '"]' ).val(),
            'city': $( 'input[name="city_' + $direction + '"]' ).val(),
            'country': $( 'input[name="country_' + $direction + '"]' ).val(),
            'width': $mapWidth,
            'height': 250,
        },
        success: function( $data ){

            $map.html( $data );

            if( typeof $callback === 'function' ) {

                $callback( $data );
            }
        },
    });
}
