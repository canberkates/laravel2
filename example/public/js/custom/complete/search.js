$( document ).ready( function() {

    $( '#header-search-form' ).submit( function(e) {

        // Get input
        var $input = $( '#page-header-search-input' );

        // Get value
        var $value = $input.val();

        // If the value is not numeric and the length is less then 3
        if( ! $.isNumeric( $value ) && $value.length < 3 ) {

            // Stop submit
            e.preventDefault();

            // Add shake boi
            $input.addClass( 'animated shake' );

            // Timeout bois
            setTimeout( function() {

                // Remove that shake boi
                $input.removeClass( 'animated shake' );

                // Wait 3 seconds
            }, 3000);
        }
    });
});
