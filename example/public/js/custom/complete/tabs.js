// Get tab from storage
var $activeTab = localStorage.getItem( 'activeTab' );

// First look for hash
if( location.hash ) {
    $( 'a[href="' + location.hash + '"' ).tab( 'show' );
}
else if( $activeTab ) {
    $( 'a[href="' + $activeTab + '"' ).tab( 'show' );
}

$( 'body' ).on( 'click', '.nav-tabs .nav-link', function(e) {

    e.preventDefault();

    // Get tab level
    var $tabLevel = $( this ).parents( '.nav-tabs' ).data( 'tab-control' );

    // Get tab name
    var $tabName = this.getAttribute( 'href' );

    // If history push state is supported
    if( history.pushState ) {

        // Set push state to tab name
        history.pushState(null, null, $tabName );

    }
    else {

        location.hash = $tabName;
    }
    localStorage.setItem( 'activeTab', $tabName );
    $( this ).tab( 'show' );
    return false;
});
$( window ).on( 'popstate', function () {
    let $anchor = location.hash ||
        $( 'a[data-toggle="tab"]' ).first().attr( 'href' );
    $( 'a[href="' + $anchor + '"]' ).tab( 'show' );
});
