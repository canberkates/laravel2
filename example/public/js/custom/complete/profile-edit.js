$( document ).ready( function() {

	$( '#profile_edit_submit' ).click( function() {

	    // Get values
		var $currentPassword = $( '#current_password' ).val();
		var $newPassword = $( '#new_password' ).val();
		var $newPasswordConfirm = $( '#new_password_confirm' ).val();
		var $userId = $( '#us_id' ).val();
		
		jQuery.ajax({
			url: "/ajax/profile/changepassword",
			method: 'get',
            data: {
                us_id: $userId,
                current_password: $currentPassword,
                new_password: $newPassword,
                new_password_confirm: $newPasswordConfirm
            },
			success: function( $result ) {

			    // Store elements
				var $messageEl = $("#change_password_errordiv");

				// If changing went successfully
				if( $result.success ) {

				    // Slide up submit button and div
				    $( '#profile_edit_submit' ).slideUp();
                    $( '#change_password_div' ).slideUp();

                    $messageEl.removeClass( 'bg-danger' ).addClass( 'bg-success' ).html( $result.message ).show();

                    setTimeout(function () {
                        $("#logout-form").submit();
                    }, 2000);
                }
				else {

                    $messageEl.addClass( 'bg-danger' ).html( $result.message ).show();
                }
			}
		});
	});
});
