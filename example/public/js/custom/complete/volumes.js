function convertVolume( $volume, $type ) {

    if( $type === 'ft3' ) {

        return ( $volume * 35.3146667 ).toFixed( 1 );
    }
    else {

        return ( $volume * 0.0283168466 ).toFixed( 1 );
    }
}
