function getMovingSizes( $requestType, $callback = '', $callbackError = '' ) {

    jQuery.ajax( {

        type: 'GET',
        url: trailingslashit( $ajaxUrl ) + 'getmovingsizes/',
        data: {
            request_type: $requestType
        },
        success: function( $result ) {

            if( typeof $callback === 'function' ) {

                $callback( $result );
            }
        },
        error: function( jqXHR, textStatus, errorThrown ) {

            if( typeof  $callbackError === 'function' ) {

                $callbackError( jqXHR, textStatus, errorThrown );
            }
        }
    } );
}
