function parseRegions( $options = {}, $callback = '' ) {

    // Needed options direction, destinationType,

    // Store elements
    var $countryEl = $( 'select[name="country_' + $options.direction + '"]' );
    var $regionEl = $( 'select[name="region_' + $options.direction + '"]' );
    var $destinationType = $options.destinationType;

    // Clear options from region select
    $regionEl.empty();

    // Get the regions
    $.ajax( {

        type: 'GET',
        url: trailingslashit( $ajaxUrl ) + 'getregions/',
        data: {
            country: $countryEl.val(),
            destination_type: $destinationType,
        },
        success: function( $result ) {

            // Get opts from result
            $result = $.parseJSON ( $result );

            // Set default select options
            var $selectOptions = '<option value=""></option>';

            // Loop result
            $.each( $result, function( $greaterRegion, $region ) {

                // If there are no optgroups
                if( typeof $region === 'string' ) {

                    // Add to options
                    $selectOptions += '<option value="' + $region + '">' + $greaterRegion + '</option>';
                }
                else {

                    // Open optgroup
                    $selectOptions += '<optgroup label="' + $greaterRegion + '">';

                    // Loop sub regions
                    $.each( $region, function( $regionVal, $regionName ) {

                        // Add to options
                        $selectOptions += '<option value="' + $regionVal + '">' + $regionName + '</option>';
                    });

                    // Close optgroup
                    $selectOptions += '</optgroup>';
                }
            });

            // Set value and trigger
            $regionEl.append( $selectOptions ).val( ( 'initVal' in $options ) ? $options.initVal : null ).trigger( 'change' );

            // Do some callback
            if( typeof $callback === 'function' ) {

                $callback( $regionEl );
            }
        }
    });
}
