$( document ).ready( function() {

    // Store body
    var $bodyEl = $( 'body' );

    // On click reset select
    $bodyEl.on( 'click', '.reset-select', function(e) {

        // Stop submitting
        e.preventDefault();

        // Get target
        var $target = $( $( this ).data( 'target' ) );

        // If the value of the target is not empty
        if( $target.val() !== '' ) {

            // Reset value and trigger change
            $target.val( '' ).trigger( 'change' );
        }
    });

    $bodyEl.on( 'click', '.reset-input', function(e) {

        // Stop submitting
        e.preventDefault();

        // Get target
        var $target = $( $( this ).data( 'target' ) );

        // If the value of the target is not empty
        if( $target.val() !== '' ) {

            // Reset value
            $target.val( '' );
        }
    });
});
