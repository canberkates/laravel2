function getTranslation( $value, $callback = '', $callbackError = '' ) {

    $.ajax( {

        type: 'GET',
        url: trailingslashit( $ajaxUrl ) + 'gettranslation/',
        data: {
            value: $value,
        },
        success: function( $result ) {

            if( typeof $callback === 'function' ) {

                $callback( $result );
            }
        },
        error: function( jqXHR, textStatus, errorThrown ) {

            if( typeof  $callbackError === 'function' ) {

                $callbackError( jqXHR, textStatus, errorThrown );
            }
        }
    } );
}
