$( document ).ready( function() {

    var $body = $( 'body' );

    $body.on( 'click', '#erp-sidebar-close', function() {

        $( '#erp-sidebar-open' ).removeClass( 'd-none' );
    } );

    $body.on( 'click', '#erp-sidebar-open', function() {

        $( '#erp-sidebar-open' ).addClass( 'd-none' );
    });
});

