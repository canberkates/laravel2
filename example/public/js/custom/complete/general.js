// This file contains general functions which can be used in all the other specific categorized files

function trailingslashit( $uri ) {

    return $uri.replace( /\/?$/, '/' );
}

