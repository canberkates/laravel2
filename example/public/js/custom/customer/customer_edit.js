$(document).ready(function() {
    setClaimPercentageDropdown();
    setCappingDivs();
    $('input[name=moda_no_claim_discount]').trigger('change');
    $('select[name=capping_method]').trigger('change');


    $('input[name=use_billing_address]').change(function() {
        if($('input[name=use_billing_address]').is(":checked")){
            $('div[name=show_billing_address]').show();
            $('button[name=office_update]').hide();
        }else{
            $('div[name=show_billing_address]').hide();
            $('button[name=office_update]').show();
        }
    });
    
    $('#public_liability').change(function(){
		if ($("input[name=public_liability]").is(':checked') == true)
		{
			$("#public_liability_value").slideDown();
		}
		else
		{
			$("#public_liability_value").slideUp();
			$("input[name=public_liability_value]").val("");
		}
	}).trigger('change');
	
	$('#goods_in_transit').change(function(){
		if ($("input[name=goods_in_transit]").is(':checked') == true)
		{
			$("#goods_in_transit_insurance_value").slideDown();
		}
		else
		{
			$("#goods_in_transit_insurance_value").slideUp();
			$("input[name=goods_in_transit_value]").val("");
		}
	}).trigger('change');
	
	if($("input[name=disable_sirelo_export]").is(":checked")){
		$("#sirelo_widget_location").hide();
	}
	
	$("input[name=disable_sirelo_export]").change(function(){
		if($(this).is(":checked")){
			$("#sirelo_widget_location").slideUp();
		}
		else{
			$("#sirelo_widget_location").slideDown();
		}
	});
});

function setClaimPercentageDropdown(){
    claim_percentage_dropdown =  $('select[name=claim_percentage]');
    //If no claim discount is selected, set claim percentage to 0%
    $('input[name=moda_no_claim_discount]').change(function() {
        if(this.checked){
            claim_percentage_dropdown.val(0);
            claim_percentage_dropdown.prop('disabled', true);
        }else{
            claim_percentage_dropdown.prop('disabled', false);
        }
    });
}

function setCappingDivs(){
    capping_method =  $('select[name=capping_method]');
    //If no claim discount is selected, set claim percentage to 0%
    capping_method.change(function() {
        if($(this).val() == 0){
            $('div[name=monthly_leads]').hide(500);
            $('div[name=monthly_spend]').hide(500);

        }else if($(this).val() == 1){
            $('div[name=monthly_leads]').show(500);
            $('div[name=monthly_spend]').hide(500);
        }else{
            $('div[name=monthly_leads]').hide(500);
            $('div[name=monthly_spend]').show(500);
        }
        
		//Change the explanation
		var message = "Select a method";
		var hideLimit = true;
		var hideCurrency = true;
		var curValue = parseInt($(this).val());
		switch(curValue){
			case 0:
				message = "All cappings are handled on portal level.";
				break;
			
			case 1:
				hideLimit = false;
				message = "A combined monthly limit of requests for all portals.<br/>" +
					"This uses the same daily average that normal cappings use, only all portals contribute to this number.<br/>" +
					"The 'Daily capping' on portal level is still taken into account with this setting.";
				break;
			
			case 2:
				hideLimit = false;
				hideCurrency = false;
				message = "A specified amount that can be spend on requests.<br/>" +
					"The maximum amount can be entered that the customer is allowed to spend in a month.<br/>" +
					"The netto price of the request will be subtracted from this number till it reaches 0.<br/>" +
					"The 'Daily capping' on portal level is still taken into account with this setting.";
				break;
		}
		
		$("#mover_capping_method_explanation").html(message);
    });
    var valid = false;
    $("#affiliate_settings").submit(function(){
    	if(valid){
			return true;
		}
  
		var hasErrors = false;
    	
		if($("input[name=revenue_share]").val() != "" && ($("input[name=price_per_lead]").val() > 0)) {
			hasErrors = true;
			
			alert("Revenue share can't filled in if price per lead is not 0.00");
		}
		
		if(hasErrors === false){
			valid = true;
			$("#affiliate_settings").submit();
			
		}
		
		return false;
	});
 
    var startCreditHold = ($('input[name=finance_credit_hold]').is(':checked') === true);
	var startPaymentMethod = $('select[name=finance_payment_method]').find('option:selected').val();
 
    $("#customers_finance, #serviceproviders_finance").submit(function(){
		var newCreditHold = ($('input[name=credit_hold]').is(':checked') == true);
		var newPaymentMethod = $('select[name=payment_method]').find('option:selected').val();
		var creditLimit = $('input[name=credit_limit]').val();
		
		// New is turned off
		// Old was on
		if(newCreditHold === false && startCreditHold === true){
			
			if(!confirm('Are you sure you want to remove credit hold? The date of credit hold will be removed.')){
				e.preventDefault();
				return false;
			}
		}
		
		//If started payment method wasn't prepayment, and if payment method is prepaypemnt after click on save button
		//Then Show message: Credit limit will be set on 0.00, are you sure?
		if (newPaymentMethod == 4 && startPaymentMethod != 4 && creditLimit != 0)
		{
			if(!confirm('The current Credit limit of ' + creditLimit + ' will be set to 0.00. Are you sure you want to do that?')){
				e.preventDefault();
				return false;
			}
		}
		
		return true;
	});
 
	if($("input[name=credit_hold]").is(":not(:checked)")){
		$("#debt_collector_div").hide();
	}
	
	$("input[name=credit_hold]").change(function(){
		if($(this).is(":checked")){
			$("#debt_collector_div").slideDown();
		}
		else{
			$("#debt_collector_div").slideUp();
		}
	});
	
	$("select[name=payment_method]").change(function(){
		var payment_method = $("select[name=payment_method]").val();
		
		if (payment_method == 4)
		{
			$("input[name=credit_hold]").prop('disabled', true);
		}
		else {
			$("input[name=credit_hold]").prop('disabled', false);
		}
	});
	
	$('input[name=credit_hold]').change(function(){
		var value = 0;
		if ($('input[name=credit_hold]').is(':checked') == true)
		{
			value = 1;
		}
		$("input[name=credit_hold_hidden]").val(value);
	});
	
}