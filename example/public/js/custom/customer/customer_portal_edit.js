$(function(){
	$(".select_eu").click(function(){
		jQuery.ajax({
			url: "/ajax/customerportal/select_eu_countries",
			method: "get",
			dataType: "json",
			success: function( data) {
				$.each(data , function(index, val) {
					$('#destinations\\['+ val.co_code +'\\]').prop("checked", true);
				});
			}
		});
	});
	
	$(".select_schengen_area").click(function(){
		$.ajax({
			type: "get",
			url: "/ajax/customerportal/select_schengen_area",
			dataType: "json",
			success: function (data){
				$.each(data , function(index, val) {
					$('#destinations\\['+ val.co_code +'\\]').prop("checked", true);
				});
			}
		});
	});
	
	$(".select_all_de").click(function(){
		$(this).parent().parent().find("input[type=checkbox]:enabled").prop("checked", true);

		$("input[type=checkbox]").trigger("change");

		return false;
	});

	$(".deselect_all_de").click(function(){
		$(this).parent().parent().find("input[type=checkbox]:enabled").prop("checked", false);

		$("input[type=checkbox]").trigger("change");

		return false;
	});
	
	$(".select_all_or").click(function(){
		$(this).parent().parent().find("input[type=checkbox]:enabled").prop("checked", true);

		$("input[type=checkbox]").trigger("change");

		return false;
	});

	$(".deselect_all_or").click(function(){
		$(this).parent().parent().find("input[type=checkbox]:enabled").prop("checked", false);

		$("input[type=checkbox]").trigger("change");

		return false;
	});
});