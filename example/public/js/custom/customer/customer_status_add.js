$(function(){
	var zero = [4,5,6,7];

	$("#source").hide();
	$("#lifetime").hide();
	$("#previous_leads").hide();
	$("#previous_capping").hide();
	$("#previous_price").hide();
	$("#reason").hide();

	$("select[name=status]").change(function(){
		if($(this).val() == 1 || $(this).val() == 3){
			$("#source").slideDown();
		}
		else{
			$("#source").slideUp();
		}
		//this?
		if($(this).val() == 8){
			$("#lifetime").slideDown();
		}
		else{
			$("#lifetime").slideUp();
		}

		if($(this).val() == 2 || $(this).val() == 9){
			$("#previous_leads").slideDown();
			$("#previous_capping").slideDown();
			$("#previous_price").slideDown();
		}else{
			$("#previous_leads").slideUp();
			$("#previous_capping").slideUp();
			$("#previous_price").slideUp();
		}

		if($(this).val() == 4 || $(this).val() == 5 || $(this).val() == 8){
		    if ($(this).val() == 8) {
                $("#cancellation_reason").slideDown();
                $("#reason").slideUp();
            }
		    else {
                $("#reason").slideDown();
                $("#cancellation_reason").slideUp();
            }
		}else{
			$("#cancellation_reason").slideUp();
			$("#reason").slideUp();
		}
	}).trigger('change');


	$("select[name=status]").change(function(){
		if($.inArray(parseInt($("select[name=status]").val()), zero) != -1){
			$("#turnover_netto input[name=turnover_netto]").val(0).attr("disabled", "disabled");
		}
		else{
			if($("#turnover_netto input[name=turnover_netto]").val() == "0")
			{
				$("#turnover_netto input[name=turnover_netto]").val("");
			}

			$("#turnover_netto input[name=turnover_netto]").removeAttr("disabled");
		}
	}).trigger('change');
});
