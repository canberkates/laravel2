//Time to wait before checking
var CHECK = 10;
var FIRST_CHECK = 5;
var status = 0;
var wait_for_finish = false;

$(document).ready(function(){
    function check_reports($callback){
        
        jQuery.ajax({
            url: "/ajax/reportprogress",
            method: 'get',
            data: {repr_id: $("#report_progress_id").val(), report_id: $("#report_id").val()},
            success: function(data){
                data = JSON.parse(data);
                console.log(data);
                
                if("single-report" in data){
                    $("#single-report-html").html(data["single-report"]);
                }
                
                if ($("#report_status").val() != 3)
                {
                    var string = data['single-report'];
                    
                    if(string.includes("Finished"))
                    {
                        location.reload(true);
                    }
                }
                
                if(typeof $callback === 'function'){
                    $callback(false);
                }
            },
            error: function(){
                
                if(typeof $callback === 'function'){
                    $callback(true);
                }
            }
        });
        
        /*$.ajax(
            
            {
                "data": {repr_id: $("#report_progress_id").val(), report_id: $("#report_id").val()},
                "dataType": "json",
                type: "GET",
                url: url('/ajax/reportprogress')
            }
        ).done(function(data){
            console.log(data);
            /*if ("single-report-deleted" in data) {
                window.location = window.location.origin + window.location.pathname;
                return;
            }
            if ("reports" in data) {
                $("#report-progress").replaceWith(data["reports"]);
                $("button, .button, input[type=submit]").button();
            }
            if ("single-report" in data) {
                $("#single-report-progress").html(data["single-report"]);

                /*if (wait_for_finish) {
                    status = $("#single-report-status").data("status");
                    if (status == 3) {
                        location.reload(true);
                    }
                }
            }
            refresh_in(CHECK);
        }).fail(function(){
            refresh_in(CHECK * 3);
        });*/
    }
    
    if(typeof (repr_id) !== "undefined"){
        status = $("#single-report-status").data("status");
        if(status < 3){
            wait_for_finish = true;
        }
    }
    
    function refresh_in($hasError){
        var time = 10;
        
        if($hasError){
            time = 30;
            console.log('error');
        }
        
        //Instant to 0
        $('#progressbar').css('width', '0');
        
        //Animate to 100
        $("#progressbar").animate({
            width: "100%"
        }, (time * 1000), "linear", function(){
            
            //Check if report finished
            check_reports(function($hasError){
                refresh_in($hasError);
            });
        });
    }
    
    //Start checking
     check_reports(function($hasError){
        refresh_in($hasError);
    });
});
