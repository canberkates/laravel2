$(function(){
	$(".select_all_de").click(function(){
		$(this).parent().parent().find("input[type=checkbox]:enabled").prop("checked", true);

		$("input[type=checkbox]").trigger("change");

		return false;
	});

	$(".deselect_all_de").click(function(){
		$(this).parent().parent().find("input[type=checkbox]:enabled").prop("checked", false);

		$("input[type=checkbox]").trigger("change");

		return false;
	});
	
	$(".select_all_or").click(function(){
		$(this).parent().parent().find("input[type=checkbox]:enabled").prop("checked", true);

		$("input[type=checkbox]").trigger("change");

		return false;
	});

	$(".deselect_all_or").click(function(){
		$(this).parent().parent().find("input[type=checkbox]:enabled").prop("checked", false);

		$("input[type=checkbox]").trigger("change");

		return false;
	});
});