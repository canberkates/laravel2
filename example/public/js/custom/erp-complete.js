// This file contains general functions which can be used in all the other specific categorized files

function trailingslashit( $uri ) {

    return $uri.replace( /\/?$/, '/' );
}


$( document ).ready( function() {

    var $body = $( 'body' );

    $body.on( 'click', '#erp-sidebar-close', function() {

        $( '#erp-sidebar-open' ).removeClass( 'd-none' );
    } );

    $body.on( 'click', '#erp-sidebar-open', function() {

        $( '#erp-sidebar-open' ).addClass( 'd-none' );
    });
});


// Get tab from storage
var $activeTab = localStorage.getItem( 'activeTab' );

// First look for hash
if( location.hash ) {
    $( 'a[href="' + location.hash + '"' ).tab( 'show' );
}
else if( $activeTab ) {
    $( 'a[href="' + $activeTab + '"' ).tab( 'show' );
}

$( 'body' ).on( 'click', '.nav-tabs .nav-link', function(e) {

    e.preventDefault();

    // Get tab level
    var $tabLevel = $( this ).parents( '.nav-tabs' ).data( 'tab-control' );

    // Get tab name
    var $tabName = this.getAttribute( 'href' );

    // If history push state is supported
    if( history.pushState ) {

        // Set push state to tab name
        history.pushState(null, null, $tabName );

    }
    else {

        location.hash = $tabName;
    }
    localStorage.setItem( 'activeTab', $tabName );
    $( this ).tab( 'show' );
    return false;
});
$( window ).on( 'popstate', function () {
    let $anchor = location.hash ||
        $( 'a[data-toggle="tab"]' ).first().attr( 'href' );
    $( 'a[href="' + $anchor + '"]' ).tab( 'show' );
});

$( document ).ready( function() {

	$( '#profile_edit_submit' ).click( function() {

	    // Get values
		var $currentPassword = $( '#current_password' ).val();
		var $newPassword = $( '#new_password' ).val();
		var $newPasswordConfirm = $( '#new_password_confirm' ).val();
		var $userId = $( '#us_id' ).val();

		jQuery.ajax({
			url: "/ajax/profile/changepassword",
			method: 'get',
            data: {
                us_id: $userId,
                current_password: $currentPassword,
                new_password: $newPassword,
                new_password_confirm: $newPasswordConfirm
            },
			success: function( $result ) {

			    // Store elements
				var $messageEl = $("#change_password_errordiv");

				// If changing went successfully
				if( $result.success ) {

				    // Slide up submit button and div
				    $( '#profile_edit_submit' ).slideUp();
                    $( '#change_password_div' ).slideUp();

                    $messageEl.removeClass( 'bg-danger' ).addClass( 'bg-success' ).html( $result.message ).show();

                    setTimeout(function () {
                        $("#logout-form").submit();
                    }, 2000);
                }
				else {

                    $messageEl.addClass( 'bg-danger' ).html( $result.message ).show();
                }
			}
		});
	});
});

$( document ).ready( function() {

    // Store body
    var $bodyEl = $( 'body' );

    // On click reset select
    $bodyEl.on( 'click', '.reset-select', function(e) {

        // Stop submitting
        e.preventDefault();

        // Get target
        var $target = $( $( this ).data( 'target' ) );

        // If the value of the target is not empty
        if( $target.val() !== '' ) {

            // Reset value and trigger change
            $target.val( '' ).trigger( 'change' );
        }
    });

    $bodyEl.on( 'click', '.reset-input', function(e) {

        // Stop submitting
        e.preventDefault();

        // Get target
        var $target = $( $( this ).data( 'target' ) );

        // If the value of the target is not empty
        if( $target.val() !== '' ) {

            // Reset value
            $target.val( '' );
        }
    });
});

$( document ).ready( function() {

    $( '#header-search-form' ).submit( function(e) {

        // Get input
        var $input = $( '#page-header-search-input' );

        // Get value
        var $value = $input.val();

        // If the value is not numeric and the length is less then 3
        if( ! $.isNumeric( $value ) && $value.length < 3 ) {

            // Stop submit
            e.preventDefault();

            // Add shake boi
            $input.addClass( 'animated shake' );

            // Timeout bois
            setTimeout( function() {

                // Remove that shake boi
                $input.removeClass( 'animated shake' );

                // Wait 3 seconds
            }, 3000);
        }
    });
});

function getTranslation( $value, $callback = '', $callbackError = '' ) {

    $.ajax( {

        type: 'GET',
        url: trailingslashit( $ajaxUrl ) + 'gettranslation/',
        data: {
            value: $value,
        },
        success: function( $result ) {

            if( typeof $callback === 'function' ) {

                $callback( $result );
            }
        },
        error: function( jqXHR, textStatus, errorThrown ) {

            if( typeof  $callbackError === 'function' ) {

                $callbackError( jqXHR, textStatus, errorThrown );
            }
        }
    } );
}

function getMovingSizes( $requestType, $callback = '', $callbackError = '' ) {

    jQuery.ajax( {

        type: 'GET',
        url: trailingslashit( $ajaxUrl ) + 'getmovingsizes/',
        data: {
            request_type: $requestType
        },
        success: function( $result ) {

            if( typeof $callback === 'function' ) {

                $callback( $result );
            }
        },
        error: function( jqXHR, textStatus, errorThrown ) {

            if( typeof  $callbackError === 'function' ) {

                $callbackError( jqXHR, textStatus, errorThrown );
            }
        }
    } );
}

function convertVolume( $volume, $type ) {

    if( $type === 'ft3' ) {

        return ( $volume * 35.3146667 ).toFixed( 1 );
    }
    else {

        return ( $volume * 0.0283168466 ).toFixed( 1 );
    }
}

function parseRegions( $options = {}, $callback = '' ) {
    // Needed options direction, destinationType,

    // Store elements
    var $countryEl = $( 'select[name="country_' + $options.direction + '"]' );
    var $regionEl = $( 'select[name="region_' + $options.direction + '"]' );
    var $destinationType = $options.destinationType;

    // Clear options from region select
    $regionEl.empty();

    $regions_found = false;


    // Get the regions
    $.ajax( {

        type: 'GET',
        async: false,
        url: trailingslashit( $ajaxUrl ) + 'getregions/',
        data: {
            country: $countryEl.val(),
            destination_type: $destinationType,
        },
        success: function( $result ) {


           if($result.length > 7){
               $regions_found = true;
           }

           console.log($result.length);
           console.log($result);
            // Get opts from result
            $result = $.parseJSON ( $result );

            // Set default select options
            var $selectOptions = '<option value=""></option>';

            // Loop result
            $.each( $result, function( $greaterRegion, $region ) {

                // If there are no optgroups
                if( typeof $region === 'string' ) {

                    // Add to options
                    $selectOptions += '<option value="' + $region + '">' + $greaterRegion + '</option>';
                }
                else {
                    // Open optgroup
                    $selectOptions += '<optgroup label="' + $greaterRegion + '">';

                    // Loop sub regions
                    $.each( $region, function( $regionVal, $regionName ) {
                        if($regionName == ""){
                            $regionName = $greaterRegion;
                        }
                        // Add to options
                        $selectOptions += '<option value="' + $regionVal + '">' + $regionName + '</option>';
                    });

                    // Close optgroup
                    $selectOptions += '</optgroup>';
                }
            });

            // Set value and trigger
            $regionEl.append( $selectOptions ).val( ( 'initVal' in $options ) ? $options.initVal : null ).trigger( 'change' );

            // Do some callback
            if( typeof $callback === 'function' ) {

                $callback( $regionEl );
            }
        }
    });

    if(!$regions_found){
        $.ajax( {

            type: 'GET',
            url: trailingslashit( $ajaxUrl ) + 'getregions/',
            data: {
                country: $countryEl.val(),
                destination_type: 1
            },
            success: function( $result ) {

                if($result.length > 7){
                    $regions_found = true;
                }

                // Get opts from result
                $result = $.parseJSON ( $result );

                // Set default select options
                var $selectOptions = '<option value=""></option>';



                // Loop result
                $.each( $result, function( $greaterRegion, $region ) {

                    // If there are no optgroups
                    if( typeof $region === 'string' ) {

                        // Add to options
                        $selectOptions += '<option value="' + $region + '">' + $greaterRegion + '</option>';
                    }
                    else {
                        // Open optgroup
                        $selectOptions += '<optgroup label="' + $greaterRegion + '">';

                        // Loop sub regions
                        $.each( $region, function( $regionVal, $regionName ) {

                            // Add to options
                            $selectOptions += '<option value="' + $regionVal + '">' + $regionName + '</option>';
                        });

                        // Close optgroup
                        $selectOptions += '</optgroup>';
                    }
                });

                // Set value and trigger
                $regionEl.append( $selectOptions ).val( ( 'initVal' in $options ) ? $options.initVal : null ).trigger( 'change' );

                // Do some callback
                if( typeof $callback === 'function' ) {

                    $callback( $regionEl );
                }
            }
        });
    }

}

function getMap( $direction, $callback = '' ) {

    // Store map
    var $map = $( '.google_maps_image_' + $direction );
    var $hidden_maps = $('input[name=google_maps_link_'+ $direction + '_hidden]')

    // Store map width
    var $mapWidth = Math.round( $map.width() );

    // Do the ajax call
    $.ajax({
        type: 'GET',
        url: trailingslashit( $ajaxUrl ) + 'loadgooglemapsimage/',
        data: {
            'street': $( 'input[name="street_' + $direction + '"]' ).val(),
            'zipcode': $( 'input[name="zipcode_' + $direction + '"]' ).val(),
            'city': $( 'input[name="city_' + $direction + '"]' ).val(),
            'country': $( 'input[name="country_' + $direction + '"]' ).val(),
            'width': $mapWidth,
            'height': 250,
        },
        success: function( $data ){

            $map.html( $data );

            var $link = $( '.google_maps_image_' + $direction + ' a' ).attr("href");

            $hidden_maps.val($link);

            if( typeof $callback === 'function' ) {

                $callback( $data );
            }
        },
    });
}
