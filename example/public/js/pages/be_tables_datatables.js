class pageTablesDatatables {
    /*
     * Init DataTables functionality
     *
     */
    static initDataTables() {

        // The default configuration
        var $config = {

            pageLength: 20,
            lengthMenu: [
                // Values
                [10, 20, 50, -1],
                // Labels
                [10, 20, 50, 'All']
            ],
            "createdRow": function (row, data, index){
                var table = this;

                $(".details_show", row).click(function(){
                    var data = $(this).data("details");

                    if(table.fnIsOpen(row)){
                        table.fnClose(row);
                    }
                    else{
                        table.fnOpen(row, data, "details");

                        $(row).next().find(".tabs").tabs({
                            activate: function(e, ui){
                                var ttInstances = TableTools.fnGetMasters();

                                for(var i in ttInstances){
                                    if(ttInstances[i].fnResizeRequired()){
                                        ttInstances[i].fnResizeButtons();
                                    }
                                }
                            }
                        });

                        $(row).next().find(".table").dataTable({
                            "paginate": false,
                            "sorting": []
                        });
                    }

                    $(row).find(".ui-button-icon-only .ui-icon").toggleClass("ui-icon-circle-plus ui-icon-circle-minus");
                });

                $(".details_load", row).click(function(){

                    if ( table.fnIsOpen(row) ) {
                        table.fnClose( row );
                    } else {
                        $.ajax({
                            type: "GET",
                            url: $(this).data("file"),
                            data: {"id": $(this).data("id")},
                            success: function(data){
                                table.fnOpen(row, JSON.parse(data), "details");

                                $(row).next().find(".table").dataTable({
                                    "paginate": false,
                                    "sorting": []
                                });
                            }
                        });
                    }

                    $(row).find(".ui-button-icon-only .ui-icon").toggleClass("ui-icon-circle-plus ui-icon-circle-minus");
                });
            },
            autoWidth: false,
            buttons: [
                { extend: 'copy', className: 'btn btn-sm btn-primary' },
                { extend: 'excel', className: 'btn btn-sm btn-primary' },
                { extend: 'print', className: 'btn btn-sm btn-primary' }
            ],
            dom: "<'row justify-content-between'<'col-sm-auto'l><'col-sm-auto'B><'col-sm-auto'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
        };

        // Override a few default classes
        jQuery.extend(jQuery.fn.dataTable.ext.classes, {
            sWrapper: "dataTables_wrapper dt-bootstrap4",
            sFilterInput:  "form-control form-control-sm",
            sLengthSelect: "form-control form-control-sm"
        });

        // Override a few defaults
        jQuery.extend(true, jQuery.fn.dataTable.defaults, {
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Search..",
                paginate: {
                    first: '<i class="fa fa-angle-double-left"></i>',
                    previous: '<i class="fa fa-angle-left"></i>',
                    next: '<i class="fa fa-angle-right"></i>',
                    last: '<i class="fa fa-angle-double-right"></i>'
                }
            }
        });

        // Init full DataTable
        jQuery('.js-dataTable-full').dataTable( $config );
    }

    /*
     * Init functionality
     *
     */
    static init() {
        this.initDataTables();
    }
}

// Initialize when page loads
jQuery(() => { pageTablesDatatables.init(); });
