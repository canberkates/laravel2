// Instructions
// Give the form-group the following data attributes:
// data-conditional="{name of other setting}"
// (optional) data-conditional-value="{the value that the other setting must have}"

// Tested with:
// Checkboxes
// Selects
// Text inputs

// initially
checkConditionals( true );

// On form changes
jQuery( 'form' ).change( function() {

    // Check conditionals on any change
    checkConditionals( false );
});

function checkConditionals( $init = false ) {

    // Loop all form groups with a conditional set
    jQuery( '.form-group[data-conditional]' ).each( function() {

        // Set default show on false
        var $show = false;

        // Get target id
        var $targetName = jQuery( this ).data( 'conditional' );

        // Get target
        var $target = jQuery( '[name="' + $targetName + '"]' );

        // Get value
        var $targetVal = $target.val();

        // If data value is set
        var $requiredVal = jQuery( this ).data( 'conditional-value' );

        // If target exists
        if( $target.length > 0 ) {

            // Here we have to check what validation technique we need to apply here

            // If we have a value that it should be
            if( typeof $requiredVal !== 'undefined' && $requiredVal.length > 0 ) {

                // If the required value is the correct value
                if( $requiredVal === $targetVal )
                    // Set show
                    $show = true;
            }
            else {

                // If the checkbox is checked
                if( $target.is(':checked') )
                    $show = true;
            }
        }

        if( $show ) {

            // If the page was just loaded
            if( $init === true ) {

                // Show it instantly
                jQuery( this ).show();

            }else{

                // Show it smoothly
                jQuery( this ).stop().slideDown();
            }

        }else {

            // If the page was just loaded
            if( $init === true ) {

                // Hide it instantly
                jQuery( this ).hide();

            }else{

                // Hide it smoothly
                jQuery( this ).stop().slideUp();
            }
        }
    });
}