function confirmDelete( $url, $method, $data, $callback = '', $button_text = "Yes, delete it!") {
    swal({
        title: "Are you sure?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: $button_text,
        preConfirm: function() {
            return new Promise(function(resolve) {
                jQuery.ajax({
                    url: $url,
                    method: $method,
                    data: $data,
                    success: function( $result) {
                        swal("Done!", "It was successfully deleted!", "success");
                        console.log( $result);

                        if( typeof $callback == 'function' ) {

                            $callback.call( $result );
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail

                        swal("Error deleting!", errorThrown, "error");
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    }
                });
            });
        }
    });
}

function confirmClick( $url, $options = {} ) {

    // Bob's default settings:
    var $defaults = {
        title: 'Are you sure?',
        text: '',
        confirmButtonColor: false,
        confirmButtonText: 'Yes',
        showCancelButton: true,
        type: 'info',
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-primary mr-1',
        cancelButtonClass: 'btn btn-secondary ml-1',
        preConfirm: function() {
            return new Promise( function() {
                window.location.href = $url;
            });
        }
    };

    // Get settings
    var $settings = $.extend( {}, $defaults, $options );

    // Execute sweetalert
    swal( $settings );
}
