jQuery(function () {

    let $datepickers = jQuery('.drp.drp-default');

    $datepickers.each(function () {

        // noinspection JSUnresolvedFunction,ES6ModulesDependencies
        jQuery(this).daterangepicker({

            showDropdowns: true,
            minYear: 2000,
            autoApply: true,
            alwaysShowCalendars: true,
            showCustomRangeLabel: false,
            autoUpdateInput: false,
            locale: {
                format: 'YYYY/MM/DD'
            },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'This Week': [moment().day(1), moment().day(7)],
                'This Month': [moment().startOf("month"), moment().endOf("month")],
                'This Year': [moment().startOf('year'), moment().endOf('year')],
                'Previous Week': [moment().day(-6), moment().day(0)],
                'Previous Month': [moment().subtract(1, "months").startOf("month"),  moment().subtract(1, "months").endOf("month")],
                'Previous Year': [moment().startOf('year').subtract(1, 'year'), moment().endOf('year').subtract(1, 'year')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 14 Days': [moment().subtract(13, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'Last 90 Days': [moment().subtract(89, 'days'), moment()],
                'Last 180 Days': [moment().subtract(179, 'days'), moment()],
                'Last 365 Days': [moment().subtract(365, 'days'), moment()],
                'All':[moment('2000-01-01'), moment()]
            }
        });
    });

    $datepickers.on('apply.daterangepicker', function (ev, picker) {
        jQuery(this).val(picker.startDate.format('YYYY/MM/DD') + ' - ' + picker.endDate.format('YYYY/MM/DD'));
    });

    $datepickers.on('cancel.daterangepicker', function (ev, picker) {
        jQuery(this).val('');
    });
});
