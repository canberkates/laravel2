@extends('layouts.backend')

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Add a Document to {{$customer->cu_company_name_business}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../../">@if ($customer->cu_type == 1) {{"Customers"}} @elseif($customer->cu_type == 6) {{"Resellers"}} @elseif($customer->cu_type == 2) {{"Service Providers"}}@else {{"Affiliate Partners"}} @endif</a></li>
                        <li class="breadcrumb-item"><a href="../edit">{{$customer->cu_company_name_business}}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Add document</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="block block-rounded block-bordered">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">General</h3>
                    </div>
                    <div class="block-content">
                        @if($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        {{$error}}<br>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form enctype="multipart/form-data" class="mb-5" method="post" action="{{action('CustomerDocumentController@store')}}">
                            @csrf
                            <input name="_method" type="hidden" value="post">
                            <input name="customer_id" type="hidden" value="{{$customer->cu_id}}">
                            <input name="cu_co_code" type="hidden" value="{{$customer->cu_co_code}}">

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="uploaded_file">File:</label>
                                <div class="col-sm-7">
                                    <input type="file" name="uploaded_file">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="type">Type:</label>
                                <div class="col-sm-7">
                                    <select id="select_type" type="text" class="form-control" name="type">
                                        <option></option>
                                        @foreach($documenttypes as $id => $type)
                                            <option value="{{$id}}">{{$type}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div id="obligations_div" style="display: none;">
                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="obligation">Name of Permit/License:</label>
                                    <div class="col-sm-7">
                                        @if (count($obligations) > 0)
                                            <select id="select_type" type="text" class="form-control" name="obligation">
                                                <option value="none">None of them of this list</option>
                                                @foreach($obligations as $obli)
                                                    <option value="{{$obli->cuob_id}}">{{$obli->cuob_name}}</option>
                                                @endforeach
                                            </select>
                                        @else
                                            There are no Permits/Licenses for this Country
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div id="obligations_number_div" style="display: none;">
                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="number">Number:</label>
                                    <div class="col-sm-7">
                                        <input class="form-control" type="text" name="number"/>
                                    </div>
                                </div>
                            </div>
                            <div id="insurances_div" style="display: none;">
                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="insurance">Name of Insurance:</label>
                                    <div class="col-sm-7">
                                        @if (count($insurances) > 0)
                                        <select id="select_type" type="text" class="form-control" name="insurance">
                                            <option value="none">None of them of this list</option>
                                            @foreach($insurances as $insurance)
                                                <option value="{{$insurance->cuin_id}}">{{$insurance->cuin_name}}</option>
                                            @endforeach
                                        </select>
                                        @else
                                            There are no Permits/Licenses for this Country
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="description">Description:</label>
                                <div class="col-sm-7">
                                    <textarea rows="4" type="text" class="form-control" name="description"></textarea>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Add document</button>
                                    <a class="btn btn-primary" href="../edit">
                                        Back
                                    </a>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
@push( 'scripts' )

    <script>
        jQuery(document).ready(function(){
            jQuery( document ).on( 'change', 'select[name=type]', function(e) {
                if ($("select[name=type]").val() == 6)
                {
                    $("div #obligations_div").css("display", "block");
                }
                else {
                    $("div #obligations_div").css("display", "none");
                }

                if ($("select[name=type]").val() == 4)
                {
                    $("div #insurances_div").css("display", "block");
                }
                else {
                    $("div #insurances_div").css("display", "none");
                }
            });

            jQuery( document ).on( 'change', 'select[name=obligation]', function(e) {

                if ($("input[name=cu_co_code]").val() == "US")
                {
                    $("div #obligations_number_div").css("display", "block");
                }
                else {
                    $("div #obligations_number_div").css("display", "none")
                }

            });
        });
    </script>

@endpush



