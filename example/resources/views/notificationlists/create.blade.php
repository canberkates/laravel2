@extends('layouts.backend')

@include('scripts.select2')

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Add a List</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../../">Admin</a></li>
                        <li class="breadcrumb-item"><a href="/admin/notification_lists">Notification lists</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Create</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="block block-rounded block-bordered">
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    {{$error}}<br>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="block-header block-header-default">
                        <h3 class="block-title">General</h3>
                    </div>
                    <div class="block-content">
                        <form class="mb-5" method="post" action="{{action('NotificationListsController@store')}}">
                            @csrf
                            <input name="_method" type="hidden" value="post">

                            <div class="row">
                                <div class="col-md-6">

                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="noli_name">Name:</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="noli_name"
                                                   value="{{Request::old('name')}}">
                                        </div>
                                    </div>

                                    @foreach($users as $user)
                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label ml-5"
                                                   for="users[{{$user->us_id}}]">@if($loop->first) {{"Users"}} @endif</label>
                                            <div class="col-sm-6">
                                                <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                                    <input type="checkbox" class="custom-control-input"
                                                           id="users[{{$user->us_id}}]"
                                                           @if(isset($selected_users) && in_array($user->us_id, $selected_users)) {{"checked=''"}} @endif
                                                           name="users[{{$user->us_id}}]">
                                                    <label class="custom-control-label"
                                                           for="users[{{$user->us_id}}]">{{$user->us_name}}</label>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach

                                </div>
                            </div>

                            <h2 class="content-heading pt-0"></h2>

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Add</button>
                                    <a class="btn btn-primary" href="../notification_lists">
                                        Back
                                    </a>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
