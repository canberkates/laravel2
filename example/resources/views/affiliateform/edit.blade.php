@extends('layouts.backend')

@include('scripts.forms')
@include('scripts.select2')

@pushonce( 'scripts:customer-office-create' )
    <script src="{{ asset('/js/custom/customer/customer_office_create.js') }}"></script>
@endpushonce

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Edit form of {{$affiliateform->customer->cu_company_name_business}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../../../">Affiliate Partners</a></li>
                        <li class="breadcrumb-item"><a href="../../edit">{{$affiliateform->customer->cu_company_name_business}}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Form edit</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="block block-rounded block-bordered">
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    {{$error}}<br>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Information</h3>
                    </div>
                    <div class="block-content">
                        <form class="mb-5" method="post" action="{{action('AffiliatePartnerFormController@update', $affiliateform->afpafo_id)}}">
                            @csrf
                            <input name="_method" type="hidden" value="PATCH">
                            <input name="customer_id" type="hidden" value="{{$affiliateform->customer->cu_id}}">

                            <h2 class="content-heading pt-0">General</h2>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="street_1">Name:</label>
                                        <div class="col-sm-7">
                                            <input class="form-control" type="text" name="name" value="{{$affiliateform->afpafo_name}}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="portal">Portal:</label>
                                        <div class="col-sm-7">
                                            <select type="text" class="form-control"
                                                    name="portal">
                                                <option value=""></option>
                                                @foreach($portals as $portal)
                                                    <option value="{{$portal->po_id}}" {{ ($affiliateform->afpafo_po_id == $portal->po_id) ? 'selected':'' }}>{{$portal->po_portal.' '.((!empty($portal->country->co_en) ? "(".$portal->country->co_en.")" : ""))}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h2 class="content-heading pt-0">Form</h2>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="form_type">Form type:</label>
                                        <div class="col-sm-7">
                                            <select type="text" class="form-control"
                                                    name="form_type">
                                                <option value=""></option>
                                                @foreach($formtypes as $id => $ft)
                                                    <option value="{{$id}}" {{ ($affiliateform->afpafo_form_type == $id) ? 'selected':'' }}>{{$ft}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="title_and_subtitle" style="display: none;">

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-4 col-form-label" for="disable_ip_check">Form title:</label>
                                            <div class="col-sm-7">
                                                <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                                    <textarea rows="3" type="text" class="form-control" name="form_title">{{$mover_form->mofose_title}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-4 col-form-label" for="disable_ip_check">Form subtitle:</label>
                                            <div class="col-sm-7">
                                                <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                                    <textarea rows="3" type="text" class="form-control" name="form_subtitle">{{$mover_form->mofose_subtext}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-4 col-form-label" for="disable_ip_check">Color:</label>
                                            <div class="col-sm-7">
                                                <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                                    <input type="text" class="form-control" name="color" value="{{$mover_form->mofose_theme_color}}" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="disable_ip_check">Disable IP check:</label>
                                        <div class="col-sm-7">
                                            <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                                <input type="checkbox" class="custom-control-input"
                                                       id="disable_ip_check"
                                                       name="disable_ip_check" {{($affiliateform->afpafo_disable_ip_check == 1 ? "checked=''" : "")}}>
                                                <label class="custom-control-label"
                                                       for="disable_ip_check"></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="language">Language:</label>
                                        <div class="col-sm-7">
                                            <select type="text" class="form-control"
                                                    name="language">
                                                <option value=""></option>
                                                @foreach($languages as $lang)
                                                    @if ($lang->la_code == "RU" || $lang->la_code == "PL" || $lang->la_code == "PT")
                                                        @continue
                                                    @endif
                                                    <option value="{{$lang->la_code}}" {{ ($affiliateform->afpafo_la_code == $lang->la_code) ? 'selected':'' }}>{{$lang->la_language}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <small>Note: Conversion Tools are only available in English, French and German.</small>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="volume_type">Volume type:</label>
                                        <div class="col-sm-7">
                                            <select type="text" class="form-control"
                                                    name="volume_type">
                                                <option value=""></option>
                                                @foreach($volumetypes as $id => $vt)
                                                    <option value="{{$id}}" {{ ($affiliateform->afpafo_volume_type == $id) ? 'selected':'' }}>{{$vt}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                             <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="hide_pre_form">Hide pre form:</label>
                                        <div class="col-sm-7">
                                            <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                                <input type="checkbox" class="custom-control-input"
                                                       id="hide_pre_form"
                                                       name="hide_pre_form" {{($affiliateform->afpafo_hide_pre_form == 1 ? "checked=''" : "")}}>
                                                <label class="custom-control-label"
                                                       for="hide_pre_form"></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                             <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="hide_questions">Hide questions:</label>
                                        <div class="col-sm-7">
                                            <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                                <input type="checkbox" class="custom-control-input"
                                                       id="hide_questions"
                                                       name="hide_questions" {{($affiliateform->afpafo_hide_questions == 1 ? "checked=''" : "")}}>
                                                <label class="custom-control-label"
                                                       for="hide_questions"></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h2 class="content-heading pt-0">Custom CSS</h2>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="custom_css">Custom CSS:</label>
                                        <div class="col-sm-7">
                                            <textarea rows="5" type="text" class="form-control" name="custom_css">{{$affiliateform->afpafo_custom_css}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h2 class="content-heading pt-0">Google</h2>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="analytics_ua_code">Analytics UA code:</label>
                                        <div class="col-sm-7">
                                            <input placeholder="UA-XXXXXXXX-X" class="form-control" type="text" name="analytics_ua_code" value="{{$affiliateform->afpafo_analytics_ua_code}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="conversion_code">Conversion code:</label>
                                        <div class="col-sm-7">
                                            <textarea rows="5" type="text" class="form-control" name="conversion_code">{{$affiliateform->afpafo_conversion_code}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h2 class="content-heading pt-0">Specific tracking</h2>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="tracking_clientid">Use client ID:</label>
                                        <div class="col-sm-7">
                                            <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                                <input type="checkbox" class="custom-control-input" id="tracking_clientid"
                                                       name="tracking_clientid" {{($affiliateform->afpafo_tracking_clientid == 1 ? "checked=''" : "")}}>
                                                <label class="custom-control-label" for="tracking_clientid"></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row" data-conditional="tracking_clientid">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="tracking_domains">Domain:</label>
                                        <div class="col-sm-7">
                                            <input type="text" id="tracking_domains" class="form-control mb-1" name="tracking_domains" value="{{$affiliateform->afpafo_tracking_domains}}"/>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h2 class="content-heading pt-0"></h2>
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Update form</button>
                                    <a class="btn btn-primary" href="../../edit">
                                        Back
                                    </a>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push( 'scripts' )
    <script type="text/javascript">
        $(document).ready(function() {
            $('select[name=form_type]').on('change', function() {
                if(this.value == 4){
                    $("#title_and_subtitle").show();
                }else{
                    $("#title_and_subtitle").hide();
                }
            });
            $('select[name=form_type]').change();
        });
    </script>
@endpush


