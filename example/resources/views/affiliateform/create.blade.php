@extends('layouts.backend')

@include('scripts.forms')
@include('scripts.select2')

@pushonce( 'scripts:customer-office-create' )
    <script src="{{ asset('/js/custom/customer/customer_office_create.js') }}"></script>
@endpushonce

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Add a Form to {{$customer->cu_company_name_business}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../../">Affiliate Partners</a></li>
                        <li class="breadcrumb-item"><a href="../edit">{{$customer->cu_company_name_business}}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Add form</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="block block-rounded block-bordered">
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    {{$error}}<br>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Information</h3>
                    </div>
                    <div class="block-content">
                        <form class="mb-5" method="post" action="{{action('AffiliatePartnerFormController@store')}}">
                            @csrf
                            <input name="_method" type="hidden" value="post">
                            <input name="customer_id" type="hidden" value="{{$customer->cu_id}}">

                            <h2 class="content-heading pt-0">General</h2>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="street_1">Name:</label>
                                        <div class="col-sm-7">
                                            <input class="form-control" type="text" name="name" value="{{Request::old('name')}}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="portal">Portal:</label>
                                        <div class="col-sm-7">
                                            <select type="text" class="js-select2 form-control"
                                                    name="portal">
                                                <option value=""></option>
                                                @foreach($portals as $portal)
                                                    <option value="{{$portal->po_id}}" {{ (Request::old('portal') == $portal->po_id) ? 'selected':'' }}>{{$portal->po_portal.' '.((!empty($portal->country->co_en) ? "(".$portal->country->co_en.")" : ""))}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h2 class="content-heading pt-0">Form</h2>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="form_type">Form type:</label>
                                        <div class="col-sm-7">
                                            <select type="text" class="form-control"
                                                    name="form_type">
                                                <option value=""></option>
                                                @foreach($formtypes as $id => $ft)
                                                    <option value="{{$id}}" {{ (Request::old('form_type') == $id) ? 'selected':'' }}>{{$ft}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="title_and_subtitle" style="display: none;">

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-4 col-form-label" for="disable_ip_check">Form title:</label>
                                            <div class="col-sm-7">
                                                <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                                    <textarea rows="3" type="text" class="form-control" name="form_title"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-4 col-form-label" for="disable_ip_check">Form subtitle:</label>
                                            <div class="col-sm-7">
                                                <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                                    <textarea rows="3" type="text" class="form-control" name="form_subtitle"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-4 col-form-label" for="disable_ip_check">Color:</label>
                                            <div class="col-sm-7">
                                                <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                                    <input type="text" class="form-control" name="color" value="#" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="disable_ip_check">Disable IP check:</label>
                                        <div class="col-sm-7">
                                            <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                                <input type="checkbox" class="custom-control-input"
                                                       id="disable_ip_check"
                                                       name="disable_ip_check" {{(Request::old('disable_ip_check') == 1 ? "checked=''" : "")}}>
                                                <label class="custom-control-label"
                                                       for="disable_ip_check"></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="language">Language:</label>
                                        <div class="col-sm-7">
                                            <select type="text" class="form-control"
                                                    name="language">
                                                <option value=""></option>
                                                @foreach($languages as $lang)
                                                    @if ($lang->la_code == "RU" || $lang->la_code == "PL" || $lang->la_code == "PT")
                                                        @continue
                                                    @endif
                                                    <option value="{{$lang->la_code}}" {{ (Request::old('language') == $lang->la_code) ? 'selected':'' }}>{{$lang->la_language}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>
                                </div>
                                <small>Note: Conversion Tools are only available in English, French and German.</small>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="volume_type">Volume type:</label>
                                        <div class="col-sm-7">
                                            <select type="text" class="form-control"
                                                    name="volume_type">
                                                <option value=""></option>
                                                @foreach($volumetypes as $id => $vt)
                                                    <option value="{{$id}}" {{ (Request::old('volume_type') == $id) ? 'selected':'' }}>{{$vt}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                             <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="hide_pre_form">Hide pre form:</label>
                                        <div class="col-sm-7">
                                            <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                                <input type="checkbox" class="custom-control-input"
                                                       id="hide_pre_form"
                                                       name="hide_pre_form" {{(Request::old('hide_pre_form') == 1 ? "checked=''" : "")}}>
                                                <label class="custom-control-label"
                                                       for="hide_pre_form"></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                             <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="hide_questions">Hide questions:</label>
                                        <div class="col-sm-7">
                                            <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                                <input type="checkbox" class="custom-control-input"
                                                       id="hide_questions"
                                                       name="hide_questions" {{(Request::old('hide_questions') == 1 ? "checked=''" : "")}}>
                                                <label class="custom-control-label"
                                                       for="hide_questions"></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h2 class="content-heading pt-0">Custom CSS</h2>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="custom_css">Custom CSS:</label>
                                        <div class="col-sm-7">
                                            <textarea rows="5" type="text" class="form-control" name="custom_css">{{Request::old('custom_css')}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h2 class="content-heading pt-0">Google</h2>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="analytics_ua_code">Analytics UA code:</label>
                                        <div class="col-sm-7">
                                            <input placeholder="UA-XXXXXXXX-X" class="form-control" type="text" name="analytics_ua_code" value="{{Request::old('analytics_ua_code')}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="conversion_code">Conversion code:</label>
                                        <div class="col-sm-7">
                                            <textarea rows="5" type="text" class="form-control" name="conversion_code">{{Request::old('conversion_code')}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h2 class="content-heading pt-0"></h2>
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Add form</button>
                                    <a class="btn btn-primary" href="../edit">
                                        Back
                                    </a>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@push( 'scripts' )
    <script type="text/javascript">
        $(document).ready(function() {
            $('select[name=form_type]').on('change', function() {
                if(this.value == 4){
                    $("#title_and_subtitle").show();
                }else{
                    $("#title_and_subtitle").hide();
                }
            });
            $('select[name=form_type]').change();
        });
    </script>
@endpush


