@extends('layouts.backend')

@include( 'scripts.datatables' )
@include( 'scripts.dialogs' )
@include('scripts.datepicker')


@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Portal Pricse</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">

        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="block block-rounded block-bordered">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">All Prices per Portal!</h3>
                    </div>
                    <form method="post" action="{{action('PortalController@storePortalPrices')}}">
                        @csrf
                        <input name="_method" type="hidden" value="post">
                        <div class="block-content block-bordered">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Portal</label>
                                <label class="col-sm-1 col-form-label">Currency</label>
                                <label class="col-sm-2 col-form-label">Minimum Price</label>
                                <label class="col-sm-2 col-form-label">Maximum Price</label>
                            </div>
                            <div class="col-md-12">
                                @foreach($portals->where("po_destination_type", 1) as $portal)
                                <div class="form-group row">

                                    <label class="col-sm-2 col-form-label">{{$portal->po_portal}}</label>
                                    <label class="col-sm-1 col-form-label">{{$portal->co_en}}</label>
                                    <label class="col-sm-1 col-form-label">{{$portal->po_pacu_code}}</label>
                                    <div class="col-sm-2">
                                        <input autocomplete="off" name="minimum_price_int[{{$portal->po_id}}]"
                                               class="form-control" step=".01"
                                               id="minimum_price_int_{{$portal->po_id}}"
                                               placeholder="Minimum Price" min="0" type="number"
                                               value="{{$portal->po_minimum_price_int}}"/>
                                    </div>
                                    <div class="col-sm-2">
                                        <input autocomplete="off" name="maximum_price_int[{{$portal->po_id}}]"
                                               class="form-control" step=".01"
                                               id="maximum_price_int_{{$portal->po_id}}"
                                               placeholder="Maximum Price" min="0"  type="number"
                                               value="{{$portal->po_maximum_price_int}}"/>
                                    </div>
                                </div>
                            @endforeach
                            </div>
                        </div>
                        <div class="block-content block-bordered">
                            <div class="form-group row" style="border-top: 1px dotted black;">
                                <label class="col-sm-2 col-form-label">Portal</label>
                                <label class="col-sm-1 col-form-label">Country</label>
                                <label class="col-sm-1 col-form-label">Currency</label>
                                <label class="col-sm-2 col-form-label">Minimum Price LOCAL</label>
                                <label class="col-sm-2 col-form-label">Maximum Price LOCAL</label>
                                <label class="col-sm-2 col-form-label">Minimum Price LONG DISTANCE</label>
                                <label class="col-sm-2 col-form-label">Maximum Price LONG DISTANCE</label>
                            </div>
                            <div class="col-md-12">
                                @foreach($portals->where("po_destination_type", 2) as $portal)
                                <div class="form-group row">

                                            <label class="col-sm-2 col-form-label">{{$portal->po_portal}}</label>
                                            <label class="col-sm-1 col-form-label">{{$portal->co_en}}</label>
                                            <label class="col-sm-1 col-form-label">{{$portal->po_pacu_code}}</label>

                                                <div class="col-sm-2">
                                                    <input autocomplete="off" name="minimum_price_nat_local[{{$portal->po_id}}]"
                                                           class="form-control" step=".01"
                                                           id="minimum_price_nat_local_{{$portal->po_id}}"
                                                           placeholder="Minimum Price" type="number"
                                                           value="{{$portal->po_minimum_price_nat_local}}"/>
                                                </div>
                                                <div class="col-sm-2">
                                                    <input autocomplete="off" name="maximum_price_nat_local[{{$portal->po_id}}]"
                                                           class="form-control" step=".01"
                                                           id="maximum_price_nat_local_{{$portal->po_id}}"
                                                           placeholder="Maximum Price" type="number"
                                                           value="{{$portal->po_maximum_price_nat_local}}"/>
                                                </div>
                                                <br>
                                                <div class="col-sm-2">
                                                    <input autocomplete="off" name="minimum_price_nat_long_distance[{{$portal->po_id}}]"
                                                           class="form-control" step=".01"
                                                           id="minimum_price_nat_long_distance_{{$portal->po_id}}"
                                                           placeholder="Minimum Price" type="number"
                                                           value="{{$portal->po_minimum_price_nat_long_distance}}"/>
                                                </div>
                                                <div class="col-sm-2">
                                                    <input autocomplete="off" name="maximum_price_nat_long_distance[{{$portal->po_id}}]"
                                                           class="form-control" step=".01"
                                                           id="maximum_price_nat_long_distance_{{$portal->po_id}}"
                                                           placeholder="Maximum Price" type="number"
                                                           value="{{$portal->po_maximum_price_nat_long_distance}}"/>
                                                </div>


                                        </div>
                            @endforeach
                            </div>

                        </div>
                        <div class="form-group col-md-2">
                            <button type="submit"  name="store" class="btn btn-primary" data-id="{{$customer->cu_id}}">
                                Update Prices!
                            </button>
                        </div>
                    </form>
                </div>


            </div>
        </div>
    </div>

    <!-- END Your Block -->
    <!-- END Page Content -->
@endsection
@push('scripts')
    <script>
        $.datetimepicker.setDateFormatter('moment');
        $('input[name=call_datetime]').datetimepicker({
            step: 10, //will change increments to 15m, default is 1m
            format: 'YYYY-MM-DD HH:mm',
        });

        $("button[name=update]").click(function () {
            $id = $(this).data("id");
            $date = $("#datetime_" + $id).val();
            $user = $("#user_" + $id).val();

            jQuery.ajax({
                url: "ajax/plan_call",
                method: 'get',
                data: {customer: $id, user: $user, date: $date},
                success: function () {
                }
            });

            $(this).text("Call planned 👌")
            $(this).attr("disabled", "disabled");
        });
    </script>
@endpush


