@extends('layouts.backend')

@include( 'scripts.datatables' )
@include( 'scripts.dialogs' )
@include( 'scripts.datepicker' )
@include( 'scripts.select2' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Portals</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Portals</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="block block-rounded block-bordered">
            <div class="block-content ">
                <form method="post" action="{{action('PortalController@filteredindex')}}">
                    @csrf
                    <input name="_method" type="hidden" value="post">
                    <h2 class="content-heading pt-0">Filters</h2>

                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Portal:</label>
                        <div class="col-md-5">
                            <select class="js-select2 form-control" name="portal" data-placeholder="Choose one.." style="width:100%;">
                                <option></option>
                                 @foreach($portals as $portal)
                                    <option @if ($selected_portal == $portal->po_id) selected @endif value="{{$portal->po_id}}">{{$portal->po_portal}} @if($portal->country)({{$portal->country->co_en}}) @endif</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Request type:</label>
                        <div class="col-md-5">
                            <select class="js-select2 form-control" name="requesttype" data-placeholder="Choose one.." style="width:100%;">
                                <option></option>
                                @foreach($requesttypes as $id => $type)
                                    <option @if ($selected_requesttype == $id) selected @endif value="{{$id}}">{{$type}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Status:</label>
                        <div class="col-md-5">
                            <select  class="js-select2 form-control" name="status" data-placeholder="Choose one.." style="width:100%;">
                                <option></option>
                             @foreach($statuses as $id => $type)
                                    <option @if ($selected_status == $id) selected @endif value="{{$id}}">{{$type}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Date:</label>
                        <div class="col-sm-5">
                            <input autocomplete="off" type="text"
                                   class="js-datepicker form-control"
                                   id="example-datepicker3" name="payment_rate"
                                   data-week-start="1" data-autoclose="true"
                                   data-today-highlight="true" data-date-format="yyyy-mm-dd"
                                   placeholder="yyyy-mm-dd"
                                   value="{{$selected_date}}">
                        </div>
                    </div>

                    <h2 class="content-heading pt-0"></h2>

                    <div class="row">
                        <div class="form-group col-md-2">
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- Your Block -->
        <div class="block block-rounded block-bordered">
            <div class="block-content block-content-full">
                <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Portal</th>
                        <th>Description</th>
                        <th>Customer</th>
                        <th>Country</th>
                        <th>Email</th>
                        <th>Request type</th>
                        <th>Status</th>
                        <th>Rate since</th>
                        <th>Payment rate</th>
                        <th>Rate discount</th>
                        <th>Edit</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($customers as $customer)
                            <tr>
                            <td>{{$customer->ktcupo_id}}</td>
                            <td>{{$customer->po_portal}}</td>
                            <td>{{$customer->ktcupo_description}}</td>
                            <td>{{$customer->cu_company_name_business}}</td>
                                @isset($customer->cu_co_code)
                                    <td>{{$countries[$customer->cu_co_code]}}</td>
                                @else
                                    <td>{{$customer->cu_co_code}}</td>
                                @endisset
                            <td>{!! str_replace(",", "<br />", $customer->cu_email)!!}</td>
                            <td>{{$requesttypes[$customer->ktcupo_request_type]}}</td>
                            <td>{{$statuses[$customer->ktcupo_status]}}</td>
                            <td>{{$customer->rate_since}}</td>
                            <td>{{$customer->rate}}</td>
                            <td>{{$customer->rate_discount}}</td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <a class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="left" title="edit" href="{{ url('customers/' . $customer->cu_id . '/customerportal/' .$customer->ktcupo_id. '/edit')}}">
                                            <i class="fa fa-pencil-alt"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Your Block -->
    </div>
    <!-- END Page Content -->
@endsection
