@extends('layouts.backend')

@include('scripts.datepicker')

@section('content')
	<div class="content">
		<div class="row justify-content-center">
			<div class="col-md-12">
				<div class="block block-rounded block-bordered">
					@if($errors->any())
						<div class="alert alert-danger">
							<ul>
								@foreach($errors->all() as $error)
									{{$error}}<br>
								@endforeach
							</ul>
						</div>
					@endif
					<div class="block-header block-header-default">
						<h3 class="block-title">Edit Payment Rate </h3>
					</div>
					<div class="block-content">
						<form class="mb-5" method="post"
						      action="{{action('PaymentRateController@update', $paymentrate->para_id)}}">
							@csrf
							<input name="_method" type="hidden" value="PATCH">
							<input name="ktcupo_id" type="hidden" value="{{$paymentrate->customerportal->ktcupo_id}}">
							<input name="cu_id" type="hidden" value="{{$paymentrate->customerportal->ktcupo_cu_id}}">

							<div class="form-group row">
								<div class="col-sm-1"></div>
								<label class="col-sm-3 col-form-label" for="date">Date:</label>
								<div class="col-sm-3">
									<input autocomplete="off" type="text" class="js-datepicker form-control"
									       id="example-datepicker3" name="date" data-week-start="1"
									       data-autoclose="true" data-today-highlight="true"
									       data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd"
									       value="{{$paymentrate->para_date}}">
								</div>
							</div>

							<div class="form-group row">
								<div class="col-sm-1"></div>
								<label class="col-sm-3 col-form-label" for="rate">Rate:</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" name="rate"
									       value="{{$paymentrate->para_rate}}">
								</div>
							</div>

							<div class="form-group row">
								<div class="col-sm-1"></div>
								<label class="col-sm-3 col-form-label" for="discount_type">Discount type:</label>
								<div class="col-sm-3">
									<select type="text" class="form-control" name="discount_type">
										@foreach($discounttypes as $id => $value)
											<option value="{{$id}}" {{ ($paymentrate->para_discount_type == $id) ? 'selected': '' }}>{{$value}}</option>
										@endforeach
									</select>
								</div>
							</div>

							<div id="discount" class="form-group row">
								<div class="col-sm-1"></div>
								<label class="col-sm-3 col-form-label" for="discount_amount">Amount:</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" name="discount_amount"
									       value="{{$paymentrate->para_discount_rate}}">
								</div>
							</div>

                            @if($destination_type == 2)

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="nat_price_type">Price type:</label>
                                    <div class="col-sm-3">
                                        <select type="text" class="form-control" name="nat_price_type">
                                            @foreach($pricetypes as $id => $value)
                                                <option value="{{$id}}" {{ ($paymentrate->para_nat_type == $id) ? 'selected': '' }}>{{$value}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                            @endif

							<div class="form-group row">
								<div class="col-sm-1"></div>
								<label class="col-sm-3 col-form-label" for="remark">Remark:</label>
								<div class="col-sm-7">
									<textarea type="text" class="form-control"
									          name="remark">{{$paymentrate->para_remark}}</textarea>
								</div>
							</div>

							<h2 class="content-heading pt-0"></h2>


							<div class="row">
								<div class="col-md-1"></div>
								<div class="form-group col-md-8">
									@if(strtotime($paymentrate->para_date) > time())
										<button type="submit" class="btn btn-primary">Update</button>
									@endif
									<a class="btn btn-primary" href="../../edit">
										Back
									</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>


@endsection

@push ('scripts')
	<script>
        if ($("select[name=discount_type]").val() == 0) {
            $("#discount").hide();
        }

        $("select[name=discount_type]").change(function () {
            if ($(this).val() == 0) {
                $("#discount").slideUp();
            } else {
                $("#discount").slideDown();
            }
        });
	</script>
@endpush




