@extends('layouts.backend')

@include('scripts.datepicker')

@section('content')
    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="block block-rounded block-bordered">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Add a Payment Rate to {{$customer->cu_company_name_business}}</h3>
                    </div>
                    <div class="block-content">
                        <form class="mb-5" method="post" action="{{action('PaymentRateController@store')}}">
                                @csrf
                                <input name="_method" type="hidden" value="post">
                                <input name="ktcupo_id" type="hidden" value="{{$customerportal}}">
                                <input name="cu_id" type="hidden" value="{{$customer->cu_id}}">


                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="date">Date:</label>
                                <div class="col-sm-3">
                                    <input autocomplete="off" type="text" class="js-datepicker form-control" id="example-datepicker3" name="date" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="rate">Rate:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="rate"
                                           value="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="discount_type">Discount type:</label>
                                <div class="col-sm-3">
                                    <select type="text" class="form-control" name="discount_type">
                                        @foreach($discounttypes as $id => $value)
                                            <option value="{{$id}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div id="discount" class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="discount_amount">Amount:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="discount_amount"
                                           value="">
                                </div>
                           </div>

                            @if($destination_type == 2)

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="nat_price_type">Price type:</label>
                                <div class="col-sm-3">
                                    <select type="text" class="form-control" name="nat_price_type">
                                        @foreach($pricetypes as $id => $value)
                                            <option value="{{$id}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            @endif

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="remark">Remark:</label>
                                <div class="col-sm-7">
                                    <textarea type="text" class="form-control" name="remark"></textarea>
                                </div>
                            </div>

                            <h2 class="content-heading pt-0"></h2>

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Create</button>
                                    <a class="btn btn-primary" href="../edit">
                                        Back
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push ('scripts')
    <script>
        if($("select[name=discount_type]").val() == 0){
            $("#discount").hide();
        }

        $("select[name=discount_type]").change(function(){
            if($(this).val() == 0){
                $("#discount").slideUp();
            }
            else{
                $("#discount").slideDown();
            }
        });
    </script>
@endpush



