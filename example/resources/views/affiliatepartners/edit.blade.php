@extends('layouts.backend')

@include('scripts.datatables')
@include('scripts.datepicker')
@include('scripts.forms')
@include('scripts.dialogs')
@include('scripts.telinput')
@include('scripts.select2')


@pushonce( 'scripts:customer-edit' )
<script src="{{ asset('/js/custom/customer/customer_edit.js') }}"></script>
@endpushonce

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">AP - {{$customer->cu_company_name_business}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/affiliatepartners">Affiliate Partners</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Edit</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <!-- Block Tabs Alternative Style -->
                <div class="block block-rounded block-bordered">
                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    {{$error}}<br>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <ul class="nav nav-tabs nav-tabs-alt" data-toggle="tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" href="#btabs-alt-static-general">General</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-emails">Emails</a>
                        </li>
                        @can('customers - finance')
                            <li class="nav-item">
                                <a class="nav-link" href="#btabs-alt-static-finance">Finance</a>
                            </li>
                        @endcan
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-contactpersons">Contact persons</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-remark">Remarks</a>
                        </li>
                        @can('customers - documents')
                            <li class="nav-item">
                                <a class="nav-link" href="#btabs-alt-static-documents">Documents</a>
                            </li>
                        @endcan
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-settings">Settings</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-forms">Forms</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-offices">Offices</a>
                        </li>
                        @can('requests')
                            <li class="nav-item">
                                <a class="nav-link" href="#btabs-alt-static-requests">Requests</a>
                            </li>
                        @endcan
                    </ul>
                    <div class="block-content tab-content">
                        <div class="tab-pane active" id="btabs-alt-static-general" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        <div class="block-content">
                                            <form method="post" enctype="multipart/form-data"
                                                  action="{{action('AffiliatePartnersController@update', $customer->cu_id)}}">
                                                @csrf
                                                <input name="_method" type="hidden" value="PATCH">
                                                <input name="form_name" type="hidden" value="General">
                                                <h2 class="content-heading pt-0">General Information</h2>

                                                <div class="row">
                                                    <div class="col-md-6">

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="type">Type of partner:</label>
                                                            <div class="col-sm-7">
                                                                <select disabled type="text" class="form-control"
                                                                        name="type">
                                                                    @foreach($movertypes as $id => $item)
                                                                        <option value="{{$id}}" {{ ($customer->cu_type == $id) ? 'selected':'' }}>{{$item}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="sales_manager">Sales Manager:</label>
                                                            <div class="col-sm-7">
                                                                <select disabled type="text" class="form-control"
                                                                        name="sales_manager">
                                                                    <option value=""></option>
                                                                    @foreach($users as $user)
                                                                        <option value="{{$user->id}}" {{ ($customer->cu_sales_manager == $user->id) ? 'selected':'' }}>{{$user->us_name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="account_manager">Account Manager:</label>
                                                            <div class="col-sm-7">
                                                                <select disabled type="text" class="form-control"
                                                                        name="account_manager">
                                                                    <option value=""></option>
                                                                    @foreach($users as $user)
                                                                        <option value="{{$user->id}}" {{ ($customer->cu_account_manager == $user->id) ? 'selected':'' }}>{{$user->us_name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="debt_manager">Debt Manager:</label>
                                                            <div class="col-sm-7">
                                                                <select disabled type="text" class="form-control"
                                                                        name="debt_manager">
                                                                    <option value=""></option>
                                                                    @foreach($users as $user)
                                                                        <option value="{{$user->id}}" {{ ($customer->cu_debt_manager == $user->id) ? 'selected':'' }}>{{$user->us_name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="la_code">Language:</label>
                                                            <div class="col-sm-7">
                                                                <select type="text" class="form-control"
                                                                        name="la_code">
                                                                    @foreach($languages as $language)
                                                                        <option value="{{$language->la_code}}" {{ ($customer->cu_la_code == $language->la_code) ? 'selected':'' }}>{{$language->la_language}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="description">Description:</label>
                                                            <div class="col-sm-7">
                                                          <textarea rows="6" type="text" class="form-control"
                                                                    name="description">{{$customer->cu_description}}</textarea>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="col-md-6">

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="company_name_legal">Company legal name:</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control"
                                                                       name="company_name_legal"
                                                                       value="{{$customer->cu_company_name_legal}}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="company_name_business">Company business name:</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control"
                                                                       name="company_name_business"
                                                                       value="{{$customer->cu_company_name_business}}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="attn">Primary contact:</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control" name="attn"
                                                                       value="{{$customer->cu_attn}}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="attn_email">Primary contact email:</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control"
                                                                       name="attn_email"
                                                                       value="{{$customer->cu_attn_email}}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="lead_email">Lead email:</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control" name="lead_email"
                                                                       value="{{$customer->cu_email}}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="lead_email">Billing email:</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control" name="billing_email"
                                                                       value="{{$customer->cu_email_bi}}">
                                                            </div>
                                                        </div>


                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="email">Chamber of Commerce:</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control" name="coc"
                                                                       value="{{$customer->cu_coc}}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="logo">Replace logo:</label>
                                                            <div class="col-sm-7">
                                                                <input type="file" name="logo"/>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <div style="margin-left:15px;">
                                                                @if(!empty($customer->cu_logo))
                                                                    <img style="max-width: 300px; max-height: 120px;" src="../../logo.php?path={{env("SHARED_FOLDER")}}uploads/logos/{{$customer->cu_logo}}"/>
                                                                @else
                                                                    <img src="{{ asset('media/logos/logo_placeholder.png') }}"
                                                                        style="max-width: 300px; max-height: 120px;"/>
                                                                @endif
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-sm-1"></div>
                                                            <div class="form-group col-md-8">
                                                                <button type="submit" class="btn btn-primary">Update
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>

                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="block block-rounded block-bordered">
                                                <div class="block-content">
                                                    <form method="post"
                                                          action="{{action('AffiliatePartnersController@update', $customer->cu_id)}}">
                                                        @csrf
                                                        <input name="_method" type="hidden" value="PATCH">
                                                        <input name="form_name" type="hidden" value="Addresses">
                                                        <div class="row">
                                                            <div class="col-md-6">

                                                                <h2 class="content-heading pt-0">Addresses</h2>

                                                                <div class="form-group row">
                                                                    <div class="col-sm-1"></div>
                                                                    <label class="col-sm-3 col-form-label"
                                                                           for="street_1">Company street
                                                                        1:</label>
                                                                    <div class="col-sm-7">
                                                                        <input type="text" class="form-control"
                                                                               name="street_1"
                                                                               value="{{$customer->cu_street_1}}">
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <div class="col-sm-1"></div>
                                                                    <label class="col-sm-3 col-form-label"
                                                                           for="street_2">Company street
                                                                        2:</label>
                                                                    <div class="col-sm-7">
                                                                        <input type="text" class="form-control"
                                                                               name="street_2"
                                                                               value="{{$customer->cu_street_2}}">
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <div class="col-sm-1"></div>
                                                                    <label class="col-sm-3 col-form-label"
                                                                           for="zipcode">Company
                                                                        zipcode:</label>
                                                                    <div class="col-sm-7">
                                                                        <input type="text" class="form-control"
                                                                               name="zipcode"
                                                                               value="{{$customer->cu_zipcode}}">
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <div class="col-sm-1"></div>
                                                                    <label class="col-sm-3 col-form-label"
                                                                           for="city">Company city:</label>
                                                                    <div class="col-sm-7">
                                                                        <input type="text" class="form-control"
                                                                               name="city"
                                                                               value="{{$customer->cu_city}}">
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <div class="col-sm-1"></div>
                                                                    <label class="col-sm-3 col-form-label"
                                                                           for="country">Company
                                                                        country:</label>
                                                                    <div class="col-sm-7">
                                                                        <select type="text" class="js-select2 form-control"
                                                                                name="country">
                                                                            <option value=""></option>
                                                                            @foreach($countries as $country)
                                                                                <option value="{{$country->co_code}}" {{ ($customer->cu_co_code == $country->co_code) ? 'selected':'' }}>{{$country->co_en}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <div class="col-sm-1"></div>
                                                                    <label class="col-sm-3 col-form-label"
                                                                           for="telephone">Company
                                                                        telephone:</label>
                                                                    <div class="col-sm-7">
                                                                        <input type="tel" name="telephone" class="form-control" value="{{$customer->cu_telephone}}">
                                                                        <input type="hidden" id="int_telephone" name="int_telephone" value="">
                                                                    </div>
                                                                </div>


                                                                <div class="form-group row">
                                                                    <div class="col-sm-1"></div>
                                                                    <label class="col-sm-3 col-form-label"
                                                                           for="website">Company
                                                                        website:</label>
                                                                    <div class="col-sm-6">
                                                                        <input type="text" class="form-control"
                                                                               name="website"
                                                                               value="{{$customer->cu_website}}">
                                                                    </div>
                                                                    <div class="col-sm-1">
                                                                        <a target="_blank" href="{{$customer->cu_website}}"><i class="fa fa-external-link-alt"></i></a>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-sm-1"></div>
                                                                    <div class="form-group col-md-8">
                                                                        <button  name="office_update" style="display:{{($customer->cu_use_billing_address ? "none;" : "block;")}}" type="submit" class="btn btn-primary">
                                                                            Update
                                                                        </button>
                                                                    </div>
                                                                </div>

                                                            </div>

                                                            <div class="col-md-6">


                                                                <h2 class="content-heading pt-0">
                                                                    Use Billing Address
                                                                    <div style="padding-top: 5px;padding-left: 10px;"
                                                                         class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                                                        <input type="checkbox"
                                                                               class="custom-control-input"
                                                                               id="use_billing_address"
                                                                               name="use_billing_address" {{($customer->cu_use_billing_address ? "checked" : "")}}>
                                                                        <label class="custom-control-label"
                                                                               for="use_billing_address"></label>
                                                                    </div>
                                                                </h2>

                                                                <div name="show_billing_address" style="display:{{($customer->cu_use_billing_address ? "block;" : "none;")}}">

                                                                    <div class="form-group row">
                                                                        <div class="col-sm-1"></div>
                                                                        <label class="col-sm-3 col-form-label"
                                                                               for="street_1_bi">Company street
                                                                            1:</label>
                                                                        <div class="col-sm-7">
                                                                            <input type="text" class="form-control"
                                                                                   name="street_1_bi"
                                                                                   value="{{$customer->cu_street_1_bi}}">
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <div class="col-sm-1"></div>
                                                                        <label class="col-sm-3 col-form-label"
                                                                               for="street_2_bi">Company street
                                                                            2:</label>
                                                                        <div class="col-sm-7">
                                                                            <input type="text" class="form-control"
                                                                                   name="street_2_bi"
                                                                                   value="{{$customer->cu_street_2_bi}}">
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <div class="col-sm-1"></div>
                                                                        <label class="col-sm-3 col-form-label"
                                                                               for="zipcode_bi">Company
                                                                            zipcode:</label>
                                                                        <div class="col-sm-7">
                                                                            <input type="text" class="form-control"
                                                                                   name="zipcode_bi"
                                                                                   value="{{$customer->cu_zipcode_bi}}">
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <div class="col-sm-1"></div>
                                                                        <label class="col-sm-3 col-form-label"
                                                                               for="city_bi">Company
                                                                            city:</label>
                                                                        <div class="col-sm-7">
                                                                            <input type="text" class="form-control"
                                                                                   name="city_bi"
                                                                                   value="{{$customer->cu_city_bi}}">
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <div class="col-sm-1"></div>
                                                                        <label class="col-sm-3 col-form-label"
                                                                               for="country_bi">Company
                                                                            country:</label>
                                                                        <div class="col-sm-7">
                                                                            <select type="text" class="js-select2 form-control"
                                                                                    name="country_bi">
                                                                                <option value=""></option>
                                                                                @foreach($countries as $country)
                                                                                    <option value="{{$country->co_code}}" {{ ($customer->cu_co_code_bi == $country->co_code) ? 'selected':'' }}>{{$country->co_en}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <div class="col-sm-1"></div>
                                                                        <label class="col-sm-3 col-form-label"
                                                                               for="telephone_bi">Company telephone:</label>
                                                                        <div class="col-sm-7">
                                                                            <input type="tel" name="telephone_bi" class="form-control" value="{{$customer->cu_telephone_bi}}">
                                                                            <input type="hidden" id="int_telephone_bi" name="int_telephone_bi" value="">
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <div class="col-sm-1"></div>
                                                                        <label class="col-sm-3 col-form-label"
                                                                               for="website_bi">Company
                                                                            website:</label>
                                                                        <div class="col-sm-6">
                                                                            <input type="text" class="form-control"
                                                                                   name="website_bi"
                                                                                   value="{{$customer->cu_website_bi}}">
                                                                        </div>
                                                                        @if (!empty($customer->cu_website_bi))
                                                                            <div class="col-sm-1">
                                                                                <a target="_blank" href="{{$customer->cu_website_bi}}"><i class="fa fa-external-link-alt"></i></a>
                                                                            </div>
                                                                        @endif
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="col-sm-1"></div>
                                                                        <div class="form-group col-md-8">
                                                                            <button type="submit"
                                                                                    class="btn btn-primary">
                                                                                Update
                                                                            </button>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="block block-rounded block-bordered">
                                                <div class="block-content">
                                                    <form method="post"
                                                          action="{{action('AffiliatePartnersController@update', $customer->cu_id)}}">
                                                        @csrf
                                                        <input name="_method" type="hidden" value="PATCH">
                                                        <input name="form_name" type="hidden" value="Finance">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <h2 class="content-heading pt-0">Finance</h2>

                                                                <div class="form-group row">
                                                                    <div class="col-sm-1"></div>
                                                                    <label class="col-sm-3 col-form-label"
                                                                           for="finance_debtor_number">Debtor number:</label>
                                                                    <div class="col-sm-7">
                                                                        <input type="text" class="form-control"
                                                                               name="finance_debtor_number"
                                                                               value="{{$customer->cu_debtor_number}}"
                                                                               disabled>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <div class="col-sm-1"></div>
                                                                    <label class="col-sm-3 col-form-label" for="credit_hold">Credit hold:</label>
                                                                    <div class="col-sm-7">
                                                                        <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                                                            <input type="checkbox" class="custom-control-input"
                                                                                   name="finance_credit_hold" {{($customer->cu_credit_hold ? "checked" : "")}}
                                                                                   disabled>
                                                                              <label class="custom-control-label"
                                                                               for="finance_credit_hold"></label>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <div class="col-sm-1"></div>
                                                                    <label class="col-sm-3 col-form-label"
                                                                           for="finance_payment_currency">Payment currency:</label>
                                                                    <div class="col-sm-7">
                                                                        <select type="text" class="form-control"
                                                                                name="finance_payment_currency" disabled>
                                                                            <option value=""></option>
                                                                            @foreach($paymentcurrencies as $pc)
                                                                                <option value="{{$pc->pacu_code}}" {{ ($customer->cu_pacu_code== $pc->pacu_code) ? 'selected':'' }}>{{$pc->pacu_name}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <div class="col-sm-1"></div>
                                                                    <label class="col-sm-3 col-form-label"
                                                                           for="finance_payment_term">Payment term:</label>
                                                                    <div class="col-sm-7">
                                                                        <input type="text" class="form-control"
                                                                               name="finance_payment_term"
                                                                               value="{{$customer->cu_payment_term}}"
                                                                               disabled>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <div class="col-sm-1"></div>
                                                                    <label class="col-sm-3 col-form-label"
                                                                           for="finance_credit_limit">Credit limit:</label>
                                                                    <div class="col-sm-7">
                                                                        <input type="text" class="form-control"
                                                                               name="finance_credit_limit"
                                                                               value="{{$customer->cu_credit_limit}}"
                                                                               disabled>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <div class="col-sm-1"></div>
                                                                    <label class="col-sm-3 col-form-label" for="finance_enforce_credit_limit">Enforce credit limit:</label>
                                                                    <div class="col-sm-7">
                                                                        <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                                                            <input type="checkbox" class="custom-control-input"
                                                                                   name="finance_enforce_credit_limit" {{($customer->cu_enforce_credit_limit ? "checked" : "")}}
                                                                                   disabled>
                                                                              <label class="custom-control-label"
                                                                               for="finance_enforce_credit_limit"></label>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <div class="col-sm-1"></div>
                                                                    <label class="col-sm-3 col-form-label"
                                                                           for="finance_vat_number">VAT number:</label>
                                                                    <div class="col-sm-7">
                                                                        <input type="text" class="form-control"
                                                                               name="finance_vat_number"
                                                                               value="{{$customer->cu_vat_number}}"
                                                                               disabled>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="btabs-alt-static-emails" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        <div class="block-content">
                                            <form method="post"
                                                  action="{{action('AffiliatePartnersController@update', $customer->cu_id)}}">
                                                @csrf
                                                <input name="_method" type="hidden" value="PATCH">
                                                <input name="form_name" type="hidden" value="Emails">

                                                <h2 class="content-heading pt-0">Emails</h2>

                                                <div class="row">
                                                    <div class="col-md-6">

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-4 col-form-label"
                                                                   for="primary_contact_email">Primary contact email:</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control"
                                                                       name="primary_contact_email"
                                                                       disabled value="{{$customer->cu_attn_email}}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-4 col-form-label"
                                                                   for="cu_email">Leads email:</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control"
                                                                       name="cu_email"
                                                                       disabled value="{{$customer->cu_email}}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-4 col-form-label"
                                                                   for="billing_email">Billing email:</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control"
                                                                       name="billing_email"
                                                                       disabled value="{{$customer->cu_email_bi}}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-4 col-form-label"
                                                                   for="review_communication">Review communication:</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control"
                                                                       name="review_communication"
                                                                       disabled value="@if(!empty($moverdata->moda_review_email)){{$moverdata->moda_review_email}}@endif">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-4 col-form-label"
                                                                   for="load_exchange_communication">Load Exchange communication:</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control"
                                                                       name="load_exchange_communication"
                                                                       disabled value="@if(!empty($moverdata->moda_load_exchange_email)){{$moverdata->moda_load_exchange_email}}@endif">
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="btabs-alt-static-finance" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        <div class="block-content">
                                            <form method="post"
                                                  action="{{action('AffiliatePartnersController@update', $customer->cu_id)}}">
                                                @csrf
                                                <input name="_method" type="hidden" value="PATCH">
                                                <input name="form_name" type="hidden" value="Finance">

                                                <h2 class="content-heading pt-0">Finance</h2>

                                                <div class="row">
                                                    <div class="col-md-6">

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="debtor_number">Debtor
                                                                number:</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control"
                                                                       name="debtor_number"
                                                                       disabled value="{{$customer->cu_debtor_number}}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="sales_manager">Sales
                                                                Manager:</label>
                                                            <div class="col-sm-7">
                                                                <select type="text" class="form-control"
                                                                        name="sales_manager">
                                                                    <option value=""></option>
                                                                    @foreach($users as $user)
                                                                        <option value="{{$user->id}}" {{ ($customer->cu_sales_manager == $user->id) ? 'selected':'' }}>{{$user->us_name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="account_manager">Account
                                                                Manager:</label>
                                                            <div class="col-sm-7">
                                                                <select type="text" class="form-control"
                                                                        name="account_manager">
                                                                    <option value=""></option>
                                                                    @foreach($users as $user)
                                                                        <option value="{{$user->id}}" {{ ($customer->cu_account_manager == $user->id) ? 'selected':'' }}>{{$user->us_name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="payment_currency">Payment
                                                                currency:</label>
                                                            <div class="col-sm-7">
                                                                <select type="text" class="form-control"
                                                                        name="payment_currency">
                                                                    @foreach($paymentcurrencies as $pc)
                                                                        <option value="{{$pc->pacu_code}}" {{ ($customer->cu_pacu_code == $pc->pacu_code) ? 'selected':'' }}>{{$pc->pacu_name." (".$pc->pacu_token.")"}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-sm-1"></div>
                                                            <div class="form-group col-md-8">
                                                                <button type="submit" class="btn btn-primary">Update
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="btabs-alt-static-contactpersons" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        <div class="block-header block-header-default">
                                            <h3 class="block-title">
                                                <a href="{{ url('affiliatepartners/' . $customer->cu_id . '/contactperson/create')}}">
                                                    <button class="btn btn-primary">Add a contact person</button>
                                                </a>
                                            </h3>
                                        </div>
                                        <div class="block-content">
                                            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                                <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Full name</th>
                                                    <th>Department</th>
                                                    <th>Role</th>
                                                    <th>Email</th>
                                                    <th>Telephone</th>
                                                    <th>Mobile</th>
                                                    <th>Language</th>
                                                    <th>Login</th>
                                                    <th>Actions</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach ($customer->contactpersons as $contactperson)
                                                    @if ($contactperson->cope_deleted)
                                                        @continue;
                                                    @endif
                                                    <tr>
                                                        <td>{{$contactperson->cope_id}}</td>
                                                        <td>{{$contactperson->cope_full_name}}</td>
                                                        <td>{{$contactpersondepartments[$contactperson->cope_department]}}</td>
                                                        <td>{{$contactpersondepartmentroles[$contactperson->cope_department_role]}}</td>
                                                        <td>{{$contactperson->cope_email}}</td>
                                                        <td>{{$contactperson->cope_telephone}}</td>
                                                        <td>{{$contactperson->cope_mobile}}</td>
                                                        <td>
                                                        @foreach($languages as $language)
                                                            @if($language->la_code == $contactperson->cope_language)
                                                                {{$language->la_language}}
                                                                @break
                                                            @endif
                                                        @endforeach
                                                        </td>

                                                        @if(empty($contactperson->application_user))
                                                            <td><i style="color:red" class="fa fa-times"></i></td>
                                                        @else
                                                            <td><a target="_blank" href="{{$contactperson->login_url}}"><i
                                                                            class="fa fa-key text-primary"></i></a></td>
                                                        @endif
                                                        <td class="text-center">
                                                            <div class="btn-group">
                                                                <a class="btn btn-sm btn-primary" data-toggle="tooltip"
                                                                   data-placement="left"
                                                                   title="edit"
                                                                   href="{{ url('affiliatepartners/' . $customer->cu_id . '/contactperson/' . $contactperson->cope_id.'/edit')}}">
                                                                    <i class="fa fa-pencil-alt"></i>
                                                                </a>
                                                            </div>
                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-sm btn-primary contactperson_delete" data-toggle="tooltip" title="Delete contact person" data-contactperson_id="{{$contactperson->cope_id}}">
                                                                    <i class="fa fa-times"></i>
                                                                </button>

                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="btabs-alt-static-remark" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        <div class="block-header block-header-default">
                                            <h3 class="block-title">
                                                <a href="{{ url('affiliatepartners/' . $customer->cu_id . '/customerremark/create')}}">
                                                    <button class="btn btn-primary">Add a remark</button>
                                                </a>

                                            </h3>
                                        </div>
                                        <div class="block-content block-content-full">
                                            <table data-order='[[1, "desc"]]' class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                                <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Timestamp</th>
                                                    <th>Contact date</th>
                                                    <th>Department</th>
                                                    <th>Employee</th>
                                                    <th>Medium</th>
                                                    <th>Direction</th>
                                                    <th>Remark</th>
                                                    <th>Delete</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach ($remarks as $remark)
                                                    <tr>
                                                        <td>{{$remark->cure_id}}</td>
                                                        <td>{{$remark->cure_timestamp}}</td>
                                                        <td>{{$remark->cure_contact_date}}</td>
                                                        <td>{{isset($remark->cure_department) ? $customerremarkdepartments[$remark->cure_department] : ''}}</td>
                                                        <td>{{isset($remark->user) ? ucfirst($remark->user->us_name) : 'Unknown'}}</td>
                                                        <td>{{isset($remark->cure_medium) ? $customerremarkmediums[$remark->cure_medium] : ''}}</td>
                                                        <td>{{isset($remark->cure_direction) ? $customerremarkdirections[$remark->cure_direction] : ''}}</td>
                                                        @if($remark->cure_status_id)
                                                            <td>
                                                                <table>
                                                                    <tbody>
                                                                    <tr>
                                                                        <th colspan="4">
                                                                            <strong>Status update</strong>
                                                                        </th>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Status
                                                                        </td>
                                                                        <td>
                                                                            Reason
                                                                        </td>
                                                                        <td>
                                                                            Netto turnover
                                                                        </td>
                                                                        <td>
                                                                            Remark
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Upsell
                                                                        </td>
                                                                        <td>
                                                                            Other
                                                                        </td>
                                                                        <td>
                                                                            {{$remark->status->cust_turnover_netto}}
                                                                        </td>
                                                                        <td>
                                                                            {{$remark->status->cust_remark}}
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        @else
                                                            <td>{!! $remark->cure_text !!}</td>
                                                        @endif
                                                        @if(!$remark->cure_status_id)
                                                            <td class="text-center">
                                                                <button type="button" class="btn btn-sm btn-primary remark_delete" data-toggle="tooltip" title="Delete" data-remark_id="{{$remark->cure_id}}">
                                                                    <i class="fa fa-times"></i>
                                                                </button>
                                                            </td>
                                                        @else
                                                            <td></td>
                                                        @endif
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="btabs-alt-static-documents" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        <div class="block-header block-header-default">
                                            <h3 class="block-title">
                                                <a href="{{ url('affiliatepartners/' . $customer->cu_id . '/customerdocument/create')}}">
                                                    <button class="btn btn-primary">Add a document</button>
                                                </a>

                                            </h3>
                                        </div>
                                        <div class="block-content block-content-full">
                                            <ul class="nav nav-tabs nav-tabs-alt" data-toggle="tabs" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" href="#btabs-alt-static-current">Current</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#btabs-alt-static-archived">Archived</a>
                                                </li>
                                            </ul>
                                            <div class="block-content tab-content">
                                                <div class="tab-pane active" id="btabs-alt-static-current" role="tabpanel">
                                                    <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                                        <thead>
                                                        <tr>
                                                            <th>ID</th>
                                                            <th>Timestamp</th>
                                                            <th>Uploaded by</th>
                                                            <th>Filename</th>
                                                            <th>Type</th>
                                                            <th>Description</th>
                                                            <th>Document</th>
                                                            <th>Archive</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach ($documents as $document)
                                                            <tr>
                                                                <td>{{$document->cudo_id}}</td>
                                                                <td>{{$document->cudo_timestamp}}</td>
                                                                <td>{{isset($document->user) ? ucfirst($document->user->us_name) : 'Unknown'}}</td>
                                                                <td>{{$document->cudo_filename}}</td>
                                                                <td>{{isset($document->cudo_type) ? $customerdocumenttypes[$document->cudo_type] : ''}}</td>
                                                                <td>{{$document->cudo_description}}</td>

                                                                <td>
                                                                    <a target="_blank" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="left" title="Download" href="{{ url('affiliatepartners/' . $customer->cu_id . '/document/'.$document['cudo_id'].'/download')}}">
                                                                        <i class="fa fa-download"></i>
                                                                    </a>
                                                                </td>
                                                                <td>
                                                                    <a class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="left" title="Archive" href="{{ url('affiliatepartners/' . $customer->cu_id . '/document/'.$document['cudo_id'].'/archive')}}">
                                                                        <i class="fa fa-minus"></i>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>

                                                <div class="tab-pane" id="btabs-alt-static-archived" role="tabpanel">
                                                    <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                                        <thead>
                                                        <tr>
                                                            <th>ID</th>
                                                            <th>Timestamp</th>
                                                            <th>Uploaded by</th>
                                                            <th>Filename</th>
                                                            <th>Type</th>
                                                            <th>Description</th>
                                                            <th>Document</th>
                                                            <th>Archive</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach ($documents_archived as $document)
                                                            <tr>
                                                                <td>{{$document->cudo_id}}</td>
                                                                <td>{{$document->cudo_timestamp}}</td>
                                                                <td>{{isset($document->user) ? ucfirst($document->user->us_name) : 'Unknown'}}</td>
                                                                <td>{{$document->cudo_filename}}</td>
                                                                <td>{{isset($document->cudo_type) ? $customerdocumenttypes[$document->cudo_type] : ''}}</td>
                                                                <td>{{$document->cudo_description}}</td>

                                                                <td>
                                                                    <a target="_blank" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="left" title="Download" href="{{ url('affiliatepartners/' . $customer->cu_id . '/document/'.$document['cudo_id'].'/download')}}">
                                                                        <i class="fa fa-download"></i>
                                                                    </a>
                                                                </td>
                                                                <td>
                                                                    <a class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="left" title="Recover" href="{{ url('affiliatepartners/' . $customer->cu_id . '/document/'.$document['cudo_id'].'/recover')}}">
                                                                        <i class="fa fa-redo"></i>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="btabs-alt-static-settings" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12 col-xl-12">
                                    <div class="block block-rounded block-bordered">
                                        <div class="block-content">
                                            <form id="affiliate_settings" method="post"
                                                  action="{{action('AffiliatePartnersController@update', $customer->cu_id)}}">
                                                @csrf
                                                <input name="_method" type="hidden" value="PATCH">
                                                <input name="form_name" type="hidden" value="Settings">

                                                <h2 class="content-heading pt-0">General</h2>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label">Status:</label>
                                                            <div class="col-sm-7">
                                                                <select type="text" class="form-control"
                                                                        name="status">
                                                                    @foreach($statuses as $id => $status)
                                                                        <option value="{{$id}}" {{ ($affiliatedata->afpada_status == $id) ? 'selected':'' }}>{{$status}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label" for="show_revs_in_portal">Show realtime data in Portal:</label>
                                                            <div class="col-sm-7">
                                                                <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                                                    <input id="show_revs_in_portal" type="checkbox" class="custom-control-input"
                                                                           name="show_revs_in_portal" {{($customer->cu_show_revenues_in_affiliate_portal ? "checked" : "")}}>
                                                                    <label class="custom-control-label"
                                                                           for="show_revs_in_portal"></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <h2 class="content-heading pt-0">Pricing Affiliate</h2>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label for="revenue_share" class="col-sm-3 col-form-label">Revenue Share %:</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control" name="revenue_share"
                                                                       value="{{$customer->cu_revenues_share}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label" for="price_per_lead">Price per Lead €</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control" name="price_per_lead"
                                                                       value="{{$affiliatedata->afpada_price_per_lead}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <h2 class="content-heading pt-0">Filters - Max Requests</h2>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-5 col-form-label" for="max_requests_month">Max. Requests per month:</label>
                                                            <div class="col-sm-5">
                                                                <input type="text" class="form-control" name="max_requests_month"
                                                                       value="{{$affiliatedata->afpada_max_requests_month}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-5 col-form-label" for="max_requests_month_left">Max. Requests per month (Left):</label>
                                                            <div class="col-sm-5">
                                                                <input type="text" class="form-control" name="max_requests_month_left"
                                                                       value="{{$affiliatedata->afpada_max_requests_month_left}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-5 col-form-label" for="max_requests_day">Max. Requests per day:</label>
                                                            <div class="col-sm-5">
                                                                <input type="text" class="form-control" name="max_requests_day"
                                                                       value="{{$affiliatedata->afpada_max_requests_day}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-5 col-form-label" for="max_requests_day_left">Max. Requests per day (Left):</label>
                                                            <div class="col-sm-5">
                                                                <input type="text" class="form-control" name="max_requests_day_left"
                                                                       value="{{$affiliatedata->afpada_max_requests_day_left}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h2 class="content-heading pt-0"></h2>

                                                        <div class="row">
                                                            <div class="col-md-1"></div>
                                                            <div class="form-group col-md-8">
                                                                <button type="submit" class="btn btn-primary">Update</button>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="btabs-alt-static-forms" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        <div class="block-header block-header-default">
                                            <h3 class="block-title">
                                                <a href="{{ url('affiliatepartners/' . $customer->cu_id . '/form/create')}}">
                                                    <button class="btn btn-primary">Add a form</button>
                                                </a>

                                            </h3>
                                        </div>
                                        <div class="block-content block-content-full">
                                            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                                <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Token</th>
                                                    <th>Name</th>
                                                    <th>Portal</th>
                                                    <th>Form type</th>
                                                    <th>Language</th>
                                                    <th>Volume type</th>
                                                    <th>Actions</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach ($forms->where("afpafo_form_type", "!=" , 3)->where("afpafo_deleted", 0) as $form)
                                                    <tr>
                                                        <td>{{$form->afpafo_id}}</td>
                                                        <td>{{$form->afpafo_token}}</td>
                                                        <td>{{$form->afpafo_name}}</td>
                                                        <td>{{$form->portal->po_portal}}</td>
                                                        <td>{{$formtypes[$form->afpafo_form_type]}}</td>
                                                        <td>{{$form->afpafo_la_code}}</td>
                                                        <td>{{$volumetypes[$form->afpafo_volume_type]}}</td>
                                                        <td class="text-center">
                                                            <div class="btn-group">
                                                                <a class="btn btn-sm btn-primary" data-toggle="tooltip"
                                                                   data-placement="left"
                                                                   title="edit"
                                                                   href="{{ url('affiliatepartners/' . $customer->cu_id . '/form/' . $form->afpafo_id.'/edit')}}">
                                                                    <i class="fa fa-pencil-alt"></i>
                                                                </a>
                                                            </div>


                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-sm btn-primary form_delete" data-toggle="tooltip" title="Delete" data-form_id="{{$form->afpafo_id}}">
                                                                    <i class="fa fa-times"></i>
                                                                </button>

                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                @foreach ($forms->where("afpafo_form_type", 3)->where("afpafo_deleted", 0) as $form)
                                                    <tr>
                                                        <td>{{$form->afpafo_id}}</td>
                                                        <td></td>
                                                        <td>{{$form->afpafo_name}}</td>
                                                        <td></td>
                                                        <td>{{$formtypes[$form->afpafo_form_type]}}</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="btabs-alt-static-offices" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        <div class="block-header block-header-default">
                                            <h3 class="block-title">
                                                <a href="{{ url('affiliatepartners/' . $customer->cu_id . '/customeroffice/create')}}">
                                                    <button class="btn btn-primary">Add an office</button>
                                                </a>

                                            </h3>
                                        </div>
                                        <div class="block-content block-content-full">
                                            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                                <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Street 1</th>
                                                    <th>Street 2</th>
                                                    <th>City</th>
                                                    <th>Zipcode</th>
                                                    <th>State</th>
                                                    <th>Country</th>
                                                    <th>Email</th>
                                                    <th>Telephone</th>
                                                    <th>Website</th>
                                                    <th>Modify</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach ($offices as $office)
                                                    <tr>
                                                        <td>{{$office->cuof_id}}</td>
                                                        <td>{{$office->cuof_street_1}}</td>
                                                        <td>{{$office->cuof_street_2}}</td>
                                                        <td>{{$office->cuof_city}}</td>
                                                        <td>{{$office->cuof_zipcode}}</td>
                                                        <td>{{$office->cuof_state}}</td>
                                                        <td>{{$office->cuof_co_code}}</td>
                                                        <td>{{$office->cuof_email}}</td>
                                                        <td>{{$office->cuof_telephone}}</td>
                                                        <td>{{$office->cuof_website}}</td>
                                                        <td class="text-center">
                                                            <div class="btn-group">
                                                                <a class="btn btn-sm btn-primary" data-toggle="tooltip"
                                                                   data-placement="left"
                                                                   title="edit"
                                                                   href="{{ url('affiliatepartners/' . $customer->cu_id . '/customeroffice/' . $office->cuof_id.'/edit')}}">
                                                                    <i class="fa fa-pencil-alt"></i>
                                                                </a>
                                                            </div>
                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-sm btn-primary office_delete" data-toggle="tooltip" title="Delete" data-office_id="{{$office->cuof_id}}">
                                                                    <i class="fa fa-times"></i>
                                                                </button>

                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="btabs-alt-static-requests" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        <form class="mb-5 mt-5" method="post" action="{{action('AffiliatePartnersController@getRequests', $customer->cu_id)}}">
                                            @csrf
                                            <input name="_method" type="hidden" value="post">

                                            <div class="form-group row">
                                                <label class="col-md-1 col-form-label ml-5" for="for-date">Date range:</label>
                                                <div class="col-md-2">
                                                    <input type="text" class="form-control drp drp-default" name="date" @if($request_date_filter != null) value="{{$request_date_filter}}" @endif autocomplete="off"/>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-1"></div>
                                                <div class="form-group col-md-8">
                                                    <button type="submit" class="btn btn-primary">Filter</button>
                                                </div>
                                            </div>
                                        </form>

                                        @if (!empty($customerportalrequests))
                                            <div class="block-content block-content-full">
                                                <table data-order='[[0, "desc"]]' class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                                    <thead>
                                                    <tr>
                                                        <th>ID</th>
                                                        <th>Received</th>
                                                        <th>Status</th>
                                                        <th>Affiliate status</th>
                                                        <th>Affiliate rejection reason</th>
                                                        <th>Name</th>
                                                        <th>Email</th>
                                                        <th>Country from</th>
                                                        <th>Country to</th>
                                                        <th>Amount</th>
                                                        <th>Origin</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach ($customerportalrequests as $request)
                                                        <tr>
                                                            <td>{{$request->request->re_id}}</td>
                                                            <td>{{$request->request->re_timestamp}}</td>
                                                            <td>{{$requeststatuses[$request->request->re_status]}}</td>
                                                            <td>{{$requeststatuses[$request->afpare_status]}}</td>
                                                            @if($request->afpare_rejection_reason)
                                                                <td>{{$affiliaterejectionreasons[$request->afpare_rejection_reason]}}</td>
                                                            @else
                                                                <td></td>
                                                            @endif
                                                            <td>{{$request->request->re_full_name}}</td>
                                                            <td>{{$request->request->re_email}}</td>
                                                            @isset($request->request->countryfrom)
                                                                <td>{{$request->request->countryfrom->co_en}}</td>
                                                            @else
                                                                <td>Unknown</td>
                                                            @endisset
                                                            @isset($request->request->countryto)
                                                                <td>{{$request->request->countryto->co_en}}</td>
                                                            @else
                                                                <td>Unknown</td>
                                                            @endisset
                                                            <td>{{$paymentcurrencies->where("pacu_code", $request->afpare_currency)->first()->pacu_token}} {{$request->afpare_amount}}</td>
                                                            <td>{{$request->request->affiliateform->afpafo_name}}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- END Block Tabs Alternative Style -->
            </div>
        </div>

    </div>

@endsection

@push( 'scripts' )

    <script>
        jQuery(document).ready(function(){

            firstInput();
            secondInput();

            function firstInput(){
                var input = document.querySelector("input[name=telephone]");
                output = $("#int_telephone");

                var iti = window.intlTelInput(input, {
                    nationalMode: true,
                    separateDialCode: true
                });

                var handleChange = function() {
                    var text = (iti.isValidNumber()) ? iti.getNumber() : 0;
                    if(text == 0){
                        $('input[name=telephone]').addClass("is-invalid");
                    }else{
                        $('input[name=telephone]').removeClass("is-invalid");
                    }
                    output.val(text);
                };

                input.addEventListener('change', handleChange);
                input.addEventListener('keyup', handleChange);

                handleChange();
            }

            function secondInput(){
                var input2 = document.querySelector("input[name=telephone_bi]");
                output2 = $("#int_telephone_bi");

                var iti2 = window.intlTelInput(input2, {
                    nationalMode: true,
                    separateDialCode: true
                });

                var handleChange = function() {
                    var text = (iti2.isValidNumber()) ? iti2.getNumber() : 0;
                    if(text == 0){
                        $('input[name=telephone_bi]').addClass("is-invalid");
                    }else{
                        $('input[name=telephone_bi]').removeClass("is-invalid");
                    }
                    output2.val(text);
                };

                input2.addEventListener('change', handleChange);
                input2.addEventListener('keyup', handleChange);

                handleChange();

            }

        	jQuery( document ).on( 'click', '.contactperson_delete', function(e) {
                e.preventDefault();

        		var $self = jQuery( this );

                confirmDelete("{{ url('ajax/customer/contactperson_delete') }}", 'get', {id:$self.data('contactperson_id')}, function() {

                    $self.parents( '.dataTable' ).DataTable().row( $self.parents( 'tr' ) ).remove().draw( 'page' );
                });
            });

        	jQuery( document ).on( 'click', '.status_delete', function(e) {
                e.preventDefault();

        		var $self = jQuery( this );

                confirmDelete("{{ url('ajax/customer/status_delete') }}", 'get', {id:$self.data('status_id')}, function() {

                    $self.parents( '.dataTable' ).DataTable().row( $self.parents( 'tr' ) ).remove().draw( 'page' );
                });
            });

        	jQuery( document ).on( 'click', '.remark_delete', function(e) {
                e.preventDefault();

        		var $self = jQuery( this );

                confirmDelete("{{ url('ajax/customer/remark_delete') }}", 'get', {id:$self.data('remark_id')}, function() {

                    $self.parents( '.dataTable' ).DataTable().row( $self.parents( 'tr' ) ).remove().draw( 'page' );
                });
            });

        	jQuery( document ).on( 'click', '.office_delete', function(e) {
                e.preventDefault();

        		var $self = jQuery( this );

                confirmDelete("{{ url('ajax/customer/office_delete') }}", 'get', {id:$self.data('office_id')}, function() {

                    $self.parents( '.dataTable' ).DataTable().row( $self.parents( 'tr' ) ).remove().draw( 'page' );
                });
            });

        	jQuery( document ).on( 'click', '.form_delete', function(e) {
                e.preventDefault();

        		var $self = jQuery( this );

                confirmDelete("{{ url('ajax/customer/form_delete') }}", 'get', {id:$self.data('form_id')}, function() {

                    $self.parents( '.dataTable' ).DataTable().row( $self.parents( 'tr' ) ).remove().draw( 'page' );
                });
            });
        });
    </script>

@endpush
