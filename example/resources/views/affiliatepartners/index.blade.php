@extends('layouts.backend')

@include( 'scripts.datatables' )
@include( 'scripts.dialogs' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Affiliate Partners</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url( 'dashboard' )}}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Affiliate Partners</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <!-- Your Block -->
        <div class="block block-rounded block-bordered">
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    <a href="{{ url('customers/create')}}"><button data-toggle="click-ripple" class="btn btn-primary">Add a Customer</button></a>
                    <a href="{{ url('customerchanges/')}}"><button data-toggle="click-ripple" class="btn btn-primary">Customer Changes ({{$customer_changes_count}})</button></a>
                    @if($cached_count['incorrect'] > 0)
                        <a href="{{ url('incorrect_customers')}}"><button data-toggle="click-ripple" class="btn btn-danger">Incorrect Customers ({{$cached_count['incorrect']}})</button></a>
                    @endif
                    <a href="{{ url('customercache')}}"><button class="btn btn-primary js-popover" data-toggle="popover" data-placement="top" data-original-title="Last updated" data-content="{{$last_timestamp_updated}}">Purge Cache</button></a>

                </h3>
            </div>
            <ul class="nav nav-tabs nav-tabs-alt" data-toggle="tabs" role="tablist">
                @if(View::exists('cache.affiliate_partners_1'))
                    <li class="nav-item">
                        <a class="nav-link active" href="#btabs-alt-static-active">Active ({{$cached_count[3][1]}})</a>
                    </li>
                @endif
                @if(View::exists('cache.affiliate_partners_0'))
                    <li class="nav-item">
                        <a class="nav-link" href="#btabs-alt-static-inactive">Inactive ({{$cached_count[3][0]}})</a>
                    </li>
                @endif
            </ul>

            <div class="block-content tab-content">

                @if(View::exists('cache.affiliate_partners_1'))
                    <div class="tab-pane active" id="btabs-alt-static-active" role="tabpanel">
                        <div class="block-content block-content-full">
                            @include('cache.affiliate_partners_1')
                        </div>
                    </div>
                @endif

                @if(View::exists('cache.affiliate_partners_0'))
                    <div class="tab-pane" id="btabs-alt-static-inactive" role="tabpanel">
                        <div class="block-content block-content-full">
                            @include('cache.affiliate_partners_0')
                        </div>
                    </div>
                @endif

            </div>
        </div>
        <!-- END Your Block -->
    </div>
    <!-- END Page Content -->
@endsection

@push( 'scripts' )
    <script>
        jQuery(document).ready(function(){
        	jQuery( document ).on( 'click', '.customer_delete', function(e) {

                e.preventDefault();

        		var $self = jQuery(this);

                @can("admin")
                    confirmDelete("{{ url('ajax/customer/delete') }}", 'get', {id:$self.data('id')}, function() {

                        $self.parents( '.dataTable' ).DataTable().row( $self.parents( 'tr' ) ).remove().draw( 'page' );
                    });
                @else
                    alert("You don't have permissions to delete a customer");
                @endcan
            });
        });
    </script>

@endpush
