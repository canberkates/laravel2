@extends('layouts.backend')

@include( 'scripts.datatables' )
@include( 'scripts.dialogs' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Users</h1>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">

        @include( 'admin.menu' )

        <div class="block block-rounded block-bordered">
            <div class="block-content block-content-full">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Permits
                        <a href="{{ url('admin/obligations/create')}}"><button data-toggle="click-ripple" class="btn btn-primary">Add a Permit</button></a>
                    </h3>
                </div>
                <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Type</th>
                        <th>Country</th>
                        <th>Name</th>
                        <th>Remark</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($obligations as $obligation)
                        <tr>
                            <td>{{$obligation->cuob_id}}</td>
                            <td>{{App\Data\ObligationType::all()[$obligation->cuob_type]}}</td>
                            <td>{{$countries[$obligation->cuob_co_code]}}</td>
                            <td>{{$obligation->cuob_name}}</td>
                            <td>{{$obligation->cuob_remark}}</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <a class="btn btn-sm btn-primary"
                                       data-toggle="tooltip"
                                       data-placement="left"
                                       title="edit"
                                       href="{{ url('/admin/obligations/' . $obligation->cuob_id . '/edit')}}">
                                        <i class="fa fa-pencil-alt"></i>
                                    </a>&nbsp;

                                    <button type='button' class='btn btn-sm btn-primary obligation_delete js-tooltip-enabled' data-toggle='tooltip' data-id='{{$obligation->cuob_id}}' data-original-title='Delete'>
                                        <i class='fa fa-times'></i>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@push( 'scripts' )
    <script>
        jQuery(document).ready(function(){
            jQuery( document ).on( 'click', '.obligation_delete', function(e) {

                e.preventDefault();

                var $self = jQuery(this);

                confirmDelete("{{ url('ajax/obligation/delete') }}", 'get', {id:$self.data('id')}, function() {

                    $self.parents( '.dataTable' ).DataTable().row( $self.parents( 'tr' ) ).remove().draw( 'page' );
                });

            });
        });
    </script>

@endpush
