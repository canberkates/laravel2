@extends('layouts.backend')

@include('scripts.datepicker')

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Edit a Request Delivery of {{$requestdelivery->customerportal->customer->cu_company_name_business}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../../../../../">Customers</a></li>
                        <li class="breadcrumb-item"><a href="../../../../edit">{{$requestdelivery->customerportal->customer->cu_company_name_business}}</a></li>
                        <li class="breadcrumb-item"><a href="../../edit">Portal</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Edit Request Delivery</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="block block-rounded block-bordered">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">General</h3>
                    </div>
                    <div class="block-content">
                        <form class="mb-5" method="post" action="{{action('RequestDeliveryController@update', $requestdelivery->rede_id)}}">
                            @csrf
                            <input name="_method" type="hidden" value="PATCH">
                            <input name="ktcupo_id" type="hidden" value="{{$requestdelivery->rede_ktcupo_id}}">
                            <input name="cu_id" type="hidden" value="{{$requestdelivery->customerportal->ktcupo_cu_id}}">

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="type">Type:</label>
                                <div class="col-sm-3">
                                    <select type="text" class="form-control" name="type">
                                        <option value=""></option>
                                        @foreach($requestdeliverytypes as $id => $value)
                                            <option value="{{$id}}" {{ ($requestdelivery->rede_type == $id) ? 'selected': '' }}>{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="value">Value:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="value" value="{{$requestdelivery->rede_value}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="extra">Extra:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="extra" value="{{$requestdelivery->rede_extra}}">
                                </div>
                            </div>

                            <h2 class="content-heading pt-0"></h2>

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection




