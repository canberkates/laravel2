@extends('layouts.backend')

@include('scripts.datepicker')

@section('content')

    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">System Settings</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url("admin/")}}">Admin</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="block block-rounded block-bordered">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Editing settings</h3>
                    </div>
                    <div class="block-content">
                        @if($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        {{$error}}<br>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="mb-5" method="post"
                              action="{{action('SystemSettingsController@update')}}">
                            @csrf
                            <input name="_method" type="hidden" value="POST">

                            @foreach($systemsettings as $group => $settings)
                                <h2 class="content-heading pt-0">{{$group}}</h2>
                                @foreach($settings as $setting)
                                    @if($setting->syse_setting == "company_co_code")
                                        @continue
                                    @endif
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-3 col-form-label" for="name">{{$setting->syse_name}}
                                            :</label>
                                        <div class="col-sm-7">
                                            @if($setting->syse_type == "text")
                                                <input type="text" class="form-control"
                                                       name="{{$setting->syse_setting}}"
                                                       value="{{$setting->syse_value}}">
                                            @else
                                                <select class="form-control" name="{{$setting->syse_setting}}">
                                                    @foreach($setting->options as $id => $option)
                                                        <option id="{{$id}}"
                                                                @if($setting->syse_value == $id) selected @endif>{{$option}}</option>
                                                    @endforeach
                                                </select>
                                            @endif
                                        </div>
                                    </div>

                                @endforeach
                            @endforeach

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>
                            </div>


                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection




