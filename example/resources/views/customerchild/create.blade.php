@extends('layouts.backend')
@include('scripts.forms')
@include('scripts.select2')

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Add a Child to {{$customer->cu_company_name_business}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../../">@if ($customer->cu_type == 1) {{"Customers"}} @elseif($customer->cu_type == 2) {{"Service Providers"}}@else {{"Affiliate Partners"}} @endif</a></li>
                        <li class="breadcrumb-item"><a href="../edit">{{$customer->cu_company_name_business}}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Add office</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="block block-rounded block-bordered">
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    {{$error}}<br>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Explanation</h3>
                    </div>
                    <div class="block-content">
                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-11 col-form-label">
                                Brand: appears in company in city lists, but leads to parent <br>
                                Branche: if child has no individual sirelo profile same as brand <br>
                                Individual company: independent legal entity
                            </label>

                        </div>
                    </div>
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Relationship Status</h3>
                    </div>
                        <div class="block-content">

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label" for="for-parent_sirelo_type">Parent Sirelo:</label>
                            <div class="col-sm-7">
                                <select id="for-parent_sirelo_type" disabled class="form-control" name="parent_sirelo_type"
                                        data-placeholder="Choose one..">
                                    @foreach($customerchildsirelotypes as $id => $type)
                                        <option value="{{$id}}" @if($id == $customer->moverdata->moda_parent_sirelo_type) selected @endif>{{$type}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label" for="for-child_sirelo_type">Child Sirelo:</label>
                            <div class="col-sm-7">
                                <select id="for-child_sirelo_type" disabled class="form-control" name="child_sirelo_type"
                                        data-placeholder="Choose one..">
                                    @foreach($customerchildsirelotypes as $id => $type)
                                        <option value="{{$id}}" @if($id == $customer->moverdata->moda_child_sirelo_type) selected @endif>{{$type}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="block-header block-header-default">
                        <h3 class="block-title">General</h3>
                    </div>
                    <div class="block-content">
                        <form class="mb-5" method="post" action="{{action('CustomerChildController@store')}}">
                            @csrf
                            <input name="_method" type="hidden" value="post">
                            <input name="customer_id" type="hidden" value="{{$customer->cu_id}}">
                            <input name="office_id" type="hidden" value="{{$office_id}}">


                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="for-child">Child customer:</label>
                                <div class="col-sm-7">
                                    <select id="for-child" class="js-select2-ajax form-control {{$errors->has('country') ? 'is-invalid' : ''}}" name="child" data-placeholder="Choose one..">
                                        <option value=""></option>
                                        @foreach($customers as $customer)
                                            @if($_GET['cu_id'] == $customer->cu_id)
                                                <option value="{{$customer->cu_id}}" selected>{{$customer->cu_company_name_business . " - ". $customer->cu_city . ", " . $customer->cu_co_code . " (" . $customer->cu_id . ")"}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="for-invoicing">Type</label>
                                <div class="col-sm-7">
                                    <select id="for-invoicing" class="form-control" name="child_type" data-placeholder="Choose one..">
                                        @foreach($customerchildtypes as $id => $type)
                                            <option value="{{$id}}" @if($id == $customer->moda_default_child_type) selected @endif >{{$type}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="for-country">Sirelo profile:</label>
                                <div class="col-sm-7">
                                    <select id="for-country" class="form-control" name="show_on_sirelo" data-placeholder="Choose one..">
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                            </div>

                            <input type="hidden" name="sirelo_type" value="{{$customer->moda_child_sirelo_type}}">

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Add child</button>
                                    <a class="btn btn-primary" href="../edit">
                                        Back
                                    </a>
                                </div>
                            </div>

                        </form>
                    </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection


@push( 'scripts' )

    <script>
        jQuery(document).ready(function(){
            $("select[name=show_on_sirelo]").change(function() {
                if($(this).val() == 1) {
                    $("#sirelo_relationship").show();
                }else{
                    $("#sirelo_relationship").hide();
                }
            });

            $("select[name=show_on_sirelo]").trigger("change");
        });

        $('.js-select2-ajax').select2({
            ajax: {
                url: "{{ url('ajax/select2customersearch') }}",
                data: function (params) {
                    var query = {
                        search: params.term
                    }
                    // Query parameters will be ?search=[term]&type=public
                    return query;
                },
                processResults: function (data) {
                    // Transforms the top-level key of the response object from 'items' to 'results'
                    console.log(data);
                    return {
                        results: data
                    };
                },
                dataType: 'json',
                delay: 150
                // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
            }
        });
    </script>

@endpush
