@extends('layouts.backend')

@include( 'scripts.datatables' )
@include('scripts.forms')
@include( 'scripts.dialogs' )
@include('scripts.select2')
@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Whitelist & Blacklist</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url( 'dashboard' )}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/validate_related_moving_companies">Validate moving companies</a></li>
                        <li class="breadcrumb-item"><a href="/validate_related_moving_companies/settings">General settings</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Country settings</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">

        <div class="block block-rounded block-bordered">
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Countries
                    <a style='float:right;' href='/validate_related_moving_companies/settings'><button class='btn btn-primary'>Whitelist/blacklist</button></a>
                </h3>
            </div>
            <div class="block-content block-content-full">
                <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                    <thead>
                    <tr>
                        <th>Country</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($countries as $co_code => $row)
                        <tr>
                            <td>{{$row['country']}} </td>
                            <td class='status_td text-center' style='font-size:10px;'>
                                <div class="custom-control custom-switch custom-control custom-control-inline custom-control-lg custom-control-primary">
                                    <input type="checkbox" class="custom-control-input country_status_checkbox" data-title='{{$row['country']}}' data-country='{{$co_code}}' id="country_{{$co_code}}" name="country_{{$co_code}}" @if($row['status'] == 1) checked @endif>
                                    <label class="custom-control-label" for="country_{{$co_code}}"></label>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>
@endsection

@push( 'scripts' )
    <script>

        $('.country_status_checkbox').change(function () {
            var on_or_off = 0;

            if(jQuery(this).is(":checked")) {
                on_or_off = 1;
            }

            if (on_or_off == 1) {
                $.ajax({
                    type: "GET",
                    url: '{{ url('/ajax/gathered_companies_put_country_active') }}',
                    data: {
                        "on_or_off": on_or_off,
                        "title": jQuery(this).data('title'),
                        "country": jQuery(this).data('country')
                    },
                    success: function(data){
                        console.log(data);
                    }
                });
            }
            else {
                $.ajax({
                    type: "GET",
                    url: '{{ url('/ajax/gathered_companies_put_country_inactive') }}',
                    data: {
                        "on_or_off": on_or_off,
                        "title": jQuery(this).data('title'),
                        "country": jQuery(this).data('country')
                    },
                    success: function(data){
                        console.log(data);
                    }
                });
            }
        });

    </script>
@endpush
