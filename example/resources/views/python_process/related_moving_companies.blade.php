@extends('layouts.backend')
@include( 'scripts.datatables' )
@include('scripts.select2')
@section('content')

    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Potential new Movers</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url( 'dashboard' )}}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Validate moving companies</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        <div class="block block-rounded block-bordered">
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Filter
                </h3>
            </div>
            <div class="block-content block-content-full">

                <form class="mb-5" method="post" action="{{action('PythonProcessController@validateRelatedMovingCompaniesFiltered')}}">
                    @csrf
                    <input name="_method" type="hidden" value="post">

<!--                    <input name="hidden_categories" type="hidden" value="">-->

                    <div class="form-group row">
                        <label class="col-md-1 col-form-label ml-5" for="country">Country:</label>
                        <div class="col-md-3">
                            <select class="js-select2 form-control" data-placeholder="Select an option..."
                                name="country" id="country" style="width:100%;">
                                <option></option>
                                @foreach($countries as $co_code => $country)
                                    <option @if(isset($selected_co_code) && $co_code == $selected_co_code) selected @endif value="{{$co_code}}">{{$country}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row" id="categories_div" @if(empty($main_categories)) style="display: none;"@endif>
                        <label class="col-md-1 col-form-label ml-5" for="categories">Categories:</label>
                        <div class="col-md-3">
                            <select multiple class="js-select2 form-control" data-placeholder="Select an option..."
                                name="categories[]" id="categories" style="width:100%;">
                                <option></option>
                                @if(isset($main_categories))
                                    @foreach($main_categories as $id => $cat)
                                        <option @if(in_array($id, $categories)) selected @endif value="{{$id}}">{{$cat}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>

                    <div class="form-group row" id="categories_div_hidden" style="display: none;">
                        <label class="col-md-1 col-form-label ml-5" for="categories_hidden">Categories:</label>
                        <div class="col-md-3">
                            <select multiple class="js-select2 form-control" data-placeholder="Select an option..."
                                name="categories_hidden[]" id="categories_hidden" style="width:100%;">
                                @if(isset($main_categories))
                                    @foreach($main_categories as $id => $cat)
                                        <option selected value="{{$id}}">{{$cat}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="form-group col-md-8">
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>

        @if($errors->any())
            @foreach($errors->all() as $error)
                <div class="alert alert-warning">
                    {{ $error }}
                </div>
            @endforeach
        @endif

        <div class="block block-rounded block-bordered">
            <div class="block block-rounded block-bordered">
                <div class="block-content">
                    <div class="block-content block-content-full">
                        <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>City</th>
                                <th>Country</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($entries as $row)
                                @if(isset($categories) && !in_array($row->gacoli_id, $categories))
                                    @continue
                                @endif
                                <tr>
                                    <td>{{$row->curesc_id}}</td>
                                    <td>{{$row->curesc_name}}</td>
                                    <td>{{$row->curesc_city}}</td>
                                    <td>{{\App\Functions\Data::country($row->gaco_searched_co_code, "EN")}}</td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <a class="btn btn-sm btn-primary open_new_tab"
                                                data-toggle="tooltip"
                                                data-placement="left"
                                                title="Add"
                                                target="_blank"
                                                href="{{ url('related_moving_companies/' .  $row->curesc_id  . '/create'.((!empty($selected_co_code)) ? '/'.$selected_co_code : ''))}}">
                                                <i class="fa fa-plus"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@push( 'scripts' )

    <script>
        jQuery(document).ready(function () {
            jQuery(".open_new_tab").click( function() {
                jQuery(this).parents('.dataTable').DataTable().row(jQuery(this).parents('tr')).remove().draw('page');
            });

            jQuery("select[name=country]").change( function() {

                jQuery.ajax({
                    url: "/ajax/related_moving_companies/get_categories",
                    method: 'get',
                    data: {co_code: jQuery("select[name=country]").val()},
                    success: function (data) {

                        var returnedData = JSON.parse(data);
                        console.log(returnedData);

                        //jQuery("input[name=hidden_categories]").val(returnedData);

                        jQuery.each(returnedData, function(key, val) {
                            console.log(key + " - " + val);

                            jQuery('#categories').append(jQuery('<option>', {
                                value: key,
                                text : val
                            }));

                            jQuery('#categories_hidden').append(jQuery('<option>', {
                                value: key + "," + val,
                                text : val
                            }));
                        });

                        jQuery("#categories_hidden > option").prop("selected","selected");// Select All Options
                        jQuery("#categories_hidden").trigger("change");

                        jQuery("#categories_div").slideDown();
                    }
                });

            });
        });
    </script>

@endpush
