@extends('layouts.backend')

@include( 'scripts.datatables' )
@include( 'scripts.dialogs' )
@include( 'scripts.datepicker' )
@include( 'scripts.select2' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Processing data - {{\App\Functions\Data::country($to_be_processed->cu_co_code, "EN")}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/validate_gathered_data">Select Country</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Process data</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="block block-rounded block-bordered">

            <div class="block-content ">
                <div class="row">
                    <h2 class="col-sm-12 content-heading pt-0">Processing <a target="_blank" href="/customers/{{$to_be_processed->cu_id}}/edit">{{$to_be_processed->cu_company_name_business}}</a> @if(isset($left))({{$left}} to process left)@endif</h2>
                    {{--<h2 class="col-sm-4 content-heading pt-0">⭐Review score: {{$to_be_processed->curesc_score}} out of {{$to_be_processed->curesc_amount}} reviews</h2>--}}

                </div>
                @if(isset($notices) && !empty($notices))
                    @foreach($notices as $warning => $err)
                        <?php (($warning == "Other domains") ? $uncheck_use_reviews = true : $uncheck_use_reviews = false) ?>
                    @endforeach
                @endif
                <?php
                    if(empty($to_be_processed->curesc_name)){
                        $uncheck_use_reviews = true;
                    }

                    if ($to_be_processed->cu_street_1 == trim($to_be_processed->curesc_street) && $to_be_processed->cu_zipcode == trim($to_be_processed->curesc_zipcode) && $to_be_processed->cu_city == trim($to_be_processed->curesc_city)) {
                        $uncheck_use_reviews = false;
                    }
                ?>
                <form method="post" action="@if(isset($skip)) {{action('PythonProcessController@validateSkippedDataProcess', $to_be_processed->curesc_id)}} @else {{action('PythonProcessController@process_next')}}@endif">
                    @csrf

                    @if(isset($warnings) && !empty($warnings))
                        <div class="alert alert-warning" style="color:black;padding-top:1rem; padding-right: 0.5rem; padding-bottom: 0.1rem; padding-left: 0.5rem;">
                            <ul>
                                @foreach($warnings as $warning_title => $message)
                                    @if ($warning_title == "Sirelo export disabled")
                                        <div class="form-group row">
                                            <label class="col-form-label" for="disable_sirelo_exp"><b>{{$warning_title}}</b> {!! $message !!}</label>
                                            <div style='margin-left:10px;'>
                                                <div class="custom-control custom-switch custom-control-lg custom-control-primary">
                                                    <input  type="checkbox" class="custom-control-input" id="disable_sirelo_exp"
                                                        name="disable_sirelo_exp" checked=''>
                                                    <label class="col-form-label custom-control-label"
                                                        for="disable_sirelo_exp"></label>
                                                </div>
                                            </div>
                                            <label class="col-form-label" for="disable_sirelo_exp"><i style='font-weight: lighter; font-size: 13px;'>Uncheck this checkbox to enable this company on Sirelo</i></label>
                                        </div>
                                    @else
                                        <b>{{$warning_title}}</b> {!! $message !!}<br/>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    @endif


                    <input name="_method" type="hidden" value="post">
                    <input name="cu_id" type="hidden" value="{{$to_be_processed->curesc_cu_id}}">
                    <input name="curesc_id" type="hidden" value="{{$to_be_processed->curesc_id}}">
                    <input name="co_code" type="hidden" value="{{$co_code}}">
                    <input name="co_code_of_customer" type="hidden" value="{{$to_be_processed->cu_co_code}}">
                    <input name="uncheck_use_reviews" type="hidden" value="{{$uncheck_use_reviews}}">
                    <input name="show_all_results" type="hidden" value="{{$show_all_results}}">
                    <input name="status" type="hidden" value="{{$status}}">
                    <input name="sirelo_export_disabled" type="hidden" value="{{$to_be_processed->moda_disable_sirelo_export}}">
                    <input name="start_url_erp" type="hidden" value="{{$to_be_processed->cu_website}}">
                    <input name="start_url_other" type="hidden" value="{{$to_be_processed->curesc_customer_website}}">
                    <input name="final_url_erp" type="hidden" value="{{$to_be_processed->curesc_final_url_erp}}">
                    <input name="final_url_other" type="hidden" value="{{$to_be_processed->curesc_final_url_other}}">
                    <input name="final_domain_erp" type="hidden" value="{{\App\Functions\System::getDomainFromURL($to_be_processed->curesc_final_url_erp)}}">
                    <input name="final_domain_other" type="hidden" value="{{\App\Functions\System::getDomainFromURL($to_be_processed->curesc_final_url_other)}}">

                    <div class="row">
                        <div class="col-sm-2">
                        </div>
                        <div class="col-sm-5">
                            <h2 class="content-heading pt-0">Our data @if(!empty($to_be_processed->moda_crm_status)) - <b @if($to_be_processed->moda_crm_status == 1) style='color:green;' @else style="font-weight: normal" @endif>{{strtoupper($crm_statuses[$to_be_processed->moda_crm_status])}} </b>@endif <a class="btn btn-outline-info" href="{{$search_url}}" target="_blank" style="float:right;">Search URL</a> @if (isset($did_we_send_leads_last_3_years) && $did_we_send_leads_last_3_years == 1) <br/><b style="font-size:13px; color: green;">Received leads in last 3 years</b> @endif</h2>
                        </div>
                        <div class="col-sm-5">
                            <h2 class="content-heading pt-0">Data from {{ucfirst($to_be_processed->curesc_platform)}} @if (!empty($to_be_processed->curesc_history)) <span id='show_history' class='btn btn-sm btn-outline-success' style='cursor:pointer; float:right;'>Show history</span><span id='hide_history' class='btn btn-sm btn-outline-danger' style='display:none; cursor:pointer; float: right;'>Hide history </span>@endif @if (isset($did_we_send_leads_last_3_years) && $did_we_send_leads_last_3_years == 1) <br/><br/> @endif </h2>
                        </div>
                    </div>
                    @if (!empty($to_be_processed->curesc_history))
                        <div class='row'>
                            <div class='col-sm-7'></div>
                            <div class='col-sm-5'>
                                <div id='google_history' style='display: none;'>
                                    <b>This are the changes compared the previous scan:</b><br />

                                    <?php
                                    $changes = false;
                                    ?>

                                    @if ($unserialized_history->curesc_name != $to_be_processed->curesc_name)
                                        <?php $changes = true; ?>
                                        <b>Name: </b> {{$unserialized_history->curesc_name}} <i class='fa fa-angle-right'></i> {{$to_be_processed->curesc_name}}<br />
                                    @endif

                                    @if ($unserialized_history->curesc_customer_website != $to_be_processed->curesc_customer_website)
                                        <?php $changes = true; ?>
                                        <b>Website: </b> {{$unserialized_history->curesc_customer_website}} <i class='fa fa-angle-right'></i> {{$to_be_processed->curesc_customer_website}}<br />
                                    @endif

                                    @if ($unserialized_history->curesc_street != $to_be_processed->curesc_street)
                                        <?php $changes = true; ?>
                                        <b>Street: </b> {{$unserialized_history->curesc_street}} <i class='fa fa-angle-right'></i> {{$to_be_processed->curesc_street}}<br />
                                    @endif

                                    @if ($unserialized_history->curesc_zipcode != $to_be_processed->curesc_zipcode)
                                        <?php $changes = true; ?>
                                        <b>Zipcode: </b> {{$unserialized_history->curesc_zipcode}} <i class='fa fa-angle-right'></i> {{$to_be_processed->curesc_zipcode}}<br />
                                    @endif

                                    @if ($unserialized_history->curesc_city != $to_be_processed->curesc_city)
                                        <?php $changes = true; ?>
                                        <b>City: </b> {{$unserialized_history->curesc_city}} <i class='fa fa-angle-right'></i> {{$to_be_processed->curesc_city}}<br />
                                    @endif

                                    @if ($unserialized_history->curesc_telephone != $to_be_processed->curesc_telephone)
                                        <?php $changes = true; ?>
                                        <b>Telephone: </b> {{$unserialized_history->curesc_telephone}} <i class='fa fa-angle-right'></i> {{$to_be_processed->curesc_telephone}}<br />
                                    @endif

                                    @if (!$changes)
                                        No changes
                                    @endif
                                    <br />
                                </div>
                            </div>
                        </div>
                    @endif
                    <div id="val_url_div" class="row" >
                        <div class="col-sm-2"></div>
                        <div class="col-sm-10" style='margin-bottom:10px;'>
                            <input type='hidden' name='website_validate_type_hidden' value='{{$to_be_processed->curesc_website_validate_type}}'/>
                            <div id="validate_url_message" class="row">
                                @if(!empty($to_be_processed->curesc_website_validate_type))
                                    {!! $website_validate_message !!}
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-sm-2 col-form-label">Website</label>

                        <div class="col-sm-5 form-group">
                            <div id="website_old" class="form-control" @if (($to_be_processed->cu_website)  == ($to_be_processed->curesc_customer_website) && !empty($to_be_processed->cu_website))) style="background-color:#ebebe4;" @endif>
                                <input autocomplete='off' type="radio" name="website" value="old" checked="checked" @if (($to_be_processed->cu_website)  == ($to_be_processed->curesc_customer_website) && !empty($to_be_processed->cu_website)) disabled @endif/>
                                <input autocomplete='off' style="pointer-events: none; width:85%; border:none;" type="text" name="old_website" value="{{$to_be_processed->cu_website}}" @if (($to_be_processed->cu_website)  == ($to_be_processed->curesc_customer_website) && !empty($to_be_processed->cu_website)) disabled @endif/>
                                <a id="link_to_website_old" href="{{$to_be_processed->cu_website}}" target="_blank" style="width:10%; text-align: center;">🔗</a>

                            ​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​</div>
                            <div id="website_old_same" style="color:red;display: none;"></div>
                            <div id="see_more_customers_with_same_url_erp_customers" style="display: none;"></div>
                            <div id="website_automatically_changed_old" style="margin-bottom: 10px;color:red;display: none;"><i style="font-size:12px;">Automatically changed from {{$to_be_processed->cu_website}}</i></div>
                        </div>

                        <div class="col-sm-5">
                            <div id="website_new" class="form-control" @if (($to_be_processed->cu_website)  == ($to_be_processed->curesc_customer_website) && !empty($to_be_processed->cu_website))) style="background-color:#ebebe4;" @endif>
                                <input autocomplete='off' type="radio" name="website" value="new" @if (($to_be_processed->cu_website)  == ($to_be_processed->curesc_customer_website) && !empty($to_be_processed->cu_website))) disabled @endif/>
                                <input autocomplete='off' style="pointer-events: none; width:85%; border:none;" type="text" name="new_website" value="{{$to_be_processed->curesc_customer_website}}" @if (($to_be_processed->cu_website)  == ($to_be_processed->curesc_customer_website) && !empty($to_be_processed->cu_website))) disabled @endif/>
                                <a id="link_to_website_new" href="{{$to_be_processed->curesc_customer_website}}" target="_blank" style="width:10%; text-align: center;">🔗</a>
                            </div>
                            <div id="website_new_same" style="color:red;display: none;"></div>
                            <div id="see_more_customers_with_same_url_other_customers" style="display: none;"></div>
                            <div id="website_automatically_changed_new" style="margin-bottom: 10px;color:red;display: none;"><i style="font-size:12px;">Automatically changed from {{$to_be_processed->curesc_customer_website}}</i></div>
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-sm-2 col-form-label">Company name (LEGAL)</label>

                        <div class="col-sm-5 form-group">

                            <div class="form-control" @if ($to_be_processed->curesc_name == $to_be_processed->cu_company_name_legal) style="background-color:#ebebe4;" @endif>
                                <input autocomplete='off' type="radio" name="company_name_legal" value="old" checked="checked" @if ($to_be_processed->curesc_name == $to_be_processed->cu_company_name_legal) disabled @endif/>
                                <input autocomplete='off' style="pointer-events: none; width:95%; border:none;" type="text" name="old_company_name_legal" value="{{$to_be_processed->cu_company_name_legal}}" @if ($to_be_processed->curesc_name == $to_be_processed->cu_company_name_legal) disabled @endif/>
                            ​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​</div>
                        </div>

                        <div class="col-sm-5">
                            <div class="form-control" @if ($to_be_processed->curesc_name == $to_be_processed->cu_company_name_legal) style="background-color:#ebebe4;" @endif>
                                <input autocomplete='off' type="radio" name="company_name_legal" value="new" @if ($to_be_processed->curesc_name == $to_be_processed->cu_company_name_legal) disabled @endif/>
                                <input autocomplete='off' style="pointer-events: none; width:95%; border:none;" type="text" name="new_company_name_legal" value="{{$to_be_processed->curesc_name}}" @if ($to_be_processed->curesc_name == $to_be_processed->cu_company_name_legal) disabled @endif/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-sm-2 col-form-label">Company name (BUSINESS)</label>

                        <div class="col-sm-5 form-group">

                            <div class="form-control" @if ($to_be_processed->curesc_name == $to_be_processed->cu_company_name_business) style="background-color:#ebebe4;" @endif>
                                <input autocomplete='off' type="radio" name="company_name_business" value="old" checked="checked" @if ($to_be_processed->curesc_name == $to_be_processed->cu_company_name_business) disabled @endif/>
                                <input autocomplete='off' style="pointer-events: none; width:95%; border:none;" type="text" name="old_company_name_business" value="{{$to_be_processed->cu_company_name_business}}" @if ($to_be_processed->curesc_name == $to_be_processed->cu_company_name_business) disabled @endif/>
                            ​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​</div>
                        </div>

                        <div class="col-sm-5">
                            <div class="form-control" @if ($to_be_processed->curesc_name == $to_be_processed->cu_company_name_business) style="background-color:#ebebe4;" @endif>
                                <input autocomplete='off' type="radio" name="company_name_business" value="new" @if ($to_be_processed->curesc_name == $to_be_processed->cu_company_name_business) disabled @endif/>
                                <input autocomplete='off' style="pointer-events: none; width:95%; border:none;" type="text" name="new_company_name_business" value="{{$to_be_processed->curesc_name}}" @if ($to_be_processed->curesc_name == $to_be_processed->cu_company_name_business) disabled @endif/>
                            </div>
                        </div>
                    </div>

                    @if($to_be_processed->curesc_permanently_closed)
                        <div class="row mb-1">
                            <div class="col-sm-7"></div>
                            <div class="col-sm-5" style="color: red;"><b>ATTENTION:</b> Google says this company is permanently closed!</div>
                        </div>
                        <br/>
                    @endif

                    @if($to_be_processed->cu_street_1 != trim($to_be_processed->curesc_street) ||$to_be_processed->cu_zipcode != trim($to_be_processed->curesc_zipcode) || $to_be_processed->cu_city != trim($to_be_processed->curesc_city))
                    <div class="row mb-1">
                        <div class="col-sm-7"></div>
                        <div class="col-sm-5"><b style="font-size:11px;cursor: pointer;" id="select_all_google_address_details"><i>SELECT ALL</i></b></div>
                    </div>
                    @endif


                    <div class="row">
                        <label class="col-sm-2 col-form-label">Street</label>


                        <div class="col-sm-5 form-group">
                            <div class="form-control" @if ($to_be_processed->cu_street_1 == trim($to_be_processed->curesc_street)) style="background-color:#ebebe4;" @endif>
                                <input autocomplete='off' type="radio" name="street" value="old" checked="checked" @if ($to_be_processed->cu_street_1 == trim($to_be_processed->curesc_street)) disabled @endif/>
                                <input autocomplete='off' style="pointer-events: none; width:95%; border:none;" type="text" name="old_street" value="{{$to_be_processed->cu_street_1}}" @if ($to_be_processed->cu_street_1 == trim($to_be_processed->curesc_street)) disabled @endif/>
                                ​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​</div>
                        </div>

                        <div class="col-sm-5">
                            <div class="form-control" @if ($to_be_processed->cu_street_1 == trim($to_be_processed->curesc_street)) style="background-color:#ebebe4;" @endif>
                                <input autocomplete='off' type="radio" name="street" value="new" @if ($to_be_processed->cu_street_1 == trim($to_be_processed->curesc_street)) disabled @endif/>
                                <input autocomplete='off' style="pointer-events: none; width:95%; border:none;" type="text" name="new_street" value="{{$to_be_processed->curesc_street}}" @if ($to_be_processed->cu_street_1 == trim($to_be_processed->curesc_street)) disabled @endif/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-sm-2 col-form-label">Zipcode</label>


                        <div class="col-sm-5 form-group">

                            <div class="form-control" @if ($to_be_processed->cu_zipcode == trim($to_be_processed->curesc_zipcode)) style="background-color:#ebebe4;" @endif>
                                <input autocomplete='off' type="radio" name="zipcode" value="old" checked="checked" @if ($to_be_processed->cu_zipcode == trim($to_be_processed->curesc_zipcode)) disabled="disabled" @endif/>
                                <input autocomplete='off' style="pointer-events: none; width:95%; border:none;" type="text" name="old_zipcode" value="{{$to_be_processed->cu_zipcode}}" @if ($to_be_processed->cu_zipcode == trim($to_be_processed->curesc_zipcode)) disabled="disabled" @endif/>
                                ​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​</div>
                        </div>

                        <div class="col-sm-5">
                            <div class="form-control" @if ($to_be_processed->cu_zipcode == trim($to_be_processed->curesc_zipcode)) style="background-color:#ebebe4;" @endif>
                                <input autocomplete='off' type="radio" name="zipcode" value="new" @if ($to_be_processed->cu_zipcode == trim($to_be_processed->curesc_zipcode)) disabled @endif/>
                                <input autocomplete='off' style="pointer-events: none; width:95%; border:none;" type="text" name="new_zipcode" value="{{$to_be_processed->curesc_zipcode}}" @if ($to_be_processed->cu_zipcode == trim($to_be_processed->curesc_zipcode)) disabled @endif/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-sm-2 col-form-label">City</label>


                        <div class="col-sm-5 form-group">

                            <div class="form-control" @if ($to_be_processed->cu_city == trim($to_be_processed->curesc_city)) style="background-color:#ebebe4;" @endif>
                                <input autocomplete='off' type="radio" name="city" value="old" checked="checked" @if ($to_be_processed->cu_city == trim($to_be_processed->curesc_city)) disabled @endif/>
                                <input autocomplete='off' style="pointer-events: none; width:95%; border:none;" type="text" name="old_city" value="{{$to_be_processed->cu_city}}" @if ($to_be_processed->cu_city == trim($to_be_processed->curesc_city)) disabled @endif/>
                                ​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​</div>
                        </div>

                        <div class="col-sm-5">
                            <div class="form-control" @if ($to_be_processed->cu_city == trim($to_be_processed->curesc_city)) style="background-color:#ebebe4;" @endif>
                                <input autocomplete='off' type="radio" name="city" value="new" @if ($to_be_processed->cu_city == trim($to_be_processed->curesc_city)) disabled @endif/>
                                <input autocomplete='off' style="pointer-events: none; width:95%; border:none;" type="text" name="new_city" value="{{$to_be_processed->curesc_city}}" @if ($to_be_processed->cu_city == trim($to_be_processed->curesc_city)) disabled @endif/>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        @if(!empty($coc_link) && $to_be_processed->cu_co_code == "NL")
                            <div class="col-sm-2 col-form-label">CoC</div>
                            <div class="col-sm-5">
                                <input class="form-control" type="text" name="coc" value="{{$to_be_processed->cu_coc}}"/> <a id="link_to_coc_erp" href="{{$coc_link}}" style="font-size:12px;" target="_blank">Link to COC</a>
                            </div>
                        @else
                            <div class="col-sm-7"></div>
                        @endif

                        <div class="col-sm-5">
                            <b style="font-size:11px;">Address details from {{ucfirst($to_be_processed->curesc_platform)}}: </b><i style="font-size:11px;">{{$to_be_processed->curesc_address}}</i>
                        </div>
                    </div>
                    <br />


                    <div class="row">
                        <label class="col-sm-2 col-form-label">Telephone (General)</label>

                        <div class="col-sm-5 form-group">

                            <div id="telephone_old" class="form-control" @if ($to_be_processed->cu_telephone == str_replace(" ", "", $to_be_processed->curesc_telephone)) style="background-color:#ebebe4;" @endif>
                                <input autocomplete='off' type="radio" name="telephone_main" value="old" checked="checked" @if ($to_be_processed->cu_telephone == str_replace(" ", "", $to_be_processed->curesc_telephone)) disabled @endif/>
                                <input autocomplete='off' style="pointer-events: none; width:95%; border:none;" type="text" name="old_telephone_main" value="{{$to_be_processed->cu_telephone}}" @if ($to_be_processed->cu_telephone == str_replace(" ", "", $to_be_processed->curesc_telephone)) disabled @endif/>
                                ​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​
                            </div>
                            <div id="telephone_old_changed" style="display: none;font-size:12px;margin-bottom:5px;margin-top:5px;">Changed from {{$to_be_processed->cu_telephone}}</div>

                        </div>

                        <div class="col-sm-5">
                            <div id="telephone_new" class="form-control" @if ($to_be_processed->cu_telephone == str_replace(" ", "", $to_be_processed->curesc_telephone)) style="background-color:#ebebe4;" @endif>
                                <input autocomplete='off' type="radio" name="telephone_main" value="new" @if ($to_be_processed->cu_telephone == str_replace(" ", "", $to_be_processed->curesc_telephone)) disabled @endif/>
                                <input autocomplete='off' style="pointer-events: none; width:95%; border:none;" type="text" name="new_telephone_main" value="{{$to_be_processed->curesc_telephone}}" @if ($to_be_processed->cu_telephone == str_replace(" ", "", $to_be_processed->curesc_telephone)) disabled @endif/>
                            </div>
                            <div id="telephone_new_changed" style="display: none;font-size:12px;margin-bottom:5px;margin-top:5px;">Changed from {{$to_be_processed->curesc_telephone}}</div>
                        </div>
                    </div>

                    <div id="telephone_main_not_valid" class="row" style="margin-top:-10px; margin-bottom:10px;display: none;">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-5" style="color:red;font-size:12px;">
                            This phone number is not valid
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-sm-2 col-form-label">Telephone (Sirelo)</label>

                        <div class="col-sm-5 form-group">

                            <div id="telephone_sirelo_old" class="form-control" @if ($to_be_processed->moda_contact_telephone == str_replace(" ", "", $to_be_processed->curesc_telephone)) style="background-color:#ebebe4;" @endif>
                                <input autocomplete='off' type="radio" name="telephone_sirelo" value="old" checked="checked" @if ($to_be_processed->moda_contact_telephone == str_replace(" ", "", $to_be_processed->curesc_telephone)) disabled @endif/>
                                <input autocomplete='off' style="pointer-events: none; width:95%; border:none;" type="text" name="old_telephone_sirelo" value="{{$to_be_processed->moda_contact_telephone}}" @if ($to_be_processed->moda_contact_telephone == str_replace(" ", "", $to_be_processed->curesc_telephone)) disabled @endif/>
                            </div>
                            <div id="telephone_sirelo_old_changed" style="display: none;font-size:12px;margin-bottom:10px;margin-top:5px;">Changed from {{$to_be_processed->moda_contact_telephone}}</div>

                        </div>

                        <div class="col-sm-5">
                            <div id="telephone_sirelo_new" class="form-control" @if ($to_be_processed->moda_contact_telephone == str_replace(" ", "", $to_be_processed->curesc_telephone)) style="background-color:#ebebe4;" @endif>
                                <input autocomplete='off' type="radio" name="telephone_sirelo" value="new" @if ($to_be_processed->moda_contact_telephone == str_replace(" ", "", $to_be_processed->curesc_telephone)) disabled @endif/>
                                <input autocomplete='off' style="pointer-events: none; width:95%; border:none;" type="text" name="new_telephone_sirelo" value="{{$to_be_processed->curesc_telephone}}" @if ($to_be_processed->moda_contact_telephone == str_replace(" ", "", $to_be_processed->curesc_telephone)) disabled @endif/>
                            </div>
                            <div id="telephone_sirelo_new_changed" style="display: none;font-size:12px;margin-bottom:10px;margin-top:5px;">Changed from {{$to_be_processed->curesc_telephone}}</div>
                        </div>
                    </div>

                    <div id="telephone_sirelo_not_valid" class="row" style="display: none;margin-top:-10px; margin-bottom:10px;">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-5" style="color:red;font-size:12px;">
                            This phone number is not valid
                        </div>
                    </div>

                    <div class="form-group row" @if($to_be_processed->moda_crm_status == 1) style='display: none;' @endif>
                        <label class="col-sm-2 col-form-label" for="email_general">Email (General):</label>
                        <div class="col-sm-5">
                            <input class="form-control" type="text" name="email_general" value="{{$to_be_processed->cu_email}}" />
                        </div>

                        <div style="display: none;" id="email_general_validate_message_success" class="col-sm-5 mt-2">✔</div>
                        <div style="display: none;" id="email_general_validate_message_err" class="col-sm-5 mt-2">❌ The domain of the email(s) doesn't match with the website domain</div>

                        @if (!empty($to_be_processed->cu_website) || !empty($to_be_processed->curesc_customer_website))
                            <div class='col-sm-2'></div>
                            <span class='col-sm-5 add_info_email_general mt-2' style='color: #0665d0;cursor:pointer;font-size:13px;'>
                                Add info@- email address
                            </span>
                            <div class='col-sm-5'></div>
                        @endif
                        <div class='col-sm-2'></div>
                        <span class='col-sm-5 email_domain_warning mt-2' style='color: #d03f06;font-size:13px;font-weight: bold; display:none;'>
                            WARNING: There is one or more email(s) with not the same domain as the domain of the chosen URL
                        </span>
                        <div class='col-sm-5'></div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label" for="email_sirelo">Email (Sirelo):</label>
                        <div class="col-sm-5">
                            <input class="form-control" type="text" name="email_sirelo" value="{{$to_be_processed->moda_contact_email}}" />
                        </div>
                        <div style="display: none;" id="email_sirelo_validate_message_success" class="col-sm-4 mt-2">✔</div>
                        <div style="display: none;" id="email_sirelo_validate_message_err" class="col-sm-4 mt-2">❌ The domain of the email doesn't match with the website domain</div>
                        @if (!empty($to_be_processed->cu_website) || !empty($to_be_processed->curesc_customer_website))
                            <div class='col-sm-2'></div>
                            <span class='col-sm-5 add_info_email_sirelo mt-2' style='color: #0665d0;cursor:pointer;font-size:13px;'>
                                Add info@- email address
                            </span>
                        @endif
                    </div>

                    <div class="form-group row" @if (empty($to_be_processed->curesc_name)) style="display: none;" @endif>
                        <label class="col-sm-2 col-form-label" for="use_reviews">Use reviews:</label>
                        <div class='col-sm-5'>
                            <div class="custom-control custom-switch custom-control-lg custom-control-primary">
                                <input type="checkbox" class="custom-control-input" id="use_reviews"
                                    name="use_reviews" @if(isset($uncheck_use_reviews) && $uncheck_use_reviews == true) '' @else checked='' @endif>
                                <label class="custom-control-label"
                                    for="use_reviews"></label>
                            </div>
                        </div>

                        <div class="col-sm-5">

                            @foreach($reviews as $platform => $row_review)
                                <div class="mb-1" style="margin-top: 0.6rem;">⭐{{ucfirst($platform)}}: {{$row_review['score']}} out of {{$row_review['amount']}} reviews</div>
                            @endforeach

                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label" for="remarks">Remarks:</label>
                        <div class="col-sm-5">
                            <textarea rows="2" type="text" class="form-control"
                                      name="remarks">{{$to_be_processed->curesc_remarks}}</textarea>
                        </div>
                    </div>

                    <h2 class="content-heading pt-0"></h2>

                    <div class="form-group">
                        <div style="float: left;">
                            <button type="button" class="btn btn-outline-danger delete_customer" data-id="{{$to_be_processed->curesc_cu_id}}">Remove from ERP</button>
                            <button type="submit" style="display: none;" name="go_to_next"></button>
                            <button type="submit" style="display: none;" name="failed_to_delete_next"></button>

                        </div>
                        <div style="text-align: center;">
                            @if(!isset($skip))
                                <button type="submit" name="skip_btn" class="btn btn-primary">Skip</button>
                            @endif
                            <button name="update_and_next" type="submit" class="btn btn-success">Update & go to next</button>
                        </div>

                    </div>

                </form>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection

@push( 'scripts' )
    <script>
        jQuery("input[type=radio]").css("transform", "scale(1.3)");

        function confirmDeleteViaPythonProcess( $url, $method, $data, $callback = '', $button_text = "Yes, delete it!") {
            swal({
                title: "Are you sure?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $button_text,
                preConfirm: function() {
                    return new Promise(function(resolve) {
                        jQuery.ajax({
                            url: $url,
                            method: $method,
                            data: $data,
                            success: function( $result) {
                                swal("Done!", "It was successfully deleted!", "success");
                                console.log( $result);

                                if( typeof $callback == 'function' ) {

                                    $callback.call( $result );
                                }
                            },
                            error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail

                                swal("Error deleting!", errorThrown, "error");
                                console.log(JSON.stringify(jqXHR));
                                console.log("AJAX error: " + textStatus + ' : ' + errorThrown);

                                jQuery("button.swal2-confirm").click( function() {
                                    jQuery("button[name=failed_to_delete_next]").click();
                                });
                            }
                        });
                    });
                }
            });
        }

        jQuery( document ).on( 'click', '.delete_customer', function(e) {

            e.preventDefault();

            var $self = jQuery(this);

            confirmDeleteViaPythonProcess("{{ url('ajax/python_process/customer_delete') }}", 'get', {id:$self.data('id')}, function() {
                jQuery("button[name=go_to_next]").click();
            });

        });

        jQuery("input[type=text],textarea").click( function() {
            jQuery(this).parent().find("input[type=radio]").prop("checked", true);
            jQuery(this).parent().find("input[type=radio]").trigger("change");
        });

        jQuery("#select_all_google_address_details").click( function() {
            $(":radio[value=old][name=street],:radio[value=old][name=zipcode],:radio[value=old][name=city]").attr('checked',false);
            $(":radio[value=new][name=street],:radio[value=new][name=zipcode],:radio[value=new][name=city]").attr('checked',true);
        });

        function validateWebsiteString() {
            var validate_url_message_div = jQuery( "div#validate_url_message" ).text();

            if( validate_url_message_div.indexOf( "redirect" ) >= 0 ) {
                jQuery( "input[name=old_website]" ).prop( "disabled", false );
                jQuery( "input[name=new_website]" ).prop( "disabled", false );
                jQuery( "div#website_old" ).css( "background-color", 'transparent' );
                jQuery( "div#website_new" ).css( "background-color", 'transparent' );
                $( ":radio[name=website]" ).prop( 'disabled', false );
            }


            if( validate_url_message_div.indexOf( "Google URL is the best URL" ) >= 0 ) {
                $( ":radio[value=old][name=website]" ).attr( 'checked', false );
                $( ":radio[value=new][name=website]" ).attr( 'checked', true ).trigger( "change" );
                jQuery( "div#website_old,div#website_new" ).css( "border", "2px solid #129a12ba" );


            }

            if( validate_url_message_div.indexOf( "ERP URL is the best URL" ) >= 0 ) {
                jQuery( "input[name=old_website]" ).prop( "disabled", true );
                jQuery( "input[name=new_website]" ).prop( "disabled", true );

                jQuery( "div#website_old" ).css( "background-color", '#ebebe4' );
                jQuery( "div#website_new" ).css( "background-color", '#ebebe4' );

                $( ":radio[value=old][name=website]" ).prop( 'disabled', true );
                $( ":radio[value=new][name=website]" ).prop( 'disabled', true );

                $( ":radio[value=new][name=website]" ).attr( 'checked', false );
                $( ":radio[value=old][name=website]" ).attr( 'checked', true ).trigger( "change" );
            }

            if( validate_url_message_div.indexOf( "Current ERP URL is good" ) >= 0 ) {
                if( jQuery( "input[name=old_website]" ).val().indexOf( "https://" ) !== - 1 ) {
                    jQuery( "input[name=old_website]" ).prop( "disabled", true );
                    jQuery( "input[name=new_website]" ).prop( "disabled", true );

                    jQuery( "div#website_old" ).css( "background-color", '#ebebe4' );
                    jQuery( "div#website_new" ).css( "background-color", '#ebebe4' );

                    $( ":radio[value=old][name=website]" ).prop( 'disabled', true );
                    $( ":radio[value=new][name=website]" ).prop( 'disabled', true );

                }
                $( ":radio[value=new][name=website]" ).attr( 'checked', false );
                $( ":radio[value=old][name=website]" ).attr( 'checked', true ).trigger( "change" );
            }

            if( validate_url_message_div.indexOf( "Final ERP URL is" ) >= 0 && validate_url_message_div.indexOf( "domain has been changed after the redirect" ) >= 0 && jQuery( "#validated_url_erp" ).data( "url" ) == jQuery( "#validated_url_other" ).data( "url" ) ) {
                if( jQuery( "input[name=old_website]" ).val().indexOf( "https://" ) !== - 1 && jQuery( "input[name=old_website]" ).val() == jQuery( "#validated_url_erp" ).data( "url" ) ) {
                    jQuery( "input[name=old_website]" ).prop( "disabled", true );
                    jQuery( "input[name=new_website]" ).prop( "disabled", true );

                    jQuery( "div#website_old" ).css( "background-color", '#ebebe4' );
                    jQuery( "div#website_new" ).css( "background-color", '#ebebe4' );

                    $( ":radio[value=old][name=website]" ).prop( 'disabled', true );
                    $( ":radio[value=new][name=website]" ).prop( 'disabled', true );

                }

                if( jQuery( "#validated_url_erp" ).data( "url" ) == jQuery( "#validated_url_other" ).data( "url" ) && jQuery( "input[name=old_website]" ).val() != jQuery( "#validated_url_erp" ).data( "url" ) ) {
                    jQuery( "input[name=old_website]" ).val( jQuery( "#validated_url_erp" ).data( "url" ) );
                    jQuery( "#website_automatically_changed_old" ).show();
                    jQuery( "a#link_to_website_old" ).attr( "href", jQuery( "input[name=old_website]" ).val() );


                    jQuery( "div#website_old,div#website_new" ).css( "border", "2px solid #129a12ba" )
                }

                $( ":radio[value=new][name=website]" ).attr( 'checked', false );
                $( ":radio[value=old][name=website]" ).attr( 'checked', true ).trigger( "change" );
            }

            if( validate_url_message_div.indexOf( "Final Google URL is" ) >= 0 && validate_url_message_div.indexOf( "domain has been changed after the redirect" ) >= 0 && jQuery( "#validated_url_erp" ).data( "url" ) == jQuery( "#validated_url_other" ).data( "url" ) ) {
                $( ":radio[value=old][name=website]" ).attr( 'checked', false );
                $( ":radio[value=new][name=website]" ).attr( 'checked', true ).trigger( "change" );
            }

            if( validate_url_message_div.indexOf( "Current Google URL is good" ) >= 0 ) {
                if( jQuery( "input[name=old_website]" ).val().indexOf( "https://" ) == - 1 && jQuery( "input[name=new_website]" ).val().indexOf( "https://" ) != - 1 ) {
                    jQuery( "input[name=old_website]" ).prop( "disabled", false );
                    jQuery( "input[name=new_website]" ).prop( "disabled", false );

                    jQuery( "div#website_old" ).css( "background-color", 'transparent' );
                    jQuery( "div#website_new" ).css( "background-color", 'transparent' );

                    $( ":radio[value=old][name=website]" ).prop( 'disabled', false );
                    $( ":radio[value=new][name=website]" ).prop( 'disabled', false );

                    $( ":radio[value=old][name=website]" ).attr( 'checked', false );
                    $( ":radio[value=new][name=website]" ).attr( 'checked', true ).trigger( "change" );
                }
            }

            if( validate_url_message_div.indexOf( "Current ERP URL is good" ) >= 0 && validate_url_message_div.indexOf( "Current Google URL is good" ) >= 0 ) {
                console.log( jQuery( "input[name=old_website]" ).val() );
                console.log( jQuery( "input[name=new_website]" ).val() );

                jQuery( "input[name=old_website]" ).prop( "disabled", false );
                jQuery( "input[name=new_website]" ).prop( "disabled", false );

                jQuery( "div#website_old" ).css( "background-color", 'transparent' );
                jQuery( "div#website_new" ).css( "background-color", 'transparent' );

                $( ":radio[value=old][name=website]" ).prop( 'disabled', false );
                $( ":radio[value=new][name=website]" ).prop( 'disabled', false );
            }

            if( validate_url_message_div.indexOf( "ERP URL is not working" ) >= 0 && validate_url_message_div.indexOf( "Current Google URL is the best one" ) >= 0 ) {
                $( ":radio[value=old][name=website]" ).attr( 'checked', false );
                $( ":radio[value=new][name=website]" ).attr( 'checked', true ).trigger( "change" );
            }

            if( validate_url_message_div.indexOf( "Google URL is not working" ) >= 0 && validate_url_message_div.indexOf( "Current ERP URL is the best one" ) >= 0 ) {
                $( ":radio[value=new][name=website]" ).attr( 'checked', false );
                $( ":radio[value=old][name=website]" ).attr( 'checked', true ).trigger( "change" );
            }

            if( validate_url_message_div.indexOf( "The current URLs are the best URLs" ) >= 0 ) {
                jQuery( "input[name=old_website]" ).prop( "disabled", true );
                jQuery( "input[name=new_website]" ).prop( "disabled", true );

                jQuery( "div#website_old" ).css( "background-color", '#ebebe4' );
                jQuery( "div#website_new" ).css( "background-color", '#ebebe4' );
                $( ":radio[value=old][name=website]" ).prop( 'disabled', true );
                $( ":radio[value=new][name=website]" ).prop( 'disabled', true );
            }

            if( validate_url_message_div.indexOf( "another domain then the final URL of Google" ) >= 0 ) {
                jQuery( "input[name=use_reviews]" ).attr( "checked", false );
                jQuery( "input[name=send_email]" ).attr( "checked", false );
            }

            if( jQuery( "input[name=old_street]" ).val() == jQuery( "input[name=new_street]" ).val() && jQuery( "input[name=old_zipcode]" ).val() == jQuery( "input[name=new_zipcode]" ).val() && jQuery( "input[name=old_city]" ).val() == jQuery( "input[name=new_city]" ).val() ) {
                jQuery( "input[name=send_email]" ).attr( "checked", true );
                jQuery( "input[name=use_reviews]" ).attr( "checked", true );
            }

            jQuery( "span#validated_url_erp,span#validated_url_other" ).css( "cursor", "pointer" );

            jQuery( "#validated_url_erp" ).click( function() {
                jQuery( "input[name=old_website]" ).prop( "disabled", false );
                jQuery( "input[name=new_website]" ).prop( "disabled", false );

                jQuery( "div#website_old" ).css( "background-color", 'transparent' );
                jQuery( "div#website_new" ).css( "background-color", 'transparent' );
                $( ":radio[value=old][name=website]" ).prop( 'disabled', false );
                $( ":radio[value=new][name=website]" ).prop( 'disabled', false );

                jQuery( "input[name=old_website]" ).val( jQuery( this ).data( "url" ) );
                jQuery( "a#link_to_website_old" ).attr( "href", jQuery( "input[name=old_website]" ).val() );

                $( ":radio[value=new][name=website]" ).attr( 'checked', false );

                $( ":radio[value=old][name=website]" ).attr( 'checked', true ).trigger( "change" );
            } );

            jQuery( "#validated_url_other" ).click( function() {
                jQuery( "input[name=old_website]" ).prop( "disabled", false );
                jQuery( "input[name=new_website]" ).prop( "disabled", false );

                jQuery( "div#website_old" ).css( "background-color", 'transparent' );
                jQuery( "div#website_new" ).css( "background-color", 'transparent' );
                $( ":radio[value=old][name=website]" ).prop( 'disabled', false );
                $( ":radio[value=new][name=website]" ).prop( 'disabled', false );

                jQuery( "input[name=new_website]" ).val( jQuery( this ).data( "url" ) );
                jQuery( "a#link_to_website_new" ).attr( "href", jQuery( "input[name=new_website]" ).val() );


                $( ":radio[value=old][name=website]" ).attr( 'checked', false );
                $( ":radio[value=new][name=website]" ).attr( 'checked', true ).trigger( "change" );
            } );
            if( ( validate_url_message_div.indexOf( "ERP URL is redirected" ) >= 0 && validate_url_message_div.indexOf( "Google URL is redirected" ) >= 0 && jQuery( "#validated_url_erp" ).data( "url" ) == jQuery( "#validated_url_other" ).data( "url" ) && jQuery( "#validated_url_erp" ).data( "url" ).length > 0 && jQuery( "#validated_url_other" ).data( "url" ).length > 0 || validate_url_message_div.indexOf( "ERP and Google URL are the same. There is a redirect in the URL." ) >= 0 && jQuery( "#validated_url_erp" ).data( "url" ).length > 0 ) ) {
                jQuery( "input[name=old_website]" ).val( jQuery( "#validated_url_erp" ).data( "url" ) );
                jQuery( "#website_automatically_changed_old" ).show();
                jQuery( "a#link_to_website_old" ).attr( "href", jQuery( "input[name=old_website]" ).val() );


                jQuery( "div#website_old,div#website_new" ).css( "border", "2px solid #129a12ba" );

                $( ":radio[value=new][name=website]" ).attr( 'checked', false );
                $( ":radio[value=old][name=website]" ).attr( 'checked', true ).trigger( "change" );
            }
            if( validate_url_message_div.indexOf( "ERP URL is not working" ) >= 0 ) {
                if( validate_url_message_div.indexOf( "Current ERP URL is not working." ) >= 0 ) {
                    jQuery( "div#validate_url_message" ).css( "color", "red" );
                }
                else {
                    jQuery( "div#validate_url_message div:first" ).css( "color", "red" );
                }

            }

            if( jQuery( "input[name=uncheck_use_reviews]" ).val() != 1 && jQuery( "input[name=use_reviews]" ).not( ":checked" ).length && jQuery( "#validated_url_erp" ).data( "url" ) == jQuery( "#validated_url_other" ).data( "url" ) ) {
                jQuery( "input[name=use_reviews]" ).prop( "checked", true );
                jQuery( "input[name=send_email]" ).prop( "checked", true );
            }

            /*if( jQuery( "input[name=old_street]" ).val() != "" && jQuery( "input[name=old_zipcode]" ).val() != "" && jQuery( "input[name=old_city]" ).val() != "" && jQuery( "input[name=new_company_name_business]" ).val() == "" && ( validate_url_message_div.indexOf( "Current ERP URL is good" ) >= 0 || validate_url_message_div.indexOf( "Current ERP URL is the best one" ) >= 0 ) && validate_url_message_div.indexOf( "No Google URL scraped" ) >= 0 && jQuery( "input[name=sirelo_export_disabled]" ).val() == "0" ) {

                $.ajax( {
                    type: "GET",
                    url: '{{ url('/ajax/validate_email_domain') }}',
                    data: {
                        "url_erp": jQuery( "input[name=old_website]" ).val(),
                        "url_other": jQuery( "input[name=new_website]" ).val(),
                        "email_general": jQuery( "input[name=email_general]" ).val(),
                        "email_sirelo": jQuery( "input[name=email_sirelo]" ).val()
                    },
                    success: function( data ) {
                        //Parse JSON
                        var returnedData = JSON.parse( data );

                        if( returnedData.email_sirelo ) {

                            $.ajax( {
                                type: "GET",
                                url: '{{ url('/ajax/validate_phone_numbers') }}',
                                data: {
                                    "phone_general": jQuery( "input[name=old_telephone_main]" ).val(),
                                    "phone_sirelo": jQuery( "input[name=old_telephone_sirelo]" ).val(),
                                    "phone_scraped": jQuery( "input[name=new_telephone_main]" ).val(),
                                    "co_code": jQuery( "input[name=co_code_of_customer]" ).val()
                                },
                                success: function( data ) {
                                    //Parse JSON
                                    var returnedDataTelephone = JSON.parse( data );

                                    console.log( returnedDataTelephone );

                                    if( returnedDataTelephone != null ) {
                                        if( returnedDataTelephone.converted_phone_sirelo != 'not_valid' ) {
                                            if( confirm( 'The website and the Sirelo email + telephone seems to be correct. Do you want to process this automatically?' ) ) {
                                                jQuery( "button[name=update_and_next]" ).click();
                                            }
                                        }

                                    }

                                }
                            } );
                        }
                    }
                } );
            }*/

            if( validate_url_message_div.indexOf( "has another domain then the final URL of" ) >= 0 ) {
                if( jQuery( "span#validated_url_erp" ).data( "url" ) != "" && jQuery( "input[name=old_website]" ).val() != jQuery( "span#validated_url_erp" ).data( "url" ) ) {
                    jQuery( "input[name=old_website]" ).val( jQuery( "span#validated_url_erp" ).data( "url" ) );
                    jQuery( "#website_automatically_changed_old" ).show();
                }
                if( jQuery( "span#validated_url_other" ).data( "url" ) != "" && jQuery( "input[name=new_website]" ).val() != jQuery( "span#validated_url_other" ).data( "url" ) ) {
                    jQuery( "input[name=new_website]" ).val( jQuery( "span#validated_url_other" ).data( "url" ) );
                    jQuery( "#website_automatically_changed_new" ).show();
                }


            }

            if( validate_url_message_div.indexOf( "ERP URL is redirected to" ) >= 0 ) {
                if( jQuery( "span#validated_url_erp" ).data( "url" ) != "" && jQuery( "input[name=old_website]" ).val() != jQuery( "span#validated_url_erp" ).data( "url" ) ) {
                    jQuery( "input[name=old_website]" ).val( jQuery( "span#validated_url_erp" ).data( "url" ) );
                    jQuery( "#website_automatically_changed_old" ).show();
                }
            }

            if( validate_url_message_div.indexOf( "Google URL is redirected to" ) >= 0 ) {
                if( jQuery( "span#validated_url_other" ).data( "url" ) != "" && jQuery( "input[name=new_website]" ).val() != jQuery( "span#validated_url_other" ).data( "url" ) ) {
                    jQuery( "input[name=new_website]" ).val( jQuery( "span#validated_url_other" ).data( "url" ) );
                    jQuery( "#website_automatically_changed_new" ).show();
                }
            }

            if( validate_url_message_div.indexOf( "There is a redirect in the scraped Google url. The best URL is" ) >= 0 ) {
                if( jQuery( "span#validated_url_other" ).data( "url" ) != "" && jQuery( "input[name=new_website]" ).val() != jQuery( "span#validated_url_other" ).data( "url" ) ) {
                    jQuery( "input[name=new_website]" ).val( jQuery( "span#validated_url_other" ).data( "url" ) );
                    jQuery( "#website_automatically_changed_new" ).show();
                }

                if( validate_url_message_div.indexOf( "ERP URL is not working" ) >= 0 ) {
                    jQuery( "input[name=old_website]" ).prop( "disabled", false );
                    jQuery( "input[name=new_website]" ).prop( "disabled", false );

                    jQuery( "div#website_old" ).css( "background-color", 'transparent' );
                    jQuery( "div#website_new" ).css( "background-color", 'transparent' );
                    $( ":radio[value=old][name=website]" ).prop( 'disabled', false );
                    $( ":radio[value=new][name=website]" ).prop( 'disabled', false );

                    $( ":radio[value=old][name=website]" ).attr( 'checked', false );

                    $( ":radio[value=new][name=website]" ).attr( 'checked', true ).trigger( "change" );
                }

            }

            if( ( validate_url_message_div.indexOf( "There is found a redirect to another domain" ) >= 0 || validate_url_message_div.indexOf( "There is a redirect in the current ERP url" ) >= 0 ) && validate_url_message_div.indexOf( "No Google URL" ) >= 0 ) {
                if( jQuery( "span#validated_url_erp" ).data( "url" ) != "" && jQuery( "input[name=old_website]" ).val() != jQuery( "span#validated_url_erp" ).data( "url" ) ) {
                    jQuery( "input[name=old_website]" ).val( jQuery( "span#validated_url_erp" ).data( "url" ) );
                    jQuery( "#website_automatically_changed_old" ).show();
                }
            }

            if( validate_url_message_div.indexOf( "Final ERP URL is" ) >= 0 && validate_url_message_div.indexOf( "The domain has been changed after the redirect" ) >= 0 ) {
                if( jQuery( "span#validated_url_erp" ).data( "url" ) != "" && jQuery( "input[name=old_website]" ).val() != jQuery( "span#validated_url_erp" ).data( "url" ) ) {
                    jQuery( "input[name=old_website]" ).val( jQuery( "span#validated_url_erp" ).data( "url" ) );
                    jQuery( "#website_automatically_changed_old" ).show();
                }

                if( jQuery( "span#validated_url_other" ).data( "url" ) != "" && jQuery( "input[name=new_website]" ).val() != jQuery( "span#validated_url_other" ).data( "url" ) ) {
                    jQuery( "input[name=new_website]" ).val( jQuery( "span#validated_url_other" ).data( "url" ) );
                    jQuery( "#website_automatically_changed_new" ).show();
                }
            }

            if( validate_url_message_div.indexOf( "The domain has been changed after the redirect" ) >= 0 && validate_url_message_div.indexOf( "Final Google URL is" ) >= 0 ) {
                if( jQuery( "span#validated_url_erp" ).data( "url" ) != "" && jQuery( "input[name=old_website]" ).val() != jQuery( "span#validated_url_erp" ).data( "url" ) ) {
                    jQuery( "input[name=old_website]" ).val( jQuery( "span#validated_url_erp" ).data( "url" ) );
                    jQuery( "#website_automatically_changed_old" ).show();
                }

                if( jQuery( "span#validated_url_other" ).data( "url" ) != "" && jQuery( "input[name=new_website]" ).val() != jQuery( "span#validated_url_other" ).data( "url" ) ) {
                    jQuery( "input[name=new_website]" ).val( jQuery( "span#validated_url_other" ).data( "url" ) );
                    jQuery( "#website_automatically_changed_new" ).show();
                }
            }

            if( validate_url_message_div.indexOf( "ERP and Google URL are the same. There is found a redirect to another domain" ) >= 0 ) {
                if( jQuery( "span#validated_url_erp" ).data( "url" ) != "" && jQuery( "input[name=old_website]" ).val() != jQuery( "span#validated_url_erp" ).data( "url" ) ) {
                    jQuery( "input[name=old_website]" ).val( jQuery( "span#validated_url_erp" ).data( "url" ) );
                    jQuery( "#website_automatically_changed_old" ).show();
                }

                if( jQuery( "span#validated_url_erp" ).data( "url" ) != "" && jQuery( "input[name=new_website]" ).val() != jQuery( "span#validated_url_erp" ).data( "url" ) ) {
                    jQuery( "input[name=new_website]" ).val( jQuery( "span#validated_url_erp" ).data( "url" ) );
                    jQuery( "#website_automatically_changed_new" ).show();
                }
            }

            if( validate_url_message_div.indexOf( "ERP and Google URL are the same. There is a redirect in the URL. The best URL is:" ) >= 0 ) {
                if( jQuery( "span#validated_url_erp" ).data( "url" ) != "" && jQuery( "input[name=old_website]" ).val() != jQuery( "span#validated_url_erp" ).data( "url" ) ) {
                    jQuery( "input[name=old_website]" ).val( jQuery( "span#validated_url_erp" ).data( "url" ) );
                    jQuery( "#website_automatically_changed_old" ).show();
                }

                if( jQuery( "span#validated_url_erp" ).data( "url" ) != "" && jQuery( "input[name=new_website]" ).val() != jQuery( "span#validated_url_erp" ).data( "url" ) ) {
                    jQuery( "input[name=new_website]" ).val( jQuery( "span#validated_url_erp" ).data( "url" ) );
                    jQuery( "#website_automatically_changed_new" ).show();
                }
            }

            if( validate_url_message_div.indexOf( "not working" ) === - 1 ) {
                jQuery( "div#validate_url_message" ).hide();
            }
            else {
                jQuery( "div#validate_url_message" ).show();
            }

            if( jQuery( "input[name=old_website]" ).val().replace( /\/$/, "" ) == jQuery( "input[name=new_website]" ).val().replace( /\/$/, "" ) && jQuery( "input[name=old_website]" ).val() != "" && jQuery( "input[name=new_website]" ).val() != "" ) {
                jQuery( "div#website_old,div#website_new" ).css( "border", "2px solid #129a12ba" );
            }
            else {
                jQuery( "div#website_old,div#website_new" ).css( "border", "1px solid #d8dfed" );
            }

            if( jQuery( "input[name=final_domain_erp]" ).val() != "" && jQuery( "input[name=final_domain_other]" ).val() != "" && jQuery( "input[name=final_domain_erp]" ).val() == jQuery( "input[name=final_domain_other]" ).val() ) {
                jQuery( "div#website_old,div#website_new" ).css( "border", "2px solid #129a12ba" );
            }

            /*if( validate_url_message_div.indexOf( "Current ERP URL is the best one" ) >= 0 && validate_url_message_div.indexOf( "No Google URL" ) >= 0 ) {
                jQuery( "#website_automatically_changed_old" ).hide();
            }*/
        }

        validateWebsiteString();

        if (jQuery("input[name=website_validate_type_hidden]").val() == ''){
            $.ajax({
                type: "GET",
                url: '{{ url('/ajax/validate_website_urls') }}',
                data: {"url_erp": jQuery("input[name=old_website]").val(), "url_other": jQuery("input[name=new_website]").val()},
                success: function(data){
                    //Parse JSON
                    var returnedData = JSON.parse(data);

                    console.log(returnedData);

                    jQuery("#validate_url_message").html(returnedData.html_string+ "<br /><br />");
                    jQuery("#val_url_div").show();

                    validateWebsiteString()

                    if (jQuery("input[name=final_url_erp]").val() != "" && jQuery("input[name=old_website]").val() != jQuery("input[name=final_url_erp]").val()) {
                        jQuery("input[name=old_website]").val(jQuery("input[name=final_url_erp]").val());
                        jQuery("#website_automatically_changed_old").show();
                    }

                    if (jQuery("input[name=final_url_other]").val() != "" && jQuery("input[name=new_website]").val() != jQuery("input[name=final_url_other]").val()) {
                        jQuery("input[name=new_website]").val(jQuery("input[name=final_url_other]").val());
                        jQuery("#website_automatically_changed_new").show();
                    }

                }
            });
        }
        /*else {
            if (jQuery("#validate_url_message").text().indexOf( "Current ERP URL is the best one" ) == -1 && jQuery("input[name=final_url_erp]").val() != "" && jQuery("input[name=old_website]").val() != jQuery("input[name=final_url_erp]").val()) {
                jQuery("input[name=website_old]").val(jQuery("input[name=final_url_erp]").val());
                jQuery("#website_automatically_changed_old").show();
            }

            if (jQuery("#validate_url_message").text().indexOf( "Current Google URL is the best one" ) == -1 && jQuery("input[name=final_url_other]").val() != "" && jQuery("input[name=new_website]").val() != jQuery("input[name=final_url_other]").val()) {
                jQuery("input[name=new_website]").val(jQuery("input[name=final_url_other]").val());
                jQuery("#website_automatically_changed_new").show();
            }
        }*/


        /**
         * Put this AJAX call in ajax call success above ^ If you want to validate by final domain instead of seperated by ERP/Google domain
         */
        $.ajax({
            type: "GET",
            url: '{{ url('/ajax/validate_email_domain') }}',
            data: {
                //"final_domain": jQuery("span#final_domain").data("domain"),
                "url_erp": jQuery("input[name=old_website]").val(),
                "url_other": jQuery("input[name=new_website]").val(),
                "email_general": jQuery("input[name=email_general]").val(),
                "email_sirelo": jQuery("input[name=email_sirelo]").val()
            },
            success: function(data){
                //Parse JSON
                var returnedData = JSON.parse(data);

                console.log(returnedData);

                if(returnedData.email_general){
                    jQuery("#email_general_validate_message_err").hide();
                    jQuery("#email_general_validate_message_success").show();
                }
                else {
                    jQuery("#email_general_validate_message_success").hide();
                    jQuery("#email_general_validate_message_err").show();
                }

                if(returnedData.email_sirelo){
                    jQuery("#email_sirelo_validate_message_err").hide();
                    jQuery("#email_sirelo_validate_message_success").show();
                }
                else {
                    jQuery("#email_sirelo_validate_message_success").hide();
                    jQuery("#email_sirelo_validate_message_err").show();
                }
            }
        });

        /**
         * Validating telephone number from ERP and Google
         */
        function validatePhoneNumbers() {
            $.ajax({
                type: "GET",
                url: '{{ url('/ajax/validate_phone_numbers') }}',
                data: {
                    "phone_general": jQuery("input[name=old_telephone_main]").val(),
                    "phone_sirelo": jQuery("input[name=old_telephone_sirelo]").val(),
                    "phone_scraped": jQuery("input[name=new_telephone_main]").val(),
                    "co_code": jQuery("input[name=co_code_of_customer]").val()
                },
                success: function(data){
                    //Parse JSON
                    var returnedData = JSON.parse(data);

                    console.log(returnedData);

                    if (returnedData != null) {
                        if (returnedData.converted_phone_scraped_int == 'not_valid')
                        {
                            jQuery("#telephone_general_google_not_valid").show();
                        }
                        else {
                            if (jQuery("input[name=new_telephone_main]").val() != returnedData.converted_phone_scraped_int){
                                jQuery("input[name=new_telephone_main]").val(returnedData.converted_phone_scraped_int);
                                jQuery("#telephone_new_changed").show();
                            }
                        }

                        if (returnedData.converted_phone_scraped_nat == 'not_valid')
                        {
                            jQuery("#telephone_sirelo_google_not_valid").show();
                        }
                        else {
                            if (jQuery("input[name=new_telephone_sirelo]").val() != returnedData.converted_phone_scraped_nat){
                                jQuery("input[name=new_telephone_sirelo]").val(returnedData.converted_phone_scraped_nat);
                                jQuery("#telephone_sirelo_new_changed").show();
                            }
                        }

                        if (returnedData.converted_phone_general == 'not_valid')
                        {
                            jQuery("#telephone_main_not_valid").show();
                        }
                        else {
                            if (returnedData.converted_phone_general == jQuery("input[name=new_telephone_main]").val() && returnedData.converted_phone_general != jQuery("input[name=old_telephone_main]").val()) {
                                $(":radio[value=old][name=telephone_main]").attr('checked',false);
                                $(":radio[value=new][name=telephone_main]").attr('checked',true);

                                jQuery("div#telephone_old, div#telephone_new").css("border", "2px solid #129a12ba");
                            }
                            else {
                                if (jQuery("input[name=old_telephone_main]").val() != returnedData.converted_phone_general) {
                                    jQuery("input[name=old_telephone_main]").val(returnedData.converted_phone_general);
                                    $(":radio[value=new][name=telephone_main]").attr('checked',false);
                                    $(":radio[value=old][name=telephone_main]").attr('checked',true);

                                    jQuery("#telephone_old_changed").show();
                                }
                            }
                        }

                        if (returnedData.converted_phone_general != jQuery("input[name=new_telephone_general]").val()) {
                            $(":radio[name=telephone_main]").prop('disabled',false);
                            jQuery("input[name=new_telephone_main]").prop("disabled", false);
                            jQuery("input[name=old_telephone_main]").prop("disabled", false);
                            jQuery("div#telephone_old").css("background-color", 'transparent');
                            jQuery("div#telephone_new").css("background-color", 'transparent');
                        }

                        if (returnedData.converted_phone_sirelo == 'not_valid')
                        {
                            jQuery("#telephone_sirelo_not_valid").show();
                        }
                        else {
                            if (returnedData.converted_phone_sirelo == jQuery("input[name=new_telephone_sirelo]").val() && returnedData.converted_phone_sirelo != jQuery("input[name=old_telephone_sirelo]").val()) {
                                $(":radio[value=old][name=telephone_sirelo]").attr('checked',false);
                                $(":radio[value=new][name=telephone_sirelo]").attr('checked',true);
                                $(":radio[value=old][name=telephone_sirelo]").prop("disabled", false);
                                $(":radio[value=new][name=telephone_sirelo]").prop("disabled", false);
                                jQuery("input[name=new_telephone_sirelo]").prop("disabled", false);
                                jQuery("input[name=old_telephone_sirelo]").prop("disabled", false);
                                jQuery("#telephone_sirelo_new").css("background-color", "transparent");
                                jQuery("#telephone_sirelo_old").css("background-color", "transparent");


                                jQuery("div#telephone_sirelo_old, div#telephone_sirelo_new").css("border", "2px solid #129a12ba");
                            }
                            else {
                                if (jQuery("input[name=old_telephone_sirelo]").val() != returnedData.converted_phone_sirelo) {
                                    jQuery("input[name=old_telephone_sirelo]").val(returnedData.converted_phone_sirelo);
                                    jQuery("#telephone_sirelo_old_changed").show();
                                }
                            }
                        }
                    }

                    if (jQuery("input[name=old_telephone_main]").val().replace(/\s/g, '') == jQuery("input[name=new_telephone_main]").val().replace(/\s/g, '') && jQuery("input[name=old_telephone_main],input[name=new_telephone_main]").val().length > 0) {
                        jQuery("div#telephone_old, div#telephone_new").css("border", "2px solid #129a12ba");
                    }

                    if (jQuery("input[name=old_telephone_sirelo]").val().replace(/\s/g, '') == jQuery("input[name=new_telephone_sirelo]").val().replace(/\s/g, '') && jQuery("input[name=old_telephone_sirelo],input[name=new_telephone_sirelo]").val().length > 0) {
                        jQuery("div#telephone_sirelo_old, div#telephone_sirelo_new").css("border", "2px solid #129a12ba");
                    }

                }
            });
        }

        validatePhoneNumbers();

        jQuery("input[name=old_telephone_main],input[name=old_telephone_sirelo]").change(function () {
            validatePhoneNumbers();
        });

        jQuery("input[name=email_general]").change(function() {

            if (jQuery("input[name=email_general]").val()) {
                var old_or_new = $("input[name=website]:checked").val();

                if (old_or_new == "old") {
                    var website = jQuery("input[name=old_website]").val();
                }
                else {
                    var website = jQuery("input[name=new_website]").val();
                }

                $.ajax({
                    type: "GET",
                    url: '{{ url('/ajax/check_for_email_warning') }}',
                    data: {
                        "email": jQuery("input[name=email_general]").val(),
                        "website": website
                    },
                    success: function(data){
                        //Parse JSON
                        var returnedData = JSON.parse(data);

                        console.log("HI");
                        console.log(returnedData);

                        if(returnedData.warning){
                            jQuery("span.email_domain_warning").slideDown();
                        }
                        else {
                            jQuery("span.email_domain_warning").slideUp();

                        }
                    }
                });
            }
            else {
                jQuery("span.email_domain_warning").slideUp();
            }

        }).trigger("change");

        jQuery("input[name=email_general],input[name=email_sirelo],input[name=old_website],input[name=new_website]").keyup(function () {
            $.ajax({
                type: "GET",
                url: '{{ url('/ajax/validate_email_domain') }}',
                data: {
                    //"final_domain": jQuery("span#final_domain").data("domain"),
                    "url_erp": jQuery("input[name=old_website]").val(),
                    "url_other": jQuery("input[name=new_website]").val(),
                    "email_general": jQuery("input[name=email_general]").val(),
                    "email_sirelo": jQuery("input[name=email_sirelo]").val(),
                    "only": jQuery(this).val()
                },
                success: function(data){
                    //Parse JSON
                    var returnedData = JSON.parse(data);

                    console.log(returnedData);

                    if(returnedData.email_general){
                        jQuery("#email_general_validate_message_err").hide();
                        jQuery("#email_general_validate_message_success").show();
                    }
                    else {
                        jQuery("#email_general_validate_message_success").hide();
                        jQuery("#email_general_validate_message_err").show();
                    }

                    if(returnedData.email_sirelo){
                        jQuery("#email_sirelo_validate_message_err").hide();
                        jQuery("#email_sirelo_validate_message_success").show();
                    }
                    else {
                        jQuery("#email_sirelo_validate_message_success").hide();
                        jQuery("#email_sirelo_validate_message_err").show();
                    }


                    /*$('input[type=radio][name=website]').change(function() {
                     if (this.value == 'new') {
                     alert("Allot Thai Gayo Bhai");
                     }
                     });*/
                }
            });
        })

        $('input[type=radio][name=website]').change(function () {
            $.ajax({
                type: "GET",
                url: '{{ url('/ajax/validate_email_domain') }}',
                data: {
                    //"final_domain": jQuery("span#final_domain").data("domain"),
                    "url_erp": jQuery("input[name=old_website]").val(),
                    "url_other": jQuery("input[name=new_website]").val(),
                    "email_general": jQuery("input[name=email_general]").val(),
                    "email_sirelo": jQuery("input[name=email_sirelo]").val(),
                    "only": jQuery(this).val()
                },
                success: function(data){
                    //Parse JSON
                    var returnedData = JSON.parse(data);

                    console.log(returnedData);

                    if(returnedData.email_general){
                        jQuery("#email_general_validate_message_err").hide();
                        jQuery("#email_general_validate_message_success").show();
                    }
                    else {
                        jQuery("#email_general_validate_message_success").hide();
                        jQuery("#email_general_validate_message_err").show();
                    }

                    if(returnedData.email_sirelo){
                        jQuery("#email_sirelo_validate_message_err").hide();
                        jQuery("#email_sirelo_validate_message_success").show();
                    }
                    else {
                        jQuery("#email_sirelo_validate_message_success").hide();
                        jQuery("#email_sirelo_validate_message_err").show();
                    }


                    /*$('input[type=radio][name=website]').change(function() {
                        if (this.value == 'new') {
                            alert("Allot Thai Gayo Bhai");
                        }
                    });*/
                }
            });
        });

        $('input[name=old_website]').change(function () {
            var website = jQuery(this).val();
            var cu_id = jQuery("input[name=cu_id]").val();

            $.ajax({
                type: "GET",
                url: '{{ url('/ajax/validate_website') }}',
                data: {
                    "website": website,
                    "cu_id": cu_id
                },
                success: function(data){
                    //Parse JSON
                    var returnedData = JSON.parse(data);

                    console.log(returnedData);

                    if(returnedData.same_website_as_others) {
                        jQuery("div#website_old_same").html("There are already other customers with this URL. Please double check: ");

                        count = 0;

                        $.each( returnedData.customers, function( index, value ){
                            count = count + 1;
                            if (count <= 3) {
                                jQuery("div#website_old_same").append(((count != 1) ? ", " : "") + "<a target='_blank' href='/customers/" + index + "/edit'>" + value + "</a>")
                            }
                            else {
                                if (count == 4) {
                                    jQuery("div#website_old_same").append(" <div><a style='cursor:pointer;' id='see_more_customers_with_same_url_erp'>See more...</a></div>")
                                    jQuery("div#see_more_customers_with_same_url_erp_customers").append("<a class='see_more_customers_with_same_url_erp_customers' target='_blank' href='/customers/" + index + "/edit'>" + value + "</a>")

                                }
                                else {
                                    jQuery("div#see_more_customers_with_same_url_erp_customers").append(", " + " <a class='see_more_customers_with_same_url_erp_customers' target='_blank' href='/customers/" + index + "/edit'>" + value + "</a>")
                                }

                            }
                        });

                        jQuery("a#see_more_customers_with_same_url_erp").click( function() {
                            jQuery("a#see_more_customers_with_same_url_erp").slideUp();
                            jQuery("div#see_more_customers_with_same_url_erp_customers").slideDown();
                        });

                        jQuery("div#website_old_same").show();

                    }
                    else {
                        jQuery("div#website_old_same").hide();
                    }
                }
            });

            jQuery("input[name=final_url_erp]").val(website);

            $.ajax({
                type: "GET",
                url: '{{ url('/ajax/save_new_domain_in_form') }}',
                data: {
                    "website": website,
                },
                success: function(data){
                    console.log(data);
                    jQuery( "span#final_domain_erp" ).data( "domain", data )
                    jQuery( "span#final_domain" ).data( "domain", data )

                }
            });
        }).trigger("change");

        $('input[name=new_website]').change(function () {
            var website = jQuery(this).val();
            var cu_id = jQuery("input[name=cu_id]").val();

            $.ajax({
                type: "GET",
                url: '{{ url('/ajax/validate_website') }}',
                data: {
                    "website": website,
                    "cu_id": cu_id
                },
                success: function(data){
                    //Parse JSON
                    var returnedData = JSON.parse(data);

                    console.log(returnedData);

                    if(returnedData.same_website_as_others) {
                        jQuery("div#website_new_same").html("There are already other customers with this URL. Please double check: ");

                        count = 0;

                        $.each( returnedData.customers, function( index, value ){
                            count = count + 1;
                            if (count <= 3) {
                                jQuery("div#website_new_same").append(((count != 1) ? ", " : "") + "<a target='_blank' href='/customers/" + index + "/edit'>" + value + "</a>")
                            }
                            else {
                                if (count == 4) {
                                    jQuery("div#website_new_same").append(" <div><a style='cursor:pointer;' id='see_more_customers_with_same_url_other'>See more...</a></div>")
                                    jQuery("div#see_more_customers_with_same_url_other_customers").append("<a class='see_more_customers_with_same_url_other_customers' target='_blank' href='/customers/" + index + "/edit'>" + value + "</a>")

                                }
                                else {
                                    jQuery("div#see_more_customers_with_same_url_other_customers").append(", " + " <a class='see_more_customers_with_same_url_other_customers' target='_blank' href='/customers/" + index + "/edit'>" + value + "</a>")
                                }

                            }
                        });


                        jQuery("a#see_more_customers_with_same_url_other").click( function() {
                            jQuery("a#see_more_customers_with_same_url_other").slideUp();
                            jQuery("div#see_more_customers_with_same_url_other_customers").slideDown();
                        });

                        jQuery("div#website_new_same").show();

                    }
                    else {
                        jQuery("div#website_new_same").hide();
                    }
                }
            });

            jQuery("input[name=final_url_other]").val(website);

            $.ajax({
                type: "GET",
                url: '{{ url('/ajax/save_new_domain_in_form') }}',
                data: {
                    "website": website,
                },
                success: function(data){
                    console.log(data);
                    jQuery( "span#final_domain_other" ).data( "domain", data )
                    jQuery( "span#final_domain" ).data( "domain", data )

                }
            });
        }).trigger("change");

        $('.form-control').click(function () {
            jQuery(this).parent().parent().find(".form-control").css("background-color", "transparent");
            jQuery(this).parent().parent().find(":input").prop("disabled", false);
            jQuery(this).parent().find(":radio").attr("checked", true);
            jQuery(this).parent().find(":input").css("pointer-events", "auto").focus();

        });

        $('.add_info_email_general').click(function () {
            var old_or_new = $("input[name=website]:checked").val();

            if (old_or_new == "old") {
                var final_domain = jQuery("span#final_domain_erp").data("domain");
            }
            else {
                var final_domain = jQuery("span#final_domain_other").data("domain");
            }

            if (typeof final_domain == 'undefined') {
                var final_domain = jQuery("span#final_domain").data("domain");
            }

            if (typeof final_domain == 'undefined') {
                $('.add_info_email_general').hide();

                alert("Could not add info@ email address automatically, because there is no final domain found.");

                return;
            }

            //Strip www. from final domain
            final_domain = final_domain.replace("www.", "");

            var already_in_input = false;

            if (jQuery("input[name=email_general]").val().indexOf("info@" + final_domain) >= 0) {
                already_in_input = true;
            }

            if (jQuery("input[name=email_general]").val().length > 0 && !already_in_input) {
                jQuery("input[name=email_general]").val(jQuery("input[name=email_general]").val() + ",info@" + final_domain);

            }
            else if (!already_in_input) {
                jQuery("input[name=email_general]").val("info@" + final_domain);
            }

            $('.add_info_email_general').hide();
        });

        $('.add_info_email_sirelo').click(function () {
            var old_or_new = $("input[name=website]:checked").val();

            if (old_or_new == "old") {
                var final_domain = jQuery("span#final_domain_erp").data("domain");
            }
            else {
                var final_domain = jQuery( "span#final_domain_other" ).data( "domain" );
            }

            if (typeof final_domain == 'undefined') {
                var final_domain = jQuery("span#final_domain").data("domain");
            }

            if (typeof final_domain == 'undefined') {
                $('.add_info_email_general').hide();

                alert("Could not add info@ email address automatically, because there is no final domain found.");

                return;
            }

            //Strip www. from final domain
            final_domain = final_domain.replace("www.", "");

            var already_in_input = false;

            console.log("info@" + final_domain);

            if (jQuery("input[name=email_sirelo]").val().indexOf("info@" + final_domain) >= 0) {
                already_in_input = true;
            }

            if (jQuery("input[name=email_sirelo]").val().length > 0 && !already_in_input) {
                jQuery("input[name=email_sirelo]").val(jQuery("input[name=email_sirelo]").val() + ",info@" + final_domain);

            }
            else {
                jQuery("input[name=email_sirelo]").val("info@" + final_domain).trigger("change");
            }

            $('.add_info_email_sirelo').hide();
        });

        jQuery("#show_history, #hide_history").click(function() {
            jQuery("#google_history").toggle();
            jQuery("#show_history").toggle();
            jQuery("#hide_history").toggle();

        });

        jQuery("button[name=update_and_next]").click(function() {
            if (!jQuery("input[name=use_reviews]").is(":checked") && jQuery("input[type=radio][value=new]").is(":checked")) {
                if (!confirm("One or more fields from the Google Data is selected, but 'Use reviews' is turned off. Do you want to continue?")) {
                    return false;
                }
            }
        });

    </script>
@endpush
