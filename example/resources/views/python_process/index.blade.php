@extends('layouts.backend')
@include('scripts.datepicker')
@include('scripts.datatables')

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Validation</h1>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">

        <div class="bg-sidebar-dark p-3 rounded push">
            <!-- Toggle navigation -->
            <div class="d-lg-none">
                <!-- Class Toggle, functionality initialized in Helpers.coreToggleClass() -->
                <button type="button" class="btn btn-block btn-dark d-flex justify-content-between align-items-center" data-toggle="class-toggle" data-target="#horizontal-navigation-hover-normal-dark" data-class="d-none">
                    Menu
                    <i class="fa fa-bars"></i>
                </button>
            </div>
            <!-- End toggle navigation -->

            <!-- Navigation -->
            <div id="horizontal-navigation-hover-normal-dark" class="d-none d-lg-block mt-2 mt-lg-0">
                <ul class="nav-main nav-main-horizontal nav-main-hover nav-main-dark">
                    <li class="nav-main-item">
                        <a class="nav-main-link" href="/validate_gathered_data">
                            <i class="nav-main-link-icon fa fa-rocket"></i>
                            <span class="nav-main-link-name">Google ({{$count_google}})</span>
                        </a>
                    </li>
                    <li class="nav-main-item">
                        <a class="nav-main-link" href="/mobilityex_validate_gathered_data">
                            <i class="nav-main-link-icon fa fa-rocket"></i>
                            <span class="nav-main-link-name">MobilityEx ({{$count_mobilityex}})</span>
                        </a>
                    </li>
                    <li class="nav-main-item">
                        <a class="nav-main-link" href="/validate_related_moving_companies">
                            <i class="nav-main-link-icon fa fa-rocket"></i>
                            <span class="nav-main-link-name">Related moving companies ({{$count_related}})</span>
                        </a>
                    </li>
                </ul>
            </div>

        </div>

    </div>
@endsection
