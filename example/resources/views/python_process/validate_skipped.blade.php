@extends('layouts.backend')

@include( 'scripts.datatables' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Validate skipped data</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url( 'dashboard' )}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{url( 'validate_gathered_data' )}}">Validate gathered data</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Skipped data</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <div class="block block-rounded block-bordered">
            <div class="block block-rounded block-bordered">
                <div class="block-content">
                    <div class="block-content block-content-full">
                        <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Customer ID</th>
                                <th>Customer</th>
                                <th>Platform</th>
                                <th>Country</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($curesc_rows as $row)
                                <tr>
                                    <td>{{$row->curesc_id}}</td>
                                    <td>{{$row->curesc_cu_id}}</td>
                                    <td>{{$row->cu_company_name_business}}</td>
                                    <td>{{$row->curesc_platform}}</td>
                                    <td>{{\App\Functions\Data::country($row->cu_co_code, "EN")}}</td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <a class="btn btn-sm btn-primary"
                                               data-toggle="tooltip"
                                               data-placement="left"
                                               title="edit"
                                               href="{{ url('validate_skipped_data/' .  $row->curesc_id  . '/validate')}}">
                                                <i class="fa fa-pencil-alt"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
