@extends('layouts.backend')
@include('scripts.select2')

@include( 'scripts.datatables' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Potential new Movers</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url( 'dashboard' )}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{url( 'validate_related_moving_companies/' )}}">Related moving companies</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Create</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        @if(session()->has('msg'))
            <div class="alert alert-warning">
                {{ session()->get('msg') }}
            </div>
        @endif


        <div class="block block-rounded block-bordered">
            <div class="block block-rounded block-bordered">
                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                {{$error}}<br>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="block-content">
                    <form id="create_related_form" method="post" enctype="multipart/form-data" action="{{action('PythonProcessController@storeRelatedMovingCompany', $curesc->curesc_id)}}">
                        @csrf
                        <input name="_method" type="hidden" value="post">
                        <input name="curesc_id" type="hidden" value="{{$curesc->curesc_id}}">
                        <input name="co_code" type="hidden" value="{{$selected_co_code}}">

                        <h2 class="content-heading pt-0">General Information <a class='ml-5 btn btn-sm btn-outline-primary' href='{{$curesc->curesc_search_url}}' target='_blank'>Search URL</a></h2>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label" for="for-la_code">Language:</label>
                            <div class="col-sm-7">
                                <select id="for-la_code" class="js-select2 form-control {{$errors->has('la_code') ? 'is-invalid' : ''}}" name="la_code" data-placeholder="Choose one.." style="width:100%;" data-minimum-results-for-search="Infinity">
                                    <option></option>
                                    @foreach($languages as $language)
                                        <option @if(!empty($selected_co_code) && in_array($selected_co_code, ["DE", "EN", "FR", "IT", "DK", "ES", "NL", "PL", "RU", "PT"])) @if ($selected_co_code == $language->la_code) selected @endif @else @if ("EN" == $language->la_code) selected @endif @endif value="{{$language->la_code}}">{{$language->la_language}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label" for="for-description">Description:</label>
                            <div class="col-sm-7">
                                <textarea id="for-description" rows="6" class="form-control" name="description">
                                    @if (!empty(Request::old('description'))) {{Request::old('description')}} @else {{$curesc->curesc_description}} @endif
                                </textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label" for="for-company_name_legal">Company legal name:</label>
                            <div class="col-sm-7">
                                <input id="for-company_name_legal" type="text" class="form-control {{$errors->has('company_name_legal') ? 'is-invalid' : ''}}" name="company_name_legal" @if (!empty(Request::old('company_name_legal'))) value="{{Request::old('company_name_legal')}}" @else value="{{$curesc->curesc_name}}" @endif>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label" for="for-company_name_business">Company business name:</label>
                            <div class="col-sm-7">
                                <input id="for-company_name_business" type="text" class="form-control {{$errors->has('company_name_business') ? 'is-invalid' : ''}}" name="company_name_business" @if (!empty(Request::old('company_name_business'))) value="{{Request::old('company_name_business')}}" @else value="{{$curesc->curesc_name}}" @endif >
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label" for="for-attn">Primary contact:</label>
                            <div class="col-sm-7">
                                <input id="for-attn" type="text" class="form-control {{$errors->has('attn') ? 'is-invalid' : ''}}" name="attn" value="{{Request::old('attn')}}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label" for="for-attn_email">Primary contact email:</label>
                            <div class="col-sm-7">
                                <input id="for-attn_email" type="text" class="form-control {{$errors->has('attn_email') ? 'is-invalid' : ''}}" name="attn_email" value="{{Request::old('attn_email')}}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label" for="coc">Chamber of commerce:</label>
                            <div class="col-sm-7">
                                <input id="coc" type="text" class="form-control {{$errors->has('coc') ? 'is-invalid' : ''}}" name="coc" value="{{Request::old('coc')}}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label"
                                for="logo">Logo:</label>
                            <div class="col-sm-3">
                                <input type="file" name="logo" class="form-control-file btn btn-primary"/>
                            </div>
                        </div>

                        <h2 class="content-heading pt-0">Emails</h2>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label" for="leads_email">Lead email:</label>
                            <div class="col-sm-7">
                                <input id="leads_email" type="text" class="form-control {{$errors->has('leads_email') ? 'is-invalid' : ''}}" name="leads_email" value="{{Request::old('leads_email')}}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label" for="review_email">Review communication:</label>
                            <div class="col-sm-7">
                                <input id="review_email" type="text" class="form-control {{$errors->has('review_email') ? 'is-invalid' : ''}}" name="review_email" value="{{Request::old('review_email')}}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label" for="load_exchange_email">Load Exchange email:</label>
                            <div class="col-sm-7">
                                <input id="load_exchange_email" type="text" class="form-control {{$errors->has('load_exchange_email') ? 'is-invalid' : ''}}" name="load_exchange_email" value="{{Request::old('load_exchange_email')}}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label" for="general_info">General info:</label>
                            <div class="col-sm-7">
                                <input id="general_info" type="text" class="form-control {{$errors->has('general_info') ? 'is-invalid' : ''}}" name="general_info" value="{{Request::old('general_info')}}">
                            </div>
                        </div>

                        <h2 class="content-heading pt-0">Office Address</h2>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label" for="for-street_1">Company street 1:</label>
                            <div class="col-sm-7">
                                <input id="for-street_1" type="text" class="form-control {{$errors->has('street_1') ? 'is-invalid' : ''}}" name="street_1" @if (!empty(Request::old('street_1'))) value="{{Request::old('street_1')}}" @else value="{{$curesc->curesc_street}}" @endif>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label" for="for-street_2">Company street 2:</label>
                            <div class="col-sm-7">
                                <input id="for-street_2" type="text" class="form-control" name="street_2" value="{{Request::old('street_2')}}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label" for="for-zipcode">Company zipcode:</label>
                            <div class="col-sm-7">
                                <input id="for-zipcode" type="text" class="form-control {{$errors->has('zipcode') ? 'is-invalid' : ''}}" name="zipcode" @if (!empty(Request::old('zipcode'))) value="{{Request::old('zipcode')}}" @else value="{{$curesc->curesc_zipcode}}" @endif>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label" for="for-city">Company city:</label>
                            <div class="col-sm-7">
                                <input id="for-city" type="text" class="form-control {{$errors->has('city') ? 'is-invalid' : ''}}" name="city" @if (!empty(Request::old('city'))) value="{{Request::old('city')}}" @else value="{{$curesc->curesc_city}}" @endif>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label" for="for-country">Company country:</label>
                            <div class="col-sm-7">
                                <select id="for-country" class="js-select2 form-control {{$errors->has('country') ? 'is-invalid' : ''}}" name="country" data-placeholder="Choose one..">
                                    <option value=""></option>
                                    @foreach($countries as $country)
                                        @if (!empty(Request::old('country')))
                                            <option @if (Request::old('country') == $country->co_code) selected @endif value="{{$country->co_code}}">{{$country->co_en}}</option>
                                        @else
                                            <option @if ($curesc->gaco_searched_co_code == $country->co_code) selected @endif value="{{$country->co_code}}">{{$country->co_en}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label" for="for-telephone">Company telephone:</label>
                            <div class="col-sm-7">
                                <input type="hidden" id="int_telephone" name="int_telephone" value="">
                                <input id="for-telephone" type="text" class="form-control {{$errors->has('telephone') ? 'is-invalid' : ''}}" name="telephone" @if (!empty(Request::old('telephone'))) value="{{Request::old('telephone')}}" @else value="{{$curesc->curesc_telephone}}" @endif>
                                <div id='telephone_not_valid' class='mt-1' style='font-size:12px; color:red; display: none;'>Phone number is not valid!</div>
                                <div id='telephone_changed' class='mt-1' style='font-size:12px; display: none;'>Changed from @if (!empty(Request::old('telephone'))) {{Request::old('telephone')}} @else {{$curesc->curesc_telephone}} @endif</div>
                            </div>
                        </div>

                        <!-- telephone_not_valid,telephone_changed -->


                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label" for="for-website">Company website:</label>
                            <div class="col-sm-5">
                                <input id="for-website" type="text" class="form-control {{$errors->has('website') ? 'is-invalid' : ''}}" name="website" @if (!empty(Request::old('website'))) value="{{Request::old('website')}}" @else value="{{$curesc->curesc_customer_website}}" @endif>

                                <div id='website_validation_html' style='display: none;'>
                                    @if (!empty($curesc->curesc_new_company_website_validation_html))
                                        {!! $curesc->curesc_new_company_website_validation_html !!}
                                    @endif
                                </div>
                                <div id='automatically_changed' class='mt-2' style='color:red; display: none;'>
                                    Automatically changed from {{$curesc->curesc_customer_website}} <a target='_blank' href='{{$curesc->curesc_customer_website}}'>🔗</a>
                                </div>
                            </div>
                            <div class='col-sm-1 mt-2'><a id='link_to_website' target='_blank' href='{{$curesc->curesc_customer_website}}'><i class='fa fa-external-link-alt'> </i></a></div>
                        </div>

                        <h2 class="content-heading pt-0">Other</h2>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label"
                                for="associations">Associations:</label>
                            <div class="col-sm-4">
                                <select type="text" multiple class="js-select2 form-control"
                                    name="associations[]">
                                    <option value=""></option>
                                    @foreach($memberships as $memb)
                                        <option value="{{$memb->me_id}}" @if(in_array($memb->me_id, Request::old("associations"))) selected @endif>{{$memb->me_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label"
                                for="fields_region">Region:</label>
                            <div class="col-sm-4">
                                <select type="text" class="js-select2 form-control"
                                    name="fields_region">
                                    <option value=""></option>
                                    @foreach($regions as $region)
                                        <option
                                            value="{{$region->reg_id}}" {{ ($customer->moverdata->moda_reg_id == $region->reg_id) ? 'selected':'' }}>{{$region->reg_parent}} {{"(".$region->co_en.")"}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label"
                                for="fields_market">Market:</label>
                            <div class="col-sm-4">
                                <select type="text" class="form-control"
                                    name="fields_market">
                                    <option value=""></option>
                                    @foreach($customermarkettypes as $id => $markettype)
                                        <option value="{{$id}}">{{$markettype}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label"
                                for="fields_services">Services:</label>
                            <div class="col-sm-4">
                                <select type="text" class="form-control"
                                    name="fields_services">
                                    <option value=""></option>
                                    @foreach($customerservices as $id => $service)
                                        <option value="{{$id}}" {{ ($customer->moverdata->moda_services == $id) ? 'selected':'' }}>{{$service}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label"
                                for="fields_status">Status:</label>
                            <div class="col-sm-4">
                                <select type="text" class="form-control"
                                    name="fields_status">
                                    <option value=""></option>
                                    @foreach($customerstatustypes as $id => $statustype)
                                        <option value="{{$id}}" {{ (2 == $id) ? 'selected':'' }}>{{$statustype}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label"
                                for="fields_owner">Owner:</label>
                            <div class="col-sm-4">
                                <select type="text" class="js-select2 form-control"
                                    name="fields_owner">
                                    <option value=""></option>
                                    @foreach($users as $user)
                                        <option value="{{$user->id}}" {{ (\Illuminate\Support\Facades\Auth::user()->us_id == $user->id) ? 'selected':'' }}>{{$user->us_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <h2 class="content-heading pt-0"></h2>

                        <div class="form-group">
                            <div style="float: left;">
                                <button name='nothing' type="submit" class="btn btn-danger">Remove this record</button>
                            </div>
                            <div style="text-align: center;">
                                <button id="add_customer_btn" name='add' type="submit" class="btn btn-success">Add Customer</button>
                                <button name='back' type="submit" class="btn btn-primary">Back</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            $("#create_related_form").submit(function (e) {
                $("#add_customer_btn").attr("disabled", "disabled");
            })

            function validatePhonenumber() {
                $.ajax({
                    type: "GET",
                    url: '{{ url('/ajax/validate_phone_numbers_adding_customer') }}',
                    data: {
                        "phone": jQuery("input[name=telephone]").val(),
                        "co_code": '{{$curesc->gaco_searched_co_code}}',
                    },
                    success: function(data){
                        //Parse JSON
                        var returnedData = JSON.parse(data);

                        console.log("Phone validated");
                        console.log(returnedData);

                        if (returnedData != null) {
                            if (returnedData.converted_phone_general == 'not_valid')
                            {
                                jQuery("#telephone_changed").hide();
                                jQuery("#telephone_not_valid").show();
                                jQuery("input[name=telephone]").css("border", "2px solid red");
                            }
                            else {
                                jQuery("input[name=telephone]").val(returnedData.converted_phone_general);
                                jQuery("input[name=telephone]").css("border", "2px solid #129a12ba");
                                jQuery("#telephone_not_valid").hide();
                                jQuery("#telephone_changed").show();
                            }
                        }
                    }
                });
            }

            validatePhonenumber();

            jQuery("input[name=telephone]").keyup( function() {
                validatePhonenumber();
            });

            jQuery("div#website_validation_html > div").removeClass("col-sm-6");
            jQuery("div#website_validation_html > div").addClass("col-sm-12");

            if (jQuery("div#website_validation_html > div").text().indexOf("redirect") >= 0) {
                jQuery("input[name=website]").val(jQuery("span#validated_url").data("url"));
                jQuery("a#link_to_website").attr("href", jQuery("span#validated_url").data("url"));
                jQuery("div#automatically_changed").show();
            }
        });
    </script>
@endpush
