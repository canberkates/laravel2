@extends('layouts.backend')

@include( 'scripts.datatables' )
@include('scripts.forms')
@include( 'scripts.dialogs' )
@include('scripts.select2')
@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Whitelist & Blacklist</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url( 'dashboard' )}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/validate_related_moving_companies">Validate moving companies</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Settings</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        <div class="block block-rounded block-bordered">
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Filter countries to see current white- and blacklist

                    <div style='float:right;'>
                        <a href='/validate_related_moving_companies/settings'><button class='btn btn-primary'>Settings</button></a>
                        <a  href='/validate_related_moving_companies/settings/countries'><button class='btn btn-primary'>Countries</button></a>
                    </div>
                </h3>
            </div>
            <div class="block-content block-content-full">

                <form class="mb-5" method="post" action="{{action('PythonProcessController@settingsRelatedMovingCompaniesWhiteAndBlacklistFiltered')}}">
                    @csrf
                    <input name="_method" type="hidden" value="post">

                    <div class="form-group row">
                        <label class="col-md-1 col-form-label ml-5" for="country">Country:</label>
                        <div class="col-md-3">
                            <select class="js-select2 form-control" data-placeholder="Select an option..."
                                name="country" id="country" style="width:100%;">
                                <option></option>
                                @foreach($countries as $co_code => $country)
                                    <option @if(isset($selected_co_code) && $co_code == $selected_co_code) selected @endif value="{{$co_code}}">{{$country['country']}}</option>
                                @endforeach
                            </select>
                            <i style='font-size:11px;'>If there are no titles white- or blacklisted for a country, then you will not see this country in the dropdown!</i>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="form-group col-md-8">
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        @isset($filtered_data)
            <div class="block block-rounded block-bordered">
                <div class="block-header block-header-default">
                    <h3 class="block-title">
                        Filtered data
                    </h3>
                </div>
                <div class="block-content block-content-full">
                    <h2>{{\App\Functions\Data::country($selected_co_code, "EN")}}</h2>

                    <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Whitelist</th>
                            <th>Blacklist</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($filtered_data as $row)
                            <tr>
                                <td>{{$row['title']}}</td>
                                <td class='whitelist_td text-center' style='font-size:10px;'><input data-title='{{$row['title']}}' data-country='{{$selected_co_code}}' class='whitelist_checkbox' style="transform: scale(1.8);" type="radio" name='white_or_black_{{$row['title']}}' value='1' @if ($row['white_black'] == 1) checked @endif></td>
                                <td class='blacklist_td text-center' style='font-size:10px;'><input data-title='{{$row['title']}}' data-country='{{$selected_co_code}}' class='blacklist_checkbox' style="transform: scale(1.8);" type="radio" name='white_or_black_{{$row['title']}}' value='2' @if ($row['white_black'] == 2) checked @endif></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        @endisset

    </div>
@endsection

@push( 'scripts' )
    <script>
        /*$('.whitelist_td').click(function () {
         var checkbox = jQuery(this).children("input:checkbox");

         checkbox.attr('checked', !checkbox.attr("checked"))
         checkbox.trigger("change");
         });*/

        $('.whitelist_checkbox').change(function () {
            var on_or_off = 0;

            if(jQuery(this).is(":checked")) {
                on_or_off = 1;
            }

            $.ajax({
                type: "GET",
                url: '{{ url('/ajax/put_on_whitelist') }}',
                data: {
                    "on_or_off": on_or_off,
                    "title": jQuery(this).data('title'),
                    //"country": jQuery(this).data('country')
                },
                success: function(data){
                    console.log(data);
                }
            });
        });

        $('.blacklist_checkbox').change(function () {
            var on_or_off = 0;

            if(jQuery(this).is(":checked")) {
                on_or_off = 1;
            }

            $.ajax({
                type: "GET",
                url: '{{ url('/ajax/put_on_blacklist') }}',
                data: {
                    "on_or_off": on_or_off,
                    "title": jQuery(this).data('title'),
                    //"country": jQuery(this).data('country')
                },
                success: function(data){
                    console.log(data);
                }
            });
        });

    </script>
@endpush
