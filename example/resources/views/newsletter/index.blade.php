@extends('layouts.backend')

@include( 'scripts.datatables' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Newsletters</h1>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <a href="/admin/newsletters/create"></a>

    <div class="content">

        <div class="block block-rounded block-bordered">
            <div class="block-content block-content-full">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Newsletters
                        <a href="{{ url('admin/newsletters/create')}}"><button data-toggle="click-ripple" class="btn btn-primary">Add a Newsletter</button></a>
                    </h3>
                </div>
                <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Delay</th>
                        <th>Modify</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($newsletters as $newsletter)
                        <tr>
                            <td>{{$newsletter->new_id}}</td>
                            <td>{{$newsletter->new_name}}</td>
                            <td>{{$newsletter->new_delay}}</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <a class="btn btn-sm btn-primary"
                                       data-toggle="tooltip"
                                       data-placement="left"
                                       title="edit"
                                       href="{{ url('/admin/newsletters/' . $newsletter->new_id . '/edit')}}">
                                        <i class="fa fa-pencil-alt"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
