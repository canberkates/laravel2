@extends('layouts.backend')

@include('scripts.datepicker')

@section('content')

    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Edit a newsletter</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url("newsletters")}}">Newsletters</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="block block-rounded block-bordered">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Edit newsletter</h3>
                    </div>
                    <div class="block-content">
                        @if($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        {{$error}}<br>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="mb-5" method="post" action="{{action('NewsletterController@update', $newsletter->new_id)}}">
                            @csrf
                            <input name="_method" type="hidden" value="PATCH">

                            <h2 class="content-heading pt-0">General</h2>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="value">Name:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="name" value="{{$newsletter->new_name}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="extra">Delay:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="delay" value="{{$newsletter->new_delay}}">
                                </div>
                            </div>

                            <h2 class="content-heading pt-0">Subject singular</h2>

                            @foreach($languages as $language)
                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="extra">Subject ({{$language->la_language}}):</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="subject_singular[{{$language->la_code}}]" value="{{$singular[$language->la_code]}}">
                                    </div>
                                </div>
                            @endforeach

                            <h2 class="content-heading pt-0">Subject plural</h2>

                            @foreach($languages as $language)
                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="extra">Subject ({{$language->la_language}}):</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="subject_plural[{{$language->la_code}}]" value="{{$plural[$language->la_code]}}">
                                    </div>
                                </div>
                            @endforeach

                            <h2 class="content-heading pt-0">Intro</h2>

                            @foreach($languages as $language)
                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="extra">Intro ({{$language->la_language}}):</label>
                                    <div class="col-sm-7">
                                        <textarea rows="5" class="form-control" name="intro[{{$language->la_code}}]">{{$intro[$language->la_code]}}</textarea>
                                    </div>
                                </div>
                            @endforeach

                            <h2 class="content-heading pt-0">Slots</h2>

                            @for ($i = 1; $i <= 10; $i++)
                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="extra">Slot {{$i}}:</label>
                                    <div class="col-sm-3">
                                        <select type="text" class="form-control" name="slot_{{$i}}">
                                            <option value=""></option>
                                            @foreach($slots as $id => $value)
                                                <option value="{{$id}}" {{ ($newsletter['new_slot_'.$i] == $id) ? 'selected': '' }}>{{$value}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @endfor


                            <h2 class="content-heading pt-0"></h2>

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection




