@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Date</th>
                    <th>Email</th>
                    <th>Phone Number</th>
                    <th>Passport Office</th>
                </tr>
                </thead>
                <tbody>
                @foreach($moverdata as $mover)
                    <tr>
                        <td>{{$mover->moda_id}}</td>
                        <td>{{$mover->moda_cu_id}}</td>
                        <td>{{$mover->customer->cu_company_name_business}}</td>
                        <td>{{$mover->customer->cu_website}}</td>
                        <td>{{$mover->customer->cu_email}}</td>
                        <td>{{$mover->moda_info_email}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
