@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboardje</div>

                    <div class="card-body">
                        {{$moverdata->moda_info_email}}  {{$moverdata->customer->cu_company_name_business}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
