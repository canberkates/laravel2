@extends('layouts.backend')

@include('scripts.select2')

@section('content')
    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="block block-rounded block-bordered">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Edit Survey 1 @if(!empty($survey->su_mover)) of {{$survey->customer->cu_company_name_business}} @endif</h3>
                    </div>
                    <div class="block-content">
                        <form class="mb-5" method="post" action="{{action('Survey1Controller@update', $survey->su_id)}}">
                            @csrf
                            <input name="_method" type="hidden" value="PATCH">

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label">Status:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text" value="{{$surveystatus}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label">Sent:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text"  value="{{$survey->su_timestamp}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label">Opened:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text" value="{{$survey->su_opened_timestamp}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label">Submitted:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text" value="{{$survey->su_submitted_timestamp}}">
                                </div>
                            </div>

                            <h2 class="content-heading pt-0">Request</h2>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label">ID:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text" value="{{$survey->request->re_id}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label">Timestamp:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text" value="{{$survey->request->re_timestamp}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label">Portal:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text" value="{{$survey->request->requestcustomerportal->portal->po_portal}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label">Source:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text"  value="{{$requestsource}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label">Form:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text" value="{{$survey->request->websiteform->website->we_website}} ({{$survey->request->websiteform->wefo_name}})">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label">Language:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text" value="{{$requestlanguage}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label">Device:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text" value="{{$device}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label">IP address:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text" value="{{$survey->request->re_ip_address}}">
                                </div>
                            </div>

                            <h2 class="content-heading pt-0">Move</h2>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label">City from:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text" value="{{$survey->request->re_city_from}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label">Country from:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text" value="{{$survey->request->countryfrom->co_en}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label">City to:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text" value="{{$survey->request->re_city_to}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label">Country to:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text" value="{{$survey->request->countryto->co_en}}">
                                </div>
                            </div>

                            <h2 class="content-heading pt-0">Contact Details</h2>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label">Company name:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text" value="{{$survey->request->re_company_name}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label">Name:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text" value="{{$survey->request->re_full_name}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label">Telephone 1:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text" value="{{$survey->request->re_telephone1}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label">Telephone 2:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text"  value="{{$survey->request->re_telephone2}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label">E-mail:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text" value="{{$survey->request->re_email}}">
                                </div>
                            </div>

                            <h2 class="content-heading pt-0">Customers</h2>

                            @foreach($survey->survey1customers as $customer)

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="remark">Remark:</label>
                                <div class="col-sm-7">
                                    <textarea class="form-control" type="text" name="remark">{{$customer->customer->cu_company_name_business}}</textarea>
                                </div>
                            </div>

                            @endforeach

                            <h2 class="content-heading pt-0">Remark</h2>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="remark">Remark:</label>
                                <div class="col-sm-7">
                                    <textarea class="form-control" type="text" name="remark">{{$survey->su_checked_remarks}}</textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="send_email">Send e-mail</label>
                                <div class="col-sm-3">
                                    <select type="text" class="form-control" name="send_email">
                                        <option></option>
                                        @foreach($recommend as $id => $value)
                                            <option value="{{$id}}" {{ ($survey->su_send_email == $id) ? 'selected':'' }}>{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <h2 class="content-heading pt-0"></h2>

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" name="publish" class="btn btn-primary">Publish</button>
                                    <button type="submit" name="reject" class="btn btn-primary">Reject</button>
                                    <button type="submit" name="place_on_hold" class="btn btn-primary">Place on hold</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection




