@extends('layouts.backend')

@include( 'scripts.datatables' )
@include( 'scripts.dialogs' )
@include( 'scripts.datepicker' )


@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Match Summary</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Messages</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="col-md-6 block block-rounded block-bordered">
            <div class="block-content ">
                <form method="post" action="{{action('MatchSummaryController@filteredindex')}}">
                    @csrf
                    <input name="_method" type="hidden" value="post">
                    <h2 class="content-heading pt-0">Filters</h2>

                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label" for="show_hours">Date:</label>
                        <div class="col-sm-9">
                            <div class="input-daterange input-group" data-date-format="yyyy-mm-dd" data-week-start="1"
                                 data-autoclose="true" data-today-highlight="true">
                                <input autocomplete="off" value="{{$date_from}}" type="text" class="form-control"
                                       id="example-daterange1" name="date_from" placeholder="From" data-week-start="1"
                                       data-autoclose="true" data-today-highlight="true">
                                <div class="input-group-prepend input-group-append">
                                <span class="input-group-text font-w600">
                                    <i class="fa fa-fw fa-arrow-right"></i>
                                </span>
                                </div>
                                <input autocomplete="off" value="{{$date_to}}" type="text" class="form-control"
                                       id="example-daterange2" name="date_to" placeholder="To" data-week-start="1"
                                       data-autoclose="true" data-today-highlight="true">
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label"
                               for="show_hours">Show hours:</label>
                        <div class="col-sm-9">
                            <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                <input type="checkbox" class="custom-control-input"
                                       id="show_hours"
                                       name="show_hours" @if(isset($show_hours) && $show_hours == "on") checked @endif>
                                <label class="custom-control-label"
                                       for="show_hours"></label>
                            </div>
                        </div>
                    </div>

                    <h2 class="content-heading pt-0"></h2>

                    <div class="row">
                        <div class="form-group col-md-2">
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="block block-rounded block-bordered">
            <div class="block-content block-content-full">
                <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                    <thead>
                    <tr>
                        <th>Employee</th>
                        <th>Total</th>
                        <th>Matched</th>
                        <th>Rejected</th>
                        <th>Rated</th>
                        <th>Good</th>
                        <th>Minor mistake</th>
                        <th>Major mistake</th>
                        <th>Called</th>
                        <th>Reached</th>
                        <th>Not Reached</th>

                        @if(isset($show_hours) && $show_hours == "on")
                            @foreach(range(0, 23) as $hour)
                                <th>{{$hour}}</th>
                            @endforeach
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($userratings as $user)
                        <tr>
                            <td>{{$user['name']}}</td>
                            <td>{{$user['total']}}</td>
                            <td>{{$user['matched']}}</td>
                            <td>{{$user['rejected']}}</td>
                            <td>{{$user['rated']}}</td>
                            <td>{{$user['type'][1]}}</td>
                            <td>{{$user['type'][2]}}</td>
                            <td>{{$user['type'][3]}}</td>
                            <td>{{$user['called']}}</td>
                            <td>{{$user['called_reached']}}</td>
                            <td>{{$user['called_not_reached']}}</td>

                            @if(isset($show_hours) && $show_hours == "on")
                                @foreach(range(0, 23) as $hour)
                                    <td>{{$user['hours_'.$hour]}}</td>
                                @endforeach
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                        <th></th>
                        <th>{{number_format($totals['total'], 0, ",", ".")}}</th>
                        <th>{{number_format($totals['matched'], 0, ",", ".")}}</th>
                        <th>{{number_format($totals['rejected'], 0, ",", ".")}}</th>
                        <th>{{number_format($totals['rated'], 0, ",", ".")}}</th>
                        <th>{{number_format($totals['good'], 0, ",", ".")}}</th>
                        <th>{{number_format($totals['minor'], 0, ",", ".")}}</th>
                        <th>{{number_format($totals['major'], 0, ",", ".")}}</th>
                        <th>{{number_format($totals['called'], 0, ",", ".")}}</th>
                        <th>{{number_format($totals['reached'], 0, ",", ".")}}</th>
                        <th>{{number_format($totals['not_reached'], 0, ",", ".")}}</th>
                        @if(isset($show_hours) && $show_hours == "on")
                            @foreach($total_hours as $hour)
                                <th>{{$hour}}</th>
                            @endforeach
                        @endif
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection

