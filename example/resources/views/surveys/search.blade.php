@extends('layouts.backend')

@include( 'scripts.datatables' )
@include( 'scripts.datepicker' )
@include('scripts.select2')

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Review search</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url( 'dashboard' )}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{url( 'surveys' )}}">Reviews</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Review search</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        <form method="post" action="{{action('SurveysController@searchResult')}}">
            @csrf
            <input name="_method" type="hidden" value="post">

            <div class="col-md-6 block block-rounded block-bordered">
                <div class="block-content">
                    <input name="_method" type="hidden" value="post">
                    <h2 class="content-heading pt-0">Filters</h2>

                    <div class="form-group row">
                        <label class="col-md-4 col-form-label" for="for-date">Date range:</label>
                        <div class="col-md-8">
                            <input type="text" autocomplete="off" class="form-control drp drp-default" name="date" value="{{$selected_date}}" id="for-date" />
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4 col-form-label">Checked:</label>
                        <div class="col-md-8">
                            <select class="form-control" name="checked" style="width:100%;">
                                @foreach($bothyesno as $id => $X)
                                    <option value="{{$id}}" @if ($id === $selected_checked) selected @endif>{{$X}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4 col-form-label">Review ID:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="survey_id" value="{{$selected_survey_id}}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4 col-form-label">Website Review ID:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="website_review_id" value="{{$selected_website_review_id}}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4 col-form-label">Request ID:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="request_id" value="{{$selected_request_id}}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4 col-form-label" for="for-type">Mover:</label>
                        <div class="col-md-8">
                            <select class="js-select2 form-control"
                                    name="mover" style="width:100%;">
                                <option value="-1">[No Mover]</option>
                                @foreach($movers as $mover)
                                    <option value="{{$mover->cu_id}}"  @if ($mover->cu_id == $selected_mover) selected @endif>{{$mover->cu_company_name_business}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4 col-form-label" for="for-type">Moved:</label>
                        <div class="col-md-8">
                            <select class="form-control"
                                    name="moved" style="width:100%;">
                                @foreach($bothyesno as $id => $X)
                                    <option value="{{$id}}"  @if ($id === $selected_moved) selected @endif>{{$X}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4 col-form-label" for="for-type">Show on website:</label>
                        <div class="col-md-8">
                            <select class="form-control"
                                    name="show_on_website" style="width:100%;">
                                @foreach($bothyesno as $id => $X)
                                    <option value="{{$id}}" @if ($id === $selected_show_on_website) selected @endif>{{$X}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4 col-form-label" for="for-type">Recommended mover:</label>
                        <div class="col-md-8">
                            <select class="form-control"
                                    name="recommended" style="width:100%;">
                                @foreach($bothyesno as $id => $X)
                                    <option value="{{$id}}" @if ($id === $selected_recommended) selected @endif>{{$X}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>


                    <div class="form-group row">
                        <label class="col-md-4 col-form-label" for="for-type">Rating</label>
                        <div class="col-md-8">
                            <select id="for-type"
                                    class="js-select2 form-control {{$errors->has('type') ? 'is-invalid' : ''}}"
                                    name="rating" data-placeholder="Choose one.." style="width:100%;"
                                    data-minimum-results-for-search="Infinity">
                                @foreach($ratings as $id => $X)
                                    <option  value="{{$id}}" @if ($id == $selected_rating) selected @endif>{{$X}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4 col-form-label" for="for-type">Experience blog:</label>
                        <div class="col-md-8">
                            <select class="form-control"
                                    name="experience_blog" style="width:100%;">
                                @foreach($bothyesno as $id => $X)
                                    <option value="{{$id}}" @if ($id === $selected_experience_blog) selected @endif>{{$X}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4 col-form-label" for="proof_files">Proof files:</label>
                        <div class="col-md-8">
                            <select class="form-control"
                                    name="proof_files" style="width:100%;">
                                @foreach($bothyesno as $id => $X)
                                    <option value="{{$id}}" @if (strval($id) == $selected_proof_files) selected @endif>{{$X}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4 col-form-label" for="email">Email:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="email" value="{{$selected_email}}">
                        </div>
                    </div>


                    <h2 class="content-heading pt-0"></h2>

                    <div class="row">
                        <div class="form-group col-md-2">
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div>
                    </div>
                </div>
            </div>

        </form>

        <div class="block block-rounded block-bordered">
            <div class="block-content block-content-full">
                <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Source</th>
                            <th>Ref. ID</th>
                            <th>Submitted</th>
                            <th>Author</th>
                            <th>Rating</th>
                            <th>Mover</th>
                            <th>Published</th>
                            <th>Edit</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($surveys as $row)
                        <tr>
                            <td>{{$row->su_id}}</td>
                            <td>{{$surveysources[$row->su_source]}}</td>
                            @if($row->su_source == 1)
                                <td>{{$row->re_id}}</td>
                            @else
                                <td>{{$row->were_id}}</td>
                            @endif
                            <td>{{$row->su_submitted_timestamp}}</td>
                            @if($row->su_source == 1)
                                <td>{{$row->re_full_name}}</td>
                            @else
                                <td>{{$row->were_name}}</td>
                            @endif
                            <td>{{$row->su_rating}}</td>
                            <td>{{$row->cu_company_name_business}}</td>
                            <td>{{($row->su_show_on_website == 1 ? "Yes" : "No")}}</td>
                            @if($row->su_submitted)
                            <td class="text-center">
                                <div class="btn-group">
                                    <a class="btn btn-sm btn-primary"
                                       data-toggle="tooltip"
                                       data-placement="left"
                                       title="edit"
                                       href="{{ url('customers/' .  ($row->su_mover ?? $row->su_other_mover ?? 0) . '/survey2/' . $row->su_id . '/edit')}}">
                                        <i class="fa fa-pencil-alt"></i>
                                    </a>
                                </div>
                            </td>
                            @else
                                <td class="text-center">
                                    <div class="btn-group">
                                        <i class="fa fa-2x fa-times"  style="color:red;"></i>
                                    </div>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
