@extends('layouts.backend')

@include( 'scripts.datatables' )
@include( 'scripts.dialogs' )

@section('content')
    <!-- Page Content -->
    <div class="content">
        <!-- Your Block -->
        <div class="block block-rounded block-bordered">
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Approving Flagged Review of ID: {{$flagged_review->su_id}}
                </h3>
            </div>
            <div class="block-content block-content-full">
                <form method="post"
                      action="{{action('SurveysController@approveFlaggedReview', [$flagged_review->flre_id])}}">
                    @csrf
                    <input name="_method" type="hidden" value="post">
                    <div class="row">
                        <div class="col-md-6">

                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label" for="personal_information"></label>
                                <div class="col-sm-9">
                                    <a class="btn btn-sm btn-primary"
                                       data-toggle="tooltip"
                                       target="_blank"
                                       data-placement="left"
                                       title="View"
                                       href="{{ url('customers/' .  ($flagged_review->su_mover ?? "0")  . '/survey2/' . $flagged_review->su_id . '/edit')}}">
                                        Click here to view the survey <i class="fa fa-eye"></i>
                                    </a>
                                </div>
                            </div>

                            @if ($flagged_review->flre_personal_information !== null)
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label" for="personal_information">Personal information:</label>
                                    <div class="col-sm-9">
                                        <textarea disabled style="height: 100px;" class="form-control" name="personal_information" id="personal_information">{{$flagged_review->flre_personal_information}}</textarea>
                                    </div>
                                </div>
                            @endif

                            @if ($flagged_review->flre_spam !== null)
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label" for="spam">Spam:</label>
                                    <div class="col-sm-9">
                                        <textarea disabled style="height: 100px;" class="form-control" name="spam" id="spam">{{$flagged_review->flre_spam}}</textarea>
                                    </div>
                                </div>
                            @endif

                            @if ($flagged_review->flre_offensive_language !== null)
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label" for="offensive_language">Offensive Language:</label>
                                    <div class="col-sm-9">
                                        <textarea disabled style="height: 100px;" class="form-control" name="offensive_language" id="offensive_language">{{$flagged_review->flre_offensive_language}}</textarea>
                                    </div>
                                </div>
                            @endif

                            @if ($flagged_review->flre_conflicts !== null)
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label" for="conflicts">Conflicts:</label>
                                    <div class="col-sm-9">
                                        <textarea disabled style="height: 100px;" class="form-control" name="conflicts" id="conflicts">{{$flagged_review->flre_conflicts}}</textarea>
                                    </div>
                                </div>
                            @endif

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button value="1" name="accept" type="submit" class="btn btn-primary">Accept</button>
                                    <button value="1" name="deny" type="submit" class="btn btn-primary">Deny</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END Your Block -->
    </div>
    <!-- END Page Content -->
@endsection
