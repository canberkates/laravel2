@extends('layouts.backend')

@include( 'scripts.datatables' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Reviews</h1>
                <a href="{{ url('/surveys/search')}}"><button data-toggle="click-ripple" class="btn btn-primary">Review search</button></a>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url( 'dashboard' )}}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Reviews</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <div class="block block-rounded block-bordered">
            <div class="block block-rounded block-bordered">
                <ul class="nav nav-tabs nav-tabs-block js-tabs-enabled" data-toggle="tabs"
                    role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active show" href="#btabs-static-survey2_active">Active ({{sizeof($surveys2['active'])}})</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#btabs-static-survey2_on_hold">On hold ({{sizeof($surveys2['on_hold'])}})</a>
                    </li>
                    @can('flagged reviews')
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-static-survey2_reported">Reported ({{sizeof($surveys2['flagged_reviews'])}})</a>
                        </li>
                    @endcan
                    @can('reviews - mover comments')
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-static-survey2_mover_comments">Mover comments ({{sizeof($surveys2['mover_comments'])}})</a>
                        </li>
                    @endcan
                </ul>
                <div class="block-content tab-content">
                    <div class="tab-pane active show" id="btabs-static-survey2_active" role="tabpanel">
                        <div class="block-content block-content-full">
                            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Source</th>
                                    <th>Request / Website ID</th>
                                    <th>Sent</th>
                                    <th>Submitted</th>
                                    <th>Author</th>
                                    <th>Rating</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($surveys2['active'] as $survey2)
                                    <tr>
                                        <td>{{$survey2->su_id}}</td>
                                        <td>{{$survey2sources[$survey2->su_source]}}</td>
                                        <td>{{($survey2->su_source == 1 ? $survey2->su_re_id : $survey2->su_were_id)}}</td>
                                        <td>{{$survey2->su_timestamp}}</td>
                                        <td>{{$survey2->su_submitted_timestamp}}</td>
                                        <td>{{($survey2->su_source == 1 ? $survey2->request->re_full_name : $survey2->website_review->were_name)}}</td>
                                        <td>
                                            @for($i = 0; $i < 5; $i++)
                                                <img src="/media/icons/icon_star_{{($i < $survey2->su_rating ? "on" : "off")}}.png" />{{($i < 4 ? " " : "")}}
                                            @endfor
                                        </td>
                                        <td class="text-center">
                                            <div class="btn-group">
                                                <a class="btn btn-sm btn-primary"
                                                   data-toggle="tooltip"
                                                   data-placement="left"
                                                   title="edit"
                                                   href="{{ url('customers/' .  ($survey2->su_mover ?? "0")  . '/survey2/' . $survey2->su_id . '/edit')}}">
                                                <i class="fa fa-pencil-alt"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane" id="btabs-static-survey2_on_hold" role="tabpanel">
                        <div class="block-content block-content-full">
                            <table data-order='[[1, "asc"]]' class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Source</th>
                                    <th>Request / Website ID</th>
                                    <th>Sent</th>
                                    <th>Submitted</th>
                                    <th>Author</th>
                                    <th>Rating</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($surveys2['on_hold'] as $survey2)
                                    <tr>
                                        <td>{{$survey2->su_id}}</td>
                                        <td>{{$survey2sources[$survey2->su_source]}}</td>
                                        <td>{{($survey2->su_source == 1 ? $survey2->su_re_id : $survey2->su_were_id)}}</td>
                                        <td>{{$survey2->su_timestamp}}</td>
                                        <td>{{$survey2->su_submitted_timestamp}}</td>
                                        <td>{{($survey2->su_source == 1 ? $survey2->request->re_full_name : $survey2->website_review->were_name)}}</td>
                                        <td>
                                            @for($i = 0; $i < 5; $i++)
                                                <img src="/media/icons/icon_star_{{($i < $survey2->su_rating ? "on" : "off")}}.png" />{{($i < 4 ? " " : "")}}
                                            @endfor
                                        </td>
                                        <td class="text-center">
                                            <div class="btn-group">
                                                <a class="btn btn-sm btn-primary"
                                                   data-toggle="tooltip"
                                                   data-placement="left"
                                                   title="edit"
                                                   href="{{ url('customers/' .  ($survey2->su_mover ?? "0")  . '/survey2/' . $survey2->su_id . '/edit')}}">
                                                    <i class="fa fa-pencil-alt"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    @can('flagged reviews')
                        <div class="tab-pane" id="btabs-static-survey2_reported" role="tabpanel">
                            <div class="block-content block-content-full">
                                <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                    <thead>
                                    <tr>
                                        <th>Survey ID</th>
                                        <th>Timestamp</th>
                                        <th>Source</th>
                                        <!--<th>Message</th>-->
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($surveys2['flagged_reviews'] as $survey2)
                                        <tr>
                                            <td>{{$survey2->su_id}}</td>
                                            <td>{{$survey2->flre_timestamp}}</td>
                                            <td>{{$survey2sources[$survey2->su_source]}}</td>
                                            <td class="text-center">
                                                <div class="btn-group">
                                                    <a class="btn btn-sm btn-primary"
                                                       data-toggle="tooltip"
                                                       data-placement="left"
                                                       title="View"
                                                       href="{{ url('flagged_review/'.$survey2->flre_id)}}">
                                                        <i class="fa fa-eye"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    @endcan
                    @can('reviews - mover comments')
                        <div class="tab-pane" id="btabs-static-survey2_mover_comments" role="tabpanel">
                            <div class="block-content block-content-full">
                                <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                    <thead>
                                    <tr>
                                        <th>Survey ID</th>
                                        <th>Timestamp</th>
                                        <th>Mover</th>
                                        <th>Type</th>
                                        <th>Comment</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($surveys2['mover_comments'] as $survey2)
                                        <tr>
                                            <td>{{$survey2->su_id}}</td>
                                            <td>{{$survey2->su_mover_comment_timestamp}}</td>
                                            <td>{{$survey2->cu_company_name_business}}</td>
                                            <td>@if($survey2->ktmoco_type == 1) {{"New"}} @else {{"Edited"}} @endif</td>
                                            <td>{{$survey2->su_mover_comment}}</td>
                                            <td class="text-center">
                                                <div class="btn-group">
                                                    <a class="btn btn-sm btn-primary" href="/customers/{{$survey2->ktmoco_cu_id}}/survey2/{{$survey2->su_id}}/edit"><i class="fa fa-eye"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    @endcan
                </div>

            </div>
        </div>
    </div>
@endsection

@push( 'scripts' )
    <script>

        jQuery(document).ready(function(){

            jQuery( document ).on( 'click', '.mark_as_read', function(e) {

                e.preventDefault();

                var $self = jQuery(this);

                jQuery.ajax({
                    url: "/ajax/mover_comments/read",
                    method: 'get',
                    data: {id: $self.data('id')},
                    success: function (data) {
                        console.log(data);
                        $self.parents( '.dataTable' ).DataTable().row( $self.parents( 'tr' ) ).remove().draw( 'page' );
                    }
                });
            });
        });
    </script>

@endpush
