@extends('layouts.backend')

@include('scripts.datepicker')

@section('content')

    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Edit a question</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url("questions")}}">Questions</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="block block-rounded block-bordered">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Edit question</h3>
                    </div>
                    <div class="block-content">
                        @if($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        {{$error}}<br>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="mb-5" method="post"
                              action="{{action('QuestionsController@update', $question->qu_id)}}">
                            @csrf
                            <input name="_method" type="hidden" value="PATCH">

                            <h2 class="content-heading pt-0">General</h2>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="value">Name:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="name" value="{{$question->qu_name}}">
                                </div>
                            </div>

                            <h2 class="content-heading pt-0">Title</h2>

                            @foreach($languages as $language)
                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="extra">Title ({{$language->la_language}}):</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="titles[{{$language->la_code}}]"
                                               value="{{$un_title[$language->la_code]}}">
                                    </div>
                                </div>
                            @endforeach

                            <h2 class="content-heading pt-0">Question</h2>

                            @foreach($languages as $language)
                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="extra">Question
                                        ({{$language->la_language}}):</label>
                                    <div class="col-sm-7">
                                        <textarea class="form-control" rows="3"
                                                  name="questions[{{$language->la_code}}]">{{$un_question[$language->la_code]}}</textarea>
                                    </div>
                                </div>
                            @endforeach

                            <h2 class="content-heading pt-0">Settings</h2>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="value">Class:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="class"
                                           value="{{$question->qu_class}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="value">Limit:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="limit"
                                           value="{{$question->qu_limit}}">
                                </div>
                            </div>


                            <h2 class="content-heading pt-0">Extra Question 1</h2>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label"
                                       for="example-hf-email">Type:</label>
                                <div class="col-sm-7">
                                    <select type="text" class="form-control"
                                            name="extra_1_type">
                                        @foreach($questionextratypes as $id => $value)
                                            <option
                                                value="{{$id}}" {{ ($question->qu_extra_1_type == $id) ? 'selected':'' }}>
                                                {{$value}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div id="extra_1">

                                @foreach($languages as $language)
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-3 col-form-label" for="extra">Question
                                            ({{$language->la_language}}):</label>
                                        <div class="col-sm-7">
                                            <textarea class="form-control" rows="2"
                                                      name="extra_1_question[{{$language->la_code}}]">{{$extra_1_questions[$language->la_code]}}</textarea>
                                        </div>
                                    </div>
                                @endforeach

                                <div id="extra_1_options">

                                    @foreach($languages as $language)
                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-3 col-form-label" for="extra">Options
                                                ({{$language->la_language}}):</label>
                                            <div class="col-sm-7">
                                                <textarea class="form-control" rows="1"
                                                          name="extra_1_option[{{$language->la_code}}]">{{$extra_1_options[$language->la_code]}}</textarea>
                                            </div>
                                        </div>
                                    @endforeach

                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="value">Class (front):</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="extra_1_class_front"
                                               value="{{$question->qu_extra_1_class_front}}">
                                    </div>
                                </div>

                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-3 col-form-label" for="value">Class (back):</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="extra_1_class_back"
                                                   value="{{$question->qu_extra_1_class_back}}">
                                        </div>
                                    </div>

                            </div>

                            <h2 class="content-heading pt-0">Extra Question 2</h2>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label"
                                       for="example-hf-email">Type:</label>
                                <div class="col-sm-7">
                                    <select type="text" class="form-control"
                                            name="extra_2_type">
                                        @foreach($questionextratypes as $id => $value)
                                            <option
                                                value="{{$id}}" {{ ($question->qu_extra_2_type == $id) ? 'selected':'' }}>
                                                {{$value}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div id="extra_2">

                                @foreach($languages as $language)
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-3 col-form-label" for="extra">Question
                                            ({{$language->la_language}}):</label>
                                        <div class="col-sm-7">
                                            <textarea class="form-control" rows="2"
                                                      name="extra_2_question[{{$language->la_code}}]">{{$extra_2_questions[$language->la_code]}}</textarea>
                                        </div>
                                    </div>
                                @endforeach

                                <div id="extra_2_options">

                                    @foreach($languages as $language)
                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-3 col-form-label" for="extra">Options
                                                ({{$language->la_language}}):</label>
                                            <div class="col-sm-7">
                                                <textarea class="form-control" rows="1"
                                                          name="extra_2_option[{{$language->la_code}}]">{{$extra_2_options[$language->la_code]}}</textarea>
                                            </div>
                                        </div>
                                    @endforeach

                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="value">Class (front):</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="extra_2_class_front"
                                               value="{{$question->qu_extra_2_class_front}}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="value">Class (back):</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="extra_2_class_back"
                                               value="{{$question->qu_extra_2_class_back}}">
                                    </div>
                                </div>

                            </div>


                            <h2 class="content-heading pt-0"></h2>

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@push( 'scripts' )
    <script>
        jQuery(document).ready(function () {

            if ($("select[name=extra_1_type]").val() == 0) {
                $("#extra_1").hide();
            }

            if ($("select[name=extra_1_type]").val() != 2) {
                $("#extra_1_options").hide();
            }

            $("select[name=extra_1_type]").change(function () {
                if ($(this).val() == 0) {
                    $("#extra_1").slideUp();
                } else {
                    $("#extra_1").slideDown();
                }

                if ($(this).val() == 2) {
                    $("#extra_1_options").slideDown();
                } else {
                    $("#extra_1_options").slideUp();
                }
            });

            if ($("select[name=extra_2_type]").val() == 0) {
                $("#extra_2").hide();
            }

            if ($("select[name=extra_2_type]").val() != 2) {
                $("#extra_2_options").hide();
            }

            $("select[name=extra_2_type]").change(function () {
                if ($(this).val() == 0) {
                    $("#extra_2").slideUp();
                } else {
                    $("#extra_2").slideDown();
                }

                if ($(this).val() == 2) {
                    $("#extra_2_options").slideDown();
                } else {
                    $("#extra_2_options").slideUp();
                }
            });
        });
    </script>

@endpush



