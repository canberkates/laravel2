@extends('layouts.backend')

@include( 'scripts.datatables' )
@include( 'scripts.dialogs' )

@section('content')
    <!-- Page Content -->
    <div class="content">
        <!-- Your Block -->
        <div class="block block-rounded block-bordered">
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Generating password for user {{$contactperson->cope_full_name}} of {{$contactperson->customer->cu_company_name_business}}
                </h3>
            </div>
            <div class="block-content block-content-full">
                <form method="post" action="{{action('ContactPersonController@generateNewPassword', [$contactperson->cope_cu_id, $contactperson->cope_id])}}">
                    @csrf
                    <input name="_method" type="hidden" value="post">
                    <div class="row">
                        <div class="col-md-6">
                            Are you sure you want to send a mail with the new password for <b>{{$contactperson->application_user->apus_username}}</b>?<br /><br/>
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Yes</button>
                                    <a href="../{{$contactperson->cope_id}}/edit" class="btn btn-primary">No</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END Your Block -->
    </div>
    <!-- END Page Content -->
@endsection
