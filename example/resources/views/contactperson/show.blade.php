@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <form method="post"
                              action="{{action('ContactPersonController@update', $contactperson->cope_id)}}">
                            @csrf
                            <input name="_method" type="hidden" value="PATCH">
                            <input name="form_name" type="hidden" value="General">
                            <h5 class="card-title">Contact Person</h5>

                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="form-group col-md-8">
                                    <label for="company_name_business">Company:</label>
                                    <input disabled type="text" class="form-control" name="company_name_business"
                                           value="{{$customer->cu_company_name_business}}">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="form-group col-md-8">
                                    <label for="full_name">Full name:</label>
                                    <input disabled type="text" class="form-control" name="full_name"
                                           value="{{$contactperson->cope_full_name}}">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="form-group col-md-8">
                                    <label for="first_name">First name:</label>
                                    <input disabled  type="text" class="form-control" name="first_name"
                                           value="{{$contactperson->cope_first_name}}">
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="form-group col-md-8">
                                    <label for="email">Email:</label>
                                    <input disabled type="text" class="form-control" name="email"
                                           value="{{$contactperson->cope_email}}">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="form-group col-md-8">
                                    <label for="telephone">Telephone:</label>
                                    <input disabled type="text" class="form-control" name="telephone"
                                           value="{{$contactperson->cope_telephone}}">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="form-group col-md-8">
                                    <label for="mobile">Mobile Phone:</label>
                                    <input disabled type="text" class="form-control" name="mobile"
                                           value="{{$contactperson->cope_mobile}}">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="form-group col-md-8">
                                    <label for="function">Function:</label>
                                    <select disabled type="text" class="form-control" name="function">
                                        <option value=""></option>
                                        @foreach($contactpersonfunctions as $id => $function)
                                            <option value="{{$id}}" {{ ($contactperson->cope_function == $id) ? 'selected':'' }}>{{$function}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="form-group col-md-8">
                                    <label for="function_remark">Function remark:</label>
                                    <input disabled type="text" class="form-control" name="function_remark"
                                           value="{{$contactperson->cope_function_remark}}">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="form-group col-md-8">
                                    <label for="remark">Remark:</label>
                                    <input disabled type="text" class="form-control" name="remark"
                                           value="{{$contactperson->cope_remark}}">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="form-group col-md-8">
                                    <label for="language">Language:</label>
                                    <select disabled type="text" class="form-control" name="language">
                                        @foreach($languages as $language)
                                            <option value="{{$language->la_code}}" {{ ($contactperson->cope_language == $language->la_code) ? 'selected':'' }}>{{$language->la_language}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                       </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

