@extends('layouts.backend')

@include( 'scripts.datatables' )
@include( 'scripts.dialogs' )

@section('content')
    <!-- Page Content -->
    <div class="content">
        <!-- Your Block -->
        <div class="block block-rounded block-bordered">
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Unblocking user of {{$contact_person->cope_full_name}}
                </h3>
            </div>
            <div class="block-content block-content-full">
                <form method="post"
                      action="{{action('ContactPersonController@unblockUser', [$contact_person->cope_cu_id, $contact_person->cope_id, $contact_person->application_user->apus_id])}}">
                    @csrf
                    <input name="_method" type="hidden" value="post">
                    <input name="cope_id" type="hidden" value="{{$contact_person->cope_id}}">
                    <input name="apus_id" type="hidden" value="{{$contact_person->application_user->apus_id}}">
                    <div class="row">
                        <div class="col-md-6">
                            Are you sure you want to unblock the user of <b>{{$contact_person->cope_full_name}}</b>?<br/><br />
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Yes</button>
                                    <a href="../" class="btn btn-primary">No</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END Your Block -->
    </div>
    <!-- END Page Content -->
@endsection
