@extends('layouts.backend')

@include( 'scripts.dialogs' )
@include('scripts.datatables')
@include( 'scripts.telinput' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Edit this Contact Person of {{$contactperson->customer->cu_company_name_business}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../../../">@if ($contactperson->customer->cu_type == 1) {{"Customers"}} @elseif($contactperson->customer->cu_type == 6) {{"Resellers"}} @elseif($contactperson->customer->cu_type == 2) {{"Service Providers"}}@else {{"Affiliate Partners"}} @endif</a></li>
                        <li class="breadcrumb-item"><a href="../../edit">{{$contactperson->customer->cu_company_name_business}}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$contactperson->cope_full_name}}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="block block-rounded block-bordered">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">General</h3>
                    </div>
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    {{$error}}<br>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="block-content">
                        <form method="post"
                              action="{{action('ContactPersonController@update', $contactperson->cope_id)}}">
                            @csrf
                            <input name="_method" type="hidden" value="PATCH">
                            <input name="form_name" type="hidden" value="General">
                            <input name="cu_co_code" type="hidden" value="{{strtolower($contactperson->customer->cu_co_code)}}">

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="title">Title:</label>
                                <div class="col-sm-7">
                                    <input type="text"
                                           class="form-control {{$errors->has('title') ? 'is-invalid' : ''}}"
                                           name="title"
                                           value="{{$contactperson->cope_title}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="full_name">Full name:</label>
                                <div class="col-sm-7">
                                    <input type="text"
                                           class="form-control {{$errors->has('full_name') ? 'is-invalid' : ''}}"
                                           name="full_name" disabled
                                           value="{{$contactperson->cope_full_name}}">
                                </div>
                            </div>


                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="first_name">First name:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="first_name"
                                           value="{{$contactperson->cope_first_name}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="last_name">Last name:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="last_name"
                                           value="{{$contactperson->cope_last_name}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="email">Email:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="email"
                                           value="{{$contactperson->cope_email}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="telephone">Telephone:</label>
                                <div class="col-sm-7">
                                    <input type="text" placeholder="e.g. 201-555-0123" class="form-control" name="telephone" value="{{$contactperson->cope_telephone}}">
                                    <input type="hidden" id="int_telephone" name="int_telephone" value="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="mobile">Mobile Phone:</label>
                                <div class="col-sm-7">
                                    <input type="text" placeholder="e.g. 201-555-0123" class="form-control" name="mobile" value="{{$contactperson->cope_mobile}}">
                                    <input type="hidden" id="int_mobile" name="int_mobile" value="">
                                </div>
                            </div>

                             <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="department">Department:</label>
                                <div class="col-sm-7">
                                    <select type="text" class="form-control" name="department">
                                        <option value=""></option>
                                        @foreach($contactpersondepartments as $id => $department)
                                            <option value="{{$id}}" {{ ($contactperson->cope_department == $id) ? 'selected':'' }}>{{$department}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                             <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="department_role">Role:</label>
                                <div class="col-sm-7">
                                    <select type="text" class="form-control" name="department_role">
                                        <option value=""></option>
                                        @foreach($contactpersondepartmentroles as $id => $department_role)
                                            <option value="{{$id}}" {{ ($contactperson->cope_department_role == $id) ? 'selected':'' }}>{{$department_role}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="remark">Remark:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="remark"
                                           value="{{$contactperson->cope_remark}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="language">Language:</label>
                                <div class="col-sm-7">
                                    <select type="text" class="form-control" name="language">
                                        @foreach($languages as $language)
                                            @if($language->la_code == "DK")
                                                @continue
                                            @endif
                                            <option value="{{$language->la_code}}" {{ ($contactperson->cope_language == $language->la_code) ? 'selected':'' }}>{{$language->la_language}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                    <a class="btn btn-primary" href="../../edit">
                                        Back
                                    </a>
                                </div>
                            </div>


                        </form>

                    </div>
                </div>
            </div>
            @if ($contactperson->customer->cu_type != 6)
                @if(!empty($contactperson->application_user))
                    <div class="col-md-6">
                        <div class="block block-rounded block-bordered">
                            <ul class="nav nav-tabs nav-tabs-alt" data-toggle="tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#btabs-alt-static-user">User</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#btabs-alt-static-user_logins">User logins</a>
                                </li>
                            </ul>

                            <div class="block-content tab-content">
                                <div class="tab-pane active" id="btabs-alt-static-user" role="tabpanel">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="block-header block-header-default">
                                                <h3 class="block-title">Edit a User of {{$contactperson->customer->cu_company_name_business}}</h3>
                                            </div>
                                            <div class="block-content">
                                                <form method="post"
                                                      action="{{action('ContactPersonController@update', $contactperson->cope_id)}}">
                                                    @csrf
                                                    <input name="_method" type="hidden" value="PATCH">
                                                    <input name="form_name" type="hidden" value="General">

                                                    <div class="form-group row">
                                                        <div class="col-sm-1"></div>
                                                        <label class="col-sm-3 col-form-label" for="username">Username:</label>
                                                        <div class="col-sm-7">
                                                            <input type="text"
                                                                   class="form-control"
                                                                   name="full_name" disabled
                                                                   value="{{$contactperson->application_user->apus_username}}">
                                                        </div>
                                                    </div>


                                                    <div class="form-group row">
                                                        <div class="col-sm-1"></div>
                                                        <label class="col-sm-3 col-form-label" for="name">Full name:</label>
                                                        <div class="col-sm-7">
                                                            <input type="text" class="form-control" name="name" disabled
                                                                   value="{{$contactperson->application_user->apus_name}}">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <div class="col-sm-1"></div>
                                                        <label class="col-sm-3 col-form-label" for="email">Email:</label>
                                                        <div class="col-sm-7">
                                                            <input type="text" class="form-control" name="email" disabled
                                                                   value="{{$contactperson->application_user->apus_email}}">
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-1"></div>
                                                        <div class="form-group col-md-11">
                                                            @if($contactperson->application_user->apus_deleted == 1)
                                                                <p style="color: red;"><b>ATTENTION:</b> This user is deleted. Contact IT if you want to undelete this user.</p>
                                                            @else
                                                                <a href="{{ url('/customers/' . $contactperson->cope_cu_id . '/contactperson/'. $contactperson->cope_id . '/generatepassword') }}" class="btn btn-primary" data-toggle="tooltip" data-placement="left">
                                                                    Send new password
                                                                </a>
                                                                <button type="button" class="btn btn-primary app_user_delete" data-toggle="tooltip" data-app_user_id="{{$contactperson->application_user->apus_id}}">
                                                                    Delete User
                                                                </button>
                                                                @if($contactperson->application_user->apus_blocked == 1)
                                                                    <a href="{{ url('/customers/' . $contactperson->cope_cu_id . '/contactperson/'. $contactperson->cope_id . '/unblock_user/'.$contactperson->application_user->apus_id) }}" class="btn btn-primary" data-toggle="tooltip" data-placement="left">
                                                                        Unblock user
                                                                    </a>
                                                                @endif
                                                            @endif


                                                        </div>
                                                    </div>

                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="btabs-alt-static-user_logins" role="tabpanel">
                                    <div class="row">
                                        <div class="col-md-12">

                                            <div class="block-content">
                                                @if (count($userlogins) > 0)
                                                    <table  data-order='[[1, "asc"]]' class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                                        <thead>
                                                        <tr>
                                                            <th>ID</th>
                                                            <th>Timestamp</th>
                                                            <th>IP Address</th>
                                                            <th>User agent</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach ($userlogins as $uslo)
                                                            <tr>
                                                                <td>{{$uslo->uslo_id}}</td>
                                                                <td>{{$uslo->uslo_timestamp}}</td>
                                                                <td>{{$uslo->uslo_ip_address}}</td>
                                                                <td>{{$uslo->uslo_user_agent}}</td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>

                                                    </table>
                                                @else
                                                    There are no user logins for this user...
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="col-md-6">
                        <div class="block block-rounded block-bordered">
                            <div class="block-header block-header-default">
                                There is no user found for this contact person!
                                <a href="{{ url('customers/' . $contactperson->customer->cu_id . '/contactperson/' . $contactperson->cope_id . "/application_user/create")}}">
                                    <button class="btn btn-primary">Add a user</button>
                                </a>
                            </div>

                        </div>
                    </div>
                @endif
            @endif
        </div>
    </div>

@endsection

@push( 'scripts' )

    <script>
        jQuery(document).ready(function(){
        	jQuery( document ).on( 'click', '.app_user_delete', function(e) {
                e.preventDefault();

        		var $self = jQuery( this );

                confirmDelete("{{ url('ajax/customer/contactperson/app_user_delete') }}", 'get', {id:$self.data('app_user_id')}, function() {
                    location.reload();
                });
            });

            firstInput();
            secondInput();

            function firstInput(){
                var input = document.querySelector("input[name=telephone]");
                output = $("#int_telephone");

                var iti = window.intlTelInput(input, {
                    nationalMode: true,
                    separateDialCode: true
                });

                if ($("input[name=telephone]").val() == "") {
                    var co_code = $('input[name=cu_co_code]').val();
                    iti.setCountry(co_code);
                }

                var handleChange = function() {
                    var text = (iti.isValidNumber()) ? iti.getNumber() : 0;
                    if(text == 0){
                        $('input[name=telephone]').addClass("is-invalid");
                    }else{
                        $('input[name=telephone]').removeClass("is-invalid");
                    }
                    output.val(text);
                };

                input.addEventListener('change', handleChange);
                input.addEventListener('keyup', handleChange);

                handleChange();
            }

            function secondInput(){
                var input2 = document.querySelector("input[name=mobile]");
                output2 = $("#int_mobile");

                var iti2 = window.intlTelInput(input2, {
                    nationalMode: true,
                    separateDialCode: true
                });

                if ($("input[name=mobile]").val() == "") {
                    var co_code = $('input[name=cu_co_code]').val();
                    iti2.setCountry(co_code);
                }

                var handleChange = function() {
                    var text = (iti2.isValidNumber()) ? iti2.getNumber() : 0;
                    if(text == 0){
                        $('input[name=mobile]').addClass("is-invalid");
                    }else{
                        $('input[name=mobile]').removeClass("is-invalid");
                    }
                    output2.val(text);
                };

                input2.addEventListener('change', handleChange);
                input2.addEventListener('keyup', handleChange);

                handleChange();

            }
        });
    </script>

@endpush
