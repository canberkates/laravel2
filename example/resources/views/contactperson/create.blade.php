@extends('layouts.backend')
@include( 'scripts.telinput' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Add a Contact Person to {{$customer->cu_company_name_business}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../../">@if ($customer->cu_type == 1) {{"Customers"}} @elseif($customer->cu_type == 6) {{"Resellers"}} @elseif($customer->cu_type == 2) {{"Service Providers"}}@else {{"Affiliate Partners"}} @endif</a></li>
                        <li class="breadcrumb-item"><a href="../edit">{{$customer->cu_company_name_business}}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Add contact person</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-10 col-xl-10">
                <div class="block block-rounded block-bordered">
                   <div class="block-header block-header-default">
                        <h3 class="block-title">General</h3>
                   </div>
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    {{$error}}<br>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="block-content">
                        <form method="post" action="{{action('ContactPersonController@store')}}">
                            @csrf
                            <input name="_method" type="hidden" value="post">
                            <input name="customer_id" type="hidden" value="{{$customer->cu_id}}">
                            <input name="cu_co_code" type="hidden" value="{{strtolower($customer->cu_co_code)}}">

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="title">Title:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control {{$errors->has('title') ? 'is-invalid' : ''}}" name="title"
                                           value="{{Request::old('title')}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="first_name">First name:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="first_name"
                                           value="{{Request::old('first_name')}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="last_name">Last name:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="last_name"
                                           value="{{Request::old('last_name')}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="email">Email:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control {{$errors->has('email') ? 'is-invalid' : ''}}" name="email"
                                           value="{{Request::old('email')}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="telephone">Telephone:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" placeholder="e.g. 201-555-0123" name="telephone" value="{{Request::old('telephone')}}">
                                    <input type="hidden" id="int_telephone" name="int_telephone" value="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="mobile">Mobile Phone:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="mobile"
                                           value="{{Request::old('mobile')}}" placeholder="e.g. 201-555-0123">
                                    <input type="hidden" id="int_mobile" name="int_mobile" value="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="department">Department:</label>
                                <div class="col-sm-7">
                                    <select type="text" class="form-control" name="department">
                                        <option value=""></option>
                                        @foreach($contactpersondepartments as $id => $department)
                                            <option @if (Request::old('department') == $id) selected @endif value="{{$id}}">{{$department}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="department_role">Role:</label>
                                <div class="col-sm-7">
                                    <select type="text" class="form-control" name="department_role">
                                        <option value=""></option>
                                        @foreach($contactpersondepartmentroles as $id => $departmentrole)
                                            <option @if (Request::old('department_role') == $id) selected @endif value="{{$id}}">{{$departmentrole}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="remark">Remark:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="remark"
                                           value="{{Request::old('remark')}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="language">Language:</label>
                                <div class="col-sm-7">
                                    <select type="text" class="form-control" name="language">
                                        <option value=""></option>
                                        @foreach($languages as $language)
                                            @if($language->la_code == "DK")
                                                @continue
                                            @endif
                                            <option @if (Request::old('language') == $language->la_code) selected @endif value="{{$language->la_code}}">{{$language->la_language}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-5">
                                    <button type="submit" class="btn btn-primary">Add Contact Person</button>
                                    <a class="btn btn-primary" href="../edit">
                                        Back
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@push('scripts')
    <script>
        $(document).ready(function () {

            firstInput();
            secondInput();

            function firstInput(){
                var input = document.querySelector("input[name=telephone]");
                output = $("#int_telephone");

                var iti = window.intlTelInput(input, {
                    nationalMode: true,
                    separateDialCode: true
                });

                if ($("input[name=telephone]").val() == "") {
                    var co_code = $('input[name=cu_co_code]').val();
                    iti.setCountry(co_code);
                }

                var handleChange = function() {
                    var text = (iti.isValidNumber()) ? iti.getNumber() : 0;
                    if(text == 0){
                        $('input[name=telephone]').addClass("is-invalid");
                    }else{
                        $('input[name=telephone]').removeClass("is-invalid");
                    }
                    output.val(text);
                };

                input.addEventListener('change', handleChange);
                input.addEventListener('keyup', handleChange);

                handleChange();
            }

            function secondInput(){
                var input2 = document.querySelector("input[name=mobile]");
                output2 = $("#int_mobile");

                var iti2 = window.intlTelInput(input2, {
                    nationalMode: true,
                    separateDialCode: true
                });

                if ($("input[name=telephone]").val() == "") {
                    var co_code = $('input[name=cu_co_code]').val();
                    iti2.setCountry(co_code);
                }

                var handleChange = function() {
                    var text = (iti2.isValidNumber()) ? iti2.getNumber() : 0;
                    if(text == 0){
                        $('input[name=mobile]').addClass("is-invalid");
                    }else{
                        $('input[name=mobile]').removeClass("is-invalid");
                    }
                    output2.val(text);
                };

                input2.addEventListener('change', handleChange);
                input2.addEventListener('keyup', handleChange);

                handleChange();

            }
        });
    </script>
@endpush
