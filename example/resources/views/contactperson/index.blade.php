@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-6">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone Number</th>
                        <th>Function</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{$contactperson->cope_id}}</td>
                        <td>{{$contactperson->cope_full_name}}</td>
                        <td>{{$contactperson->cope_email}}</td>
                        <td>{{$contactperson->cope_telephone}}</td>
                        <td>{{$contactpersonfunctions[$contactperson->cope_function]}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
