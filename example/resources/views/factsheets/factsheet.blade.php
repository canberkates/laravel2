<?php
$colors = [

    'blue1'     => '#0D5A92',
    'blue2'     => '#159DD9',
    'blue3'     => '#84D7F7',
    'orange1'   => '#F58220',
    'grey1'     => '#F3F3F3',
];
?>
<head>

    <meta name="robots" content="noindex,nofollow"/>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <link href='https://fonts.googleapis.com/css?family=Ubuntu' rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Bebas+Neue' rel="stylesheet">
    <style>

        /* Defaults */

        .xdebug-error {

            position: relative;
            z-index: 999999;
        }

        * {

            margin: 0;
            padding: 0;
            border: none;
            line-height: 1;
            box-sizing: border-box;
            font-family: "Ubuntu", sans-serif;
        }

        p {

            font-size: 20px;
            margin: 0;
            line-height: 1;
        }

        .title {

            font-size: 16px;
            font-weight: bold;
            color: <?php echo COLORS['blue1']; ?>;
        }

        .daterange {

            font-size: 11px;
            color: <?php echo COLORS['blue1']; ?>;
            font-weight: bold;
            margin-bottom: 10px;
        }

        .page {

            /*position: absolute;*/
            width: 100%;
            right: 0;
            height: 100%;
            left: 0;

            background-size: 100% 100%;
            background-repeat: no-repeat;
            background-position: center center;
        }

        #factsheet {

            top: 0;
            background-image: url( {{ asset('/media/factsheets/template/factsheet_empty.png') }} );
        }

        .elm {

            #position: absolute;
        }

        .elm.row_year {
            color: #ffffff;
            padding-top: 31px;
            padding-left: 26px;
            font-family: "Bebas Neue", sans-serif;
            font-size: 18px;
        }

        .elm.flag {
            padding-top: 30px;
            padding-left: 10px
        }

        .elm.row_country {
            font-size: 57px;
            color: #124e91;
            padding-top: 31px;
            padding-left: 74px;
            font-family: "Bebas Neue", sans-serif;
        }

        .elm.row_total_removals_text {
            color: #124e91;
            padding-top: 3px;
            padding-left: 77px;
        }
        .elm .strong_removal_text {
            color: #00AFEE;
        }

        .elm.row_export_import_europe {
            color: #f58221;
            padding-top: 177px;
            font-size: 13px;
            font-weight: bold;
            padding-left: 17px;
        }

        .elm.row_export_import_asia_middle_east {
            color: #214e96;
            padding-top: 125px;
            font-size: 13px;
            font-weight: bold;
            padding-left: 4px;
        }

        .elm.row_export_import_america {
            color: #1ba7da;
            padding-top: 113px;
            font-size: 13px;
            font-weight: bold;
            padding-left: 19px;
        }

        .elm.row_export_import_africa {
            color: #f01347;
            padding-top: 91px;
            font-size: 13px;
            font-weight: bold;
            padding-left: 1px;
        }

        .elm.row_export_import_oceania {
            color: #5cbf64;
            padding-top: 152px;
            font-size: 13px;
            font-weight: bold;
            padding-left: 35px;
        }



        #factsheet .leads_per_month {

            padding-right: 0;
            padding-bottom: 0;
        }

        #factsheet .leads_bar_con {

            height: 100%;
            position: relative;
            display: inline-block;
            margin: 0 6.5px;
            width: 43px;
        }

        #factsheet .leads_bar_con:first-child {

            margin-left: 0;
        }

        #factsheet .leads_bar_con:last-child {

            margin-right: 0;
        }

        #factsheet .leads_bar_con .leads_bar {

            position: relative;
            background: blue;
            text-align: center;
            width: 100%;
        }

        #factsheet .leads_bar_con:nth-child(odd) .leads_bar {

            background: #00afee;
            color: #124d91;
        }

        #factsheet .leads_bar_con:nth-child(even) .leads_bar {

            background: #124d91;
            color: #00afee;
        }

        #factsheet .leads_bar_con .leads_bar .leads_bar_amount {

            -webkit-transform: translateX(-50%) translateY(-50%) rotate(-90deg);
            -moz-transform: translateX(-50%) translateY(-50%) rotate(-90deg);
            -ms-transform: translateX(-50%) translateY(-50%) rotate(-90deg);
            -o-transform: translateX(-50%) translateY(-50%) rotate(-90deg);
            position: absolute;
            bottom: 10px;
            left: 50%;
            font-size: 25px;
            font-weight: bold;
        }

        #factsheet .leads_bar_con .month_height {

            position: relative;
            height: 100%;
        }

        #factsheet .leads_bar_con .month_height .month_name {

            -webkit-transform: translateY(-50%) translateX(-50%);
            -moz-transform: translateY(-50%) translateX(-50%);
            -ms-transform: translateY(-50%) translateX(-50%);
            -o-transform: translateY(-50%) translateX(-50%);
            top: 50%;
            left: 50%;
            position: absolute;
            font-weight: bold;
            color: <?php echo $colors['blue1']; ?>;
        }

        .elm.row_destination_chart {
            padding-left:76px;
            padding-top:35px;
        }

        .elm.row_removals {
            font-size: 40px;
            font-weight: bold;
            padding-top: 55px;
        }
        .elm.total_removals {
            color: #f58221;
            padding-left: 116px;
        }
        .elm.export_removals {
            color: #124e91;
            padding-left:84px;
        }

        .elm.import_removals {
            color: #00afee;
            padding-left:82px;
        }

        .elm_row {

            display: inline-block;
            width: 100%;
        }

        <?php echo ( isset( $web ) ? '.page{ height: 1093; }' : '' ); ?>

    </style>

</head>

<body @if(isset($web)) style='width: 800px; margin: 0 auto; position: relative;' @endif>

<div id='factsheet' class='page'>

    <?php

    $row_height = 209;

    // Percentage on top and bottom
    $vert_space = 15; // 2

    // px on the sides
    $horz_space = 57; // 6

    // Set spaces
    $elm_space = $vert_space.'px '.$horz_space.'px';

    ?>

    <div class='elm_row' style='display:inline-flex'>
        <div class='elm empty_div' style='width:77px; height:63px;'>

        </div>
        <div class='elm row_year' style='width:175px; height:63px;'>
            <img width='20px' style='margin-right:7px;' src='{{ asset('/media/flags/'.strtolower($co_code).".png") }}'/>
            STATISTICS 2020
        </div>
        <div class='elm empty_div' style='width:75px; height:63px;'>

        </div>
    </div>

    <div class='elm_row'>
        <div class='elm row_country' style='height:90px;'>
            {{$country_name}}
        </div>
    </div>

    <div class='elm_row'>
        <div class='elm row_total_removals_text' style='height:10px;font-size: 15px;'>
            <b class='strong_removal_text'>Total Removals</b> received between <b class='strong_removal_text'>{{$date_text_top}}</b>
        </div>
    </div>

    <div class='elm_row' style='display: inline-flex'>
        <div class='elm row_removals total_removals' style='width: 255px; height:100px;'>
            {{$total_removals}}
        </div>
        <div class='elm row_removals export_removals' style='width: 225px; height:100px;'>
            {{$export_removals}}
        </div>
        <div class='elm row_removals import_removals' style='width: 225px; height:100px;'>
            {{$import_removals}}
        </div>
    </div>

    <div class='elm_row'>
        <div class='elm row_bars leads_per_month' style='height:250px;padding:20px;padding-right:0;padding-bottom:0;'>
            <div class='elm_inner'>

                <div class='lead_bars' style='padding-left: 50px;padding-right:30px;'>

                    <?php

                    $count = 0;
                    $max_removals_per_month = max($removals_per_month);

                    if(isset($removals_per_month)) {
                        foreach ($removals_per_month as $month => $amount)
                        {
                            if( $count == 12 )
                                break;


                            if( $amount && $all_removals ) {

                                // Height of the bar
                                $height = round($amount / $all_removals * 100);

                            }else{

                                $height = 0;
                            }

                            $height = (100 / $max_removals_per_month) * $amount;

                            // The space above the bar
                            $space_from_top = 100 - $height;

                            ?>

                            <div class='leads_bar_con'>

                                <div class='bar_height' style='height: 75%;'>

                                    <div class='leads_bar'
                                        style='top: <?php echo $space_from_top; ?>%; height: <?php echo $height; ?>%;'>

                                        <div class='leads_bar_amount'><?php echo $amount; ?></div>

                                    </div>

                                </div>

                                <div class='month_height' style='height: 25%;'>

                                    <div class='month_name'><?php echo $month; ?></div>

                                </div>

                            </div>

                            <?php

                            $count++;
                        }
                    }
                    ?>
                </div>

            </div>
        </div>
    </div>

    <div class='elm_row' style='display: inline-flex'>
        <div class='elm row_date_text' style='width: 520px; height:257px; font-size: 10px; color: #b6bdc7; padding-left: 96px; padding-top:61px;'>
            {{$date_text}}
        </div>
        <div class='elm row_export_import_europe' style='width: 287px; height:257px;'>
            <div style='margin-bottom:2px;'>{{number_format($removals_per_continent['europe']['export'], 0, ",", ".")}}</div>
            <div>{{number_format($removals_per_continent['europe']['import'], 0, ",", ".")}}</div>
        </div>
        <div class='elm row_export_import_asia_middle_east' style='width: 141px; height:257px;'>
            <div style='margin-bottom:2px;'>{{number_format($removals_per_continent['asia_middle_east']['export'], 0, ",", ".")}}</div>
            <div>{{number_format($removals_per_continent['asia_middle_east']['import'], 0, ",", ".")}}</div>
        </div>
    </div>

    <div class='elm_row' style='display: inline-flex'>
        <div class='elm row_destination_chart' style='width: 250px; height:201px;'>
            <div id="destination_piechart" style="width: 100%; height:100%;"></div>

            <script type='text/javascript'>

                google.charts.load('current', {'packages':['corechart']});
                google.charts.setOnLoadCallback(drawChart);

                function drawChart() {

                    var data = google.visualization.arrayToDataTable([
                        ['Continents', 'Removals'],
                        <?php
                            foreach ($donut_continent_removals as $continent => $removals) {
                                ?>
                                ['<?= $continent; ?>', <?= $removals; ?>],
                            <?php
                            }
                        ?>
                    ]);

                    var options = {
                        title: '',
                        slices: {0: {color: '<?= $donut_colors[0]?>'}, 1:{color: '<?= $donut_colors[1]?>'}, 2:{color: '<?= $donut_colors[2]?>'}, 3: {color: '<?= $donut_colors[3]?>'}, 4:{color: '<?= $donut_colors[4]?>'}},
                        legend: {position: 'none'},
                        theme: 'maximized',
                        backgroundColor: 'transparent',
                        'width':130,
                        'height':130,
                        enableInteractivity: false
                    };

                    var chart = new google.visualization.PieChart(document.getElementById('destination_piechart'));

                    chart.draw(data, options);
                }

            </script>
        </div>
        <div class='elm row_export_import_america' style='width: 225px; height:201px;'>
            <div style='margin-bottom:2px;'>{{number_format($removals_per_continent['america']['export'], 0, ",", ".")}}</div>
            <div>{{number_format($removals_per_continent['america']['import'], 0, ",", ".")}}</div>
        </div>
        <div class='elm row_export_import_africa' style='width: 174px; height:201px;'>
            <div style='margin-bottom:2px;'>{{number_format($removals_per_continent['africa']['export'], 0, ",", ".")}}</div>
            <div>{{number_format($removals_per_continent['africa']['import'], 0, ",", ".")}}</div>
        </div>
        <div class='elm row_export_import_oceania' style='width: 251px; height:201px;'>
            <div style='margin-bottom:2px;'>{{number_format($removals_per_continent['oceania']['export'], 0, ",", ".")}}</div>
            <div>{{number_format($removals_per_continent['oceania']['import'], 0, ",", ".")}}</div>
        </div>
    </div>

</div>

</body>
