@extends('layouts.backend')

@include('scripts.select2')

@section('content')
    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="block block-rounded block-bordered">

                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    {{$error}}<br>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="block-content">
                        <form method="post" action="{{action('CustomersController@store')}}">
                            @csrf
                            <input name="_method" type="hidden" value="post">
                            <h2 class="content-heading pt-0">General Information</h2>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-2 col-form-label" for="sirelo">Sirelo:</label>
                                <div class="col-sm-7">
                                    <select id="sirelo" class="js-select2 form-control {{$errors->has('sirelo') ? 'is-invalid' : ''}}" name="sirelo" data-placeholder="Choose one..">
                                        <option value=""></option>
                                        @foreach($websites as $id => $website)
                                            <option @if (Request::old('sirelo') == $website->we_sirelo_co_code) selected @endif value="{{$website->we_sirelo_co_code}}">{{$website->we_website}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-2 col-form-label" for="membership">Membership:</label>
                                <div class="col-sm-7">
                                    <select id="membership" class="js-select2 form-control {{$errors->has('membership') ? 'is-invalid' : ''}}" name="membership" data-placeholder="Choose one..">
                                        <option value=""></option>
                                        @foreach($memberships as $membership)
                                            <option @if (Request::old('membership') == $membership->me_id) selected @endif value="{{$membership->me_id}}">{{$membership->me_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-2 col-form-label" for="link">Link:</label>
                                <div class="col-sm-7">
                                    <input id="link" type="text" class="form-control {{$errors->has('link') ? 'is-invalid' : ''}}" name="link" value="{{Request::old('link')}}">
                                </div>
                            </div>


                            <h2 class="content-heading pt-0"></h2>

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Add hyperlink</button>
                                    <a class="btn btn-primary" href="/customers">
                                        Back
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@push('scripts')
    <script>
        $(document).ready(function () {

        });
    </script>
@endpush
