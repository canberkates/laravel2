@extends('layouts.backend')

@include( 'scripts.datatables' )
@include( 'scripts.dialogs' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Sirelo Links</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Sirelo Links</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        {{--<div class="block block-rounded block-bordered">
            <div class="block-content ">
                <form method="post" action="{{action('MessagesController@index')}}">
                    @csrf
                    <input name="_method" type="hidden" value="post">
                    <h2 class="content-heading pt-0">Filters</h2>

                    <div class="form-group row">
                        <label class="col-md-1 col-form-label" for="for-type">Read:</label>
                        <div class="col-md-2">
                            <select id="for-type" class="js-select2 form-control {{$errors->has('type') ? 'is-invalid' : ''}}" name="read" data-placeholder="Choose one.." style="width:100%;" data-minimum-results-for-search="Infinity">

                                @foreach($bothyesno as $id => $X)
                                    <option @if (Request::old('read') == $id) selected @endif value="{{$id}}">{{$X}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <h2 class="content-heading pt-0"></h2>

                    <div class="row">
                        <div class="form-group col-md-2">
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>--}}
        <!-- Your Block -->
        <div class="block block-rounded block-bordered">
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    <a href="{{ url('sirelo_links/create')}}"><button data-toggle="click-ripple" class="btn btn-primary">Create Sirelo Link</button></a>
                </h3>
            </div>
            <div class="block-content block-content-full">
                <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Sirelo</th>
                        <th>Link</th>
                        <th>Timestamp</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($sirelo_links as $link)
                        <tr>
                            <td>{{$link->sihy_id}}</td>
                            <td>{{$memberships[$link->sihy_me_id]->me_name}}</td>
                            <td>{{$link->website->we_website}}</td>
                            <td>{{$link->sihy->link}}</td>
                            <td>{{$link->sihy_timestamp}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Your Block -->
    </div>
    <!-- END Page Content -->
@endsection
