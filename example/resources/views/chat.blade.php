@include( 'scripts.dialogs' )

<div class="row chat-row">
</div>

<div id="tooManyChats" class="modal fade" role="dialog">
    <div id="viewInvoiceModal" >
        <div class="modal-dialog">
            <div class="modal-content" style="width:700px;">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Maximum chats open</h3>
                    </div>
                    <div class="block-content">
                        <p>You can only have 3 chats open at a time</p>
                    </div>
                    <div class="block-content block-content-full text-right bg-light">
                        <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Okay. I won't do it again.</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script id="chat_block_template" type="text/template">
    <div class="block block-chat block-rounded block-themed block-bordered block-fx-shadow animated mb-0 bounceInUp" data-channel="" data-other="" style="width: 300px;">
        <div class="block-header block-header-default bg-primary">
            <h3 class="block-title chat-username"></h3>
            <div class="block-options">
                <a href="#" class="btn-block-option chat-toggle">
                    <i class="si si-arrow-down"></i>
                </a>
                <button type="button" class="btn-block-option chat-close" >
                    <i class="si si-close"></i>
                </button>
            </div>
        </div>
        <div class="js-chat-messages block-content block-content-full text-wrap-break-word overflow-y-auto" style="height: 300px;">
            <!-- Messages will be here -->
        </div>
        <div class="js-chat-form block-content p-2 bg-body-dark">
            <div class="input-group">
                <input type="text" class="js-chat-input form-control form-control-alt" placeholder="Type a message"/>
                <div class="input-group-append">
                    <a href="#" class="btn btn-primary chat-send-message"><i class="fa fa-angle-right fa-fw"></i></a>
                </div>
            </div>
        </div>
    </div>
</script>

<script id="their-message-tmpl" type="text/template">
    <div>
        <div class="message mr-4">
            <div class="d-inline-block font-w600 animated fadeIn bg-body-light border-3x px-3 py-2 mb-2 shadow mw-100 border-left border-dark rounded-right">
                <div class="message-data">
                    <span class="timestamp badge badge-primary"></span>
                </div>
                <p class="message-body mb-0"></p>
            </div>
        </div>
    </div>
</script>

<script id="my-message-tmpl" type="text/template">
    <div>
        <div class="message text-right ml-4">
            <div class="d-inline-block font-w600 animated fadeIn bg-body-light border-3x px-3 py-2 mb-2 shadow mw-100 border-right border-primary rounded-left text-left">
                <div class="message-data">
                    <span class="timestamp badge badge-primary"></span>
                </div>
                <p class="message-body mb-0"></p>
            </div>
        </div>
    </div>
</script>

@pushonce( 'scripts:chat' )
	<script src="https://cdn.rawgit.com/samsonjs/strftime/master/strftime-min.js"></script>
	<script src="https://js.pusher.com/4.4/pusher.min.js"></script>
    <!--suppress ES6ModulesDependencies, JSIgnoredPromiseFromCall -->
    <script>

        ( function() {

            if( 'Notification' in window ) {

                var permission = Notification.permission;

                if( permission === 'denied' || permission === 'granted' )
                    return;

                Notification.requestPermission();
            }
        })();

        // Ensure CSRF token is sent with AJAX requests
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        // Added Pusher logging
        Pusher.log = function (msg) {
            console.log( msg );
        };

        var pusher = new Pusher('{{config('pusher.connections.main.auth_key')}}', {
            cluster: '{{config( 'pusher.connections.main.options.cluster' )}}',
            disableStats: true,
        });
        var $notifications = 'chat-requests';
        var $localStorage = 'chats';

        // Set icons
        var iconContent         = 'si si-arrow-down';
        var iconContentActive   = 'si si-arrow-up';

        // Subscribe to channel
        var $channel = pusher.subscribe( $notifications );

        // Get favicon
        var $favicon = $( 'link[rel="shortcut icon"]' ).attr( 'href' );

        // Setup request
        var $request = {
            id: {{Auth::id()}}
        };

        //Open all unread chats
        $.post( '/chat/openunreadchats', $request ).done( function( $data ) {

            $.each( $data, function( $i, $value ) {

                var $channelId = newChat( $value.ch_us_id_from );
            });
        });

        // Setup the storage
        initStorage();

        // Bind chat request handler
        $channel.bind( 'request', function( $data ) {

            var $log = 'Chat request from %c ' + $data.from + ' to ' + $data.to + ' ';
            var $logStyle = 'background: #fff;color: blue; border-radius: 10px; margin: 10px; padding: 4px; font-weight: bold;';

            // Log the request
            console.log( $log, $logStyle );

            // Get channel id
            var $channelId = getChannel( $request.id, $data.to );

            // If the current user is the one that got a new message
            if( parseInt( $data.to ) === parseInt( $request.id ) ) {

                console.log( chatExists( $channelId ) );

                // If the chat does not exist yet and the user is not chatting to itself
                if( chatExists( $channelId ) === false && parseInt( $data.from ) !== parseInt( $request.id ) ) {

                    // // Do the notification
                    // doNotification( 'You got a new message', {
                    //
                    //     // Set an icon
                    //     icon: $favicon
                    // });
                }

                // Set up a chat to the sender
                $channelId = newChat( $data.from );
            }
        } );

        // Store body
        var $body = $( 'body' );

        // Set default status
        var $sending = false;

        // The order of sending a message
        // Defaults:
        // Sending = false
        // On message wanting to send:
        // Check if sending is false
        // Set sending to true
        // Verify the value length of the message
        // Clear the input to prevent spam messages
        // Get the data of the chat box and send the message
        // After sending the message set sending to false

        // On message send
        $body.on( 'keypress', '.js-chat-input', function( e ) {

            // If a message is sending or the key is not an enter
            if( $sending || e.keyCode !== 13 )
                return;

            // Set busy to true
            $sending = true;

            // Store the input
            var $input = $( this );

            // Get val
            var $value = $input.val();

            // If value is not enough
            if( $value.length < 2 ) {

                // Set busy to false
                $sending = false;
                return;
            }

            // Clear val
            $input.val( '' );

            // Get box
            var $box = $( this ).parents( '.block-chat' );

            // Get channel id
            var $channel_id = $box.data( 'channel' );

            // Get other user id
            var $other_id = $box.data( 'other' );

            // Send the message
            sendMessage( $value, $other_id, $channel_id, function() {

                $sending = false;
            } );
        });

        // On click message send
        $body.on( 'click', '.chat-send-message', function() {

            if( $sending )
                return;

            $sending = true;

            // Get box
            var $box = $( this ).parents( '.block-chat' );

            // Store the input
            var $input = $box.find( '.js-chat-input' );

            // Get val
            var $value = $input.val();

            // If value is not enough
            if( $value.length < 2 ) {

                // Set busy to false
                $sending = false;
                return;
            }

            // Clear val
            $input.val( '' );

            // Get channel id
            var $channel_id = $box.data( 'channel' );

            // Get other user id
            var $other_id = $box.data( 'other' );

            // Send the message
            sendMessage( $value, $other_id, $channel_id, function() {

                $sending = false;
            } );
        } );

        // On chat open
        $body.on( 'click', '.chat-open', function(e) {

            // Get id from other user
            var $other = $( this ).attr( 'data-userid' );

            // Build a new chat
            var $channelId = newChat( $other );
        });

        // On chat close
        $body.on( 'click', '.chat-close', function(e) {

            // Prevent the link
            e.preventDefault();

            // Store the box
            var $box = $( this ).parents( '.block-chat' );

            // Get other target id
            var $other = $box.data( 'other' );

            // Store the channel
            var $channel = $box.data( 'channel' );

            // Remove the box
            $box.fadeOut( 400, function() {

                $( this ).remove();
            });

            // Remove from storage
            removeFromStorage( $other );

            // Unsubscribe from the chat channel
            pusher.unsubscribe( $channel );
        });

        // On chat toggle
        $body.on( 'click', '.chat-toggle', function(e) {

            // Prevent the link
            e.preventDefault();

            // Store the box
            var $box = $( this ).parents( '.block-chat' ).data( 'channel' );

            // Toggle the chat
            toggleChat( $box );
        });

        // Open chat when clicking the username
        $body.on( 'click', '.block-chat.block-mode-hidden .chat-username', function(e) {

            // Prevent the link
            e.preventDefault();

            // Store the box
            var $box = $( this ).parents( '.block-chat' ).data( 'channel' );

            // Toggle the chat
            toggleChat( $box );
        });

        // Open a new chat window with another person
        function newChat( $other ) {

            // Set default create
            var $self = $request.id;
            var $channel_id = getChannel( $self, $other );
            var $usernames = [@json( $globaldata['users'] )][0];
            var $username = '';

            // Check if the chat exists already
            if( chatExists( $channel_id ) ) {

                // Check if the chat isn't visible yet
                if( ! isChatVisible( $channel_id ) ) {

                    // Toggle the visibility of the chat
                    toggleChat( $channel_id );
                }

                // The chat does exist and is visible
                return false;
            }

            // Loop usernames
            $.each( $usernames, function( index, value ) {

                if( parseInt( value.id ) === parseInt( $other ) ) {

                    $username = value.us_name;
                }
            });

            // Find chat amount
            if( countChats() === 20 ) {

                // This is too many
                $('#tooManyChats').modal('toggle');

                return false;
            }

            // Get local storage
            var $status = getStatusFromStorage( $other );

            // Get chat template
            var $chat = getMessageEl( '#chat_block_template' );

            // Set channel id
            $chat.data( 'channel', $channel_id );

            // Set other user id
            $chat.data( 'other', $other );

            // Set channel class
            $chat.addClass( 'channel-' + $channel_id );

            // Set username
            $chat.find( '.chat-username' ).text( $username );

            // If the chat should be hidden
            if( $status === 'hidden' ) {

                $chat.addClass( 'block-mode-hidden' );
                $chat.find( '.chat-toggle i' ).removeClass(iconContent).addClass(iconContentActive);
            }

            // Get the chat row
            var $chatRow = $( '.chat-row' );

            // Append the new chat to the chat row
            $chatRow.prepend( $chat );

            // Get chat history
            getHistory( $self, $other );

            // Subscribe to channel
            var $channel = pusher.subscribe( $channel_id );

            // Bind new message handler
            $channel.bind( 'new-message', addMessage );

			// Add the storage
            addToStorage( $other, true );

            return $channel_id;
        }

        // Handle the send message
        function sendMessage( $value, $to, $channel_id, $callback = '' ) {

            var $sender = $request.id;

            // Do the chat request
            $.post( '/chat/request', {

                sender: $sender,
                to: $to,

            } ).done( function( data ) {

                // Prepare message
                $.post( '/chat/message', {

                    chat_text: $value,
                    id: $channel_id,
                    sender: $sender,
                } ).done( function() {

                    if( typeof $callback === 'function' ) {

                        $callback();
                    }
                });
            } );

            // Ensure the normal browser event doesn't take place
            return false;
        }

        // Build the correct channel id
        function getChannel( $user_id, $target_id ) {

            return 'chat-' + ( ( parseInt( $user_id ) < parseInt( $target_id ) ) ? $user_id + '-' + $target_id : $target_id + '-' + $user_id );
        }

        // Get the history of the chat between user_id and target_id
        function getHistory( $user_id, $target_id, $callback = '' ) {

            let data = {
                userid: $user_id,
	            targetid: $target_id
            };

            var $channelId = getChannel( $user_id, $target_id );

            $.post( '/chat/gethistory', data ).done( function( $result ) {

                var $messages = '';
                var $selfId = parseInt( {{Auth::id()}} );

                $result.sort((a, b) => (a.id > b.id) ? 1 : -1);

                // Loop the messages
                $.each( $result, function( $index, $value ) {

                    var $message = buildMessage( {
                        message: $value.ch_message,
                        timestamp: $value.ch_timestamp,
                    }, ( parseInt( $value.ch_us_id_from ) === $selfId )  );

                    // Add the html
                    $messages += $message.html();
                });

                // Get correct chat box
                var $box = $( '.channel-' + $channelId );
                var $boxMessages = $box.find( '.js-chat-messages' );
                $boxMessages.append( $messages );

                if( $messages.length > 1 ) {

                    $boxMessages.scrollTop( $boxMessages[0].scrollHeight );
                }

                if( typeof $callback === 'function' ) {

                    $callback();
                }
            });
        }

        // Build the html for the message
        function buildMessage( $data, $self = false ) {

            var $message = getMessageEl( ( $self ) ? '#my-message-tmpl' : '#their-message-tmpl'  );

            $message.find( '.message-body' ).html( $data.message );
            $message.find( '.timestamp' ).text( strftime('%H:%M %P', new Date( $data.timestamp ) ) );

            return $message;
        }

        // Build the UI for a new message and add to the DOM
        function addMessage( $data ) {

            var $selfId = parseInt( {{Auth::id()}} );
            var $otherId = $data.sender;

            var $message = buildMessage( {
                message: $data.text,
                timestamp: $data.timestamp,
            }, ( parseInt( $otherId ) === $selfId ) );

            // Find the right box to post this message in
            var $box = $( '.channel-' + $data.chat_id );
            var $boxMessages = $box.find( '.js-chat-messages' );
            $boxMessages.append( $message );
            $boxMessages.scrollTop( $boxMessages[0].scrollHeight );
        }

        // Get the message html
        function getMessageEl( $id ) {

            var $text = $( $id ).text();
            return $( $text );
        }

        // If the chat exists already
        function chatExists( $channel ) {

            var $output = false;

            // If the channel exists
            if( getChannelElement( $channel ).length > 0 ) {

                $output = true;
            }

            return $output;
        }

        // Count the amount of open chats
        function countChats() {

            return $( '.block-chat' ).length;
        }

        // If the chat is shown
        function isChatVisible( $channel ) {

            var $output = true;

            if( getChannelElement( $channel ).hasClass( 'block-mode-hidden' ) )
                $output = false;

            return $output;
        }

        // Get channel element from channel id
        function getChannelElement( $channel ) {

            return $( '.channel-' + $channel );
        }

        // Toggle the visibility of the chat
        function toggleChat( $channel ) {

            // Get box related to channel
            var $box = getChannelElement( $channel );

            $box.toggleClass('block-mode-hidden');

            if( $box.hasClass( 'block-mode-hidden' ) ) {

                // Change icons
                $box.find( '.chat-toggle i' ).removeClass(iconContent).addClass(iconContentActive);

                // Modify the storage to visible false
                modifyStorage( $box.data( 'other' ), false );

            } else {

                // change icons
                $box.find( '.chat-toggle i' ).removeClass(iconContentActive).addClass(iconContent);

                // Get messages box
                var $boxMessages = $box.find( '.js-chat-messages' );

                // Scroll to bottom
                $boxMessages.scrollTop( $boxMessages[0].scrollHeight );

                // Modify the storage to visible true
                modifyStorage( $box.data( 'other' ) );
            }
        }

        // Prepare storage
        function initStorage() {

            // Get chats
            var $storage = localStorage.getItem( $localStorage );

            // If the storage is null and the length
            if( $storage === null || $storage.length < 1 ) {

                // Set default
                localStorage.setItem( $localStorage, JSON.stringify( {} ) );
            }
            else {

                var $chats = JSON.parse( $storage );

                $.each( $chats, function( $key, $value ) {

                    var $channelId = newChat( $key );
                });
            }
        }

        // Add chat to storage
        function addToStorage( $other, $visible = true ) {

            // Get chats
            var $chats = localStorage.getItem( $localStorage );

            // Parse the json
            $chats = JSON.parse( $chats );

            if( $other !== null && ! ( $other in $chats ) ) {

                $chats[$other] = ($visible ? 'visible' : 'hidden');
            }

            // Set chats to json
            $chats = JSON.stringify( $chats );

            // Set storage
            localStorage.setItem( $localStorage, $chats );
        }

        // Modify chat storage
        function modifyStorage( $other, $visible = true ) {

            // Get chats
            var $chats = localStorage.getItem( $localStorage );

            // Parse the json
            $chats = JSON.parse( $chats );

            if( $other !== null && ( $other in $chats ) ) {

                $chats[$other] = ($visible ? 'visible' : 'hidden');
            }

            // Set chats to json
            $chats = JSON.stringify( $chats );

            // Set storage
            localStorage.setItem( $localStorage, $chats );
        }

        // Remove chat from storage
        function removeFromStorage( $other ) {

            // Get chats
            var $chats = localStorage.getItem( $localStorage );

            // Parse the json
            $chats = JSON.parse( $chats );

            if( $other !== null && ( $other in $chats ) ) {

                delete $chats[$other];
            }

            // Set chats to json
            $chats = JSON.stringify( $chats );

            // Set storage
            localStorage.setItem( $localStorage, $chats );
        }

        // Get chat status from storage
        function getStatusFromStorage( $other ) {

            // Get chats
            var $chats = localStorage.getItem( $localStorage );

            // Parse the json
            $chats = JSON.parse( $chats );

            if( $other !== null && ( $other in $chats ) ) {

                return $chats[$other];
            }
            return 'visible';
        }

        // Send a notification
        function doNotification( $title, $options = {} ) {

            // If we have permission
            if( Notification.permission === 'granted' ) {

                // Do the notification
                var n = new Notification( $title, $options );
            }
        }

	</script>
@endpushonce


