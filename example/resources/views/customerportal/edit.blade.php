@extends('layouts.backend')

@include('scripts.datepicker')
@include('scripts.datatables')

@pushonce( 'scripts:customer-portal-edit' )
<script src="{{ asset('/js/custom/customer/customer_portal_edit.js') }}"></script>
@endpushonce

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">{{$portal->customer->cu_company_name_business}}
                    - {{$portal->portal->po_portal}} - {{$requesttypes[$portal->ktcupo_request_type]}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a
                                href="{{ url('customers') }}">@if($portal->customer->cu_type == 1) {{"Customers"}} @else {{"Resellers"}} @endif</a>
                        </li>
                        <li class="breadcrumb-item"><a
                                href="/customers/{{$portal->customer->cu_id}}/edit">{{$portal->customer->cu_company_name_business}}</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Portal</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <div class="block block-rounded block-bordered">
            <ul class="nav nav-tabs nav-tabs-block js-tabs-enabled" data-toggle="tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" href="#btabs-static-home">General</a>
                </li>
                <li class="nav-item" @if($portal->ktcupo_type == 2) style="display: none;" @endif>
                    <a class="nav-link" href="#btabs-static-profile">Status updates</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#btabs-static-request_deliveries">Request deliveries</a>
                </li>
                <li class="nav-item" @if($portal->ktcupo_type == 2) style="display: none;" @endif>
                    <a class="nav-link" href="#btabs-static-payment_rates">Payment rates</a>
                </li>
                <li class="nav-item" @if($portal->ktcupo_type == 2) style="display: none;" @endif>
                    <a class="nav-link" href="#btabs-static-free_trials">Free trials</a>
                </li>
                <li class="nav-item" @if($portal->ktcupo_type == 2) style="display: none;" @endif>
                    <a class="nav-link" href="#btabs-static-origins">Origins</a>
                </li>
                <li class="nav-item" @if($portal->ktcupo_type == 2) style="display: none;" @endif>
                    <a class="nav-link" href="#btabs-static-destination">Destinations</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#btabs-static-history">History</a>
                </li>
            </ul>
            <div class="block-content tab-content">
                <div class="tab-pane active" id="btabs-static-home" role="tabpanel">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="block block-rounded block-bordered">
                                <div class="block-header block-header-default">
                                    <h3 class="block-title">General</h3>
                                </div>
                                <div class="block-content">
                                    <form class="mb-5" method="post"
                                          action="{{action('CustomerPortalController@update', $portal->ktcupo_id)}}">
                                        @csrf
                                        <input name="_method" type="hidden" value="PATCH">
                                        <input name="form_name" type="hidden" value="General">

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-4 col-form-label"
                                                   for="description">Description:</label>
                                            <div class="col-sm-6">
                                                <input type="text" autocomplete="off" class="form-control"
                                                       name="description" value="{{$portal->ktcupo_description}}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-4 col-form-label"
                                                   for="status">Status:</label>
                                            <div class="col-sm-6">
                                                <select type="text" class="form-control" name="status">
                                                    <option value=""></option>
                                                    @foreach($portalstatuses as $id => $status)
                                                        <option
                                                            value="{{$id}}" {{ ($portal->ktcupo_status == $id) ? 'selected': '' }} >{{$status}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-md-1"></div>
                                            <div class="form-group col-md-8">
                                                <button type="submit" class="btn btn-primary">Update</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="block block-rounded block-bordered">

                                <form class="mb-5" method="post"
                                      action="{{action('CustomerPortalController@update', $portal->ktcupo_id)}}">

                                    <div class="block-header block-header-default">
                                        <h3 class="block-title">Filters - Request</h3>

                                        <label class="col-form-label"
                                               for="ktcupo_enable_room_sizes">Only receive leads with room sizes:&nbsp;&nbsp;</label>

                                        <div
                                            class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                            <input type="checkbox" class="custom-control-input"
                                                   id="ktcupo_enable_room_sizes"
                                                   name="ktcupo_enable_room_sizes" {{$portal['ktcupo_room_sizes_enabled'] ? "checked='checked'" : ""}} >
                                            <label class="custom-control-label"
                                                   for="ktcupo_enable_room_sizes"></label>
                                        </div>

                                    </div>
                                    <div class="block-content">

                                        @csrf
                                        <input name="_method" type="hidden" value="PATCH">
                                        <input name="form_name" type="hidden" value="Filters - Request">

                                        <div id="moving_sizes_block"
                                             style="{{$portal['ktcupo_room_sizes_enabled'] ? "display:none;" : "display:block;"}}">

                                            @foreach($moving_sizes as $id => $size)
                                                <div class="form-group row">
                                                    <div class="col-sm-1"></div>
                                                    <label class="col-sm-4 col-form-label"
                                                           for="ktcupo_moving_size_{{$id}}">{{$size}}:</label>
                                                    <div class="col-sm-7">
                                                        <div
                                                            class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                                            <input type="checkbox" class="custom-control-input"
                                                                   id="ktcupo_moving_size_{{$id}}"
                                                                   name="ktcupo_moving_size_{{$id}}" {{($portal['ktcupo_moving_size_'.$id] ? "checked=''" : "")}}>
                                                            <label class="custom-control-label"
                                                                   for="ktcupo_moving_size_{{$id}}"></label>
                                                        </div>
                                                    </div>
                                                </div>

                                            @endforeach

                                        </div>

                                        <div id="room_sizes_block"
                                             style="{{$portal['ktcupo_room_sizes_enabled'] ? "display:block;" : "display:none;"}}">

                                            @foreach($roomamounts as $id => $size)
                                                <div class="form-group row">
                                                    <div class="col-sm-1"></div>
                                                    <label class="col-sm-4 col-form-label"
                                                           for="ktcupo_room_size_{{$id}}">{{$size}} rooms</label>
                                                    <div class="col-sm-7">
                                                        <div
                                                            class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                                            <input type="checkbox" class="custom-control-input"
                                                                   id="ktcupo_room_size_{{$id}}"
                                                                   name="ktcupo_room_size_{{$id}}" {{($portal['ktcupo_room_size_'.$id] ? "checked=''" : "")}}>
                                                            <label class="custom-control-label"
                                                                   for="ktcupo_room_size_{{$id}}"></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach

                                        </div>

                                        @if($portal->portal->po_destination_type == 2)

                                        <h2 class="content-heading pt-0"></h2>

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-4 col-form-label" for="local_moves">Local moves:</label>
                                            <div class="col-sm-7">
                                                <div
                                                    class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                                    <input type="checkbox" class="custom-control-input"
                                                           id="local_moves"
                                                           name="local_moves" {{($portal->ktcupo_nat_local_moves ? "checked=''" : "")}}>
                                                    <label class="custom-control-label" for="local_moves"></label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-4 col-form-label" for="long_distance_moves">Long distance moves:</label>
                                            <div class="col-sm-7">
                                                <div
                                                    class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                                    <input type="checkbox" class="custom-control-input"
                                                           id="long_distance_moves"
                                                           name="long_distance_moves" {{($portal->ktcupo_nat_long_distance_moves ? "checked=''" : "")}}>
                                                    <label class="custom-control-label" for="long_distance_moves"></label>
                                                </div>
                                            </div>
                                        </div>

                                        @endif

                                        <h2 class="content-heading pt-0"></h2>

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-4 col-form-label" for="export_moves">Export
                                                moves:</label>
                                            <div class="col-sm-7">
                                                <div
                                                    class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                                    <input type="checkbox" class="custom-control-input"
                                                           id="export_moves"
                                                           name="export_moves" {{($portal->ktcupo_export_moves ? "checked=''" : "")}}>
                                                    <label class="custom-control-label" for="export_moves"></label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-4 col-form-label" for="import_moves">Import
                                                moves:</label>
                                            <div class="col-sm-7">
                                                <div
                                                    class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                                    <input type="checkbox" class="custom-control-input"
                                                           id="import_moves"
                                                           name="import_moves" {{($portal->ktcupo_import_moves ? "checked=''" : "")}}>
                                                    <label class="custom-control-label" for="import_moves"></label>
                                                </div>
                                            </div>
                                        </div>

                                        <h2 class="content-heading pt-0"></h2>

                                        <div class="row">
                                            <div class="col-md-1"></div>
                                            <div class="form-group col-md-8">
                                                <button type="submit" class="btn btn-primary">Update</button>
                                            </div>
                                        </div>

                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="block block-rounded block-bordered"
                                 @if($portal->ktcupo_type == 2) style="display: none;" @endif>
                                <div class="block-header block-header-default">
                                    <h3 class="block-title">Filter - Max requests</h3> @if($capping_method !== 0) <span
                                        style="color:red;">Overwritten by customer settings. Capping Method is not on Portal level!</span> @endif
                                </div>
                                <div class="block-content">
                                    <form class="mb-5" method="post"
                                          action="{{action('CustomerPortalController@update', $portal->ktcupo_id)}}">
                                        @csrf
                                        <input name="_method" type="hidden" value="PATCH">
                                        <input name="form_name" type="hidden" value="Filters - Cappings">

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-6 col-form-label" for="monthly_capping_triglobal">Monthly
                                                capping TriGlobal</label>
                                            <div class="col-sm-4">
                                                <input type="text" autocomplete="off" class="form-control"
                                                       name="monthly_capping_triglobal"
                                                       value="{{$portal->ktcupo_monthly_capping_triglobal}}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-6 col-form-label" for="monthly_capping_client">Monthly
                                                capping Client</label>
                                            <div class="col-sm-4">
                                                <input type="text" autocomplete="off" class="form-control"
                                                       name="monthly_capping_client"
                                                       value="{{$portal->ktcupo_monthly_capping_client}}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-6 col-form-label" for="max_requests_month">Max.
                                                requests
                                                per month:</label>
                                            <div class="col-sm-4">
                                                <input type="text" autocomplete="off" class="form-control"
                                                       name="max_requests_month"
                                                       value="{{$portal->ktcupo_max_requests_month}}" disabled>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-6 col-form-label" for="max_requests_month_left">Max.
                                                requests
                                                per month (left):</label>
                                            <div class="col-sm-4">
                                                <input type="text" autocomplete="off" class="form-control"
                                                       name="max_requests_month_left" disabled
                                                       value="{{$portal->ktcupo_max_requests_month_left}}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-6 col-form-label" for="max_requests_day">Max. requests
                                                per day:</label>
                                            <div class="col-sm-4">
                                                <input type="text" autocomplete="off" class="form-control"
                                                       name="max_requests_day"
                                                       value="{{$portal->ktcupo_max_requests_day}}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-6 col-form-label" for="max_requests_day_left">Max.
                                                requests
                                                per day (left):</label>
                                            <div class="col-sm-4">
                                                <input type="text" autocomplete="off" class="form-control"
                                                       name="max_requests_day_left" disabled
                                                       value="{{$portal->ktcupo_max_requests_day_left}}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-6 col-form-label" for="daily_average">Daily
                                                average:</label>
                                            <div class="col-sm-4">
                                                <input type="text" disabled autocomplete="off" class="form-control"
                                                       name="daily_average"
                                                       value="{{number_format($portal->ktcupo_daily_average, 2, '.', '')}}">

                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-md-1"></div>
                                            <div class="form-group col-md-8">
                                                <button type="submit" class="btn btn-primary">Update</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="block block-rounded block-bordered">
                                <div class="block-header block-header-default">
                                    <h3 class="block-title">Match summary</h3>
                                </div>
                                <div class="block-content">
                                    <button type="button"
                                            class="btn btn-sm btn-primary view_match_summary js-tooltip-enabled mb-2"
                                            data-ktcupo_id="{{$portal->ktcupo_id}}"
                                            data-ktcupo_type="{{$portal->ktcupo_type}}" data-toggle="tooltip"
                                            data-original-title="Summary">View
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="btabs-static-profile" role="tabpanel">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="block block-rounded block-bordered">
                                <div class="block-header block-header-default">
                                    <h3 class="block-title">
                                        <a href="{{ url(''.(($portal->customer->cu_type == 1) ? 'customers' : 'resellers').'/' . $portal->ktcupo_cu_id . '/customerportal/' . $portal->ktcupo_id . '/customerportalstatus/create')}}">
                                            <button class="btn btn-primary">Add a status update</button>
                                        </a>
                                    </h3>
                                </div>
                                <div class="block-content">
                                    <table
                                        class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Status</th>
                                            <th>Date</th>
                                            <th>Hour</th>
                                            <th>Description</th>
                                            <th>Processed</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if (sizeof($statusupdates) > 0)
                                            @foreach ($statusupdates as $statusupdate)
                                                <tr>
                                                    <td>{{$statusupdate->stup_id}}</td>
                                                    <td>{{$portalstatuses[$statusupdate->stup_status]}}</td>
                                                    <td>{{$statusupdate->stup_date}}</td>
                                                    <td>{{$statusupdate->stup_hour}}</td>
                                                    <td>{{$statusupdate->stup_description}}</td>
                                                    <td>{{$statusupdate->stup_processed}}</td>
                                                    <td class="text-center">
                                                        <div class="btn-group">
                                                            <form
                                                                action="{{ route('customerportalstatus.destroy' , $statusupdate->stup_id)}}"
                                                                method="POST">
                                                                <input name="_method" type="hidden" value="DELETE">
                                                                @csrf
                                                                <button class="btn btn-sm btn-primary"
                                                                        data-toggle="tooltip"
                                                                        data-placement="left"
                                                                        title="Delete"
                                                                        type="submit">
                                                                    <i class="fa fa-times"></i>
                                                                </button>
                                                            </form>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="btabs-static-request_deliveries" role="tabpanel">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="block block-rounded block-bordered">
                                <div class="block-header block-header-default">
                                    <h3 class="block-title">
                                        <a href="{{ url(''.(($portal->customer->cu_type == 1) ? 'customers' : 'resellers').'/' . $portal->ktcupo_cu_id . '/customerportal/'. $portal->ktcupo_id . '/requestdelivery/create')}}">
                                            <button class="btn btn-primary">Add a request delivery</button>
                                        </a>
                                    </h3>
                                </div>
                                <div class="block-content">
                                    <table
                                        class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Type</th>
                                            <th>Value</th>
                                            <th>Extra</th>
                                            <th>Modify</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(sizeof($requestdeliveries) > 0)
                                            @foreach ($requestdeliveries as $requestdelivery)
                                                <tr>
                                                    <td>{{$requestdelivery->rede_id}}</td>
                                                    <td>{{$requestdeliverytypes[$requestdelivery->rede_type]}}</td>
                                                    <td>{{$requestdelivery->rede_value}}</td>
                                                    <td>{{$requestdelivery->rede_extra}}</td>
                                                    <td class="text-center">
                                                        <div class="btn-group">
                                                            <a class="btn btn-sm btn-primary" data-toggle="tooltip"
                                                               data-placement="left"
                                                               title="edit"
                                                               href="{{ url('customers/' . $portal->ktcupo_cu_id . '/customerportal/' . $portal->ktcupo_id . '/requestdelivery/' . $requestdelivery->rede_id . '/edit')}}">
                                                                <i class="fa fa-pencil-alt"></i>
                                                            </a>

                                                            <form
                                                                action="{{ route('requestdelivery.destroy', $requestdelivery->rede_id)}}"
                                                                method="POST">
                                                                <input name="_method" type="hidden" value="DELETE">
                                                                @csrf
                                                                <button class="btn btn-sm btn-primary"
                                                                        data-toggle="tooltip"
                                                                        data-placement="left"
                                                                        title="Delete"
                                                                        type="submit">
                                                                    <i class="fa fa-times"></i>
                                                                </button>
                                                            </form>
                                                        </div>

                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="btabs-static-payment_rates" role="tabpanel">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="block block-rounded block-bordered">
                                <div class="block-header block-header-default">
                                    <h3 class="block-title">
                                        <a href="{{ url(''.(($portal->customer->cu_type == 1) ? 'customers' : 'resellers').'/' . $portal->ktcupo_cu_id . '/customerportal/'. $portal->ktcupo_id . '/paymentrate/create')}}">
                                            <button class="btn btn-primary">Add a Payment rate</button>
                                        </a>
                                    </h3>
                                </div>

                                <div class="block-content">
                                    <table
                                        class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Date</th>
                                            <th>Rate</th>
                                            <th>Discount</th>
                                            @if($portal->portal->po_destination_type == 2)
                                                <th>NAT Type</th>
                                            @endif
                                            <th>Modify</th>
                                        </tr>
                                        </thead>
                                        @if(sizeof($paymentrates) > 0)
                                            <tbody>
                                            @foreach ($paymentrates as $paymentrate)
                                                <tr>
                                                    <td>{{$paymentrate->para_id}}</td>
                                                    <td>{{$paymentrate->para_date}}</td>
                                                    <td>{{$payment_currency. " ".$paymentrate->para_rate}}</td>
                                                    <td>@if ($paymentrate->para_discount_type == 1) @foreach ($paymentcurrencies as $pc) @if ($portal->portal->po_pacu_code == $pc->pacu_code) {{$pc->pacu_token.' '.$paymentrate->para_discount_rate}} @endif  @endforeach  @endif  @if ($paymentrate->para_discount_type == 0) {{"-"}} @endif @if ($paymentrate->para_discount_type == 2) {{$paymentrate->para_discount_rate." %"}} @endif </td>
                                                    @if($portal->portal->po_destination_type == 2)
                                                        <td>{{$pricetypes[$paymentrate->para_nat_type]}}</td>
                                                    @endif
                                                    <td class="text-center">
                                                        @if(strtotime($paymentrate->para_date) > time())
                                                            <form
                                                                action="{{ route('paymentrate.destroy',$paymentrate->para_id)}}"
                                                                method="POST">
                                                                <input name="_method" type="hidden" value="DELETE">
                                                                @csrf
                                                                <div class="btn-group">
                                                                    <a class="btn btn-sm btn-primary"
                                                                       data-toggle="tooltip"
                                                                       data-placement="left"
                                                                       title="edit"
                                                                       href="{{ url('customers/' . $portal->ktcupo_cu_id . '/customerportal/' . $portal->ktcupo_id . '/paymentrate/' . $paymentrate->para_id . '/edit')}}">
                                                                        <i class="fa fa-pencil-alt"></i>
                                                                    </a>
                                                                </div>
                                                            </form>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        @endif
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="btabs-static-free_trials" role="tabpanel">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="block block-rounded block-bordered">
                                <div class="block-content">
                                    <form class="mb-5" method="post"
                                          action="{{action('CustomerPortalController@update', $portal->ktcupo_id)}}">
                                        @csrf
                                        <input name="_method" type="hidden" value="PATCH">
                                        <input name="form_name" type="hidden" value="Free Trial">

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-4 col-form-label" for="free_trial">Free trial:</label>
                                            <div class="col-sm-7">
                                                <div
                                                    class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                                    <input type="checkbox" class="custom-control-input"
                                                           id="free_trial"
                                                           name="free_trial" {{($portal->ktcupo_free_trial ? "checked=''" : "")}}>
                                                    <label class="custom-control-label" for="free_trial"></label>
                                                </div>
                                            </div>
                                        </div>

                                        <h2 class="content-heading pt-0"></h2>

                                        <div class="row">
                                            <div class="col-md-1"></div>
                                            <div class="form-group col-md-8">
                                                <button type="submit" class="btn btn-primary">Update</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="block-header block-header-default">
                                <h3 class="block-title">
                                    Free trials
                                    <a href="{{ url(''.(($portal->customer->cu_type == 1) ? 'customers' : 'resellers').'/' . $portal->ktcupo_cu_id . '/customerportal/' . $portal->ktcupo_id . '/freetrial/create')}}">
                                        <button class="btn btn-primary">Add a free trial</button>
                                    </a>
                                </h3>
                            </div>
                            <div class="block-content">
                                <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Date</th>
                                        <th>Stop type</th>
                                        <th>Stop date</th>
                                        <th>Stop requests</th>
                                        <th>Stop requests (left)</th>
                                        <th>Finished</th>
                                        <th>Modify</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(sizeof($freetrials) > 0)
                                        @foreach ($freetrials as $freetrial)
                                            <tr>
                                                <td>{{$freetrial->frtr_id}}</td>
                                                <td>{{$freetrial->frtr_date}}</td>
                                                <td>{{$stoptypes[$freetrial->frtr_stop_type]}}</td>
                                                <td>{{$freetrial->frtr_stop_date}}</td>
                                                <td>{{$freetrial->frtr_stop_requests}}</td>
                                                <td>{{$freetrial->frtr_stop_requests_left}}</td>
                                                <td>{{$yesno[$freetrial->frtr_finished]}}</td>
                                                <td class="text-center">
                                                    @if(!$freetrial->frtr_finished)
                                                        <div class="btn-group">
                                                            <form
                                                                action="{{ route('freetrial.destroy', $freetrial->frtr_id)}}"
                                                                method="POST">
                                                                <input name="_method" type="hidden" value="DELETE">
                                                                @csrf
                                                                <button class="btn btn-sm btn-primary"
                                                                        data-toggle="tooltip"
                                                                        data-placement="left"
                                                                        title="Delete"
                                                                        type="submit">
                                                                    <i class="fa fa-times"></i>
                                                                </button>
                                                            </form>
                                                        </div>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="btabs-static-origins" role="tabpanel">
                    <a href="#" class="btn btn-primary select_all_or">Select all</a>
                    <a href="#" class="btn btn-primary deselect_all_or">Deselect all</a>
                    <br/><br/>
                    <form class="mb-5" method="post"
                          action="{{action('CustomerPortalController@update', $portal->ktcupo_id)}}">
                        @csrf
                        <input name="_method" type="hidden" value="PATCH">
                        <input name="form_name" type="hidden" value="Origins">
                        <div class="row">
                            <div class="form-group col-md-8">
                                <button type="submit" class="btn btn-primary">Update regions</button>
                            </div>
                        </div>
                        <div class="row">
                            @foreach($origins as $country => $parents)
                                @foreach($parents as $parent_name => $regions)
                                    <div class="col-md-2">
                                        <div class="well" style="position: relative;display: block;">
                                            <div class="block block-rounded block-bordered">
                                                <div class="block-header block-header-default">
                                                    <h3 class="block-title"> {{$parent_name}}</h3>
                                                    <a href="#" class="btn-sm btn-primary select_macro">+</a>
                                                    <a href="#" class="btn-sm btn-primary deselect_macro">-</a>
                                                </div>
                                                <div class="block-content">
                                                    @foreach($regions as $id => $region)
                                                        <div class="row">
                                                            <label class="col-sm-9 col-form-label"
                                                                   for="origins[{{$id}}]">{{$region[0]}}</label>
                                                            <div class="col-sm-3">
                                                                <div
                                                                    class="custom-control custom-switch custom-control-inline custom-control-primary">
                                                                    <input type="checkbox" class="custom-control-input"
                                                                           id="origins[{{$id}}]"
                                                                           name="origins[{{$id}}]"
                                                                        {{($region[1] ? "checked" : "")}}>
                                                                    <label class="custom-control-label"
                                                                           for="origins[{{$id}}]"></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endforeach

                        </div>
                        <div class="row">
                            <div class="form-group col-md-8">
                                <button type="submit" class="btn btn-primary">Update regions</button>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="tab-pane" id="btabs-static-destination" role="tabpanel">

                    <a href="#" class="btn btn-primary select_all_de">Select all</a>
                    <a href="#" class="btn btn-primary deselect_all_de">Deselect all</a>
                    <a href="#" class="btn btn-primary select_eu">Select all EU destinations</a>
                    <a href="#" class="btn btn-primary select_schengen_area">Select all Schengen destinations</a>
                    <br/>
                    <br/>
                    <form class="mb-5" method="post"
                          action="{{action('CustomerPortalController@update', $portal->ktcupo_id)}}">
                        @csrf
                        <input name="_method" type="hidden" value="PATCH">
                        <input name="form_name" type="hidden" value="Destinations">
                        <div class="row">
                            <div class="form-group col-md-9">
                                <button type="submit" class="btn btn-primary">Update regions</button>
                            </div>
                            <div class='col-md-3'>
                                <h3 id='total_leads_text' style='display: none; float:right;margin-right:20px;'>Total leads: <span id='total_leadamount'></span></h3>
                            </div>
                        </div>
                        @foreach($destinations as $continent => $continents)
                            @if($portal->portal->po_destination_type == 1)
                                <div style="margin-bottom: 10px;"><label
                                        style="font-size:1.25em;color:black;margin-right:10px;">{{$continent}}</label>
                                    <a href="#" class="btn btn-primary select_all">Select all</a>
                                    <a href="#" class="btn btn-primary deselect_all">Deselect all</a>
                                </div>
                            @endif

                            <div class="row">
                                @foreach($continents as $macro_region => $countries)
                                    <div style="width:18%;">
                                        <div class="well" style="position: relative;display: block;">
                                            <div class="block block-rounded block-bordered">
                                                <div class="block-header block-header-default">
                                                    <h3 class="block-title">{{$macro_region}} <span data-header_title='{{$macro_region}}' class="header_title badge badge-primary badge-pill"><img src="{{asset('/media/videos/loading_gif.gif') }}" width="15" /></span></h3>
<!--                                                    <h3 class="block-title">{{$macro_region}}</h3>-->
                                                    <a href="#" class="btn-sm btn-primary select_macro">+</a>
                                                    <a href="#" class="btn-sm btn-primary deselect_macro">-</a>
                                                </div>
                                                <div class="block-content">
                                                    @foreach($countries as $co_code => $country)
                                                        <div class="row">
                                                            <label class="col-sm-9 col-form-label"
                                                                   for="destinations[{{$co_code}}]">{{$country[0]}}</label>
                                                            <div class="col-sm-3">
                                                                <div
                                                                    class="custom-control custom-switch custom-control-inline custom-control-primary">
                                                                    <input type="checkbox" class="custom-control-input"
                                                                           id="destinations[{{$co_code}}]"
                                                                           name="destinations[{{$co_code}}]"
                                                                        {{($country[1] ? "checked" : "")}}>
                                                                    <label class="custom-control-label"
                                                                           for="destinations[{{$co_code}}]"></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div style='width:2%;'></div>
                                @endforeach
                            </div>
                        @endforeach

                        <div class="row">
                            <div class="form-group col-md-8">
                                <button type="submit" class="btn btn-primary">Update regions</button>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="tab-pane" id="btabs-static-history" role="tabpanel">
                    <div class="block block-rounded block-bordered">
                        <div class="block-content block-content-full">
                            <table data-order='[[0, "desc"]]'
                                   class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Timestamp</th>
                                    <th>Changed by</th>
                                    <th>Change group</th>
                                    <th>Change</th>
                                    <th>Old value</th>
                                    <th>New value</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($portal_region_history as $change)
                                    <tr>
                                        <td>{{$change->id}}</td>
                                        <td>{{$change->created_at}}</td>
                                        <td>{{$change->userResponsible()->us_name ?? "System"}}</td>
                                        <td>{{$change->revisionable_type}}</td>
                                        <td>@if($change->revisionable_type == "App\\Models\\KTCustomerPortalCountry") {{$country_codes[$change->key]}} @else {{$region_names[$change->key]}} @endif</td>
                                        <td>{{$change->old_value}}</td>
                                        <td>{{$change->new_value}}</td>
                                    </tr>
                                @endforeach
                                @foreach ($portal_history as $change)
                                    <tr>
                                        <td>{{$change->id}}</td>
                                        <td>{{$change->created_at}}</td>
                                        <td>{{$change->userResponsible()->us_name ?? "System"}}</td>
                                        <td>{{$change->revisionable_type}}</td>
                                        <td>{{$change->key}}</td>
                                        @if($change->key == 'ktcupo_status')
                                            <td>{{$portalstatuses[$change->old_value]}}</td>
                                            <td>{{$portalstatuses[$change->new_value]}}</td>
                                        @else
                                            <td>{{$change->old_value}}</td>
                                            <td>{{$change->new_value}}</td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="viewPortalSummary" class="modal fade" role="dialog" style='padding-right: 52px;'>
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content" style="width:800px;">
                <div class="modal-header">
                    <h5 class="modal-title">Viewing Portal Summary
                        of: {{$portal->customer->cu_company_name_business}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="portal_summary">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div id="viewFormulaCalculation" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content" style="width:750px;box-shadow: 0px 0px 10px #0000004a;">
                <div class="modal-header">
                    <h5 class="modal-title">Formula calculation</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h2>Current</h2>
                    <div>
                        Average match: <span id='calc_average_match'></span><br/>
                        Payment rate: <span id='calc_payment_rate'></span><br/>
                        Claim percentage: <span id='calc_claim_percentage'></span><br/>
                        Claim adjusted lead price: <span id='calc_adjusted_lead_price'></span><br/>
                        Correction bonus: <span id='calc_correction_bonus'></span><br/>
                        Formula price: <span id='calc_formula_price'></span><br/>
                    </div>

                    <br/>

                    <h2>Calculate new price</h2>
                    <div>
                        Payment rate <span class='pr_currency_token'></span>: <input name='calculation_payment_rate'
                                                                                     type='number' min="0" value="0"
                                                                                     step=".01"/>
                        <button data-ktcupo_id='{{$portal->ktcupo_id}}' data-cu_id='{{$portal->customer->cu_id}}'
                                id='calculate_formula' type='button' class='btn btn-sm btn-primary'>Calculate
                        </button>
                    </div>

                    <div id='calculated_formula' class='mt-5' style='display:none;'>
                        Average match: <span id='calculated_average_match'></span><br/>
                        Payment rate: <span class='pr_currency_token'></span><span
                            id='calculated_payment_rate'></span><br/>
                        Claim percentage: <span id='calculated_claim_percentage'></span><br/>
                        Claim adjusted lead price: <span class='pr_currency_token'></span><span
                            id='calculated_adjusted_lead_price'></span><br/>
                        Correction bonus: <span id='calculated_correction_bonus'></span><br/>
                        Formula price: <span class='pr_currency_token'></span><span
                            id='calculated_formula_price'></span><br/>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default close_modal" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@push( 'scripts' )

    <script>

        $("#ktcupo_enable_room_sizes").click(function () {
            $("#room_sizes_block").toggle(250);
            $("#moving_sizes_block").toggle(250);
        });

        jQuery(document).ready(function () {
            //Calculate lead amounts per destination header based on selected origins
            jQuery.ajax({
                url: "/ajax/customer_portal_calculate_lead_amounts",
                method: 'get',
                data: {portal_id: {{$portal->ktcupo_id}}, destination_type: {{$portal->portal->po_destination_type}}},
                success: function (data) {
                    var returnedData = JSON.parse(data);
                    console.log(returnedData);

                    var total_leads = 0;

                    jQuery.each(returnedData , function(index, val) {
                        $span = jQuery("span[data-header_title='" + index + "']");
                        $span.html(val);
                        $span.show();

                        total_leads += val;
                    });

                    jQuery("#total_leadamount").html(total_leads);
                    jQuery("#total_leads_text").show();
                }
            });

            $("form .select_all").click(function () {
                $(this).parent().next().find("input[type=checkbox]:enabled").prop("checked", true);

                $("input[type=checkbox]").trigger("change");

                return false;
            });

            $("form .select_macro").click(function () {
                $(this).parent().next().find("input[type=checkbox]:enabled").prop("checked", true);

                $("input[type=checkbox]").trigger("change");

                return false;
            });

            $("form .deselect_all").click(function () {
                $(this).parent().next().find("input[type=checkbox]:enabled").prop("checked", false);

                $("input[type=checkbox]").trigger("change");

                return false;
            });

            $("form .deselect_macro").click(function () {
                $(this).parent().next().find("input[type=checkbox]:enabled").prop("checked", false);

                $("input[type=checkbox]").trigger("change");

                return false;
            });


            jQuery(document).on('click', '.view_match_summary', function (e) {
                e.preventDefault();

                var $self = jQuery(this);

                if ($self.data("ktcupo_type") == 2) {
                    var link_to_function = "/ajax/customer_portal_summary_leadsstore";
                } else {
                    var link_to_function = "/ajax/customer_portal_summary";
                }

                jQuery.ajax({
                    url: link_to_function,
                    method: 'get',
                    data: {ktcupo_id: $self.data('ktcupo_id')},
                    success: function ($result) {
                        $("#portal_summary").html($result);

                        $("#viewPortalSummary").modal("toggle");
                    }
                });
            });


            jQuery(document).on('click', '.view_formula_calculation', function (e) {
                e.preventDefault();

                //jQuery("span#calculated_average_match").text(jQuery("span#portal_avg_match").text());
                jQuery("span#calc_average_match").text(jQuery("span#portal_avg_match").text());
                jQuery("span#calc_payment_rate").text(jQuery("span#portal_payment_rate").text());
                jQuery("span#calc_claim_percentage").text(jQuery("span#portal_claim_percentage").text());
                jQuery("span#calc_adjusted_lead_price").text(jQuery("span#portal_adjusted_lead_price").text());
                jQuery("span#calc_correction_bonus").text(jQuery("span#portal_correction_bonus").text());
                jQuery("span#calc_formula_price").text(jQuery("span#portal_formula_price").text());
                jQuery("span.pr_currency_token").text(jQuery("span#portal_currency_token").text());

                $("#viewFormulaCalculation").modal("toggle");

                //$("#viewPortalSummary").css("opacity", "0.5")
            });


            jQuery(document).on('click', '#viewFormulaCalculation .close,#viewFormulaCalculation .close_modal', function (e) {
                e.preventDefault();

                //$("#viewPortalSummary").css("opacity", "unset")

            });

            jQuery(document).on('click', '#calculate_formula', function (e) {
                e.preventDefault();

                jQuery.ajax({
                    url: "/ajax/customer_portal_summary_calculate_formula",
                    method: 'get',
                    data: {
                        payment_rate: jQuery("input[name=calculation_payment_rate]").val(),
                        cu_id: jQuery(this).data('cu_id'),
                        ktcupo_id: jQuery(this).data('ktcupo_id'),
                        currency_token: jQuery(this).data('currency_token'),
                        average_match: jQuery("span#portal_average_match_portal_level").text()
                    },
                    success: function (data) {
                        var returnedData = JSON.parse(data);
                        console.log(returnedData)

                        jQuery("span#calculated_average_match").text(returnedData.average_match)
                        jQuery("span#calculated_payment_rate").text(returnedData.payment_rate)
                        jQuery("span#calculated_claim_percentage").text(returnedData.claim_percentage)
                        jQuery("span#calculated_adjusted_lead_price").text(returnedData.claim_adjusted_lead_price)
                        jQuery("span#calculated_correction_bonus").text(returnedData.correction_bonus)
                        jQuery("span#calculated_formula_price").text(returnedData.formula_price)

                        $("#calculated_formula").slideDown();
                    }
                });
            });

        });
    </script>

@endpush

