@extends('layouts.backend')

@include('scripts.select2')

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Add a Portal to {{$customer->cu_company_name_business}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../../">@if($customer->cu_type == 1) {{"Customers"}} @else {{"Resellers"}} @endif</a></li>
                        <li class="breadcrumb-item"><a href="../edit">{{$customer->cu_company_name_business}}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Add Portal</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="block block-rounded block-bordered">
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    {{$error}}<br>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="block-header block-header-default">
                        <h3 class="block-title">General</h3>
                    </div>
                    <div class="block-content">
                        <form class="mb-5" method="post" action="{{action('CustomerPortalController@store')}}">
                            @csrf
                            <input name="_method" type="hidden" value="post">
                            <input name="customer_id" type="hidden" value="{{$customer->cu_id}}">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="portal_type">Type:</label>
                                        <div class="col-sm-7">
                                            <select type="text" class="form-control"
                                                    name="portal_type">
                                                <option value=""></option>
                                                @foreach($portal_types as $id => $type)
                                                    <option value="{{$id}}">{{$type}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row type_all" >
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="portal">Portal:</label>
                                        <div class="col-sm-7">
                                            <select type="text" class="js-select2 form-control"
                                                    name="portal">
                                                <option value=""></option>
                                                @foreach($portals as $portal)
                                                    <option value="{{$portal->po_id}}" {{ (Request::old('portal') == $portal->po_id) ? 'selected':'' }}>{{$portal->po_portal.' '.((!empty($portal->country->co_en) ? "(".$portal->country->co_en.")" : ""))}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row type_core">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="request_type">Request type:</label>
                                        <div class="col-sm-7">
                                            <select type="text" class="form-control"
                                                    name="request_type">
                                                <option value=""></option>
                                                @foreach($requesttypes as $id => $type)
                                                    <option value="{{$id}}" {{ (Request::old('request_type') == $id) ? 'selected':'' }}>{{$type}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row type_all">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="description">Description:</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="description"
                                                   value="">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h2 class="content-heading pt-0"></h2>

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Add</button>
                                    <a class="btn btn-primary" href="../edit">
                                        Back
                                    </a>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@push( 'scripts' )

    <script>
        jQuery(document).ready(function(){
            $(".type_all").hide();
            $(".type_core").hide();

            $("select[name=portal_type]").change(function(){
                if ($(this).val() == "") {
                    $(".type_all").slideUp();
                    $(".type_core").slideUp();
                }
                else {
                    $(".type_all").slideDown();
                    $(".type_all").css("width", "100%");
                    if ($(this).val() == 1) {
                        $(".type_core").slideDown();
                    }
                    else if($(this).val() == 2) {
                        $(".type_core").slideUp()
                    }
                }
            });

        });
    </script>

@endpush


