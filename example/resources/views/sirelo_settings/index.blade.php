@extends('layouts.backend')

@include( 'scripts.datatables' )
@include( 'scripts.dialogs' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Sirelo Settings</h1>

                <div class="btn-group">
                    <a href="{{ url('movingassistant')}}" class="btn btn-primary">
                        Moving Assistant
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">

        <div class="block block-rounded block-bordered">
            <div class="block-content block-content-full">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Sirelo Settings</h3>
                </div>
                <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Value</th>
                        <th>Last updated on</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($sirelo_settings as $setting)
                        <tr>
                            <td>{{$setting->sise_id}}</td>
                            <td>{{$setting->sise_description}}</td>
                            <td>{{$setting->sise_value}}</td>
                            <td>{{$setting->sise_updated_timestamp}}</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <a class="btn btn-sm btn-primary"
                                       data-toggle="tooltip"
                                       data-placement="left"
                                       title="edit"
                                       href="{{url('/sirelo_settings/'.$setting->sise_id.'/edit')}}">
                                        <i class="fa fa-pencil-alt"></i>
                                    </a>&nbsp;
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
