@extends('layouts.backend')

@include( 'scripts.datatables' )
@include( 'scripts.dialogs' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Moving Assistant Emails</h1>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">

        <div class="block block-rounded block-bordered">
            <div class="block-content block-content-full">
                <a href="{{ url('movingassistant/add_email')}}"><button data-toggle="click-ripple" class="btn btn-primary">Add Email</button></a>
                <br/>
                <br/>
                <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name of email</th>
                        <th>Description</th>
                        <th>Days before sending mail</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($emails as $email)
                        <tr>
                            <td>{{$email->moasem_id}}</td>
                            <td>{{$email->email->embute_name}}</td>
                            <td>{{$email->email->embute_description}}</td>
                            <td>{{$email->moasem_days_before_mail." day(s)"}}</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <a class="btn btn-sm btn-primary"
                                       data-toggle="tooltip"
                                       data-placement="left"
                                       title="edit"
                                       href="{{url('/movingassistant/'.$email->moasem_id.'/edit')}}">
                                        <i class="fa fa-pencil-alt"></i>
                                    </a>&nbsp;
                                    <button type="button" class="btn btn-sm btn-primary moasem_delete" data-toggle="tooltip"
                                            title="Delete" data-moasem_id="{{$email->moasem_id}}">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push( 'scripts' )

    <script>
        jQuery(document).ready(function () {
            jQuery(document).on('click', '.moasem_delete', function (e) {
                e.preventDefault();

                var $self = jQuery(this);

                confirmDelete("{{ url('ajax/moving_assistant/delete') }}", 'get', {id: $self.data('moasem_id')}, function () {

                    $self.parents('.dataTable').DataTable().row($self.parents('tr')).remove().draw('page');
                });
            });
        });
    </script>

@endpush
