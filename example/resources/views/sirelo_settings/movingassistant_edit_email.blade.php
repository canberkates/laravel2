@extends('layouts.backend')

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Edit Email {{$moasem->email->embute_name}} of the Moving Assistant</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/sirelo_settings">Sirelo Settings</a></li>
                        <li class="breadcrumb-item"><a href="/moving_assistant">Moving Assistant</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Edit Email</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="block block-rounded block-bordered">
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    {{$error}}<br>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="block-header block-header-default">
                        <h3 class="block-title">General</h3>
                    </div>
                    <div class="block-content">
                        <form class="mb-5" method="post" action="{{action('SireloSettingController@movingAssistantEditEmailUpdate', $moasem->moasem_id)}}">
                            @csrf
                            <input name="_method" type="hidden" value="post">

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-2 col-form-label" for="email_name">Email:</label>
                                <div class="col-sm-3">
                                    <select type="text" class="form-control" name="email_name">
                                        @foreach($emails as $email)
                                            <option @if($moasem->moasem_embute_id == $email->embute_id) selected @endif value="{{$email->embute_id}}">{{$email->embute_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-2 col-form-label" for="days_before">Days before:</label>
                                <div class="col-sm-3">
                                    <input type="number" class="form-control" name="days_before"
                                           value="{{$moasem->moasem_days_before_mail}}">
                                </div>
                            </div>

                            <div class="row" style="font-size:14px;">
                                <div class="col-sm-3"></div>
                                <div class="col-sm-5">
                                    <i>Example:<br />4 is 4 days <u>before</u> the moving date. <br />
                                        -10 is 10 days <u>after</u> the moving date.</i>
                                </div>
                            </div>

                            <h2 class="content-heading pt-0"></h2>

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                    <a class="btn btn-primary" href="/movingassistant">
                                        Back
                                    </a>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection




