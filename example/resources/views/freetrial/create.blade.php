@extends('layouts.backend')

@include('scripts.datepicker')

@section('content')
    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="block block-rounded block-bordered">
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    {{$error}}<br>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Add a Free Trial to {{$customer->cu_company_name_business}}</h3>
                    </div>
                    <div class="block-content">
                        <form class="mb-5" method="post" action="{{action('FreeTrialController@store')}}">
                                @csrf
                                <input name="_method" type="hidden" value="post">
                                <input name="ktcupo_id" type="hidden" value="{{$customerportal}}">

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="date">Date:</label>
                                <div class="col-sm-3">
                                    <input autocomplete="off" type="text" class="js-datepicker form-control" id="example-datepicker3" name="date" value="{{  Request::old('date') }}" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="stop_type">Stop type:</label>
                                <div class="col-sm-3">
                                    <select id="stop_type" type="text" class="form-control" name="stop_type">
                                        @foreach($freetrialstoptypes as $id => $status)
                                            <option @if (Request::old('stop_type') == $id) selected @endif value="{{$id}}">{{$status}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row" id="show_stop_type_date">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="stop_date">Stop date:</label>
                                <div class="col-sm-3">
                                    <input autocomplete="off" type="text" value="{{ Request::old('stop_date') }}" class="js-datepicker form-control" id="example-datepicker3" name="stop_date" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd">
                                </div>
                            </div>
                            <div id="show_stop_type_requests" style="display:none;">
                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="stop_requests">Stop requests:</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="stop_requests"
                                               value="{{Request::old('stop_requests')}}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="stop_requests_left">Stop requests (Left):</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="stop_requests_left"
                                               value="{{Request::old('stop_requests_left')}}">
                                    </div>
                                </div>
                            </div>


                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="remark">Remark:</label>
                                <div class="col-sm-7">
                                    <textarea type="text" class="form-control" name="remark"></textarea>
                                </div>
                            </div>

                            <h2 class="content-heading pt-0"></h2>


                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Create</button>
                                    <a class="btn btn-primary" href="../edit">
                                        Back
                                    </a>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@push('scripts')
    <script>
    $( "#stop_type" ).change(function() {
        var stop_type = $("#stop_type").val();

        var id_date_type_fields = $("#show_stop_type_date");
        var id_requests_type_fields = $("#show_stop_type_requests");

        if (stop_type == 1)
        {
            id_date_type_fields.slideDown();
            id_requests_type_fields.slideUp();
        }
        else if(stop_type == 2)
        {
            id_date_type_fields.slideUp();
            id_requests_type_fields.slideDown();
        }
        else if (stop_type == 3)
        {
            id_date_type_fields.slideDown();
            id_requests_type_fields.slideDown();
        }

    }).trigger('change');
    </script>
@endpush



