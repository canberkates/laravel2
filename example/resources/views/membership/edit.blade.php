@extends('layouts.backend')

@include('scripts.datepicker')

@section('content')

    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Edit a membership</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url("admin/memberships/")}}">Memberships</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Edit</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="block block-rounded block-bordered">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Editing {{$membership->me_name}}</h3>
                    </div>
                    <div class="block-content">
                        @if($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        {{$error}}<br>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="mb-5" method="post"
                              action="{{action('MembershipController@update', $membership->me_id)}}">
                            @csrf
                            <input name="_method" type="hidden" value="POST">

                            <h2 class="content-heading pt-0">General</h2>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="name">Name:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="name"
                                           value="{{$membership->me_name}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="location">Location:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="location"
                                           value="{{$membership->me_location}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="website">Website:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="website"
                                           value="{{$membership->me_website}}">
                                </div>
                            </div>

                            <h2 class="content-heading pt-0">Sirelo Links</h2>

                            @foreach($websites as $id => $website)
                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="sirelo[{{$id}}]">{{$website['website_name']}}:</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="sirelo[{{$id}}]"
                                               value="{{$website['link']}}">
                                    </div>
                                </div>
                            @endforeach

                            <h2 class="content-heading pt-0"></h2>

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection




