@extends('layouts.backend')

@include( 'scripts.datatables' )
@include( 'scripts.dialogs' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Users</h1>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">

        @include( 'admin.menu' )

        <div class="block block-rounded block-bordered">
            <div class="block-content block-content-full">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Memberships
                        <a href="{{ url('admin/memberships/create')}}"><button data-toggle="click-ripple" class="btn btn-primary">Add a membership</button></a>
                    </h3>
                </div>
                <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Location</th>
                        <th>Website</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($memberships as $membership)
                        <tr>
                            <td>{{$membership->me_id}}</td>
                            <td>{{$membership->me_name}}</td>
                            <td>{{$membership->me_location}}</td>
                            <td>{{$membership->me_website}}</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <a class="btn btn-sm btn-primary"
                                       data-toggle="tooltip"
                                       data-placement="left"
                                       title="edit"
                                       href="{{ url('/admin/memberships/' . $membership->me_id . '/edit')}}">
                                        <i class="fa fa-pencil-alt"></i>
                                    </a>&nbsp;

                                    <button type='button' class='btn btn-sm btn-primary membership_delete js-tooltip-enabled' data-toggle='tooltip' data-id='{{$membership->me_id}}' data-original-title='Delete'>
                                        <i class='fa fa-times'></i>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@push( 'scripts' )
    <script>
        jQuery(document).ready(function(){
            jQuery( document ).on( 'click', '.membership_delete', function(e) {

                e.preventDefault();

                var $self = jQuery(this);

                confirmDelete("{{ url('ajax/membership/delete') }}", 'get', {id:$self.data('id')}, function() {

                    $self.parents( '.dataTable' ).DataTable().row( $self.parents( 'tr' ) ).remove().draw( 'page' );
                });

            });
        });
    </script>

@endpush
