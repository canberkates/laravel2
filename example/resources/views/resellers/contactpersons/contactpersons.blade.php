<div class="block-header block-header-default">
    <h3 class="block-title">
        <a href="{{ url('resellers/' . $customer->cu_id . '/contactperson/create')}}">
            <button class="btn btn-primary">Add a contact person</button>
        </a>
    </h3>
</div>
<div class="block-content">
    <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
        <thead>
        <tr>
            <th>ID</th>
            <th>Full name</th>
            <th>Department</th>
            <th>Role</th>
            <th>Email</th>
            <th>Telephone</th>
            <th>Mobile</th>
            <th>Language</th>
            <th>Login</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($customer->contactpersons as $contactperson)
            @if ($contactperson->cope_deleted)
                @continue;
            @endif
            <tr>
                <td>{{$contactperson->cope_id}}</td>
                <td>{{$contactperson->cope_full_name}}</td>
                <td>{{$contactpersondepartments[$contactperson->cope_department]}}</td>
                <td>{{$contactpersondepartmentroles[$contactperson->cope_department_role]}}</td>
                <td>{{$contactperson->cope_email}}</td>
                <td>{{$contactperson->cope_telephone}}</td>
                <td>{{$contactperson->cope_mobile}}</td>
                <td>
                    @foreach($languages as $language)
                        @if($language->la_code == $contactperson->cope_language)
                            {{$language->la_language}}
                            @break
                        @endif
                    @endforeach
                </td>

                @if(empty($contactperson->application_user) || $contactperson->application_user->apus_deleted)
                    <td><i style="color:red" class="fa fa-times"></i></td>
                @else
                    <td><a target="_blank" href="{{$contactperson->login_url}}"><i
                                class="fa fa-key text-primary"></i></a></td>
                @endif
                <td class="text-center">
                    <div class="btn-group">
                        <a class="btn btn-sm btn-primary" data-toggle="tooltip"
                           data-placement="left"
                           title="edit"
                           href="{{ url('resellers/' . $customer->cu_id . '/contactperson/' . $contactperson->cope_id.'/edit')}}">
                            <i class="fa fa-pencil-alt"></i>
                        </a>
                    </div>
                    <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-primary contactperson_delete" data-toggle="tooltip" title="Delete contact person" data-contactperson_id="{{$contactperson->cope_id}}">
                            <i class="fa fa-times"></i>
                        </button>

                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
