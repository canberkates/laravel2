<div class="block-content">
    <form method="post" action="{{action('ResellersController@update', $customer->cu_id)}}">
        @csrf
        <input name="_method" type="hidden" value="PATCH">
        <input name="form_name" type="hidden" value="Finance">
        <div class="row">
            <div class="col-md-12">
                <h2 class="content-heading pt-0">Finance</h2>
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="finance_debtor_number">Debtor number:</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control"
                               name="finance_debtor_number"
                               value="{{$customer->cu_debtor_number}}"
                               disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label" for="credit_hold">Credit hold:</label>
                    <div class="col-sm-7">
                        <div
                            class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                            <input type="checkbox" class="custom-control-input"
                                   name="finance_credit_hold" {{($customer->cu_credit_hold ? "checked" : "")}}
                                   disabled>
                            <label class="custom-control-label"
                                   for="finance_credit_hold"></label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="finance_payment_currency">Payment currency:</label>
                    <div class="col-sm-7">
                        <select type="text" class="form-control"
                                name="finance_payment_currency" disabled>
                            <option value=""></option>
                            @foreach($paymentcurrencies as $pc)
                                <option
                                    value="{{$pc->pacu_code}}" {{ ($customer->cu_pacu_code== $pc->pacu_code) ? 'selected':'' }}>{{$pc->pacu_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="finance_payment_method">Payment method:</label>
                    <div class="col-sm-7">
                        <select type="text" class="form-control"
                                name="finance_payment_method" disabled>
                            <option value=""></option>
                            @foreach($paymentmethods as $id => $pm)
                                <option
                                    value="{{$id}}" {{ ($customer->cu_payment_method == $id) ? 'selected':'' }}>{{$pm}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="finance_payment_term">Payment term:</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control"
                               name="finance_payment_term"
                               value="{{$customer->cu_payment_term}}"
                               disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="finance_credit_limit">Credit limit:</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control"
                               name="finance_credit_limit"
                               value="{{$customer->cu_credit_limit}}"
                               disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label" for="finance_enforce_credit_limit">Enforce credit
                        limit:</label>
                    <div class="col-sm-7">
                        <div
                            class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                            <input type="checkbox" class="custom-control-input"
                                   name="finance_enforce_credit_limit"
                                   {{($customer->cu_enforce_credit_limit ? "checked" : "")}}
                                   disabled>
                            <label class="custom-control-label"
                                   for="finance_enforce_credit_limit"></label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="finance_vat_number">VAT number:</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control"
                               name="finance_vat_number"
                               value="{{$customer->cu_vat_number}}"
                               disabled>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
