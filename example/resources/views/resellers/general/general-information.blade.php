<div class="block-content">
    <form method="post" enctype="multipart/form-data"
          action="{{action('CustomersController@update', $customer->cu_id)}}">
        @csrf
        <input name="_method" type="hidden" value="PATCH">
        <input name="form_name" type="hidden" value="General">
        <h2 class="content-heading pt-0">General Information</h2>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="type">Type of partner:</label>
                    <div class="col-sm-7">
                        <select disabled type="text" class="form-control"
                                name="type">
                            @foreach($movertypes as $id => $item)
                                <option
                                    value="{{$id}}" {{ ($customer->cu_type == $id) ? 'selected':'' }}>{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="sales_manager">Sales Manager:</label>
                    <div class="col-sm-7">
                        <select disabled type="text" class="form-control"
                                name="sales_manager">
                            <option value=""></option>
                            @foreach($users as $user)
                                <option
                                    value="{{$user->id}}" {{ ($customer->cu_sales_manager == $user->id) ? 'selected':'' }}>{{$user->us_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="account_manager">Account Manager:</label>
                    <div class="col-sm-7">
                        <select disabled type="text" class="form-control"
                                name="account_manager">
                            <option value=""></option>
                            @foreach($users as $user)
                                <option
                                    value="{{$user->id}}" {{ ($customer->cu_account_manager == $user->id) ? 'selected':'' }}>{{$user->us_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="debt_manager">Debt Manager:</label>
                    <div class="col-sm-7">
                        <select disabled type="text" class="form-control"
                                name="debt_manager">
                            <option value=""></option>
                            @foreach($users as $user)
                                <option
                                    value="{{$user->id}}" {{ ($customer->cu_debt_manager == $user->id) ? 'selected':'' }}>{{$user->us_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="la_code">Language:</label>
                    <div class="col-sm-7">
                        <select type="text" class="form-control"
                                name="la_code">
                            @foreach($languages as $language)
                                <option
                                    value="{{$language->la_code}}" {{ ($customer->cu_la_code == $language->la_code) ? 'selected':'' }}>{{$language->la_language}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="description">Description:</label>
                    <div class="col-sm-7">
                          <textarea rows="6" type="text" class="form-control"
                                    name="description">{{$customer->cu_description}}</textarea>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="company_name_legal">Company legal name:</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control"
                               name="company_name_legal"
                               value="{{$customer->cu_company_name_legal}}">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="company_name_business">Company business name:</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control"
                               name="company_name_business"
                               value="{{$customer->cu_company_name_business}}">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="attn">Primary contact:</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control" name="attn"
                               value="{{$customer->cu_attn}}">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="attn_email">Primary contact email:</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control"
                               name="attn_email"
                               value="{{$customer->cu_attn_email}}">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="lead_email">Lead email:</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control" name="lead_email"
                               value="{{$customer->cu_email}}">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="lead_email">Billing email:</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control" name="billing_email"
                               value="{{$customer->cu_email_bi}}">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="email">Chamber of Commerce:</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control" name="coc"
                               value="{{$customer->cu_coc}}">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="logo">Replace logo:</label>
                    <div class="col-sm-7">
                        <input type="file" name="logo" class="form-control-file btn btn-primary"/>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <div style="margin-left:15px;">
                    @if(!empty($customer->cu_logo))
                        <img style="max-width: 300px; max-height: 120px;" src="../../logo.php?path={{env("SHARED_FOLDER")}}uploads/logos/{{$customer->cu_logo}}"/>
                    @else
                        <img src="{{ asset('media/logos/logo_placeholder.png') }}"
                            style="max-width: 300px; max-height: 120px;"/>
                    @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="form-group col-md-8">
                        <button type="submit" class="btn btn-primary">Update
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
