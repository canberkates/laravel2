<div class="block-content block-content-full">
    <form method="post"
          action="{{action('ResellersController@update', $customer->cu_id)}}">
        @csrf
        <input name="_method" type="hidden" value="PATCH">
        <input name="form_name" type="hidden" value="Memberships">
        <table data-page-length='50' class="table table-bordered table-striped table-vcenter js-dataTable-full">
            <thead>
            <tr>
                <th>Member</th>
                <th>ID</th>
                <th>Name</th>
                <th>Location</th>
                <th>Website</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($memberships_all as $membership)
                <tr>
                    <td><input name="memberships[{{$membership->me_id}}]"
                               {{in_array($membership->me_id, $memberships) ? "checked" : ""}} type="checkbox">
                    </td>
                    <td>{{$membership->me_id}}</td>
                    <td>{{$membership->me_name}}</td>
                    <td>{{$membership->me_location}}</td>
                    <td>{{$membership->me_website}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <h2 class="content-heading pt-0"></h2>

        <div class="row">
            <div class="form-group col-md-8">
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
        </div>
    </form>
</div>
