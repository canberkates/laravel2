<div class="block-content block-content-full">
    @if (count($obligations) > 0)
        <form method="post"
              action="{{action('ResellersController@update', $customer->cu_id)}}">
            @csrf
            <input name="_method" type="hidden" value="PATCH">
            <input name="form_name" type="hidden" value="Obligations">
            <div class="form-group row">
                @foreach ($obligations as $permit)
                    <label class="col-sm-1 col-form-label mt-3" for="permit[{{$permit->cuob_id}}]">{{$permit->cuob_name}}</label>
                    <div class="col-sm-3 mt-3">
                        <select name="permit[{{$permit->cuob_id}}]" class="form-control">
                            <option @if(!isset($permits_obligations_verified[$permit->cuob_id])) selected @endif value="0">We don't know</option>

                            <option
                                @if(isset($permits_obligations_verified[$permit->cuob_id]) && $permits_obligations_verified[$permit->cuob_id]['verified'] === 0)
                                selected
                                @endif

                                value="1">
                                Verified and they don't have this permit
                            </option>

                            @if(isset($permits_obligations_verified[$permit->cuob_id]) && $permits_obligations_verified[$permit->cuob_id]['verified'] === 1)
                                <option selected value="2">Verified and they have this permit</option>
                            @endif
                        </select>
                    </div>
                    <div class="col-sm-1 mt-3" style="font-size:14px;">{{"Verified on: ".$permits_obligations_verified[$permit->cuob_id]['verified_timestamp']}}</div>
                    <div class="col-sm-1"></div>
                @endforeach
            </div>
            <h2 class="content-heading pt-0"></h2>

            <div class="row">
                <div class="form-group col-md-8">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </div>
        </form>
    @else
        There are no known permits for this country
    @endif

</div>
