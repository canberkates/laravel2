<div class="block-header block-header-default">
    <h3 class="block-title">
        <a href="{{ url('resellers/' . $customer->cu_id . '/customeroffice/create')}}">
            <button class="btn btn-primary">Add an office</button>
        </a>

    </h3>
</div>
<div class="block-content block-content-full">
    <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
        <thead>
        <tr>
            <th>ID</th>
            <th>Street 1</th>
            <th>Street 2</th>
            <th>City</th>
            <th>Zipcode</th>
            <th>State</th>
            <th>Country</th>
            <th>Email</th>
            <th>Telephone</th>
            <th>Website</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($offices as $office)
            <tr>
                <td>{{$office->cuof_id}}</td>
                <td>{{$office->cuof_street_1}}</td>
                <td>{{$office->cuof_street_2}}</td>
                <td>{{$office->cuof_city}}</td>
                <td>{{$office->cuof_zipcode}}</td>
                <td>{{$office->cuof_state}}</td>
                <td>{{$office->cuof_co_code}}</td>
                <td>{{$office->cuof_email}}</td>
                <td>{{$office->cuof_telephone}}</td>
                <td>{{$office->cuof_website}}</td>
                <td class="text-center">
                    <div class="btn-group">
                        <a class="btn btn-sm btn-primary" data-toggle="tooltip"
                           data-placement="left"
                           title="edit"
                           href="{{ url('resellers/' . $customer->cu_id . '/customeroffice/' . $office->cuof_id.'/edit')}}">
                            <i class="fa fa-pencil-alt"></i>
                        </a>
                    </div>
                    <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-primary office_delete" data-toggle="tooltip"
                                title="Delete" data-office_id="{{$office->cuof_id}}">
                            <i class="fa fa-times"></i>
                        </button>

                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
