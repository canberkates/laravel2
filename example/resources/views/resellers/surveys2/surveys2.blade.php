<form class="mb-5 mt-5" method="post" action="{{action('CustomersController@getSurveys', $customer->cu_id)}}">
    @csrf
    <input name="_method" type="hidden" value="post">

    <div class="form-group row">
        <label class="col-md-1 col-form-label ml-5" for="published">Published:</label>
        <div class="col-md-2">
            <select type="text" class="form-control"
                    name="published">
                <option @if(isset($surveys2_filter['published']) && $surveys2_filter['published'] != null && $surveys2_filter['published'] == "both") selected @endif value="both" @if($surveys2_filter['published'] == null) selected @endif>Both</option>
                <option @if(isset($surveys2_filter['published']) && $surveys2_filter['published'] != null && $surveys2_filter['published']== "1") selected @endif  value="1">Yes</option>
                <option @if(isset($surveys2_filter['published']) && $surveys2_filter['published'] != null && $surveys2_filter['published'] == "2") selected @endif  value="2">No</option>
            </select>
        </div>
    </div>

    <div class="form-group row">
        <label class="col-md-1 col-form-label ml-5" for="for-date">Date range:</label>
        <div class="col-md-2">
            <input type="text" class="form-control drp drp-default" name="date" @if($surveys2_filter['date'] != null) value="{{$surveys2_filter['date']}}" @endif autocomplete="off"/>
        </div>
    </div>

    <div class="row">
        <div class="col-md-1"></div>
        <div class="form-group col-md-8">
            <button type="submit" class="btn btn-primary">Filter</button>
        </div>
    </div>

</form>
@if (!empty($surveys2))

    <ul class="nav nav-tabs nav-tabs-block js-tabs-enabled" data-toggle="tabs"
        role="tablist">
        <li class="nav-item">
            <a class="nav-link active show" href="#btabs-static-reaction">Reaction speed ({{sizeof($surveys1)}})</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#btabs-static-reviews">Reviews ({{sizeof($surveys2)}})</a>
        </li>
    </ul>

    <div class="block-content tab-content">
        <div class="tab-pane active show" id="btabs-static-reaction" role="tabpanel">
            <div class="block block-rounded block-bordered">
                <div class="block-content block-content-full">
                    <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Sent</th>
                            <th>Submitted</th>
                            <th>Request ID</th>
                            <th>Name</th>
                            <th>Contacted</th>
                            <th>Quote received</th>
                            <th>Remark</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($surveys1 as $survey1)
                            <tr>
                                <td>{{$survey1->su_id}}</td>
                                <td>{{$survey1->su_timestamp}}</td>
                                <td>{{$survey1->su_submitted_timestamp}}</td>
                                <td>{{$survey1->su_re_id}}</td>
                                <td>{{$survey1->re_full_name}}</td>
                                <td>{{$contacted[$survey1->sucu_contacted]}}</td>
                                <td>{{$emptyyesno[$survey1->sucu_quote_received]}}</td>
                                <td>{{$survey1->sucu_remark}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="btabs-static-reviews" role="tabpanel">
            <div class="block-content block-content-full">
                <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Source</th>
                        <th>Request / Website ID</th>
                        <th>Sent</th>
                        <th>Submitted</th>
                        <th>Author</th>
                        <th>Rating</th>
                        <th>View</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($surveys2 as $survey2)
                        <tr>
                            <td>{{$survey2->su_id}}</td>
                            <td>{{$survey2sources[$survey2->su_source]}}</td>
                            <td>{{($survey2->su_source == 1 ? $survey2->su_re_id : $survey2->su_were_id)}}</td>
                            <td>{{$survey2->su_timestamp}}</td>
                            <td>{{$survey2->su_submitted_timestamp}}</td>
                            <td>{{($survey2->su_source == 1 ? $survey2->request->re_full_name : $survey2->website_review->were_name)}}</td>
                            <td>{{($survey2->su_rating > 0 ? $survey2->su_rating : "-" )}}</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <a class="btn btn-sm btn-primary"
                                       data-toggle="tooltip"
                                       data-placement="left"
                                       title="edit"
                                       href="{{ url('customers/' . $customer->cu_id . '/survey2/' . $survey2->su_id . '/edit')}}">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endif
