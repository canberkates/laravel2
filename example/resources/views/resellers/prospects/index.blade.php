@extends('layouts.backend')

@include( 'scripts.datatables' )
@include( 'scripts.dialogs' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Customers (Prospects & Deleted)</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Customers</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->
    <!-- Page Content -->
    <div class="content">


        <!-- Your Block -->
        <div class="block block-rounded block-bordered">

            <ul class="nav nav-tabs nav-tabs-alt" data-toggle="tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" href="#btabs-alt-static-prospect">Prospect ({{$cached_count[1][5]}})</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#btabs-alt-static-deleted">Deleted ({{$cached_count[1][8]}})</a>
                </li>
            </ul>

            <div class="block-content tab-content">
                <div class="tab-pane active" id="btabs-alt-static-prospect" role="tabpanel">
                    <div class="block-content block-content-full">
                        @include('cache.customers_5')
                    </div>
                </div>

                <div class="tab-pane" id="btabs-alt-static-deleted" role="tabpanel">
                    <div class="block-content block-content-full">
                        @include('cache.customers_8')
                    </div>
                </div>
            </div>
        </div>
        <!-- END Your Block -->
    </div>
    <!-- END Page Content -->
@endsection

@push( 'scripts' )
    <script>
        jQuery(document).ready(function(){
        	jQuery( document ).on( 'click', '.customer_delete', function(e) {

                e.preventDefault();

        		var $self = jQuery(this);

                confirmDelete("{{ url('ajax/customer/delete') }}", 'get', {id:$self.data('id')}, function() {

                    $self.parents( '.dataTable' ).DataTable().row( $self.parents( 'tr' ) ).remove().draw( 'page' );
                });

            });
        });
    </script>

@endpush
