<div class="block-content">
    <form method="post"
          action="{{action('ResellersController@update', $customer->cu_id)}}">
        @csrf
        <input name="_method" type="hidden" value="PATCH">
        <input name="form_name" type="hidden" value="Settings">

        <h2 class="content-heading pt-0">Special agreements</h2>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label" for="moda_special_agreements_checkbox">Special agreements:</label>
            <div class="col-sm-7">
                <div
                    class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                    <input type="checkbox" class="custom-control-input"
                           id="moda_special_agreements_checkbox"
                           name="moda_special_agreements_checkbox"{{($moverdata->moda_special_agreements_checkbox ? "checked=''" : "")}}>
                    <label class="custom-control-label"
                           for="moda_special_agreements_checkbox"></label>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label" for="special_agreements"></label>
            <div class="col-sm-7">
                    <textarea name="special_agreements"{{($moverdata->moda_special_agreements_checkbox == 0 ? "disabled" : "")}}
                              class="form-control">{{$moverdata->moda_special_agreements}}</textarea>
            </div>
        </div>

        <h2 class="content-heading pt-0">Claim percentage</h2>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label" for="claim_percentage">Claim
                percentage:</label>
            <div class="col-sm-3">
                <select type="text" name="claim_percentage" class="form-control"
                        name="type">
                    @foreach($claim_dropdown as $id => $value)
                        <option
                            value="{{$id}}" {{$moverdata->moda_max_claim_percentage == $id ? "selected" : ""}}>{{$value}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label" for="moda_no_claim_discount">No
                claim discount</label>
            <div class="col-sm-7">
                <div
                    class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                    <input type="checkbox" class="custom-control-input"
                           id="moda_no_claim_discount"
                           name="moda_no_claim_discount"{{($moverdata->moda_no_claim_discount ? "checked=''" : "")}}>
                    <label class="custom-control-label"
                           for="moda_no_claim_discount">Turn this on if you want the customer to receive a no claim discount. This will automatically set the claim percentage to 0%.</label>
                </div>
            </div>
        </div>

        <div class="form-group row" data-conditional="moda_no_claim_discount">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label" for="no_claim_discount">No
                claim discount percentage:</label>
            <div class="col-sm-3">
                <select type="text" class="form-control"
                        name="no_claim_discount">
                    @foreach($no_claim_dropdown as $id => $value)
                        <option
                            value="{{$id}}" {{$moverdata->moda_no_claim_discount_percentage == $id ? "selected" : ""}}>{{$value}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label">Claim
                rate (30 days):</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" disabled
                       value="{{number_format($claim_rates[30]['percentage'], 2, '.', '')}}%  ({{$claim_rates[30]['total_claimed_requests']}} claimed out of {{$claim_rates[30]['requests']}} requests) @if (!empty($claim_rates_30_special_agreements)) {{"/ ".$claim_rates_30_special_agreements}} @endif">
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label">Claim
                rate (60 days):</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" disabled
                       value="{{number_format($claim_rates[60]['percentage'], 2, '.', '')}}%  ({{$claim_rates[60]['total_claimed_requests']}} claimed out of {{$claim_rates[60]['requests']}} requests) @if (!empty($claim_rates_60_special_agreements)) {{"/ ".$claim_rates_60_special_agreements}} @endif">
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label">Claim
                rate (90 days):</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" disabled
                       value="{{number_format($claim_rates[90]['percentage'], 2, '.', '')}}%  ({{$claim_rates[90]['total_claimed_requests']}} claimed out of {{$claim_rates[90]['requests']}} requests) @if (!empty($claim_rates_90_special_agreements)) {{"/ ".$claim_rates_90_special_agreements}} @endif">
            </div>
        </div>

        <h2 class="content-heading pt-0">Quality Score</h2>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label">Quality
                score</label>
            <div class="col-sm-3">
                <input type="text" class="form-control" disabled
                       value="{{$moverdata->moda_quality_score}}">
            </div>
        </div>


        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label" for="quality_score_override">Quality
                score override:</label>
            <div class="col-sm-3">
                <select type="text" class="form-control"
                        name="quality_score_override">
                    @foreach($qualityscoreoverrides as $id => $item)
                        <option
                            value="{{$id}}" {{ ($moverdata->moda_quality_score_override == $id) ? 'selected':'' }}>{{$item}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <h2 class="content-heading pt-0">Capping Method</h2>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label" for="capping_method">Capping
                Method:</label>
            <div class="col-sm-3">
                <select type="text" class="form-control" name="capping_method">
                    @foreach($cappingmethod as $id => $item)
                        <option
                            value="{{$id}}" {{ ($moverdata->moda_capping_method == $id) ? 'selected':'' }}>{{$item}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label" for="method_explanation">Method explanation:</label>
            <div class="col-sm-8">
                <p id="mover_capping_method_explanation" style="display: inline-block; line-height: 22px;"></p>
            </div>
        </div>

        <div name="monthly_leads">

            <h2 class="content-heading pt-0">Monthly leads</h2>

            <div class="form-group row">
                <div class="col-sm-1"></div>
                <label class="col-sm-3 col-form-label"
                       for="apply_leads_capping_this_month">Apply to this
                    month</label>
                <div class="col-sm-7">
                    <div
                        class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                        <input type="checkbox" class="custom-control-input"
                               id="apply_leads_capping_this_month"
                               name="apply_leads_capping_this_month">
                        <label class="custom-control-label"
                               for="apply_leads_capping_this_month">Otherwise
                            the
                            changes
                            won't be applied till next month.</label>
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-1"></div>
                <label class="col-sm-3 col-form-label"
                       for="leads_monthly_limit">Monthly
                    limit</label>
                <div class="col-sm-3">
                    <input type="text" class="form-control"
                           name="leads_monthly_limit"
                           value="{{$moverdata->moda_capping_monthly_limit}}">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-1"></div>
                <label class="col-sm-3 col-form-label"
                       for="leads_monthly_limit_left">Monthly
                    limit (left)</label>
                <div class="col-sm-3">
                    <input type="text" class="form-control"
                           name="leads_monthly_limit_left"
                           value="{{$moverdata->moda_capping_monthly_limit_left}}">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-1"></div>
                <label class="col-sm-3 col-form-label"
                       for="leads_monthly_limit_modifier">Monthly
                    limit modifier</label>
                <div class="col-sm-3">
                    <input type="text" class="form-control"
                           name="leads_monthly_limit_modifier"
                           value="{{$moverdata->moda_capping_monthly_limit_modifier}}">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10">
                    <p>The modifier adds a certain amount to the monthly limit
                        for
                        the sole purpose of raising the daily average. This is
                        helpful to take claims into account.
                        If a customer has a monthly limit of €10K and they claim
                        10%, you can add a modifier of €1K.
                        The monthly limit will still be €10K while the daily
                        average
                        is calculated based on the monthly limit combined with
                        the
                        modifier (so €11K in this case).</p>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-1"></div>
                <label class="col-sm-3 col-form-label">Daily
                    average</label>
                <div class="col-sm-3">
                    <input type="text" disabled class="form-control"
                           value="{{number_format($moverdata->moda_capping_daily_average, 2, '.', '')}}">
                </div>
            </div>
        </div>

        <div name="monthly_spend">

            <h2 class="content-heading pt-0">Monthly spend</h2>

            <div class="form-group row">
                <div class="col-sm-1"></div>
                <label class="col-sm-3 col-form-label"
                       for="apply_spend_capping_this_month">Apply to this
                    month</label>
                <div class="col-sm-7">
                    <div
                        class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                        <input type="checkbox" class="custom-control-input"
                               id="apply_spend_capping_this_month"
                               name="apply_spend_capping_this_month">
                        <label class="custom-control-label"
                               for="apply_spend_capping_this_month">Otherwise
                            the
                            changes
                            won't be applied till next month.</label>
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-1"></div>
                <label class="col-sm-3 col-form-label"
                       for="spend_monthly_limit">Monthly
                    limit</label>
                <div class="col-sm-3">
                    <input type="text" class="form-control"
                           name="spend_monthly_limit"
                           value="{{$moverdata->moda_capping_monthly_limit}}">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-1"></div>
                <label class="col-sm-3 col-form-label"
                       for="spend_monthly_limit_left">Monthly
                    limit (left)</label>
                <div class="col-sm-3">
                    <input type="text" class="form-control"
                           name="spend_monthly_limit_left"
                           value="{{$moverdata->moda_capping_monthly_limit_left}}">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-1"></div>
                <label class="col-sm-3 col-form-label"
                       for="spend_monthly_limit_modifier">Monthly
                    limit modifier</label>
                <div class="col-sm-3">
                    <input type="text" class="form-control"
                           name="spend_monthly_limit_modifier"
                           value="{{$moverdata->moda_capping_monthly_limit_modifier}}">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10">
                    <p>The modifier adds a certain amount to the monthly limit
                        for
                        the sole purpose of raising the daily average. This is
                        helpful to take claims into account.
                        If a customer has a monthly limit of €10K and they claim
                        10%, you can add a modifier of €1K.
                        The monthly limit will still be €10K while the daily
                        average
                        is calculated based on the monthly limit combined with
                        the
                        modifier (so €11K in this case).</p>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-1"></div>
                <label class="col-sm-3 col-form-label">Daily
                    average</label>
                <div class="col-sm-3">
                    <input type="text" disabled class="form-control"
                           value="{{number_format($moverdata->moda_capping_daily_average, 2, '.', '')}}">
                </div>
            </div>

        </div>


        <h2 class="content-heading pt-0"></h2>


        <div class="row">
            <div class="col-md-1"></div>
            <div class="form-group col-md-8">
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
        </div>

    </form>
</div>
