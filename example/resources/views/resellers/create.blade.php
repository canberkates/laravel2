@extends('layouts.backend')

@include('scripts.select2')
@include( 'scripts.telinput' )

@section('content')
    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="block block-rounded block-bordered">

                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    {{$error}}<br>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="block-content">
                        <form method="post" action="{{action('CustomersController@store')}}">
                            @csrf
                            <input name="_method" type="hidden" value="post">
                            <h2 class="content-heading pt-0">General Information</h2>

                            <div class="form-group row">
                                <div class="col-md-1"></div>
                                <label class="col-md-3 col-form-label" for="for-type">Type of partner:</label>
                                <div class="col-md-7">
                                    <select id="for-type" class="js-select2 form-control {{$errors->has('type') ? 'is-invalid' : ''}}" name="type" data-placeholder="Choose one.." style="width:100%;" data-minimum-results-for-search="Infinity">
                                        <option></option>
                                        @foreach($movertypes as $id => $item)
                                            <option @if (Request::old('type') == $id) selected @endif value="{{$id}}">{{$item}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="for-la_code">Language:</label>
                                <div class="col-sm-7">
                                    <select id="for-la_code" class="js-select2 form-control {{$errors->has('la_code') ? 'is-invalid' : ''}}" name="la_code" data-placeholder="Choose one.." style="width:100%;" data-minimum-results-for-search="Infinity">
                                        <option></option>
                                        @foreach($languages as $language)
                                            <option @if (Request::old('la_code') == $language->la_code) selected @endif value="{{$language->la_code}}">{{$language->la_language}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="for-description">Description:</label>
                                <div class="col-sm-7">
                                    <textarea id="for-description" rows="6" class="form-control" name="description">{{Request::old('description')}}</textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="for-company_name_legal">Company legal name:</label>
                                <div class="col-sm-7">
                                    <input id="for-company_name_legal" type="text" class="form-control {{$errors->has('company_name_legal') ? 'is-invalid' : ''}}" name="company_name_legal" value="{{Request::old('company_name_legal')}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="for-company_name_business">Company business name:</label>
                                <div class="col-sm-7">
                                    <input id="for-company_name_business" type="text" class="form-control {{$errors->has('company_name_business') ? 'is-invalid' : ''}}" name="company_name_business" value="{{Request::old('company_name_business')}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="for-attn">Primary contact:</label>
                                <div class="col-sm-7">
                                    <input id="for-attn" type="text" class="form-control {{$errors->has('attn') ? 'is-invalid' : ''}}" name="attn" value="{{Request::old('attn')}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="for-attn_email">Primary contact email:</label>
                                <div class="col-sm-7">
                                    <input id="for-attn_email" type="text" class="form-control {{$errors->has('attn_email') ? 'is-invalid' : ''}}" name="attn_email" value="{{Request::old('attn_email')}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="coc">Chamber of commerce:</label>
                                <div class="col-sm-7">
                                    <input id="coc" type="text" class="form-control {{$errors->has('coc') ? 'is-invalid' : ''}}" name="coc" value="{{Request::old('coc')}}">
                                </div>
                            </div>

                            <h2 class="content-heading pt-0">Emails</h2>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="leads_email">Lead email:</label>
                                <div class="col-sm-7">
                                    <input id="leads_email" type="text" class="form-control {{$errors->has('leads_email') ? 'is-invalid' : ''}}" name="leads_email" value="{{Request::old('leads_email')}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="review_email">Review communication:</label>
                                <div class="col-sm-7">
                                    <input id="review_email" type="text" class="form-control" name="review_email" value="{{Request::old('review_email')}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="load_exchange_email">Load Exchange email:</label>
                                <div class="col-sm-7">
                                    <input id="load_exchange_email" type="text" class="form-control {{$errors->has('load_exchange_email') ? 'is-invalid' : ''}}" name="load_exchange_email" value="{{Request::old('load_exchange_email')}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="general_info">General info:</label>
                                <div class="col-sm-7">
                                    <input id="general_info" type="text" class="form-control {{$errors->has('general_info') ? 'is-invalid' : ''}}" name="general_info" value="{{Request::old('general_info')}}">
                                </div>
                            </div>

                            <h2 class="content-heading pt-0">Office Address</h2>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="for-street_1">Company street 1:</label>
                                <div class="col-sm-7">
                                    <input id="for-street_1" type="text" class="form-control {{$errors->has('street_1') ? 'is-invalid' : ''}}" name="street_1" value="{{Request::old('street_1')}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="for-street_2">Company street 2:</label>
                                <div class="col-sm-7">
                                    <input id="for-street_2" type="text" class="form-control" name="street_2" value="{{Request::old('street_2')}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="for-zipcode">Company zipcode:</label>
                                <div class="col-sm-7">
                                    <input id="for-zipcode" type="text" class="form-control {{$errors->has('zipcode') ? 'is-invalid' : ''}}" name="zipcode" value="{{Request::old('zipcode')}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="for-city">Company city:</label>
                                <div class="col-sm-7">
                                    <input id="for-city" type="text" class="form-control {{$errors->has('city') ? 'is-invalid' : ''}}" name="city" value="{{Request::old('city')}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="for-country">Company country:</label>
                                <div class="col-sm-7">
                                    <select id="for-country" class="js-select2 form-control {{$errors->has('country') ? 'is-invalid' : ''}}" name="country" data-placeholder="Choose one..">
                                        <option value=""></option>
                                        @foreach($countries as $country)
                                            <option @if (Request::old('country') == $country->co_code) selected @endif value="{{$country->co_code}}">{{$country->co_en}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="for-telephone">Company telephone:</label>
                                <div class="col-sm-7">
                                    <input type="hidden" id="int_telephone" name="int_telephone" value="">
                                    <input id="for-telephone" type="tel" class="form-control {{$errors->has('telephone') ? 'is-invalid' : ''}}" name="telephone" value="{{Request::old('telephone')}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="for-website">Company website:</label>
                                <div class="col-sm-7">
                                    <input id="for-website" type="text" class="form-control {{$errors->has('website') ? 'is-invalid' : ''}}" name="website" value="{{Request::old('website')}}">
                                </div>
                            </div>

                            <h2 class="content-heading pt-0">Billing Address</h2>
                                Use Billing Address
                                <div style="padding-top: 5px;padding-left: 10px;"
                                     class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                    <input type="checkbox"
                                           class="custom-control-input"
                                           id="use_billing_address"
                                           name="use_billing_address">
                                    <label class="custom-control-label"
                                           for="use_billing_address"></label>
                                </div>

                            <div id="billing_address" style="display:none;">
                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="for-street_1_bi">Company street 1:</label>
                                    <div class="col-sm-7">
                                        <input id="for-street_1_bi" type="text" class="form-control" name="street_1_bi" value="{{Request::old('street_1_bi')}}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="for-street_2_bi">Company street 2:</label>
                                    <div class="col-sm-7">
                                        <input id="for-street_2_bi" type="text" class="form-control" name="street_2_bi" value="{{Request::old('street_2_bi')}}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="for-zipcode_bi">Company zipcode:</label>
                                    <div class="col-sm-7">
                                        <input id="for-zipcode_bi" type="text" class="form-control" name="zipcode_bi" value="{{Request::old('zipcode_bi')}}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="for-city_bi">Company city:</label>
                                    <div class="col-sm-7">
                                        <input id="for-city_bi" type="text" class="form-control" name="city_bi" value="{{Request::old('city_bi')}}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="for-country_bi">Company country:</label>
                                    <div class="col-sm-7">
                                        <select id="for-country_bi" class="js-select2 form-control" name="country_bi" data-placeholder="Choose one..">
                                            <option value=""></option>
                                            @foreach($countries as $country)
                                                <option @if (Request::old('country_bi') == $country->co_code) selected @endif value="{{$country->co_code}}">{{$country->co_en}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="for-telephone_bi">Company telephone:</label>
                                    <div class="col-sm-7">
                                        <input type="hidden" id="int_telephone_bi" name="int_telephone_bi" value="">
                                        <input id="for-telephone_bi" type="text" class="form-control" name="telephone_bi" value="{{Request::old('telephone_bi')}}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="for-website_bi">Company website:</label>
                                    <div class="col-sm-7">
                                        <input id="for-website_bi" type="text" class="form-control" name="website_bi" value="{{Request::old('website_bi')}}">
                                    </div>
                                </div>
                            </div>

                            <h2 class="content-heading pt-0"></h2>

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Add Customer</button>
                                    <a class="btn btn-primary" href="/customers">
                                        Back
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@push('scripts')
    <script>
        $(document).ready(function () {

            firstInput();
            secondInput();

            function firstInput(){
                var input = document.querySelector("input[name=telephone]");
                output = $("#int_telephone");

                var iti = window.intlTelInput(input, {
                    nationalMode: true,
                    separateDialCode: true
                });

                var handleChange = function() {
                    var text = (iti.isValidNumber()) ? iti.getNumber() : 0;
                    if(text == 0){
                        $('input[name=telephone]').addClass("is-invalid");
                    }else{
                        $('input[name=telephone]').removeClass("is-invalid");
                    }
                    output.val(text);
                };

                input.addEventListener('change', handleChange);
                input.addEventListener('keyup', handleChange);

                handleChange();
            }

            function secondInput(){
                var input2 = document.querySelector("input[name=telephone_bi]");
                output2 = $("#int_telephone_bi");

                var iti2 = window.intlTelInput(input2, {
                    nationalMode: true,
                    separateDialCode: true
                });

                var handleChange = function() {
                    var text = (iti2.isValidNumber()) ? iti2.getNumber() : 0;
                    if(text == 0){
                        $('input[name=telephone_bi]').addClass("is-invalid");
                    }else{
                        $('input[name=telephone_bi]').removeClass("is-invalid");
                    }
                    output2.val(text);
                };

                input2.addEventListener('change', handleChange);
                input2.addEventListener('keyup', handleChange);

                handleChange();

            }

            $("input[name=use_billing_address]").change(function(){
                var billing_address_div = $("#billing_address");
                if ($("input[name=use_billing_address]").is(":checked"))
                {
                    billing_address_div.slideDown();
                }
                else
                {
                	billing_address_div.slideUp();
                }
            }).trigger();
        });
    </script>
@endpush