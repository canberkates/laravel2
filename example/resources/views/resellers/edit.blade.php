@extends('layouts.backend')

@include('scripts.datatables')
@include('scripts.datepicker')
@include('scripts.forms')
@include('scripts.dialogs')
@include( 'scripts.charts' )
@include( 'scripts.telinput' )

@pushonce( 'scripts:customer-edit' )
<script src="{{ asset('/js/custom/customer/customer_edit.js') }}"></script>
@endpushonce

@section('content')

    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">LR
                    - {{$customer->cu_company_name_business}} @if($customer->cu_credit_hold)- Credit hold @endif
                    - {{$customerstatus}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('resellers') }}">Resellers</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Reseller edit</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <h3></h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <!-- Block Tabs Alternative Style -->
                <div class="block block-rounded block-bordered">
                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    {{$error}}<br>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <ul class="nav nav-tabs nav-tabs-alt" data-toggle="tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" href="#btabs-alt-static-general">General</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-emails">Emails</a>
                        </li>
                        @can('customers - finance')
                            <li class="nav-item">
                                <a class="nav-link" href="#btabs-alt-static-finance">Finance</a>
                            </li>
                        @endcan
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-contactpersons">Contact persons</a>
                        </li>
                        @can('conference')
                            @can('admin')
                                <li class="nav-item">
                                    <a class="nav-link" href="#btabs-alt-static-status">Status</a>
                                </li>
                            @endcan
                        @else
                            <li class="nav-item">
                                <a class="nav-link" href="#btabs-alt-static-status">Status</a>
                            </li>
                        @endif
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-remark">Remarks</a>
                        </li>
                        @can('customers - documents')
                            <li class="nav-item">
                                <a class="nav-link" href="#btabs-alt-static-documents">Documents</a>
                            </li>
                        @endcan
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-settings">Settings</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-portals">Portals</a>
                        </li>
                        @canany(['requests', 'conference'])
                            <li class="nav-item">
                                <a class="nav-link" href="#btabs-alt-static-requests">Requests</a>
                            </li>
                        @endcan
                        @can('customers - invoices')
                            <li class="nav-item">
                                <a class="nav-link" href="#btabs-alt-static-invoices">Invoices</a>
                            </li>
                        @endcan
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-history">History</a>
                        </li>
                    </ul>
                    <div class="block-content tab-content">
                        <div class="tab-pane active" id="btabs-alt-static-general" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        @include( 'resellers.general.general-information' )
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        @include( 'resellers.general.addresses' )
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="block block-rounded block-bordered">
                                        @include( 'resellers.general.finance' )
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="btabs-alt-static-emails" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        @include( 'resellers.emails.emails' )
                                    </div>
                                </div>
                            </div>
                        </div>
                        @can('customers - finance')
                            <div class="tab-pane" id="btabs-alt-static-finance" role="tabpanel">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="block block-rounded block-bordered">
                                            @include( 'resellers.finance.finance' )
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="block block-rounded block-bordered">
                                            @include( 'resellers.finance.credit-cards' )
                                        </div>
                                    </div>
                                    @if($customer->cu_payment_method == 3)
                                        <div class="col-md-6">
                                            <div class="block block-rounded block-bordered">
                                                @include( 'resellers.finance.auto_debit' )
                                            </div>
                                        </div>
                                 @endif
                                </div>
                            </div>
                        @endcan

                        <div class="tab-pane" id="btabs-alt-static-contactpersons" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        @include( 'resellers.contactpersons.contactpersons' )
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="btabs-alt-static-status" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        @include( 'resellers.status.status' )
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="btabs-alt-static-remark" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        @include( 'resellers.remarks.remarks' )
                                    </div>
                                </div>
                            </div>
                        </div>
                        @can('customers - documents')
                            <div class="tab-pane" id="btabs-alt-static-documents" role="tabpanel">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="block block-rounded block-bordered">
                                            @include( 'resellers.documents.documents' )
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endcan
                        <div class="tab-pane" id="btabs-alt-static-settings" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12 col-xl-12">
                                    <div class="block block-rounded block-bordered">
                                        @include( 'resellers.settings.settings' )
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="btabs-alt-static-portals" role="tabpanel">
                            @include( 'resellers.portals.portals' )
                        </div>
                        @canany(['requests', 'conference'])
                            <div class="tab-pane" id="btabs-alt-static-requests" role="tabpanel">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="block block-rounded block-bordered">
                                            @include( 'resellers.requests.requests' )
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endcan
                        @can('customers - invoices')
                            <div class="tab-pane" id="btabs-alt-static-invoices" role="tabpanel">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="block block-rounded block-bordered">
                                            @include( 'resellers.invoices.invoices' )
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endcan
                        <div class="tab-pane" id="btabs-alt-static-history" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        @include( 'resellers.history.history' )
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- END Block Tabs Alternative Style -->
            </div>
        </div>
    </div>

    <div id="viewInvoiceModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content" style="width:700px;">
                <div class="modal-header">
                    <h5 class="modal-title">Viewing Invoice</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="invoice_view_iframe">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push( 'scripts' )

    <script>
        jQuery(document).ready(function () {
            jQuery(document).on('click', '#moda_special_agreements_checkbox', function (e) {
                var textarea = jQuery("textarea[name=special_agreements]");

                if ($(this).is(':checked')) {
                    textarea.prop("disabled", false);
                } else {
                    textarea.prop("disabled", true);
                }
            });

            jQuery(document).on('click', '.contactperson_delete', function (e) {
                e.preventDefault();

                var $self = jQuery(this);

                confirmDelete("{{ url('ajax/customer/contactperson_delete') }}", 'get', {id: $self.data('contactperson_id')}, function () {

                    $self.parents('.dataTable').DataTable().row($self.parents('tr')).remove().draw('page');
                });
            });

            jQuery(document).on('click', '#moda_activate_premium_leads', function (e) {
                var premium_leads_section = $("#premium_leads_section");

                if ($(this).is(":checked")) {
                    premium_leads_section.slideDown();
                } else {
                    premium_leads_section.slideUp();
                }
            });

            jQuery(document).on('click', '.status_delete', function (e) {
                e.preventDefault();

                var $self = jQuery(this);

                @can("admin")
                confirmDelete("{{ url('ajax/customer/status_delete') }}", 'get', {id: $self.data('status_id')}, function () {

                    $self.parents('.dataTable').DataTable().row($self.parents('tr')).remove().draw('page');
                });
                @endcan
            });

            jQuery(document).on('click', '.remark_delete', function (e) {
                e.preventDefault();

                var $self = jQuery(this);
                confirmDelete("{{ url('ajax/customer/remark_delete') }}", 'get', {id: $self.data('remark_id')}, function () {

                    $self.parents('.dataTable').DataTable().row($self.parents('tr')).remove().draw('page');
                });
            });

            jQuery(document).on('click', '.office_delete', function (e) {
                e.preventDefault();

                var $self = jQuery(this);

                confirmDelete("{{ url('ajax/customer/office_delete') }}", 'get', {id: $self.data('office_id')}, function () {

                    $self.parents('.dataTable').DataTable().row($self.parents('tr')).remove().draw('page');
                });
            });

            jQuery(document).on('click', '.invoice_view', function (e) {
                e.preventDefault();

                var $self = jQuery(this);

                jQuery.ajax({
                    url: "/ajax/invoice/view",
                    method: 'get',
                    data: {invoice_id: $self.data('invoice_id')},
                    success: function ($result) {
                        $("#invoice_view_iframe").html('');
                        $("#invoice_view_iframe").append($result);

                        $("#viewInvoiceModal").modal("toggle");
                    }
                });
            });
        });
    </script>

@endpush
