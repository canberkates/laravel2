@extends('layouts.backend')

@include( 'scripts.datatables' )
@include( 'scripts.dialogs' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Resellers</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url( 'dashboard' ) }}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Resellers</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->
    <!-- Page Content -->
    <div class="content">
        <!-- Your Block -->

        <div class="block block-rounded block-bordered">
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    <a href="{{ url('customers/create')}}"><button data-toggle="click-ripple" class="btn btn-primary">Add a Customer</button></a>
                    <a href="{{ url('customerchanges/')}}"><button data-toggle="click-ripple" class="btn btn-primary">Customer Changes ({{$customer_changes_count}})</button></a>
                    <a href="{{ url('customerportals/')}}"><button data-toggle="click-ripple" class="btn btn-primary">Portals</button></a>
                    <a href="{{ url('customers/prospects/')}}"><button data-toggle="click-ripple" class="btn btn-primary prospects-confirm">Prospects & Deleted</button></a>
                    @if($cached_count['incorrect'] > 0)
                        <a href="{{ url('incorrect_customers')}}"><button data-toggle="click-ripple" class="btn btn-danger">Incorrect Customers <span class="badge badge-primary">({{$cached_count['incorrect']}})</span></button></a>
                    @endif
                    <a href="{{ url('customercache')}}"><button class="btn btn-primary js-popover cache-confirm" data-toggle="popover" data-placement="top" data-original-title="Last updated" data-content="{{$last_timestamp_updated}}">Purge Cache</button></a>
                </h3>
            </div>

            <ul class="nav nav-tabs nav-tabs-alt" data-toggle="tabs" role="tablist">
                @if(View::exists('cache.resellers_1'))
                    <li class="nav-item">
                        <a class="nav-link active" href="#btabs-alt-static-active">Active ({{$cached_count[6][1]}})</a>
                    </li>
                @endif

                @if(View::exists('cache.resellers_2'))
                    <li class="nav-item">
                        <a class="nav-link" href="#btabs-alt-static-freetrial">Free Trial ({{$cached_count[6][2]}})</a>
                    </li>
                @endif

                @if(View::exists('cache.resellers_3'))
                    <li class="nav-item">
                        <a class="nav-link" href="#btabs-alt-static-pause">Pause ({{$cached_count[6][3]}})</a>
                    </li>
                @endif

                @if(View::exists('cache.resellers_4'))
                    <li class="nav-item">
                        <a class="nav-link" href="#btabs-alt-static-cancelled">Cancelled ({{$cached_count[6][4]}})</a>
                    </li>
                @endif

                @if(View::exists('cache.resellers_6'))
                    <li class="nav-item">
                        <a class="nav-link" href="#btabs-alt-static-credit_hold">Credit hold ({{$cached_count[6][6]}})</a>
                    </li>
                @endif

                @if(View::exists('cache.resellers_9'))
                    <li class="nav-item">
                        <a class="nav-link" href="#btabs-alt-static-prepayment_credit_hold">Prepayment Credit hold @if(isset($cached_count[6][9])) {{"(".$cached_count[6][9].")"}} @else {{"(0)"}} @endif</a>
                    </li>
                @endif

                @if(View::exists('cache.resellers_7'))
                    <li class="nav-item">
                        <a class="nav-link" href="#btabs-alt-static-debt_collector">Debt collector({{$cached_count[6][7]}})</a>
                    </li>
                @endif
            </ul>

            <div class="block-content tab-content">
                @if(View::exists('cache.resellers_1'))
                    <div class="tab-pane active" id="btabs-alt-static-active" role="tabpanel">
                        <div class="block-content block-content-full">
                            @include('cache.resellers_1')
                        </div>
                    </div>
                @endif

                @if(View::exists('cache.resellers_2'))
                    <div class="tab-pane" id="btabs-alt-static-freetrial" role="tabpanel">
                        <div class="block-content block-content-full">
                            @include('cache.resellers_2')
                        </div>
                    </div>
                @endif

                @if(View::exists('cache.resellers_3'))
                    <div class="tab-pane" id="btabs-alt-static-pause" role="tabpanel">
                        <div class="block-content block-content-full">
                            @include('cache.resellers_3')
                        </div>
                    </div>
                @endif

                @if(View::exists('cache.resellers_4'))
                    <div class="tab-pane" id="btabs-alt-static-cancelled" role="tabpanel">
                        <div class="block-content block-content-full">
                            @include('cache.resellers_4')
                        </div>
                    </div>
                @endif

                @if(View::exists('cache.resellers_6'))
                    <div class="tab-pane" id="btabs-alt-static-credit_hold" role="tabpanel">
                        <div class="block-content block-content-full">
                            @include('cache.resellers_6')
                        </div>
                    </div>
                @endif

                @if(View::exists('cache.resellers_9'))
                    <div class="tab-pane" id="btabs-alt-static-prepayment_credit_hold" role="tabpanel">
                        <div class="block-content block-content-full">
                            @include('cache.resellers_9')
                        </div>
                    </div>
                @endif

                @if(View::exists('cache.resellers_7'))
                    <div class="tab-pane" id="btabs-alt-static-debt_collector" role="tabpanel">
                        <div class="block-content block-content-full">
                            @include('cache.resellers_7')
                        </div>
                    </div>
                @endif

            </div>
        </div>
        <!-- END Your Block -->
    </div>
    <!-- END Page Content -->
@endsection

@push( 'scripts' )
    <script>
        jQuery(document).ready(function(){
            jQuery( document ).on( 'click', '.customer_delete', function(e) {

                e.preventDefault();

                var $self = jQuery(this);

                confirmDelete("{{ url('ajax/customer/delete') }}", 'get', {id:$self.data('id')}, function() {

                    $self.parents( '.dataTable' ).DataTable().row( $self.parents( 'tr' ) ).remove().draw( 'page' );
                });

            });

            $( '.prospects-confirm' ).click( function(e) {

                // Stop initial link
                e.preventDefault();

                // Set options
                var $options = {
                    text : 'Loading the prospects might take a while..',
                    type: 'warning'
                };

                // Confirm dialog
                confirmClick( "{{ url('customers/prospects/')}}", $options );
            });

            $( '.cache-confirm' ).click( function(e) {

                // Stop initial link
                e.preventDefault();

                // Set options
                var $options = {
                    title: 'Are you sure you want to clear the customer cache?',
                    type: 'warning',
                };

                // Confirm dialog
                confirmClick( "{{ url('customercache') }}", $options );
            });
        });
    </script>

@endpush
