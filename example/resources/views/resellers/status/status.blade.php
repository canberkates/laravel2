<div class="block-header block-header-default">
    <h3 class="block-title">
        <a href="{{ url('resellers/' . $customer->cu_id . '/customerstatus/create')}}">
            <button class="btn btn-primary">Add a status</button>
        </a>
    </h3>
</div>
<div class="block-content block-content-full">
    <table data-order='[[1, "desc"]]' class="table table-bordered table-striped table-vcenter js-dataTable-full">
        <thead>
        <tr>
            <th>DATA</th>
            <th>ID</th>
            <th>Date</th>
            <th>Employee</th>
            <th>Status</th>
            <th>Reason</th>
            <th>Turnover Netto</th>
            <th>Remark</th>
            <th>Delete</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($statuses as $status)
            <tr data-status-id="{{$status->cust_id}}">
                <td class="text-center">
                    <button type="button" class="btn btn-sm btn-primary details_show_blank" data-toggle="tooltip"
                            title="Expand"
                            data-details="{{$customer_status_expand[$status->cust_id]}}">
                        <i class="fa fa-plus"></i>
                    </button>
                </td>

                <td>{{$status->cust_id}}</td>
                <td>{{$status->cust_date}}</td>
                <td>{{isset($status->user) ? ucfirst($status->user->us_name) : 'Unknown'}}</td>
                <td>{{isset($status->cust_status) ? $customerstatusstatuses[$status->cust_status] : ''}}</td>
                <td>{{isset($status->cust_reason) ? $customerstatusreasons[$status->cust_reason] : ''}}</td>
                <td>{{$status->cust_turnover_netto}}</td>
                <td>{{$status->cust_remark}}</td>

                <td class="text-center">
                    <button type="button" class="btn btn-sm btn-primary status_delete" data-toggle="tooltip"
                            title="Delete" data-status_id="{{$status->cust_id}}">
                        <i class="fa fa-times"></i>
                    </button>
                </td>

            </tr>
        @endforeach
        </tbody>
    </table>
</div>
