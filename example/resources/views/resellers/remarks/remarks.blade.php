<div class="block-header block-header-default">
    <h3 class="block-title">
        <a href="{{ url('resellers/' . $customer->cu_id . '/customerremark/create')}}">
            <button class="btn btn-primary">Add a remark</button>
        </a>

    </h3>
</div>
<div class="block-content block-content-full">
    <table data-order='[[0, "desc"]]' class="table table-bordered table-striped table-vcenter js-dataTable-full">
        <thead>
        <tr>
            <th>ID</th>
            <th>Timestamp</th>
            <th>Contact date</th>
            <th>Department</th>
            <th>Employee</th>
            <th>Medium</th>
            <th>Direction</th>
            <th>Remark</th>
            <th>Delete</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($remarks as $remark)
            <tr>
                <td>{{$remark->cure_id}}</td>
                <td>{{$remark->cure_timestamp}}</td>
                <td>{{$remark->cure_contact_date}}</td>
                <td>{{isset($remark->cure_department) ? $customerremarkdepartments[$remark->cure_department] : ''}}</td>
                <td>{{isset($remark->user) ? $remark->user->us_name : 'Unknown'}}</td>
                <td>{{isset($remark->cure_medium) ? $customerremarkmediums[$remark->cure_medium] : ''}}</td>
                <td>{{isset($remark->cure_direction) ? $customerremarkdirections[$remark->cure_direction] : ''}}</td>
                @if($remark->cure_status_id)
                    <td>
                        <table>
                            <tbody>
                            <tr>
                                <th colspan="4">
                                    <strong>Status update</strong>
                                </th>
                            </tr>
                            <tr>
                                <td>
                                    Status
                                </td>
                                <td>
                                    Reason
                                </td>
                                <td>
                                    Netto turnover
                                </td>
                                <td>
                                    Remark
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    {{$customerstatusstatuses[$remark->status->cust_status]}}
                                </td>
                                <td>
                                    {{$customerstatusreasons[$remark->status->cust_reason]}}
                                </td>
                                <td>
                                    {{$remark->status->cust_turnover_netto}}
                                </td>
                                <td>
                                    {{$remark->status->cust_remark}}
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                @else
                    <td>{!! $remark->cure_text !!}</td>
                @endif
                @if(!$remark->cure_status_id)
                    <td class="text-center">
                        <button type="button" class="btn btn-sm btn-primary remark_delete" data-toggle="tooltip"
                                title="Delete" data-remark_id="{{$remark->cure_id}}">
                            <i class="fa fa-times"></i>
                        </button>
                    </td>
                @else
                    <td></td>
                @endif
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
