@extends('layouts.backend')

@include( 'scripts.datatables' )
@include( 'scripts.dialogs' )

@section('content')
    <!-- Page Content -->
    <div class="content">
        <!-- Your Block -->
        <div class="block block-rounded block-bordered">
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Confirming...
                </h3>
            </div>
            <div class="block-content block-content-full">
                <form method="post"
                      action="{{action('CustomersController@store')}}">
                    @csrf
                    <input name="_method" type="hidden" value="post">
                    <div class="row">
                        <div class="col-md-6">
                            {!! $double_company_message_html !!}

                            <input type="hidden" name="type" value="{{$request->type}}" />
                            <input type="hidden" name="la_code" value="{{$request->la_code}}" />
                            <input type="hidden" name="description" value="{{$request->description}}" />
                            <input type="hidden" name="company_name_legal" value="{{$request->company_name_legal}}" />
                            <input type="hidden" name="company_name_business" value="{{$request->company_name_business}}" />
                            <input type="hidden" name="attn" value="{{$request->attn}}" />
                            <input type="hidden" name="attn_email" value="{{$request->attn_email}}" />
                            <input type="hidden" name="coc" value="{{$request->coc}}" />
                            <input type="hidden" name="leads_email" value="{{$request->leads_email}}" />
                            <input type="hidden" name="review_email" value="{{$request->review_email}}" />
                            <input type="hidden" name="load_exchange_email" value="{{$request->load_exchange_email}}" />
                            <input type="hidden" name="general_info" value="{{$request->general_info}}" />
                            <input type="hidden" name="street_1" value="{{$request->street_1}}" />
                            <input type="hidden" name="street_2" value="{{$request->street_2}}" />
                            <input type="hidden" name="zipcode" value="{{$request->zipcode}}" />
                            <input type="hidden" name="city" value="{{$request->city}}" />
                            <input type="hidden" name="country" value="{{$request->country}}" />
                            <input type="hidden" name="int_telephone" value="{{$request->int_telephone}}" />
                            <input type="hidden" name="telephone" value="{{$request->telephone}}" />
                            <input type="hidden" name="website" value="{{$request->website}}" />
                            <input type="hidden" name="street_1_bi" value="{{$request->street_1_bi}}" />
                            <input type="hidden" name="street_2_bi" value="{{$request->street_2_bi}}" />
                            <input type="hidden" name="zipcode_bi" value="{{$request->zipcode_bi}}" />
                            <input type="hidden" name="city_bi" value="{{$request->city_bi}}" />
                            <input type="hidden" name="country_bi" value="{{$request->country_bi}}" />
                            <input type="hidden" name="int_telephone_bi" value="{{$request->int_telephone_bi}}" />
                            <input type="hidden" name="telephone_bi" value="{{$request->telephone_bi}}" />
                            <input type="hidden" name="website_bi" value="{{$request->website_bi}}" />

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" name="confirmed" class="btn btn-primary">Yes, create anyways!</button>
                                    <a href="/customers" class="btn btn-primary">Back to Customers</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END Your Block -->
    </div>
    <!-- END Page Content -->
@endsection
