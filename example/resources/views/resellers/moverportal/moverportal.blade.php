<div class="block-content">
    <form method="post"
          action="{{action('ResellersController@update', $customer->cu_id)}}">
        @csrf
        <input name="_method" type="hidden" value="PATCH">
        <input name="form_name" type="hidden" value="Mover Portal">

        <h2 class="content-heading pt-0">Credits
            <a href="{{ url('resellers/' . $customer->cu_id . '/customercredit/create')}}">
                <button type="button" class="btn btn-primary">Add credits
                </button>
            </a>
        </h2>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label">Current
                credits:</label>
            <div class="col-sm-2">
                <input type="text" class="form-control" disabled
                       value="{{$credits['now']}}">
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label">Credits
                +14 days:</label>
            <div class="col-sm-2">
                <input type="text" class="form-control" disabled
                       value="{{$credits['2weeks']}}">
            </div>
        </div>

        <h2 class="content-heading pt-0">Functionalities</h2>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label"
                   for="moda_hide_lead_partner_functions">Hide lead partner
                functions:</label>
            <div class="col-sm-7">
                <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                    <input type="checkbox" class="custom-control-input"
                           id="moda_hide_lead_partner_functions"
                           name="moda_hide_lead_partner_functions"{{($moverdata->moda_hide_lead_partner_functions ? "checked=''" : "")}}>
                    <label class="custom-control-label"
                           for="moda_hide_lead_partner_functions">Turn this on
                        if you want to hide the leads chart and 'Requests' +
                        'Billing' tab.</label>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label"
                   for="moda_hide_response_chart">Hide response chart:</label>
            <div class="col-sm-7">
                <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                    <input type="checkbox" class="custom-control-input"
                           id="moda_hide_response_chart"
                           name="moda_hide_response_chart"{{($moverdata->moda_hide_response_chart ? "checked=''" : "")}}>
                    <label class="custom-control-label"
                           for="moda_hide_response_chart">Turn this on if you
                        want to hide the reponse chart in the mover
                        application.</label>
                </div>
            </div>
        </div>

        <h2 class="content-heading pt-0">Free Leads</h2>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label"
                   for="moda_activate_lead_pick">Activate free leads</label>
            <div class="col-sm-7">
                <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                    <input type="checkbox" class="custom-control-input"
                           id="moda_activate_lead_pick"
                           name="moda_activate_lead_pick"{{($moverdata->moda_activate_lead_pick ? "checked=''" : "")}}>
                    <label class="custom-control-label"
                           for="moda_activate_lead_pick">Turn this on if you
                        want to activate the lead pick in the mover
                        application.</label>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label" for="moda_lead_pick_import">Add
                import to the free leads:</label>
            <div class="col-sm-7">
                <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                    <input type="checkbox" class="custom-control-input"
                           id="moda_lead_pick_import"
                           name="moda_lead_pick_import"{{($moverdata->moda_lead_pick_import ? "checked=''" : "")}}>
                    <label class="custom-control-label"
                           for="moda_lead_pick_import">Turn this on if you want
                        to add import leads to the lead pick.</label>
                </div>
            </div>
        </div>

        <h2 class="content-heading pt-0">Load Exchange</h2>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label"
                   for="moda_activate_load_exchange">Activate Load
                Exchange</label>
            <div class="col-sm-7">
                <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                    <input type="checkbox" class="custom-control-input"
                           id="moda_activate_load_exchange"
                           name="moda_activate_load_exchange"{{($moverdata->moda_activate_load_exchange ? "checked=''" : "")}}>
                    <label class="custom-control-label"
                           for="moda_activate_load_exchange">Turn this on if you
                        want to add the Load Exchange to the mover
                        application.</label>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label" for="moda_load_exchange_email">Load
                Exchange mail:</label>
            <div class="col-sm-7">
                <input type="text" class="form-control"
                       name="moda_load_exchange_email"
                       value="{{$moverdata->moda_load_exchange_email}}">
            </div>
        </div>

        <h2 class="content-heading pt-0">Premium Leads</h2>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label"
                   for="moda_activate_premium_leads">Activate Premium leads</label>
            <div class="col-sm-7">
                <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                    <input type="checkbox" class="custom-control-input"
                           id="moda_activate_premium_leads"
                           name="moda_activate_premium_leads"{{($moverdata->moda_activate_premium_leads ? "checked=''" : "")}}>
                    <label class="custom-control-label"
                           for="moda_activate_premium_leads">Turn this on if you
                        want to activate Premium Leads in the mover
                        application.</label>
                </div>
            </div>
        </div>

        <div id="premium_leads_section" style="@if ($moverdata->moda_activate_premium_leads) {{"display:block;"}} @else {{"display:none;"}} @endif">
            <div class="form-group row">
                <div class="col-sm-1"></div>
                <label class="col-sm-3 col-form-label" for="moda_premium_profile_summary">Summary:</label>
                <div class="col-sm-7">
                    <textarea class="form-control"
                              name="moda_premium_profile_summary">{{$moverdata->moda_premium_profile_summary}}</textarea>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-1"></div>
                <label class="col-sm-3 col-form-label" for="iso">ISO:</label>
                <div class="col-sm-7">
                    <select size="4" class="chosen-select" multiple="multiple" name="iso[]" id="iso">
                        <option value="moda_quality_management_iso" @if($moverdata->moda_quality_management_iso) {{"selected"}}  @endif>Quality Management</option>
                        <option value="moda_environmental_management_iso" @if($moverdata->moda_environmental_management_iso) {{"selected"}}  @endif>Environmental Management</option>
                        <option value="moda_occupational_health_and_safety_iso" @if($moverdata->moda_occupational_health_and_safety_iso) {{"selected"}}  @endif>Occupational Health and Safety</option>
                        <option value="moda_information_security_management_iso" @if($moverdata->moda_information_security_management_iso) {{"selected"}}  @endif>Information Security Management</option>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-1"></div>
                <label class="col-sm-3 col-form-label" for="moda_premium_profile_payment_terms">Payment terms:</label>
                <div class="col-sm-7">
                    <input type="text" class="form-control"
                           name="moda_premium_profile_payment_terms"
                           value="{{$moverdata->moda_premium_profile_payment_terms}}">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-1"></div>
                <label class="col-sm-3 col-form-label" for="moda_payment_terms">Payment methods:</label>
                <div class="col-sm-7">
                    <select size="6" class="chosen-select" multiple="multiple" name="payment_methods[]" id="payment_methods">
                        <option value="moda_payment_method_cash" @if($moverdata->moda_payment_method_cash) {{"selected"}}  @endif>Cash</option>
                        <option value="moda_payment_method_cheque" @if($moverdata->moda_payment_method_cheque) {{"selected"}}  @endif>Cheque</option>
                        <option value="moda_payment_method_bank_transfer" @if($moverdata->moda_payment_method_bank_transfer) {{"selected"}}  @endif>Bank Transfer</option>
                        <option value="moda_payment_method_auto_debit" @if($moverdata->moda_payment_method_auto_debit) {{"selected"}}  @endif>Auto Debit</option>
                        <option value="moda_payment_method_credit_card" @if($moverdata->moda_payment_method_credit_card) {{"selected"}}  @endif>Credit Card</option>
                        <option value="moda_payment_method_paypal" @if($moverdata->moda_payment_method_paypal) {{"selected"}}  @endif>PayPal</option>
                    </select>
                </div>
            </div>

        </div>

        <h2 class="content-heading pt-0"></h2>

        <div class="row">
            <div class="col-md-1"></div>
            <div class="form-group col-md-8">
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
        </div>

    </form>
</div>
