<div class="block-content">
    <form method="post"
          action="{{action('ResellersController@update', $customer->cu_id)}}">
        @csrf
        <input name="_method" type="hidden" value="PATCH">
        <input name="form_name" type="hidden" value="Sirelo">

        <h2 class="content-heading pt-0">Sirelo
            <a target="_blank" href="{{$sirelopage}}">
                <button style="margin-left: 10px;" type="button"
                        class="btn btn-primary">Visit Sirelo
                </button>
            </a>
        </h2>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label">Sirelo
                Page:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" disabled
                       value="{{$sirelopage}}">
            </div>
            <div class="col-sm-1">
                <a target="_blank" href="{{$sirelopage}}"><i class="fa fa-external-link-alt"></i></a>
            </div>
        </div>

        @if(!empty($customer->moverdata->moda_sirelo_forward_1))

            <div class="form-group row">
                <div class="col-sm-1"></div>
                <label class="col-sm-3 col-form-label">Sirelo
                    Forward 1:</label>
                <div class="col-sm-7">
                    <input type="text" class="form-control" disabled
                           value="{{$sirelourl.$customer->moverdata->moda_sirelo_forward_1}}">
                </div>
            </div>

        @endif

        @if(!empty($customer->moverdata->moda_sirelo_forward_2))
            <div class="form-group row">
                <div class="col-sm-1"></div>
                <label class="col-sm-3 col-form-label">Sirelo
                    Forward 2:</label>
                <div class="col-sm-7">
                    <input type="text" class="form-control" disabled
                           value="{{$sirelourl.$customer->moverdata->moda_sirelo_forward_2}}">
                </div>
            </div>

        @endif

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label">Sirelo
                Rating:</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" disabled
                       value="Average score of {{round($sirelorating['rating'] * 2, 1)}} out of {{$sirelorating['reviews']}} reviews">
            </div>
        </div>

        <h2 class="content-heading pt-0">Settings</h2>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label" for="sirelo_bucket">Checked for Sirelo-bucket:</label>
            <div class="col-sm-7">
                <div class="custom-control custom-switch custom-control-lg custom-control-primary">
                    <input type="checkbox" class="custom-control-input" id="sirelo_bucket"
                           name="sirelo_bucket" {{($moverdata->moda_checked_sirelo ? "checked=''" : "")}}>
                    <label class="custom-control-label"
                           for="sirelo_bucket">{{($moverdata->moda_checked_sirelo ? 'This company has been checked by '.$sirelo_bucked_user.' on '.$moverdata->moda_checked_timestamp : '')}}</label>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label" for="disable_sirelo_export">Disable
                Sirelo export</label>
            <div class="col-sm-7">
                <div class="custom-control custom-switch custom-control-lg custom-control-primary">
                    <input type="checkbox" class="custom-control-input"
                           id="disable_sirelo_export"
                           name="disable_sirelo_export"{{($moverdata->moda_disable_sirelo_export ? "checked=''" : "")}}>
                    <label class="custom-control-label"
                           for="disable_sirelo_export">Turn this on if you don't
                        want this company to be shown on Sirelo</label>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label"
                   for="disable_sirelo_top_mover">Disable Sirelo top
                mover</label>
            <div class="col-sm-7">
                <div class="custom-control custom-switch custom-control-lg custom-control-primary">
                    <input type="checkbox" class="custom-control-input"
                           id="disable_sirelo_top_mover"
                           name="disable_sirelo_top_mover" {{($moverdata->moda_sirelo_disable_top_mover ? "checked=''" : "")}}>
                    <label class="custom-control-label"
                           for="disable_sirelo_top_mover">Turn this on if you
                        don’t want the Sirelo quality certificate to be shown
                        for this moving company</label>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label"
                   for="disable_request_form">Disable Request form on (Sirelo) Mover Profile</label>
            <div class="col-sm-7">
                <div class="custom-control custom-switch custom-control-lg custom-control-primary">
                    <input type="checkbox" class="custom-control-input"
                           id="disable_request_form"
                           name="disable_request_form" {{($moverdata->moda_sirelo_disable_request_form ? "checked=''" : "")}}>
                    <label class="custom-control-label"
                           for="disable_request_form">Turn this on if you
                        don’t want to show the request button on Sirelo customer page.</label>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label"
                   for="sirelo_review_verification">Sirelo review
                verification</label>
            <div class="col-sm-7">
                <div class="custom-control custom-switch custom-control-lg custom-control-primary">
                    <input type="checkbox" class="custom-control-input"
                           id="sirelo_review_verification"
                           name="sirelo_review_verification" {{($moverdata->moda_sirelo_review_verification ? "checked=''" : "")}}>
                    <label class="custom-control-label"
                           for="sirelo_review_verification">For this company,
                        each review has to be verified before publishing</label>
                </div>
            </div>
        </div>

        <div id="sirelo_widget_location" class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label" for="widget_location">Sirelo
                widget location:</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" name="widget_location"
                       value="{{$moverdata->moda_sirelo_widget_location}}">
            </div>
            <div class="col-sm-1">
                <a target="_blank" href="{{$moverdata->moda_sirelo_widget_location}}"><i class="fa fa-external-link-alt"></i></a>
            </div>
        </div>

        <h2 class="content-heading pt-0"></h2>

        <div class="alert alert-danger" role="alert">
            <p class="mb-0">ATTENTION: The data below will be shared on our
                Sirelo websites!</p>
        </div>

        <h2 class="content-heading pt-0">Contact</h2>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label" for="review_communication_email">Review
                communication email:</label>
            <div class="col-sm-7">
                <input type="text" class="form-control"
                       name="review_communication_email"
                       value="{{$moverdata->moda_review_email}}">
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label" for="general_info_email">General
                info email:</label>
            <div class="col-sm-7">
                <input type="text" class="form-control"
                       name="general_info_email"
                       value="{{$moverdata->moda_info_email}}">
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label" for="contact_email">Shown
                on Sirelo email:</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" name="contact_email"
                       value="{{$moverdata->moda_contact_email}}">
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label" for="contact_telephone">Contact
                telephone:</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" name="contact_telephone"
                       value="{{$moverdata->moda_contact_telephone}}">
            </div>
        </div>

        <h2 class="content-heading pt-0">Moves</h2>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label" for="international_moves">International
                moves:</label>
            <div class="col-sm-7">
                <div class="custom-control custom-switch custom-control-lg custom-control-primary">
                    <input type="checkbox" class="custom-control-input"
                           id="international_moves"
                           name="international_moves" {{($moverdata->moda_international_moves ? "checked=''" : "")}}>
                    <label class="custom-control-label"
                           for="international_moves"></label>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label" for="national_moves">National
                moves:</label>
            <div class="col-sm-7">
                <div class="custom-control custom-switch custom-control-lg custom-control-primary">
                    <input type="checkbox" class="custom-control-input"
                           id="national_moves"
                           name="national_moves" {{($moverdata->moda_national_moves ? "checked=''" : "")}}>
                    <label class="custom-control-label"
                           for="national_moves"></label>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label" for="excess_baggage">Excess
                baggage:</label>
            <div class="col-sm-7">
                <div class="custom-control custom-switch custom-control-lg custom-control-primary">
                    <input type="checkbox" class="custom-control-input"
                           id="excess_baggage"
                           name="excess_baggage" {{($moverdata->moda_excess_baggage ? "checked=''" : "")}}>
                    <label class="custom-control-label"
                           for="excess_baggage"></label>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label" for="man_and_van">Man &
                van:</label>
            <div class="col-sm-7">
                <div class="custom-control custom-switch custom-control-lg custom-control-primary">
                    <input type="checkbox" class="custom-control-input"
                           id="man_and_van"
                           name="man_and_van" {{($moverdata->moda_man_and_van ? "checked=''" : "")}}>
                    <label class="custom-control-label"
                           for="man_and_van"></label>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label"
                   for="car_and_vehicle_transport">Car & Vehicle
                transport:</label>
            <div class="col-sm-7">
                <div class="custom-control custom-switch custom-control-lg custom-control-primary">
                    <input type="checkbox" class="custom-control-input"
                           id="car_and_vehicle_transport"
                           name="car_and_vehicle_transport" {{($moverdata->moda_car_and_vehicle_transport ? "checked=''" : "")}}>
                    <label class="custom-control-label"
                           for="car_and_vehicle_transport"></label>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label" for="piano_transport">Piano
                transport:</label>
            <div class="col-sm-7">
                <div class="custom-control custom-switch custom-control-lg custom-control-primary">
                    <input type="checkbox" class="custom-control-input"
                           id="piano_transport"
                           name="piano_transport" {{($moverdata->moda_piano_transport ? "checked=''" : "")}}>
                    <label class="custom-control-label"
                           for="piano_transport"></label>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label" for="pet_transport">Pet
                transport:</label>
            <div class="col-sm-7">
                <div class="custom-control custom-switch custom-control-lg custom-control-primary">
                    <input type="checkbox" class="custom-control-input"
                           id="pet_transport"
                           name="pet_transport" {{($moverdata->moda_pet_transport ? "checked=''" : "")}}>
                    <label class="custom-control-label"
                           for="pet_transport"></label>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label" for="art_transport">Art
                transport:</label>
            <div class="col-sm-7">
                <div class="custom-control custom-switch custom-control-lg custom-control-primary">
                    <input type="checkbox" class="custom-control-input"
                           id="art_transport"
                           name="art_transport" {{($moverdata->moda_art_transport ? "checked=''" : "")}}>
                    <label class="custom-control-label"
                           for="art_transport"></label>
                </div>
            </div>
        </div>

        <h2 class="content-heading pt-0">Company</h2>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label" for="established">Established:</label>
            <div class="col-sm-2">
                <input autocomplete="off" type="text"
                       class="js-datepicker form-control"
                       id="example-datepicker3" name="established"
                       data-week-start="1" data-autoclose="true"
                       data-today-highlight="true" data-date-format="yyyy-mm-dd"
                       placeholder="yyyy-mm-dd"
                       value="{{$moverdata->moda_established}}">
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label" for="employees">Employees:</label>
            <div class="col-sm-2">
                <input type="number" autocomplete="off" class="form-control"
                       name="employees" min="0"
                       value="{{$moverdata->moda_employees}}">
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label" for="trucks">Trucks:</label>
            <div class="col-sm-2">
                <input type="number" autocomplete="off" class="form-control"
                       name="trucks" min="0"
                       value="{{$moverdata->moda_trucks}}">
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label" for="type_of_mover">Type of mover:</label>
            <div class="col-sm-4">
                <select type="text" name="type_of_mover" class="form-control"
                        name="type">
                    @foreach($typeofmovers as $id => $value)
                        <option
                            value="{{$id}}" {{$customer->cu_type_of_mover == $id ? "selected" : ""}}>{{$value}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label" for="public_liability">Public liability:</label>
            <div class="col-sm-7">
                <div class="custom-control custom-switch custom-control-lg custom-control-primary">
                    <input type="checkbox" class="custom-control-input"
                           id="public_liability"
                           name="public_liability" {{($customer->cu_public_liability_insurance ? "checked=''" : "")}}>
                    <label class="custom-control-label"
                           for="public_liability"></label>
                </div>
            </div>
        </div>

        <div class="form-group row" id="public_liability_value">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label" for="public_liability_value">Public liability up to:</label>
            <div class="col-sm-2">
                <input type="text" autocomplete="off" class="form-control"
                       name="public_liability_value"
                       value="{{$customer->cu_public_liability_insurance_value}}">
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label" for="goods_in_transit">Goods in transit:</label>
            <div class="col-sm-7">
                <div class="custom-control custom-switch custom-control-lg custom-control-primary">
                    <input type="checkbox" class="custom-control-input"
                           id="goods_in_transit"
                           name="goods_in_transit" {{($customer->cu_goods_in_transit_insurance ? "checked=''" : "")}}>
                    <label class="custom-control-label"
                           for="goods_in_transit"></label>
                </div>
            </div>
        </div>

        <div class="form-group row" id="goods_in_transit_insurance_value">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label" for="goods_in_transit_value">Goods in transit up to:</label>
            <div class="col-sm-2">
                <input type="text" autocomplete="off" class="form-control"
                       name="goods_in_transit_value"
                       value="{{$customer->cu_goods_in_transit_insurance_value}}">
            </div>
        </div>

        @if (!empty($intmoverpermits[$customer->cu_co_code]))
            <div class="form-group row">
                <div class="col-sm-1"></div>
                <label class="col-sm-3 col-form-label" for="international_permit"><b>(INT)</b> {{$intmoverpermits[$customer->cu_co_code]}}</label>
                <div class="col-sm-7">
                    <div class="custom-control custom-switch custom-control-lg custom-control-primary">
                        <input type="checkbox" class="custom-control-input"
                               name="international_permit" {{($customer->cu_international_permit ? "checked=''" : "")}}
                                id="international_permit">
                        <label class="custom-control-label"
                               for="international_permit"></label>
                    </div>
                </div>
            </div>
        @endif

        @if (!empty($natmoverpermits[$customer->cu_co_code]))
            <div class="form-group row">
                <div class="col-sm-1"></div>
                <label class="col-sm-3 col-form-label" for="national_permit"><b>(NAT)</b> {{$natmoverpermits[$customer->cu_co_code]}}</label>
                <div class="col-sm-7">
                    <div class="custom-control custom-switch custom-control-lg custom-control-primary">
                        <input type="checkbox" class="custom-control-input"
                               name="national_permit" {{($customer->cu_national_permit ? "checked=''" : "")}}
                                id="national_permit">
                        <label class="custom-control-label"
                               for="national_permit"></label>
                    </div>
                </div>
            </div>
        @endif


        <h2 class="content-heading pt-0"></h2>

        <div class="row">
            <div class="col-md-1"></div>
            <div class="form-group col-md-8">
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
        </div>
    </form>
</div>
