@extends('layouts.backend')

@section('content')
	<div class="content">
		<form class="mb-5" method="post" action="{{action('ClaimsController@acceptclaim', $claim->cl_id)}}">
			@csrf
			<div class="row">

				<div class="col-md-12">
					<div class="block block-rounded block-bordered">
						<div class="block-header block-header-default">
							<h3 class="block-title">Accept claim</h3>
						</div>
						<div class="block-content">

							<div class="form-group row">
								<div class="col-sm-1"></div>
								<label class="col-sm-3 col-form-label"
								       for="reason">Reason</label>
								<div class="col-sm-7">
									<select type="text" class="form-control"
									        name="reason">
										@foreach($claimreasons as $id => $reason)
											<option @if($claim->cl_reason == $id) selected @endif value="{{$id}}">{{$reason}}</option>
										@endforeach
									</select>
								</div>
							</div>

							<div class="form-group row">
								<div class="col-sm-1"></div>
								<label class="col-sm-3 col-form-label"
								       for="remarks">Remarks:</label>
								<div class="col-sm-7">
                                    <textarea type="text" disabled class="form-control"
                                              name="remarks">{{$claim->cl_remarks}}</textarea>
								</div>
							</div>

						</div>
					</div>
				</div>

			</div>

			<div class="row">
				<div class="form-group col-md-8">
					<button type="submit" class="btn btn-primary">Accept</button>
					<a href="../{{$claim->cl_id}}/edit">
						<button type="button" class="btn btn-primary">Back</button>
					</a>
				</div>
			</div>

		</form>
	</div>


@endsection




