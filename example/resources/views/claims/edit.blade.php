@extends('layouts.backend')

@include('scripts.select2')

@section('content')
    <div class="content">
        @if($claim->cl_status > 0)
            <div class="alert alert-danger alert-dismissable" role="alert">
                <p class="mb-0">This claim has already been accepted or rejected</p>
            </div>
        @endif
        <div class="row">

            <div class="col-md-6">
                <div class="block block-rounded block-bordered">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Claim</h3>
                    </div>
                    <div class="block-content">
                        <form class="mb-5" method="post" action="{{action('ClaimsController@update', $claim->cl_id)}}">
                            @csrf
                            <input name="_method" type="hidden" value="PATCH">
                            <input name="customer_id" type="hidden" value="">

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label"
                                       for="submitted_timestamp">Submitted:</label>
                                <div class="col-sm-7">
                                    <input type="text" disabled class="form-control"
                                           name="submitted_timestamp"
                                           value="{{$claim->cl_timestamp}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label"
                                       for="status">Status:</label>
                                <div class="col-sm-7">
                                    <input type="text" disabled class="form-control"
                                           name="status"
                                           value="{{$claimstatuses[$claim->cl_status]}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label"
                                       for="reason">Reason:</label>
                                <div class="col-sm-7">
                                    <input type="text" disabled class="form-control"
                                           name="reason"
                                           value="{{$claimreasons[$claim->cl_reason]}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label"
                                       for="remarks">Remarks:</label>
                                <div class="col-sm-7">
                                    <textarea type="text" disabled class="form-control"
                                           name="remarks">{{$claim->cl_remarks}}</textarea>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="block block-rounded block-bordered">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Internal information</h3>
                    </div>
                    <div class="block-content">
                        <form class="mb-5" method="post" action="{{action('ClaimsController@update', $claim->cl_id)}}">
                            @csrf
                            <input name="_method" type="hidden" value="PATCH">
                            <input name="customer_id" type="hidden" value="">

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label"
                                       for="called">Called:</label>
                                <div class="col-sm-7">
                                    <input type="text" disabled class="form-control"
                                           name="called"
                                           value="{{$cl_called}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label"
                                       for="internal_called_timestamp">Called on:</label>
                                <div class="col-sm-7">
                                    <input type="text" disabled class="form-control"
                                           name="internal_called_timestamp"
                                           value="{{$claim->customerrequestportal->request->re_internal_called_timestamp}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label"
                                       for="internal_remarks">Remarks:</label>
                                <div class="col-sm-7">
                                    <textarea type="text" disabled class="form-control"
                                              name="internal_remarks">{{$claim->customerrequestportal->request->re_internal_remarks}}</textarea>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="block block-rounded block-bordered">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Customer details</h3>
                    </div>
                    <div class="block-content">
                        <form class="mb-5" method="post" action="{{action('ClaimsController@update', $claim->cl_id)}}">
                            @csrf
                            <input name="_method" type="hidden" value="PATCH">
                            <input name="customer_id" type="hidden" value="">

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label"
                                       for="company_name_business">Customer:</label>
                                <div class="col-sm-7">
                                    <input type="text" disabled class="form-control"
                                           name="company_name_business"
                                           value="{{$claim->customerrequestportal->customer->cu_company_name_business}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label"
                                       for="email">Email:</label>
                                <div class="col-sm-7">
                                    <input type="text" disabled class="form-control"
                                           name="email"
                                           value="{{$claim->customerrequestportal->customer->cu_email}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label"
                                       for="special_agreements">Special agreements:</label>
                                <div class="col-sm-7">
                                    <textarea type="text" disabled class="form-control"
                                              name="special_agreements">{{$claim->customerrequestportal->customer->moverdata->moda_special_agreements}}</textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label"
                                       for="claim_rate">Claim rate:</label>
                                <div class="col-sm-7">
                                    <input type="text" disabled class="form-control"
                                           name="claim_rate"
                                           value="{{number_format($claimrate['percentage'], 2, '.', '')}}%  ({{$claimrate['total_claimed_requests']}} claimed out of {{$claimrate['requests']}} requests)">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="block block-rounded block-bordered">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Matches</h3>
                    </div>
                    <div class="block-content">
                        <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Customer</th>
                                <th>Matched</th>
                                <th>Type</th>
                                <th>Rematch</th>
                                <th>Claim</th>
                                <th>Claim reason</th>
                                <th>Free</th>
                                <th>Free reason</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($other_claims as $request)
                                <tr>
                                    <td>{{$request->ktrecupo_id}}</td>
                                    <td>{{$request->cu_company_name_business}} @if($claim->customerrequestportal->customer->cu_id == $request->cu_id) <i class="fa fa-check btn btn-sm btn-primary"></i>  @endif</td>
                                    <td>{{$request->ktrecupo_timestamp}}</td>
                                    <td>{{$requestcustomerportaltyes[$request->ktrecupo_type]}}</td>
                                    <td>{{$yesno[$request->ktrecupo_rematch]}}</td>
                                    <td>{{($request->cl_status === null ? "-" : $claimstatuses[$request->cl_status])}}</td>
                                    <td>{{($request->cl_reason === null ? "-" : $claimreasons[$request->cl_reason])}}</td>
                                    <td>{{$yesno[$request->ktrecupo_free]}}</td>
                                    <td>{{($request->ktrecupo_free == 0 ? "-" : $claimreasons[$request->ktrecupo_free_reason])}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="block block-rounded block-bordered">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Request Information</h3>
                    </div>
                    <div class="block-content">
                        <div class="row justify-content-center">
                            <div class="col-md-6">
                                <div class="block block-rounded block-bordered">
                                    <div class="block-header block-header-default">
                                        <h3 class="block-title">General</h3>
                                    </div>
                                    <div class="block-content">

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-3 col-form-label" for="for-received">Received:</label>
                                            <div class="col-sm-7">
                                                <input disabled class="form-control" name="received" id="for-received" type="text" value="{{$request->re_timestamp}}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-3 col-form-label" for="example-hf-email">Status:</label>
                                            <div class="col-sm-7">
                                                <input disabled class="form-control" type="text"
                                                       value="{{$requeststatuses[$request->re_status]}}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-3 col-form-label" for="example-hf-email">Portal:</label>
                                            <div class="col-sm-7">
                                                <input disabled class="form-control" type="text" value="{{$portal}}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-3 col-form-label" for="example-hf-email">Source:</label>
                                            <div class="col-sm-7">
                                                <input disabled class="form-control" type="text" value="{{$requestsource}}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-3 col-form-label" for="example-hf-email">Form:</label>
                                            <div class="col-sm-7">
                                                <input disabled class="form-control" type="text" value="{{$form}}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-3 col-form-label" for="example-hf-email">Language:</label>
                                            <div class="col-sm-7">
                                                <input disabled class="form-control" type="text"
                                                       value="{{$language}}">
                                            </div>
                                        </div>

	                                    <div class="form-group row">
		                                    <div class="col-sm-1"></div>
		                                    <label class="col-sm-3 col-form-label" for="example-hf-email">IP address:</label>
		                                    @if (!empty($request->re_ip_address_country))
			                                    <div class="col-sm-3" style="align-self: center;"><img src="/media/flags/{{strtolower($request->re_ip_address_country)}}.png"/> {{$countries[$request->re_ip_address_country]}}</div>
		                                    @endif
		                                    <div class="col-sm-4">

			                                    <input disabled class="form-control" type="text"
			                                           value="{{$request->re_ip_address}}">
		                                    </div>

	                                    </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="block block-rounded block-bordered">
                                    <div class="block-header block-header-default">
                                        <h3 class="block-title">Moving details</h3>
                                    </div>
                                    <div class="block-content">

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-3 col-form-label" for="example-hf-email">Destination type:</label>
                                            <div class="col-sm-7">
                                                <input disabled class="form-control" name="destination_type" type="text" data-id="{{$request->re_destination_type}}"  value="{{$destinationtype}}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-3 col-form-label" for="example-hf-email">Request type:</label>
                                            <div class="col-sm-7">
                                                @foreach($requesttypes as $id => $name)
                                                    <div class="custom-control custom-radio custom-control-primary">
                                                        <input disabled type="radio" class="custom-control-input" id="request_type-{{$id}}" name="request_type" {{ ($request->re_request_type == $id) ? 'checked':'' }}>
                                                        <label class="custom-control-label" for="request_type-{{$id}}">{{$name}}</label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-3 col-form-label" for="example-hf-email">Moving size:</label>
                                            <div class="col-sm-7">
                                                <input disabled class="form-control" type="text" value="{{$movingsizes[$request->re_moving_size]}}">
                                            </div>
                                        </div>

	                                    <div class="form-group row">
		                                    <div class="col-sm-1"></div>
		                                    <label class="col-sm-3 col-form-label" for="example-hf-email">Remarks:</label>
		                                    <div class="col-sm-7">
			                                    <textarea style="height: 100px;" disabled name="remark" class="form-control" type="text">{{$request->re_remarks}}</textarea>
		                                    </div>
	                                    </div>

	                                    <div class="form-group row" name="translate_div" style="display:none;">
		                                    <div class="col-sm-1"></div>
		                                    <label class="col-sm-3 col-form-label" for="remark_translation">Translated:</label>
		                                    <div class="col-sm-7">
                                             <textarea style="height: 100px;"  class="form-control" type="text" disabled
                                              name="remark_translation"></textarea>
		                                    </div>
	                                    </div>

	                                    <div class="row">
		                                    <div class="col-sm-1"></div>
		                                    <div class="col-sm-3"></div>
		                                    <div class="form-group col-md-7">
			                                    <button type="button" name="translate" class="btn btn-sm btn-primary">Translate</button>
		                                    </div>
	                                    </div>

	                                    <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-3 col-form-label" for="example-hf-email">Moving date:</label>
                                            <div class="col-sm-7">
                                                <input disabled autocomplete="off" value="{{$request->re_moving_date}}" type="text"
                                                       class="js-datepicker form-control" id="example-datepicker3" name="date"
                                                       data-week-start="1" data-autoclose="true" data-today-highlight="true"
                                                       data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-3 col-form-label" for="example-hf-email">Volume:</label>
                                            <div class="col-sm-3">
                                                <input disabled type="text" autocomplete="off" class="form-control"
                                                       name="min_volume_m3" value="{{$request->re_volume_m3}}">
                                            </div>
                                            m3
                                            <div class="col-sm-3">
                                                <input disabled type="text" autocomplete="off" class="form-control"
                                                       name="min_volume_ft3" value="{{$request->re_volume_ft3}}">
                                            </div>
                                            ft
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-md-6">
                                <div class="block block-rounded block-bordered">
                                    <div class="block-header block-header-default">
                                        <h3 class="block-title">Moving from</h3>
                                    </div>
                                    <div class="block-content">


                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-3 col-form-label" for="example-hf-email">Street:</label>
                                            <div class="col-sm-7">
                                                <input disabled class="form-control" type="text" value="{{$request->re_street_from}}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-3 col-form-label" for="example-hf-email">Zipcode:</label>
                                            <div class="col-sm-7">
                                                <input disabled class="form-control" type="text" value="{{$request->re_zipcode_from}}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-3 col-form-label" for="example-hf-email">City:</label>
                                            <div class="col-sm-7">
                                                <input disabled class="form-control" type="text" name="city_from"  value="{{$request->re_city_from}}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-3 col-form-label"
                                                   for="example-hf-email">Country:</label>
                                            <div class="col-sm-7">
                                                <select disabled type="text" class="form-control"
                                                        name="country_from">
                                                    @foreach($countries as $id => $country)
                                                        <option
                                                                value="{{$id}}" {{ ($request->re_co_code_from ==  $id) ? 'selected':'' }}>{{$country}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-3 col-form-label"
                                                   for="example-hf-email">Region:</label>
                                            <div class="col-sm-7">
                                                <input disabled class="form-control" type="text" value="{{$region_from}}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-3 col-form-label"
                                                   for="select_residence_from">Residence:</label>
                                            <div class="col-sm-7">
                                                <select disabled type="text" class="form-control"
                                                        name="select_residence_from">
                                                    @foreach($requestresidences as $id => $residence)
                                                        <option
                                                                value="{{$id}}" {{ ($request->re_residence_from ==  $id) ? 'selected':'' }}>{{$residence}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label disabled class="col-sm-3 col-form-label"
                                                   for="example-hf-email">Google maps:</label>
                                            <div class="col-sm-7">
                                                <input disabled class="form-control" type="text" value="{{$request->re_google_maps_from}}">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="block block-rounded block-bordered">
                                    <div class="block-header block-header-default">
                                        <h3 class="block-title">Moving to</h3>
                                    </div>
                                    <div class="block-content">

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-3 col-form-label" for="example-hf-email">Street:</label>
                                            <div class="col-sm-7">
                                                <input disabled class="form-control" type="text" value="{{$request->re_street_to}}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-3 col-form-label" for="example-hf-email">Zipcode:</label>
                                            <div class="col-sm-7">
                                                <input disabled class="form-control" type="text" value="{{$request->re_zipcode_to}}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-3 col-form-label" for="example-hf-email">City:</label>
                                            <div class="col-sm-7">
                                                <input disabled class="form-control" type="text" name="city_to"  value="{{$request->re_city_to}}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-3 col-form-label"
                                                   for="example-hf-email">Country:</label>
                                            <div class="col-sm-7">
                                                <select disabled type="text" class="form-control"
                                                        name="country_to">
                                                    @foreach($countries as $id => $country)
                                                        <option
                                                                value="{{$id}}" {{ ($request->re_co_code_to ==  $id) ? 'selected':'' }}>{{$country}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-3 col-form-label"
                                                   for="example-hf-email">Region:</label>
                                            <div class="col-sm-7">
                                                <input disabled class="form-control" type="text" value="{{$region_to}}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-3 col-form-label"
                                                   for="select_residence_to">Residence:</label>
                                            <div class="col-sm-7">
                                                <select type="text" class="form-control"
                                                        name="select_residence_to">
                                                    @foreach($requestresidences as $id => $residence)
                                                        <option
                                                                value="{{$id}}" {{ ($request->re_residence_to ==  $id) ? 'selected':'' }}>{{$residence}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-3 col-form-label"
                                                   for="example-hf-email">Google maps:</label>
                                            <div class="col-sm-7">
                                                <input disabled class="form-control" type="text" value="{{$request->re_google_maps_to}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-md-6">
                                <div class="block block-rounded block-bordered">
                                    <div class="block-header block-header-default">
                                        <h3 class="block-title">Contact details</h3>
                                    </div>
                                    <div class="block-content">

                                        @if($request->re_company_name)
                                            <div class="form-group row">
                                                <div class="col-sm-1"></div>
                                                <label class="col-sm-3 col-form-label" for="example-hf-email">Company name:</label>
                                                <div class="col-sm-7">
                                                    <input disabled class="form-control" type="text" value="{{$request->re_company_name}}">
                                                </div>
                                            </div>
                                        @endif

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-3 col-form-label" for="example-hf-email">Full name:</label>
                                            <div class="col-sm-7">
                                                <input disabled class="form-control" type="text" value="{{$request->re_full_name}}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-3 col-form-label" for="example-hf-email">Telephone 1:</label>
                                            <div class="col-sm-4">
                                                <input disabled class="form-control" type="text" value="{{$request->re_telephone1}}">
                                            </div>
                                            <div class="col-sm-1"><a href="#" class="add_to_dialfire" data-id="{{$request->re_id}}" data-type="requests"><i class="fa fa-2x fa-phone-square"></i></a></div>
                                            @isset($notices['phone_1'])
                                                <label class="col-sm-3" style="color:red">This phone number is NOT
                                                    valid!</label>
                                            @endisset
                                        </div>

                                        @if($request->re_telephone2)
                                            <div class="form-group row">
                                                <div class="col-sm-1"></div>
                                                <label class="col-sm-3 col-form-label" for="example-hf-email">Telephone 2:</label>
                                                <div class="col-sm-4">
                                                    <input disabled class="form-control" type="text" value="{{$request->re_telephone2}}">
                                                </div>
                                                @isset($notices['phone_2'])
                                                    <label class="col-sm-3" style="color:red">This phone number is NOT
                                                        valid!</label>
                                                @endisset
                                            </div>
                                        @endif

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-3 col-form-label" for="example-hf-email">Email:</label>
                                            <div class="col-sm-7">
                                                <input disabled class="form-control" type="text" value="{{$request->re_email}}">
                                            </div>
                                        </div>

                                        @isset($notices['email_bounce'])
                                            <div class="form-group row">
                                                <div class="col-sm-1"></div>
                                                <label class="col-sm-3 col-form-label">Bounce:</label>
                                                <div class="col-sm-7" style="color:red">
                                                    <label>{!! $notices['email_bounce'] !!}</label>
                                                </div>
                                            </div>
                                        @endisset
                                    </div>
                                </div>
                            </div>
	                        <div class="col-md-6">
		                        <div class="block block-rounded block-bordered">
			                        <div class="block-header block-header-default">
				                        <h3 class="block-title">Internal information</h3>
			                        </div>
			                        <div class="block-content">


				                        <div class="form-group row">
					                        <div class="col-sm-1"></div>
					                        <label class="col-sm-3 col-form-label"
					                               for="example-hf-email">Called:</label>
					                        <div class="col-sm-7">
						                        <select disabled type="text" class="form-control"
						                                name="debt_manager">
							                        @foreach($called as $id => $value)
								                        <option
										                        value="{{$id}}" {{ ($request->re_internal_called == $id) ? 'selected':'' }}>{{$value}}</option>
							                        @endforeach
						                        </select>
					                        </div>
				                        </div>

				                        <div class="form-group row">
					                        <div class="col-sm-1"></div>
					                        <label class="col-sm-3 col-form-label"
					                               for="example-hf-email">Called on:</label>
					                        <div class="col-sm-7">
						                        <input disabled autocomplete="off" value="{{$request->re_internal_called_timestamp}}"
						                               type="text" class="js-datepicker form-control" id="example-datepicker3"
						                               name="date" data-week-start="1" data-autoclose="true"
						                               data-today-highlight="true" data-date-format="yyyy-mm-dd"
						                               placeholder="yyyy-mm-dd">
					                        </div>
				                        </div>

				                        <div class="form-group row">
					                        <div class="col-sm-1"></div>
					                        <label class="col-sm-3 col-form-label"
					                               for="example-hf-email">Remarks:</label>
					                        <div class="col-sm-7">
                                    <textarea disabled type="text" class="form-control"
                                              name="company_name_legal">{{$request->re_internal_remarks}}</textarea>
					                        </div>
				                        </div>
			                        </div>
		                        </div>
	                        </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="form-group col-md-8">
                @if($claim->cl_status == 0)
                <a href="/claims/{{$claim->cl_id}}/accept"><button type="button" class="btn btn-primary">Accept</button></a>
                <a href="/claims/{{$claim->cl_id}}/reject"><button type="button" class="btn btn-primary">Reject</button></a>
                @endif
                <a href="../"><button type="button" class="btn btn-primary">Back</button></a>
            </div>
        </div>

    </div>

@endsection

@push( 'scripts' )
    <script>
        jQuery(document).ready(function(){
        	$( '.add_to_dialfire' ).click( function() {

            if( ! confirm( 'Are you sure you want to add this to dialfire?' ) )
                return false;

            var $id = $(this).data('id');
            var $type = $(this).data('type');

            $.ajax({
                type: "GET",
                url: '{{ url('/ajax/addtodialfire') }}',
                data: {
                    "id": $id,
                    "type": $type
                },
                success: function(data){
                    response = JSON.parse(data);
                    if(response.success) {
                        alert('Telephone number succesfully added to DialFire! (Can be found at ' + response.dialfire_task + ')');
                    }else{
                        alert(response.error);
                    }
                }
            });

            return false;
        });

            jQuery( document ).on( 'click', 'button[name="translate"]', function() {

                // Get value
                getTranslation( $('textarea[name="remark"]').val(), function( $result ) {

                    $('div[name=translate_div]').slideDown();
                    $('textarea[name=remark_translation]').val($result);
                    $('button[name=translate]').attr('disabled', 'disabled');
                });
            });
        });
    </script>

@endpush
