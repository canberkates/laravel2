@extends('layouts.backend')

@include( 'scripts.datatables' )

@include('scripts.forms')
@include('scripts.datepicker')
@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Claims</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url( 'dashboard' )}}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Claims</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <div class="block block-rounded block-bordered col-sm-4">
            <div class="block-content ">
                <form method="post" action="{{action('ClaimsController@filteredindex')}}">
                    {{ csrf_field() }}
                    <input name="_method" type="hidden" value="post">
                    <h2 class="content-heading pt-0">Filters</h2>

                    <div class="form-group row">
                        <label class="col-md-4 col-form-label " for="status">Status:</label>
                        <div class="col-md-7">
                            <select id="status_select" type="text" class="form-control" name="status">
                                @foreach (App\Data\ClaimStatus::all() as $id => $status)
                                    <option @if(isset($filter_status) && $filter_status == $id) selected @endif value="{{$id}}">{{$status}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4 col-form-label " for="reason">Reason:</label>
                        <div class="col-md-7">
                            <select type="text" class="form-control" name="reason">
                                <option value=""></option>
                                @foreach (App\Data\ClaimReason::all() as $id => $reason)
                                    <option @if(isset($filter_reason) && $filter_reason == $id) selected @endif value="{{$id}}">{{$reason}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div id="div_date_range" class="form-group row" @if(!isset($filter_date)) style="display:none;" @endif>
                        <label class="col-md-4 col-form-label" for="date">Date range:</label>
                        <div class="col-md-7">
                            <input id="date" type="text" class="form-control drp drp-default" name="date" @if(isset($filter_date)) value="{{$filter_date}}" @else value="{{$default_date_filter}}" @endif autocomplete="off"/>
                        </div>
                    </div>

                    <div id="div_employee" class="form-group row" @if(!isset($filter_employee) && !isset($filter_date)) style="display:none;" @endif>
                        <label class="col-md-4 col-form-label " for="employee">Employee:</label>
                        <div class="col-md-7">
                            <select type="text" class="form-control" name="employee">
                                <option value=""></option>
                                @foreach ($users as $id => $employee)
                                    <option @if(isset($filter_employee) && $filter_employee == $employee->us_id) selected @endif value="{{$employee->us_id}}">{{$employee->us_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <h2 class="content-heading pt-0"></h2>

                    <div class="row">
                        <div class="form-group col-md-3">
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
        <div class="block block-rounded block-bordered">
            <div class="block-content block-content-full">
                @can('claims - check')
                    <a href="{{ url('/claimscheck')}}"><button data-toggle="click-ripple" class="btn btn-primary mb-5">Claims check</button></a>
                @endcan

                <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Submitted</th>
                        <th>Status</th>
                        @if(isset($filter_status) && $filter_status != 0)
                            <th>Processed</th>
                            <th>Processed by</th>
                        @endif
                        <th>Customer</th>
                        <th>Request ID</th>
                        <th>Name</th>
                        <th>Country from</th>
                        <th>Country to</th>
                        <th>Telephone</th>
                        <th>Reason</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($open_claims as $claim)
                        <tr>
                            <td>{{$claim->cl_id}}</td>
                            <td>{{$claim->cl_timestamp}}</td>
                            <td>{{$claimstatuses[$claim->cl_status]}}</td>
                            @if(isset($filter_status) && $filter_status != 0)
                                <td>{{$claim->cl_processed_timestamp}}</td>
                                <td>{{$claim->claimprocessedbyuser->us_name}}</td>
                            @endif
                            <td>{{$claim->customerrequestportal->customer->cu_company_name_business}}</td>
                            <td>{{$claim->customerrequestportal->ktrecupo_re_id}}</td>
                            <td>{{$claim->customerrequestportal->request->re_full_name}}</td>
                            <td>{{$countries[$claim->customerrequestportal->request->re_co_code_from]}}</td>
                            <td>{{$countries[$claim->customerrequestportal->request->re_co_code_to]}}</td>
                            <td>{{$claim->customerrequestportal->request->re_telephone1}}</td>
                            <td>{{$claimreasons[$claim->cl_reason]}}</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <a class="btn btn-sm btn-primary"
                                       data-toggle="tooltip"
                                       data-placement="left"
                                       title="edit"
                                       href="{{ url('/claims/' . $claim->cl_id . '/edit')}}">
                                        <i class="fa fa-pencil-alt"></i>
                                    </a>
                                </div>
                            </td>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push( 'scripts' )

    <script>
        jQuery(document).ready(function () {
            $( "#status_select" ).change(function() {
                var dateRangeDiv = $("#div_date_range");
                var employeeDiv = $("#div_employee");

                if ($(this).val() == 0)
                {
                    dateRangeDiv.slideUp();
                    employeeDiv.slideUp();
                }
                else if($(this).val() == 1)
                {
                    dateRangeDiv.slideDown();
                    employeeDiv.slideDown();
                }
                else if($(this).val() == 2)
                {
                    dateRangeDiv.slideDown();
                    employeeDiv.slideDown();
                }
            });
        }).trigger("change");
    </script>

@endpush
