@extends('layouts.backend')

@section('content')
    <div class="content">
        <div class="alert alert-danger alert-dismissable" role="alert">
            <p class="mb-0">This claim has already been matched or rejected!</p>
            <br>
            <a href="/claims/"><button class="btn btn-primary">Go Back</button></a>
        </div>
    </div>


@endsection




