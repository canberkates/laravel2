@extends('layouts.backend')

@section('content')
    <div class="content">
        <form class="mb-5" method="post" action="{{action('ClaimsController@rejectclaim', $claim->cl_id)}}">
            @csrf
        <div class="row">
            <div class="col-lg-8">
                <div class="block block-rounded block-bordered">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Reject claim</h3>
                    </div>
                    <div class="block-content">
                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label"
                                   for="for-rejectionreason">Rejection reason:</label>
                            <div class="col-sm-7">
                                <textarea type="text" class="form-control" id="for-rejectionreason" name="rejectionreason"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label" for="sendmail">Send mail (Unable to reach):</label>
                            <div class="col-sm-7">
                                <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                    <input type="checkbox" class="custom-control-input" id="sendmail" name="sendmail">
                                    <label class="custom-control-label" for="sendmail"></label>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="form-group col-md-8">
                <button type="submit" class="btn btn-primary">Reject</button>
                <a href="../{{$claim->cl_id}}/edit"><button type="button" class="btn btn-primary">Back</button></a>
            </div>
        </div>

        </form>
    </div>


@endsection




