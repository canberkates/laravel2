@extends('layouts.backend')

@section('content')
    <div class="content">

        <div class="col-md-6 block block-rounded block-bordered">
            <form method="post" action="{{action('CustomerQuestionController@resendLead', $ktrecuqu->ktrecuqu_id)}}">
                @csrf
                <input name="_method" type="hidden" value="post">
                <div class="block-content">

                <h2 class="content-heading pt-0">Resend lead</h2>
                    <p>Are you sure you want to resend the request of <strong>{{$ktrecuqu->request->re_full_name}}</strong> to <strong>{{$ktrecuqu->customer->cu_company_name_business}}</strong></p>

                <div class="row">
                    <div class="form-group col-md-2">
                        <button type="submit" class="btn btn-primary">Resend</button>
                    </div>
                </div>
            </div>
            </form>
        </div>

    </div>
@endsection




