@extends('layouts.backend')

@section('content')
    <div class="content">

        <div class="col-md-6 block block-rounded block-bordered">
            <form method="post" action="{{action('CustomerQuestionController@freeLead', $id)}}">
                @csrf
                <input name="_method" type="hidden" value="post">
                <div class="block-content">

                    <h2 class="content-heading pt-0">Request</h2>

                    <div class="form-group row">
                        <div class="col-sm-1"></div>
                        <label class="col-sm-3 col-form-label" for="description">Requester:</label>
                        <div class="col-sm-8">
                            <input type="text" disabled class="form-control" name="description"
                                   value="{{$ktrecuqu->request->re_full_name}}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-1"></div>
                        <label class="col-sm-3 col-form-label" for="description">Customer:</label>
                        <div class="col-sm-8">
                            <input type="text" disabled class="form-control" name="description"
                                   value="{{$ktrecuqu->customer->cu_company_name_business}}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-1"></div>
                        <label class="col-sm-3 col-form-label" for="description">Requester:</label>
                        <div class="col-sm-8">
                            <input type="text" disabled class="form-control" name="description"
                                   value="{{$ktrecuqu->question->qu_name}}">
                        </div>
                    </div>

                    <h2 class="content-heading pt-0">Free</h2>

                    <div class="form-group row">
                        <div class="col-md-1"></div>
                        <label class="col-md-3 col-form-label">Free:</label>
                        <div class="col-md-8">
                            <select data-placeholder="Select some options..." class="js-select2 form-control"
                                    name="free" style="width:100%;">
                                @foreach($yesno as $id => $answer)
                                    <option value="{{$id}}">{{$answer}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <h2 class="content-heading pt-0"></h2>

                    <div class="row">
                        <div class="form-group col-md-2">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

    </div>
@endsection




