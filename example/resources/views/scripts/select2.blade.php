@pushonce('styles:select2')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css">
@endpushonce

@pushonce('scripts:select2')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
@endpushonce

@push('scripts')
    <script>jQuery(function(){ Dashmix.helpers('select2'); });</script>
@endpush
