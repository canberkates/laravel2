@pushonce( 'styles:telinput' )
    <link rel="stylesheet" href="{{ asset('js/plugins/telinput/css/intlTelInput.css') }}">
@endpushonce

@pushonce( 'scripts:telinput' )
    <script src="{{ asset('js/plugins/telinput/js/utils.js') }}"></script>
    <script src="{{ asset('js/plugins/telinput/js/intlTelInput.js') }}"></script>
@endpushonce
