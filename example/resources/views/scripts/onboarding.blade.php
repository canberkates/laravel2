@pushonce( 'styles:onboarding' )
    <link rel="stylesheet" href="{{ asset('js/plugins/slick-carousel/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/slick-carousel/slick-theme.css') }}">
@endpushonce

@pushonce( 'scripts:onboarding' )
    <script src="{{ asset('js/plugins/slick-carousel/slick.min.js') }}"></script>
    <script src="{{ asset('js/pages/be_comp_onboarding.min.js') }}"></script>
@endpushonce
