@pushonce( 'styles:dropzone' )
    <link rel="stylesheet" href="{{ asset('js/plugins/dropzone/dist/min/dropzone.min.css') }}">
@endpushonce

@pushonce( 'scripts:dropzone' )
    <script src="{{ asset('js/plugins/dropzone/dropzone.min.js') }}"></script>
@endpushonce
