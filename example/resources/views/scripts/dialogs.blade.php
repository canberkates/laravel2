@pushonce( 'styles:dialogs' )
    <link rel="stylesheet" href="{{ asset('js/plugins/sweetalert2/sweetalert2.min.css') }}">
@endpushonce

@pushonce( 'scripts:dialogs' )
    <script src="{{ asset('js/plugins/es6-promise/es6-promise.auto.min.js') }}"></script>
    <script src="{{ asset('js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('js/plugins/dialogs/dialogs-helper.js') }}"></script>
@endpushonce
