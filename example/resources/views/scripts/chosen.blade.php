@pushonce('styles:select2')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.min.css">
@endpushonce

@pushonce('scripts:select2')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.jquery.min.js"></script>
@endpushonce

@push('scripts')
    <script> $(".chosen-select").chosen({
            search_contains: true,
            allow_single_deselect: true
        }); </script>
@endpush
