@push( 'scripts' )
    <script>



        function regionFinder() {

            $.each( ['from','to'], function( $index, $from_to ) {


                // Set default
                var $regionFinder = true;

                var $zipcode = $( 'input[name="zipcode_' + $from_to + '"]' );
                var $country = $( 'input[name="country_' + $from_to + '"]' );
                var region = $("#for-region_" + $from_to);

                console.log(region.val());

                if( $country.val() === 'FR' ) {

                    $regionFinder = false;
                }

                if($zipcode.val() !== '' && region.val()) {
                    $regionFinder = false;
                }

                $.ajax({
                    type: "GET",
                    url: '{{ url('/ajax/findsuggestedregion') }}',
                    data: {
                        "from_to": $from_to,
                        "destination_type": $("input[name=destination_type]").data("id"),
                        "city": $("input[name=city_" + $from_to + "]").val(),
                        "country": $("select[name=country_" + $from_to + "]").val()
                    },

                    success: function(data){

                        var select = $("#region_suggestion_" + $from_to);
                        var obj = jQuery.parseJSON(data);
                        var total_matches = 0;
                        var counter = 1;
                        var html = '';

                        $.each(obj, function(key,value) {
                            total_matches = total_matches + parseInt(value.count);
                        });

                        var region_id = 0;

                        $.each(obj, function(key,value) {

                            if( counter === 1 ) {

                                if( $from_to === "from" ) {

                                    region_id = value.re_reg_id_from;
                                }

                                if( $from_to === "to" ) {

                                    region_id = value.re_reg_id_to;
                                }
                            }
                            //calculate the percentages, after that, make the list :)
                            if (counter <= 3)
                            {
                                var percentage = 100 / total_matches * value.count;

                                if(value.reg_name == "") {
                                    var name = value.reg_parent;
                                }else{
                                    var name = value.reg_name;
                                }

                                if (percentage.toFixed(0) < 90 && counter === 1 ) {
                                    html +=  '<div class="alert alert-danger" role="alert"><p class="mb-0">#'+counter+'. '+ name +' Matches: '+ value.count +' - Chance: '+ percentage.toFixed(0)+'%</p></div>';
                                }
                                else {
                                    html +=  '<div class="alert" role="alert"><p class="mb-0">#'+counter+'. '+ name +' Matches: '+ value.count +' - Chance: '+ percentage.toFixed(0)+'%</p></div>';
                                }

                            }
                            counter++;
                        });

                        select.before( html );

                        if( $regionFinder === true) {

                            if (region_id >= 1) {

                                var select_region = $("select[name=region_" + $from_to + "]");
                                select_region.val(region_id).trigger( 'change' );
                            }
                        }
                    }
                });
            });
        }
    </script>

@endpush
