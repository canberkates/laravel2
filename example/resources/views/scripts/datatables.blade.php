@pushonce( 'styles:datatables' )
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.css') }}">
@endpushonce

@pushonce( 'scripts:datatables' )
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.hideEmptyColumns.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/jszip.js') }}"></script>
    <script src="{{ asset('js/pages/be_tables_datatables.js') }}"></script>
    <script type="text/javascript">

	var $detailRows = [];
	var $plusIcon = 'fa-plus';
	var $minIcon = 'fa-minus';

	(function(){

		var currency_regex = /(\€|\£|\$)+\s([0-9-\.,]+)/;
        var range_regex = /^([0-9]+)(\-[0-9]+|\+)$/;

        jQuery.fn.dataTableExt.aTypes.unshift(
            function(data){
                if(data.match(currency_regex)){
                    return "currency";
                }
                else if(data.match(range_regex)){
                    return "range";
                }
                else{
                    return null;
                }
            }
        );

        jQuery.extend(jQuery.fn.dataTableExt.oSort, {
            "currency-pre": function(a){
                return parseFloat(currency_regex.exec(a)[2].replace(/\./g, "").replace(/[^-0-9\.]/g, ""));
            },
            "range-pre": function(a){
                return parseFloat(range_regex.exec(a)[1]);
            }
        });

        function removeAccents ( data ) {
            return data
                .replace( /έ/g, 'ε' )
                .replace( /[ύϋΰ]/g, 'υ' )
                .replace( /ό/g, 'ο' )
                .replace( /ώ/g, 'ω' )
                .replace( /ά/g, 'α' )
                .replace( /[ίϊΐ]/g, 'ι' )
                .replace( /ή/g, 'η' )
                .replace( /\n/g, ' ' )
                .replace( /á/g, 'a' )
                .replace( /é/g, 'e' )
                .replace( /í/g, 'i' )
                .replace( /ó/g, 'o' )
                .replace( /ú/g, 'u' )
                .replace( /ê/g, 'e' )
                .replace( /î/g, 'i' )
                .replace( /ô/g, 'o' )
                .replace( /è/g, 'e' )
                .replace( /ï/g, 'i' )
                .replace( /ü/g, 'u' )
                .replace( /ã/g, 'a' )
                .replace( /õ/g, 'o' )
                .replace( /ç/g, 'c' )
                .replace( /ì/g, 'i' );
        }

        var searchType = jQuery.fn.DataTable.ext.type.search;

        searchType.string = function ( data ) {
            return ! data ?
                '' :
                typeof data === 'string' ?
                    removeAccents( data ) :
                    data;
        };

        searchType.html = function ( data ) {
            return ! data ?
                '' :
                typeof data === 'string' ?
                    removeAccents( data.replace( /<.*?>/g, '' ) ) :
                    data;
        };

    }());


	$( 'body' ).on( 'click', '.details_show_blank', function() {

		var $table = $( this ).parents( '.table' ).DataTable();

		// Get table row
		var $tr = $( this ).parents( 'tr' );

		// Get row
		var $row = $table.row( $tr );

		// Get details
		var $details = $( this ).data( 'details' );

		// Get details rows
		var $idx = $.inArray( $tr.attr( 'id' ), $detailRows );

		// Get icon
		var $icon = $( this ).find( 'i' );

		// If the row is shown
		if ( $row.child.isShown() ) {

			$tr.removeClass( 'details' );
			$row.child.hide();

			// Remove from the 'open' array
			$detailRows.splice( $idx, 1 );

			// Switch the icon
			$icon.removeClass( $minIcon ).addClass( $plusIcon );
		}
		else {

			$tr.addClass( 'details' );
			$row.child( $details ).show();
			$icon.removeClass( $plusIcon ).addClass( $minIcon );

			// Add to the 'open' array
			if ( $idx === -1 ) {

				$detailRows.push( $tr.attr('id') );
			}
		}

		$table.on( 'draw', function () {
			$.each( $detailRows, function ( i, id ) {
				$('#'+id+' td.details-control').trigger( 'click' );
			} );
		} );
	});

    </script>
@endpushonce
