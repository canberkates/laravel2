@pushonce( 'styles:editor' )
    <link rel="stylesheet" href="{{ asset('js/plugins/summernote/summernote-lite.css') }}">
    <link rel="stylesheet" href="{{ asset( 'js/plugins/simplemde/simplemde.min.css' ) }}">
@endpushonce

@pushonce( 'scripts:editor' )
    <script src="{{ asset('js/plugins/simplemde/simplemde.min.js') }}"></script>
    <script src="{{ asset('js/plugins/summernote/summernote-lite.min.js') }}"></script>
    <script src="{{ asset('js/plugins/ckeditor/ckeditor.js') }}"></script>
    <script>jQuery(function(){ Dashmix.helpers(['summernote', 'ckeditor','simplemde']); });</script>
@endpushonce
