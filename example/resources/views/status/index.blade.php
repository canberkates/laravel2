@extends('layouts.backend')


@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Status</h1>
            </div>
       </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="row">
            <div class="col-md-6">
            Last SEA lead received: <span id="last_own"></span> on <span id="last_own_source"></span> <br>
            Last Sirelo lead received: <span id="last_sirelo"></span> on <span id="last_sirelo_source"></span> <br>
            Last affiliate lead received: <span id="last_affiliate"></span> on <span id="last_affiliate_source"></span> <br>
            Last lead matched: <span id="last_matched"></span> by <span id="last_matched_by"></span> <br> <br>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">



                <table id="timestamps_websites"
                       class="table table-bordered table-striped ">
                    <thead>
                        <tr>
                            <th>Website</th>
                            <th>Last lead</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>

                </table>

            </div>
            <div class="col-md-6">

                <table id="timestamps_affiliates"
                       class="table table-bordered table-striped ">
                    <thead>
                        <tr>
                            <th>Affiliate</th>
                            <th>Last lead</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>

                </table>

            </div>
        </div>

    </div>
    <!-- END Page Content -->
@endsection
@section('js_after')
    <script>

        jQuery.ajax({
            url: "/getlastreceivedandmatchedlead",
            method: 'get',
            async: true,
            success: function ($result) {
                json = JSON.parse($result);

                $("#last_own").text(json['last_own']['re_timestamp'])
                $("#last_own_source").text(json['last_own']['we_website'])

                $("#last_sirelo").text(json['last_sirelo']['re_timestamp'])
                $("#last_sirelo_source").text(json['last_sirelo']['we_website'])

                $("#last_affiliate").text(json['last_affiliate']['re_timestamp'])
                $("#last_affiliate_source").text(json['last_affiliate']['afpafo_name'])

                $("#last_matched").text(json['last_matched']['ktrecupo_timestamp'])
                $("#last_matched_by").text(json['last_matched']['us_name'])
            }
        });

        $('#timestamps_websites').DataTable({
            "Processing": true,
            "ajax": {
                "url": "/statusajaxcall",
                "dataSrc": "data"
            },
            "columns": [
                { "data": "Website" },
                { "data": "Last lead" },
            ]
        });

        $('#timestamps_affiliates').DataTable({
            "Processing": true,
            "ajax": {
                "url": "/getlastaffiliateleads",
                "dataSrc": "data"
            },
            "columns": [
                { "data": "Affiliate" },
                { "data": "Last lead" },
            ]
        });


    </script>
    @include('scripts.datatables')

@endsection
