@extends('layouts.backend')
@include('scripts.datepicker')
@include('scripts.editor')
@section('content')


    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Process a call with {{$call->customer->cu_company_name_business}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/calendar"> Calendar </a></li>
                        <li class="breadcrumb-item active" aria-current="page">Process call</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->


    <div class="content">

        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        {{$error}}<br>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="block block-rounded block-bordered">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Call information</h3>
                    </div>
                    <div class="block-content">
                        <form class="mb-5" method="post" action="{{action('CalendarController@update', $call->plca_id)}}">
                            @csrf
                            <input name="_method" type="hidden" value="PATCH">

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="date">Planned datetime:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="js-datepicker form-control" id="example-datepicker3" name="date" data-week-start="1" disabled data-autoclose="true" data-today-highlight="true" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd" autocomplete="off" value="{{$call->plca_planned_date}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="date">Reason:</label>
                                <div class="col-sm-7">
                                    <input class="form-control" type="text" name="reason"  disabled autocomplete="off" value="{{$planned_call_reason}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="date">Actual call datetime:</label>
                                <div class="col-sm-7">
                                    <input autocomplete="off" name="call_datetime" class="form-control" id="datetimepicker" type="text" value="{{date('Y-m-d H:m')}}" />
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="employee">Employee:</label>
                                <div class="col-sm-7">
                                    <select type="text" class="form-control" name="employee">
                                        <option value=""></option>
                                        @foreach($users as $id => $user)
                                            <option @if ($id == \Illuminate\Support\Facades\Auth::id()) selected @endif value="{{$id}}">{{$user}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="employee">Status of the call:</label>
                                <div class="col-sm-7">
                                    <select type="text" class="form-control" name="status" id="status">
                                        @foreach($statuses as $id => $status)
                                            @if($loop->first) @continue @endif
                                            <option value="{{$id}}">{{$status}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div id="reschedule_div" style="display: none;">
                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="date">Reschedule this call:</label>
                                    <div class="col-sm-7">
                                        <input autocomplete="off" name="rescheduled_call_datetime" class="form-control" id="datetimepicker2" type="text" value="{{date('Y-m-d H:m')}}" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="date">Person spoken to:</label>
                                <div class="col-sm-7">
                                    <input class="form-control" type="text" name="person_spoken_to" autocomplete="off" value="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="call_remark">Call Remark:</label>
                                <div class="col-sm-7">
                                    <textarea rows="4" type="text" class="form-control" name="call_remark"></textarea>
                                </div>
                            </div>

                            <h2 class="content-heading pt-0"></h2>

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Create</button>
                                    <a class="btn btn-primary" href="../">
                                        Back
                                    </a>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@push('scripts')
    <script>
        $.datetimepicker.setDateFormatter('moment');
        $('#datetimepicker').datetimepicker({
            step: 10, //will change increments to 15m, default is 1m
            format:'YYYY-MM-DD HH:mm',
        });
        $('#datetimepicker2').datetimepicker({
            step: 10, //will change increments to 15m, default is 1m
            format:'YYYY-MM-DD HH:mm',
        });

        $('#datetimepicker2').datetimepicker({
            step: 10, //will change increments to 15m, default is 1m
            format:'YYYY-MM-DD HH:mm',
        });


        $( "#status" ).change(function() {

            if($(this).val() == 2){
                $('#reschedule_div').show(250);
            }else{
                $('#reschedule_div').hide(250);
            }

        });

    </script>
@endpush


