@extends('layouts.backend')

@include( 'scripts.datatables' )
@include( 'scripts.dialogs' )
@include('scripts.datepicker')


@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Plan calls</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/calendar">Calendar</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Plan calls</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">

        <div class="block-header block-header-default">
            <h3 class="block-title">
                <a href="{{ url('calendar')}}">
                    <button data-toggle="click-ripple" class="btn btn-primary">Back to my calendar</button>
                </a>
            </h3>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="block block-rounded block-bordered">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Refresh to get the next 100 unplanned companies!</h3>
                    </div>
                    @foreach($customers as $customer)
                        <div class="block-content">
                            <form method="post" action="{{action('CustomersController@update', $customer->cu_id)}}">
                                @csrf
                                <input name="_method" type="hidden" value="PATCH">
                                <input name="form_name" type="hidden" value="Fields">
                                <div class="row" @if(!$loop->last) style="margin-bottom: -45px;" @endif>
                                    <div class="col-md-12">
                                        <div class="form-group row">

                                            <label class="col-sm-4 col-form-label">{{$customer->cu_company_name_business}}</label>
                                            <label class="col-sm-2 col-form-label">{{$countries[$customer->cu_co_code]}}</label>
                                            <div class="col-sm-2">
                                                <input autocomplete="off" name="call_datetime" class="form-control call_datetime"
                                                       id="datetime_{{$customer->cu_id}}" type="text" value="{{ date('Y-m-d H:m', strtotime("+60 days"))}}" />
                                            </div>
                                            <div class="col-sm-2">
                                                <select type="text" class="js-select2 form-control"
                                                        name="call_reason" id="user_{{$customer->cu_id}}">
                                                    @foreach($employees as $employee)
                                                        <option value="{{$employee->us_id}}" @if($customer->moverdata->moda_owner == null && $employee->us_id == 1778) selected @elseif($employee->us_id == $customer->moverdata->moda_owner) selected @endif>{{$employee->us_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-md-2">
                                                <button type="button" id="button_{{$customer->cu_id}}"
                                                       name="update" class="btn btn-primary" data-id="{{$customer->cu_id}}">
                                                    Update
                                                </button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </form>
                        </div>

                    @endforeach

                </div>
            </div>
        </div>

        <!-- END Your Block -->
    </div>
    <!-- END Page Content -->
@endsection
@push('scripts')
    <script>
        $.datetimepicker.setDateFormatter('moment');
        $('input[name=call_datetime]').datetimepicker({
            step: 10, //will change increments to 15m, default is 1m
            format:'YYYY-MM-DD HH:mm',
        });

        $("button[name=update]").click(function(){
            $id = $(this).data("id");
            $date = $("#datetime_"+$id).val();
            $user = $("#user_"+$id).val();

            jQuery.ajax({
                url: "ajax/plan_call",
                method: 'get',
                data: {  customer : $id, user : $user, date : $date},
                success: function() {
                }
            });

            $(this).text("Call planned 👌")
            $(this).attr("disabled", "disabled");
        });
    </script>
@endpush


