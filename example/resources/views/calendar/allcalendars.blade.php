@extends('layouts.backend')

@include( 'scripts.datatables' )
@include( 'scripts.dialogs' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Full calls calendar</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Calls calendar</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">

        <div class="block-header block-header-default">
            <h3 class="block-title">
                <a href="{{ url('calendar')}}"><button data-toggle="click-ripple" class="btn btn-primary">Back to my calendar</button></a>
            </h3>
        </div>

        <ul class="nav nav-tabs nav-tabs-alt" data-toggle="tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" href="#btabs-alt-static-open">Open</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#btabs-alt-static-future">Future</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#btabs-alt-static-processed">Processed</a>
            </li>
        </ul>
        <div class="block-content tab-content">
            <div class="tab-pane active" id="btabs-alt-static-open" role="tabpanel">
                <!-- Your Block -->
                <div class="block block-rounded block-bordered">
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Reports
                </h3>
            </div>
            <div class="block-content block-content-full">
                <table data-order='[[3, "asc"]]'
                       class="table table-bordered table-striped table-vcenter js-dataTable-full">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>User</th>
                        <th>Company name (ID)</th>
                        <th>Date & time</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>Reason</th>
                        <th>Explanation</th>
                        <th>Edit</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($open_calls as $call)
                        <tr>
                            <td>{{$call->plca_id}}</td>
                            <td>{{$call->user->us_name}}</td>
                            <td>{{$call->customer->cu_company_name_business}} ({{$call->customer->cu_id}})</td>
                            <td>{{$call->plca_planned_date}}</td>
                            <td>{{$planned_call_types[$call->plca_type]}}</td>
                            <td>{{$planned_call_status[$call->plca_status]}}</td>
                            <td>{{$planned_call_reasons[$call->plca_reason]}}</td>
                            <td>{{$call->plca_reason_extra}}</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <a href="{{ url("/calendar/" . $call->plca_id . "/edit")}}"
                                       class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="left">
                                        <i class="fa fa-arrow-circle-right" style="color:white;"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
            </div>
            <div class="tab-pane" id="btabs-alt-static-future" role="tabpanel">
                <!-- Your Block -->
                <div class="block block-rounded block-bordered">
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Reports
                </h3>
            </div>
            <div class="block-content block-content-full">
                <table data-order='[[3, "asc"]]'
                       class="table table-bordered table-striped table-vcenter js-dataTable-full">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>User</th>
                        <th>Company name (ID)</th>
                        <th>Date & time</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>Reason</th>
                        <th>Explanation</th>
                        <th>Edit</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($future_calls as $call)
                        <tr>
                            <td>{{$call->plca_id}}</td>
                            <td>{{$call->user->us_name}}</td>
                            <td>{{$call->customer->cu_company_name_business}} ({{$call->customer->cu_id}})</td>
                            <td>{{$call->plca_planned_date}}</td>
                            <td>{{$planned_call_types[$call->plca_type]}}</td>
                            <td>{{$planned_call_status[$call->plca_status]}}</td>
                            <td>{{$planned_call_reasons[$call->plca_reason]}}</td>
                            <td>{{$call->plca_reason_extra}}</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <a href="{{ url("/calendar/" . $call->plca_id . "/edit")}}"
                                       class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="left">
                                        <i class="fa fa-arrow-circle-right" style="color:white;"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
            </div>
            <div class="tab-pane" id="btabs-alt-static-processed" role="tabpanel">
                <!-- Your Block -->
                <div class="block block-rounded block-bordered">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">
                            Reports
                        </h3>
                    </div>
                    <div class="block-content block-content-full">
                        <table data-order='[[3, "asc"]]'
                               class="table table-bordered table-striped table-vcenter js-dataTable-full">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>User</th>
                                <th>Company name (ID)</th>
                                <th>Date & time</th>
                                <th>Type</th>
                                <th>Status</th>
                                <th>Reason</th>
                                <th>Explanation</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($processed_calls as $call)
                                <tr>
                                    <td>{{$call->plca_id}}</td>
                                    <td>{{$call->user->us_name}}</td>
                                    <td>{{$call->customer->cu_company_name_business}} ({{$call->customer->cu_id}})</td>
                                    <td>{{$call->plca_planned_date}}</td>
                                    <td>{{$planned_call_types[$call->plca_type]}}</td>
                                    <td>{{$planned_call_status[$call->plca_status]}}</td>
                                    <td>{{$planned_call_reasons[$call->plca_reason]}}</td>
                                    <td>{{$call->plca_reason_extra}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Your Block -->
    </div>
    <!-- END Page Content -->
@endsection


