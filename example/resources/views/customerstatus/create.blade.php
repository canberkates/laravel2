@extends('layouts.backend')

@include('scripts.datepicker')

@pushonce( 'scripts:customer-status-add' )
    <script src="{{ asset('/js/custom/customer/customer_status_add.js') }}"></script>
@endpushonce

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Add a Status to {{$customer->cu_company_name_business}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../../">@if ($customer->cu_type == 1) {{"Customers"}} @elseif($customer->cu_type == 6) {{"Resellers"}} @elseif($customer->cu_type == 2) {{"Service Providers"}} @else {{"Affiliate Partners"}} @endif</a></li>
                        <li class="breadcrumb-item"><a href="../edit">{{$customer->cu_company_name_business}}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Add status</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="block block-rounded block-bordered">
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    {{$error}}<br>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="block-header block-header-default">
                        <h3 class="block-title">General</h3>
                    </div>
                    <div class="block-content">
                        <form class="mb-5" method="post" action="{{action('CustomerStatusController@store')}}">
                            @csrf
                            <input name="_method" type="hidden" value="post">
                            <input name="customer_id" type="hidden" value="{{$customer->cu_id}}">


                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="date">Date:</label>
                                <div class="col-sm-3">
                                    <input autocomplete="off" type="text" value="{{date("Y-m-d")}}" class="js-datepicker form-control" id="example-datepicker3" name="date" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="employee">Employee:</label>
                                <div class="col-sm-3">
                                    <select type="text" class="form-control" name="employee">
                                        <option value=""></option>
                                        @foreach($employees as $employee)
                                            <option @if(Auth::id() == $employee->us_id) selected @endif @if (Request::old('employee') == $employee->us_id) selected @endif value="{{$employee->us_id}}">{{$employee->us_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="status">Status:</label>
                                <div class="col-sm-3">
                                    <select type="text" class="form-control" name="status">
                                        <option value=""></option>
                                        @foreach($statuses as $id => $status)
                                            <option @if (Request::old('status') == $id) selected @endif value="{{$id}}">{{$status}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div id="reason" style="display:none;">
                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="reason">Reason:</label>
                                    <div class="col-sm-3">
                                        <select type="text" class="form-control" name="reason">
                                            <option value=""></option>
                                            @foreach($reasons as $id => $reason)
                                                <option @if (Request::old('reason') == $id) selected @endif value="{{$id}}">{{$reason}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div id="cancellation_reason" style="display:none;">
                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="cancellation_reason">Reason:</label>
                                    <div class="col-sm-3">
                                        <select type="text" class="form-control" name="cancellation_reason">
                                            <option value=""></option>
                                            @foreach($cancellation_reasons as $id => $reason)
                                                <option @if (Request::old('cancellation_reason') == $id) selected @endif value="{{$id}}">{{$reason}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div id="turnover_netto">
                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="turnover_netto">Net revenues (€):</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" name="turnover_netto"
                                               value="{{Request::old('reason')}}">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="number_of_monthly_leads">Number of leads (per month):</label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control" name="number_of_monthly_leads"
                                           value="{{Request::old('number_of_monthly_leads')}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="capping_number_of_leads">Cappings (number of leads):</label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control" name="capping_number_of_leads"
                                           value="{{Request::old('capping_number_of_leads')}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="price_per_lead">Price per lead:</label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control" name="price_per_lead"
                                           value="{{Request::old('price_per_lead')}}">
                                </div>
                                <label class="col-sm-3 col-form-label" for="price_per_lead">Make sure to add a currency symbol! £ € $</label>
                            </div>

                            <div id="previous_leads">
                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="previous_number_of_monthly_leads">Previous # of leads (per month):</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" name="previous_number_of_monthly_leads"
                                               value="{{Request::old('previous_number_of_monthly_leads')}}">
                                    </div>
                                </div>
                            </div>

                            <div id="previous_capping">
                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="previous_capping_number_of_leads">Previous capping (# of leads):</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" name="previous_capping_number_of_leads"
                                               value="{{Request::old('previous_capping_number_of_leads')}}">
                                    </div>
                                </div>
                            </div>

                            <div id="previous_price">
                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="previous_price_per_lead">Previous price per lead:</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" name="previous_price_per_lead"
                                               value="{{Request::old('previous_price_per_lead')}}">
                                    </div>
                                </div>
                            </div>

                            <div id="source">
                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="source">Source:</label>
                                    <div class="col-sm-3">
                                        <select type="text" class="form-control" name="source">
                                            <option value=""></option>
                                            @foreach($sources as $id => $source)
                                                <option @if (Request::old('source') == $id) selected @endif value="{{$id}}">{{$source}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div id="lifetime">
                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="lifetime_in_months">Lifetime (in months):</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" name="lifetime_in_months" value="{{Request::old('lifetime_in_months')}}">
                                    </div>
                                </div>
                            </div>


                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="customer_status_remark">Remark:</label>
                                <div class="col-sm-7">
                                    <textarea rows="4" type="text" class="form-control" name="customer_status_remark">{{Request::old('customer_status_remark')}}</textarea>
                                </div>
                            </div>

                            <h2 class="content-heading pt-0"></h2>

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                    <a class="btn btn-primary" href="../edit">
                                        Back
                                    </a>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection




