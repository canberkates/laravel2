@extends('layouts.backend')

@include('scripts.datepicker')

@section('content')
    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="block block-rounded block-bordered">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Add a Status to {{$customer->cu_company_name_business}}</h3>
                    </div>
                    <div class="block-content">
                        <form class="mb-5" method="post" action="{{action('CustomerStatusController@store')}}">
                            @csrf
                            <input name="_method" type="hidden" value="post">
                            <input name="customer_id" type="hidden" value="{{$customer->cu_id}}">


                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="example-hf-email">Date:</label>
                                <div class="col-sm-7">
                                    <input autocomplete="off" type="text" class="js-datepicker form-control" id="example-datepicker3" name="date" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="example-hf-email">Status:</label>
                                <div class="col-sm-7">
                                    <select type="text" class="form-control" name="status">
                                        <option value=""></option>
                                        @foreach($statuses as $id => $status)
                                            <option value="{{$id}}">{{$status}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="example-hf-email">Net revenues (€):</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="net_revenues"
                                           value="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="example-hf-email">Number of leads (per month):</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="number_of_monthly_leads"
                                           value="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="example-hf-email">Cappings (number of leads):</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="capping_number_of_leads"
                                           value="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="example-hf-email">Price per lead:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="price_per_lead"
                                           value="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="example-hf-email">Previous # of leads (per month):</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="previous_number_of_monthly_leads"
                                           value="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="example-hf-email">Previous capping (# of leads):</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="previous_capping_number_of_leads"
                                           value="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="example-hf-email">Previous price per lead:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="previous_price_per_lead"
                                           value="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="example-hf-email">Reason:</label>
                                <div class="col-sm-7">
                                    <select type="text" class="form-control" name="reason">
                                        <option value=""></option>
                                        @foreach($reasons as $id => $reason)
                                            <option value="{{$id}}">{{$reason}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="example-hf-email">Lifetime (in months):</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="lifetime_in_months" value="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="example-hf-email">Remark:</label>
                                <div class="col-sm-7">
                                    <textarea rows="4" type="text" class="form-control" name="customer_status_remark"></textarea>
                                </div>
                            </div>

                            <h2 class="content-heading pt-0"></h2>

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection




