@extends('layouts.backend')

@include( 'scripts.charts' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Dashboard</h1>
            </div>
       </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        @if( count( $data['birthdays'] ) > 0 )
            <div class="row">
                <div class="col-xl-6">
                    <div class="block block-rounded block-bordered block-birthday block-themed">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">PARTY TIME 🎉</h3>
                        </div>
                        <div class="block-content">
                            <p>
                                    @foreach( $data['birthdays'] as $birthday )
                                        {{ $birthday->us_name }} turned {{ ( $birthday->age <= 30 ? $birthday->age : '30+' ) }} years old today!

                                        <span style="font-size: 25px;">
                                        @if( $birthday->us_id == 4559 )
                                            🚴‍♂️
                                        @elseif( $birthday->us_id == 4186 )
                                            🍺👌
                                        @elseif( $birthday->us_id == 3653 )
                                            💪😆
                                        @elseif( $birthday->us_id == 1427 )
                                            🤓🤡
                                        @endif
                                        </span>
                                        <br>
                                    @endforeach
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6">
                    <div class="block block-rounded block-bordered block-themed">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Requests</h3>
                        </div>
                        <div class="block-content block-content-full text-center">
                            <div class="py-3">
                                <!-- Pie Chart Container -->
                                <canvas id='requests-chart' class="js-chartjs-pie"></canvas>
                                @push( 'scripts' )
                                    <script>
                                        new Chart( document.getElementById( 'requests-chart' ), {
                                            type: 'pie',
                                            data: {
                                                labels: ['Open', 'Matched', 'Rejected'],
                                                datasets: [{
                                                    data: [@json($data['requests'])][0],
                                                    backgroundColor: [
                                                        'rgba(22, 211, 19, 0.7)',
                                                        'rgba(54, 162, 235, 0.7)',
                                                        'rgba(211, 60, 19, 0.7)',
                                                    ],
                                                    borderColor: [
                                                        'rgba(22, 211, 19, 1)',
                                                        'rgba(54, 162, 235, 1)',
                                                        'rgba(211, 60, 19, 1)',
                                                    ],
                                                    borderWidth: 3
                                                }]
                                            },
                                        });
                                    </script>
                                @endpush
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @can('requests')
        <div class="row">
            <div class="col-md-12 col-xl-12 invisible" data-toggle="appear">
                <div class="block block-rounded block-themed" >
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Number of requests received this year compared to last year <small style="float:right;">last update {{$data['dashboard_timestamp']}}</small></h3>
                    </div>
                    <div class="block-content d-flex align-items-center justify-content-between">
                        <div class="block block-rounded block-bordered">
                            <canvas width="1200"  height="300" id="request_bar_chart" class="js-chartjs-bars" style="max-width: 100%;"></canvas>
                            @push( 'scripts' )
                                <script>
                                    new Chart( document.getElementById('request_bar_chart'), {
                                        type: 'bar',
                                        data: {
                                            labels: [@json($data['labels'])][0],
                                            datasets: [
                                                    {
                                                    label: 'Previous year',
                                                    data: [@json($data['request_bar_previous_year'])][0],
                                                    backgroundColor: [
                                                        'rgba(54, 162, 235, 0.3)',
                                                        'rgba(54, 162, 235, 0.3)',
                                                        'rgba(54, 162, 235, 0.3)',
                                                        'rgba(54, 162, 235, 0.3)',
                                                        'rgba(54, 162, 235, 0.3)',
                                                        'rgba(54, 162, 235, 0.3)',
                                                        'rgba(54, 162, 235, 0.3)',
                                                        'rgba(54, 162, 235, 0.3)',
                                                        'rgba(54, 162, 235, 0.3)',
                                                        'rgba(54, 162, 235, 0.3)',
                                                        'rgba(54, 162, 235, 0.3)',
                                                        'rgba(54, 162, 235, 0.3)',
                                                    ],
                                                    borderColor: [
                                                        'rgba(54, 162, 235, 1)',
                                                        'rgba(54, 162, 235, 1)',
                                                        'rgba(54, 162, 235, 1)',
                                                        'rgba(54, 162, 235, 1)',
                                                        'rgba(54, 162, 235, 1)',
                                                        'rgba(54, 162, 235, 1)',
                                                        'rgba(54, 162, 235, 1)',
                                                        'rgba(54, 162, 235, 1)',
                                                        'rgba(54, 162, 235, 1)',
                                                        'rgba(54, 162, 235, 1)',
                                                        'rgba(54, 162, 235, 1)',
                                                        'rgba(54, 162, 235, 1)',
                                                    ],
                                                    borderWidth: 1

                                                },
                                                {
                                                    label: 'Current year',
                                                    data: [@json($data['request_bar_current_year'])][0],
                                                    backgroundColor: [
                                                        'rgba(255, 99, 132, 0.3)',
                                                        'rgba(255, 99, 132, 0.3)',
                                                        'rgba(255, 99, 132, 0.3)',
                                                        'rgba(255, 99, 132, 0.3)',
                                                        'rgba(255, 99, 132, 0.3)',
                                                        'rgba(255, 99, 132, 0.3)',
                                                        'rgba(255, 99, 132, 0.3)',
                                                        'rgba(255, 99, 132, 0.3)',
                                                        'rgba(255, 99, 132, 0.3)',
                                                        'rgba(255, 99, 132, 0.3)',
                                                        'rgba(255, 99, 132, 0.3)',
                                                        'rgba(255, 99, 132, 0.3)',
                                                    ],
                                                    borderColor: [
                                                        'rgba(255, 99, 132, 1)',
                                                        'rgba(255, 99, 132, 1)',
                                                        'rgba(255, 99, 132, 1)',
                                                        'rgba(255, 99, 132, 1)',
                                                        'rgba(255, 99, 132, 1)',
                                                        'rgba(255, 99, 132, 1)',
                                                        'rgba(255, 99, 132, 1)',
                                                        'rgba(255, 99, 132, 1)',
                                                        'rgba(255, 99, 132, 1)',
                                                        'rgba(255, 99, 132, 1)',
                                                        'rgba(255, 99, 132, 1)',
                                                        'rgba(255, 99, 132, 1)',
                                                    ],
                                                    borderWidth: 1

                                                },
                                            ]
                                        },
                                        options: {
                                            scales: {
                                                yAxes: [{

                                                    ticks: {
                                                        beginAtZero: true
                                                    }
                                                }],
                                                xAxes: [{
                                                    barPercentage: 0.7,
                                                }],
                                            }
                                        }
                                    });
                                </script>
                            @endpush
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endcan
        @can('customers')
            <div class="row">
                <div class="col-md-12 col-xl-12 invisible" data-toggle="appear">
                    <div class="block block-rounded block-themed" >
                        <div class="block-header block-header-default">
                            <h3 class="block-title">
                                Since February 2020, we can put customers on not operational. Below you can see how many times we use this option per month.
                            </h3>
                        </div>
                        <div class="block-content d-flex align-items-center justify-content-between">
                            <div class="block block-rounded block-bordered">
                                <canvas width="1200"  height="300" id="not_operational_chart" class="js-chartjs-bars" style="max-width: 100%;"></canvas>
                                @push( 'scripts' )
                                    <script>
                                        new Chart( document.getElementById('not_operational_chart'), {
                                            type: 'bar',
                                            data: {
                                                labels: [@json($data['labels'])][0],
                                                datasets: [
                                                    {
                                                        label: 'Previous year',
                                                        data: [@json($data['not_operational_previous_year'])][0],
                                                        backgroundColor: [
                                                            'rgba(54, 162, 235, 0.3)',
                                                            'rgba(54, 162, 235, 0.3)',
                                                            'rgba(54, 162, 235, 0.3)',
                                                            'rgba(54, 162, 235, 0.3)',
                                                            'rgba(54, 162, 235, 0.3)',
                                                            'rgba(54, 162, 235, 0.3)',
                                                            'rgba(54, 162, 235, 0.3)',
                                                            'rgba(54, 162, 235, 0.3)',
                                                            'rgba(54, 162, 235, 0.3)',
                                                            'rgba(54, 162, 235, 0.3)',
                                                            'rgba(54, 162, 235, 0.3)',
                                                            'rgba(54, 162, 235, 0.3)',
                                                        ],
                                                        borderColor: [
                                                            'rgba(54, 162, 235, 1)',
                                                            'rgba(54, 162, 235, 1)',
                                                            'rgba(54, 162, 235, 1)',
                                                            'rgba(54, 162, 235, 1)',
                                                            'rgba(54, 162, 235, 1)',
                                                            'rgba(54, 162, 235, 1)',
                                                            'rgba(54, 162, 235, 1)',
                                                            'rgba(54, 162, 235, 1)',
                                                            'rgba(54, 162, 235, 1)',
                                                            'rgba(54, 162, 235, 1)',
                                                            'rgba(54, 162, 235, 1)',
                                                            'rgba(54, 162, 235, 1)',
                                                        ],
                                                        borderWidth: 1

                                                    },
                                                    {
                                                        label: 'Current year',
                                                        data: [@json($data['not_operational_current_year'])][0],
                                                        backgroundColor: [
                                                            'rgba(255, 99, 132, 0.3)',
                                                            'rgba(255, 99, 132, 0.3)',
                                                            'rgba(255, 99, 132, 0.3)',
                                                            'rgba(255, 99, 132, 0.3)',
                                                            'rgba(255, 99, 132, 0.3)',
                                                            'rgba(255, 99, 132, 0.3)',
                                                            'rgba(255, 99, 132, 0.3)',
                                                            'rgba(255, 99, 132, 0.3)',
                                                            'rgba(255, 99, 132, 0.3)',
                                                            'rgba(255, 99, 132, 0.3)',
                                                            'rgba(255, 99, 132, 0.3)',
                                                            'rgba(255, 99, 132, 0.3)',
                                                        ],
                                                        borderColor: [
                                                            'rgba(255, 99, 132, 1)',
                                                            'rgba(255, 99, 132, 1)',
                                                            'rgba(255, 99, 132, 1)',
                                                            'rgba(255, 99, 132, 1)',
                                                            'rgba(255, 99, 132, 1)',
                                                            'rgba(255, 99, 132, 1)',
                                                            'rgba(255, 99, 132, 1)',
                                                            'rgba(255, 99, 132, 1)',
                                                            'rgba(255, 99, 132, 1)',
                                                            'rgba(255, 99, 132, 1)',
                                                            'rgba(255, 99, 132, 1)',
                                                            'rgba(255, 99, 132, 1)',
                                                        ],
                                                        borderWidth: 1

                                                    },
                                                ]
                                            },
                                            options: {
                                                scales: {
                                                    yAxes: [{

                                                        ticks: {
                                                            beginAtZero: true
                                                        }
                                                    }],
                                                    xAxes: [{
                                                        barPercentage: 0.7,
                                                    }],
                                                }
                                            }
                                        });
                                    </script>
                                @endpush
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endcan
    </div>
    <!-- END Page Content -->
@endsection

@section( 'js_before' )
    <div class="modal fade" id="modal-onboarding" tabindex="-1" role="dialog" aria-labelledby="modal-onboarding" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content rounded overflow-hidden bg-image" style="background-image: url('{{asset('media/photos/photo24.jpg') }}');">
                <div class="row">
                    <div class="col-md-5">
                        <div class="p-3 text-right text-md-left">
                            <a class="font-w600 text-white" href="#" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-share text-danger-light mr-1"></i> Skip Intro
                            </a>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="bg-white shadow-lg">
                            <div class="js-slider slick-dotted-inner" data-dots="true" data-arrows="false" data-infinite="false">
                                <div class="p-4">
                                    <i class="fa fa-award fa-3x text-muted my-4"></i>
                                    <h3 class="font-size-h2 font-w300 mb-2">Welcome to the new ERP!</h3>
                                    <p class="text-muted">
                                        Our new environment.
                                    </p>
                                    <button type="button" class="btn btn-hero btn-primary mb-4" onclick="jQuery('.js-slider').slick('slickGoTo', 1);">
                                        See features <i class="fa fa-arrow-right ml-1"></i>
                                    </button>
                                </div>
                                <div class="slick-slide p-4">
                                    <i class="fa fa-frog fa-3x text-muted my-4"></i>
                                    <h3 class="font-size-h2 font-w300 mb-2">Features</h3>
                                    <ul class="text-muted">
                                        <li>New style</li>
                                        <li>New code</li>
                                        <li>Better forms</li>
                                        <li>Fancy modals (like this one)</li>
                                    </ul>
                                    <button type="button" class="btn btn-hero btn-primary mb-4" onclick="jQuery('.js-slider').slick('slickGoTo', 2);">
                                        View Examples <i class="fa fa-arrow-right ml-1"></i>
                                    </button>
                                </div>
                                <div class="slick-slide p-4">
                                    <i class="fa fa-tools fa-3x text-muted my-4"></i>
                                    <h3 class="font-size-h2 font-w300">New form controls</h3>
                                    <form class="mb-3">
                                        <div class="form-group row">
                                            <div class="col-lg-3">
                                                <label for="modal_setting_1">Checkbox</label>
                                            </div>
                                            <div class="col-lg-9">
                                                <div class="custom-control custom-checkbox custom-control-lg custom-control-inline custom-control-primary">
                                                    <input type="checkbox" class="custom-control-input" id="modal_setting_1" name="modal_setting_1" checked="">
                                                    <label class="custom-control-label" for="modal_setting_1"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-lg-3">
                                                <label for="modal_setting_2">Switch</label>
                                            </div>
                                            <div class="col-lg-9">
                                                <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                                    <input type="checkbox" class="custom-control-input" id="modal_setting_2" name="modal_setting_2" checked="">
                                                    <label class="custom-control-label" for="modal_setting_2"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-lg-3">
                                                <label for="modal_setting_3">Input</label>
                                            </div>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control form-control-alt" id="modal_setting_3" name="modal_setting_3" checked="">
                                            </div>
                                        </div>
                                    </form>
                                    <a href="#" class="btn btn-hero btn-success mb-4" data-dismiss="modal" aria-label="Close">
                                        Let's go! <i class="fa fa-check ml-1"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
