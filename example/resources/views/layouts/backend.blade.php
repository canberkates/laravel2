@if (Auth::user()->us_force_change_password == 0 && (strpos(url()->current(), 'force_pass_change') == false))
    @push( 'scripts' )
        <script>
            //Redirect to force_pass_change if user is logged in and has to change his password and if URL is not already force_pass_change
            window.location.href = '{{url("/force_pass_change")}}';
        </script>
    @endpush
@endif

<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

    <title>Phoenix</title>

    <meta name="description" content="Phoenix - 2019">
    <meta name="robots" content="noindex, nofollow">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Icons -->
    <link rel="shortcut icon" href="{{ asset('media/favicons/favicon.png') }}">
    <link rel="icon" sizes="192x192" type="image/png" href="{{ asset('media/favicons/favicon-192x192.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('media/favicons/apple-touch-icon-180x180.png') }}">

    <!-- Fonts and Styles -->
    @yield('css_before')
    @stack('styles')

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,400i,600,700">
    <link rel="stylesheet" id="css-main" href="{{ mix('css/dashmix.css') }}">
    <link rel="stylesheet" href="{{ asset( 'js/plugins/magnific-popup/magnific-popup.css' ) }}">

    <!-- You can include a specific file from public/css/themes/ folder to alter the default color theme of the template. eg: -->
<!-- <link rel="stylesheet" href="{{ mix('css/themes/xwork.css') }}"> -->

@yield('css_after')

<!-- Scripts -->
    <script>window.Laravel = {!! json_encode(['csrfToken' => csrf_token(),]) !!};</script>
</head>
<body>
<!-- Page Container -->
<!--
    Available classes for #page-container:

GENERIC

    'enable-cookies'                            Remembers active color theme between pages (when set through color theme helper Template._uiHandleTheme())

SIDEBAR & SIDE OVERLAY

    'sidebar-r'                                 Right Sidebar and left Side Overlay (default is left Sidebar and right Side Overlay)
    'sidebar-o'                                 Visible Sidebar by default (screen width > 991px)
    'sidebar-o-xs'                              Visible Sidebar by default (screen width < 992px)
    'sidebar-dark'                              Dark themed sidebar

    'side-overlay-hover'                        Hoverable Side Overlay (screen width > 991px)
    'side-overlay-o'                            Visible Side Overlay by default

    'enable-page-overlay'                       Enables a visible clickable Page Overlay (closes Side Overlay on click) when Side Overlay opens

    'side-scroll'                               Enables custom scrolling on Sidebar and Side Overlay instead of native scrolling (screen width > 991px)

HEADER

    ''                                          Static Header if no class is added
    'page-header-fixed'                         Fixed Header


Footer

    ''                                          Static Footer if no class is added
    'page-footer-fixed'                         Fixed Footer (please have in mind that the footer has a specific height when is fixed)

HEADER STYLE

    ''                                          Classic Header style if no class is added
    'page-header-dark'                          Dark themed Header
    'page-header-glass'                         Light themed Header with transparency by default
                                                (absolute position, perfect for light images underneath - solid light background on scroll if the Header is also set as fixed)
    'page-header-glass page-header-dark'         Dark themed Header with transparency by default
                                                (absolute position, perfect for dark images underneath - solid dark background on scroll if the Header is also set as fixed)

MAIN CONTENT LAYOUT

    ''                                          Full width Main Content if no class is added
    'main-content-boxed'                        Full width Main Content with a specific maximum width (screen width > 1200px)
    'main-content-narrow'                       Full width Main Content with a percentage width (screen width > 1200px)
-->
<div id="page-container"
     class="sidebar-o enable-page-overlay side-scroll page-header-fixed page-header-dark main-content-narrow enable-cookies">
    <!-- Side Overlay-->
    <aside id="side-overlay">
        <!-- Side Header -->
        <div class="bg-image" style="background-image: url('{{ asset('media/various/bg_side_overlay_header.jpg') }}');">
            <div class="bg-primary-op">
                <div class="content-header">
                    <!-- User Avatar -->
                    <a class="img-link mr-1" href="javascript:void(0)">
                    </a>
                    <!-- END User Avatar -->

                    <!-- User Info -->
                    <div class="ml-2">
                        <a class="text-white font-w600" href="javascript:void(0)"><?php echo ''; ?></a>
                        <div
                            class="text-white-75 font-size-sm font-italic">{{{ isset(Auth::user()->email) ? Auth::user()->email : '' }}}</div>
                    </div>
                    <!-- END User Info -->

                    <!-- Close Side Overlay -->
                    <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                    <a class="ml-auto text-white" href="javascript:void(0)" data-toggle="layout"
                       data-action="side_overlay_close">
                        <i class="fa fa-times-circle"></i>
                    </a>
                    <!-- END Close Side Overlay -->
                </div>
            </div>
        </div>
        <!-- END Side Header -->

        <!-- Side Content -->
        <div class="content-side">
            <!-- Side Overlay Tabs -->
            <div class="block block-transparent pull-x pull-t">
                <ul class="nav nav-tabs nav-tabs-block nav-justified" data-toggle="tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" href="#so-settings">
                            <i class="fa fa-fw fa-cog"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#so-profile">
                            <i class="far fa-fw fa-edit"></i>
                        </a>
                    </li>
                </ul>
                <div class="block-content tab-content overflow-hidden">

                    <!-- Settings Tab -->
                    <div class="tab-pane pull-x fade fade-up show active" id="so-settings" role="tabpanel">
                        <div class="block mb-0">
                            <!-- Color Themes -->
                            <!-- Toggle Themes functionality initialized in Template._uiHandleTheme() -->
                            <div class="block-content block-content-sm block-content-full bg-body">
                                <span class="text-uppercase font-size-sm font-w700">Color Themes</span>
                            </div>
                            <div class="block-content block-content-full">
                                <div class="row gutters-tiny text-center">
                                    <div class="col-4 mb-1">
                                        <a class="d-block py-3 text-white font-size-sm font-w600 bg-default"
                                           data-toggle="theme" data-theme="default" href="#">
                                            Default
                                        </a>
                                    </div>
                                    <div class="col-4 mb-1">
                                        <a class="d-block py-3 text-white font-size-sm font-w600 bg-xwork"
                                           data-toggle="theme" data-theme="/css/themes/xwork.min.css" href="#">
                                            xWork
                                        </a>
                                    </div>
                                    <div class="col-4 mb-1">
                                        <a class="d-block py-3 text-white font-size-sm font-w600 bg-xmodern"
                                           data-toggle="theme" data-theme="/css/themes/xmodern.min.css" href="#">
                                            xModern
                                        </a>
                                    </div>
                                    <div class="col-4 mb-1">
                                        <a class="d-block py-3 text-white font-size-sm font-w600 bg-xeco"
                                           data-toggle="theme" data-theme="/css/themes/xeco.min.css" href="#">
                                            xEco
                                        </a>
                                    </div>
                                    <div class="col-4 mb-1">
                                        <a class="d-block py-3 text-white font-size-sm font-w600 bg-xsmooth"
                                           data-toggle="theme" data-theme="/css/themes/xsmooth.min.css" href="#">
                                            xSmooth
                                        </a>
                                    </div>
                                    <div class="col-4 mb-1">
                                        <a class="d-block py-3 text-white font-size-sm font-w600 bg-xinspire"
                                           data-toggle="theme" data-theme="/css/themes/xinspire.min.css" href="#">
                                            xInspire
                                        </a>
                                    </div>
                                    <div class="col-4 mb-1">
                                        <a class="d-block py-3 text-white font-size-sm font-w600 bg-xdream"
                                           data-toggle="theme" data-theme="/css/themes/xdream.min.css" href="#">
                                            xDream
                                        </a>
                                    </div>
                                    <div class="col-4 mb-1">
                                        <a class="d-block py-3 text-white font-size-sm font-w600 bg-xpro"
                                           data-toggle="theme" data-theme="/css/themes/xpro.min.css" href="#">
                                            xPro
                                        </a>
                                    </div>
                                    <div class="col-4 mb-1">
                                        <a class="d-block py-3 text-white font-size-sm font-w600 bg-xplay"
                                           data-toggle="theme" data-theme="/css/themes/xplay.min.css" href="#">
                                            xPlay
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!-- END Color Themes -->
                        </div>
                    </div>
                    <!-- END Settings Tab -->

                    <!-- Profile -->
                    <div class="tab-pane pull-x fade fade-up" id="so-profile" role="tabpanel">

                        <form action="" method="POST" onsubmit="return false;" id="profile_edit">
                            <input type="hidden" id="us_id" name="us_id" value="{{Auth::id()}}">
                            <div class="block mb-0">

                                <!-- Password Update -->
                                <div class="block-content block-content-sm block-content-full bg-body">
                                    <span class="text-uppercase font-size-sm font-w700">Change your password</span>
                                </div>
                                <div class="block-content block-content-full">
                                    <div class="p-3 mb-2 text-white" id="change_password_errordiv"
                                         style="display:none;">

                                    </div>

                                    <div id="change_password_div">
                                        <div class="form-group">
                                            <label for="current_password">Current Password</label>
                                            <input type="password" class="form-control" id="current_password"
                                                   name="current_password">
                                        </div>
                                        <div class="form-group">
                                            <label for="new_password">New Password</label>
                                            <input type="password" class="form-control" id="new_password"
                                                   name="new_password">
                                        </div>
                                        <div class="form-group">
                                            <label for="new_password_confirm">Confirm New Password</label>
                                            <input type="password" class="form-control" id="new_password_confirm"
                                                   name="new_password_confirm">
                                        </div>
                                    </div>
                                </div>
                                <!-- END Password Update -->

                                <!-- Submit -->
                                <div class="block-content row justify-content-center border-top">
                                    <div class="col-9">
                                        <button id="profile_edit_submit" type="submit"
                                                class="btn btn-block btn-hero-primary">
                                            <i class="fa fa-fw fa-save mr-1"></i> Save
                                        </button>
                                    </div>
                                </div>
                                <!-- END Submit -->
                            </div>
                        </form>
                    </div>
                    <!-- END Profile -->
                </div>
            </div>
            <!-- END Side Overlay Tabs -->
        </div>
        <!-- END Side Content -->
    </aside>
    <!-- END Side Overlay -->

    <!-- Sidebar -->
    <!--
        Sidebar Mini Mode - Display Helper classes

        Adding 'smini-hide' class to an element will make it invisible (opacity: 0) when the sidebar is in mini mode
        Adding 'smini-show' class to an element will make it visible (opacity: 1) when the sidebar is in mini mode
            If you would like to disable the transition animation, make sure to also add the 'no-transition' class to your element

        Adding 'smini-hidden' to an element will hide it when the sidebar is in mini mode
        Adding 'smini-visible' to an element will show it (display: inline-block) only when the sidebar is in mini mode
        Adding 'smini-visible-block' to an element will show it (display: block) only when the sidebar is in mini mode
    -->
    <nav id="sidebar" aria-label="Main Navigation">
        <!-- Side Header -->
        <div class="bg-header-dark">
            <div class="content-header bg-white-10">
                <!-- Logo -->
                <a class="link-fx font-w600 font-size-lg text-white" href="/">
                            <span class="smini-visible">
                                <span class="text-white-75">P</span><span class="text-white">x</span>
                            </span>
                    <span class="smini-hidden">
                                <span class="text-white-75">Phoeni</span><span class="text-white">x</span>
                            </span>
                </a>
                <!-- END Logo -->

                <!-- Options -->
                <div>
                    <a id="erp-sidebar-close" class="text-white font-size-lg" data-toggle="layout"
                       data-action="sidebar_close" href="#" data-target="">
                        <i class="fa fa-fw fa-angle-left"></i>
                    </a>


                    <!-- END Toggle Sidebar Style -->

                    <!-- Close Sidebar, Visible only on mobile screens -->
                    <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                    <a class="d-lg-none text-white ml-2" data-toggle="layout" data-action="sidebar_close"
                       href="javascript:void(0)">
                        <i class="fa fa-times-circle"></i>
                    </a>
                    <!-- END Close Sidebar -->
                </div>
                <!-- END Options -->
            </div>
        </div>
        <!-- END Side Header -->

        <!-- Side Navigation -->
        <div class="content-side content-side-full">
            <ul class="nav-main">
                <li class="nav-main-item">
                    <a class="nav-main-link{{ request()->is('dashboard') ? ' active' : '' }}"
                       href="{{ url( 'dashboard') }}">
                        <i class="nav-main-link-icon si si-arrow-right"></i>
                        <span class="nav-main-link-name">Dashboard</span>
                    </a>
                    @can('requests')
                        <a class="nav-main-link" href="{{ url( 'requests') }}">
                            <i class="nav-main-link-icon si si-arrow-right"></i>
                            <span class="nav-main-link-name">Requests @if ($globaldata['menu_open_requests'] > 0) <span
                                    class="badge badge-primary badge-pill">{{$globaldata['menu_open_requests']}}</span>@endif</span>
                        </a>
                    @endcan
                    @can('premium leads')
                        <a class="nav-main-link" href="{{ url( 'premium_leads') }}">
                            <i class="nav-main-link-icon si si-arrow-right"></i>
                            <span class="nav-main-link-name">Premium Leads @if ($globaldata['menu_open_premium_leads'] > 0)
                                    <span
                                        class="badge badge-primary badge-pill">{{$globaldata['menu_open_premium_leads']}}</span>@endif</span>
                        </a>
                    @endcan

                    @can('customers')
                        <a class="nav-main-link" href="{{ url( 'customers') }}">
                            <i class="nav-main-link-icon si si-arrow-right"></i>
                            <span class="nav-main-link-name">Customers</span>
                        </a>
                    @endcan
                    @can('affiliates')
                    <a class="nav-main-link" href="{{ url( 'affiliatepartners' ) }}">
                            <i class="nav-main-link-icon si si-arrow-right"></i>
                            <span class="nav-main-link-name">Affiliate Partners</span>
                        </a>
                    @endcan
                    @can('service providers')
                    <a class="nav-main-link" href="{{ url( 'serviceproviders') }}">
                            <i class="nav-main-link-icon si si-arrow-right"></i>
                            <span class="nav-main-link-name">Service Providers</span>
                        </a>
                    @endcan
                    @can('lead resellers')
                    <a class="nav-main-link" href="{{ url( 'resellers') }}">
                            <i class="nav-main-link-icon si si-arrow-right"></i>
                            <span class="nav-main-link-name">Lead Resellers</span>
                        </a>
                    @endcan

                    @can('customers')
                        <a class="nav-main-link" href="{{url('calendar')}}">
                            <i class="nav-main-link-icon si si-arrow-right"></i>
                            <span class="nav-main-link-name">Calls <span id="calls_amount" style="display: none;"
                                                                         class="badge badge-primary badge-pill">0</span></span>
                        </a>
                    @endcan


                    @can('surveys')
                        <a class="nav-main-link" href="{{ url( 'surveys' ) }}">
                            <i class="nav-main-link-icon si si-arrow-right"></i>
                            <span class="nav-main-link-name">Reviews @if ($globaldata['menu_open_surveys'] > 0) <span
                                    class="badge badge-primary badge-pill">{{$globaldata['menu_open_surveys']}}</span>@endif</span>
                        </a>
                    @endcan

                    @can('claims')
                        <a class="nav-main-link" href="{{ url( 'claims') }}">
                            <i class="nav-main-link-icon si si-arrow-right"></i>
                            <span class="nav-main-link-name">Claims @if ($globaldata['menu_open_claims'] > 0) <span
                                    class="badge badge-primary badge-pill">{{$globaldata['menu_open_claims']}}</span> @endif</span>
                        </a>
                    @endcan

                    @can('admin')
                        <a class="nav-main-link" href="{{ url( 'admin' ) }}">
                            <i class="nav-main-link-icon si si-arrow-right"></i>
                            <span class="nav-main-link-name">Admin</span>
                        </a>
                    @endcan

                    @can('finance')
                        <a class="nav-main-link" href="{{ url( 'finance' ) }}">
                            <i class="nav-main-link-icon si si-arrow-right"></i>
                            <span class="nav-main-link-name">Finance</span>
                        </a>
                    @endcan

                    @can('reports')
                        <a class="nav-main-link" href="{{url('reports')}}">
                            <i class="nav-main-link-icon si si-arrow-right"></i>
                            <span class="nav-main-link-name">Reports</span>
                        </a>
                    @endcan
                    @can('sirelo settings')
                        <a class="nav-main-link" href="{{ url( 'sirelo_settings') }}">
                            <i class="nav-main-link-icon si si-arrow-right"></i>
                            <span class="nav-main-link-name">Sirelo Settings</span>
                        </a>
                    @endcan

                    @can('email builder')
                        <a class="nav-main-link" href="{{ url( 'email_builder_templates') }}">
                            <i class="nav-main-link-icon si si-arrow-right"></i>
                            <span class="nav-main-link-name">Email Builder</span>
                        </a>
                    @endcan

                    @can('validation')
                        <a class="nav-main-link" href="{{ url( 'validation') }}">
                            <i class="nav-main-link-icon si si-arrow-right"></i>
                            <span class="nav-main-link-name">Validation @if ($globaldata['menu_open_validation'] > 0) <span
                                    class="badge badge-primary badge-pill">{{$globaldata['menu_open_validation']}}</span>@endif</span>
                        </a>
                    @endcan
                    {{--
                    <a class="nav-main-link" href="/reports">
                        <i class="nav-main-link-icon si si-arrow-right"></i>
                        <span class="nav-main-link-name">Reports <span class="badge badge-primary">new erp</span></span>
                    </a>--}}

                </li>
            </ul>
        </div>
        <!-- END Side Navigation -->
    </nav>
    <!-- END Sidebar -->

    <!-- Header -->
    <header id="page-header">
        <!-- Header Content -->
        <div class="content-header">
            <!-- Left Section -->
            <div>
                <!-- Toggle Sidebar -->
                <!-- Layout API, functionality initialized in Template._uiApiLayout()-->
                <button id="erp-sidebar-open" type="button" class="d-none btn btn-dual mr-1 font-size-lg"
                        data-toggle="layout" data-action="sidebar_open">
                    <i class="fa fa-fw fa-angle-right"></i>
                </button>
                <!-- END Toggle Sidebar -->

                <!-- Open Search Section -->
                <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                <button type="button" class="btn btn-dual" data-toggle="layout" data-action="header_search_on">
                    <i class="fa fa-fw fa-search"></i> <span class="ml-1 d-none d-sm-inline-block">Search</span>
                </button>
                <!-- END Open Search Section -->
            </div>
            <!-- END Left Section -->

            <!-- Right Section -->
            <div>
                <!-- User Dropdown -->
                <div class="dropdown d-inline-block">
                    <button type="button" class="btn btn-dual" id="page-header-user-dropdown" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-fw fa-user d-sm-none"></i>
                        <span class="d-none d-sm-inline-block">{{Auth::user()->us_name}}</span>
                        <i class="fa fa-fw fa-angle-down ml-1 d-none d-sm-inline-block"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right p-0" aria-labelledby="page-header-user-dropdown"
                         style="min-width: 0;">

                        <form class="" id="logout-form" action="{{ route('logout') }}" method="POST">
                            @csrf
                            <button class="dropdown-item mb-0 text-right btn btn-light" type="submit">
                                <i class="far fa-fw fa-arrow-alt-circle-left mr-1"></i> {{ __('Logout') }}
                            </button>
                        </form>


                    </div>
                </div>

                <div class="dropdown d-inline-block show">
                    <button type="button" class="btn btn-dual" id="page-header-notifications-dropdown"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        <i class="fa fa-fw fa-phone"></i>
                        <span class="badge badge-secondary badge-pill"><div id="count_calls">Calculating...</div></span>
                    </button>

                    <div
                        class="dropdown-menu dropdown-menu-lg dropdown-menu-right p-0 @if(Route::current()->getName() == 'dashboard.index') show @endif"
                        aria-labelledby="page-header-notifications-dropdown" x-placement="bottom-end"
                        style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-229px, 38px, 0px);">
                        <div class="bg-primary-darker rounded-top font-w600 text-white text-center p-3">
                            Scheduled Calls
                        </div>
                        <ul class="nav-items my-2" id="call_notifications">
                            <li>No calls today</li>
                        </ul>
                        @push( 'scripts' )
                            <script>
                                jQuery(document).ready(function () {

                                    jQuery.ajax({
                                        url: "/ajax/get_calls_count",
                                        method: 'get',
                                        async: true,
                                        data: {user_id: {{\Illuminate\Support\Facades\Auth::id()}} },
                                        success: function ($result) {
                                            $("#count_calls").html('').append($result);
                                            $("#calls_amount").html('').append($result).show();
                                        }
                                    });

                                    jQuery.ajax({
                                        url: "/ajax/get_call_notifications",
                                        method: 'get',
                                        async: true,
                                        data: {user_id: {{\Illuminate\Support\Facades\Auth::id()}} },
                                        success: function ($result) {
                                            $("#call_notifications").html('').append($result);
                                        }
                                    });
                                });
                            </script>
                        @endpush
                        <div class="p-2 border-top">
                            <a class="btn btn-primary btn-block text-center" href="{{url('/calendar')}}">
                                <i class="fa fa-fw fa-eye mr-1"></i> View all your calls
                            </a>
                        </div>
                    </div>
                </div>

                <!-- END User Dropdown -->
                <!-- Notifications Dropdown -->
                <div class="dropdown d-inline-block">
                    <a href='{{ url( 'messages' ) }}' class='btn btn-dual'>
                        <i class="fa fa-fw fa-bell"></i>
                        <span class="badge badge-secondary badge-pill"><div
                                id="count_messages">Calculating...</div></span>


                        @push( 'scripts' )
                            <script>
                                jQuery(document).ready(function () {

                                    jQuery.ajax({
                                        url: "/ajax/get_messages_count",
                                        method: 'get',
                                        async: true,
                                        data: {user_id: {{\Illuminate\Support\Facades\Auth::id()}} },
                                        success: function ($result) {
                                            $("#count_messages").html('');
                                            $("#count_messages").append($result);
                                        }
                                    });
                                });
                            </script>
                        @endpush
                    </a>
                </div>
                <!-- END Notifications Dropdown -->


                <!-- Toggle Side Overlay -->
                <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                <button type="button" class="btn btn-dual" data-toggle="layout" data-action="side_overlay_toggle">
                    <i class="far fa-fw fa-list-alt"></i>
                </button>
                <!-- END Toggle Side Overlay -->
            </div>
            <!-- END Right Section -->
        </div>
        <!-- END Header Content -->

        <!-- Header Search -->
        <div id="page-header-search" class="overlay-header bg-primary">
            <div class="content-header">
                <form class="w-100" action="/search" method="POST" role="search" id="header-search-form">
                    @csrf
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                            <button type="button" class="btn btn-primary" data-toggle="layout"
                                    data-action="header_search_off">
                                <i class="fa fa-fw fa-times-circle"></i>
                            </button>
                        </div>
                        <input type="text" class="form-control border-0" placeholder="Search customer or id"
                               id="page-header-search-input" name="page-header-search-input">
                    </div>
                </form>
            </div>
        </div>
        <!-- END Header Search -->

        <!-- Header Loader -->
        <!-- Please check out the Loaders page under Components category to see examples of showing/hiding it -->
        <div id="page-header-loader" class="overlay-header bg-primary-darker">
            <div class="content-header">
                <div class="w-100 text-center">
                    <i class="fa fa-fw fa-2x fa-sun fa-spin text-white"></i>
                </div>
            </div>
        </div>
        <!-- END Header Loader -->
    </header>
    <!-- END Header -->

    <!-- Main Container -->
    <main id="main-container">
        @yield('content')
    </main>
    <!-- END Main Container -->

    <!-- Footer -->
    <footer id="page-footer" class="bg-body-light">
        <div class="content py-0">
            <div class="row font-size-sm">
                <div class="col-sm-6 order-sm-2 mb-1 mb-sm-0 text-center text-sm-right js-gallery">
                    Crafted with <i class="fa fa-heart text-danger"></i> by <a class="img-link img-lightbox"
                                                                               target="_blank"
                                                                               href="https://i.imgur.com/uz08aor.png">your
                        IT department</a>
                </div>
                <div class="col-sm-6 order-sm-1 text-center text-sm-left">
                    Phoenix &copy; <span data-toggle="year-copy">2018</span>
                </div>
            </div>
        </div>
    </footer>
    <!-- END Footer -->
</div>
<!-- END Page Container -->

@yield('js_before')

<!-- Global variables -->
<script>
    var $ajaxUrl = "{{ url( '/ajax/') }}";
</script>

<script src="{{ asset('js/dashmix.app.js') }}"></script>
<script src="{{ asset('js/laravel.app.js') }}"></script>
<script src="{{ asset('js/plugins/magnific-popup/jquery.magnific-popup.js') }}"></script>
<script src="{{ asset('js/custom/erp-complete.js') }}"></script>

<script>jQuery(function () {
        Dashmix.helpers('magnific-popup');
    });</script>

@stack('scripts')
@yield('js_after')
</body>
</html>
