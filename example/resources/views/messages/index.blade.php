@extends('layouts.backend')

@include( 'scripts.datatables' )
@include( 'scripts.dialogs' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Messages</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Messages</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="block block-rounded block-bordered">
            <div class="block-content ">
                <form method="post" action="{{action('MessagesController@index')}}">
                    @csrf
                    <input name="_method" type="hidden" value="post">
                    <h2 class="content-heading pt-0">Filters</h2>

                    <div class="form-group row">
                        <label class="col-md-1 col-form-label" for="for-type">Read:</label>
                        <div class="col-md-2">
                            <select id="for-type" class="js-select2 form-control {{$errors->has('type') ? 'is-invalid' : ''}}" name="read" data-placeholder="Choose one.." style="width:100%;" data-minimum-results-for-search="Infinity">

                                @foreach($bothyesno as $id => $X)
                                    <option @if (Request::old('read') == $id) selected @endif value="{{$id}}">{{$X}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <h2 class="content-heading pt-0"></h2>

                    <div class="row">
                        <div class="form-group col-md-2">
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- Your Block -->
        <div class="block block-rounded block-bordered">
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    <a href="{{ url('messages/readallmessages')}}"><button data-toggle="click-ripple" class="btn btn-primary">Read all messages</button></a>
                </h3>
            </div>
            <div class="block-content block-content-full">
                <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Timestamp</th>
                        <th>Read</th>
                        <th>Subject</th>
                        <th>Message</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($messages as $message)
                        <tr>
                            <td>{{$message->mes_id}}</td>
                            <td>{{$message->mes_timestamp}}</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    @if ($message->mes_read == 0)
                                        <a class="btn btn-sm btn-primary read_message" data-toggle="tooltip" data-mes_id="{{$message->mes_id}}" data-placement="left">
                                            <i class="fa fa-envelope" style="color:white;"></i>
                                        </a>
                                    @endif
                                </div>
                            </td>
                            <td>{{$message->mes_subject}}</td>
                            <td>{!! $message->mes_message !!}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Your Block -->
    </div>
    <!-- END Page Content -->
@endsection

@push ('scripts')
    <script>
        jQuery(document).ready(function(){
            jQuery( document ).on( 'click', '.read_message', function(e) {

                e.preventDefault();

                var $self = jQuery(this);

                swal({
                    title: "Are you sure?",
                    text: "Are you sure to mark this message as read?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, mark as read!",
                    preConfirm: function() {
                        return new Promise(function(resolve) {
                            jQuery.ajax({
                                url: "{{ url('ajax/message/markasread') }}",
                                method: 'get',
                                data: {id:$self.data('mes_id')},
                                success: function( $result) {
                                    swal("Done!", "It was successfully marked as read!", "success");

                                    if( typeof $callback == 'function' ) {

                                        $callback.call( $result );
                                    }
                                    $self.parents( '.dataTable' ).DataTable().row( $self.parents( 'tr' ) ).remove().draw( 'page' );
                                },
                                error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail

                                    swal("Error marking as read!", errorThrown, "error");
                                    console.log(JSON.stringify(jqXHR));
                                    console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                                }
                            });
                        });
                    }
                });
            });
        });
    </script>
@endpush

