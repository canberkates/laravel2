@extends('layouts.backend')

@include( 'scripts.datatables' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">MobilityEx - Validate skipped data</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url( 'dashboard' )}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{url( 'mobilityex_validate_gathered_data' )}}">MobilityEx - Validate gathered data</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Skipped data</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <div class="block block-rounded block-bordered">
            <div class="block block-rounded block-bordered">
                <div class="block-content">
                    <div class="block-content block-content-full">
                        <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Type</th>
                                <th>Customer ID</th>
                                <th>MobilityEx Customer</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($mofeda_rows as $row)
                                <tr>
                                    <td>{{$row->mofeda_id}}</td>
                                    <td>{{$row->mofeda_type}}</td>
                                    <td>@if(!empty($row->mofeda_cu_id)) {{$row->mofeda_cu_id}} @else {{"Unknown"}} @endif</td>
                                    <td>{{$row->mofeda_company_name_legal}}</td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <a class="btn btn-sm btn-primary"
                                               data-toggle="tooltip"
                                               data-placement="left"
                                               title="edit"
                                               href="{{ url('mobilityex_validate_skipped_data/' .  $row->mofeda_id  . '/validate')}}">
                                                <i class="fa fa-pencil-alt"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
