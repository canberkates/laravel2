@extends('layouts.backend')
@include( 'scripts.datatables' )
@include('scripts.datepicker')
@include('scripts.forms')
@include( 'scripts.dialogs' )
@include('scripts.select2')


@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">

            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">MobilityEx - Validate Gathered Data</h1>
                <div class="btn-group">
                    <a href="{{ url('mobilityex_validate_skipped_data')}}" class="btn btn-outline-secondary">
                        Skipped ({{$skipped_count}})
                    </a>
                </div>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">MobilityEx - Validate Gathered data</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="block block-rounded block-bordered">
            @if($none_left)
                <div class="alert alert-success">
                    All data for this country is validated. Well done! 😊
                </div>
            @endif

            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            {{$error}}<br>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Filters
                </h3>
            </div>
            <div class="block-content block-content-full">

                <form class="mb-5" method="post" action="{{action('MobilityexFetchedDataController@validateGatheredDataFiltered')}}">
                    @csrf
                    <input name="_method" type="hidden" value="post">


                    <div class="form-group row">
                        <label class="col-md-2 col-form-label ml-5" for="mobilityex_type">Type:</label>
                        <div class="col-md-4">
                            <select type="text" name="mobilityex_type" class="form-control" style="width:100%;">
                                <option value='1'>Validate customers</option>
                                <option value='2'>Match customer details</option>
                                <option value='3'>Add new customers</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row type_1_filter">
                        <label class="col-md-2 col-form-label ml-5" for="country">Country:</label>
                        <div class="col-md-4">
                            <select class="js-select2 form-control" data-placeholder="Select an option..."
                                    name="country" id="country" style="width:100%;">
                                <option></option>
                                @foreach($countries as $co_code => $country)
                                    <option value="{{$co_code}}">{{$country}}</option>
                                @endforeach
                            </select>
                            <i style="font-size:11px;">You can only choose countries which we didn't process all yet.</i><br /><br />
                        </div>
                    </div>

                    <div class="form-group row type_1_filter">
                        <label class="col-md-2 col-form-label ml-5" for="status">Status:</label>
                        <div class="col-md-4">
                            <select type="text" name="status" class="form-control" style="width:100%;">
                                <option value="0">All</option>
                                <option value='1'>Partners</option>
                                <option value='2'>Non-partners</option>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="form-group col-md-8">
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection

@push( 'scripts' )
    <script>
        jQuery("select[name=mobilityex_type]").change( function() {
            var type = jQuery(this).val();

            if (type == 1) {
                jQuery("select[name=country], select[name=status]").prop("disabled", false);
                jQuery(".type_1_filter").slideDown();

            }
            else {
                jQuery("select[name=country], select[name=status]").prop("disabled", true);
                jQuery(".type_1_filter").slideUp();

            }
        });

    </script>
@endpush
