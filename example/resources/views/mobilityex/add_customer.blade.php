@extends('layouts.backend')

@include('scripts.select2')
@include( 'scripts.telinput' )

@section('content')
    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="block block-rounded block-bordered">

                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    {{$error}}<br>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="block-content">
                        <form method="post" action="{{action('MobilityexFetchedDataController@addCustomerStore')}}">
                            @csrf
                            <input name="_method" type="hidden" value="post">
                            <input name="mofeda_id" type="hidden" value="@if(!empty(Request::old("mofeda_id"))) {{Request::old("mofeda_id")}} @else {{$mofeda->mofeda_id}} @endif">
                            <h2 class="content-heading pt-0">General Information</h2>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="for-la_code">Language:</label>
                                <div class="col-sm-7">
                                    <select id="for-la_code" class="js-select2 form-control {{$errors->has('la_code') ? 'is-invalid' : ''}}" name="la_code" data-placeholder="Choose one.." style="width:100%;" data-minimum-results-for-search="Infinity">
                                        <option></option>
                                        @foreach($languages as $language)
                                            <option @if ("EN" == $language->la_code) selected @endif value="{{$language->la_code}}">{{$language->la_language}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="for-description">Description:</label>
                                <div class="col-sm-7">
                                    <textarea id="for-description" rows="6" class="form-control" name="description">{{Request::old('description')}}</textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="for-company_name_legal">Company legal name:</label>
                                <div class="col-sm-7">
                                    <input id="for-company_name_legal" type="text" class="form-control {{$errors->has('company_name_legal') ? 'is-invalid' : ''}}" name="company_name_legal" @if (!empty(Request::old('company_name_legal'))) value="{{Request::old('company_name_legal')}}" @else value="{{$mofeda->mofeda_company_name_legal}}" @endif>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="for-company_name_business">Company business name:</label>
                                <div class="col-sm-7">
                                    <input id="for-company_name_business" type="text" class="form-control {{$errors->has('company_name_business') ? 'is-invalid' : ''}}" name="company_name_business" @if(!empty(Request::old('company_name_business')) || !empty($mofeda->mofeda_company_name_business)) @if (!empty(Request::old('company_name_business'))) value="{{Request::old('company_name_business')}}" @else value="{{$mofeda->mofeda_company_name_business}}" @endif @else @if (!empty(Request::old('company_name_legal'))) value="{{Request::old('company_name_legal')}}" @else value="{{$mofeda->mofeda_company_name_legal}}" @endif @endif >
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="for-attn">Primary contact:</label>
                                <div class="col-sm-7">
                                    <input id="for-attn" type="text" class="form-control {{$errors->has('attn') ? 'is-invalid' : ''}}" name="attn" value="{{Request::old('attn')}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="for-attn_email">Primary contact email:</label>
                                <div class="col-sm-7">
                                    <input id="for-attn_email" type="text" class="form-control {{$errors->has('attn_email') ? 'is-invalid' : ''}}" name="attn_email" @if (!empty(Request::old('attn_email'))) value="{{Request::old('attn_email')}}" @else value="{{$mofeda->mofeda_email}}" @endif>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="coc">Chamber of commerce:</label>
                                <div class="col-sm-7">
                                    <input id="coc" type="text" class="form-control {{$errors->has('coc') ? 'is-invalid' : ''}}" name="coc" value="{{Request::old('coc')}}">
                                </div>
                            </div>

                            <h2 class="content-heading pt-0">Emails</h2>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="leads_email">Lead email:</label>
                                <div class="col-sm-7">
                                    <input id="leads_email" type="text" class="form-control {{$errors->has('leads_email') ? 'is-invalid' : ''}}" name="leads_email" @if (!empty(Request::old('leads_email'))) value="{{Request::old('leads_email')}}" @else value="{{$mofeda->mofeda_email}}" @endif>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="review_email">Review communication:</label>
                                <div class="col-sm-7">
                                    <input id="review_email" type="text" class="form-control" name="review_email" @if (!empty(Request::old('review_email'))) value="{{Request::old('review_email')}}" @else value="{{$mofeda->mofeda_email}}" @endif>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="load_exchange_email">Load Exchange email:</label>
                                <div class="col-sm-7">
                                    <input id="load_exchange_email" type="text" class="form-control {{$errors->has('load_exchange_email') ? 'is-invalid' : ''}}" name="load_exchange_email" @if (!empty(Request::old('load_exchange_email'))) value="{{Request::old('load_exchange_email')}}" @else value="{{$mofeda->mofeda_email}}" @endif>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="general_info">General info:</label>
                                <div class="col-sm-7">
                                    <input id="general_info" type="text" class="form-control {{$errors->has('general_info') ? 'is-invalid' : ''}}" name="general_info" @if (!empty(Request::old('general_info'))) value="{{Request::old('general_info')}}" @else value="{{$mofeda->mofeda_email}}" @endif>
                                </div>
                            </div>

                            <h2 class="content-heading pt-0">Office Address</h2>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="for-street_1">Company street 1:</label>
                                <div class="col-sm-7">
                                    <input id="for-street_1" type="text" class="form-control {{$errors->has('street_1') ? 'is-invalid' : ''}}" name="street_1" @if (!empty(Request::old('street_1'))) value="{{Request::old('street_1')}}" @else value="{{$mofeda->mofeda_street}}" @endif>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="for-street_2">Company street 2:</label>
                                <div class="col-sm-7">
                                    <input id="for-street_2" type="text" class="form-control" name="street_2" value="{{Request::old('street_2')}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="for-zipcode">Company zipcode:</label>
                                <div class="col-sm-7">
                                    <input id="for-zipcode" type="text" class="form-control {{$errors->has('zipcode') ? 'is-invalid' : ''}}" name="zipcode" @if (!empty(Request::old('zipcode'))) value="{{Request::old('zipcode')}}" @else value="{{$mofeda->mofeda_zipcode}}" @endif>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="for-city">Company city:</label>
                                <div class="col-sm-7">
                                    <input id="for-city" type="text" class="form-control {{$errors->has('city') ? 'is-invalid' : ''}}" name="city" @if (!empty(Request::old('city'))) value="{{Request::old('city')}}" @else value="{{$mofeda->mofeda_city}}" @endif>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="for-country">Company country:</label>
                                <div class="col-sm-7">
                                    <select id="for-country" class="js-select2 form-control {{$errors->has('country') ? 'is-invalid' : ''}}" name="country" data-placeholder="Choose one..">
                                        <option value=""></option>
                                        @foreach($countries as $country)
                                            @if (!empty(Request::old('country')))
                                                <option @if (Request::old('country') == $country->co_code) selected @endif value="{{$country->co_code}}">{{$country->co_en}}</option>
                                            @else
                                                <option @if ($mofeda_co_code == $country->co_code) selected @endif value="{{$country->co_code}}">{{$country->co_en}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="for-telephone">Company telephone:</label>
                                <div class="col-sm-7">
                                    <input type="hidden" id="int_telephone" name="int_telephone" value="">
                                    <input id="for-telephone" type="text" class="form-control {{$errors->has('telephone') ? 'is-invalid' : ''}}" name="telephone" @if (!empty(Request::old('telephone'))) value="{{Request::old('telephone')}}" @else value="{{$mofeda->mofeda_telephone}}" @endif>
                                    <div id='telephone_not_valid' class='mt-1' style='font-size:12px; color:red; display: none;'>Phone number is not valid!</div>
                                    <div id='telephone_changed' class='mt-1' style='font-size:12px; display: none;'>Changed from @if (!empty(Request::old('telephone'))) {{Request::old('telephone')}} @else {{$mofeda->mofeda_telephone}} @endif</div>
                                </div>
                            </div>

                            <!-- telephone_not_valid,telephone_changed -->


                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="for-website">Company website:</label>
                                <div class="col-sm-7">
                                    <input id="for-website" type="text" class="form-control {{$errors->has('website') ? 'is-invalid' : ''}}" name="website" @if (!empty(Request::old('website'))) value="{{Request::old('website')}}" @else value="{{$mofeda->mofeda_website}}" @endif>
                                </div>
                            </div>

                            <h2 class="content-heading pt-0">Fields</h2>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label"
                                    for="finance_payment_currency">Region:</label>
                                <div class="col-sm-4">
                                    <select type="text" class="js-select2 form-control"
                                        name="fields_region">
                                        <option value=""></option>
                                        @foreach($regions as $region)
                                            <option
                                                value="{{$region->reg_id}}" {{ ($customer->moverdata->moda_reg_id == $region->reg_id) ? 'selected':'' }}>{{$region->reg_parent}} {{"(".$region->co_en.")"}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label"
                                    for="finance_payment_currency">Market:</label>
                                <div class="col-sm-4">
                                    <select type="text" class="form-control"
                                        name="fields_market">
                                        <option value=""></option>
                                        @foreach($customermarkettypes as $id => $markettype)
                                            <option value="{{$id}}" {{ (1 === $id) ? 'selected':'' }}>{{$markettype}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label"
                                    for="finance_payment_currency">Services:</label>
                                <div class="col-sm-4">
                                    <select type="text" class="form-control"
                                        name="fields_services">
                                        <option value=""></option>
                                        @foreach($customerservices as $id => $service)
                                            <option value="{{$id}}" {{ ($customer->moverdata->moda_services == $id) ? 'selected':'' }}>{{$service}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label"
                                    for="finance_payment_currency">Status:</label>
                                <div class="col-sm-4">
                                    <select type="text" class="form-control"
                                        name="fields_status">
                                        <option value=""></option>
                                        @foreach($customerstatustypes as $id => $statustype)
                                            <option value="{{$id}}" {{ (2 == $id) ? 'selected':'' }}>{{$statustype}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label"
                                    for="finance_payment_currency">Owner:</label>
                                <div class="col-sm-4">
                                    <select type="text" class="js-select2 form-control"
                                        name="fields_owner">
                                        <option value=""></option>
                                        @foreach($users as $user)
                                            <option value="{{$user->id}}" {{ (\Illuminate\Support\Facades\Auth::user()->us_id == $user->id) ? 'selected':'' }}>{{$user->us_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                            <h2 class="content-heading pt-0"></h2>

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Add Customer</button>
<!--                                    <a class="btn btn-primary" href="/customers">
                                        Back
                                    </a>-->
                                </div>
                            </div>
                        </form>
                    </div>



                </div>
            </div>
        </div>
    </div>


@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            function validatePhonenumber() {
                $.ajax({
                    type: "GET",
                    url: '{{ url('/ajax/validate_phone_numbers_adding_customer') }}',
                    data: {
                        "phone": jQuery("input[name=telephone]").val(),
                    },
                    success: function(data){
                        //Parse JSON
                        var returnedData = JSON.parse(data);

                        console.log("Phone validated");
                        console.log(returnedData);

                        if (returnedData != null) {
                            if (returnedData.converted_phone_general == 'not_valid')
                            {
                                jQuery("#telephone_changed").hide();
                                jQuery("#telephone_not_valid").show();
                                jQuery("input[name=telephone]").css("border", "2px solid red");
                            }
                            else {
                                jQuery("input[name=telephone]").val(returnedData.converted_phone_general);
                                jQuery("input[name=telephone]").css("border", "2px solid #129a12ba");
                                jQuery("#telephone_not_valid").hide();
                                jQuery("#telephone_changed").show();
                            }
                        }
                    }
                });
            }

            validatePhonenumber();

            jQuery("input[name=telephone]").keyup( function() {
                validatePhonenumber();
            });
        });
    </script>
@endpush
