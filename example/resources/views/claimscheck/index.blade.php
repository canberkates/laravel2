@extends('layouts.backend')

@include( 'scripts.datatables' )
@include( 'scripts.dialogs' )
@include( 'scripts.datepicker' )


@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Claims check</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/claims">Claims</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Claims check</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->
    <!-- Page Content -->
    <div class="content">
        <div class="col-md-6 block block-rounded block-bordered">
            <div class="block-content ">
                <form method="post" action="{{action('ClaimsCheckController@filteredindex')}}">
                    @csrf
                    <input name="_method" type="hidden" value="post">
                    <h2 class="content-heading pt-0">Filters</h2>

                    <div class="form-group row">
                        <label class="col-md-3 col-form-label ml-5" for="for-date">Date range:</label>
                        <div class="col-md-6">
                            <input  type="text" class="form-control drp drp-default" name="date" value="{{$date}}" autocomplete="off"/>
                        </div>
                    </div>

                    <h2 class="content-heading pt-0"></h2>

                    <div class="row">
                        <div class="form-group col-md-2">
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        @if (isset($filtered) && $filtered == true)
            <div class="block block-rounded block-bordered">
                <div class="block-content block-content-full">
                    <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                        <thead>
                            <tr>
                                <th>Employee</th>
                                <th>Accepted</th>
                                <th>Rejected</th>
                                @foreach(range(0, 23) as $hour)
                                    <th>{{$hour}}</th>
                                @endforeach
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($userclaims as $user)
                            <tr>
                                <td>{{$user['name']}}</td>
                                <td>{{$user['accepted']}}</td>
                                <td>{{$user['rejected']}}</td>
                               @foreach(range(0, 23) as $hour)
                                    <td>{{$user['hours'][$hour]}}</td>
                               @endforeach
                                <td>{{$user['total']}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                            <th></th>
                            <th>{{number_format($totals['accepted'], 0, ",", ".")}}</th>
                            <th>{{number_format($totals['rejected'], 0, ",", ".")}}</th>
                            @foreach(range(0, 23) as $hour)
                                <th>{{$totals['hours'][$hour]}}</th>
                            @endforeach
                            <th>{{number_format($totals['total'], 0, ",", ".")}}</th>
                        </tfoot>
                    </table>
                </div>
            </div>
        @endif
    </div>
    <!-- END Page Content -->
@endsection

