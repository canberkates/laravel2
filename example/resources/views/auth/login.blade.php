@extends('layouts.simple')

@section('css_before')
    <link rel="stylesheet" id="css-main" href="{{asset('css/dashmix.css')}}">
@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/pages/op_auth_signin.min.js') }}"></script>
@endsection

@section('content')
    <div class="bg-image" style='background-image: url("{{ asset('media/photos/photo22@2x.jpg') }}");'>
        <div class="row no-gutters justify-content-center bg-primary-dark-op">
            <div class="hero-static col-sm-8 col-md-6 col-xl-4 d-flex align-items-center p-2 px-sm-0">
                <!-- Sign In Block -->
                <div class="block block-transparent block-rounded w-100 mb-0 overflow-hidden">
                    <div class="block-content block-content-full px-lg-5 px-xl-6 py-4 py-md-5 py-lg-6 bg-white">
                        <!-- Header -->
                        <div class="mb-2 text-center">
                            <a class="link-fx font-w700 font-size-h1">
                                <span class="text-dark">Phoeni</span><span class="text-primary">x</span>
                            </a>
                            <p class="text-uppercase font-w700 font-size-sm text-muted">Sign In to Phoenix</p>
                        </div>
                        <!-- END Header -->

                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="form-group">
                                <div class="input-group">
                                    <input id="name" type="hidden" class="form-control" name="name" required autofocus>
                                    <input type="text" class="form-control" id="login-username" name="email" placeholder="Username" required autofocus value="{{ old('username') }}">
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="fa fa-user-circle"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" id="login-password" name="password" placeholder="Password" required>
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="fa fa-asterisk"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-hero-primary">
                                    <i class="fa fa-fw fa-sign-in-alt mr-1"></i> Log in
                                </button>
                            </div>
                        </form>
                        <!-- END Sign In Form -->
                    </div>
                    <div class="block-content bg-body">
                        <div class="d-flex justify-content-center text-center push">
                            <a class="item item-circle item-tiny mr-1 bg-default" data-toggle="theme" data-theme="{{ asset('css/dashmix.css') }}" href="#"></a>
                            <a class="item item-circle item-tiny mr-1 bg-xplay" data-toggle="theme" data-theme="{{ asset('css/themes/xplay.css')}}" href="#"></a>
                            <a class="item item-circle item-tiny mr-1 bg-xinspire" data-toggle="theme" data-theme="{{ asset('css/themes/xinspire.css')}}" href="#"></a>
                            <a class="item item-circle item-tiny mr-1 bg-xsmooth" data-toggle="theme" data-theme="{{ asset('css/themes/xsmooth.css')}}" href="#"></a>
                            <a class="item item-circle item-tiny mr-1 bg-xmodern" data-toggle="theme" data-theme="{{ asset('css/themes/xmodern.css') }}" href="#"></a>
                            <a class="item item-circle item-tiny mr-1 bg-xeco" data-toggle="theme" data-theme="{{ asset('css/themes/xeco.css') }}" href="#"></a>
                            <a class="item item-circle item-tiny mr-1 bg-xdream" data-toggle="theme" data-theme="{{ asset('css/themes/xdream.css')}}" href="#"></a>
                            <a class="item item-circle item-tiny mr-1 bg-xpro" data-toggle="theme" data-theme="{{ asset('css/themes/xpro.css')}}" href="#"></a>
                            <a class="item item-circle item-tiny bg-xwork" data-toggle="theme" data-theme="{{ asset('css/themes/xdream.css') }}" href="#"></a>
                        </div>
                    </div>
                </div>
                <!-- END Sign In Block -->
            </div>
        </div>
    </div>

@endsection
