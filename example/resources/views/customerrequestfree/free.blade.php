@extends('layouts.backend')

@section('content')
    <div class="content">

        <div class="col-md-6 block block-rounded block-bordered">
            <form method="post" id="claim_form" action="{{action('CustomerRequestController@freeLead', $id)}}">
                @csrf
                <input name="_method" type="hidden" value="post">
            <div class="block-content">

                <h2 class="content-heading pt-0">General</h2>

                @if($notice)
                    <div class="alert alert-danger" role="alert">
                        <p class="mb-0">{!! $notice !!}</p>
                    </div>
                @endif
                <div class="form-group row">
                    <div class="col-md-1"></div>
                    <label class="col-md-3 col-form-label">Free:</label>
                    <div class="col-md-8">
                        <select data-placeholder="Select some options..." class="js-select2 form-control"
                                name="free" style="width:100%;">
                            @foreach($yesno as $id => $answer)
                                <option value="{{$id}}">{{$answer}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-1"></div>
                    <label class="col-md-3 col-form-label">Reason:</label>
                    <div class="col-md-8">
                        <select data-placeholder="Select some options..." class="js-select2 form-control"
                                name="reason" style="width:100%;">
                            @foreach($reasons as $id => $reason)
                                <option value="{{$id}}">{{$reason}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-1"></div>
                    <label class="col-md-3 col-form-label">Send email:</label>
                    <div class="col-md-8">
                        <select data-placeholder="Select some options..." class="js-select2 form-control"
                                name="email" style="width:100%;">
                            @foreach($yesno as $id => $answer)
                                <option value="{{$id}}">{{$answer}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <h2 class="content-heading pt-0"></h2>

                <div class="row">
                    <div class="form-group col-md-2">
                        <button type="submit" id="claim_submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
            </form>
        </div>

    </div>
@endsection

@push('scripts')
    <script>
        jQuery(document).ready(function () {

            $("#claim_form").submit(function (e) {
                $("#claim_submit").attr("disabled", "disabled");
            })
        });
    </script>
@endpush




