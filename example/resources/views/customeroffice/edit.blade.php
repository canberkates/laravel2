@extends('layouts.backend')

@include('scripts.select2')

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Edit a Office of {{$office->customer->cu_company_name_business}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../../../">@if ($office->customer->cu_type == 1) {{"Customers"}} @elseif($office->customer->cu_type == 2) {{"Service Providers"}}@else {{"Affiliate Partners"}} @endif</a></li>
                        <li class="breadcrumb-item"><a href="../../edit">{{$office->customer->cu_company_name_business}}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Add office</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="block block-rounded block-bordered">
                     @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    {{$error}}<br>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="block-header block-header-default">
                        <h3 class="block-title">General</h3>
                    </div>
                    <div class="block-content">
                        <form class="mb-5" method="post" action="{{action('CustomerOfficeController@update', $office->cuof_id)}}">
                            @csrf
                            <input name="_method" type="hidden" value="PATCH">
                            <input name="customer_id" type="hidden" value="{{$office->customer->cu_id}}">

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="street_1">Street 1:</label>
                                <div class="col-sm-7">
                                    <input class="form-control" type="text" name="street_1" value="{{$office->cuof_street_1}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="street_2">Street 2:</label>
                                <div class="col-sm-7">
                                    <input class="form-control" type="text" name="street_2" value="{{$office->cuof_street_2}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="city">City:</label>
                                <div class="col-sm-7">
                                    <input class="form-control" type="text" name="city" value="{{$office->cuof_city}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="zipcode">Zipcode:</label>
                                <div class="col-sm-7">
                                    <input class="form-control" type="text" name="zipcode" value="{{$office->cuof_zipcode}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="for-country">Country:</label>
                                <div class="col-sm-7">
                                    <select id="for-country" class="js-select2 form-control {{$errors->has('country') ? 'is-invalid' : ''}}" name="country" data-placeholder="Choose one..">
                                        <option value=""></option>
                                        @foreach($countries as $country)
                                            <option value="{{$country->co_code}}" {{ ($office->cuof_co_code == $country->co_code) ? 'selected':'' }}>{{$country->co_en}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row" id="state_div" @if ($office->cuof_co_code != "US" && $office->cuof_co_code != "CA") style="display:none;" @endif>
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="select_states">State:</label>
                                <div class="col-sm-7">
                                    <select style="width:100%;" id="select_states" class="js-select2 form-control {{$errors->has('state') ? 'is-invalid' : ''}}" name="state" data-placeholder="Choose one..">
                                        <option value=""></option>
                                        @if(!empty($states))
                                            @foreach($states as $state)
                                                <option value="{{$state->st_zipcode}}" {{ ($office->cuof_state == $state->st_zipcode) ? 'selected':'' }}>{{$state->st_name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="email">Email:</label>
                                <div class="col-sm-7">
                                    <input class="form-control" type="text" name="email" value="{{$office->cuof_email}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="telephone">Telephone:</label>
                                <div class="col-sm-7">
                                    <input class="form-control" type="text" name="telephone" value="{{$office->cuof_telephone}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="website">Website:</label>
                                <div class="col-sm-7">
                                    <input class="form-control" type="text" name="website" value="{{$office->cuof_website}}">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Update office</button>
                                    <a class="btn btn-primary" href="../../edit">
                                        Back
                                    </a>
                                    @if(!$office->cuof_child_id)
                                    <a class="btn btn-warning" href="/customers/create?office_id={{$office->cuof_id}}">
                                        Convert to Child
                                    </a>
                                    @endif
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection




