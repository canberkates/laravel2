@extends('layouts.backend')
@include('scripts.forms')
@include('scripts.select2')

@pushonce( 'scripts:customer-office-create' )
    <script src="{{ asset('/js/custom/customer/customer_office_create.js') }}"></script>
@endpushonce

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Add a Office to {{$customer->cu_company_name_business}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../../">@if ($customer->cu_type == 1) {{"Customers"}} @elseif($customer->cu_type == 2) {{"Service Providers"}}@else {{"Affiliate Partners"}} @endif</a></li>
                        <li class="breadcrumb-item"><a href="../edit">{{$customer->cu_company_name_business}}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Add office</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="block block-rounded block-bordered">
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    {{$error}}<br>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="block-header block-header-default">
                        <h3 class="block-title">General</h3>
                    </div>
                    <div class="block-content">
                        <form class="mb-5" method="post" action="{{action('CustomerOfficeController@store')}}">
                            @csrf
                            <input name="_method" type="hidden" value="post">
                            <input name="customer_id" type="hidden" value="{{$customer->cu_id}}">

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="street_1">Street 1:</label>
                                <div class="col-sm-7">
                                    <input class="form-control" type="text" name="street_1" value="{{Request::old('street_1')}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="street_2">Street 2:</label>
                                <div class="col-sm-7">
                                    <input class="form-control" type="text" name="street_2" value="{{Request::old('street_2')}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="city">City:</label>
                                <div class="col-sm-7">
                                    <input class="form-control" type="text" name="city" value="{{Request::old('city')}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="zipcode">Zipcode:</label>
                                <div class="col-sm-7">
                                    <input class="form-control" type="text" name="zipcode" value="{{Request::old('zipcode')}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="for-country">Country:</label>
                                <div class="col-sm-7">
                                    <select id="for-country" class="js-select2 form-control {{$errors->has('country') ? 'is-invalid' : ''}}" name="country" data-placeholder="Choose one..">
                                        <option value=""></option>
                                        @foreach($countries as $country)
                                            <option @if (Request::old('country') == $country->co_code) selected @endif  value="{{$country->co_code}}">{{$country->co_en}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row" id="state_div" style="display:none;">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="for-state">State:</label>
                                <div class="col-sm-7">
                                    <select style="width:100%;" id="select_states" class="js-select2 form-control {{$errors->has('state') ? 'is-invalid' : ''}}" name="state" data-placeholder="Choose one..">
                                        <option value=""></option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="email">Email:</label>
                                <div class="col-sm-7">
                                    <input class="form-control" type="text" name="email" value="{{Request::old('email')}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="telephone">Telephone:</label>
                                <div class="col-sm-7">
                                    <input class="form-control" type="text" name="telephone" value="{{Request::old('telephone')}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="website">Website:</label>
                                <div class="col-sm-7">
                                    <input class="form-control" type="text" name="website" value="{{Request::old('website')}}">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Add office</button>
                                    <a class="btn btn-primary" href="../edit">
                                        Back
                                    </a>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@push( 'scripts' )
<script>
    $(function(){
        $("select[name=country]").change(function(){
            var dropDown = $("select[name=country]");
            var co_code = dropDown.val();
            var selectStates = $("#select_states");

            selectStates.empty();

            if(co_code == "US" || co_code == "CA") {
                $.ajax({
                    type: "GET",
                    url: '{{ url('/ajax/customer/getstates') }}',
                    data: {'co_code': co_code},
                    success: function(data){
                        // Parse the returned json data
                        var opts = $.parseJSON(data);
                        // Use jQuery's each to iterate over the opts value
                        selectStates.append('<option value=""></option>');

                        $.each(opts, function(i, d){
                            // You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
                            selectStates.append('<option value="' + i + '">' + d + '</option>');
                        });


                    }
                });

                $('#state_div').slideDown();
            }else{
                selectStates.empty();
                $('#state_div').slideUp();
            }
        });
    });
</script>
@endpush



