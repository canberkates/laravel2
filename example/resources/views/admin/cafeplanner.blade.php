@extends('layouts.backend')

@include('scripts.select2')

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Café planner</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active" aria-current="page">Café planner</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div id="confetti-wrapper">
                </div>

                <style>
                    :root {
                        --border-color: #D7DBE3;
                        font-family: -apple-system, BlinkMacSystemFont, 'Roboto', 'Open Sans', 'Helvetica Neue', sans-serif;
                        --green: #0CD977;
                        --red: #FF1C1C;
                        --pink: #FF93DE;
                        --purple: #5767ED;
                        --yellow: #FFC61C;
                        --rotation: 0deg;
                    }

                    body {
                        overflow: hidden;
                        width: 100%;
                        height: 100%;
                    }

                    .wrapper {
                        display: flex;
                        justify-content: center;
                        align-items: center;
                        height: 100vh;

                    }

                    .modal {
                        width: 300px;
                        margin: 0 auto;
                        border: 1px solid var(--border-color);
                        box-shadow: 0px 0px 5px rgba(0,0,0,0.16);
                        background-color: #fff;
                        border-radius: 0.25rem;
                        padding: 2rem;
                        z-index: 1;
                    }

                    .emoji {
                        display: block;
                        text-align: center;
                        font-size: 5rem;
                        line-height: 5rem;
                        transform: scale(0.5);
                        animation: scaleCup 2s infinite alternate;
                    // padding: 10px;
                    // width: 100px;
                    // height: 100px;
                    }
                    // .round {
                       //   border-radius: 100px;
                       //   border: 1px solid #000;
                       // }

                    @keyframes scaleCup {
                        0% {
                            transform: scale(0.6);
                        }

                        100% {
                            transform: scale(1);
                        }
                    }

                    h1 {
                        text-align: center;
                        font-size: 1em;
                        margin-top: 20px;
                        margin-bottom: 20px;
                    }


                    .modal-btn {
                        display: block;
                        margin: 0 -2rem -2rem -2rem;
                        padding: 1rem 2rem;
                        font-size: .75rem;
                        text-transform: uppercase;
                        text-align: center;
                        color: #fff;
                        font-weight: 600;
                        border-radius: 0 0 .25rem .25rem;
                        background-color: var(--green);
                        text-decoration: none;
                    }

                    @keyframes confettiRain {
                        0% {
                            opacity: 1;
                            margin-top: -100vh;
                            margin-left: -200px;
                        }

                        100% {
                            opacity: 1;
                            margin-top: 100vh;
                            margin-left: 200px;
                        }
                    }

                    .confetti {
                        opacity: 0;
                        position: absolute;
                        width: 1rem;
                        height: 1.5rem;
                        transition: 500ms ease;
                        animation: confettiRain 5s infinite;
                    }

                    #confetti-wrapper {
                        overflow: hidden !important;
                    }

                </style>
                <div class="block block-rounded block-bordered">
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    {{$error}}<br>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="block-header block-header-default">
                        <h3 class="block-title">General</h3>
                    </div>
                    <div class="block-content">
                        <form class="mb-5" method="post" action="{{action('AdminController@cafePlannerSubmit')}}">
                            @csrf
                            <input name="_method" type="hidden" value="post">

                            @if ($all_dates_done)
                                <h3>All dates are done!</h3>
                            @else
                                <input name="date_1_person_1" type="hidden" value="{{$persons_in_date["date_1_person_1"]}}">
                                <input name="date_1_person_2" type="hidden" value="{{$persons_in_date["date_1_person_2"]}}">
                                @if(isset($persons_in_date["date_2_person_1"]) && isset($persons_in_date["date_2_person_2"]))
                                    <input name="date_2_person_1" type="hidden" value="{{$persons_in_date["date_2_person_1"]}}">
                                    <input name="date_2_person_2" type="hidden" value="{{$persons_in_date["date_2_person_2"]}}">
                                @endif
                            @endif


                            <div class="row">
                                <div class="col-md-6">
                                    @if (!$all_dates_done)
                                        The following dates are random generated, based on how many dates he/she had before:<br /><br />
                                        <p>
                                            <b>Date 1:</b> {{$persons[$persons_in_date['date_1_person_1']]['name']}} & {{$persons[$persons_in_date['date_1_person_2']]['name']}}.<br />
                                            @if(isset($persons_in_date["date_2_person_1"]) && isset($persons_in_date["date_2_person_2"]))
                                                <b>Date 2:</b> {{$persons[$persons_in_date['date_2_person_1']]['name']}} & {{$persons[$persons_in_date['date_2_person_2']]['name']}}.<br />
                                            @endif
                                            <br />
                                        </p>

                                        <h2 class="content-heading pt-0"></h2>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    @endif
                                </div>

                                <div class="col-md-6">
                                    <b>Previous couples:</b><br />
                                    @foreach ($previous as $prev)
                                        {{$prev}}<br />
                                    @endforeach
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@push( 'scripts' )
    <script>
        jQuery(document).ready(function(){

            for(i=0; i<100; i++) {
                // Random rotation
                var randomRotation = Math.floor(Math.random() * 360);
                // Random Scale
                var randomScale = Math.random() * 1;
                // Random width & height between 0 and viewport
                var randomWidth = Math.floor(Math.random() * Math.max(document.documentElement.clientWidth, window.innerWidth || 0));
                var randomHeight =  Math.floor(Math.random() * Math.max(document.documentElement.clientHeight, window.innerHeight || 500));

                // Random animation-delay
                var randomAnimationDelay = Math.floor(Math.random() * 5);
                console.log(randomAnimationDelay);

                // Random colors
                var colors = ['#0CD977', '#FF1C1C', '#FF93DE', '#5767ED', '#FFC61C', '#8497B0']
                var randomColor = colors[Math.floor(Math.random() * colors.length)];

                // Create confetti piece
                var confetti = document.createElement('div');
                confetti.className = 'confetti';
                confetti.style.top=randomHeight + 'px';
                confetti.style.right=randomWidth + 'px';
                confetti.style.backgroundColor=randomColor;
                // confetti.style.transform='scale(' + randomScale + ')';
                confetti.style.obacity=randomScale;
                confetti.style.transform='skew(15deg) rotate(' + randomRotation + 'deg)';
                confetti.style.animationDelay=randomAnimationDelay + 's';
                document.getElementById("confetti-wrapper").appendChild(confetti);
            }
        });
    </script>

@endpush
