@extends('layouts.simple')

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Admin</h1>
                <a href="/admin"><button class="btn btn-primary">Go back to Admin Panel</button></a>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        <?php phpinfo(); ?>
    </div>
@endsection
