@extends('layouts.backend')

@include('scripts.select2')

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Café planner</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active" aria-current="page">Café planner</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="block block-rounded block-bordered">
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    {{$error}}<br>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="block-header block-header-default">
                        <h3 class="block-title">General</h3>
                    </div>
                    <div class="block-content">
                        <form class="mb-5" method="post" action="{{action('AdminController@cafePlannerFiltered')}}">
                            @csrf
                            <input name="_method" type="hidden" value="post">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-3" for="exclude">Exclude:</label>
                                        <div class="col-md-8">
                                            <select id="exclude" data-placeholder="Select users to exclude..." multiple
                                                    class="js-select2 form-control" name="exclude[]" style="width:100%;">
                                                <option></option>
                                                @foreach($users as $id => $user)
                                                    <option value="{{$id}}">{{$user['name']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <h2 class="content-heading pt-0"></h2>
                                    <button type="submit" class="btn btn-primary">Filter</button>
                                </div>

                                <div class="col-md-6">
                                    <b>Previous couples:</b><br />
                                    @foreach ($last_dates as $prev)
                                        {{$prev}}<br />
                                    @endforeach
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
