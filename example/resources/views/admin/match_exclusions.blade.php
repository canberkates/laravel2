@extends('layouts.backend')

@include( 'scripts.datatables' )
@include( 'scripts.datepicker' )
@include('scripts.select2')


@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Match Exclusions</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active" aria-current="page">Match Exclusions</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="block block-rounded block-bordered">

                    <div class="block-header block-header-default">
                        <h3 class="block-title">General</h3>
                    </div>
                    <div class="block-content">
                        <form class="mb-12" method="post" action="{{action('AdminController@updateMatchExclusions')}}">
                            @csrf
                            <input name="_method" type="hidden" value="post">
                            @foreach($exclusions as $name => $ids)
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control"
                                                       name="name" value="{{$name}}">
                                            </div>
                                            <div class="col-sm-7">
                                                <select multiple type="text" class="js-select2 form-control"
                                                        name="{{$name}}[]">
                                                    @foreach($customers as $customer)
                                                        <option value="{{$customer->cu_id}}" {{ in_array($customer->cu_id, $ids) ? 'selected':'' }}>{{$customer->cu_company_name_business. " (".$customer->cu_co_code.")"}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            {{-- @foreach($exclusions as $name => $ids)
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control"
                                                       name="language" value="{{$name}}">
                                            </div>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control"
                                                        name="language" value="{{implode(",",$ids)}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach --}}
                            <h3 class="block-title" style="color:red">New Values:</h3>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control"
                                                   name="new_name" value="">
                                        </div>
                                        <div class="col-sm-7">
                                            <select multiple type="text" class="js-select2 form-control"
                                                    name="new[]">
                                                @foreach($customers as $customer)
                                                    <option value="{{$customer->cu_id}}">{{$customer->cu_company_name_business. " (".$customer->cu_co_code.")"}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-primary">Update Match List</button>
                            <br />
                            <br />

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
