<a href="{{ url('clearcache')}}"><button data-toggle="click-ripple" class="btn btn-primary">Clear all cache</button></a>
<a href="{{ url('customercache')}}"><button class="btn btn-primary js-popover cache-confirm" data-toggle="popover" data-placement="top">Purge Cache</button></a><br/><br/>
<div class="bg-sidebar-dark p-3 rounded push">
    <!-- Toggle navigation -->
    <div class="d-lg-none">
        <!-- Class Toggle, functionality initialized in Helpers.coreToggleClass() -->
        <button type="button" class="btn btn-block btn-dark d-flex justify-content-between align-items-center" data-toggle="class-toggle" data-target="#horizontal-navigation-hover-normal-dark" data-class="d-none">
            Menu
            <i class="fa fa-bars"></i>
        </button>
    </div>
    <!-- End toggle navigation -->

    <!-- Navigation -->
    <div id="horizontal-navigation-hover-normal-dark" class="d-none d-lg-block mt-2 mt-lg-0">
        <ul class="nav-main nav-main-horizontal nav-main-hover nav-main-dark">
            <li class="nav-main-item">
                <a class="nav-main-link {{ request()->is('admin') ? ' active' : '' }}" href="/admin">
                    <i class="nav-main-link-icon fa fa-rocket"></i>
                    <span class="nav-main-link-name">Admin</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link {{ request()->is('ledgeraccounts') ? ' active' : '' }}" href="/admin/ledgeraccounts">
                    <i class="nav-main-link-icon fa fa-boxes"></i>
                    <span class="nav-main-link-name">Ledger Accounts</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link {{ request()->is('admin/memberships') ? ' active' : '' }}" href="/admin/memberships">
                    <i class="nav-main-link-icon fa fa-money-bill"></i>
                    <span class="nav-main-link-name">Memberships</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link {{ request()->is('admin/obligations') ? ' active' : '' }}" href="/admin/obligations">
                    <i class="nav-main-link-icon fa fa-money-bill"></i>
                    <span class="nav-main-link-name">Permits</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link {{ request()->is('admin/insurances') ? ' active' : '' }}" href="/admin/insurances">
                    <i class="nav-main-link-icon fa fa-money-bill"></i>
                    <span class="nav-main-link-name">Insurances</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link {{ request()->is('admin/notification_lists') ? ' active' : '' }}" href="/admin/notification_lists">
                    <i class="nav-main-link-icon fa fa-list"></i>
                    <span class="nav-main-link-name">Notification lists</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link {{ request()->is('newsletters') ? ' active' : '' }}" href="/newsletters">
                    <i class="nav-main-link-icon fa fa-money-bill"></i>
                    <span class="nav-main-link-name">Newsletters</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link {{ request()->is('admin/permissions') ? ' active' : '' }}" href="/admin/permissions">
                    <i class="nav-main-link-icon fa fa-user-lock"></i>
                    <span class="nav-main-link-name">Permissions</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link {{ request()->is('admin/users') ? ' active' : '' }}" href="/admin/users">
                    <i class="nav-main-link-icon fa fa-users"></i>
                    <span class="nav-main-link-name">Users</span>
                </a>
            </li>
        </ul>
    </div>
    <div id="horizontal-navigation-hover-normal-dark" class="d-none d-lg-block mt-2 mt-lg-0">
        <ul class="nav-main nav-main-horizontal nav-main-hover nav-main-dark">
            <li class="nav-main-item">
                <a class="nav-main-link" href="/admin/systemsettings">
                    <i class="nav-main-link-icon fa fa-boxes"></i>
                    <span class="nav-main-link-name">System settings</span>
                </a>
            </li>
        </ul>
    </div>
    <div id="horizontal-navigation-hover-normal-dark" class="d-none d-lg-block mt-2 mt-lg-0">
        <ul class="nav-main nav-main-horizontal nav-main-hover nav-main-dark">
            <li class="nav-main-item">
                <a class="nav-main-link" href="/admin/websites">
                    <i class="nav-main-link-icon fa fa-boxes"></i>
                    <span class="nav-main-link-name">Websites</span>
                </a>
            </li>
        </ul>
    </div>
    <div id="horizontal-navigation-hover-normal-dark" class="d-none d-lg-block mt-2 mt-lg-0">
        <ul class="nav-main nav-main-horizontal nav-main-hover nav-main-dark">
            <li class="nav-main-item">
                <a class="nav-main-link" href="/admin/phpinfo">
                    <i class="nav-main-link-icon fa fa-boxes"></i>
                    <span class="nav-main-link-name">PHP info</span>
                </a>
            </li>
        </ul>
    </div>
    <!-- End navigation -->
</div>
