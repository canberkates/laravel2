@extends('layouts.backend')

@include('scripts.select2')
@include('scripts.datatables')

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Reports Usage</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item" aria-current="page">><a href="../">Admin</a></li>
                    </ol>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active" aria-current="page">Reports Usage</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="block block-rounded block-bordered">
                    <div class="block-content">
                        <table data-order='[[1, "asc"]]'
                               class="table table-bordered table-striped table-vcenter js-dataTable-full">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Owner</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($datas as $data)
                                    <tr>
                                        <td>{{$data->cu_id}}</td>
                                        <td>{{$data->cu_company_name_business}}</td>
                                        <td>{{$data->us_name}}</td>
                                    </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
