@extends('layouts.backend')

@include('scripts.select2')

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Reports Usage</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item" aria-current="page">><a href="../">Admin</a></li>
                    </ol>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active" aria-current="page">Reports Usage</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="block block-rounded block-bordered">
                    <div class="block-content">
                        @foreach($reports_usage as $rep_name => $rep)
                            <strong>{{$rep['rep_name']}}</strong>: {{$rep['times_used']}} times<br />
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
