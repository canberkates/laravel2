@extends('layouts.backend')

@include('scripts.datatables')
@include('scripts.dialogs')

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Search results</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">App</li>
                        <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                    </ol>
                </nav>
            </div>
       </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="block block-rounded block-bordered">
            <div class="block-content">
                @if(isset($details))
                    <p>The Search results for your query <b>{{ $query }}</b> are:</p>
                    <table class="table table-bordered table-vcenter js-dataTable-full table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Status</th>
                            <th>Email</th>
                            <th>Country code</th>
                            <th>Language code</th>
                            <th>Edit</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($details as $customer)
                            <tr>
                                <td>{{$customer->cu_id}}</td>
                                <td>{{$customer->cu_company_name_business}} <span class="badge badge-primary">{{$customertypes[$customer->cu_type]}}</span></td>
                                <td style="color: @if($customer->status == "Active") green; @elseif($customer->status == "Pause") orange; @else red; @endif"> {{$customer->status}} @if($customer->cu_credit_hold) <i class="fa fa-euro-sign" style="color:red;"></i> @endif </td>
                                <td>{{$customer->cu_email}}</td>
                                @if($customer->cu_co_code)
                                    <td> <img src="/media/flags/{{strtolower($customer->cu_co_code)}}.png"/> {{$countries[$customer->cu_co_code]}} </td>
                                @else
                                    <td></td>
                                @endif
                                @if($customer->cu_la_code)
                                    <td>{{$languages[$customer->cu_la_code]}}</td>
                                @else
                                    <td></td>
                                @endif
                                <td class="text-center">
                                    <div class="btn-group">
                                        @if($customer->cu_type == 1)
                                            <a class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="left" title="edit" href="{{  url('customers/' . $customer->cu_id . '/edit')}}">
                                        @elseif($customer->cu_type == 2)
                                            <a class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="left" title="edit" href="{{  url('serviceproviders/' . $customer->cu_id . '/edit')}}">
                                        @elseif($customer->cu_type == 6)
                                            <a class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="left" title="edit" href="{{  url('resellers/' . $customer->cu_id . '/edit')}}">
                                        @else
                                            <a class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="left" title="edit" href="{{  url('affiliatepartners/' . $customer->cu_id . '/edit')}}">
                                        @endif
                                            <i class="fa fa-pencil-alt"></i>
                                        </a>
                                        <button type="button" class="btn btn-sm btn-primary" id="customer_delete" data-id="{{$customer->cu_id}}" data-toggle="tooltip" title="Delete">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @elseif(isset($message))
                    <p>There are no results for your query</p>
                @endif
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection

@push( 'scripts' )
<script>
    jQuery( document ).on( 'click', '#customer_delete', function(e) {

        e.preventDefault();

        var $self = jQuery(this);

        confirmDelete("{{ url('ajax/customer/delete') }}", 'get', {id:$self.data('id')}, function() {
            $self.parents( '.dataTable' ).DataTable().row( $self.parents( 'tr' ) ).remove().draw( 'page' );
        });

    });
</script>
@endpush
