@extends('layouts.backend')

@include('scripts.forms')
@include('scripts.select2')

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Edit form of {{$customer->cu_company_name_business}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../../../">Customers</a></li>
                        <li class="breadcrumb-item"><a href="../../edit">{{$customer->cu_company_name_business}}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Form edit</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <div class="block block-rounded block-bordered">
            <ul class="nav nav-tabs nav-tabs-block js-tabs-enabled" data-toggle="tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" href="#btabs-static-home">General</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#btabs-static-request_deliveries">Request deliveries</a>
                </li>
            </ul>
            <div class="block-content tab-content">
                <div class="tab-pane active" id="btabs-static-home" role="tabpanel">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="block block-rounded block-bordered">
                                @if($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach($errors->all() as $error)
                                                {{$error}}<br>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <div class="block-header block-header-default">
                                    <h3 class="block-title">Information</h3>
                                </div>
                                <div class="block-content">
                                    <form class="mb-5" method="post" action="{{action('ConversionToolsController@update', [$conversion_form->mofose_cu_id, $conversion_form->mofose_id])}}">
                                        @csrf
                                        <input name="_method" type="hidden" value="post">
                                        <input name="afpafo_id" type="hidden" value="{{$conversion_form->afpafo_id}}">


                                        <h2 class="content-heading pt-0">General</h2>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <div class="col-sm-1"></div>
                                                    <label class="col-sm-4 col-form-label" for="form_name">Name:</label>
                                                    <div class="col-sm-7">
                                                        <input class="form-control" type="text" name="form_name" value="{{$conversion_form->afpafo_name}}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <div class="col-sm-1"></div>
                                                    <label class="col-sm-4 col-form-label" for="email_from">Send e-mails from:</label>
                                                    <div class="col-sm-7">
                                                        <input class="form-control" type="text" name="email_from" value="{{$conversion_form->mofose_email_from}}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <div class="col-sm-1"></div>
                                                    <label class="col-sm-4 col-form-label" for="email_to">Send e-mails to:</label>
                                                    <div class="col-sm-7">
                                                        <input class="form-control" type="text" name="email_to" value="{{$conversion_form->mofose_email_to}}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <h2 class="content-heading pt-0">Form</h2>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <div class="col-sm-1"></div>
                                                    <label class="col-sm-4 col-form-label" for="volume_type">Volume type:</label>
                                                    <div class="col-sm-7">
                                                        <select type="text" class="form-control"
                                                                name="volume_type">
                                                            @foreach($volumetypes as $id => $vt)
                                                                <option value="{{$id}}" {{ ($conversion_form->afpafo_volume_type == $id) ? 'selected':'' }}>{{$vt}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <div class="col-sm-1"></div>
                                                    <label class="col-sm-4 col-form-label" for="language">Language:</label>
                                                    <div class="col-sm-7">
                                                        <select type="text" class="form-control"
                                                                name="language">
                                                            @foreach($languages as $language)
                                                                <option value="{{$language->la_code}}" {{ ($conversion_form->afpafo_la_code == $language->la_code) ? 'selected':'' }}>{{$language->la_language}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <h2 class="content-heading pt-0"></h2>
                                        <div class="row">
                                            <div class="col-md-1"></div>
                                            <div class="form-group col-md-8">
                                                <button type="submit" class="btn btn-primary">Update form</button>
                                                <a class="btn btn-primary" href="../../edit">
                                                    Back
                                                </a>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="btabs-static-request_deliveries" role="tabpanel">
                    <h2 style="color:red;">Currently unused!</h2>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="block block-rounded block-bordered">
                                <div class="block-header block-header-default">
                                    <h3 class="block-title">
                                        <a href="{{ url('customers/' . $customer->cu_id . '/conversion_tools/' . $conversion_form->mofose_id . '/requestdelivery/create')}}">
                                            <button class="btn btn-primary">Add a request delivery</button>
                                        </a>
                                    </h3>
                                </div>
                                <div class="block-content">
                                    <table
                                        class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Type</th>
                                            <th>Value</th>
                                            <th>Extra</th>
                                            <th>Modify</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($requestdeliveries as $requestdelivery)
                                                <tr>
                                                    <td>{{$requestdelivery->rede_id}}</td>
                                                    <td>{{$requestdeliverytypes[$requestdelivery->rede_type]}}</td>
                                                    <td>{{$requestdelivery->rede_value}}</td>
                                                    <td>{{$requestdelivery->rede_extra}}</td>
                                                    <td class="text-center">
                                                        <div class="btn-group">
                                                            <a class="btn btn-sm btn-primary" data-toggle="tooltip"
                                                               data-placement="left"
                                                               title="edit"
                                                               href="{{ url('customers/' . $customer->cu_id . '/conversion_tools/' . $requestdelivery->rede_mofose_id . '/requestdelivery/' . $requestdelivery->rede_id . '/edit')}}">
                                                                <i class="fa fa-pencil-alt"></i>
                                                            </a>

                                                            <form
                                                                action="{{ route('requestdelivery.destroy', $requestdelivery->rede_id)}}"
                                                                method="POST">
                                                                <input name="_method" type="hidden" value="DELETE">
                                                                @csrf
                                                                <button class="btn btn-sm btn-primary"
                                                                        data-toggle="tooltip"
                                                                        data-placement="left"
                                                                        title="Delete"
                                                                        type="submit">
                                                                    <i class="fa fa-times"></i>
                                                                </button>
                                                            </form>
                                                        </div>

                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection




