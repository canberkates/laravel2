@extends('layouts.backend')

@include('scripts.forms')
@include('scripts.select2')

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Add a Mover Form to {{$customer->cu_company_name_business}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../../">Customers</a></li>
                        <li class="breadcrumb-item"><a href="../edit">{{$customer->cu_company_name_business}}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Add form</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="block block-rounded block-bordered">
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    {{$error}}<br>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Information</h3>
                    </div>
                    <div class="block-content">
                        <form class="mb-5" method="post" action="{{action('ConversionToolsController@store', $customer->cu_id)}}">
                            @csrf
                            <input name="_method" type="hidden" value="post">

                            <h2 class="content-heading pt-0">General</h2>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="form_name">Name:</label>
                                        <div class="col-sm-7">
                                            <input class="form-control" type="text" name="form_name" value="{{Request::old('name')}}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="email_from">Send e-mails from:</label>
                                        <div class="col-sm-7">
                                            <input class="form-control" type="text" name="email_from" value="{{Request::old('name')}}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="email_to">Send e-mails to:</label>
                                        <div class="col-sm-7">
                                            <input class="form-control" type="text" name="email_to" value="{{Request::old('name')}}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h2 class="content-heading pt-0">Form</h2>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="volume_type">Volume type:</label>
                                        <div class="col-sm-7">
                                            <select type="text" class="form-control"
                                                    name="volume_type">
                                                @foreach($volumetypes as $id => $vt)
                                                    <option value="{{$id}}" {{ (Request::old('volume_type') == $id) ? 'selected':'' }}>{{$vt}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="language">Language:</label>
                                        <div class="col-sm-7">
                                            <select type="text" class="form-control"
                                                    name="language">
                                                @foreach($languages as $language)
                                                    <option value="{{$language->la_code}}" {{ (Request::old('language') == $language->la_code) ? 'selected':'' }}>{{$language->la_language}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h2 class="content-heading pt-0"></h2>
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Add form</button>
                                    <a class="btn btn-primary" href="../edit">
                                        Back
                                    </a>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection




