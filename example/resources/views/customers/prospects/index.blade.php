@extends('layouts.backend')

@include( 'scripts.datatables' )
@include( 'scripts.dialogs' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Customers (Prospects & Deleted)</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Customers</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->
    <!-- Page Content -->
    <div class="content">


        <!-- Your Block -->
        <div class="block block-rounded block-bordered">

            <ul class="nav nav-tabs nav-tabs-alt" data-toggle="tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" href="#btabs-alt-static-prospect">Prospect ({{count($customers_prospect)}})</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#btabs-alt-static-deleted">Deleted ({{count($customers_prospect_deleted)}})</a>
                </li>
            </ul>

            <div class="block-content tab-content">
                <div class="tab-pane active" id="btabs-alt-static-prospect" role="tabpanel">
                    <div class="block-content block-content-full">
                        <table data-order='[[1, "asc"]]'
                               class="table table-bordered table-striped table-vcenter js-dataTable-full">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Company legal Name</th>
                                <th>Company business Name</th>
                                <th>Country</th>
                                <th>City</th>
                                <th>Email</th>
                                <th>Telephone</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($customers_prospect as $id => $data)
                                <tr>
                                    <td>{{$data->cu_id}}</td>
                                    <td>{{$data->cu_company_name_legal}}</td>
                                    <td>{{$data->cu_company_name_business}}</td>
                                    <td>{{$countries[$data->cu_co_code]}}</td>
                                    <td>{{$data->cu_city}}</td>
                                    <td>{{$data->cu_email}}</td>
                                    <td>{{$data->cu_telephone}}</td>
                                    <td><div class='btn-group'><a href='/customers/{{$data->cu_id}}/edit'><button type='button' class='btn btn-sm btn-primary js-tooltip-enabled' data-toggle='tooltip' data-id='{{$data->cu_id}}' data-original-title='Edit'><i class='fa fa-pencil-alt'></i></button></a></div> <div class='btn-group'> <button type='button' class='btn btn-sm btn-primary customer_delete js-tooltip-enabled' data-toggle='tooltip' data-id='{{$data->cu_id}}' data-original-title='Delete'><i class='fa fa-times'></i></button></div></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="tab-pane" id="btabs-alt-static-deleted" role="tabpanel">
                    <div class="block-content block-content-full">
                        <table data-order='[[1, "asc"]]'
                               class="table table-bordered table-striped table-vcenter js-dataTable-full">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Company legal Name</th>
                                <th>Company business Name</th>
                                <th>Country</th>
                                <th>City</th>
                                <th>Email</th>
                                <th>Telephone</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($customers_prospect_deleted as $id => $data)
                                <tr>
                                    <td>{{$data->cu_id}}</td>
                                    <td>{{$data->cu_company_name_legal}}</td>
                                    <td>{{$data->cu_company_name_business}}</td>
                                    <td>{{$countries[$data->cu_co_code]}}</td>
                                    <td>{{$data->cu_city}}</td>
                                    <td>{{$data->cu_email}}</td>
                                    <td>{{$data->cu_telephone}}</td>
                                    <td><div class='btn-group'><a href='/customers/{{$data->cu_id}}/edit'><button type='button' class='btn btn-sm btn-primary js-tooltip-enabled' data-toggle='tooltip' data-id='{{$data->cu_id}}' data-original-title='Edit'><i class='fa fa-pencil-alt'></i></button></a></div> <div class='btn-group'> <button type='button' class='btn btn-sm btn-primary customer_delete js-tooltip-enabled' data-toggle='tooltip' data-id='{{$data->cu_id}}' data-original-title='Delete'><i class='fa fa-times'></i></button></div></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Your Block -->
    </div>
    <!-- END Page Content -->
@endsection

@push( 'scripts' )
    <script>
        jQuery(document).ready(function(){
        	jQuery( document ).on( 'click', '.customer_delete', function(e) {

                e.preventDefault();

        		var $self = jQuery(this);

                confirmDelete("{{ url('ajax/customer/delete') }}", 'get', {id:$self.data('id')}, function() {

                    $self.parents( '.dataTable' ).DataTable().row( $self.parents( 'tr' ) ).remove().draw( 'page' );
                });

            });
        });
    </script>

@endpush
