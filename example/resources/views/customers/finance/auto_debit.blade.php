<div class="block-content">

    <h2 class="content-heading pt-0">Auto Debit</h2>
    <form method="post" id="customers_finance"
          action="{{action('CustomersController@update', $customer->cu_id)}}">
        @csrf
        <input name="_method" type="hidden" value="PATCH">
        <input name="form_name" type="hidden" value="Auto debit">

    <div class="row">
        <div class="col-md-12">

            <div class="form-group row">
                <div class="col-sm-1"></div>
                <label class="col-sm-3 col-form-label"
                       for="auto_debit_name">Name</label>
                <div class="col-sm-7">
                    <input id="auto_debit_name" type="text" class="form-control"
                           name="auto_debit_name"
                           value="{{$customer->cu_auto_debit_name}}">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-1"></div>
                <label class="col-sm-3 col-form-label"
                       for="auto_debit_city">City</label>
                <div class="col-sm-7">
                    <input id="auto_debit_city" type="text" class="form-control"
                           name="auto_debit_city" autocomplete="off"
                           value="{{$customer->cu_auto_debit_city}}">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-1"></div>
                <label class="col-sm-3 col-form-label"
                       for="example-hf-email">Country:</label>
                <div class="col-sm-7">
                    <select type="text" class="form-control"
                            name="auto_debit_co_code">
                        @foreach($countries as $country)
                            <option value="{{$country->co_code}}" {{ ($customer->cu_auto_debit_co_code == $country->co_code) ? 'selected':'' }}>{{$country->co_en}}</option>
                        @endforeach
                    </select>
                </div>
            </div>



            <div class="form-group row">
                <div class="col-sm-1"></div>
                <label class="col-sm-3 col-form-label"
                       for="auto_debit_iban">IBAN</label>
                <div class="col-sm-7">
                    <input id="auto_debit_iban" type="text" class="form-control"
                           name="auto_debit_iban" autocomplete="off"
                           value="{{$customer->cu_auto_debit_iban}}">
                </div>
            </div>


            <div class="form-group row">
                <div class="col-sm-1"></div>
                <label class="col-sm-3 col-form-label"
                       for="auto_debit_bic">BIC</label>
                <div class="col-sm-7">
                    <input id="auto_debit_bic" type="text" class="form-control"
                           name="auto_debit_bic" autocomplete="off"
                           value="{{$customer->cu_auto_debit_bic}}">
                </div>
            </div>

            <div class="row">
                <div class="col-sm-1"></div>
                <div class="form-group col-md-8">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </div>

        </div>

    </div>

    </form>
</div>
