@extends('layouts.backend')

@include('scripts.select2')

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Increase Credit Limit</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../../">Customers</a></li>
                        <li class="breadcrumb-item"><a href="/customers/{{$cu_id}}/edit">Edit</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Increase Credit Limit</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="block block-rounded block-bordered">
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    {{$error}}<br>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="block-header block-header-default">
                        <h3 class="block-title">General</h3>
                    </div>
                    <div class="block-content">
                        <form class="mb-5" method="post" action="{{action('CustomersController@submitIncreaseCreditLimit', $customer->cu_id)}}">
                            @csrf
                            <input name="_method" type="hidden" value="post">

                            <div class="row">
                                <div class="col-md-6">

                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-11 col-form-label" for="date">Credit limit is currently {{$customer->cu_credit_limit}}</label>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="date">Increase till:</label>
                                        <div class="col-sm-5">
                                            <input autocomplete="off" type="text" value="{{$default_date_value}}" class="form-control" id="datepicker" name="date" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd"">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label"
                                               for="increase_with">Replace by:</label>
                                        <div class="col-sm-5">
                                            <input class="form-control" type="text" name="increase_with" value="{{number_format(($customer->cu_credit_limit/100)*110,2,'.','')}}"/>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h2 class="content-heading pt-0"></h2>

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Increase temporarily</button>
                                    <a class="btn btn-primary" href="/customers/{{$customer->cu_id}}/edit">
                                        Back
                                    </a>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $("#datepicker").datepicker({
            dateFormat: "yy-mm-dd",
            minDate: 1,
            maxDate: 7
        });
    </script>
@endpush
