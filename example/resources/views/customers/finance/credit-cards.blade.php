<div class="block-content">

    <h2 class="content-heading pt-0">Credit Cards

        <a href="creditcard/create"><button class="btn btn-primary">Add a credit card!</button></a></h2>

    <div class="row">
        <div class="col-md-12">

            <div class="form-group row">
                <div class="col-sm-1"></div>
                <label class="col-sm-3 col-form-label"
                       for="for_card_last_digits">Active card digits</label>
                <div class="col-sm-7">
                    <input id="for_card_last_digits" type="text" class="form-control"
                           name="card_last_digits"
                           disabled
                           value="{{ (!sizeof($creditcards) ? "-" : $creditcards[0]->adcade_card_last_digits)}}">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-1"></div>
                <label class="col-sm-3 col-form-label"
                       for="for_active_card_expiry">Active card expiry</label>
                <div class="col-sm-7">
                    <input id="for_active_card_expiry" type="text" class="form-control"
                           name="active_card_expiry"
                           disabled
                           value="{{ (!sizeof($creditcards) ? "-" :  $creditcards[0]->adcade_card_expiry_year."/".$creditcards[0]->adcade_card_expiry_month)}}">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-1"></div>
                <label class="col-sm-3 col-form-label"
                       for="for_associated_cards">Associated cards</label>
                <div class="col-sm-7">
                    <input id="for_associated_cards" type="text" class="form-control"
                           name="associated_cards"
                           disabled value="{{sizeof($creditcards)}}">
                </div>
            </div>

        </div>

    </div>
</div>
