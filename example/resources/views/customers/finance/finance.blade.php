<div class="block-content">
    <form method="post" id="customers_finance"
          action="{{action('CustomersController@update', $customer->cu_id)}}">
        @csrf
        <input name="_method" type="hidden" value="PATCH">
        <input name="form_name" type="hidden" value="Finance">

        <h2 class="content-heading pt-0">Finance</h2>

        <div class="row">
            <div class="col-md-6">

                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="for_debtor_number">Debtor
                        number:</label>
                    <div class="col-sm-7">
                        <input id="for_debtor_number" type="text" class="form-control"
                               name="debtor_number"
                               disabled value="{{$customer->cu_debtor_number}}">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="for_sales_manager">Sales
                        Manager:</label>
                    <div class="col-sm-7">
                        <select id="for_sales_manager" type="text" class="form-control"
                                name="sales_manager">
                            <option value=""></option>
                            @foreach($users as $user)
                                <option
                                    value="{{$user->id}}" {{ ($customer->cu_sales_manager == $user->id) ? 'selected':'' }}>{{$user->us_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="for_account_manager">Account
                        Manager:</label>
                    <div class="col-sm-7">
                        <select id="for_account_manager" type="text" class="form-control"
                                name="account_manager">
                            <option value=""></option>
                            @foreach($users as $user)
                                <option
                                    value="{{$user->id}}" {{ ($customer->cu_account_manager == $user->id) ? 'selected':'' }}>{{$user->us_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="for_debt_manager">Debt
                        Manager:</label>
                    <div class="col-sm-7">
                        <select id="for_debt_manager" type="text" class="form-control"
                                name="debt_manager">
                            <option value=""></option>
                            @foreach($users as $user)
                                <option
                                    value="{{$user->id}}" {{ ($customer->cu_debt_manager == $user->id) ? 'selected':'' }}>{{$user->us_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="for_debtor_status">Debtor
                        status:</label>
                    <div class="col-sm-7">
                        <select id="for_debtor_status" type="text" class="form-control"
                                name="debtor_status">
                            @foreach($debtorstatuses as $id => $item)
                                <option
                                    value="{{$id}}" {{ ($customer->cu_debtor_status == $id) ? 'selected':'' }}>{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label" for="finance_lock">Finance lock:</label>
                    <div class="col-sm-7">
                        <div
                            class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                            <input id="finance_lock" type="checkbox" class="custom-control-input"
                                   name="finance_lock" {{($customer->cu_finance_lock ? "checked" : "")}}>
                            <label class="custom-control-label"
                                   for="finance_lock"></label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label" for="credit_hold">Credit hold:</label>
                    <div class="col-sm-7">
                        <div
                            class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                            <input id="credit_hold" type="checkbox" class="custom-control-input"
                                   name="credit_hold" {{($customer->cu_credit_hold ? "checked" : "")}}>
                            <label class="custom-control-label"
                                   for="credit_hold">@if($customer->cu_credit_hold_timestamp != "0000-00-00 00:00:00" && $customer->cu_credit_hold == 1) {{$customer->cu_credit_hold_timestamp}} @endif</label>
                            <input id="credit_hold_hidden" type="hidden"
                                   name="credit_hold_hidden" value="{{$customer->cu_credit_hold}}">
                        </div>
                    </div>
                </div>

                <div class="form-group row" id="debt_collector_div">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label" for="debt_collector">Debt collector:</label>
                    <div class="col-sm-7">
                        <div
                            class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                            <input id="debt_collector" type="checkbox" class="custom-control-input"
                                   name="debt_collector" {{($customer->cu_debt_collector ? "checked" : "")}}>
                            <label class="custom-control-label"
                                   for="debt_collector"></label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="for_payment_reminder_status">Payment
                        reminder status:</label>
                    <div class="col-sm-7">
                        <select id="for_payment_reminder_status" type="text" class="form-control"
                                name="payment_reminder_status">
                            @foreach($paymentreminderstatuses as $id => $item)
                                <option
                                    value="{{$id}}" {{ ($customer->cu_payment_reminder_status == $id) ? 'selected':'' }}>{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label" for="skip_for_overdue_cronjob">Skip next automatic payment reminder:</label>
                    <div class="col-sm-7">
                        <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                            <input id="skip_for_overdue_cronjob" type="checkbox" class="custom-control-input"
                                   name="skip_for_overdue_cronjob" {{($customer->cu_skip_for_overdue_cronjob ? "checked" : "")}}>
                            <label class="custom-control-label"
                                   for="skip_for_overdue_cronjob"></label>
                            <input id="skip_for_overdue_cronjob_hidden" type="hidden"
                                   name="skip_for_overdue_cronjob_hidden" value="{{$customer->cu_skip_for_overdue_cronjob}}">
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-6">
                <div id="payment_method" class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="for_payment_method">Payment
                        method:</label>
                    <div class="col-sm-7">
                        <select id="for_payment_method" type="text" class="form-control"
                                name="payment_method">
                            @foreach($paymentmethods as $id => $item)
                                <option
                                        value="{{$id}}" {{ ($customer->cu_payment_method == $id) ? 'selected':'' }}>{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div id="pp_charge" @if ($customer->cu_payment_method != 5 && $customer->cu_payment_method != 6) style="display:none;" @endif>
                    <div class="form-group row">
                        <div class="col-sm-1"></div>
                        <label class="col-sm-3 col-form-label"
                               for="monthly_budget">Charge budget:</label>
                        <div class="col-sm-7">
                            <input id="monthly_budget" type="number" class="form-control"
                                   name="monthly_budget"
                                   value="{{$customer->cu_pp_monthly_budget}}">
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-11"><i style="font-size:14px;">This is the budget that will be charged when the threshold is reached.</i></div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-1"></div>
                        <label class="col-sm-3 col-form-label"
                               for="charge_threshold">Charge threshold (FC):</label>
                        <div class="col-sm-7">
                            <input id="charge_threshold" type="number" class="form-control"
                                   name="charge_threshold"
                                   value="{{$customer->cu_pp_charge_threshold}}">
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-11"><i style="font-size:14px;">When the balance is below this threshold the partner will be automatically charged.</i></div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="for_payment_currency">Payment
                        currency:</label>
                    <div class="col-sm-7">
                        <select id="for_payment_currency" type="text" class="form-control"
                                name="payment_currency">
                            @foreach($paymentcurrencies as $pc)
                                <option
                                    value="{{$pc->pacu_code}}" {{ ($customer->cu_pacu_code == $pc->pacu_code) ? 'selected':'' }}>{{$pc->pacu_name." (".$pc->pacu_token.")"}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="invoice_period">Invoice period:</label>
                    <div class="col-sm-7">
                        <select type="text" class="form-control"
                                name="invoice_period">
                            @foreach($invoiceperiods as $id => $period)
                                <option value="{{$id}}" {{ ($customer->cu_invoice_period == $id) ? 'selected':'' }}>{{$period}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="for_payment_term">Payment
                        term:</label>
                    <div class="col-sm-7">
                        <input id="for_payment_term" type="text" class="form-control"
                               name="payment_term"
                               value="{{$customer->cu_payment_term}}">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="for_credit_limit">Credit
                        limit:</label>
                    <div class="col-sm-3">
                        <input id="for_credit_limit" type="text" class="form-control"
                               name="credit_limit" @if($customer->cu_payment_method == 4 || $customer->cu_payment_method == 5 || $customer->cu_payment_method == 6 || !empty($customer->cu_credit_limit_original)) disabled="disabled" @endif
                               value="{{$customer->cu_credit_limit}}">
                    </div>
                    <div class="col-sm-4 @if(!empty($customer->cu_credit_limit_original)) mt-2 @endif">
                        @if(!empty($customer->cu_credit_limit_original))
                            <a href="remove_inc_credit_limit" class="remove_inc_credit_limit"><i style="color:red;" class="fa fa-times"></i> Remove increased CL</a>
                        @else
                            <a href="increase_credit_limit" class="btn btn-primary">Increase temporarily</a>
                        @endif


                    </div>
                </div>
                @if(!empty($customer->cu_credit_limit_original))
                    <div class="form-group row">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-11" style="font-weight:800;">Credit limit was {{$customer->cu_credit_limit_original}}. <br />Increased Credit limit will be automatically removed at {{$customer->cu_credit_limit_increase_timestamp}}</div>

                    </div>
                @endif
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="for_ledger_account">Ledger
                        account:</label>
                    <div class="col-sm-7">
                        <select id="for_ledger_account" type="text" class="form-control"
                                name="ledger_account">
                            @foreach($ledgeraccounts as $la)
                                <option value="{{$la->leac_number}}" {{ ($customer->cu_leac_number == $la->leac_number) ? 'selected':'' }}>{{$la->leac_number." (".$la->leac_name.")"}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="for_vat_number">VAT
                        number:</label>
                    <div class="col-sm-7">
                        <input id="for_vat_number" type="text" class="form-control"
                               name="vat_number"
                               value="{{$customer->cu_vat_number}}">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="for_finance_remark">Remark:</label>
                    <div class="col-sm-7">
                            <textarea id="for_finance_remark" type="text" class="form-control"
                                      name="finance_remark">{{$customer->cu_finance_remarks}}</textarea>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="form-group col-md-8">
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

@push( 'scripts' )
    <script>
        jQuery(document).ready(function(){
            jQuery( document ).on( 'click', '.remove_inc_credit_limit', function(e) {

                if(confirm('Are you sure you want to remove the increased credit limit?')){
                    return true;
                }
                else {
                    e.preventDefault();
                    return false;
                }
            });

            $( "select[name=payment_method]" ).change(function() {
                var div_pp_charge= jQuery('#pp_charge');

                if(jQuery(this).val() == 5 || jQuery(this).val() == 6)
                {
                    div_pp_charge.slideDown();
                }
                else {
                    div_pp_charge.slideUp();
                }
            });
        });
    </script>
@endpush
