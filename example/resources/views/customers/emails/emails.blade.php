<div class="block-content">
    <form method="post" action="{{action('CustomersController@update', $customer->cu_id)}}">
        @csrf
        <input name="_method" type="hidden" value="PATCH">
        <input name="form_name" type="hidden" value="Emails">

        <h2 class="content-heading pt-0">Emails</h2>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-2 col-form-label"
                           for="for_primary_contact_email">Primary contact email:</label>
                    <div class="col-sm-4">
                        <input id="for_primary_contact_email" type="text" class="form-control"
                               name="primary_contact_email" disabled value="{{$customer->cu_attn_email}}">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-2 col-form-label" for="for_cu_email">Leads email:</label>
                    <div class="col-sm-4">
                        <input id="for_cu_email" type="text" class="form-control" name="cu_email" disabled
                               value="{{$customer->cu_email}}">
                    </div>
                    @if($hasdeliveries)
                        <label class="col-sm-5 col-form-label" style="color:red" for="for_primary_contact_email">Overwritten by request deliveries (Portals)</label>
                    @endif
                </div>


                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-2 col-form-label" for="for_billing_email">Billing email:</label>
                    <div class="col-sm-4">
                        <input id="for_billing_email" type="text" class="form-control" name="billing_email" disabled value="{{$customer->cu_email_bi}}">
                    </div>
                    @if($customer->cu_use_billing_address == 0)
                        <label class="col-sm-5 col-form-label" style="color:red" for="for_primary_contact_email">Overwritten by Leads email.</label>
                    @endif
                </div>
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-2 col-form-label"
                           for="for_review_communication">Review communication:</label>
                    <div class="col-sm-4">
                        <input id="for_review_communication" type="text" class="form-control"
                               name="review_communication"
                               disabled value="{{$moverdata->moda_review_email}}">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-2 col-form-label"
                           for="for_load_exchange_communication">Load Exchange communication:</label>
                    <div class="col-sm-4">
                        <input id="for_load_exchange_communication" type="text" class="form-control"
                               name="load_exchange_communication"
                               disabled value="{{$moverdata->moda_load_exchange_email}}">
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
