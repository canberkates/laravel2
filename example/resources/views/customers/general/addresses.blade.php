

<div class="block-content">
    <form method="post" action="{{action('CustomersController@update', $customer->cu_id)}}">
        @csrf
        <input name="_method" type="hidden" value="PATCH">
        <input name="form_name" type="hidden" value="Addresses">
        <div class="row">
            <div class="col-md-6">
                <h2 class="content-heading pt-0">Addresses</h2>
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="street_1">Company street
                        1:</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control"
                               name="street_1"
                               value="{{$customer->cu_street_1}}">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="street_2">Company street
                        2:</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control"
                               name="street_2"
                               value="{{$customer->cu_street_2}}">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label" for="zipcode">Company zipcode:</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control" name="zipcode"
                               value="{{$customer->cu_zipcode}}">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="city">Company city:</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control"
                               name="city"
                               value="{{$customer->cu_city}}">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="country">Company country:</label>
                    <div class="col-sm-7">
                        <select type="text" class="js-select2 form-control"
                                name="country">
                            <option value=""></option>
                            @foreach($countries as $country)
                                <option
                                    value="{{$country->co_code}}" {{ ($customer->cu_co_code == $country->co_code) ? 'selected':'' }}>{{$country->co_en}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="telephone">Company telephone:</label>
                    <div class="col-sm-7">
                        <input type="tel" name="telephone" class="form-control" value="{{$customer->cu_telephone}}">
                        <input type="hidden" id="int_telephone" name="int_telephone" value="">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="website">Company website:</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control"
                               name="website"
                               value="{{$customer->cu_website}}">
                    </div>
                    <div class="col-sm-1">
                        <a target="_blank" href="{{$customer->cu_website}}"><i class="fa fa-external-link-alt"></i></a>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="form-group col-md-8">
                        <button name="office_update"
                                style="display:{{($customer->cu_use_billing_address ? "none;" : "block;")}}"
                                type="submit" class="btn btn-primary">
                            Update
                        </button>
                    </div>
                </div>

            </div>

            <div class="col-md-6">


                <h2 class="content-heading pt-0">
                    Use Billing Address
                    <div style="padding-top: 5px;padding-left: 10px;"
                         class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                        <input type="checkbox"
                               class="custom-control-input"
                               id="use_billing_address"
                               name="use_billing_address" {{($customer->cu_use_billing_address ? "checked" : "")}}>
                        <label class="custom-control-label"
                               for="use_billing_address"></label>
                    </div>
                </h2>

                <div name="show_billing_address"
                     style="display:{{($customer->cu_use_billing_address ? "block;" : "none;")}}">

                    <div class="form-group row">
                        <div class="col-sm-1"></div>
                        <label class="col-sm-3 col-form-label"
                               for="street_1_bi">Company street
                            1:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control"
                                   name="street_1_bi"
                                   value="{{$customer->cu_street_1_bi}}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-1"></div>
                        <label class="col-sm-3 col-form-label"
                               for="street_2_bi">Company street
                            2:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control"
                                   name="street_2_bi"
                                   value="{{$customer->cu_street_2_bi}}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-1"></div>
                        <label class="col-sm-3 col-form-label"
                               for="zipcode_bi">Company
                            zipcode:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control"
                                   name="zipcode_bi"
                                   value="{{$customer->cu_zipcode_bi}}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-1"></div>
                        <label class="col-sm-3 col-form-label"
                               for="city_bi">Company
                            city:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control"
                                   name="city_bi"
                                   value="{{$customer->cu_city_bi}}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-1"></div>
                        <label class="col-sm-3 col-form-label"
                               for="country_bi">Company
                            country:</label>
                        <div class="col-sm-7">
                            <select type="text" class="js-select2 form-control"
                                    name="country_bi">
                                <option value=""></option>
                                @foreach($countries as $country)
                                    <option
                                        value="{{$country->co_code}}" {{ ($customer->cu_co_code_bi == $country->co_code) ? 'selected':'' }}>{{$country->co_en}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-1"></div>
                        <label class="col-sm-3 col-form-label"
                               for="telephone_bi">Company
                            telephone:</label>
                        <div class="col-sm-7">
                            <input type="tel" name="telephone_bi" class="form-control" value="{{$customer->cu_telephone_bi}}">
                            <input type="hidden" id="int_telephone_bi" name="int_telephone_bi" value="">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-1"></div>
                        <label class="col-sm-3 col-form-label"
                               for="website_bi">Company
                            website:</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control"
                                   name="website_bi"
                                   value="{{$customer->cu_website_bi}}">
                        </div>
                        @if (!empty($customer->cu_website_bi))
                            <div class="col-sm-1">
                                <a target="_blank" href="{{$customer->cu_website_bi}}"><i class="fa fa-external-link-alt"></i></a>
                            </div>
                        @endif
                    </div>

                    <div class="row">
                        <div class="col-sm-1"></div>
                        <div class="form-group col-md-8">
                            <button type="submit"
                                    class="btn btn-primary">
                                Update
                            </button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </form>
</div>

@push('scripts')
    <script>
        $(document).ready(function () {

            firstInput();
            secondInput();

            function firstInput(){
                var input = document.querySelector("input[name=telephone]");
                output = $("#int_telephone");

                var iti = window.intlTelInput(input, {
                    nationalMode: true,
                    separateDialCode: true
                });

                var handleChange = function() {
                    var text = (iti.isValidNumber()) ? iti.getNumber() : 0;
                    if(text == 0){
                        $('input[name=telephone]').addClass("is-invalid");
                    }else{
                        $('input[name=telephone]').removeClass("is-invalid");
                    }
                    output.val(text);
                };

                input.addEventListener('change', handleChange);
                input.addEventListener('keyup', handleChange);

                handleChange();
            }

            function secondInput(){
                var input2 = document.querySelector("input[name=telephone_bi]");
                output2 = $("#int_telephone_bi");

                var iti2 = window.intlTelInput(input2, {
                    nationalMode: true,
                    separateDialCode: true
                });

                var handleChange = function() {
                    var text = (iti2.isValidNumber()) ? iti2.getNumber() : 0;
                    if(text == 0){
                        $('input[name=telephone_bi]').addClass("is-invalid");
                    }else{
                        $('input[name=telephone_bi]').removeClass("is-invalid");
                    }
                    output2.val(text);
                };

                input2.addEventListener('change', handleChange);
                input2.addEventListener('keyup', handleChange);

                handleChange();

            }
        });
    </script>
@endpush
