<div class="block-content">
    <form method="post" action="{{action('CustomersController@update', $customer->cu_id)}}">
        @csrf
        <input name="_method" type="hidden" value="PATCH">
        <input name="form_name" type="hidden" value="Fields">
        <div class="row">
            <div class="col-md-12">
                <h2 class="content-heading pt-0">Fields</h2>

                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="finance_payment_currency">Region:</label>
                    <div class="col-sm-7">
                        <select type="text" class="js-select2 form-control"
                                name="fields_region">
                            <option value=""></option>
                            @foreach($regions as $region)
                                <option
                                    value="{{$region->reg_id}}" {{ ($customer->moverdata->moda_reg_id == $region->reg_id) ? 'selected':'' }}>{{$region->reg_parent}} {{"(".$region->co_en.")"}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="finance_payment_currency">Market:</label>
                    <div class="col-sm-7">
                        <select type="text" class="form-control"
                                name="fields_market">
                            <option value=""></option>
                            @foreach($customermarkettypes as $id => $markettype)
                                <option value="{{$id}}" {{ ($customer->moverdata->moda_market_type === $id) ? 'selected':'' }}>{{$markettype}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="finance_payment_currency">Services:</label>
                    <div class="col-sm-7">
                        <select type="text" class="form-control"
                                name="fields_services">
                            <option value=""></option>
                            @foreach($customerservices as $id => $service)
                                <option value="{{$id}}" {{ ($customer->moverdata->moda_services == $id) ? 'selected':'' }}>{{$service}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="finance_payment_currency">Status:</label>
                    <div class="col-sm-7">
                        <select type="text" class="form-control"
                                name="fields_status">
                            <option value=""></option>
                            @foreach($customerstatustypes as $id => $statustype)
                                <option value="{{$id}}" {{ ($customer->moverdata->moda_crm_status == $id) ? 'selected':'' }}>{{$statustype}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                {{--

                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="finance_payment_currency">Service:</label>
                    <div class="col-sm-7">
                        <select type="text" class="form-control"
                                name="fields_service">
                            <option value=""></option>
                            @foreach($customerservicetypes as $id => $servicetype)
                                <option value="{{$id}}" {{ ($customer->cu_pacu_code== $servicetype) ? 'selected':'' }}>{{$servicetype}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>


                --}}

                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="finance_payment_currency">Source:</label>
                    <div class="col-sm-7">
                        <select type="text" class="form-control"
                                name="fields_source">
                            <option value=""></option>
                            @foreach($customersource_types as $id => $statustype)
                                <option value="{{$id}}" {{ ($customer->moverdata->moda_source == $id) ? 'selected':'' }}>{{$statustype}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>


                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label"
                           for="finance_payment_currency">Owner:</label>
                    <div class="col-sm-7">
                        <select type="text" class="js-select2 form-control"
                                name="fields_owner">
                            <option value=""></option>
                            @foreach($users as $user)
                                <option value="{{$user->id}}" {{ ($customer->moverdata->moda_owner == $user->id) ? 'selected':'' }}>{{$user->us_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>



                <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="form-group col-md-8">
                        <button type="submit"
                                class="btn btn-primary">
                            Update
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </form>
</div>
