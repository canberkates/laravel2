<form class=" mt-5" method="post" action="{{action('CustomersController@getInvoices', $customer->cu_id)}}">
    @csrf
    <input name="_method" type="hidden" value="post">

    <div class="form-group row">
        <label class="col-sm-1 col-form-label ml-5"
               for="invoice_period">Paid:</label>
        <div class="col-sm-2">
            <select type="text" class="form-control"
                    name="paid">
                <option @if(isset($invoices_paid_filter) && $invoices_paid_filter != null && $invoices_paid_filter == "both") selected @endif value="both" @if($invoices_paid_filter == null) selected @endif>Both</option>
                <option @if(isset($invoices_paid_filter) && $invoices_paid_filter != null && $invoices_paid_filter == "1") selected @endif  value="1">Yes</option>
                <option @if(isset($invoices_paid_filter) && $invoices_paid_filter != null && $invoices_paid_filter == "0") selected @endif  value="0">No</option>
            </select>
        </div>
    </div>
    <div class="row">
        <div class="col-md-1"></div>
        <div class="form-group col-md-8">
            <button type="submit" class="btn btn-primary">Filter</button>
        </div>
    </div>
</form>

@if($invoices_paid_filter != null)
    <div class="block-content block-content-full">
        <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
            <thead>
            <tr>
                <th>Timestamp</th>
                <th>Invoice number</th>
                <th>Invoice date</th>
                <th>Expiration date</th>
                <th>Days overdue</th>
                <th>Reminder</th>
                <th>Payment method</th>
                <th>Amount excl. VAT €</th>
                <th>Amount incl. VAT €</th>
                <th>Amount VAT FC</th>
                <th>Paid</th>
                <th>Amount paid</th>
                <th>Amount left</th>
                <th>Payment date(s)</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($invoices as $invoice)
                @if($invoices_paid_filter == "both" || ($invoices_paid_filter == "1" && ($invoice->in_amount_netto_eur == round($amount_paired_per_invoice[$invoice->in_id], 2))) || ($invoices_paid_filter == "0" && ($invoice->in_amount_netto_eur != round($amount_paired_per_invoice[$invoice->in_id], 2))))
                    <tr>
                        <td>{{$invoice->in_timestamp}}</td>
                        <td>{{$invoice->in_number}}</td>
                        <td>{{$invoice->in_date}}</td>
                        <td>{{$invoice->in_expiration_date}}</td>
                        <td>@if ($invoice->in_amount_netto_eur != round($amount_paired_per_invoice[$invoice->in_id], 2) && $invoice->days_overdue > 0) {{$invoice->days_overdue}} @else {{"0"}} @endif</td>
                        <td>{{$paymentreminder[$invoice->in_payment_reminder]}}</td>
                        <td>{{$paymentmethods[$invoice->in_payment_method]}}</td>
                        <td class="align_right_column">{{$currency_tokens["EUR"]." ".number_format($invoice->in_amount_gross_eur, 2, ",", ".")}}</td>
                        <td class="align_right_column">{{$currency_tokens["EUR"]." ".number_format($invoice->in_amount_netto_eur, 2, ",", ".")}}</td>
                        <td class="align_right_column">{{$currency_tokens[$invoice->in_currency]." ".number_format($invoice->in_amount_gross, 2, ",", ".")}}</td>
                        <td>{{(($invoice->in_amount_netto_eur == round($amount_paired_per_invoice[$invoice->in_id], 2)) ? "Yes" : "No")}}</td>
                        <td class="align_right_column">{{$currency_tokens["EUR"]." ".number_format($amount_paired_per_invoice[$invoice->in_id], 2, ",", ".")}}</td>
                        <td class="align_right_column">{{$currency_tokens["EUR"]." "}}{{number_format(round($invoice->in_amount_netto_eur - $amount_paired_per_invoice[$invoice->in_id], 2), 2, ",", ".")}}</td>
                        <td>@if(!empty($payment_dates_per_invoice[$invoice->in_id])) {{implode(", ", $payment_dates_per_invoice[$invoice->in_id])}} @else {{"-"}} @endif</td>
                        <td class="text-center">
                            <div class="btn-group">
                                <button type="button" class="btn btn-sm btn-primary invoice_view mr-1" data-toggle="tooltip"
                                        title="View" data-invoice_id="{{$invoice->in_id}}">
                                    <i class="fa fa-eye"></i>
                                </button>
                                <a class="btn btn-sm btn-primary mr-1" data-toggle="tooltip"
                                   data-placement="left"
                                   href="{{ url('customers/' . $customer->cu_id . '/invoice/' . $invoice->in_id.'/send_invoice')}}">
                                    <i class="fa fa-envelope"></i>
                                </a>
                                <a class="btn btn-sm btn-primary mr-1" data-toggle="tooltip"
                                   data-placement="left"
                                   target="_blank"
                                   href="../../invoice_download.php?invoice={{$invoice->in_number}}&date={{$invoice->in_date}}&env={{env("SHARED_FOLDER")}}">
                                    <i class="fa fa-download"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                @endif
            @endforeach
            </tbody>
            <tfoot>
                <th colspan="7"></th>
                <th class="align_right_column">€{{number_format($invoice_totals['total_amount_gross'], 2, ',', '.')}}</th>
                <th class="align_right_column">€{{number_format($invoice_totals['total_amount_netto'], 2, ',', '.')}}</th>
                <th colspan="2"></th>
                <th class="align_right_column">€{{number_format($invoice_totals['total_amount_paid'], 2, ',', '.')}}</th>
                <th class="align_right_column">€{{number_format($invoice_totals['total_amount_netto'] - $invoice_totals['total_amount_paid'], 2, ',', '.')}}</th>
                <th colspan="2"></th>
            </tfoot>
        </table>
    </div>
@endif

@push( 'scripts' )
    <script>
        jQuery(document).ready(function(){
            $('.align_right_column').css('text-align', 'right');
        });
    </script>
@endpush
