<div class="block-header block-header-default">
    <h3 class="block-title">
        <a href="{{ url('customers/' . $customer->cu_id . '/customerdocument/create')}}">
            <button class="btn btn-primary">Add a document</button>
        </a>

    </h3>
</div>
<div class="block-content block-content-full">
    <ul class="nav nav-tabs nav-tabs-alt" data-toggle="tabs" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" href="#btabs-alt-static-current">Current</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#btabs-alt-static-archived">Archived</a>
        </li>
    </ul>
    <div class="block-content tab-content">
        <div class="tab-pane active" id="btabs-alt-static-current" role="tabpanel">
            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Timestamp</th>
                    <th>Uploaded by</th>
                    <th>Filename</th>
                    <th>Type</th>
                    <th>Description</th>
                    @if($customer->cu_co_code == "US")
                        <th>Number</th>
                    @endif
                    <th>Document</th>
                    <th>Archive</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($documents as $document)
                    <tr>
                        <td>{{$document->cudo_id}}</td>
                        <td>{{$document->cudo_timestamp}}</td>
                        <td>{{isset($document->user) ? $document->user->us_name : 'Unknown'}}</td>
                        <td>{{$document->cudo_filename}}</td>
                        <td>{{isset($document->cudo_type) ? $customerdocumenttypes[$document->cudo_type] : ''}}</td>
                        <td>{{$document->cudo_description}}</td>
                        @if($customer->cu_co_code == "US")
                            <td>{{$document->cudo_number}}</td>
                        @endif

                        <td>
                            <a target="_blank" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="left" title="Download" href="{{ url('customers/' . $customer->cu_id . '/document/'.$document['cudo_id'].'/download')}}">
                                <i class="fa fa-download"></i>
                            </a>
                        </td>
                        <td>
                            <a class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="left" title="Archive" href="{{ url('customers/' . $customer->cu_id . '/document/'.$document['cudo_id'].'/archive')}}">
                                <i class="fa fa-minus"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <div class="tab-pane" id="btabs-alt-static-archived" role="tabpanel">
            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Timestamp</th>
                    <th>Uploaded by</th>
                    <th>Filename</th>
                    <th>Type</th>
                    <th>Description</th>
                    <th>Document</th>
                    <th>Archive</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($documents_archived as $document)
                    <tr>
                        <td>{{$document->cudo_id}}</td>
                        <td>{{$document->cudo_timestamp}}</td>
                        <td>{{isset($document->user) ? $document->user->us_name : 'Unknown'}}</td>
                        <td>{{$document->cudo_filename}}</td>
                        <td>{{isset($document->cudo_type) ? $customerdocumenttypes[$document->cudo_type] : ''}}</td>
                        <td>{{$document->cudo_description}}</td>

                        <td>
                            <a target="_blank" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="left" title="Download" href="{{ url('customers/' . $customer->cu_id . '/document/'.$document['cudo_id'].'/download')}}">
                                <i class="fa fa-download"></i>
                            </a>
                        </td>
                        <td>
                            <a class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="left" title="Recover" href="{{ url('customers/' . $customer->cu_id . '/document/'.$document['cudo_id'].'/recover')}}">
                                <i class="fa fa-redo"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
