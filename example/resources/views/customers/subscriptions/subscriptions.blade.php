<div class="block-header block-header-default">
    <h3 class="block-title">
        <a href="{{ url('customers/' . $customer->cu_id . '/subscription/create')}}">
            <button class="btn btn-primary">Add subscription</button>
        </a>
    </h3>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="block block-rounded block-bordered">
            <ul class="nav nav-tabs nav-tabs-block js-tabs-enabled" data-toggle="tabs"
                role="tablist">
                <li class="nav-item">
                    <a class="nav-link active show" href="#btabs-static-active">Active
                        ({{sizeof($subscriptions['active'])}})</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#btabs-static-upcoming">Upcoming
                        ({{sizeof($subscriptions['upcoming'])}})</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#btabs-static-expired">Expired
                        ({{sizeof($subscriptions['expired'])}})</a>
                </li>
            </ul>
            <div class="block-content tab-content">
                <div class="tab-pane active show" id="btabs-static-active" role="tabpanel">
                    <div class="block block-rounded block-bordered">
                        <div class="block-content block-content-full">
                            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Price</th>
                                    <th>Price this month</th>
                                    <th>Start date</th>
                                    <th>End date</th>
                                    <th>Cancellation date</th>
                                    <th>Payment Interval</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($subscriptions['active'] as $sub)
                                    <tr>
                                        <td>{{$sub->ktcusu_id}}</td>
                                        <td>{{$sub->subscription->sub_name}}</td>
                                        <td>{{$paymentcurrencies_sub[$sub->ktcusu_pacu_code]." ".$sub->ktcusu_price}}</td>
                                        @if($sub->ktcusu_payment_interval == 1)
                                        <td>{{$paymentcurrencies_sub[$sub->ktcusu_pacu_code]." ".$sub->ktcusu_price_this_month}}</td>
                                        @else
                                            <td>-</td>
                                        @endif
                                        <td>{{$sub->ktcusu_start_date}}</td>
                                        <td>{{$sub->ktcusu_end_date}}</td>
                                        <td>{{$sub->ktcusu_cancellation_date}}</td>
                                        <td>{{(($sub->ktcusu_payment_interval == 1) ? "Monthly" : "Yearly")}}</td>
                                        <td class="text-center">
                                            <div class="btn-group">
                                                @if(empty($sub->ktcusu_cancellation_date))
                                                    <button type="button" class="btn btn-sm btn-primary subscription_cancel" data-toggle="tooltip"
                                                            title="Cancel" data-id="{{$sub->ktcusu_id}}">
                                                        <i class="fa fa-minus"></i>
                                                    </button>
                                                @else
                                                    {{"Cancelled"}}
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="btabs-static-upcoming" role="tabpanel">
                    <div class="block block-rounded block-bordered">
                        <div class="block-content block-content-full">
                            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Price</th>
                                    <th>Start date</th>
                                    <th>End date</th>
                                    <th>Cancellation date</th>
                                    <th>Payment Interval</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($subscriptions['upcoming'] as $sub)
                                    <tr>
                                        <td>{{$sub->ktcusu_id}}</td>
                                        <td>{{$sub->subscription->sub_name}}</td>
                                        <td>{{$paymentcurrencies_sub[$sub->ktcusu_pacu_code]." ".$sub->ktcusu_price}}</td>
                                        <td>{{$sub->ktcusu_start_date}}</td>
                                        <td>{{$sub->ktcusu_end_date}}</td>
                                        <td>{{$sub->ktcusu_cancellation_date}}</td>
                                        <td>{{(($sub->ktcusu_payment_interval == 1) ? "Monthly" : "Yearly")}}</td>
                                        <td class="text-center">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-sm btn-primary subscription_delete" data-toggle="tooltip"
                                                        title="Delete" data-id="{{$sub->ktcusu_id}}">
                                                    <i class="fa fa-times"></i>
                                                </button>
                                            </div>
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="btabs-static-expired" role="tabpanel">
                    <div class="block block-rounded block-bordered">
                        <div class="block-content block-content-full">
                            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Price</th>
                                    <th>Start date</th>
                                    <th>End date</th>
                                    <th>Cancellation date</th>
                                    <th>Payment Interval</th>
                                    {{--<th>Actions</th>--}}
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($subscriptions['expired'] as $sub)
                                    <tr>
                                        <td>{{$sub->ktcusu_id}}</td>
                                        <td>{{$sub->subscription->sub_name}}</td>
                                        <td>{{$paymentcurrencies_sub[$sub->ktcusu_pacu_code]." ".$sub->ktcusu_price}}</td>
                                        <td>{{$sub->ktcusu_start_date}}</td>
                                        <td>{{$sub->ktcusu_end_date}}</td>
                                        <td>{{$sub->ktcusu_cancellation_date}}</td>
                                        <td>{{(($sub->ktcusu_payment_interval == 1) ? "Monthly" : "Yearly")}}</td>
                                        {{--<td class="text-center">
                                            <div class="btn-group">
                                                <a class="btn btn-sm btn-primary"
                                                   data-toggle="tooltip"
                                                   data-placement="left"
                                                   title="edit"
                                                   href="{{ url('customers/' . $customer->cu_id . '/customerportal/' . $customerportal->ktcupo_id.'/edit')}}">
                                                    <i class="fa fa-pencil-alt"></i>
                                                </a>
                                            </div>
                                        </td>--}}
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>

@push( 'scripts' )
    <script>
        jQuery(document).ready(function(){
            jQuery( document ).on( 'click', '.subscription_cancel', function(e) {

                e.preventDefault();

                var $self = jQuery(this);

                confirmCancel("{{ url('ajax/subscription/cancel') }}", 'get', {id:$self.data('id')}, function() {
                    location.reload();
                });

                function confirmCancel( $url, $method, $data, $callback = '', $button_text = "Yes, cancel it!") {
                    swal({
                        title: "Are you sure?",
                        text: "",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: $button_text,
                        preConfirm: function() {
                            return new Promise(function(resolve) {
                                jQuery.ajax({
                                    url: $url,
                                    method: $method,
                                    data: $data,
                                    success: function( $result) {
                                        swal("Done!", "It was successfully cancelled!", "success");
                                        console.log( $result);

                                        if( typeof $callback == 'function' ) {

                                            $callback.call( $result );
                                        }
                                    },
                                    error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail

                                        swal("Error cancelling!", errorThrown, "error");
                                        console.log(JSON.stringify(jqXHR));
                                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                                    }
                                });
                            });
                        }
                    });
                }
            });

            jQuery( document ).on( 'click', '.subscription_delete', function(e) {

                e.preventDefault();

                var $self = jQuery(this);

                confirmDelete("{{ url('ajax/subscription/delete') }}", 'get', {id:$self.data('id')}, function() {
                    $self.parents( '.dataTable' ).DataTable().row( $self.parents( 'tr' ) ).remove().draw( 'page' );
                });
            });
        });
    </script>

@endpush
