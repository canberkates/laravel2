@extends('layouts.backend')

@include('scripts.select2')
@include('scripts.datepicker')

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Add subscription to {{$customer->cu_company_name_business}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../../">@if($customer->cu_type == 1) {{"Customers"}} @else {{"Resellers"}} @endif</a></li>
                        <li class="breadcrumb-item"><a href="../edit">{{$customer->cu_company_name_business}}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Add Subscription</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="block block-rounded block-bordered">
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    {{$error}}<br>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="block-header block-header-default">
                        <h3 class="block-title">General</h3>
                    </div>
                    <div class="block-content">
                        <form class="mb-5" method="post" action="{{action('CustomerSubscriptionController@store', $customer->cu_id)}}">
                            @csrf
                            <input name="_method" type="hidden" value="post">
                            <input name="customer_id" type="hidden" value="{{$customer->cu_id}}">

                            <div class="row">
                                <div class="col-md-7">
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="subscription">Subscription: <span style="color:red;">*</span></label>
                                        <div class="col-sm-7">
                                            <select type="text" class="form-control"
                                                    name="subscription">
                                                <option value=""></option>
                                                @foreach($subscriptions as $sub)
                                                    <option value="{{$sub->sub_id}}">{{$sub->sub_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="start_date">Start date: <span style="color:red;">*</span></label>
                                        <div class="col-sm-7">
                                            <input autocomplete="off" type="text"
                                                   class="js-datepicker form-control" id="start_date" name="start_date"
                                                   data-week-start="1" data-autoclose="true" data-today-highlight="true"
                                                   data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd">
                                        </div>
                                    </div>

                                    {{--<div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="cancellation_date">Cancellation date:</label>
                                        <div class="col-sm-7">
                                            <input autocomplete="off" type="text"
                                                   class="js-datepicker form-control" id="cancellation_date" name="cancellation_date"
                                                   data-week-start="1" data-autoclose="true" data-today-highlight="true"
                                                   data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd">
                                        </div>
                                    </div>--}}

                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="cancellation_date">Price {{$currency}}: <span style="color:red;">*</span></label>
                                        <div class="col-sm-7">
                                            <input autocomplete="off" class="form-control"  type="text" name="price">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="payment_interval">Payment interval: <span style="color:red;">*</span></label>
                                        <div class="col-sm-7">
                                            <select type="text" class="form-control"
                                                    name="payment_interval">
                                                <option value=""></option>
                                                <option value="1">Monthly</option>
                                                <option value="2">Yearly</option>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <h2 class="content-heading pt-0"></h2>

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Add</button>
                                    <a class="btn btn-primary" href="../edit">
                                        Back
                                    </a>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

