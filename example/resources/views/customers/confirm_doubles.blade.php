@extends('layouts.backend')

@include( 'scripts.datatables' )
@include( 'scripts.dialogs' )

@section('content')
    <!-- Page Content -->
    <div class="content">
        <!-- Your Block -->
        <div class="block block-rounded block-bordered">
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Confirming...
                </h3>
            </div>
            <div class="block-content block-content-full">
                <form method="post"
                      action="{{action('CustomersController@store')}}">
                    @csrf
                    <input name="_method" type="hidden" value="post">
                    <input type="hidden" name="full_request" value="{{$request}}" />
                    <div class="row">
                        <div class="col-md-6">
                            {!! $double_company_message_html !!}

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" name="confirmed" class="btn btn-primary">Yes, create anyways!</button>
                                    <a href="/customers" class="btn btn-primary">Back to Customers</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END Your Block -->
    </div>
    <!-- END Page Content -->
@endsection
