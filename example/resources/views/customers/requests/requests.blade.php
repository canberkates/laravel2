<div class="block block-rounded block-themed" id="request_chart_div" style="display: none;">
	<div class="block-header block-header-default">
		<h3 class="block-title">Number of requests received in the last 60 days</h3>
	</div>
	<div class="block-content d-flex align-items-center justify-content-between">
		<div class="block block-rounded block-bordered">
			<canvas width="2000" height="500" id="request_bar_chart" class="js-chartjs-bars"
			        style="max-width: 100%;"></canvas>
			@push( 'scripts' )
				<script>
                    $(document).ready(function () {

                        $("button[name=chart_button]").click(function(){
                           drawChart();
                           $(this).hide();
                        });

                        function drawChart() {
                            $('#request_chart_div').show();

                            var themecolor = $('.btn-primary').css("background-color");

                            var color = Chart.helpers.color;
                            var barChartData = {
                                labels: {!! $chart_labels !!},
                                datasets: [{
                                    label: "Requests received",
                                    backgroundColor: color(themecolor).alpha(0.9).rgbString(),
                                    borderColor: themecolor,
                                    borderWidth: 1,
                                    data: {!! $chart_data !!}
                                }]
                            };

                            new Chart(document.getElementById('request_bar_chart'), {
                                type: 'bar',
                                data: barChartData,
                                options: {
                                    responsive: true,
                                    scales: {
                                        yAxes: [{

                                            ticks: {
                                                beginAtZero: true,
                                                stepSize: 1
                                            }
                                        }],
                                        xAxes: [{
                                            barPercentage: 0.7,
                                        }],
                                    }
                                }
                            });

                        }
                    });
				</script>
			@endpush
		</div>
	</div>

</div>

<button type="button" name="chart_button" class="btn btn-primary" style="margin: 15px;">Show request chart</button>

<form class="mb-5 mt-5" method="post" action="{{action('CustomersController@getRequests', $customer->cu_id)}}">
	@csrf
	<input name="_method" type="hidden" value="post">

	<div class="form-group row">
		<label class="col-md-1 col-form-label ml-5" for="for-date">Date range:</label>
		<div class="col-md-2">
			<input type="text" class="form-control drp drp-default" name="date"
			       @if($request_date_filter != null) value="{{$request_date_filter}}"
			       @else value="{{date("Y/m/d",  strtotime("-30 days"))}} - {{date("Y/m/d")}}" @endif autocomplete="off"/>
		</div>
	</div>

	<div class="row">
		<div class="col-md-1"></div>
		<div class="form-group col-md-8">
			<button type="submit" class="btn btn-primary">Filter</button>
		</div>
	</div>

</form>
@if (!empty($customerportalrequests))
	<div class="block-content block-content-full">
		<table class="table table-bordered table-striped table-vcenter js-dataTable-full">
			<thead>
			<tr>
				<th>ID</th>
				<th>Customer Re ID</th>
				<th>Received</th>
				<th>Matched</th>
				<th>Type</th>
				<th>Portal Type</th>
				<th>Portal</th>
				<th>Request type</th>
				<th>Moving size</th>
				<th>Name</th>
				<th>Email</th>
				<th>Origin</th>
				<th>Destination</th>
				<th>Volume (m3)</th>
				<th>Free reason</th>
				<th>Invoiced</th>
				<th>Actions</th>
			</tr>
			</thead>
			<tbody>
			@foreach ($customerportalrequests as $ktcupo)
				<tr>
					<td>{{$ktcupo->request->re_id}}</td>
					<td>{{$ktcupo->ktrecupo_cu_re_id}}</td>
					<td>{{$ktcupo->request->re_timestamp}}</td>
					<td>{{$ktcupo->ktrecupo_timestamp}}</td>
					<td>{{$requestcustomerportaltypes[$ktcupo->ktrecupo_type]}}</td>
                    @if($ktcupo->request->re_destination_type == 2)
					    <td>{{$nattypes[$ktcupo->request->re_nat_type]}}</td>
                    @else
                        <td>INT</td>
                    @endif
					<td>{{$ktcupo->portal->po_portal}}</td>
					<td>{{$requesttypes[$ktcupo->request->re_request_type]}}</td>
					<td>{{$movingsizes[$ktcupo->request->re_moving_size]}}</td>
					<td>{{$ktcupo->request->re_full_name}}</td>
					<td>{{$ktcupo->request->re_email}}</td>
					<td>{{$ktcupo->request->countryfrom->co_en}}</td>
					<td>{{$ktcupo->request->countryto->co_en}}</td>
					<td>{{$ktcupo->request->re_volume_m3}}</td>
					@if($ktcupo->ktrecupo_free)
						<td>{{$claimreasons[$ktcupo->ktrecupo_free_reason]}}</td>
					@else
						<td>-</td>
					@endif
					<td>{{$yesno[$ktcupo->ktrecupo_invoiced]}}</td>
					<td class="text-center">
						<div class="btn-group">
							<a class="btn btn-sm btn-primary mr-1" data-toggle="tooltip"
							   data-placement="left"
							   title="View"
                               @if($ktcupo->request->re_request_type == 5)
                                   href="{{ url('premium_leads/' . $ktcupo->request->re_id)}}">
                               @else
                                   href="{{ url('requests/' . $ktcupo->request->re_id)}}">
                               @endif
								<i class="fa fa-eye"></i>
							</a>

							@if($ktcupo->ktrecupo_type == 1)
								<a class="btn btn-sm @if($ktcupo->ktrecupo_free) btn-success @else btn-danger @endif mr-1"
								   data-toggle="tooltip"
								   data-placement="left"
								   title="Free"
								   href="{{ url('customerrequest/'.$ktcupo->ktrecupo_id.'/free')}}">
									<i class="fa fa-coins"></i>
								</a>
							@endif

							<a class="btn btn-sm btn-primary mr-1" data-toggle="tooltip"
							   data-placement="left"
							   title="Resend"
							   href="{{ url('customerrequest/'.$ktcupo->ktrecupo_id.'/resend')}}">
								<i class="fa fa-envelope"></i>
							</a>
						</div>
					</td>
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>
@endif
