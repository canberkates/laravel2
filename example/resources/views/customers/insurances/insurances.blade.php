<div class="block-content block-content-full">
    <form method="post"
          action="{{action('CustomersController@update', $customer->cu_id)}}">
        @csrf
        <input name="_method" type="hidden" value="PATCH">
        <input name="form_name" type="hidden" value="Insurances">
        @foreach ($insurances as $insurance)
            <div class="form-group row">

            <label class="col-sm-2 col-form-label mt-3" for="insurance[{{$insurance->cuin_id}}]">{{$insurance->cuin_name}}</label>
                <div class="col-sm-3 mt-3">
                    <select name="insurance[{{$insurance->cuin_id}}]" class="form-control">
                        <option @if(!isset($permits_insurances_verified[$insurance->cuin_id])) selected @endif value="0">We don't know</option>

                        <option
                            @if(isset($permits_insurances_verified[$insurance->cuin_id]) && $permits_insurances_verified[$insurance->cuin_id]['verified'] === 0)
                            selected
                            @endif

                            value="1">
                            Verified and they don't have this permit
                        </option>

                        @if(isset($permits_insurances_verified[$insurance->cuin_id]) && $permits_insurances_verified[$insurance->cuin_id]['verified'] === 1)
                            <option selected value="2">Verified and they have this permit</option>
                        @endif
                    </select>
                </div>
                @if(isset($permits_insurances_verified[$insurance->cuin_id]) && $permits_insurances_verified[$insurance->cuin_id]['verified'] === 1)
                    <div class="col-sm-1 mt-4">Amount</div>
                    <div class="col-sm-2 mt-3"><input class="form-control" type="text" name="insuranceamount[{{$insurance->cuin_id}}]" value="{{$permits_insurances_verified[$insurance->cuin_id]['insurance_amount']}}"/></div>
                @endif
                <div class="col-sm-1 mt-3" style="font-size:14px;">@if(!empty($permits_insurances_verified[$insurance->cuin_id]['verified_timestamp'])) {{"Verified on: ".$permits_insurances_verified[$insurance->cuin_id]['verified_timestamp']}}@endif</div>

            </div>
        @endforeach
        <h2 class="content-heading pt-0"></h2>

        <div class="row">
            <div class="form-group col-md-8">
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
        </div>
    </form>

</div>
