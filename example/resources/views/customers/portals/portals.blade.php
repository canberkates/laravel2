<div class="block-header block-header-default">
    <h3 class="block-title">
        <a href="{{ url('customers/' . $customer->cu_id . '/customerportal/create')}}">
            <button class="btn btn-primary">Add a portal</button>
        </a>
    </h3>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="block block-rounded block-bordered">
            <ul class="nav nav-tabs nav-tabs-block js-tabs-enabled" data-toggle="tabs"
                role="tablist">
                <li class="nav-item">
                    <a class="nav-link active show" href="#btabs-static-home">Active
                        ({{sizeof($portals['active'])}})</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#btabs-static-profile">Paused
                        ({{sizeof($portals['paused'])}})</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#btabs-static-profile2">Inactive
                        ({{sizeof($portals['inactive'])}})</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#btabs-static-history">History</a>
                </li>
            </ul>
            <div class="block-content tab-content">
                <div class="tab-pane active show" id="btabs-static-home" role="tabpanel">
                    <div class="block block-rounded block-bordered">
                        <div class="block-content block-content-full">
                            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Portal</th>
                                    <th>Type</th>
                                    <th>Request type</th>
                                    <th>Description</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($portals['active'] as $customerportal)
                                    <tr>
                                        <td>{{$customerportal->ktcupo_id}}</td>
                                        <td>{{$customerportal->portal->po_portal}}</td>
                                        <td>{{$portaltypes[$customerportal->ktcupo_type]}}</td>
                                        <td>{{$requesttypes[$customerportal->ktcupo_request_type]}}</td>
                                        <td>{{$customerportal->ktcupo_description}}</td>
                                        <td class="text-center">
                                            <div class="btn-group">
                                                <a class="btn btn-sm btn-primary"
                                                   data-toggle="tooltip"
                                                   data-placement="left"
                                                   title="edit"
                                                   href="{{ url('customers/' . $customer->cu_id . '/customerportal/' . $customerportal->ktcupo_id.'/edit')}}">
                                                    <i class="fa fa-pencil-alt"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="btabs-static-profile" role="tabpanel">
                    <div class="block block-rounded block-bordered">
                        <div class="block-content block-content-full">
                            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Portal</th>
                                    <th>Type</th>
                                    <th>Request type</th>
                                    <th>Description</th>
                                    <th>Edit</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($portals['paused'] as $customerportal)
                                    <tr>
                                        <td>{{$customerportal->ktcupo_id}}</td>
                                        <td>{{$customerportal->portal->po_portal}}</td>
                                        <td>{{$portaltypes[$customerportal->ktcupo_type]}}</td>
                                        <td>{{$requesttypes[$customerportal->ktcupo_request_type]}}</td>
                                        <td>{{$customerportal->ktcupo_description}}</td>
                                        <td class="text-center">
                                            <div class="btn-group">
                                                <a class="btn btn-sm btn-primary"
                                                   data-toggle="tooltip"
                                                   data-placement="left"
                                                   title="edit"
                                                   href="{{ url('customers/' . $customer->cu_id . '/customerportal/' . $customerportal->ktcupo_id.'/edit')}}">
                                                    <i class="fa fa-pencil-alt"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="btabs-static-profile2" role="tabpanel">
                    <div class="block block-rounded block-bordered">
                        <div class="block-content block-content-full">
                            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Portal</th>
                                    <th>Request type</th>
                                    <th>Type</th>
                                    <th>Description</th>
                                    <th>Edit</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($portals['inactive'] as $customerportal)
                                    <tr>
                                        <td>{{$customerportal->ktcupo_id}}</td>
                                        <td>{{$customerportal->portal->po_portal}}</td>
                                        <td>{{$portaltypes[$customerportal->ktcupo_type]}}</td>
                                        <td>{{$requesttypes[$customerportal->ktcupo_request_type]}}</td>
                                        <td>{{$customerportal->ktcupo_description}}</td>
                                        <td class="text-center">
                                            <div class="btn-group">
                                                <a class="btn btn-sm btn-primary"
                                                   data-toggle="tooltip"
                                                   data-placement="left"
                                                   title="edit"
                                                   href="{{ url('customers/' . $customer->cu_id . '/customerportal/' . $customerportal->ktcupo_id.'/edit')}}">
                                                    <i class="fa fa-pencil-alt"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="btabs-static-history" role="tabpanel">
                    <div class="block block-rounded block-bordered">
                        <div class="block-content block-content-full">
                            <table data-order='[[1, "desc"]]'
                                   class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Timestamp</th>
                                    <th>Changed by</th>
                                    <th>Change group</th>
                                    <th>Change</th>
                                    <th>Old value</th>
                                    <th>New value</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($portal_history as $change)
                                    <tr>
                                        <td>{{$change->id}}</td>
                                        <td>{{$change->created_at}}</td>
                                        <td>{{$change->userResponsible()->us_name ?? "System"}}</td>
                                        <td>{{$change->revisionable_type}}</td>
                                        <td>{{$change->key}}</td>
                                        @if($change->key == 'ktcupo_status')
                                            <td>{{$portalstatuses[$change->old_value]}}</td>
                                            <td>{{$portalstatuses[$change->new_value]}}</td>
                                        @else
                                            <td>{{$change->old_value}}</td>
                                            <td>{{$change->new_value}}</td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
