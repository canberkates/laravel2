<div class="block-content block-content-full">
    <form method="post"
          action="{{action('CustomersController@update', $customer->cu_id)}}">
        @csrf
        <input name="_method" type="hidden" value="PATCH">
        <input name="form_name" type="hidden" value="Memberships">
        <table data-page-length='50' class="table table-bordered table-striped table-vcenter js-dataTable-full">
            <thead>
            <tr>
                <th>Member</th>
                <th>ID</th>
                <th>Name</th>
                <th>Link</th>
                <th>Location</th>
                <th>Website</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($memberships_all as $membership)
                <tr>
                    <td><input id='memberships_arr_{{$membership->me_id}}' name="memberships[{{$membership->me_id}}]"
                               {{in_array($membership->me_id, $memberships) ? "checked" : ""}} type="checkbox">
                    </td>
                    <td>{{$membership->me_id}}</td>
                    <td>{{$membership->me_name}}</td>
                    <td><input style='width:100%;' id='membership_link_{{$membership->me_id}}' name="membership_links[{{$membership->me_id}}]"
                            @if(in_array($membership->me_id, $memberships) && !empty($membership_links[$membership->me_id])) value='{{$membership_links[$membership->me_id]}}' @endif type="text"></td>
                    <td>{{$membership->me_location}}</td>
                    <td>{{$membership->me_website}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <h2 class="content-heading pt-0"></h2>

        <div class="row">
            <div class="form-group col-md-8">
                <button name='membership_submit' type="submit" class="btn btn-primary">Update</button>
            </div>
        </div>
    </form>
</div>

@push( 'scripts' )

    <script>
        jQuery(document).ready(function () {
            jQuery("button[name=membership_submit]").click(function() {
                var submit = true;
                if (jQuery("#memberships_arr_1").is(":checked")) {
                    if (jQuery("#membership_link_1").val() == "") {
                        submit = false;
                        jQuery("#membership_link_1").css("border", "1px solid red")
                    }
                    else {
                        jQuery("#membership_link_1").css("border", "2px solid #10d810")
                    }
                }
                if (jQuery("#memberships_arr_3").is(":checked")) {
                    if (jQuery("#membership_link_3").val() == "") {
                        submit = false;
                        jQuery("#membership_link_3").css("border", "1px solid red")
                    }
                    else {
                        jQuery("#membership_link_3").css("border", "2px solid #10d810")
                    }
                }
                if (jQuery("#memberships_arr_4").is(":checked")) {
                    if (jQuery("#membership_link_4").val() == "") {
                        submit = false;
                        jQuery("#membership_link_4").css("border", "1px solid red")
                    }
                    else {
                        jQuery("#membership_link_4").css("border", "2px solid #10d810")
                    }
                }

                if (submit == false) {
                    alert("You have selected one or more memberships which needs a link. Check the red fields and click update again.");
                    return false;
                }
            });
        });
    </script>

@endpush
