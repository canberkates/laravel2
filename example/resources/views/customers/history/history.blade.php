<div class="block-content block-content-full">
    <table data-order='[[1, "desc"]]' class="table table-bordered table-striped table-vcenter js-dataTable-full">
        <thead>
        <tr>
            <th>ID</th>
            <th>Timestamp</th>
            <th>Changed by</th>
            <th>Change group</th>
            <th>Change</th>
            <th>Old value</th>
            <th>New value</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($history as $change)
            <tr>
                <td>{{$change->id}}</td>
                <td>{{$change->created_at}}</td>
                <td>{{$change->userResponsible()->us_name ?? "System"}}</td>
                <td>{{$change->revisionable_type}}</td>
                <td>{{$change->key}}</td>
                <td>{{$change->old_value}}</td>
                <td>{{$change->new_value}}</td>
            </tr>
        @endforeach
        @foreach ($moverdata_history as $change2)
            <tr>
                <td>{{$change2->id}}</td>
                <td>{{$change2->created_at}}</td>
                <td>{{$change2->userResponsible()->us_name ?? "System"}}</td>
                <td>{{$change2->revisionable_type}}</td>
                <td>{{$change2->key}}</td>
                <td>{{$change2->old_value}}</td>
                <td>{{$change2->new_value}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
