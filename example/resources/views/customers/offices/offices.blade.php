@if(!$parent)
<div class="block-header block-header-default">
    <h3 class="block-title">

        <a href="{{ url('customers/' . $customer->cu_id . '/customeroffice/create')}}">
            <button class="btn btn-primary">Add an office</button>
        </a>

        <a href="{{ url('customers/' . $customer->cu_id . '/child/create')}}">
            <button class="btn btn-primary">Add a child</button>
        </a>
    </h3>
</div>

<div class="block-content">

    <form method="post" action="{{action('CustomersController@update', $customer->cu_id)}}">
        @csrf
        <input name="_method" type="hidden" value="PATCH">
        <input name="form_name" type="hidden" value="Child Defaults">


        <div class="row">

            <div class="col-md-12">
                <h2 class="content-heading pt-0">Relationship settings</h2>

                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label" for="for-parent_sirelo_type">Parent Sirelo:</label>
                    <div class="col-sm-7">
                        <select id="for-parent_sirelo_type" class="form-control" name="parent_sirelo_type"
                                data-placeholder="Choose one..">
                            @foreach($customerchildsirelotypes as $id => $type)
                                <option value="{{$id}}" @if($id == $customer->moverdata->moda_parent_sirelo_type) selected @endif>{{$type}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <label class="col-sm-3 col-form-label" for="for-child_sirelo_type">Child Sirelo:</label>
                    <div class="col-sm-7">
                        <select id="for-child_sirelo_type" class="form-control" name="child_sirelo_type"
                                data-placeholder="Choose one..">
                            @foreach($customerchildsirelotypes as $id => $type)
                                <option value="{{$id}}" @if($id == $customer->moverdata->moda_child_sirelo_type) selected @endif>{{$type}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>


                <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="form-group col-md-8">
                        <button type="submit"
                                class="btn btn-primary">
                            Update
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </form>

    <h2 class="content-heading pt-0">Offices & Children</h2>
    <div class="block-content block-content-full">
        <table data-order='[[0, "desc"]]' class="table table-bordered table-striped table-vcenter js-dataTable-full">
            <thead>
            <tr>
                <th>ID</th>
                <th>Customer Name</th>
                <th>Street 1</th>
                <th>Street 2</th>
                <th>City</th>
                <th>Zipcode</th>
                <th>State</th>
                <th>Country</th>
                <th>Email</th>
                <th>Telephone</th>
                <th>Website</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($offices as $office)
                <tr>
                    @if(!$office->cuof_child_id)
                        <td>{{$office->cuof_id}}</td>
                    @else
                        <td><span class="badge badge-primary">CHILD</span></td>
                    @endif
                    @if($office->cuof_child_id)
                        <td>{{$children->where("cu_id", $office->cuof_child_id)->first()->cu_company_name_business}}</td>
                    @else
                        <td></td>
                    @endif
                    <td>{{$office->cuof_street_1}}</td>
                    <td>{{$office->cuof_street_2}}</td>
                    <td>{{$office->cuof_city}}</td>
                    <td>{{$office->cuof_zipcode}}</td>
                    <td>{{$office->cuof_state}}</td>
                    <td>{{$office->cuof_co_code}}</td>
                    <td>{{$office->cuof_email}}</td>
                    <td>{{$office->cuof_telephone}}</td>
                    <td>{{$office->cuof_website}}</td>
                    <td class="text-center">
                        <div class="btn-group">

                            <a class="btn btn-sm btn-primary" data-toggle="tooltip"
                               data-placement="left"
                               title="edit"
                               @if(!$office->cuof_child_id)
                               href="{{ url('customers/' . $customer->cu_id . '/customeroffice/' . $office->cuof_id.'/edit')}}"
                               @else
                               href="{{ url('customers/' . $customer->cu_id . '/child/' . $office->cuof_id.'/edit')}}"
                                @endif
                            >
                                <i class="fa fa-pencil-alt"></i>
                            </a>
                        </div>
                        <div class="btn-group">
                            <button type="button" class="btn btn-sm btn-primary @if(!$office->cuof_child_id) office_delete @else child_delete @endif" data-toggle="tooltip"
                                    title="Delete" @if(!$office->cuof_child_id) data-office_id="{{$office->cuof_id}}" @else data-child_id="{{$office->cuof_child_id}}"  @endif>
                                <i class="fa fa-times"></i>
                            </button>

                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

</div>
@else
    <h3>You have a parent so you're not allowed to modify any settings here!</h3>

    <div class="form-group row">
        <div class="col-sm-1"></div>
        <label class="col-sm-3 col-form-label" for="for-child_sirelo_type">Inhereted Sirelo Setting (from Parent):</label>
        <div class="col-sm-7">
                <input type="text"
                       disabled
                       class="form-control"
                       name="title"
                       value="{{$customerchildsirelotypes[$parent->moverdata->moda_child_sirelo_type]}}">
        </div>
    </div>
@endif
