<?php $total_days[date("Y")] = 60; ?>
<div class="block-header block-header-default">
    <h3 class="block-title">
        <a href="{{ url('customers/' . $customer->cu_id . '/customerpause/create')}}">
            <button class="btn btn-primary">Add a pause</button>
        </a>
        <span style="margin-left: 25px;">This company has requested a pause for {{$days_used}} out of {{$days_total}} days this year!</span>
    </h3>
</div>
<div class="block-content block-content-full">
    <table data-order='[[0, "desc"]]' class="table table-bordered table-striped table-vcenter js-dataTable-full">
        <thead>
        <tr>
            <th>ID</th>
            <th>Timestamp</th>
            <th>User</th>
            <th>Remark</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Total days</th>
            <th>Remaining days</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>

        @foreach ($pauses as $pause)
            <tr>
                <td>{{$pause->ph_id}}</td>
                <td>{{$pause->ph_created_timestamp}}</td>
                @if($pause->ph_us_id)
                    <td style="color:red;">{{$pause->user->us_name}}</td>
                @else
                    <td>{{$pause->application_user->apus_name}}</td>
                @endif
                <td>{{$pause->ph_remark}}</td>
                <td>{{$pause->ph_start_date}}</td>
                <td>{{$pause->ph_end_date}}</td>
                <td>{{$pause->ph_total_days}}</td>

                <?php

                $remaining = "Old";
                $year =  "Data";

                if(date('Y') == substr($pause->ph_start_date, 0 ,4) && date('Y') == substr($pause->ph_end_date, 0 ,4)){
                    $remaining = $total_days[date('Y')] -= $pause->ph_total_days;
                    $year = date('Y');
                }

                //The pause started this year, but ends in the next one
                if(date('Y') == substr($pause->ph_start_date, 0 ,4) && date('Y') != substr($pause->ph_end_date, 0 ,4)){

                    $format = 'Y-m-d';

                    $today = DateTime::createFromFormat($format, $pause->ph_start_date);
                    $appt  = DateTime::createFromFormat($format, date("Y").'-12-31');

                    $remaining = $total_days[date('Y')] -= ($appt->diff($today)->days+1);
                    $year = date('Y');
                }

                //The pause started last year, but ends in this year
                if(date('Y') != substr($pause->ph_start_date, 0 ,4) && date('Y') == substr($pause->ph_end_date, 0 ,4)){

                    $format = 'Y-m-d';

                    $start  = DateTime::createFromFormat($format, date("Y").'-01-01');
                    $to = DateTime::createFromFormat($format, $pause->ph_end_date);

                    $remaining = $total_days[date('Y')] -= $start->diff($to)->days;
                    $year = date('Y');
                }



                //The pause is not from this year at all
                if(date('Y') != substr($pause->ph_start_date, 0 ,4) && date('Y') != substr($pause->ph_end_date, 0 ,4)){

                    $format = 'Y-m-d';

                    $start  = DateTime::createFromFormat($format, $pause->ph_start_date);
                    $to = DateTime::createFromFormat($format, $pause->ph_end_date);

                    if(array_key_exists($start->format("Y"), $total_days)){
                        $remaining = $total_days[$start->format("Y")] -= $start->diff($to)->days;
                        $year = $start->format("Y");
                    }else{
                        $total_days[$start->format("Y")] = 60;
                        $remaining = $total_days[$start->format("Y")] -= $start->diff($to)->days;
                        $year = $start->format("Y");
                    }

                }

                ?>

                <td>{{$remaining}} ({{$year}})</td>
                <td class="text-center">
                    <div class="btn-group">
                        <a class="btn btn-sm btn-primary" data-toggle="tooltip"
                           data-placement="left"
                           title="edit"
                           href="{{ url('customers/' . $customer->cu_id . '/customerpause/' . $pause->ph_id.'/edit')}}">
                            <i class="fa fa-pencil-alt"></i>
                        </a>
                    </div>
                    <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-primary pause_delete" data-toggle="tooltip"
                                title="Delete" data-pause_id="{{$pause->ph_id}}">
                            <i class="fa fa-times"></i>
                        </button>

                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
