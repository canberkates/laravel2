@extends('layouts.backend')

@include('scripts.datepicker')

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Add a Pause to {{$customer->cu_company_name_business}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../../">@if($customer->cu_type == 1) Customers @else Resellers @endif</a></li>
                        <li class="breadcrumb-item"><a href="../edit">{{$customer->cu_company_name_business}}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Add a Pause</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">

        <div class="row justify-content-center">

            <div class="col-md-10">
                <div class="block block-rounded block-bordered">
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    {{$error}}<br>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="block-header block-header-default">
                        <h3 class="block-title">General
                            <span style="margin-left: 25px;font-size:15px;font-style: italic">This company has requested a pause for {{$days_used}} out of {{$days_total}} days this year!</span></h3>
                    </div>
                    <div class="block-content">
                        <form class="mb-5" method="post" action="{{action('CustomerPauseController@update', [$customer->cu_id, $pause->ph_id])}}">
                            @csrf
                            <input name="_method" type="hidden" value="patch">

                            <div class="row">
                                <div class="col-md-6">

                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="example-hf-email">Pause date form:</label>
                                        <div class="col-sm-7">
                                            <input autocomplete="off" type="text" class="js-datepicker form-control" id="pause_from"
                                                   name="pause_from" data-week-start="1" data-autoclose="true" data-today-highlight="true"
                                                   data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd" value="{{$pause->ph_start_date}}">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="example-hf-email">Pause date to:</label>
                                        <div class="col-sm-7">
                                            <input autocomplete="off" type="text" class="js-datepicker form-control" id="pause_to"
                                                   name="pause_to" data-week-start="1" data-autoclose="true" data-today-highlight="true"
                                                   data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd" value="{{$pause->ph_end_date}}">
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="remark">Remark:</label>
                                        <div class="col-sm-7">
                                            <textarea type="text" class="form-control" name="remark">{{$pause->ph_remark}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <h2 class="content-heading pt-0"></h2>

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Edit Pause</button>
                                    <a class="btn btn-primary" href="../edit">
                                        Back
                                    </a>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection



