@extends('layouts.backend')

@include( 'scripts.datatables' )
@include( 'scripts.dialogs' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Customers (<span id="total_customer_count"></span> / <span id="total_leads_store"></span> / <span id="total_conversion_tools"></span>)
                    <span id="emojis"></span>

                </h1>

                <div class="btn-group">
                    <a href="{{ url('customersearch')}}" class="btn btn-outline-secondary">
                        Customer search
                    </a>
                </div>

                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url( 'dashboard' ) }}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Customers</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->
    <!-- Page Content -->
    <div class="content">
        <!-- Your Block -->

        <div class="block block-rounded block-bordered">
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    <a href="{{ url('customers/create')}}"><button data-toggle="click-ripple" class="btn btn-primary">Add a Customer</button></a>
                    @can("partnerdesk")<a href="{{ url('customerchanges/')}}"><button data-toggle="click-ripple" class="btn btn-primary">Customer Changes ({{$customer_changes_count}})</button></a>@endcan
                    @can("customers - portals")<a href="{{ url('customerportals/')}}"><button data-toggle="click-ripple" class="btn btn-primary">Portals</button></a>@endcan
                    <a href="{{ url('customers/prospects/')}}"><button data-toggle="click-ripple" class="btn btn-primary prospects-confirm">Prospects & Deleted</button></a>
                    @if($cached_count['incorrect'] > 0)
                        <a href="{{ url('incorrect_customers')}}"><button data-toggle="click-ripple" class="btn btn-danger">Incorrect Customers <span class="badge badge-primary">({{$cached_count['incorrect']}})</span></button></a>
                    @endif
                    <a href="{{ url('customercache')}}"><button class="btn btn-primary js-popover cache-confirm" data-toggle="popover" data-placement="top" data-original-title="Last updated" data-content="{{$last_timestamp_updated}}">Purge Cache</button></a>
                </h3>
            </div>

            <ul class="nav nav-tabs nav-tabs-alt" data-toggle="tabs" role="tablist">
                @if(View::exists('cache.customers_int_nat'))
                    <li class="nav-item">
                        <a class="nav-link active" href="#btabs-alt-static-active_int_nat">Active INT + NAT ({{count($int_nat)}})</a>
                    </li>
                @endif

                @if(View::exists('cache.customers_int'))
                    <li class="nav-item">
                        <a class="nav-link" href="#btabs-alt-static-active_int">Active INT ({{count($int)}})</a>
                    </li>
                @endif

                @if(View::exists('cache.customers_nat'))
                    <li class="nav-item">
                        <a class="nav-link" href="#btabs-alt-static-active_nat">Active NAT ({{count($nat)}})</a>
                    </li>
                @endif

                @if(View::exists('cache.customers_leads_store'))
                    <li class="nav-item">
                        <a class="nav-link" href="#btabs-alt-static-active_leads_store">Leads Store ({{count($leads_store)}})</a>
                    </li>
                @endif

                @if(View::exists('cache.customers_conversion_tools'))
                    <li class="nav-item">
                        <a class="nav-link" href="#btabs-alt-static-active_conversion_tools">Conversion Tools ({{count($conversion_tools)}})</a>
                    </li>
                @endif

                @if(View::exists('cache.customers_paused'))
                    <li class="nav-item">
                        <a class="nav-link" href="#btabs-alt-static-pause">Pause ({{count($pause)}})</a>
                    </li>
                @endif

                @if(View::exists('cache.customers_cancelled'))
                    <li class="nav-item">
                        <a class="nav-link" href="#btabs-alt-static-cancelled">Cancelled ({{count($cancelled)}})</a>
                    </li>
                @endif

                @if(View::exists('cache.customers_credit_hold'))
                    <li class="nav-item">
                        <a class="nav-link" href="#btabs-alt-static-credit_hold">Credit hold ({{count($credit_hold)}})</a>
                    </li>
                @endif

                @if(View::exists('cache.customers_prepayment_credit_hold'))
                    <li class="nav-item">
                        <a class="nav-link" href="#btabs-alt-static-prepayment_credit_hold">Prepayment Credit hold ({{count($prepayment_credit_hold)}})</a>
                    </li>
                @endif

                @if(View::exists('cache.customers_debt_collector'))
                    <li class="nav-item">
                        <a class="nav-link" href="#btabs-alt-static-debt_collector">Debt collector ({{count($debt_collector)}})</a>
                    </li>
                @endif

            </ul>

            <div class="block-content tab-content">
                <div class="tab-pane active" id="btabs-alt-static-active_int_nat" role="tabpanel">
                    <div class="block-content block-content-full">
                        <table data-order='[[1, "asc"]]'
                               class="table table-bordered table-striped table-vcenter js-dataTable-full">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Company legal Name</th>
                                <th>Company business Name</th>
                                <th>Country</th>
                                <th>City</th>
                                <th>Email</th>
                                <th>Telephone</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($int_nat as $id => $data)
                                <tr>
                                    <td>{{$data->cu_id}}</td>
                                    <td>{{$data->cu_company_name_legal}}</td>
                                    <td>{{$data->cu_company_name_business}}</td>
                                    <td>{{$countries[$data->cu_co_code]}}</td>
                                    <td>{{$data->cu_city}}</td>
                                    <td>{{$data->cu_email}}</td>
                                    <td>{{$data->cu_telephone}}</td>
                                    <td><div class='btn-group'><a href='/customers/{{$data->cu_id}}/edit'><button type='button' class='btn btn-sm btn-primary js-tooltip-enabled' data-toggle='tooltip' data-id='{{$data->cu_id}}' data-original-title='Edit'><i class='fa fa-pencil-alt'></i></button></a></div> <div class='btn-group'> <button type='button' class='btn btn-sm btn-primary customer_delete js-tooltip-enabled' data-toggle='tooltip' data-id='{{$data->cu_id}}' data-original-title='Delete'><i class='fa fa-times'></i></button></div></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="tab-pane" id="btabs-alt-static-active_int" role="tabpanel">
                    <div class="block-content block-content-full">
                        <table data-order='[[1, "asc"]]'
                               class="table table-bordered table-striped table-vcenter js-dataTable-full">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Company legal Name</th>
                                <th>Company business Name</th>
                                <th>Country</th>
                                <th>City</th>
                                <th>Email</th>
                                <th>Telephone</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($int as $id => $data)
                                <tr>
                                    <td>{{$data->cu_id}}</td>
                                    <td>{{$data->cu_company_name_legal}}</td>
                                    <td>{{$data->cu_company_name_business}}</td>
                                    <td>{{$countries[$data->cu_co_code]}}</td>
                                    <td>{{$data->cu_city}}</td>
                                    <td>{{$data->cu_email}}</td>
                                    <td>{{$data->cu_telephone}}</td>
                                    <td><div class='btn-group'><a href='/customers/{{$data->cu_id}}/edit'><button type='button' class='btn btn-sm btn-primary js-tooltip-enabled' data-toggle='tooltip' data-id='{{$data->cu_id}}' data-original-title='Edit'><i class='fa fa-pencil-alt'></i></button></a></div> <div class='btn-group'> <button type='button' class='btn btn-sm btn-primary customer_delete js-tooltip-enabled' data-toggle='tooltip' data-id='{{$data->cu_id}}' data-original-title='Delete'><i class='fa fa-times'></i></button></div></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="tab-pane" id="btabs-alt-static-active_nat" role="tabpanel">
                    <div class="block-content block-content-full">
                        <table data-order='[[1, "asc"]]'
                               class="table table-bordered table-striped table-vcenter js-dataTable-full">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Company legal Name</th>
                                <th>Company business Name</th>
                                <th>Country</th>
                                <th>City</th>
                                <th>Email</th>
                                <th>Telephone</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($nat as $id => $data)
                                <tr>
                                    <td>{{$data->cu_id}}</td>
                                    <td>{{$data->cu_company_name_legal}}</td>
                                    <td>{{$data->cu_company_name_business}}</td>
                                    <td>{{$countries[$data->cu_co_code]}}</td>
                                    <td>{{$data->cu_city}}</td>
                                    <td>{{$data->cu_email}}</td>
                                    <td>{{$data->cu_telephone}}</td>
                                    <td><div class='btn-group'><a href='/customers/{{$data->cu_id}}/edit'><button type='button' class='btn btn-sm btn-primary js-tooltip-enabled' data-toggle='tooltip' data-id='{{$data->cu_id}}' data-original-title='Edit'><i class='fa fa-pencil-alt'></i></button></a></div> <div class='btn-group'> <button type='button' class='btn btn-sm btn-primary customer_delete js-tooltip-enabled' data-toggle='tooltip' data-id='{{$data->cu_id}}' data-original-title='Delete'><i class='fa fa-times'></i></button></div></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="tab-pane" id="btabs-alt-static-active_leads_store" role="tabpanel">
                    <div class="block-content block-content-full">
                        <table data-order='[[1, "asc"]]'
                               class="table table-bordered table-striped table-vcenter js-dataTable-full">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Company legal Name</th>
                                <th>Company business Name</th>
                                <th>Country</th>
                                <th>City</th>
                                <th>Email</th>
                                <th>Telephone</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($leads_store as $id => $data)
                                <tr>
                                    <td>{{$data->cu_id}}</td>
                                    <td>{{$data->cu_company_name_legal}}</td>
                                    <td>{{$data->cu_company_name_business}}</td>
                                    <td>{{$countries[$data->cu_co_code]}}</td>
                                    <td>{{$data->cu_city}}</td>
                                    <td>{{$data->cu_email}}</td>
                                    <td>{{$data->cu_telephone}}</td>
                                    <td><div class='btn-group'><a href='/customers/{{$data->cu_id}}/edit'><button type='button' class='btn btn-sm btn-primary js-tooltip-enabled' data-toggle='tooltip' data-id='{{$data->cu_id}}' data-original-title='Edit'><i class='fa fa-pencil-alt'></i></button></a></div> <div class='btn-group'> <button type='button' class='btn btn-sm btn-primary customer_delete js-tooltip-enabled' data-toggle='tooltip' data-id='{{$data->cu_id}}' data-original-title='Delete'><i class='fa fa-times'></i></button></div></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="tab-pane" id="btabs-alt-static-active_conversion_tools" role="tabpanel">
                    <div class="block-content block-content-full">
                        <table data-order='[[1, "asc"]]'
                               class="table table-bordered table-striped table-vcenter js-dataTable-full">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Company legal Name</th>
                                <th>Company business Name</th>
                                <th>Country</th>
                                <th>City</th>
                                <th>Email</th>
                                <th>Telephone</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($conversion_tools as $id => $data)
                                <tr>
                                    <td>{{$data->cu_id}}</td>
                                    <td>{{$data->cu_company_name_legal}}</td>
                                    <td>{{$data->cu_company_name_business}}</td>
                                    <td>{{$countries[$data->cu_co_code]}}</td>
                                    <td>{{$data->cu_city}}</td>
                                    <td>{{$data->cu_email}}</td>
                                    <td>{{$data->cu_telephone}}</td>
                                    <td><div class='btn-group'><a href='/customers/{{$data->cu_id}}/edit'><button type='button' class='btn btn-sm btn-primary js-tooltip-enabled' data-toggle='tooltip' data-id='{{$data->cu_id}}' data-original-title='Edit'><i class='fa fa-pencil-alt'></i></button></a></div> <div class='btn-group'> <button type='button' class='btn btn-sm btn-primary customer_delete js-tooltip-enabled' data-toggle='tooltip' data-id='{{$data->cu_id}}' data-original-title='Delete'><i class='fa fa-times'></i></button></div></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="tab-pane" id="btabs-alt-static-pause" role="tabpanel">
                    <div class="block-content block-content-full">
                        <table data-order='[[1, "asc"]]'
                               class="table table-bordered table-striped table-vcenter js-dataTable-full">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Company legal Name</th>
                                <th>Company business Name</th>
                                <th>Country</th>
                                <th>City</th>
                                <th>Email</th>
                                <th>Telephone</th>
                                <th>Paused Till</th>
                                <th>Account Manager</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($pause as $data)
                                <tr>
                                    <td>{{$data->cu_id}}</td>
                                    <td>{{$data->cu_company_name_legal}}</td>
                                    <td>{{$data->cu_company_name_business}}</td>
                                    <td>{{$countries[$data->cu_co_code]}}</td>
                                    <td>{{$data->cu_city}}</td>
                                    <td>{{$data->cu_email}}</td>
                                    <td>{{$data->cu_telephone}}</td>
                                    <td>{{$paused_till_dates[$data->cu_id]}}</td>
                                    <td>{{$users[$data->cu_account_manager]}}</td>
                                    <td><div class='btn-group'><a href='/customers/{{$data->cu_id}}/edit'><button type='button' class='btn btn-sm btn-primary js-tooltip-enabled' data-toggle='tooltip' data-id='{{$data->cu_id}}' data-original-title='Edit'><i class='fa fa-pencil-alt'></i></button></a></div> <div class='btn-group'> <button type='button' class='btn btn-sm btn-primary customer_delete js-tooltip-enabled' data-toggle='tooltip' data-id='{{$data->cu_id}}' data-original-title='Delete'><i class='fa fa-times'></i></button></div></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="tab-pane" id="btabs-alt-static-cancelled" role="tabpanel">
                    <div class="block-content block-content-full">
                        <table data-order='[[1, "asc"]]'
                               class="table table-bordered table-striped table-vcenter js-dataTable-full">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Company legal Name</th>
                                <th>Company business Name</th>
                                <th>Country</th>
                                <th>City</th>
                                <th>Email</th>
                                <th>Telephone</th>
                                <th>Cancelled on</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($cancelled as $data)
                                <tr>
                                    <td>{{$data->cu_id}}</td>
                                    <td>{{$data->cu_company_name_legal}}</td>
                                    <td>{{$data->cu_company_name_business}}</td>
                                    <td>{{$countries[$data->cu_co_code]}}</td>
                                    <td>{{$data->cu_city}}</td>
                                    <td>{{$data->cu_email}}</td>
                                    <td>{{$data->cu_telephone}}</td>
                                    <td>{{$cancelled_on_dates[$data->cu_id]}}</td>
                                    <td><div class='btn-group'><a href='/customers/{{$data->cu_id}}/edit'><button type='button' class='btn btn-sm btn-primary js-tooltip-enabled' data-toggle='tooltip' data-id='{{$data->cu_id}}' data-original-title='Edit'><i class='fa fa-pencil-alt'></i></button></a></div> <div class='btn-group'> <button type='button' class='btn btn-sm btn-primary customer_delete js-tooltip-enabled' data-toggle='tooltip' data-id='{{$data->cu_id}}' data-original-title='Delete'><i class='fa fa-times'></i></button></div></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="tab-pane" id="btabs-alt-static-credit_hold" role="tabpanel">
                    <div class="block-content block-content-full">
                        <table data-order='[[1, "asc"]]'
                               class="table table-bordered table-striped table-vcenter js-dataTable-full">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Company legal Name</th>
                                <th>Company business Name</th>
                                <th>Country</th>
                                <th>City</th>
                                <th>Email</th>
                                <th>Telephone</th>
                                <th>Days credit hold</th>
                                <th>Debt manager</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($credit_hold as $data)
                                <tr>
                                    <td>{{$data->cu_id}}</td>
                                    <td>{{$data->cu_company_name_legal}}</td>
                                    <td>{{$data->cu_company_name_business}}</td>
                                    <td>{{$countries[$data->cu_co_code]}}</td>
                                    <td>{{$data->cu_city}}</td>
                                    <td>{{$data->cu_email}}</td>
                                    <td>{{$data->cu_telephone}}</td>
                                    <td>{{$days_on_ch[$data->cu_id]}}</td>
                                    <td>{{$users[$data->cu_debt_manager]}}</td>
                                    <td><div class='btn-group'><a href='/customers/{{$data->cu_id}}/edit'><button type='button' class='btn btn-sm btn-primary js-tooltip-enabled' data-toggle='tooltip' data-id='{{$data->cu_id}}' data-original-title='Edit'><i class='fa fa-pencil-alt'></i></button></a></div> <div class='btn-group'> <button type='button' class='btn btn-sm btn-primary customer_delete js-tooltip-enabled' data-toggle='tooltip' data-id='{{$data->cu_id}}' data-original-title='Delete'><i class='fa fa-times'></i></button></div></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="tab-pane" id="btabs-alt-static-prepayment_credit_hold" role="tabpanel">
                    <div class="block-content block-content-full">
                        <table data-order='[[1, "asc"]]'
                               class="table table-bordered table-striped table-vcenter js-dataTable-full">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Company legal Name</th>
                                <th>Company business Name</th>
                                <th>Country</th>
                                <th>City</th>
                                <th>Email</th>
                                <th>Telephone</th>
                                <th>Days credit hold</th>
                                <th>Debt manager</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($prepayment_credit_hold as $data)
                                <tr>
                                    <td>{{$data->cu_id}}</td>
                                    <td>{{$data->cu_company_name_legal}}</td>
                                    <td>{{$data->cu_company_name_business}}</td>
                                    <td>{{$countries[$data->cu_co_code]}}</td>
                                    <td>{{$data->cu_city}}</td>
                                    <td>{{$data->cu_email}}</td>
                                    <td>{{$data->cu_telephone}}</td>
                                    <td>{{$days_on_ch[$data->cu_id]}}</td>
                                    <td>{{$users[$data->cu_debt_manager]}}</td>
                                    <td><div class='btn-group'><a href='/customers/{{$data->cu_id}}/edit'><button type='button' class='btn btn-sm btn-primary js-tooltip-enabled' data-toggle='tooltip' data-id='{{$data->cu_id}}' data-original-title='Edit'><i class='fa fa-pencil-alt'></i></button></a></div> <div class='btn-group'> <button type='button' class='btn btn-sm btn-primary customer_delete js-tooltip-enabled' data-toggle='tooltip' data-id='{{$data->cu_id}}' data-original-title='Delete'><i class='fa fa-times'></i></button></div></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="tab-pane" id="btabs-alt-static-debt_collector" role="tabpanel">
                    <div class="block-content block-content-full">
                        <table data-order='[[1, "asc"]]'
                               class="table table-bordered table-striped table-vcenter js-dataTable-full">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Company legal Name</th>
                                <th>Company business Name</th>
                                <th>Country</th>
                                <th>City</th>
                                <th>Email</th>
                                <th>Telephone</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($debt_collector as $data)
                                <tr>
                                    <td>{{$data->cu_id}}</td>
                                    <td>{{$data->cu_company_name_legal}}</td>
                                    <td>{{$data->cu_company_name_business}}</td>
                                    <td>{{$countries[$data->cu_co_code]}}</td>
                                    <td>{{$data->cu_city}}</td>
                                    <td>{{$data->cu_email}}</td>
                                    <td>{{$data->cu_telephone}}</td>
                                    <td><div class='btn-group'><a href='/customers/{{$data->cu_id}}/edit'><button type='button' class='btn btn-sm btn-primary js-tooltip-enabled' data-toggle='tooltip' data-id='{{$data->cu_id}}' data-original-title='Edit'><i class='fa fa-pencil-alt'></i></button></a></div> <div class='btn-group'> <button type='button' class='btn btn-sm btn-primary customer_delete js-tooltip-enabled' data-toggle='tooltip' data-id='{{$data->cu_id}}' data-original-title='Delete'><i class='fa fa-times'></i></button></div></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Your Block -->
    </div>
    <!-- END Page Content -->
@endsection

@push( 'scripts' )
    <script>

        $active_int_nat = {{count($int_nat)}};
        $active_int = {{count($int)}};
        $active_nat = {{count($nat)}};
        $active_leads_store = {{count($leads_store)}};
        $active_conversion_tools = {{count($conversion_tools)}};
        $total_customers = (Number($active_int) + Number($active_nat)) - Number($active_int_nat);

        $("#total_customer_count").text($total_customers);
        $("#total_conversion_tools").text($active_conversion_tools);
        $("#total_leads_store").text($active_leads_store);

        if($total_customers < 400 ) $emoji = "🐡";
        else if($total_customers > 400 && $total_customers < 425) $emoji = "🐞"
        else if($total_customers >= 425 && $total_customers < 450) $emoji = "🤠😌"
        else if($total_customers >= 450 && $total_customers < 475) $emoji = "💪😁"
        else if($total_customers >= 475 && $total_customers < 500) $emoji = "🤩🙌"
        else $emoji = "🥳🎉😁😁🎉🎉🙌🙌 <b>Lekker gewerkt!</b>"

        $("#emojis").html($emoji);

        //$("#active_int_nat_count").text("("+$active_int_nat+")");
        //$("#active_int_count").text("("+$active_int+")");
        //$("#active_nat_count").text("("+$active_nat+")");
        //$("#active_leads_store_count").text("("+$active_leads_store+")");
        //$("#active_conversion_tools_count").text("("+$active_conversion_tools+")");
        //$("#pause_count").text("("+$("#btabs-alt-static-pause").find("input[name=table_count]").val()+")");
        //$("#cancelled_count").text("("+$("#btabs-alt-static-cancelled").find("input[name=table_count]").val()+")");
        //$("#credit_hold_count").text("("+$("#btabs-alt-static-credit_hold").find("input[name=table_count]").val()+")");
        //$("#prepayment_credit_hold_count").text("("+$("#btabs-alt-static-prepayment_credit_hold").find("input[name=table_count]").val()+")");
        //$("#debt_collector_count").text("("+$("#btabs-alt-static-debt_collector").find("input[name=table_count]").val()+")");



        jQuery(document).ready(function(){

        	jQuery( document ).on( 'click', '.customer_delete', function(e) {

                e.preventDefault();

        		var $self = jQuery(this);

                confirmDelete("{{ url('ajax/customer/delete') }}", 'get', {id:$self.data('id')}, function() {

                    $self.parents( '.dataTable' ).DataTable().row( $self.parents( 'tr' ) ).remove().draw( 'page' );
                });

            });

        	$( '.prospects-confirm' ).click( function(e) {

        	    // Stop initial link
                e.preventDefault();

                // Set options
                var $options = {
                    text : 'Loading the prospects might take a while..',
                    type: 'warning'
                };

                // Confirm dialog
                confirmClick( "{{ url('customers/prospects/')}}", $options );
            });

        	$( '.cache-confirm' ).click( function(e) {

                // Stop initial link
                e.preventDefault();

                // Set options
                var $options = {
                    title: 'Are you sure you want to clear the customer cache?',
                    type: 'warning',
                };

                // Confirm dialog
                confirmClick( "{{ url('customercache') }}", $options );
            });
        });
    </script>

@endpush
