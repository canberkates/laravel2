@extends('layouts.backend')

@include('scripts.datepicker')

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Add a planned call to {{$customer->cu_company_name_business}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../../">@if($customer->cu_type == 1) Customers @else Resellers @endif</a></li>
                        <li class="breadcrumb-item"><a href="../edit">{{$customer->cu_company_name_business}}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Add a Pause</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">

        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif

        <div class="row justify-content-center">

            <div class="col-md-10">
                <div class="block block-rounded block-bordered">
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    {{$error}}<br>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="block-header block-header-default">
                        <h3 class="block-title">General</h3>
                    </div>
                    <div class="block-content">
                        <form class="mb-5" method="post" action="{{action('CustomerPlannedCallController@store', 1)}}">
                            @csrf
                            <input name="_method" type="hidden" value="post">
                            <input name="customer_id" type="hidden" value="{{$customer->cu_id}}">

                            <div class="row">
                                <div class="col-md-12">

                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-3 col-form-label" for="user">User</label>
                                        <div class="col-sm-3">
                                            <select type="text" class="js-select2 form-control"
                                                    name="user">
                                                <option value=""></option>
                                                @foreach($users as $id => $user)
                                                    <option value="{{$id}}" @if($id == Auth::id()) selected="selected" @endif>{{$user}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-3 col-form-label" for="user">Type</label>
                                        <div class="col-sm-3">
                                            <select type="text" class="js-select2 form-control"
                                                    name="type">
                                                @foreach($call_types as $id => $type)
                                                    <option value="{{$id}}">{{$type}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-3 col-form-label" for="call_datetime">Planned date & time</label>
                                        <div class="col-sm-3">
                                            <input autocomplete="off" name="call_datetime" class="form-control"
                                                   id="datetimepicker" type="text" />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-3 col-form-label" for="call_reason">Reason:</label>
                                        <div class="col-sm-3">
                                            <select type="text" class="js-select2 form-control"
                                                    name="call_reason">
                                                <option value=""></option>
                                                @foreach($planned_call_reasons as $id => $planned_call_reason)
                                                    <option value="{{$id}}">{{$planned_call_reason}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-3 col-form-label" for="explain_reason">Explain reason:</label>
                                        <div class="col-sm-3">
                                            <textarea type="text" class="form-control" name="explain_reason"></textarea>
                                        </div>
                                    </div>

                                </div>
                            </div>



                            <h2 class="content-heading pt-0"></h2>

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Add Planned Call</button>
                                    <button type="submit" name="finish_call" class="btn btn-danger">Finish Call Immediately</button>
                                    <a class="btn btn-primary" href="../edit">
                                        Back
                                    </a>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@push('scripts')
    <script>
        $.datetimepicker.setDateFormatter('moment');
        $('#datetimepicker').datetimepicker({
            step: 10 //will change increments to 15m, default is 1m
        });
    </script>
@endpush
