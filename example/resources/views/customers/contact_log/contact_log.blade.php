<div class="block-header block-header-default">
    <h3 class="block-title">
        <a href="{{ url('customers/' . $customer->cu_id . '/customerplannedcall/create')}}">
            <button class="btn btn-primary">Add a Planned Call</button>
        </a>

    </h3>
</div>
<div class="block-content block-content-full">
    <table data-order='[[0, "desc"]]' class="table table-bordered table-striped table-vcenter js-dataTable-full">
        <thead>
        <tr>
            <th>ID</th>
            <th>User</th>
            <th>Type</th>
            <th>Status</th>
            <th>Reason</th>
            <th>Person spoken to</th>
            <th>Comment</th>
            <th>Date</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($contact_logs as $contact_log)
            <tr>
                <td>{{$contact_log->plca_id}}</td>
                <td>{{$contact_log->user->us_name}}</td>
                <td>{{$planned_call_type[$contact_log->plca_type]}}</td>
                <td>{{$planned_call_status[$contact_log->plca_status]}}</td>
                <td>{{$planned_call_reason[$contact_log->plca_reason]}}</td>
                <td>{{$contact_log->plca_person_spoken_to}}</td>
                <td>{{$contact_log->plca_comment}}</td>
                <td>{{$contact_log->plca_planned_date}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
