<div class="block-content block-content-full">
    <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
        <thead>
        <tr>
            <th>ID</th>
            <th>Sent</th>
            <th>Submitted</th>
            <th>Request ID</th>
            <th>Name</th>
            <th>Contacted</th>
            <th>Quote received</th>
            <th>Remark</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($surveys1 as $survey1)
            <tr>
                <td>{{$survey1->survey->su_id}}</td>
                <td>{{$survey1->survey->su_timestamp}}</td>
                <td>{{$survey1->survey->su_submitted_timestamp}}</td>
                <td>{{$survey1->survey->su_re_id}}</td>
                <td>{{$survey1->survey->request->re_full_name}}</td>
                <td>{{$contacted[$survey1->sucu_contacted]}}</td>
                <td>{{$emptyyesno[$survey1->sucu_quote_received]}}</td>
                <td>{{$survey1->sucu_remark}}</td>
        @endforeach
        </tbody>
    </table>
</div>
