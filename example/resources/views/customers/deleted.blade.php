@extends('layouts.backend')

@section('content')
    <div class="content">
        <div class="alert alert-danger alert-dismissable" role="alert">
            <p class="mb-0">This customer has been deleted!</p>
            <br>
            <a href="/customers"><button class="btn btn-primary">Go Back</button></a>
        </div>
    </div>


@endsection




