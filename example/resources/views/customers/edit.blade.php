@extends('layouts.backend')

@include('scripts.datatables')
@include('scripts.datepicker')
@include('scripts.forms')
@include('scripts.dialogs')
@include( 'scripts.charts' )
@include( 'scripts.telinput' )
@include('scripts.select2')

@pushonce( 'scripts:customer-edit' )
<script src="{{ asset('/js/custom/customer/customer_edit.js') }}"></script>
@endpushonce

@section('content')

    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">MV
                    - {{$customer->cu_company_name_business}} @if($customer->cu_credit_hold)- Credit hold @endif
                    - {{$customerstatus}}

                    @if($parent && $parent->count())
                        <br>
                        <small style="font-size: 45%;">{{$relationshiptypes[$relationship->cuof_child_type]}} of <a href="/customers/{{$parent->cu_id}}/edit">{{$parent->cu_company_name_business}}</a></small>
                    @endif

                    @if($siblings && $siblings->count())
                        <br>
                        <small style="font-size: 45%;">Siblings with @foreach($siblings as $sibling)<a href="/customers/{{$sibling->cu_id}}/edit">{{$sibling->cu_company_name_business}}@if(!$loop->last)</a> and @endif @endforeach</small>
                    @endif

                    @if($children && $children->count())
                        <br>
                        <small style="font-size: 45%;"> Has children @foreach($children as $child)<a href="/customers/{{$child->cu_id}}/edit">{{$child->cu_company_name_business}}@if(!$loop->last)</a> and @endif @endforeach </small>
                    @endif

                </h1>

                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('customers') }}">Customers</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Customer edit</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        <div class="mt-element-step">
            <div style="float:right;"><a class="reset"><i class="fa fa-times-circle" style="color:red;"></i></a></div>
            <div class="row step-line">
                <div class="col-md-2 mt-step-col first done">
                    <div class="mt-step-number bg-white">
                        <i class="fa fa-star"></i>
                    </div>
                    <div class="mt-step-title font-grey-cascade">{{$progresstypes[0]}}</div>
                </div>
                <div class="col-md-2 mt-step-col  @if($progress[1][0]) done @else first_contact @endif">
                    <div class="mt-step-number bg-white">
                        <i class="fa fa-comment-dots"></i>
                    </div>
                    <div class="mt-step-title font-grey-cascade">{{$progresstypes[1]}}</div>
                    <div class="mt-step-content font-grey-cascade">{{$progress[1][1]}}</div>
                </div>
                <div class="col-md-2 mt-step-col @if($progress[2][0]) done @else working_on @endif">
                    <div class="mt-step-number bg-white">
                        <i class="fa fa-truck-moving"></i>
                    </div>
                    <div class="mt-step-title font-grey-cascade">{{$progresstypes[2]}}</div>
                    <div class="mt-step-content font-grey-cascade">{{$progress[2][1]}}</div>
                </div>
                <div class="col-md-2 mt-step-col @if($progress[3][0]) done @else proposal_sent @endif">
                    <div class="mt-step-number bg-white">
                        <i class="fa fa-file-contract"></i>
                    </div>
                    <div class="mt-step-title font-grey-cascade">{{$progresstypes[3]}}</div>
                    <div class="mt-step-content font-grey-cascade">{{$progress[3][1]}}</div>
                </div>
                <div class="col-md-2 mt-step-col @if($progress[4][0]) done @else contract_sent @endif">
                    <div class="mt-step-number bg-white">
                        <i class="fa fa-file-invoice-dollar"></i>
                    </div>
                    <div class="mt-step-title font-grey-cascade">{{$progresstypes[4]}}</div>
                    <div class="mt-step-content font-grey-cascade">{{$progress[4][1]}}</div>
                </div>
                <div class="col-md-2 mt-step-col last @if($progress[5][0] || $progress[7][0]) error @elseif($progress[6][0]) done cancelled @else finalize @endif">
                    <div class="mt-step-number bg-white">
                        @if($progress[5][0])
                            <i class="fa fa-times-circle"></i>
                        @else
                            <i class="fa fa-check-circle"></i>
                        @endif
                    </div>
                    @if($progress[7][0])
                        <div class="mt-step-title font-grey-cascade">{{$progresstypes[7]}} 🥺</div>
                        <div class="mt-step-content font-grey-cascade">{{$progress[7][1]}}</div>
                    @elseif($progress[5][0])
                        <div class="mt-step-title font-grey-cascade">{{$progresstypes[5]}} / {{$progresslostcontracttypes[$progress[5][2]]}} 😞</div>
                        <div class="mt-step-content font-grey-cascade">{{$progress[5][1]}}</div>
                    @elseif($progress[6][0])
                        <div class="mt-step-title font-grey-cascade">{{$progresstypes[6]}} 💪😁</div>
                        <div class="mt-step-content font-grey-cascade">{{$progress[6][1]}}</div>
                    @else
                        <div class="mt-step-title font-grey-cascade">Won or lost? 👀</div>
                    @endif

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h3></h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <!-- Block Tabs Alternative Style -->
                <div class="block block-rounded block-bordered">
                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    {{$error}}<br>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <ul class="nav nav-tabs nav-tabs-alt" data-toggle="tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" href="#btabs-alt-static-general">General</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-emails">Emails</a>
                        </li>
                        @can('customers - finance')
                            <li class="nav-item">
                                <a class="nav-link" href="#btabs-alt-static-finance">Finance</a>
                            </li>
                        @endcan
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-contactpersons">Contact persons</a>
                        </li>
                        @can('conference')
                            @can('admin')
                                <li class="nav-item">
                                    <a class="nav-link" href="#btabs-alt-static-status">Status</a>
                                </li>
                            @endcan
                        @else
                            <li class="nav-item">
                                <a class="nav-link" href="#btabs-alt-static-status">Status</a>
                            </li>
                        @endif
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-remark">Remarks</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-contact_log">Contact logs</a>
                        </li>
                        @can('customers - documents')
                            <li class="nav-item">
                                <a class="nav-link" href="#btabs-alt-static-documents">Documents</a>
                            </li>
                        @endcan
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-sirelo">Sirelo</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-settings">Settings</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-mover_portal">Mover Portal</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-forms">Forms</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-offices">Offices & Children</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-memberships">Memberships</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-obligations">Permits</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-insurances">Insurances</a>
                        </li>
                        @can('customers - finance')
                            <li class="nav-item">
                                <a class="nav-link" href="#btabs-alt-static-subscriptions">Subscriptions</a>
                            </li>
                        @endcan
                        @can('customers - portals')
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-portals">Portals</a>
                        </li>
                        @endcan
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-pauses">Pauses</a>
                        </li>
                        @can('conference')
                            @can('admin')
                                <li class="nav-item">
                                    <a class="nav-link" href="#btabs-alt-static-surveys2">Reviews</a>
                                </li>
                            @endcan
                        @else
                            <li class="nav-item">
                                <a class="nav-link" href="#btabs-alt-static-surveys2">Reviews</a>
                            </li>
                        @endif
                        @canany(['requests', 'conference'])
                            <li class="nav-item">
                                <a class="nav-link" href="#btabs-alt-static-requests">Requests</a>
                            </li>
                        @endcan
                        @can('customers - invoices')
                            <li class="nav-item">
                                <a class="nav-link" href="#btabs-alt-static-invoices">Invoices</a>
                            </li>
                        @endcan
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-history">History</a>
                        </li>
                    </ul>
                    <div class="block-content tab-content">
                        <div class="tab-pane active" id="btabs-alt-static-general" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        @include( 'customers.general.general-information' )
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        @include( 'customers.general.addresses' )
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="block block-rounded block-bordered">
                                        @include( 'customers.general.finance' )
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="block block-rounded block-bordered">
                                        @include( 'customers.general.fields' )
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="btabs-alt-static-emails" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        @include( 'customers.emails.emails' )
                                    </div>
                                </div>
                            </div>
                        </div>
                        @can('customers - finance')
                            <div class="tab-pane" id="btabs-alt-static-finance" role="tabpanel">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="block block-rounded block-bordered">
                                            @include( 'customers.finance.finance' )
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="block block-rounded block-bordered">
                                            @include( 'customers.finance.credit-cards' )
                                        </div>
                                    </div>
                                    @if($customer->cu_payment_method == 3 || $customer->cu_payment_method == 6)
                                        <div class="col-md-6">
                                            <div class="block block-rounded block-bordered">
                                                @include( 'customers.finance.auto_debit' )
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        @endcan

                        <div class="tab-pane" id="btabs-alt-static-contactpersons" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        @include( 'customers.contactpersons.contactpersons' )
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="btabs-alt-static-status" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        @include( 'customers.status.status' )
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="btabs-alt-static-remark" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        @include( 'customers.remarks.remarks' )
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="btabs-alt-static-contact_log" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        @include( 'customers.contact_log.contact_log' )
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="btabs-alt-static-pauses" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        @include( 'customers.pauses.pauses' )
                                    </div>
                                </div>
                            </div>
                        </div>
                        @can('customers - documents')
                            <div class="tab-pane" id="btabs-alt-static-documents" role="tabpanel">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="block block-rounded block-bordered">
                                            @include( 'customers.documents.documents' )
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endcan
                        <div class="tab-pane" id="btabs-alt-static-sirelo" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12 col-xl-12">
                                    <div class="block block-rounded block-bordered">
                                        @include( 'customers.sirelo.sirelo' )
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="btabs-alt-static-settings" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12 col-xl-12">
                                    <div class="block block-rounded block-bordered">
                                        @include( 'customers.settings.settings' )
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="btabs-alt-static-mover_portal" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12 col-xl-12">
                                    <div class="block block-rounded block-bordered">
                                        @include( 'customers.moverportal.moverportal' )
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="btabs-alt-static-forms" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        <div class="block-header block-header-default">
                                            <h3 class="block-title">
                                                <a href="{{ url('customers/' . $customer->cu_id . '/conversion_tools/create')}}">
                                                    <button class="btn btn-primary">Add a form</button>
                                                </a>
                                                <strong> Don't touch this unless you are Ellena or Lorenzo! 😠🧐</strong>
                                            </h3>
                                        </div>
                                        @if($forms)
                                        <div class="block-content block-content-full">
                                            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                                <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Token</th>
                                                    <th>Name</th>
                                                    <th>Form type</th>
                                                    <th>Language</th>
                                                    <th>Volume type</th>
                                                    <th>Actions</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach ($forms->whereIn("afpafo_form_type", [\App\Data\AffiliateFormType::CONVERSION_TOOLS]) as $form)
                                                    <tr>
                                                        <td>{{$form->afpafo_id}}</td>
                                                        <td>{{$form->afpafo_token}}</td>
                                                        <td>{{$form->afpafo_name}}</td>
                                                        <td>{{$formtypes[$form->afpafo_form_type]}}</td>
                                                        <td>{{$form->afpafo_la_code}}</td>
                                                        <td>{{$volumetypes[$form->afpafo_volume_type]}}</td>
                                                        <td class="text-center">
                                                            <div class="btn-group">
                                                                <a class="btn btn-sm btn-primary" data-toggle="tooltip"
                                                                   data-placement="left"
                                                                   title="edit"
                                                                   href="{{ url('customers/' . $customer->cu_id . '/conversion_tools/' . $form->mofose_id.'/edit')}}">
                                                                    <i class="fa fa-pencil-alt"></i>
                                                                </a>
                                                            </div>
                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-sm btn-primary lead_form_delete" data-toggle="tooltip" title="Delete the Lead Form" data-lead_form_id="{{$form->afpafo_id}}">
                                                                    <i class="fa fa-times"></i>
                                                                </button>

                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                @foreach ($forms->where("afpafo_form_type", 3)->where("afpafo_deleted", 0) as $form)
                                                    <tr>
                                                        <td>{{$form->afpafo_id}}</td>
                                                        <td></td>
                                                        <td>{{$form->afpafo_name}}</td>
                                                        <td></td>
                                                        <td>{{$formtypes[$form->afpafo_form_type]}}</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="btabs-alt-static-offices" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        @include( 'customers.offices.offices' )
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="btabs-alt-static-memberships" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        @include( 'customers.memberships.memberships' )
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="btabs-alt-static-obligations" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        @include( 'customers.obligations.obligations' )
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="btabs-alt-static-insurances" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        @include( 'customers.insurances.insurances' )
                                    </div>
                                </div>
                            </div>
                        </div>
                        @can('customers - finance')
                            <div class="tab-pane" id="btabs-alt-static-subscriptions" role="tabpanel">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="block block-rounded block-bordered">
                                            @include( 'customers.subscriptions.subscriptions' )
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endcan
                        <div class="tab-pane" id="btabs-alt-static-portals" role="tabpanel">
                            @include( 'customers.portals.portals' )
                        </div>
                        <div class="tab-pane" id="btabs-alt-static-surveys2" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        @include( 'customers.surveys2.surveys2' )
                                    </div>
                                </div>
                            </div>
                        </div>
                        @canany(['requests', 'conference'])
                            <div class="tab-pane" id="btabs-alt-static-requests" role="tabpanel">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="block block-rounded block-bordered">
                                            @include( 'customers.requests.requests' )
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endcan
                        @can('customers - invoices')
                            <div class="tab-pane" id="btabs-alt-static-invoices" role="tabpanel">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="block block-rounded block-bordered">
                                            @include( 'customers.invoices.invoices' )
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endcan
                        <div class="tab-pane" id="btabs-alt-static-history" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        @include( 'customers.history.history' )
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- END Block Tabs Alternative Style -->
            </div>
        </div>
    </div>

    <div id="addProgressRemarkModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content" style="width:800px;">
                <div class="modal-header">
                    <h5 class="modal-title">Add progress to {{$customer->cu_company_name_business}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="progress_body"></div>
                </div>
                <div class="modal-footer">
                    <a id="progress_submit"
                       href="{{ url("customers/".$customer->cu_id."/customerremark/create?type=".$a) }}"
                       class="btn btn-primary">Add remark</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="finalizeProgressModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content" style="width:800px;">
                <div class="modal-header">
                    <h5 class="modal-title">Finalize negotiations with {{$customer->cu_company_name_business}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="background-color: #ffbdbd;">
                    <div id="progress_failure">
                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label" for="for-country">Rejection reason:</label>
                            <div class="col-sm-4">
                                <select id="rejection_reason" class="js-select2 form-control" name="rejection_reason"
                                        data-placeholder="Choose one..">
                                    <option value=""></option>
                                    @foreach($progresslostcontracttypes as $id => $type)
                                        <option value="{{$id}}">{{$type}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-4"><a id="lost_submit"
                                                     href="{{ url("customers/".$customer->cu_id."/customerremark/create?type=5&reason=") }}"
                                                     class="btn btn-danger">Better luck next time 😫</a></div>
                        </div>

                    </div>
                </div>
                <div class="modal-body" style="background-color: #8cff8c;">
                    <div id="progress_success">
                        <div class="form-group row">
                            <div class="col-md-1"></div>
                            <label class="col-sm-7" style="margin-top: 15px;">Press this button if you have scored the contract!</label>
                            <div class="col-sm-4"><a id="won_submit" style="background-color: green;"
                                                     href="{{ url("customers/".$customer->cu_id."/customerremark/create?type=6") }}"
                                                     class="btn btn-success">Got 'em! 🙌</a></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="cancelledProgressModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content" style="width:800px;">
                <div class="modal-header">
                    <h5 class="modal-title">Cancellation of {{$customer->cu_company_name_business}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="background-color: #ffbdbd;">
                    <div id="progress_failure">
                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-5 col-form-label" for="for-country">{{$customer->cu_company_name_business}} wants to cancel</label>
                            <div class="col-sm-4"><a id="lost_submit"
                                                     href="{{ url("customers/".$customer->cu_id."/customerremark/create?type=7") }}"
                                                     class="btn btn-danger">All good things must come to an end 😭</a></div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="resetProgressModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content" style="width:800px;">
                <div class="modal-header">
                    <h5 class="modal-title">Reset the bar of {{$customer->cu_company_name_business}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="background-color: #ffbdbd;">
                    <div id="progress_failure">
                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-2 col-form-label" for="for-country">Reset to:</label>
                            <div class="col-sm-6">
                                <select id="reset_to" class="form-control" name="reset_to"
                                        data-placeholder="Choose one..">
                                    @foreach($progresstypes as $id => $type)
                                        @if($id > 4) @continue @endif
                                        <option value="{{$id}}">{{$type}}</option>
                                    @endforeach
                                </select>
                                <input type="hidden" id="customer_id" value="{{$customer->cu_id}}">
                            </div>
                            <div class="col-sm-3">
                                <a id="reset_bar" style="color:white;" class="btn btn-danger">Reset!</a></div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="viewInvoiceModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content" style="width:700px;">
                <div class="modal-header">
                    <h5 class="modal-title">Viewing Invoice</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="invoice_view_iframe">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push( 'scripts' )

    <script>
        jQuery(document).ready(function () {
            jQuery(document).on('click', '#moda_special_agreements_checkbox', function (e) {
                var textarea = jQuery("textarea[name=special_agreements]");

                if ($(this).is(':checked')) {
                    textarea.prop("disabled", false);
                } else {
                    textarea.prop("disabled", true);
                }
            });

            jQuery(document).on('click', '.contactperson_delete', function (e) {
                e.preventDefault();

                var $self = jQuery(this);

                confirmDelete("{{ url('ajax/customer/contactperson_delete') }}", 'get', {id: $self.data('contactperson_id')}, function () {

                    $self.parents('.dataTable').DataTable().row($self.parents('tr')).remove().draw('page');
                });
            });


            jQuery(document).on('click', '.lead_form_delete', function (e) {
                e.preventDefault();

                var $self = jQuery(this);

                confirmDelete("{{ url('ajax/customer/lead_form_delete') }}", 'get', {id: $self.data('lead_form_id')}, function () {

                    $self.parents('.dataTable').DataTable().row($self.parents('tr')).remove().draw('page');
                });
            });

            jQuery(document).on('click', '#moda_activate_premium_leads', function (e) {
                var premium_leads_section = $("#premium_leads_section");

                if ($(this).is(":checked")) {
                    premium_leads_section.slideDown();
                } else {
                    premium_leads_section.slideUp();
                }
            });

            jQuery(document).on('click', '.status_delete', function (e) {
                e.preventDefault();

                var $self = jQuery(this);

                @can("admin")
                confirmDelete("{{ url('ajax/customer/status_delete') }}", 'get', {id: $self.data('status_id')}, function () {

                    $self.parents('.dataTable').DataTable().row($self.parents('tr')).remove().draw('page');
                });
                @endcan
            });

            jQuery(document).on('click', '.remark_delete', function (e) {
                e.preventDefault();

                var $self = jQuery(this);
                confirmDelete("{{ url('ajax/customer/remark_delete') }}", 'get', {id: $self.data('remark_id')}, function () {

                    $self.parents('.dataTable').DataTable().row($self.parents('tr')).remove().draw('page');
                });
            });

            jQuery(document).on('click', '.office_delete', function (e) {
                e.preventDefault();

                var $self = jQuery(this);

                confirmDelete("{{ url('ajax/customer/office_delete') }}", 'get', {id: $self.data('office_id')}, function () {
                    $self.parents('.dataTable').DataTable().row($self.parents('tr')).remove().draw('page');
                });
            });

            jQuery(document).on('click', '.child_delete', function (e) {
                e.preventDefault();

                var $self = jQuery(this);

                confirmDelete("{{ url('ajax/customer/child_delete') }}", 'get', {id: $self.data('child_id')}, function () {
                    $self.parents('.dataTable').DataTable().row($self.parents('tr')).remove().draw('page');
                });
            });

            jQuery(document).on('click', '.pause_delete', function (e) {
                e.preventDefault();

                var $self = jQuery(this);

                console.log($self.data('pause_id'));

                confirmDelete("{{ url('ajax/customer/pause_delete') }}", 'get', {id: $self.data('pause_id')}, function () {

                    $self.parents('.dataTable').DataTable().row($self.parents('tr')).remove().draw('page');
                });
            });

            jQuery(document).on('click', '.invoice_view', function (e) {
                e.preventDefault();

                var $self = jQuery(this);

                jQuery.ajax({
                    url: "/ajax/invoice/view",
                    method: 'get',
                    data: {invoice_id: $self.data('invoice_id')},
                    success: function ($result) {
                        $("#invoice_view_iframe").html('');
                        $("#invoice_view_iframe").append($result);

                        $("#viewInvoiceModal").modal("toggle");
                    }
                });
            });

            jQuery(document).on('click', '#remove_logo', function () {
                if(confirm("Are you sure you want to remove the logo?")) {
                    jQuery.ajax({
                        url: "/ajax/customerlogo/remove",
                        method: 'get',
                        data: {cu_id: jQuery(this).data('cu_id')},
                        success: function (result) {
                            if (result == "success") {
                                jQuery("#customerlogo_div").hide();
                                jQuery("#placeholder_logo").show();
                            }
                        }
                    });
                }
            });

            var $body = $("#progress_body");
            var $button = $("#progress_submit");
            var $onclick_url = $button.attr("href");
            var $lost_button_url = $("#lost_submit").attr("href");
            var $changed = false;

            jQuery(document).on('click', '.first_contact', function (e) {
                $body.html("Are you sure want to register a moment of First Contact? <br> You will have to create a remark!");
                $button.attr("href", $onclick_url + "1");
                $("#addProgressRemarkModal").modal("toggle");
            });

            jQuery(document).on('click', '.working_on', function (e) {
                $body.html("Are you sure want to register that you are in conversation with them? <br> You will have to create a remark!");
                $button.attr("href", $onclick_url + "2");
                $("#addProgressRemarkModal").modal("toggle");
            });

            jQuery(document).on('click', '.proposal_sent', function (e) {
                $("#progress_body").html("Are you sure want to register that you have sent them a proposal? <br> You will have to create a remark!");
                $button.attr("href", $onclick_url + "3");
                $("#addProgressRemarkModal").modal("toggle");
            });

            jQuery(document).on('click', '.contract_sent', function (e) {
                $("#progress_body").html("Are you sure want to register that you have sent them a contract? <br> You will have to create a remark!");
                $button.attr("href", $onclick_url + "4");
                $("#addProgressRemarkModal").modal("toggle");
            });

            jQuery(document).on('click', '.finalize', function (e) {
                $("#finalizeProgressModal").modal("toggle");
            });

            jQuery(document).on('click', '.cancelled', function (e) {
                $("#cancelledProgressModal").modal("toggle");
            });

            jQuery(document).on('click', '.reset', function (e) {
                $("#resetProgressModal").modal("toggle");
            });

            $('#reset_bar').click(function() {
                $to = $('select[name=reset_to]').val();
                $id = $('#customer_id').val();

                jQuery.ajax({
                    url: "/ajax/customerprogress/reset",
                    method: 'get',
                    data: {
                        cu_id: $id,
                        to: $to
                    },
                    success: function (result) {
                        location.reload();
                    }
                });
            });

            $('select[name=rejection_reason]').change(function() {
                $("#lost_submit").attr("href", $lost_button_url + this.value)
                $changed = true;
            });

            $("#lost_submit").click(function(e) {
                if(!$changed){
                    e.preventDefault();
                }
            });


        });
    </script>

@endpush
