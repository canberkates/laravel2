@extends('layouts.backend')

@include( 'scripts.datatables' )
@include( 'scripts.dialogs' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Customer Changes</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Customer Changes</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <!-- Your Block -->
        <div class="block block-rounded block-bordered">
            <ul class="nav nav-tabs nav-tabs-alt" data-toggle="tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" href="#btabs-alt-static-waiting_for_approval">Waiting for approval</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#btabs-alt-static-processed">Processed</a>
                </li>
            </ul>

            <div class="block-content tab-content">
                <div class="tab-pane active" id="btabs-alt-static-waiting_for_approval" role="tabpanel">
                    <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                        <thead>
                        <tr>
                            <th>Company</th>
                            <th>Field</th>
                            <th>From Value</th>
                            <th>To Value</th>
                            <th>Received</th>
                            <th>Approve</th>
                            <th>Deny</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($customer_changes_approval as $cca)
                            <tr>
                                <td>{{$cca->customer->cu_company_name_business}}</td>
                                <td>{{$cca->cuch_field_to_change}}</td>

                                @if ($cca->cuch_field_to_change == "Logo")
                                    @if (strpos($cca->cuch_from_value, 'placeholder') !== false)
                                        <td><img style='height: 35px;' src="../../logo.php?path={{env("SHARED_FOLDER")}}uploads/logos/{{$cca->customer->cu_logo}}"/></td>
                                    @else
                                        <td>NO LOGO</td>
                                    @endif
                                @else
                                    @if ($cca->cuch_from_value == "1")
                                        <td>{{"Yes"}}</td>
                                    @elseif ($cca->cuch_from_value == "0")
                                        <td>{{"No"}}</td>
                                    @else
                                        <td>{{$cca->cuch_from_value}}</td>
                                    @endif
                                @endif

                                @if ($cca->cuch_field_to_change == "Logo")
                                    <td><img style='height: 35px;' src="../../logo.php?path={{env("SHARED_FOLDER")}}uploads/temp/logos/{{$cca->cuch_to_value}}"/></td>
                                @else
                                    @if ($cca->cuch_to_value == "1")
                                        <td>{{"Yes"}}</td>
                                    @elseif ($cca->cuch_to_value == "0")
                                        <td>{{"No"}}</td>
                                    @else
                                        <td>{{$cca->cuch_to_value}}</td>
                                    @endif
                                @endif

                                <td>{{$cca->cuch_timestamp}}</td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <a href="{{ url('/customerchanges/' . $cca->cuch_id . '/approve_confirmation') }}" class="btn" data-toggle="tooltip" data-cuch_id="{{$cca->cuch_id}}" data-placement="left">
                                            <i class="fa fa-check-circle" style="color:green;"></i>
                                        </a>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <a href="{{ url('/customerchanges/' . $cca->cuch_id . '/deny_confirmation') }}" class="btn" data-toggle="tooltip" data-cuch_id="{{$cca->cuch_id}}" data-placement="left">
                                            <i class="fa fa-times-circle" style="color:red;"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="btabs-alt-static-processed" role="tabpanel">
                    <table data-order='[[4, "desc"]]' data-page-length="20" class="table table-bordered table-striped table-vcenter js-dataTable-full">
                        <thead>
                        <tr>
                            <th>Company</th>
                            <th>Field</th>
                            <th>From Value</th>
                            <th>To Value</th>
                            <th>Received</th>
                            <th>Approved/Denied</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($customer_changes_processed as $ccp)
                            <tr>
                                <td>{{$ccp->customer->cu_company_name_business}}</td>
                                <td>{{$ccp->cuch_field_to_change}}</td>
                                @if ($ccp->cuch_field_to_change == "Logo")
                                    <td></td>
                                @else
                                    @if ($ccp->cuch_from_value == "1")
                                        <td>{{"Yes"}}</td>
                                    @elseif ($ccp->cuch_from_value == "0")
                                        <td>{{"No"}}</td>
                                    @else
                                        <td>{{$ccp->cuch_from_value}}</td>
                                    @endif
                                @endif

                                @if ($ccp->cuch_field_to_change == "Logo")
                                    <td></td>
                                @else
                                    @if ($ccp->cuch_to_value == "1")
                                        <td>{{"Yes"}}</td>
                                    @elseif ($ccp->cuch_to_value == "0")
                                        <td>{{"No"}}</td>
                                    @else
                                        <td>{{$ccp->cuch_to_value}}</td>
                                    @endif
                                @endif

                                <td>{{$ccp->cuch_timestamp}}</td>
                                @if ($ccp->cuch_approve_deny == 1)
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <i class="fa fa-check-circle" style="color:green;"></i>
                                        </div>
                                    </td>
                                @else
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <i class="fa fa-times-circle" style="color:red;"></i>
                                        </div>
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END Your Block -->
    </div>
    <!-- END Page Content -->
@endsection

