@extends('layouts.backend')

@include( 'scripts.datatables' )
@include( 'scripts.dialogs' )

@section('content')
    <!-- Page Content -->
    <div class="content">
        <!-- Your Block -->
        <div class="block block-rounded block-bordered">
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Approving Customer Change of {{$customer_change->customer->cu_company_name_business}}
                </h3>
            </div>
            <div class="block-content block-content-full">
                <form method="post"
                      action="{{action('CustomerChangesController@approveChange', $customer_change->cuch_id)}}">
                    @csrf
                    <input name="_method" type="hidden" value="post">
                    <div class="row">
                        <div class="col-md-6">
                            @if ($customer_change->cuch_field_to_change == "Logo")
                                <p>
                                    <strong>{{$customer_change->cuch_field_to_change}}</strong> will be set <br />
                                    <strong>FROM</strong>
                                    @if (strpos($customer_change->cuch_from_value, 'placeholder') !== false)
                                        <td><img style='height: 150px;' src="../../logo.php?path={{env("SHARED_FOLDER")}}uploads/logos/{{$customer_change->customer->cu_logo}}"/></td>
                                    @else
                                        <td>NO LOGO</td>
                                    @endif
                                    <br/>
                                    <strong>TO</strong>
                                    <td><img style='height: 150px;' src="../../logo.php?path={{env("SHARED_FOLDER")}}uploads/temp/logos/{{$customer_change->cuch_to_value}}"/></td>
                                </p>
                            @else
                                Are you sure you want to approve the changes for <b>{{$customer_change->customer->cu_company_name_business}}</b>?<br/><br />

                                @if (($customer_change->cuch_from_value == "0" || $customer_change->cuch_from_value == "1") && ($customer_change->cuch_to_value == "0" || $customer_change->cuch_to_value == "1"))
                                    <b>{{$customer_change->cuch_field_to_change}}</b> will be set <b>FROM</b> {{$yesno_from}} <b>TO</b> {{$yesno_to}}.<br /><br/>
                                    <input name="to_value" type="hidden" value="{{$customer_change->cuch_to_value}}">
                                @else
                                    <b>{{$customer_change->cuch_field_to_change}}</b> will be set<br /><br/>
                                    <b>FROM: </b> {{$customer_change->cuch_from_value}}<br/><br/>
                                    <div class="form-group row">
                                        <label class="col-sm-1 col-form-label"
                                               for="to_value">TO:</label>
                                        <div class="col-sm-7">
                                            <textarea class="form-control"
                                                   name="to_value">{{$customer_change->cuch_to_value}}</textarea>
                                        </div>
                                    </div>
                                @endif
                            @endif

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Yes</button>
                                    <a href="../" class="btn btn-primary">No</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END Your Block -->
    </div>
    <!-- END Page Content -->
@endsection
