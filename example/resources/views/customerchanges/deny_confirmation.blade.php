@extends('layouts.backend')

@include( 'scripts.datatables' )
@include( 'scripts.dialogs' )

@section('content')
    <!-- Page Content -->
    <div class="content">
        <!-- Your Block -->
        <div class="block block-rounded block-bordered">
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Deny Customer Change of {{$customer_change->customer->cu_company_name_business}}
                </h3>
            </div>
            <div class="block-content block-content-full">
                <form method="post"
                      action="{{action('CustomerChangesController@denyChange', $customer_change->cuch_id)}}">
                    @csrf
                    <input name="_method" type="hidden" value="post">
                    <div class="row">
                        <div class="col-md-6">
                            @if ($customer_change->cuch_field_to_change == "Logo")
                                <p>
                                    Change of <strong>{{$customer_change->cuch_field_to_change}}</strong> will be denied <br />
                                    <strong>FROM</strong>
                                    @if (strpos($customer_change->cuch_from_value, 'placeholder') !== false)
                                        <td><img style='height: 150px;' src="../../logo.php?path={{env("SHARED_FOLDER")}}uploads/logos/{{$customer_change->customer->cu_logo}}"/></td>
                                    @else
                                        <td>NO LOGO</td>
                                    @endif
                                    <br/>
                                    <strong>TO</strong>
                                    <td><img style='height: 150px;' src="../../logo.php?path={{env("SHARED_FOLDER")}}uploads/temp/logos/{{$customer_change->cuch_to_value}}"/></td>
                                </p>
                            @else
                                Are you sure you want to deny the changes for <b>{{$customer_change->customer->cu_company_name_business}}</b>?<br/><br />

                                You'll deny the request to change <b>{{$customer_change->cuch_field_to_change}}</b> <b>FROM: </b>{{$from}} <b>TO:</b> {{$to}}<br/><br/>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label"
                                           for="reason">Reason:</label>
                                    <div class="col-sm-7">
                                        <textarea type="text" class="form-control"
                                               name="reason"></textarea>
                                    </div>
                                </div>
                            @endif

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label" for="send_mail">Send email</label>
                                <div class="col-sm-7">
                                    <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                        <input type="checkbox" class="custom-control-input"
                                               id="send_mail"
                                               name="send_mail">
                                        <label class="custom-control-label"
                                               for="send_mail"></label>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Deny</button>
                                    <a href="../" class="btn btn-primary">Back</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END Your Block -->
    </div>
    <!-- END Page Content -->
@endsection
