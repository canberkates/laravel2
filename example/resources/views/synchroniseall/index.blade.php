@extends('layouts.backend')

@include('scripts.datepicker')

@section('content')

    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Sirelo Synchronise</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url("admin/")}}">Admin</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        <div class="row">
            <div class="col-md-6">
                <div class="block block-rounded block-bordered">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Sync Sirelo All :)</h3>
                    </div>
                    <div id="progress"></div>
                    <div class="block-content">
                        @if($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        {{$error}}<br>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="mb-5" method="post" id="sync"
                              action="{{action('SireloSynchroniseAllController@update')}}">
                            @csrf
                            <input name="_method" type="hidden" value="POST">

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-5 col-form-label"
                                       for="logo_skip">Skip logos</label>
                                <div class="col-sm-5">
                                    <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                        <input type="checkbox" class="custom-control-input" id="logo_skip"
                                               name="logo_skip">
                                        <label class="custom-control-label" for="logo_skip"></label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-5 col-form-label"
                                       for="only_answer">Only retrieve answer</label>
                                <div class="col-sm-5">
                                    <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                        <input type="checkbox" class="custom-control-input" id="only_answer"
                                               name="only_answer">
                                        <label class="custom-control-label" for="only_answer"></label>
                                    </div>
                                </div>
                            </div>

                            <h2 class="content-heading pt-0"></h2>

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>
                            </div>


                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#sync').on('submit', function() {
                setInterval(function(){
                    $.getJSON('/progress', function(data) {
                        $('#progress').html(data[0]);
                    });
                }, 1000);
                return false;
            });
        });
    </script>
@stop




