@extends('layouts.backend')
@include( 'scripts.dialogs' )
@include( 'scripts.editor' )
@pushonce( 'scripts:serviceprovider-newsletter-edit' )
    <script src="{{ asset('/js/custom/serviceprovider/serviceprovider_newsletter_edit.js') }}"></script>
@endpushonce

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Edit Newsletter Block of {{$customer->cu_company_name_business}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../../../">Service Providers</a></li>
                        <li class="breadcrumb-item"><a href="../../edit">{{$customer->cu_company_name_business}}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Newsletter Block edit</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="block block-rounded block-bordered">
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    {{$error}}<br>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <ul class="nav nav-tabs nav-tabs-alt" data-toggle="tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" href="#btabs-alt-static-general">General</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-content">Content</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-origins">Origins</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-destinations">Destinations</a>
                        </li>
                    </ul>

                    <div class="block-content tab-content">
                        <div class="tab-pane active" id="btabs-alt-static-general" role="tabpanel">
                            <form class="mb-5" method="post" action="{{action('ServiceProviderNewsletterBlockController@update', $newsletterblock_id)}}">
                                @csrf
                                <input name="_method" type="hidden" value="PATCH">
                                <input name="form_name" type="hidden" value="General">

                                <h2 class="content-heading pt-0">General</h2>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="slot">Slot:</label>
                                    <div class="col-sm-7">
                                        <select type="text" class="form-control" name="slot">
                                            <option value=""></option>
                                            @foreach($newsletterslots as $id => $slot)
                                                <option @if ($id == $newsletterblock->seprnebl_slot) selected @endif value="{{$id}}">{{$slot}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="description">Description:</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="description"
                                               value="{{$newsletterblock->seprnebl_description}}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="status">Status:</label>
                                    <div class="col-sm-7">
                                        <select type="text" class="form-control" name="status">
                                            @foreach($statuses as $id => $status)
                                                <option @if ($id == $newsletterblock->seprnebl_status) selected @endif value="{{$id}}">{{$status}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <h2 class="content-heading pt-0">Filters - Moves</h2>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="international_moves">International moves:</label>
                                    <div class="col-sm-7">
                                        <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                            <input type="checkbox" class="custom-control-input" id="international_moves"
                                                   name="international_moves" {{($newsletterblock->seprnebl_international_moves ? "checked" : "")}}>
                                              <label class="custom-control-label"
                                               for="international_moves"></label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="national_moves">National moves:</label>
                                    <div class="col-sm-7">
                                        <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                            <input type="checkbox" class="custom-control-input" id="national_moves"
                                                   name="national_moves" {{($newsletterblock->seprnebl_national_moves ? "checked" : "")}}>
                                              <label class="custom-control-label"
                                               for="national_moves"></label>
                                        </div>
                                    </div>
                                </div>

                                <h2 class="content-heading pt-0">Filters - Languages</h2>

                                <div id="languages">
                                    <button type="button" id="select_all" class="btn btn-sm btn-primary" data-toggle="tooltip">Select All</button>
                                    <button type="button" id="deselect_all" class="btn btn-sm btn-primary" data-toggle="tooltip">Deselect All</button>

                                    @foreach($languages as $lang)
                                        @if ($lang->la_code == 'RU' || $lang->la_code == 'PL' || $lang->la_code == 'PT')
                                            @continue
                                        @endif

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-3 col-form-label" for="checkbox_languages_{{$lang->la_code}}">{{$lang->la_language}}:</label>
                                            <div class="col-sm-7">
                                                <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                                    <input type="checkbox" class="custom-control-input" id="checkbox_languages_{{$lang->la_code}}"
                                                           name="blocklanguages[{{$lang->la_code}}]" {{(in_array($lang->la_code, $nebl_languages) ? "checked" : "")}}>
                                                      <label class="custom-control-label"
                                                       for="checkbox_languages_{{$lang->la_code}}"></label>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>

                                <h2 class="content-heading pt-0"></h2>

                                <div class="row">
                                    <div class="col-md-1"></div>
                                    <div class="form-group col-md-8">
                                        <button type="submit" class="btn btn-primary">Update</button>
                                        <a class="btn btn-primary" href="../../edit">
                                            Back
                                        </a>
                                    </div>
                                </div>

                            </form>
                        </div>

                        <div class="tab-pane" id="btabs-alt-static-content" role="tabpanel">
                            <form class="mb-5" method="post" action="{{action('ServiceProviderNewsletterBlockController@update', $newsletterblock_id)}}">
                                @csrf
                                <input name="_method" type="hidden" value="PATCH">
                                <input name="form_name" type="hidden" value="Content">

                                <h2 class="content-heading pt-0">General</h2>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-1 col-form-label" for="title">Title:</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="title"
                                               value="{{$newsletterblock->seprnebl_title}}">
                                    </div>
                                </div>



                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-1 col-form-label" for="content">Content:</label>
                                    <div class="col-sm-9">
                                        <textarea id="textarea_content" name="blockcontent">{{$newsletterblock->seprnebl_content}}</textarea>
                                    </div>
                                </div>


                                <h2 class="content-heading pt-0"></h2>

                                <div class="row">
                                    <div class="col-md-1"></div>
                                    <div class="form-group col-md-8">
                                        <button type="submit" class="btn btn-primary">Update</button>
                                        <a style="color:white;" data-seprnebl_id="{{$newsletterblock->seprnebl_id}}" class="btn btn-primary" id="preview_newsletterblock">
                                            Preview
                                        </a>
                                        <a style="color:white;" data-seprnebl_id="{{$newsletterblock->seprnebl_id}}" class="btn btn-primary" id="send_newsletterblock">
                                            Send preview to your email
                                        </a>
                                        <a class="btn btn-primary" href="../../edit">
                                            Back
                                        </a>
                                    </div>
                                </div>

                            </form>
                        </div>

                        <div class="tab-pane" id="btabs-alt-static-origins" role="tabpanel">

                            <a href="#" class="btn btn-primary select_all_or">Select all</a>
                            <a href="#" class="btn btn-primary deselect_all_or">Deselect all</a>

                            <br /><br />

                            <form class="mb-5" method="post" action="{{action('ServiceProviderNewsletterBlockController@update', $newsletterblock_id)}}">
                                @csrf
                                <input name="_method" type="hidden" value="PATCH">
                                <input name="form_name" type="hidden" value="Origins">

                                @foreach($macro_regions as $continent => $mare_row)
                                    <h3>{{$continent}}</h3>
                                    <div class="row">
                                        @foreach($mare_row as $id => $row)
                                            <div class="col-md-3">
                                                <div class="well" style="position: relative;display: block;">
                                                    <div class="block block-rounded block-bordered">
                                                        <div class="block-header block-header-default">
                                                            <h3 class="block-title"> {{$row['mare_name']}}</h3>
                                                        </div>
                                                        <div class="block-content">
                                                            @foreach($countries[$id] as $co_code => $country)
                                                                <div class="form-group row">
                                                                    <label class="col-sm-9 col-form-label"
                                                                           for="origins[{{$co_code}}]">{{$country}}</label>
                                                                    <div class="col-sm-3">
                                                                        <div class="custom-control custom-switch custom-control-inline custom-control-primary">
                                                                            <input type="checkbox" class="custom-control-input"
                                                                                   id="origins[{{$co_code}}]"
                                                                                   name="origins[{{$co_code}}]"
                                                                                    {{(isset($originschecked[$co_code]) && $originschecked[$co_code] ? "checked" : "")}}>
                                                                            <label class="custom-control-label"
                                                                                   for="origins[{{$co_code}}]"></label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @endforeach

                                <div class="row">
                                    <div class="form-group col-md-8">
                                        <button type="submit" class="btn btn-primary">Update</button>
                                        <a class="btn btn-primary" href="../../edit">
                                            Back
                                        </a>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="tab-pane" id="btabs-alt-static-destinations" role="tabpanel">
                            <a href="#" class="btn btn-primary select_all_or">Select all</a>
                            <a href="#" class="btn btn-primary deselect_all_or">Deselect all</a>

                            <br /><br />

                            <form class="mb-5" method="post" action="{{action('ServiceProviderNewsletterBlockController@update', $newsletterblock_id)}}">
                                @csrf
                                <input name="_method" type="hidden" value="PATCH">
                                <input name="form_name" type="hidden" value="Destinations">

                                @foreach($macro_regions as $continent => $mare_row)
                                    <h3>{{$continent}}</h3>
                                    <div class="row">
                                        @foreach($mare_row as $id => $row)
                                            <div class="col-md-3">
                                                <div class="well" style="position: relative;display: block;">
                                                    <div class="block block-rounded block-bordered">
                                                        <div class="block-header block-header-default">
                                                            <h3 class="block-title"> {{$row['mare_name']}}</h3>
                                                        </div>
                                                        <div class="block-content">
                                                            @foreach($countries[$id] as $co_code => $country)
                                                                <div class="form-group row">
                                                                    <label class="col-sm-9 col-form-label"
                                                                           for="destinations[{{$co_code}}]">{{$country}}</label>
                                                                    <div class="col-sm-3">
                                                                        <div class="custom-control custom-switch custom-control-inline custom-control-primary">
                                                                            <input type="checkbox" class="custom-control-input"
                                                                                   id="destinations[{{$co_code}}]"
                                                                                   name="destinations[{{$co_code}}]"
                                                                                    {{(isset($destinationschecked[$co_code]) && $destinationschecked[$co_code] ? "checked" : "")}}>
                                                                            <label class="custom-control-label"
                                                                                   for="destinations[{{$co_code}}]"></label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @endforeach

                                <div class="row">
                                    <div class="form-group col-md-8">
                                        <button type="submit" class="btn btn-primary">Update</button>
                                        <a class="btn btn-primary" href="../../edit">
                                            Back
                                        </a>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="viewNewsletterModal" class="modal fade" role="dialog" >
        <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content" style="width:800px;">
                <div class="modal-header">
                    <h5 class="modal-title">Viewing Newsletter</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="newsletter_iframe">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@push( 'scripts' )

    <script>
        $( "#select_all" ).click(function() {
            var checkboxes = $(this).closest('#languages').find(':checkbox');

            checkboxes.prop('checked', true);
        });

        $( "#deselect_all" ).click(function() {
            var checkboxes = $(this).closest('#languages').find(':checkbox');

            checkboxes.prop('checked', false);
        });

        $( "#preview_newsletterblock" ).click(function(e) {
            e.preventDefault();

            var $self = jQuery(this);

            jQuery.ajax({
                url: "/ajax/view_newsletterblock_preview_or_send",
                method: 'get',
                data: { seprnebl_id : $self.data('seprnebl_id') },
                success: function( $result) {
                	var newsletter_iframe = $("#newsletter_iframe");

                	newsletter_iframe.html('');
                    newsletter_iframe.append($result);

                    $("#viewNewsletterModal").modal("toggle");
                }
            });
        });

        $( "#send_newsletterblock" ).click(function(e) {
            e.preventDefault();

            var $self = jQuery(this);

            jQuery.ajax({
                url: "/ajax/view_newsletterblock_preview_or_send",
                method: 'get',
                data: { seprnebl_id : $self.data('seprnebl_id'), mail: 1 },
                success: function( $result) {
                	var newsletter_iframe = $("#newsletter_iframe");

                	newsletter_iframe.html('');
                    newsletter_iframe.append($result);

                    $("#viewNewsletterModal").modal("toggle");
                }
            });
        });
    </script>
    <script src="https://cdn.tiny.cloud/1/qk69cz08kh45b4art9orrfek5ajlgggajl9k6owydfanq7rl/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<!--    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>-->
    <script>tinymce.init({ selector:"#textarea_content", plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking save table contextmenu directionality emoticons template paste textcolor colorpicker textpattern imagetools"], height: 500 });</script>

@endpush
