@extends('layouts.backend')

@include('scripts.datepicker')

@section('content')

    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Edit a ledger account</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url("/admin/ledgeraccounts")}}">Ledger accounts</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        <div class="row">
            <div class="col-md-6">
                <div class="block block-rounded block-bordered">
                    <div class="block-content">
                        @if($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        {{$error}}<br>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="mb-5" method="post" action="{{action('LedgerAccountController@update', $ledger->leac_number)}}">
                            @csrf
                            <input name="_method" type="hidden" value="PATCH">

                            <h2 class="content-heading pt-0">General</h2>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="value">Number:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" autocomplete="off" disabled name="number" value="{{$ledger->leac_number}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="extra">Type:</label>
                                <div class="col-sm-7">
                                    <select type="text" class="form-control" autocomplete="off" name="ledgertype">
                                        <option value=""></option>
                                        @foreach($ledgeraccounttypes as $id => $value)
                                            <option value="{{$id}}" @if($id == $ledger->leac_type) selected @endif >{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="extra">Name:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="name" autocomplete="off" value="{{$ledger->leac_name}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="extra">Turnover type:</label>
                                <div class="col-sm-7">
                                    <select type="text" class="form-control" autocomplete="off" name="turnovertype">
                                        <option value=""></option>
                                        @foreach($ledgeraccounttypes as $id => $value)
                                            <option value="{{$id}}" @if($ledger->leac_turnover_type == $id) selected @endif >{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="extra">VAT %:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" autocomplete="off" name="vat" value="{{$ledger->leac_vat}}">
                                </div>
                            </div>

                            <h2 class="content-heading pt-0"></h2>

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection




