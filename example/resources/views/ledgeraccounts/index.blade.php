@extends('layouts.backend')

@include( 'scripts.datatables' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Ledger accounts</h1>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <a href="/admin/newsletters/create"></a>

    <div class="content">
        <div class="block block-rounded block-bordered">
            <div class="block-content block-content-full">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Ledger accounts
                        <a href="{{ url('admin/ledgeraccounts/create')}}"><button data-toggle="click-ripple" class="btn btn-primary">Add a Ledger Account</button></a>
                    </h3>
                </div>
                <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                    <thead>
                    <tr>
                        <th>Number</th>
                        <th>Type</th>
                        <th>Name</th>
                        <th>Turnover type</th>
                        <th>VAT %</th>
                        <th>Modify</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($ledgeraccounts as $ledgeraccount)
                        <tr>
                            <td>{{$ledgeraccount->leac_number}}</td>
                            <td>{{$ledgeraccounttypes[$ledgeraccount->leac_type]}}</td>
                            <td>{{$ledgeraccount->leac_name}}</td>
                            <td>{{$turnovertypes[$ledgeraccount->leac_turnover_type]}}</td>
                            <td>{{$ledgeraccount->leac_vat}}</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <a class="btn btn-sm btn-primary"
                                       data-toggle="tooltip"
                                       data-placement="left"
                                       title="edit"
                                       href="{{ url('/admin/ledgeraccounts/' . $ledgeraccount->leac_number . '/edit')}}">
                                        <i class="fa fa-pencil-alt"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
