@extends('layouts.backend')

@push( 'styles' )
    <link rel="stylesheet" href="{{ asset('js/plugins/adyen/adyen.cardtype.css') }}">
@endpush
@push('scripts')
    <script src="{{env("ADYEN_API_ENCRYPTION_URL")}}"></script>
    <script src="{{ asset('js/plugins/adyen/adyen.cardtype.min.js') }}"></script>
    <script>
        var options = {};

        $(function(){
            var form = document.getElementById('adyen-encrypted-form');
            options = {
                enableValidations: true
            };
            var x = adyen.createEncryptedForm(form, options);
            x.addCardTypeDetection(document.getElementById('cardType'));
        });
    </script>
@endpush
@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Edit/Add a credit card</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url("/customers/")}}">Customers</a></li>
                        <li class="breadcrumb-item"><a href="{{url("/customers/".$customer->cu_id."/edit")}}">{{$customer->cu_company_name_business}}</a></li>
                        <li class="breadcrumb-item">Credit card create</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        {{$error}}<br>
                    @endforeach
                </ul>
            </div>
        @endif
        @if($message)
            <div class="alert alert-info">
               {{$message}}
            </div>
        @endif
        @if(session()->has('message'))
            <div class="alert alert-info">
                {{ session()->get('message') }}
            </div>
        @endif
        <div class="row">
            <div class="col-md-6">
                <div class="block block-rounded block-bordered">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Add a <b><i><u>Credit Card</u></i></b>
                            to {{$customer->cu_company_name_business}}</h3>
                    </div>
                    <div class="block-content">
                        <form method="post" id="adyen-encrypted-form" name="adyen-encrypted-form" action="{{action('CreditCardController@store')}}">
                            @csrf
                            <input name="_method" type="hidden" value="post">
                            <input name="customer_id" type="hidden" value="{{$customer->cu_id}}">

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="card_number">Card number:</label>
                                <div class="col-sm-5">
                                    <input type="text"
                                           class="form-control {{$errors->has('card_number') ? 'is-invalid' : ''}}"
                                           data-encrypted-name="number"
                                           id="adyen_text_number"
                                           placeholder="5555 4444 3333 1111">
                                </div>
                                <div class="col-sm-3">
                                    <span id="cardType"></span>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="card_holder">Card holder:</label>
                                <div class="col-sm-7">
                                    <input type="text"
                                           class="form-control {{$errors->has('card_holder') ? 'is-invalid' : ''}}"
                                           data-encrypted-name="holderName"
                                           id="adyen_text_holderName"
                                           placeholder="John Doe">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="expiry_month">Expiry month:</label>
                                <div class="col-sm-7">
                                    <input type="text"
                                           class="form-control {{$errors->has('expiry_month') ? 'is-invalid' : ''}}"
                                           data-encrypted-name="expiryMonth"
                                           id="adyen_text_expiryMonth"
                                           placeholder="08">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="expiry_year">Expiry Year:</label>
                                <div class="col-sm-7">
                                    <input type="text"
                                           class="form-control {{$errors->has('expiry_year') ? 'is-invalid' : ''}}"
                                           data-encrypted-name="expiryYear"
                                           id="adyen_text_expiryYear"
                                           placeholder="2018">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="cvc">CVC/CVV/CID:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control {{$errors->has('cvc') ? 'is-invalid' : ''}}"
                                           data-encrypted-name="cvc"
                                           id="adyen_text_cvc"
                                           placeholder="737">
                                </div>
                            </div>

                            <input type="hidden" value="{{$timestamp}}" data-encrypted-name="generationtime">


                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" name="form_submit"  id="submit_form_submit" class="btn btn-primary">Add Credit Card</button>
                                </div>
                            </div>


                        </form>

                    </div>
                </div>
            </div>
            <div class="col-md-6">
                @foreach($creditcards as $creditcard)
                    <div class="block block-rounded block-bordered">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Card {{$creditcard->adcade_id}} -
                                @if($creditcard->adcade_enabled)
                                    ACTIVE
                                @elseif($creditcard->has_expired)
                                    EXPIRED
                                @else
                                    INACTIVE
                                @endif
                            </h3>
                        </div>
                        <div class="block-content">
                            <form method="post" action="{{action('CreditCardController@updateCard', $customer->cu_id)}}">
                                @csrf
                                <input name="_method" type="hidden" value="post">
                                <input name="customer_id" type="hidden" value="{{$customer->cu_id}}">
                                <input name="credit_card_id" type="hidden" value="{{$creditcard->adcade_id}}">

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="expiry_month">Expiry month:</label>
                                    <div class="col-sm-7">
                                        <input type="text"
                                               class="form-control {{$errors->has('expiry_month') ? 'is-invalid' : ''}}"
                                               name="expiry_month"
                                               value="{{$creditcard->adcade_card_expiry_month}}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="expiry_year">Expiry Year:</label>
                                    <div class="col-sm-7">
                                        <input type="text"
                                               class="form-control {{$errors->has('expiry_year') ? 'is-invalid' : ''}}"
                                               name="expiry_year"
                                               value="{{$creditcard->adcade_card_expiry_year}}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="last_digits">Last digits:</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="last_digits" disabled
                                               value="{{$creditcard->adcade_card_last_digits}}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="card_status">Status:</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="card_status" disabled
                                               value="@if($creditcard->adcade_enabled)ACTIVE @elseif($creditcard->has_expired)EXPIRED @else INACTIVE @endif">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-1"></div>
                                    <div class="form-group col-md-8">
                                        <button type="submit" name="update" class="btn btn-primary">Update expiry date</button>
                                        @if(!$creditcard->has_expired && !$creditcard->adcade_enabled)
                                            <button type="submit" name="enable" class="btn btn-primary">Enable</button>
                                        @endif
                                        <button type="submit" name="remove" class="btn btn-primary">Remove</button>
                                    </div>
                                </div>

                            </form>

                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection
@push('scripts')
    <script>
        $(document).ready(function () {

        });
    </script>
@endpush
