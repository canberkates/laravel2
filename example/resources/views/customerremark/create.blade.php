@extends('layouts.backend')
@include('scripts.datepicker')
@include('scripts.editor')
@section('content')



    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                @if($request->type && $request->reason)
                    <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Add progress ({{$progresstypes[$request->type]}} / {{$progresslostcontracttypes[$request->reason]}}) to {{$customer->cu_company_name_business}}</h1>
                @elseif($request->type)
                    <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Add progress ({{$progresstypes[$request->type]}}) to {{$customer->cu_company_name_business}}</h1>
                @else
                    <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Add a Remark to {{$customer->cu_company_name_business}}</h1>
                @endif
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../../">@if ($customer->cu_type == 1) {{"Customers"}} @elseif($customer->cu_type == 6) {{"Resellers"}} @elseif($customer->cu_type == 2) {{"Service Providers"}}@else {{"Affiliate Partners"}} @endif</a></li>
                        <li class="breadcrumb-item"><a href="../edit">{{$customer->cu_company_name_business}}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Add remark</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->


    <div class="content">

        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        {{$error}}<br>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="block block-rounded block-bordered">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">General</h3>
                    </div>
                    <div class="block-content">
                        <form class="mb-5" method="post" action="{{action('CustomerRemarkController@store')}}">
                            @csrf
                            <input name="_method" type="hidden" value="post">
                            <input name="customer_id" type="hidden" value="{{$customer->cu_id}}">
                            <input name="progress_type" type="hidden" value="{{$request->type}}">
                            <input name="progress_reason" type="hidden" value="{{$request->reason}}">

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="date">Date:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="js-datepicker form-control" id="example-datepicker3" name="date" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd" autocomplete="off" value="{{date('Y-m-d')}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="employee">Employee:</label>
                                <div class="col-sm-7">
                                    <select type="text" class="form-control" name="employee">
                                        <option value=""></option>
                                        @foreach($users as $user)
                                            <option @if ($user->id == \Illuminate\Support\Facades\Auth::id()) selected @endif value="{{$user->id}}">{{$user->us_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="department">Department:</label>
                                <div class="col-sm-7">
                                    <select type="text" class="form-control" name="department">
                                        <option value=""></option>
                                        @foreach($departments as $id => $department)
                                            <option value="{{$id}}">{{$department}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="medium">Medium:</label>
                                <div class="col-sm-7">
                                    <select type="text" class="form-control" name="medium">
                                        <option value=""></option>
                                        @foreach($mediums as $id => $medium)
                                            <option value="{{$id}}">{{$medium}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="direction">Direction:</label>
                                <div class="col-sm-7">
                                    <select type="text" class="form-control" name="direction">
                                        <option value=""></option>
                                        @foreach($directions as $id => $direction)
                                            <option value="{{$id}}">{{$direction}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="title">Title of remark:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="title" autocomplete="off" value="{{$title}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="customer_remark">Remark:</label>
                                <div class="col-sm-7">
                                    <textarea rows="4" type="text" class="form-control js-summernote" name="customer_remark"></textarea>
                                </div>
                            </div>

                            <h2 class="content-heading pt-0"></h2>

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Create</button>
                                    <a class="btn btn-primary" href="../edit">
                                        Back
                                    </a>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection




