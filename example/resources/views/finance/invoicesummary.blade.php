@extends('layouts.backend')
@include('scripts.datatables')
@include('scripts.datepicker')
@include('scripts.forms')
@include('scripts.dialogs' )
@include('scripts.select2')


@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Invoice summary</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/finance">Finance</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="col-md-6">
            <div class="block block-rounded block-bordered">
                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                {{$error}}<br>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="block-header block-header-default">
                    <h3 class="block-title">
                        Filters
                    </h3>
                </div>
                <div class="block-content block-content-full">

                    <form class="mb-5" method="post" action="{{action('InvoiceSummaryController@filteredIndex')}}">
                        @csrf
                        <input name="_method" type="hidden" value="post">

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label" for="type">Period:</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control drp drp-default" name="period"
                                       @if($selected_period != null) value="{{$selected_period}}"
                                       @else value="{{date("Y/m/d",  strtotime("-30 days"))}} - {{date("Y/m/d")}}"
                                       @endif autocomplete="off"/>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label" for="type">Invoice date:</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control drp drp-default" name="invoicedate"
                                       @if($selected_invoicedate != null) value="{{$selected_invoicedate}}"
                                       @else value="{{date("Y/m/d",  strtotime("-30 days"))}} - {{date("Y/m/d")}}"
                                       @endif autocomplete="off"/>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label">Customer:</label>
                            <div class="col-sm-6">
                                <select type="text" class="js-select2 form-control" name="mover">
                                    <option></option>
                                    @foreach($customers as $customer)
                                        <option
                                            value="{{$customer->cu_id}}">{{$customer->cu_company_name_legal}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label">Paid:</label>
                            <div class="col-sm-6">
                                <select type="text" class="form-control"
                                        name="paid">
                                    <option
                                        @if(isset($invoices_paid_filter) && $invoices_paid_filter != null && $invoices_paid_filter == "both") selected
                                        @endif value="both" @if($invoices_paid_filter == null) selected @endif>Both
                                    </option>
                                    <option
                                        @if(isset($invoices_paid_filter) && $invoices_paid_filter != null && $invoices_paid_filter == "1") selected
                                        @endif  value="1">Yes
                                    </option>
                                    <option
                                        @if(isset($invoices_paid_filter) && $invoices_paid_filter != null && $invoices_paid_filter == "0") selected
                                        @endif  value="0">No
                                    </option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label">Category:</label>
                            <div class="col-sm-6">
                                <select type="text" class="form-control"
                                        name="category">
                                    <option value=""></option>
                                    <option
                                        @if(isset($invoices_category_filter)  && $invoices_category_filter == "1") selected
                                        @endif  value="1">Only INT
                                    </option>
                                    <option
                                        @if(isset($invoices_category_filter)  && $invoices_category_filter == "2") selected
                                        @endif  value="2">Only NAT
                                    </option>
                                    <option
                                        @if(isset($invoices_category_filter)  && $invoices_category_filter == "3") selected
                                        @endif  value="3">INT + NAT
                                    </option>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="form-group col-md-8">
                                <button type="submit" class="btn btn-primary">Filter</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
        @if ($selected_period != null)
            <div class="col-md-12">
                <div class="block block-rounded block-bordered">
                    <div class="block-content block-content-full">
                        <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                            <thead>
                            <tr>
                                <th>Customer</th>
                                <th>Country</th>
                                <th>Timestamp</th>
                                <th>Categories</th>
                                <th>Invoice number</th>
                                <th>Invoice date</th>
                                <th>Expiration date</th>
                                <th>Days overdue</th>
                                <th>Reminder</th>
                                <th>Payment method</th>
                                <th>Amount excl. VAT €</th>
                                <th>Amount incl. VAT €</th>
                                <th>Amount VAT FC</th>
                                <th>Paid</th>
                                <th>Amount paid</th>
                                <th>Amount left</th>
                                <th>Payment date(s)</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($invoices as $invoice)
                                @if($invoices_paid_filter == "both" || ($invoices_paid_filter == "1" && ($invoice->in_amount_netto_eur == round($amount_paired_per_invoice[$invoice->in_id], 2))) || ($invoices_paid_filter == "0" && ($invoice->in_amount_netto_eur != round($amount_paired_per_invoice[$invoice->in_id], 2))))
                                    <tr>
                                        <td>{{$invoice->cu_company_name_legal}}</td>
                                        <td>{{$countries[$invoice->cu_co_code]}}</td>
                                        <td>{{$invoice->in_timestamp}}</td>
                                        @if($invoice->categories)
                                            <td>{{implode(', ', $invoice->categories)}}</td>
                                        @else
                                            <td></td>
                                        @endif
                                        <td>{{$invoice->in_number}}</td>
                                        <td>{{$invoice->in_date}}</td>
                                        <td>{{$invoice->in_expiration_date}}</td>
                                        <td>@if ($invoice->in_amount_netto_eur != round($amount_paired_per_invoice[$invoice->in_id], 2) && $invoice->days_overdue > 0) {{$invoice->days_overdue}} @else {{"0"}} @endif</td>
                                        <td>{{$paymentreminder[$invoice->in_payment_reminder]}}</td>
                                        <td>{{$paymentmethods[$invoice->in_payment_method]}}</td>
                                        <td>{{$currency_tokens["EUR"]." ".number_format($invoice->in_amount_gross_eur, 2, ',', '.')}}</td>
                                        <td>{{$currency_tokens["EUR"]." ".number_format($invoice->in_amount_netto_eur, 2, ',', '.')}}</td>
                                        <td>{{$currency_tokens[$invoice->in_currency]." ".number_format($invoice->in_amount_gross, 2, ',', '.')}}</td>
                                        <td>{{(($invoice->in_amount_netto_eur == round($amount_paired_per_invoice[$invoice->in_id], 2)) ? "Yes" : "No")}}</td>
                                        <td>{{$currency_tokens["EUR"]." ".number_format($amount_paired_per_invoice[$invoice->in_id], 2, ',', '.')}}</td>
                                        <td>{{$currency_tokens["EUR"]." "}}{{number_format($invoice->in_amount_netto_eur - $amount_paired_per_invoice[$invoice->in_id], 2, ',', '.')}}</td>
                                        <td>@if(!empty($payment_dates_per_invoice[$invoice->in_id])) {{implode(", ", $payment_dates_per_invoice[$invoice->in_id])}} @else {{"-"}} @endif</td>
                                        <td class="text-center">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-sm btn-primary invoice_view mr-1"
                                                        data-toggle="tooltip"
                                                        title="View" data-invoice_id="{{$invoice->in_id}}">
                                                    <i class="fa fa-eye"></i>
                                                </button>
                                                <a class="btn btn-sm btn-primary mr-1" data-toggle="tooltip"
                                                   data-placement="left"
                                                   href="{{ url('customers/' . $customer->cu_id . '/invoice/' . $invoice->in_id.'/send_invoice')}}">
                                                    <i class="fa fa-envelope"></i>
                                                </a>
                                                <a class="btn btn-sm btn-primary mr-1" data-toggle="tooltip"
                                                   data-placement="left"
                                                   target="_blank"
                                                   href="../../invoice_download.php?invoice={{$invoice->in_number}}&date={{$invoice->in_date}}&env={{env("SHARED_FOLDER")}}">
                                                    <i class="fa fa-download"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                            <tfoot>
                            <th colspan="8"></th>
                            <th>€{{number_format($invoice_totals['total_amount_gross'], 2, ',', '.')}}</th>
                            <th>€{{number_format($invoice_totals['total_amount_netto'], 2, ',', '.')}}</th>
                            <th colspan="3"></th>
                            <th>€{{number_format($invoice_totals['total_amount_paid'], 2, ',', '.')}}</th>
                            <th>€{{number_format($invoice_totals['total_amount_netto'] - $invoice_totals['total_amount_paid'], 2, ',', '.')}}</th>
                            <th colspan="2"></th>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        @endif
    </div>
    <div id="viewInvoiceModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content" style="width:700px;">
                <div class="modal-header">
                    <h5 class="modal-title">Viewing Invoice</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="invoice_view_iframe">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection

@push( 'scripts' )
    <script>
		jQuery(document).ready(function(){
			jQuery(document).on('click', '.invoice_view', function(e){
				e.preventDefault();

				var $self = jQuery(this);

				jQuery.ajax({
					url: "/ajax/invoice/view",
					method: 'get',
					data: {invoice_id: $self.data('invoice_id')},
					success: function($result){
						$("#invoice_view_iframe").html('');
						$("#invoice_view_iframe").append($result);

						$("#viewInvoiceModal").modal("toggle");
					}
				});
			});
		});
    </script>
@endpush
