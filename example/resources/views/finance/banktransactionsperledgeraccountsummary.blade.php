@extends('layouts.backend')
@include('scripts.datatables')
@include('scripts.datepicker')
@include('scripts.forms')
@include('scripts.dialogs' )
@include('scripts.select2')


@section('content')
	<!-- Hero -->
	<div class="bg-body-light">
		<div class="content content-full">
			<div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
				<h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Bank transactions per ledger account summary</h1>
				<nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/finance">Finance</a></li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
	<!-- END Hero -->
	<!-- Page Content -->
	<div class="content">
		<div class="col-md-6">
			<div class="block block-rounded block-bordered">
				@if($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach($errors->all() as $error)
								{{$error}}<br>
							@endforeach
						</ul>
					</div>
				@endif
				<div class="block-header block-header-default">
					<h3 class="block-title">
						Filters
					</h3>
				</div>
				<div class="block-content block-content-full">

                    <form class="mb-5" method="post" action="{{action('BankTransactionsPerLedgerAccountSummaryController@filteredIndex')}}">
                        @csrf
                        <input name="_method" type="hidden" value="post">

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label" for="type">Date:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control drp drp-default" name="date"
                                       @if($selected_period != null) value="{{$selected_period}}"
                                       @else value="{{date("Y/m/d",  strtotime("-30 days"))}} - {{date("Y/m/d")}}" @endif autocomplete="off"/>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label" for="type">Statement number:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" name="statement_number" value="{{$selected_statement}}" autocomplete="off"/>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label">Ledger account type:</label>
                            <div class="col-sm-7">
                                <select type="text" class="js-select2 form-control" name="ledger">
                                    <option></option>
                                    @foreach($ledgeraccounts as $id => $ledger)
                                        <option @if($id == $selected_ledger) selected @endif value="{{$id}}">{{$ledger}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="form-group col-md-8">
                                <button type="submit" class="btn btn-primary">Filter</button>
                            </div>
                        </div>

                    </form>
				</div>
			</div>
		</div>
		@if ($selected_period != null)
			<div class="col-md-12">
				<div class="block block-rounded block-bordered">
					<div class="block-content block-content-full">
						<table data-order='[[0, "asc"]]'
						       class="table table-bordered table-striped table-vcenter js-dataTable-full">
							<thead>
                            <tr>
                                <th>Banklines</th>
                                <th>Ledger account</th>
                                <th>Credit</th>
                                <th>Debit</th>
                                <th>Total</th>
                            </tr>
							</thead>
							<tbody>
							@foreach ($ledger_amounts as $id => $ledger)
								<tr role="row">
                                    <td><button type="button" class="details_load btn btn-sm btn-primary js-tooltip-enabled" data-id="{{$ledger['leac_number']}}&date={{$selected_period}}" data-file="{{url('ajax/banklines')}}"><i class="fa fa-plus"></i></button></td>
                                    <td>{{$ledger['leac_number']. " (".$ledger['leac_name'].")"}}</td>
                                    <td>€ {{number_format($ledger['credit'], 2, ",", ".")}}</td>
                                    <td>€ {{number_format($ledger['debit'], 2, ",", ".")}}</td>
                                    <td>€ {{number_format($ledger['total'], 2, ",", ".")}}</td>
								</tr>
							@endforeach
							</tbody>
                            <tfoot>
                            <td colspan="2"></td>
                            <td>€{{number_format($totals['credit'], 2, ",", ".")}}</td>
                            <td>€{{number_format($totals['debit'], 2, ",", ".")}}</td>
                            <td>€{{number_format($totals['total'], 2, ",", ".")}}</td>
                            </tfoot>
						</table>
					</div>
				</div>
			</div>
		@endif
	</div>
	<!-- END Page Content -->
@endsection
