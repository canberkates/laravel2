@extends('layouts.backend')
@include('scripts.datatables')
@include('scripts.datepicker')
@include('scripts.forms')
@include('scripts.dialogs' )
@include('scripts.select2')


@section('content')
	<!-- Hero -->
	<div class="bg-body-light">
		<div class="content content-full">
			<div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
				<h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Credit / Debit invoice lines</h1>
				<nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/finance">Finance</a></li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
	<!-- END Hero -->

	<!-- Page Content -->
	<div class="content">
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        {{$error}}<br>
                    @endforeach
                </ul>
            </div>
        @endif

		<div class="col-md-6">
			<div class="block block-rounded block-bordered">

				<div class="block-header block-header-default">
					<h3 class="block-title">
						Filters <a href="{{url("finance/addcreditdebitinvoiceline")}}"><button type="button" class="btn btn-primary">Add credit/debit invoice line</button></a>
					</h3>
				</div>
				<div class="block-content block-content-full">

					<form class="mb-5" method="post" action="{{action('CreditDebitInvoiceLinesController@filteredIndex')}}">
						@csrf
						<input name="_method" type="hidden" value="post">

						<div class="form-group row">
							<div class="col-sm-1"></div>
							<label class="col-sm-3 col-form-label" for="type">Date:</label>
							<div class="col-sm-6">
								<input type="text" class="form-control drp drp-default" name="date"
								       @if($selected_date != null) value="{{$selected_date}}"
								       @else value="{{date("Y/m/d",  strtotime("-30 days"))}} - {{date("Y/m/d")}}" @endif autocomplete="off"/>
							</div>
						</div>

						<div class="form-group row">
							<div class="col-sm-1"></div>
							<label class="col-sm-3 col-form-label">Customer:</label>
							<div class="col-sm-6">
								<select type="text" class="js-select2 form-control" name="mover">
									<option></option>
									@foreach($customers as $customer)
										<option value="{{$customer->cu_id}}">{{$customer->cu_company_name_legal}}</option>
									@endforeach
								</select>
							</div>
						</div>

						<div class="row">
							<div class="col-md-1"></div>
							<div class="form-group col-md-8">
								<button type="submit" class="btn btn-primary">Filter</button>
							</div>
						</div>

					</form>
				</div>
			</div>
		</div>
		@if ($selected_date != null)
			<div class="col-md-12">
				<div class="block block-rounded block-bordered">
					<div class="block-content block-content-full">
						<table data-order='[[0, "asc"]]'
						       class="table table-bordered table-striped table-vcenter js-dataTable-full">
							<thead>
							<tr>
								<th>ID</th>
								<th>Timestamp</th>
								<th>Customer</th>
								<th>Ledger account</th>
								<th>Description</th>
								<th>Type</th>
								<th>Amount</th>
								<th>Invoiced</th>
								<th>Delete</th>
							</tr>
							</thead>
							<tbody>
							@foreach ($credit_debit_lines as $lines)
								<tr>
									<td>{{$lines->crdeinli_id}}</td>
									<td>{{$lines->crdeinli_timestamp}}</td>
									<td>{{$lines->cu_company_name_legal}}</td>
									<td>{{$lines->crdeinli_leac_number}} ({{$lines->leac_name}})</td>
									<td>{!! $lines->crdeinli_description !!}</td>
									<td>{{$credit_debit_types[$lines->crdeinli_type]}}</td>
									<td>{{$currency_tokens[$lines->crdeinli_currency]}} {{$lines->crdeinli_amount}}</td>
									<td>{{$lines->crdeinli_invoiced ? "Yes" : "No"}}</td>
                                    @if($lines->crdeinli_invoiced == 0 && $lines->crdeinli_ktrecupo_id == 0 && $lines->crdeinli_ktrecuqu_id == 0)
                                        <td class="text-center">
                                            <button type="button" class="btn btn-sm btn-primary credit_debit_line_delete" data-toggle="tooltip"
                                                    title="Delete" data-crdeinli_id="{{$lines->crdeinli_id}}">
                                                <i class="fa fa-times"></i>
                                            </button>
                                        </td>
                                    @else
                                     <td>-</td>
                                    @endif
                                </tr>
							@endforeach
							</tbody>
						</table>

					</div>
				</div>
			</div>
		@endif
	</div>
	<!-- END Page Content -->
@endsection

@push( 'scripts' )
<script>
    jQuery(document).on('click', '.credit_debit_line_delete', function (e) {
        e.preventDefault();

        var $self = jQuery(this);
        confirmDelete("{{ url('ajax/customer/credit_debit_line_delete') }}", 'get', {id: $self.data('crdeinli_id')}, function () {
            $self.parents('.dataTable').DataTable().row($self.parents('tr')).remove().draw('page');
        });
    });
</script>
@endpush

