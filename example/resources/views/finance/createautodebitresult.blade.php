@extends('layouts.backend')
@include('scripts.datatables')
@include('scripts.datepicker')
@include('scripts.forms')
@include('scripts.dialogs' )
@include('scripts.select2')


@section('content')
	<!-- Hero -->
	<div class="bg-body-light">
		<div class="content content-full">
			<div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
				<h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Create auto debit file</h1>
				<nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/finance">Finance</a></li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
	<!-- END Hero -->
	<!-- Page Content -->
	<div class="content">
		<div class="col-md-12">
			<div class="block block-rounded block-bordered">
				@if($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach($errors->all() as $error)
								{{$error}}<br>
							@endforeach
						</ul>
					</div>
				@endif
				<div class="block-header block-header-default">
					<h3 class="block-title">
						Auto debit lines
					</h3>
				</div>
				<div class="block-content block-content-full">

                    <form class="mb-12" method="post" action="{{action('CreateAutoDebitFileController@createFile')}}">
                        @csrf
                        <input name="_method" type="hidden" value="post">
                        <input type="hidden" name="type" value="{{$type}}">
                        <input type="hidden" name="year" value="{{$year}}">
                        <input type="hidden" name="month" value="{{$month}}">

                        <p>
                            <button type="button" class="btn btn-primary select_all">Select all</button>
                            <button type="button" class="btn btn-primary deselect_all">Deselect all</button>
                        </p>
                        <table data-order='[[0, "asc"]]'
                               class="table table-bordered table-striped table-vcenter js-dataTable-full">
                            <thead>
                            <tr>
                                <th>Collect</th>
                                <th>Name</th>
                                <th>City</th>
                                <th>Country</th>
                                <th>IBAN</th>
                                <th>BIC</th>
                                <th>Payment reference</th>
                                <th>Amount FC</th>
                                <th>Amount €</th>
                                <th>Amount paid</th>
                                <th>Amount left</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($lines as $line)
                                <tr>
                                    <td><input name="invoices[{{$line['in_id']}}]" type="checkbox"></td>
                                    <td>{{$line['cu_auto_debit_name']}}</td>
                                    <td>{{$line['cu_auto_debit_city']}}</td>
                                    @if($line['cu_auto_debit_co_code'])
                                    <td>{{$countries[$line['cu_auto_debit_co_code']]}}</td>
                                    @else
                                    <td></td>
                                    @endif
                                    <td>{{$line['cu_auto_debit_iban']}}</td>
                                    <td>{{$line['cu_auto_debit_bic']}}</td>
                                    <td>{{$line['in_number']}}</td>
                                    <td>{{$currencies[$line['in_currency']]}} {{$line['in_amount_netto']}}</td>
                                    <td>€ {{number_format($line['in_amount_netto_eur'], 2, ",", ".")}}</td>
                                    <td>€ {{number_format($line['amount_paired'], 2, ",", ".")}}</td>
                                    <td>€ {{number_format($line['amount_left'], 2, ",", ".")}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <div class="row">
                            <div class="form-group col-md-8">
                                <button type="submit" class="btn btn-primary">Create file</button>
                            </div>
                        </div>

                    </form>
				</div>
			</div>
		</div>
	</div>
	<!-- END Page Content -->
@endsection

@push('scripts')
    <script>
        jQuery(document).ready(function () {

            $("form .select_all").click(function () {
                $(this).parent().parent().find("input[type=checkbox]:enabled").prop("checked", true);

                $("input[type=checkbox]").trigger("change");

                return false;
            });

            $("form .deselect_all").click(function () {
                $(this).parent().parent().find("input[type=checkbox]:enabled").prop("checked", false);

                $("input[type=checkbox]").trigger("change");

                return false;
            });
        });
    </script>
@endpush
