@extends('layouts.backend')
@include('scripts.datatables')
@include('scripts.datepicker')
@include('scripts.forms')
@include('scripts.dialogs' )
@include('scripts.select2')


@section('content')
	<!-- Hero -->
	<div class="bg-body-light">
		<div class="content content-full">
			<div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
				<h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Bank lines successfully imported!</h1>
				<nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/finance">Finance</a></li>
					</ol>
				</nav>
			</div>
		</div>
	</div>

	<!-- END Hero -->
	<!-- Page Content -->
	<div class="content">
		<div class="col-md-12">
            <p class="mb-0">The uploaded file has been successfully imported and {{$amount}} bank lines have been added.</p>
            <p class="mb-0">Credit: €{{$creditsum}}, Debit: €{{$debitsum}}, Sum: €{{$sumsum}}</p>
            <a href="/finance/pairbanklines"><button type="button" class="btn btn-primary">Pair bank lines</button></a>
		</div>
	</div>
	<!-- END Page Content -->
@endsection

