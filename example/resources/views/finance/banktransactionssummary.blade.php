@extends('layouts.backend')
@include('scripts.datatables')
@include('scripts.datepicker')
@include('scripts.forms')
@include('scripts.dialogs' )
@include('scripts.chosen')

@section('content')
	<!-- Hero -->
	<div class="bg-body-light">
		<div class="content content-full">
			<div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
				<h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Bank transactions summary</h1>
				<nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/finance">Finance</a></li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
	<!-- END Hero -->
	<!-- Page Content -->
	<div class="content">
		<div class="col-md-6">
			<div class="block block-rounded block-bordered">
				@if($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach($errors->all() as $error)
								{{$error}}<br>
							@endforeach
						</ul>
					</div>
				@endif
				<div class="block-header block-header-default">
					<h3 class="block-title">
						Filters
					</h3>
				</div>
				<div class="block-content block-content-full">

                    <form class="mb-5" method="post" action="{{action('BankTransactionsSummaryController@filteredIndex')}}">
                        @csrf
                        <input name="_method" type="hidden" value="post">

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label" for="type">Date:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control drp drp-default" name="date"
                                       @if($selected_period != null) value="{{$selected_period}}"
                                       @else value="{{date("Y/m/d",  strtotime("-30 days"))}} - {{date("Y/m/d")}}" @endif autocomplete="off"/>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label" for="type">Statement number:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" name="statement_number" value="{{$selected_statement}}" autocomplete="off"/>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label">Mutation type:</label>
                            <div class="col-sm-7">
                                <select class="chosen-select form-control" name="mutation">
                                    <option></option>
                                    @foreach($mutationtypes as $id => $type)
                                        <option @if($id == $selected_mutation) selected @endif value="{{$id}}">{{$type}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label">Customer:</label>
                            <div class="col-sm-7">
                                 <select class="chosen-select form-control" name="customer">
                                    <option></option>
                                    @foreach($customers as $customer)
                                        <option @if($selected_customer == $customer->cu_id) selected @endif value="{{$customer->cu_id}}">{{$customer->cu_company_name_legal}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label">Ledger account:</label>
                            <div class="col-sm-7">
                                <select type="text" class="chosen-select form-control" name="ledger">
                                    <option></option>
                                    @foreach($ledgeraccounts as $id => $ledger)
                                        <option @if($id == $selected_ledger) selected @endif value="{{$id}}">{{$ledger}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label"
                                   for="not_paired">Not paired with invoice:</label>
                            <div class="col-sm-7">
                                <div
                                    class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                    <input type="checkbox" class="custom-control-input" id="not_paired"
                                           name="not_paired"  @if($selected_pair) checked @endif >
                                    <label class="custom-control-label" for="not_paired"></label>
                                </div>
                            </div>
                        </div>



                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="form-group col-md-8">
                                <button type="submit" class="btn btn-primary">Filter</button>
                            </div>
                        </div>

                    </form>
				</div>
			</div>
		</div>
		@if ($selected_period != null)
			<div class="col-md-12">
				<div class="block block-rounded block-bordered">
					<div class="block-content block-content-full">
						<table data-order='[[0, "asc"]]'
						       class="table table-bordered table-striped table-vcenter js-dataTable-full">
							<thead>
                            <tr>
                                <th>ID</th>
                                <th>Statement number</th>
                                <th>Mutation type</th>
                                <th>Invoice number</th>
                                <th>Customer</th>
                                <th>Ledger account</th>
                                <th>Date</th>
                                <th>Reference</th>
                                <th>Credit / Debit</th>
                                <th>Debtor details</th>
                                <th>Amount</th>
                                <th>Amount FC</th>
                                <th>Information</th>
                                <th>Pair</th>
                            </tr>
							</thead>
							<tbody>
							@foreach ($invoices as $invoice)
								<tr role="row">
									<td>{{$invoice->ktbaliinculeac_id}}</td>
									<td>{{$invoice->bali_statement_number}}</td>
									<td>{{$mutationtypes[$invoice->ktbaliinculeac_cu_id != 0 ? 1 : 2]}}</td>
									<td>{{!empty($invoice->in_id) ? $invoice->in_number : "-"}}</td>
									<td>{{!empty($invoice->cu_id) ? $invoice->cu_company_name_legal : "-"}}</td>
                                    <td>{{$ledgeraccounts[$invoice->ktbaliinculeac_leac_number]}}</td>
                                    <td>{{$invoice->bali_date}}</td>
                                    <td>{{$invoice->bali_reference}}</td>
                                    <td>{{ucfirst($invoice->bali_credit_debit)}}</td>
                                    <td>{!! nl2br($invoice->bali_debtor_details) !!}</td>
                                    <td>€ {{number_format($invoice->ktbaliinculeac_amount, 2, ",", ".")}}</td>
                                    @if(empty($invoice->ktbaliinculeac_amount_fc))
                                        <td>€{{number_format($invoice->ktbaliinculeac_amount, 2, ",", ".")}}</td>
                                    @else
                                        <td>{{$currencies[$invoice->ktbaliinculeac_currency]}} {{number_format($invoice->ktbaliinculeac_amount_fc, 2, ",", ".")}}</td>
                                    @endif
                                    <td>{!! nl2br($invoice->bali_information) !!}</td>
                                    <td><a href="{{url('/finance/'.$invoice->bali_id.'/pair')}}"><i class="fa fa-2x fa-euro-sign"></i></a></td>
								</tr>
							@endforeach
							</tbody>
                            <tfoot>
                            <td colspan="10"></td>
                            <td>€{{number_format($sum, 2, ",", ".")}}</td>
                            <td colspan="3"></td>
                            </tfoot>
						</table>
					</div>
				</div>
			</div>
		@endif
	</div>
	<!-- END Page Content -->
@endsection
