@extends('layouts.backend')
@include('scripts.datatables')
@include('scripts.datepicker')
@include('scripts.forms')
@include('scripts.dialogs' )
@include('scripts.select2')

@section('content')
	<!-- Hero -->
	<div class="bg-body-light">
		<div class="content content-full">
			<div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
				<h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Credit card payment history</h1>
				<nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/finance">Finance</a></li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
	<!-- END Hero -->

	<!-- Page Content -->
	<div class="content">
		<div class="col-md-6">
			<div class="block block-rounded block-bordered">
				@if($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach($errors->all() as $error)
								{{$error}}<br>
							@endforeach
						</ul>
					</div>
				@endif
				<div class="block-header block-header-default">
					<h3 class="block-title">
						Filters
					</h3>
				</div>
				<div class="block-content block-content-full">

                    <form class="mb-5" method="post" action="{{action('CreditCardPaymentHistoryController@filteredIndex')}}">
						@csrf
						<input name="_method" type="hidden" value="post">

						<div class="form-group row">
							<div class="col-sm-1"></div>
							<label class="col-sm-4 col-form-label" for="type">Date:</label>
							<div class="col-sm-7">
								<input type="text" class="form-control drp drp-default" name="date"
								       @if($selected_date != null) value="{{$selected_date}}"
								       @else value="{{date("Y/m/d",  strtotime("-30 days"))}} - {{date("Y/m/d")}}" @endif autocomplete="off"/>
							</div>
						</div>

                        <div class="form-group row">
                            <div class="col-md-1"></div>
                            <label class="col-md-4 col-form-label" for="for-status">Payment status:</label>
                            <div class="col-md-7">
                                <select class="js-select2 form-control" multiple data-placeholder="Select some options..."
                                        name="status[]" id="for-status" style="width:100%;">
                                    <option></option>
                                    @foreach($adyenpaymentstatus as $id => $status)
                                        <option value="{{$id}}" @if (in_array($id, $selected_statuses)) selected @endif >{{$status}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-4 col-form-label" for="type">Find for payout batch number:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" name="batch_number"
                                       @if($selected_batch_number != null) value="{{$selected_batch_number}}" @endif autocomplete="off"/>
                            </div>
                        </div>

						<div class="row">
							<div class="col-md-1"></div>
							<div class="form-group col-md-8">
								<button type="submit" class="btn btn-primary">Filter</button>
							</div>
						</div>

					</form>
				</div>
			</div>
		</div>
		@if ($selected_date != null)
			<div class="col-md-12">
				<div class="block block-rounded block-bordered">
					<div class="block-content block-content-full">
						<table class="table table-bordered table-striped table-vcenter js-dataTable-full">
							<thead>
							<tr>
								<th>ID</th>
								<th>Customer</th>
                                <th>Timestamp</th>
								<th>Merchant Reference</th>
								<th>Result</th>
								<th>Amount</th>
								<th>Status</th>
								<th>Card</th>
								<th>Card status</th>
								<th>Details</th>
							</tr>
							</thead>
							<tbody>
							@foreach ($payments as $payment)
								<tr style="{{$payment->style}}">
									<td>{{$payment->adpa_id}}</td>
									<td>{{$payment->cu_company_name_business}}</td>
									<td>{{$payment->adpa_timestamp}}</td>
									<td>{{$payment->adpa_merchant_reference}}</td>
									<td>{{$payment->adpa_result_code}} @if($payment->adpa_refusal_reason) {{" - ".$payment->adpa_refusal_reason}} @endif</td>
                                    <td>{{$currencies[$payment->adpa_pacu_code]}} {{number_format($payment->adpa_amount, 2, ",", ".")}}</td>
                                    <td>{{$adyenpaymentstatus[$payment->adpa_status]}}</td>
                                    <td>Digits: {{$payment->adcade_card_last_digits}} <br />Expires: {{$payment->adcade_card_expiry_year}}/{{$payment->adcade_card_expiry_month}} </td>
                                    <td>{{$payment->adcade_removed ? "Removed" : ($payment->adcade_enabled ? "Active" : "Inactive")}}</td>
                                    @if($payment->details)
                                        <td><button type="button" class="btn btn-sm btn-primary details_show" data-details="{{$payment->details}}."><i class="fa fa-plus"></i></button></td>
                                    @else
                                        <td></td>
                                    @endif
                                </tr>
							@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		@endif
	</div>
	<!-- END Page Content -->
@endsection


