@extends('layouts.backend')
@include('scripts.datatables')
@include('scripts.datepicker')
@include('scripts.forms')
@include('scripts.dialogs' )
@include('scripts.chosen')


@section('content')
	<!-- Hero -->
	<div class="bg-body-light">
		<div class="content content-full">
			<div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
				<h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Journal entry summary</h1>
				<nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/finance">Finance</a></li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
	<!-- END Hero -->
	<!-- Page Content -->
	<div class="content">
		<div class="col-md-6">
			<div class="block block-rounded block-bordered">
				@if($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach($errors->all() as $error)
								{{$error}}<br>
							@endforeach
						</ul>
					</div>
				@endif
				<div class="block-header block-header-default">
					<h3 class="block-title">
						Filters
					</h3>
				</div>
				<div class="block-content block-content-full">

                    <form class="mb-5" method="post" action="{{action('JournalEntrySummaryController@filteredIndex')}}">
                        @csrf
                        <input name="_method" type="hidden" value="post">

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label" for="type">Date:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control drp drp-default" name="date"
                                       @if($selected_period != null) value="{{$selected_period}}"
                                       @else value="{{date("Y/m/d",  strtotime("-30 days"))}} - {{date("Y/m/d")}}" @endif autocomplete="off"/>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label">Customer:</label>
                            <div class="col-sm-7">
                                <select class="chosen-select form-control" name="customer">
                                    <option></option>
                                    @foreach($customers as $customer)
                                        <option @if($selected_customer == $customer->cu_id) selected @endif value="{{$customer->cu_id}}">{{$customer->cu_company_name_legal}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label">Ledger account:</label>
                            <div class="col-sm-7">
                                <select class="chosen-select form-control" name="ledger">
                                    <option></option>
                                    @foreach($ledgeraccounts as $id => $ledger)
                                        <option @if($id == $selected_ledger) selected @endif value="{{$id}}">{{$ledger}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="form-group col-md-8">
                                <button type="submit" class="btn btn-primary">Filter</button>
                            </div>
                        </div>

                    </form>
				</div>
			</div>
		</div>
		@if ($selected_period != null)
			<div class="col-md-12">
				<div class="block block-rounded block-bordered">
					<div class="block-content block-content-full">
						<table data-order='[[0, "asc"]]'
						       class="table table-bordered table-striped table-vcenter js-dataTable-full">
							<thead>
                            <tr>
                                <th>ID</th>
                                <th>Bankline ID</th>
                                <th>Bankline date</th>
                                <th>Journal date</th>
                                <th>Invoice number</th>
                                <th>Customer</th>
                                <th>Ledger account</th>
                                <th>Amount</th>
                            </tr>
							</thead>
							<tbody>
							@foreach ($banklines as $bankline)
								<tr role="row">
									<td>{{$bankline->ktbaliinculeac_id}}</td>
									<td>{{$bankline->bali_id}}</td>
									<td>{{$bankline->bali_date}}</td>
									<td>{{$bankline->ktbaliinculeac_date}}</td>
									<td>{{((!empty($bankline->in_id)) ? $bankline->in_number : "-")}}</td>
									<td>{{((!empty($bankline->cu_id)) ? $bankline->cu_company_name_legal : "-")}}</td>
									<td>{{$ledgeraccounts[$bankline->ktbaliinculeac_leac_number]}}</td>
                                    <td>€ {{number_format($bankline->ktbaliinculeac_amount, 2, ",", ".")}}</td>
								</tr>
							@endforeach
							</tbody>
                            <tfoot>
                            <td colspan="7"></td>
                            <td>€{{number_format($total, 2, ",", ".")}}</td>
                            </tfoot>
						</table>
					</div>
				</div>
			</div>
		@endif
	</div>
	<!-- END Page Content -->
@endsection
