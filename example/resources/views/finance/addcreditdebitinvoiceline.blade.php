@extends('layouts.backend')
@include('scripts.datatables')
@include('scripts.datepicker')
@include('scripts.forms')
@include('scripts.dialogs' )
@include('scripts.select2')

@section('content')
	<!-- Hero -->
	<div class="bg-body-light">
		<div class="content content-full">
			<div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
				<h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Add credit / debit invoice line</h1>
				<nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/finance">Finance</a></li>
						<li class="breadcrumb-item"><a href="/finance/creditdebitinvoicelines">Credit / Debit invoice lines</a></li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
	<!-- END Hero -->
	<!-- Page Content -->
	<div class="content">
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        {{$error}}<br>
                    @endforeach
                </ul>
            </div>
        @endif

		<div class="col-md-6">
			<div class="block block-rounded block-bordered">

				<div class="block-header block-header-default">
					<h3 class="block-title">
                        Filters
					</h3>
				</div>
				<div class="block-content block-content-full">

                    <form class="mb-5" method="post" action="{{action('CreditDebitInvoiceLinesController@addLine')}}">
                        @csrf
                        <input name="_method" type="hidden" value="post">

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label">Customer:</label>
                            <div class="col-sm-7">
                                <select type="text" class="js-select2 form-control" name="customer">
                                    <option></option>
                                    @foreach($customers as $customer)
                                        <option value="{{$customer->cu_id}}">{{$customer->cu_company_name_business}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label">Ledger account:</label>
                            <div class="col-sm-7">
                                <select type="text" class="form-control" name="ledger">
                                    <option></option>
                                    @foreach($ledgeraccounts as $id => $ledger)
                                        <option value="{{$id}}">{{$ledger}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label">Description:</label>
                            <div class="col-sm-7">
                               <input  class="form-control" type="text" name="description">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label">Type:</label>
                            <div class="col-sm-7">
                                <select type="text" class="form-control" name="type">
                                    @foreach($credit_debit_types as $id => $type)
                                        <option value="{{$id}}">{{$type}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label">Currency:</label>
                            <div class="col-sm-7">
                                <select type="text" class="form-control" name="currency">
                                    <option></option>
                                    @foreach($currencies as $id => $type)
                                        <option value="{{$id}}">{{$type}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label">Amount:</label>
                            <div class="col-sm-7">
                                <input  class="form-control" type="text" name="amount">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="form-group col-md-8">
                                <button type="submit" class="btn btn-primary">Add</button>
                            </div>
                        </div>

                    </form>
				</div>
			</div>
		</div>
	</div>
	<!-- END Page Content -->
@endsection

@push('scripts')
    <script>
        $(function(){
            $("select[name=customer]").change(function(){
                if($("select[name=customer]").val() != ""){

                    $("select[name=ledger]").attr("disabled", "disabled");
                    $("select[name=currency]").attr("disabled", "disabled");
                    $("input[name=amount]").attr("disabled", "disabled");

                    $.ajax({
                        type: "GET",
                        url: '{{ url('/ajax/getCustomerFields') }}',
                        data: {"customer": $("select[name=customer]").val()},
                        success: function(data){
                            data = JSON.parse(data);

                            $("select[name=ledger]").val(data.cu_leac_number).removeAttr("disabled");
                            $("select[name=currency]").val(data.cu_pacu_code).removeAttr("disabled");
                            $("input[name=amount]").val("").removeAttr("disabled");
                        }
                    });

                }
                else{
                    $("select[name=ledger], select[name=currency], input[name=amount]").val("");
                }
            });
        });
    </script>
@endpush
