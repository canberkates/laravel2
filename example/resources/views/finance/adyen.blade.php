@extends('layouts.backend')
@include('scripts.datatables')
@include('scripts.datepicker')
@include('scripts.forms')
@include('scripts.dialogs' )
@include('scripts.select2')

@section('content')
	<!-- Hero -->
	<div class="bg-body-light">
		<div class="content content-full">
			<div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
				<h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Adyen</h1>
				<nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/finance">Finance</a></li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
	<!-- END Hero -->

	<!-- Page Content -->
	<div class="content">
		<div class="col-md-6">
			<div class="block block-rounded block-bordered">
				@if($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach($errors->all() as $error)
								{{$error}}<br>
							@endforeach
						</ul>
					</div>
				@endif
				<div class="block-header block-header-default">
					<h3 class="block-title">
						Filters
					</h3>
				</div>
				<div class="block-content block-content-full">

					<form class="mb-5" method="post" action="{{action('AdyenController@filteredIndex')}}">
						@csrf
						<input name="_method" type="hidden" value="post">

						<div class="form-group row">
							<div class="col-sm-1"></div>
							<label class="col-sm-3 col-form-label" for="type">Date:</label>
							<div class="col-sm-6">
								<input type="text" class="form-control drp drp-default" name="date"
								       @if($selected_date != null) value="{{$selected_date}}"
								       @else value="{{date("Y/m/d",  strtotime("-30 days"))}} - {{date("Y/m/d")}}" @endif autocomplete="off"/>
							</div>
						</div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label">Customer:</label>
                            <div class="col-sm-6">
                                <select type="text" class="js-select2 form-control" name="customer">
                                    <option></option>
                                    @foreach($customers as $customer)
                                        <option @if($selected_customer == $customer->cu_id) selected @endif value="{{$customer->cu_id}}">{{$customer->cu_company_name_business}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label"
                                   for="exclude_ccs">Exclude Credit Card payments</label>
                            <div class="col-sm-7">
                                <div
                                    class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                    <input type="checkbox" class="custom-control-input" id="exclude_ccs"
                                           name="exclude_ccs" {{($selected_exclude ? "checked" : "")}}>
                                    <label class="custom-control-label" for="exclude_ccs">Exclude CC-charged adyen payments</label>
                                </div>
                            </div>
                        </div>

						<div class="row">
							<div class="col-md-1"></div>
							<div class="form-group col-md-8">
								<button type="submit" class="btn btn-primary">Filter</button>
							</div>
						</div>

					</form>
				</div>
			</div>
		</div>
		@if ($selected_date != null)
			<div class="col-md-12">
				<div class="block block-rounded block-bordered">
					<div class="block-content block-content-full">
						<table class="table table-bordered table-striped table-vcenter js-dataTable-full">
							<thead>
							<tr>
								<th>Data</th>
								<th>ID</th>
								<th>Timestamp</th>
								<th>Customer</th>
								<th>PSP Reference</th>
								<th>Merchant Reference</th>
								<th>Event code</th>
								<th>Payment method</th>
								<th>Amount €</th>
								<th>Amount FC</th>
								<th>Success</th>
							</tr>
							</thead>
							<tbody>
							@foreach ($invoices as $invoice)
								<tr>
                                    <td><button type="button" class="btn btn-sm btn-primary details_show" data-details="{{$invoice->details}}."><i class="fa fa-plus"></i></button></td>
									<td>{{$invoice->ad_id}}</td>
									<td>{{$invoice->ad_timestamp}}</td>
									<td>{{$invoice->cu_company_name_legal}}</td>
									<td>{{$invoice->ad_psp_reference}}</td>
									<td>{{$invoice->ad_merchant_reference}}</td>
									<td>{{$invoice->ad_event_code}}</td>
									<td>{{$invoice->ad_payment_method}}</td>
                                    <td>€ {{number_format($invoice->euro, 2, ",", ".")}}</td>
                                    <td>{{$currencies[$invoice->ad_currency]}} {{number_format($invoice->ad_amount, 2, ",", ".")}}</td>
                                    @if($invoice->ad_success)
                                        <td><i class="fa fa-2x fa-check" style="color:green;"></i></td>
                                    @else
                                        <td><i class="fa fa-2x fa-times-circle" style="color:red;"></i></td>
                                    @endif
								</tr>
							@endforeach
							<tfoot>
							<th colspan="8"></th>
							<th>€ {{number_format($total, 2, ",", ".")}}</th>
                            <th colspan="2"></th>
							</tfoot>
							</tbody>
						</table>

					</div>
				</div>
			</div>
		@endif
	</div>
	<!-- END Page Content -->
@endsection


