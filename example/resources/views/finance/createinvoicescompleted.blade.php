@extends('layouts.backend')
@include('scripts.datatables')
@include('scripts.datepicker')
@include('scripts.forms')
@include('scripts.dialogs' )
@include('scripts.select2')


@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Invoices successfully created!</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/finance">Finance</a></li>
                        <li class="breadcrumb-item"><a href="/finance/createinvoices">Create Invoices</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->
    <!-- Page Content -->
    <div class="content">
        <div class="col-md-12">
            <div class="block block-rounded block-bordered">
                <div class="block-content block-content-full">
                    Invoices succesfully created and @if(!$email) <strong>NOT</strong> @endif sent to:
                    @foreach($customers as $id => $customer)
                        <li>{{$customer['cu_company_name_business']}} ({{$id}})</li>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection

