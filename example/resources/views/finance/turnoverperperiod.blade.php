@extends('layouts.backend')
@include('scripts.datatables')
@include('scripts.datepicker')
@include('scripts.forms')
@include('scripts.dialogs' )

@section('content')
	<!-- Hero -->
	<div class="bg-body-light">
		<div class="content content-full">
			<div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
				<h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Turnover per period</h1>
				<nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/finance">Finance</a></li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
	<!-- END Hero -->

	<!-- Page Content -->
	<div class="content">
		<div class="col-md-6">
			<div class="block block-rounded block-bordered">
				@if($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach($errors->all() as $error)
								{{$error}}<br>
							@endforeach
						</ul>
					</div>
				@endif
				<div class="block-header block-header-default">
					<h3 class="block-title">
						Filters
					</h3>
				</div>
				<div class="block-content block-content-full">

					<form class="mb-5" method="post" action="{{action('TurnoverPerPeriodController@filteredIndex')}}">
						@csrf
						<input name="_method" type="hidden" value="post">

						<div class="form-group row">
							<div class="col-sm-1"></div>
							<label class="col-sm-3 col-form-label" for="type">Date:</label>
							<div class="col-sm-6">
								<input type="text" class="form-control drp drp-default" name="date"
								       @if($selected_date != null) value="{{$selected_date}}"
								       @else value="{{date("Y/m/d",  strtotime("-30 days"))}} - {{date("Y/m/d")}}" @endif autocomplete="off"/>
							</div>
						</div>

						<div class="form-group row">
							<div class="col-sm-1"></div>
							<label class="col-sm-3 col-form-label" for="type">Dimension:</label>
							<div class="col-sm-6">
								<select type="text" class="form-control" name="dimension">
									@foreach($dimensions as $id => $value)
										<option value="{{$id}}"
										        @if($id == $selected_dimension) selected @endif >{{$value}}</option>
									@endforeach
								</select>
							</div>
						</div>

						<div class="row">
							<div class="col-md-1"></div>
							<div class="form-group col-md-8">
								<button type="submit" class="btn btn-primary">Filter</button>
							</div>
						</div>

					</form>
				</div>
			</div>
		</div>
		@if ($selected_dimension == 1)
			<div class="col-md-12">
				<div class="block block-rounded block-bordered">
					<div class="block-content block-content-full">
						<table data-order='[[0, "asc"]]'
						       class="table table-bordered table-striped table-vcenter js-dataTable-full">
							<thead>
							<tr>
								<th>Dimension</th>
								<th>Turnover</th>
								<th>No claim</th>
							</tr>
							</thead>
							<tbody>
							@foreach ($total_dimensions as $leac => $amount)
								<tr>
									<td>{{$leac}}</td>
									<td>€ {{number_format($amount, 2, ",", ".")}}</td>
									<td></td>
								</tr>
							@endforeach
							<tfoot>
							<th></th>
							<th>€ {{number_format($total_turnover, 2, ",", ".")}}</th>
							<th>€ {{number_format($no_claim, 2, ",", ".")}}</th>
							</tfoot>
							</tbody>
						</table>

					</div>
				</div>
			</div>
		@endif
		@if ($selected_dimension == 2)
			<div class="col-md-12">
				<div class="block block-rounded block-bordered">
					<div class="block-content block-content-full">
						<table data-order='[[0, "asc"]]'
						       class="table table-bordered table-striped table-vcenter js-dataTable-full">
							<thead>
							<tr>
								<th>Dimension</th>
								<th>Turnover</th>
								<th>No claim</th>
							</tr>
							</thead>
							<tbody>
							@foreach ($total_dimensions as $leac => $amount)
								<tr>
									<td>{{$leac}}</td>
									<td>€ {{number_format($amount, 2, ",", ".")}}</td>
									<td></td>
								</tr>
							@endforeach
							<tfoot>
							<th></th>
							<th>€ {{number_format($total_turnover, 2, ",", ".")}}</th>
							<th>€ {{number_format($no_claim, 2, ",", ".")}}</th>
							</tfoot>
							</tbody>
						</table>

					</div>
				</div>
			</div>
		@endif
		@if ($selected_dimension == 3)
			<div class="col-md-12">
				<div class="block block-rounded block-bordered">
					<div class="block-content block-content-full">
						<table data-order='[[0, "asc"]]'
						       class="table table-bordered table-striped table-vcenter js-dataTable-full">
							<thead>
							<tr>
								<th>Dimension</th>
								<th>Turnover</th>
								<th>No claim</th>
							</tr>
							</thead>
							<tbody>
							@foreach ($total_dimensions as $leac => $amount)
								<tr>
									<td>{{$leac}}</td>
									<td>€ {{number_format($amount, 2, ",", ".")}}</td>
									<td></td>
								</tr>
							@endforeach
							<tfoot>
							<th></th>
							<th>€ {{number_format($total_turnover, 2, ",", ".")}}</th>
							<th>€ {{number_format($no_claim, 2, ",", ".")}}</th>
							</tfoot>
							</tbody>
						</table>

					</div>
				</div>
			</div>
		@endif
	</div>
	<!-- END Page Content -->
@endsection


