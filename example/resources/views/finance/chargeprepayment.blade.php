@extends('layouts.backend')
@include('scripts.datatables')
@include('scripts.datepicker')
@include('scripts.forms')
@include('scripts.dialogs' )
@include('scripts.chosen')

@section('content')
	<!-- Hero -->
	<div class="bg-body-light">
		<div class="content content-full">
			<div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
				<h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Charge prepayment via Adyen</h1>
				<nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/finance">Finance</a></li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
	<!-- END Hero -->
	<!-- Page Content -->
	<div class="content">

        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        {{$error}}<br>
                    @endforeach
                </ul>
            </div>
        @endif
        @if(session()->has('message'))
            <div class="alert alert-info">
                {{ session()->get('message') }}
            </div>
        @endif

        @if($index)
	    	<div class="col-md-6">
			<div class="block block-rounded block-bordered">

				<div class="block-header block-header-default">
					<h3 class="block-title">
						Filters
					</h3>
				</div>
				<div class="block-content block-content-full">

                    <form method="post" action="{{action('ChargePrepaymentController@chargeConfirmation')}}">
                        @csrf
                        <input name="_method" type="hidden" value="post">

                        <h2 class="content-heading pt-0">Customer</h2>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label">Customer:</label>
                            <div class="col-sm-7">
                                <select class="chosen-select form-control" id="select_customer" name="customer">
                                    <option></option>
                                    @foreach($customers as $id => $customer)
                                        <option value="{{$id}}">{{$customer}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label">Credit card:</label>
                            <div class="col-sm-7">
                                <select class="chosen-select form-control" id="select_credit_card" name="creditcard">
                                    <option>Please select a customer</option>
                                </select>
                            </div>
                        </div>

                        <h2 class="content-heading pt-0">Amounts</h2>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label">Charge amount (<span id="amount-cur">€</span>):</label>
                            <div class="col-sm-7">
                               <input type="text" class="form-control" name="amount">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-8">
                                <button type="submit" class="btn btn-primary">Charge customer</button>
                            </div>
                        </div>

                    </form>
				</div>
			</div>
		</div>
        @else
            <div class="col-md-6">
                <div class="block block-rounded block-bordered">

                    <div class="block-header block-header-default">
                        <h3 class="block-title">
                            Confirmation
                        </h3>
                    </div>
                    <div class="block-content block-content-full">

                        <form method="post" action="{{action('ChargePrepaymentController@charge')}}">
                            @csrf
                            <input name="_method" type="hidden" value="post">

                            <input name="cu_id" type="hidden" value="{{$results['cu_id']}}">
                            <input name="adcade_id" type="hidden" value="{{$results['adcade_id']}}">
                            <input name="amount" type="hidden" value="{{$results['rate']}}">

                            <p>Are you sure you want to charge <strong>{{$results['amount']}}</strong> to <strong>{{$results['customer']}}</strong>'s creditcard: <strong>{{$results['lastdigits']}} (Expires: {{$results['expiryyear']}}/{{$results['expirymonth']}})</strong>?</p>

                            <div class="row">
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Charge customer</button>
                                    <a href="/finance/chargeprepayment"><button type="button" class="btn btn-primary">Back</button></a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        @endif


	</div>


	<!-- END Page Content -->
@endsection

@push('scripts')
    <script>
        $(function(){
            $("#select_customer").change(function(){
                var val = $(this).val();
                if (val == "") {
                    update_chosen_select("#select_credit_card", {"" : "Select a customer"}, "");
                } else {
                    update_chosen_select("#select_credit_card", {"" : "Loading..."}, "");

                    jQuery.ajax({
                        type: "GET",
                        url: '{{ url('/ajax/getadyenccdetails') }}',
                        data: {"customer" : val},
                        success: function(data) {
                            data = JSON.parse(data);
                            $("#amount-cur").text(data.pacu_token);
                            update_chosen_select("#select_credit_card", data.cards, "");
                        },
                        error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                            update_chosen_select("#select_credit_card", {"" : "No credit cards"}, "");
                        }
                    });
                }
            }).trigger("change");
        });

        function update_chosen_select(element, data, val){
            $(element).empty();

            $(element).append(add_options(data));

            if(typeof(val) == "undefined"){
                val = "";
            }

            $(element).val(val).trigger("change").trigger("chosen:updated");
        }

        function add_options(data){
            var html = "";
            if (data.constructor === Array) {
                $.each(data, function(index, keyValue){
                    $.each(keyValue, function(key, value){
                        if(value.constructor === Array){
                            html += "<optgroup label=\"" + key + "\">";
                            html += add_options(value);
                            html += "</optgroup>";
                        }
                        else {
                            html += "<option value=\"" + key + "\">" + value + "</option>";
                        }
                    });
                });
            }
            else{
                $.each(data, function(id, value){
                    if(typeof(value) === "object"){
                        html += "<optgroup label=\"" + id + "\">";
                        html += add_options(value);
                        html += "</optgroup>";
                    }
                    else{
                        html += "<option value=\"" + id + "\">" + value + "</option>";
                    }
                });
            }

            return html;
        }
    </script>
@endpush
