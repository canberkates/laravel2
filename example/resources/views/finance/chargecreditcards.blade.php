@extends('layouts.backend')
@include('scripts.datatables')
@include('scripts.datepicker')
@include('scripts.forms')
@include('scripts.dialogs' )
@include('scripts.select2')


@section('content')
    <style>
        span.number-currency {
            position: absolute;
            margin-top: 13px;
            margin-left: 5px;
            z-index: 1;
        }
        input.number-input.modified {
            border: 2px red solid;
        }
        input.number-input {
            width: 150px;
            padding-left: 35px;
            position: relative;
        }
    </style>
	<!-- Hero -->
	<div class="bg-body-light">
		<div class="content content-full">
			<div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
				<h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Charge creditcards</h1>
				<nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/finance">Finance</a></li>
					</ol>
				</nav>
			</div>
		</div>
	</div>


        <!-- END Hero -->
        <!-- Page Content -->
        <div class="content">
            <div class="col-md-8">
                <div class="block block-rounded block-bordered">
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    {{$error}}<br>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="block-header block-header-default">
                        <h3 class="block-title">
                            Filters
                        </h3>
                    </div>
                    <div class="block-content block-content-full">
                        <form class="mb-5" method="post" action="{{action('ChargeCreditCardsController@chargeConfirmation')}}">
                            @csrf
                            <input name="_method" type="hidden" value="post">

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="example-hf-email">Date:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control drp drp-default" name="date"
                                           @if($selected_date) value="{{$selected_date}}" @else value="{{date("Y/m/d",  strtotime("-90 days"))}} - {{date("Y/m/d")}}" @endif autocomplete="off"/>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label"
                                       for="exclude_outstanding">Exclude outstanding payments</label>
                                <div class="col-sm-7">
                                    <div
                                        class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                        <input type="checkbox" class="custom-control-input" id="exclude_outstanding"
                                               name="exclude_outstanding" @if($selected_outstanding) checked @endif >
                                        <label class="custom-control-label" for="exclude_outstanding"></label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label"
                                       for="exclude_virtually_paid">Exclude virtually paid</label>
                                <div class="col-sm-7">
                                    <div
                                        class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                        <input type="checkbox" class="custom-control-input" id="exclude_virtually_paid"
                                               name="exclude_virtually_paid" @if($selected_virtually) checked @endif >
                                        <label class="custom-control-label" for="exclude_virtually_paid"></label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label"
                                       for="exclude_amex">Exclude Amex</label>
                                <div class="col-sm-7">
                                    <div
                                        class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                        <input type="checkbox" class="custom-control-input" id="exclude_amex"
                                               name="exclude_amex" @if($selected_amex) checked @endif>
                                        <label class="custom-control-label" for="exclude_amex"></label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label"
                                       for="exclude_bank_transfers">Exclude bank transfers</label>
                                <div class="col-sm-7">
                                    <div
                                        class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                        <input type="checkbox" class="custom-control-input" id="exclude_bank_transfers"
                                               name="exclude_bank_transfers" @if($selected_bank) checked @endif>
                                        <label class="custom-control-label" for="exclude_bank_transfers"></label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Preview invoices</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    @if($results)
        <!-- END Hero -->
        <!-- Page Content -->
        <div class="content">
            <div class="col-md-12">
                <form class="mb-5" id="charge_credit_cards" method="post" action="{{action('ChargeCreditCardsController@charge')}}">
                    @csrf
                    <input name="_method" type="hidden" value="post">
                    <div class="block block-rounded block-bordered">
                        <div class="block-content block-content-full">
                            <p>
                                <button type="button" class="btn btn-primary select_all">Select all</button>
                                <button type="button" class="btn btn-primary deselect_all">Deselect all</button>
                            </p>
                            <table data-order='[[0, "asc"]]'
                                   class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                <thead>
                                <tr>
                                    <th>Select</th>
                                    <th>ID</th>
                                    <th>Customer</th>
                                    <th>Card expiry</th>
                                    <th>Invoice number</th>
                                    <th>Invoice date</th>
                                    <th>Expiration date</th>
                                    <th>€ incl. VAT</th>
                                    <th>FC excl. VAT</th>
                                    <th>Status</th>
                                    <th>Amount paid</th>
                                    <th>Virtually left</th>
                                    <th>Amount left</th>
                                    <th>Charge</th>
                                    <th>Details</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($results as $invoice)
                                    <tr role="row">
                                        <td><input name="invoices[{{$invoice['combined']['in_id']}}]" type="checkbox"></td>
                                        <td>{{$invoice['combined']['in_id']}}</td>
                                        <td>{{$invoice['cu_company_name_business']}}</td>
                                        <td>{{$invoice['card']['adcade_card_expiry_year']}}/{{$invoice['card']['adcade_card_expiry_month']}}</td>
                                        <td>{{$invoice['combined']['in_number']}}</td>
                                        <td>{{$invoice['combined']['in_date']}}</td>
                                        <td>{{$invoice['combined']['in_expiration_date']}}</td>
                                        <td>{{$invoice['combined']['in_amount_netto_eur']}}</td>
                                        <td>{{$invoice['combined']['in_amount_netto']}}</td>
                                        <td>{{$invoice['latest_status']}}</td>
                                        <td>{{$invoice['paired']}}</td>
                                        <td>{{$invoice['vl']}}</td>
                                        <td>{{$invoice['amount_left']}}</td>
                                        <td><span class="number-currency">{{$invoice['FC']}}</span><input class="form form-control {{$invoice['number_input_class']}}" name="{{$invoice['combined']['in_id']}}-charge" type="number" step="any" value="{{number_format($invoice['left'], 2, ".", "")}}"></td>
                                        @if($invoice['details'])
                                            <td class="text-center">
                                                <button type="button" class="btn btn-sm btn-primary details_show" data-toggle="tooltip"
                                                        title="Expand"
                                                        data-details="{{$invoice['details']}}">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </td>
                                        @else
                                            <td class="text-center">
                                               -
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <button type="submit" class="btn btn-primary">Charge credit cards</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
	<!-- END Page Content -->
    @endif
@endsection

@push('scripts')
    <script>
        jQuery(document).ready(function () {

            $("form .select_all").click(function () {
                $(this).parent().parent().find("input[type=checkbox]:enabled").prop("checked", true);

                $("input[type=checkbox]").trigger("change");

                return false;
            });

            $("form .deselect_all").click(function () {
                $(this).parent().parent().find("input[type=checkbox]:enabled").prop("checked", false);

                $("input[type=checkbox]").trigger("change");

                return false;
            });

            $("#charge_credit_cards").submit(function(){
                if(!confirm("Are you sure you want to charge these credit cards?")){
                    return false;
                }
            });
        });
    </script>
@endpush
