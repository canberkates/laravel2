@extends('layouts.backend')
@include('scripts.datatables')
@include('scripts.datepicker')
@include('scripts.forms')
@include('scripts.dialogs' )
@include('scripts.select2')

@section('content')
	<!-- Hero -->
	<div class="bg-body-light">
		<div class="content content-full">
			<div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
				<h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Create invoices</h1>
				<nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/finance">Finance</a></li>
					</ol>
				</nav>
			</div>
		</div>
	</div>

    @if(empty($invoices))
	<!-- END Hero -->
	<!-- Page Content -->
	<div class="content">
		<div class="col-md-8">
			<div class="block block-rounded block-bordered">
				@if($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach($errors->all() as $error)
								{{$error}}<br>
							@endforeach
						</ul>
					</div>
				@endif
				<div class="block-header block-header-default">
					<h3 class="block-title">
						Filters
					</h3>
				</div>
				<div class="block-content block-content-full">

                    <form class="mb-5" method="post" action="{{action('CreateInvoicesController@previewInvoices')}}">
                        @csrf
                        <input name="_method" type="hidden" value="post">

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label">Year:</label>
                            <div class="col-sm-7">
                                <select type="text" class="form-control" name="year">
                                    <option></option>
                                    @foreach($years as $id => $year)
                                        <option value="{{$year}}">{{$year}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label">Month:</label>
                            <div class="col-sm-7">
                                <select type="text" class="form-control" name="month">
                                    <option></option>
                                    @foreach($months as $id => $month)
                                        <option value="{{$id}}">{{$month}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div id="periods" style="display:none;">
                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="example-hf-email">Period from:</label>
                                <div class="col-sm-7">
                                    <input autocomplete="off" type="text" class="js-datepicker form-control" id="period_from"
                                           name="period_from" data-week-start="1" data-autoclose="true" data-today-highlight="true"
                                           data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="example-hf-email">Period to:</label>
                                <div class="col-sm-7">
                                    <input autocomplete="off" type="text" class="js-datepicker form-control" id="period_to"
                                           name="period_to" data-week-start="1" data-autoclose="true" data-today-highlight="true"
                                           data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="example-hf-email">Invoice date:</label>
                                <div class="col-sm-7">
                                    <input autocomplete="off" type="text" class="js-datepicker form-control" id="invoice_date"
                                           name="invoice_date" data-week-start="1" data-autoclose="true" data-today-highlight="true"
                                           data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd">
                                </div>
                            </div>

                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label">Single month:</label>
                            <div class="col-sm-7">
                                <select type="text" class="form-control" name="single_month">
                                    @foreach($yesno as $id => $type)
                                        <option value="{{$id}}" @if($id==1) selected @endif>{{$type}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label">First letter:</label>
                            <div class="col-sm-7">
                                <select multiple data-placeholder="Select some options..."  type="text" class="js-select2 form-control" name="first_letters[]">
                                    @foreach($letters as $type => $letter)
                                        @foreach($letter as $id => $let)
                                            <option value="{{$id}}">{{$let}}</option>
                                        @endforeach
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label">Customer:</label>
                            <div class="col-sm-7">
                                <select data-placeholder="Select a customer" id="customer"  type="text" class="js-select2 form-control" name="customer">
                                    <option></option>
                                    @foreach($customers as $customer)
                                        <option value="{{$customer->cu_id}}">{{$customer->cu_company_name_business}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div id="wrapped_information" style="display:none;">
                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label">Additional information:</label>
                                <div class="col-sm-7">
                                    <textarea rows="4" class="form-control" name="additional_information"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label">Invoice period:</label>
                            <div class="col-sm-7">
                                <select type="text" class="form-control" name="invoice_period">
                                    @foreach($invoiceperiods as $id => $type)
                                        <option value="{{$id}}">{{$type}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label">Payment method:</label>
                            <div class="col-sm-7">
                                <select type="text" class="form-control" name="payment_method">
                                    <option></option>
                                    @foreach($paymentmethods as $id => $type)
                                        <option value="{{$id}}">{{$type}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label">Invoice amount:</label>
                            <div class="col-sm-7">
                                <select type="text" class="form-control" name="invoice_amount">
                                    @foreach($invoiceamounts as $id => $type)
                                        <option value="{{$id}}">{{$type}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label">Unions:</label>
                            <div class="col-sm-7">
                                <select type="text" class="form-control" name="unions">
                                    <option value="">All unions</option>
                                    <option value="1">EU</option>
                                    <option value="2">All unions, but EU</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label">Only credit/debit lines:</label>
                            <div class="col-sm-7">
                                <select type="text" class="form-control" name="credit_debit_lines">
                                    @foreach ($creditdebitinvoicetypes as $id => $type)
                                        <option value='{{$id}}'>{{$type}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label">Send email:</label>
                            <div class="col-sm-7">
                                <select type="text" class="form-control" name="email">
                                    @foreach($yesno as $id => $type)
                                        <option value="{{$id}}" @if($id==1) selected @endif>{{$type}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="form-group col-md-8">
                                <button type="submit" class="btn btn-primary">Preview invoices</button>
                            </div>
                        </div>

                    </form>
				</div>
			</div>
		</div>
	</div>
    @else
    <!-- END Hero -->
    <!-- Page Content -->
    <div class="content">
        <div class="col-md-12">
            <form class="mb-5" method="post" action="{{action('CreateInvoicesController@createInvoices')}}">
                @csrf
                <input name="_method" type="hidden" value="post">

                <input type="hidden" name="period_from" value="{{$request->period_from}}">
                <input type="hidden" name="period_to" value="{{$request->period_to}}">
                <input type="hidden" name="additional_information" value="{{$request->additional_information}}">
                <input type="hidden" name="single_month" value="{{$request->single_month}}">
                <input type="hidden" name="first_letters" value="{{$request->first_letters}}">
                <input type="hidden" name="customer" value="{{$request->customer}}">
                <input type="hidden" name="payment_method" value="{{$request->payment_method}}">
                <input type="hidden" name="invoice_date" value="{{$request->invoice_date}}">
                <input type="hidden" name="invoice_period" value="{{$request->invoice_period}}">
                <input type="hidden" name="invoice_amount" value="{{$request->invoice_amount}}">
                <input type="hidden" name="credit_debit_lines" value="{{$request->credit_debit_lines}}">
                <input type="hidden" name="email" value="{{$request->email}}">

                @if(isset($soap_failed) && $soap_failed)
                    <div class="alert alert-danger">
                        <strong>Failed to validate the VAT-numbers. For now, you can filter also on all unions BUT EU. Please try again later!</strong>
                    </div>
                @endif

                <div class="block block-rounded block-bordered">
                    <div class="block-content block-content-full">
                        <p>
                            <button type="button" class="btn btn-primary select_all">Select all</button>
                            <button type="button" class="btn btn-primary deselect_all">Deselect all</button>
                        </p>
                        <table data-order='[[0, "asc"]]'
                               class="table table-bordered table-striped table-vcenter js-dataTable-full">
                            <thead>
                            <tr>
                                <th>Invoice</th>
                                <th>Valid</th>
                                <th>Customer</th>
                                <th>Status</th>
                                <th>Lines</th>
                                <th>Amount</th>
                                <th>Discount</th>
                                <th>Amount exc.</th>
                                <th>Amount incl.</th>
                                <th>Preview</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($invoices as $invoice)
                                <tr role="row">
                                    <td><input name="invoices[{{$invoice['cu_id']}}]" type="checkbox"></td>
                                    @if($invoice['valid'] == 2)
                                        <td style="color:red;">VAT</td>
                                    @elseif($invoice['valid'] == 1)
                                        <td><i class="fa fa-check" style="color:green;"></i></td>
                                    @else
                                        <td><i class="fa fa-times-circle" style="color:red;"></i></td>
                                    @endif
                                    <td>{{$invoice['cu_company_name_legal']}}</td>
                                    <td>{{$invoice['status']}}</td>
                                    <td>{{$invoice['invoice_lines']}}</td>
                                    <td>{{$invoice['amount']}}</td>
                                    <td>{{$invoice['discount']}}</td>
                                    <td>{{$invoice['amount_exc']}}</td>
                                    <td>{{$invoice['amount_incl']}}</td>
                                    <td>
                                        <button type="button" class="btn btn-sm btn-primary preview_invoice"
                                                data-toggle="tooltip" title="View"
                                                data-cu_id="{{$invoice['preview']['cu_id']}}"
                                                data-period_from="{{$invoice['preview']['period_from']}}"
                                                data-period_to="{{$invoice['preview']['period_to']}}"
                                                data-single_month="{{$invoice['preview']['single_month']}}"
                                                data-invoice_date="{{$invoice['preview']['invoice_date']}}"
                                                data-only_credit_debit_invoice_lines="{{$invoice['preview']['only_credit_debit_invoice_lines']}}"
                                                data-additional_information="{{$invoice['preview']['additional_information']}}"
                                        >
                                            <i class="fa fa-eye"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <button type="submit" class="btn btn-primary">Create invoices</button>
                        <a href="/finance/createinvoices/"><button type="button" class="btn btn-primary">Back</button></a>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- END Page Content -->
    <div id="previewInvoiceModal" class="modal fade" role="dialog" >
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content" style="width:700px;">
                <div class="modal-header">
                    <h5 class="modal-title">Previewing Invoice</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="invoice_view_iframe">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
	<!-- END Page Content -->
    @endif
@endsection

@push('scripts')
    <script>
        jQuery(document).ready(function () {

            if($("select[name=year]").val() == "" || $("select[name=month]").val() == ""){
                $("#periods").hide();
            }

            $("select[name=year], select[name=month]").change(function(){
                if($("select[name=year]").val() != "" && $("select[name=month]").val() != ""){
                    $("#periods").slideDown();

                    var dateMoment = moment($("select[name=year]").val() + "-" + $("select[name=month]").val() + "-01", "YYYY-MM-DD");
                    $("input[name=period_from]").val(dateMoment.format("YYYY-MM-DD"));
                    $("input[name=period_to], input[name=invoice_date]").val(dateMoment.endOf("month").format("YYYY-MM-DD"));
                }
                else{
                    $("input[name=period_from], input[name=period_to], input[name=invoice_date]").val("");

                    $("#periods").slideUp();
                }
            });

            $("#customer").change(function(){
                if ($(this).val() == "") {
                    $("#wrapped_information").hide();
                } else {
                    $("#wrapped_information").show();
                }
            }).trigger('change');

            $("form .select_all").click(function () {
                $(this).parent().parent().find("input[type=checkbox]:enabled").prop("checked", true);

                $("input[type=checkbox]").trigger("change");

                return false;
            });

            $("form .deselect_all").click(function () {
                $(this).parent().parent().find("input[type=checkbox]:enabled").prop("checked", false);

                $("input[type=checkbox]").trigger("change");

                return false;
            });

            jQuery( document ).on( 'click', '.preview_invoice', function(e) {
                e.preventDefault();

                var $self = jQuery( this );

                console.log($self.data());

                jQuery.ajax({
                    url: "/ajax/invoice/preview",
                    method: 'get',
                    data: {
                        cu_id : $self.data('cu_id') ,
                        period_from : $self.data('period_from') ,
                        period_to : $self.data('period_to') ,
                        single_month : $self.data('single_month') ,
                        invoice_date : $self.data('invoice_date') ,
                        only_credit_debit_invoice_lines : $self.data('only_credit_debit_invoice_lines') ,
                        additional_information : $self.data('additional_information')
                    },
                    success: function( $result) {
                        $("#invoice_view_iframe").html('');
                        $("#invoice_view_iframe").append($result);

                        $("#previewInvoiceModal").modal("toggle");
                    }
                });
            });

        });
    </script>
@endpush
