@extends('layouts.backend')
@include('scripts.datatables')
@include('scripts.datepicker')
@include('scripts.forms')
@include('scripts.dialogs' )
@include('scripts.select2')

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Add journal entry</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/finance">Finance</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <form class="mb-5" id="addJournalEntry" method="post" action="{{action('AddJournalEntryController@filteredIndex')}}">
            @csrf
            <input name="_method" type="hidden" value="post">
            <div class="col-md-6">
                <div class="block block-rounded block-bordered">
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    {{$error}}<br>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="block-header block-header-default">
                        <h3 class="block-title">
                            Filters
                        </h3>
                    </div>
                    <div class="block-content block-content-full">


                        <div class="form-group row">
                            <div class="col-md-1"></div>
                            <label class="col-md-4 col-form-label" for="for-ledger">Ledger account:</label>
                            <div class="col-md-7">
                                <select class="js-select2 form-control" data-placeholder="Select some options..."
                                        name="ledger" id="for-ledger" style="width:100%;">
                                    <option></option>
                                    @foreach($ledgeraccounts as $ledger)
                                        <option value="{{$ledger->leac_number}}">{{$ledger->leac_number }} ({{$ledger->leac_name}})</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="block block-rounded block-bordered">

                    <div class="block-header block-header-default">
                        <h3 class="block-title">
                            Invoices
                        </h3>
                    </div>
                    <div class="block-content block-content-full">

                        <div class="form-group row">
                            <div class="col-md-1"></div>
                            <label class="col-md-4 col-form-label" for="for-invoice1">Invoice 1:</label>
                            <div class="col-md-7">
                                <select class="js-select2 form-control invoice" data-placeholder="Select some options..."
                                        name="invoice[1]" id="for-invoice1" style="width:100%;">
                                    <option></option>
                                    @foreach($invoices as $id => $invoice)
                                        <option value="{{$id}}">{{$invoice}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-4 col-form-label" for="type">Amount:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control invoice_pair_amount amount1" name="amount[1]" id="amount1" autocomplete="off"/>
                            </div>
                        </div>

                        <h2 class="content-heading pt-0"></h2>

                        <div class="form-group row">
                            <div class="col-md-1"></div>
                            <label class="col-md-4 col-form-label" for="for-invoice2">Invoice 2:</label>
                            <div class="col-md-7">
                                <select class="js-select2 form-control invoice" data-placeholder="Select some options..."
                                        name="invoice[2]" id="for-invoice2" style="width:100%;">
                                    <option></option>
                                    @foreach($invoices as $id => $invoice)
                                        <option value="{{$id}}">{{$invoice}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-4 col-form-label" for="type">Amount:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control invoice_pair_amount" name="amount[2]" id="amount2" autocomplete="off"/>
                            </div>
                        </div>

                        <h2 class="content-heading pt-0"></h2>

                        <div class="form-group row">
                            <div class="col-md-1"></div>
                            <label class="col-md-4 col-form-label" for="for-invoice3">Invoice 3:</label>
                            <div class="col-md-7">
                                <select class="js-select2 form-control invoice" data-placeholder="Select some options..."
                                        name="invoice[3]" id="for-invoice3" style="width:100%;">
                                    <option></option>
                                    @foreach($invoices as $id => $invoice)
                                        <option value="{{$id}}">{{$invoice}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-4 col-form-label" for="type">Amount:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control invoice_pair_amount" name="amount[3]" id="amount3" autocomplete="off"/>
                            </div>
                        </div>

                        <h2 class="content-heading pt-0"></h2>

                        <div class="form-group row">
                            <div class="col-md-1"></div>
                            <label class="col-md-4 col-form-label" for="for-invoice4">Invoice 4:</label>
                            <div class="col-md-7">
                                <select class="js-select2 form-control invoice" data-placeholder="Select some options..."
                                        name="invoice[4]" id="for-invoice4" style="width:100%;">
                                    <option></option>
                                    @foreach($invoices as $id => $invoice)
                                        <option value="{{$id}}">{{$invoice}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-4 col-form-label" for="type">Amount:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control invoice_pair_amount" name="amount[4]" id="amount4" autocomplete="off"/>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="block block-rounded block-bordered">

                    <div class="block-header block-header-default">
                        <h3 class="block-title">
                            Invoices
                        </h3>
                    </div>
                    <div class="block-content block-content-full">

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-4 col-form-label" for="type">Amount:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" name="pair_amount" autocomplete="off"/>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-4 col-form-label" for="type">Amount left:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" disabled name="amount_left" autocomplete="off"/>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="form-group col-md-8">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- END Page Content -->
@endsection

@push('scripts')
    <script>
        $(function(){
            $(".invoice").change(function(){
                var amount_class = $(this).attr("name").replace("invoice[", "amount").replace("]", "");

                if($(this).val() != ""){

                    jQuery.ajax({
                        type: "GET",
                        url: '{{ url('/ajax/getinvoicefields') }}',
                        data: {'invoice': $(this).val()},
                        success: function(data) {
                            data = jQuery.parseJSON(data);
                            $("#" + amount_class).val(data.amount_left);
                            $(".invoice_pair_amount").trigger("keyup");
                        },
                        error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                            console.log(JSON.stringify(jqXHR));
                            console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                        }
                    });
                }
                else{
                    $("input." + amount_class).val("");

                    $(".invoice_pair_amount").trigger("keyup");
                }
            });

            $(".invoice_pair_amount, input[name=pair_amount]").keyup(function(){
                var i = 0;
                var amount = parseFloat((($("input[name=pair_amount]").val() != "")?$("input[name=pair_amount]").val().replace(",", "."):0));

                $(".invoice_pair_amount").each(function(){
                    i++;

                    if($(this).val() != ""){
                        amount += parseFloat($(this).val().replace(/,/g, "."));
                    }

                    if(i == 4){
                        $("input[name=amount_left]").val(amount.toFixed(2));
                    }
                });
            });

            $("#addJournalEntry").submit(function(e) {

                if($("input[name=amount_left]").val() != 0) {
                    alert("Amount left has to be zero");
                    //prevent Default functionality
                    e.preventDefault();
                }


            });
        });

    </script>
@endpush

