<form class="mb-6" id="pair_fc_form" enctype="multipart/form-data" method="post" action="{{action('PairBankLinesController@pairFCLine', $bank_line->bali_id)}}">
    @csrf
    <input name="_method" type="hidden" value="post">
    <input name="currency" type="hidden" value="{{$bank_line->bali_currency}}">
    <input name="invoice_amount_left" type="hidden">
    <input name="invoice_amount_left_fc" type="hidden">
    <input name="amount_left_default" type="hidden" value="{{$paired_extras['amount_left']}}">
    <input name="amount_left_default_fc" type="hidden" value="{{$paired_extras['amount_left_fc']}}">
    <input name="amount_clean_fc" type="hidden" value="{{$bank_line->bali_amount_fc}}">
    <input name="amount_clean" type="hidden" value="{{$bank_line->bali_amount}}">

    <div class="block-header block-header-default">
        <h3 class="block-title">
            Pair FC
        </h3>
    </div>
    <div class="block-content block-content-full">


        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label" for="type">Amount left €:</label>
            <div class="col-sm-3">
                <input type="text" class="form-control" disabled id="amount_left" name="amount_left"
                       value="{{number_format($paired_extras['amount_left'], 2, ",", ".")}}"/>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label" for="type">Amount left {{$currencies[$bank_line->bali_currency]['token']}}
                :</label>
            <div class="col-sm-3">
                <input type="text" class="form-control" disabled id="amount_left" name="amount_left_fc"
                       value="{{number_format($paired_extras['amount_left_fc'], 2, ",", ".")}}"/>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label">Invoice:</label>
            <div class="col-sm-8">
                <select id="select_invoice" class="chosen-select form-control" name="pair_invoice">
                    <option></option>
                    @foreach($found_invoices as $id => $invoice)
                        <option @if($bank_line->bali_source == 2 && $id == $bank_line->bali_debtor_in_id) selected @endif value="{{$id}}">{{$invoice}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label">Customer:</label>
            <div class="col-sm-8">
                <select class="chosen-select form-control" name="pair_customer">
                    <option></option>
                    @foreach($found_customers as $id => $customers)
                        <optgroup label="{{$id}}">
                            @foreach($customers as $id => $customer)
                                <option @if($bank_line->bali_source == 2 && $id == $bank_line->bali_debtor_cu_id) selected @endif value="{{$id}}">{{$customer}}</option>
                            @endforeach
                        </optgroup>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label" for="type">Amount left €:</label>
            <div class="col-sm-3">
                <input id="input_amount" type="text" class="form-control" readonly name="pair_amount"/>
                <input id="input_amount_hidden" type="hidden" name="pair_amount_hidden"/>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label" for="type">Amount left {{$currencies[$bank_line->bali_currency]['token']}}
                :</label>
            <div class="col-sm-3">
                <input type="text" class="form-control" name="pair_amount_fc"/>
            </div>
        </div>

        <h2 class="content-heading pt-0"></h2>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label">Ledger account:</label>
            <div class="col-sm-8">
                <select class="js-select2 form-control" name="pair_ledger_account">
                    <option></option>
                    @foreach($ledger_accounts as $id => $ledgers)
                        @foreach($ledgers as $id => $ledger)
                            <option value="{{$id}}">{{$ledger}}</option>
                        @endforeach
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label" for="type">Amount €:</label>
            <div class="col-sm-3">
                <input type="text" class="form-control" name="pair_ledger_account_amount"/>
            </div>
        </div>

        <div id="ledgerfcdiv" style="display: none;" class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label" for="type">Amount {{$currencies[$bank_line->bali_currency]['token']}}:</label>
            <div class="col-sm-3">
                <input type="text" class="form-control" name="pair_ledger_account_amount_fc"/>
            </div>
        </div>

        <h2 class="content-heading pt-0"></h2>

        <div id="invoice_div" style="display: none;">
            <div class="form-group row">
                <div class="col-sm-1"></div>
                <label class="col-sm-3 col-form-label" for="type">Invoice left €
                    / {{$currencies[$bank_line->bali_currency]['token']}} :</label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" disabled value="" name="invoice_left"/>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-1"></div>
            <div class="form-group col-md-8">
                <button type="submit" id="pair_balance" class="btn btn-primary">Pair</button>
                @if(isset($show_pair_balance_to_invoice_button) && $show_pair_balance_to_invoice_button == true && isset($customer_paired))
                    <button type="submit" id="pair_balance_to_invoice" name="pair_balance_to_invoice" class="btn btn-primary" disabled>Pair balance to invoice</button>
                @endif
            </div>
        </div>
    </div>
</form>

@push('scripts')
    <script>

        jQuery(document).ready(function () {

          /*  $("#pair_fc_form").submit(function (e) {
                $("#pair_balance_to_invoice").attr("disabled", "disabled");
                $("#pair_balance").attr("disabled", "disabled");
            })*/
        });

        jQuery(document).ready(function () {
            function pairBalanceToInvoiceChecker(){
                if($("#select_invoice").val() == "" || $("#input_amount").val() == "")
                {
                    $("#pair_balance_to_invoice").prop("disabled", true);
                }
                else {
                    $("#pair_balance_to_invoice").prop("disabled", false);
                }
            }

            var current_currency = $("input[name=currency]").val();

            $("select[name=pair_invoice]").change(function () {
                var in_id = $("select[name=pair_invoice]").val();
                if (in_id != "") {
                    $("#invoice_div").show(500);

                    $("select[name=pair_customer]").attr("disabled", "disabled").trigger("chosen:updated");

                    jQuery.ajax({
                        url: "/finance/getInvoiceFields",
                        method: 'get',
                        data: {invoice_id: in_id},
                        success: function ($result) {
                            data = JSON.parse($result);

                            $("select[name=pair_customer]").val(data.in_cu_id).removeAttr("disabled").trigger("chosen:updated");

                            $("input[name=invoice_amount_left]").val(data.amount_left);
                            $("input[name=invoice_amount_left_fc]").val(data.amount_left_fc);


                            if($("input[name=amount_clean_fc]").val() < data.amount_left_fc){
                                $("input[name=pair_amount_fc]").val($("input[name=amount_clean_fc]").val()).removeAttr("disabled");
                            }
                            else{
                                amount_FC = parseFloat(data.amount_left_fc).toFixed(2);
                                $("input[name=pair_amount_fc]").val(amount_FC).removeAttr("disabled");
                            }


                            pair_value = ($("input[name=pair_amount_fc]").val() / data.in_amount_netto * data.in_amount_netto_eur).toFixed(2);

                            $("input[name=pair_amount]").val(pair_value);
                            $("input[name=pair_amount_hidden]").val(pair_value);

                            calculateAmounts();
                            pairBalanceToInvoiceChecker();
                        }

                    });

                } else {
                    $("input[name=invoice_amount_left]").val("");

                    calculateAmounts();

                    $("#invoice_div").hide(500);
                }
            });

            $("select[name=pair_customer]").change(function(){
                $("select[name=pair_invoice]").val("").trigger("chosen:updated");

                $("#invoice_div").hide(500);

                calculateAmounts();
            });


            $("input[name=pair_amount], input[name=pair_ledger_account_amount],  input[name=pair_ledger_account_amount_fc], input[name=pair_amount_fc]").keyup(function () {

                    if ($("select[name=pair_invoice]").val() == "") {

                        var pair_amount_FC = $("input[name=pair_amount_fc]").val().replace(/,/g, ".");
                        var amount_FC_clean = $("input[name=amount_clean_fc]").val().replace(/,/g, ".");
                        var amount_clean = $("input[name=amount_clean]").val().replace(/,/g, ".");

                        pair_amount = (pair_amount_FC / amount_FC_clean * amount_clean).toFixed(2);

                        $("input[name=pair_amount]").val(pair_amount);
                        $("input[name=pair_amount_hidden]").val(pair_amount);
                    } else {

                        pair_amount = ($("input[name=pair_amount_fc]").val() / data.in_amount_netto * data.in_amount_netto_eur).toFixed(2);

                        $("input[name=pair_amount]").val(pair_amount);
                        $("input[name=pair_amount_hidden]").val(pair_amount);
                    }

                    if ($("select[name=pair_ledger_account]").val() == "770000" || $("select[name=pair_ledger_account]").val() == "470200" || $("select[name=pair_ledger_account]").val() == "470105" || $("select[name=pair_ledger_account]").val() == "771020") {
                        console.log(data);
                        amount = ($("input[name=pair_ledger_account_amount_fc]").val() / data.in_amount_netto * data.in_amount_netto_eur).toFixed(2);

                        $("input[name=pair_ledger_account_amount]").val(amount);
                    } else {
                        $("input[name=pair_ledger_account_amount_fc]").val(0);
                    }


                    calculateAmounts();

                }
            );

            $("select[name=pair_ledger_account]").change(function(){
                calculateAmounts();

                if($("select[name=pair_ledger_account]").val() == "770000" || $("select[name=pair_ledger_account]").val() == "470200" || $("select[name=pair_ledger_account]").val() == "470105" || $("select[name=pair_ledger_account]").val() == "771020"){
                    $('#ledgerfcdiv').show();
                }
                else{
                    $('#ledgerfcdiv').hide();
                }
            });



            function calculateAmounts() {
                var amount_left = parseFloat($("input[name=amount_left_default]").val());
                var amount_left_FC = parseFloat($("input[name=amount_left_default_fc]").val());
                var invoice_amount_left = parseFloat((($("input[name=invoice_amount_left]").val() != "") ? $("input[name=invoice_amount_left]").val() : 0)).toFixed(2);
                var invoice_amount_left_FC = parseFloat((($("input[name=invoice_amount_left_fc]").val() != "") ? $("input[name=invoice_amount_left_fc]").val() : 0)).toFixed(2);
                var pair_amount = parseFloat((($("input[name=pair_amount]").val() != "") ? $("input[name=pair_amount]").val().replace(/,/g, ".") : 0)).toFixed(2);
                var pair_amount_FC = parseFloat((($("input[name=pair_amount_fc]").val() != "") ? $("input[name=pair_amount_fc]").val().replace(/,/g, ".") : 0)).toFixed(2);
                var pair_ledger_account_amount = parseFloat((($("input[name=pair_ledger_account]").val() != "" && $("input[name=pair_ledger_account_amount]").val() != "") ? $("input[name=pair_ledger_account_amount]").val().replace(/,/g, ".") : 0)).toFixed(2);
                var pair_ledger_account_amount_FC = parseFloat((($("input[name=pair_ledger_account]").val() != "" && $("input[name=pair_ledger_account_amount_fc]").val() != "") ? $("input[name=pair_ledger_account_amount_fc]").val().replace(/,/g, ".") : 0)).toFixed(2);

                $("input[name=amount_left]").val((amount_left - pair_amount - pair_ledger_account_amount).toFixed(2));
                $("input[name=amount_left_fc]").val((amount_left_FC - pair_amount_FC - pair_ledger_account_amount_FC).toFixed(2));
                $("input[name=invoice_left]").val((invoice_amount_left - pair_amount).toFixed(2) + " / " + (invoice_amount_left_FC - pair_amount_FC).toFixed(2));

            }

            $("select[name=pair_invoice]").trigger("change");


            $("input[name=pair_amount_fc], #select_invoice").change(function (){
                pairBalanceToInvoiceChecker();
            });

        });
    </script>
@endpush
