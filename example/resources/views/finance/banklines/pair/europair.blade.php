<form class="mb-6 form-small" id="pair_euro_form" method="post" action="{{action('PairBankLinesController@pairEuroLine', $bank_line->bali_id)}}">
    @csrf
    <input name="_method" type="hidden" value="post">
    <input name="currency" type="hidden" value="{{$bank_line->bali_currency}}">
    <input name="invoice_amount_left" type="hidden">
    <input name="amount_left_default" type="hidden" value="{{$paired_extras['amount_left']}}">

    @if(isset($show_pair_balance_to_invoice_button) && $show_pair_balance_to_invoice_button == true && isset($customer) && ($customer->cu_payment_method == 4 || $customer->cu_payment_method == 5 || $customer->cu_payment_method == 6))
        <input name="cu_id" type="hidden" value="{{$customer['cu_id']}}">
    @endif

    <div class="block-header block-header-default">
        <h3 class="block-title">
            Pair Euro
        </h3>
    </div>
    <div class="block-content block-content-full">


        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label" for="type">Amount left €:</label>
            <div class="col-sm-3">
                <input type="text" class="form-control" disabled id="amount_left" name="amount_left"
                       value="{{number_format($paired_extras['amount_left'], 2, ",", ".")}}"/>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label">Invoice:</label>
            <div class="col-sm-8">
                <select id="select_invoice" class="chosen-select form-control" name="pair_invoice">
                    <option></option>
                    @foreach($found_invoices as $id => $invoice)
                        <option @if($bank_line->bali_source == 2 && $id == $bank_line->bali_debtor_in_id) selected @endif value="{{$id}}">{{$invoice}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label">Customer:</label>
            <div class="col-sm-8">
                <select class="chosen-select form-control" name="pair_customer">
                    <option></option>
                    @foreach($found_customers as $id => $customers)
                        <optgroup label="{{$id}}">
                            @foreach($customers as $id => $customer)
                                <option @if($bank_line->bali_source == 2 && $id == $bank_line->bali_debtor_cu_id) selected @endif value="{{$id}}">{{$customer}}</option>
                            @endforeach
                        </optgroup>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label" for="type">Amount left €:</label>
            <div class="col-sm-3">
                <input type="text" class="form-control" name="pair_amount" id="input_amount"/>
            </div>
        </div>

        <h2 class="content-heading pt-0"></h2>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label">Ledger account:</label>
            <div class="col-sm-8">
                <select class="js-select2 form-control" name="pair_ledger_account">
                    <option></option>
                    @foreach($ledger_accounts as $id => $ledgers)
                        @foreach($ledgers as $id => $ledger)
                            <option value="{{$id}}">{{$ledger}}</option>
                        @endforeach
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label" for="type">Amount €:</label>
            <div class="col-sm-3">
                <input type="text" class="form-control" name="pair_ledger_account_amount"/>
            </div>
        </div>

        <h2 class="content-heading pt-0"></h2>

        <div id="invoice_left">
            <div class="form-group row">
                <div class="col-sm-1"></div>
                <label class="col-sm-3 col-form-label" for="type">Invoice left €:</label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" disabled value="" name="invoice_left"/>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-1"></div>
            <div class="form-group col-md-8">
                <button type="submit" id="pair_balance" class="btn btn-primary">Pair</button>

                @if(isset($show_pair_balance_to_invoice_button) && $show_pair_balance_to_invoice_button == true && isset($customer_paired))
                    <button type="submit" id="pair_balance_to_invoice" name="pair_balance_to_invoice" class="btn btn-primary" disabled>Pair balance to invoice</button>
                @endif
            </div>
        </div>
    </div>
</form>

@push('scripts')
    <script>
        jQuery(document).ready(function () {


           /* $("#pair_euro_form").submit(function (e) {
                $("#pair_balance_to_invoice").attr("disabled", "disabled");
                $("#pair_balance").attr("disabled", "disabled");
            })*/


            $("#input_amount, #select_invoice").change(function (){
                pairBalanceToInvoiceChecker();
            });

            function pairBalanceToInvoiceChecker(){
                if($("#select_invoice").val() == "" || $("#input_amount").val() == "")
                {
                    $("#pair_balance_to_invoice").prop("disabled", true);
                }
                else {
                    $("#pair_balance_to_invoice").prop("disabled", false);
                }
            }

            $("select[name=pair_invoice]").change(function () {
                if ($("select[name=pair_invoice]").val() != "") {

                    $("#invoice_left").slideDown();

                    $("select[name=pair_customer]").attr("disabled", "disabled").trigger('change');

                    jQuery.ajax({
                        url: "/finance/getInvoiceFields",
                        method: 'get',
                        data: {invoice_id: $("select[name=pair_invoice]").val()},
                        success: function ($result) {

                            data = JSON.parse($result);

                            $("select[name=pair_customer]").val(data.in_cu_id).removeAttr("disabled").trigger("chosen:updated");


                            $("input[name=invoice_amount_left]").val(data.amount_left);

                            amount_left = parseFloat(data.amount_left).toFixed(2);

                            $("input[name=pair_amount]").val(amount_left);

                            calculateAmounts();
                            pairBalanceToInvoiceChecker();
                        }

                    });

                } else {
                    $("input[name=invoice_amount_left]").val("");

                    calculateAmounts();

                    $("#invoice_left").slideUp();
                }
            });

            $("input[name=pair_amount], input[name=pair_ledger_account_amount],  input[name=pair_ledger_account_amount_FC], input[name=pair_amount_FC]").keyup(function () {
                    $("input[name=pair_amount_hidden]").val($("input[name=pair_amount]").val());
                    calculateAmounts();
                }
            );

            function calculateAmounts() {
                var amount_left = parseFloat($("input[name=amount_left_default]").val());
                var invoice_amount_left = parseFloat((($("input[name=invoice_amount_left]").val() != "") ? $("input[name=invoice_amount_left]").val() : 0)).toFixed(2);
                var pair_amount = parseFloat((($("input[name=pair_amount]").val() != "") ? $("input[name=pair_amount]").val().replace(/,/g, ".") : 0)).toFixed(2);
                var pair_ledger_account_amount = parseFloat((($("input[name=pair_ledger_account]").val() != "" && $("input[name=pair_ledger_account_amount]").val() != "") ? $("input[name=pair_ledger_account_amount]").val().replace(/,/g, ".") : 0)).toFixed(2);

                $("input[name=amount_left]").val((amount_left - pair_amount - pair_ledger_account_amount).toFixed(2));
                $("input[name=invoice_left]").val((invoice_amount_left - pair_amount).toFixed(2));
            }


            $("select[name=pair_invoice]").trigger("change");

        });
    </script>
@endpush
