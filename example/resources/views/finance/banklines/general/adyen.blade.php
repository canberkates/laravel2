<div class="block-content block-content-full">

    <div class="form-group row">
        <div class="col-sm-1"></div>
        <label class="col-sm-3 col-form-label">Timestamp:</label>
        <div class="col-sm-6">
            <input type="text" class="form-control" disabled name="timestamp"
                   value="{{$bank_line->bali_timestamp}}" autocomplete="off"/>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-sm-1"></div>
        <label class="col-sm-3 col-form-label">Reference:</label>
        <div class="col-sm-6">
            <input type="text" class="form-control" disabled name="reference"
                   value="{{$bank_line->bali_reference}}" autocomplete="off"/>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-sm-1"></div>
        <label class="col-sm-3 col-form-label">Credit / Debit:</label>
        <div class="col-sm-6">
            <input type="text" class="form-control" disabled name="credit_debit"
                   value="{{ucfirst($bank_line->bali_credit_debit)}}" autocomplete="off"/>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-sm-1"></div>
        <label class="col-sm-3 col-form-label">Customer:</label>
        <div class="col-sm-6">
            <input type="text" class="form-control" disabled name="customer"
                   value="{{$bank_line->bali_debtor_cu_id}}" autocomplete="off"/>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-sm-1"></div>
        <label class="col-sm-3 col-form-label">Invoice:</label>
        <div class="col-sm-6">
            <input type="text" class="form-control" disabled name="invoice"
                   value="{{$bank_line->bali_debtor_in_id}}" autocomplete="off"/>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-sm-1"></div>
        <label class="col-sm-3 col-form-label">Event code:</label>
        <div class="col-sm-6">
            <input type="text" class="form-control" disabled name="event_code"
                   value="{{$bank_line->bali_event_code}}"
                   autocomplete="off"/>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-sm-1"></div>
        <label class="col-sm-3 col-form-label">Amount:</label>
        <div class="col-sm-6">
            <input type="text" class="form-control" disabled name="amount"
                   value="€ {{$bank_line->bali_amount}}"
                   autocomplete="off"/>
        </div>
    </div>

</div>
