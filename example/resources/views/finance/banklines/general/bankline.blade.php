<div class="block-content block-content-full">
    <input type="hidden" id="bali_id" value="{{$bank_line->bali_id}}">
    <div class="form-group row">
        <div class="col-sm-1"></div>
        <label class="col-sm-3 col-form-label">Statement number:</label>
        <div class="col-sm-6">
            <input type="text" class="form-control" disabled name="statement_number"
                   value="{{$bank_line->bali_statement_number}}" autocomplete="off"/>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-sm-1"></div>
        <label class="col-sm-3 col-form-label">Date:</label>
        <div class="col-sm-6">
            <input type="text" class="form-control" disabled name="date"
                   value="{{$bank_line->bali_date}}" autocomplete="off"/>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-sm-1"></div>
        <label class="col-sm-3 col-form-label">Reference:</label>
        <div class="col-sm-6">
            <input type="text" class="form-control" disabled name="reference"
                   value="{{$bank_line->bali_reference}}" autocomplete="off"/>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-sm-1"></div>
        <label class="col-sm-3 col-form-label">Credit / Debit:</label>
        <div class="col-sm-6">
            <input type="text" class="form-control" disabled name="statement_number"
                   value="{{ucfirst($bank_line->bali_credit_debit)}}" autocomplete="off"/>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-sm-1"></div>
        <label class="col-sm-3 col-form-label">Debtor details</label>
        <div class="col-sm-6">
            <textarea rows="4" class="form-control" disabled name="debtor"
                      autocomplete="off">{{ucfirst($bank_line->bali_debtor_details)}}</textarea>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-sm-1"></div>
        <label class="col-sm-3 col-form-label">Bank details</label>
        <div class="col-sm-6">
            <textarea rows="4" class="form-control" disabled name="bank"
                      autocomplete="off">{{ucfirst($bank_line->bali_bank_details)}}</textarea>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-sm-1"></div>
        <label class="col-sm-3 col-form-label">Amount</label>
        <div class="col-sm-3">
            <input type="text" class="form-control" disabled name="amount"
                   value="€ {{$bank_line->bali_amount}}"
                   autocomplete="off"/>
        </div>
        @if($bank_line->bali_currency == "EUR" && count($paired_bank_lines) == 0)
            <input id="text_amount" type="hidden" value="{{$bank_line->bali_amount}}">
            <div class="col-sm-3"><button type='button' class="btn btn-primary" id='force_fc'>Add foreign currency value</button></div>
        @else
            <div class="col-sm-3"><button type='button' class="btn btn-primary" id='force_euro'>Force FC to become Euro</button></div>
        @endif
    </div>


    <div id="force_fc_div" style="display:none;">

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label">Currency</label>
            <div class="col-sm-3">
                <select class="js-select2 form-control" id="new_foreign_currency" name="new_foreign_currency">
                    <option></option>
                    @foreach($currencies as $id => $currency)
                        <option value="{{$id}}">{{$currency['name']}} ({{$currency['token']}})</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-1"></div>
            <label class="col-sm-3 col-form-label">Foreign Amount</label>
            <div class="col-sm-3">
                <input type="text" class="form-control" id="new_fc_amount" name="new_fc_amount"
                       value=""
                       autocomplete="off"/>
            </div>
            <div class="col-sm-3"><button type='button' class="btn btn-primary" id='save_fc'>Save foreign currency value</button></div>
        </div>
    </div>

    @if($bank_line->bali_currency != "EUR")
    <div class="form-group row">
        <div class="col-sm-1"></div>
        <label class="col-sm-3 col-form-label">Amount FC</label>
        <div class="col-sm-6">
            <input type="text" class="form-control" disabled name="amount"
                   value="{{$currencies[$bank_line->bali_currency]['token']}} {{ucfirst($bank_line->bali_amount_fc)}}"
                   autocomplete="off"/>
        </div>
    </div>
    @endif

    <div class="form-group row">
        <div class="col-sm-1"></div>
        <label class="col-sm-3 col-form-label">Information</label>
        <div class="col-sm-6">
            <textarea rows="4" class="form-control" disabled name="statement_number"
                  autocomplete="off">{{ucfirst($bank_line->bali_information)}}</textarea>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        jQuery(document).ready(function () {

            //Show extra fields for adding a foreign currency to this bank line
            $("#force_fc").click(function () {
                $('#force_fc_div').show();
                $(this).hide();
            });

            $("#new_fc_amount").keypress(function(event) {
                if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                    event.preventDefault();
                }
            });

            $("#force_euro").click(function () {
                if (confirm("Are you sure you want to replace the FC with Euro?")) {
                    $.ajax({
                        type: "GET",
                        url: "/ajax/forceEuro",
                        data: {
                            "bali_id": $("#bali_id").val()
                        },
                        async: false,
                        success: function () {
                            location.reload();
                        }
                    });
                }
            });

            $("#save_fc").click(function(){

                if($("#new_foreign_currency").val() == 0 || $('#new_fc_amount').val() == 0){
                    return false;
                }

                console.log($("#new_foreign_currency").val());

                $.ajax({
                    type: "GET",
                    url: "/ajax/getCurrencyRates",
                    data: {"currency": $("#new_foreign_currency").val()},
                    success: function (data) {
                        data = JSON.parse(data);
                        new_fc_rate = data.pacu_rate;

                        current_value = $('#text_amount').val();
                        current_value = current_value.replace(",", ".");
                        new_value = $('#new_fc_amount').val();

                        current_conversion_max = (current_value * new_fc_rate) * 1.1;
                        current_conversion_min = (current_value * new_fc_rate) * 0.9;

                        if (new_value > current_conversion_max || new_value < current_conversion_min) {
                            if (!confirm("The amount you entered is not between the expected values of " + current_conversion_min.toFixed() + " and " + current_conversion_max.toFixed() + ", are you sure you want to continue?")) {
                                return false;
                            }
                        }

                        console.log($("#bali_id").val());
                        console.log($("#new_foreign_currency").val());
                        console.log(new_value);

                        $.ajax({
                            type: "GET",
                            url: "/ajax/addFCToBankLine",
                            data: {
                                "bali_id": $("#bali_id").val(),
                                "currency": $("#new_foreign_currency").val(),
                                "amount": new_value
                            },
                            async: false,
                            success: function () {
                                location.reload();
                            }
                        });
                    }
                });



            });
        });
    </script>
@endpush
