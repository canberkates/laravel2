@extends('layouts.backend')
@include('scripts.datatables')
@include('scripts.datepicker')
@include('scripts.forms')
@include('scripts.dialogs' )
@include('scripts.select2')


@section('content')
	<!-- Hero -->
	<div class="bg-body-light">
		<div class="content content-full">
			<div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
				<h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Invoice summary</h1>
				<nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/finance">Finance</a></li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
	<!-- END Hero -->

	<!-- Page Content -->
	<div class="content">
		<div class="col-md-8">
			<div class="block block-rounded block-bordered">
				@if($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach($errors->all() as $error)
								{{$error}}<br>
							@endforeach
						</ul>
					</div>
				@endif
				<div class="block-header block-header-default">
					<h3 class="block-title">
						Filters
					</h3>
				</div>
				<div class="block-content block-content-full">

					<form class="mb-6" method="post" action="{{action('PairBankLinesController@filteredIndex')}}">
						@csrf
						<input name="_method" type="hidden" value="post">

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label">Bank line source:</label>
                            <div class="col-sm-6">
                            <select type="text" class="form-control" name="source">
                                @foreach($sources as $id => $value)
                                    <option @if($selected_source == $id) selected @endif value="{{$id}}">{{$value}}</option>
                                @endforeach
                            </select>
                            </div>
                        </div>

						<div class="form-group row">
							<div class="col-sm-1"></div>
							<label class="col-sm-3 col-form-label" for="type">Period:</label>
							<div class="col-sm-6">
								<input type="text" class="form-control drp drp-default" name="period"
								       @if($selected_period != null) value="{{$selected_period}}"
								       @else value="{{date("Y/m/d",  strtotime("-365 days"))}} - {{date("Y/m/d")}}" @endif autocomplete="off"/>
							</div>
						</div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label" for="type">Statement number:</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="statement_number" value="{{$selected_statement}}" autocomplete="off"/>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label">Ledger account:</label>
                            <div class="col-sm-6">
                                <select type="text" class="js-select2 form-control" name="ledger">
                                    <option></option>
                                    @foreach($ledgeraccounts as $la)
                                        <option value="{{$la->leac_number}}">{{$la->leac_number." (".$la->leac_name.")"}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label" for="open">Open</label>
                            <div class="col-sm-7">
                                <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                    <input type="checkbox" @if($open) checked @endif class="custom-control-input" id="open"
                                           name="open">
                                    <label class="custom-control-label" for="open"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label"
                                   for="paired">Paired</label>
                            <div class="col-sm-7">
                                <div
                                    class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                    <input type="checkbox" @if($paired) checked @endif class="custom-control-input" id="paired"
                                           name="paired" >
                                    <label class="custom-control-label" for="paired"></label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
							<div class="col-md-1"></div>
							<div class="form-group col-md-8">
								<button type="submit" class="btn btn-primary">Filter</button>
							</div>
						</div>

					</form>
				</div>
			</div>
		</div>
		@if ($bank_lines != null)
            @if($selected_source == 1)
			<div class="col-md-12">
				<div class="block block-rounded block-bordered">
					<div class="block-content block-content-full">
						<table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                            <thead>
                            <tr>
                                <th>Details</th>
                                <th>Statement number</th>
                                <th>Date</th>
                                <th>Reference</th>
                                <th>Credit / Debit</th>
                                <th>Debtor details</th>
                                <th>Bank details</th>
                                <th>Amount</th>
                                <th>Amount FC</th>
                                <th>Paired</th>
                                <th>Left</th>
                                <th>Information</th>
                                <th>Pair</th>
                            </tr>
                            </thead>
							<tbody>
                                @foreach($bank_lines as $bank_line)
                                    <tr>
                                        <td><button type="button" class="details_load btn btn-sm btn-primary js-tooltip-enabled" data-id="{{$bank_line->bali_id}}" data-file="{{url('ajax/banklinerecords')}}"><i class="fa fa-plus"></i></button></td>
                                        <td>{{$bank_line->bali_statement_number}}</td>
                                        <td>{{$bank_line->bali_date}}</td>
                                        <td>{{$bank_line->bali_reference}}</td>
                                        <td>{{ucfirst($bank_line->bali_credit_debit)}}</td>
                                        <td>{!! nl2br($bank_line->bali_debtor_details)!!}</td>
                                        <td>{!! nl2br($bank_line->bali_bank_details)!!}</td>
                                        <td>€{{number_format($bank_line->bali_amount, 2, ",", ".")}}</td>
                                        <td>{{$paymentcurrencies[$bank_line->bali_currency]}}{{number_format($bank_line->bali_amount_fc, 2, ",", ".") }}</td>
                                        <td>€{{number_format($bank_line->amount_paired, 2, ",", ".")}}</td>
                                        <td>€{{number_format($bank_line->amount_left, 2, ",", ".")}}</td>
                                        <td>{{$bank_line->bali_information}}</td>
                                        <td><a href="{{url('/finance/'.$bank_line->bali_id.'/pair')}}"><i class="fa fa-2x fa-euro-sign"></i></a></td>
                                    </tr>
                                @endforeach
							</tbody>
						</table>
                        <p>Credit: €{{number_format($total_credit, 2, ",", ".")}}</p>
                        <p>Debit: €{{number_format($total_debit, 2, ",", ".")}}</p>
                        <p>Sum:  €{{number_format($total_debit + $total_credit, 2, ",", ".")}}</p>
					</div>
				</div>
			</div>
            @else
                <div class="col-md-12">
                    <div class="block block-rounded block-bordered">
                        <div class="block-content block-content-full">
                            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                <thead>
                                <tr>
                                    <th>Details</th>
                                    <th>Timestamp</th>
                                    <th>Reference</th>
                                    <th>Credit / Debit</th>
                                    <th>Customer</th>
                                    <th>Invoice</th>
                                    <th>Event code</th>
                                    <th>Original Reference</th>
                                    <th>Amount</th>
                                    <th>Amount FC</th>
                                    <th>Paired</th>
                                    <th>Left</th>
                                    @if($selected_source == 2)
                                    <th>Pair</th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($bank_lines as $bank_line)
                                    <tr>
                                        <td><button type="button" data-details="{{$bank_line->details}}." class="details_show">&nbsp;</button></td>
                                        <td>{{$bank_line->bali_timestamp}}</td>
                                        <td>{{$bank_line->bali_reference}}</td>
                                        <td>{{ucfirst($bank_line->bali_credit_debit)}}</td>
                                        <td>{{$bank_line->cu_company_name_business}}</td>
                                        <td>{{$bank_line->in_number}}</td>
                                        <td>{{$bank_line->bali_event_code}}</td>
                                        <td>{{$bank_line->bali_original_reference}}</td>
                                        <td>{{$paymentcurrencies["EUR"]}}{{number_format($bank_line->bali_amount, 2, ",", ".")}}</td>
                                        <td>{{$paymentcurrencies[$bank_line->bali_currency]}}{{number_format($bank_line->bali_amount_fc, 2, ",", ".") }}</td>
                                        <td>{{$paymentcurrencies[$bank_line->bali_currency]}}{{number_format($bank_line->amount_paired, 2, ",", ".")}}</td>
                                        <td>{{$paymentcurrencies[$bank_line->bali_currency]}}{{number_format($bank_line->amount_left, 2, ",", ".")}}</td>
                                        @if($selected_source == 2)<td><a href="{{url('/finance/'.$bank_line->bali_id.'/pair')}}"><i class="fa fa-2x fa-euro-sign"></i></a></td>@endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <p>Credit: €{{number_format($total_credit, 2, ",", ".")}}</p>
                            <p>Debit: €{{number_format($total_debit, 2, ",", ".")}}</p>
                            <p>Sum:  €{{number_format($total_debit + $total_credit, 2, ",", ".")}}</p>
                        </div>
                    </div>
                </div>
		    @endif
    @endif
	</div>
	<!-- END Page Content -->
@endsection

@push('scripts')
    <script>
        jQuery( document ).on( 'click', '.paired_bankline_delete', function(e) {

            e.preventDefault();

            var $self = jQuery(this);

            confirmDelete("{{ url('ajax/pairedbankline/delete') }}", 'get', {id:$self.data('id')}, function() {
                $self.parents( '.dataTable' ).DataTable().row( $self.parents( 'tr' ) ).remove().draw( 'page' );
            });

        });
    </script>
@endpush


