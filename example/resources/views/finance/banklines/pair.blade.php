@extends('layouts.backend')

@include('scripts.datatables')
@include('scripts.datepicker')
@include('scripts.forms')
@include('scripts.dialogs' )
@include('scripts.chosen')


@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Pair bank line</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/finance">Finance</a></li>
                        <li class="breadcrumb-item"><a href="/finance/pairbanklines">Bank Line Pair</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        {{$error}}<br>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="col-md-8">
            <div class="block block-rounded block-bordered">

                    @csrf
                    <input name="_method" type="hidden" value="post">

                    <div class="block-header block-header-default">
                        <h3 class="block-title">
                            General
                        </h3>
                    </div>
                    @if($bank_line->bali_source == 1)
                        @include( 'finance.banklines.general.bankline')
                    @else
                        @include( 'finance.banklines.general.adyen')
                    @endif
            </div>
        </div>
        <div class="col-md-8">
            <div class="block block-rounded block-bordered">
                @if($bank_line->bali_currency == "EUR")
                    @include( 'finance.banklines.pair.europair')
                @else
                    @include('finance.banklines.pair.fcpair')
                @endif
            </div>
        </div>

        <div class="col-md-12">
            <div class="block block-rounded block-bordered">
                <div class="block-content block-content-full">
                    <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Invoice number</th>
                            <th>Customer</th>
                            <th>Ledger account</th>
                            <th>Amount</th>
                            <th>Currency</th>
                            <th>Amount in FC</th>
                            <th>Unpair</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($paired_bank_lines as $row)
                            <tr>
                                <td>{{$row->ktbaliinculeac_id}}</td>
                                <td>{{($row->ktbaliinculeac_in_id == 0 ? "-" : $row->in_number)}}</td>
                                <td>{{($row->ktbaliinculeac_cu_id == 0 ? "-" : $row->cu_company_name_legal)}}</td>
                                <td>{{$paired_extras['ledger_accounts'][$row->ktbaliinculeac_leac_number]}}</td>
                                <td>{{$paired_extras["eur"]." ".number_format($row->ktbaliinculeac_amount, 2, ",", ".")}}</td>
                                <td>{{(empty($row->ktbaliinculeac_currency) ? "N/A" : $row->ktbaliinculeac_currency)}}</td>
                                <td>{{(empty($row->ktbaliinculeac_amount_FC) ? "N/A" : number_format($row->ktbaliinculeac_amount_FC, 2, ",", "."))}}</td>
                                <td>
                                    <button type='button'
                                            class='btn btn-sm btn-primary paired_bankline_delete js-tooltip-enabled'
                                            data-toggle='tooltip' data-id='{{$row->ktbaliinculeac_id}}'
                                            data-original-title='Delete'><i class='fa fa-times'></i></button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection

@push('scripts')
    <script>
        jQuery(document).ready(function () {

            jQuery(document).on('click', '.paired_bankline_delete', function (e) {

                e.preventDefault();

                var $self = jQuery(this);

                console.log($self.data('id'));

                confirmDelete("{{ url('ajax/pairedbankline/delete') }}", 'get', {id: $self.data('id')}, function () {
                    location.reload();
                });

            });

            jQuery("#pair_balance_to_invoice").click( function () {
                var cu_id_of_invoice = $("select[name=pair_customer]").val();
                array_allowed_customers = [];

                <?php
                    foreach ($balance_on_customer_ids as $key => $cu_id) {
                    ?>
                    array_allowed_customers['<?php echo $key;?>'] = '<?php echo $cu_id; ?>'
                <?php
                }
                ?>

                if ($.inArray(cu_id_of_invoice, array_allowed_customers) == -1) {
                    if (!confirm("Are you sure that you want to pair this amount to the invoice? There is no customer line for this customer!")){
                        return false;
                    }
                }
            })

        });
    </script>
@endpush
