@extends('layouts.backend')
@include('scripts.datatables')
@include('scripts.datepicker')
@include('scripts.forms')
@include('scripts.dialogs' )
@include('scripts.chosen')

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Charge prepayment Auto Debit</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/finance">Finance</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="col-md-12">
            <form class="mb-5" id="charge_credit_cards" method="post" action="{{action('ChargePrepaymentAutoDebitController@createAutoDebitFile')}}">
                @csrf
                <input name="_method" type="hidden" value="post">
                <div class="block block-rounded block-bordered">
                    <div class="block-content block-content-full">
                        <p>
                            <button type="button" class="btn btn-primary select_all">Select all</button>
                            <button type="button" class="btn btn-primary deselect_all">Deselect all</button>
                        </p>
                        <table data-order='[[0, "asc"]]'
                               class="table table-bordered table-striped table-vcenter js-dataTable-full">
                            <thead>
                            <tr>
                                <th>Select</th>
                                <th>ID</th>
                                <th>Customer</th>
                                <th>Balance</th>
                                <th>Threshold</th>
                                <th>Charge</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($customers)
                                @foreach ($customers as $invoice)
                                    <tr role="row">
                                        <td><input name="customers[{{$invoice['cu_id']}}]" type="checkbox"></td>
                                        <td>{{$invoice['cu_id']}}</td>
                                        <td>{{$invoice['customer']}}</td>
                                        <td>{{$invoice['FC']}} {{$invoice['current_balance']}}</td>
                                        <td>{{$invoice['FC']}} {{number_format($invoice['threshold'], 2, ".", "")}}</td>
                                        <td>
                                            <span style="position: absolute;margin-top: 13px;margin-left: 5px;z-index: 1;">{{$invoice['FC']}}</span>
                                            <input class="form form-control" style="width: 150px;padding-left: 35px;position: relative;" name="monthly_budget_charge" type="number" step="any" value="{{number_format($invoice['monthly_budget'], 2, ".", "")}}"></td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                        @if($customers)
                            <button type="submit" class="btn btn-primary">Create Auto Debit file</button>
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- END Page Content -->

    <!-- END Page Content -->
@endsection

@push('scripts')
    <script>
        $("form .select_all").click(function () {
            $(this).parent().parent().find("input[type=checkbox]:enabled").prop("checked", true);

            $("input[type=checkbox]").trigger("change");

            return false;
        });

        $("form .deselect_all").click(function () {
            $(this).parent().parent().find("input[type=checkbox]:enabled").prop("checked", false);

            $("input[type=checkbox]").trigger("change");

            return false;
        });

        function update_chosen_select(element, data, val){
            $(element).empty();

            $(element).append(add_options(data));

            if(typeof(val) == "undefined"){
                val = "";
            }

            $(element).val(val).trigger("change").trigger("chosen:updated");
        }

        function add_options(data){
            var html = "";
            if (data.constructor === Array) {
                $.each(data, function(index, keyValue){
                    $.each(keyValue, function(key, value){
                        if(value.constructor === Array){
                            html += "<optgroup label=\"" + key + "\">";
                            html += add_options(value);
                            html += "</optgroup>";
                        }
                        else {
                            html += "<option value=\"" + key + "\">" + value + "</option>";
                        }
                    });
                });
            }
            else{
                $.each(data, function(id, value){
                    if(typeof(value) === "object"){
                        html += "<optgroup label=\"" + id + "\">";
                        html += add_options(value);
                        html += "</optgroup>";
                    }
                    else{
                        html += "<option value=\"" + id + "\">" + value + "</option>";
                    }
                });
            }

            return html;
        }
    </script>
@endpush
