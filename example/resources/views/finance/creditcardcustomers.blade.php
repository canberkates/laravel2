@extends('layouts.backend')
@include('scripts.datatables')
@include('scripts.datepicker')
@include('scripts.forms')
@include('scripts.dialogs' )
@include('scripts.select2')


@section('content')
	<!-- Hero -->
	<div class="bg-body-light">
		<div class="content content-full">
			<div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
				<h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Credit card customers</h1>
				<nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/finance">Finance</a></li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
	<!-- END Hero -->

	<!-- Page Content -->
	<div class="content">
		<div class="col-md-6">
			<div class="block block-rounded block-bordered">
				@if($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach($errors->all() as $error)
								{{$error}}<br>
							@endforeach
						</ul>
					</div>
				@endif
				<div class="block-header block-header-default">
					<h3 class="block-title">
						Filters
					</h3>
				</div>
				<div class="block-content block-content-full">

					<form class="mb-5" method="post" action="{{action('CreditCardCustomersController@filteredIndex')}}">
						@csrf
						<input name="_method" type="hidden" value="post">

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label">Contract:</label>
                            <div class="col-sm-6">
                                <select type="text" class="form-control" name="contract">
                                    <option></option>
                                    @foreach($contracts as $id => $contract)
                                        <option @if ($id === $selected_contract) selected @endif value="{{$id}}">{{$contract}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label">Payment method:</label>
                            <div class="col-sm-6">
                                <select type="text" multiple class="js-select2 form-control" name="paymentmethod[]">
                                    <option></option>
                                    @foreach($paymentmethods as $id => $method)
                                        <option @if (in_array($id, $selected_paymentmethod)) selected @endif value="{{$id}}">{{$method}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

						<div class="row">
							<div class="col-md-1"></div>
							<div class="form-group col-md-8">
								<button type="submit" class="btn btn-primary">Filter</button>
							</div>
						</div>

					</form>
				</div>
			</div>
		</div>
		@if ($selected_paymentmethod != null)
			<div class="col-md-12">
				<div class="block block-rounded block-bordered">
					<div class="block-content block-content-full">
						<table class="table table-bordered table-striped table-vcenter js-dataTable-full">
							<thead>
							<tr>
								<th>ID</th>
								<th>Customer</th>
								<th>Contract</th>
								<th>Shopper reference</th>
								<th>Card expiry</th>
								<th>Card last digits</th>
								<th>Edit</th>
							</tr>
							</thead>
							<tbody>
                            @foreach ($customers as $customer)
                                <tr>
                                    <td>{{$customer->cu_id}}</td>
                                    <td>{{$customer->cu_company_name_business}}</td>
                                    @if($customer->cu_payment_method)
                                        <td>{{$paymentmethods[$customer->cu_payment_method]}}</td>
                                    @else
                                        <td></td>
                                    @endif
                                    <td>{{($customer->adcade_merchant_reference ?? "-")}}</td>
                                    <td>{{($customer->adcade_card_expiry_year ?? "-")}}</td>
                                    <td>{{($customer->adcade_card_last_digits ?? "-")}}</td>
                                    <td>
                                        <button type="button" class="btn btn-sm btn-primary invoice_view" data-toggle="tooltip"
                                                title="Edit" data-invoice_id="{{$customer->cu_id}}">
                                            <i class="fa fa-eye"></i>
                                        </button>
                                    </td>
                                </tr>
							@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		@endif
	</div>
	<!-- END Page Content -->
@endsection


