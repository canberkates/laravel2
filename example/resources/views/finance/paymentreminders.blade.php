@extends('layouts.backend')
@include('scripts.datatables')
@include('scripts.datepicker')
@include('scripts.forms')
@include('scripts.dialogs' )
@include('scripts.select2')


@section('content')
	<!-- Hero -->
	<div class="bg-body-light">
		<div class="content content-full">
			<div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
				<h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Payment reminders</h1>
				<nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/finance">Finance</a></li>
					</ol>
				</nav>
			</div>
		</div>
	</div>

	<!-- END Hero -->
	<!-- Page Content -->
	<div class="content">
		<div class="col-md-12">
			<form class="mb-5" method="post" action="{{action('PaymentRemindersController@sendPaymentReminders')}}">
				@csrf
				<input name="_method" type="hidden" value="post">

				<div class="block block-rounded block-bordered">
					<div class="block-content block-content-full">
						<p>
							<button type="button" class="btn btn-primary select_all">Select all</button>
							<button type="button" class="btn btn-primary deselect_all">Deselect all</button>
						</p>
						<table data-order='[[0, "asc"]]'
						       class="table table-bordered table-striped table-vcenter js-dataTable-full">
							<thead>
							<tr>
								<th>Remind</th>
								<th>Reminder type</th>
								<th>Invoices</th>
								<th>ID</th>
								<th>Customer</th>
								<th>Country</th>
								<th>City</th>
								<th>Status</th>
								<th>Debtor status</th>
								<th>Credit hold</th>
								<th>Payment method</th>
								<th>Outstanding invoices</th>
								<th>Outstanding amount €</th>
							</tr>
							</thead>
							<tbody>
							@foreach ($customers as $id => $customer)
								<tr role="row">
									<td><input name="payment_reminder[{{$id}}]" type="checkbox"></td>
									<td>{{$customer['customer_info']['payment_reminder']}}</td>
									<td><button type="button" class="details_load btn btn-sm btn-primary js-tooltip-enabled" data-id="{{$customer['customer_info']['cu_id']}}" data-file="{{url('ajax/paymentreminderinvoices')}}"><i class="fa fa-plus"></i></button></td>
									<td>{{$customer['customer_info']['cu_id']}}</td>
									<td>{{$customer['customer_info']['cu_company_name_legal']}}</td>
									<td>{{$customer['customer_info']['country']}}</td>
									<td>{{$customer['customer_info']['cu_city']}}</td>
									<td>{{$customer['customer_info']['status']}}</td>
									<td>{{$customer['customer_info']['debtor_status']}}</td>
									<td>{{$customer['customer_info']['cu_credit_hold'] ? "Yes" : "No"}}</td>
									<td>{{$customer['customer_info']['payment_method']}}</td>
									<td>{{count($customer['invoices'])}}</td>
									<td>€ {{number_format($customer['customer_info']['amount_left_eur'], 2, ",", ".")}}</td>
								</tr>
							@endforeach
							</tbody>
                            <tfoot>
                            <th colspan="12"></th>
                            <th>€ {{number_format($total_amount_left, 2, ",", ".")}}</th>
                            </tfoot>
						</table>
						<button type="submit" class="btn btn-primary">Send payment reminders</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<!-- END Page Content -->
@endsection

@push('scripts')
	<script>
        jQuery(document).ready(function () {

            $("form .select_all").click(function () {
                $(this).parent().parent().find("input[type=checkbox]:enabled").prop("checked", true);

                $("input[type=checkbox]").trigger("change");

                return false;
            });

            $("form .deselect_all").click(function () {
                $(this).parent().parent().find("input[type=checkbox]:enabled").prop("checked", false);

                $("input[type=checkbox]").trigger("change");

                return false;
            });
        });
	</script>
@endpush
