@extends('layouts.backend')
@include('scripts.datatables')
@include('scripts.datepicker')
@include('scripts.forms')
@include('scripts.dialogs' )
@include('scripts.select2')


@section('content')
	<!-- Hero -->
	<div class="bg-body-light">
		<div class="content content-full">
			<div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
				<h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Import bank lines</h1>
				<nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/finance">Finance</a></li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
	<!-- END Hero -->

	<!-- Page Content -->
	<div class="content">
		<div class="col-md-6">
			<div class="block block-rounded block-bordered">
				@if($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach($errors->all() as $error)
								{{$error}}<br>
							@endforeach
						</ul>
					</div>
				@endif
				<div class="block-header block-header-default">
					<h3 class="block-title">
						Filters
					</h3>
				</div>
				<div class="block-content block-content-full">

					<form class="mb-5" enctype="multipart/form-data" method="post" action="{{action('ImportBankLinesController@import')}}">
						@csrf
						<input name="_method" type="hidden" value="post">

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label" for="type">Statement number:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="statement_number"
                                       value="{{$next_statement_number}}" autocomplete="off"/>
                            </div>
                            <label class="col-sm-3 col-form-label" for="type" style="color:grey;">Last: {{$last_statement_number}}</label>
                        </div>

						<div class="form-group row">
							<div class="col-sm-1"></div>
							<label class="col-sm-3 col-form-label" for="type">Date:</label>
							<div class="col-sm-4">
								<input type="text" class="js-datepicker form-control" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd" name="date"
								       value="{{$next_date}}" autocomplete="off"/>
							</div>
                            <label class="col-sm-3 col-form-label" for="type" style="color:grey;">Last: {{$last_date}}</label>
						</div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label" for="type">CAMT.053 (XML):</label>
                            <div class="col-sm-4">
                                <input type="file" name="xml"/>
                            </div>
                        </div>

						<div class="row">
							<div class="col-md-1"></div>
							<div class="form-group col-md-8">
								<button type="submit" class="btn btn-primary">Import</button>
							</div>
						</div>

					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- END Page Content -->
@endsection


