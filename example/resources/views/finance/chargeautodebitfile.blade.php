@extends('layouts.backend')
@include('scripts.datatables')
@include('scripts.datepicker')
@include('scripts.forms')
@include('scripts.dialogs' )
@include('scripts.select2')

@section('content')
	<!-- Hero -->
	<div class="bg-body-light">
		<div class="content content-full">
			<div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
				<h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Charge auto debit file</h1>
				<nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/finance">Finance</a></li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
	<!-- END Hero -->
	<!-- Page Content -->
	<div class="content">
		<div class="col-md-6">
			<div class="block block-rounded block-bordered">
				@if($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach($errors->all() as $error)
								{{$error}}<br>
							@endforeach
						</ul>
					</div>
				@endif
				<div class="block-header block-header-default">
					<h3 class="block-title">
						Filters
					</h3>
				</div>
				<div class="block-content block-content-full">

                    <form method="post" action="{{action('ChargeAutoDebitController@createFile')}}">
                        @csrf
                        <input name="_method" type="hidden" value="post">

                        <h2 class="content-heading pt-0">Customer</h2>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label">Customer:</label>
                            <div class="col-sm-7">
                                <select type="text" class="js-select2 form-control" name="customer">
                                    <option></option>
                                    @foreach($customers as $id => $customer)
                                        <option value="{{$id}}">{{$customer}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label">Type:</label>
                            <div class="col-sm-7">
                                <select type="text" class="form-control" name="type">
                                    <option></option>
                                    @foreach($autodebittypes as $id => $type)
                                        <option value="{{$id}}">{{$type}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <h2 class="content-heading pt-0">Amounts</h2>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label">Charge amount (€):</label>
                            <div class="col-sm-7">
                               <input type="text" class="form-control" name="amount">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label">Description:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" name="description">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-8">
                                <button type="submit" class="btn btn-primary">Charge customer</button>
                            </div>
                        </div>

                    </form>
				</div>
			</div>
		</div>
	</div>
	<!-- END Page Content -->
@endsection
