@extends('layouts.backend')
@include('scripts.datatables')
@include('scripts.datepicker')
@include('scripts.forms')
@include('scripts.dialogs' )
@include('scripts.select2')


@section('content')
	<!-- Hero -->
	<div class="bg-body-light">
		<div class="content content-full">
			<div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
				<h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Open debtors</h1>
				<nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/finance">Finance</a></li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
	<!-- END Hero -->
	<!-- Page Content -->
	<div class="content">
		<div class="col-md-6">
			<div class="block block-rounded block-bordered">
				@if($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach($errors->all() as $error)
								{{$error}}<br>
							@endforeach
						</ul>
					</div>
				@endif
				<div class="block-header block-header-default">
					<h3 class="block-title">
						Filters
					</h3>
				</div>
				<div class="block-content block-content-full">

					<form class="mb-5" method="post" action="{{action('OpenDebtorsController@filteredIndex')}}">
						@csrf
						<input name="_method" type="hidden" value="post">

						<div class="form-group row">
							<div class="col-sm-1"></div>
							<label class="col-sm-3 col-form-label" for="type">Date:</label>
							<div class="col-sm-6">
								<input autocomplete="off" value="{{empty($state_per) ? date("Y-m-d") : $state_per}}"
								       type="text" class="js-datepicker form-control" id="example-datepicker3"
								       name="state_per" data-week-start="1" data-autoclose="true"
								       data-today-highlight="true" data-date-format="yyyy-mm-dd"
								       placeholder="yyyy-mm-dd">
							</div>
						</div>

						<div class="form-group row">
							<div class="col-sm-1"></div>
							<label class="col-sm-3 col-form-label">Minimum outstanding invoices:</label>
							<div class="col-sm-6">
								<input class="btn" type="number" name="min_outstanding" value="0">
							</div>
						</div>

						<div class="row">
							<div class="col-md-1"></div>
							<div class="form-group col-md-8">
								<button type="submit" class="btn btn-primary">Filter</button>
							</div>
						</div>

					</form>
				</div>
			</div>
		</div>
		@if ($state_per != null)
			<div class="col-md-12">
				<div class="block block-rounded block-bordered">
					<div class="block-content block-content-full">
						<table data-order='[[0, "asc"]]'
						       class="table table-bordered table-striped table-vcenter js-dataTable-full">
							<thead>
							<tr>
								<th>Invoices</th>
								<th>Customer</th>
								<th>Country</th>
								<th>City</th>
								<th>Status</th>
								<th>Debt manager</th>
								<th>Debtor status</th>
								<th>Credit hold</th>
								<th>Days</th>
								<th>Payment method</th>
								<th>American Express</th>
								<th>Outstanding invoices</th>
								<th>Reminder</th>
								<th>Not overdue</th>
								<th>0-30 days overdue</th>
								<th>30+ days overdue</th>
								<th>Balance</th>
								<th>Latest remark</th>
								<th>Edit</th>
							</tr>
							</thead>
							<tbody>
							@foreach ($customers as $id => $customer)
								<tr role="row">
									<td><button type="button" class="details_load btn btn-sm btn-primary js-tooltip-enabled" data-id="{{$customer['customer']['cu_id']}}&state_per={{$state_per}}" data-file="{{url('ajax/opendebtorsinvoices')}}"><i class="fa fa-plus"></i></button></td>
									<td>{{$customer['customer']['cu_company_name_legal']}}</td>
									<td>{{$customer['customer']['cu_co_code']}}</td>
									<td>{{$customer['customer']['cu_city']}}</td>
									<td>{{$customer['customer']['status']}}</td>
									<td>{{$customer['customer']['debt_manager']}}</td>
									<td>{{$customer['customer']['debtor_status']}}</td>
									<td>{{$customer['customer']['cu_credit_hold'] ? "Yes" : "No"}}</td>
									<td>{{$customer['customer']['days_difference']}}</td>
									<td>{{$customer['customer']['payment_method']}}</td>
									<td>{{$customer['customer']['cu_credit_card_amex'] == 1 ? "Amex" : "No"}}</td>
									<td>{{$customer['customer']['outstanding_invoices']}}</td>
									<td>{{$customer['customer']['payment_reminder']}}</td>
									<td>€ {{number_format($customer['data']['0'], 2, ",", ".")}}</td>
									<td>€ {{number_format($customer['data']['0-30'], 2, ",", ".")}}</td>
									<td>€ {{number_format($customer['data']['30+'], 2, ",", ".")}}</td>
									<td>€ {{number_format($customer['data']['balance'], 2, ",", ".")}}</td>
									<td> <b>{{$customer['customer']['remark']['timestamp']}}</b><br> {!! $customer['customer']['remark']['text']!!} </td>
									<td class="text-center">
										<div class="btn-group">
											<a target="_blank" class="btn btn-sm btn-primary" data-toggle="tooltip"
											   data-placement="left"
											   href="{{ url('customers/' . $customer['customer']['cu_id'] . '/edit')}}">
												<i class="fa fa-eye"></i>
											</a>
										</div>
									</td>
								</tr>
							@endforeach
							</tbody>
							<tfoot>
							<th colspan="11"></th>
							<th>{{$totals['total_outstanding_invoices']}}</th>
							<th></th>
							<th>€{{number_format($totals['total_amount_0'], 2, ',', '.')}}</th>
							<th>€{{number_format($totals['total_amount_0_30'], 2, ',', '.')}}</th>
							<th>€{{number_format($totals['total_amount_30_plus'], 2, ',', '.')}}</th>
							<th>€{{number_format($totals['total_balance'], 2, ',', '.')}}</th>
							<th colspan="2"></th>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		@endif
	</div>
	<!-- END Page Content -->
@endsection
