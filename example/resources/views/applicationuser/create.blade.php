@extends('layouts.backend')

@include('scripts.select2')

@section('content')
    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="block block-rounded block-bordered">

                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    {{$error}}<br>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="block-content">
                        <form method="post" action="{{action('ApplicationUserController@store')}}">
                            @csrf
                            <input name="_method" type="hidden" value="post">
                            <input name="cope_id" type="hidden" value="{{$contactperson->cope_id}}">

                            <input name="redirect_url" type="hidden" value="{{$start_redirect_url}}{{$contactperson->cope_cu_id}}/contactperson/{{$contactperson->cope_id}}/edit">
                            <h2 class="content-heading pt-0">General Information</h2>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="username">Username:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="username" value="{{$contactperson->cope_email}}" disabled>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="name">Name:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="name" value="{{$contactperson->cope_full_name}}" disabled>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="email">Email:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="email" value="{{$contactperson->cope_email}}" disabled>
                                </div>
                            </div>

                            <h2 class="content-heading pt-0"></h2>

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Add User</button>
                                    <a class="btn btn-primary" href="/customers">
                                        Back
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

