@extends('layouts.backend')

@include('scripts.select2')
@include('scripts.raty')


@section('content')


    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Edit Review @if(!empty($survey->su_mover)) of {{$survey->customer->cu_company_name_business}} @endif</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">

        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        {!! $error !!}<br>
                    @endforeach
                </ul>
            </div>
        @endif

        @isset($notices['double'])
            @foreach($notices['double'] as $notice)
                <div class="alert alert-dismissable" role="alert">
                    <p class="mb-0">{!! $notice !!}</p>
                </div>
            @endforeach
        @endisset

        @isset($notices['top'])
            @foreach($notices['top'] as $notice)
                <div class="alert alert-danger alert-dismissable" role="alert">
                    <p class="mb-0">{!! $notice !!}</p>
                </div>
            @endforeach
        @endisset

        @if(count($proof_files) > 0)
                <div class="alert alert-success alert-dismissable" role="alert">
                    <p class="mb-0">The online-user has submitted {{count($proof_files)}} proof files!</p>
                </div>
        @endif
        <div class="row justify-content-center">
            <div class="col-md-12">
                <form class="mb-5 form-small" method="post" id="survey_2_form" action="{{action('Survey2Controller@update', $survey->su_id)}}">

                <div class="block block-rounded block-bordered">
                    <div class="block-content">
                        @csrf
                        <input name="_method" type="hidden" value="PATCH">

                        <h2 class="content-heading pt-0">Statistics</h2>

                        @if ($bounced_mail)
                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <div class="col-sm-11" style="color: red;">
                                    {!! $notices['email_bounce'] !!}
                                </div>
                            </div>
                            <br />
                        @endif


                        @if ($first_review)
                            <div class="form-group row">
                                <div class="col-sm-1"></div>

                                <div class="col-sm-11" style="color:limegreen;font-weight:800;">🎉 THIS IS THE FIRST REVIEW OF THIS MOVER! 🎉</div>
                            </div>
                            <br />
                        @endif

                        @if (!empty($reviews_from_cookies))
                            {!! $reviews_from_cookies !!}
                        @endif

                        @if(!empty($survey->su_were_id))
                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label">How many times is this IP used in the last year:</label>
                            <div class="col-sm-7">
                                <input disabled class="form-control" name="times_ip_used" type="text" value="{{$times_ip_used_last_year}} time(s)">
                            </div>
                        </div>
                        @endif

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label">Average reviews per day:</label>
                            <div class="col-sm-7">
                                <input disabled class="form-control" name="times_ip_used" type="text" value="Last 112 days: {{$avg_112_days_per_day}} per day / Last 30 days: {{$avg_30_days_per_day}} per day">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label">Total reviews last 112 days / 30 days:</label>
                            <div class="col-sm-7">
                                <input disabled class="form-control" name="times_ip_used" type="text" value="{{$received_last_112_days}} / {{$received_last_30_days}}">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="block block-rounded block-bordered">
                    <div class="block-content">
                            @csrf
                            <input name="_method" type="hidden" value="PATCH">

                            <h2 class="content-heading pt-0">General</h2>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label">Source:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" data-id="{{$survey->su_source}}" name="source" type="text" value="{{$surveysource}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label">Status:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text" value="{{$surveystatus}}">
                                </div>
                            </div>

                            <div class="form-group row" @if ($survey->su_source == 2) style="display:none;" @endif>
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label">Sent:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text" value="{{$survey->su_timestamp}}">
                                </div>
                            </div>

                            <div class="form-group row" @if ($survey->su_source == 2) style="display:none;" @endif>
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label">Opened:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text"
                                           value="{{$survey->su_opened_timestamp}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label">Submitted:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text"
                                           value="{{$survey->su_submitted_timestamp}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label">Sent:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text"
                                           value="{{$survey->su_sent}} time(s)">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label">Show on website:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text" value="@if( $showonwebsite == 1 ) {{'Yes'}} @else {{ 'No' }} @endif">
                                </div>
                            </div>

                            <h2 class="content-heading pt-0">Review Details</h2>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label">Moved:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text" value="{{$moved}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label"
                                       for="experience_blog">Experience blog:</label>
                                <div class="col-sm-7">
                                    <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                        <input type="checkbox" class="custom-control-input"
                                               id="experience_blog"
                                               name="experience_blog"
                                               disabled
                                                {{($survey->su_experience_blog ? "checked" : "")}}>
                                        <label class="custom-control-label"
                                               for="experience_blog">Shows whether this user has signed up for the
                                            experience blog</label>
                                    </div>
                                </div>
                            </div>

                            @if($survey->su_moved == 3)
                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label">New moving date:</label>
                                    <div class="col-sm-7">
                                        <input disabled class="form-control" type="text"
                                               value="{{$survey->su_new_moving_date}}">
                                    </div>
                                </div>
                            @elseif($survey->su_moved == 1)
                                @if(!empty($survey->su_mover))

                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-3 col-form-label">Mover:</label>
                                        <div class="col-sm-7">
                                            <input disabled data-id="{{$survey->su_mover}}" class="form-control" name="mover" type="text"
                                                   value="{{$survey->customer->cu_company_name_business}}">
                                        </div>
                                    </div>

                                @else

                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-3 col-form-label">Pick a Mover:</label>
                                        <div class="col-sm-7">
                                            <select type="text" class="js-select2 form-control" name="mover">
                                                <option value="-1">[No Mover]</option>
                                                @foreach($customers as $customer)
                                                    <option value="{{$customer->cu_id}}">{{$customer->cu_company_name_business}} @if($customer->country)({{$customer->country->co_en}})@endif</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-3 col-form-label">Other mover description:</label>
                                        <div class="col-sm-7">
                                            <input disabled class="form-control" type="text"
                                                   value="{{$survey->su_other_mover_description}}">
                                        </div>
                                    </div>

                                @endif

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label">Why this mover:</label>
                                    <div class="col-sm-7">
                                        <input disabled class="form-control" type="text"
                                               value="{{$survey->su_why_mover}}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="rating">Rating:</label>
                                    <div class="col-sm-3">
                                        <div id="stars_rating" data-score="{{$survey->su_rating}}" style="float: left; width: auto;"></div>
                                        <input type="text" name="rating" style="position: absolute; top: -9999px;" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="rating_motivation">Rating motivation:</label>
                                    <div class="col-sm-6">
                                    <textarea style="height: 75px;" class="form-control" type="text" name="rating_motivation">{{$survey->su_rating_motivation}}</textarea>
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="button" name="translate" class="btn btn-primary">Translate</button>
                                    </div>
                                </div>

                                <div class="form-group row" name="translate_div" style="display:none;">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="motivation_translation">Translated motivation:</label>
                                    <div class="col-sm-6">
                                    <textarea style="height: 75px;" class="form-control" type="text" disabled
                                          name="motivation_translation"></textarea>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="recommend_mover">Recommend
                                        mover:</label>
                                    <div class="col-sm-3">
                                        <select type="text" class="form-control" name="recommend_mover">
                                            <option @if($survey->su_recommend_mover == 2) selected @endif value="2">No</option>
                                            <option @if($survey->su_recommend_mover == 1) selected @endif value="1">Yes</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="pro_1">Pro 1:</label>
                                    <div class="col-sm-3">
                                        <input class="form-control" type="text" name="pro_1"
                                               value="{{$survey->su_pro_1}}">
                                    </div>
                                    <div class="col-sm-3">
                                        <select type="text" class="form-control" name="pro_1_category">
                                            <option></option>
                                            @foreach($procategories as $category)
                                                <option value="{{$category->prcoca_id}}" {{ ($survey->su_pro_1_category == $category->prcoca_id) ? 'selected': '' }}>{{$category->prcoca_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="pro_2">Pro 2:</label>
                                    <div class="col-sm-3">
                                        <input class="form-control" type="text" name="pro_2"
                                               value="{{$survey->su_pro_2}}">
                                    </div>
                                    <div class="col-sm-3">
                                        <select type="text" class="form-control" name="pro_2_category">
                                            <option></option>
                                            @foreach($procategories as $category)
                                                <option value="{{$category->prcoca_id}}" {{ ($survey->su_pro_2_category == $category->prcoca_id) ? 'selected': '' }}>{{$category->prcoca_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="con_1">Con 1:</label>
                                    <div class="col-sm-3">
                                        <input class="form-control" type="text" name="con_1"
                                               value="{{$survey->su_con_1}}">
                                    </div>
                                    <div class="col-sm-3">
                                        <select type="text" class="form-control" name="con_1_category">
                                            <option></option>
                                            @foreach($concategories as $category)
                                                <option value="{{$category->prcoca_id}}" {{ ($survey->su_con_1_category == $category->prcoca_id) ? 'selected': '' }}>{{$category->prcoca_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="con_2">Con 2:</label>
                                    <div class="col-sm-3">
                                        <input class="form-control" type="text" name="con_2"
                                               value="{{$survey->su_con_2}}">
                                    </div>
                                    <div class="col-sm-3">
                                        <select type="text" class="form-control" name="con_2_category">
                                            <option></option>
                                            @foreach($concategories as $category)
                                                <option value="{{$category->prcoca_id}}" {{ ($survey->su_con_2_category == $category->prcoca_id) ? 'selected': '' }}>{{$category->prcoca_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                @if($survey->su_source == 1)
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-3 col-form-label">City from:</label>
                                        <div class="col-sm-7">
                                            <input disabled class="form-control" type="text"
                                                   value="{{$survey->request->re_city_from}}">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-3 col-form-label">Country from:</label>
                                        <div class="col-sm-7">
                                            <input disabled class="form-control" type="text"
                                                   value="{{$survey->request->countryfrom->co_en}}">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-3 col-form-label">City to:</label>
                                        <div class="col-sm-7">
                                            <input disabled class="form-control" type="text"
                                                   value="{{$survey->request->re_city_to}}">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-3 col-form-label">Country to:</label>
                                        <div class="col-sm-7">
                                            <input disabled class="form-control" type="text"
                                                   value="{{$survey->request->countryto->co_en}}">
                                        </div>
                                    </div>
                                @else
                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-3 col-form-label" for="city_from">City from:</label>
                                            <div class="col-sm-7">
                                                <input class="form-control" type="text" name="city_from"
                                                       value="{{$survey->website_review->were_city_from}}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-3 col-form-label" for="for-country_from">Country from:</label>
                                            <div class="col-sm-7">
                                                <select id="for-country_from" class="js-select2 form-control"
                                                        name="country_from" data-placeholder="Choose one..">
                                                    <option value=""></option>
                                                    @foreach($countries as $country)
                                                        <option value="{{$country->co_code}}" {{ ($survey->website_review->were_co_code_from == $country->co_code) ? 'selected':'' }}>{{$country->co_en}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-3 col-form-label" for="city_to">City to:</label>
                                            <div class="col-sm-7">
                                                <input class="form-control" type="text" name="city_to"
                                                       value="{{$survey->website_review->were_city_to}}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-3 col-form-label" for="for-country_to">Country to:</label>
                                            <div class="col-sm-7">
                                                <select id="for-country_to" class="js-select2 form-control" name="country_to"
                                                        data-placeholder="Choose one..">
                                                    <option value=""></option>
                                                    @foreach($countries as $country)
                                                        <option value="{{$country->co_code}}" {{ ($survey->website_review->were_co_code_to == $country->co_code) ? 'selected':'' }}>{{$country->co_en}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                @endif
                            @endif

                            <h2 class="content-heading pt-0">Mover comment</h2>

                            @if(!empty($survey->su_mover_comment))

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label">Submitted:</label>
                                    <div class="col-sm-7">
                                        <input disabled class="form-control" type="text"
                                               value="{{$survey->su_mover_comment_timestamp}}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="mover_comment">Comment:</label>
                                    <div class="col-sm-6">
                                        <textarea style="height: 75px;" class="form-control" type="text" name="mover_comment">{{$survey->su_mover_comment}}</textarea>
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="button" name="translate_comment" class="btn btn-primary">Translate</button>
                                    </div>
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-2">
                                        <button type="submit" name="save_mover_comment" class="btn btn-primary mt-3">Approve</button>
                                    </div>
                                </div>

                                <div class="form-group row" name="translate_comment_div" style="display:none;">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="comment_translation">Translated comment:</label>
                                    <div class="col-sm-6">
                                        <textarea style="height: 75px;" class="form-control" type="text" disabled
                                                  name="comment_translation"></textarea>
                                    </div>
                                </div>
                            @else
                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-11 col-form-label">The mover didn't comment on this survey
                                        yet.</label>
                                </div>

                            @endif

                            @if($survey->su_source == 1)
                                <h2 class="content-heading pt-0">Request</h2>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label">ID:</label>
                                    <div class="col-sm-7">
                                        <input disabled class="form-control" type="text"
                                               value="{{$survey->request->re_id}}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label">Timestamp:</label>
                                    <div class="col-sm-7">
                                        <input disabled class="form-control" type="text"
                                               value="{{$survey->request->re_timestamp}}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label">Portal:</label>
                                    <div class="col-sm-7">
                                        <input disabled class="form-control" type="text"
                                               value="{{$survey->request->requestcustomerportal->portal->po_portal}}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label">Source:</label>
                                    <div class="col-sm-7">
                                        <input disabled class="form-control" type="text" value="{{$requestsource}}">
                                    </div>
                                </div>

                                @if($survey->request->websiteform)
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-3 col-form-label">Form:</label>
                                        <div class="col-sm-7">
                                            <input disabled class="form-control" type="text" value="{{$survey->request->websiteform->website->we_website}} ({{$survey->request->websiteform->wefo_name}})">
                                        </div>
                                    </div>
                                @else
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-3 col-form-label">Form:</label>
                                        <div class="col-sm-7">
                                            <input disabled class="form-control" type="text" value="{{$survey->request->affiliateform->afpafo_name}}">
                                        </div>
                                    </div>
                                @endif
                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label">Language:</label>
                                    <div class="col-sm-7">
                                        <input disabled class="form-control" type="text" value="{{$requestlanguage}}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label">Device:</label>
                                    <div class="col-sm-7">
                                        <input disabled class="form-control" type="text" value="{{$device}}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="for-ip_address">IP address:</label>
                                    @if (!empty($survey->request->re_ip_address_country))
                                        <div class="col-sm-2" style="align-self: center;"><img src="/media/flags/{{strtolower($survey->request->re_ip_address_country)}}.png"/> {{$ip_countries[$survey->request->re_ip_address_country]}}</div>
                                    @endif
                                    <div class="col-sm-5">
                                        <input disabled class="form-control" type="text" id="for-ip_address" value="{{$survey->request->re_ip_address}}">
                                    </div>

                                </div>

                                @isset($notices['ip_address'])
                                    <div class="form-group row">
                                        <div class="col-sm-11" style="color:red;">
                                            <label>{!! $notices['ip_address'] !!}</label>
                                        </div>
                                    </div>
                                @endisset


                                <h2 class="content-heading pt-0">Contact Details</h2>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label">Company name:</label>
                                    <div class="col-sm-4">
                                        <input disabled class="form-control" type="text"
                                               value="{{$survey->request->re_company_name}}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label">Name:</label>
                                    <div class="col-sm-4">
                                        <input disabled class="form-control" type="text"
                                               value="{{$survey->request->re_full_name}}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label">Telephone 1:</label>
                                    <div class="col-sm-4">
                                        <input disabled class="form-control" type="text"
                                               value="{{$survey->request->re_telephone1}}">
                                    </div>
                                    @isset($notices['phone_1'])
                                        <label class="col-sm-4" style="color:red">This phone number is NOT
                                            valid!</label>
                                    @endisset
                                </div>

                                @if(!empty($survey->request->re_telephone2))

                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-3 col-form-label">Telephone 2:</label>
                                        <div class="col-sm-4">
                                            <input disabled class="form-control" type="text"
                                                   value="{{$survey->request->re_telephone2}}">
                                        </div>
                                        @isset($notices['phone_2'])
                                            <label class="col-sm-4" style="color:red">This phone number is NOT
                                                valid!</label>
                                        @endisset
                                    </div>
                                @endif

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label">E-mail:</label>
                                    <div class="col-sm-4">
                                        <input disabled class="form-control" type="text"
                                               value="{{$survey->request->re_email}}">
                                    </div>
                                </div>

                                @isset($notices['email_bounce'])
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-3 col-form-label">Bounce:</label>
                                        <div class="col-sm-7" style="color:red">
                                            <label>{!! $notices['email_bounce'] !!}</label>
                                        </div>
                                    </div>
                                @endisset


                            @else
                                <h2 class="content-heading pt-0">Website review</h2>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label">ID:</label>
                                    <div class="col-sm-7">
                                        <input disabled class="form-control" type="text"
                                               value="{{$survey->website_review->were_id}}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label">Portal:</label>
                                    <div class="col-sm-7">
                                        <input disabled class="form-control" type="text"
                                               value="{{$survey->website_review->portal->po_portal}} @if($survey->website_review->portal->country) ({{$survey->website_review->portal->country->co_en}} @endif">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label">Form:</label>
                                    <div class="col-sm-7">
                                        <input disabled class="form-control" type="text"
                                               value="{{$survey->website_review->websiteform->website->we_website}} ({{$survey->website_review->websiteform->wefo_name}})">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label">Device:</label>
                                    <div class="col-sm-7">
                                        <input disabled class="form-control" type="text" value="{{$device}}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label">User agent:</label>
                                    <div class="col-sm-6">
                                        <input disabled class="form-control" type="text" value="{{$survey->website_review->were_user_agent}}">
                                    </div>
                                    @if(count($user_agents_last_10_reviews) > 0)
                                        <div class="col-sm-1">

                                            <div id="user_agent_more" class="btn btn-sm btn-primary">
                                                Show more
                                            </div>
                                            <div id="user_agent_less" class="btn btn-sm btn-primary" style="display:none;">
                                                Show less
                                            </div>

                                        </div>
                                    @endif
                                </div>

                                <div id="user_agents_last_10" class="form-group row" style="display:none;">
                                    @if(count($user_agents_last_10_reviews) > 0)
                                        <div class="col-sm-4"></div>
                                        <div class="col-sm-7">
                                            <i><b>Last @if(count($user_agents_last_10_reviews) == 1) {{"review"}} @else {{count($user_agents_last_10_reviews)." reviews"}} @endif for this mover</b></i>
                                        </div>

                                    @foreach ($user_agents_last_10_reviews as $user_agent)
                                            <div class="col-sm-4"></div>
                                            <div class="col-sm-7">
                                                {{$user_agent}}
                                            </div>
                                            <div class="col-sm-1"></div>
                                        @endforeach
                                    @endif
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label">IP Address:</label>
                                    @if (!empty($survey->website_review->were_ip_address_country))
                                        <div class="col-sm-2" style="align-self: center;"><img src="/media/flags/{{strtolower($survey->website_review->were_ip_address_country)}}.png"/> {{$ip_countries[strtoupper($survey->website_review->were_ip_address_country)]}}</div>
                                    @endif
                                    <div class="col-sm-5">
                                        <input disabled class="form-control" type="text"
                                               value="{{$survey->website_review->were_ip_address}}">
                                    </div>
                                </div>

                                @isset($notices['ip_address'])
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-11" style="color:red;">
                                            <label>{!! $notices['ip_address'] !!}</label>
                                        </div>
                                    </div>
                                @endisset

                                <h2 class="content-heading pt-0">Contact Details</h2>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="full_name">Name:</label>
                                    <div class="col-sm-7">
                                        <input class="form-control" type="text" name="full_name"
                                               value="{{$survey->website_review->were_name}}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="email">E-mail:</label>
                                    <div class="col-sm-6">
                                        <input class="form-control" type="text" name="email"
                                               value="{{$survey->website_review->were_email}}">
                                    </div>
                                    <div class="col-sm-1"> <a href="#" data-id="{{$survey->su_id}}" class="confirm_review"> <i class="fa fa-2x fa-envelope"></i></a></div>
                                </div>

                                @isset($notices['email_bounce'])
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-3 col-form-label">Bounce:</label>
                                        <div class="col-sm-7" style="color:red">
                                            <label>{!! $notices['email_bounce'] !!}</label>
                                        </div>
                                    </div>
                                @endisset

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label"
                                           for="contact_confirmation">Confirmation sent</label>
                                    <div class="col-sm-7">
                                        <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                            <input type="checkbox" class="custom-control-input"
                                                   id="contact_confirmation"
                                                   name="contact_confirmation"
                                                   disabled
                                                    {{($survey->su_confirmation_email != "0000-00-00 00:00:00" ? "checked" : "")}}>
                                            <label class="custom-control-label"
                                                   for="contact_confirmation"></label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label"
                                           for="contact_verified">Verified</label>
                                    <div class="col-sm-7">
                                        <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                            <input type="checkbox" class="custom-control-input"
                                                   id="contact_verified"
                                                   name="contact_verified"
                                                    {{($survey->website_review->were_verified  ? "checked" : "")}}>
                                            <label class="custom-control-label"
                                                   for="contact_verified"></label>

                                            @if(count($proof_files) > 0)
                                                <div class='mt-1'>
                                                    @foreach ($proof_files as $name => $url)
                                                        <a href='{{$url}}' target='_blank'>{{$name}}@if(!$loop->last) , @endif</a>
                                                    @endforeach
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label"
                                           for="contact_anonymous">Anonymous</label>
                                    <div class="col-sm-7">
                                        <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                            <input type="checkbox" class="custom-control-input"
                                                   id="contact_anonymous"
                                                   name="contact_anonymous"
                                                   disabled
                                                    {{($survey->website_review->were_anonymous ? "checked" : "")}}>
                                            <label class="custom-control-label"
                                                   for="contact_anonymous"></label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label"
                                           for="contact_send_details">Send contact details to mover:</label>
                                    <div class="col-sm-7">
                                        <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                            <input type="checkbox" class="custom-control-input"
                                                   id="contact_send_details"
                                                   name="contact_send_details"
                                                   disabled
                                                    {{($survey->website_review->were_send_contact_details_to_mover ? "checked" : "")}}>
                                            <label class="custom-control-label"
                                                   for="contact_send_details"></label>
                                        </div>
                                    </div>
                                </div>
                            @endif

                            <h2 class="content-heading pt-0">Remark</h2>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="remark">Remark:</label>
                                <div class="col-sm-7">
                                    <textarea rows="5" class="form-control" type="text"
                                              name="remark">{{$survey->su_checked_remarks}}</textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="send_email">Send e-mail</label>
                                <div class="col-sm-3">
                                    <select type="text" class="form-control" name="send_email">
                                        <option></option>
                                        @foreach($recommend as $id => $value)
                                            <option value="{{$id}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <h2 class="content-heading pt-0"></h2>

                            <div class="row">
                                <div class="col-md-8"></div>
                                <div class="form-group col-md-4">
                                    <button type="submit" name="publish" class="btn btn-success">Publish</button>
                                    <button type="submit" name="reject" class="btn btn-danger">Reject</button>
                                    @if ($survey->su_on_hold == 0)
                                        <button type="submit" name="place_on_hold" class="btn btn-warning">Place on hold</button>
                                    @endif
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>

            <!-- Large Default Modal -->
            <div class="modal" id="modal-block-large" tabindex="-1" role="dialog" aria-labelledby="modal-block-large" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="block block-themed block-transparent mb-0">
                            <div class="block-header bg-primary-dark">
                                <h3 class="block-title">Confirm your review on Sirelo</h3>
                                <div class="block-options">
                                    <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                        <i class="fa fa-fw fa-times"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="block-content">
                                <div id="kekdiffie"><i style="margin-bottom: 50px;margin-top: 50px; margin-left: 300px;" class="fa fa-10x fa-sync fa-spin text-muted"></i></div>
                            </div>
                            <div class="block-content block-content-full text-right bg-light">
                                <button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-sm btn-primary" id="send-email" >Send E-mail</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>




@endsection


@push( 'scripts' )
    <script>
        jQuery(document).ready(function(){
            jQuery( document ).on( 'click', 'button[name=translate]', function(e) {

                var string = $('textarea[name=rating_motivation]').val();

                jQuery.ajax({
                    type: "GET",
                    url: '{{ url('/ajax/gettranslation') }}',
                    data: {'value': string},
                    success: function( $result) {
                        $('div[name=translate_div]').show();
                        $('textarea[name=motivation_translation]').val($result);
                        $('button[name=translate]').attr("disabled", "disabled");
                        console.log( $result);
                    },
                    error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    }
                });
            });
            jQuery( document ).on( 'click', 'button[name=translate_comment]', function(e) {

                var string = $('textarea[name=mover_comment]').val();

                jQuery.ajax({
                    type: "GET",
                    url: '{{ url('/ajax/gettranslation') }}',
                    data: {'value': string},
                    success: function( $result) {
                        $('div[name=translate_comment_div]').show();
                        $('textarea[name=comment_translation]').val($result);
                        $('button[name=translate_comment]').attr("disabled", "disabled");
                        console.log( $result);
                    },
                    error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    }
                });
            });

            $(".confirm_review").click(function(){
                $("#send-email").prop("disabled", false);

                var id = $(this).data("id");
                var email = $("input[name=email]").val();
                var type = $("input[name=source]").data("id");
                var mover = $("input[name=mover]").data("id");
                var other_mover = $("select[name=other_mover]").val();

                if(mover == -1){
                    mover = other_mover;
                }

                if(mover != "") {

                    $("#kekdiffie").html('<i style="margin-bottom: 50px;margin-top: 50px; margin-left: 300px;" class="fa fa-10x fa-sync fa-spin text-muted"></i>');
                    $('#modal-block-large').modal('show');

                    $.ajax({
                        type: "GET",
                        url: '{{ url('/ajax/confirmsurvey') }}',
                        data: {"function": "check", "id": id, "email": email, "type": type, "mover": mover},
                        success: function (data) {

                            $("#kekdiffie").html(data);

                            email_button = $("#kekdiffie").parent().next().find("#send-email");
                            email_button.attr("data-id", id);
                        }
                    });

                }

                return false;
            });

            $("button[name=publish]").click(function(){
                if (jQuery("select[name=mover]").val() == "-1"){
                    alert("No mover selected!")
                    return false;
                }
            });

            $("#send-email").click(function () {

                $this = $(this);
                $(this).attr("disabled", "disabled");

                var id = $(this).data("id");
                var email = $("input[name=email]").val();
                var type = $("input[name=source]").data("id");
                var mover = $("input[name=mover]").data("id");
                var other_mover = $("select[name=other_mover]").val();

                if(mover == -1){
                    mover = other_mover;
                }

                if(mover != "") {

                    $("#kekdiffie").html('<i style="margin-bottom: 50px;margin-top: 50px; margin-left: 300px;" class="fa fa-10x fa-sync fa-spin text-muted"></i>');
                    $('#modal-block-large').modal('show');

                    $.ajax({
                        type: "GET",
                        url: '{{ url('/ajax/confirmsurvey') }}',
                        data: {"function": "send", "id": id, "email": email, "type": type, "mover": mover},
                        success: function (data) {
                            $("#kekdiffie").html('<i style="margin-bottom: 50px;margin-top: 50px; margin-left: 300px;color:green;" class="fa fa-10x fa-check"></i>');
                        }
                    });

                }
            });

            $("#user_agent_more").click(function () {
                $("#user_agent_less").show();
                $("#user_agent_more").hide();
                $("#user_agents_last_10").slideDown();

            });

            $("#user_agent_less").click(function () {
                $("#user_agent_less").hide();
                $("#user_agent_more").show();
                $("#user_agents_last_10").slideUp();
            });


            $("#stars_rating").raty({
                path: "/media/icons/",
                starOff: "icon_star_off.png",
                starOn: "icon_star_on.png",
                target: "input[name=rating]",
                targetType: "score",
                targetKeep: true,
                score: function(){
                    return $(this).data("score");
                }
            });

            $("#survey_2_form ").submit(function(){
                var btn = $(this).find("button[type=submit]:focus");

                if(btn.is($("button[name=publish]"))){
                    var error = 0;

                    if($("input[name=pro_1]").val() != "" && $("select[name=pro_1_category]").val() == ""){
                        alert('Please attach a category to the Pro 1 value!');
                        error++;
                    }

                    if($("input[name=pro_2]").val() != "" && $("select[name=pro_2_category]").val() == ""){
                        alert('Please attach a category to the Pro 2 value!');
                        error++;
                    }

                    if($("input[name=con_1]").val() != "" && $("select[name=con_1_category]").val() == ""){
                        alert('Please attach a category to the Con 1 value!');
                        error++;
                    }

                    if($("input[name=con_2]").val() != "" && $("select[name=con_2_category]").val() == ""){
                        alert('Please attach a category to the Con 2 value!');
                        error++;
                    }

                    if(error > 0){
                        return false;
                    }
                }

                if(btn.is($("button[name=reject]"))){
                    if($("select[name=send_email]").val() == ""){
                        alert('Please decide to send en e-mail or not!');
                        return false;
                    }
                }
            });
        });
    </script>

@endpush

