@extends('layouts.backend')

@include('scripts.datepicker')

@section('content')

    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Edit a Template</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url("email_builder_templates/")}}">Email builder templates</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Edit</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="block block-rounded block-bordered">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Editing {{$template->embute_name}}</h3>
                    </div>
                    <div class="block-content">
                        @if($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        {{$error}}<br>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="mb-5" method="post"
                              action="{{action('EmailBuilderTemplateController@update', $template->embute_name)}}">
                            @csrf
                            <input name="_method" type="hidden" value="POST">

                            <h2 class="content-heading pt-0">General</h2>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="name">Name:</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="name"
                                                   value="{{$template->embute_name}}">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-4 col-form-label" for="description">Description:</label>
                                        <div class="col-sm-7">
                                            <textarea class="form-control" name="description">{{$template->embute_description}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="block-header block-header-default">
                                <h3 class="block-title">Email details</h3>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-10 col-form-label" for="from_email">From (EMAIL):</label>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-10">
                                            <input type="email" class="form-control" name="from_email"
                                                   value="{{$template->embute_from_email}}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-10 col-form-label" for="to_cc">To CC:</label>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="to_cc"
                                                   value="{{$template->embute_to_cc}}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-10 col-form-label" for="to_bcc">To BCC:</label>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="to_bcc"
                                                   value="{{$template->embute_to_bcc}}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="block-header block-header-default">
                                <h3 class="block-title">Email content</h3>
                            </div>
                            <div class="row">
                                <div class="col-md-11 ml-4">
                                    <strong>Tags:</strong>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2 ml-5">
                                    <strong>Customer name:</strong>
                                </div>
                                <div class="col-md-8">
                                    [customername]
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2 ml-5">
                                    <strong>Button:</strong>
                                </div>
                                <div class="col-md-8">
                                    [button link="INSERT LINK HERE" text="INSERT TEXT HERE"]
                                </div>
                            </div>

                            <ul class="nav nav-tabs nav-tabs-alt" data-toggle="tabs" role="tablist">
                                @foreach($templates as $template)
                                    <li class="nav-item">
                                        <a class="nav-link @if ($loop->first) active @endif" href="#btabs-alt-static-{{strtolower($template->embute_language)}}">{{$languages[strtolower($template->embute_language)]}} ({{strtoupper($template->embute_language)}})</a>
                                    </li>
                                @endforeach
                            </ul>

                            <div class="block-content tab-content">
                                @foreach($templates as $template)
                                    <div class="tab-pane {{strtolower($template->embute_language)}} @if ($loop->first) active @endif" id="btabs-alt-static-{{strtolower($template->embute_language)}}" role="tabpanel">
                                        <div class="block-content block-content-full">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group row">
                                                        <div class="col-sm-1"></div>
                                                        <label class="col-sm-10 col-form-label" for="subject_{{strtolower($template->embute_language)}}">Subject:</label>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-sm-1"></div>
                                                        <div class="col-sm-10">
                                                            <input type="text" class="form-control" data-lang="{{strtolower($template->embute_language)}}" name="subject_{{strtolower($template->embute_language)}}"
                                                                   value="{{$data_per_lang[$template->embute_language]['embute_subject']}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-sm-1"></div>
                                                        <label class="col-sm-10 col-form-label" for="email_content_{{strtolower($template->embute_language)}}">Email Content:</label>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-sm-1"></div>
                                                        <div class="col-sm-10">
                                                            <textarea id="email_content_{{strtolower($template->embute_language)}}" data-lang="{{strtolower($template->embute_language)}}" rows="20" class="form-control" name="email_content_{{strtolower($template->embute_language)}}">{{$data_per_lang[$template->embute_language]['embute_content_html']}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="block-header block-header-default">
                                                <h3 class="block-title">Preview</h3>
                                            </div>

                                            <div class="row email_builder_preview">
                                                <div class="col-sm-1"></div>
                                                <div id="previewdata_{{strtolower($template->embute_language)}}" class="col-sm-10">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>





                            <h2 class="content-heading pt-0"></h2>

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                    <a class="btn btn-primary" href="/email_builder_templates">
                                        Back
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script>
        jQuery(document).ready(function () {
            //TRIGGER KEYUP

            jQuery("input[name=subject_en], input[name=subject_nl], input[name=subject_it], input[name=subject_fr], input[name=subject_dk], input[name=subject_de], input[name=subject_es]").keyup(delay(function() {
                var language = jQuery(this).data("lang");
                var textarea = tinymce.get('email_content_' + language).getContent();
                var subject = jQuery("input[name=subject_" + language + "]").val();

                updatePreview(subject, textarea, language);
            }, 500));
        });
    </script>

    <script src="https://cdn.tiny.cloud/1/qk69cz08kh45b4art9orrfek5ajlgggajl9k6owydfanq7rl/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

    <script>
        function delay(callback, ms) {
            var timer = 0;
            return function() {
                var context = this, args = arguments;
                clearTimeout(timer);
                timer = setTimeout(function () {
                    callback.apply(context, args);
                }, ms || 0);
            };
        }

        function updatePreview(subject, textarea, language) {
            jQuery.ajax({
                url: "{{ url('ajax/emailBuilderLivePreview') }}",
                method: 'get',
                data: {"subject": subject,"data":textarea},
                success: function( data) {
                    jQuery("#previewdata_" + language).html(data);
                }
            });
        }

        const languages = [
            'en', 'nl', 'it', 'fr', 'dk', 'de', 'es'
        ];

        jQuery.each(languages, function(key, lang) {
            tinymce.init({
                selector:"#email_content_" + lang,
                plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking save table contextmenu directionality emoticons template paste textcolor colorpicker textpattern imagetools"],
                height: 500,
                setup: function(ed) {
                    ed.on('keyup', delay(function(e) {
                        var textarea = ed.getContent();
                        var subject = jQuery("input[name=subject_" + lang + "]").val();
                        updatePreview(subject, textarea, lang);
                    }, 500));
                }
            });

            var textarea = jQuery("textarea[name=email_content_" + lang + "]").val();
            var subject = jQuery("input[name=subject_" + lang + "]").val();

            updatePreview(subject, textarea, lang);
        });

    </script>
@endpush
