@extends('layouts.backend')

@include( 'scripts.datatables' )
@include( 'scripts.dialogs' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Email Builder</h1>
            </div>
        </div>
    </div>
    <!-- END Hero -->
    <div class="content">

        <div class="block block-rounded block-bordered">
            <div class="block-content block-content-full">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Email Templates
                        <a href="{{ url('email_builder_templates/create')}}"><button data-toggle="click-ripple" class="btn btn-primary">Add template</button></a>
                    </h3>
                </div>
                <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Last updated</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($emails as $template)
                        <tr>
                            <td>{{$template->embute_id}}</td>
                            <td>{{$template->embute_name}}</td>
                            <td>{{$template->embute_description}}</td>
                            <td>{{$template->embute_last_updated_timestamp}}</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <a class="btn btn-sm btn-primary"
                                       data-toggle="tooltip"
                                       data-placement="left"
                                       title="edit"
                                       href="{{ url('email_builder_templates/' . $template->embute_id . '/edit')}}">
                                        <i class="fa fa-pencil-alt"></i>
                                    </a>&nbsp;
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
