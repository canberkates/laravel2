@extends('layouts.backend')

@include('scripts.datatables')
@include('scripts.datepicker')
@include('scripts.forms')
@include( 'scripts.dialogs' )
@include( 'scripts.telinput' )


@pushonce( 'scripts:customer-edit' )
<script src="{{ asset('/js/custom/customer/customer_edit.js') }}"></script>
@endpushonce

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">SP - {{$customer->cu_company_name_business}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/serviceproviders">Service Providers</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Edit</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <!-- Block Tabs Alternative Style -->
                <div class="block block-rounded block-bordered">
                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    {{$error}}<br>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <ul class="nav nav-tabs nav-tabs-alt" data-toggle="tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" href="#btabs-alt-static-general">General</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-emails">Emails</a>
                        </li>
                        @can('customers - finance')
                            <li class="nav-item">
                                <a class="nav-link" href="#btabs-alt-static-finance">Finance</a>
                            </li>
                        @endcan
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-contactpersons">Contact persons</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-status">Status</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-remark">Remarks</a>
                        </li>
                        @can('customers - documents')
                            <li class="nav-item">
                                <a class="nav-link" href="#btabs-alt-static-documents">Documents</a>
                            </li>
                        @endcan
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-offices">Offices</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-questions">Lead Form</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-newsletter_blocks">Newsletters</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-sirelo_advertising">Sirelo Advertising</a>
                        </li>
                        @can('requests')
                            <li class="nav-item">
                                <a class="nav-link" href="#btabs-alt-static-requests">Requests</a>
                            </li>
                        @endcan
                        @can('customers - invoices')
                            <li class="nav-item">
                                <a class="nav-link" href="#btabs-alt-static-invoices">Invoices</a>
                            </li>
                        @endcan
                    </ul>
                    <div class="block-content tab-content">
                        <div class="tab-pane active" id="btabs-alt-static-general" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        <div class="block-content">
                                            <form method="post" enctype="multipart/form-data"
                                                  action="{{action('ServiceProvidersController@update', $customer->cu_id)}}">
                                                @csrf
                                                <input name="_method" type="hidden" value="PATCH">
                                                <input name="form_name" type="hidden" value="General">
                                                <h2 class="content-heading pt-0">General Information</h2>

                                                <div class="row">
                                                    <div class="col-md-6">

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="type">Type of partner:</label>
                                                            <div class="col-sm-7">
                                                                <select disabled type="text" class="form-control"
                                                                        name="type">
                                                                    @foreach($movertypes as $id => $item)
                                                                        <option value="{{$id}}" {{ ($customer->cu_type == $id) ? 'selected':'' }}>{{$item}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="sales_manager">Sales Manager:</label>
                                                            <div class="col-sm-7">
                                                                <select disabled type="text" class="form-control"
                                                                        name="sales_manager">
                                                                    <option value=""></option>
                                                                    @foreach($users as $user)
                                                                        <option value="{{$user->id}}" {{ ($customer->cu_sales_manager == $user->id) ? 'selected':'' }}>{{$user->us_name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="account_manager">Account Manager:</label>
                                                            <div class="col-sm-7">
                                                                <select disabled type="text" class="form-control"
                                                                        name="account_manager">
                                                                    <option value=""></option>
                                                                    @foreach($users as $user)
                                                                        <option value="{{$user->id}}" {{ ($customer->cu_account_manager == $user->id) ? 'selected':'' }}>{{$user->us_name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="debt_manager">Debt Manager:</label>
                                                            <div class="col-sm-7">
                                                                <select disabled type="text" class="form-control"
                                                                        name="debt_manager">
                                                                    <option value=""></option>
                                                                    @foreach($users as $user)
                                                                        <option value="{{$user->id}}" {{ ($customer->cu_debt_manager == $user->id) ? 'selected':'' }}>{{$user->us_name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="la_code">Language:</label>
                                                            <div class="col-sm-7">
                                                                <select type="text" class="form-control"
                                                                        name="la_code">
                                                                    @foreach($languages as $language)
                                                                        <option value="{{$language->la_code}}" {{ ($customer->cu_la_code == $language->la_code) ? 'selected':'' }}>{{$language->la_language}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label" for="sub_type_lead_form">Sub type:</label>
                                                            <div class="col-sm-7">
                                                                <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                                                    <input id='sub_type_lead_form' type="checkbox" class="custom-control-input"
                                                                        name="sub_type_lead_form" {{($customer->serviceproviderdata->seprda_lead_form ? "checked" : "")}}>
                                                                    <label class="custom-control-label"
                                                                        for="sub_type_lead_form">Lead Form</label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label" for="sub_type_email_marketing"></label>
                                                            <div class="col-sm-7">
                                                                <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                                                    <input id='sub_type_email_marketing' type="checkbox" class="custom-control-input"
                                                                        name="sub_type_email_marketing" {{($customer->serviceproviderdata->seprda_email_marketing ? "checked" : "")}}>
                                                                    <label class="custom-control-label"
                                                                        for="sub_type_email_marketing">Email Marketing</label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label" for="sub_type_sirelo_advertising"></label>
                                                            <div class="col-sm-7">
                                                                <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                                                    <input id='sub_type_sirelo_advertising' type="checkbox" class="custom-control-input"
                                                                        name="sub_type_sirelo_advertising" {{($customer->serviceproviderdata->seprda_sirelo_advertising ? "checked" : "")}}>
                                                                    <label class="custom-control-label"
                                                                        for="sub_type_sirelo_advertising">Sirelo Advertising</label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="description">Description:</label>
                                                            <div class="col-sm-7">
                                                          <textarea rows="6" type="text" class="form-control"
                                                                    name="description">{{$customer->cu_description}}</textarea>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="col-md-6">

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="company_name_legal">Company legal name:</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control"
                                                                       name="company_name_legal"
                                                                       value="{{$customer->cu_company_name_legal}}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="company_name_business">Company business name:</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control"
                                                                       name="company_name_business"
                                                                       value="{{$customer->cu_company_name_business}}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="attn">Primary contact:</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control" name="attn"
                                                                       value="{{$customer->cu_attn}}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="attn_email">Primary contact email:</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control"
                                                                       name="attn_email"
                                                                       value="{{$customer->cu_attn_email}}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="lead_email">Lead email:</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control" name="lead_email"
                                                                       value="{{$customer->cu_email}}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="lead_email">Billing email:</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control" name="billing_email"
                                                                       value="{{$customer->cu_email_bi}}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="email">Chamber of Commerce:</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control" name="coc"
                                                                       value="{{$customer->cu_coc}}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="logo">Replace logo:</label>
                                                            <div class="col-sm-7">
                                                                <input type="file" name="logo"/>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <div style="margin-left:15px;"><img
                                                                @if(!empty($customer->cu_logo))
                                                                    <img style="max-width: 300px; max-height: 120px;" src="../../logo.php?path={{env("SHARED_FOLDER")}}uploads/logos/{{$customer->cu_logo}}"/>
                                                                @else
                                                                    <img src="{{ asset('media/logos/logo_placeholder.png') }}"
                                                                        style="max-width: 300px; max-height: 120px;"/>
                                                                @endif
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-sm-1"></div>
                                                            <div class="form-group col-md-8">
                                                                <button type="submit" class="btn btn-primary">Update
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>

                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="block block-rounded block-bordered">
                                                <div class="block-content">
                                                    <form method="post"
                                                          action="{{action('ServiceProvidersController@update', $customer->cu_id)}}">
                                                        @csrf
                                                        <input name="_method" type="hidden" value="PATCH">
                                                        <input name="form_name" type="hidden" value="Addresses">
                                                        <div class="row">
                                                            <div class="col-md-6">

                                                                <h2 class="content-heading pt-0">Addresses</h2>

                                                                <div class="form-group row">
                                                                    <div class="col-sm-1"></div>
                                                                    <label class="col-sm-3 col-form-label"
                                                                           for="street_1">Company street
                                                                        1:</label>
                                                                    <div class="col-sm-7">
                                                                        <input type="text" class="form-control"
                                                                               name="street_1"
                                                                               value="{{$customer->cu_street_1}}">
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <div class="col-sm-1"></div>
                                                                    <label class="col-sm-3 col-form-label"
                                                                           for="street_2">Company street
                                                                        2:</label>
                                                                    <div class="col-sm-7">
                                                                        <input type="text" class="form-control"
                                                                               name="street_2"
                                                                               value="{{$customer->cu_street_2}}">
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <div class="col-sm-1"></div>
                                                                    <label class="col-sm-3 col-form-label"
                                                                           for="zipcode">Company
                                                                        zipcode:</label>
                                                                    <div class="col-sm-7">
                                                                        <input type="text" class="form-control"
                                                                               name="zipcode"
                                                                               value="{{$customer->cu_zipcode}}">
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <div class="col-sm-1"></div>
                                                                    <label class="col-sm-3 col-form-label"
                                                                           for="city">Company city:</label>
                                                                    <div class="col-sm-7">
                                                                        <input type="text" class="form-control"
                                                                               name="city"
                                                                               value="{{$customer->cu_city}}">
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <div class="col-sm-1"></div>
                                                                    <label class="col-sm-3 col-form-label"
                                                                           for="country">Company
                                                                        country:</label>
                                                                    <div class="col-sm-7">
                                                                        <select type="text" class="form-control"
                                                                                name="country">
                                                                            <option value=""></option>
                                                                            @foreach($countries as $country)
                                                                                <option value="{{$country->co_code}}" {{ ($customer->cu_co_code == $country->co_code) ? 'selected':'' }}>{{$country->co_en}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <div class="col-sm-1"></div>
                                                                    <label class="col-sm-3 col-form-label"
                                                                           for="telephone">Company
                                                                        telephone:</label>
                                                                    <div class="col-sm-7">
                                                                        <input type="tel" name="telephone" class="form-control" value="{{$customer->cu_telephone}}">
                                                                        <input type="hidden" id="int_telephone" name="int_telephone" value="">
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <div class="col-sm-1"></div>
                                                                    <label class="col-sm-3 col-form-label"
                                                                           for="website">Company
                                                                        website:</label>
                                                                    <div class="col-sm-6">
                                                                        <input type="text" class="form-control"
                                                                               name="website"
                                                                               value="{{$customer->cu_website}}">
                                                                    </div>
                                                                    <div class="col-sm-1">
                                                                        <a target="_blank" href="{{$customer->cu_website}}"><i class="fa fa-external-link-alt"></i></a>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-sm-1"></div>
                                                                    <div class="form-group col-md-8">
                                                                        <button  name="office_update" style="display:{{($customer->cu_use_billing_address ? "none;" : "block;")}}" type="submit" class="btn btn-primary">
                                                                            Update
                                                                        </button>
                                                                    </div>
                                                                </div>

                                                            </div>

                                                            <div class="col-md-6">


                                                                <h2 class="content-heading pt-0">
                                                                    Use Billing Address
                                                                    <div style="padding-top: 5px;padding-left: 10px;"
                                                                         class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                                                        <input type="checkbox"
                                                                               class="custom-control-input"
                                                                               id="use_billing_address"
                                                                               name="use_billing_address" {{($customer->cu_use_billing_address ? "checked" : "")}}>
                                                                        <label class="custom-control-label"
                                                                               for="use_billing_address"></label>
                                                                    </div>
                                                                </h2>

                                                                <div name="show_billing_address" style="display:{{($customer->cu_use_billing_address ? "block;" : "none;")}}">

                                                                    <div class="form-group row">
                                                                        <div class="col-sm-1"></div>
                                                                        <label class="col-sm-3 col-form-label"
                                                                               for="street_1_bi">Company street
                                                                            1:</label>
                                                                        <div class="col-sm-7">
                                                                            <input type="text" class="form-control"
                                                                                   name="street_1_bi"
                                                                                   value="{{$customer->cu_street_1_bi}}">
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <div class="col-sm-1"></div>
                                                                        <label class="col-sm-3 col-form-label"
                                                                               for="street_2_bi">Company street
                                                                            2:</label>
                                                                        <div class="col-sm-7">
                                                                            <input type="text" class="form-control"
                                                                                   name="street_2_bi"
                                                                                   value="{{$customer->cu_street_2_bi}}">
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <div class="col-sm-1"></div>
                                                                        <label class="col-sm-3 col-form-label"
                                                                               for="zipcode_bi">Company
                                                                            zipcode:</label>
                                                                        <div class="col-sm-7">
                                                                            <input type="text" class="form-control"
                                                                                   name="zipcode_bi"
                                                                                   value="{{$customer->cu_zipcode_bi}}">
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <div class="col-sm-1"></div>
                                                                        <label class="col-sm-3 col-form-label"
                                                                               for="city_bi">Company
                                                                            city:</label>
                                                                        <div class="col-sm-7">
                                                                            <input type="text" class="form-control"
                                                                                   name="city_bi"
                                                                                   value="{{$customer->cu_city_bi}}">
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <div class="col-sm-1"></div>
                                                                        <label class="col-sm-3 col-form-label"
                                                                               for="country_bi">Company
                                                                            country:</label>
                                                                        <div class="col-sm-7">
                                                                            <select type="text" class="form-control"
                                                                                    name="country_bi">
                                                                                <option value=""></option>
                                                                                @foreach($countries as $country)
                                                                                    <option value="{{$country->co_code}}" {{ ($customer->cu_co_code_bi == $country->co_code) ? 'selected':'' }}>{{$country->co_en}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <div class="col-sm-1"></div>
                                                                        <label class="col-sm-3 col-form-label"
                                                                               for="telephone_bi">Company telephone:</label>
                                                                        <div class="col-sm-7">
                                                                            <input type="tel" name="telephone_bi" class="form-control" value="{{$customer->cu_telephone_bi}}">
                                                                            <input type="hidden" id="int_telephone_bi" name="int_telephone_bi" value="">
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <div class="col-sm-1"></div>
                                                                        <label class="col-sm-3 col-form-label"
                                                                               for="website_bi">Company
                                                                            website:</label>
                                                                        <div class="col-sm-6">
                                                                            <input type="text" class="form-control"
                                                                                   name="website_bi"
                                                                                   value="{{$customer->cu_website_bi}}">
                                                                        </div>
                                                                        @if (!empty($customer->cu_website_bi))
                                                                            <div class="col-sm-1">
                                                                                <a target="_blank" href="{{$customer->cu_website_bi}}"><i class="fa fa-external-link-alt"></i></a>
                                                                            </div>
                                                                        @endif
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="col-sm-1"></div>
                                                                        <div class="form-group col-md-8">
                                                                            <button type="submit"
                                                                                    class="btn btn-primary">
                                                                                Update
                                                                            </button>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="block block-rounded block-bordered">
                                                <div class="block-content">
                                                    <form method="post"
                                                          action="{{action('ServiceProvidersController@update', $customer->cu_id)}}">
                                                        @csrf
                                                        <input name="_method" type="hidden" value="PATCH">
                                                        <input name="form_name" type="hidden" value="Finance">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <h2 class="content-heading pt-0">Finance</h2>

                                                                <div class="form-group row">
                                                                    <div class="col-sm-1"></div>
                                                                    <label class="col-sm-3 col-form-label"
                                                                           for="finance_debtor_number">Debtor number:</label>
                                                                    <div class="col-sm-7">
                                                                        <input type="text" class="form-control"
                                                                               name="finance_debtor_number"
                                                                               value="{{$customer->cu_debtor_number}}"
                                                                               disabled>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <div class="col-sm-1"></div>
                                                                    <label class="col-sm-3 col-form-label" for="credit_hold">Credit hold:</label>
                                                                    <div class="col-sm-7">
                                                                        <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                                                            <input type="checkbox" class="custom-control-input"
                                                                                   name="finance_credit_hold" {{($customer->cu_credit_hold ? "checked" : "")}}
                                                                                   disabled>
                                                                              <label class="custom-control-label"
                                                                               for="finance_credit_hold"></label>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <div class="col-sm-1"></div>
                                                                    <label class="col-sm-3 col-form-label"
                                                                           for="finance_payment_currency">Payment currency:</label>
                                                                    <div class="col-sm-7">
                                                                        <select type="text" class="form-control"
                                                                                name="finance_payment_currency" disabled>
                                                                            <option value=""></option>
                                                                            @foreach($paymentcurrencies as $pc)
                                                                                <option value="{{$pc->pacu_code}}" {{ ($customer->cu_pacu_code== $pc->pacu_code) ? 'selected':'' }}>{{$pc->pacu_name}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <div class="col-sm-1"></div>
                                                                    <label class="col-sm-3 col-form-label"
                                                                           for="finance_payment_method">Payment method:</label>
                                                                    <div class="col-sm-7">
                                                                        <select type="text" class="form-control"
                                                                                name="finance_payment_method" disabled>
                                                                            <option value=""></option>
                                                                            @foreach($paymentmethods as $id => $pm)
                                                                                <option value="{{$id}}" {{ ($customer->cu_payment_method == $id) ? 'selected':'' }}>{{$pm}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <div class="col-sm-1"></div>
                                                                    <label class="col-sm-3 col-form-label"
                                                                           for="finance_payment_term">Payment term:</label>
                                                                    <div class="col-sm-7">
                                                                        <input type="text" class="form-control"
                                                                               name="finance_payment_term"
                                                                               value="{{$customer->cu_payment_term}}"
                                                                               disabled>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <div class="col-sm-1"></div>
                                                                    <label class="col-sm-3 col-form-label"
                                                                           for="finance_credit_limit">Credit limit:</label>
                                                                    <div class="col-sm-7">
                                                                        <input type="text" class="form-control"
                                                                               name="finance_credit_limit"
                                                                               value="{{$customer->cu_credit_limit}}"
                                                                               disabled>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <div class="col-sm-1"></div>
                                                                    <label class="col-sm-3 col-form-label" for="finance_enforce_credit_limit">Enforce credit limit:</label>
                                                                    <div class="col-sm-7">
                                                                        <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                                                            <input type="checkbox" class="custom-control-input"
                                                                                   name="finance_enforce_credit_limit" {{($customer->cu_enforce_credit_limit ? "checked" : "")}}
                                                                                   disabled>
                                                                              <label class="custom-control-label"
                                                                               for="finance_enforce_credit_limit"></label>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <div class="col-sm-1"></div>
                                                                    <label class="col-sm-3 col-form-label"
                                                                           for="finance_vat_number">VAT number:</label>
                                                                    <div class="col-sm-7">
                                                                        <input type="text" class="form-control"
                                                                               name="finance_vat_number"
                                                                               value="{{$customer->cu_vat_number}}"
                                                                               disabled>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="btabs-alt-static-emails" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        <div class="block-content">
                                            <form method="post"
                                                  action="{{action('ServiceProvidersController@update', $customer->cu_id)}}">
                                                @csrf
                                                <input name="_method" type="hidden" value="PATCH">
                                                <input name="form_name" type="hidden" value="Emails">

                                                <h2 class="content-heading pt-0">Emails</h2>

                                                <div class="row">
                                                    <div class="col-md-6">

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-4 col-form-label"
                                                                   for="primary_contact_email">Primary contact email:</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control"
                                                                       name="primary_contact_email"
                                                                       disabled value="{{$customer->cu_attn_email}}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-4 col-form-label"
                                                                   for="cu_email">Leads email:</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control"
                                                                       name="cu_email"
                                                                       disabled value="{{$customer->cu_email}}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-4 col-form-label"
                                                                   for="billing_email">Billing email:</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control"
                                                                       name="billing_email"
                                                                       disabled value="{{$customer->cu_email_bi}}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-4 col-form-label"
                                                                   for="review_communication">Review communication:</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control"
                                                                       name="review_communication"
                                                                       disabled value="@if(!empty($moverdata->moda_review_email)){{$moverdata->moda_review_email}}@endif">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-4 col-form-label"
                                                                   for="load_exchange_communication">Load Exchange communication:</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control"
                                                                       name="load_exchange_communication"
                                                                       disabled value="@if(!empty($moverdata->moda_load_exchange_email)){{$moverdata->moda_load_exchange_email}}@endif">
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @can('customers - finance')
                            <div class="tab-pane" id="btabs-alt-static-finance" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        <div class="block-content">
                                            <form method="post" id="serviceproviders_finance"
                                                  action="{{action('ServiceProvidersController@update', $customer->cu_id)}}">
                                                @csrf
                                                <input name="_method" type="hidden" value="PATCH">
                                                <input name="form_name" type="hidden" value="Finance">

                                                <h2 class="content-heading pt-0">Finance</h2>

                                                <div class="row">
                                                    <div class="col-md-6">

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="debtor_number">Debtor
                                                                number:</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control"
                                                                       name="debtor_number"
                                                                       disabled value="{{$customer->cu_debtor_number}}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="sales_manager">Sales
                                                                Manager:</label>
                                                            <div class="col-sm-7">
                                                                <select type="text" class="form-control"
                                                                        name="sales_manager">
                                                                    <option value=""></option>
                                                                    @foreach($users as $user)
                                                                        <option value="{{$user->id}}" {{ ($customer->cu_sales_manager == $user->id) ? 'selected':'' }}>{{$user->us_name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="account_manager">Account
                                                                Manager:</label>
                                                            <div class="col-sm-7">
                                                                <select type="text" class="form-control"
                                                                        name="account_manager">
                                                                    <option value=""></option>
                                                                    @foreach($users as $user)
                                                                        <option value="{{$user->id}}" {{ ($customer->cu_account_manager == $user->id) ? 'selected':'' }}>{{$user->us_name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="debt_manager">Debt
                                                                Manager:</label>
                                                            <div class="col-sm-7">
                                                                <select type="text" class="form-control"
                                                                        name="debt_manager">
                                                                    <option value=""></option>
                                                                    @foreach($users as $user)
                                                                        <option value="{{$user->id}}" {{ ($customer->cu_debt_manager == $user->id) ? 'selected':'' }}>{{$user->us_name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="debtor_status">Debtor
                                                                status:</label>
                                                            <div class="col-sm-7">
                                                                <select type="text" class="form-control"
                                                                        name="debtor_status">
                                                                    @foreach($debtorstatuses as $id => $item)
                                                                        <option value="{{$id}}" {{ ($customer->cu_debtor_status == $id) ? 'selected':'' }}>{{$item}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label" for="finance_lock">Finance lock:</label>
                                                            <div class="col-sm-7">
                                                                <div
                                                                    class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                                                    <input id="finance_lock" type="checkbox" class="custom-control-input"
                                                                           name="finance_lock" {{($customer->cu_finance_lock ? "checked" : "")}}>
                                                                    <label class="custom-control-label"
                                                                           for="finance_lock"></label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label" for="credit_hold">Credit hold:</label>
                                                            <div class="col-sm-7">
                                                                <div
                                                                    class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                                                    <input id="credit_hold" type="checkbox" class="custom-control-input"
                                                                           name="credit_hold" {{($customer->cu_credit_hold ? "checked" : "")}}>
                                                                    <label class="custom-control-label"
                                                                           for="credit_hold">@if($customer->cu_credit_hold_timestamp != "0000-00-00 00:00:00" && $customer->cu_credit_hold == 1) {{$customer->cu_credit_hold_timestamp}} @endif</label>
                                                                    <input id="credit_hold_hidden" type="hidden"
                                                                        name="credit_hold_hidden" value="{{$customer->cu_credit_hold}}">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row" id="debt_collector_div">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label" for="debt_collector">Debt collector:</label>
                                                            <div class="col-sm-7">
                                                                <div
                                                                    class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                                                    <input id="debt_collector" type="checkbox" class="custom-control-input"
                                                                           name="debt_collector" {{($customer->cu_debt_collector ? "checked" : "")}}>
                                                                    <label class="custom-control-label"
                                                                           for="debt_collector"></label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="payment_reminder_status">Payment
                                                                reminder status:</label>
                                                            <div class="col-sm-7">
                                                                <select type="text" class="form-control"
                                                                        name="payment_reminder_status">
                                                                    @foreach($paymentreminderstatuses as $id => $item)
                                                                        <option value="{{$id}}" {{ ($customer->cu_payment_reminder_status == $id) ? 'selected':'' }}>{{$item}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="payment_method">Payment
                                                                method:</label>
                                                            <div class="col-sm-7">
                                                                <select type="text" class="form-control"
                                                                        name="payment_method">
                                                                    @foreach($paymentmethods as $id => $item)
                                                                        <option value="{{$id}}" {{ ($customer->cu_payment_method == $id) ? 'selected':'' }}>{{$item}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="payment_currency">Payment
                                                                currency:</label>
                                                            <div class="col-sm-7">
                                                                <select type="text" class="form-control"
                                                                        name="payment_currency">
                                                                    @foreach($paymentcurrencies as $pc)
                                                                        <option value="{{$pc->pacu_code}}" {{ ($customer->cu_pacu_code == $pc->pacu_code) ? 'selected':'' }}>{{$pc->pacu_name." (".$pc->pacu_token.")"}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="invoice_period">Invoice period:</label>
                                                            <div class="col-sm-7">
                                                                <select type="text" class="form-control"
                                                                        name="invoice_period">
                                                                    @foreach($invoiceperiods as $id => $period)
                                                                        <option value="{{$id}}" {{ ($customer->cu_invoice_period == $id) ? 'selected':'' }}>{{$period}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="payment_term">Payment
                                                                term:</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control"
                                                                       name="payment_term"
                                                                       value="{{$customer->cu_payment_term}}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="credit_limit">Credit
                                                                limit:</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control"
                                                                       name="credit_limit"
                                                                       disabled value="{{$customer->cu_credit_limit}}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="ledger_account">Ledger
                                                                account:</label>
                                                            <div class="col-sm-7">
                                                                <select type="text" class="form-control"
                                                                        name="ledger_account">
                                                                    @foreach($ledgeraccounts as $la)
                                                                        <option value="{{$la->leac_number}}" {{ ($customer->cu_leac_number == $la->leac_number) ? 'selected':'' }}>{{$la->leac_number." (".$la->leac_name.")"}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="vat_number">VAT
                                                                number:</label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control"
                                                                       name="vat_number"
                                                                       value="{{$customer->cu_vat_number}}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-1"></div>
                                                            <label class="col-sm-3 col-form-label"
                                                                   for="finance_remark">Remark:</label>
                                                            <div class="col-sm-7">
                                                                <textarea type="text" class="form-control"
                                                                          name="finance_remark">{{$customer->cu_finance_remarks}}
                                                                </textarea>
                                                            </div>
                                                        </div>


                                                        <div class="row">
                                                            <div class="col-sm-1"></div>
                                                            <div class="form-group col-md-8">
                                                                <button type="submit" class="btn btn-primary">Update
                                                                </button>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="block block-rounded block-bordered">
                                        <div class="block-content">


                                            <h2 class="content-heading pt-0">Credit Cards</h2>

                                            <div class="row">
                                                <div class="col-md-12">

                                                    <div class="form-group row">
                                                        <div class="col-sm-1"></div>
                                                        <label class="col-sm-3 col-form-label"
                                                               for="card_last_digits">Active card digits</label>
                                                        <div class="col-sm-7">
                                                            <input type="text" class="form-control"
                                                                   name="card_last_digits"
                                                                   disabled
                                                                   value="{{ (!sizeof($creditcards) ? "-" : $creditcards[0]->adcade_card_last_digits)}}">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <div class="col-sm-1"></div>
                                                        <label class="col-sm-3 col-form-label"
                                                               for="active_card_expiry">Active card expiry</label>
                                                        <div class="col-sm-7">
                                                            <input type="text" class="form-control"
                                                                   name="active_card_expiry"
                                                                   disabled
                                                                   value="{{ (!sizeof($creditcards) ? "-" :  $creditcards[0]->adcade_card_expiry_year."/".$creditcards[0]->adcade_card_expiry_month)}}">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <div class="col-sm-1"></div>
                                                        <label class="col-sm-3 col-form-label"
                                                               for="associated_cards">Associated cards</label>
                                                        <div class="col-sm-7">
                                                            <input type="text" class="form-control"
                                                                   name="associated_cards"
                                                                   disabled value="{{sizeof($creditcards)}}">
                                                        </div>
                                                    </div>

                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endcan
                        <div class="tab-pane" id="btabs-alt-static-contactpersons" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        <div class="block-header block-header-default">
                                            <h3 class="block-title">
                                                <a href="{{ url('serviceproviders/' . $customer->cu_id . '/contactperson/create')}}">
                                                    <button class="btn btn-primary">Add a contact person</button>
                                                </a>
                                            </h3>
                                        </div>
                                        <div class="block-content">
                                            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                                <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Full name</th>
                                                    <th>Department</th>
                                                    <th>Role</th>
                                                    <th>Email</th>
                                                    <th>Telephone</th>
                                                    <th>Mobile</th>
                                                    <th>Language</th>
                                                    <th>Login</th>
                                                    <th>Actions</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach ($customer->contactpersons as $contactperson)
                                                    @if ($contactperson->cope_deleted)
                                                        @continue;
                                                    @endif
                                                    <tr>
                                                        <td>{{$contactperson->cope_id}}</td>
                                                        <td>{{$contactperson->cope_full_name}}</td>
                                                        <td>{{$contactpersondepartments[$contactperson->cope_department]}}</td>
                                                        <td>{{$contactpersondepartmentroles[$contactperson->cope_department_role]}}</td>
                                                        <td>{{$contactperson->cope_email}}</td>
                                                        <td>{{$contactperson->cope_telephone}}</td>
                                                        <td>{{$contactperson->cope_mobile}}</td>
                                                        <td>
                                                        @foreach($languages as $language)
                                                            @if($language->la_code == $contactperson->cope_language)
                                                                {{$language->la_language}}
                                                                @break
                                                            @endif
                                                        @endforeach
                                                        </td>

                                                        @if(empty($contactperson->application_user))
                                                            <td><i style="color:red" class="fa fa-times"></i></td>
                                                        @else
                                                            <td><a target="_blank" href="{{$contactperson->login_url}}"><i
                                                                            class="fa fa-key text-primary"></i></a></td>
                                                        @endif
                                                        <td class="text-center">
                                                            <div class="btn-group">
                                                                <a class="btn btn-sm btn-primary" data-toggle="tooltip"
                                                                   data-placement="left"
                                                                   title="edit"
                                                                   href="{{ url('serviceproviders/' . $customer->cu_id . '/contactperson/' . $contactperson->cope_id.'/edit')}}">
                                                                    <i class="fa fa-pencil-alt"></i>
                                                                </a>
                                                            </div>
                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-sm btn-primary contactperson_delete" data-toggle="tooltip" title="Delete contact person" data-contactperson_id="{{$contactperson->cope_id}}">
                                                                    <i class="fa fa-times"></i>
                                                                </button>

                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="btabs-alt-static-status" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        <div class="block-header block-header-default">
                                            <h3 class="block-title">
                                                <a href="{{ url('serviceproviders/' . $customer->cu_id . '/customerstatus/create')}}">
                                                    <button class="btn btn-primary">Add a status</button>
                                                </a>
                                            </h3>
                                        </div>
                                        <div class="block-content block-content-full">
                                            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                                <thead>
                                                <tr>
                                                    <th>DATA</th>
                                                    <th>ID</th>
                                                    <th>Date</th>
                                                    <th>Employee</th>
                                                    <th>Status</th>
                                                    <th>Reason</th>
                                                    <th>Turnover Netto</th>
                                                    <th>Remark</th>
                                                    <th>Delete</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach ($statuses as $status)
                                                    <tr>
                                                        {{--<td>
                                                            <button type="button"
                                                                    data-details="<tr> <td><b>Number of leads:</b></td> <td>2</td> </tr> <tr> <td><b>Capping of leads:</b></td> <td>3</td> </tr> <tr> <td><b>Price per lead:</b></td> <td>4</td> </tr> <tr> <td><b>Booking source:</b></td> <td>Reactivation project</td> </tr> <tr> <td><b>Number of leads:</b></td> <td>1</td> </tr> <tr> <td><b>Capping of leads:</b></td> <td>2</td> </tr> <tr> <td><b>Price per lead:</b></td> <td>3</td> </tr> <tr> <td><b>Booking source:</b></td> <td>Sales inbox</td> </tr> <tr> <td><b>Number of leads:</b></td> <td>2</td> </tr> <tr> <td><b>Capping of leads:</b></td> <td>3</td> </tr> <tr> <td><b>Price per lead:</b></td> <td>4</td> </tr> <tr> <td><b>Previous amount of leads:</b></td> <td>5</td> </tr> <tr> <td><b>Previous amount of capping:</b></td> <td>6</td> </tr> <tr> <td><b>Previous price per lead:</b></td> <td>7</td> </tr> <tr> <td><b>Number of leads:</b></td> <td>130</td> </tr> <tr> <td><b>Capping of leads:</b></td> <td>200</td> </tr> <tr> <td><b>Price per lead:</b></td> <td>€12</td> </tr> <tr> <td><b>Reason:</b></td> <td>Bankruptcy</td> </tr> <tr> <td><b>Lifetime (in months):</b></td> <td>1</td> </tr> <tr> <td><b>Number of leads:</b></td> <td>40</td> </tr> <tr> <td><b>Capping of leads:</b></td> <td>50</td> </tr> <tr> <td><b>Price per lead:</b></td> <td>€10</td> </tr> <tr> <td><b>Booking source:</b></td> <td>Sirelo</td> </tr> <tr> <td><b>Number of leads:</b></td> <td>2</td> </tr> <tr> <td><b>Capping of leads:</b></td> <td>200</td> </tr> <tr> <td><b>Price per lead:</b></td> <td>®5</td> </tr> <tr> <td><b>Previous amount of leads:</b></td> <td>1</td> </tr> <tr> <td><b>Previous amount of capping:</b></td> <td>100</td> </tr> <tr> <td><b>Previous price per lead:</b></td> <td>€5</td> </tr> <tr> <td><b>Number of leads:</b></td> <td>5</td> </tr> <tr> <td><b>Capping of leads:</b></td> <td>5</td> </tr> <tr> <td><b>Price per lead:</b></td> <td>5</td> </tr> <tr> <td><b>Reason:</b></td> <td>Bankruptcy</td> </tr> <tr> <td><b>Lifetime (in months):</b></td> <td>5</td> </tr> <tr> <td><b>Number of leads:</b></td> <td></td> </tr> <tr> <td><b>Capping of leads:</b></td> <td></td> </tr> <tr> <td><b>Price per lead:</b></td> <td></td> </tr> <tr> <td><b>Booking source:</b></td> <td></td> </tr> <tr> <td><b>Number of leads:</b></td> <td></td> </tr> <tr> <td><b>Capping of leads:</b></td> <td></td> </tr> <tr> <td><b>Price per lead:</b></td> <td></td> </tr> <tr> <td><b>Previous amount of leads:</b></td> <td></td> </tr> <tr> <td><b>Previous amount of capping:</b></td> <td></td> </tr> <tr> <td><b>Previous price per lead:</b></td> <td></td> </tr> <tr> <td><b>Number of leads:</b></td> <td></td> </tr> <tr> <td><b>Capping of leads:</b></td> <td></td> </tr> <tr> <td><b>Price per lead:</b></td> <td></td> </tr> <tr> <td><b>Booking source:</b></td> <td></td> </tr>"
                                                                    class="details_show_blank">&nbsp;
                                                            </button>
                                                        </td>--}}
                                                        <td class="text-center">
                                                            <button type="button" class="btn btn-sm btn-primary details_show_blank" data-toggle="tooltip" title="Expand"
                                                                data-details="<tr> <td><b>Number of leads:</b></td> <td>2</td> </tr> <tr> <td><b>Capping of leads:</b></td> <td>3</td> </tr> <tr> <td><b>Price per lead:</b></td> <td>4</td> </tr> <tr> <td><b>Booking source:</b></td> <td>Reactivation project</td> </tr>">
                                                                <i class="fa fa-plus"></i>
                                                            </button>
                                                        </td>

                                                        <td>{{$status->cust_id}}</td>
                                                        <td>{{$status->cust_date}}</td>
                                                        <td>{{isset($status->user) ? $status->user->us_name : 'Unknown'}}</td>
                                                        <td>{{isset($status->cust_status) ? $customerstatusstatuses[$status->cust_status] : ''}}</td>
                                                        <td>{{isset($status->cust_reason) ? $customerstatusreasons[$status->cust_reason] : ''}}</td>
                                                        <td>{{$status->cust_turnover_netto}}</td>
                                                        <td>{{$status->cust_remark}}</td>


                                                        <td class="text-center">
                                                            <button type="button" class="btn btn-sm btn-primary status_delete" data-toggle="tooltip" title="Delete" data-status_id="{{$status->cust_id}}">
                                                                <i class="fa fa-times"></i>
                                                            </button>
                                                        </td>

                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="btabs-alt-static-remark" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        <div class="block-header block-header-default">
                                            <h3 class="block-title">
                                                <a href="{{ url('serviceproviders/' . $customer->cu_id . '/customerremark/create')}}">
                                                    <button class="btn btn-primary">Add a remark</button>
                                                </a>

                                            </h3>
                                        </div>
                                        <div class="block-content block-content-full">
                                            <table data-order='[[1, "desc"]]' class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                                <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Timestamp</th>
                                                    <th>Contact date</th>
                                                    <th>Department</th>
                                                    <th>Employee</th>
                                                    <th>Medium</th>
                                                    <th>Direction</th>
                                                    <th>Remark</th>
                                                    <th>Delete</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach ($remarks as $remark)
                                                    <tr>
                                                        <td>{{$remark->cure_id}}</td>
                                                        <td>{{$remark->cure_timestamp}}</td>
                                                        <td>{{$remark->cure_contact_date}}</td>
                                                        <td>{{isset($remark->cure_department) ? $customerremarkdepartments[$remark->cure_department] : ''}}</td>
                                                        <td>{{isset($remark->user) ? $remark->user->us_name : 'Unknown'}}</td>
                                                        <td>{{isset($remark->cure_medium) ? $customerremarkmediums[$remark->cure_medium] : ''}}</td>
                                                        <td>{{isset($remark->cure_direction) ? $customerremarkdirections[$remark->cure_direction] : ''}}</td>
                                                        @if($remark->cure_status_id)
                                                            <td>
                                                                <table>
                                                                    <tbody>
                                                                    <tr>
                                                                        <th colspan="4">
                                                                            <strong>Status update</strong>
                                                                        </th>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Status
                                                                        </td>
                                                                        <td>
                                                                            Reason
                                                                        </td>
                                                                        <td>
                                                                            Netto turnover
                                                                        </td>
                                                                        <td>
                                                                            Remark
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Upsell
                                                                        </td>
                                                                        <td>
                                                                            Other
                                                                        </td>
                                                                        <td>
                                                                            {{$remark->status->cust_turnover_netto}}
                                                                        </td>
                                                                        <td>
                                                                            {{$remark->status->cust_remark}}
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        @else
                                                            <td>{!! $remark->cure_text !!}</td>
                                                        @endif
                                                        @if(!$remark->cure_status_id)
                                                            <td class="text-center">
                                                                <button type="button" class="btn btn-sm btn-primary remark_delete" data-toggle="tooltip" title="Delete" data-remark_id="{{$remark->cure_id}}">
                                                                    <i class="fa fa-times"></i>
                                                                </button>
                                                            </td>
                                                        @else
                                                            <td></td>
                                                        @endif
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @can('customers - documents')
                            <div class="tab-pane" id="btabs-alt-static-documents" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        <div class="block-header block-header-default">
                                            <h3 class="block-title">
                                                <a href="{{ url('serviceproviders/' . $customer->cu_id . '/customerdocument/create')}}">
                                                    <button class="btn btn-primary">Add a document</button>
                                                </a>

                                            </h3>
                                        </div>
                                        <div class="block-content block-content-full">
                                            <ul class="nav nav-tabs nav-tabs-alt" data-toggle="tabs" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" href="#btabs-alt-static-current">Current</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#btabs-alt-static-archived">Archived</a>
                                                </li>
                                            </ul>
                                            <div class="block-content tab-content">
                                                <div class="tab-pane active" id="btabs-alt-static-current" role="tabpanel">
                                                    <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                                        <thead>
                                                        <tr>
                                                            <th>ID</th>
                                                            <th>Timestamp</th>
                                                            <th>Uploaded by</th>
                                                            <th>Filename</th>
                                                            <th>Type</th>
                                                            <th>Description</th>
                                                            <th>Document</th>
                                                            <th>Archive</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach ($documents as $document)
                                                            <tr>
                                                                <td>{{$document->cudo_id}}</td>
                                                                <td>{{$document->cudo_timestamp}}</td>
                                                                <td>{{isset($document->user) ? $document->user->us_name : 'Unknown'}}</td>
                                                                <td>{{$document->cudo_filename}}</td>
                                                                <td>{{isset($document->cudo_type) ? $customerdocumenttypes[$document->cudo_type] : ''}}</td>
                                                                <td>{{$document->cudo_description}}</td>

                                                                <td>
                                                                    <a target="_blank" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="left" title="Download" href="{{ url('serviceproviders/' . $customer->cu_id . '/document/'.$document['cudo_id'].'/download')}}">
                                                                        <i class="fa fa-download"></i>
                                                                    </a>
                                                                </td>
                                                                <td>
                                                                    <a class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="left" title="Archive" href="{{ url('serviceproviders/' . $customer->cu_id . '/document/'.$document['cudo_id'].'/archive')}}">
                                                                        <i class="fa fa-minus"></i>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>

                                                <div class="tab-pane" id="btabs-alt-static-archived" role="tabpanel">
                                                    <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                                        <thead>
                                                        <tr>
                                                            <th>ID</th>
                                                            <th>Timestamp</th>
                                                            <th>Uploaded by</th>
                                                            <th>Filename</th>
                                                            <th>Type</th>
                                                            <th>Description</th>
                                                            <th>Document</th>
                                                            <th>Archive</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach ($documents_archived as $document)
                                                            <tr>
                                                                <td>{{$document->cudo_id}}</td>
                                                                <td>{{$document->cudo_timestamp}}</td>
                                                                <td>{{isset($document->user) ? $document->user->us_name : 'Unknown'}}</td>
                                                                <td>{{$document->cudo_filename}}</td>
                                                                <td>{{isset($document->cudo_type) ? $customerdocumenttypes[$document->cudo_type] : ''}}</td>
                                                                <td>{{$document->cudo_description}}</td>

                                                                <td>
                                                                    <a target="_blank" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="left" title="Download" href="{{ url('serviceproviders/' . $customer->cu_id . '/document/'.$document['cudo_id'].'/download')}}">
                                                                        <i class="fa fa-download"></i>
                                                                    </a>
                                                                </td>
                                                                <td>
                                                                    <a class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="left" title="Recover" href="{{ url('serviceproviders/' . $customer->cu_id . '/document/'.$document['cudo_id'].'/recover')}}">
                                                                        <i class="fa fa-redo"></i>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endcan
                        <div class="tab-pane" id="btabs-alt-static-offices" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        <div class="block-header block-header-default">
                                            <h3 class="block-title">
                                                <a href="{{ url('serviceproviders/' . $customer->cu_id . '/customeroffice/create')}}">
                                                    <button class="btn btn-primary">Add an office</button>
                                                </a>

                                            </h3>
                                        </div>
                                        <div class="block-content block-content-full">
                                            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                                <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Street 1</th>
                                                    <th>Street 2</th>
                                                    <th>City</th>
                                                    <th>Zipcode</th>
                                                    <th>State</th>
                                                    <th>Country</th>
                                                    <th>Email</th>
                                                    <th>Telephone</th>
                                                    <th>Website</th>
                                                    <th>Actions</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach ($offices as $office)
                                                    <tr>
                                                        <td>{{$office->cuof_id}}</td>
                                                        <td>{{$office->cuof_street_1}}</td>
                                                        <td>{{$office->cuof_street_2}}</td>
                                                        <td>{{$office->cuof_city}}</td>
                                                        <td>{{$office->cuof_zipcode}}</td>
                                                        <td>{{$office->cuof_state}}</td>
                                                        <td>{{$office->cuof_co_code}}</td>
                                                        <td>{{$office->cuof_email}}</td>
                                                        <td>{{$office->cuof_telephone}}</td>
                                                        <td>{{$office->cuof_website}}</td>
                                                        <td class="text-center">
                                                            <div class="btn-group">
                                                                <a class="btn btn-sm btn-primary" data-toggle="tooltip"
                                                                   data-placement="left"
                                                                   title="edit"
                                                                   href="{{ url('serviceproviders/' . $customer->cu_id . '/customeroffice/' . $office->cuof_id.'/edit')}}">
                                                                    <i class="fa fa-pencil-alt"></i>
                                                                </a>
                                                            </div>
                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-sm btn-primary office_delete" data-toggle="tooltip" title="Delete" data-office_id="{{$office->cuof_id}}">
                                                                    <i class="fa fa-times"></i>
                                                                </button>

                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="btabs-alt-static-questions" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        <div class="block-header block-header-default">
                                            <h3 class="block-title">
                                                <a href="{{ url('serviceproviders/' . $customer->cu_id . '/question/create')}}">
                                                    <button class="btn btn-primary">Add a question</button>
                                                </a>

                                            </h3>
                                        </div>
                                        <div class="block-content block-content-full">
                                            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                                <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Question</th>
                                                    <th>Description</th>
                                                    <th>Status</th>
                                                    <th>Actions</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach ($questions as $question)
                                                    <tr>
                                                        <td>{{$question->ktcuqu_id}}</td>
                                                        <td>{{$question->questions->qu_name}}</td>
                                                        <td>{{$question->ktcuqu_description}}</td>
                                                        <td>{{$questionstatuses[$question->ktcuqu_status]}}</td>
                                                        <td class="text-center">
                                                            <div class="btn-group">
                                                                <a class="btn btn-sm btn-primary" data-toggle="tooltip"
                                                                   data-placement="left"
                                                                   title="edit"
                                                                   href="{{ url('serviceproviders/' . $customer->cu_id . '/question/' . $question->ktcuqu_id.'/edit')}}">
                                                                    <i class="fa fa-pencil-alt"></i>
                                                                </a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="btabs-alt-static-newsletter_blocks" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        <div class="block-header block-header-default">
                                            <h3 class="block-title">
                                                <a href="{{ url('serviceproviders/' . $customer->cu_id . '/newsletterblock/create')}}">
                                                    <button class="btn btn-primary">Add a newsletter block</button>
                                                </a>

                                            </h3>
                                        </div>
                                        <div class="block-content block-content-full">
                                            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                                <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Slot</th>
                                                    <th>Description</th>
                                                    <th>Status</th>
                                                    <th>Actions</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach ($newsletterblocks as $newsletter_block)
                                                    <tr>
                                                        <td>{{$newsletter_block->seprnebl_id}}</td>
                                                        <td>{{$newsletterslots[$newsletter_block->seprnebl_slot]}}</td>
                                                        <td>{{$newsletter_block->seprnebl_description}}</td>
                                                        <td>{{$questionstatuses[$newsletter_block->seprnebl_status]}}</td>
                                                        <td class="text-center">
                                                            <div class="btn-group">
                                                                <a class="btn btn-sm btn-primary" data-toggle="tooltip"
                                                                   data-placement="left"
                                                                   title="edit"
                                                                   href="{{ url('serviceproviders/' . $customer->cu_id . '/newsletterblock/' . $newsletter_block->seprnebl_id.'/edit')}}">
                                                                    <i class="fa fa-pencil-alt"></i>
                                                                </a>
                                                            </div>
                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-sm btn-primary newsletterblock_delete" data-toggle="tooltip" title="Delete newsletter block" data-newsletterblock_id="{{$newsletter_block->seprnebl_id}}">
                                                                    <i class="fa fa-times"></i>
                                                                </button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="btabs-alt-static-sirelo_advertising" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        <div class="block-header block-header-default">
                                            <h3 class="block-title">
                                                <a href="{{ url('serviceproviders/' . $customer->cu_id . '/advertorialblock/create')}}">
                                                    <button class="btn btn-primary">Add advertorial</button>
                                                </a>

                                            </h3>
                                        </div>
                                        <div class="block-content block-content-full">
                                            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                                <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Slot</th>
                                                    <th>Description</th>
                                                    <th>Status</th>
                                                    <th>Actions</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach ($advertorialblocks as $advertorial_block)
                                                    <tr>
                                                        <td>{{$advertorial_block->sepradbl_id}}</td>
                                                        <td>{{$newsletterslots[$advertorial_block->sepradbl_slot]}}</td>
                                                        <td>{{$advertorial_block->sepradbl_description}}</td>
                                                        <td>{{$questionstatuses[$advertorial_block->sepradbl_status]}}</td>
                                                        <td class="text-center">
                                                            <div class="btn-group">
                                                                <a class="btn btn-sm btn-primary" data-toggle="tooltip"
                                                                   data-placement="left"
                                                                   title="edit"
                                                                   href="{{ url('serviceproviders/' . $customer->cu_id . '/advertorialblock/' . $advertorial_block->sepradbl_id.'/edit')}}">
                                                                    <i class="fa fa-pencil-alt"></i>
                                                                </a>
                                                            </div>
                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-sm btn-primary advertorialblock_delete" data-toggle="tooltip" title="Delete advertorial block" data-advertorialblock_id="{{$advertorial_block->sepradbl_id}}">
                                                                    <i class="fa fa-times"></i>
                                                                </button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @can('requests')
                            <div class="tab-pane" id="btabs-alt-static-requests" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">

                                        <form class="mb-5 mt-5" method="post" action="{{action('ServiceProvidersController@getRequests', $customer->cu_id)}}">
                                            @csrf
                                            <input name="_method" type="hidden" value="post">

                                            <div class="form-group row">
                                                <label class="col-md-1 col-form-label ml-5" for="for-date">Date range:</label>
                                                <div class="col-md-2">
                                                    <input type="text" class="form-control drp drp-default" name="date" @if($request_date_filter != null) value="{{$request_date_filter}}" @endif autocomplete="off"/>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-1"></div>
                                                <div class="form-group col-md-8">
                                                    <button type="submit" class="btn btn-primary">Filter</button>
                                                </div>
                                            </div>
                                        </form>

                                        @if (!empty($customerrequests))
                                            <div class="block-content block-content-full">
                                                <table id="questionstable"  class="table table-bordered table-striped table-vcenter">
                                                    <thead>
                                                    <tr>
                                                        <th>ID</th>
                                                        <th>Customer ID</th>
                                                        <th>Submitted</th>
                                                        <th>Sent</th>
                                                        <th>Sent</th>
                                                        <th>Question</th>
                                                        <th>Answer 1</th>
                                                        <th>Answer 2</th>
                                                        <th>Status</th>
                                                        <th>Name</th>
                                                        <th>Email</th>
                                                        <th>Origin</th>
                                                        <th>Destination</th>
                                                        <th>Free</th>
                                                        <th>Invoiced</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach ($customerrequests as $request)
                                                        <tr>
                                                            <td>{{$request->ktrecuqu_id}}</td>
                                                            <td>{{$request->ktrecuqu_cu_re_id}}</td>
                                                            <td>{{$request->ktrecuqu_timestamp}}</td>
                                                            <td>{{$yesno[$request->ktrecuqu_sent]}}</td>
                                                            <td>{{$request->ktrecuqu_sent_timestamp}}</td>
                                                            <td>{{$request->question->qu_name}}</td>
                                                            <td>{{$request->ktrecuqu_answer_1}}</td>
                                                            <td>{{$request->ktrecuqu_answer_2}}</td>
                                                            <td>{{$requeststatus[$request->request->re_status]}}</td>
                                                            <td>{{$request->request->re_full_name}}</td>
                                                            <td>{{$request->request->re_email}}</td>
                                                            <td>{{$request->request->countryfrom->co_en}}</td>
                                                            <td>{{$request->request->countryto->co_en}}</td>
                                                            <td>{{$yesno[$request->ktrecuqu_free]}}</td>
                                                            <td>{{$yesno[$request->ktrecuqu_invoiced]}}</td>
                                                            <td class="text-center">
                                                                <div class="btn-group">
                                                                    <a class="btn btn-sm @if($request->ktrecuqu_free) btn-success @else btn-danger @endif mr-1"
                                                                       data-toggle="tooltip"
                                                                       data-placement="left"
                                                                       title="Free"
                                                                       href="{{ url('customerquestion/'.$request->ktrecuqu_id.'/free')}}">
                                                                        <i class="fa fa-coins"></i>
                                                                    </a>
                                                                </div>
                                                                <div class="btn-group">
                                                                    <a class="btn btn-sm btn-primary" data-toggle="tooltip"
                                                                       data-placement="left"
                                                                       title="Resend"
                                                                       href="{{ url('customerquestion/'.$request->ktrecuqu_id.'/resend')}}">
                                                                        <i class="fa fa-envelope"></i>
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endcan
                        @can('customers - invoices')
                            <div class="tab-pane" id="btabs-alt-static-invoices" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        <form class=" mt-5" method="post" action="{{action('ServiceProvidersController@getInvoices', $customer->cu_id)}}">
                                            @csrf
                                            <input name="_method" type="hidden" value="post">

                                            <div class="form-group row">
                                                <label class="col-sm-1 col-form-label ml-5"
                                                       for="invoice_period">Paid:</label>
                                                <div class="col-sm-2">
                                                    <select type="text" class="form-control"
                                                            name="paid">
                                                        <option @if(isset($invoices_paid_filter) && $invoices_paid_filter != null && $invoices_paid_filter == "both") selected @endif value="both">Both</option>
                                                        <option @if(isset($invoices_paid_filter) && $invoices_paid_filter != null && $invoices_paid_filter == "1") selected @endif  value="1">Yes</option>
                                                        <option @if(isset($invoices_paid_filter) && $invoices_paid_filter != null && $invoices_paid_filter == "0") selected @endif  value="0" @if($invoices_paid_filter == null) selected @endif>No</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-1"></div>
                                                <div class="form-group col-md-8">
                                                    <button type="submit" class="btn btn-primary">Filter</button>
                                                </div>
                                            </div>
                                        </form>
                                        @if($invoices_paid_filter != null)
                                            <div class="block-content block-content-full">
                                                <table class="table table-bordered table-striped table-vcenter  js-dataTable-full">
                                                    <thead>
                                                    <tr>
                                                        <th>ID</th>
                                                        <th>Timestamp</th>
                                                        <th>Invoice number</th>
                                                        <th>Inoice date</th>
                                                        <th>Expiration date</th>
                                                        <th>Days overdue</th>
                                                        <th>Reminder</th>
                                                        <th>Payment method</th>
                                                        <th>Amount excl. VAT €</th>
                                                        <th>Amount incl. VAT €</th>
                                                        <th>Amount excl. VAT FC</th>
                                                        <th>Amount incl. VAT FC</th>
                                                        <th>Paid</th>
                                                        <th>Amount paid</th>
                                                        <th>Amount left</th>
                                                        <th>Payment date(s)</th>
                                                        <th>View</th>
                                                        <th>Send</th>
                                                        <th>PDF</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach ($invoices as $invoice)
                                                        @if($invoices_paid_filter == "both" || ($invoices_paid_filter == "1" && ($invoice->in_amount_netto_eur == round($amount_paired_per_invoice[$invoice->in_id], 2))) || ($invoices_paid_filter == "0" && ($invoice->in_amount_netto_eur != round($amount_paired_per_invoice[$invoice->in_id], 2))))
                                                            <tr>
                                                                <td>{{$invoice->in_id}}</td>
                                                                <td>{{$invoice->in_timestamp}}</td>
                                                                <td>{{$invoice->in_number}}</td>
                                                                <td>{{$invoice->in_date}}</td>
                                                                <td>{{$invoice->in_expiration_date}}</td>
                                                                <td>@if ($invoice->in_amount_netto_eur != round($amount_paired_per_invoice[$invoice->in_id], 2) && floor((strtotime("Y-m-d") - strtotime($invoice->in_expiration_date)) / 3600 / 24) > 0) {{floor((strtotime("Y-m-d") - strtotime($invoice->in_expiration_date)) / 3600 / 24)}} @else {{"0"}} @endif</td>
                                                                <td>{{$paymentreminder[$invoice->in_payment_reminder]}}</td>
                                                                <td>{{$paymentmethods[$invoice->in_payment_method]}}</td>
                                                                <td class="align_right_column">{{$currency_tokens["EUR"]." ".$invoice->in_amount_gross_eur}}</td>
                                                                <td class="align_right_column">{{$currency_tokens["EUR"]." ".$invoice->in_amount_netto_eur}}</td>
                                                                <td class="align_right_column">{{$currency_tokens[$invoice->in_currency]." ".$invoice->in_amount_gross}}</td>
                                                                <td class="align_right_column">{{$currency_tokens[$invoice->in_currency]." ".$invoice->in_amount_netto}}</td>
                                                                <td>{{(($invoice->in_amount_netto_eur == round($amount_paired_per_invoice[$invoice->in_id], 2)) ? "Yes" : "No")}}</td>
                                                                <td class="align_right_column">{{$currency_tokens["EUR"]." ".$amount_paired_per_invoice[$invoice->in_id]}}</td>
                                                                <td class="align_right_column">{{$currency_tokens["EUR"]." "}}{{round($invoice->in_amount_netto_eur - $amount_paired_per_invoice[$invoice->in_id], 2)}}</td>
                                                                <td>@if(!empty($payment_dates_per_invoice[$invoice->in_id])) {{implode(", ", $payment_dates_per_invoice[$invoice->in_id])}} @else {{"-"}} @endif</td>
                                                                <td>
                                                                    <button type="button" class="btn btn-sm btn-primary invoice_view" data-toggle="tooltip"
                                                                            title="View" data-invoice_id="{{$invoice->in_id}}">
                                                                        <i class="fa fa-eye"></i>
                                                                    </button>
                                                                </td>
                                                                <td class="text-center">
                                                                    <div class="btn-group">
                                                                        <a class="btn btn-sm btn-primary" data-toggle="tooltip"
                                                                           data-placement="left"
                                                                           href="{{ url('customers/' . $customer->cu_id . '/invoice/' . $invoice->in_id.'/send_invoice')}}">
                                                                            <i class="fa fa-envelope"></i>
                                                                        </a>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <a class="btn btn-sm btn-primary" data-toggle="tooltip"
                                                                       data-placement="left"
                                                                       target="_blank"
                                                                       href="../../invoice_download.php?invoice={{$invoice->in_number}}&date={{$invoice->in_date}}&env={{env("SHARED_FOLDER")}}">
                                                                        <i class="fa fa-download"></i>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        @endif

                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endcan
                    </div>
                </div>
                <!-- END Block Tabs Alternative Style -->
            </div>
        </div>

    </div>

    <div id="viewInvoiceModal" class="modal fade" role="dialog" >
        <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content" style="width:700px;">
                <div class="modal-header">
                    <h5 class="modal-title">Viewing Invoice</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="invoice_view_iframe">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection


@push( 'scripts' )

    <script>
        jQuery(document).ready(function(){

            $('.align_right_column').css('text-align', 'right');

            firstInput();
            secondInput();

            function firstInput(){
                var input = document.querySelector("input[name=telephone]");
                output = $("#int_telephone");

                var iti = window.intlTelInput(input, {
                    nationalMode: true,
                    separateDialCode: true
                });

                var handleChange = function() {
                    var text = (iti.isValidNumber()) ? iti.getNumber() : 0;
                    if(text == 0){
                        $('input[name=telephone]').addClass("is-invalid");
                    }else{
                        $('input[name=telephone]').removeClass("is-invalid");
                    }
                    output.val(text);
                };

                input.addEventListener('change', handleChange);
                input.addEventListener('keyup', handleChange);

                handleChange();
            }

            function secondInput(){
                var input2 = document.querySelector("input[name=telephone_bi]");
                output2 = $("#int_telephone_bi");

                var iti2 = window.intlTelInput(input2, {
                    nationalMode: true,
                    separateDialCode: true
                });

                var handleChange = function() {
                    var text = (iti2.isValidNumber()) ? iti2.getNumber() : 0;
                    if(text == 0){
                        $('input[name=telephone_bi]').addClass("is-invalid");
                    }else{
                        $('input[name=telephone_bi]').removeClass("is-invalid");
                    }
                    output2.val(text);
                };

                input2.addEventListener('change', handleChange);
                input2.addEventListener('keyup', handleChange);

                handleChange();

            }

        	jQuery( document ).on( 'click', '.contactperson_delete', function(e) {
                e.preventDefault();

        		var $self = jQuery( this );

                confirmDelete("{{ url('ajax/customer/contactperson_delete') }}", 'get', {id:$self.data('contactperson_id')}, function() {

                    $self.parents( '.dataTable' ).DataTable().row( $self.parents( 'tr' ) ).remove().draw( 'page' );
                });
            });

        	jQuery( document ).on( 'click', '.status_delete', function(e) {
                e.preventDefault();

        		var $self = jQuery( this );

                confirmDelete("{{ url('ajax/customer/status_delete') }}", 'get', {id:$self.data('status_id')}, function() {

                    $self.parents( '.dataTable' ).DataTable().row( $self.parents( 'tr' ) ).remove().draw( 'page' );
                });
            });

        	jQuery( document ).on( 'click', '.remark_delete', function(e) {
                e.preventDefault();

        		var $self = jQuery( this );

                confirmDelete("{{ url('ajax/customer/remark_delete') }}", 'get', {id:$self.data('remark_id')}, function() {

                    $self.parents( '.dataTable' ).DataTable().row( $self.parents( 'tr' ) ).remove().draw( 'page' );
                });
            });

        	jQuery( document ).on( 'click', '.office_delete', function(e) {
                e.preventDefault();

        		var $self = jQuery( this );

                confirmDelete("{{ url('ajax/customer/office_delete') }}", 'get', {id:$self.data('office_id')}, function() {

                    $self.parents( '.dataTable' ).DataTable().row( $self.parents( 'tr' ) ).remove().draw( 'page' );
                });
            });

        	jQuery( document ).on( 'click', '.newsletterblock_delete', function(e) {
                e.preventDefault();

        		var $self = jQuery( this );

                confirmDelete("{{ url('ajax/customer/newsletterblock_delete') }}", 'get', {id:$self.data('newsletterblock_id')}, function() {
                    $self.parents( '.dataTable' ).DataTable().row( $self.parents( 'tr' ) ).remove().draw( 'page' );
                });
            });

        	jQuery( document ).on( 'click', '.advertorialblock_delete', function(e) {
                e.preventDefault();

        		var $self = jQuery( this );

                confirmDelete("{{ url('ajax/customer/advertorialblock_delete') }}", 'get', {id:$self.data('advertorialblock_id')}, function() {
                    $self.parents( '.dataTable' ).DataTable().row( $self.parents( 'tr' ) ).remove().draw( 'page' );
                });
            });

        	jQuery( document ).on( 'click', '.invoice_view', function(e) {
                e.preventDefault();

        		var $self = jQuery( this );

        		jQuery.ajax({
                    url: "/ajax/invoice/view",
                    method: 'get',
                    data: { invoice_id : $self.data('invoice_id') },
                    success: function( $result) {
                    	$("#invoice_view_iframe").html('');
                    	$("#invoice_view_iframe").append($result);

                        $("#viewInvoiceModal").modal("toggle");
                    }
                });
            });
        });

        $('#questionstable').DataTable({
            hideEmptyCols: true
        });

    </script>

@endpush
