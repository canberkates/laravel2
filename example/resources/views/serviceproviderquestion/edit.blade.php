@extends('layouts.backend')
@include( 'scripts.dialogs' )
@pushonce( 'scripts:serviceprovider-question-edit' )
    <script src="{{ asset('/js/custom/serviceprovider/serviceprovider_question_edit.js') }}"></script>
@endpushonce
@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Edit question of {{$customer->cu_company_name_business}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../../../">Service Providers</a></li>
                        <li class="breadcrumb-item"><a href="../../edit">{{$customer->cu_company_name_business}}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Question edit</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="block block-rounded block-bordered">
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    {{$error}}<br>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <ul class="nav nav-tabs nav-tabs-alt" data-toggle="tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" href="#btabs-alt-static-general">General</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-request_deliveries">Request deliveries</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-origins">Origins</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-destinations">Destinations</a>
                        </li>
                    </ul>

                    <div class="block-content tab-content">
                        <div class="tab-pane active" id="btabs-alt-static-general" role="tabpanel">
                            <form class="mb-5" method="post" action="{{action('ServiceProviderQuestionsController@update', $question_id)}}">
                                @csrf
                                <input name="_method" type="hidden" value="PATCH">
                                <input name="ktcuqu_id" type="hidden" value="{{$question->ktcuqu_id}}">
                                <input name="form_name" type="hidden" value="General">

                                <h2 class="content-heading pt-0">General</h2>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="question">Question:</label>
                                    <div class="col-sm-7">
                                        <select type="text" class="form-control" name="question" disabled>
                                            <option value=""></option>
                                            @foreach($questions as $qu)
                                                <option @if ($qu->qu_id == $question->ktcuqu_qu_id) selected @endif value="{{$qu->qu_id}}">{{$qu->qu_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="description">Description:</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="description"
                                               value="{{$question->ktcuqu_description}}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="status">Status:</label>
                                    <div class="col-sm-7">
                                        <select type="text" class="form-control" name="status">
                                            @foreach($questionstatuses as $id => $qs)
                                                <option @if ($id == $question->ktcuqu_status) selected @endif value="{{$id}}">{{$qs}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="delay">Delay:</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="delay"
                                               value="{{$question->ktcuqu_delay}}">
                                    </div>
                                </div>

                                <h2 class="content-heading pt-0">Rate</h2>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="rate">Rate:</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="rate"
                                               value="{{$question->ktcuqu_rate}}">
                                    </div>
                                </div>

                                <h2 class="content-heading pt-0">Filters - Moves</h2>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="international_moves">International moves:</label>
                                    <div class="col-sm-7">
                                        <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                            <input type="checkbox" class="custom-control-input" id="international_moves"
                                                   name="international_moves" {{($question->ktcuqu_international_moves ? "checked" : "")}}>
                                              <label class="custom-control-label"
                                               for="international_moves"></label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="national_moves">National moves:</label>
                                    <div class="col-sm-7">
                                        <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                            <input type="checkbox" class="custom-control-input" id="national_moves"
                                                   name="national_moves" {{($question->ktcuqu_national_moves ? "checked" : "")}}>
                                              <label class="custom-control-label"
                                               for="national_moves"></label>
                                        </div>
                                    </div>
                                </div>

                                <h2 class="content-heading pt-0">Filters - Languages</h2>

                                <div id="languages">
                                    <button type="button" id="select_all" class="btn btn-sm btn-primary" data-toggle="tooltip">Select All</button>
                                    <button type="button" id="deselect_all" class="btn btn-sm btn-primary" data-toggle="tooltip">Deselect All</button>

                                    @foreach($languages as $lang)
                                        @if ($lang->la_code == 'RU' || $lang->la_code == 'PL' || $lang->la_code == 'PT')
                                            @continue
                                        @endif

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-3 col-form-label" for="checkbox_languages_{{$lang->la_code}}">{{$lang->la_language}}:</label>
                                            <div class="col-sm-7">
                                                <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                                    <input type="checkbox" class="custom-control-input" id="checkbox_languages_{{$lang->la_code}}"
                                                           name="newslanguages[{{$lang->la_code}}]" {{(in_array($lang->la_code, $question_languages) ? "checked" : "")}}>
                                                      <label class="custom-control-label"
                                                       for="checkbox_languages_{{$lang->la_code}}"></label>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>

                                <h2 class="content-heading pt-0">Filters - Max. requests</h2>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="max_requests_month">Max. requests per month:</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="max_requests_month"
                                               value="{{$question->ktcuqu_max_requests_month}}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="max_requests_month_left">Max. requests per month (Left):</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="max_requests_month_left"
                                               value="{{$question->ktcuqu_max_requests_month_left}}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="max_requests_day">Max. requests per day:</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="max_requests_day"
                                               value="{{$question->ktcuqu_max_requests_day}}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="max_requests_day_left">Max. requests per day (Left):</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="max_requests_day_left"
                                               value="{{$question->ktcuqu_max_requests_day_left}}">
                                    </div>
                                </div>

                                <h2 class="content-heading pt-0"></h2>

                                <div class="row">
                                    <div class="col-md-1"></div>
                                    <div class="form-group col-md-8">
                                        <button type="submit" class="btn btn-primary">Update</button>
                                        <a class="btn btn-primary" href="../../edit">
                                            Back
                                        </a>
                                    </div>
                                </div>

                            </form>
                        </div>

                        <div class="tab-pane" id="btabs-alt-static-request_deliveries" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        <div class="block-header block-header-default">
                                            <h3 class="block-title">
                                                <a href="{{ url('serviceproviders/' . $customer->cu_id . '/question/'.$question->ktcuqu_id.'/request_delivery/create')}}">
                                                    <button class="btn btn-primary">Add a Request delivery</button>
                                                </a>

                                            </h3>
                                        </div>
                                        <div class="block-content block-content-full">
                                            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                                <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Type</th>
                                                    <th>Value</th>
                                                    <th>Extra</th>
                                                    <th>Modify</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach ($request_deliveries as $req_delivery)
                                                    <tr>
                                                        <td>{{$req_delivery->rede_id}}</td>
                                                        <td>{{$requestdeliveries[$req_delivery->rede_type]}}</td>
                                                        <td>{{$req_delivery->rede_value}}</td>
                                                        <td>{{$req_delivery->rede_extra}}</td>

                                                        <td class="text-center">
                                                            <div class="btn-group">
                                                                <a class="btn btn-sm btn-primary" data-toggle="tooltip"
                                                                   data-placement="left"
                                                                   title="edit"
                                                                   href="{{ url('serviceproviders/' . $customer->cu_id .'/question/'.$question->ktcuqu_id .'/request_delivery/' . $req_delivery->rede_id.'/edit')}}">
                                                                    <i class="fa fa-pencil-alt"></i>
                                                                </a>

                                                                <button type="button" class="btn btn-sm btn-primary request_delivery_delete" data-toggle="tooltip" title="Delete" data-rede_id="{{$req_delivery->rede_id}}">
                                                                    <i class="fa fa-times"></i>
                                                                </button>

                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="btabs-alt-static-origins" role="tabpanel">
                            <a href="#" class="btn btn-primary select_all_or">Select all</a>
                            <a href="#" class="btn btn-primary deselect_all_or">Deselect all</a>
                            <br /><br />

                            <form class="mb-5" method="post" action="{{action('ServiceProviderQuestionsController@update', $question->ktcuqu_id)}}">
                                @csrf
                                <input name="_method" type="hidden" value="PATCH">
                                <input name="form_name" type="hidden" value="Origins">

                                @foreach($macro_regions as $continent => $mare_row)
                                    <h3>{{$continent}}</h3>
                                    <div class="row">
                                        @foreach($mare_row as $id => $row)
                                            <div class="col-md-2">
                                                <div class="well" style="position: relative;display: block;">
                                                    <div class="block block-rounded block-bordered">
                                                        <div class="block-header block-header-default">
                                                            <h3 class="block-title"> {{$row['mare_name']}}</h3>
                                                        </div>
                                                        <div class="block-content">
                                                            @foreach($countries[$id] as $co_code => $country)
                                                                <div class="row">
                                                                    <label class="col-sm-9 col-form-label"
                                                                           for="origins[{{$co_code}}]">{{$country}}</label>
                                                                    <div class="col-sm-3">
                                                                        <div class="custom-control custom-switch custom-control-inline custom-control-primary">
                                                                            <input type="checkbox" class="custom-control-input"
                                                                                   id="origins[{{$co_code}}]"
                                                                                   name="origins[{{$co_code}}]"
                                                                                    {{(isset($originschecked[$co_code]) && $originschecked[$co_code] ? "checked" : "")}}>
                                                                            <label class="custom-control-label"
                                                                                   for="origins[{{$co_code}}]"></label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @endforeach

                                <div class="row">
                                    <div class="form-group col-md-8">
                                        <button type="submit" class="btn btn-primary">Update</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="tab-pane" id="btabs-alt-static-destinations" role="tabpanel">
                            <a href="#" class="btn btn-primary select_all_de">Select all</a>
                            <a href="#" class="btn btn-primary deselect_all_de">Deselect all</a>

                            <br/><br/>

                            <form class="mb-5" method="post" action="{{action('ServiceProviderQuestionsController@update', $question->ktcuqu_id)}}">
                                @csrf
                                <input name="_method" type="hidden" value="PATCH">
                                <input name="form_name" type="hidden" value="Destinations">

                                @foreach($macro_regions as $continent => $mare_row)
                                    <h3>{{$continent}}</h3>
                                    <div class="row">
                                        @foreach($mare_row as $id => $row)
                                            <div class="col-md-2">
                                                <div class="well" style="position: relative;display: block;">
                                                    <div class="block block-rounded block-bordered">
                                                        <div class="block-header block-header-default">
                                                            <h3 class="block-title"> {{$row['mare_name']}}</h3>
                                                        </div>
                                                        <div class="block-content">
                                                            @foreach($countries[$id] as $co_code => $country)
                                                                <div class="row">
                                                                    <label class="col-sm-9 col-form-label"
                                                                           for="destinations[{{$co_code}}]">{{$country}}</label>
                                                                    <div class="col-sm-3">
                                                                        <div class="custom-control custom-switch custom-control-inline custom-control-primary">
                                                                            <input type="checkbox" class="custom-control-input"
                                                                                   id="destinations[{{$co_code}}]"
                                                                                   name="destinations[{{$co_code}}]"
                                                                                    {{(isset($destinationschecked[$co_code]) && $destinationschecked[$co_code] ? "checked" : "")}}>
                                                                            <label class="custom-control-label"
                                                                                   for="destinations[{{$co_code}}]"></label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @endforeach

                                <div class="row">
                                    <div class="form-group col-md-8">
                                        <button type="submit" class="btn btn-primary">Update</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@push('scripts')

    <script>
        $( "#select_all" ).click(function() {
            var checkboxes = $(this).closest('#languages').find(':checkbox');

            checkboxes.prop('checked', true);
        });

        $( "#deselect_all" ).click(function() {
            var checkboxes = $(this).closest('#languages').find(':checkbox');

            checkboxes.prop('checked', false);
        });

        jQuery(document).ready(function(){
        	jQuery( document ).on( 'click', '.request_delivery_delete', function(e) {
                e.preventDefault();

        		var $self = jQuery( this );

                confirmDelete("{{ url('ajax/customer/request_delivery_delete') }}", 'get', {id:$self.data('rede_id')}, function() {

                    $self.parents( '.dataTable' ).DataTable().row( $self.parents( 'tr' ) ).remove().draw( 'page' );
                });
            });
        });
    </script>

@endpush
