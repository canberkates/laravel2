@extends('layouts.backend')
@include( 'scripts.dialogs' )
@include( 'scripts.editor' )
@pushonce( 'scripts:serviceprovider-newsletter-edit' )
    <script src="{{ asset('/js/custom/serviceprovider/serviceprovider_newsletter_edit.js') }}"></script>
@endpushonce

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Edit Advertorial Block of {{$customer->cu_company_name_business}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../../../">Service Providers</a></li>
                        <li class="breadcrumb-item"><a href="../../edit">{{$customer->cu_company_name_business}}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Advertorial Block edit</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="block block-rounded block-bordered">
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    {{$error}}<br>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <ul class="nav nav-tabs nav-tabs-alt" data-toggle="tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" href="#btabs-alt-static-general">General</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-content">Content</a>
                        </li>
                    </ul>

                    <div class="block-content tab-content">
                        <div class="tab-pane active" id="btabs-alt-static-general" role="tabpanel">
                            <form class="mb-5" method="post" action="{{action('ServiceProviderAdvertorialBlockController@update', $advertorialblock_id)}}">
                                @csrf
                                <input name="_method" type="hidden" value="PATCH">
                                <input name="form_name" type="hidden" value="General">

                                <h2 class="content-heading pt-0">General</h2>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="slot">Slot:</label>
                                    <div class="col-sm-7">
                                        <select type="text" class="form-control" name="slot">
                                            <option value=""></option>
                                            @foreach($newsletterslots as $id => $slot)
                                                <option @if ($id == $advertorialblock->sepradbl_slot) selected @endif value="{{$id}}">{{$slot}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="description">Description:</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="description"
                                               value="{{$advertorialblock->sepradbl_description}}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="status">Status:</label>
                                    <div class="col-sm-7">
                                        <select type="text" class="form-control" name="status">
                                            @foreach($statuses as $id => $status)
                                                <option @if ($id == $advertorialblock->sepradbl_status) selected @endif value="{{$id}}">{{$status}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <h2 class="content-heading pt-0">Tracking info</h2>

                                <div class='row'>
                                    <div class="col-sm-1"></div>
                                    <div class='col-sm-11 mb-2'>
                                        <i style='font-size:14px;'>Please note: Its possible to use: {slot} and {company_name}</i>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="event_category">GA Event Category:</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="event_category"
                                               value="{{$advertorialblock->sepradbl_event_category}}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="event_label">GA Event Label:</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="event_label"
                                               value="{{$advertorialblock->sepradbl_event_label}}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="event_action">GA Event Action:</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="event_action"
                                               value="{{$advertorialblock->sepradbl_action}}">
                                    </div>
                                </div>

                                <h2 class="content-heading pt-0">Filters - Websites</h2>

                                <div id="websites">
                                    @foreach($websites as $we_id => $website)
                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-3 col-form-label" for="checkbox_websites_{{$we_id}}">{{$website}}:</label>
                                            <div class="col-sm-7">
                                                <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                                    <input type="checkbox" class="custom-control-input" id="checkbox_websites_{{$we_id}}"
                                                           name="blockwebsites[{{$we_id}}]" {{(in_array($we_id, $adbl_websites) ? "checked" : "")}}>
                                                      <label class="custom-control-label"
                                                       for="checkbox_websites_{{$we_id}}"></label>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>

                                <h2 class="content-heading pt-0">Filters - Languages</h2>

                                <div id="languages">
                                    @foreach($languages as $lang)
                                        @if ($lang->la_code == 'RU' || $lang->la_code == 'PL' || $lang->la_code == 'PT')
                                            @continue
                                        @endif

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-3 col-form-label" for="checkbox_languages_{{$lang->la_code}}">{{$lang->la_language}}:</label>
                                            <div class="col-sm-7">
                                                <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                                    <input type="checkbox" class="custom-control-input" id="checkbox_languages_{{$lang->la_code}}"
                                                           name="blocklanguages[{{$lang->la_code}}]" {{(in_array($lang->la_code, $adbl_languages) ? "checked" : "")}}>
                                                      <label class="custom-control-label"
                                                       for="checkbox_languages_{{$lang->la_code}}"></label>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>

                                <h2 class="content-heading pt-0"></h2>

                                <div class="row">
                                    <div class="col-md-1"></div>
                                    <div class="form-group col-md-8">
                                        <button type="submit" class="btn btn-primary">Update</button>
                                        <a class="btn btn-primary" href="../../edit">
                                            Back
                                        </a>
                                    </div>
                                </div>

                            </form>
                        </div>

                        <div class="tab-pane" id="btabs-alt-static-content" role="tabpanel">
                            <form class="mb-5" method="post" action="{{action('ServiceProviderAdvertorialBlockController@update', $advertorialblock_id)}}">
                                @csrf
                                <input name="_method" type="hidden" value="PATCH">
                                <input name="form_name" type="hidden" value="Content">

                                @foreach($languages as $lang)
                                    <div @if(!in_array($lang->la_code, $adbl_languages))style='display:none;'@endif>
                                        <h2 class="content-heading pt-0">{{$lang->la_language}}</h2>
                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-2 col-form-label" for="title[{{$lang->la_code}}]">Title:</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" name="title[{{$lang->la_code}}]"
                                                    value="{{$titles_per_lang[$lang->la_code]}}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-2 col-form-label" for="content[{{$lang->la_code}}]">Content:</label>
                                            <div class="col-sm-5">
                                                <textarea class='form-control' id="textarea_content" name="blockcontent[{{$lang->la_code}}]">{{$content_per_lang[$lang->la_code]}}</textarea>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-2 col-form-label" for="button[{{$lang->la_code}}]">Button:</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" name="button[{{$lang->la_code}}]"
                                                    value="{{$buttons_per_lang[$lang->la_code]}}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-2 col-form-label" for="url[{{$lang->la_code}}]">Url:</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" name="url[{{$lang->la_code}}]"
                                                    value="{{$urls_per_lang[$lang->la_code]}}">
                                            </div>
                                        </div>
                                    </div>
                                @endforeach

                                <h2 class="content-heading pt-0"></h2>

                                <div class="row">
                                    <div class="col-md-1"></div>
                                    <div class="form-group col-md-8">
                                        <button type="submit" class="btn btn-primary">Update</button>
<!--                                        <a style="color:white;" data-seprnebl_id="{{$newsletterblock->seprnebl_id}}" class="btn btn-primary" id="preview_newsletterblock">
                                            Preview
                                        </a>
                                        <a style="color:white;" data-seprnebl_id="{{$newsletterblock->seprnebl_id}}" class="btn btn-primary" id="send_newsletterblock">
                                            Send preview to your email
                                        </a>-->
                                        <a class="btn btn-primary" href="../../edit">
                                            Back
                                        </a>
                                    </div>
                                </div>

                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="viewNewsletterModal" class="modal fade" role="dialog" >
        <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content" style="width:800px;">
                <div class="modal-header">
                    <h5 class="modal-title">Viewing Newsletter</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="newsletter_iframe">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@push( 'scripts' )

    <script>
        $( "#select_all" ).click(function() {
            var checkboxes = $(this).closest('#languages').find(':checkbox');

            checkboxes.prop('checked', true);
        });

        $( "#deselect_all" ).click(function() {
            var checkboxes = $(this).closest('#languages').find(':checkbox');

            checkboxes.prop('checked', false);
        });

    </script>
@endpush
