@extends('layouts.backend')

@include('scripts.select2')
@include('scripts.datepicker')

@section('content')

	<div class="content">
		<form class="mb-5 form-small" id="match_form" method="post"
		      action="{{action('RequestsController@matchlead', $request->re_id)}}">
			@csrf
			@if($errors->any())
				<div class="alert alert-danger">
					<h4>
						@foreach($errors->all() as $error)
							{{$error}}<br>
						@endforeach
					</h4>
				</div>
			@endif

            <input type="hidden" name="matching_customers[]" value="{{json_encode($matchingcustomers)}}">


            <h2>Lead information</h2>
			<div class="row">
				<div class="col-md-6">

					<div class="block block-rounded block-bordered">
						<div class="block-content">

							@if($request->re_company_name)
								<div class="form-group row">
									<div class="col-sm-1"></div>
									<label class="col-sm-3 col-form-label" for="example-hf-email">Company name:</label>
									<div class="col-sm-7">
										<input disabled class="form-control" type="text"
										       value="{{$request->re_company_name}}">
									</div>
								</div>
							@endif

							<div class="form-group row">
								<div class="col-sm-1"></div>
								<label class="col-sm-3 col-form-label" for="example-hf-email">Full name:</label>
								<div class="col-sm-7">
									<input disabled class="form-control" type="text" value="{{$request->re_full_name}}">
								</div>
							</div>

							<div class="form-group row">
								<div class="col-sm-1"></div>
								<label class="col-sm-3 col-form-label" for="example-hf-email">Telephone 1:</label>
								<div class="col-sm-7">
									<input disabled class="form-control" type="text"
									       value="{{$request->re_telephone1}}">
								</div>
							</div>

							@if($request->re_telephone2)
								<div class="form-group row">
									<div class="col-sm-1"></div>
									<label class="col-sm-3 col-form-label" for="example-hf-email">Telephone 2:</label>
									<div class="col-sm-7">
										<input disabled class="form-control" type="text"
										       value="{{$request->re_telephone2}}">
									</div>
								</div>
							@endif

							<div class="form-group row">
								<div class="col-sm-1"></div>
								<label class="col-sm-3 col-form-label" for="example-hf-email">Email:</label>
								<div class="col-sm-7">
									<input disabled class="form-control" type="text" value="{{$request->re_email}}">
								</div>
							</div>
                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="example-hf-email">Moving size:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text" value="{{App\Data\MovingSize::name($request->re_moving_size)}}">
                                </div>
                            </div>
						</div>
					</div>
				</div>
			</div>


            @if($amountofmatches > 0)
                <h2>The following {{$amountofmatches}} customer(s) have been found:</h2>
            @else
                <h2>No customers have been found for this request</h2>
            @endif
            @if($sirelo_customer)
                <div class="row">
                    <div class="col-md-6">
                        <div class="block block-rounded block-bordered">
                            <div class="block-header block-header-default">
                                <h3 class="block-title">Free Sirelo Lead</h3>
                            </div>
                            <div class="block-content">
                                <div class="row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-8 col-form-label"
                                           for="customers[{{$sirelo_customer->cu_id}}]">{{$sirelo_customer->cu_company_name_business}}
                                        :</label>
                                    <div class="col-sm-3">
                                        <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                            <input type="checkbox" class="custom-control-input"
                                                   id="customers[{{$sirelo_customer->cu_id}}]"
                                                   name="customers[{{$sirelo_customer->cu_id}}]"
                                                   disabled checked>
                                            <label class="custom-control-label"
                                                   for="customers[{{$sirelo_customer->cu_id}}]">100.00%</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

			@if($amountofmatches > 0)
                <div class="row">
                    <div class="col-md-6">
                        <div class="block block-rounded block-bordered">
                            <div class="block-header block-header-default">
                                <h3 class="block-title">Customers</h3>
                            </div>
                            <div class="block-content">

                                @foreach($matchingcustomers["customers"] as $customer)
                                    <div class="row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-8 col-form-label"
                                               for="customers[{{$customer['ktcupo_id']}}]">{{$customer['cu_company_name_business']}}:</label>
                                        <div class="col-sm-3">
                                            <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                                <input type="checkbox" class="custom-control-input"
                                                       id="customers[{{$customer['ktcupo_id']}}]"
                                                       name="customers[{{$customer['ktcupo_id']}}]"
                                                    {{($customer['checked'] ? "checked" : "")}}
                                                >
                                                <label class="custom-control-label"
                                                       for="customers[{{$customer['ktcupo_id']}}]">{{number_format($customer['matching_percentage_after_podium'],2)}}
                                                    %</label>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group col-md-8" id="match_action_buttons" style="display:none;">
					<button type="submit" name="match" id="match_button" class="btn btn-primary">Match</button>
					<a href="../{{$request->re_id}}/edit">
						<button type="button" class="btn btn-primary">Back</button>
					</a>
				</div>
			@else
				<div class="form-group col-md-8">
					<a href="../{{$request->re_id}}/reject">
						<button type="button" class="btn btn-primary">Reject</button>
					</a>
					<a href="../{{$request->re_id}}/edit">
						<button type="button" class="btn btn-primary">Back</button>
					</a>
				</div>
			@endif
		</form>
	</div>

@endsection

@push('scripts')
    <script>
        jQuery(document).ready(function () {

            $("#match_action_buttons").show(400);

            $("#match_form").submit(function (e) {
              $("#match_button").attr("disabled", "disabled");
            })
        });
    </script>
@endpush



