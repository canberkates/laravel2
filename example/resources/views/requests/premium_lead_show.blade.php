@extends('layouts.backend')

@include('scripts.select2')
@include('scripts.datepicker')

@include('scripts.region_suggestions')

@section('content')
	<div class="content">
		@if($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach($errors->all() as $error)
						{!! $error !!}<br>
					@endforeach
				</ul>
			</div>
		@endif

		@isset($notices['double'])
			@if($request->re_double)
				@foreach($notices['double'] as $notice)
					<div class="alert alert-dismissable" role="alert">
						<p class="mb-0">{!! $notice !!}</p>
					</div>
				@endforeach
			@endif
		@endisset

		<form class="mb-5 form-small" id="request_edit_form" method="post" action="{{action('RequestsController@updatePremiumLead', $request->re_id)}}" enctype="multipart/form-data">
			@csrf
			<input name="_method" type="hidden" value="PUT">

			<div class="row justify-content">
				<div class="col-lg-6 col-md-12">
					<div class="block block-rounded block-bordered">
						<div class="block-header block-header-default">
							<h3 class="block-title">Premium lead info</h3>
						</div>
						<div class="block-content">

                            <div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-received">Received:</label>
								<div class="col-sm-9">
									<input disabled class="form-control" name="received" id="for-received" type="text" value="{{$request->re_timestamp}}">
								</div>
							</div>

                            <div class="form-group row">
								<label class="col-sm-3 col-form-label"
								       for="moving_size">Premium Lead Status:</label>
								<div class="col-sm-9">
									<select disabled type="text" class="form-control"
									        name="premiumlead_status">
										@foreach($premiumleadstatuses as $id => $status)
											<option value="{{$id}}" {{ ($request->prle_status == $id) ? 'selected':'' }}>{{$status}}</option>
										@endforeach
									</select>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="preferred_date_appointment">Preferred date for Appointment:</label>
								<div class="col-sm-9">
									<input disabled class="form-control" name="preferred_date_appointment" id="preferred_date_appointment" type="text" value="{{$request->prle_preferred_appointment_timestamp}}">
								</div>
							</div>


							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="planned_date_appointment">Planned date for Appointment:</label>
								<div class="col-sm-9">
                                    <input disabled autocomplete="off" name="planned_date_appointment" class="form-control" id="datetimepicker" type="text" value="{{$request->prle_planned_appointment_timestamp}}" />
								</div>
							</div>

                            @if ($request->prle_status != 0)
							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-request_status">Planned at + Who:</label>
								<div class="col-sm-9">
									<input disabled class="form-control" id="for-request_status" type="text" value="@if(!empty($request->prle_planned_timestamp) && !empty($request->prle_planned_by)) Planned on {{$request->prle_planned_timestamp}} by {{$users[$request->prle_planned_by]}} @endif">
								</div>
							</div>
                            @endif

                            <div class="form-group row">
								<label class="col-sm-3 col-form-label"
								       for="survey_executor">Survey Executor:</label>
								<div class="col-sm-9">
									<select disabled type="text" class="form-control"
									        name="survey_executor">
                                        <option value=""></option>
										@foreach($surveyexecutors as $id => $executor)
											<option value="{{$id}}" {{ ($request->prle_survey_executor == $id) ? 'selected':'' }}>{{$executor}}</option>
										@endforeach
									</select>
								</div>
							</div>

						</div>
					</div>

				</div>
                <div class="col-lg-6 col-md-12">
					<div class="block block-rounded block-bordered">
						<div class="block-header block-header-default">
							<h3 class="block-title">Volume of move</h3>
						</div>
						<div class="block-content">

                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label" for="uploaded_excel_file">List of all items:</label>

                                <div class="uploadedfile col-sm-7" @if (empty($request->prle_list_of_all_items)) style="display:none;" @endif>
                                    {{$request->prle_list_of_all_items}}
                                    <a style="margin-left: 10px; font-size: 18px;" target="_blank" class="" data-toggle="tooltip" data-placement="left" title="Download" href="{{ url('premium_leads/' . $request->re_id . '/file/download')}}">
                                        <i class="fa fa-download"></i>
                                    </a>
                                </div>
                            </div>

                            <div class="form-group row">
								<label class="col-sm-3 col-form-label" for="special_items">Special items:</label>
								<div class="col-sm-9">
                                    <textarea rows="1" disabled class="form-control" name="special_items" id="special_items">{{$request->prle_special_items}}</textarea>
								</div>
							</div>

                            <div class="form-group row">
								<label class="col-sm-3 col-form-label">Volume:</label>
								<div class="row pr-0 col-sm-9">
									<div class="col-sm-6 pr-0">
										<div class="input-group">
											<input disabled type="text" autocomplete="off" class="form-control" name="volume_m3" id="for-volume_m3" value="{{$request->re_volume_m3}}">
											<label class="input-group-append" for="for-volume_m3">
												<span class="input-group-text">m3</span>
											</label>
										</div>
									</div>
									<div class="col-sm-6 pr-0">
										<div class="input-group">
											<input disabled type="text" autocomplete="off" class="form-control" name="volume_ft3" id="for-volume_ft3" value="{{$request->re_volume_ft3}}">
											<label class="input-group-append" for="for-volume_ft3">
												<span class="input-group-text">ft3</span>
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row justify-content-center">
				<div class="col-lg-6 col-md-12">
					<div id="moving_from" class="block block-rounded block-bordered">
						<div class="block-header block-header-default">
							<h3 class="block-title">Loading address</h3>
						</div>
						<div class="block-content">
							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-street_from">Street:</label>
								<div class="col-sm-9">
									<input disabled autocomplete="off" class="form-control refreshMapOnKeyup" name="street_from" id="for-street_from" data-from-to="from" type="text" value="{{$request->re_street_from}}">
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-zipcode_from">Zipcode:</label>
								<div class="col-sm-9">
									<input disabled autocomplete="off" class="form-control refreshMapOnKeyup" name="zipcode_from" id="for-zipcode_from" data-from-to="from" type="text" value="{{$request->re_zipcode_from}}">
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-city_from">City:</label>
								<div class="col-sm-9">
									<input disabled autocomplete="off" class="form-control refreshMapOnKeyup" type="text" name="city_from" data-from-to="from" id="for-city_from" value="{{$request->re_city_from}}">
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-country_from">Country:</label>
								<div class="col-sm-9">
									<div class="input-group">
										<select disabled type="text" class="js-select2 form-control refreshMapOnChange" data-from-to="from" data-placeholder="Choose a country.." name="country_from" id="for-country_from">
											<option></option>
											@foreach($countries as $id => $country)
												<option value="{{$id}}" {{ ($request->re_co_code_from ==  $id) ? 'selected':'' }}>{{$country}}</option>
											@endforeach
										</select>
									</div>

								</div>
							</div>

                            <div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-region_from">Region:</label>
								<div class="col-sm-9">
									<div class="input-group">
										<select disabled type="text" class="form-control js-select2" data-placeholder="Choose a region.." name="region_from" id="for-region_from" data-allow-clear></select>
									</div>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="region_suggestion_from">Region Suggestion:</label>
								<div class="col-sm-9">
									<div id="region_suggestion_from"></div>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="type_of_residence_from">Type of residence:</label>
								<div class="col-sm-9">
									<select disabled type="text" class="form-control" name="type_of_residence_from" id="type_of_residence_from">
										@foreach($premiumleadresidencetypes as $id => $residence)
											<option value="{{$id}}" {{ ($request->prle_residence_type_from ==  $id) ? 'selected':'' }}>{{$residence}}</option>
										@endforeach
									</select>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="floor_level_from">Floor level:</label>
								<div class="col-sm-9">
									<select disabled type="text" class="form-control" name="floor_level_from" id="floor_level_from">
										@foreach($requestresidences as $id => $residence)
											<option value="{{$id}}" {{ ($request->re_residence_from ==  $id) ? 'selected':'' }}>{{$residence}}</option>
										@endforeach
									</select>
								</div>
							</div>

                            <div class="form-group row">
								<label class="col-sm-3 col-form-label" for="inside_elevator_from">Inside elevator:</label>
								<div class="col-sm-9">
                                    <textarea disabled rows="1" class="form-control" name="inside_elevator_from" id="inside_elevator_from">{{$request->prle_inside_elevator_from}}</textarea>
								</div>
							</div>

                            <div class="form-group row">
								<label class="col-sm-3 col-form-label" for="obstacles_for_moving_van_from">Obstacles for moving van:</label>
								<div class="col-sm-9">
                                    <textarea disabled rows="1" class="form-control" name="obstacles_for_moving_van_from" id="obstacles_for_moving_van_from">{{$request->prle_obstacles_for_moving_van_from}}</textarea>
								</div>
							</div>

                            <div class="form-group row">
								<label class="col-sm-3 col-form-label" for="parking_restrictions_and_permits_from">Parking restrictions/permits:</label>
								<div class="col-sm-9">
                                    <textarea disabled rows="1" class="form-control" name="parking_restrictions_and_permits_from" id="parking_restrictions_and_permits_from">{{$request->prle_parking_restrictions_and_permits_from}}</textarea>
								</div>
							</div>

                            <div class="form-group row">
								<label class="col-sm-3 col-form-label" for="walking_distance_to_house_from">Walking distance to house:</label>
								<div class="col-sm-9">
                                    <textarea disabled rows="1" class="form-control" name="walking_distance_to_house_from" id="walking_distance_to_house_from">{{$request->prle_walking_distance_to_house_from}}</textarea>
								</div>
							</div>

						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-12">
					<div id="moving_to" class="block block-rounded block-bordered">
						<div class="block-header block-header-default">
							<h3 class="block-title">Destination address</h3>
						</div>
						<div class="block-content">
                            <div class="form-group row">
								<label class="col-sm-3 col-form-label" for="street_to">Street:</label>
								<div class="col-sm-9">
									<input disabled autocomplete="off" class="form-control refreshMapOnKeyup" name="street_to" id="street_to" data-from-to="to" type="text" value="{{$request->re_street_to}}">
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="zipcode_to">Zipcode:</label>
								<div class="col-sm-9">
									<input disabled autocomplete="off" class="form-control refreshMapOnKeyup" name="zipcode_to" id="zipcode_to" data-from-to="to" type="text" value="{{$request->re_zipcode_to}}">
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="city_to">City:</label>
								<div class="col-sm-9">
									<input disabled autocomplete="off" class="form-control refreshMapOnKeyup" type="text" name="city_to" data-from-to="to" id="city_to" value="{{$request->re_city_to}}">
								</div>
							</div>

                            <div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-country_to">Country:</label>
								<div class="col-sm-9">
									<div class="input-group">
										<select disabled type="text" class="js-select2 reset-select form-control refreshMapOnChange" data-from-to="to" data-placeholder="Choose a country.." name="country_to" id="for-country_to">
											<option></option>
											@foreach($countries as $id => $country)
												<option value="{{$id}}" {{ ($request->re_co_code_to ==  $id) ? 'selected':'' }}>{{$country}}</option>
											@endforeach
										</select>
									</div>
								</div>
							</div>

                            <div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-region_to">Region:</label>
								<div class="col-sm-9">
									<div class="input-group">
										<select disabled type='text' class='form-control js-select2' data-placeholder='Choose a region..' name='region_to' id='for-region_to'>
										</select>
									</div>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="region_suggestion_to">Region Suggestion:</label>
								<div class="col-sm-9">
									<div id="region_suggestion_to"></div>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="type_of_residence_to">Type of residence:</label>
								<div class="col-sm-9">
									<select disabled type="text" class="form-control" name="type_of_residence_to" id="type_of_residence_to">
										@foreach($premiumleadresidencetypes as $id => $residence)
											<option value="{{$id}}" {{ ($request->prle_residence_type_to ==  $id) ? 'selected':'' }}>{{$residence}}</option>
										@endforeach
									</select>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="floor_level_to">Floor level:</label>
								<div class="col-sm-9">
									<select disabled type="text" class="form-control" name="floor_level_to" id="floor_level_to">
										@foreach($requestresidences as $id => $residence)
											<option value="{{$id}}" {{ ($request->re_residence_to ==  $id) ? 'selected':'' }}>{{$residence}}</option>
										@endforeach
									</select>
								</div>
							</div>

                            <div class="form-group row">
								<label class="col-sm-3 col-form-label" for="inside_elevator_to">Inside elevator:</label>
								<div class="col-sm-9">
                                    <textarea disabled rows="1" class="form-control" name="inside_elevator_to" id="inside_elevator_to">{{$request->prle_inside_elevator_to}}</textarea>
								</div>
							</div>

                            <div class="form-group row">
								<label class="col-sm-3 col-form-label" for="obstacles_for_moving_van_to">Obstacles for moving van:</label>
								<div class="col-sm-9">
                                    <textarea disabled rows="1" class="form-control" name="obstacles_for_moving_van_to" id="obstacles_for_moving_van_to">{{$request->prle_obstacles_for_moving_van_to}}</textarea>
								</div>
							</div>

                            <div class="form-group row">
								<label class="col-sm-3 col-form-label" for="parking_restrictions_and_permits_to">Parking restrictions/permits:</label>
								<div class="col-sm-9">
                                    <textarea disabled rows="1" class="form-control" name="parking_restrictions_and_permits_to" id="parking_restrictions_and_permits_to">{{$request->prle_parking_restrictions_and_permits_to}}</textarea>
								</div>
							</div>

                            <div class="form-group row">
								<label class="col-sm-3 col-form-label" for="walking_distance_to_house_to">Walking distance to house:</label>
								<div class="col-sm-9">
                                    <textarea disabled rows="1" class="form-control" name="walking_distance_to_house_to" id="walking_distance_to_house_to">{{$request->prle_walking_distance_to_house_to}}</textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-lg-6 col-md-12">
					<div class="block block-rounded block-bordered">
						<div class="block-header block-header-default">
							<h3 class="block-title">Contact details</h3>
						</div>
						<div class="block-content">

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-company_name">Company name:</label>
								<div class="col-sm-9">
									<input disabled autocomplete="off" class="form-control" name="company_name" id="for-company_name" type="text" value="{{$request->re_company_name}}">
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-full_name">Full name:</label>
								<div class="col-sm-9">
									<input disabled autocomplete="off" class="form-control" name="full_name" id="for-full_name" type="text" value="{{$request->re_full_name}}">
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-telephone1">Telephone 1:</label>
								<div class="col-sm-4">
									<input disabled autocomplete="off" class="form-control" name="telephone1" id="for-telephone1" type="text" value="{{$request->re_telephone1}}">
								</div>
								<div class="col-sm-1"><a href="#" class="add_to_dialfire" data-id="{{$request->re_id}}" data-type="requests"><i class="fa fa-2x fa-phone-square"></i></a></div>
							</div>
							@isset($notices['phone_1'])
								<div class="form-group row">
									<label class="col-sm-3"></label>
									<label class="col-sm-6" style="color:red">This phone number is NOT valid!</label>
								</div>
							@else
								<div class="form-group row">
									<label class="col-sm-3"></label>
									<label class="col-sm-6" style="color:green">This phone number seems to be valid!</label>
								</div>
							@endisset
                            @if($request->re_telephone_1_mail)
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label" for="for-mail_sent_telephone_1">Mail sent for telephone 1:</label>
                                    <div class="col-sm-4">
                                        <div class="custom-control custom-switch custom-control custom-control-inline custom-control-primary">
                                            <input disabled type="checkbox" class="custom-control-input" id="for-mail_sent_telephone_1" name="mail_sent_telephone_1" {{($request->re_telephone_1_mail ? "checked" : "")}}>
                                            <label class="custom-control-label" for="for-mail_sent_telephone_1"></label>
                                        </div>
                                    </div>
                                </div>
                            @endif

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-telephone2">Telephone 2:</label>
								<div class="col-sm-4">
									<input disabled autocomplete="off" class="form-control" name="telephone2" id="for-telephone2" type="text" value="{{$request->re_telephone2}}">
								</div>
							</div>
							@if($request->re_telephone2)
								@isset($notices['phone_2'])
									<div class="form-group row">
										<label class="col-sm-3"></label>
										<label class="col-sm-6" style="color:red">This phone number is NOT valid!</label>
									</div>
								@endisset
                                @if($request->re_telephone_2_mail)
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label" for="for-mail_sent_telephone_2">Mail sent for telephone 2:</label>
                                        <div class="col-sm-4">
                                            <div class="custom-control custom-switch custom-control custom-control-inline custom-control-primary">
                                                <input disabled type="checkbox" class="custom-control-input" id="for-mail_sent_telephone_2" name="mail_sent_telephone_2" {{($request->re_telephone_2_mail ? "checked" : "")}}>
                                                <label class="custom-control-label" for="for-mail_sent_telephone_2"></label>
                                            </div>
                                        </div>
                                    </div>
                                @endif
							@endif
							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-email">Email:</label>
								<div class="col-sm-9">
									<input disabled autocomplete="off" class="form-control" name="email" id="for-email" type="text" value="{{$request->re_email}}">
								</div>
							</div>

							@isset($notices['email_bounce'])
								<div class="form-group row">
									<label class="col-sm-3 col-form-label">Bounce:</label>
									<div class="col-sm-9" style="color:red">
										<label>{!! $notices['email_bounce'] !!}</label>
									</div>
								</div>
							@endisset

                            <div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-language">Language:</label>
								<div class="col-sm-9">
									<select disabled type="text" class="form-control" name="language" id="for-language">
										@foreach($languages as $language)
											<option value="{{$language->la_code}}" {{ ($request->re_la_code == $language->la_code) ? 'selected':'' }}>{{$language->la_language}}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-12">
					<div class="block block-rounded block-bordered">
						<div class="block-header block-header-default">
							<h3 class="block-title">Moving details</h3>
						</div>
						<div class="block-content">
                            <div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-destination_type">Destination type:</label>
								<div class="col-sm-9">
									<input disabled class="form-control" name="destination_type" id="for-destination_type" data-id="{{$request->re_destination_type}}" type="text" value="{{$destinationtype}}">
								</div>
							</div>

                            <div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-moving_date">Moving date:</label>
								<div class="col-sm-9">
									<input disabled autocomplete="off" value="{{$request->re_moving_date}}" type="text"
									       class="js-datepicker form-control" id="for-moving_date" name="moving_date"
									       data-week-start="1" data-autoclose="true" data-today-highlight="true"
									       data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd">
								</div>
							</div>

                            <div class="form-group row">
								<label class="col-sm-3 col-form-label" for="extra_info_moving_date">Extra info moving date:</label>
								<div class="col-sm-9">
                                    <textarea disabled rows="1" class="form-control" name="extra_info_moving_date" id="extra_info_moving_date">{{$request->prle_extra_info_moving_date}}</textarea>
								</div>
							</div>

                            <div class="form-group row">
								<label class="col-sm-3 col-form-label" for="packing_by_mover">Packing by Mover:</label>
								<div class="col-sm-2">
									<select disabled type="text" class="form-control" name="packing_by_mover" id="packing_by_mover">
										@foreach($emptyyesno as $id => $value)
											<option value="{{$id}}" {{ ($request->prle_packing_by_mover == $id) ? 'selected':'' }}>{{$value}}</option>
										@endforeach
									</select>
								</div>
                                <div class="col-sm-7">
                                    <textarea disabled rows="1" class="form-control" name="packing_by_mover_remarks" id="packing_by_mover_remarks" placeholder="Remark">{{$request->prle_packing_by_mover_remarks}}</textarea>
                                </div>
							</div>

                            <div class="form-group row">
								<label class="col-sm-3 col-form-label" for="unpacking_by_mover">Unpacking by Mover:</label>
								<div class="col-sm-2">
									<select disabled type="text" class="form-control" name="unpacking_by_mover" id="unpacking_by_mover">
										@foreach($emptyyesno as $id => $value)
											<option value="{{$id}}" {{ ($request->prle_unpacking_by_mover == $id) ? 'selected':'' }}>{{$value}}</option>
										@endforeach
									</select>
								</div>
                                <div class="col-sm-7">
                                    <textarea disabled rows="1" class="form-control" name="unpacking_by_mover_remarks" id="unpacking_by_mover_remarks" placeholder="Remark">{{$request->prle_unpacking_by_mover_remarks}}</textarea>
                                </div>
							</div>

                            <div class="form-group row">
								<label class="col-sm-3 col-form-label" for="disassembling_by_mover">Disassembling by Mover:</label>
								<div class="col-sm-2">
									<select disabled type="text" class="form-control" name="disassembling_by_mover" id="disassembling_by_mover">
										@foreach($emptyyesno as $id => $value)
											<option value="{{$id}}" {{ ($request->prle_disassembling_by_mover == $id) ? 'selected':'' }}>{{$value}}</option>
										@endforeach
									</select>
								</div>
                                <div class="col-sm-7">
                                    <textarea disabled rows="1" class="form-control" name="disassembling_by_mover_remarks" id="disassembling_by_mover_remarks" placeholder="Remark">{{$request->prle_disassembling_by_mover_remarks}}</textarea>
                                </div>
							</div>

                            <div class="form-group row">
								<label class="col-sm-3 col-form-label" for="assembling_by_mover">Assembling by Mover:</label>
								<div class="col-sm-2">
									<select disabled type="text" class="form-control" name="assembling_by_mover" id="assembling_by_mover">
										@foreach($emptyyesno as $id => $value)
											<option value="{{$id}}" {{ ($request->prle_assembling_by_mover == $id) ? 'selected':'' }}>{{$value}}</option>
										@endforeach
									</select>
								</div>
                                <div class="col-sm-7">
                                    <textarea disabled rows="1" class="form-control" name="assembling_by_mover_remarks" id="assembling_by_mover_remarks" placeholder="Remark">{{$request->prle_assembling_by_mover_remarks}}</textarea>
                                </div>
							</div>

                            <div class="form-group row">
								<label class="col-sm-3 col-form-label" for="storage">Storage:</label>
								<div class="col-sm-2">
									<select disabled type="text" class="form-control" name="storage" id="storage">
										@foreach($emptyyesno as $id => $value)
											<option value="{{$id}}" {{ ($request->prle_storage == $id) ? 'selected':'' }}>{{$value}}</option>
										@endforeach
									</select>
								</div>
                                <div class="col-sm-7">
                                    <textarea disabled rows="1" class="form-control" name="storage_remarks" id="storage_remarks" placeholder="Remark">{{$request->prle_storage_remarks}}</textarea>
                                </div>
							</div>

                            <div class="form-group row">
								<label class="col-sm-3 col-form-label" for="storage">Handyman service:</label>
								<div class="col-sm-9">
									<textarea disabled rows="1" class="form-control" name="handyman_service" id="handyman_service">{{$request->prle_handyman_service}}</textarea>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
			<div class="row">
                <div class="col-lg-6 col-md-12">
                    <div class="block block-rounded block-bordered">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Insurance</h3>
                        </div>
                        <div class="block-content">
                            <div class="form-group row">
								<label class="col-sm-3 col-form-label" for="insurance">Insurance:</label>
								<div class="col-sm-2">
									<select disabled type="text" class="form-control" name="insurance" id="insurance">
										@foreach($emptyyesno as $id => $value)
											<option value="{{$id}}" {{ ($request->prle_insurance == $id) ? 'selected':'' }}>{{$value}}</option>
										@endforeach
									</select>
								</div>
                                <div class="col-sm-7">
                                    <textarea disabled rows="1" class="form-control" name="insurance_remarks" id="insurance_remarks" placeholder="Remark">{{$request->prle_insurance_remarks}}</textarea>
                                </div>
							</div>

                            <div class="form-group row">
								<label class="col-sm-3 col-form-label" for="insurance_amount">Amount:</label>
                                <div class="col-sm-9">
                                    <textarea disabled rows="1" class="form-control" name="insurance_amount" id="insurance_amount">{{$request->prle_insurance_amount}}</textarea>
                                </div>
							</div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-12">
                    <div class="block block-rounded block-bordered">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Remarks - Additional information</h3>
                        </div>
                        <div class="block-content">
                            @if(!empty($request->prle_screenshots))
                                <div class="form-group row js-gallery">
                                    <label class="col-sm-12 col-form-label" for="uploaded_excel_file">Current screenshots:</label>

                                    @foreach(unserialize($request->prle_screenshots) as $screenie)
                                        <div class="col-sm-3">
                                            <div style="margin-bottom:30px; float: left;" id="{{$screenie}}">
                                                <a class="img-link img-lightbox" target="_blank" href="{{url("function/getscreenshot/".$request->re_id."/".$screenie)}}">
                                                    <img style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.5), 0 6px 20px 0 rgba(0, 0, 0, 0.4); max-height:100px; max-width: 130px;" src="{{url("function/getscreenshot/".$request->re_id."/".$screenie)}}" />

                                                </a>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @endif

                            <div class="form-group row">
								<label class="col-sm-3 col-form-label" for="remarks">Remarks:</label>
                                <div class="col-sm-9">
                                    <textarea disabled style="height: 100px;" class="form-control" name="remarks" id="remarks">{{$request->re_remarks}}</textarea>
                                </div>
							</div>
                        </div>
                    </div>
                </div>
			</div>
			<div class="row">
                <div class="col-lg-6 col-md-12">
					<div class="block block-rounded block-bordered">
						<div class="block-header block-header-default">
							<h3 class="block-title">Internal information</h3>
						</div>
						<div class="block-content">
							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-called">Called:</label>
								<div class="col-sm-9">
									<select disabled type="text" class="form-control" name="called" id="for-called">
										@foreach($called as $id => $value)
											<option value="{{$id}}" {{ ($request->re_internal_called == $id) ? 'selected':'' }}>{{$value}}</option>
										@endforeach
									</select>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-called_on">Called on:</label>
								<div class="col-sm-9">
                                    <input disabled autocomplete="off" name="called_on" class="form-control" id="datetimepicker2" type="text" value="{{$request->re_internal_called_timestamp}}" />
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label"
								       for="for-internal_remarks">Remarks:</label>
								<div class="col-sm-9">
									<textarea disabled style="height:100px;" type="text" class="form-control" name="internal_remarks" id="for-internal_remarks">{{$request->re_internal_remarks}}</textarea>
								</div>
							</div>
						</div>
					</div>
				</div>

				@if(!empty($request->volumecalculator->voca_volume_calculator) && $request->volumecalculator->voca_volume_calculator != "YTowOnt9"&& $request->volumecalculator->voca_volume_calculator != "czowOiIiOw==")
					<div class="col-lg-6 col-md-12">
						<div class="block block-rounded block-bordered">
							<div class="block-header block-header-default">
								<h3 class="block-title">Volume calculator ({{$request->re_volume_m3}} m<sup>3</sup>)</h3>
							</div>
							<div class="block-content">
								{!! $volumecalculator !!}
							</div>
						</div>
					</div>
				@endif
			</div>

			<!-- Large Default Modal -->
			<div class="modal" id="modal-block-large" tabindex="-1" role="dialog" aria-labelledby="modal-block-large" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="block block-themed block-transparent mb-0">
							<div class="block-header bg-primary-dark">
								<h3 class="block-title">Missing telephone number e-mail</h3>
								<div class="block-options">
									<button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
										<i class="fa fa-fw fa-times"></i>
									</button>
								</div>
							</div>
							<div class="block-content">
								<div id="kekdiffie"><i style="margin-bottom: 50px;margin-top: 50px; margin-left: 300px;" class="fa fa-10x fa-sync fa-spin text-muted"></i></div>
							</div>
							<div class="block-content block-content-full text-right bg-light">
								<button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Close</button>
								<button type="button" class="btn btn-sm btn-primary" id="send-email" >Send E-mail</button>
							</div>
						</div>
					</div>
				</div>
			</div>

			@if($request->re_status != 0)
				<strong>This request has already been matched or rejected, you can't update it any more!</strong>
				<br>
				<br>
				<button type="submit" name="back" class="btn btn-primary">Back</button>
				<br>
			@else
				<div class="form-group col-md-8">
                    <button type="submit" name="save" class="btn btn-primary">Save</button>
                    @if($request->prle_status == 2)
                        <button type="submit" name="match" class="btn btn-primary">Match</button>
                        <button type="submit" name="reject" class="btn btn-primary">Reject</button>
                    @endif
					<a href="/premium_leads"><button type="button" name="back" class="btn btn-primary">Back</button></a>
				</div>
			@endif
		</form>
	</div>

@endsection

@push( 'scripts' )
	<script>

        jQuery( document ).ready( function() {
        	$.datetimepicker.setDateFormatter('moment');
            $('#datetimepicker').datetimepicker();
            $('#datetimepicker2').datetimepicker();

            // On translate
            jQuery( document ).on( 'click', 'button[name="translate"]', function() {

                // Get value
                getTranslation( $('textarea[name="remark"]').val(), function( $result ) {

                    $('div[name=translate_div]').slideDown();
                    $('textarea[name=remark_translation]').val($result);
                    $('button[name=translate]').attr('disabled', 'disabled');
                });
            });

            // On request type change
            $( 'input[name="request_type"]' ).change( function() {

                // Get checked value
                var $requestType = $( 'input[name="request_type"]:checked' ).val();

                // Store moving size element
                var $movingSizeEl = $( 'select[name="moving_size"]' );

                // Get the moving sizes
                getMovingSizes( $requestType, function( $result ) {

                    // Parse the returned json data
                    var $opts = $.parseJSON( $result );

                    // Clear moving sizes
                    $movingSizeEl.empty();

                    // Loop moving sizes
                    $.each( $opts, function( id, value ) {

                        // Append to dropdown
                        $movingSizeEl.append( '<option value="' + id + '">' + value + '</option>' );
                    });
                });
            });

            var $m3El = $( 'input[name="volume_m3"]' );
            var $ftEl = $( 'input[name="volume_ft3"]' );

            // On key up on the m3 element
            $m3El.keyup( function() {

                // Convert comma to dot
                $( this ).val( $( this ).val().replace( /,/g, '.' ) );

                // Convert volume to ft3
                $ftEl.val( convertVolume( $( this ).val(), 'ft3' ) );
            });

            // On key up on the ft3 element
            $ftEl.keyup( function() {

                // Convert comma to dot
                $( this ).val( $( this ).val().replace( /,/g, '.' ) );

                // Convert volume to m3
                $m3El.val( convertVolume( $( this).val(), 'm3' ) );
            });

            var $requestOnHoldEl = $( 'input[name="request_on_hold"]' );
            var $onHoldByEl = $( '.onholdby' );

            // On request on hold change
            $requestOnHoldEl.change(function(){
                // If on hold is checked
                if($(this).is(':checked'))
                {
                    $onHoldByEl.show();
                    $("select[name=on_hold_by]").prop("disabled", false)
                }
                else
                {
                    $onHoldByEl.hide()
                }
            });

            // Trigger initially
            $requestOnHoldEl.trigger('change');

            // Store country selects and destination type
            let $countryFromEl = $( '#for-country_from' );
            let $countryToEl = $( '#for-country_to' );
            var $destinationType = $( 'input[name="destination_type"]' ).data( 'id' );

            $countryFromEl.data("dest", $destinationType);
            $countryToEl.data("dest", $destinationType);

            // On country from change
            $countryFromEl.change( function() {

                if($countryFromEl.val() == $countryToEl.val()){
                    $destinationType = 2;
                }else{
                    $destinationType = 1;
                }

                parseRegions( {
                    destinationType: $destinationType,
                    direction: 'from',
                });

                $(this).data("dest", $destinationType);

                parseRegions({
                    destinationType: $destinationType,
                    direction: 'to',
                });

            });

            // On country to change
            $countryToEl.change( function() {

                if($countryFromEl.val() == $countryToEl.val()){
                    $destinationType = 2;
                }else{
                    $destinationType = 1;
                }

                parseRegions( {
                    destinationType: $destinationType,
                    direction: 'to',
                });

                $(this).data("dest", $destinationType);

                parseRegions({
                    destinationType: $destinationType,
                    direction: 'from',
                });

            });

            // Parse Regions initially
            parseRegions( {
                direction: 'from',
                destinationType: $destinationType,
                initVal: '{{ $request->re_reg_id_from }}'
            }, function()
            {
                parseRegions( {
                    direction: 'to',
                    destinationType: $destinationType,
                    initVal: '{{ $request->re_reg_id_to }}'
                }, function()
                {
                    // Find the regions
                    regionFinder();
                });
            });

            $(".remove_screenshot").click(function(){
            	var re_id = $(this).data('re_id');
            	var screenshot_name = $(this).data('screenshot_name');
            	var divToHide = $(this).parents().eq(1);

                $.ajax({
                    type: "GET",
                    url: '{{ url('/ajax/deletescreenshot') }}',
                    data: {"re_id": re_id, "screenshot_name": screenshot_name},
                    success: function(){
                        divToHide.remove();
                    }
                });
            });

            $("#delete_file").click(function(){
            	var re_id = $(this).data('re_id');

                $.ajax({
                    type: "GET",
                    url: '{{ url('/ajax/deletefile') }}',
                    data: {"re_id": re_id},
                    success: function(data){
                        var fileUploadDiv = $("div.fileupload");
                        var uploadedFileDiv = $("div.uploadedfile");
                        fileUploadDiv.show();
                        uploadedFileDiv.hide();
                    }
                });
            });

            $(".wrong_telephone_number").click(function(){
                $("#send-email").prop("disabled", false);
                var id = $(this).data("id");
                var email = $("input[name=email]").val();
                var telephone_number = $(this).parent().prev().find("input").val();

                $("#kekdiffie").html('<i style="margin-bottom: 50px;margin-top: 50px; margin-left: 300px;" class="fa fa-10x fa-sync fa-spin text-muted"></i>');
                $('#modal-block-large').modal('show');

                $.ajax({
                    type: "GET",
                    url: '{{ url('/ajax/requestwrongtelephonenumber') }}',
                    data: {"function": "check", "id": id, "email": email, "telephone_number": telephone_number},
                    success: function(data){

                        $("#kekdiffie").html(data);

                        email_button = $("#kekdiffie").parent().next().find("#send-email");
                        email_button.attr("id", id);
                        email_button.attr("email", email);
                        email_button.attr("telephone_number", telephone_number);

                    }
                });

                return false;
            });

            $("#send-email").click(function () {

                $this = $(this);
                $(this).attr("disabled", "disabled");

                id = $(this).data("id");
                email = $(this).attr("email");
                telephone_number = $(this).attr("telephone_number");

                $.ajax({
                    type: "GET",
                    url: '{{ url('/ajax/requestwrongtelephonenumber') }}',
                    data: {"function": "send", "id": id, "email": email, "telephone_number": telephone_number},
                    success: function(data){
                        $("#kekdiffie").html('<i style="margin-bottom: 50px;margin-top: 50px; margin-left: 300px;color:green;" class="fa fa-10x fa-check"></i>');
                    }
                });
            });

            var start_request_type = $("input[name=request_type_premium_before]").val();
            var start_on_hold = $("input[name=request_on_hold]").is(":checked");
            var request_type = start_request_type;
            $("input[name=request_type_premium]").change(function(){
                //Set on hold input variable
                var on_hold_input = $("input[name=request_on_hold]");
                if(this.checked)
                {
                    //Premium lead
                    request_type = 2;
                    //Show confirmation
                    if(!confirm('Are you sure you want to make this a Premium lead? The lead will removed from ON HOLD when the lead is on hold.'))
                    {
                        $(this).prop("checked", false);
                    }
                    else
                    {
                        //On hold input disabled and unchecked. Also hide on hold by div
                        on_hold_input.prop("checked", false);
                        on_hold_input.prop("disabled", true);
                        $("div .onholdby").slideUp();
                    }
                }
                else
                {
                    //Normal lead
                    request_type = 1;
                    //Show confirmating 'Are you sure you want to make this a normal lead?'
                    if(!confirm('Are you sure you want to make this a Normal lead?'))
                    {
                        //If cancelled confirmation
                        $(this).prop( "checked", true );
                    }
                    else
                    {
                        //Enable the on hold input when maded this lead normal
                        on_hold_input.prop("disabled", false);
                        $(this).prop("checked", false);
                    }
                }
            });


        });

        $( '.add_to_dialfire' ).click( function() {

            if( ! confirm( 'Are you sure you want to add this to dialfire?' ) )
                return false;

            var $id = $(this).data('id');
            var $type = $(this).data('type');

            $.ajax({
                type: "GET",
                url: '{{ url('/ajax/addtodialfire') }}',
                data: {
                    "id": $id,
                    "type": $type
                },
                success: function(data){
                    response = JSON.parse(data);
                    if(response.success) {
                        alert('Telephone number succesfully added to DialFire! (Can be found at ' + response.dialfire_task + ')');
                    }else{
                        alert(response.error);
                    }
                }
            });

            return false;
        });

        //request_edit_form
        $("#request_edit_form").submit(function(){
            var btn = $(this).find("button[type=submit]:focus");

            if(btn.is($("button[name=match]"))){
                var errors = 0;

                var count = $("select[name=region_from] option").length;

                if($("select[name=region_from]").val() == "" && count > 1){
                    alert("Please select the origin region.");

                    errors++;
                }

                var count = $("select[name=region_to] option").length;

                if($("select[name=region_to]").val() == "" && count > 1){
                    alert("Please select the destination region.");

                    errors++;
                }

                if($("#for-request_spam").is(':checked') || $("#for-request_double").is(':checked')) {
                    alert("You can't match a lead that is marked as SPAM or DOUBLE");

                    errors++;
                }

                if($("#request_spam").is(':checked') || $("#request_double").is(':checked')) {
                    alert("You can't match a lead that is marked as SPAM or DOUBLE");

                    errors++;
                }

                if(errors > 0){
                    return false;
                }
            }
            else if(btn.is($("button[name=save]"))){
                var errors = 0;
                var premiumLeadStatus = $("select[name=premiumlead_status]").val();

                if ($("input[name=planned_date_appointment]").val() == "" && (premiumLeadStatus == 1 || premiumLeadStatus == 2)) {
                    alert("You have to fill in an appointment date before you save the lead with another Lead Status");

                    errors++;
                }

                if(errors > 0){
                    return false;
                }
            }
        });

	</script>

@endpush
