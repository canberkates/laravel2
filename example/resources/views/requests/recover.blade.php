@extends('layouts.backend')
@section('content')
    <div class="content">
        <form class="mb-5" method="post" action="{{action('RequestsController@recoverLead', $request->re_id)}}">
            @csrf
            <input type="hidden" name="re_id" value="{{$request->re_id}}"/>
            <div class="row">
                <div class="col-md-6">
                    <div class="block block-rounded block-bordered">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Recovering Request</h3>
                        </div>
                        <div class="block-content">
                            <p>Are you sure you want to recover the request of <strong>{{$request->re_full_name}}</strong>?
                                <br>
                                The request will be put on hold under your name!
                            </p>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <p>Reason for recovering:</p>
                                </div>
                                <div class="col-sm-7">
                                    <select type="text" class="form-control"
                                            name="recover_reason">
                                        @foreach($recover_reasons as $id => $value)
                                            <option value="{{$id}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary mb-3 mr-1">Yes, recover the lead</button>
                            <a href="/requests/search" class="btn btn-primary mb-3">No</a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection


