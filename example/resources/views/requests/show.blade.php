@extends('layouts.backend')

@include('scripts.select2')
@include('scripts.datepicker')
@include('scripts.region_suggestions')

@section('content')
    <div class="content">
        <input type="hidden" id="setregion" value="0">
        <input type="hidden" id="request_id" value="{{$request->re_id}}">

        @isset($notices['double'])
            @if($request->re_double)
                @foreach($notices['double'] as $notice)
                    <div class="alert alert-danger alert-dismissable" role="alert">
                        <p class="mb-0">{!! $notice !!}</p>
                    </div>
                @endforeach
            @endif
        @endisset

            @csrf
            <input name="_method" type="hidden" value="PATCH">
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <div class="block block-rounded block-bordered">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">General</h3>
                        </div>
                        <div class="block-content">

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="for-received">Received:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" name="received" id="for-received" type="text" value="{{$request->re_timestamp}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="example-hf-email">Status:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text"
                                           value="{{$requeststatuses[$request->re_status]}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label"
                                       for="request_double">Double</label>
                                <div class="col-sm-7">
                                    <div
                                        class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                        <input type="checkbox" class="custom-control-input" id="request_double"
                                               name="request_double"
                                               disabled  {{($request->re_double ? "checked" : "")}}>
                                        <label class="custom-control-label" for="request_double"></label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label"
                                       for="request_spam">Spam</label>
                                <div class="col-sm-7">
                                    <div
                                        class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                        <input type="checkbox" class="custom-control-input" id="request_spam"
                                               name="request_spam"
                                               disabled  {{($request->re_spam ? "checked" : "")}}>
                                        <label class="custom-control-label" for="request_spam"></label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="request_on_hold">On Hold</label>
                                <div class="col-sm-7">
                                    <div
                                        class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                        <input type="checkbox" class="custom-control-input" id="request_on_hold"
                                               name="request_on_hold"
                                               disabled   {{($request->re_on_hold ? "checked" : "")}}>
                                        <label class="custom-control-label" for="request_on_hold"></label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="example-hf-email">Matches left:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text" value="{{$amountmatched}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="example-hf-email">Portal:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text" value="{{$portal}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="example-hf-email">Source:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text" value="{{$requestsource}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="example-hf-email">Form:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text" value="{{$form}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="example-hf-email">Platform source:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text"
                                           value="{{$request->re_platform_source}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="example-hf-email">Category:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text"
                                           value="{{$request->re_category}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="example-hf-email">Language:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text"
                                           value="{{$language}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="example-hf-email">Device:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text" value="{{$device}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="example-hf-email">IP address:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text"
                                           value="{{$request->re_ip_address}}">
                                </div>
                            </div>

                            @isset($notices['ip_address'])
                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label">IP address country:</label>
                                    <div class="col-sm-7" style="color:red">
                                        <label>{!! $notices['ip_address'] !!}</label>
                                    </div>
                                </div>
                            @endisset

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="example-hf-email">Quality score:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text" value="{{$request->re_score}}">
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="block block-rounded block-bordered">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Moving details</h3>
                        </div>
                        <div class="block-content">

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="example-hf-email">Destination type:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" name="destination_type" type="text" data-id="{{$request->re_destination_type}}"  value="{{$destinationtype}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="example-hf-email">Request type:</label>
                                <div class="col-sm-7">
                                    @foreach($requesttypes as $id => $name)
                                        <div class="custom-control custom-radio custom-control-primary">
                                            <input disabled type="radio" class="custom-control-input" id="request_type-{{$id}}" name="request_type" {{ ($request->re_request_type == $id) ? 'checked':'' }}>
                                            <label class="custom-control-label" for="request_type-{{$id}}">{{$name}}</label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="example-hf-email">Moving size:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text" value="{{$movingsizes[$request->re_moving_size]}}">
                                </div>
                            </div>


                            @if($request->re_room_size)
                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="example-hf-email">Room size:</label>
                                    <div class="col-sm-7">
                                        <input disabled class="form-control" type="text" value="{{$request->re_room_size}}{{$request->re_room_size_more ? "+" : ""}} bedroom(s)">
                                    </div>
                                </div>
                            @endif

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="example-hf-email">Moving date:</label>
                                <div class="col-sm-7">
                                    <input disabled autocomplete="off" value="{{$request->re_moving_date}}" type="text"
                                           class="js-datepicker form-control" id="example-datepicker3" name="date"
                                           data-week-start="1" data-autoclose="true" data-today-highlight="true"
                                           data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="example-hf-email">Volume:</label>
                                <div class="col-sm-3">
                                    <input disabled type="text" autocomplete="off" class="form-control"
                                           name="min_volume_m3" value="{{$request->re_volume_m3}}">
                                </div>
                                m3
                                <div class="col-sm-3">
                                    <input disabled type="text" autocomplete="off" class="form-control"
                                           name="min_volume_ft3" value="{{$request->re_volume_ft3}}">
                                </div>
                                ft
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="for-remark">Remarks:</label>
                                <div class="col-sm-7">
                                    <textarea style="height: 100px;" disabled name="remark" id="for-remark" class="form-control" type="text">{{$request->re_remarks}}</textarea>
                                </div>
                            </div>

                            <div class="form-group row" name="translate_div" style="display:none;">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="for-remark_translation">Translated:</label>
                                <div class="col-sm-7">
                                    <textarea style="height: 100px;"  class="form-control" type="text" disabled name="remark_translation" id="for-remark_translation"></textarea>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-1"></div>
                                <div class="col-sm-3"></div>
                                <div class="form-group col-md-8">
                                    <button type="button" name="translate" class="btn btn-sm btn-primary">Translate</button>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label"
                                       for="example-hf-email">Storage:</label>
                                <div class="col-sm-7">
                                    <select disabled type="text" class="form-control"
                                            name="debt_manager">
                                        @foreach($emptyyesno as $id => $value)
                                            <option
                                                value="{{$id}}" {{ ($request->re_storage == $id) ? 'selected':'' }}>{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label"
                                       for="example-hf-email">Packing:</label>
                                <div class="col-sm-7">
                                    <select disabled type="text" class="form-control"
                                            name="debt_manager">
                                        @foreach($byselfcompany as $id => $value)
                                            <option
                                                value="{{$id}}" {{ ($request->re_packing == $id) ? 'selected':'' }}>{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label"
                                       for="example-hf-email">Assembly:</label>
                                <div class="col-sm-7">
                                    <select  disabled type="text" class="form-control"
                                            name="debt_manager">
                                        @foreach($byselfcompany as $id => $value)
                                            <option
                                                value="{{$id}}" {{ ($request->re_assembly == $id) ? 'selected':'' }}>{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label"
                                       for="contact_anonymous">Business</label>
                                <div class="col-sm-7">
                                    <div
                                        class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                        <input type="checkbox" class="custom-control-input"
                                               id="contact_anonymous"
                                               name="contact_anonymous"
                                               disabled
                                            {{($request->re_business ? "checked" : "")}}>
                                        <label class="custom-control-label"
                                               for="contact_anonymous"></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <div class="block block-rounded block-bordered">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Moving from</h3>
                        </div>
                        <div class="block-content">


                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="example-hf-email">Street:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text" value="{{$request->re_street_from}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="example-hf-email">Zipcode:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text" value="{{$request->re_zipcode_from}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="example-hf-email">City:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text" name="city_from"  value="{{$request->re_city_from}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label"
                                       for="example-hf-email">Country:</label>
                                <div class="col-sm-7">
                                    <select disabled type="text" class="form-control"
                                            name="country_from">
                                        <option></option>
                                        @foreach($countries as $id => $country)
                                            <option
                                                    value="{{$id}}" {{ ($request->re_co_code_from ==  $id) ? 'selected':'' }}>{{$country}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label"
                                       for="example-hf-email">Region:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text" value="{{$region_from}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label"
                                       for="region_suggestion_from">Region Suggestion:</label>
                                <div class="col-sm-7">
                                    <div id="region_suggestion_from"></div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label"
                                       for="select_residence_from">Residence:</label>
                                <div class="col-sm-7">
                                    <select disabled type="text" class="form-control"
                                            name="select_residence_from">
                                        @foreach($requestresidences as $id => $residence)
                                            <option
                                                    value="{{$id}}" {{ ($request->re_residence_from ==  $id) ? 'selected':'' }}>{{$residence}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label disabled class="col-sm-3 col-form-label"
                                       for="example-hf-email">Google maps:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text" value="{{$request->re_google_maps_from}}">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="block block-rounded block-bordered">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Moving to</h3>
                        </div>
                        <div class="block-content">

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="example-hf-email">Street:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text" value="{{$request->re_street_to}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="example-hf-email">Zipcode:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text" value="{{$request->re_zipcode_to}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="example-hf-email">City:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text" name="city_to"  value="{{$request->re_city_to}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label"
                                       for="example-hf-email">Country:</label>
                                <div class="col-sm-7">
                                    <select disabled type="text" class="form-control"
                                            name="country_to">
                                        <option></option>
                                        @foreach($countries as $id => $country)
                                            <option value="{{$id}}" {{ ($request->re_co_code_to ==  $id) ? 'selected':'' }}>{{$country}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label"
                                       for="example-hf-email">Region:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text" value="{{$region_to}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label"
                                       for="region_suggestion_to">Region Suggestion:</label>
                                <div class="col-sm-7">
                                    <div id="region_suggestion_to"></div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label"
                                       for="select_residence_to">Residence:</label>
                                <div class="col-sm-7">
                                    <select type="text" class="form-control"
                                            name="select_residence_to">
                                        @foreach($requestresidences as $id => $residence)
                                            <option
                                                    value="{{$id}}" {{ ($request->re_residence_to ==  $id) ? 'selected':'' }}>{{$residence}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label"
                                       for="example-hf-email">Google maps:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text" value="{{$request->re_google_maps_to}}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <div class="block block-rounded block-bordered">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Contact details</h3>
                        </div>
                        <div class="block-content">

                            @if($request->re_company_name)
                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="example-hf-email">Company name:</label>
                                    <div class="col-sm-7">
                                        <input disabled class="form-control" type="text" value="{{$request->re_company_name}}">
                                    </div>
                                </div>
                            @endif

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="example-hf-email">Full name:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text" value="{{$request->re_full_name}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="example-hf-email">Telephone 1:</label>
                                <div class="col-sm-4">
                                    <input disabled class="form-control" type="text" value="{{$request->re_telephone1}}">
                                </div>
                                <div class="col-sm-4"></div>
                                @if ($request->re_co_code_from == $request->re_co_code_to)
                                    @if ($isValid_from || $isValid_to)
                                        <div class="form-group row">
                                            <label class="col-sm-12" style="color:green">This phone number seems to be valid!</label>
                                        </div>
                                    @else
                                        <div class="form-group row">
                                            <label class="col-sm-12" style="color:red">This phone number is NOT valid!</label>
                                        </div>
                                    @endif
                                @else
                                    @if ($isValid_from)
                                        <div class="form-group row">
                                            <label class="col-sm-12" style="color:green">This phone number seems to be valid!</label>
                                        </div>
                                    @endif

                                    @if ($isValid_to && !$isValid_from)
                                        <div class="form-group row">
                                            <label class="col-sm-12" style="color:green">This phone number seems to be valid!</label>
                                        </div>
                                    @endif

                                    @if (!$isValid_from && !$isValid_to && $isValid_ip)
                                            <div class="form-group row">
                                                <label class="col-sm-12" style="color:green">This phone number seems to be valid!</label>
                                            </div>
                                    @endif

                                    @if (!$isValid_from && !$isValid_to && !$isValid_ip)
                                        <div class="form-group row">
                                            <label class="col-sm-12" style="color:red">This phone number is NOT valid!</label>
                                        </div>
                                    @endif
                                @endif
                            </div>
                            @if($request->re_telephone_1_mail)
                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="for-mail_sent_telephone_1">Mail sent for telephone 1:</label>
                                    <div class="col-sm-4">
                                        <div class="custom-control custom-switch custom-control custom-control-inline custom-control-primary">
                                            <input disabled type="checkbox" class="custom-control-input" id="for-mail_sent_telephone_1" name="mail_sent_telephone_1" {{($request->re_telephone_1_mail ? "checked" : "")}}>
                                            <label class="custom-control-label" for="for-mail_sent_telephone_1"></label>
                                        </div>
                                    </div>
                                </div>
                            @endif

                            @if($request->re_telephone2)
                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="example-hf-email">Telephone 2:</label>
                                    <div class="col-sm-4">
                                        <input disabled class="form-control" type="text" value="{{$request->re_telephone2}}">
                                    </div>
                                    @isset($notices['phone_2'])
                                        <label class="col-sm-3" style="color:red">This phone number is NOT
                                            valid!</label>
                                    @endisset
                                </div>
                                @if($request->re_telephone_2_mail)
                                    <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <label class="col-sm-3 col-form-label" for="for-mail_sent_telephone_2">Mail sent for telephone 2:</label>
                                        <div class="col-sm-4">
                                            <div class="custom-control custom-switch custom-control custom-control-inline custom-control-primary">
                                                <input disabled type="checkbox" class="custom-control-input" id="for-mail_sent_telephone_2" name="mail_sent_telephone_2" {{($request->re_telephone_2_mail ? "checked" : "")}}>
                                                <label class="custom-control-label" for="for-mail_sent_telephone_2"></label>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endif

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="example-hf-email">Email:</label>
                                <div class="col-sm-7">
                                    <input disabled class="form-control" type="text" value="{{$request->re_email}}">
                                </div>
                            </div>

                            @isset($notices['email_bounce'])
                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label">Bounce:</label>
                                    <div class="col-sm-7" style="color:red">
                                        <label>{!! $notices['email_bounce'] !!}</label>
                                    </div>
                                </div>
                            @endisset
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="block block-rounded block-bordered">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Internal information</h3>
                        </div>
                        <div class="block-content">


                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label"
                                       for="example-hf-email">Called:</label>
                                <div class="col-sm-7">
                                    <select disabled type="text" class="form-control"
                                            name="debt_manager">
                                        @foreach($called as $id => $value)
                                            <option
                                                value="{{$id}}" {{ ($request->re_internal_called == $id) ? 'selected':'' }}>{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label"
                                       for="example-hf-email">Called on:</label>
                                <div class="col-sm-7">
                                    <input disabled autocomplete="off" value="{{$request->re_internal_called_timestamp}}"
                                           type="text" class="js-datepicker form-control" id="example-datepicker3"
                                           name="date" data-week-start="1" data-autoclose="true"
                                           data-today-highlight="true" data-date-format="yyyy-mm-dd"
                                           placeholder="yyyy-mm-dd">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label"
                                       for="example-hf-email">Remarks:</label>
                                <div class="col-sm-7">
                                    <textarea disabled type="text" class="form-control"
                                              name="company_name_legal">{{$request->re_internal_remarks}}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="block block-rounded block-bordered">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Mover Portal</h3>
                        </div>
                        <div class="block-content">
                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label"
                                       for="example-hf-email">Extra information:</label>
                                <div class="col-sm-7">
                                    <textarea rows="4" type="text" class="form-control"
                                              name="extra_information_text">{{$request->re_extra_information_text}}</textarea>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="button" name="add_extra_info" class="btn btn-primary">Add extra information to the lead</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="block block-rounded block-bordered">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Companies considered for this lead</h3>
                        </div>
                        <div class="block-content">
                            <div class="form-group row">
                                <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Customer</th>
                                        <th>Matched</th>
                                        <th>Capping</th>
                                        <th>Link</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($matches as $match)
                                        <tr>
                                            <td>{{$match->cu_id}}</td>
                                            <td>{{$match->cu_company_name_business}}</td>
                                            <td>Yes</td>
                                            <td></td>
                                            <td>
                                                <a href="/customers/{{$match->cu_id}}/edit">
                                                    <button type="button" class="btn btn-sm btn-primary" data-toggle="tooltip"
                                                            title="Edit">
                                                        <i class="fa fa-pen"></i>
                                                    </button>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    @foreach ($capping_statistics as $capping)
                                        <tr>
                                            <td>{{$capping->cu_id}}</td>
                                            <td>{{$capping->cu_company_name_business}}</td>
                                            <td>No</td>
                                            @if($capping->mast_capping_limitation == 1)
                                                <td>Capping on Mover level (Montly Spend/Leads)</td>
                                            @elseif($capping->mast_capping_limitation == 2)
                                                <td>Capping on Portal level</td>
                                            @elseif($capping->mast_capping_limitation == 3)
                                                <td>Forced daily capping</td>
                                            @else
                                                <td></td>
                                            @endif
                                            <td>
                                                <a href="/customers/{{$capping->cu_id}}/edit">
                                                    <button type="button" class="btn btn-sm btn-primary" data-toggle="tooltip"
                                                            title="Edit">
                                                        <i class="fa fa-pen"></i>
                                                    </button>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            @if(!empty($request->volumecalculator->voca_volume_calculator) && $request->volumecalculator->voca_volume_calculator != "YTowOnt9" && $request->volumecalculator->voca_volume_calculator != "czowOiIiOw==")
                <div class="row">
                    <div class="col-md-6">
                        <div class="block block-rounded block-bordered">
                            <div class="block-header block-header-default">
                                <h3 class="block-title">Volume calculator ({{$request->re_volume_m3}} m<sup>3</sup>)</h3>
                            </div>
                            <div class="block-content">
                                {!! $volumecalculator !!}
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            <div class="form-group col-md-8">
                <button type="submit" name="back" class="btn btn-primary">Back</button>
            </div>
    </div>


@endsection

@push( 'scripts' )
    <script>
        jQuery(document).ready(function(){
            jQuery( document ).on( 'click', 'button[name=add_extra_info]', function(e) {

                var string = $('textarea[name=extra_information_text]').val();

                jQuery.ajax({
                    type: "GET",
                    url: '{{ url('/ajax/addrequestextrainfo') }}',
                    data: {'string': string, 'request_id' : $('#request_id').val()},
                    success: function() {
                        $('textarea[name=extra_information_text]').attr("disabled", "disabled");
                    },
                    error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    }
                });
            });

            // On translate
            jQuery( document ).on( 'click', 'button[name="translate"]', function() {

                // Get value
                getTranslation( $('textarea[name="remark"]').val(), function( $result ) {

                    $('div[name=translate_div]').slideDown();
                    $('textarea[name=remark_translation]').val($result);
                    $('button[name=translate]').attr('disabled', 'disabled');
                });
            });

        });


    </script>

@endpush


