@extends('layouts.backend')

@include('scripts.select2')
@include('scripts.datepicker')

@section('content')
    <div class="content">
        @isset($notices['double'])
            @if($request->re_double)
                @foreach($notices['double'] as $notice)
                    <div class="alert alert-dismissable" role="alert">
                        <p class="mb-0">{!! $notice !!}</p>
                    </div>
                @endforeach
            @endif
        @endisset

        <form class="mb-5" method="post" action="{{action('RequestsController@rejectlead', $request->re_id)}}">
            @csrf
            <div class="row">
                <div class="col-md-10">
                    <div class="block block-rounded block-bordered">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Contact details</h3>
                        </div>
                        <div class="block-content">

                            @if($request->re_company_name)
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label" for="example-hf-email">Company name:</label>
                                    <div class="col-sm-10">
                                        <input disabled class="form-control" type="text" value="{{$request->re_company_name}}">
                                    </div>
                                </div>
                            @endif

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label" for="example-hf-email">Full name:</label>
                                <div class="col-sm-10">
                                    <input disabled class="form-control" type="text" value="{{$request->re_full_name}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label" for="example-hf-email">Telephone 1:</label>
                                <div class="col-sm-10">
                                    <input disabled class="form-control" type="text" value="{{$request->re_telephone1}}">
                                </div>
                            </div>

                            @if($request->re_telephone2)
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label" for="example-hf-email">Telephone 2:</label>
                                    <div class="col-sm-10">
                                        <input disabled class="form-control" type="text" value="{{$request->re_telephone2}}">
                                    </div>
                                </div>
                            @endif

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label" for="example-hf-email">Email:</label>
                                <div class="col-sm-10">
                                    <input disabled class="form-control" type="text" value="{{$request->re_email}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label" for="example-hf-email">Moving size:</label>
                                <div class="col-sm-10">
                                    <input disabled class="form-control" type="text" value="{{App\Data\MovingSize::name($request->re_moving_size)}}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10">
                    <div class="block block-rounded block-bordered">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Reject the lead</h3>
                        </div>
                        <div class="block-content">


                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label"
                                       for="example-hf-email">Reason:</label>
                                <div class="col-sm-10">
                                    <select type="text" class="js-select2 form-control"
                                            name="rejection_reason">
                                        @foreach($rejectionreasons as $id => $value)
                                            <option value="{{explode(";",$id)[0]}}" class="{{explode(";",$id)[1]}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label"
                                       for="example-hf-email">Send email:</label>
                                <div class="col-sm-10">
                                    <select type="text" class="form-control"
                                            name="send_email">
                                        @foreach($yesno as $id => $value)
                                            <option @if($id) selected @endif value="{{$id}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>

            @if($request->re_status != 0)
                <strong>This request has already been matched or rejected, you can't update it any more!</strong>
                <br>
                <br> <a href="../{{$request->re_id}}/edit"><button type="button" class="btn btn-primary">Back</button></a>
                <br>
            @else
                <div class="form-group col-md-8">
                    <button type="submit" name="reject" class="btn btn-primary">Reject</button>
                   <a href="../{{$request->re_id}}/edit"><button type="button" class="btn btn-primary">Back</button></a>
                </div>
            @endif
        </form>
    </div>


@endsection

@push( 'scripts' )
    <script>
        jQuery(document).ready(function(){
            $("select[name=rejection_reason]").change(function(){
                if($(this).find("option:selected").attr("class") == "valid"){
                    $("select[name=send_email]").val(1).removeAttr("disabled");
                }
                else if($(this).find("option:selected").attr("class") == "unvalid"){
                    $("select[name=send_email]").val(0).attr("disabled", "disabled");
                }
            });
        });
    </script>
@endpush


