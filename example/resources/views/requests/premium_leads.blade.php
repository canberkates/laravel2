@extends('layouts.backend')

@include( 'scripts.datatables' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Premium leads</h1>
                <div class="btn-group">
                    <a href="{{ url('requests/search')}}" class="btn btn-outline-secondary">
                        Request search
                    </a>
                    <a href="{{ url( 'serviceproviderrequests') }}" class="btn btn-outline-secondary">
                        Service provider requests ({{$sp_requests_count}})
                    </a>
                    @can('requests - match check')
                    <a href="{{ url( 'requests/rate') }}" class="btn btn-outline-secondary">
                       Rate matches
                    </a>
                    @endcan
                    <a href="{{ url( 'matchsummary') }}" class="btn btn-outline-secondary">
                        Match summary
                    </a>
                </div>

                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url( 'dashboard' ) }}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Premium Leads</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <div class="block block-rounded block-bordered">
            <ul class="nav nav-tabs nav-tabs-block js-tabs-enabled" data-tab-control="1" data-toggle="tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" href="#tab-make_appointment">Make Appointment ({{sizeof($requests['make_appointment'])}})</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#tab-planned_for_survey">Planned for Survey ({{sizeof($requests['planned_for_survey'])}})</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#tab-ready_for_matching">Ready for Matching ({{sizeof($requests['ready_for_matching'])}})</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#tab-matched">Matched ({{sizeof($requests['matched'])}})</a>
                </li>
            </ul>
            <div class="block-content tab-content">
                <div class="tab-pane active" id="tab-make_appointment" role="tabpanel">
                    <div class="block block-rounded block-bordered">
                        <div class="block-content block-content-full">
                            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Received</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Country from</th>
                                    <th>Country to</th>
                                    <th>Request type</th>
                                    <th>Moving size</th>
                                    <th>Moving date</th>
                                    <th>Volume (m3)</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($requests['make_appointment'] as $request)
                                    <tr>
                                        <td>{{$request->re_id}}</td>
                                        <td>{{$request->re_timestamp}}</td>
                                        <td>{{$request->re_full_name}}</td>
                                        <td>{{$request->re_email}}</td>
                                        @if($request->re_co_code_from)
                                            <td>{{$countries[$request->re_co_code_from]}}</td>
                                        @else
                                            <td></td>
                                        @endif
                                        @if($request->re_co_code_to)
                                            <td>{{$countries[$request->re_co_code_to]}}</td>
                                        @else
                                            <td></td>
                                        @endif
                                        @if($request->re_request_type)
                                            <td>{{$requesttypes[$request->re_request_type]}}</td>
                                        @else
                                            <td></td>
                                        @endif
                                        @if($request->re_moving_size)
                                            <td>{{$movingsizes[$request->re_moving_size]}}</td>
                                        @else
                                            <td></td>
                                        @endif
                                        <td>{{$request->re_moving_date}}</td>
                                        <td>{{$request->re_volume_m3}}</td>
                                        <td class="text-center">
                                            <div class="btn-group">
                                                <a class="btn btn-sm btn-primary"
                                                   data-toggle="tooltip"
                                                   data-placement="left"
                                                   title="edit"
                                                   href="{{ url('/premium_leads/' . $request->re_id. '/edit')}}">
                                                    <i class="fa fa-pencil-alt"></i>
                                                </a>
                                            </div>
                                            <div class="btn-group">
                                                <a class="btn btn-sm btn-primary"
                                                   data-toggle="tooltip"
                                                   data-placement="left"
                                                   title="on hold"
                                                   href="{{ url('/premium_leads/' . $request->re_id. '/edit')}}">
                                                    <i class="fa fa-pause"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab-planned_for_survey" role="tabpanel">
                    <div class="block block-rounded block-bordered">
                        <div class="block-content block-content-full">
                            <table data-order='[[1, "asc"]]' class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Planned timestamp</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Country from</th>
                                    <th>Country to</th>
                                    <th>Request type</th>
                                    <th>Moving size</th>
                                    <th>Moving date</th>
                                    <th>Volume (m3)</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($requests['planned_for_survey'] as $request)
                                    <tr>
                                        <td>{{$request->re_id}}</td>
                                        <td>{{$request->prle_planned_appointment_timestamp}}</td>
                                        <td>{{$request->re_full_name}}</td>
                                        <td>{{$request->re_email}}</td>
                                        @if($request->re_co_code_from)
                                            <td>{{$countries[$request->re_co_code_from]}}</td>
                                        @else
                                            <td></td>
                                        @endif
                                        @if($request->re_co_code_to)
                                            <td>{{$countries[$request->re_co_code_to]}}</td>
                                        @else
                                            <td></td>
                                        @endif
                                        <td>{{$requesttypes[$request->re_request_type]}}</td>
                                        <td>{{$movingsizes[$request->re_moving_size]}}</td>
                                        <td>{{$request->re_moving_date}}</td>
                                        <td>{{$request->re_volume_m3}}</td>
                                        <td class="text-center">
                                            <div class="btn-group">
                                                <a class="btn btn-sm btn-primary"
                                                   data-toggle="tooltip"
                                                   data-placement="left"
                                                   title="edit"
                                                   href="{{ url('/premium_leads/' . $request->re_id. '/edit')}}">
                                                    <i class="fa fa-pencil-alt"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab-ready_for_matching" role="tabpanel">
                    <div class="block block-rounded block-bordered">
                        <div class="block-content block-content-full">
                            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Received</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Country from</th>
                                    <th>Country to</th>
                                    <th>Request type</th>
                                    <th>Moving size</th>
                                    <th>Moving date</th>
                                    <th>Volume (m3)</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($requests['ready_for_matching'] as $request)
                                    <tr>
                                        <td>{{$request->re_id}}</td>
                                        <td>{{$request->re_timestamp}}</td>
                                        <td>{{$request->re_full_name}}</td>
                                        <td>{{$request->re_email}}</td>
                                        @if($request->re_co_code_from)
                                            <td>{{$countries[$request->re_co_code_from]}}</td>
                                        @else
                                            <td></td>
                                        @endif
                                        @if($request->re_co_code_to)
                                            <td>{{$countries[$request->re_co_code_to]}}</td>
                                        @else
                                            <td></td>
                                        @endif
                                        <td>{{$requesttypes[$request->re_request_type]}}</td>
                                        <td>{{$movingsizes[$request->re_moving_size]}}</td>
                                        <td>{{$request->re_moving_date}}</td>
                                        <td>{{$request->re_volume_m3}}</td>
                                        <td class="text-center">
                                            <div class="btn-group">
                                                <a class="btn btn-sm btn-primary"
                                                   data-toggle="tooltip"
                                                   data-placement="left"
                                                   title="edit"
                                                   href="{{ url('/premium_leads/' . $request->re_id. '/edit')}}">
                                                    <i class="fa fa-pencil-alt"></i>
                                                </a>
                                            </div>
                                            <div class="btn-group">
                                                <a class="btn btn-sm btn-primary"
                                                   data-toggle="tooltip"
                                                   data-placement="left"
                                                   href="{{ url('/premium_leads/' . $request->re_id. '/generate')}}">
                                                    <i class="fa fa-download"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab-matched" role="tabpanel">
                    <div class="block block-rounded block-bordered">
                        <div class="block-content block-content-full">
                            <table data-order='[[1, "desc"]]' class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Matched</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Country from</th>
                                    <th>Country to</th>
                                    <th>Request type</th>
                                    <th>Moving size</th>
                                    <th>Moving date</th>
                                    <th>Volume (m3)</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($requests['matched'] as $request)
                                    <tr>
                                        <td>{{$request->re_id}}</td>
                                        <td>{{$request->requestcustomerportal->ktrecupo_timestamp}}</td>
                                        <td>{{$request->re_full_name}}</td>
                                        <td>{{$request->re_email}}</td>
                                        @if($request->re_co_code_from)
                                            <td>{{$countries[$request->re_co_code_from]}}</td>
                                        @else
                                            <td></td>
                                        @endif
                                        @if($request->re_co_code_to)
                                            <td>{{$countries[$request->re_co_code_to]}}</td>
                                        @else
                                            <td></td>
                                        @endif
                                        <td>{{$requesttypes[$request->re_request_type]}}</td>
                                        <td>{{$movingsizes[$request->re_moving_size]}}</td>
                                        <td>{{$request->re_moving_date}}</td>
                                        <td>{{$request->re_volume_m3}}</td>
                                        <td class="text-center">
                                            {{--<div class="btn-group">
                                                <a class="btn btn-sm btn-primary"
                                                   data-toggle="tooltip"
                                                   data-placement="left"
                                                   title="view"
                                                   href="{{ url('/premium_leads/' . $request->re_id. '/edit')}}">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                            </div>--}}
                                            <div class="btn-group">
                                                <a class="btn btn-sm btn-primary"
                                                   data-toggle="tooltip"
                                                   data-placement="left"
                                                   href="{{ url('/premium_leads/' . $request->re_id. '/generate')}}">
                                                    <i class="fa fa-download"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
