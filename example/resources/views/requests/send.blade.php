@extends('layouts.backend')

@section('content')
    <div class="content">
        <div class="block block-rounded block-bordered">
            <ul class="nav nav-tabs nav-tabs-block js-tabs-enabled" data-toggle="tabs"
                role="tablist">
                <li class="nav-item">
                    <a class="nav-link active show" href="#btabs-static-mover">Mover</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#btabs-static-user">User</a>
                </li>
            </ul>
            <div class="block-content tab-content">
                <div class="tab-pane active show" id="btabs-static-mover" role="tabpanel">
                    <form method="post" action="{{action('RequestsController@sendlead', $request_id)}}">
                            @csrf
                            <input name="_method" type="hidden" value="post">
                            <input name="form" type="hidden" value="mover">

                            <div class="col-md-6 block block-rounded block-bordered">
                                <div class="block-content">
                                    <input name="_method" type="hidden" value="post">

                                    <h2 class="content-heading pt-0">General</h2>

                                    <div class="form-group row">
                                        <div class="col-md-1"></div>
                                        <label class="col-md-3 col-form-label">Receiver:</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="receiver">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-md-1"></div>
                                        <label class="col-md-3 col-form-label">Extra:</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="extra">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-md-1"></div>
                                        <label class="col-md-3 col-form-label">Language:</label>
                                        <div class="col-md-8">
                                            <select data-placeholder="Select some options..." class="js-select2 form-control"
                                                    name="language" style="width:100%;">
                                                @foreach($languages as $language)
                                                    <option value="{{$language->la_code}}" @if($language->la_code == 'EN') selected @endif>{{$language->la_language}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-md-1"></div>
                                        <label class="col-md-3 col-form-label">Request ID:</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="request_id" >
                                        </div>
                                    </div>

                                    <h2 class="content-heading pt-0">Request delivery</h2>

                                    <div class="form-group row">
                                        <div class="col-md-1"></div>
                                        <label class="col-md-3 col-form-label">Type:</label>
                                        <div class="col-md-8">
                                            <select data-placeholder="Select some options..." class="js-select2 form-control"
                                                    name="type" style="width:100%;">
                                                @foreach($requestdeliveries as $id => $requestdelivery)
                                                    <option value="{{$id}}">{{$requestdelivery}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>


                                    <h2 class="content-heading pt-0"></h2>

                                    <div class="row">
                                        <div class="form-group col-md-2">
                                            <button type="submit" class="btn btn-primary">Send E-mail</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </form>
                </div>
                <div class="tab-pane" id="btabs-static-user" role="tabpanel">
                    <form method="post" action="{{action('RequestsController@sendlead', $request_id)}}">
                            @csrf
                            <input name="_method" type="hidden" value="post">
                            <input name="form" type="hidden" value="user">

                            <div class="col-md-6 block block-rounded block-bordered">
                                <div class="block-content">
                                    <input name="_method" type="hidden" value="post">

                                    <h2 class="content-heading pt-0">Online user e-mail</h2>

                                    <div class="form-group row">
                                        <div class="col-md-1"></div>
                                        <label class="col-md-3 col-form-label">E-mail:</label>
                                        <div class="col-md-8">
                                            <select data-placeholder="Select some options..."
                                                    class="js-select2 form-control"
                                                    name="type" style="width:100%;">
                                                @foreach($usermailtypes as $id => $type)
                                                    <option value="{{$id}}">{{$type}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>


                                    <h2 class="content-heading pt-0"></h2>

                                    <div class="row">
                                        <div class="form-group col-md-2">
                                            <button type="submit" class="btn btn-primary">Send E-mail</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </form>
                </div>
            </div>
        </div>
    </div>
@endsection




