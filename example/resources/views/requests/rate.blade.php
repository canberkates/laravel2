@extends('layouts.backend')

@include( 'scripts.datatables' )
@include( 'scripts.dialogs' )
@include( 'scripts.datepicker' )
@include( 'scripts.select2' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Rate matches</h1>
                @if (isset($rated_requests) && count($rated_requests) > 0)
                    <a class="btn btn-sm btn-success"
                       data-toggle="tooltip"
                       data-placement="left"
                       href="{{ url('requests/sync_feedback')}}">
                        Sync feedback to users
                    </a>
                @endif
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        <div class="block block-rounded block-bordered">
            <div class="block-content ">
                <form method="post" action="{{action('RequestRateController@filteredindex')}}">
                    @csrf
                    <input name="_method" type="hidden" value="post">
                    <h2 class="content-heading pt-0">Filters</h2>

                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="for-date">Date range:</label>
                        <div class="col-md-5">
                            <input type="text" class="form-control drp drp-default" name="date" id="for-date" value="{{$selected_date}}" autocomplete="off"/>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Status:</label>
                        <div class="col-md-5">
                            <select class="form-control" name="status" data-placeholder="Choose one.." style="width:100%;">
                                @foreach($statuses as $id => $type)
                                    <option @if ($selected_status == $id) selected @endif value="{{$id}}">{{$type}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Employee:</label>
                        <div class="col-md-5">
                            <select class="js-select2 form-control" name="employee" style="width:100%;">
                                <option></option>
                                @foreach($users as $user)
                                    <option @if ($selected_employee == $user->id) selected @endif value="{{$user->id}}">{{$user->us_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>


                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Rating:</label>
                        <div class="col-md-5">
                            <select  class="form-control" name="rating" data-placeholder="Choose one.." style="width:100%;">
                                <option></option>
                                @foreach($ratings as $id => $type)
                                    <option @if ($selected_rating == $id) selected @endif value="{{$id}}">{{$type}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>


                    <h2 class="content-heading pt-0"></h2>

                    <div class="row">
                        <div class="form-group col-md-2">
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="block block-rounded block-bordered">

        <!-- Your Block -->
            @if($portalrequests)
                <div class="block-content block-content-full">
                    <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Received</th>
                            <th>Matched</th>
                            <th>Matched by</th>
                            <th>Mistake type</th>
                            <th>Major/minor mistakes</th>
                            <th>Name</th>
                            <th>E-mail</th>
                            <th>Country from</th>
                            <th>Country to</th>
                            <th>Request type</th>
                            <th>Moving size</th>
                            <th>Moving date</th>
                            <th>Vol (m3)</th>
                            <th>Rate</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($portalrequests as $request)
                            <tr>
                                <td>{{$request->re_id}}</td>
                                <td>{{$request->re_timestamp}}</td>
                                <td>{{$request->ktrecupo_timestamp}}</td>
                                <td>{{$request->matchedby}}</td>
                                <td>{{$matchmistaketypes[$request->re_match_rating][0]}}</td>
                                <td>{{$ratings[$matchmistaketypes[$request->re_match_rating][1]]}}</td>
                                <td>{{$request->re_full_name}}</td>
                                <td>{{$request->re_email}}</td>
                                @if($request->country_from)
                                    <td>{{$request->country_from}}</td>
                                @else
                                    <td>Unknown</td>
                                @endif
                                @if($request->country_to)
                                    <td>{{$request->country_to}}</td>
                                @else
                                    <td>Unknown</td>
                                @endif
                                <td>{{$requesttypes[$request->re_request_type]}}</td>
                                <td>{{$movingsizes[$request->re_moving_size]}}</td>
                                <td>{{$request->re_moving_date}}</td>
                                <td>{{$request->re_volume_m3}}</td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <a class="btn btn-sm btn-primary"
                                           data-toggle="tooltip"
                                           data-placement="left"
                                           title="edit"
                                           href="{{ url('requests/' . $request->re_id . "/rate/". ((count($portalrequests) == 1) ? "-1" : $toberated))}}">
                                            <i class="fa fa-pencil-alt"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            @else
                <div class="block block-rounded block-bordered">
                    <div class="block-content block-content-full">
                        <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Received</th>
                                <th>Rejected</th>
                                <th>Rejected by</th>
                                <th>Rejection reason</th>
                                <th>Mistake type</th>
                                <th>Major/minor mistakes</th>
                                <th>Name</th>
                                <th>E-mail</th>
                                <th>Country from</th>
                                <th>Country to</th>
                                <th>Request type</th>
                                <th>Moving size</th>
                                <th>Moving date</th>
                                <th>Vol (m3)</th>
                                <th>Rate</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($requests as $request)
                                <tr>
                                    <td>{{$request->re_id}}</td>
                                    <td>{{$request->re_timestamp}}</td>
                                    <td>{{$request->re_rejection_timestamp}}</td>
                                    <td>{{$request->rejectedby->us_name}}</td>
                                    <td>{{$rejectionreasons[$request->re_rejection_reason][0]}}</td>
                                    <td>{{$matchmistaketypes[$request->re_match_rating][0]}}</td>
                                    <td>{{$ratings[$matchmistaketypes[$request->re_match_rating][1]]}}</td>
                                    <td>{{$request->re_full_name}}</td>
                                    <td>{{$request->re_email}}</td>
                                    @if($request->countryfrom)
                                        <td>{{$request->countryfrom->co_en}}</td>
                                    @else
                                        <td>Unknown</td>
                                    @endif
                                    @if($request->countryto)
                                        <td>{{$request->countryto->co_en}}</td>
                                    @else
                                        <td>Unknown</td>
                                    @endif
                                    <td>{{$requesttypes[$request->re_request_type]}}</td>
                                    <td>{{$request->re_moving_size}}</td>
                                    <td>{{$request->re_moving_date}}</td>
                                    <td>{{$request->re_volume_m3}}</td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <a class="btn btn-sm btn-primary"
                                               data-toggle="tooltip"
                                               data-placement="left"
                                               title="edit"
                                               href="{{ url('requests/' . $request->re_id . "/rate/". ((count($requests) == 1) ? "-1" : $toberated))}}">
                                            <i class="fa fa-pencil-alt"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @endif
        <!-- END Your Block -->
        </div>
    </div>

@endsection
