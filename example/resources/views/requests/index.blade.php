@extends('layouts.backend')

@include( 'scripts.datatables' )
@include( 'scripts.dialogs' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <div style="width: 40%">
                    <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Requests </h1>
                    <br>
                    <small style="font-size: 15px;">({{$requests['automatic']}} in the automatic match process)</small>
                </div>
                <div class="btn-group">
                    <a href="{{ url('requests/search')}}" class="btn btn-outline-secondary">
                        Request search
                    </a>
                    <a href="{{ url( 'serviceproviderrequests') }}" class="btn btn-outline-secondary">
                        Service provider requests ({{$sp_requests_count}})
                    </a>
                    @can('requests - match check')
                    <a href="{{ url( 'requests/rate') }}" class="btn btn-outline-secondary">
                       Rate matches
                    </a>
                    @endcan
                    @can('requests anonymize')
                        <a href="{{ url('/anonymize') }}" class="btn btn-outline-secondary">
                            Anonymize
                        </a>
                    @endcan
                    <a href="{{ url( 'matchsummary') }}" class="btn btn-outline-secondary">
                        Match summary
                    </a>
                </div>

                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url( 'dashboard' ) }}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Requests</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">
        @if (isset($feedback_first) && !empty($feedback_first))
            <div class="alert alert-danger">
                <strong>You have recently received matching feedback. Please read it before you continue matching.</strong>
                <a class="btn btn-sm btn-danger" href="/requests/{{$feedback_first}}/feedback/{{((isset($feedback_requests) && !empty($feedback_requests)) ? $feedback_requests: "-1")}}">
                    Click here to view!
                </a>
            </div>
        @endif
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <div class="block block-rounded block-bordered">
            <ul class="nav nav-tabs nav-tabs-block js-tabs-enabled" data-tab-control="1" data-toggle="tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" href="#tab-leads">Leads ({{sizeof($requests['leads'])}})</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#tab-double">Double ({{sizeof($requests['double'])}})</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#tab-spam">Spam ({{sizeof($requests['spam'])}})</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#tab-on_hold">On hold ({{sizeof($requests['on_hold'])}})</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#tab-pd_submitted">PD Submitted ({{sizeof($requests['pd_submitted'])}})</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#tab-auto_reject">Automatically Rejected (0)</a>
                </li>
            </ul>
            <div class="block-content tab-content">
                <div class="tab-pane active" id="tab-leads" role="tabpanel">
                    <div class="block block-rounded block-bordered">
                        <div class="block-content block-content-full">
                            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Received</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Country from</th>
                                    <th>Country to</th>
                                    <th>Request type</th>
                                    <th>Moving size</th>
                                    <th>Moving date</th>
                                    <th>Volume (m3)</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($requests['leads'] as $request)
                                    <tr>
                                        <td>{{$request->re_id}}</td>
                                        <td>{{$request->re_timestamp}}</td>
                                        <td>{{$request->re_full_name}}</td>
                                        <td>{{$request->re_email}}</td>
                                        @if($request->re_co_code_from)
                                            <td>{{$countries[$request->re_co_code_from]}}</td>
                                        @else
                                            <td></td>
                                        @endif
                                        @if($request->re_co_code_to)
                                            <td>{{$countries[$request->re_co_code_to]}}</td>
                                        @else
                                            <td></td>
                                        @endif
                                        @if($request->re_request_type)
                                            <td>{{$requesttypes[$request->re_request_type]}}</td>
                                        @else
                                            <td></td>
                                        @endif
                                        @if($request->re_moving_size)
                                            <td>{{$movingsizes[$request->re_moving_size]}}</td>
                                        @else
                                            <td></td>
                                        @endif
                                        <td>{{$request->re_moving_date}}</td>
                                        <td>{{$request->re_volume_m3}}</td>
                                        <td class="text-center">
                                            <div class="btn-group">
                                                <a class="btn btn-sm btn-primary"
                                                   data-toggle="tooltip"
                                                   data-placement="left"
                                                   title="edit"
                                                   href="{{ url('/requests/' . $request->re_id. '/edit')}}">
                                                    <i class="fa fa-pencil-alt"></i>
                                                </a>
                                            </div>
                                            <div class="btn-group">
                                                <a class="btn btn-sm btn-primary"
                                                   data-toggle="tooltip"
                                                   data-placement="left"
                                                   title="on hold"
                                                   href="{{ url('/requests/' . $request->re_id. '/edit')}}">
                                                    <i class="fa fa-pause"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab-double" role="tabpanel">
                    <div class="block block-rounded block-bordered">
                        <div class="block-content block-content-full">
                            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Received</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Country from</th>
                                    <th>Country to</th>
                                    <th>Request type</th>
                                    <th>Moving size</th>
                                    <th>Moving date</th>
                                    <th>Volume (m3)</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($requests['double'] as $request)
                                    <tr>
                                        <td>{{$request->re_id}}</td>
                                        <td>{{$request->re_timestamp}}</td>
                                        <td>{{$request->re_full_name}}</td>
                                        <td>{{$request->re_email}}</td>
                                        @if($request->re_co_code_from)
                                            <td>{{$countries[$request->re_co_code_from]}}</td>
                                        @else
                                            <td></td>
                                        @endif
                                        @if($request->re_co_code_to)
                                            <td>{{$countries[$request->re_co_code_to]}}</td>
                                        @else
                                            <td></td>
                                        @endif
                                        <td>{{$requesttypes[$request->re_request_type]}}</td>
                                        <td>{{$movingsizes[$request->re_moving_size]}}</td>
                                        <td>{{$request->re_moving_date}}</td>
                                        <td>{{$request->re_volume_m3}}</td>
                                        <td class="text-center">
                                            <div class="btn-group">
                                                <a class="btn btn-sm btn-primary"
                                                   data-toggle="tooltip"
                                                   data-placement="left"
                                                   title="edit"
                                                   href="{{ url('/requests/' . $request->re_id. '/edit')}}">
                                                    <i class="fa fa-pencil-alt"></i>
                                                </a>
                                            </div>
                                            <div class="btn-group">
                                                <a class="btn btn-sm btn-primary"
                                                   data-toggle="tooltip"
                                                   data-placement="left"
                                                   title="on hold"
                                                   href="{{ url('/requests/' . $request->re_id. '/edit')}}">
                                                    <i class="fa fa-pause"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab-spam" role="tabpanel">
                    <div class="block block-rounded block-bordered">
                        <div class="block-content block-content-full">
                            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Received</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Country from</th>
                                    <th>Country to</th>
                                    <th>Request type</th>
                                    <th>Moving size</th>
                                    <th>Moving date</th>
                                    <th>Volume (m3)</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($requests['spam'] as $request)
                                    <tr>
                                        <td>{{$request->re_id}}</td>
                                        <td>{{$request->re_timestamp}}</td>
                                        <td>{{$request->re_full_name}}</td>
                                        <td>{{$request->re_email}}</td>
                                        @if($request->re_co_code_from)
                                            <td>{{$countries[$request->re_co_code_from]}}</td>
                                        @else
                                            <td></td>
                                        @endif
                                        @if($request->re_co_code_to)
                                            <td>{{$countries[$request->re_co_code_to]}}</td>
                                        @else
                                            <td></td>
                                        @endif
                                        <td>{{$requesttypes[$request->re_request_type]}}</td>
                                        <td>{{$movingsizes[$request->re_moving_size]}}</td>
                                        <td>{{$request->re_moving_date}}</td>
                                        <td>{{$request->re_volume_m3}}</td>
                                        <td class="text-center">
                                            <div class="btn-group">
                                                <a class="btn btn-sm btn-primary"
                                                   data-toggle="tooltip"
                                                   data-placement="left"
                                                   title="edit"
                                                   href="{{ url('/requests/' . $request->re_id. '/edit')}}">
                                                    <i class="fa fa-pencil-alt"></i>
                                                </a>
                                            </div>
                                            <div class="btn-group">
                                                <a class="btn btn-sm btn-primary"
                                                   data-toggle="tooltip"
                                                   data-placement="left"
                                                   title="on hold"
                                                   href="{{ url('/requests/' . $request->re_id. '/edit')}}">
                                                    <i class="fa fa-pause"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab-on_hold" role="tabpanel">
                    <div class="block block-rounded block-bordered">
                        <ul class="nav nav-tabs nav-tabs-block js-tabs-enabled" data-toggle="tabs" role="tablist">
                            @foreach($on_hold_requests_per_users as $id => $request_sizes)
                                <li class="nav-item">
                                    <a class="nav-link @if ($loop->first) active @endif"
                                       href="#btabs-static-{{$id}}">@if ($request_sizes[0]['onholdby']['us_name'] == "") {{"System"}} @else {{$request_sizes[0]['onholdby']['us_name']}} @endif
                                        ({{sizeof($request_sizes)}})</a>
                                </li>
                            @endforeach
                        </ul>
                        <div class="block-content tab-content">
                            @foreach($on_hold_requests_per_users as $id => $on_hold_requests)
                                <div class="tab-pane @if ($loop->first) active @endif" id="btabs-static-{{$id}}" role="tabpanel">
                                    <div class="block block-rounded block-bordered">
                                        <div class="block-content block-content-full">
                                            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                                <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Received</th>
                                                    <th>Name</th>
                                                    <th>Email</th>
                                                    <th>Country from</th>
                                                    <th>Country to</th>
                                                    <th>Request type</th>
                                                    <th>Moving size</th>
                                                    <th>Moving date</th>
                                                    <th>Volume (m3)</th>
                                                    <th>Actions</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach ($on_hold_requests as $request)
                                                    <tr>
                                                        <td>{{$request->re_id}}</td>
                                                        <td>{{$request->re_timestamp}}</td>
                                                        <td>{{$request->re_full_name}}</td>
                                                        <td>{{$request->re_email}}</td>
                                                        @if($request->re_co_code_from)
                                                            <td>{{$countries[$request->re_co_code_from]}}</td>
                                                        @else
                                                            <td></td>
                                                        @endif
                                                        @if($request->re_co_code_to)
                                                            <td>{{$countries[$request->re_co_code_to]}}</td>
                                                        @else
                                                            <td></td>
                                                        @endif
                                                        <td>{{$requesttypes[$request->re_request_type]}}</td>
                                                        @if($request->re_moving_size)
                                                            <td>{{$movingsizes[$request->re_moving_size]}}</td>
                                                        @else
                                                            <td></td>
                                                        @endif
                                                        <td>{{$request->re_moving_date}}</td>
                                                        <td>{{$request->re_volume_m3}}</td>
                                                        <td class="text-center">
                                                            <div class="btn-group">
                                                                <a class="btn btn-sm btn-primary"
                                                                   data-toggle="tooltip"
                                                                   data-placement="left"
                                                                   title="edit"
                                                                   href="{{ url('/requests/' . $request->re_id. '/edit')}}">
                                                                    <i class="fa fa-pencil-alt"></i>
                                                                </a>
                                                            </div>
                                                            <div class="btn-group">
                                                                <a class="btn btn-sm btn-primary"
                                                                   data-toggle="tooltip"
                                                                   data-placement="left"
                                                                   title="Restore"
                                                                   href="{{ url('/requests/' . $request->re_id. '/restore')}}">
                                                                    <i class="fa fa-redo-alt"></i>
                                                                </a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab-pd_submitted" role="tabpanel">
                    <div class="block block-rounded block-bordered">
                        <div class="block-content block-content-full">
                            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Received</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Country from</th>
                                    <th>Country to</th>
                                    <th>Request type</th>
                                    <th>Moving size</th>
                                    <th>Moving date</th>
                                    <th>Volume (m3)</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($requests['pd_submitted'] as $request)
                                    <tr>
                                        <td>{{$request->re_id}}</td>
                                        <td>{{$request->re_timestamp}}</td>
                                        <td>{{$request->re_full_name}}</td>
                                        <td>{{$request->re_email}}</td>
                                        @if($request->re_co_code_from)
                                            <td>{{$countries[$request->re_co_code_from]}}</td>
                                        @else
                                            <td></td>
                                        @endif
                                        @if($request->re_co_code_to)
                                            <td>{{$countries[$request->re_co_code_to]}}</td>
                                        @else
                                            <td></td>
                                        @endif
                                        <td>{{$requesttypes[$request->re_request_type]}}</td>
                                        <td>{{$movingsizes[$request->re_moving_size]}}</td>
                                        <td>{{$request->re_moving_date}}</td>
                                        <td>{{$request->re_volume_m3}}</td>
                                        <td class="text-center">
                                            <div class="btn-group">
                                                <a class="btn btn-sm btn-primary"
                                                   data-toggle="tooltip"
                                                   data-placement="left"
                                                   title="edit"
                                                   href="{{ url('/requests/' . $request->re_id. '/edit')}}">
                                                    <i class="fa fa-pencil-alt"></i>
                                                </a>
                                            </div>
                                            <div class="btn-group">
                                                <a class="btn btn-sm btn-primary"
                                                   data-toggle="tooltip"
                                                   data-placement="left"
                                                   title="on hold"
                                                   href="{{ url('/requests/' . $request->re_id. '/edit')}}">
                                                    <i class="fa fa-pause"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab-auto_reject" role="tabpanel">
                    <div class="block block-rounded block-bordered">
                        <div class="block-content block-content-full">
                            <table data-order='[[0, "desc"]]' class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Received</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Country from</th>
                                    <th>Country to</th>
                                    <th>Moving date</th>
                                    <th>Rejection reason</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                {{-- @foreach ($requests['auto_reject'] as $request)
                                    <tr>
                                        <td>{{$request->re_id}}</td>
                                        <td>{{$request->re_timestamp}}</td>
                                        <td>{{$request->re_full_name}}</td>
                                        <td>{{$request->re_email}}</td>
                                        @if($request->re_co_code_from)
                                            <td>{{$countries[$request->re_co_code_from]}}</td>
                                        @else
                                            <td></td>
                                        @endif
                                        @if($request->re_co_code_to)
                                            <td>{{$countries[$request->re_co_code_to]}}</td>
                                        @else
                                            <td></td>
                                        @endif
                                        <td>{{$request->re_moving_date}}</td>
                                        <td>{{$rejectionreasons[$request->re_rejection_reason][0]}}</td>
                                        <td class="text-center">
                                            <div class="btn-group">
                                                <a class="btn btn-sm btn-primary"
                                                   data-toggle="tooltip"
                                                   data-placement="left"
                                                   title="show"
                                                   href="{{ url('/requests/' . $request->re_id)}}">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                            </div>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-sm btn-primary automatic_checked" data-toggle="tooltip"
                                                        title="Approve" data-request_id="{{$request->re_id}}">
                                                    <i class="fa fa-check"></i>
                                                </button>
                                            </div>
                                            <div class="btn-group">
                                                <a class="btn btn-sm btn-primary"
                                                   data-toggle="tooltip"
                                                   data-placement="left"
                                                   title="Recover request"
                                                   href="{{ url('/requests/' . $request->re_id. '/recover')}}">
                                                    <i class="fa fa-redo"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach --}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        jQuery(document).on('click', '.automatic_checked', function (e) {
            e.preventDefault();

            var $self = jQuery(this);
            confirmDelete("{{ url('ajax/approveautomaticchecked') }}", 'get', {id: $self.data('request_id')}, function () {
                $self.parents('.dataTable').DataTable().row($self.parents('tr')).remove().draw('page');
            }, "Yes, remove from the list.");
        });
    </script>
@endpush
