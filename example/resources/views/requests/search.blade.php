@extends('layouts.backend')

@include( 'scripts.datatables' )
@include( 'scripts.datepicker' )
@include('scripts.select2')

@section('content')
	<!-- Hero -->
	<div class="bg-body-light">
		<div class="content content-full">
			<div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
				<h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Request search</h1>
			</div>
		</div>
	</div>
	<!-- END Hero -->

	<div class="content">
		<form method="post" action="{{action('RequestSearchController@searchResult')}}">
			@csrf
			<input name="_method" type="hidden" value="post">
			<div class="col-md-6 block block-rounded block-bordered">
				<div class="block-content">
					<input name="_method" type="hidden" value="post">
					<h2 class="content-heading pt-0">Filters</h2>

					<div class="form-group row">

						<div class="col-md-1"></div>
						<label class="col-md-3 col-form-label" for="for-date">Date range:</label>
						<div class="col-md-8">
							<input type="text" class="form-control drp drp-default" name="date" id="for-date"
							       value="{{$selected_date}}" autocomplete="off"/>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-md-1"></div>
						<label class="col-md-3 col-form-label" for="for-request_id">Request ID:</label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="request_id" id="for-request_id"
							       value="{{$selected_request_id}}">
						</div>
					</div>

					<div class="form-group row">
						<div class="col-md-1"></div>
						<label class="col-md-3 col-form-label" for="for-name">Name:</label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="name" id="for-name"
							       value="{{$selected_name}}">
						</div>
					</div>

					<div class="form-group row">
						<div class="col-md-1"></div>
						<label class="col-md-3 col-form-label" for="for-email">E-mail:</label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="email" id="for-email"
							       value="{{$selected_email}}">
						</div>
					</div>

					<div class="form-group row">
						<div class="col-md-1"></div>
						<label class="col-md-3 col-form-label" for="for-ip_address">IP Address:</label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="ip_address" id="for-ip_address"
							       value="{{$selected_ip_address}}">
						</div>
					</div>

					<h2 class="content-heading pt-0">Move information</h2>

					<div class="form-group row">
						<div class="col-md-1"></div>
						<label class="col-md-3 col-form-label" for="for-zipcode_from">Zipcode from:</label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="zipcode_from" id="for-zipcode_from"
							       value="{{$selected_zipcode_from}}">
						</div>
					</div>

					<div class="form-group row">
						<div class="col-md-1"></div>
						<label class="col-md-3 col-form-label" for="for-city_from">City from:</label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="city_from" id="for-city_from"
							       value="{{$selected_city_from}}">
						</div>
					</div>

					<div class="form-group row">
						<div class="col-md-1"></div>
						<label class="col-md-3 col-form-label" for="for-country_from">Country from:</label>
						<div class="col-md-8">
							<select id="for-country_from" data-placeholder="Select some options..." multiple
							        class="js-select2 form-control" name="country_from[]" style="width:100%;">
								<option></option>
								@foreach($countries as $country)
									<option value="{{$country->co_code}}"
									        @if (in_array($country->co_code, $selected_country_from)) selected @endif>{{$country->co_en}}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-md-1"></div>
						<label class="col-md-3 col-form-label" for="for-zipcode_to">Zipcode to:</label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="zipcode_to" id="for-zipcode_to"
							       value="{{$selected_zipcode_to}}">
						</div>
					</div>

					<div class="form-group row">
						<div class="col-md-1"></div>
						<label class="col-md-3 col-form-label" for="for-city_to">City to:</label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="city_to" id="for-city_to"
							       value="{{$selected_city_to}}">
						</div>
					</div>

					<div class="form-group row">
						<div class="col-md-1"></div>
						<label class="col-md-3 col-form-label" for="for-country_to">Country to:</label>
						<div class="col-md-8">
							<select data-placeholder="Select some options..." multiple class="js-select2 form-control"
							        name="country_to[]" id="for-country_to" style="width:100%;">
								<option></option>
								@foreach($countries as $country)
									<option value="{{$country->co_code}}"
									        @if (in_array($country->co_code, $selected_country_to)) selected @endif>{{$country->co_en}}</option>
								@endforeach
							</select>
						</div>
					</div>

					<h2 class="content-heading pt-0">Request filters</h2>

					<div class="form-group row">
						<div class="col-md-1"></div>
						<label class="col-md-3 col-form-label" for="for-size">Request type:</label>
						<div class="col-md-8">
							<select type="text" name="request_type" class="form-control" style="width:100%;">
								<option value="">Both</option>
								<option @if($selected_request_type == 1) selected @endif value="1">Requests</option>
								<option @if($selected_request_type == 2) selected @endif value="2">Premium leads</option>
							</select>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-md-1"></div>
						<label class="col-md-3 col-form-label" for="for-size">Moving size:</label>
						<div class="col-md-8">
							<select multiple data-placeholder="Select some options..." class="js-select2 form-control"
							        name="size[]" id="for-size" style="width:100%;">
								<option></option>
								@foreach($movingsizes as $id => $X)
									<option value="{{$id}}"
									        @if (in_array($id, $selected_size)) selected @endif>{{$X}}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-md-1"></div>
						<label class="col-md-3 col-form-label" for="for-rejection_reason">Rejection reasons:</label>
						<div class="col-md-8">
							<select multiple data-placeholder="Select some options..." class="js-select2 form-control"
							        name="rejection_reason[]" id="for-rejection_reason" style="width:100%;">
								<option></option>
								@foreach($rejectionreasons as $id => $X)
									<option value="{{$id}}"
									        @if (in_array($id, $selected_rejection_reason)) selected @endif>{{$X[0]}}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-md-1"></div>
						<label class="col-md-3 col-form-label" for="for-rejected_by">Rejected by:</label>
						<div class="col-md-8">
							<select multiple data-placeholder="Select some options..." class="js-select2 form-control"
							        name="rejected_by[]" id="for-rejected_by" style="width:100%;">
								<option></option>
								@foreach($users as $user)
									<option value="{{$user->id}}"
									        @if (in_array($user->id, $selected_rejected)) selected @endif>{{$user->us_name}}</option>
								@endforeach
							</select>
						</div>
					</div>

                    <div class="form-group row">
                        <div class="col-md-1"></div>
                        <label class="col-md-3 col-form-label" for="for-size">Destination type:</label>
                        <div class="col-md-8">
                            <select type="text" name="destination_type" class="form-control" style="width:100%;">
                                <option value="">Both</option>
                                <option @if($selected_destination_type == 1) selected @endif value="1">INTMOVING</option>
                                <option @if($selected_destination_type == 2) selected @endif value="2">NATMOVING</option>
                            </select>
                        </div>
                    </div>

					<h2 class="content-heading pt-0">Source filters</h2>

					<div class="form-group row">
						<div class="col-md-1"></div>
						<label class="col-md-3 col-form-label" for="for-website">Website:</label>
						<div class="col-md-8">
							<select class="js-select2 form-control" multiple data-placeholder="Select some options..."
							        name="website[]" id="for-website" style="width:100%;">
								<option></option>
								@foreach($websites as $id => $website)
									<option value="{{$id}}" @if (in_array($id, $selected_website)) selected @endif >{{$website}}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-md-1"></div>
						<label class="col-md-3 col-form-label" for="for-affiliate">Affiliate partner:</label>
						<div class="col-md-8">
							<select class="js-select2 form-control" multiple data-placeholder="Select some options..."
							        name="affiliate[]" id="for-affiliate" style="width:100%;">
								<option></option>

								@foreach($affiliates as $affiliate)

									<option value="{{$affiliate->afpafo_id}}"
									        @if (in_array($affiliate->afpafo_id, $selected_affiliate)) selected @endif> {{$affiliate->afpafo_name}} @if($affiliate->afpafo_form_type == 3) {{"(".$affiliate->cu_company_name_business.")"}} @else {{"(".$affiliate->afpafo_la_code.")"}} @endif</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-md-1"></div>
						<label class="col-md-3 col-form-label" for="for-source">Platform source:</label>
						<div class="col-md-8">
							<select multiple data-placeholder="Select some options..." class="js-select2 form-control"
							        name="source[]" id="for-source" style="width:100%;">
								<option></option>
								@foreach($platforms as $id => $X)
									<option value="{{$id}}"
									        @if (in_array($id, $selected_source)) selected @endif>{{$X}}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-md-1"></div>
						<label class="col-md-3 col-form-label" for="for-category">Category:</label>
						<div class="col-md-8">
							<select multiple data-placeholder="Select some options..." class="js-select2 form-control"
							        name="category[]" id="for-category" style="width:100%;">
								<option></option>
								@foreach($categories as $id => $X)
									<option value="{{$id}}"
									        @if (in_array($id, $selected_category)) selected @endif>{{$X}}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-md-1"></div>
						<label class="col-md-3 col-form-label" for="for-device">Device:</label>
						<div class="col-md-8">
							<select class="js-select2 form-control" multiple data-placeholder="Select some options..."
									name="device[]" id="for-device" style="width:100%;">
								<option></option>
								@foreach($devices as $id => $device)
									<option value="{{$id}}" @if (in_array($id, $selected_devices)) selected @endif >{{$device}}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-md-1"></div>
						<label class="col-md-3 col-form-label" for="pd_resubmit">Personal Dashboard:</label>
						<div class="col-md-8">
							<select class="js-select2 form-control" data-placeholder="Select some options..."
									name="pd_resubmit" id="pd_resubmit" style="width:100%;">
                                <option @if ($selected_pd_resubmit == 3) selected @endif value="3">Both</option>
                                <option @if ($selected_pd_resubmit == 1) selected @endif value="1">Yes</option>
                                <option @if ($selected_pd_resubmit == 2) selected @endif value="2">No</option>
							</select>
						</div>
					</div>

					<h2 class="content-heading pt-0"></h2>

					<div class="row">
						<div class="form-group col-md-2">
							<button type="submit" class="btn btn-primary">Filter</button>
						</div>
					</div>
				</div>
			</div>

		</form>

		@isset($requests)
			<div class="block block-rounded block-bordered">
				<ul class="nav nav-tabs nav-tabs-block js-tabs-enabled" data-toggle="tabs"
				    role="tablist">
					<li class="nav-item">
						<a class="nav-link active show" href="#btabs-static-open">Open
							({{sizeof($requests['open'])}})</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#btabs-static-matched">Matched
							({{sizeof($requests['matched'])}})</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#btabs-static-rejected">Rejected
							({{sizeof($requests['rejected'])}})</a>
					</li>
				</ul>
				<div class="block-content tab-content">
					<div class="tab-pane active show" id="btabs-static-open" role="tabpanel">
						<div class="block block-rounded block-bordered">
							<div class="block-content block-content-full">
								<table class="table table-bordered table-striped table-vcenter js-dataTable-full">
									<thead>
										<tr>
											<th>ID</th>
											<th>Received</th>
											<th>Name</th>
											<th>E-mail</th>
											<th>Country from</th>
											<th>Country to</th>
											<th>Request type</th>
											<th>Vol (m3)</th>
											<th>Device</th>
											<th>Origins</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
									@foreach ($requests['open'] as $request)
										<tr>
											<td>{{$request->re_id}}</td>
											<td>{{$request->re_timestamp}}</td>
											<td>{{$request->re_full_name}}</td>
											<td>{{$request->re_email}}</td>
											@isset($request->countryfrom)
												<td>{{$request->countryfrom->co_en}}</td>
											@else
												<td>Unknown</td>
											@endisset
											@isset($request->countryfrom)
												<td>{{$request->countryto->co_en}}</td>
											@else
												<td>Unknown</td>
											@endisset
											<td>{{$requesttypes[$request->re_request_type]}}</td>
											<td>{{$request->re_volume_m3}}</td>
											<td>{{$devices[$request->re_device]}}</td>
											@if($request->websiteform)
												<td>{{$request->websiteform->website->we_website}}</td>
											@elseif($request->affiliateform)
												<td>{{$request->affiliateform->afpafo_name}}</td>
                                            @else
                                                <td></td>
											@endif
											<td class="text-center">
												<div class="btn-group">
													<a class="btn btn-sm btn-primary"
													   data-toggle="tooltip"
													   data-placement="left"
													   title="Edit request"
													   href="{{ url('requests/' . $request->re_id . '/edit')}}">
														<i class="fa fa-pencil-alt"></i>
													</a>
												</div>
											</td>
										</tr>
									@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="btabs-static-matched" role="tabpanel">
						<div class="block block-rounded block-bordered">
							<div class="block-content block-content-full">
								<table class="table table-bordered table-striped table-vcenter js-dataTable-full">
									<thead>
										<tr>
											<th>Matches</th>
											<th>Matches left</th>
											<th>ID</th>
											<th>Received</th>
											<th>Name</th>
											<th>E-mail</th>
											<th>Country from</th>
											<th>Country to</th>
											<th>Request type</th>
											<th>Vol (m3)</th>
											<th>Device</th>
											<th>Origins</th>
											<th>Actions
										</tr>
									</thead>
									<tbody>
									@foreach ($requests['matched'] as $request)
										<tr>
											<td class="text-center">
												<button type="button" class="btn btn-sm btn-primary details_show_blank" data-toggle="tooltip"
												        title="Expand"
												        data-details="{{$request->details}}">
													<i class="fa fa-plus"></i>
												</button>
											</td>
											<td>{{$request->matches_left}}</td>
											<td>{{$request->re_id}}</td>
											<td>{{$request->re_timestamp}}</td>
											<td>{{$request->re_full_name}}</td>
											<td>{{$request->re_email}}</td>
											@isset($request->countryfrom)
												<td>{{$request->countryfrom->co_en}}</td>
											@else
												<td>Unknown</td>
											@endisset
											@isset($request->countryfrom)
												<td>{{$request->countryto->co_en}}</td>
											@else
												<td>Unknown</td>
											@endisset
											<td>{{$requesttypes[$request->re_request_type]}}</td>
											<td>{{$request->re_volume_m3}}</td>
											<td>{{$devices[$request->re_device]}}</td>
                                            @if($request->websiteform)
                                                <td>{{$request->websiteform->website->we_website}}</td>
                                            @elseif($request->affiliateform)
                                                <td>{{$request->affiliateform->afpafo_name}}</td>
                                            @else
                                                <td></td>
                                            @endif
											<td class="text-center">
												<div class="btn-group">
													<a class="btn btn-sm btn-primary"
													   data-toggle="tooltip"
													   data-placement="left"
													   title="Send request"
													   href="{{ url('requests/' . $request->re_id . "/send")}}">
														<i class="fa fa-envelope"></i>
													</a>
												</div>
												<div class="btn-group">
													<a class="btn btn-sm btn-primary"
													   data-toggle="tooltip"
													   data-placement="left"
													   title="View request"
                                                       @if($request->re_request_type == 5)
                                                           href="{{ url('premium_leads/' . $request->re_id)}}">
                                                       @else
                                                           href="{{ url('requests/' . $request->re_id)}}">
                                                       @endif
														<i class="fa fa-eye"></i>
													</a>
												</div>
											</td>
										</tr>
									@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="btabs-static-rejected" role="tabpanel">
						<div class="block block-rounded block-bordered">
							<div class="block-content block-content-full">
								<table class="table table-bordered table-striped table-vcenter js-dataTable-full">
									<thead>
									<tr>
										<th>ID</th>
										<th>Received</th>
										<th>Name</th>
										<th>E-mail</th>
										<th>Country from</th>
										<th>Country to</th>
										<th>Request type</th>
										<th>Vol (m3)</th>
										<th>Device</th>
										<th>Origins</th>
										<th>Rejected by</th>
										<th>Rejection reason</th>
										<th>Actions</th>
									</tr>
									</thead>
									<tbody>
									@foreach ($requests['rejected'] as $request)
										<tr>
											<td>{{$request->re_id}}</td>
											<td>{{$request->re_timestamp}}</td>
											<td>{{$request->re_full_name}}</td>
											<td>{{$request->re_email}}</td>
											@if($request->countryfrom)
												<td>{{$request->countryfrom->co_en}}</td>
											@else
												<td>Unknown</td>
											@endif
											@if($request->countryto)
												<td>{{$request->countryto->co_en}}</td>
											@else
												<td>Unknown</td>
											@endif
											<td>{{$requesttypes[$request->re_request_type]}}</td>
											<td>{{$request->re_volume_m3}}</td>
											<td>{{$devices[$request->re_device]}}</td>
											@if($request->websiteform)
												<td>{{$request->websiteform->website->we_website}}</td>
											@elseif($request->affiliateform)
												<td>{{$request->affiliateform->afpafo_name}}</td>
											@else
												<td>Unknown</td>
											@endif
                                            @if($request->rejectedby)
											<td>{{$request->rejectedby->us_name}}</td>
                                            @else
                                                <td>Unknown</td>
                                            @endif
											<td>{{$rejectionreasons[$request->re_rejection_reason][0]}}</td>

											<td class="text-center">
                                                <div class="btn-group">
                                                    <a class="btn btn-sm btn-primary"
                                                       data-toggle="tooltip"
                                                       data-placement="left"
                                                       title="Send request"
                                                       href="{{ url('requests/' . $request->re_id . "/send")}}">
                                                        <i class="fa fa-envelope"></i>
                                                    </a>
                                                </div>
												<div class="btn-group">
													<a class="btn btn-sm btn-primary"
													   data-toggle="tooltip"
													   data-placement="left"
													   title="Recover request"
													   href="{{ url('requests/' . $request->re_id.'/recover')}}">
														<i class="fa fa-redo"></i>
													</a>
												</div>
												<div class="btn-group">
													<a class="btn btn-sm btn-primary"
													   data-toggle="tooltip"
													   data-placement="left"
													   title="View request"
                                                       @if($request->re_request_type == 5)
                                                           href="{{ url('premium_leads/' . $request->re_id)}}">
                                                       @else
                                                           href="{{ url('requests/' . $request->re_id)}}">
                                                       @endif
														<i class="fa fa-eye"></i>
													</a>
												</div>
											</td>
										</tr>
									@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		@endisset
	</div>
@endsection
