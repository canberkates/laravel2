@extends('layouts.backend')

@include('scripts.select2')
@include('scripts.datepicker')

@include('scripts.region_suggestions')

@section('content')
	<div class="content">
		@if($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach($errors->all() as $error)
						{!! $error !!}<br>
					@endforeach
				</ul>
			</div>
		@endif

		@isset($notices['double'])
			@if($request->re_double)
				@foreach($notices['double'] as $notice)
					<div class="alert alert-dismissable" role="alert">
						<p class="mb-0">{!! $notice !!}</p>
					</div>
				@endforeach
			@endif
		@endisset

        @if (!empty($rejected_before_reason))
                <div class="alert alert-info" role="alert">
                    <p class="mb-0">{!! $rejected_before_reason !!}</p>
                </div>
        @endif

        @if (!empty($request_update_remarks))
                <div class="alert alert-info" role="alert">
                    <b>The user submitted the following remarks:</b>
                    <p class="mb-0">{!! $request_update_remarks !!}</p>
                </div>
        @endif

        @if (!empty($request_update_fields) && $request->re_pd_validate == 1)
                <div class="alert alert-info" role="alert">
                    <b>The following fields are changed:</b>
                    <ul>
                        @foreach ($request_update_fields as $key => $field)
                            <li>{{$key}} to {{$field}}</li>
                        @endforeach
                    </ul>
                </div>
        @endif

		<form class="mb-5 form-small" id="request_edit_form" method="post" action="{{action('RequestsController@update', $request->re_id)}}">
			@csrf
			<input name="_method" type="hidden" value="PATCH">

			<div class="row justify-content-center">
				<div class="col-lg-6 col-md-12">
					<div class="block block-rounded block-bordered">
						<div class="block-header block-header-default">
							<h3 class="block-title">General</h3>
						</div>
						<div class="block-content">

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-received">Received:</label>
								<div class="col-sm-9">
									<input disabled class="form-control" name="received" id="for-received" type="text" value="{{$request->re_timestamp}}">
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-request_status">Status:</label>
								<div class="col-sm-9">
									<input disabled class="form-control" id="for-request_status" type="text" value="{{$requeststatuses[$request->re_status]}}">
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-request_double">Double</label>
								<div class="col-sm-9">
									<div class="custom-control custom-switch custom-control custom-control-inline custom-control-primary">
										<input type="checkbox" class="custom-control-input" id="for-request_double" name="request_double" {{($request->re_double ? "checked" : "")}}>
										<label class="custom-control-label" for="for-request_double"></label>
									</div>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-request_spam">Spam</label>
								<div class="col-sm-9">
									<div class="custom-control custom-switch custom-control custom-control-inline custom-control-primary">
										<input type="checkbox" class="custom-control-input" id="for-request_spam" name="request_spam" {{($request->re_spam ? "checked" : "")}}>
										<label class="custom-control-label" for="for-request_spam"></label>
									</div>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-request_on_hold">On Hold</label>
								<div class="col-sm-9">
									<div class="custom-control custom-switch custom-control custom-control-inline custom-control-primary">
                                        <input @if ($request->re_request_type == 5) disabled @endif type="checkbox" class="custom-control-input" id="for-request_on_hold" name="request_on_hold" {{($request->re_on_hold ? "checked" : "")}}>
										<label class="custom-control-label" for="for-request_on_hold"></label>
									</div>
								</div>
							</div>

							<div class="form-group row onholdby" style="display: none;">
								<label class="col-sm-3 col-form-label" for="for-on_hold_by">On hold by:</label>
								<div class="col-sm-9">
									<select type="text" class="form-control" name="on_hold_by" id="for-on_hold_by">
										<option value="">System</option>
										@foreach($users as $user)
											<option value="{{$user->id}}" @if($request->re_on_hold == 0 && $user->id == \Illuminate\Support\Facades\Auth::id()) {{'selected'}} @endif {{ ($request->re_on_hold_by == $user->id) ? 'selected':'' }}>{{$user->us_name}}</option>
										@endforeach
									</select>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-amountmatched">Matches left:</label>
								<div class="col-sm-9">
									<input disabled class="form-control" type="text" id="for-amountmatched" value="{{$amountmatched}}">
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-portal">Portal:</label>
								<div class="col-sm-9">
									<input disabled class="form-control" type="text" id="for-portal" value="{{$portal}}">
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-requestsource">Source:</label>
								<div class="col-sm-9">
									<input disabled class="form-control" type="text" id="for-requestsource" value="{{$requestsource}}">
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-form">Form:</label>
								<div class="col-sm-9">
									<input disabled class="form-control" type="text" id="for-form" value="{{$form}}">
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-platform_source">Platform source:</label>
								<div class="col-sm-9">
									<input disabled class="form-control" type="text" id="for-platform_source" value="{{$request->re_platform_source}}">
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-request_platform_source">Category:</label>
								<div class="col-sm-9">
									<input disabled class="form-control" type="text" id="for-request_platform_source" value="{{$request->re_category}}">
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-language">Language:</label>
								<div class="col-sm-9">
									<select type="text" class="form-control" name="language" id="for-language">
										@foreach($languages as $language)
											<option value="{{$language->la_code}}" {{ ($request->re_la_code == $language->la_code) ? 'selected':'' }}>{{$language->la_language}}</option>
										@endforeach
									</select>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-device">Device:</label>
								<div class="col-sm-9">
									<input disabled class="form-control" type="text" id="for-device" value="{{$device}}">
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-ip_address">IP address:</label>
								@if (!empty($request->re_ip_address_country))
									<div class="col-sm-3" style="align-self: center;"><img src="/media/flags/{{strtolower($request->re_ip_address_country)}}.png"/> {{$countries[$request->re_ip_address_country]}}</div>
								@endif
								<div class="col-sm-6">
									<input disabled class="form-control" type="text" id="for-ip_address" value="{{$request->re_ip_address}}">
								</div>

							</div>

							@isset($notices['ip_address'])
								<div class="form-group row">
									<div class="col-sm-11" style="color:red;">
										<label>{!! $notices['ip_address'] !!}</label>
									</div>
								</div>
							@endisset

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-re_score">Quality score:</label>
								<div class="col-sm-9">
									<input disabled class="form-control" type="text" id="for-re_score" value="{{$request->re_score}}">
								</div>
							</div>

						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-12">
					<div class="block block-rounded block-bordered">
						<div class="block-header block-header-default">
							<h3 class="block-title">Moving details</h3>
						</div>
						<div class="block-content">

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-destination_type">Destination type:</label>
								<div class="col-sm-9">
									<input disabled class="form-control" name="destination_type" id="for-destination_type" data-id="{{$request->re_destination_type}}" type="text" value="{{$destinationtype}}">
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Request type:</label>
								<div class="col-sm-9">
									@foreach($requesttypes as $id => $name)
										@if($id == 4) @continue; @endif
                                        @if($request->re_sirelo_customer == 1 || $request->re_exclusive_match == 1 && $id == 5) {{"Premium request type is disabled, because this lead is an exclusive match or a free lead for a Sirelo customer."}} @continue; @endif
										<div class="custom-control custom-radio custom-control-primary">
											<input type="radio" class="custom-control-input" value="{{$id}}" id="request_type-{{$id}}" name="request_type" {{ ($request->re_request_type == $id) ? 'checked':'' }}>
											<label class="custom-control-label" for="request_type-{{$id}}">{{$name}}</label>
										</div>
									@endforeach
								</div>
							</div>

                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label"
                                       for="moving_size">Moving size:</label>
                                <div class="col-sm-9">
                                    <select type="text" class="form-control"
                                            name="moving_size">
                                        @foreach($movingsizes as $id => $value)
                                            <option value="{{$id}}" {{ ($request->re_moving_size == $id) ? 'selected':'' }}>{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                            @if($request->re_room_size)

                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label"
                                           for="moving_size">Room size:</label>
                                    <div class="col-sm-9">
                                        <select type="text" class="form-control"
                                                name="room_size" disabled>
                                            @foreach($roomsizes as $id => $value)
                                                <option value="{{$id}}" {{ ($request->re_room_size == $id) ? 'selected':'' }}>{{$value}}{{$request->re_room_size_more ? "+" : ""}} bedroom(s)</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                            @endif

							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Volume:</label>
								<div class="row pr-0 col-sm-9">
									<div class="col-sm-6 pr-0">
										<div class="input-group">
											<input type="text" autocomplete="off" class="form-control" name="volume_m3" id="for-volume_m3" value="{{$request->re_volume_m3}}">
											<label class="input-group-append" for="for-volume_m3">
												<span class="input-group-text">m3</span>
											</label>
										</div>
									</div>
									<div class="col-sm-6 pr-0">
										<div class="input-group">
											<input type="text" autocomplete="off" class="form-control" name="volume_ft3" id="for-volume_ft3" value="{{$request->re_volume_ft3}}">
											<label class="input-group-append" for="for-volume_ft3">
												<span class="input-group-text">ft3</span>
											</label>
										</div>
									</div>
								</div>

							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-remark">Remarks:</label>
								<div class="col-sm-9">
									<textarea style="height: 100px;" name="remark" id="for-remark" class="form-control" type="text">{{$request->re_remarks}}</textarea>
								</div>
							</div>

							<div class="form-group row" name="translate_div" style="display:none;">
								<label class="col-sm-3 col-form-label" for="for-remark_translation">Translated:</label>
								<div class="col-sm-9">
									<textarea style="height: 100px;"  class="form-control" type="text" disabled name="remark_translation" id="for-remark_translation"></textarea>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-3"></div>
								<div class="form-group col-md-9">
									<button type="button" name="translate" class="btn btn-sm btn-primary">Translate</button>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-moving_date">Moving date:</label>
								<div class="col-sm-9">
									<input autocomplete="off" value="{{$request->re_moving_date}}" type="text"
									       class="js-datepicker form-control" id="for-moving_date" name="moving_date"
									       data-week-start="1" data-autoclose="true" data-today-highlight="true"
									       data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd">
								</div>
							</div>

							@isset($notices['moving_date'])
								<div class="form-group row">
									<div class="col-md-3"></div>
									<div class="col-sm-9" style="color:red;">
										<label>{!! $notices['moving_date'] !!}</label>
									</div>
								</div>
							@endisset

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-storage">Storage:</label>
								<div class="col-sm-9">
									<select type="text" class="form-control" name="storage" id="for-storage">
										@foreach($emptyyesno as $id => $value)
											<option value="{{$id}}" {{ ($request->re_storage == $id) ? 'selected':'' }}>{{$value}}</option>
										@endforeach
									</select>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-packing">Packing:</label>
								<div class="col-sm-9">
									<select type="text" class="form-control" name="packing" id="for-packing">
										@foreach($byselfcompany as $id => $value)
											<option value="{{$id}}" {{ ($request->re_packing == $id) ? 'selected':'' }}>{{$value}}</option>
										@endforeach
									</select>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-assembly">Assembly:</label>
								<div class="col-sm-9">
									<select type="text" class="form-control" name="assembly" id="for-assembly">
										@foreach($byselfcompany as $id => $value)
											<option value="{{$id}}" {{ ($request->re_assembly == $id) ? 'selected':'' }}>{{$value}}</option>
										@endforeach
									</select>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-business">Business</label>
								<div class="col-sm-9">
									<div class="custom-control custom-switch custom-control custom-control-inline custom-control-primary">
										<input type="checkbox" class="custom-control-input" id="for-business" name="business" {{($request->re_business ? "checked" : "")}}>
										<label class="custom-control-label" for="for-business"></label>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>

			<div class="row justify-content-center">
				<div class="col-lg-6 col-md-12">
					<div id="moving_from" class="block block-rounded block-bordered">
						<div class="block-header block-header-default">
							<h3 class="block-title">Moving from</h3>
						</div>
						<div class="block-content">
							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-street_from">Street:</label>
								<div class="col-sm-9">
									<input class="form-control refreshMapOnKeyup" name="street_from" id="for-street_from" data-from-to="from" type="text" value="{{$request->re_street_from}}">
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-zipcode_from">Zipcode:</label>
								<div class="col-sm-9">
									<input class="form-control refreshMapOnKeyup" name="zipcode_from" id="for-zipcode_from" data-from-to="from" type="text" value="{{$request->re_zipcode_from}}">
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-city_from">City:</label>
								<div class="col-sm-9">
									<input class="form-control refreshMapOnKeyup" type="text" name="city_from" data-from-to="from" id="for-city_from" value="{{$request->re_city_from}}">
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-country_from">Country:</label>
								<div class="col-sm-9">
									<div class="input-group">
										<select type="text" class="js-select2 form-control refreshMapOnChange" data-from-to="from" data-placeholder="Choose a country.." name="country_from" id="for-country_from">
											<option></option>
											@foreach($countries as $id => $country)
												<option value="{{$id}}" {{ ($request->re_co_code_from ==  $id) ? 'selected':'' }}>{{$country}}</option>
											@endforeach
										</select>
										<div class="input-group-append">
											<a href="#" class="reset-select btn btn-primary" data-target="#for-country_from">
												<i class="fa fa-times"></i>
											</a>
										</div>
									</div>

								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-region_from">Region:</label>
								<div class="col-sm-9">
									<div class="input-group">
										<select type="text" class="form-control js-select2" data-placeholder="Choose a region.." name="region_from" id="for-region_from" data-allow-clear></select>
										<div class="input-group-append">
											<a href="#" class="reset-select btn btn-primary" data-target="#for-region_from">
												<i class="fa fa-times"></i>
											</a>
										</div>
									</div>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="region_suggestion_from">Region Suggestion:</label>
								<div class="col-sm-9">
									<div id="region_suggestion_from"></div>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-select_residence_from">Residence:</label>
								<div class="col-sm-9">
									<select type="text" class="form-control" name="select_residence_from" id="for-select_residence_from">
										@foreach($requestresidences as $id => $residence)
											<option value="{{$id}}" {{ ($request->re_residence_from ==  $id) ? 'selected':'' }}>{{$residence}}</option>
										@endforeach
									</select>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Google maps:</label>
								<div class="col-sm-9 google_maps_image_from" style="float: left; overflow: hidden"></div>
                                <input type="hidden" name="google_maps_link_from_hidden" value=""/>
                            </div>

						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-12">
					<div id="moving_to" class="block block-rounded block-bordered">
						<div class="block-header block-header-default">
							<h3 class="block-title">Moving to</h3>
						</div>
						<div class="block-content">

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-street_to">Street:</label>
								<div class="col-sm-9">
									<input class="form-control refreshMapOnKeyup" data-from-to="to" name="street_to" id="for-street_to" type="text" value="{{$request->re_street_to}}">
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-zipcode_to">Zipcode:</label>
								<div class="col-sm-9">
									<input class="form-control refreshMapOnKeyup" data-from-to="to" name="zipcode_to" id="for-zipcode_to" type="text" value="{{$request->re_zipcode_to}}">
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-city_to">City:</label>
								<div class="col-sm-9">
									<input class="form-control refreshMapOnKeyup" data-from-to="to" type="text" name="city_to" id="for-city_to" value="{{$request->re_city_to}}">
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-country_to">Country:</label>
								<div class="col-sm-9">
									<div class="input-group">
										<select type="text" class="js-select2 reset-select form-control refreshMapOnChange" data-from-to="to" data-placeholder="Choose a country.." name="country_to" id="for-country_to">
											<option></option>
											@foreach($countries as $id => $country)
												<option value="{{$id}}" {{ ($request->re_co_code_to ==  $id) ? 'selected':'' }}>{{$country}}</option>
											@endforeach
										</select>

										<div class="input-group-append">
											<a href="#" class="reset-select btn btn-primary" data-target="#for-country_to">
												<i class="fa fa-times"></i>
											</a>
										</div>
									</div>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-region_to">Region:</label>
								<div class="col-sm-9">
									<div class="input-group">
										<select type='text' class='form-control js-select2' data-placeholder='Choose a region..' name='region_to' id='for-region_to'>
										</select>
										<div class="input-group-append">
											<a href="#" class="reset-select btn btn-primary" data-target="#for-region_to">
												<i class="fa fa-times"></i>
											</a>
										</div>
									</div>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="region_suggestion_to">Region Suggestion:</label>
								<div class="col-sm-9">
									<div id="region_suggestion_to"></div>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-select_residence_to">Residence:</label>
								<div class="col-sm-9">
									<select type="text" class="form-control" name="select_residence_to" id="for-select_residence_to">
										@foreach($requestresidences as $id => $residence)
											<option value="{{$id}}" {{ ($request->re_residence_to ==  $id) ? 'selected':'' }}>{{$residence}}</option>
										@endforeach
									</select>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Google maps:</label>
								<div class="col-sm-9 google_maps_image_to" style="float: left;"></div>

                                <input type="hidden" name="google_maps_link_to_hidden" value=""/>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-lg-6 col-md-12">
					<div class="block block-rounded block-bordered">
						<div class="block-header block-header-default">
							<h3 class="block-title">Contact details</h3>
						</div>
						<div class="block-content">

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-company_name">Company name:</label>
								<div class="col-sm-9">
									<input class="form-control" name="company_name" id="for-company_name" type="text" value="{{$request->re_company_name}}">
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-full_name">Full name:</label>
								<div class="col-sm-9">
									<input class="form-control" name="full_name" id="for-full_name" type="text" value="{{$request->re_full_name}}">
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-telephone1">Telephone 1:</label>
								<div class="col-sm-4">
									<input class="form-control" name="telephone1" id="for-telephone1" type="text" value="{{$request->re_telephone1}}">
								</div>
								<div class="col-sm-1"> <a href="#" data-id="{{$request->re_id}}" data-telephone="1" class="wrong_telephone_number"> <i class="fa fa-2x fa-envelope"></i></a></div>
								<div class="col-sm-1"><a href="#" class="add_to_dialfire" data-id="{{$request->re_id}}" data-type="requests"><i class="fa fa-2x fa-phone-square"></i></a></div>
							</div>

                            @if ($request->re_co_code_from == $request->re_co_code_to)
                                @if (!empty($suggested_phone_number_from) && $isValid_from)
                                    <div class="form-group row">
                                        <label class="col-sm-3"></label>
                                        <label class="col-sm-6" style="color:green">This phone number seems to be valid!</label>
                                    </div>
                                @elseif(!$isValid_from && !$isValid_to && !empty($suggested_phone_number_ip) && $isValid_ip)
                                    <div class="form-group row">
                                        <label class="col-sm-3"></label>
                                        <label class="col-sm-6" style="color:green"><b>Suggested (IP):</b> {{$suggested_phone_number_ip}}</label>
                                    </div>
                                @else
                                    <div class="form-group row">
                                        <label class="col-sm-3"></label>
                                        <label class="col-sm-6" style="color:red">This phone number is NOT valid!</label>
                                    </div>
                                @endif
                            @else
                                @if (!empty($suggested_phone_number_from) && $isValid_from)
                                    @if ($suggested_phone_number_from != $request->re_telephone1)
                                        <div class="form-group row">
                                            <label class="col-sm-3"></label>
                                            <label class="col-sm-6" style="color:green"><b>Suggested (FROM):</b> {{$suggested_phone_number_from}}</label>
                                        </div>
                                    @else
                                        <div class="form-group row">
                                            <label class="col-sm-3"></label>
                                            <label class="col-sm-6" style="color:green">This phone number seems to be valid!</label>
                                        </div>
                                    @endif
                                @endif

                                @if (!empty($suggested_phone_number_to) && $isValid_to && !$isValid_from)
                                        @if ($suggested_phone_number_to != $request->re_telephone1)
                                            <div class="form-group row">
                                                <label class="col-sm-3"></label>
                                                <label class="col-sm-6" style="color:green"><b>Suggested (TO):</b> {{$suggested_phone_number_to}}</label>
                                            </div>
                                        @else
                                            <div class="form-group row">
                                                <label class="col-sm-3"></label>
                                                <label class="col-sm-6" style="color:green">This phone number seems to be valid!</label>
                                            </div>
                                        @endif
                                @endif

                                @if (!$isValid_from && !$isValid_to && $isValid_ip && !empty($suggested_phone_number_ip))
                                    @if ($suggested_phone_number_ip != $request->re_telephone1)
                                        <div class="form-group row">
                                            <label class="col-sm-3"></label>
                                            <label class="col-sm-6" style="color:green"><b>Suggested (IP):</b> {{$suggested_phone_number_ip}}</label>
                                        </div>
                                    @else
                                        <div class="form-group row">
                                            <label class="col-sm-3"></label>
                                            <label class="col-sm-6" style="color:green">This phone number seems to be valid!</label>
                                        </div>
                                    @endif

                                @endif

                                @if (!$isValid_from && !$isValid_to && !$isValid_ip)
                                    <div class="form-group row">
                                        <label class="col-sm-3"></label>
                                        <label class="col-sm-6" style="color:red">This phone number is NOT valid!</label>
                                    </div>
                                @endif
                            @endif

                            @if (isset($request_update_fields["Telephone"]) && $request->re_pd_validate == 1)
                                The user changed the phone number to: {{$request_update_fields["Telephone"]}}.<br /><br/>
                            @endif

                            @if($request->re_telephone_1_mail)
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label" for="for-mail_sent_telephone_1">Mail sent for telephone 1:</label>
                                    <div class="col-sm-9">
                                        <div class="custom-control custom-switch custom-control custom-control-inline custom-control-primary">
                                            <input disabled type="checkbox" class="custom-control-input" id="for-mail_sent_telephone_1" name="mail_sent_telephone_1" {{($request->re_telephone_1_mail ? "checked" : "")}}>
                                            <label class="custom-control-label" for="for-mail_sent_telephone_1"></label>
                                        </div>
                                    </div>
                                </div>
                            @endif


                            <div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-telephone2">Telephone 2:</label>
								<div class="col-sm-4">
									<input class="form-control" name="telephone2" id="for-telephone2" type="text" value="{{$request->re_telephone2}}">
								</div>
								<div class="col-sm-1"> <a href="#" data-id="{{$request->re_id}}" data-telephone="2" class="wrong_telephone_number"> <i class="fa fa-2x fa-envelope"></i></a></div>
							</div>
							@if($request->re_telephone2)
								@isset($notices['phone_2'])
									<div class="form-group row">
										<label class="col-sm-3"></label>
										<label class="col-sm-6" style="color:red">This phone number is NOT valid!</label>
									</div>
								@endisset

                                @if($request->re_telephone_2_mail)
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label" for="for-mail_sent_telephone_2">Mail sent for telephone 2:</label>
                                        <div class="col-sm-9">
                                            <div class="custom-control custom-switch custom-control custom-control-inline custom-control-primary">
                                                <input disabled type="checkbox" class="custom-control-input" id="for-mail_sent_telephone_2" name="mail_sent_telephone_2" {{($request->re_telephone_2_mail ? "checked" : "")}}>
                                                <label class="custom-control-label" for="for-mail_sent_telephone_2"></label>
                                            </div>
                                        </div>
                                    </div>
                                @endif
							@endif
							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-email">Email:</label>
								<div class="col-sm-9">
									<input class="form-control" name="email" id="for-email" type="text" value="{{$request->re_email}}">
								</div>
							</div>

							@isset($notices['email_bounce'])
								<div class="form-group row">
									<label class="col-sm-3 col-form-label">Bounce:</label>
									<div class="col-sm-9" style="color:red">
										<label>{!! $notices['email_bounce'] !!}</label>
									</div>
								</div>
							@endisset
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-12">
					<div class="block block-rounded block-bordered">
						<div class="block-header block-header-default">
							<h3 class="block-title">Internal information</h3>
						</div>
						<div class="block-content">
							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-called">Called:</label>
								<div class="col-sm-9">
									<select type="text" class="form-control" name="called" id="for-called">
										@foreach($called as $id => $value)
											<option value="{{$id}}" {{ ($request->re_internal_called == $id) ? 'selected':'' }}>{{$value}}</option>
										@endforeach
									</select>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label" for="for-called_on">Called on:</label>
								<div class="col-sm-9">
									<input autocomplete="off" value="{{$request->re_internal_called_timestamp}}" type="text" class="js-datepicker form-control" id="for-called_on" name="called_on" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd">
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label"
								       for="for-internal_remarks">Remarks:</label>
								<div class="col-sm-9">
									<textarea style="height:100px;" type="text" class="form-control" name="internal_remarks" id="for-internal_remarks">{{$request->re_internal_remarks}}</textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				@if(!empty($request->volumecalculator->voca_volume_calculator) && $request->volumecalculator->voca_volume_calculator != "YTowOnt9"&& $request->volumecalculator->voca_volume_calculator != "czowOiIiOw==")
					<div class="col-lg-6 col-md-12">
						<div class="block block-rounded block-bordered">
							<div class="block-header block-header-default">
								<h3 class="block-title">Volume calculator ({{$request->re_volume_m3}} m<sup>3</sup>)</h3>
							</div>
							<div class="block-content">
								{!! $volumecalculator !!}
							</div>
						</div>
					</div>
				@endif
			</div>

			<!-- Large Default Modal -->
			<div class="modal" id="modal-block-large" tabindex="-1" role="dialog" aria-labelledby="modal-block-large" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="block block-themed block-transparent mb-0">
							<div class="block-header bg-primary-dark">
								<h3 class="block-title">Missing telephone number e-mail</h3>
								<div class="block-options">
									<button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
										<i class="fa fa-fw fa-times"></i>
									</button>
								</div>
							</div>
							<div class="block-content">
								<div id="kekdiffie"><i style="margin-bottom: 50px;margin-top: 50px; margin-left: 300px;" class="fa fa-10x fa-sync fa-spin text-muted"></i></div>
							</div>
							<div class="block-content block-content-full text-right bg-light">
								<button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Close</button>
								<button type="button" class="btn btn-sm btn-primary" id="send-email" >Send E-mail</button>
							</div>
						</div>
					</div>
				</div>
			</div>

			@if($request->re_status != 0)
				<strong>This request has already been matched or rejected, you can't update it any more!</strong>
				<br>
				<br>
				<button type="submit" name="back" class="btn btn-primary">Back</button>
				<br>
			@else
				<div class="form-group col-md-8">
					<button type="submit" name="save" class="btn btn-primary">Save</button>
					<button type="submit" name="match" class="btn btn-primary">Match</button>
					<button type="submit" name="reject" class="btn btn-primary">Reject</button>
					<a href="/requests"><button type="button" name="back" class="btn btn-primary">Back</button></a>
				</div>
			@endif
		</form>
	</div>

@endsection

@push( 'scripts' )
	<script>

        jQuery( document ).ready( function() {

            // Init
            getMap( 'from' );
            getMap( 'to' );

            // On translate
            jQuery( document ).on( 'click', 'button[name="translate"]', function() {

                // Get value
                getTranslation( $('textarea[name="remark"]').val(), function( $result ) {

                    $('div[name=translate_div]').slideDown();
                    $('textarea[name=remark_translation]').val($result);
                    $('button[name=translate]').attr('disabled', 'disabled');
                });
            });

            if ($("input[id=request_type-1]").is(":checked"))
            {
            	start_request_type = 1;
            }
            if ($("input[id=request_type-2]").is(":checked"))
            {
            	start_request_type = 2;
            }
            if ($("input[id=request_type-3]").is(":checked"))
            {
            	start_request_type = 3;
            }
            if ($("input[id=request_type-4]").is(":checked"))
            {
            	start_request_type = 4;
            }
            if ($("input[id=request_type-5]").is(":checked"))
            {
            	start_request_type = 5;
            }

            // On request type change
            $( 'input[name="request_type"]' ).change( function() {
            	var on_hold_input = $("input[name=request_on_hold]");

            	var rememberedRequestType = start_request_type;
                // Get checked value
                var $requestType = $( 'input[name="request_type"]:checked' ).val();
                start_request_type = $requestType;


                // Store moving size element
                var $movingSizeEl = $( 'select[name="moving_size"]' );

                // Get the moving sizes
                getMovingSizes( $requestType, function( $result ) {

                    // Parse the returned json data
                    var $opts = $.parseJSON( $result );

                    // Clear moving sizes
                    $movingSizeEl.empty();

                    // Loop moving sizes
                    $.each( $opts, function( id, value ) {

                        // Append to dropdown
                        $movingSizeEl.append( '<option value="' + id + '">' + value + '</option>' );
                    });
                });

                if ($("input[id=request_type-5]").is(":checked"))
                {
                    if(!confirm('Are you sure you want to make this a Premium lead? The lead will removed from ON HOLD when the lead is on hold. You will redirected to a new page where you can fill in all the Premium lead details if you continue.'))
                    {
                        $(this).prop("checked", false);
                        $("input[id=request_type-" + rememberedRequestType + "]").prop("checked", true);
                    }
                    else
                    {
                        $("button[name=save]").click();
                    }
                }
                else
                {
                    //On hold input disabled and unchecked. Also hide on hold by div
                    on_hold_input.prop("disabled", false);
                }
            });

            var $m3El = $( 'input[name="volume_m3"]' );
            var $ftEl = $( 'input[name="volume_ft3"]' );

            // On key up on the m3 element
            $m3El.keyup( function() {

                // Convert comma to dot
                $( this ).val( $( this ).val().replace( /,/g, '.' ) );

                // Convert volume to ft3
                $ftEl.val( convertVolume( $( this ).val(), 'ft3' ) );
            });

            // On key up on the ft3 element
            $ftEl.keyup( function() {

                // Convert comma to dot
                $( this ).val( $( this ).val().replace( /,/g, '.' ) );

                // Convert volume to m3
                $m3El.val( convertVolume( $( this).val(), 'm3' ) );
            });

            var $requestOnHoldEl = $( 'input[name="request_on_hold"]' );
            var $onHoldByEl = $( '.onholdby' );

            // On request on hold change
            $requestOnHoldEl.change(function(){
                // If on hold is checked
                if($(this).is(':checked'))
                {
                    $onHoldByEl.show();
                    $("select[name=on_hold_by]").prop("disabled", false)
                }
                else
                {
                    $onHoldByEl.hide()
                }
            });

            // Trigger initially
            $requestOnHoldEl.trigger('change');

            // Store country selects and destination type
            let $countryFromEl = $( '#for-country_from' );
            let $countryToEl = $( '#for-country_to' );
            var $destinationType = $( 'input[name="destination_type"]' ).data( 'id' );

            $countryFromEl.data("dest", $destinationType);
            $countryToEl.data("dest", $destinationType);

            // On country from change
            $countryFromEl.change( function() {

                if($countryFromEl.val() == $countryToEl.val()){
                    $destinationType = 2;
                }else{
                    $destinationType = 1;
                }

                parseRegions( {
                    destinationType: $destinationType,
                    direction: 'from',
                });

                $(this).data("dest", $destinationType);

                parseRegions({
                    destinationType: $destinationType,
                    direction: 'to',
                });

            });

            // On country to change
            $countryToEl.change( function() {

                if($countryFromEl.val() == $countryToEl.val()){
                    $destinationType = 2;
                }else{
                    $destinationType = 1;
                }

                parseRegions( {
                    destinationType: $destinationType,
                    direction: 'to',
                });

                $(this).data("dest", $destinationType);

                parseRegions({
                    destinationType: $destinationType,
                    direction: 'from',
                });

            });

            // On keyup for the input elements that should update the map
            $( '.refreshMapOnKeyup' ).keyup( function() {

                // Refresh the map
                getMap( $( this ).data( 'from-to' ) );
            });

            // On change for the select elements that should update the map
            $( '.refreshMapOnChange' ).change( function() {

                // Refresh the map
                getMap( $( this ).data( 'from-to' ) );
            });

            // Parse Regions initially
            parseRegions( {
                direction: 'from',
                destinationType: $destinationType,
                initVal: '{{ $request->re_reg_id_from }}'
            }, function()
            {
                parseRegions( {
                    direction: 'to',
                    destinationType: $destinationType,
                    initVal: '{{ $request->re_reg_id_to }}'
                }, function()
                {
                    // Find the regions
                    regionFinder();
                });
            });

            $(".wrong_telephone_number").click(function(){

                $("#send-email").prop("disabled", false);
                var id = $(this).data("id");
                var telephone = $(this).data("telephone");
                var email = $("input[name=email]").val();
                var telephone_number = $(this).parent().prev().find("input").val();

                $("#kekdiffie").html('<i style="margin-bottom: 50px;margin-top: 50px; margin-left: 300px;" class="fa fa-10x fa-sync fa-spin text-muted"></i>');
                $('#modal-block-large').modal('show');

                $.ajax({
                    type: "GET",
                    url: '{{ url('/ajax/requestwrongtelephonenumber') }}',
                    data: {"function": "check", "id": id, "email": email, "telephone_number": telephone_number, "telephone" : telephone},
                    success: function(data){

                        $("#kekdiffie").html(data);

                        email_button = $("#kekdiffie").parent().next().find("#send-email");
                        email_button.attr("id", id);
                        email_button.attr("telephone", telephone);
                        email_button.attr("email", email);
                        email_button.attr("telephone_number", telephone_number);

                    }
                });

                return false;
            });

            $("#send-email").click(function () {

                $this = $(this);
                $(this).attr("disabled", "disabled");

                var id = $(this).attr("id");
                var email = $(this).attr("email");
                var telephone = $(this).attr("telephone");
                var telephone_number = $(this).attr("telephone_number");

                $.ajax({
                    type: "GET",
                    url: '{{ url('/ajax/requestwrongtelephonenumber') }}',
                    data: {"function": "send", "id": id, "email": email, "telephone_number": telephone_number, "telephone": telephone},
                    success: function(data){
                        $("#kekdiffie").html('<i style="margin-bottom: 50px;margin-top: 50px; margin-left: 300px;color:green;" class="fa fa-10x fa-check"></i>');
                    }
                });
            });
        });

        $( '.add_to_dialfire' ).click( function() {

            if( ! confirm( 'Are you sure you want to add this to dialfire?' ) )
                return false;

            var $id = $(this).data('id');
            var $type = $(this).data('type');

            $.ajax({
                type: "GET",
                url: '{{ url('/ajax/addtodialfire') }}',
                data: {
                    "id": $id,
                    "type": $type
                },
                success: function(data){
                    response = JSON.parse(data);
                    if(response.success) {
                        alert('Telephone number succesfully added to DialFire! (Can be found at ' + response.dialfire_task + ')');
                    }else{
                        alert(response.error);
                    }
                }
            });

            return false;
        });

        //request_edit_form
        $("#request_edit_form").submit(function(){
            var btn = $(this).find("button[type=submit]:focus");

            if(btn.is($("button[name=match]"))){
                var errors = 0;

                var count = $("select[name=region_from] option").length;

                if($("select[name=region_from]").val() == "" && count > 1){
                    alert("Please select the origin region.");

                    errors++;
                }

                var count = $("select[name=region_to] option").length;

                if($("select[name=region_to]").val() == "" && count > 1){
                    alert("Please select the destination region.");

                    errors++;
                }

                if($("#for-request_spam").is(':checked') || $("#for-request_double").is(':checked')) {
                    alert("You can't match a lead that is marked as SPAM or DOUBLE");

                    errors++;
                }

                if($("#request_spam").is(':checked') || $("#request_double").is(':checked')) {
                    alert("You can't match a lead that is marked as SPAM or DOUBLE");

                    errors++;
                }

                if(errors > 0){
                    return false;
                }
            }
        });

	</script>

@endpush





