<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Premium Lead #{{$request->prle_re_id}}</title>
</head>
<body>
    <style>
        table, th, td {
            border: 1px solid slategrey;
            border-collapse: collapse;
            padding: 5px 10px 5px 10px;
        }
        .width100percent {
            width: 100%;
        }
        .width70percent {
            width: 70%;
        }
        .width30percent {
            width: 30%;
        }
    </style>
    <table style="width:100%;">
        <!-- Loading address -->
        <tr>
            <td colspan="2" class="width100percent"><b style="font-size:18px;">Loading address</b></td>
        </tr>
        <tr>
            <td class="width30percent">Street name + house number</td>
            <td class="width70percent">{{$request->re_street_from}}</td>
        </tr>
        <tr>
            <td class="width30percent">Postcode</td>
            <td class="width70percent">{{$request->re_zipcode_from}}</td>
        </tr>
        <tr>
            <td class="width30percent">City</td>
            <td class="width70percent">{{$request->re_city_from}}</td>
        </tr>
        <tr>
            <td class="width30percent">Country</td>
            <td class="width70percent">{{$countries[$request->re_co_code_from]}}</td>
        </tr>
        <tr>
            <td class="width30percent">Type of residence</td>
            <td class="width70percent">{{$premiumleadresidencetypes[$request->prle_residence_type_from]}}</td>
        </tr>
        <tr>
            <td class="width30percent">Floor level</td>
            <td class="width70percent">{{$requestresidences[$request->re_residence_from]}}</td>
        </tr>
        <tr>
            <td class="width30percent">Inside elevator</td>
            <td class="width70percent">{{$request->prle_inside_elevator_from}}</td>
        </tr>
        <tr>
            <td class="width30percent">Obstacles for moving van</td>
            <td class="width70percent">{{$request->prle_obstacles_for_moving_van_from}}</td>
        </tr>
        <tr>
            <td class="width30percent">Parking restrictions/permits</td>
            <td class="width70percent">{{$request->prle_parking_restrictions_and_permits_from}}</td>
        </tr>
        <tr>
            <td class="width30percent">Walking distance to house</td>
            <td class="width70percent">{{$request->prle_walking_distance_to_house_from}}</td>
        </tr>
        <!-- end -->

        <!-- Destination address -->
        <tr>
            <td colspan="2" class="width100percent"><b style="font-size:18px;">Destination address</b></td>
        </tr>
        <tr>
            <td class="width30percent">Street name + house number</td>
            <td class="width70percent">{{$request->re_street_to}}</td>
        </tr>
        <tr>
            <td class="width30percent">Postcode</td>
            <td class="width70percent">{{$request->re_zipcode_to}}</td>
        </tr>
        <tr>
            <td class="width30percent">City</td>
            <td class="width70percent">{{$request->re_city_to}}</td>
        </tr>
        <tr>
            <td class="width30percent">Country</td>
            <td class="width70percent">{{$countries[$request->re_co_code_to]}}</td>
        </tr>
        <tr>
            <td class="width30percent">Type of residence</td>
            <td class="width70percent">{{$premiumleadresidencetypes[$request->prle_residence_type_to]}}</td>
        </tr>
        <tr>
            <td class="width30percent">Floor level</td>
            <td class="width70percent">{{$requestresidences[$request->re_residence_to]}}</td>
        </tr>
        <tr>
            <td class="width30percent">Inside elevator</td>
            <td class="width70percent">{{$request->prle_inside_elevator_to}}</td>
        </tr>
        <tr>
            <td class="width30percent">Obstacles for moving van</td>
            <td class="width70percent">{{$request->prle_obstacles_for_moving_van_to}}</td>
        </tr>
        <tr>
            <td class="width30percent">Parking restrictions/permits</td>
            <td class="width70percent">{{$request->prle_parking_restrictions_and_permits_to}}</td>
        </tr>
        <tr>
            <td class="width30percent">Walking distance to house</td>
            <td class="width70percent">{{$request->prle_walking_distance_to_house_to}}</td>
        </tr>
        <!-- end -->

        <!-- Volume of Move -->
        <tr>
            <td colspan="2" class="width100percent"><b style="font-size:18px;">Volume of Move</b></td>
        </tr>
        <tr>
            <td class="width30percent">List of all items</td>
            <td class="width70percent">(Excel sheet)</td>
        </tr>
        <tr>
            <td class="width30percent">Special items</td>
            <td class="width70percent">{{$request->prle_special_items}}</td>
        </tr>
        <tr>
            <td class="width30percent">Volume of the move</td>
            <td class="width70percent">{{$request->re_volume_m3."m³"}} / {{$request->re_volume_ft3."ft³"}}</td>
        </tr>
        <!-- end -->

        <!-- Moving details -->
        <tr>
            <td colspan="2" class="width100percent"><b style="font-size:18px;">Moving details</b></td>
        </tr>
        <tr>
            <td class="width30percent">Date of the move</td>
            <td class="width70percent">{{$request->re_moving_date}}</td>
        </tr>
        <tr>
            <td class="width30percent">Extra info moving date</td>
            <td class="width70percent">{{$request->prle_extra_info_moving_date}}</td>
        </tr>
        <tr>
            <td class="width30percent">Packing by mover</td>
            <td class="width70percent">{{$emptyyesno[$request->prle_packing_by_mover]}}</td>
        </tr>
        @if(!empty($request->prle_packing_by_mover_remarks))
            <tr>
                <td class="width30percent">Packing by mover remarks</td>
                <td class="width70percent">{{$request->prle_packing_by_mover_remarks}}</td>
            </tr>
        @endif
        <tr>
            <td class="width30percent">Unpacking by mover</td>
            <td class="width70percent">{{$emptyyesno[$request->prle_unpacking_by_mover]}}</td>
        </tr>
        @if(!empty($request->prle_unpacking_by_mover_remarks))
            <tr>
                <td class="width30percent">Unpacking by mover remarks</td>
                <td class="width70percent">{{$request->prle_unpacking_by_mover_remarks}}</td>
            </tr>
        @endif
        <tr>
            <td class="width30percent">Disassembling by mover</td>
            <td class="width70percent">{{$emptyyesno[$request->prle_disassembling_by_mover]}}</td>
        </tr>
        @if(!empty($request->prle_disassembling_by_mover_remarks))
            <tr>
                <td class="width30percent">Disassembling by mover remarks</td>
                <td class="width70percent">{{$request->prle_disassembling_by_mover_remarks}}</td>
            </tr>
        @endif
        <tr>
            <td class="width30percent">Assembling by mover</td>
            <td class="width70percent">{{$emptyyesno[$request->prle_assembling_by_mover]}}</td>
        </tr>
        @if(!empty($request->prle_assembling_by_mover_remarks))
            <tr>
                <td class="width30percent">Assembling by mover remarks</td>
                <td class="width70percent">{{$request->prle_assembling_by_mover_remarks}}</td>
            </tr>
        @endif
        <tr>
            <td class="width30percent">Handyman service</td>
            <td class="width70percent">{{$request->prle_handyman_service}}</td>
        </tr>
        <tr>
            <td class="width30percent">Storage</td>
            <td class="width70percent">{{$emptyyesno[$request->prle_storage]}}</td>
        </tr>
        @if(!empty($request->prle_storage_remarks))
            <tr>
                <td class="width30percent">Storage remarks</td>
                <td class="width70percent">{{$request->prle_storage_remarks}}</td>
            </tr>
        @endif
        <!-- end -->

        <!-- Insurance -->
        <tr>
            <td colspan="2" class="width100percent"><b style="font-size:18px;">Insurance</b></td>
        </tr>
        <tr>
            <td class="width30percent">Insurance</td>
            <td class="width70percent">{{$emptyyesno[$request->prle_insurance]}}</td>
        </tr>
        @if(!empty($request->prle_insurance_remarks))
            <tr>
                <td class="width30percent">Remarks</td>
                <td class="width70percent">{{$request->prle_insurance_remarks}}</td>
            </tr>
        @endif
        @if(!empty($request->prle_insurance_amount))
            <tr>
                <td class="width30percent">Amount</td>
                <td class="width70percent">{{$request->prle_insurance_amount}}</td>
            </tr>
        @endif
        <!-- end -->

        <!-- Contact details -->
        <tr>
            <td colspan="2" class="width100percent"><b style="font-size:18px;">Contact details</b></td>
        </tr>
        @if(!empty($request->re_company_name))
            <tr>
                <td class="width30percent">Company name</td>
                <td class="width70percent">{{$request->re_company_name}}</td>
            </tr>
        @endif
        <tr>
            <td class="width30percent">Name</td>
            <td class="width70percent">{{$request->re_full_name}}</td>
        </tr>
        <tr>
            <td class="width30percent">Phone number</td>
            <td class="width70percent">{{$request->re_telephone1}}</td>
        </tr>
        <tr>
            <td class="width30percent">Email</td>
            <td class="width70percent">{{$request->re_email}}</td>
        </tr>
        <tr>
            <td class="width30percent">Language</td>
            <td class="width70percent">
                @foreach($languages as $language)
                    @if($request->re_la_code == $language->la_code) {{$language->la_language}} @endif
                @endforeach
            </td>
        </tr>
        <!-- end -->

        <!-- Remarks - Additional information -->
        <tr>
            <td colspan="2" class="width100percent"><b style="font-size:18px;">Remarks - Additional information</b></td>
        </tr>
        @if(!empty($request->re_remarks))
            <tr>
                <td colspan="2" class="width100percent">{{$request->re_remarks}}</td>
            </tr>
        @endif
        <!-- end -->

    </table>

    @if(!empty($request->prle_screenshots))
        <div class="width100percent">
            @foreach(unserialize($request->prle_screenshots) as $screenie)
                <img style="margin-top:100px; margin-left:100px; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.5), 0 6px 20px 0 rgba(0, 0, 0, 0.4); max-width:700px; max-height: 900px;" src="{{config('app.shared_folder')."uploads/premium_leads_screenshots/".$request->re_id."/".$screenie}}" />
            @endforeach
        </div>
    @endif
</body>

</html>
