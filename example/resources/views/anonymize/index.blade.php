@extends('layouts.backend')

@include('scripts.datatables')

@section('content')
	<!-- Hero -->
	<div class="bg-body-light">
		<div class="content content-full">
			<div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
				<h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Anonymize</h1>
				<nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/finance">Finance</a></li>
					</ol>
				</nav>
			</div>
		</div>
	</div>

    @if(empty($selected))
	<!-- END Hero -->
	<!-- Page Content -->
	<div class="content">
		<div class="col-md-8">
			<div class="block block-rounded block-bordered">
				@if($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach($errors->all() as $error)
								{{$error}}<br>
							@endforeach
						</ul>
					</div>
				@endif
				<div class="block-header block-header-default">
					<h3 class="block-title">
						Filters
					</h3>
				</div>
				<div class="block-content block-content-full">

                    <form class="mb-5" method="post" action="{{action('AnonymizeController@filteredIndex')}}">
                        @csrf
                        <input name="_method" type="hidden" value="post">

                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <label class="col-sm-3 col-form-label">Email:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" name="email">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="form-group col-md-8">
                                <button type="submit" class="btn btn-primary">Filter</button>
                            </div>
                        </div>

                    </form>
				</div>
			</div>
		</div>
	</div>
    @else
    <!-- END Hero -->
    <!-- Page Content -->
    <div class="content">
        <div class="col-md-12">
            <form class="mb-5" method="post" action="{{action('AnonymizeController@anonymize')}}">
                @csrf
                <input name="_method" type="hidden" value="post">
                <input type="hidden" name="email" value="{{$request->email}}">

                <h1>Requests</h1>

                <div class="block block-rounded block-bordered">
                    <div class="block-content block-content-full">
                        <table data-order='[[0, "asc"]]'
                               class="table table-bordered table-striped table-vcenter js-dataTable-full">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Received</th>
                                <th>Name</th>
                                <th>Email</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($requests as $request)
                                <tr role="row">
                                    <td>{{$request->re_id}}</td>
                                    <td>{{$request->re_timestamp}}</td>
                                    <td>{{$request->re_full_name}}</td>
                                    <td>{{$request->re_email}}</td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <h1>Organic reviews</h1>

                <div class="block block-rounded block-bordered">
                    <div class="block-content block-content-full">
                        <table data-order='[[0, "asc"]]'
                               class="table table-bordered table-striped table-vcenter js-dataTable-full">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Received</th>
                                <th>Name</th>
                                <th>Email</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($websitereviews as $websitereview)
                                <tr role="row">
                                    <td>{{$websitereview->su_id}}</td>
                                    <td>{{$websitereview->su_submitted_timestamp}}</td>
                                    <td>{{$websitereview->were_name}}</td>
                                    <td>{{$websitereview->were_email}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <h1>Found Survey_2 reviews</h1>

                <div class="block block-rounded block-bordered">
                    <div class="block-content block-content-full">
                        <table data-order='[[0, "asc"]]'
                               class="table table-bordered table-striped table-vcenter js-dataTable-full">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Received</th>
                                <th>Name</th>
                                <th>Email</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($surveys2 as $survey2)
                                <tr role="row">
                                    <td>{{$survey2->su_id}}</td>
                                    <td>{{$survey2->su_submitted_timestamp}}</td>
                                    <td>{{$survey2->re_full_name}}</td>
                                    <td>{{$survey2->re_email}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <h1>Found Questions</h1>

                <div class="block block-rounded block-bordered">
                    <div class="block-content block-content-full">
                        <table data-order='[[0, "asc"]]'
                               class="table table-bordered table-striped table-vcenter js-dataTable-full">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Received</th>
                                <th>Name</th>
                                <th>Email</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($questions as $question)
                                <tr role="row">
                                    <td>{{$question->ktrecuqu_id}}</td>
                                    <td>{{$question->ktrecuqu_sent_timestamp}}</td>
                                    <td>{{$question->re_full_name}}</td>
                                    <td>{{$question->re_email}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <h1>Found Survey 1</h1>

                <div class="block block-rounded block-bordered">
                    <div class="block-content block-content-full">
                        <table data-order='[[0, "asc"]]'
                               class="table table-bordered table-striped table-vcenter js-dataTable-full">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Received</th>
                                <th>Name</th>
                                <th>Email</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($surveys1 as $survey1)
                                <tr role="row">
                                    <td>{{$survey1->su_id}}</td>
                                    <td>{{$survey1->su_submitted_timestamp}}</td>
                                    <td>{{$survey1->re_full_name}}</td>
                                    <td>{{$survey1->re_email}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <h1>Found answered questions</h1>

                <div class="block block-rounded block-bordered">
                    <div class="block-content block-content-full">
                        <table data-order='[[0, "asc"]]'
                               class="table table-bordered table-striped table-vcenter js-dataTable-full">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Received</th>
                                <th>Name</th>
                                <th>Email</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($reactiontimes as $reactiontime)
                                <tr role="row">
                                    <td>{{$reactiontime->sucu_id}}</td>
                                    <td>{{$reactiontime->su_submitted_timestamp}}</td>
                                    <td>{{$reactiontime->re_full_name}}</td>
                                    <td>{{$reactiontime->re_email}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <h1>Found Sirelo invites</h1>

                <div class="block block-rounded block-bordered">
                    <div class="block-content block-content-full">
                        <table data-order='[[0, "asc"]]'
                               class="table table-bordered table-striped table-vcenter js-dataTable-full">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Received</th>
                                <th>Name</th>
                                <th>Email</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($sireloinvites as $sireloinvite)
                                <tr role="row">
                                    <td>{{$sireloinvite->sirein_id}}</td>
                                    <td>{{$sireloinvite->sirein_timestamp}}</td>
                                    <td>{{$sireloinvite->sirein_name}}</td>
                                    <td>{{$sireloinvite->sirein_email}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>


                <button type="submit" class="btn btn-primary">Anonymize</button>
                <a href="/anonymize"><button type="button" class="btn btn-primary">Back</button></a>
            </form>
        </div>
    </div>

    @endif
@endsection

