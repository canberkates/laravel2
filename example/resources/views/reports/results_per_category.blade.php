@extends('layouts.backend')
@include( 'scripts.datatables' )
@include('scripts.datepicker')
@include('scripts.forms')
@include( 'scripts.dialogs' )
@include('scripts.select2')
@pushonce( 'scripts:reports' )
    <script src="{{ asset('/js/custom/reports/reports.js') }}"></script>
@endpushonce

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Report - {{$report->rep_name}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/reports">Reports</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$report->rep_name}}</li>
                    </ol>
                </nav>
            </div>
            <p>
                @if (!empty($report->rep_description)) <b>{{"Description: "}}</b>{{$report->rep_description}} @endif
                @if (isset($times_used)) <br />({{$times_used}} times used in the last 90 days) @endif
            </p>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="row">
        <div class="block block-rounded block-bordered col-md-6">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            {{$error}}<br>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Filters
                </h3>
            </div>
            <div class="block-content block-content-full">

                <form class="mb-5" method="post" action="{{action('ReportController@queueResultsPerCategory', $report->rep_id)}}">
                    @csrf
                    <input name="_method" type="hidden" value="post">

                    <div class="form-group row">
                        <label class="col-md-4 col-form-label ml-5" for="for-date">Date range:</label>
                        <div class="col-md-6">
                            <input  @if(isset($repr_id) && $clone === false) disabled @endif type="text" class="form-control drp drp-default" name="date" @if(isset($repr_id)) value="{{App\Functions\System::unserialize($report_progress->repr_posted_values)['date_from']." - ".App\Functions\System::unserialize($report_progress->repr_posted_values)['date_to']}}" @endif autocomplete="off"/>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4 col-form-label ml-5" for="destination_type">Destination type {{App\Functions\System::unserialize($report_progress->repr_posted_values)['request_type']}}</label>
                        <div class="col-md-6">
                            <select @if(isset($repr_id) && $clone === false) disabled @endif type="text" name="destination_type" class="chosen-select form-control" style="width:100%;">
                                <option @if(App\Functions\System::unserialize($report_progress->repr_posted_values)['destination_type'] != 1 && App\Functions\System::unserialize($report_progress->repr_posted_values)['destination_type'] != 2) selected @endif value="">Both</option>
                                <option @if(App\Functions\System::unserialize($report_progress->repr_posted_values)['destination_type'] == 1) selected @endif value="1">INT</option>
                                <option @if(App\Functions\System::unserialize($report_progress->repr_posted_values)['destination_type'] == 2) selected @endif value="2">Domestic</option>
                            </select>
                        </div>
                    </div>

                    @foreach(\App\Data\MovingSize::all() as $id => $type)
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label ml-5"
                                   for="moda_activate_premium_leads">@if($loop->first) {{"Moving size"}} @endif</label>
                            <div class="col-sm-6">
                                <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                    <input @if(isset($repr_id) && $clone === false) disabled @endif type="checkbox" class="custom-control-input"
                                           id="moving_size[{{$id}}]"
                                           @if(App\Functions\System::unserialize($report_progress->repr_posted_values)['moving_size'][$id] == "on")
                                               checked=""
                                           @elseif(!isset($repr_id))
                                               checked=""
                                           @endif
                                           name="moving_size[{{$id}}]">
                                    <label class="custom-control-label"
                                           for="moving_size[{{$id}}]">{{$type}}</label>
                                </div>
                            </div>
                        </div>
                    @endforeach

                    <div class="form-group row">
                        <label class="col-md-4 col-form-label ml-5" for="source">Source</label>
                        <div class="col-md-6">
                            <select multiple @if(isset($repr_id) && $clone === false) disabled @endif type="text" class="js-select2 form-control" id="source" name="source[]">
                                <option value=""></option>
                                @foreach (App\Data\PlatformSource::all() as $id => $source)
                                    <option @if(in_array($id, App\Functions\System::unserialize($report_progress->repr_posted_values)['source'])) selected @endif value="{{$id}}">{{$source}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4 col-form-label ml-5" for="portal">Comment:</label>
                        <div class="col-md-6">
                            <input class="form-control" @if(isset($repr_id) && $clone === false) disabled @endif name="report_comment" type="text" value="{{App\Functions\System::unserialize($report_progress->repr_posted_values)['report_comment']}}">
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="form-group col-md-8">
                            <button type="submit" class="btn btn-primary">Queue report</button>
                        </div>
                    </div>
                    @if(isset($repr_id))
                        <hr>
                        <div class="row">
                            <div class="col-md-1"></div>
                            <p>
                            <a href="/reports/{{$report->rep_id}}/view/" @if(isset($repr_id) && $clone === false) class="btn btn-primary" @else class="btn btn-danger" @endif>New report</a>
                            @if($clone === false)
                                <a href="/reports/{{$report->rep_id}}/view/{{$report->rep_report}}/{{$repr_id}}/clone/" class="btn btn-primary">Clone report</a>
                                <a href="/reports/report_progress/{{$repr_id}}/delete" class="btn btn-primary">Delete report</a>
                            @endif
                            <a href="/reports" class="btn btn-primary">Back</a>
                            </p>
                        </div>
                    @endif
                </form>
            </div>
        </div>
        <div class="block block-rounded block-bordered col-md-6">
            <form method="post" id="single-report-progress" name="single-report-progress">
                <input type="hidden" id="report_id" name="report_id" value="{{$report->rep_id}}"/>
                <input type="hidden" id="report_progress_id" name="report_progress_id" value="{{$repr_id}}"/>
                <input type="hidden" id="report_status" name="report_status" value="@if (isset($report_progress)) {{$report_progress->repr_status}} @endif"/>
                <div id="single-report-html"></div>
            </form>

            <div class="progress push" style="height: 10px; margin-top: 10px">
                <div id="progressbar" class="progress-bar progress-bar-striped bg-success" role="progressbar" style="" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <hr>
            @if (isset($repr_id))
                {!! App\Functions\Reporting::getAllGeneratedReportsByReportId($report->rep_id, $repr_id) !!}
            @else
                {!! App\Functions\Reporting::getAllGeneratedReportsByReportId($report->rep_id) !!}
            @endif

        </div>
        </div>
    </div>
    @isset($file)
        <div class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="block-content tab-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="block block-rounded block-bordered">
                                    <div class="block-content">
                                        <h2 class="content-heading pt-0">{{$report->rep_name}} - Report</h2>

                                        <div class="row">
                                            <div class="col-md-12">
                                                {!! $file !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endisset
    <!-- END Page Content -->
@endsection
