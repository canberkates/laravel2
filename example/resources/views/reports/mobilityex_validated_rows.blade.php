@extends('layouts.backend')
@include( 'scripts.datatables' )
@include('scripts.datepicker')
@include('scripts.forms')
@include( 'scripts.dialogs' )
@include('scripts.select2')

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Report - {{$report->rep_name}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/reports">Reports</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$report->rep_name}}</li>
                    </ol>
                </nav>
            </div>
            <p>
                @if (!empty($report->rep_description)) <b>{{"Description: "}}</b>{{$report->rep_description}} @endif
                @if (isset($times_used)) <br />({{$times_used}} times used in the last 90 days) @endif
            </p>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="block block-rounded block-bordered">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            {{$error}}<br>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Filters
                </h3>
            </div>
            <div class="block-content block-content-full">

                <form class="mb-5" method="post" action="{{action('ReportController@viewMobilityExValidatedRows', $report->rep_id)}}">
                    @csrf
                    <input name="_method" type="hidden" value="post">

                    <div class="form-group row">
                        <label class="col-md-1 col-form-label ml-5" for="for-date">Date range:</label>
                        <div class="col-md-3">
                            <input type="text" class="form-control drp drp-default" name="date" @if(isset($date_filter) && $date_filter != null) value="{{$date_filter}}" @endif autocomplete="off"/>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="form-group col-md-8">
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div>
                    </div>

                </form>
                @if (isset($filtered_data) && $filtered_data != null)
                    <table data-order='[[2, "asc"]]'
                        class="table table-bordered table-striped table-vcenter js-dataTable-full">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Processed by</th>
                            <th>Processed timestamp</th>
                            <th>Customer ID</th>
                            <th>Connected or Created customer</th>
                            <th>What happend?</th>
                            <th>Revisions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($filtered_data as $id => $data)
                            <tr>
                                <td>{{$data['name']}}</td>
                                <td>{{$data['processed_by']}}</td>
                                <td>{{$data['processed_timestamp']}}</td>
                                <td>{{$data['cu_id']}}</td>
                                <td>{{$data['connected_or_created_customer']}}</td>
                                <td>{{$data['what_happend']}}</td>
                                <td>
                                    @if(!empty($data['revisions']))
                                        <span style='text-decoration: underline; cursor: pointer' href='#' class='show_changes changes_show_{{$id}}' data-row_id='{{$id}}'>Show changes</span>
                                        <span style='text-decoration: underline; cursor: pointer; display: none;' href='#' class='hide_changes changes_hide_{{$id}}' data-row_id='{{$id}}'>Hide changes</span>
                                        <div id='changes_row_{{$id}}' style='display: none;'>{!! $data['revisions'] !!}</div>
                                    @else
                                        Nothing
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection

@push( 'scripts' )

    <script>
        jQuery(document).ready(function(){
            jQuery(document).on( 'click', ".show_changes",  function(e) {
                row_id = jQuery(this).data("row_id");

                jQuery(this).hide();
                jQuery("span.changes_hide_" + row_id).show();
                jQuery("div#changes_row_" + row_id).slideDown();
            });

            jQuery(document).on( 'click', ".hide_changes",  function(e) {
                row_id = jQuery(this).data("row_id");

                jQuery(this).hide();
                jQuery("span.changes_show_" + row_id).show();

                jQuery("div#changes_row_" + row_id).slideUp();
            });
        });
    </script>

@endpush
