@extends('layouts.backend')
@include( 'scripts.datatables' )
@include('scripts.datepicker')
@include('scripts.forms')
@include( 'scripts.dialogs' )
@include('scripts.select2')


@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Report - {{$report->rep_name}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/reports">Reports</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$report->rep_name}}</li>
                    </ol>
                </nav>
            </div>
            <p>
                @if (!empty($report->rep_description)) <b>{{"Description: "}}</b>{{$report->rep_description}} @endif
                @if (isset($times_used)) <br />({{$times_used}} times used in the last 90 days) @endif
            </p>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="block block-rounded block-bordered">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            {{$error}}<br>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="block-content block-content-full">
                <div class="block-header block-header-default">
                    <h3 class="block-title">
                        Filters
                    </h3>
                </div>
                <div class="block-content block-content-full">

                    <form class="mb-5" method="post" action="{{action('ReportController@viewRecoveredDashboardLeads', $report->rep_id)}}">
                        @csrf
                        <input name="_method" type="hidden" value="post">

                        <div class="form-group row">
                            <label class="col-md-2 col-form-label ml-5" for="for-date">Date range:</label>
                            <div class="col-md-3">
                                <input type="text" class="form-control drp drp-default" name="date" @if(isset($date_filter) && $date_filter != null) value="{{$date_filter}}" @endif autocomplete="off"/>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="form-group col-md-8">
                                <button type="submit" class="btn btn-primary">Filter</button>
                            </div>
                        </div>

                    </form>
                    @if (isset($filtered_data) && $filtered_data != null)
                        <div style='color:green;'>
                            <b>R/R = Rejected / Recovered</b>
                        </div>
                        <br />
                        <table data-order='[[1, "asc"]]'
                               class="table table-bordered table-striped table-vcenter js-dataTable-full">
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th>Leads</th>
                                <th>Rejected</th>
                                <th>Recovered</th>
                                <th>Recovered + phone update</th>
                                <th>Sent Phone email</th>
                                <th>Updated phone number</th>
                                @foreach ($filtered_data['rejection_reasons'] as $column => $amount)
                                    @if ($amount['rejected'] == 0) @continue @endif
                                    <th>{{$column}}</th>
                                @endforeach
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{$date_filter}}</td>
                                    <td>{{number_format($filtered_data['leads'], 0, ",", ".")}}</td>
                                    <td>{{number_format($filtered_data['rejected'], 0, ",", ".")}}</td>
                                    <td>{{number_format($filtered_data['recovered'], 0, ",", ".")}}</td>
                                    <td>{{number_format($filtered_data['recovered_telephone'], 0, ",", ".")}}</td>
                                    <td>{{number_format($filtered_data['telephone_mail'], 0, ",", ".")}}</td>
                                    <td>{{number_format($filtered_data['updated_telephone'], 0, ",", ".")}}</td>
                                    @foreach ($filtered_data['rejection_reasons'] as $column => $amount)
                                        @if ($amount['rejected'] == 0) @continue @endif
                                        <td>{{$amount['rejected']." / ".$amount['recovered']}}</td>
                                    @endforeach
                                </tr>
                            </tbody>
                            <tfoot>
                                <th colspan="2"></th>
                                <th>{{number_format($filtered_data['percentages']['rejected'], 2, ",", ".")}}%</th>
                                <th>{{number_format($filtered_data['percentages']['recovered'], 2, ",", ".")}}%</th>
                                <th>{{number_format($filtered_data['percentages']['recovered_telephone'], 2, ",", ".")}}%</th>
                                <th colspan="2"></th>
                                @foreach ($filtered_data['rejection_reasons'] as $column => $amount)
                                    @if ($amount['rejected'] == 0) @continue @endif
                                    <td>{{number_format((100 / (number_format($filtered_data['rejected'], 0, ",", "."))) * $amount['rejected'], 2, ",", ".")}}% / {{number_format((100 / (number_format($filtered_data['recovered'], 0, ",", "."))) * $amount['recovered'], 2, ",", ".")}}%</td>
                                @endforeach
                            </tfoot>
                        </table>
                    @endif

            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection


