@extends('layouts.backend')
@include( 'scripts.datatables' )
@include('scripts.datepicker')
@include('scripts.forms')
@include( 'scripts.dialogs' )
@include( 'scripts.chosen' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Report - {{$report->rep_name}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/reports">Reports</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$report->rep_name}}</li>
                    </ol>
                </nav>
            </div>
            <p>
                @if (!empty($report->rep_description)) <b>{{"Description: "}}</b>{{$report->rep_description}} @endif
                @if (isset($times_used)) <br />({{$times_used}} times used in the last 90 days) @endif
            </p>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="block block-rounded block-bordered">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            {{$error}}<br>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Filters
                </h3>
            </div>
            <div class="block-content block-content-full">

                <form class="mb-5" method="post" action="{{action('ReportController@viewReviewsPerCustomer', $report->rep_id)}}">
                    @csrf
                    <input name="_method" type="hidden" value="post">

                    <div class="form-group row">
                        <label class="col-md-2 col-form-label ml-5" for="for-date">Date range:</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control drp drp-default" name="date" @if(isset($date_filter) && $date_filter != null) value="{{$date_filter}}" {{--@else value="{{"1970/01/01 - 2019/09/03"}}" --}}@endif autocomplete="off"/>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-2 col-form-label ml-5" for="customer">Customer:</label>
                        <div class="col-sm-3">
                            <select type="text" class="chosen-select form-control" name="customer">
                                <option value=""></option>
                                @foreach(\App\Functions\Data::customers() as $id => $customer)
                                    <option @if (isset($customer_filter) && $id == $customer_filter) selected @endif value="{{$id}}">{{$customer}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-2 col-form-label ml-5" for="mover_status">Mover status:</label>
                        <div class="col-sm-3">
                            <select type="text" class="form-control" name="mover_status">
                                <option value=""></option>
                                <option @if(isset($moverstatus_filter) && $moverstatus_filter == 'active') {{"selected"}} @endif value="active">Active</option>
                                <option @if(isset($moverstatus_filter) && $moverstatus_filter == 'inactive') {{"selected"}} @endif value="inactive">Inactive</option>
                                <option @if(isset($moverstatus_filter) && $moverstatus_filter == 'pause') {{"selected"}} @endif value="pause">Pause</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-2 col-form-label ml-5" for="sirelo">Sirelo:</label>
                        <div class="col-sm-3">
                            <select type="text" class="form-control" name="sirelo">
                                <option value=""></option>
                                @foreach(\App\Functions\Data::getSirelos() as $id => $sirelo)
                                    <option @if (isset($sirelo_filter) && $id == $sirelo_filter) selected @endif value="{{$id}}">{{$sirelo}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="form-group col-md-8">
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div>
                    </div>

                </form>
                @if (isset($filtered_data) && $filtered_data != null)

                    <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Customer</th>
                            <th>Status</th>
                            <th>Country</th>
                            <th>Reviews</th>
                            <th>Average score</th>
                            <th>1</th>
                            <th>2</th>
                            <th>3</th>
                            <th>4</th>
                            <th>5</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($filtered_data as $id => $data)
                            <tr>
                                <td>{{$data['id']}}</td>
                                <td>{{$data['customer']}}</td>
                                <td>{{$data['status']}}</td>
                                <td>{{$data['country']}}</td>
                                <td>{{$data['reviews']}}</td>
                                <td>{{$data['average']}}</td>
                                <td>{{$data['1star']}}</td>
                                <td>{{$data['2stars']}}</td>
                                <td>{{$data['3stars']}}</td>
                                <td>{{$data['4stars']}}</td>
                                <td>{{$data['5stars']}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                        @if (isset($total_reviews) && $total_reviews != 0)
                            <tfoot>
                            <tr>
                                <th colspan="4"></th>
                                <th>{{$total_reviews}}</th>
                                <th>{{$total_average}}</th>
                                <th>{{$total_1}}</th>
                                <th>{{$total_2}}</th>
                                <th>{{$total_3}}</th>
                                <th>{{$total_4}}</th>
                                <th>{{$total_5}}</th>
                            </tr>
                            </tfoot>
                        @endif
                    </table>
                @endif
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection


