@extends('layouts.backend')
@include( 'scripts.datatables' )
@include('scripts.datepicker')
@include('scripts.forms')
@include( 'scripts.dialogs' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Report - {{$report->rep_name}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/reports">Reports</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$report->rep_name}}</li>
                    </ol>
                </nav>
            </div>
            <p>
                @if (!empty($report->rep_description)) <b>{{"Description: "}}</b>{{$report->rep_description}} @endif
                @if (isset($times_used)) <br />({{$times_used}} times used in the last 90 days) @endif
            </p>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="block block-rounded block-bordered">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            {{$error}}<br>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Filters
                </h3>
            </div>
            <div class="block-content block-content-full">

                <form class="mb-5" method="post" action="{{action('ReportController@viewPerformancePerEmployee', $report->rep_id)}}">
                    @csrf
                    <input name="_method" type="hidden" value="post">

                    <div class="form-group row">
                        <label class="col-md-2 col-form-label ml-5" for="for-date">Date range:</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control drp drp-default" name="date" @if(isset($date_filter) && $date_filter != null) value="{{$date_filter}}" @endif autocomplete="off"/>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="form-group col-md-8">
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div>
                    </div>

                </form>
                @if (isset($filtered_data) && $filtered_data != null)

                    <table data-order='[[1, "asc"]]' class="table table-bordered table-striped table-vcenter js-dataTable-full">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Employee</th>
                            <th>New booking</th>
                            <th>Upsell</th>
                            <th>Reactivation</th>
                            <th>Pause</th>
                            <th>Unpause</th>
                            <th>Credithold</th>
                            <th>Out of credithold</th>
                            <th>Cancellation</th>
                            <th>Downsell</th>
                            <th>Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($filtered_data as $id => $data)
                            <tr>
                                <td>{{$data['id']}}</td>
                                <td>{{$data['employee']}}</td>
                                <td>{{"€ ".$data['new_booking'].",00 (".$data['new_booking_count'].")"}}</td>
                                <td>{{"€ ".$data['upsell'].",00 (".$data['upsell_count'].")"}}</td>
                                <td>{{"€ ".$data['reactivation'].",00 (".$data['reactivation_count'].")"}}</td>
                                <td>{{"€ ".$data['pause'].",00 (".$data['pause_count'].")"}}</td>
                                <td>{{"€ ".$data['unpause'].",00 (".$data['unpause_count'].")"}}</td>
                                <td>{{"€ ".$data['credithold'].",00 (".$data['credithold_count'].")"}}</td>
                                <td>{{"€ ".$data['out_of_credithold'].",00 (".$data['out_of_credithold_count'].")"}}</td>
                                <td>{{"€ ".$data['cancellation'].",00 (".$data['cancellation_count'].")"}}</td>
                                <td>{{"€ ".$data['downsell'].",00 (".$data['downsell_count'].")"}}</td>
                                <td>{{"€ ".$data['total'].",00 (".$data['total_count'].")"}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="2"></th>
                                    <th>{{"€ ".$total_new_booking.",00 (".$total_count_new_booking.")"}}</th>
                                    <th>{{"€ ".$total_upsell.",00 (".$total_count_upsell.")"}}</th>
                                    <th>{{"€ ".$total_reactivation.",00 (".$total_count_reactivation.")"}}</th>
                                    <th>{{"€ ".$total_pause.",00 (".$total_count_pause.")"}}</th>
                                    <th>{{"€ ".$total_unpause.",00 (".$total_count_unpause.")"}}</th>
                                    <th>{{"€ ".$total_credithold.",00 (".$total_count_credithold.")"}}</th>
                                    <th>{{"€ ".$total_out_of_credithold.",00 (".$total_count_out_of_credithold.")"}}</th>
                                    <th>{{"€ ".$total_cancellation.",00 (".$total_count_cancellation.")"}}</th>
                                    <th>{{"€ ".$total_downsell.",00 (".$total_count_downsell.")"}}</th>
                                    <th>{{"€ ".$total_total.",00 (".$total_count_total.")"}}</th>
                                </tr>
                            </tfoot>
                    </table>
                @endif
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection


