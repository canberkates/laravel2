@extends('layouts.backend')
@include( 'scripts.datatables' )
@include('scripts.datepicker')
@include('scripts.forms')
@include( 'scripts.dialogs' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Report - {{$report->rep_name}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/reports">Reports</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$report->rep_name}}</li>
                    </ol>
                </nav>
            </div>
            <p>
                @if (!empty($report->rep_description)) <b>{{"Description: "}}</b>{{$report->rep_description}} @endif
                @if (isset($times_used)) <br />({{$times_used}} times used in the last 90 days) @endif
            </p>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="block block-rounded block-bordered">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            {{$error}}<br>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Filters
                </h3>
            </div>
            <div class="block-content block-content-full">

                <form class="mb-5" method="post" action="{{action('ReportController@viewLoadExchangeUsage', $report->rep_id)}}">
                    @csrf
                    <input name="_method" type="hidden" value="post">

                    <div class="form-group row">
                        <label class="col-md-2 col-form-label ml-5" for="for-date">Date range:</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control drp drp-default" name="date" @if(isset($date_filter) && $date_filter != null) value="{{$date_filter}}" @endif autocomplete="off"/>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-2 col-form-label ml-5" for="table_with">Table with:</label>
                        <div class="col-sm-3">
                            <select type="text" class="form-control" name="table_with">
                                <option @if(isset($table_with_filter) && $table_with_filter == 1) {{"selected"}} @endif value="1">Statistics</option>
                                <option @if(isset($table_with_filter) && $table_with_filter == 2) {{"selected"}} @endif value="2">Loads</option>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="form-group col-md-8">
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div>
                    </div>

                </form>
                @if (isset($filtered_data) && $filtered_data != null && $table_with_filter == 1)

                    <table data-order='[[1, "asc"]]'
                           class="table table-bordered table-striped table-vcenter js-dataTable-full">
                        <thead>
                        <tr>
                            <th>Company Name</th>
                            <th>(Amount of) Loads posted</th>
                            <th>Quotes offered (on Loads)</th>
                            <th>Shown interest in a quote</th>
                            <th>Got contacted about a quote</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($filtered_data as $id => $data)
                            <tr>
                                <td>{{$data['company_name']}}</td>
                                <td>{{$data['loads_posted']}}</td>
                                <td>{{$data['quotes_offered']}}</td>
                                <td>{{$data['shown_interest']}}</td>
                                <td>{{$data['got_contacted']}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                        @if (isset($total_loads_posted))
                            <tfoot>
                            <tr>
                                <th colspan="1"></th>
                                <th>{{$total_loads_posted}}</th>
                                <th>{{$total_quotes_offered}}</th>
                                <th>{{$total_shown_interest}}</th>
                                <th>{{$total_got_contacted}}</th>
                            </tr>
                            </tfoot>
                        @endif
                    </table>
                @endif

                @if (isset($filtered_data) && $filtered_data != null && $table_with_filter == 2)

                    <table data-order='[[4, "asc"]]'
                           class="table table-bordered table-striped table-vcenter js-dataTable-full">
                        <thead>
                        <tr>
                            <th>Quotes on loads</th>
                            <th>Company name</th>
                            <th>City and Country from</th>
                            <th>City and Country to</th>
                            <th>Load timestamp</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($filtered_data as $id => $data)
                            <tr>
                                <td>
                                    <button type="button" class="btn btn-sm btn-primary details_show_blank js-tooltip-enabled" data-toggle="tooltip" title="" data-details="{{$data['sub_table']}}" data-original-title="Expand">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </td>
                                <td>{{$data['company_name']}}</td>
                                <td>{{$data['city_and_country_from']}}</td>
                                <td>{{$data['city_and_country_to']}}</td>
                                <td>{{$data['load_timestamp']}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif


            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection

