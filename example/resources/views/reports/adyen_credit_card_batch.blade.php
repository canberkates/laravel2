@extends('layouts.backend')
@include( 'scripts.datatables' )
@include('scripts.datepicker')
@include('scripts.forms')
@include( 'scripts.dialogs' )
@include('scripts.select2')
@pushonce( 'scripts:reports' )
    <script src="{{ asset('/js/custom/reports/reports.js') }}"></script>
@endpushonce

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Report - {{$report->rep_name}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/reports">Reports</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$report->rep_name}}</li>
                    </ol>
                </nav>
            </div>
            <p>
                @if (!empty($report->rep_description)) <b>{{"Description: "}}</b>{{$report->rep_description}} @endif
                @if (isset($times_used)) <br />({{$times_used}} times used in the last 90 days) @endif
            </p>        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="block block-rounded block-bordered col-md-6">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            {{$error}}<br>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
        <div class="row">

        <div class="block block-rounded block-bordered col-md-12">
            <form method="post" id="single-report-progress" name="single-report-progress">
                <input type="hidden" id="report_id" name="report_id" value="{{$report->rep_id}}"/>
                <input type="hidden" id="report_progress_id" name="report_progress_id" value="{{$repr_id}}"/>
                <input type="hidden" id="report_status" name="report_status" value="@if (isset($report_progress)) {{$report_progress->repr_status}} @endif"/>
                <div id="single-report-html"></div>
            </form>

            <div class="progress push" style="height: 10px; margin-top: 10px">
                <div id="progressbar" class="progress-bar progress-bar-striped bg-success" role="progressbar" style="" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <hr>
            @if (isset($repr_id))
                {!! App\Functions\Reporting::getAllGeneratedReportsByReportId($report->rep_id, $repr_id) !!}
            @else
                {!! App\Functions\Reporting::getAllGeneratedReportsByReportId($report->rep_id) !!}
            @endif

        </div>
        </div>
    </div>
    @isset($file)
        <div class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="block-content tab-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="block block-rounded block-bordered">
                                    <div class="block-content">
                                        <h2 class="content-heading pt-0">{{$report->rep_name}} - Report</h2>

                                        <div class="row">
                                            <div class="col-md-12">
                                                {!! $file !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endisset

    <!-- END Page Content -->
@endsection

