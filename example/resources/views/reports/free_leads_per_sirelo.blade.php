@extends('layouts.backend')
@include( 'scripts.datatables' )
@include('scripts.datepicker')
@include('scripts.forms')
@include( 'scripts.dialogs' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Report - {{$report->rep_name}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/reports">Reports</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$report->rep_name}}</li>
                    </ol>
                </nav>
            </div>
            <p>
                @if (!empty($report->rep_description)) <b>{{"Description: "}}</b>{{$report->rep_description}} @endif
                @if (isset($times_used)) <br />({{$times_used}} times used in the last 90 days) @endif
            </p>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="block block-rounded block-bordered">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            {{$error}}<br>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Filters
                </h3>
            </div>
            <div class="block-content block-content-full">

                <form class="mb-5" method="post" action="{{action('ReportController@viewFreeLeadsPerSirelo', $report->rep_id)}}">
                    @csrf
                    <input name="_method" type="hidden" value="post">

                    <div class="form-group row">
                        <label class="col-md-2 col-form-label ml-5" for="for-date">Date range:</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control drp drp-default" name="date" @if(isset($date_filter) && $date_filter != null) value="{{$date_filter}}" @endif autocomplete="off"/>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label ml-5" for="show_who_received">Show who received:</label>
                        <div class="col-sm-4">
                            <div class="custom-control custom-switch custom-control custom-control-inline custom-control-lg custom-control-primary">
                                <input type="checkbox" class="custom-control-input" id="show_who_received" name="show_who_received" @if((isset($show_who_received) && $show_who_received == "on") && $show_who_received != null) checked @endif>
                                <label class="custom-control-label" for="show_who_received"></label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label ml-5" for="show_exclusive">Show exclusive:</label>
                        <div class="col-sm-4">
                            <div class="custom-control custom-switch custom-control custom-control-inline custom-control-lg custom-control-primary">
                                <input type="checkbox" class="custom-control-input" id="show_exclusive" name="show_exclusive" @if((isset($show_exclusive) && $show_exclusive == "on") && $show_exclusive != null) checked @endif>
                                <label class="custom-control-label" for="show_exclusive"></label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="form-group col-md-8">
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div>
                    </div>

                </form>
                @if (isset($filtered_data) && $filtered_data != null)

                    <table data-order='[[0, "asc"]]'
                        class="table table-bordered table-striped table-vcenter js-dataTable-full">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Amount</th>

                            @if($show_exclusive === "on")
                                <th>Exclusive amount</th>
                            @endif

                            @if($show_who_received === "on")
                                <th>Received by</th>
                            @endif

                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($filtered_data as $data)
                            <tr>
                                <td>{{$data['name']}}</td>
                                <td>{{$data['amount']}}</td>

                                @if ($show_exclusive === "on")
                                    <td>{{$data['exclusive']}}</td>
                                @endif

                                @if ($show_who_received === "on")
                                    <td>{{$data['company_name']}}</td>
                                @endif


                            </tr>
                        @endforeach
                        </tbody>
                        @if (isset($total_free_leads))
                            <tfoot>
                            <tr>
                                <th colspan="1"></th>
                                <th>{{$total_free_leads}}</th>
                                @if ($show_exclusive === "on")
                                    <th>{{$total_exclusive}}</th>
                                @endif
                                @if ($show_who_received === "on")
                                    <th colspan='1'></th>
                                @endif

                            </tr>
                            </tfoot>
                        @endif
                    </table>
                @endif
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection
