@extends('layouts.backend')
@include( 'scripts.datatables' )
@include('scripts.datepicker')
@include('scripts.forms')
@include( 'scripts.dialogs' )
@include('scripts.select2')


@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Report - {{$report->rep_name}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/reports">Reports</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$report->rep_name}}</li>
                    </ol>
                </nav>
            </div>
            <p>
                @if (!empty($report->rep_description)) <b>{{"Description: "}}</b>{{$report->rep_description}} @endif
                @if (isset($times_used)) <br />({{$times_used}} times used in the last 90 days) @endif
            </p>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="block block-rounded block-bordered">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            {{$error}}<br>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Filters
                </h3>
            </div>
            <div class="block-content block-content-full">

                <form class="mb-5" method="post" action="{{action('ReportController@viewActiveCampaign', $report->rep_id)}}">
                    @csrf
                    <input name="_method" type="hidden" value="post">

                    <div class="form-group row">
                        <label class="col-md-2 col-form-label ml-5" for="types">Types:</label>
                        <div class="col-md-4">
                            <select class="js-select2 form-control" multiple data-placeholder="Select some options..."
                                    name="types[]" id="types" style="width:100%;">
                                <option></option>

                                @foreach(\App\Data\CustomerType::all() as $id => $type)
                                    <option @if (in_array($id, $types_filter)) selected @endif value="{{$id}}">{{$type}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-2 col-form-label ml-5" for="countries">Countries:</label>
                        <div class="col-md-4">
                            <select class="js-select2 form-control" multiple data-placeholder="Select some options..."
                                    name="countries[]" id="countries" style="width:100%;">
                                <option></option>

                                @foreach($countries as $country)
                                    <option @if (in_array($country->co_code, $countries_filter)) selected @endif value="{{$country->co_code}}">{{$country->co_en}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-2 col-form-label ml-5" for="markets">Markets:</label>
                        <div class="col-md-4">
                            <select class="js-select2 form-control" multiple data-placeholder="Select some options..."
                                    name="markets[]" id="markets" style="width:100%;">
                                <option></option>

                                @foreach($markets as $id => $market)
                                    <option @if (in_array($id, $market_filter)) selected @endif value="{{$id}}">{{$market}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-2 col-form-label ml-5" for="statuses">Statuses:</label>
                        <div class="col-md-4">
                            <select class="js-select2 form-control" multiple data-placeholder="Select some options..."
                                    name="statuses[]" id="statuses" style="width:100%;">
                                <option></option>

                                @foreach($statuses as $id => $status)
                                    <option @if (in_array($id, $status_filter)) selected @endif value="{{$id}}">{{$status}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-2 col-form-label ml-5" for="memberships">Memberships:</label>
                        <div class="col-md-4">
                            <select class="js-select2 form-control" multiple data-placeholder="Select some options..."
                                    name="memberships[]" id="memberships" style="width:100%;">
                                <option></option>

                                @foreach($all_memberships as $membership)
                                    <option @if (in_array($membership->me_id, $membership_filter)) selected @endif value="{{$membership->me_id}}">{{$membership->me_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="form-group col-md-8">
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div>
                    </div>

                </form>
                @if (isset($filtered_data) && $filtered_data != null)
                    <table data-order='[[1, "asc"]]'
                           class="table table-bordered table-striped table-vcenter js-dataTable-full">
                        <thead>
                        <tr>
                            <th>Company Name</th>
                            <th>Country</th>
                            <th>Region</th>
                            <th>Continent</th>
                            <th>Market</th>
                            <th>Services</th>
                            <th>Associations</th>
                            <th>Status</th>
                            <th>Title</th>
                            <th>First name</th>
                            <th>Last name</th>
                            <th>Email</th>
                            <th>Language</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($filtered_data as $id => $data)
                            <tr>
                                <td>{{$data['company_name']}}</td>
                                <td>{{$data['country']}}</td>
                                <td>{{$data['region']}}</td>
                                <td>{{$data['continent']}}</td>
                                <td>{{$data['market']}}</td>
                                <td>{{$data['services']}}</td>
                                <td>{{$data['associations']}}</td>
                                <td>{{$data['status']}}</td>
                                <td>{{$data['title']}}</td>
                                <td>{{$data['first_name']}}</td>
                                <td>{{$data['last_name']}}</td>
                                <td>{{$data['email']}}</td>
                                <td>{{$data['language']}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection


