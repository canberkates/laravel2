@extends('layouts.backend')
@include( 'scripts.datatables' )
@include('scripts.datepicker')
@include('scripts.forms')
@include( 'scripts.dialogs' )
@include('scripts.select2')


@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Report - {{$report->rep_name}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/reports">Reports</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$report->rep_name}}</li>
                    </ol>
                </nav>
            </div>
            <p>
                @if (!empty($report->rep_description)) <b>{{"Description: "}}</b>{{$report->rep_description}} @endif
                @if (isset($times_used)) <br />({{$times_used}} times used in the last 90 days) @endif
            </p>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="block block-rounded block-bordered">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            {{$error}}<br>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="block-content block-content-full">

                <table data-order='[[0, "asc"]]'
                       class="table table-bordered table-striped table-vcenter js-dataTable-full">
                    <thead>
                    <tr>
                        <th>Country</th>
                        <th>To validate</th>
                        <th>Processed</th>
                        <th>Left to process (First scan)</th>
                        <th>Left to process (Updated)</th>
                        <th>Skipped</th>
                        <th>Unique results</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($filtered_data as $id => $data)
                        <tr @if($data['color']  != 'none') style="background-color: lightgreen;" @endif>
                            <td>{{$data['country']}}</td>
                            <td>{{$data['gathered']}}</td>
                            <td>{{$data['processed']}}</td>
                            <td>{{$data['processed_first_left']}}</td>
                            <td>{{$data['processed_updated_left']}}</td>
                            <td>{{$data['skipped']}}</td>
                            <td>{{$data['unique_results']}}</td>
                        </tr>

                    @endforeach
                    </tbody>
                    <tfoot style='font-weight: bold;'>
                        <tr>
                            <td colspan='1'></td>
                            <td>{{$total_gathered}}</td>
                            <td>{{$total_processed}}</td>
                            <td>{{$total_processed_first_time_left}}</td>
                            <td>{{$total_processed_not_first_left}}</td>
                            <td>{{$total_skipped}}</td>
                            <td>{{$total_unique}}</td>
                        </tr>
                    </tfoot>
                </table>

            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection


