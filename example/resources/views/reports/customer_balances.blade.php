@extends('layouts.backend')
@include( 'scripts.datatables' )
@include('scripts.datepicker')
@include('scripts.forms')
@include( 'scripts.dialogs' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Report - {{$report->rep_name}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/reports">Reports</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$report->rep_name}}</li>
                    </ol>
                </nav>
            </div>
            <p>
                @if (!empty($report->rep_description)) <b>{{"Description: "}}</b>{{$report->rep_description}} @endif
                @if (isset($times_used)) <br />({{$times_used}} times used in the last 90 days) @endif
            </p>        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="block block-rounded block-bordered">
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Filters
                </h3>
            </div>
            <div class="block-content block-content-full">

                <form class="mb-5" method="post" action="{{action('ReportController@viewCustomerBalances', $report->rep_id)}}">
                    @csrf
                    <input name="_method" type="hidden" value="post">

                    @foreach(\App\Data\CustomerPairStatus::all() as $id => $value)
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="status[{{$id}}]">@if ($loop->first) {{"Status:"}} @endif</label>
                            <div class="col-sm-1">
                                {{$value}}
                            </div>
                            <div class="col-sm-7">
                                <div class="custom-control custom-switch custom-control custom-control-inline custom-control-primary">
                                    <input type="checkbox" class="custom-control-input" id="status[{{$id}}]" name="status[{{$id}}]" @if (isset($dataFiltered) && $dataFiltered == true) {{(isset($status_checked[$id]) && $status_checked[$id] == "checked" ? "checked" : "")}} @else {{(($id == 1 || $id == 2) ? "checked" : "")}} @endif >
                                    <label class="custom-control-label" for="status[{{$id}}]"></label>
                                </div>
                            </div>
                        </div>
                    @endforeach

                    @foreach(\App\Data\PaymentMethod::all() as $id => $value)
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="payment_method[{{$id}}]">@if ($loop->first) {{"Payment method:"}} @endif</label>
                            <div class="col-sm-1">
                                {{$value}}
                            </div>
                            <div class="col-sm-7">
                                <div class="custom-control custom-switch custom-control custom-control-inline custom-control-primary">
                                    <input type="checkbox" class="custom-control-input" id="payment_method[{{$id}}]" name="payment_method[{{$id}}]" @if (isset($dataFiltered) && $dataFiltered == true) {{(isset($payment_methods_checked[$id]) && $payment_methods_checked[$id] == "checked" ? "checked" : "")}} @else {{"checked"}} @endif >
                                    <label class="custom-control-label" for="payment_method[{{$id}}]"></label>
                                </div>
                            </div>
                        </div>
                    @endforeach

                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="form-group col-md-8">
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div>
                    </div>

                </form>
                @if (isset($filtered_data) && $filtered_data != null)
                <table data-order='[[0, "asc"]]'
                       class="table table-bordered table-striped table-vcenter js-dataTable-full">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Customer</th>
                        <th>Country</th>
                        <th>Status</th>
                        <th>Debtor status</th>
                        <th>Credit hold</th>
                        <th>Payment method</th>
                        <th>Credit limit</th>
                        <th>Balance (incl. not invoiced)</th>
                        <th>To spend</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($filtered_data as $id => $data)
                        <tr>
                            <td>{{$data['cu_id']}}</td>
                            <td>{{$data['cu_company_name_business']}}</td>
                            <td>{{$data['country']}}</td>
                            <td>{{$data['status']}}</td>
                            <td>{{$data['debtorstatus']}}</td>
                            <td>{{$data['credithold']}}</td>
                            <td>{{$data['paymentmethod']}}</td>
                            <td>{{$data['creditlimit']}}</td>
                            <td>{{$data['balance']}}</td>
                            <td>{{$data['to_spend']}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @endif
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection


