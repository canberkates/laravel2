@extends('layouts.backend')
@include( 'scripts.datatables' )
@include('scripts.datepicker')
@include('scripts.forms')
@include( 'scripts.dialogs' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Report - {{$report->rep_name}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/reports">Reports</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$report->rep_name}}</li>
                    </ol>
                </nav>
            </div>
            <p>
                @if (!empty($report->rep_description)) <b>{{"Description: "}}</b>{{$report->rep_description}} @endif
                @if (isset($times_used)) <br />({{$times_used}} times used in the last 90 days) @endif
            </p>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="block block-rounded block-bordered">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            {{$error}}<br>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Filters
                </h3>
            </div>
            <div class="block-content block-content-full">

                <form class="mb-5" method="post" action="{{action('ReportController@viewRequestsPerRejectionReason', $report->rep_id)}}">
                    @csrf
                    <input name="_method" type="hidden" value="post">

                    <div class="form-group row">
                        <label class="col-md-2 col-form-label ml-5" for="for-date">Date range:</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control drp drp-default" name="date" @if(isset($date_filter) && $date_filter != null) value="{{$date_filter}}" @endif autocomplete="off"/>
                        </div>
                    </div>

                    @foreach(App\Data\RequestType::all() as $id => $type)
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label ml-5"
                                   for="moda_activate_premium_leads">@if($loop->first) {{"Request type"}} @endif</label>
                            <div class="col-sm-7">
                                <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                    <input type="checkbox" class="custom-control-input"
                                           id="request_type[{{$id}}]"
                                           name="request_type[{{$id}}]"{{(isset($request_type_filter) && in_array($id, $request_type_filter) ? "checked=''" : (!isset($request_type_filter) ? "checked=''" : ""))}}>
                                    <label class="custom-control-label"
                                           for="request_type[{{$id}}]">{{$type}}</label>
                                </div>
                            </div>
                        </div>
                    @endforeach

                    <div class="form-group row">
                        <label class="col-md-2 col-form-label ml-5" for="request_origin">Request origin:</label>
                        <div class="col-sm-3">
                            <select type="text" class="form-control" name="request_origin">
                                <option value=""></option>
                                @foreach(\App\Data\RequestSource::all() as $id => $origin)
                                    <option @if (isset($request_origin_filter) && $id == $request_origin_filter) selected @endif value="{{$id}}">{{$origin}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="form-group col-md-8">
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div>
                    </div>

                </form>
                @if (isset($filtered_data) && $filtered_data != null)

                    <table data-order='[]'
                           class="table table-bordered table-striped table-vcenter js-dataTable-full">
                        <thead>
                        <tr>
                            <th>Rejection reason</th>
                            <th>Requests</th>
                            <th>%</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($filtered_data as $id => $data)
                            <tr>
                                <td>{{$data['rejection_reason']}}</td>
                                <td>{{$data['requests']}}</td>
                                <td>{{$data['percentage']}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                        @if (isset($total_requests))
                            <tfoot>
                            <tr>
                                <th colspan="1"></th>
                                <th>{{$total_requests}}</th>
                                <th>100%</th>
                            </tr>
                            </tfoot>
                        @endif
                    </table>
                @endif
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection
