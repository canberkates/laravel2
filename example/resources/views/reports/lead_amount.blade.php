@extends('layouts.backend')
@include( 'scripts.datatables' )
@include('scripts.datepicker')
@include('scripts.forms')
@include( 'scripts.dialogs' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Report - {{$report->rep_name}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/reports">Reports</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$report->rep_name}}</li>
                    </ol>
                </nav>
            </div>
            <p>
                @if (!empty($report->rep_description)) <b>{{"Description: "}}</b>{{$report->rep_description}} @endif
                @if (isset($times_used)) <br />({{$times_used}} times used in the last 90 days) @endif
            </p>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="block block-rounded block-bordered">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            {{$error}}<br>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Filters
                </h3>
            </div>
            <div class="block-content block-content-full">

                <form class="mb-5" method="post" action="{{action('ReportController@viewLeadAmounts', $report->rep_id)}}">
                    @csrf
                    <input name="_method" type="hidden" value="post">

                    <div class="form-group row">
                        <label class="col-md-2 col-form-label ml-5" for="for-date">Date range:</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control drp drp-default" name="date" @if(isset($date_filter) && $date_filter != null) value="{{$date_filter}}" @endif autocomplete="off"/>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="form-group col-md-8">
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div>
                    </div>

                </form>
                @if (isset($filtered_data) && $filtered_data != null)

                    <ul class="nav nav-tabs nav-tabs-alt" data-toggle="tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" href="#btabs-alt-static-int_af">INT Affiliate leads</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-int_tgb">INT TriGlobal leads</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-int_total">INT Total leads</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-nat_af">NAT Affiliate leads</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-nat_tgb">NAT TriGlobal leads</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-alt-static-nat_total">NAT Total leads</a>
                        </li>
                    </ul>
                    <div class="block-content tab-content">
                        <div class="tab-pane active" id="btabs-alt-static-int_af" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        <table data-order='[[1, "desc"]]'
                                               class="table cell-border row-border table-bordered  table-striped table-vcenter js-dataTable-full">
                                            <thead>
                                            <tr>
                                                <th>Country</th>
                                                <th>Affiliate leads</th>
                                                <th>Sirelo leads</th>
                                                <th>Total</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($filtered_data['INT_AF']['TABLE_TOTAL'] as $index => $data)
                                                <tr>
                                                    <td>{{$data['co_en']}}</td>
                                                    <td>{{$filtered_data['INT_AF']['TABLE_AF'][$index]['count'] ?? 0}}</td>
                                                    <td>{{$filtered_data['INT_AF']['TABLE_SIRELO'][$index]['count'] ?? 0}}</td>
                                                    <td>{{$filtered_data['INT_AF']['TABLE_TOTAL'][$index]['count']}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th colspan="1"></th>
                                                <th>{{$filtered_data['INT_AF']['TOTAL_AF']}}</th>
                                                <th>{{$filtered_data['INT_AF']['TOTAL_SIRELO']}}</th>
                                                <th>{{$filtered_data['INT_AF']['TOTAL_COMBINED']}}</th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="btabs-alt-static-int_tgb" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        <table data-order='[[1, "desc"]]'
                                               class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                            <thead>
                                            <tr>
                                                <th width="50%">Country</th>
                                                <th>TriGlobal Leads (Excl. Sirelo)</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($filtered_data['INT_TGB']['TABLE'] as $data)
                                                <tr>
                                                    <td>{{$data['co_en']}}</td>
                                                    <td>{{$data['count']}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th colspan="1"></th>
                                                <th>{{$filtered_data['INT_TGB']['TOTAL']}}</th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="btabs-alt-static-int_total" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        <table data-order='[[1, "desc"]]'
                                               class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                            <thead>
                                            <tr>
                                                <th width="50%">Country</th>
                                                <th>Total leads</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($filtered_data['INT_TOTAL']['TABLE'] as $data)
                                                <tr>
                                                    <td>{{$data['co_en']}}</td>
                                                    <td>{{$data['count']}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th colspan="1"></th>
                                                <th>{{$filtered_data['INT_TOTAL']['TOTAL']}}</th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="btabs-alt-static-nat_af" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        <table data-order='[[1, "desc"]]'
                                               class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                            <thead>
                                            <tr>
                                                <th>Country</th>
                                                <th>Affiliate leads</th>
                                                <th>Sirelo leads</th>
                                                <th>Total</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($filtered_data['NAT_AF']['TABLE_TOTAL'] as $index => $data)
                                                <tr>
                                                    <td>{{$data['co_en']}}</td>
                                                    <td>{{$filtered_data['NAT_AF']['TABLE_AF'][$index]['count'] ?? 0}}</td>
                                                    <td>{{$filtered_data['NAT_AF']['TABLE_SIRELO'][$index]['count'] ?? 0}}</td>
                                                    <td>{{$filtered_data['NAT_AF']['TABLE_TOTAL'][$index]['count']}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th colspan="1"></th>
                                                <th>{{$filtered_data['NAT_AF']['TOTAL_AF']}}</th>
                                                <th>{{$filtered_data['NAT_AF']['TOTAL_SIRELO']}}</th>
                                                <th>{{$filtered_data['NAT_AF']['TOTAL_COMBINED']}}</th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="btabs-alt-static-nat_tgb" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        <table data-order='[[1, "desc"]]'
                                               class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                            <thead>
                                            <tr>
                                                <th width="50%">Country</th>
                                                <th>TriGlobal Leads (Excl. Sirelo)</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($filtered_data['NAT_TGB']['TABLE'] as $data)
                                                <tr>
                                                    <td>{{$data['co_en']}}</td>
                                                    <td>{{$data['count']}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th colspan="1"></th>
                                                <th>{{$filtered_data['NAT_TGB']['TOTAL']}}</th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="btabs-alt-static-nat_total" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="block block-rounded block-bordered">
                                        <table data-order='[[1, "desc"]]'
                                               class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                            <thead>
                                            <tr>
                                                <th width="50%">Country</th>
                                                <th>Total leads</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($filtered_data['NAT_TOTAL']['TABLE'] as $data)
                                                <tr>
                                                    <td>{{$data['co_en']}}</td>
                                                    <td>{{$data['count']}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th colspan="1"></th>
                                                <th>{{$filtered_data['NAT_TOTAL']['TOTAL']}}</th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                @endif


            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection

