@extends('layouts.backend')
@include( 'scripts.datatables' )
@include('scripts.datepicker')
@include('scripts.forms')
@include( 'scripts.dialogs' )
{{--@include('scripts.select2')--}}
@include('scripts.chosen')
@pushonce( 'scripts:reports' )
    <script src="{{ asset('/js/custom/reports/reports.js') }}"></script>
@endpushonce

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Report - {{$report->rep_name}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/reports">Reports</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$report->rep_name}}</li>
                    </ol>
                </nav>
            </div>
            <p>
                @if (!empty($report->rep_description)) <b>{{"Description: "}}</b>{{$report->rep_description}} @endif
                @if (isset($times_used)) <br />({{$times_used}} times used in the last 90 days) @endif
            </p>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="row">
        <div class="block block-rounded block-bordered col-md-6">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            {{$error}}<br>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Filters
                </h3>
            </div>
            <div class="block-content block-content-full">

                <form class="mb-5" method="post" action="{{action('ReportController@queueMissedMatches', $report->rep_id)}}">
                    @csrf
                    <input name="_method" type="hidden" value="post">

                    <div class="form-group row">
                        <label class="col-md-4 col-form-label ml-5" for="for-date">Date range:</label>
                        <div class="col-md-6">
                            <input  @if(isset($repr_id) && $clone === false) disabled @endif type="text" class="form-control drp drp-default" name="date" @if(isset($repr_id)) value="{{App\Functions\System::unserialize($report_progress->repr_posted_values)['date_from']." - ".App\Functions\System::unserialize($report_progress->repr_posted_values)['date_to']}}" @endif autocomplete="off"/>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4 col-form-label ml-5" for="report_type">Report type</label>
                        <div class="col-md-6">
                            <select @if(isset($repr_id) && $clone === false) disabled @endif type="text" class="chosen-select form-control" id="select_report_type" name="report_type">
                                <option value=""></option>
                                <option @if(App\Functions\System::unserialize($report_progress->repr_posted_values)['report_type'] == 1) selected @endif value="1">INTMOVING</option>
                                <option @if(App\Functions\System::unserialize($report_progress->repr_posted_values)['report_type'] == 2) selected @endif value="2">NATMOVING</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4 col-form-label ml-5" for="request_type">Request type {{App\Functions\System::unserialize($report_progress->repr_posted_values)['request_type']}}</label>
                        <div class="col-md-6">
                            <select @if(isset($repr_id) && $clone === false) disabled @endif type="text" name="request_type" class="chosen-select form-control" style="width:100%;">
                                <option @if(in_array(1, App\Functions\System::unserialize($report_progress->repr_posted_values)['request_type']) && in_array(5, App\Functions\System::unserialize($report_progress->repr_posted_values)['request_type'])) selected @endif value="">Both</option>
                                <option @if(in_array(1, App\Functions\System::unserialize($report_progress->repr_posted_values)['request_type']) && !in_array(5, App\Functions\System::unserialize($report_progress->repr_posted_values)['request_type'])) selected @endif value="1">Requests</option>
                                <option @if(in_array(5, App\Functions\System::unserialize($report_progress->repr_posted_values)['request_type']) && !in_array(1, App\Functions\System::unserialize($report_progress->repr_posted_values)['request_type'])) selected @endif value="2">Premium leads</option>
                            </select>
                        </div>
                    </div>

                    <div id="origin_type_div" class="form-group row">
                        <label class="col-md-4 col-form-label ml-5" for="origin_type">Origin type</label>
                        <div class="col-md-6">
                            <select @if(isset($repr_id) && $clone === false) disabled @endif type="text" class="chosen-select form-control" id="select_origin_type" name="origin_type">
                                <option value=""></option>
                                <option @if(App\Functions\System::unserialize($report_progress->repr_posted_values)['origin_type'] == 1) selected @endif value="1">Regions</option>
                                <option @if(App\Functions\System::unserialize($report_progress->repr_posted_values)['origin_type'] == 2) selected @endif value="2">Greater Regions</option>
                                <option @if(App\Functions\System::unserialize($report_progress->repr_posted_values)['origin_type'] == 3) selected @endif value="3">Countries</option>
                            </select>
                        </div>
                    </div>

                    <div id="origin_values_div" class="form-group row">
                        <label class="col-md-4 col-form-label ml-5" for="origin_values">Filter Origins</label>
                        <div class="col-md-6">
                            <select @if(isset($repr_id) && $clone === false) disabled @endif type="text" multiple class="chosen-select form-control" id="select_origin_values" name="origin_values[]">
                                @if(isset($repr_id))
                                    @if(isset($filter_data['filter_origins']))
                                        @foreach ($filter_data['filter_origins'][0] as $optgroup => $array)
                                            <optgroup label="{{$optgroup}}">
                                                @foreach($array as $id => $value)
                                                    @if($value == "") @continue; @endif
                                                    <option @if (in_array($id, App\Functions\System::unserialize($report_progress->repr_posted_values)['origin_values'])) {{"selected"}} @endif value="{{$id}}">{{$value}}</option>
                                                @endforeach
                                            </optgroup>
                                        @endforeach
                                    @endif
                                @endif
                            </select>
                        </div>
                    </div>

                    <div id="destination_type_div" class="form-group row">
                        <label class="col-md-4 col-form-label ml-5" for="destination_type">Destination type</label>
                        <div class="col-md-6">
                            <select @if(isset($repr_id) && $clone === false) disabled @endif type="text" class="chosen-select form-control" id="select_destination_type" name="destination_type">
                                @if(isset($repr_id))
                                    @if(isset($filter_data['destination_types']))
                                        @foreach ($filter_data['destination_types'] as $id => $type)
                                            <option @if(App\Functions\System::unserialize($report_progress->repr_posted_values)['destination_type'] == $id) {{'selected'}} @endif value="{{$id}}">{{$type}}</option>
                                        @endforeach
                                    @endif
                                @endif
                            </select>
                        </div>
                    </div>

                    <div id="destination_values_div" class="form-group row">
                        <label class="col-md-4 col-form-label ml-5" for="destination_values">Filter destinations</label>
                        <div class="col-md-6">
                            <select @if(isset($repr_id) && $clone === false) disabled @endif type="text" multiple class="chosen-select form-control" id="select_destination_values" name="destination_values[]">
                                @if(isset($repr_id))
                                    @if(isset($filter_data['filter_destinations']))
                                        @if(App\Functions\System::unserialize($report_progress->repr_posted_values)['destination_type'] == 3)
                                            @foreach ($filter_data['filter_destinations'][0] as $id => $destination)
                                                <option @if (in_array($id, App\Functions\System::unserialize($report_progress->repr_posted_values)['destination_values'])) {{"selected"}} @endif value="{{$id}}">{{$destination}}</option>
                                            @endforeach
                                        @else

                                            @foreach ($filter_data['filter_destinations'][0] as $optgroup)
                                                <optgroup label="{{$optgroup}}">
                                                    @foreach($optgroup as $id => $value)
                                                        <option @if (in_array($id, App\Functions\System::unserialize($report_progress->repr_posted_values)['destination_values'])) {{"selected"}} @endif value="{{$id}}">{{$value}}</option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        @endif
                                    @endif
                                @endif
                            </select>
                        </div>
                    </div>

                    @foreach(\App\Data\MovingSize::all() as $id => $type)
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label ml-5"
                                   for="moda_activate_premium_leads">@if($loop->first) {{"Moving size"}} @endif</label>
                            <div class="col-sm-6">
                                <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                    <input @if(isset($repr_id) && $clone === false) disabled @endif type="checkbox" class="custom-control-input"
                                           id="moving_size[{{$id}}]"
                                           name="moving_size[{{$id}}]"
                                        @if(App\Functions\System::unserialize($report_progress->repr_posted_values)['moving_size'][$id] == "on") {{"checked=''"}} @endif>
                                    <label class="custom-control-label"
                                           for="moving_size[{{$id}}]">{{$type}}</label>
                                </div>
                            </div>
                        </div>
                    @endforeach

                    @foreach(\App\Data\RequestSource::all() as $id => $source)
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label ml-5"
                                   for="moda_activate_premium_leads">@if($loop->first) {{"Source"}} @endif</label>
                            <div class="col-sm-6">
                                <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                    <input @if(isset($repr_id) && $clone === false) disabled @endif type="checkbox" class="custom-control-input source_{{$id}}"
                                           id="source[{{$id}}]"
                                           name="source[{{$id}}]"
                                        @if(App\Functions\System::unserialize($report_progress->repr_posted_values)['source'][$id] == "on" || !isset($repr_id)) {{"checked=''"}} @endif>
                                    <label class="custom-control-label"
                                           for="source[{{$id}}]">{{$source}}</label>
                                </div>
                            </div>
                        </div>
                    @endforeach



                    <div id="filter_websites_div" class="form-group row mb-1">
                        <label class="col-md-4 col-form-label ml-5" for="origin_values">Websites</label>
                        <div class="col-md-6">
                            <select @if(isset($repr_id) && $clone === false) disabled @endif type="text" multiple class="chosen-select form-control" id="select_websites" name="website[]">
                                @foreach($websites as $id => $website)
                                    <option value="{{$id}}" @if (in_array($id, $selected_websites)) selected @endif >{{$website}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div id="select_all_sirelos" class="row">
                        <label class="col-md-4 col-form-label ml-5"></label>
                        <div class="col-sm-6">
                            <a id="select_all_sirelos_link" style="color:#0665d0; transition: color 0.12s ease-out; cursor: pointer;">Select all Sirelo's</a>
                        </div>
                    </div>

                    <div id="select_all_non_sirelos" class="row">
                        <label class="col-md-4 col-form-label ml-5"></label>
                        <div class="col-sm-6">
                            <a id="select_all_non_sirelos_link" style="color:#0665d0; transition: color 0.12s ease-out; cursor: pointer;">Select all NON-Sirelo's</a>
                        </div>
                    </div>





                    <div class="form-group row mt-2">
                        <label class="col-md-4 col-form-label ml-5" for="portal">Comment:</label>
                        <div class="col-md-6">
                            <input @if(isset($repr_id) && $clone === false) disabled @endif class="form-control" name="report_comment" type="text"
                                   value="{{App\Functions\System::unserialize($report_progress->repr_posted_values)['report_comment']}}">
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="form-group col-md-8">
                            <button type="submit" class="btn btn-primary">Queue report</button>
                        </div>
                    </div>
                    @if(isset($repr_id))
                        <hr>
                        <div class="row">
                            <div class="col-md-1"></div>
                            <p>
                            <a href="/reports/{{$report->rep_id}}/view/" @if(isset($repr_id) && $clone === false) class="btn btn-primary" @else class="btn btn-danger" @endif>New report</a>
                            @if($clone === false)
                                <a href="/reports/{{$report->rep_id}}/view/{{$report->rep_report}}/{{$repr_id}}/clone/" class="btn btn-primary">Clone report</a>
                                <a href="/reports/report_progress/{{$repr_id}}/delete" class="btn btn-primary">Delete report</a>
                            @endif
                            <a href="/reports" class="btn btn-primary">Back</a>
                            </p>
                        </div>
                    @endif
                </form>
            </div>
        </div>
        <div class="block block-rounded block-bordered col-md-6">
            <form method="post" id="single-report-progress" name="single-report-progress">
                <input type="hidden" id="report_id" name="report_id" value="{{$report->rep_id}}"/>
                <input type="hidden" id="report_progress_id" name="report_progress_id" value="{{$repr_id}}"/>
                <input type="hidden" id="report_status" name="report_status" value="@if (isset($report_progress)) {{$report_progress->repr_status}} @endif"/>
                <div id="single-report-html"></div>
            </form>

            <div class="progress push" style="height: 10px; margin-top: 10px">
                <div id="progressbar" class="progress-bar progress-bar-striped bg-success" role="progressbar" style="" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <hr>
        @if (isset($repr_id))
                {!! App\Functions\Reporting::getAllGeneratedReportsByReportId($report->rep_id, $repr_id) !!}
            @else
                {!! App\Functions\Reporting::getAllGeneratedReportsByReportId($report->rep_id) !!}
            @endif

        </div>
        </div>
    </div>
    @isset($file)
        <div class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="block-content tab-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="block block-rounded block-bordered">
                                    <div class="block-content">
                                        <h2 class="content-heading pt-0">{{$report->rep_name}} - Report</h2>

                                        <div class="row">
                                            <div class="col-md-12">
                                                {!! $file !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endisset

    <!-- END Page Content -->
@endsection

@push('scripts')
<script type="text/javascript">
    function json_targets(target_div, current_data, options, data_function) {
        if (current_data === null) {
            update_chosen_select(target_div, {"" : "Loading..."});
            $.getJSON(
                "{{ url('/json/portal_options') }}",
                options,
                function(data){
                    if (data_function(data)) {
                    	console.log(data);
                        update_chosen_select(target_div, data);
                    }
                }
            );
        } else {
            update_chosen_select(target_div, current_data);
        }
    }

    function update_chosen_select(element, data, val){
        $(element).empty();

		if(typeof(val) == "undefined"){
			val = "";
		}

        //$(element).append(add_options(data)).val( val ).select2("destroy").select2();
        $(element).append(add_options(data)).val( val ).trigger("change").trigger("chosen:updated");

    }

    function add_options(data){
        var html = "";
        if (data.constructor === Array) {
            $.each(data, function(index, keyValue){
                $.each(keyValue, function(key, value){
                    if(value.constructor === Array){
                        html += "<optgroup label=\"" + key + "\">";
                        html += add_options(value);
                        html += "</optgroup>";
                    }
                    else {
                        html += "<option value=\"" + key + "\">" + value + "</option>";
                    }
                });
            });
        }
        else{
            $.each(data, function(id, value){
                if(typeof(value) === "object"){
                    html += "<optgroup label=\"" + id + "\">";
                    html += add_options(value);
                    html += "</optgroup>";
                }
                else{
                    html += "<option value=\"" + id + "\">" + value + "</option>";
                }
            });
        }

        return html;
    }

    function checkType($element, getCurrentValue, setCurrentValue, div, targetDiv) {
        //Do nothing if nothing changed
        var type = $($element).val();
        var currentValue = getCurrentValue();

        if (type == currentValue) {
            return;
        }

        setCurrentValue(type);

        //Check if nothing selected
        if (type === "" || type == 5 || type == null) {
            $(div).slideUp();
        }
        else
        {
            var reportType = $("#select_report_type").val();
            switch (parseInt(type)) {
                case 1:
                    json_targets(
                        targetDiv,
                        null, { "regions" : 1, "type" : reportType == "" ? 0 : reportType},
                        function(data) {
                            return getCurrentValue() == 1;
                        }
                    );
                    break;

                case 2:
                	console.log("test - greater regions");
                    json_targets(
                        targetDiv,
                        null, { "greater_regions" : 1, "type" : reportType == "" ? 0 : reportType},
                        function(data) {
                            return getCurrentValue() == 2;
                        }
                    );
                    break;

                case 3:
                    json_targets(targetDiv, countries, { "countries" : 1 }, function(data){
                        countries = data;
                        return getCurrentValue() == 3;
                    });
                    break;

                case 4:
                    console.log("test - continents");
                	json_targets(targetDiv, continents, { "continents" : 1 }, function(data){
                        continents = data;
                        return getCurrentValue() == 4;
                    });
                    break;

                case 6:
                    console.log("test - macro regions");
                	json_targets(targetDiv, continents, { "macroregions" : 1 }, function(data){
                        continents = data;
                        return getCurrentValue() == 6;
                    });
                    break;
            }

            $(div).slideDown();
        }
    }

    var originType = -1;
    var destinationType = -1;
    var reportType = -1;

    var countries = null;
    var continents = null;


    $(function(){
        if ($("#select_report_type").prop("disabled") == true) {
            return;
        }

        originType = $("#select_origin_type").val();
        if (originType == "") {
            originType = -1;
        }
        destinationType = $("#select_destination_type").val();
        if (destinationType == "") {
            destinationType = -1;
        }

        var initialLoad = true;

        $("#select_report_type").change(function(){
            var type = $(this).val();
            if (type == reportType) {
                return;
            }
            reportType = type;

            var destinationTypes = {"" : ""};
            if (type == "") {
                $("#destination_type_div").slideUp();
                $("#origin_type_div").slideUp();
            } else {
                if (type == 2) {
                    destinationTypes = [
                        {"" : ""},
                        {1 : "Regions"},
                        {3 : "Nationwide"}
                    ];
                } else {
                    destinationTypes = [
                        {"" : ""},
                        {3 : "Countries"},
                        {6 : "Macro-regions"},
                        {4 : "Continents"},
                        {5 : "Worldwide"}
                    ];
                }

                $("#destination_type_div").slideDown();
                $("#origin_type_div").slideDown();
            }

            update_chosen_select("#select_destination_type", destinationTypes, destinationType);

            if (!initialLoad && ((originType == 1 || originType == 2) || type == "")) {
                originType = -1;
                $("#select_origin_type").trigger("change");
            }
            initialLoad = false;
        }).trigger('change');


        $("#select_origin_type").change(function(){
            checkType(
                this,
                function() {
                    return originType;
                },
                function(type) {
                    originType = type;
                },
                "#origin_values_div",
                "#select_origin_values"
            );

            $("#select_origin_values").css("width", "100%");

        }).trigger('change');

        $("#select_destination_type").change(function(){
            checkType(
                this,
                function () {
                    return destinationType;
                },
                function(type) {
                    destinationType = type;
                },
                "#destination_values_div",
                "#select_destination_values"
            );

            $("#select_destination_values").css("width", "100%");

        }).trigger("change");

        $("input.source_1").click(function(){
            if($("input.source_1").is(":checked")) {
                $("#filter_websites_div").slideDown();
                $("#select_all_sirelos").slideDown();
                $("#select_all_non_sirelos").slideDown();
            }
            else {
                $("#filter_websites_div").slideUp();
                $("#select_all_sirelos").slideUp();
                $("#select_all_non_sirelos").slideUp();
            }
        });

        $("#select_all_sirelos_link").click(function(){
            $.ajax({
                type: "GET",
                url: '<?php echo e(url('/ajax/select_all_sirelos')); ?>',
                data: {},
                success: function(data){
                    response = JSON.parse(data);

                    $.each(response, function(i, item) {
                        $("#select_websites option[value='" + item + "']").prop("selected", true);
                    });

                    $('#select_websites').trigger("chosen:updated");

                    $("#select_all_sirelos").hide();
                }
            });
        });

        $("#select_all_non_sirelos_link").click(function(){

            $.ajax({
                type: "GET",
                url: '<?php echo e(url('/ajax/select_all_non_sirelos')); ?>',
                data: {},
                success: function(data){
                    response = JSON.parse(data);

                    $.each(response, function(i, item) {
                        $("#select_websites option[value='" + item + "']").prop("selected", true);
                    });

                    $('#select_websites').trigger("chosen:updated");

                    $("#select_all_non_sirelos").hide();
                }
            });
        });

        $(document).on("click", ".chosen-container-multi .group-result", function(){
            var unselected = $(this).nextUntil(".group-result").not(".result-selected");
            if(unselected.length){
                unselected.trigger("mouseup");
            }
            else {
                $(this).nextUntil(".group-result").each(function(){
                    $("a.search-choice-close[data-option-array-index=" + $(this).data("option-array-index") + "]").trigger("click");
                });
            }
        });

    });

</script>
@endpush
