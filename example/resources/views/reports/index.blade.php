@extends('layouts.backend')

@include( 'scripts.datatables' )
@include( 'scripts.dialogs' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Reports</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Reports</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <!-- Your Block -->
        <div class="block block-rounded block-bordered">
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Reports
                </h3>
            </div>
            <div class="block-content block-content-full">
                <table data-order='[[1, "asc"]]'
                       class="table table-bordered table-striped table-vcenter js-dataTable-full">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Last used</th>
                        <th>View</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($reports as $report)
                        @if (in_array($report->rep_id, $report_rights))
                            <tr>
                                <td>{{$report->rep_id}}</td>
                                <td>{{$report->rep_name}}</td>
                                <td>{{$report->rep_description}}</td>
                                <td>@if($last_used_per_report[$report->rep_id] != "Unknown") {{date("Y-m-d", strtotime($last_used_per_report[$report->rep_id]))}} @else Unknown @endif</td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <a href="{{ url("/reports/" . $report->rep_id . "/view")}}"
                                           class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="left">
                                            <i class="fa fa-eye" style="color:white;"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Your Block -->
    </div>
    <!-- END Page Content -->
@endsection


