@extends('layouts.backend')
@include( 'scripts.datatables' )
@include('scripts.datepicker')
@include('scripts.forms')
@include( 'scripts.dialogs' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Report - {{$report->rep_name}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/reports">Reports</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$report->rep_name}}</li>
                    </ol>
                </nav>
            </div>
            <p>
                @if (!empty($report->rep_description)) <b>{{"Description: "}}</b>{{$report->rep_description}} @endif
                @if (isset($times_used)) <br />({{$times_used}} times used in the last 90 days) @endif
            </p>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="block block-rounded block-bordered">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            {{$error}}<br>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Filters
                </h3>
            </div>
            <div class="block-content block-content-full">

                    <table data-order='[[0, "asc"]]'
                           class="table table-bordered table-striped table-vcenter js-dataTable-full">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Company Name</th>
                            <th>Country</th>
                            <th>Baggage</th>
                            <th>National</th>
                            <th>International</th>
                            <th>Credit hold</th>
                        </tr>
                        </thead>
                        <tbody>

                        @if (isset($filtered_data) && $filtered_data != null)
                            @foreach ($filtered_data as $id => $customer)
                                <tr>
                                    <td>{{$customer['cu_id']}}</td>
                                    <td>{{$customer['name']}}</td>
                                    <td>{{$customer['country']}}</td>
                                    <td>{{$customer['baggage']}}</td>
                                    <td>{{$customer['national']}}</td>
                                    <td>{{$customer['international']}}</td>
                                    <td>{{$customer['credithold']}}</td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection
