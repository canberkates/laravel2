@extends('layouts.backend')
@include( 'scripts.datatables' )
@include('scripts.datepicker')
@include('scripts.forms')
@include( 'scripts.dialogs' )
@include('scripts.select2')


@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Report - {{$report->rep_name}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/reports">Reports</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$report->rep_name}}</li>
                    </ol>
                </nav>
            </div>
            <p>
                @if (!empty($report->rep_description)) <b>{{"Description: "}}</b>{{$report->rep_description}} @endif
                @if (isset($times_used)) <br />({{$times_used}} times used in the last 90 days) @endif
            </p>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="block block-rounded block-bordered">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            {{$error}}<br>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="block-content block-content-full">
                <div class="block-header block-header-default">
                    <h3 class="block-title">
                        Filters
                    </h3>
                </div>
                <div class="block-content block-content-full">

                    <form class="mb-5" method="post" action="{{action('ReportController@viewReviewRatingsCorrection', $report->rep_id)}}">
                        @csrf
                        <input name="_method" type="hidden" value="post">

                        <div class="form-group row">
                            <label class="col-md-2 col-form-label ml-5" for="sirelo">Sirelo:</label>
                            <div class="col-md-4">
                                <select class="js-select2 form-control" multiple data-placeholder="Select some options..."
                                        name="sirelo[]" id="sirelo" style="width:100%;">
                                    <option></option>

                                    @foreach($sirelos as $sirelo)
                                        <option @if (in_array($sirelo->we_id, $sirelo_filter)) selected @endif value="{{$sirelo->we_id}}">{{$sirelo->we_website}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="form-group col-md-8">
                                <button type="submit" class="btn btn-primary">Filter</button>
                            </div>
                        </div>

                    </form>
                    @if (isset($filtered_data) && $filtered_data != null)
                        <table data-order='[[1, "asc"]]'
                               class="table table-bordered table-striped table-vcenter js-dataTable-full">
                            <thead>
                            <tr>
                                <th>Customer ID</th>
                                <th>Customer</th>
                                <th>Total reviews</th>
                                <th>Old rating</th>
                                <th>New rating</th>
                                <th>Difference</th>
                                <th>Country</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($filtered_data as $id => $data)
                                <tr>
                                    <td>{{$id}}</td>
                                    <td>{{$data['customer']}}</td>
                                    <td>{{$data['total_reviews']}}</td>
                                    <td>{{$data['old_rating']}}</td>
                                    <td>{{$data['new_rating']}}</td>
                                    <td>{{$data['difference']}}</td>
                                    <td>{{$data['country']}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endif

            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection


