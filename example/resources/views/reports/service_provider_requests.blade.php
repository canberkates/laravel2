@extends('layouts.backend')
@include( 'scripts.datatables' )
@include('scripts.datepicker')
@include('scripts.forms')
@include( 'scripts.dialogs' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Report - {{$report->rep_name}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/reports">Reports</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$report->rep_name}}</li>
                    </ol>
                </nav>
            </div>
            <p>
                @if (!empty($report->rep_description)) <b>{{"Description: "}}</b>{{$report->rep_description}} @endif
                @if (isset($times_used)) <br />({{$times_used}} times used in the last 90 days) @endif
            </p>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="block block-rounded block-bordered">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            {{$error}}<br>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Filters
                </h3>
            </div>
            <div class="block-content block-content-full">

                <form class="mb-5" method="post" action="{{action('ReportController@viewServiceProviderRequests', $report->rep_id)}}">
                    @csrf
                    <input name="_method" type="hidden" value="post">

                    <div class="form-group row">
                        <label class="col-md-2 col-form-label ml-5" for="for-date">Date range:</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control drp drp-default" name="date" @if(isset($date_filter) && $date_filter != null) value="{{$date_filter}}" @endif autocomplete="off"/>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-2 col-form-label ml-5" for="service_provider">Service Provider:</label>
                        <div class="col-sm-3">
                            <select type="text" class="form-control" name="service_provider">
                                <option value=""></option>
                                @foreach(\App\Models\Customer::where("cu_type", "=", 2)->where("cu_deleted", "=", 0)->orderBy("cu_company_name_business", "asc")->get() as $customer)
                                    <option @if (isset($service_provider_filter) && $customer->cu_id == $service_provider_filter) selected @endif value="{{$customer->cu_id}}">{{$customer->cu_company_name_business}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-2 col-form-label ml-5" for="sent">Sent:</label>
                        <div class="col-sm-3">
                            <select type="text" class="form-control" name="sent">
                                <option @if (!isset($sent_filter) || $sent_filter == "") {{"selected"}} @endif value="">Both</option>
                                <option @if (isset($sent_filter) && $sent_filter == "1") {{"selected"}} @endif value="1">Yes</option>
                                <option @if (isset($sent_filter) && $sent_filter == "0") {{"selected"}} @endif value="0">No</option>

                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="form-group col-md-8">
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div>
                    </div>

                </form>
                @if (isset($filtered_data) && $filtered_data != null)

                    <table data-order='[[1, "asc"]]'
                           class="table table-bordered table-striped table-vcenter js-dataTable-full">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Customer RE ID</th>
                            <th>Submitted</th>
                            <th>Sent</th>
                            <th>Sent</th>
                            <th>Service Provider</th>
                            <th>Question</th>
                            <th>Answer 1</th>
                            <th>Answer 2</th>
                            <th>Status</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Country from</th>
                            <th>Country to</th>
                            <th>Free</th>
                            <th>TO gross FC</th>
                            <th>TO gross €</th>
                            <th>TO netto €</th>
                            <th>Origin</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($filtered_data as $id => $data)
                            <tr>
                                <td>{{$data['id']}}</td>
                                <td>{{$data['cu_re_id']}}</td>
                                <td>{{$data['submitted']}}</td>
                                <td>{{$data['sent']}}</td>
                                <td>{{$data['sent_date']}}</td>
                                <td>{{$data['service_provider']}}</td>
                                <td>{{$data['question']}}</td>
                                <td>{{$data['answer_1']}}</td>
                                <td>{{$data['answer_2']}}</td>
                                <td>{{$data['status']}}</td>
                                <td>{{$data['name']}}</td>
                                <td>{{$data['email']}}</td>
                                <td>{{$data['country_from']}}</td>
                                <td>{{$data['country_to']}}</td>
                                <td>{{$data['free']}}</td>
                                <td>{{$data['to_gross_fc']}}</td>
                                <td>{{$data['to_gross_eu']}}</td>
                                <td>{{$data['to_netto_eu']}}</td>
                                <td>{{$data['origin']}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                        @if (isset($total_gross_eu))
                            <tfoot>
                            <tr>
                                <th colspan="16"></th>
                                <th>{{$total_gross_eu}}</th>
                                <th>{{$total_netto_eu}}</th>
                                <th colspan="1"></th>
                            </tr>
                            </tfoot>
                        @endif
                    </table>
                @endif
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection
