@extends('layouts.backend')
@include( 'scripts.datatables' )
@include('scripts.datepicker')
@include('scripts.forms')
@include( 'scripts.dialogs' )
@include('scripts.select2')


@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Report - {{$report->rep_name}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/reports">Reports</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$report->rep_name}}</li>
                    </ol>
                </nav>
            </div>
            <p>
                @if (!empty($report->rep_description)) <b>{{"Description: "}}</b>{{$report->rep_description}} @endif
                @if (isset($times_used)) <br />({{$times_used}} times used in the last 90 days) @endif
            </p>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="block block-rounded block-bordered">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            {{$error}}<br>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Filters
                </h3>
            </div>
            <div class="block-content block-content-full">

                <form class="mb-5" method="post" action="{{action('ReportController@viewReviewAnalyse', $report->rep_id)}}">
                    @csrf
                    <input name="_method" type="hidden" value="post">

                    <div class="form-group row">
                        <label class="col-md-2 col-form-label ml-5" for="date">Date range:</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control drp drp-default" name="date" @if(isset($date_filter) && $date_filter != null) value="{{$date_filter}}" @endif autocomplete="off"/>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-2 col-form-label ml-5" for="websites">Website:</label>
                        <div class="col-md-3">
                            <select class="js-select2 form-control" multiple data-placeholder="Select some options..."
                                    name="websites[]" id="websites" style="width:100%;">
                                <option></option>

                                @foreach($sirelos as $website)
                                    <option @if (in_array($website->we_id, $website_filter)) selected @endif value="{{$website->we_id}}">{{$website->we_website}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class='col-md-4 mt-2'>
                            Leave this empty to filter on all websites
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-2 col-form-label ml-5" for="source">Source:</label>
                        <div class="col-md-4">
                            <select class="form-control" data-placeholder=""
                                    name="source" id="source" style="width:100%;">
                                <option @if(isset($source_filter) && $source_filter == "both") selected @endif value="both">Both</option>
                                <option @if(isset($source_filter) && $source_filter == "1") selected @endif value="1">Request</option>
                                <option @if(isset($source_filter) && $source_filter == "2") selected @endif value="2">Website review</option>
                            </select>
                        </div>
                    </div>
<!--
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label ml-5" for="show_accepted_rejected">Show accepted/rejected:</label>
                        <div class="col-sm-4">
                            <div class="custom-control custom-switch custom-control custom-control-inline custom-control-primary">
                                <input type="checkbox" class="custom-control-input" id="show_accepted_rejected" name="show_accepted_rejected" @if((isset($accepted_rejected_filter) && $accepted_rejected_filter == "on") && $accepted_rejected_filter !== 0) checked @endif @if(!isset($accepted_rejected_filter)) checked @endif>
                                <label class="custom-control-label" for="show_accepted_rejected"></label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label ml-5" for="show_verified">Show verified:</label>
                        <div class="col-sm-4">
                            <div class="custom-control custom-switch custom-control custom-control-inline custom-control-primary">
                                <input type="checkbox" class="custom-control-input" id="show_verified" name="show_verified" @if((isset($verified_filter) && $verified_filter == "on") && $verified_filter !== 0) checked @endif @if(!isset($verified_filter)) checked @endif>
                                <label class="custom-control-label" for="show_verified"></label>
                            </div>
                        </div>
                    </div>-->

                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="form-group col-md-8">
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div>
                    </div>

                </form>
                @if (isset($filtered_data) && $filtered_data != null)
                    <table data-order='[[0, "asc"]]'
                        class="table table-bordered table-striped table-vcenter js-dataTable-full">
                        <thead>
                        <tr>
                            <th>Date</th>
                            <th>Total submissions</th>
                            <th>To be checked/on hold</th>
                            <th>Published outright</th>
                            <th>Published after email</th>
                            <th>Rejected outright</th>
                            <th>Rejected after email</th>
                            <th>Confirmation mail sent</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($filtered_data as $data)
                            <tr>
                                <td>{{$data['date']}}</td>
                                <td>{{$data['total_submissions']}}</td>
                                <td>{{$data['to_be_checked_on_hold']}}</td>
                                <td>{{$data['published_outright']}}</td>
                                <td>{{$data['published_after_email']}}</td>
                                <td>{{$data['rejected_outright']}}</td>
                                <td>{{$data['rejected_after_email']}}</td>
                                <td>{{$data['confirmation_mail_sent']}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="1"></th>
                                <th>{{$total_total_submissions}}</th>
                                <th>{{$total_to_be_checked_on_hold}}</th>
                                <th>{{$total_published_outright}}</th>
                                <th>{{$total_published_after_email}}</th>
                                <th>{{$total_rejected_outright}}</th>
                                <th>{{$total_rejected_after_email}}</th>
                                <th>{{$total_confirmation_mail_sent}}</th>
                            </tr>
                        </tfoot>
                    </table>
                @endif
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection


