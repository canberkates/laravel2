@extends('layouts.backend')
@include( 'scripts.datatables' )
@include('scripts.datepicker')
@include('scripts.forms')
@include( 'scripts.dialogs' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Report - {{$report->rep_name}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/reports">Reports</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$report->rep_name}}</li>
                    </ol>
                </nav>
            </div>
            <p>
                @if (!empty($report->rep_description)) <b>{{"Description: "}}</b>{{$report->rep_description}} @endif
                @if (isset($times_used)) <br />({{$times_used}} times used in the last 90 days) @endif
            </p>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="block block-rounded block-bordered">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            {{$error}}<br>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Filters
                </h3>
            </div>
            <div class="block-content block-content-full">

                <form class="mb-5" method="post" action="{{action('ReportController@viewMatchingSpeed', $report->rep_id)}}">
                    @csrf
                    <input name="_method" type="hidden" value="post">

                    <div class="form-group row">
                        <label class="col-md-2 col-form-label ml-5" for="for-date">Date range:</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control drp drp-default" name="date" @if(isset($date_filter) && $date_filter != null) value="{{$date_filter}}" @endif autocomplete="off"/>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="form-group col-md-8">
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div>
                    </div>

                </form>
                @if (isset($filtered_data) && $filtered_data != null)

                    <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                        <thead>
                        <tr>
                            <th>Employee</th>
                            <th>Total leads</th>
                            <th>Matched</th>
                            <th>Rejected</th>
                            <th>% matched within 15 min</th>
                            <th>% matched between 15 - 30 min</th>
                            <th>% matched between 30 min - 1 hour</th>
                            <th>% matched between 1 - 2 hours</th>
                            <th>% matched between 2 - 4 hours</th>
                            <th>% matched between 4 - 12 hours</th>
                            <th>% matched over 12 hours</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($filtered_data as $id => $data)
                            <tr>
                                <td>{{$data['employee']}}</td>
                                <td>{{$data['total_leads']}}</td>
                                <td>{{$data['matched']}}</td>
                                <td>{{$data['rejected']}}</td>
                                <td>{{$data['percent_matched_within_15_min']}}</td>
                                <td>{{$data['percent_matched_betweem_15_30_min']}}</td>
                                <td>{{$data['percent_matched_between_30_min_1_hour']}}</td>
                                <td>{{$data['percent_matched_between_1_2_hour']}}</td>
                                <td>{{$data['percent_matched_between_2_4_hour']}}</td>
                                <td>{{$data['percent_matched_between_4_12_hour']}}</td>
                                <td>{{$data['percent_matched_over_12_hours']}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                        @if (isset($total_leads) && $total_leads > 0)
                            <tfoot>
                                <tr>
                                    <th colspan="1"></th>
                                    <th>{{$total_leads}}</th>
                                    <th>{{$total_matched}}</th>
                                    <th>{{$total_rejected}}</th>
                                    <th>{{$total_within_15}}</th>
                                    <th>{{$total_between_15_30}}</th>
                                    <th>{{$total_between_30_min_to_1_hour}}</th>
                                    <th>{{$total_between_1_to_2_hours}}</th>
                                    <th>{{$total_between_2_to_4_hours}}</th>
                                    <th>{{$total_between_4_to_12_hours}}</th>
                                    <th>{{$total_over_12_hours}}</th>
                                </tr>
                            </tfoot>
                        @endif
                    </table>
                @endif
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection


