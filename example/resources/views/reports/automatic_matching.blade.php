@extends('layouts.backend')
@include( 'scripts.datatables' )
@include('scripts.datepicker')
@include('scripts.forms')
@include( 'scripts.dialogs' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Report - {{$report->rep_name}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/reports">Reports</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$report->rep_name}}</li>
                    </ol>
                </nav>
            </div>
            <p>
                @if (!empty($report->rep_description)) <b>{{"Description: "}}</b>{{$report->rep_description}} @endif
                @if (isset($times_used)) <br />({{$times_used}} times used in the last 90 days) @endif
            </p>        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="block block-rounded block-bordered">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            {{$error}}<br>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Filters
                </h3>
            </div>
            <div class="block-content block-content-full">

                <form class="mb-5" method="post" action="{{action('ReportController@viewAutomaticMatching', $report->rep_id)}}">
                    @csrf
                    <input name="_method" type="hidden" value="post">

                    <div class="form-group row">
                        <label class="col-md-2 col-form-label ml-5" for="for-date">Date range:</label>
                        <div class="col-md-3">
                            <input type="text" class="form-control drp drp-default" name="date" @if(isset($date_filter) && $date_filter != null) value="{{$date_filter}}" @endif autocomplete="off"/>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label ml-5"
                               for="moda_activate_premium_leads">Type</label>
                        <div class="col-sm-6">
                            <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                <select class="chosen-select form-control" name="type">
                                    <option value="all">All</option>
                                    <option @if (isset($type_filter) && $type_filter == 'matched') selected @endif value="matched">Matched</option>
                                    <option @if (isset($type_filter) && $type_filter == 'rejected') selected @endif value="rejected">Rejected</option>
                                    <option @if (isset($type_filter) && $type_filter == 'sent_back') selected @endif value="sent_back">Sent back</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="form-group col-md-8">
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div>
                    </div>

                </form>
                @if (isset($filtered_data) && $filtered_data != null)

                    <p>For the lead to be matched, all match requirements must be set to 1. <br> For the lead to be rejected, all entries in any group must be set to 1</p>
                    <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                        <thead>
                        <tr>
                            <th>Request ID</th>
                            <th>Timestamp</th>
                            <th>Automatic Decision</th>
                            <th>Manual Decision</th>
                            <th>Match requirements</th>
                            <th>Rejection requirements</th>
                            <th>Request changes</th>
                            <th>Request</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($filtered_data as $data)
                            <?php
                                $matching_requirements = json_decode(base64_decode($data->aure_details), true)[0];
                                $rejection_requirements = json_decode(base64_decode($data->aure_details), true)[1];
                            ?>
                            <tr>
                                <td>{{$data->re_id}}</td>
                                <td>{{$data->aure_timestamp}}</td>
                                <td>{{$decisions[$data->aure_decision]}}</td>
                                <td>{{$requeststatuses[$data->re_status]}} @if($data->re_status == 2) ({{$rejectionreasons[$data->re_rejection_reason][0]}}) @endif</td>
                                <td><span class="show_requirements" href="#">Toggle matching requirements</span><div class="print_requirements" style="display: none;"><?php echo "<pre>"; print_r($matching_requirements); echo "</pre>"; ?></div></td>
                                <td><span class="show_requirements">Toggle rejection requirements</span><div class="print_requirements" style="display: none;"><?php echo "<pre>"; print_r($rejection_requirements); echo "</pre>"; ?></div></td>
                                @if($data->revisions)
                                <td><span class="show_requirements">Toggle request changes</span><div class="print_requirements" style="display: none;"><?php echo "<pre>"; print_r($data->revisions); echo "</pre>"; ?></div></td>
                                @else
                                    <td></td>
                                @endif
                                <td><div class="btn-group">
                                    <a class="btn btn-sm btn-primary"
                                       data-toggle="tooltip"
                                       data-placement="left"
                                       title="show"
                                       href="<?= url('/requests/' . $data->re_id. '/'); ?>">
                                        <i class="fa fa-pencil-alt"></i>
                                    </a>
                                </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection

@push('scripts')
    <style>
        .show_requirements{
            color: blue;
            cursor: pointer;
        }
        .show_requirements:hover{
            text-decoration: underline;
        }
    </style>
    <script>
        jQuery(document).ready(function () {
            jQuery(document).on( 'click', ".show_requirements",  function(e) {
                console.log($(this).parent().children().next());
                $(this).parent().children().next().toggle();
            });
        });
    </script>
@endpush

