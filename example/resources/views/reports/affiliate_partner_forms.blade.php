@extends('layouts.backend')
@include( 'scripts.datatables' )
@include('scripts.datepicker')
@include('scripts.forms')
@include( 'scripts.dialogs' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Report - {{$report->rep_name}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/reports">Reports</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$report->rep_name}}</li>
                    </ol>
                </nav>
            </div>
            <p>
                @if (!empty($report->rep_description)) <b>{{"Description: "}}</b>{{$report->rep_description}} @endif
                @if (isset($times_used)) <br />({{$times_used}} times used in the last 90 days) @endif
            </p>        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="block block-rounded block-bordered">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            {{$error}}<br>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Filters
                </h3>
            </div>
            <div class="block-content block-content-full">

                <form class="mb-5" method="post" action="{{action('ReportController@viewAffiliatePartnerForms', $report->rep_id)}}">
                    @csrf
                    <input name="_method" type="hidden" value="post">

                    <div class="form-group row">
                        <label class="col-md-2 col-form-label ml-5" for="for-date">Date range:</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control drp drp-default" name="date" @if(isset($date_filter) && $date_filter != null) value="{{$date_filter}}" @endif autocomplete="off"/>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-2 col-form-label ml-5" for="affiliate_partner">Affiliate partner:</label>
                        <div class="col-sm-3">
                            <select type="text" class="form-control" name="affiliate_partner">
                                <option value=""></option>
                                @foreach(\App\Models\Customer::where("cu_type", "=", 3)->where("cu_deleted", "=", 0)->orderBy("cu_company_name_business", "asc")->get() as $customer)
                                    <option @if (isset($affiliate_partner_filter) && $customer->cu_id == $affiliate_partner_filter) selected @endif value="{{$customer->cu_id}}">{{$customer->cu_company_name_business}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label ml-5"
                            for="merge_iframe_data_per_partner">Merge iFrame data per Partner</label>
                        <div class="col-sm-7">
                            <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                <input type="checkbox" class="custom-control-input"
                                    id="merge_iframe_data_per_partner"
                                    name="merge_iframe_data_per_partner"{{(isset($merge_iframes_filter) ? "checked=''" : "")}}>
                                <label class="custom-control-label"
                                    for="merge_iframe_data_per_partner"></label>
                                <div class="col-sm-2"></div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="form-group col-md-8">
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div>
                    </div>

                </form>
                @if (isset($filtered_data) && $filtered_data != null)

                    @if($merge_iframes_filter)
                        <table data-order='[[1, "asc"]]'
                            class="table table-bordered table-striped table-vcenter js-dataTable-full">
                            <thead>
                            <tr>
                                <th>Affiliate Partner</th>
                                <th>Status</th>
                                <th>Visits</th>
                                <th>Requests</th>
                                <th>Revenues</th>
                                <th>Conversion</th>
                                <th>Revenue per visit</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($filtered_data as $id => $data)
                                <tr>
                                    <td>{{$data['cu_company_name_business']}}</td>
                                    <td>{{$affiliatepartnerstatuses[$data['afpada_status']]}}</td>
                                    <td>{{$data['visits']}}</td>
                                    <td>{{$data['requests']}}</td>
                                    <td>{{$data['revenues']}}</td>
                                    <td>{{$data['conversion']}}</td>
                                    <td>{{$data['revenue_per_visit']}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                            @if (isset($total_visits) && isset($total_requests))
                                <tfoot>
                                <tr>
                                    <th colspan="2"></th>
                                    <th>{{$total_visits}}</th>
                                    <th>{{$total_requests}}</th>
                                    <th>{{$total_revenues}}</th>
                                    <th>{{$total_conversion}}</th>
                                    <th>{{$total_revenue_per_visit}}</th>
                                </tr>
                                </tfoot>
                            @endif
                        </table>
                    @else
                        <table data-order='[[1, "asc"]]'
                            class="table table-bordered table-striped table-vcenter js-dataTable-full">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Affiliate Partner</th>
                                <th>Status</th>
                                <th>Form</th>
                                <th>Portal</th>
                                <th>Language</th>
                                <th>Visits</th>
                                <th>Requests</th>
                                <th>Revenues</th>
                                <th>Conversion</th>
                                <th>Revenue per visit</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($filtered_data as $id => $data)
                                <tr>
                                    <td>{{$data['afpafo_id']}}</td>
                                    <td>{{$data['cu_company_name_business']}}</td>
                                    <td>{{$affiliatepartnerstatuses[$data['afpada_status']]}}</td>
                                    <td>{{$data['afpafo_name']}}</td>
                                    <td>{{$data['po_portal']}}</td>
                                    <td>{{$data['afpafo_la_code']}}</td>
                                    <td>{{$data['visits']}}</td>
                                    <td>{{$data['requests']}}</td>
                                    <td>{{$data['revenues']}}</td>
                                    <td>{{$data['conversion']}}</td>
                                    <td>{{$data['revenue_per_visit']}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                            @if (isset($total_visits) && isset($total_requests))
                                <tfoot>
                                <tr>
                                    <th colspan="6"></th>
                                    <th>{{$total_visits}}</th>
                                    <th>{{$total_requests}}</th>
                                    <th>{{$total_revenues}}</th>
                                    <th>{{$total_conversion}}</th>
                                    <th>{{$total_revenue_per_visit}}</th>
                                </tr>
                                </tfoot>
                            @endif
                        </table>
                    @endif


                @endif
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection


