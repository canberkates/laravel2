@extends('layouts.backend')
@include( 'scripts.datatables' )
@include('scripts.datepicker')
@include('scripts.forms')
@include( 'scripts.dialogs' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Report - {{$report->rep_name}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/reports">Reports</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$report->rep_name}}</li>
                    </ol>
                </nav>
            </div>
            <p>
                @if (!empty($report->rep_description)) <b>{{"Description: "}}</b>{{$report->rep_description}} @endif
                @if (isset($times_used)) <br />({{$times_used}} times used in the last 90 days) @endif
            </p>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="block block-rounded block-bordered">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            {{$error}}<br>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Filters
                </h3>
            </div>
            <div class="block-content block-content-full">

                <form class="mb-5" method="post" action="{{action('ReportController@viewLeadMonitoringPerSite', $report->rep_id)}}">
                    @csrf
                    <input name="_method" type="hidden" value="post">

                    <div class="form-group row">
                        <label class="col-md-2 col-form-label ml-5" for="for-date">Date range:</label>
                        <div class="col-md-3">
                            <input type="text" class="form-control drp drp-default" name="date" @if(isset($date_filter) && $date_filter != null) value="{{$date_filter}}" @endif autocomplete="off"/>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label ml-5"
                               for="moda_activate_premium_leads">Destination type</label>
                        <div class="col-sm-6">
                            <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                <select class="chosen-select form-control" name="destination_type">
                                    <option value="both">Both</option>
                                    <option @if (isset($destination_type_filter) && $destination_type_filter == 'int') selected @endif  value="int">International</option>
                                    <option @if (isset($destination_type_filter) && $destination_type_filter == 'nat') selected @endif value="nat">National</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="form-group col-md-8">
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div>
                    </div>

                </form>
                @if (isset($filtered_data) && $filtered_data != null)

                    <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                        <thead>
                        <tr>
                            <th>Origin</th>
                            <th>0-30 days</th>
                            <th>30-60 days</th>
                            <th>0-60 days</th>
                            <th>0-90 days</th>
                            <th>0-120 days</th>
                            <th>120 days+</th>
                            <th>Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($filtered_data as $origin => $data)
                            <tr>
                                <td>{{$origin}}</td>
                                <td>{{$data['0_30_days']}}</td>
                                <td>{{$data['30_60_days']}}</td>
                                <td>{{$data['0_60_days']}}</td>
                                <td>{{$data['0_90_days']}}</td>
                                <td>{{$data['0_120_days']}}</td>
                                <td>{{$data['120_plus_days']}}</td>
                                <td>{{$data['total']}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                        @if (isset($total))
                            <tfoot>
                            <tr>
                                <th colspan="1"></th>
                                <th>{{$total['0_30_days']}}</th>
                                <th>{{$total['30_60_days']}}</th>
                                <th>{{$total['0_60_days']}}</th>
                                <th>{{$total['0_90_days']}}</th>
                                <th>{{$total['0_120_days']}}</th>
                                <th>{{$total['120_plus_days']}}</th>
                                <th>{{$total['total']}}</th>
                            </tr>
                            </tfoot>
                        @endif
                    </table>
                @endif
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection


