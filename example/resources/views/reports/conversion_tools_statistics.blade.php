@extends('layouts.backend')
@include( 'scripts.datatables' )
@include('scripts.datepicker')
@include('scripts.forms')
@include( 'scripts.dialogs' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Report - {{$report->rep_name}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/reports">Reports</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$report->rep_name}}</li>
                    </ol>
                </nav>
            </div>
            <p>
                @if (!empty($report->rep_description)) <b>{{"Description: "}}</b>{{$report->rep_description}} @endif
                @if (isset($times_used)) <br />({{$times_used}} times used in the last 90 days) @endif
            </p>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="block block-rounded block-bordered">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            {{$error}}<br>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Filters
                </h3>
            </div>
            <div class="block-content block-content-full">

                <form class="mb-5" method="post" action="{{action('ReportController@viewConversionTools', $report->rep_id)}}">
                    @csrf
                    <input name="_method" type="hidden" value="post">

                    <div class="form-group row">
                        <label class="col-md-2 col-form-label ml-5" for="for-date">Date range:</label>
                        <div class="col-md-3">
                            <input type="text" class="form-control drp drp-default" name="date" @if(isset($date_filter) && $date_filter != null) value="{{$date_filter}}" @endif autocomplete="off"/>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="form-group col-md-8">
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div>
                    </div>

                </form>
                @if (isset($filtered_data) && $filtered_data != null)

                    <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Customer</th>
                            <th>Volume Calc Submissions</th>
                            <th>Volume Calc to Lead Form</th>
                            <th>Form Views</th>
                            <th>Step 1</th>
                            <th>Step 2</th>
                            <th>Step 3</th>
                            <th>Step 4</th>
                            <th>Conversions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($filtered_data as $key => $data)
                            <tr>
                                <td>{{$key}}</td>
                                <td>{{$data['name']}}</td>
                                <td>{{$data['vc'][2] ?? 0}}</td>
                                <td>{{$data['vc'][3] ?? 0}}</td>
                                <td>{{$data['visits'] ?? 0}}</td>
                                <td>{{$data['step'][1] ?? 0}}</td>
                                <td>{{$data['step'][2] ?? 0}}</td>
                                <td>{{$data['step'][3] ?? 0}}</td>
                                <td>{{$data['step'][4] ?? "-"}}</td>
                                <td>{{$data['requests'] ?? 0}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <th colspan="2"></th>
                        <th>{{$total_vcs[2] ?? 0}}</th>
                        <th>{{$total_vcs[3] ?? 0}}</th>
                        <th>{{$total_visits ?? 0}}</th>
                        <th>{{$total_steps['step'][1] ?? 0}}</th>
                        <th>{{$total_steps['step'][2] ?? 0}}</th>
                        <th>{{$total_steps['step'][3] ?? 0}}</th>
                        <th>{{$total_steps['step'][4] ?? 0}}</th>
                        <th>{{$total_requests ?? 0}}</th>
                        </tfoot>
                    </table>
                @endif
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection

@push('scripts')
    <style>
        .show_requirements{
            color: blue;
            cursor: pointer;
        }
        .show_requirements:hover{
            text-decoration: underline;
        }
    </style>
    <script>
        jQuery(document).ready(function () {
            jQuery(document).on( 'click', ".show_requirements",  function(e) {
                console.log($(this).parent().children().next());
                $(this).parent().children().next().toggle();
            });
        });
    </script>
@endpush

