@extends('layouts.backend')
@include( 'scripts.datatables' )
@include('scripts.datepicker')
@include('scripts.forms')
@include( 'scripts.dialogs' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Report - {{$report->rep_name}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/reports">Reports</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$report->rep_name}}</li>
                    </ol>
                </nav>
            </div>
            <p>
                @if (!empty($report->rep_description)) <b>{{"Description: "}}</b>{{$report->rep_description}} @endif
                @if (isset($times_used)) <br />({{$times_used}} times used in the last 90 days) @endif
            </p>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="block block-rounded block-bordered">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            {{$error}}<br>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Filters
                </h3>
            </div>
            <div class="block-content block-content-full">

                <form class="mb-5" method="post" action="{{action('ReportController@viewTurnoverPerSalesManager', $report->rep_id)}}">
                    @csrf
                    <input name="_method" type="hidden" value="post">

                    <div class="form-group row">
                        <label class="col-md-2 col-form-label ml-5" for="sales_manager">Sales manager:</label>
                        <div class="col-sm-3">
                            <select type="text" class="form-control" name="sales_manager">
                                <option value=""></option>
                                @foreach(\App\Models\User::where('us_sales', 1)->orderBy('us_name', 'ASC')->get() as $id => $user)
                                    <option @if (isset($sales_manager_filter) && $user->us_id == $sales_manager_filter) selected @endif value="{{$user->us_id}}">{{$user->us_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="form-group col-md-8">
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div>
                    </div>

                </form>
                @if (isset($filtered_data) && $filtered_data != null)

                    <table data-order='[[0, "asc"]]'
                           class="table table-bordered table-striped table-vcenter js-dataTable-full">
                        <thead>
                        <tr>
                            <th>Customer</th>
                            <th>Method</th>
                            <th>Invoices</th>
                            <th>Status</th>
                            <th>Credit hold</th>
                            @foreach ($months as $month_number)
                                <th width="80px;">{{$month_number}}</th>
                            @endforeach

                            <th width="100px;">Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($filtered_data as $id => $data)
                            <tr>
                                <td>{{$data['customer']}}</td>
                                <td>{{$data['method']}}</td>
                                <td>{{$data['invoices']}}</td>
                                <td>{{$data['status']}}</td>
                                <td>{{$data['credit_hold']}}</td>
                                <td style="color:{{$data['month_0_pay_status_color']}};">{{$data['month_0']}}</td>
                                <td style="color:{{$data['month_1_pay_status_color']}};">{{$data['month_1']}}</td>
                                <td style="color:{{$data['month_2_pay_status_color']}};">{{$data['month_2']}}</td>
                                <td style="color:{{$data['month_3_pay_status_color']}};">{{$data['month_3']}}</td>
                                <td style="color:{{$data['month_4_pay_status_color']}};">{{$data['month_4']}}</td>
                                <td style="color:{{$data['month_5_pay_status_color']}};">{{$data['month_5']}}</td>
                                <td style="color:{{$data['month_6_pay_status_color']}};">{{$data['month_6']}}</td>
                                <td style="color:{{$data['month_7_pay_status_color']}};">{{$data['month_7']}}</td>
                                <td style="color:{{$data['month_8_pay_status_color']}};">{{$data['month_8']}}</td>
                                <td style="color:{{$data['month_9_pay_status_color']}};">{{$data['month_9']}}</td>
                                <td style="color:{{$data['month_10_pay_status_color']}};">{{$data['month_10']}}</td>
                                <td style="color:{{$data['month_11_pay_status_color']}};">{{$data['month_11']}}</td>
                                <td style="color:{{$data['month_12_pay_status_color']}};">{{$data['month_12']}}</td>
                                <td>{{$data['total']}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                        @if (isset($total_total))
                            <tfoot>
                            <tr>
                                <th colspan="2"></th>
                                <th>{{$total_invoices}}</th>
                                <th colspan="2"></th>

                                <th>{{$total_month_0}}</th>
                                <th>{{$total_month_1}}</th>
                                <th>{{$total_month_2}}</th>
                                <th>{{$total_month_3}}</th>
                                <th>{{$total_month_4}}</th>
                                <th>{{$total_month_5}}</th>
                                <th>{{$total_month_6}}</th>
                                <th>{{$total_month_7}}</th>
                                <th>{{$total_month_8}}</th>
                                <th>{{$total_month_9}}</th>
                                <th>{{$total_month_10}}</th>
                                <th>{{$total_month_11}}</th>
                                <th>{{$total_month_12}}</th>
                                <th>{{$total_total}}</th>
                            </tr>
                            </tfoot>
                        @endif
                    </table>
                @endif
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection
