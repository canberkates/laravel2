@extends('layouts.backend')
@include( 'scripts.datatables' )
@include('scripts.datepicker')
@include('scripts.forms')
@include( 'scripts.dialogs' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Report - {{$report->rep_name}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/reports">Reports</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$report->rep_name}}</li>
                    </ol>
                </nav>
            </div>
            <p>
                @if (!empty($report->rep_description)) <b>{{"Description: "}}</b>{{$report->rep_description}} @endif
                @if (isset($times_used)) <br />({{$times_used}} times used in the last 90 days) @endif
            </p>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="block block-rounded block-bordered">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            {{$error}}<br>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Filters
                </h3>
            </div>
            <div class="block-content block-content-full">

                <form class="mb-5" method="post" action="{{action('ReportController@viewRejectedRequests', $report->rep_id)}}">
                    @csrf
                    <input name="_method" type="hidden" value="post">

                    <div class="form-group row">
                        <label class="col-md-2 col-form-label ml-5" for="portal">Portal:</label>
                        <div class="col-sm-3">
                            <select type="text" class="form-control" name="portal">
                                <option value=""></option>
                                @foreach(\App\Functions\Data::portals() as $id => $portal)
                                    <option @if (isset($portal_filter) && $id == $portal_filter) selected @endif value="{{$id}}">{{$portal}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-2 col-form-label ml-5" for="for-date">Date range:</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control drp drp-default" name="date" @if(isset($date_filter) && $date_filter != null) value="{{$date_filter}}" @endif autocomplete="off"/>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-2 col-form-label ml-5" for="rejection_reason">Rejection reason:</label>
                        <div class="col-sm-3">
                            <select type="text" class="form-control" name="rejection_reason">
                                <option value=""></option>
                                @foreach(\App\Data\RejectionReason::all() as $id => $reason)
                                    @if($reason[1] == 0)
                                        @continue;
                                    @endif

                                    <option @if (isset($rejection_reason_filter) && $id == $rejection_reason_filter) selected @endif value="{{$id}}">{{$reason[0]}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label ml-5"
                               for="moda_activate_premium_leads">Destination type</label>
                        <div class="col-sm-7">
                            <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                <input type="checkbox" class="custom-control-input"
                                       id="destination_type[1]"
                                       name="destination_type[1]"{{(isset($destination_checked) && in_array(1, $destination_checked) ? "checked=''" : (!isset($destination_checked) ? "checked=''" : ""))}}>
                                <label class="custom-control-label"
                                       for="destination_type[1]">INTMOVING</label>
                                <div class="col-sm-2"></div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label ml-5"
                               for="moda_activate_premium_leads"></label>
                        <div class="col-sm-7">
                            <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                <input type="checkbox" class="custom-control-input"
                                       id="destination_type[2]"
                                       name="destination_type[2]"{{(isset($destination_checked) && in_array(2, $destination_checked) ? "checked=''" : (!isset($destination_checked) ? "checked=''" : ""))}}>
                                <label class="custom-control-label"
                                       for="destination_type[2]">NATMOVING</label>
                            </div>
                        </div>
                    </div>

                    @foreach(App\Functions\System::movingSizes(null, true) as $id => $moving_size)
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label ml-5"
                                   for="moda_activate_premium_leads">@if($loop->first) {{"Moving size"}} @endif</label>
                            <div class="col-sm-7">
                                <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                    <input type="checkbox" class="custom-control-input"
                                           id="moving_size[{{$id}}]"
                                           name="moving_size[{{$id}}]"{{(isset($moving_size_checked) && in_array($id, $moving_size_checked) ? "checked=''" : (!isset($moving_size_checked) ? "checked=''" : ""))}}>
                                    <label class="custom-control-label"
                                           for="moving_size[{{$id}}]">{{$moving_size}}</label>
                                </div>
                            </div>
                        </div>
                    @endforeach

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label ml-5"
                               for="moda_activate_premium_leads">Recover button</label>
                        <div class="col-sm-7">
                            <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                <input type="checkbox" class="custom-control-input"
                                       id="recover_button"
                                       name="recover_button"{{(isset($recover_button_filter) && $recover_button_filter) ? "checked=''" : ""}}>
                                <label class="custom-control-label"
                                       for="recover_button"></label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="form-group col-md-8">
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div>
                    </div>

                </form>
                @if (isset($filtered_data) && $filtered_data != null)

                    <table data-order='[[2, "asc"]]'
                           class="table table-bordered table-striped table-vcenter js-dataTable-full">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Received</th>
                            <th>Rejected</th>
                            <th>Rejected by</th>
                            <th>Rejection reason</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Country from</th>
                            <th>Country to</th>
                            <th>Request type</th>
                            <th>Moving size</th>
                            <th>Moving date</th>
                            <th>Volume(m<sup>3</sup>)</th>
                            <th>Origin</th>

                            @if(isset($recover_button_filter) && $recover_button_filter == 1)
                                <th>Recover</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($filtered_data as $id => $data)
                            <tr>
                                <td>{{$data['re_id']}}</td>
                                <td>{{$data['timestamp']}}</td>
                                <td>{{$data['rejected']}}</td>
                                <td>{{$data['rejected_by']}}</td>
                                <td>{{$data['rejection_reason']}}</td>
                                <td>{{$data['name']}}</td>
                                <td>{{$data['email']}}</td>
                                <td>{{$data['country_from']}}</td>
                                <td>{{$data['country_to']}}</td>
                                <td>{{$data['request_type']}}</td>
                                <td>{{$data['moving_size']}}</td>
                                <td>{{$data['moving_date']}}</td>
                                <td>{{$data['volume']}}</td>
                                <td>{{$data['origin']}}</td>
                                @if(isset($recover_button_filter) && $recover_button_filter == 1)
                                    <td><a href="/requests/{{$data['re_id']}}/recover"><i class="fa fa-redo"/></a></td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection
