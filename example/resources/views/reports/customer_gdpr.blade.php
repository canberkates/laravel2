@extends('layouts.backend')
@include( 'scripts.datatables' )
@include('scripts.datepicker')
@include('scripts.forms')
@include( 'scripts.dialogs' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Report - {{$report->rep_name}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/reports">Reports</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$report->rep_name}}</li>
                    </ol>
                </nav>
            </div>
            <p>
                @if (!empty($report->rep_description)) <b>{{"Description: "}}</b>{{$report->rep_description}} @endif
                @if (isset($times_used)) <br />({{$times_used}} times used in the last 90 days) @endif
            </p>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="block block-rounded block-bordered">
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Filters
                </h3>
            </div>
            <div class="block-content block-content-full">

                <form class="mb-5" method="post" action="{{action('ReportController@viewCustomerGDPR', $report->rep_id)}}">
                    @csrf
                    <input name="_method" type="hidden" value="post">

                    <div class="form-group row">
                        <label class="col-md-2 col-form-label ml-5" for="status">Status:</label>
                        <div class="col-md-2">
                            <select type="text" class="form-control" name="status">
                                <option @if(isset($status_filter) && $status_filter != null && $status_filter == "1") selected @endif value="1" @if (isset($status_filter) && $status_filter == null) selected @endif>All</option>
                                <option @if(isset($status_filter) && $status_filter != null && $status_filter == "2") selected @endif  value="2">Filled In</option>
                                <option @if(isset($status_filter) && $status_filter != null && $status_filter == "3") selected @endif  value="3">Not Filled In</option>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="form-group col-md-8">
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div>
                    </div>

                </form>
                @if (isset($filtered_data) && $filtered_data != null)

                    <table data-order='[[1, "asc"]]'
                           class="table table-bordered table-striped table-vcenter js-dataTable-full">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Customer</th>
                            <th>Email</th>
                            <th>Country code</th>
                            <th>Region</th>
                            <th>Status</th>
                            <th>Credit hold</th>
                            <th>Agreement 1</th>
                            <th>Agreement 2</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($filtered_data as $id => $data)
                            <tr>
                                <td>{{$data['id']}}</td>
                                <td>{{$data['customer']}}</td>
                                <td>{{$data['email']}}</td>
                                <td>{{$data['country_code']}}</td>
                                <td>{{$data['region']}}</td>
                                <td>{{$data['status']}}</td>
                                <td>{{$data['credit_hold']}}</td>
                                <td style="text-align: center;">@if($data['agreement_1'] == "confirmed") <img src="/media/icons/icon_confirm.png"/> @elseif ($data['agreement_1'] == "not_confirmed") <img src="/media/icons/icon_cancel.png"/> @else {{""}}@endif</td>
                                <td style="text-align: center;">@if($data['agreement_2'] == "confirmed") <img src="/media/icons/icon_confirm.png"/> @elseif ($data['agreement_2'] == "not_confirmed") <img src="/media/icons/icon_cancel.png"/> @else {{""}}@endif</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection
