@extends('layouts.backend')
@include( 'scripts.datatables' )
@include('scripts.datepicker')
@include('scripts.forms')
@include( 'scripts.dialogs' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Report - {{$report->rep_name}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/reports">Reports</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$report->rep_name}}</li>
                    </ol>
                </nav>
            </div>
            <p>
                @if (!empty($report->rep_description)) <b>{{"Description: "}}</b>{{$report->rep_description}} @endif
                @if (isset($times_used)) <br />({{$times_used}} times used in the last 90 days) @endif
            </p>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="block block-rounded block-bordered">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            {{$error}}<br>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Filters
                </h3>
            </div>
            <div class="block-content block-content-full">

                <form class="mb-5" method="post" action="{{action('ReportController@viewInvoicedAffiliatePartners', $report->rep_id)}}">
                    @csrf
                    <input name="_method" type="hidden" value="post">

                    <div class="form-group row">
                        <label class="col-md-2 col-form-label ml-5" for="for-date">Date range:</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control drp drp-default" name="date" @if(isset($date_filter) && $date_filter != null) value="{{$date_filter}}" @endif autocomplete="off"/>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label ml-5"
                               for="moda_activate_premium_leads">Status</label>
                        <div class="col-sm-7">
                            <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                <input type="checkbox" class="custom-control-input"
                                       id="status_active"
                                       name="status_active"{{(isset($status_active_filter) && $status_active_filter ? "checked=''" : (!isset($status_active_filter) ? "checked=''" : ""))}}>
                                <label class="custom-control-label"
                                       for="status_active">Active</label>
                                <div class="col-sm-1"></div>
                            </div>
                            <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                <input type="checkbox" class="custom-control-input"
                                       id="status_inactive"
                                       name="status_inactive"{{(isset($status_inactive_filter) && $status_inactive_filter ? "checked=''" : (!isset($status_inactive_filter) ? "checked=''" : ""))}}>
                                <label class="custom-control-label"
                                       for="status_inactive">Inactive</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="form-group col-md-8">
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div>
                    </div>

                </form>
                @if (isset($filtered_data) && $filtered_data != null)

                    <table data-order='[[1, "asc"]]'
                           class="table table-bordered table-striped table-vcenter js-dataTable-full">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Affiliate Partner</th>
                            <th>Status</th>
                            <th>Requests</th>
                            <th>Matched</th>
                            <th>% Matched</th>
                            <th>% Rejected</th>
                            <th>% Moves</th>
                            <th>% Baggage</th>
                            <th>Gross revenues €</th>
                            <th>Free €</th>
                            <th>% Free</th>
                            <th>Revenues €</th>
                            <th>Costs €</th>
                            <th>Margin €</th>
                            <th>% Margin</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($filtered_data as $id => $data)
                            @continue(($data['requests'] <= 0) && ($data['gross_revenues'] <= 0)  && ($data['costs'] <= 0))
                            <tr>
                                <td>{{$data['id']}}</td>
                                <td>{{$data['affiliate_partner']}}</td>
                                <td>{{$data['status']}}</td>
                                <td>{{$data['requests']}}</td>
                                <td>{{$data['matched']}}</td>
                                <td>{{$data['percentage_matched']}}</td>
                                <td>{{$data['percentage_rejected']}}</td>
                                <td>{{$data['percentage_moves']}}</td>
                                <td>{{$data['percentage_baggage']}}</td>
                                <td>{{$data['gross_revenues']}}</td>
                                <td>{{$data['free']}}</td>
                                <td>{{$data['percentage_free']}}</td>
                                <td>{{$data['revenues']}}</td>
                                <td>{{$data['costs']}}</td>
                                <td>{{$data['margin']}}</td>
                                <td>{{$data['percentage_margin']}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                        @if (isset($total_requests) && $total_requests != 0)
                            <tfoot>
                            <tr>
                                <th colspan="3"></th>
                                <th>{{$total_requests}}</th>
                                <th>{{$total_requests_matched}}</th>
                                <th>{{$total_requests_matched_percentage}}</th>
                                <th>{{$total_requests_rejected_percentage}}</th>
                                <th>{{$total_requests_move_percentage}}</th>
                                <th>{{$total_requests_baggage_service_percentage}}</th>
                                <th>{{$total_gross_revenue}}</th>
                                <th>{{$total_free}}</th>
                                <th>{{$total_free_percentage}}</th>
                                <th>{{$total_turnover}}</th>
                                <th>{{$total_costs}}</th>
                                <th>{{$total_margin}}</th>
                                <th>{{$total_margin_percentage}}</th>
                            </tr>
                            </tfoot>
                        @endif
                    </table>
                @endif
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection
