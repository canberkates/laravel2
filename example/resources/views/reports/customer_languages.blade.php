@extends('layouts.backend')
@include( 'scripts.datatables' )
@include('scripts.datepicker')
@include('scripts.forms')
@include( 'scripts.dialogs' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Report - {{$report->rep_name}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/reports">Reports</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$report->rep_name}}</li>
                    </ol>
                </nav>
            </div>
            <p>
                @if (!empty($report->rep_description)) <b>{{"Description: "}}</b>{{$report->rep_description}} @endif
                @if (isset($times_used)) <br />({{$times_used}} times used in the last 90 days) @endif
            </p>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="block block-rounded block-bordered">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            {{$error}}<br>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Filters
                </h3>
            </div>
            <div class="block-content block-content-full">

                    <table data-order='[[0, "asc"]]'
                           class="table table-bordered table-striped table-vcenter js-dataTable-full">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Customer</th>
                            <th>Country</th>
                            <th>Language</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>

                        @if (isset($filtered_data) && $filtered_data != null)
                            @foreach ($filtered_data as $id => $customer)
                                @if (($customer['cu_co_code'] == "SG" && $customer['cu_co_code'] == "GR" && $customer['cu_co_code'] == "AE" &&
                                    $customer['cu_co_code'] == "IE" && $customer['cu_co_code'] == "BR" &&
                                    $customer['cu_co_code'] == "SE" && $customer['cu_co_code'] == "NZ" &&
                                    $customer['cu_co_code'] == "NO" && $customer['cu_co_code'] == "PT" &&
                                    $customer['cu_co_code'] == "HK" && $customer['cu_co_code'] == "IN" &&
                                    $customer['cu_co_code'] == "CN" && $customer['cu_co_code'] == "TR" &&
                                    $customer['cu_co_code'] == "QA" && $customer['cu_co_code'] == "RU" &&
                                    $customer['cu_co_code'] == "ID" && $customer['cu_co_code'] == "JP" &&
                                    $customer['cu_co_code'] == "RO" && $customer['cu_co_code'] == "MY" &&
                                    $customer['cu_co_code'] == "KR" && $customer['cu_co_code'] == "TH" &&
                                    $customer['cu_co_code'] == "BH" && $customer['cu_co_code'] == "KW" &&
                                    $customer['cu_co_code'] == "VN" && $customer['cu_co_code'] == "PL" &&
                                    $customer['cu_co_code'] == "PH" && $customer['cu_co_code'] == "FI" &&
                                    $customer['cu_co_code'] == "HU" && $customer['cu_co_code'] == "IL" &&
                                    $customer['cu_co_code'] == "CZ" && $customer['cu_co_code'] == "SA" &&
                                    $customer['cu_co_code'] == "BG" && $customer['cu_co_code'] == "LU" &&
                                    $customer['cu_co_code'] == "OM" && $customer['cu_co_code'] == "HR" &&
                                    $customer['cu_co_code'] == "MT" && $customer['cu_co_code'] == "EE" &&
                                    $customer['cu_co_code'] == "SK" && $customer['cu_co_code'] == "SI" &&
                                    $customer['cu_co_code'] == "LT" && $customer['cu_co_code'] == "LV" &&
                                    $customer['cu_co_code'] == "RS" && $customer['cu_co_code'] == "UA" &&
                                    $customer['cu_co_code'] == "BY" && $customer['cu_co_code'] == "EG" &&
                                    $customer['cu_co_code'] == "KE") && $customer['language'] != "EN")
                                    <tr>
                                        <td>{{$customer['cu_id']}}</td>
                                        <td>{{$customer['cu_company_name_business']}}</td>
                                        <td>{{$customer['country']}}</td>
                                        <td>{{$customer['language']}}</td>
                                        <td>{{$customer['status']}}</td>
                                    </tr>
                                @elseif($customer['cu_co_code'] == "DK" && $customer['language'] != "DK")
                                    <tr>
                                        <td>{{$customer['cu_id']}}</td>
                                        <td>{{$customer['cu_company_name_business']}}</td>
                                        <td>{{$customer['country']}}</td>
                                        <td>{{$customer['language']}}</td>
                                        <td>{{$customer['status']}}</td>
                                    </tr>
                                @elseif($customer['cu_co_code'] == "CA" && ($customer['language'] != "EN" && $customer['language'] != "FR"))
                                    <tr>
                                        <td>{{$customer['cu_id']}}</td>
                                        <td>{{$customer['cu_company_name_business']}}</td>
                                        <td>{{$customer['country']}}</td>
                                        <td>{{$customer['language']}}</td>
                                        <td>{{$customer['status']}}</td>
                                    </tr>
                                @elseif($customer['cu_co_code'] == "CH" && ($customer['language'] != "DE" && $customer['language'] != "FR" && $customer['language'] != "IT"))
                                    <tr>
                                        <td>{{$customer['cu_id']}}</td>
                                        <td>{{$customer['cu_company_name_business']}}</td>
                                        <td>{{$customer['country']}}</td>
                                        <td>{{$customer['language']}}</td>
                                        <td>{{$customer['status']}}</td>
                                    </tr>
                                @elseif(($customer['cu_co_code'] == "MX" && $customer['cu_co_code'] == "AR" &&
                                        $customer['cu_co_code'] == "CO" && $customer['cu_co_code'] == "CR" &&
                                        $customer['cu_co_code'] == "CL" && $customer['cu_co_code'] == "PE" &&
                                        $customer['cu_co_code'] == "EC" && $customer['cu_co_code'] == "PY" &&
                                        $customer['cu_co_code'] == "UY" && $customer['cu_co_code'] == "BO" &&
                                        $customer['cu_co_code'] == "PA") && $customer['language'] != "ES")
                                    <tr>
                                        <td>{{$customer['cu_id']}}</td>
                                        <td>{{$customer['cu_company_name_business']}}</td>
                                        <td>{{$customer['country']}}</td>
                                        <td>{{$customer['language']}}</td>
                                        <td>{{$customer['status']}}</td>
                                    </tr>
                                @elseif(($customer['cu_co_code'] == "MA" && $customer['cu_co_code'] == "DZ" &&
                                        $customer['cu_co_code'] == "TN" && $customer['cu_co_code'] == "NG")
                                        && $customer['language'] != "FR")
                                    <tr>
                                        <td>{{$customer['cu_id']}}</td>
                                        <td>{{$customer['cu_company_name_business']}}</td>
                                        <td>{{$customer['country']}}</td>
                                        <td>{{$customer['language']}}</td>
                                        <td>{{$customer['status']}}</td>
                                    </tr>
                                @elseif($customer['cu_co_code'] == "US" && ($customer['language'] != "EN" && $customer['language'] != "ES"))
                                    <tr>
                                        <td>{{$customer['cu_id']}}</td>
                                        <td>{{$customer['cu_company_name_business']}}</td>
                                        <td>{{$customer['country']}}</td>
                                        <td>{{$customer['language']}}</td>
                                        <td>{{$customer['status']}}</td>
                                    </tr>
                                @elseif($customer['cu_co_code'] == "BE" && ($customer['language'] != "NL" && $customer['language'] != "FR"))
                                    <tr>
                                        <td>{{$customer['cu_id']}}</td>
                                        <td>{{$customer['cu_company_name_business']}}</td>
                                        <td>{{$customer['country']}}</td>
                                        <td>{{$customer['language']}}</td>
                                        <td>{{$customer['status']}}</td>
                                    </tr>
                                @endif
                            @endforeach
                        @endif
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection
