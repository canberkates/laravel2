@extends('layouts.backend')
@include( 'scripts.datatables' )
@include('scripts.datepicker')
@include('scripts.forms')
@include( 'scripts.dialogs' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Report - {{$report->rep_name}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/reports">Reports</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$report->rep_name}}</li>
                    </ol>
                </nav>
            </div>
            <p>
                @if (!empty($report->rep_description)) <b>{{"Description: "}}</b>{{$report->rep_description}} @endif
                @if (isset($times_used)) <br />({{$times_used}} times used in the last 90 days) @endif
            </p>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="block block-rounded block-bordered">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            {{$error}}<br>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Filters
                </h3>
            </div>
            <div class="block-content block-content-full">

                <form class="mb-5" method="post" action="{{action('ReportController@viewRequestQuality', $report->rep_id)}}">
                    @csrf
                    <input name="_method" type="hidden" value="post">

                    <div class="form-group row">
                        <label class="col-md-2 col-form-label ml-5" for="for-date">Date range:</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control drp drp-default" name="date" @if(isset($date_filter) && $date_filter != null) value="{{$date_filter}}" @endif autocomplete="off"/>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-2 col-form-label ml-5" for="groupby">Group By:</label>
                        <div class="col-sm-3">
                            <select type="text" class="form-control" name="groupby">
                                <option @if(isset($groupby_filter) && $groupby_filter == 'country') {{"selected"}} @endif value="country">Country</option>
                                <option @if(isset($groupby_filter) && $groupby_filter == 'website') {{"selected"}} @endif value="website">Website</option>
                                <option @if(isset($groupby_filter) && $groupby_filter == 'form') {{"selected"}} @endif value="form">Form</option>
                                <option @if(isset($groupby_filter) && $groupby_filter == 'portal') {{"selected"}} @endif value="portal">Portal</option>
                                <option @if(isset($groupby_filter) && $groupby_filter == 'user') {{"selected"}} @endif value="user">User</option>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="form-group col-md-8">
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div>
                    </div>

                </form>
                @if (isset($filtered_data) && $filtered_data != null)

                    <table data-order='[[2, "desc"]]'
                           class="table table-bordered table-striped table-vcenter js-dataTable-full">
                        <thead>
                        <tr>
                            <th>
                                @if(isset($groupby_filter) && $groupby_filter == 'country') {{"Country"}} @endif
                                @if(isset($groupby_filter) && $groupby_filter == 'website') {{"Website"}} @endif
                                @if(isset($groupby_filter) && $groupby_filter == 'form') {{"Form"}} @endif
                                @if(isset($groupby_filter) && $groupby_filter == 'portal') {{"Portal"}} @endif
                                @if(isset($groupby_filter) && $groupby_filter == 'user') {{"User"}} @endif
                            </th>

                            <th>Average quality</th>
                            <th>Requests</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($filtered_data as $id => $data)
                            @if ($data['groupby'] == "-")
                                @continue;
                            @endif
                            <tr>
                                <td>{{$data['groupby']}}</td>
                                <td>{{$data['average']}}</td>
                                <td>{{$data['requests']}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection


