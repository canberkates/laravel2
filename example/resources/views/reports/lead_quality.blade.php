@extends('layouts.backend')
@include( 'scripts.datatables' )
@include('scripts.datepicker')
@include('scripts.forms')
@include( 'scripts.dialogs' )
@include('scripts.chosen')

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Report - {{$report->rep_name}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/reports">Reports</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$report->rep_name}}</li>
                    </ol>
                </nav>
            </div>
            <p>
                @if (!empty($report->rep_description)) <b>{{"Description: "}}</b>{{$report->rep_description}} @endif
                @if (isset($times_used)) <br />({{$times_used}} times used in the last 90 days) @endif
            </p>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="block block-rounded block-bordered">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            {{$error}}<br>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Filters
                </h3>
            </div>
            <div class="block-content block-content-full">

                <form class="mb-5" method="post" action="{{action('ReportController@viewLeadQuality', $report->rep_id)}}">
                    @csrf
                    <input name="_method" type="hidden" value="post">

                    <div class="form-group row">
                        <label class="col-md-2 col-form-label ml-5" for="for-date">Date range:</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control drp drp-default" name="date" @if(isset($date_filter) && $date_filter != null) value="{{$date_filter}}" @endif autocomplete="off"/>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-2 col-form-label ml-5" for="request_types">Request Type:</label>
                        <div class="col-md-4">
                            <select class="chosen-select form-control" multiple data-placeholder="Select some options..."
                                    name="request_types[]" id="request_types" style="width:100%;">
                                <option></option>

                                @foreach(\App\Data\RequestType::all() as $id => $type)
                                    @if($id == 5) @continue @endif
                                    <option @if (in_array($id, $request_types_filter)) selected @endif value="{{$id}}">{{$type}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div id="select_all_request_types" class="row" style="margin-top:-10px;">
                        <label class="col-md-2 col-form-label ml-5"></label>
                        <div class="col-sm-4">
                            <a id="select_all_request_types_link" style="color:#0665d0; transition: color 0.12s ease-out; cursor: pointer;">Select all request types</a>
                        </div>
                    </div>
                    <br/>

                    <div class="form-group row">
                        <label class="col-md-2 col-form-label ml-5" for="affiliates">Affiliates:</label>
                        <div class="col-md-4">
                            <select class="chosen-select form-control" multiple data-placeholder="Select some options..."
                                    name="affiliates[]" id="affiliates" style="width:100%;">
                                <option></option>

                                @foreach(\App\Models\Customer::where("cu_type", 3)->where("cu_deleted", 0)->orderBy("cu_company_name_business", "ASC")->get() as $customer)
                                    <option @if (in_array($customer->cu_id, $affiliates_filter)) selected @endif value="{{$customer->cu_id}}">{{$customer->cu_company_name_business}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div id="select_all_affiliates" class="row" style="margin-top:-10px;">
                        <label class="col-md-2 col-form-label ml-5"></label>
                        <div class="col-sm-4">
                            <a id="select_all_affiliates_link" style="color:#0665d0; transition: color 0.12s ease-out; cursor: pointer;">Select all affiliates</a>
                        </div>
                    </div>
                    <br/>

                    <div id="filter_websites_div" class="form-group row mb-1">
                        <label class="col-md-2 col-form-label ml-5" for="origin_values">Websites</label>
                        <div class="col-md-4">
                            <select type="text" multiple class="chosen-select form-control" id="select_websites" name="website[]" data-placeholder="Select some options...">
                                @foreach($websites as $website)
                                    <option value="{{$website->we_id}}" @if (in_array($website->we_id, $websites_filter)) selected @endif >{{$website->we_website}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div id="select_all_websites" class="row">
                        <label class="col-md-2 col-form-label ml-5"></label>
                        <div class="col-sm-4">
                            <a id="select_all_websites_link" style="color:#0665d0; transition: color 0.12s ease-out; cursor: pointer;">Select all websites</a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="form-group col-md-8">
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div>
                    </div>

                </form>
                @if (isset($filtered_data) && $filtered_data != null)
                    <table data-order='[[0, "asc"]]'
                           class="table table-bordered table-striped table-vcenter js-dataTable-full">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Leads</th>
                            {{--<th>Matched</th>
                            <th>Rejected</th>
                            <th>Matches</th>--}}
                            <th>% Matched</th>
                            <th>% Rejected</th>
                            <th>% Volume</th>
                            <th>% Volume calc</th>
                            <th>% Remarks</th>
                            <th>% Volume & Remarks</th>
                            <th>% Complete origin addr.</th>
                            <th>% Complete destination addr.</th>
                            <th>% Complete addresses</th>
                            <th>% Claims</th>
                            <th>Avg. Matches</th>
                            <th>Avg. Turnover (netto)</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($filtered_data as $id => $data)
                            <tr>
                                <td>{{$data['name']}}</td>
                                <td>{{$data['leads']}}</td>
                                {{--<td>{{$data['matched']}}</td>
                                <td>{{$data['rejected']}}</td>
                                <td>{{$data['matches']}}</td>--}}
                                <td>{{$data['matched_percentage']}}%</td>
                                <td>{{$data['rejected_percentage']}}%</td>
                                <td>{{$data['volume_percentage']}}%</td>
                                <td>{{$data['volume_calc_percentage']}}%</td>
                                <td>{{$data['remarks_percentage']}}%</td>
                                <td>{{$data['volume_and_remarks_percentage']}}%</td>
                                <td>{{$data['complete_origin_address_percentage']}}%</td>
                                <td>{{$data['complete_destination_address_percentage']}}%</td>
                                <td>{{$data['complete_addresses_percentage']}}%</td>
                                <td>{{$data['claims_percentage']}}%</td>
                                <td>{{$data['avg_matched']}}</td>
                                <td>€ {{$data['avg_turnover']}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="1"></th>
                            <th>{{$total_leads}}</th>
                            <th>{{$total_matched}}%</th>
                            <th>{{$total_rejected}}%</th>
                            <th>{{$total_with_volume}}%</th>
                            <th>{{$total_with_volume_calc}}%</th>
                            <th>{{$total_with_remarks}}%</th>
                            <th>{{$total_with_volume_and_remarks}}%</th>
                            <th>{{$total_complete_origin_address_details}}%</th>
                            <th>{{$total_complete_destination_address_details}}%</th>
                            <th>{{$total_complete_address_details}}%</th>
                            <th>{{$total_claims}}%</th>
                            <th>{{$total_matches}}</th>
                            <th>€ {{$total_turnover}}</th>
                        </tr>
                        </tfoot>
                    </table>
                @endif
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection

@push('scripts')
    <script type="text/javascript">
        $(function(){

            $("#select_all_affiliates_link").click(function(){

                $("#affiliates option").prop("selected", true);

                $('#affiliates').trigger("chosen:updated");;
            });

            $("#select_all_websites_link").click(function(){

                $("#select_websites option").prop("selected", true);

                $('#select_websites').trigger("chosen:updated");;

                $("#select_all_websites").hide();
                $("#select_all_sirelos").hide();
                $("#select_all_non_sirelos").hide();
            });

            $("#select_all_request_types_link").click(function(){

                $("#request_types option").prop("selected", true);

                $('#request_types').trigger("chosen:updated");;

                $("#select_all_request_types").hide();
            });
        });

    </script>
@endpush
