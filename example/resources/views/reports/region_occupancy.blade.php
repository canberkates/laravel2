@extends('layouts.backend')
@include( 'scripts.datatables' )
@include('scripts.datepicker')
@include('scripts.forms')
@include( 'scripts.dialogs' )
{{--@include('scripts.select2')--}}
@include('scripts.chosen')
@pushonce( 'scripts:reports' )
    <script src="{{ asset('/js/custom/reports/reports.js') }}"></script>
@endpushonce

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Report - {{$report->rep_name}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/reports">Reports</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$report->rep_name}}</li>
                    </ol>
                </nav>
            </div>
            <p>
                @if (!empty($report->rep_description)) <b>{{"Description: "}}</b>{{$report->rep_description}} @endif
                @if (isset($times_used)) <br />({{$times_used}} times used in the last 90 days) @endif
            </p>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="row">
        <div class="block block-rounded block-bordered col-md-4">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            {{$error}}<br>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Filters
                </h3>
            </div>
            <div class="block-content block-content-full">

                <form class="mb-5" method="post" action="{{action('ReportController@queueRegionOccupancy', $report->rep_id)}}">
                    @csrf
                    <input name="_method" type="hidden" value="post">

                    <div class="form-group row">
                        <label class="col-md-4 col-form-label ml-5" for="portal">Portal/Origin</label>
                        <div class="col-md-6">
                            <select @if(isset($repr_id) && $clone === false) disabled @endif type="text" class="chosen-select form-control" id="portal" name="portal">
                                <option value=""></option>
                                @foreach(App\Models\Portal::all() as $portal)
                                    <option @if(isset($repr_id) && $portal->po_id == App\Functions\System::unserialize($report_progress->repr_posted_values)['portal']) {{"selected"}} @endif
                                        value="{{$portal->po_id}}">{{$portal->po_portal}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>


                    <div id="div_origin_type" class="form-group row">
                        <label class="col-md-4 col-form-label ml-5" for="origin_type">Origin type</label>
                        <div class="col-md-6">
                            <select @if(isset($repr_id) && $clone === false) disabled @endif type="text" class="chosen-select form-control" id="select_origin_type" name="origin_type">
                                <option value=""></option>
                                <option @if(App\Functions\System::unserialize($report_progress->repr_posted_values)['origin_type'][1]) selected @endif value="1">Regions</option>
                                <option @if(App\Functions\System::unserialize($report_progress->repr_posted_values)['origin_type'][2]) selected @endif value="2">Greater Regions</option>
                            </select>
                        </div>
                    </div>

                    <div id="div_origins" class="form-group row">
                        <label class="col-md-4 col-form-label ml-5" for="region_from">From regions</label>
                        <div class="col-md-6">
                            <select @if(isset($repr_id) && $clone === false) disabled @endif multiple type="text" class="chosen-select form-control" id="region" name="region[]">

                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4 col-form-label ml-5" for="destination_type">Destination type</label>
                        <div class="col-md-6">
                            <select @if(isset($repr_id) && $clone === false) disabled @endif type="text" class="chosen-select form-control" id="select_destination_type" name="destination_type">

                            </select>
                        </div>
                    </div>

                    <div id="div_targets" class="form-group row">
                        <label class="col-md-4 col-form-label ml-5" for="targets">To targets</label>
                        <div class="col-md-6">
                            <select @if(isset($repr_id) && $clone === false) disabled @endif multiple type="text" class="chosen-select form-control" id="targets" name="targets[]">

                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4 col-form-label ml-5" for="for-date">Date range:</label>
                        <div class="col-md-6">
                            <input  @if(isset($repr_id) && $clone === false) disabled @endif type="text" class="form-control drp drp-default" name="date" @if(isset($repr_id)) value="{{App\Functions\System::unserialize($report_progress->repr_posted_values)['date_from']." - ".App\Functions\System::unserialize($report_progress->repr_posted_values)['date_to']}}" @endif autocomplete="off"/>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label ml-5"
                               for="moda_activate_premium_leads">Status</label>
                        <div class="col-sm-6">
                            <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                <input @if(isset($repr_id) && $clone === false) disabled @endif type="checkbox" class="custom-control-input"
                                       id="status[1]"
                                       @if(App\Functions\System::unserialize($report_progress->repr_posted_values)['status'][1] == "on")
                                           checked=""
                                       @elseif(!isset($repr_id))
                                           checked=""
                                       @endif
                                       name="status[1]">
                                <label class="custom-control-label"
                                       for="status[1]">Active</label>
                                <div class="col-sm-1"></div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label ml-5"
                               for="moda_activate_premium_leads"></label>
                        <div class="col-sm-6">
                            <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                <input @if(isset($repr_id) && $clone === false) disabled @endif type="checkbox" class="custom-control-input"
                                       id="status[2]"
                                       @if(App\Functions\System::unserialize($report_progress->repr_posted_values)['status'][2] == "on")
                                           checked=""
                                       @elseif(!isset($repr_id))
                                           checked=""
                                       @endif
                                       name="status[2]">
                                <label class="custom-control-label"
                                       for="status[2]">Pause</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4 col-form-label ml-5" for="request_type">Request type:</label>
                        <div class="col-md-6">
                            <select @if(isset($repr_id) && $clone === false) disabled @endif type="text" class="chosen-select form-control" id="select_request_type" name="request_type">
                                <option value=""></option>
                                @foreach(App\Data\RequestType::all() as $id => $type)
                                    <option @if(isset($repr_id) && $id == App\Functions\System::unserialize($report_progress->repr_posted_values)['request_type']) {{"selected"}} @endif
                                        value="{{$id}}">{{$type}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div id="div_targets" class="form-group row">
                        <label class="col-md-4 col-form-label ml-5" for="moving_size">Moving size</label>
                        <div class="col-md-6">
                            <select @if(isset($repr_id) && $clone === false) disabled @endif multiple type="text" class="chosen-select form-control" id="moving_size" name="moving_size[]">

                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4 col-form-label ml-5" for="portal">Volume restriction</label>
                        <div class="col-md-6">
                            <select @if(isset($repr_id) && $clone === false) disabled @endif type="text" class="form-control" name="volume_restriction">
                                <option @if(App\Functions\System::unserialize($report_progress->repr_posted_values)['volume_restriction'] === "") {{"selected"}}  @endif value="">Both</option>
                                <option @if(App\Functions\System::unserialize($report_progress->repr_posted_values)['volume_restriction'] === "1") {{"selected"}}  @endif value="1">Yes</option>
                                <option @if(App\Functions\System::unserialize($report_progress->repr_posted_values)['volume_restriction'] === "0") {{"selected"}}  @endif value="0">No</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label ml-5"
                               for="business_only">Business only</label>
                        <div class="col-sm-6">
                            <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                <input @if(isset($repr_id) && $clone === false) disabled @endif type="checkbox" class="custom-control-input"
                                       id="business_only"
                                       @if(App\Functions\System::unserialize($report_progress->repr_posted_values)['business_only'] == "on")
                                           checked=""
                                       @endif
                                       name="business_only">
                                <label class="custom-control-label"
                                       for="business_only"></label>
                                <div class="col-sm-1"></div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label ml-5"
                               for="hide_import_data">Hide import data</label>
                        <div class="col-sm-6">
                            <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                <input @if(isset($repr_id) && $clone === false) disabled @endif type="checkbox" class="custom-control-input"
                                       id="hide_import_data"
                                       @if(App\Functions\System::unserialize($report_progress->repr_posted_values)['hide_import_data'] == "on")
                                           checked=""
                                       @endif
                                       name="hide_import_data">
                                <label class="custom-control-label"
                                       for="hide_import_data"></label>
                                <div class="col-sm-1"></div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label ml-5"
                               for="hide_cappings">Do not show capping info</label>
                        <div class="col-sm-6">
                            <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                <input @if(isset($repr_id) && $clone === false) disabled @endif type="checkbox" class="custom-control-input"
                                       id="hide_cappings"
                                       @if(App\Functions\System::unserialize($report_progress->repr_posted_values)['hide_cappings'] == "on")
                                           checked=""
                                       @endif
                                       name="hide_cappings">
                                <label class="custom-control-label"
                                       for="hide_cappings"></label>
                                <div class="col-sm-1"></div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4 col-form-label ml-5" for="min_requests">Min. requests</label>
                        <div class="col-md-6">
                            <input @if(isset($repr_id) && $clone === false) disabled @endif type="text" class="form-control" id="min_requests" name="min_requests" value="{{App\Functions\System::unserialize($report_progress->repr_posted_values)['minimum_requests']}}"/>
                        </div>
                    </div>

                    <hr>

                    <div class="form-group row">
                        <label class="col-md-4 col-form-label ml-5" for="portal">Comment:</label>
                        <div class="col-md-6">
                            <input @if(isset($repr_id) && $clone === false) disabled @endif class="form-control" name="report_comment" type="text" value="{{App\Functions\System::unserialize($report_progress->repr_posted_values)['report_comment']}}">
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="form-group col-md-8">
                            <button type="submit" class="btn btn-primary">Queue report</button>
                        </div>
                    </div>
                    @if(isset($repr_id))
                        <hr>
                        <div class="row">
                            <div class="col-md-1"></div>
                            <p>
                            <a href="/reports/{{$report->rep_id}}/view/" @if(isset($repr_id) && $clone === false) class="btn btn-primary" @else class="btn btn-danger" @endif>New report</a>
                            @if($clone === false)
                                <a href="/reports/{{$report->rep_id}}/view/{{$report->rep_report}}/{{$repr_id}}/clone/" class="btn btn-primary">Clone report</a>
                                <a href="/reports/report_progress/{{$repr_id}}/delete" class="btn btn-primary">Delete report</a>
                            @endif
                            <a href="/reports" class="btn btn-primary">Back</a>
                            </p>
                        </div>
                    @endif
                </form>
            </div>
        </div>
        <div class="block block-rounded block-bordered col-md-6">
            <form method="post" id="single-report-progress" name="single-report-progress">
                <input type="hidden" id="report_id" name="report_id" value="{{$report->rep_id}}"/>
                <input type="hidden" id="report_progress_id" name="report_progress_id" value="{{$repr_id}}"/>
                <input type="hidden" id="report_status" name="report_status" value="@if (isset($report_progress)) {{$report_progress->repr_status}} @endif"/>
                <div id="single-report-html"></div>
            </form>

            <div class="progress push" style="height: 10px; margin-top: 10px">
                <div id="progressbar" class="progress-bar progress-bar-striped bg-success" role="progressbar" style="" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <hr>
        @if (isset($repr_id))
                {!! App\Functions\Reporting::getAllGeneratedReportsByReportId($report->rep_id, $repr_id) !!}
            @else
                {!! App\Functions\Reporting::getAllGeneratedReportsByReportId($report->rep_id) !!}
            @endif

        </div>
        <div class="block block-rounded block-bordered col-md-2">
            <div class="block block-rounded block-bordered col-md-12">

                <div class="block-header block-header-default">
                    <h3 class="block-title">
                        Legend
                    </h3>
                </div>
                <div class="block-content block-content-full">
                    <div class="customer export" style="">
                        Expert Move
                    </div>
                    <div class="customer capped-e" style="">
                        Capped Export
                    </div>
                    <div class="customer import" style="">
                        Import Move
                    </div>
                    <div class="customer capped-i" style="">
                        Capped Import
                    </div>
                    <div class="customer missed" style="">
                        No company
                    </div>
                    <div style="padding: 5px; margin:5px; border: 1px solid black;">
                        #R = Requests
                    </div>
                    <div style="padding: 5px; margin:5px; border: 1px solid black;">
                        #E = Export Reqs
                    </div>
                    <div style="padding: 5px; margin:5px; border: 1px solid black;">
                        #I = Import Reqs
                    </div>
                    <div style="padding: 5px; margin:5px; border: 1px solid black;">
                        #CU = Customers (excludes #FT)
                    </div>
                    <div style="padding: 5px; margin:5px; border: 1px solid black;">
                        #UCU = Uncapped Customers
                    </div>
                    <div style="padding: 5px; margin:5px; border: 1px solid black;">
                        #CCU = Capped Customers
                    </div>
                </div>
        </div>
        </div>
        </div>

    </div>
    @isset($file)
        <div class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="block-content tab-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="block block-rounded block-bordered">
                                    <div class="block-content">
                                        <h2 class="content-heading pt-0">{{$report->rep_name}} - Report</h2>

                                        <div class="row">
                                            <div class="col-md-12">
                                                {!! $file !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endisset

    <!-- END Page Content -->
@endsection

@push('scripts')
<script type="text/javascript">
    var portal_val = null;

    var regions = null;
    var greater_regions = null;
    var countries = null;
    var continents = null;
    var from_regions = null;

    var national = false;

    var current_destination_type = 0;
    var current_destination_type_options = 0;


    function json_targets(current_data, options, data_function) {
        if (current_data === null) {
            update_chosen_select("#targets", {"" : "Loading..."});
            $.getJSON(
                "{{ url('/json/portal_options') }}",
                options,
                function(data){
                    if (data_function(data)) {
                        update_chosen_select("#targets", data);
                    }
                }
            );
        } else {
            update_chosen_select("#targets", current_data);
        }
    }

    function update_chosen_select(element, data, val){
        $(element).empty();

        if(typeof(val) == "undefined"){
            val = "";
        }

        //$(element).append(add_options(data)).val( val ).select2("destroy").select2();
        $(element).append(add_options(data)).val( val ).trigger("change").trigger("chosen:updated");
    }

    function add_options(data){
	var html = "";
	if (data.constructor === Array) {
		$.each(data, function(index, keyValue){
			$.each(keyValue, function(key, value){
				if(value.constructor === Array){
					html += "<optgroup label=\"" + key + "\">";
					html += add_options(value);
					html += "</optgroup>";
				}
				else {
					html += "<option value=\"" + key + "\">" + value + "</option>";
				}
			});
		});
	}
	else{
		$.each(data, function(id, value){
			if(typeof(value) === "object"){
				html += "<optgroup label=\"" + id + "\">";
				html += add_options(value);
				html += "</optgroup>";
			}
			else{
				html += "<option value=\"" + id + "\">" + value + "</option>";
			}
		});
	}

	return html;
}


    function get_greater_regions() {
        greater_regions = {};
        var nrOfGreaterRegions = 0;
        $.each(from_regions, function(key, data) {
            greater_regions[key] = key;
            nrOfGreaterRegions++;
        });
        if (nrOfGreaterRegions > 1) {
            greater_regions = {"All" : greater_regions};
        }
    }

    $(function(){
        $("#select_request_type").change(function(){
            var request_type = $(this).val();
            update_chosen_select("#moving_size", {});
            if (request_type != "") {
                $.getJSON(
                    "{{ url('/json/moving_sizes') }}",
                    { "request_type": request_type },
                    function(data){
                        var all_data = {"All" : data};
                        update_chosen_select("#moving_size", all_data, Object.keys(data));
                    }
                );
            }
        });

        $("#portal").change(function() {
            portal_val = $(this).val();
            from_regions = null;
            update_chosen_select("#region", {}, "");
            $("#select_origin_type").val("").trigger("chosen:updated");
            if (portal_val == "") {
                portal_val = null;
                return;
            }
            portal_val = parseInt(portal_val);

            //Check for specials
            var special_portal = (portal_val >= -4 && portal_val <= -1);

            //Update the targets
            var new_region_content = null;
            var selected = "";
            if ($("#portal option:selected").text().indexOf("INTMOVING") >= 0) {
                national = false;
                if (current_destination_type_options !== 1) {
                    new_region_content = {1: "Regions", 2: "Countries", 3: "Continents", 4: "Worldwide"};
                    current_destination_type_options = 1;
                }
            } else if (special_portal) {
                if (current_destination_type_options !== portal_val) {
                    new_region_content = {};
                    switch (portal_val) {
                        case -1:
                            new_region_content[2] = "Countries";
                        case -2:
                            new_region_content[3] = "Continents";
                        case -3:
                            new_region_content[4] = "Worldwide";
                    }
                    current_destination_type_options = special_portal;
                }

                var min_requests = parseInt($("#min_requests").val());
                if (isNaN(min_requests) || min_requests === 0) {
                    $("#min_requests").val("1");
                }
            } else {
                national = true;
                if (current_destination_type_options !== 2) {
                    new_region_content = {1: "Regions", 5: "Greater Regions", 6: "Nationwide", 7 : "National (Local)", 8 : "National (Long Distance)"};
                    selected = 1;
                    current_destination_type_options = 2;
                }
            }
            if (new_region_content !== null) {
                update_chosen_select("#select_destination_type", new_region_content, selected);
            }

            //Special cases
            if (special_portal) {
                $("#div_origin_type").slideUp();
                $("#div_origins").slideUp();
            } else {
                $("#div_origin_type").slideDown();
                $("#div_origins").slideDown();
            }
        });

        $("#select_origin_type").change(function(){
            var origin_type = $(this).val();
            if (origin_type == "") {
                update_chosen_select("#region", {}, "");
                return;
            }
            var portal_options = {
                "portal": portal_val,
                "type": origin_type
            };
            if (national) {
                portal_options["national_portal"] = true;
            }

            $.getJSON(
                "{{ url('/json/portal_options') }}",
                portal_options,
                function(data){
                	console.log( data );
                    from_regions = data;

                    var select_values = data;
                    if (origin_type == 2) {
                    	get_greater_regions();
                        select_values = greater_regions;
                    }
                    update_chosen_select("#region", select_values);
                    if (national === true) {
                        update_chosen_select("#targets", data);
                        $("#select_destination_type").trigger('change');
                    }
                }
            );
        });

        $("#select_destination_type").change(function(){
            current_destination_type = $(this).val();
            if (current_destination_type == "" || current_destination_type == null) {
                current_destination_type = 0;
                update_chosen_select("#targets", {});
                $("#div_destination").slideDown();
                return;
            }
            current_destination_type = parseInt(current_destination_type);
            switch(current_destination_type) {
                case 1:
                    if (national === true) {
                        if (from_regions !== null) {
                            update_chosen_select("#targets", from_regions);
                        } else {
                            update_chosen_select("#targets", {"": "Loading..."});
                        }
                    } else {
                        json_targets(regions, { "regions" : 1 }, function(data){
                            regions = data;
                            return current_destination_type == 1;
                        });
                    }
                    break;

                case 2:
                    json_targets(countries, { "countries" : 1 }, function(data){
                        countries = data;
                        return current_destination_type == 2;
                    });
                    break;

                case 3:
                    json_targets(continents, { "continents" : 1 }, function(data){
                        continents = data;
                        return current_destination_type == 3;
                    });
                    break;

                case 5:
                    if (!from_regions) {
                        //Ajax request still going
                        break;
                    }
                    get_greater_regions();
                    update_chosen_select("#targets", greater_regions);
                    break;

                case 4:
                case 6:
                    update_chosen_select("#targets", {});
                    break;
            }

            //Show the correct fields
            if (current_destination_type === 4 || current_destination_type === 6 || current_destination_type === 7 || current_destination_type === 8) {
                $("#div_targets").slideUp();
            } else {
                $("#div_targets").slideDown();
            }
        });

        $(document).on("click", ".chosen-container-multi .group-result", function(){
            var unselected = $(this).nextUntil(".group-result").not(".result-selected");
            if(unselected.length){
                unselected.trigger("mouseup");
            }
            else {
                $(this).nextUntil(".group-result").each(function(){
                    $("a.search-choice-close[data-option-array-index=" + $(this).data("option-array-index") + "]").trigger("click");
                });
            }
        });

        //Check if correct fields are shown
        var destination_var = $("#select_destination_type").val();
        if (destination_var != "") {
            current_destination_type = parseInt(destination_var);
            if (current_destination_type === 4 || current_destination_type === 6) {
                $("#div_targets").slideUp();
            }
        }
        var portal_val = parseInt($("#portal").val());
        if (portal_val <= -1 && portal_val >= -4) {
            $("#div_origin_type").slideUp();
            $("#div_origins").slideUp();
        }
    });
</script>
@endpush
                                
