@extends('layouts.backend')

@include( 'scripts.datatables' )
@include( 'scripts.dialogs' )

@section('content')
    <!-- Page Content -->
    <div class="content">
        <!-- Your Block -->
        <div class="block block-rounded block-bordered">
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    Sending Invoice to {{$invoice->customer->cu_company_name_business}}
                </h3>
            </div>
            <div class="block-content block-content-full">
                <form method="post"
                      action="{{action('InvoiceController@sendInvoice', ['customer_id' => $invoice->in_cu_id, 'invoice_id' => $invoice->in_id])}}">
                    @csrf
                    <input name="_method" type="hidden" value="post">
                    <input name="redirect_url" type="hidden" value="{{$redirect}}">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-1 col-form-label"
                                       for="to_value">Email:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control"
                                           name="email" value="{{$debtor_data['email']}}">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Send</button>
                                    <a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END Your Block -->
    </div>
    <!-- END Page Content -->
@endsection
