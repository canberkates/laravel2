@extends('layouts.backend')

@include('scripts.select2')

@section('content')
    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="block block-rounded block-bordered">

                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    {{$error}}<br>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="block-content">
                        <form method="post" action="{{action('WebsiteFormController@store')}}">
                            @csrf
                            <input name="_method" type="hidden" value="post">
                            <input name="website_id" type="hidden" value="{{$website_id}}">

                            <h2 class="content-heading pt-0">General</h2>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-2 col-form-label" for="link">Name:</label>
                                <div class="col-sm-4">
                                    <input id="name" type="text" class="form-control" name="name" value="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-2 col-form-label" for="portal">Portal:</label>
                                <div class="col-sm-4">
                                    <select id="portal" class="js-select2 form-control" name="portal" data-placeholder="Choose one..">
                                        <option value=""></option>
                                        @foreach($portals as $portal)
                                            <option value="{{$portal->po_id}}">{{$portal->po_portal}} @if($portal->country) ({{$portal->country->co_en}}) @endif</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <h2 class="content-heading pt-0">Form</h2>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-2 col-form-label" for="language">Language:</label>
                                <div class="col-sm-4">
                                    <select id="language" class="js-select2 form-control" name="language" data-placeholder="Choose one..">
                                        <option value=""></option>
                                        @foreach($languages as $language)
                                            <option value="{{$language->la_code}}">{{$language->la_language}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-2 col-form-label" for="volume">Volume:</label>
                                <div class="col-sm-4">
                                    <select id="volume" class="form-control" name="volume" data-placeholder="Choose one..">
                                        <option value=""></option>
                                        @foreach($volumes as $id => $volume)
                                            <option value="{{$id}}">{{$volume}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <h2 class="content-heading pt-0">Google</h2>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-2 col-form-label" for="link">Analytics UA code:</label>
                                <div class="col-sm-4">
                                    <input id="analytics" type="text" class="form-control" name="analytics" value="">
                                </div>
                                <label class="col-sm-2 col-form-label">UA-XXXXXXXX-X</label>
                            </div>

                            <h2 class="content-heading pt-0">Ads</h2>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label"
                                       for="facebook">Facebook form</label>
                                <div class="col-sm-4">
                                    <div
                                        class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                        <input type="checkbox" class="custom-control-input" id="facebook"
                                               name="facebook">
                                        <label class="custom-control-label" for="facebook"></label>
                                    </div>
                                </div>
                            </div>

                            <h2 class="content-heading pt-0"></h2>

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Create form</button>
                                </div>
                            </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@push('scripts')
    <script>
        $(document).ready(function () {

        });
    </script>
@endpush
