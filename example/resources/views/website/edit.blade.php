@extends('layouts.backend')

@include('scripts.datatables')
@include('scripts.select2')

@section('content')

    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Edit {{$website->we_website}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url("admin/websites")}}">Websites</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <div class="content">

        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        {{$error}}<br>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="row justify-content-center">
            <div class="col-md-12">


                <div class="block block-rounded block-bordered">
                    <ul class="nav nav-tabs nav-tabs-block js-tabs-enabled" data-toggle="tabs"
                        role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active show" href="#btabs-static-general">General</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-static-forms">Forms</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#btabs-static-subdomains">Subdomains</a>
                        </li>
                    </ul>
                    <div class="block-content tab-content">
                        <div class="tab-pane active show" id="btabs-static-general" role="tabpanel">
                            <div class="block block-rounded block-bordered">
                                <div class="block-content">
                                    <form class="mb-5" method="post" action="{{action('WebsiteController@update', $website->we_id)}}">
                                        @csrf
                                        <input name="_method" type="hidden" value="PATCH">

                                        <h2 class="content-heading pt-0">General</h2>

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-3 col-form-label" for="value">Website:</label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" name="name" value="{{$website->we_website}}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-3 col-form-label" for="extra">Mail from name:</label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" name="delay" value="{{$website->we_website}}">
                                            </div>
                                        </div>

                                        <h2 class="content-heading pt-0">Sirelo</h2>

                                        <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <label class="col-sm-3 col-form-label"
                                                   for="sirelo_check">Sirelo</label>
                                            <div class="col-sm-7">
                                                <div
                                                    class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                                    <input type="checkbox" class="custom-control-input" id="sirelo_check"
                                                           name="sirelo_check" {{($website->we_sirelo ? "checked" : "")}}>
                                                    <label class="custom-control-label" for="sirelo_check"></label>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="sirelo" style="display: @if($website->we_sirelo) block; @else none; @endif">

                                            <div class="form-group row">
                                                <div class="col-sm-1"></div>
                                                <label class="col-sm-3 col-form-label"
                                                       for="request_double">Active</label>
                                                <div class="col-sm-7">
                                                    <div
                                                        class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                                        <input type="checkbox" class="custom-control-input" id="request_double"
                                                               name="request_double" {{($website->we_sirelo ? "checked" : "")}}>
                                                        <label class="custom-control-label" for="request_double"></label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-sm-1"></div>
                                                <label class="col-sm-3 col-form-label"
                                                       for="request_double">Sirelo Bucket</label>
                                                <div class="col-sm-7">
                                                    <div
                                                        class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                                        <input type="checkbox" class="custom-control-input" id="request_double"
                                                               name="request_double" {{($website->we_sirelo ? "checked" : "")}}>
                                                        <label class="custom-control-label" for="request_double"></label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-sm-1"></div>
                                                <label class="col-sm-3 col-form-label" for="for-country">Country:</label>
                                                <div class="col-sm-3">
                                                    <select id="for-country" class="js-select2 form-control" name="country" data-placeholder="Choose one..">
                                                        <option value=""></option>
                                                        @foreach($countries as $code => $country)
                                                            <option value="{{$code}}" {{ ($website->we_sirelo_co_code == $code) ? 'selected':'' }}>{{$country}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-sm-1"></div>
                                                <label class="col-sm-3 col-form-label" for="value">Language:</label>
                                                <div class="col-sm-3">
                                                    <input type="text" class="form-control" name="name" disabled value="{{$website->language->la_language}}">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-sm-1"></div>
                                                <label class="col-sm-3 col-form-label" for="value">URL:</label>
                                                <div class="col-sm-3">
                                                    <input type="text" class="form-control" name="name" value="{{$website->we_sirelo_url}}">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-sm-1"></div>
                                                <label class="col-sm-3 col-form-label" for="value">Top mover months:</label>
                                                <div class="col-sm-3">
                                                    <input type="text" class="form-control" name="name" value="{{$website->we_sirelo_top_mover_months}}">
                                                </div>
                                            </div>

                                            <h2 class="content-heading pt-0">Multilingual</h2>

                                            <div class="form-group row">
                                                <div class="col-sm-1"></div>
                                                <label class="col-sm-3 col-form-label" for="for-country">Languages:</label>
                                                <div class="col-sm-4">
                                                    <select id="for-country" class="js-select2 form-control" name="country" multiple data-placeholder="Choose multiple..">
                                                        <option value=""></option>
                                                        @foreach($languages as $code => $language)
                                                            <option value="{{$code}}">{{$language}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <h2 class="content-heading pt-0">FTP</h2>

                                            <div class="form-group row">
                                                <div class="col-sm-1"></div>
                                                <label class="col-sm-3 col-form-label" for="value">FTP server:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="name" value="{{$website->we_sirelo_ftp_server}}">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-sm-1"></div>
                                                <label class="col-sm-3 col-form-label" for="value">FTP username:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="name" value="{{$website->we_sirelo_ftp_username}}">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-sm-1"></div>
                                                <label class="col-sm-3 col-form-label" for="value">FTP password:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="name" value="{{$website->we_sirelo_ftp_password}}">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-sm-1"></div>
                                                <label class="col-sm-3 col-form-label" for="value">FTP folder:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="name" value="{{$website->we_sirelo_ftp_folder}}">
                                                </div>
                                            </div>

                                            <h2 class="content-heading pt-0">Pages</h2>

                                            <div class="form-group row">
                                                <div class="col-sm-1"></div>
                                                <label class="col-sm-3 col-form-label" for="value">Customer location:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" disabled class="form-control" name="name" value="{{$website->we_sirelo_customer_location}}">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-sm-1"></div>
                                                <label class="col-sm-3 col-form-label" for="value">Customer review slug:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" disabled class="form-control" name="name" value="{{$website->we_sirelo_customer_review_slug}}">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-sm-1"></div>
                                                <label class="col-sm-3 col-form-label" for="value">Partner program location:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" disabled class="form-control" name="name" value="{{$website->we_sirelo_partner_program_location}}">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-sm-1"></div>
                                                <label class="col-sm-3 col-form-label" for="value">Surveys location:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" disabled class="form-control" name="name" value="{{$website->we_sirelo_survey_location}}">
                                                </div>
                                            </div>

                                        </div>


                                        <div class="row">
                                            <div class="col-md-1"></div>
                                            <div class="form-group col-md-8">
                                                <button type="submit" class="btn btn-primary">Update</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="btabs-static-forms" role="tabpanel">
                            <div class="block block-rounded block-bordered">
                                <div class="block-content block-content-full">
                                    <a href="{{ url('admin/websites/' . $website->we_id . '/forms/create')}}"><button data-toggle="click-ripple" class="btn btn-primary" style="margin-bottom: 10px;">Add a Form</button></a>
                                    <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Token</th>
                                            <th>Name</th>
                                            <th>Portal</th>
                                            <th>Language</th>
                                            <th>Volume type</th>
                                            <th>Analytics UA code</th>
                                            <th>Modify</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($website->websiteforms as $form)
                                            <tr>
                                                <td>{{$form->wefo_id}}</td>
                                                <td>{{$form->wefo_token}}</td>
                                                <td>{{$form->wefo_name}}</td>
                                                <td>{{$form->portal->po_portal}} ({{$form->portal->country->co_en}})</td>
                                                <td>{{$form->language->la_language}}</td>
                                                <td>{{$volumetypes[$form->wefo_volume_type]}}</td>
                                                <td>{{$form->wefo_analytics_ua_code}}</td>
                                                <td class="text-center">
                                                    <div class="btn-group">
                                                        <a class="btn btn-sm btn-primary"
                                                           data-toggle="tooltip"
                                                           data-placement="left"
                                                           title="edit"
                                                           href="{{ url('admin/websites/' . $website->we_id . '/forms/' . $form->wefo_id.'/edit')}}">
                                                            <i class="fa fa-pencil-alt"></i>
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="btabs-static-subdomains" role="tabpanel">
                            <div class="block block-rounded block-bordered">
                                <div class="block-content block-content-full">
                                    <a href="{{ url('admin/websites/' . $website->we_id . '/subdomain/create')}}"><button data-toggle="click-ripple" class="btn btn-primary" style="margin-bottom: 10px;">Add a Subdomain</button></a>

                                    <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>URL</th>
                                            <th>Country</th>
                                            <th>Default language</th>
                                            <th>Languages</th>
                                            <th>Modify</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($website->subdomains as $domain)
                                            <tr>
                                                <td>{{$domain->wesu_id}}</td>
                                                <td>{{$domain->wesu_name}}</td>
                                                <td>{{$domain->wesu_url}}</td>
                                                <td>{{$domain->country->co_en}}</td>
                                                <td>{{$domain->language->la_language}}</td>
                                                <td>{{$domain->languages}}</td>
                                                <td class="text-center">
                                                    <div class="btn-group">
                                                        <a class="btn btn-sm btn-primary"
                                                           data-toggle="tooltip"
                                                           data-placement="left"
                                                           title="edit"
                                                           href="{{ url('customers/' . $customer->cu_id . '/customerportal/' . $customerportal->ktcupo_id.'/edit')}}">
                                                            <i class="fa fa-pencil-alt"></i>
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


@endsection

@push('scripts')
    <script>
        jQuery( document ).on( 'click', '#sirelo_check', function(e) {
            $('#sirelo').toggle();
        });
    </script>
@endpush


