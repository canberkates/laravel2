@extends('layouts.backend')

@include( 'scripts.datatables' )

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Websites</h1>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <a href="/admin/newsletters/create"></a>

    <div class="content">

        <div class="block block-rounded block-bordered">
            <div class="block-content block-content-full">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Newsletters
                        <a href="{{ url('admin/website/create')}}"><button data-toggle="click-ripple" class="btn btn-primary">Add a Website</button></a>
                    </h3>
                </div>
                <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Website</th>
                        <th>Mail from name</th>
                        <th>Sirelo</th>
                        <th>Modify</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($websites as $website)
                        <tr>
                            <td>{{$website->we_id}}</td>
                            <td>{{$website->we_website}}</td>
                            <td>{{$website->we_mail_from_name}}</td>
                            @if($website->we_sirelo)
                                <td><i class="fa fa-check-circle" style="color:green;"></i></td>
                            @else
                               <td><i class="fa fa-times" style="color:red;"></i></td>
                            @endif
                            <td class="text-center">
                                <div class="btn-group">
                                    <a class="btn btn-sm btn-primary"
                                       data-toggle="tooltip"
                                       data-placement="left"
                                       title="edit"
                                       href="{{ url('/admin/websites/' . $website->we_id . '/edit')}}">
                                        <i class="fa fa-pencil-alt"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
