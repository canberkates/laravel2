@extends('layouts.backend')

@include( 'scripts.datatables' )
@include( 'scripts.datepicker' )
@include( 'scripts.dialogs' )
@include('scripts.select2')

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Service Provider Requests</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/requests">Requests</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Service Provider Requests</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="col-md-6 block block-rounded block-bordered">
            <div class="block-content">
                <form method="post" action="{{action('ServiceProviderRequestController@filteredindex')}}">
                @csrf
                <input name="_method" type="hidden" value="post">
                <h2 class="content-heading pt-0">Filters</h2>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="customer">Service provider:</label>
                    <div class="col-md-7">
                        <select id="customer" class="js-select2 form-control" name="customer" data-placeholder="Choose one.." style="width:100%;">
                            <option></option>
                            @foreach($customers as $customer)
                                <option value="{{$customer->cu_id}}" {{$customer->cu_id == $selected_customer ? "selected" : ""}}>{{$customer->cu_company_name_business}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="for-type">Rejection status:</label>
                    <div class="col-md-7">
                        <select id="for-type" class="form-control {{$errors->has('type') ? 'is-invalid' : ''}}" name="status" data-placeholder="Choose one.." style="width:100%;" data-minimum-results-for-search="Infinity">
                            <option @if(isset($filtered) && $filtered == true && $selected_status < 0) selected @endif></option>
                            @foreach($statuses as $id => $status)
                                <option value="{{$id}}" @if (isset($filtered) && $filtered == true) @if ($selected_status != ""){{$id == $selected_status ? "selected" : ""}}@endif @else @if(isset($filtered) && $filtered == false && $id == 0) {{"selected"}} @endif @endif>{{$status}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="for-date">Date range:</label>
                    <div class="col-md-7">
                        <input type="text" class="form-control drp drp-default" name="date" id="for-date"
                               value="{{$selected_date}}" autocomplete="off"/>
                    </div>
                </div>

                <h2 class="content-heading pt-0"></h2>

                <div class="row">
                    <div class="form-group col-md-2">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <!-- Your Block -->
        <div class="block block-rounded block-bordered">
            <div class="block-content block-content-full">
                <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Submitted</th>
                        <th>Service provider</th>
                        <th>Rejection reason</th>
                        <th>Rejection status</th>
                        <th>Question</th>
                        <th>Answer 1</th>
                        <th>Answer 2</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Country from</th>
                        <th>Country to</th>
                        <th>Amount</th>
                        <th>Origin</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($requests as $request)
                        <tr data-id="{{$request->ktrecuqu_id}}">
                            <td>{{$request->ktrecuqu_id}}</td>
                            <td>{{$request->ktrecuqu_timestamp}}</td>
                            <td>{{$request->customer->cu_company_name_business}}</td>
                            <td>{{$rejectionreasons[$request->request->re_rejection_reason][0]}}</td>
                            <td>
                                <select id="for-type" class="form-control" name="rejection_status">
                                    <option></option>
                                    @foreach($statuses as $id => $status)
                                        <option value="{{$id}}" {{$id === $request->ktrecuqu_rejection_status ? "selected" : ""}}>{{$status}}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td>{{$request->question->qu_name}}</td>
                            <td>{{$request->ktrecuqu_answer_1}}</td>
                            <td>{{$request->ktrecuqu_answer_2}}</td>
                            <td>{{$request->request->re_full_name}}</td>
                            <td>{{$request->request->re_email}}</td>
                            <td>{{$countries[$request->request->re_co_code_from]}}</td>
                            <td>{{$countries[$request->request->re_co_code_to]}}</td>
                            <td>{{$request->ktrecuqu_currency}} {{$request->ktrecuqu_amount}}</td>
                            @if($request->request->re_source == 1)
                               <td>{{$request->request->websiteform->website->we_website}}</td>
                            @else
                                <td>{{$request->request->affiliateform->afpafo_name}}</td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Your Block -->
    </div>
    <!-- END Page Content -->
@endsection

@push( 'scripts' )
<script>
    $(function(){
        $("select[name=rejection_status]").change(function(){

            //So this is available in the success-function
            $this = $(this);

            $this.attr("disabled", "disabled");

            $.ajax({
                type: "GET",
                url: '{{ url('/ajax/updateserviceproviderrequest') }}',
                data: {"id": $this.parent().parent().data("id"), "rejection_status": $this.val()},
                success: function(data){
                    $this.removeAttr("disabled");
                }
            });
        });
    });
</script>
@endpush
