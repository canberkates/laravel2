@extends('layouts.backend')

@include('scripts.datepicker')

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Add a Customer Portal Status to a Portal of {{$customer->cu_company_name_business}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../../../../">@if ($customer->cu_type == 1) {{"Customers"}} @elseif($customer->cu_type == 6) {{"Resellers"}}  @elseif($customer->cu_type == 2) {{"Service Providers"}} @else {{"Affiliate Partners"}} @endif</a></li>
                        <li class="breadcrumb-item"><a href="../../../edit">{{$customer->cu_company_name_business}}</a></li>
                        <li class="breadcrumb-item"><a href="../edit">Portal</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Add Portal Status</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->


    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="block block-rounded block-bordered">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">General</h3>
                    </div>
                    <div class="block-content">
                        <form class="mb-5" method="post" action="{{action('CustomerPortalStatusController@store')}}">
                                @csrf
                                <input name="_method" type="hidden" value="post">
                                <input name="ktcupo_id" type="hidden" value="{{$portal->ktcupo_id}}">

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="status">Status:</label>
                                <div class="col-sm-3">
                                    <select type="text" class="form-control" name="status">
                                        <option value=""></option>
                                        @foreach($portalstatuses as $id => $status)
                                            <option value="{{$id}}">{{$status}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="date">Date:</label>
                                <div class="col-sm-3">
                                    <input autocomplete="off" type="text" class="js-datepicker form-control" id="example-datepicker3" name="date" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="hour">Hour:</label>
                                <div class="col-sm-3">
                                    <select type="text" class="form-control" name="hour">
                                        <option value=""></option>
                                        @foreach($hour_dropdown as $id => $value)
                                            <option value="{{$id}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="description">Description:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="description"
                                           value="">
                                </div>
                            </div>

                            <h2 class="content-heading pt-0">Select the users that need to receive a message when the status has been updated.</h2>

                            @foreach($users as $user)
                                <div class="row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="users[{{$user->id}}]">{{$user->us_name}}</label>
                                    <div class="col-sm-7">
                                        <div class="custom-control custom-switch custom-control-inline custom-control-primary">
                                            <input type="checkbox" class="custom-control-input"
                                                   id="users[{{$user->id}}]"
                                                   name="users[{{$user->id}}]"
                                                   @if($user->id == \Illuminate\Support\Facades\Auth::id()) {{"checked"}}  @endif>
                                            <label class="custom-control-label"
                                                   for="users[{{$user->id}}]"></label>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                            <h2 class="content-heading pt-0"></h2>


                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Create</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection




