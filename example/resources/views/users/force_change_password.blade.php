@extends('layouts.backend')

@section('content')
    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="block block-rounded block-bordered">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Time to change your password!</h3>
                    </div>
                    <div class="block-content">
                        @if($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        {{$error}}<br>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form class="mb-5" method="post" action="{{action('UserController@forcePasswordChangePost', $user->us_id)}}">
                            @csrf

                            <h2 class="content-heading pt-0">Change Password</h2>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <div class="col-sm-6">
                                    <b>You have to change your password for security reasons.</b> <br /><br />

                                    When changing your password, it will be checked whether it appears in the list of frequently used passwords. <br /><br />

                                    It's not allowed to take a password which is in our list of most used passwords.<br />

                                    Thats why you're forced to change the password of our ERP.<br /><br />

                                    Kind regards,<br /><br />

                                    IT Department
                                </div>
                            </div>
                            <h2 class="content-heading pt-0"></h2>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="value">New password:</label>
                                <div class="col-sm-7">
                                    <input type="password" class="form-control" name="password" value="">
                                </div>
                            </div>

                            <h2 class="content-heading pt-0"></h2>

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Change</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection




