@extends('layouts.backend')

@include('scripts.datepicker')

@section('content')
    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="block block-rounded block-bordered">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Add user</h3>
                    </div>
                    <div class="block-content">
                        <form class="mb-5" method="post" action="{{action('UserController@store')}}">
                            @csrf

                            <h2 class="content-heading pt-0">General</h2>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="value">Username:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="username">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="value">Password:</label>
                                <div class="col-sm-7">
                                    <input type="password" class="form-control" name="password">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="cafe_planner">Café planner:</label>
                                <div class="col-sm-7">
                                    <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                        <input type="checkbox" class="custom-control-input" id="cafe_planner" name="cafe_planner" checked="checked">
                                        <label class="custom-control-label" for="cafe_planner" ></label>
                                    </div>
                                </div>
                            </div>

                            <h2 class="content-heading pt-0">Authenticator</h2>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="bypass_authenticator">Bypass Authenticator</label>
                                <div class="col-sm-7">
                                    <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                        <input type="checkbox" class="custom-control-input" id="bypass_authenticator" name="bypass_authenticator">
                                        <label class="custom-control-label" for="bypass_authenticator" ></label>
                                    </div>
                                </div>
                            </div>

                            <h2 class="content-heading pt-0">Details</h2>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="value">Name:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="name">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="value">Email:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="email">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="value">Date of birth:</label>
                                <div class="col-sm-7">
                                    <input autocomplete="off" type="text"
                                           class="js-datepicker form-control" id="example-datepicker3" name="date_of_birth"
                                           data-week-start="1" data-autoclose="true" data-today-highlight="true"
                                           data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd">
                                </div>
                            </div>


                            <h2 class="content-heading pt-0">Telephone</h2>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="value">Telephone internal:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="telephone_internal">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="value">Telephone external:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="telephone_external">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="value">Mobile:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="mobile">
                                </div>
                            </div>


                            <h2 class="content-heading pt-0">Departments</h2>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="department_sales">Sales</label>
                                <div class="col-sm-7">
                                    <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                        <input type="checkbox" class="custom-control-input" id="department_sales" name="department_sales">
                                        <label class="custom-control-label" for="department_sales" ></label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="department_partnerdesk">Partnerdesk</label>
                                <div class="col-sm-7">
                                    <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                        <input type="checkbox" class="custom-control-input" id="department_partnerdesk" name="department_partnerdesk">
                                        <label class="custom-control-label" for="department_partnerdesk" ></label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="department_customerservice">Customerservice</label>
                                <div class="col-sm-7">
                                    <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                        <input type="checkbox" class="custom-control-input" id="department_customerservice" name="department_customerservice">
                                        <label class="custom-control-label" for="department_customerservice" ></label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="department_finance">Finance</label>
                                <div class="col-sm-7">
                                    <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                        <input type="checkbox" class="custom-control-input" id="department_finance" name="department_finance">
                                        <label class="custom-control-label" for="department_finance" ></label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-1"></div>
                                <label class="col-sm-3 col-form-label" for="department_sirelo">Sirelo</label>
                                <div class="col-sm-7">
                                    <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                        <input type="checkbox" class="custom-control-input" id="department_sirelo" name="department_sirelo">
                                        <label class="custom-control-label" for="department_sirelo" ></label>
                                    </div>
                                </div>
                            </div>

                            <h2 class="content-heading pt-0">Permission</h2>

                            @foreach($permissions as $permission)
                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="permission[{{$permission->id}}]">{{$permission->name}}</label>
                                    <div class="col-sm-7">
                                        <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                            <input type="checkbox" class="custom-control-input" id="permission[{{$permission->id}}]" name="permission[{{$permission->id}}]">
                                            <label class="custom-control-label" for="permission[{{$permission->id}}]" ></label>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                            <h2 class="content-heading pt-0">Reports</h2>

                            @foreach($reports as $report)
                                <div class="form-group row">
                                    <div class="col-sm-1"></div>
                                    <label class="col-sm-3 col-form-label" for="report[{{$report->rep_id}}]">{{$report->rep_name}}</label>
                                    <div class="col-sm-7">
                                        <div class="custom-control custom-switch custom-control-lg custom-control-inline custom-control-primary">
                                            <input type="checkbox" class="custom-control-input" id="report[{{$report->rep_id}}]" name="report[{{$report->rep_id}}]">
                                            <label class="custom-control-label" for="report[{{$report->rep_id}}]" ></label>
                                        </div>
                                    </div>
                                </div>
                            @endforeach


                            <h2 class="content-heading pt-0"></h2>

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-primary">Create</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection




