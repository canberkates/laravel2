@extends('layouts.backend')

@include( 'scripts.datatables' )
@include( 'scripts.datepicker' )
@include('scripts.select2')

@section('content')
	<!-- Hero -->
	<div class="bg-body-light">
		<div class="content content-full">
			<div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
				<h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Customer search</h1>
			</div>
		</div>
	</div>
	<!-- END Hero -->

	<div class="content">
		<form method="post" action="{{action('CustomerSearchController@searchResult')}}">
			@csrf
			<input name="_method" type="hidden" value="post">
			<div class="col-md-6 block block-rounded block-bordered">
				<div class="block-content">
					<input name="_method" type="hidden" value="post">
					<h2 class="content-heading pt-0">Filters</h2>

                    <div class="form-group row">
                        <div class="col-sm-1"></div>
                        <label class="col-sm-3 col-form-label"
                               for="finance_payment_currency">Country:</label>
                        <div class="col-sm-7">
                            <select type="text" class="js-select2 form-control"
                                  multiple  name="countries[]">
                                <option value=""></option>
                                @foreach($countries as $country)
                                    <option value="{{$country->co_code}}" @if(in_array($country->co_code, $selected_countries)) selected @endif >{{$country->co_en}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-1"></div>
                        <label class="col-sm-3 col-form-label"
                               for="finance_payment_currency">Region:</label>
                        <div class="col-sm-7">
                            <select type="text" class="js-select2 form-control"
                                   multiple name="regions[]">
                                <option value=""></option>
                                @foreach($regions as $region)
                                    <option
                                        value="{{$region->reg_id}}"  @if(in_array($region->reg_id, $selected_regions)) selected @endif >{{$region->reg_parent}} {{"(".$region->co_en.")"}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-1"></div>
                        <label class="col-sm-3 col-form-label"
                               for="finance_payment_currency">Market:</label>
                        <div class="col-sm-7">
                            <select type="text" class="form-control"
                                    name="market">
                                <option value=""></option>
                                @foreach($customermarkettypes as $id => $markettype)
                                    <option value="{{$id}}" @if($id == $selected_market) selected @endif>{{$markettype}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-1"></div>
                        <label class="col-sm-3 col-form-label"
                               for="fields_services">Services:</label>
                        <div class="col-sm-7">
                            <select multiple type="text" class="js-select2 form-control"
                                    name="services[]">
                                <option value=""></option>
                                @foreach($customerservices as $id => $service)
                                    <option value="{{$id}}" @if(in_array($id, $selected_services)) selected @endif >{{$service}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-1"></div>
                        <label class="col-sm-3 col-form-label"
                               for="finance_payment_currency">Status:</label>
                        <div class="col-sm-7">
                            <select multiple type="text" class="js-select2 form-control"
                                    name="statuses[]">
                                <option value=""></option>
                                @foreach($customerstatustypes as $id => $statustype)
                                    <option value="{{$id}}"  @if(in_array($id, $selected_statuses)) selected @endif >{{$statustype}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-1"></div>
                        <label class="col-sm-3 col-form-label"
                               for="finance_payment_currency">Progress:</label>
                        <div class="col-sm-7">
                            <select type="text" class="js-select2 form-control"
                                    name="progress">
                                <option value=""></option>
                                @foreach($progresstypes as $id => $progresstype)
                                    <option value="{{$id}}"  @if($id == $selected_progress) selected @endif >{{$progresstype}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-1"></div>
                        <label class="col-sm-3 col-form-label"
                               for="finance_payment_currency">Progress timestamp:</label>
                        <div class="col-md-7">
                            <input type="text" class="form-control drp drp-default" name="date" @if(isset($date_filter) && $date_filter != null) value="{{$date_filter}}" @endif autocomplete="off"/>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-1"></div>
                        <label class="col-sm-3 col-form-label"
                               for="finance_payment_currency">Owner:</label>
                        <div class="col-sm-7">
                            <select multiple type="text" class="js-select2 form-control"
                                    name="owners[]">
                                <option value=""></option>
                                @foreach($users as $user)
                                    <option value="{{$user->us_id}}"  @if(in_array($user->us_id, $selected_owners)) selected @endif >{{$user->us_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-1"></div>
                        <label class="col-sm-3 col-form-label"
                               for="finance_payment_currency">Associations:</label>
                        <div class="col-sm-7">
                            <select type="text" class="js-select2 form-control"
                                    multiple name="associations[]">
                                <option value=""></option>
                                @foreach($associations as $association)
                                    <option value="{{$association->me_id}}"  @if(in_array($association->me_id, $selected_associations)) selected @endif>{{$association->me_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-1"></div>
                        <label class="col-sm-3 col-form-label" for="for-call_date_before">Last call date before:</label>
                        <div class="col-sm-7">
                            <input autocomplete="off" value="{{$selected_call_before_date}}" type="text"
                                   class="js-datepicker form-control" id="for-call_date_before" name="call_date_before"
                                   data-week-start="1" data-autoclose="true" data-today-highlight="true"
                                   data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd">
                        </div>
                    </div>

					<div class="row">
						<div class="form-group col-md-2">
							<button type="submit" class="btn btn-primary">Filter</button>
						</div>
					</div>
				</div>
			</div>

		</form>

		@isset($customers)
			<div class="block block-rounded block-bordered">
				<div class="block-content">
                    <div class="block block-rounded block-bordered">
							<div class="block-content block-content-full">
								<table class="table table-bordered table-striped table-vcenter js-dataTable-full">
									<thead>
										<tr>
											<th>ID</th>
											<th>Name</th>
											<th>Country</th>
											<th>Owner</th>
											<th>Timestamp</th>
											<th>Last Call</th>
											<th>Next Call</th>
                                            <th>Edit</th>
										</tr>
									</thead>
									<tbody>
									@foreach ($customers as $customer)
										<tr>
											<td>{{$customer->cu_id}}</td>
											<td>{{$customer->cu_company_name_business}}</td>
											<td>{{$customer->co_en}}</td>
											<td>{{$customer->moverdata->owner->us_name}}</td>
											<td>{{$customer->progress_timestamp}}</td>
											<td>{{$customer->last_call}}</td>
											<td>{{$customer->next_call}}</td>
											<td class="text-center">
												<div class="btn-group">
													<a class="btn btn-sm btn-primary"
													   data-toggle="tooltip"
													   data-placement="left"
													   title="Edit request"
													   href="{{ url('customers/' . $customer->cu_id . '/edit')}}">
														<i class="fa fa-pencil-alt"></i>
													</a>
												</div>
											</td>
										</tr>
									@endforeach
									</tbody>
								</table>
							</div>
						</div>
				</div>
			</div>
		@endisset
	</div>
@endsection
